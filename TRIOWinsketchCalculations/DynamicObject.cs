﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Threading;

namespace TRIOWinsketchCalculations
{
    /// <summary>
    /// Custom fast implementation of a dynamic object that can handle the JSON
    /// serialization and deserialization.
    /// 
    /// This class implements IDynamicMetaObjectProvider allowing the DLR to compile the
    /// dynamic member access on the fly. We could have derived from System.Dynamic.Dynamic but
    /// the implementation in the .NET framework uses exceptions to manage custom properties.
    /// 
    /// </summary>
    [TypeConverter(typeof(DynamicObjectConverter))]
    public sealed class DynamicObject : IDynamicMetaObjectProvider, ICloneable, IList<DynamicObject.Member>, ISerializable, ICustomTypeDescriptor, INotifyPropertyChanged
    {
        // the name/object repository.
        private MetaStore store;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DynamicObject()
        {
        }

        /// <summary>
        /// Optional constructor.
        /// </summary>
        public DynamicObject(int capacity)
        {
            if (capacity > 0)
                this.store = new MetaStore(capacity);
        }

        /// <summary>
        /// Creates a new DynamicObject cloned from the source object.
        /// </summary>
        /// <param name="source"></param>
        public DynamicObject(DynamicObject source)
        {
            if (source != null)
                this.store = new MetaStore(source.store);
        }

        /// <summary>
        /// Converts javascript dates to .NET DateTime objects.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static object ParseDate(object value)
        {
            string text = value as string;

            if (text != null && text.Length > 8 && text[text.Length - 1] == 'Z')
            {
                DateTime date;
                if (DateTime.TryParse(text, out date))
                    return date;
            }
            return value;
        }

        /// <summary>
        /// Deletes the field.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Itself, this call can be chained.</returns>
        public DynamicObject Delete(string name)
        {
            this.store?.Remove(name);
            return this;
        }

        /// <summary>
        /// Returns the number of fields.
        /// </summary>
        /// <returns></returns>
        public int Count
        {
            get { return this.store?.Length ?? 0; }
        }

        /// <summary>
        /// Wipes out the object.
        /// </summary>
        /// <returns></returns>
        public void Clear()
        {
            this.store?.Clear();
        }

        /// <summary>
        /// Removes the excess entries in the inner array and in all child fields, recursively.
        /// </summary>
        public void TrimExcess()
        {
            if (this.store != null)
            {
                this.store.TrimExcess();

                foreach (var f in this.store)
                {
                    DynamicObject o = f.Value as DynamicObject;
                    if (o != null)
                        o.TrimExcess();
                }
            }
        }

        /// <summary>
        /// Sorts the field by their name.
        /// </summary>
        public void Sort()
        {
            this.store?.Sort();
        }

        /// <summary>
        /// Clones the members of the dynamic object.
        /// </summary>
        /// <remarks>This is a shallow copy. Reference members are copied by reference.</remarks>
        /// <returns>The cloned instance.</returns>
        public DynamicObject Clone()
        {
            return new DynamicObject(this);
        }

        /// <summary>
        /// Returns or sets the value of the specified member.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object this[string name]
        {
            get { return this.store?[name] ?? null; }
            set
            {
                if (this.store == null)
                    Interlocked.CompareExchange(ref this.store, new MetaStore(1), null);

                this.store[name] = value;

                // fire the PropertyChanged event.
                this._propertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Returns whether the object contains the specified field.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Contains(string name)
        {
            return (this.store?.IndexOf(name) ?? -1) > -1;
        }

        /// <summary>
        /// Tries to retrieve the value for the specified member.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns>True if the member was found, otherwise false.</returns>
        public bool TryGetValue(string name, out object value)
        {
            value = null;

            if (this.store != null)
            {
                int position = this.store.IndexOf(name);
                if (position < 0)
                    return false;

                value = this.store[position].Value;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns if the dynamic object contains any member.
        /// </summary>
        public bool IsEmpty { get { return (this.store?.Length ?? 0) == 0; } }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Member> GetEnumerator()
        {
            return new MetaStore.Enumerator(this.store);
        }

        /// <summary>
        /// Renames the field.
        /// </summary>
        /// <param name="oldNname"></param>
        /// <param name="newName"></param>
        /// <returns>True if the field was found and renamed, otherwise false.</returns>

        internal bool Rename(string oldNname, string newName)
        {
            if (this.store != null)
            {
                int index = this.store.IndexOf(oldNname);
                if (index < 0)
                    return false;

                this.store[index] = new Member(newName, this.store[index].Value);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Moves the specified member up one position in the list of fields.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if the member was moved, false if not found.</returns>
        internal bool MoveUp(string name)
        {
            if (this.store != null)
            {
                int position = this.store.IndexOf(name);
                if (position < 1)
                    return false;

                var t = this.store[position - 1];
                this.store[position - 1] = this.store[position];
                this.store[position] = t;

                return true;
            }
            return false;
        }

        /// <summary>
        /// Moves the specified member down one position in the list of fields.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>True if the member was moved, false if not found.</returns>
        internal bool MoveDown(string name)
        {
            if (this.store != null)
            {
                int position = this.store.IndexOf(name);
                if (position == (this.Count - 1))
                    return false;

                var t = this.store[position + 1];
                this.store[position + 1] = this.store[position];
                this.store[position] = t;

                return true;
            }
            return false;
        }

        #region ICollection, IList

        Member IList<Member>.this[int index]
        {
            get { return this.store[index]; }
            set { this.store[index] = value; }
        }

        bool ICollection<Member>.IsReadOnly
        {
            get { return false; }
        }

        int IList<Member>.IndexOf(Member item)
        {
            return this.store.IndexOf(item.Name);
        }

        void IList<Member>.Insert(int index, Member item)
        {
            throw new NotSupportedException();
        }

        void IList<Member>.RemoveAt(int index)
        {
            this.store.RemoveAt(index);
        }

        void ICollection<Member>.Add(Member item)
        {
            this[item.Name] = item.Value;
        }

        bool ICollection<Member>.Contains(Member item)
        {
            return this.store.IndexOf(item.Name) > -1;
        }

        void ICollection<Member>.CopyTo(Member[] array, int index)
        {
            this.store.CopyTo(array, index);
        }

        bool ICollection<Member>.Remove(Member item)
        {
            int index = this.store.IndexOf(item.Name);
            if (index < 0)
                return false;

            this.store.RemoveAt(index);
            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new MetaStore.Enumerator(this.store);
        }

        #endregion

        #region Diff/Merge Implementation

        /// <summary>
        /// Merges the fields from the source object into this object.
        /// </summary>
        /// <param name="source"></param>
        internal dynamic Merge(DynamicObject source)
        {
            if (source == null)
                return this;

            if (source != null)
            {
                foreach (var field in source)
                    this[field.Name] = field.Value;
            }

            return this;
        }

        /// <summary>
        /// Returns a new ComponentConfiguration object containing only the values that
        /// don't exist in the ComponentConfiguration object passed to the method.
        /// </summary>
        /// <param name="original">previous values to diff against.</param>
        /// <returns>Returns null if there are no differences.</returns>
        internal dynamic Diff(DynamicObject original)
        {
            Debug.Assert(original != null);

            if (original == null)
                return Clone();

            // this is the new object that will contain only the differences.
            DynamicObject diff = new DynamicObject(original.Count);

            foreach (var field in this)
            {
                object origValue = null;
                object newValue = field.Value;

                // if the original object doesn't contain the field
                // add the current value to the diff, unless it's null.
                if (!original.TryGetValue(field.Name, out origValue))
                {
                    if (newValue != null)
                        diff[field.Name] = newValue;

                    continue;
                }

                // if the original value and the current value are the same, skip the field.
                // at this stage we only perform the simple Object.Equals test.
                if (Object.Equals(newValue, origValue))
                    continue;

                // if the new value is null at this point it is different from the original.
                if (newValue == null)
                {
                    diff[field.Name] = null;
                    continue;
                }

                // if the value is a primitive or a struct or a string, add it to the diff and keep going.
                Type fieldType = newValue.GetType();
                if (fieldType.IsPrimitive || fieldType.IsValueType || fieldType == typeof(string))
                {
                    diff[field.Name] = newValue;
                    continue;
                }

                // if the value is a collection, compare all the items in the collection.
                // if any item doesn't match, including the position of the item, add the entire collection to the diff.
                ICollection coll = newValue as ICollection;
                if (coll != null)
                {
                    if (IsDifferent(coll, origValue as ICollection))
                        diff[field.Name] = newValue;

                    continue;
                }

                // if the value is a collection, compare all the items in the collection.
                // if any item doesn't match, including the position of the item, add the entire collection to the diff.
                IEnumerable enu = newValue as IEnumerable;
                if (enu != null)
                {
                    if (IsDifferent(enu, origValue as IEnumerable))
                        diff[field.Name] = newValue;

                    continue;
                }

                // if the value is an object, compare all the fields and
                // if any field doesn't match, add the entire object to the diff.
                if (IsDifferent(newValue, origValue))
                    diff[field.Name] = newValue;
            }

            diff.TrimExcess();

            if (diff.Count == 0)
                return null;

            return diff;
        }

        /// <summary>
        /// Returns true if the specified dynamic object contains the
        /// same values.
        /// </summary>
        /// <param name="original">previous values to diff against.</param>
        /// <returns>Returns false if there are no differences.</returns>
        internal bool IsEqual(DynamicObject original)
        {
            if (original == null)
                return false;

            if (this.Count != original.Count)
                return false;

            foreach (var field in this)
            {
                object origValue = null;
                object newValue = field.Value;

                // if the original object doesn't contain the field
                // add the current value to the diff.
                if (!original.TryGetValue(field.Name, out origValue))
                    return false;

                // if the original value and the current value are the same, skip the field.
                // at this stage we only perform the simple Object.Equals test.
                if (Object.Equals(newValue, origValue))
                    continue;

                // if the new value is null at this point it is different from the original.
                if (newValue == null)
                    return false;

                // if the value is a primitive or a struct or a string, add it to the diff and keep going.
                Type fieldType = newValue.GetType();
                if (fieldType.IsPrimitive || fieldType.IsValueType || fieldType == typeof(string))
                    return false;

                // if the value is a collection, compare all the items in the collection.
                // if any item doesn't match, including the position of the item, add the entire collection to the diff.
                if (newValue is ICollection)
                {
                    if (IsDifferent((ICollection)newValue, origValue as ICollection))
                        return false;

                    continue;
                }

                // if the value is an object, compare all the fields and
                // if any field doesn't match, add the entire object to the diff.
                if (IsDifferent(newValue, origValue))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Compares two collections.
        /// </summary>
        /// <param name="coll1"></param>
        /// <param name="coll2"></param>
        /// <returns></returns>
        private bool IsDifferent(ICollection coll1, ICollection coll2)
        {
            if (coll1 == null || coll2 == null)
                return coll1 != coll2;

            if (coll1.Count != coll2.Count)
                return true;

            IEnumerator enum1 = coll1.GetEnumerator();
            IEnumerator enum2 = coll2.GetEnumerator();
            if (enum1 == null || enum2 == null)
                return enum1 != enum2;
            try
            {
                while (enum1.MoveNext() && enum2.MoveNext())
                {
                    if (IsDifferent(enum1.Current, enum2.Current))
                        return true;
                }
            }
            catch (Exception ex)
            {
                Debug.Assert(false);
            }

            return false;
        }

        /// <summary>
        /// Compares two entries.
        /// </summary>
        /// <param name="member1"></param>
        /// <param name="member2"></param>
        /// <returns></returns>
        private bool IsDifferent(Member member1, Member member2)
        {
            if (member1.Name != member2.Name)
                return true;

            return Object.Equals(member1.Value, member2.Value);
        }

        /// <summary>
        /// Compares two enumerable values.
        /// </summary>
        /// <param name="coll1"></param>
        /// <param name="coll2"></param>
        /// <returns></returns>
        private bool IsDifferent(IEnumerable coll1, IEnumerable coll2)
        {
            if (coll1 == null || coll2 == null)
                return coll1 != coll2;

            IEnumerator enum1 = coll1.GetEnumerator();
            IEnumerator enum2 = coll2.GetEnumerator();
            if (enum1 == null || enum2 == null)
                return enum1 != enum2;

            try
            {
                while (enum1.MoveNext() && enum2.MoveNext())
                {
                    if (IsDifferent(enum1.Current, enum2.Current))
                        return true;
                }
            }
            catch (Exception ex)
            {
                Debug.Assert(false);
            }

            // if either one of the enumerations still has
            // an entry, it means that the number of items is different.
            try
            {
                if (enum1.MoveNext() || enum2.MoveNext())
                    return true;
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Compares two objects.
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        private bool IsDifferent(object obj1, object obj2)
        {
            if (obj1 == null || obj2 == null)
                return obj1 != obj2;

            Type objType = obj1.GetType();

            if (objType != obj2.GetType())
                return true;

            if (objType.IsPrimitive || objType == typeof(string))
                return !Object.Equals(obj1, obj2);

            if (objType == typeof(Member))
                return IsDifferent((Member)obj1, (Member)obj2);

            if (objType.IsValueType)
                return !Object.Equals(obj1, obj2);

            DynamicObject d1 = obj1 as DynamicObject;
            if (d1 != null)
                return d1.Diff(obj2 as DynamicObject) != null;

            ICollection c1 = obj1 as ICollection;
            if (c1 != null)
                return IsDifferent(c1, obj2 as ICollection);

            IEnumerable e1 = obj1 as IEnumerable;
            if (e1 != null)
                return IsDifferent(e1, obj2 as IEnumerable);

            //IWisejComponent w1 = obj1 as IWisejComponent;
            //if (w1 != null)
            //    return w1.Id != ((IWisejComponent)obj2).Id;

            FieldInfo[] fields = objType.GetFields(BindingFlags.Public | BindingFlags.Instance);

            foreach (var f in fields)
            {
                object val1 = f.GetValue(obj1);
                object val2 = f.GetValue(obj2);

                if (IsDifferent(val1, val2))
                    return true;
            }

            PropertyInfo[] properties = objType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var p in properties)
            {
                object val1 = p.GetValue(obj1, null);
                object val2 = p.GetValue(obj2, null);

                if (IsDifferent(val1, val2))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a string that represents the <see cref="T:Wisej.Core.DynamicObject" /> values.
        /// </summary>
        /// <returns>A <see cref="T:System.String" /> that represent the type and state of the instance.</returns>
        public override string ToString()
        {
            return WisejSerializer.Serialize(this);
        }

        /// <summary>
        /// Compares this instance with another DynamicObject.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return IsEqual(obj as DynamicObject);
        }

        /// <returns>
        /// The hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Entry

        /// <summary>
        /// Represent a member in the <see cref="T:Wisej.Core.DynamicObject"/> instance.
        /// </summary>
        public struct Member
        {
            /// <summary>
            /// Returns a default empty member.
            /// </summary>
            public static readonly Member Empty;

            /// <summary>
            /// Creates a new initialized Member structure.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="value"></param>
            internal Member(string name, object value) : this()
            {
                this.Name = name;
                this.Value = value;
            }

            /// <summary>
            /// Returns the name of the member.
            /// </summary>
            public string Name;

            /// <summary>
            /// Returns the value of the member.
            /// </summary>
            public object Value;
        }

        #endregion

        #endregion

        #region ICloneable

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion

        #region  INotifyPropertyChanged

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { this._propertyChanged += value; }
            remove { this._propertyChanged -= value; }
        }
        private PropertyChangedEventHandler _propertyChanged;

        #endregion

        #region MetaStore

        /// <summary>
        /// Optimized dictionary implementation to
        /// preserve fields' relative index in the collection and
        /// speed up data management operations.
        /// </summary>
        private class MetaStore : IEnumerable<Member>
        {
            // number of used elements in the array.
            private int length = 0;
            // the actual values stored.
            private Member[] members;

            /// <summary>
            /// Creates a new Dictionary instance.
            /// </summary>
            internal MetaStore() : this(1) { }

            /// <summary>
            /// Creates a new Dictionary instance initialize with the specified capacity.
            /// </summary>
            /// <param name="capacity"></param>
            internal MetaStore(int capacity)
            {
                EnsureCapacity(capacity);
            }

            /// <summary>
            /// Creates a new Dictionary instance as a clone of the source.
            /// </summary>
            /// <param name="source"></param>
            internal MetaStore(MetaStore source)
            {
                if (source?.length > 0)
                {
                    this.length = source.length;
                    this.members = new Member[this.length];
                    Array.Copy(source.members, this.members, this.length);
                }
            }

            // Enlarges the store as needed to ensure that the specified capacity is  available in the underlying array.
            private void EnsureCapacity(int capacity)
            {
                if (this.members == null)
                {
                    this.members = new Member[capacity];
                }
                else
                {
                    int length = this.members.Length;
                    if (length < capacity)
                    {
                        // extend the stores to match the capacity * 2.
                        int newLength = capacity + capacity;
                        var newMembers = new Member[newLength];
                        Array.Copy(this.members, newMembers, length);

                        this.members = newMembers;
                    }
                }
            }

            /// <summary>
            /// Returns the number of members in the store, not the capacity.
            /// </summary>
            internal int Length
            {
                get { return this.length; }
            }

            /// <summary>
            /// Sets or gets the value of the specified member.
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            internal object this[string name]
            {
                get
                {
                    int position = IndexOf(name);
                    return position > -1
                        ? this.members[position].Value
                        : null;
                }
                set
                {
                    int position = IndexOf(name);
                    if (position > -1)
                        this.members[position].Value = value;
                    else
                        Add(name, value);
                }
            }

            /// <summary>
            /// Sets or gets the member at the specified index.
            /// </summary>
            /// <param name="position"></param>
            /// <returns></returns>
            internal Member this[int position]
            {
                get { return this.members[position]; }
                set { this.members[position] = value; }
            }

            /// <summary>
            /// Returns the index of the specified member, or -1 if not found.
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            internal int IndexOf(string name)
            {
                if (this.members == null)
                    return -1;

                for (int i = 0; i < this.length; i++)
                {
                    if (String.Equals(this.members[i].Name, name, StringComparison.Ordinal))
                        return i;
                }
                return -1;
            }

            /// <summary>
            /// Adds the member to the store.
            /// Doesn't check whether a member with the same name already exists.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="value"></param>
            internal int Add(string name, object value)
            {
                int index = this.length;
                EnsureCapacity(this.length + 1);
                this.members[this.length].Name = name;
                this.members[this.length].Value = value;
                this.length++;

                return index;
            }

            /// <summary>
            /// Clears the store from all members.
            /// </summary>
            internal void Clear()
            {
                if (this.length > 0)
                {
                    this.length = 0;
                    this.members = null;
                }
            }

            /// <summary>
            /// Removes the excess array entries from the inner array.
            /// </summary>
            internal void TrimExcess()
            {
                if (this.members == null)
                    return;

                int excess = this.members.Length - this.length;
                if (excess > 0)
                {
                    // shrink the stores.
                    int newLength = this.length;
                    var newMembers = new Member[newLength];
                    Array.Copy(this.members, newMembers, newLength);

                    this.members = newMembers;
                }
            }

            /// <summary>
            /// Sorts the members by name.
            /// </summary>
            internal void Sort()
            {
                if (this.members != null && this.length > 0)
                {
                    Array.Sort(this.members, 0, this.length, Comparer<Member>.Create((x, y) => String.Compare(x.Name, y.Name, StringComparison.InvariantCulture)));
                }
            }

            /// <summary>
            /// Removes the specified member.
            /// </summary>
            /// <param name="name"></param>
            /// <returns>True if the member was removed, false if not found.</returns>
            internal bool Remove(string name)
            {
                int position = IndexOf(name);
                if (position < 0)
                    return false;

                RemoveAt(position);
                return true;
            }

            /// <summary>
            /// Removes the member at the specified position.
            /// </summary>
            /// <param name="position"></param>
            internal void RemoveAt(int position)
            {
                int last = this.length - 1;
                if (position < last)
                    Array.Copy(this.members, position + 1, this.members, position, last - position);

                this.length--;
                this.members[last] = Member.Empty;
            }

            /// <summary>
            ///  Copies the members into the specified destination array.
            /// </summary>
            /// <param name="array"></param>
            /// <param name="index"></param>
            internal void CopyTo(Member[] array, int index)
            {
                Array.Copy(this.members, 0, array, index, this.length);
            }

            IEnumerator<Member> IEnumerable<Member>.GetEnumerator()
            {
                return new Enumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return new Enumerator(this);
            }

            #region Enumerator

            internal class Enumerator : IEnumerator<Member>
            {
                private int max = 0;
                private int position = -1;
                private MetaStore store;

                public Enumerator(MetaStore store)
                {
                    this.position = -1;
                    this.store = store;
                    this.max = (store?.length ?? 0) - 1;
                }

                public Member Current
                {
                    get { return this.store?[this.position] ?? Member.Empty; }
                }

                object IEnumerator.Current
                {
                    get { return this.store?[this.position] ?? Member.Empty; }
                }

                public bool MoveNext()
                {
                    if (this.position >= this.max)
                        return false;

                    this.position++;
                    return true;
                }

                public void Reset()
                {
                    this.position = -1;
                }

                void IDisposable.Dispose()
                {
                    this.store = null;
                }
            }


            #endregion
        }

        #endregion

        #region MetaDynamic

        /// <summary>
        /// Returns the value of the specified field by name.
        /// This is referenced by the dynamic expression built in the DynamicMetaObject implementation.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        //		internal static object GetMemberInternal(DynamicObject target, int index, string name)
        internal static object GetMemberInternal(DynamicObject target, string name)
        {
            return target[name];
        }

        /// <summary>
        /// Sets the value of the field specified by name.
        /// This is referenced by the dynamic expression built in the DynamicMetaObject implementation.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static object SetMemberInternal(DynamicObject target, string name, object value)
        {
            target[name] = value;
            return value;
        }

        /// <summary>
        /// Provides a <see cref="T:System.Dynamic.DynamicMetaObject" /> that dispatches to the dynamic virtual methods.
        /// </summary>
        /// <returns>An object of the <see cref="T:System.Dynamic.DynamicMetaObject" /> type.</returns>
        /// <param name="parameter">The expression that represents <see cref="T:System.Dynamic.DynamicMetaObject" /> to dispatch to the dynamic virtual methods.</param>
        DynamicMetaObject IDynamicMetaObjectProvider.GetMetaObject(Expression parameter)
        {
            return new DynamicObject.MetaDynamic(parameter, this);
        }

        private class MetaDynamic : DynamicMetaObject
        {
            private UnaryExpression Self;
            private static MethodInfo SetMemberInternal;
            private static MethodInfo GetMemberInternal;
            private static MethodInfo TestVersionInternal;
            private static Type DynamicObjectType = typeof(DynamicObject);
            private static MemberTypes MemberTypes = MemberTypes.Field | MemberTypes.Property | MemberTypes.Method;
            private static BindingFlags BindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

            static MetaDynamic()
            {
                SetMemberInternal = DynamicObjectType.GetMethod("SetMemberInternal", BindingFlags.Static | BindingFlags.NonPublic);
                GetMemberInternal = DynamicObjectType.GetMethod("GetMemberInternal", BindingFlags.Static | BindingFlags.NonPublic);
                TestVersionInternal = DynamicObjectType.GetMethod("TestVersionInternal", BindingFlags.Static | BindingFlags.NonPublic);
            }

            public MetaDynamic(Expression expression, DynamicObject value)
                : base(expression, BindingRestrictions.Empty, value)
            {
                this.Self = Expression.Convert(base.Expression, DynamicObjectType);
            }

            public override IEnumerable<string> GetDynamicMemberNames()
            {
                return this.Value.Where(o => !String.IsNullOrEmpty(o.Name)).Select(o => o.Name);
            }

            public new DynamicObject Value
            {
                get { return (DynamicObject)base.Value; }
            }

            public override DynamicMetaObject BindGetMember(GetMemberBinder binder)
            {
                // check if the member is actually defined - except for "Item" or we
                // get the indexer and it clashed with a potential dynamic property named "Item".
                if (binder.Name != "Item")
                    if (DynamicObjectType.GetMember(binder.Name, MemberTypes, BindingFlags).Length > 0)
                        return binder.FallbackGetMember(this);

                return
                    new DynamicMetaObject(
                        Expression.Call(
                            GetMemberInternal,
                            this.Self,
                            Expression.Constant(binder.Name)),
                        BindingRestrictions.GetTypeRestriction(base.Expression, base.LimitType)
                    );
            }

            public override DynamicMetaObject BindSetMember(SetMemberBinder binder, DynamicMetaObject value)
            {
                // check if the member is actually defined.
                if (DynamicObjectType.GetMember(binder.Name, MemberTypes, BindingFlags).Length > 0)
                    return binder.FallbackSetMember(this, value);

                return
                    new DynamicMetaObject(
                        Expression.Call(
                            SetMemberInternal,
                            this.Self,
                            Expression.Constant(binder.Name),
                            Expression.Convert(value.Expression, typeof(object))
                        ),
                        BindingRestrictions.GetTypeRestriction(base.Expression, base.LimitType)
                    );
            }

            public override DynamicMetaObject BindInvokeMember(InvokeMemberBinder binder, DynamicMetaObject[] args)
            {
                // check if the member is actually defined.
                if (DynamicObjectType.GetMember(binder.Name, MemberTypes, BindingFlags).Length > 0)
                    return binder.FallbackInvokeMember(this, args);

                return
                    new DynamicMetaObject(
                        Expression.Call(
                            GetMemberInternal,
                            this.Self,
                            Expression.Constant(binder.Name)),
                        BindingRestrictions.GetTypeRestriction(base.Expression, base.LimitType)
                    );
            }
        }

        #endregion

        #region ISerializable

        public DynamicObject(SerializationInfo info, StreamingContext context)
        {
            // deserialize the values
            foreach (SerializationEntry entry in info)
            {
                if (entry.Name == "JSON")
                {
                    DynamicObject obj = (DynamicObject)WisejSerializer.Parse((string)entry.Value);
                    this.store = new MetaStore(obj.store);
                    break;
                }
            }
        }

        [System.Security.SecurityCritical]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (!this.IsEmpty)
            {
                info.AddValue("JSON", this.ToString());
            }
        }

        #endregion

        #region ICustomTypeDescriptor

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            return ((ICustomTypeDescriptor)this).GetProperties();
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            if (this.store != null && this.store.Length > 0)
            {
                var attributes = new[] { BrowsableAttribute.Yes };
                var properties = new DynamicPropertyDescriptor[this.store.Length];
                for (int i = 0; i < this.store.Length; i++)
                {
                    properties[i] = new DynamicPropertyDescriptor(this, this.store[i].Name, attributes);
                }

                return new PropertyDescriptorCollection(properties);
            }
            else
            {
                return new PropertyDescriptorCollection(null);
            }
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor prop)
        {
            return this;
        }

        #endregion

        #region DynamicPropertyDescriptor

        /// <summary>
        /// The PropertyDescriptor that reads/writes properties
        /// from a DyamicObject.
        /// </summary>
        internal class DynamicPropertyDescriptor : PropertyDescriptor
        {
            private DynamicObject owner;

            public DynamicPropertyDescriptor(DynamicObject owner, string name, Attribute[] attributes)
                : base(name, attributes)
            {
                this.owner = owner;
            }

            public override bool CanResetValue(object component)
            {
                return true;
            }

            public override Type ComponentType
            {
                get { return this.owner.GetType(); }
            }

            public override object GetValue(object component)
            {
                var target = component as DynamicObject;
                return target == null ? null : target[this.Name];
            }

            public override bool IsReadOnly
            {
                get { return false; }
            }

            public override Type PropertyType
            {
                get { return this.owner[this.Name]?.GetType() ?? typeof(void); }
            }

            public override void ResetValue(object component)
            {
                var target = component as DynamicObject;
                if (target != null) target[this.Name] = null;
            }

            public override void SetValue(object component, object value)
            {
                var target = component as DynamicObject;
                if (target != null) target[this.Name] = value;
            }

            public override bool ShouldSerializeValue(object component)
            {
                return true;
            }
        }

        #endregion


    }
}
