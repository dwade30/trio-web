﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRIOWinsketchCalculations
{
    class Program
    {
        private static winskt.WinSkt appWinSketch;

        static void Main(string[] args)
        {
            var appDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Console.WriteLine($"Searching for sketch files in {appDirectory}");
            var sketchFiles = Directory.GetFiles(appDirectory, "*.skt", SearchOption.AllDirectories).Where(file => file.EndsWith(".skt")).ToArray();
            if (sketchFiles.Length > 0)
            {
                Console.WriteLine($"Found {sketchFiles.Length} sketch files in {appDirectory}");
                int processedFilesCount = 0;
                foreach (var sketchFile in sketchFiles)
                {
                    Console.WriteLine("-----------------------------------");
                    Console.WriteLine($"  PROCESSING file {sketchFile}");
                    if (GenerateCalculationFile(sketchFile))
                    {
                        Console.WriteLine($"  CALCULATIONS FILE GENERATED");
                        processedFilesCount++;
                    }
                    else
                    {
                        Console.WriteLine($"  CALCULATIONS FILE NOT GENERATED");
                    }
                }

                Console.WriteLine("-----------------------------------");
                Console.WriteLine($"Closing WinSketch applications");
                foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
                {
                    process.Kill();
                }
                Console.WriteLine($"PROCESS COMPLETED: Generated calculation files for {processedFilesCount} of {sketchFiles.Length} sketch files");
            }
            else
            {
                Console.WriteLine($"There are no sketch files in {appDirectory}");
            }
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static bool GenerateCalculationFile(string sketchFile)
        {
            try
            {
                Console.WriteLine($"    Closing previous WinSketch applications");
                foreach (var process in System.Diagnostics.Process.GetProcessesByName("WinSkt"))
                {
                    process.Kill();
                }
                appWinSketch = null;
                appWinSketch = new winskt.WinSkt();
                Console.WriteLine($"    Opening WinSketch file");
                appWinSketch.OpenSketch(sketchFile);

                object calculations = null;
                Console.WriteLine($"    Get sketch calculations");
                if (appWinSketch.GetCalculations(out calculations))
                {
                    Console.WriteLine($"    Generate sketch calculations file");
                    string calculationFilePath = sketchFile.Replace(".skt", ".skt-calculations");
                    if (File.Exists(calculationFilePath))
                    {
                        File.Delete(calculationFilePath);
                    }
                    string calculationData = WisejSerializer.Serialize(calculations);
                    using (var textFileWriter = File.CreateText(calculationFilePath))
                    {
                        textFileWriter.Write(calculationData);
                    }
                    return true;
                }
                else
                {
                    Console.WriteLine($"    ERROR:   Cannot generate calculation file for sketch file {sketchFile}");
                    Console.WriteLine($"    DETAILS: WinSketch application error");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"    ERROR:   Cannot generate calculation file for sketch file {sketchFile}");
                Console.WriteLine($"    DETAILS: Exception {ex}");
                return false;
            }
        }
    }
}
