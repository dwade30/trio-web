﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TRIOWinsketchCalculations
{

    /// <summary>
    /// Wisej super performant JSON serializer.
    /// 
    /// This is a high performant and low memory footprint JSON serializer/deserializer.
    /// It has been optimized to work with Wisej DynamicObject and to streamline serialization/deserialization.
    /// 
    /// It appears to be the fastest JSON serializer/deserializer using the least memory.
    /// </summary>
    public static class WisejSerializer
    {
        private const char INVALID_CHAR = (char)65533;
        private static CultureInfo InvariantCulture = CultureInfo.InvariantCulture;
        private static long MinTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        #region JsonWriter

        /// <summary>
        /// Writes the serialized JSON to the destination stream.
        /// </summary>
        /// <remarks>
        /// The output can be formatted setting the Formatted property to true.
        /// </remarks>
        private class JsonWriter : StringWriter
        {
            private const char TAB = '\t';
            private const string CRLF = "\r\n";

            // indentation.
            public int Indent;

            // enables formatting of the output.
            public bool Formatted;

            // enables the serialization of field and property names using camel case: first lowercase.
            public bool CamelCase;

            // skips nulls values.
            public bool IgnoreNulls;

            // options for the current serialization/parsing call stack.
            public WisejSerializerOptions Options;

            public JsonWriter(WisejSerializerOptions options)
            {
                this.Options = options;
                this.Formatted = (options & WisejSerializerOptions.Formatted) == WisejSerializerOptions.Formatted;
                this.CamelCase = (options & WisejSerializerOptions.CamelCase) == WisejSerializerOptions.CamelCase;
                this.IgnoreNulls = (options & WisejSerializerOptions.IgnoreNulls) == WisejSerializerOptions.IgnoreNulls;
            }

            public JsonWriter(int capacity = 128)
                : base(new StringBuilder(capacity))
            {
            }

            internal void WriteNewLine()
            {
                Write(CRLF);
            }

            internal void WriteTabs()
            {
                for (int i = 0; i < this.Indent; i++)
                    Write(TAB);
            }
        }

        #endregion

        #region Serialization

        /// <summary>
        /// Serializes an object to a JSON string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="options">Serializer options.</param>
        /// <returns></returns>
        private static string SerializeInternal(object value, WisejSerializerOptions options)
        {
            using (JsonWriter writer = new JsonWriter(options))
            {
                SerializeValue(writer, value);

                writer.Flush();
                return writer.ToString();
            }
        }

        //
        // Serializes an object and recurses it's members.
        //
        private static void SerializeObject(JsonWriter writer, object value)
        {
            if (writer.Formatted)
            {
                writer.WriteNewLine();
                writer.WriteTabs();
            }

            writer.Write('{');

            if (writer.Formatted)
            {
                writer.WriteNewLine();
                writer.Indent++;
            }

            // serialize a DynamicObject object.
            bool first = true;
            DynamicObject map = value as DynamicObject;
            if (map != null)
            {
                foreach (var member in map)
                {
                    if (SerializeField(writer, member.Name, member.Value, first))
                        first = false;
                }
            }
            else
            {
                // serialize any object.
                Type type = value.GetType();

                FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);
                Dictionary<string, bool> serialized = new Dictionary<string, bool>(fields.Length);
                foreach (var field in fields)
                {
                    // skip fields that have already been serialized.
                    // a type may contain multiple fields with the same name when using
                    // the "new" operator to shadow an inherited field.
                    if (serialized.ContainsKey(field.Name))
                        continue;

                    if (SerializeField(writer, field.Name, field.GetValue(value), first))
                        first = false;

                    serialized[field.Name] = true;
                }

                PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);
                foreach (var property in properties)
                {
                    // skip properties that have already been serialized.
                    // a type may contain multiple properties with the same name when using
                    // the "new" operator to shadow an inherited field.
                    // skip fields that have already been serialized.
                    // a type may contain multiple fields with the same name when using
                    // the "new" operator to shadow an inherited field.
                    if (serialized.ContainsKey(property.Name))
                        continue;

                    // skip  properties with parameters
                    MethodInfo getMethod = property.GetGetMethod();
                    if (getMethod == null || getMethod.GetParameters().Count() > 0)
                        continue;

                    if (SerializeField(writer, property.Name, property.GetValue(value), first))
                        first = false;

                    serialized[property.Name] = true;
                }
            }

            // end object definition.

            if (writer.Formatted)
            {
                writer.Indent--;
                if (!first)
                    writer.WriteNewLine();

                writer.WriteTabs();
            }

            writer.Write('}');
        }

        //
        // Serializes a single field.
        //
        private static bool SerializeField(JsonWriter writer, string name, object value, bool first)
        {
            if (value == null && writer.IgnoreNulls)
                return false;

            if (!first)
            {
                writer.Write(',');
                if (writer.Formatted)
                    writer.WriteNewLine();
            }

            if (writer.Formatted)
                writer.WriteTabs();

            writer.Write('\"');
            if (writer.CamelCase)
                writer.Write(Char.ToLowerInvariant(name[0]) + name.Substring(1));
            else
                writer.Write(name);
            writer.Write("\":");

            if (writer.Formatted)
                writer.Write(' ');

            SerializeValue(writer, value);

            return true;
        }

        //
        // Serializes an IDictionary and it's members to a JSON map.
        // NOTE: There is no deserialization for IDictionary, maps are parsed into DynamicObject objects.
        //
        private static void SerializeDictionary(JsonWriter writer, IDictionary dictionary)
        {
            IEnumerator enumerator = dictionary.GetEnumerator();
            if (enumerator == null)
            {
                writer.Write("null");
                return;
            }

            if (dictionary.Count == 0)
            {
                writer.Write("{}");
                return;
            }

            bool first = true;
            writer.Write('{');
            while (enumerator.MoveNext())
            {
                dynamic item = enumerator.Current;
                SerializeField(writer, item.Key, item.Value, first);
                first = false;
            }
            writer.Write('}');
        }

        //
        // Serializes an enumerable object (array, list, ...) and recurses its items.
        //
        private static void SerializeArray(JsonWriter writer, IEnumerable list)
        {
            IEnumerator enumerator = list.GetEnumerator();
            if (enumerator == null)
            {
                writer.Write("null");
                return;
            }

            bool first = true;
            writer.Write('[');
            try
            {
                while (enumerator.MoveNext())
                {
                    object element = enumerator.Current;
                    if (!first)
                    {
                        writer.Write(',');
                        if (writer.Formatted)
                            writer.Write(' ');
                    }
                    first = false;

                    SerializeValue(writer, element);
                }
            }
            catch { }
            writer.Write(']');
        }

        //
        // Serialize a single value.
        //
        private static void SerializeValue(JsonWriter writer, object value)
        {
            if (value == null)
            {
                writer.Write("null");
                return;
            }

            // string?
            string text = value as string;
            if (text != null)
            {
                writer.Write('\"');
                WriteStringValue(writer, text);
                writer.Write('\"');
                return;
            }

            // any primitive value?
            Type type = value.GetType();
            if (type.IsPrimitive)
            {
                string primitive = null;
                switch (Type.GetTypeCode(type))
                {
                    case TypeCode.Boolean: primitive = ((bool)value) ? "true" : "false"; break;
                    case TypeCode.Int16: primitive = ((short)value).ToString(InvariantCulture); break;
                    case TypeCode.Int32: primitive = (Convert.ToInt32(value)).ToString(InvariantCulture); break;
                    case TypeCode.Int64: primitive = ((long)value).ToString(InvariantCulture); break;

                    case TypeCode.Char:
                        {
                            writer.Write('\"');
                            WriteStringValue(writer, Convert.ToString(value, InvariantCulture));
                            writer.Write('\"');
                        }
                        return;

                    default: primitive = Convert.ToString(value, InvariantCulture); break;
                }
                writer.Write(primitive);
                return;
            }

            // enumeration? convert to string with the first character lowercase to
            // follow JavaScript standard notation.
            if (type.IsEnum)
            {
                text = value.ToString();
                writer.Write('\"');
                writer.Write(Char.ToLowerInvariant(text[0]));
                if (text.Length > 1)
                    writer.Write(text.Substring(1));
                writer.Write('\"');
                return;
            }

            // any known type?
            if (type.IsValueType)
            {
                // date/time?
                if (type == typeof(DateTime))
                {
                    writer.Write("\"\\/Date(");
                    DateTime dateTime = ((DateTime)value).ToUniversalTime();
                    writer.Write((dateTime.Ticks - WisejSerializer.MinTicks) / (long)10000);
                    writer.Write(")\\/\"");
                    return;
                }
                else if (type == typeof(Decimal))
                {
                    writer.Write(((decimal)value).ToString(InvariantCulture));
                }
                else if (type == typeof(Size))
                {
                    Size size = (Size)value;
                    writer.Write("{\"width\":");
                    writer.Write(size.Width);
                    writer.Write(',');
                    if (writer.Formatted)
                        writer.Write(' ');
                    writer.Write("\"height\":");
                    writer.Write(size.Height);
                    writer.Write("}");
                }
                else if (type == typeof(Point))
                {
                    Point point = (Point)value;
                    writer.Write("{\"x\":");
                    writer.Write(point.X);
                    writer.Write(',');
                    if (writer.Formatted)
                        writer.Write(' ');
                    writer.Write("\"y\":");
                    writer.Write(point.Y);
                    writer.Write("}");
                }
                else if (type == typeof(Rectangle))
                {
                    Rectangle rect = (Rectangle)value;
                    writer.Write("{\"x\":");
                    writer.Write(rect.X);
                    writer.Write(',');
                    if (writer.Formatted)
                        writer.Write(' ');
                    writer.Write("\"y\":");
                    writer.Write(rect.Y);
                    writer.Write(',');
                    if (writer.Formatted)
                        writer.Write(' ');
                    writer.Write("\"width\":");
                    writer.Write(rect.Width);
                    writer.Write(',');
                    if (writer.Formatted)
                        writer.Write(' ');
                    writer.Write("\"height\":");
                    writer.Write(rect.Height);
                    writer.Write("}");
                }
                else if (type == typeof(Color))
                {
                    Color color = (Color)value;
                    if (color.IsEmpty)
                    {
                        writer.Write("null");
                    }
                    else
                    {
                        writer.Write('"');
                        writer.Write(TranslateColor(color));
                        writer.Write('"');
                    }
                }
                else
                {
                    // IWisejSerializable?
                    if (value is IWisejSerializable)
                    {
                        if (((IWisejSerializable)value).Serialize(writer, writer.Options))
                            return;
                    }

                    SerializeObject(writer, value);
                }

                return;
            }

            // object map?
            if (value is DynamicObject)
            {
                SerializeObject(writer, value);
                return;
            }

            // IWisejSerializable?
            if (value is IWisejSerializable)
            {
                if (((IWisejSerializable)value).Serialize(writer, writer.Options))
                    return;
            }

            // dictionary?
            if (value is IDictionary)
            {
                SerializeDictionary(writer, (IDictionary)value);
                return;
            }

            // enumerable?
            if (value is IEnumerable)
            {
                SerializeArray(writer, (IEnumerable)value);
                return;
            }

            // image?
            //if (value is Image)
            //{
            //    writer.Write('"');
            //    writer.Write(ResourceManager.GetImageBase64((Image)value));
            //    writer.Write('"');
            //    return;
            //}

            // font?
            if (value is Font)
            {
                // re-enter: a font can translate to a string if it's a themed (unchanged) font, or to
                // a dynamic object map that defines the font and its properties.
                SerializeValue(writer, TranslateFont((Font)value));
                return;
            }

            // recurse
            SerializeObject(writer, value);
        }

        //
        // Outputs encoded text.
        //
        private static void WriteStringValue(JsonWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                switch (c)
                {
                    case '\b': writer.Write(@"\b"); break;
                    case '\f': writer.Write(@"\f"); break;
                    case '\n': writer.Write(@"\n"); break;
                    case '\r': writer.Write(@"\r"); break;
                    case '\t': writer.Write(@"\t"); break;
                    case '\"': writer.Write(@"\"""); break;
                    case '\\': writer.Write(@"\\"); break;
                    default: writer.Write(c < 32 ? INVALID_CHAR : c); break;
                }
            }
        }

        /// <summary>
        /// Translates the color to the equivalent HTML definition.
        /// 
        /// - System colors are converted to their name matching the theme color.
        /// - Known named colors are converted to their lower case name.
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static string TranslateColor(Color color)
        {
            if (color.IsEmpty)
                return null;

            //if (ClientThemeReader.IsThemeColor(color))
            //{
            //    string name = color.Name;
            //    name = name.Substring(1);
            //    return name;
            //}

            if (color.IsSystemColor)
            {
                string name = color.Name;
                if (name != null && name.Length > 0)
                    name = Char.ToLowerInvariant(name[0]) + name.Substring(1);

                return name;
            }

            if (color.IsNamedColor)
            {
                string name = color.Name;
                return name.ToLowerInvariant();
            }

            if (color.A == 255)
            {
                return ColorTranslator.ToHtml(color);
            }
            else
            {
                return String.Format("rgba({0},{1},{2},{3})",
                    color.R,
                    color.G,
                    color.B,
                    Math.Round((color.A / 255d), 2).ToString("0.##", InvariantCulture));
            }
        }

        /// <summary>
        /// Translate the windows font to a list of CSS font properties separated by a space.
        /// </summary>
        /// <param name="font"></param>
        /// <returns></returns>
        private static dynamic TranslateFont(Font font)
        {
            if (font == null)
                return null;

            string fontName = font.OriginalFontName ?? font.Name;

//            if (ClientThemeReader.IsThemeFont(font))
//            {
//                fontName = font.OriginalFontName.Substring(1);

//#if !THEME_BUILDER
//                // if a theme font has been modified we need to serialize all
//                // its properties.
//                var theme = ApplicationBase.Theme;
//                if (theme == null || FontEquals(theme.GetThemeFont(fontName), font))
//                    return fontName;
//#else
//				return fontName;
//#endif
//            }

            if (font.IsSystemFont)
            {

                string systemName = font.SystemFontName.ToLowerInvariant();

                return systemName;
            }

            dynamic config = new DynamicObject(6);

            config.family = fontName.Split(',');
            config.size = font.Size > -1 ? (object)font.Size : null;
            config.unit = font.Unit == GraphicsUnit.Point ? "pt" : "px";
            config.bold = font.Bold;
            config.italic = font.Italic;

            if (font.Underline || font.Strikeout)
                config.decoration = (font.Underline ? "underline " : "") + (font.Strikeout ? "line-through " : "");

            return config;
        }

        // Compares two fonts without comparing the GdiVertical property.
        private static bool FontEquals(Font font1, Font font2)
        {
            if (font1 == null || font2 == null)
                return font1 == font2;

            return
                (font1 == font2) ||
                !(font1.Size != font2.Size
                || font1.Unit != font2.Unit
                || font1.Style != font2.Style
                || font1.OriginalFontName != font2.OriginalFontName
                || !FontFamily.Equals(font1.FontFamily, font2.FontFamily));
        }

        #endregion

        #region Deserialization

        /// <summary>
        /// Deserializes a json string to a DynamicObject instance.
        /// </summary>
        /// <param name="json">The JSON source string.</param>
        /// <returns></returns>
        private static DynamicObject ParseInternal(string json)
        {
            if (String.IsNullOrEmpty(json))
                return new DynamicObject();

            using (TextReader reader = new StringReader(json))
            {
                return ParseObject(reader) as DynamicObject;
            }
        }

        /// <summary>
        /// Deserializes a json string stream to a DynamicObject instance.
        /// </summary>
        /// <param name="stream">The JSON source stream.</param>
        /// <returns></returns>
        private static DynamicObject ParseInternal(Stream stream)
        {
            if (stream == null)
                return null;

            using (TextReader reader = new StreamReader(stream))
            {
                return ParseObject(reader) as DynamicObject;
            }
        }

        //
        // Parse an object and recurse it's members.
        //
        private static object ParseObject(TextReader reader)
        {
            string token = GetNextToken(reader);

            switch (token)
            {
                case "{":
                    return ParseFields(reader);

                case "null":
                    return null;

                default:
                    throw new Exception("Unexpected token: " + token + ". Looking for {.");
            }
        }

        //
        // Parses a single value.
        //
        private static object ParseValue(string value)
        {
            if (String.IsNullOrEmpty(value))
                return null;

            // string?
            if (value.StartsWith("\"") || value.StartsWith("\'"))
            {
                value = value.Substring(1, value.Length - 2);

                // or JSON date? "2015-03-16T18:12:46.926Z"
                if (value.EndsWith("Z") && value.Length > 11 && value[10] == 'T')
                {
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                        return dateTime;
                }
                // or our serialized date?
                if (value.StartsWith("/Date("))
                {
                    long ticks = 0;
                    if (long.TryParse(value.Substring(6, value.Length - 8), out ticks))
                        return new DateTime(ticks * 10000L + WisejSerializer.MinTicks, DateTimeKind.Utc).ToLocalTime();
                }

                return value;
            }

            // bool?
            if (value == "true")
                return true;
            if (value == "false")
                return false;

            // null or undefined?
            if (value == "null" || value == "undefined")
                return null;

            // double?
            if (value.IndexOf('.') > -1)
            {
                double d;
                double.TryParse(value, NumberStyles.Number, InvariantCulture, out d);
                return d;
            }

            // int?
            int i;
            if (int.TryParse(value, out i))
                return i;

            // long?
            long l;
            if (long.TryParse(value, out l))
                return l;

            // whatever...
            throw new Exception("Unexpected value: " + value);
        }

        //
        // Parses the fields in a token and returns an object map.
        //
        private static DynamicObject ParseFields(TextReader reader)
        {
            DynamicObject obj = new DynamicObject();

            while (true)
            {
                string name = GetNextToken(reader);

                // skip lonely commas.
                if (name == ",")
                    continue;

                if (name == "}")
                    return obj;

                string separator = GetNextToken(reader);
                if (separator != ":")
                    throw new Exception("Unexpected token: " + separator + ". Looking for :.");

                if (name.StartsWith("\""))
                    name = name.Substring(1, name.Length - 2);

                name = name.Trim();
                if (name.Length == 0)
                    throw new Exception("Invalid field name: " + name + ".");

                object value = null;
                string token = GetNextToken(reader);
                switch (token)
                {
                    case "{": value = ParseFields(reader); break;
                    case "[": value = ParseArray(reader); break;
                    default: value = ParseValue(token); break;
                }

                obj[name] = value;

                token = GetNextToken(reader);
                switch (token)
                {
                    case ",": continue;
                    case "}": return obj;
                    default: throw new Exception("Unexpected token: " + token + ". Looking for } or ,.");
                }
            }
        }

        //
        // Parse an array and all its elements.
        //
        private static Array ParseArray(TextReader reader)
        {
            ArrayList list = new ArrayList();

            while (true)
            {
                string token = GetNextToken(reader);
                switch (token)
                {
                    case "]": return TypedArray(list);
                    case "{": list.Add(ParseFields(reader)); break;
                    case "[": list.Add(ParseArray(reader)); break;
                    default: list.Add(ParseValue(token)); break;
                }

                token = GetNextToken(reader);
                switch (token)
                {
                    case ",": continue;
                    case "]": return TypedArray(list);
                    default: throw new Exception("Unexpected token: " + token + ". Looking for ] or ,.");
                }
            }
        }

        //
        // Converts the list to a typed array
        // using the type of the first element.
        //
        private static Array TypedArray(ArrayList list)
        {
            if (list.Count == 0)
                return new object[0];

            Type elementType = null;
            foreach (object element in list)
            {
                if (element != null)
                {
                    if (elementType == null)
                    {
                        elementType = element.GetType();
                        continue;
                    }

                    if (element.GetType() != elementType)
                    {
                        elementType = null;
                        break;
                    }

                }
            }

            if (elementType != null)
                return list.ToArray(elementType);

            return list.ToArray();
        }

        //
        // Moves the reader to the beginning of the next token
        // and returns the length of the token.
        //
        private static string GetNextToken(TextReader reader)
        {
            StringBuilder sb = null;
            bool inDoubleQuotes = false, inSingleQuotes = false;

            while (true)
            {
                int c = reader.Read();
                if (c == -1)
                    break;

                switch (c)
                {
                    // comment?
                    case '/':
                        {
                            if (inSingleQuotes || inDoubleQuotes)
                                break;

                            int next = reader.Read();

                            // line
                            if (next == '/')
                            {
                                // skip until \n
                                while (next != -1)
                                {
                                    next = reader.Read();
                                    if (next == '\n')
                                        break;
                                }
                                continue;
                            }
                            // block
                            if (next == '*')
                            {
                                // skip until */
                                bool star = false;
                                while (next != -1)
                                {
                                    next = reader.Read();
                                    if (star && next == '/')
                                        break;
                                    star = (next == '*');
                                }
                                continue;
                            }
                            break;
                        }

                    // escape?
                    case '\\':
                        {
                            if (!(inDoubleQuotes || inSingleQuotes))
                                throw new Exception("Unexpected char \\.");

                            int next = reader.Read();
                            switch (next)
                            {
                                case 't': c = '\t'; break;
                                case 'r': c = '\r'; break;
                                case 'n': c = '\n'; break;
                                default: c = next; break;
                            }
                        }
                        break;

                    // object, array or field boundaries?
                    case '{':
                    case '[':
                    case ':':
                    case '}':
                    case ']':
                    case ',':
                        {
                            if (!(inSingleQuotes || inDoubleQuotes))
                            {
                                return new string((char)c, 1);
                            }

                            break;
                        }

                    // string?
                    case '\"':
                        {
                            if (inSingleQuotes)
                                break;

                            if (inDoubleQuotes)
                            {
                                sb.Append('\"');
                                return sb.ToString();
                            }

                            inDoubleQuotes = !inDoubleQuotes;
                            break;
                        }

                    // string?
                    case '\'':
                        {
                            if (inDoubleQuotes)
                                break;

                            if (inSingleQuotes)
                            {
                                sb.Append('\'');
                                return sb.ToString();
                            }

                            inSingleQuotes = !inSingleQuotes;
                            break;
                        }

                    default:
                        {
                            if (inSingleQuotes || inDoubleQuotes)
                                break;

                            if (!Char.IsWhiteSpace((char)c))
                            {
                                // read the literal to the end.
                                if (sb == null)
                                    sb = new StringBuilder();

                                while (c > -1)
                                {
                                    sb.Append((char)c);

                                    switch (reader.Peek())
                                    {
                                        case '}':
                                        case ']':
                                        case ',':
                                        case ':': return sb.ToString().Trim();
                                    }

                                    c = reader.Read();
                                }
                                return sb.ToString().Trim();
                            }
                            break;
                        }
                }

                if (inSingleQuotes || inDoubleQuotes || !Char.IsWhiteSpace((char)c))
                {
                    if (sb == null)
                        sb = new StringBuilder();

                    sb.Append((char)c);
                }

            }

            return sb != null ? sb.ToString() : null;
        }

        #endregion

        /// <summary>
        /// Serializes an object to standard JSON.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="options">Serializer options.</param>
        /// <returns></returns>
        public static string Serialize(object value, WisejSerializerOptions options = WisejSerializerOptions.CamelCase)
        {
            if (value == null)
                return null;

            return SerializeInternal(value, options);
        }

        /// <summary>
        /// Deserializes a JSON string to a System.Wisej.Core.DynamicObject.
        /// </summary>
        /// <param name="json">The JSON source string.</param>
        /// <returns></returns>
        public static dynamic Parse(string json)
        {
            if (String.IsNullOrEmpty(json))
                return null;

            return ParseInternal(json);
        }

        /// <summary>
        /// Deserializes a JSON stream to a System.Wisej.Core.DynamicObject.
        /// </summary>
        /// <param name="stream">The JSON source stream.</param>
        /// <returns></returns>
        public static dynamic Parse(Stream stream)
        {
            if (stream == null)
                return null;

            return ParseInternal(stream);
        }
    }


    /// <summary>
    /// Options for WisejSerializer.
    /// </summary>
    [Flags]
    public enum WisejSerializerOptions
    {
        /// <summary>
        /// No options.
        /// </summary>
        None = 0,

        /// <summary>
        /// The output JSON string is pretty.
        /// </summary>
        Formatted = 1,

        /// <summary>
        /// The names of the fields are serialized using camel casing: first char is lowercase (default).
        /// </summary>
        CamelCase = 2,

        /// <summary>
        /// Null values are not serialized.
        /// </summary>
        IgnoreNulls = 4,
    }

    /// <summary>
    /// Allows an object to serialize itself.
    /// </summary>
    public interface IWisejSerializable
    {
        /// <summary>
        /// Serialize the object in JSON format.
        /// </summary>
        /// <param name="writer">The JSON text writer.</param>
        /// <param name="options">Set of serializer options.</param>
        /// <returns>True of the object handled the serialization, otherwise false to perform the default serialization.</returns>
        bool Serialize(TextWriter writer, WisejSerializerOptions options);
    }

    /// <summary>
    /// Wisej super performant JSON serializer.
    /// 
    /// This is a high performant and low memory footprint JSON serializer/deserializer.
    /// It has been optimized to work with Wisej DynamicObject and to streamline serialization/deserialization.
    /// 
    /// It appears to be the fastest JSON serializer/deserializer using the least memory.
    /// </summary>

}
