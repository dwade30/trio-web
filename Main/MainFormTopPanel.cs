﻿using System;
using Wisej.Web;

namespace Main
{
    public partial class MainFormTopPanel : Wisej.Web.UserControl
    {
        public event EventHandler ThemeButtonClick;
        public event EventHandler SettingsButtonMainClick;
        public event EventHandler HelpButtonMainClick;
        public event EventHandler UserButtonMainClick;

        public MainFormTopPanel()
        {
            InitializeComponent();
        }

        internal string UserName
        {
            get
            {
                return UserUserName.Text;
            }
            set
            {
                UserUserName.Text = value;
            }
        }

        private void themeButton_Click(object sender, System.EventArgs e)
        {
            ThemeButtonClick?.Invoke(themeButtonMain, EventArgs.Empty);
        }

        private void settingsButtonMain_Click(object sender, System.EventArgs e)
        {
            SettingsButtonMainClick?.Invoke(settingsButtonMain, EventArgs.Empty);
        }

        private void userMenuClose_Click(object sender, System.EventArgs e)
        {
            userMenuClose.Visible = false;
            userMenuOpen.Visible = true;
        }

        private void helpButtonMain_Click(object sender, System.EventArgs e)
        {
            HelpButtonMainClick?.Invoke(helpButtonMain, EventArgs.Empty);
        }

        private void userButtonMain_Click(object sender, System.EventArgs e)
        {
            UserButtonMainClick?.Invoke(userButtonMain, EventArgs.Empty);
        }

    }
}
