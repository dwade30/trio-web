﻿using System.IO.Abstractions;
using Autofac;
using SharedApplication;

namespace Main
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType< System.IO.Abstractions.FileSystem> ().As<IFileSystem>();
        }
    }
}