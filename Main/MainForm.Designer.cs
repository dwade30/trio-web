﻿using fecherFoundation;

namespace Main
{
	partial class MainForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            fecherFoundation.FCMainFormModule fcMainFormModule1 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule2 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule3 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule4 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule5 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule6 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule7 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule8 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule9 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule10 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule11 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule12 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule13 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule14 = new fecherFoundation.FCMainFormModule();
            fecherFoundation.FCMainFormModule fcMainFormModule15 = new fecherFoundation.FCMainFormModule();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.slideLeftButton = new Wisej.Web.PictureBox();
            this.pictureBox1 = new Wisej.Web.PictureBox();
            this.label2 = new Wisej.Web.Label();
            this.label3 = new Wisej.Web.Label();
            this.labelUserName = new Wisej.Web.Label();
            this.settingsButton = new Wisej.Web.PictureBox();
            this.themeButton = new Wisej.Web.PictureBox();
            this.newsButton = new Wisej.Web.PictureBox();
            this.helpButton = new Wisej.Web.PictureBox();
            this.line1 = new Wisej.Web.Line();
            this.navigationUserButton = new Wisej.Web.PictureBox();
            this.navigationUserMenuOpen = new Wisej.Web.PictureBox();
            this.navigationUserMenuClose = new Wisej.Web.PictureBox();
            this.userPanel = new Wisej.Web.Panel();
            this.PicArchive = new fecherFoundation.FCPictureBox();
            this.txtCommDest = new fecherFoundation.FCTextBox();
            this.imgArchive = new fecherFoundation.FCPictureBox();
            this.expirationButton = new Wisej.Web.Button();
            this.userContextMenu = new fecherFoundation.FCArrowContextMenu(this.components);
            this.settingsContextMenuMain = new fecherFoundation.FCArrowContextMenu(this.components);
            this.settingsContextMenu = new fecherFoundation.FCArrowContextMenu(this.components);
            this.helpContextMenu = new fecherFoundation.FCArrowContextMenu(this.components);
            this.helpContextMenuMain = new fecherFoundation.FCArrowContextMenu(this.components);
            this.mainFormTopPanel1 = new Main.MainFormTopPanel();
            this.navigationPanel.SuspendLayout();
            this.favouritesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homePageButton)).BeginInit();
            this.homePagePanel.SuspendLayout();
            this.modulesPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.centerPanel.SuspendLayout();
            this.aboutPanel.SuspendLayout();
            this.currentUserPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slideLeftButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserMenuOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserMenuClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicArchive)).BeginInit();
            this.PicArchive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgArchive)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationPanel
            // 
            this.navigationPanel.BackColor = System.Drawing.Color.FromArgb(60, 64, 70);
            this.navigationPanel.Size = new System.Drawing.Size(300, 480);
            // 
            // menuTree
            // 
            this.menuTree.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.menuTree.Size = new System.Drawing.Size(300, 249);
            // 
            // favouritesPanel
            // 
            this.favouritesPanel.Controls.Add(this.helpButton);
            this.favouritesPanel.Controls.Add(this.slideLeftButton);
            this.favouritesPanel.Controls.Add(this.settingsButton);
            this.favouritesPanel.Controls.Add(this.newsButton);
            this.favouritesPanel.Controls.Add(this.themeButton);
            this.favouritesPanel.Size = new System.Drawing.Size(60, 480);
            this.favouritesPanel.Controls.SetChildIndex(this.themeButton, 0);
            this.favouritesPanel.Controls.SetChildIndex(this.newsButton, 0);
            this.favouritesPanel.Controls.SetChildIndex(this.settingsButton, 0);
            this.favouritesPanel.Controls.SetChildIndex(this.slideLeftButton, 0);
            this.favouritesPanel.Controls.SetChildIndex(this.helpButton, 0);
            this.favouritesPanel.Controls.SetChildIndex(this.homePageButton, 0);
            // 
            // homePageButton
            // 
            this.homePageButton.AppearanceKey = "FlatButton";
            this.homePageButton.Display = Wisej.Web.Display.Icon;
            this.homePageButton.ImageSource = "trio-logo";
            this.homePageButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.homePageButton.TextImageRelation = Wisej.Web.TextImageRelation.Overlay;
            // 
            // homePagePanel
            // 
            this.homePagePanel.Size = new System.Drawing.Size(630, 480);
            // 
            // modulesPanel
            // 
            this.modulesPanel.Location = new System.Drawing.Point(0, 0);
            this.modulesPanel.Size = new System.Drawing.Size(1080, 635);
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.mainFormTopPanel1);
            this.topPanel.Size = new System.Drawing.Size(630, 84);
            // 
            // bottomPanel
            // 
            this.bottomPanel.Location = new System.Drawing.Point(180, 344);
            this.bottomPanel.Size = new System.Drawing.Size(630, 136);
            // 
            // centerPanel
            // 
            this.centerPanel.Size = new System.Drawing.Size(630, 396);
            // 
            // aboutPanel
            // 
            this.aboutPanel.Controls.Add(this.label2);
            this.aboutPanel.Controls.Add(this.pictureBox1);
            this.aboutPanel.Location = new System.Drawing.Point(135, 17);
            // 
            // currentUserPanel
            // 
            this.currentUserPanel.Controls.Add(this.line1);
            this.currentUserPanel.Controls.Add(this.navigationUserMenuOpen);
            this.currentUserPanel.Controls.Add(this.navigationUserMenuClose);
            this.currentUserPanel.Controls.Add(this.navigationUserButton);
            this.currentUserPanel.Controls.Add(this.label3);
            this.currentUserPanel.Controls.Add(this.labelUserName);
            // 
            // slideLeftButton
            // 
            this.slideLeftButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.slideLeftButton.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.slideLeftButton.ImageSource = "icon-slide-left?color=app-sidebar-icon";
            this.javaScript1.SetJavaScript(this.slideLeftButton, resources.GetString("slideLeftButton.JavaScript"));
            this.slideLeftButton.Location = new System.Drawing.Point(18, 436);
            this.slideLeftButton.Name = "slideLeftButton";
            this.slideLeftButton.Size = new System.Drawing.Size(24, 24);
            this.slideLeftButton.Click += new System.EventHandler(this.SlideLeftButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Main.Properties.Resources.harris_logo;
            this.pictureBox1.BackgroundImageLayout = Wisej.Web.ImageLayout.BestFit;
            this.pictureBox1.Location = new System.Drawing.Point(88, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 54);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(165, 165, 165);
            this.label2.Location = new System.Drawing.Point(40, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(327, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "© 2017 Harris Local Government. All rights reserved";
            // 
            // label3
            // 
            this.label3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.FromName("@loggedinForeColor");
            this.label3.Location = new System.Drawing.Point(73, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Logged in as";
            this.label3.Click += new System.EventHandler(this.NavigationUserMenuOpen_Click);
            // 
            // labelUserName
            // 
            this.labelUserName.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelUserName.ForeColor = System.Drawing.Color.FromName("@loggedinUsernameForeColor");
            this.labelUserName.Location = new System.Drawing.Point(73, 44);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(33, 16);
            this.labelUserName.TabIndex = 2;
            this.labelUserName.Text = "User";
            this.labelUserName.Click += new System.EventHandler(this.NavigationUserMenuOpen_Click);
            // 
            // settingsButton
            // 
            this.settingsButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.settingsButton.ForeColor = System.Drawing.Color.FromArgb(112, 122, 138);
            this.settingsButton.ImageSource = "icon-settings-btn?color=app-sidebar-icon";
            this.settingsButton.Location = new System.Drawing.Point(18, 340);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(24, 24);
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // themeButton
            // 
            this.themeButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.themeButton.ForeColor = System.Drawing.Color.FromArgb(112, 122, 138);
            this.themeButton.ImageSource = "icon-themes?color=app-sidebar-icon";
            this.themeButton.Location = new System.Drawing.Point(18, 244);
            this.themeButton.Name = "themeButton";
            this.themeButton.Size = new System.Drawing.Size(24, 24);
            this.themeButton.Click += new System.EventHandler(this.themeButton_Click);
            // 
            // newsButton
            // 
            this.newsButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.newsButton.ImageSource = "icon-news?color=app-sidebar-icon";
            this.newsButton.Location = new System.Drawing.Point(18, 292);
            this.newsButton.Name = "newsButton";
            this.newsButton.Size = new System.Drawing.Size(24, 24);
            // 
            // helpButton
            // 
            this.helpButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.helpButton.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.helpButton.ImageSource = "icon-help-btn?color=app-sidebar-icon";
            this.helpButton.Location = new System.Drawing.Point(18, 388);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(24, 24);
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // line1
            // 
            this.line1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.line1.LineColor = System.Drawing.Color.FromArgb(84, 88, 94);
            this.line1.LineSize = 1;
            this.line1.Location = new System.Drawing.Point(0, 0);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(300, 1);
            // 
            // navigationUserButton
            // 
            this.navigationUserButton.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.navigationUserButton.AppearanceKey = "roundedPictureBox";
            this.navigationUserButton.BackColor = System.Drawing.Color.White;
            this.navigationUserButton.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.navigationUserButton.ImageSource = "icon-user?color=app-sidebar-icon";
            this.navigationUserButton.Location = new System.Drawing.Point(24, 26);
            this.navigationUserButton.Name = "navigationUserButton";
            this.navigationUserButton.Size = new System.Drawing.Size(36, 36);
            this.navigationUserButton.Click += new System.EventHandler(this.NavigationUserMenuOpen_Click);
            // 
            // navigationUserMenuOpen
            // 
            this.navigationUserMenuOpen.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.navigationUserMenuOpen.ForeColor = System.Drawing.Color.White;
            this.navigationUserMenuOpen.ImageSource = "icon-caret-up";
            this.navigationUserMenuOpen.Location = new System.Drawing.Point(258, 39);
            this.navigationUserMenuOpen.Name = "navigationUserMenuOpen";
            this.navigationUserMenuOpen.Size = new System.Drawing.Size(10, 6);
            this.navigationUserMenuOpen.Click += new System.EventHandler(this.NavigationUserMenuOpen_Click);
            // 
            // navigationUserMenuClose
            // 
            this.navigationUserMenuClose.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Right)));
            this.navigationUserMenuClose.ForeColor = System.Drawing.Color.White;
            this.navigationUserMenuClose.ImageSource = "icon-close-menu";
            this.navigationUserMenuClose.Location = new System.Drawing.Point(258, 39);
            this.navigationUserMenuClose.Name = "navigationUserMenuClose";
            this.navigationUserMenuClose.Size = new System.Drawing.Size(10, 10);
            this.navigationUserMenuClose.Visible = false;
            this.navigationUserMenuClose.Click += new System.EventHandler(this.NavigationUserMenuClose_Click);
            // 
            // userPanel
            // 
            this.userPanel.Location = new System.Drawing.Point(0, 340);
            this.userPanel.Name = "userPanel";
            this.userPanel.Size = new System.Drawing.Size(300, 85);
            this.userPanel.TabIndex = 0;
            // 
            // PicArchive
            // 
            this.PicArchive.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.PicArchive.BorderStyle = Wisej.Web.BorderStyle.None;
            this.PicArchive.Controls.Add(this.txtCommDest);
            this.PicArchive.Controls.Add(this.imgArchive);
            this.PicArchive.FillColor = 16777215;
            this.PicArchive.Location = new System.Drawing.Point(400, 150);
            this.PicArchive.Name = "PicArchive";
            this.PicArchive.Size = new System.Drawing.Size(712, 423);
            this.PicArchive.Visible = false;
            // 
            // txtCommDest
            // 
            this.txtCommDest.BackColor = System.Drawing.SystemColors.Window;
            this.txtCommDest.Location = new System.Drawing.Point(0, 324);
            this.txtCommDest.Name = "txtCommDest";
            this.txtCommDest.Size = new System.Drawing.Size(27, 40);
            this.txtCommDest.TabIndex = 3;
            this.txtCommDest.Text = "Text1";
            this.txtCommDest.Visible = false;
            // 
            // imgArchive
            // 
            this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgArchive.FillColor = 16777215;
            this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
            this.imgArchive.Location = new System.Drawing.Point(14, 40);
            this.imgArchive.Name = "imgArchive";
            this.imgArchive.Size = new System.Drawing.Size(400, 250);
            this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgArchive.Visible = false;
            // 
            // expirationButton
            // 
            this.expirationButton.Focusable = false;
            this.expirationButton.Location = new System.Drawing.Point(0, 0);
            this.expirationButton.Name = "expirationButton";
            this.expirationButton.Shortcut = Wisej.Web.Shortcut.ShiftF9;
            this.expirationButton.Size = new System.Drawing.Size(10, 10);
            this.expirationButton.TabIndex = 100;
            this.expirationButton.Click += new System.EventHandler(this.ExpirationButton_Click);
            // 
            // userContextMenu
            // 
            this.userContextMenu.Alignment = Wisej.Web.Placement.TopRight;
            this.userContextMenu.Location = new System.Drawing.Point(312, 99);
            this.userContextMenu.Name = "userContextMenu";
            this.userContextMenu.Size = new System.Drawing.Size(268, 99);
            this.userContextMenu.TabIndex = 1;
            this.userContextMenu.ItemClick += new System.EventHandler(this.UserContextMenu_ItemClick);
            this.userContextMenu.Closed += new System.EventHandler(this.UserContextMenu_Closed);
            // 
            // settingsContextMenuMain
            // 
            this.settingsContextMenuMain.Alignment = Wisej.Web.Placement.TopRight;
            this.settingsContextMenuMain.Location = new System.Drawing.Point(312, 99);
            this.settingsContextMenuMain.Name = "settingsContextMenuMain";
            this.settingsContextMenuMain.Size = new System.Drawing.Size(268, 554);
            this.settingsContextMenuMain.TabIndex = 1;
            this.settingsContextMenuMain.ItemClick += new System.EventHandler(this.SettingsContextMenuMain_ItemClick);
            // 
            // settingsContextMenu
            // 
            this.settingsContextMenu.Alignment = Wisej.Web.Placement.TopRight;
            this.settingsContextMenu.Location = new System.Drawing.Point(312, 99);
            this.settingsContextMenu.Name = "settingsContextMenu";
            this.settingsContextMenu.Size = new System.Drawing.Size(268, 554);
            this.settingsContextMenu.TabIndex = 1;
            this.settingsContextMenu.ItemClick += new System.EventHandler(this.SettingsContextMenuMain_ItemClick);
            // 
            // helpContextMenu
            // 
            this.helpContextMenu.Alignment = Wisej.Web.Placement.TopRight;
            this.helpContextMenu.Location = new System.Drawing.Point(312, 99);
            this.helpContextMenu.Name = "helpContextMenu";
            this.helpContextMenu.Size = new System.Drawing.Size(268, 169);
            this.helpContextMenu.TabIndex = 1;
            this.helpContextMenu.ItemClick += new System.EventHandler(this.helpContextMenu_ItemClick);
            // 
            // helpContextMenuMain
            // 
            this.helpContextMenuMain.Alignment = Wisej.Web.Placement.TopRight;
            this.helpContextMenuMain.Location = new System.Drawing.Point(312, 99);
            this.helpContextMenuMain.Name = "helpContextMenuMain";
            this.helpContextMenuMain.Size = new System.Drawing.Size(268, 169);
            this.helpContextMenuMain.TabIndex = 1;
            this.helpContextMenuMain.ItemClick += new System.EventHandler(this.helpContextMenu_ItemClick);
            // 
            // mainFormTopPanel1
            // 
            this.mainFormTopPanel1.Dock = Wisej.Web.DockStyle.Fill;
            this.mainFormTopPanel1.Name = "mainFormTopPanel1";
            this.mainFormTopPanel1.TabIndex = 101;
            this.mainFormTopPanel1.ThemeButtonClick += new System.EventHandler(this.themeButton_Click);
            this.mainFormTopPanel1.SettingsButtonMainClick += new System.EventHandler(this.settingsButtonMain_Click);
            this.mainFormTopPanel1.HelpButtonMainClick += new System.EventHandler(this.helpButtonMain_Click);
            this.mainFormTopPanel1.UserButtonMainClick += new System.EventHandler(this.userButtonMain_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(990, 480);
            this.Controls.Add(this.PicArchive);
            this.Controls.Add(this.expirationButton);
            fcMainFormModule1.BackColor = System.Drawing.Color.FromArgb(111, 154, 233);
            fcMainFormModule1.Image = null;
            fcMainFormModule1.InactiveImage = "";
            fcMainFormModule1.IsActive = true;
            fcMainFormModule1.Name = "re";
            fcMainFormModule1.PinnedImage = "module-real-estate";
            fcMainFormModule1.Text = "Real Estate";
            fcMainFormModule2.BackColor = System.Drawing.Color.FromArgb(80, 227, 194);
            fcMainFormModule2.Image = null;
            fcMainFormModule2.InactiveImage = "";
            fcMainFormModule2.IsActive = true;
            fcMainFormModule2.Name = "pp";
            fcMainFormModule2.PinnedImage = "module-personal-property";
            fcMainFormModule2.Text = "Personal Property";
            fcMainFormModule3.BackColor = System.Drawing.Color.FromArgb(245, 166, 35);
            fcMainFormModule3.Image = null;
            fcMainFormModule3.InactiveImage = "";
            fcMainFormModule3.IsActive = true;
            fcMainFormModule3.Name = "bl";
            fcMainFormModule3.PinnedImage = "module-tax-billing";
            fcMainFormModule3.Text = "Tax Billing";
            fcMainFormModule4.BackColor = System.Drawing.Color.FromArgb(0, 165, 214);
            fcMainFormModule4.Image = null;
            fcMainFormModule4.InactiveImage = "";
            fcMainFormModule4.IsActive = true;
            fcMainFormModule4.Name = "cl0";
            fcMainFormModule4.PinnedImage = "module-personal-property-collections";
            fcMainFormModule4.Text = "Personal Property Collections";
            fcMainFormModule5.BackColor = System.Drawing.Color.FromArgb(245, 72, 134);
            fcMainFormModule5.Image = null;
            fcMainFormModule5.InactiveImage = "";
            fcMainFormModule5.IsActive = true;
            fcMainFormModule5.Name = "ck";
            fcMainFormModule5.PinnedImage = "module-clerk";
            fcMainFormModule5.Text = "Clerk";
            fcMainFormModule6.BackColor = System.Drawing.Color.FromArgb(249, 202, 123);
            fcMainFormModule6.Image = null;
            fcMainFormModule6.InactiveImage = "";
            fcMainFormModule6.IsActive = true;
            fcMainFormModule6.Name = "ce";
            fcMainFormModule6.PinnedImage = "module-code-enforcement";
            fcMainFormModule6.Text = "Code Enforcement";
            fcMainFormModule7.BackColor = System.Drawing.Color.FromArgb(53, 191, 74);
            fcMainFormModule7.Image = null;
            fcMainFormModule7.InactiveImage = "";
            fcMainFormModule7.IsActive = true;
            fcMainFormModule7.Name = "bd";
            fcMainFormModule7.PinnedImage = "module-budgetary-system";
            fcMainFormModule7.Text = "Budgetary System";
            fcMainFormModule8.BackColor = System.Drawing.Color.FromArgb(255, 109, 96);
            fcMainFormModule8.Image = null;
            fcMainFormModule8.InactiveImage = "";
            fcMainFormModule8.IsActive = true;
            fcMainFormModule8.Name = "cr";
            fcMainFormModule8.PinnedImage = "module-cash-receipting";
            fcMainFormModule8.Text = "Cash Receipting";
            fcMainFormModule9.BackColor = System.Drawing.Color.FromArgb(98, 154, 169);
            fcMainFormModule9.Image = null;
            fcMainFormModule9.InactiveImage = "";
            fcMainFormModule9.IsActive = true;
            fcMainFormModule9.Name = "ut";
            fcMainFormModule9.PinnedImage = "module-utility-billing";
            fcMainFormModule9.Text = "Utility Billing";
            fcMainFormModule10.BackColor = System.Drawing.Color.FromArgb(188, 112, 254);
            fcMainFormModule10.Image = null;
            fcMainFormModule10.InactiveImage = "";
            fcMainFormModule10.IsActive = true;
            fcMainFormModule10.Name = "cl1";
            fcMainFormModule10.PinnedImage = "module-real-estate-collections";
            fcMainFormModule10.Text = "Real Estate Collections";
            fcMainFormModule11.BackColor = System.Drawing.Color.FromArgb(39, 84, 186);
            fcMainFormModule11.Image = null;
            fcMainFormModule11.InactiveImage = "";
            fcMainFormModule11.IsActive = true;
            fcMainFormModule11.Name = "bb";
            fcMainFormModule11.PinnedImage = "module-blue-book";
            fcMainFormModule11.Text = "Blue Book";
            fcMainFormModule12.BackColor = System.Drawing.Color.FromArgb(235, 122, 189);
            fcMainFormModule12.Image = null;
            fcMainFormModule12.InactiveImage = "";
            fcMainFormModule12.IsActive = true;
            fcMainFormModule12.Name = "mv";
            fcMainFormModule12.PinnedImage = "module-motor-vehicle-registration";
            fcMainFormModule12.Text = "Motor Vehicle Registration";
            fcMainFormModule13.BackColor = System.Drawing.Color.FromArgb(139, 87, 42);
            fcMainFormModule13.Image = null;
            fcMainFormModule13.InactiveImage = "";
            fcMainFormModule13.IsActive = true;
            fcMainFormModule13.Name = "fa";
            fcMainFormModule13.PinnedImage = "module-fixed-assets";
            fcMainFormModule13.Text = "Fixed Assets";
            fcMainFormModule14.BackColor = System.Drawing.Color.FromArgb(220, 99, 75);
            fcMainFormModule14.Image = null;
            fcMainFormModule14.InactiveImage = "";
            fcMainFormModule14.IsActive = true;
            fcMainFormModule14.Name = "ar";
            fcMainFormModule14.PinnedImage = "module-accounts-receivable";
            fcMainFormModule14.Text = "Accounts Receivable";
            fcMainFormModule15.BackColor = System.Drawing.Color.FromArgb(184, 69, 146);
            fcMainFormModule15.Image = null;
            fcMainFormModule15.InactiveImage = "";
            fcMainFormModule15.IsActive = true;
            fcMainFormModule15.Name = "py";
            fcMainFormModule15.PinnedImage = "module-payroll";
            fcMainFormModule15.Text = "Payroll";
            this.Modules.Add(fcMainFormModule1);
            this.Modules.Add(fcMainFormModule2);
            this.Modules.Add(fcMainFormModule3);
            this.Modules.Add(fcMainFormModule4);
            this.Modules.Add(fcMainFormModule5);
            this.Modules.Add(fcMainFormModule6);
            this.Modules.Add(fcMainFormModule7);
            this.Modules.Add(fcMainFormModule8);
            this.Modules.Add(fcMainFormModule9);
            this.Modules.Add(fcMainFormModule10);
            this.Modules.Add(fcMainFormModule11);
            this.Modules.Add(fcMainFormModule12);
            this.Modules.Add(fcMainFormModule13);
            this.Modules.Add(fcMainFormModule14);
            this.Modules.Add(fcMainFormModule15);
            this.Name = "MainForm";
            this.Text = "Window";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Controls.SetChildIndex(this.expirationButton, 0);
            this.Controls.SetChildIndex(this.PicArchive, 0);
            this.Controls.SetChildIndex(this.favouritesPanel, 0);
            this.Controls.SetChildIndex(this.navigationPanel, 0);
            this.Controls.SetChildIndex(this.homePagePanel, 0);
            this.navigationPanel.ResumeLayout(false);
            this.favouritesPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.homePageButton)).EndInit();
            this.homePagePanel.ResumeLayout(false);
            this.modulesPanel.ResumeLayout(false);
            this.modulesPanel.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.centerPanel.ResumeLayout(false);
            this.aboutPanel.ResumeLayout(false);
            this.aboutPanel.PerformLayout();
            this.currentUserPanel.ResumeLayout(false);
            this.currentUserPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slideLeftButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserMenuOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationUserMenuClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicArchive)).EndInit();
            this.PicArchive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgArchive)).EndInit();
            this.ResumeLayout(false);

		}

        #endregion

        private Wisej.Web.PictureBox pictureBox1;
        private Wisej.Web.PictureBox themeButton;
        private Wisej.Web.PictureBox settingsButton;
		private Wisej.Web.PictureBox newsButton;
		private Wisej.Web.PictureBox helpButton;
        private Wisej.Web.PictureBox slideLeftButton;
        private Wisej.Web.PictureBox navigationUserButton;
        private Wisej.Web.PictureBox navigationUserMenuOpen;
        private Wisej.Web.PictureBox navigationUserMenuClose;
        private Wisej.Web.Label label2;
        private Wisej.Web.Label label3;
		private Wisej.Web.Label labelUserName;
		private Wisej.Web.Line line1;
        private fecherFoundation.FCArrowContextMenu userContextMenu;
        private fecherFoundation.FCArrowContextMenu settingsContextMenuMain;
		private fecherFoundation.FCArrowContextMenu settingsContextMenu;
		private fecherFoundation.FCArrowContextMenu helpContextMenuMain;
		private fecherFoundation.FCArrowContextMenu helpContextMenu;
        private Wisej.Web.JavaScript javaScript1;
        private Wisej.Web.Panel userPanel;
        //FC:FINAL:CHN - issue #1516: Add showing archive image after running archive.
        public fecherFoundation.FCPictureBox PicArchive;
        public fecherFoundation.FCTextBox txtCommDest;
        public fecherFoundation.FCPictureBox imgArchive;
        private Wisej.Web.Button expirationButton;
        private MainFormTopPanel mainFormTopPanel1;
    }
}
