﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Autofac;
using fecherFoundation;
using SharedApplication.Extensions;
using SharedApplication.Telemetry;
using TWSharedLibrary;
using TWSharedLibrary.PrinterUtility;
using Wisej.Core;
using Wisej.Web;

namespace Main
{
    extern alias TWCL0000;

    static class Program
    {
        private static object logFileLocker = new object();
        private static object cleanTempFilesLocker = new object();
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Application.StartTask(CleanupTempFolder);

            //telemetry = StaticSettings.GlobalTelemetryService;
            var appStartedMsg = $"Application started. Session ID: {Application.SessionId.ToUpper()}";
            LogText(appStartedMsg);
            
            MainForm window = new MainForm();
            window.Show();
            Application.Title = "TRIO";

            //FC:FINAL:SBE - #1941 - implemented page reload in Default.html. Used Application.Exit to execute the client side event which reloads the app
            ////FC:FINAL:DDU:#1941 - assign event
            Application.SessionTimeout -= Application_SessionTimeout;
            Application.SessionTimeout += Application_SessionTimeout;

            //FC:FINAL:SBE - #1227 - log unhandled exception to log file
            Application.ThreadException -= Application_ThreadException;
            Application.ThreadException += Application_ThreadException;

        }

        private static void CleanupTempFolder()
        {
            DateTime currentTime = DateTime.Now;
            lock (cleanTempFilesLocker)
            {
                try
                {
                    foreach (var tempFileName in Directory.GetFiles(Path.Combine(Application.StartupPath, "Temp"), "*.*", SearchOption.AllDirectories))
                    {
                        FileInfo fileInfo = new FileInfo(tempFileName);
                        //delete files older than 1 day
                        if (fileInfo.Exists && fileInfo.CreationTime.AddDays(1) <= currentTime)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //don't do anything if file can't be deleted now. The process will run again when new session is started.
                }
            }
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Exception ex = e.Exception;
            if (ex.InnerException != null) ex = ex.InnerException;

            var telem = StaticSettings.GlobalTelemetryService;
            telem.TrackException(ex);

            var errmsg = $"Exception in session with ID {Application.SessionId.ToUpper()}: {ex.Message} - {ex.StackTrace}";
            LogText(errmsg);

            FCMessageBox.Show(ex.Message,MsgBoxStyle.Critical,"ERROR");
            //throw ex;

        }

        private static void LogText(string text)
        {
            //telemetry.TrackTrace($"{DateTime.Now.ToShortTimeString()} - {text}");

            string logFolder = Path.Combine(Application.StartupPath, "Logs");
            if (!Directory.Exists(logFolder))
            {
                Directory.CreateDirectory(logFolder);
            }
            DateTime now = DateTime.Now;
            string logFileName = $"ApplicationThreadExceptions-{now.Year}-{now.Month}-{now.Day} .log";
            logFileName = Path.Combine(logFolder, logFileName);
            lock (logFileLocker)
            {
                using (var logFileWriter = new StreamWriter(logFileName, true))
                {
                    logFileWriter.WriteLine($"{now.ToShortTimeString()} - {text}");
                    StaticSettings.GlobalTelemetryService?.TrackTrace(text);
                }
            }
        }

        private static void Application_SessionTimeout(object sender, System.ComponentModel.HandledEventArgs e)
        {
            //List<Form> forms = new List<Form>();
            //foreach (Form f in Application.OpenForms)
            //{
            //    forms.Add(f);
            //}
            //foreach (Form f in forms)
            //{
            //    try
            //    {
            //        if (!f.IsDisposed && !f.Disposing)
            //            f.Close();
            //    }
            //    catch { }
            //}
            //Application.Session.Clear();
            //e.Handled = true;


            StaticSettings.GlobalTelemetryService.TrackEvent("Session Timeout");
            
            Application.Exit();
        }


        [WebMethod]
        public static void SetMachineIdentifier(string identifier)
        {

            StaticSettings.gGlobalSettings.MachineOwnerID = identifier;
            PrintingViaWebSocketsVariables.Statics.MachineName = identifier;
            if (!identifier.IsNullOrWhiteSpace())
            {
                PrinterUtility.LoadPrintingSettings();
            }
        }
    }
}