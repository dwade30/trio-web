﻿
using System;
using Wisej.Web;
using fecherFoundation;
using System.ComponentModel;

namespace Main
{
    public partial class HomePage : Form
    {
        private FCMainFormModules modules;

        public HomePage()
        {
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            this.modules = new FCMainFormModules(this);
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCMainFormModules Modules
        {
            get
            {
                return this.modules;
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (this.modulesPanel != null)
            {
                //center modules list
                this.CenterControl(this.modulesPanel, this);
            }
            if (this.aboutPanel != null)
            {
                //center about panel
                this.CenterControl(this.aboutPanel, this);
            }
        }

        private void CenterControl(Control child, ContainerControl parent)
        {
            if (parent.Width < child.Width)
            {
                child.Left = 0;
            }
            else
            {
                child.Left = (parent.Width - child.Width) / 2;
            }
        }

        internal void RefreshModules()
        {
            int pinnedCurrent = 1;
            int moduleCurrent = 1;
            int pinnedCount = this.modules.Where(module => module.IsPinned).Count();
            int notPinnedCount = this.modules.Count - pinnedCount;
            int pinnedRows = 0;
            if (pinnedCount > 0)
            {
                pinnedRows = ((pinnedCount - 1) / 5) + 1;
                pinnedModulesLabel.Visible = true;
            }
            else
            {
                pinnedModulesLabel.Visible = false;
            }
            int col1Count = (int)Math.Ceiling((decimal)(notPinnedCount) / 2);

            int PINNED_HEIGHT = 204;
            int PINNED_WIDTH = 160;
            int MODULE_HEIGHT = 58;
            int MODULE_WIDTH = 400;
            int TOP_PADDING = 25;
            int FAVOURITES_TOP = 60;
            int FAVOURITES_HEIGHT = 52;

            foreach (var module in this.modules)
            {
                module.FavouritesButton.Parent = null;

                //module.FavouritesButton.Parent = null;
                //add pinned button to form if it was not added before
                if (module.IsPinned && !this.modulesPanel.Contains(module.PinnedModuleButton))
                {
                    this.modulesPanel.Controls.Add(module.PinnedModuleButton);
                }
                //add favourites button to form if it was not added before
                if (module.IsPinned && !this.favouritesPanel.Controls.Contains(module.FavouritesButton))
                {
                    this.favouritesPanel.Controls.Add(module.FavouritesButton);
                }
                //add module button to form if it was not added before
                if (!module.IsPinned && !this.modulesPanel.Contains(module.ModuleButton))
                {
                    this.modulesPanel.Controls.Add(module.ModuleButton);
                }

                if (module.IsPinned)
                {
                    //pinned button
                    int left = ((pinnedCurrent - 1) % 5) * PINNED_WIDTH;
                    int top = ((pinnedCurrent - 1) / 5) * PINNED_HEIGHT;
                    module.PinnedModuleButton.Left = left;
                    module.PinnedModuleButton.Top = top + TOP_PADDING;

                    //favourites button
                    int favouritesTop = ((pinnedCurrent - 1) * FAVOURITES_HEIGHT) + FAVOURITES_TOP;
                    module.FavouritesButton.Top = favouritesTop;
                    module.FavouritesButton.Left = 0;

                    if (pinnedCurrent == 2)
                    {
                        module.FavouritesButton.Active = true;
                    }
                    else
                    {
                        module.FavouritesButton.Active = false;
                    }

                    pinnedCurrent++;
                }
                else
                {

                    int left = ((moduleCurrent - 1) / col1Count) * MODULE_WIDTH;
                    int top = (moduleCurrent - 1) * MODULE_HEIGHT;
                    if (moduleCurrent > col1Count)
                    {
                        left = MODULE_WIDTH;
                        top = (moduleCurrent - (col1Count + 1)) * MODULE_HEIGHT;
                    }

                    if (pinnedRows > 0)
                    {
                        top = top + (pinnedRows * PINNED_HEIGHT);
                        top = top + TOP_PADDING;
                    }

                    module.ModuleButton.Top = top;
                    module.ModuleButton.Left = left;

                    moduleCurrent++;
                }
            }

            int height = (pinnedRows * PINNED_HEIGHT) + (col1Count * MODULE_HEIGHT);
            if (pinnedRows > 0)
            {
                height += TOP_PADDING;
            }

            this.modulesPanel.Height = height;
        }

        protected internal void OnModuleClick(FCMainFormModule module, bool hideHomePage = true)
        {
            foreach (var button in this.favouritesPanel.Controls)
            {
                if (button is FCFavouritesModuleButton)
                {
                    FCFavouritesModuleButton favouriteButton = button as FCFavouritesModuleButton;
                    if (favouriteButton.Module == module)
                    {
                        favouriteButton.Active = true;
                    }
                    else
                    {
                        favouriteButton.Active = false;
                    }
                }
            }
            if (hideHomePage)
            {
                this.HomePageVisible = false;
            }
        }
    }
}
