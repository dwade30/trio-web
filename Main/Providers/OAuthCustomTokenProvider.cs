﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using Autofac.Integration.Owin;
using Main.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security;
using SharedApplication.AppConfiguration;
using SharedDataAccess.ClientSettings;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Ajax.Utilities;
using SharedApplication;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.Extensions;
using SharedDataAccess;

namespace Main.Providers
{
    public class OAuthCustomTokenProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
           // HttpRuntime.Cache.Get()
           //if (HttpRuntime.Cache.Get("ClientSettings") == null)
           //{
           //    var config = LoadClientConfiguration();
           //    if (config != null)
           //    {
           //        HttpRuntime.Cache.Add("ClientSettings", config, null,System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0,30,0), CacheItemPriority.Normal,null );
           //    }
           //}
            return Task.Factory.StartNew(() =>
            {
                TrioWebConfiguration config;
                if (HttpRuntime.Cache.Get("ClientSettings") == null)
                {
                    try
                    {
                        config = LoadClientConfiguration();
                    }
                    catch (Exception ex)
                    {
                        context.SetError("invalid configuration", "invalid client");
                        return;
                    }
                    if (config != null)
                    {
                        HttpRuntime.Cache.Add("ClientSettings", config, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 30, 0), CacheItemPriority.Normal, null);
                    }
                }
                else
                {
                    config = (TrioWebConfiguration)HttpRuntime.Cache.Get("ClientSettings");
                }

                if (config == null)
                {
                    context.SetError("invalid configuration", "invalid client");
                    return;
                }
                var userName = context.UserName;
                var password = context.Password;
                var host = context.Request.Host.Value;
                var clientName = "";
                if (host.ToUpper().Contains(".TRIO-WEB.COM"))
                {
                    clientName = host.Left(host.ToUpper().IndexOf(".TRIO-WEB.COM"));
                    clientName = clientName.ToUpper().Replace("HTTPS://", "");
                }
                else
                {
                    clientName = "Default";
                }

                var authenticationContext = GetClientSettingsContext(config, clientName);
                if (authenticationContext == null)
                {
                    context.SetError("invalid client context", "invalid client context");
                    return;
                }
                var clientGuid = GetClientGuid(authenticationContext, clientName);
                if (clientGuid == Guid.Empty)
                {
                    context.SetError("invalid_grant", "Provided credentials are incorrect");
                    return;
                }
                var userObject = GetUser(authenticationContext, userName, clientGuid);
                if (userObject == null)
                {
                    context.SetError("invalid_grant", "Provided credentials are incorrect");
                    return;
                }
                string strCalcHash = "";

                CryptoUtility cryptoUtility = new CryptoUtility();

                strCalcHash = cryptoUtility.ComputeSHA512Hash(password, userObject.Salt ?? "");
                if (strCalcHash != userObject.Password)
                {
                    context.SetError("invalid_grant", "Provided credentials are incorrect");
                    return;
                }
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Sid, userObject.Id.ToString()),
                    new Claim(ClaimTypes.Name, userObject.UserID),                   
                };
                claims.Add(new Claim(ClaimTypes.Role,"user"));
                var data = new Dictionary<string,string>()
                {
                    {"userName",userObject.UserID },
                    {"roles","user" }
                };
                var properties = new AuthenticationProperties(data);
                ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims,StartUp.OAuthOptions.AuthenticationType);
                var ticket = new AuthenticationTicket(oAuthIdentity,properties);
                context.Validated(ticket);
            });
        }

        private Guid GetClientGuid(IClientSettingsContext clientSettingsContext, string clientName)
        {
            if (clientName == "")
            {
                clientName = "Default";
            }

            var clientObject = clientSettingsContext.Clients.FirstOrDefault(c => c.Name == clientName || (c.Name == "" && clientName == "Default"));
            if (clientObject != null)
            {
                return clientObject.ClientIdentifier;
            }

            return Guid.Empty;
        }
        private SharedApplication.ClientSettings.Models.User GetUser(IClientSettingsContext clientSettingsContext,
            string userName,Guid clientGuid)
        {
            return clientSettingsContext.Users.FirstOrDefault(u =>
                u.UserID == userName && u.ClientIdentifier == clientGuid);
        }

        private IClientSettingsContext GetClientSettingsContext(TrioWebConfiguration trioConfiguration,string clientName)
        {
            var trioClient = trioConfiguration.Clients.FirstOrDefault(c => c.Name == clientName || (c.Name == "" && clientName == "Default"));
            if (trioClient == null)
            {
                return null;
            }
            var contextFactory = new TrioContextFactory(new DataContextDetails()
            {
                DataEnvironment = "",
                DataSource = trioClient.DataSource,
                EnvironmentGroup = clientName,
                Password = trioClient.Password,
                UserID = trioClient.User
            }, new cGlobalSettings()
            {
                ClientEnvironment = clientName,
                EnvironmentGroup = "Live",
                DataEnvironment = "All"
            });
            return contextFactory.GetClientSettingsContext();           
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key,property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        private TrioWebConfiguration LoadClientConfiguration()
        {
            var configLoader = new TrioConfigLoader();
            var config = configLoader.LoadConfiguration(System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath,
                "TRIOClientConfig.xml"));       
            return config;
        }
    }
}