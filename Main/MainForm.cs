extern alias TWCL0000;
extern alias TWCL0001;
extern alias TWCR0000;
extern alias TWGNENTY;
extern alias TWPP0000;
extern alias TWRE0000;

using Autofac;
using fecherFoundation;
using Main.Telemetry;
using MediatR.Extensions.Autofac.DependencyInjection;
using SharedApplication;
using SharedApplication.Authorization;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Models;
using SharedApplication.Telemetry;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Extensions;
using SharedDataAccess;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using Microsoft.ApplicationInsights;
using Newtonsoft.Json;
using SharedApplication.BlueBook;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Queries;
using SharedDataAccess.Extensions;
using SharedDataAccess.TaxCollections;
using TrioDataAPIConsumer.BlueBook;
using TRIOPriceDigestsAPIConsumer;
using TWGNENTY;
using TWGNENTY.Authentication;
using TWMV0000;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Core;
using Wisej.Web;

namespace Main
{
    public partial class MainForm : fecherFoundation.FCMainForm
    {
        //FC:FINAL:IPI - #159 - save/ retrieve user's pinned modules
        private TWGNENTY::Global.cSettingsController settingsController = new TWGNENTY::Global.cSettingsController();
        private Dictionary<string, string> moduleKeyToModuleName = new Dictionary<string, string>();
        private Dictionary<string, string> moduleNameToModuleKey = new Dictionary<string, string>();
        private string currentModule = string.Empty;
        private bool currentModuleHasExit = false;
        private Autofac.IContainer diContainer;
        internal const string DEFAULT_COLOR_SCHEME = "slate";
        public MainForm()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            FCMainForm.InstancePtr = this;
            //FC:FINAL:SBE - #4618 - set the MainForm name to "MDIParent", because FCUtils.CloseChildForms() has this name hardcoded to skip it from Close.
            //the name is automacally overriden in MainForm.designer.cs to "MainForm", then we need to set it here to "MDIParent"
            this.Name = "MDIParent";
            moduleKeyToModuleName.Add("TWCL0000", "Real Estate Collections");
            moduleKeyToModuleName.Add("TWCL0001", "Personal Property Collections");
            moduleKeyToModuleName.Add("TWFA0000", "Fixed Assets");
            moduleKeyToModuleName.Add("TWBD0000", "Budgetary System");
            moduleKeyToModuleName.Add("TWCR0000", "Cash Receipting");
            moduleKeyToModuleName.Add("TWAR0000", "Accounts Receivable");
            moduleKeyToModuleName.Add("TWBL0000", "Tax Billing");
            moduleKeyToModuleName.Add("TWPP0000", "Personal Property");
            moduleKeyToModuleName.Add("TWUT0000", "Utility Billing");
            moduleKeyToModuleName.Add("TWRE0000", "Real Estate");
            moduleKeyToModuleName.Add("TWCE0000", "Code Enforcement");
            moduleKeyToModuleName.Add("TWRB0000", "Blue Book");
            moduleKeyToModuleName.Add("TWMV0000", "Motor Vehicle Registration");
            moduleKeyToModuleName.Add("TWCK0000", "Clerk");
            moduleKeyToModuleName.Add("TWPY0000", "Payroll");
            moduleKeyToModuleName.Add("TWGNENTY", "General Entry");
            moduleKeyToModuleName.Add("TWE90000", "E911");

            foreach (var key in moduleKeyToModuleName.Keys)
            {
                moduleNameToModuleKey.Add(moduleKeyToModuleName[key], key);
            }
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            var telemInit = new TelemetryInitialization
            {
                DeviceType = Application.Browser.Device,
                DeviceId = Application.ServerName,
                WiseJSessionId = Application.SessionId.ToUpper(),
                Environment = "",
                Username = "",
                UserAgent = Application.UserAgent,
                AppVersion = $"{App.Major}.{App.Minor}.{App.Revision}",
                Browser = $"{Application.Browser.Type} {Application.Browser.Version}",
                UserHostAddress = Application.UserHostAddress,
                UserHostName = Application.UserHostName
            };
            StaticSettings.GlobalTelemetryService = new TelemetryService(telemInit);
            var succes = TWGNENTY::TWGNENTY.modTrioStart.Main();
            //FC:FINAL:AM:#1883 - show the login dialog when the check digits routine fails
            if (succes != StartupResponse.StartNormally)
            {
                if (succes == StartupResponse.RedirectToSupport)
                {
                    Application.Navigate("https://trio-web.na2.teamsupport.com", "_top");
                    Close();
                    return;
                }
                else
                {
                    Logout();
                }
            }
            LoadColorScheme();

            WireUpDependencies();
            WireUpGlobalHacks();
            this.NavigationMenu.menuLogger = new TelemetryMenuLogger(StaticSettings.GlobalTelemetryService);
			currentModule = "TWGNENTY";
            //FC:FINAL:IPI - #1231 - set the logged in user
            labelUserName.Text = TWSharedLibrary.Variables.Statics.UserName;
            
            SaveRetrieveUsersPinnedModules();

            SetupHelpAndUserContextMenus();

            PicArchive.SendToBack();

            LoadEnvironmentStatics();
            LoadUserStatics();
            LoadModuleSettings();

            modEntryMenuForm.EMMain();

            HomePageVisible = true;
            RefreshModules();

            mainFormTopPanel1.UserName = TWSharedLibrary.Variables.Statics.UserName + "\r\n" +
                                TWSharedLibrary.Variables.Statics.DataGroup;

            UpdateSiteVersion();
           
           // LoadPrintingSettings();
            SendStartupAnalytics();
        }

        public override void LoadColorScheme(bool archiveMode = false)
        {
	        string colorScheme = "";

			if (!archiveMode)
	        {
		        colorScheme = TWGNENTY::Global.modRegistry.GetRegistryKey("UserColorScheme");
		        if (string.IsNullOrEmpty(colorScheme))
		        {
			        colorScheme = DEFAULT_COLOR_SCHEME;
		        }
			}
	        else
	        {
		        colorScheme = "archive";
	        }
            
            ApplyColorScheme(colorScheme);
        }

        internal void ApplyColorScheme(string colorScheme)
        {
            if (colorScheme == DEFAULT_COLOR_SCHEME)
            {
                Application.LoadTheme("HarrisTheme");
            }
            else
            {
                Application.LoadTheme("HarrisTheme", new string[] { $"{colorScheme}\\ColorScheme.mixin" });
            }
            //FC:FINAl:SBE - should be removed after using a Wisej version greater than 2.0.54
            Application.Reload();
        }

        private void UpdateSiteVersion()
        {
	        var siteVersion = StaticSettings.GlobalCommandDispatcher.Send(new GetSiteVersion()).Result;

	        if (siteVersion != Application.ProductVersion)
	        {
		        StaticSettings.GlobalCommandDispatcher.Send(new SaveSiteVersion
		        {
			        Version = Application.ProductVersion
		        });
	        }
        }

        private void SaveRetrieveUsersPinnedModules()
        {
            string[] pinnedModules = settingsController.GetSettingValue("PinnedModules", "", "User", TWSharedLibrary.Variables.Statics.OwnerID, "")
                                                       .Split(';');

            if (pinnedModules != null && pinnedModules.Length > 0)
            {
                foreach (FCMainFormModule module in Modules)
                {
                    module.IsPinned = Array.Exists(pinnedModules, moduleName => moduleName.Trim()
                                                                                          .ToUpper()
                                                                                == module.Name.Trim()
                                                                                         .ToUpper());
                }
            }
        }

        private void SetupHelpAndUserContextMenus()
        {
            userContextMenu.Items.Add(new FCArrowContextMenuItem("Change User Password", true));
            userContextMenu.Items.Add(new FCArrowContextMenuItem("Logout", true));

            helpContextMenuMain.Items.Add(new FCArrowContextMenuItem("Start WebEx Support Session", true));
            helpContextMenuMain.Items.Add(new FCArrowContextMenuItem("Check Database Structure", true));
            helpContextMenuMain.Items.Add(new FCArrowContextMenuItem("Program Information", true));
            helpContextMenuMain.Items.Add(new FCArrowContextMenuItem("About", true));

            helpContextMenu.Items.Add(new FCArrowContextMenuItem("Start WebEx Support Session", true));
            helpContextMenu.Items.Add(new FCArrowContextMenuItem("Check Database Structure", true));
            helpContextMenu.Items.Add(new FCArrowContextMenuItem("Program Information", true));
            helpContextMenu.Items.Add(new FCArrowContextMenuItem("About", true));
        }

        public override void ConfigureSettingsContextMenu(bool permissionSystemMaintenance, bool permissionEliminate, bool permissionDelete)
        {
            base.ConfigureSettingsContextMenu(permissionSystemMaintenance, permissionEliminate, permissionDelete);
            var userQueryHandler = diContainer.Resolve<IQueryHandler<UserSearchCriteria, IEnumerable<User>>>();
            bool allowSiteUpdate;

            if (!StaticSettings.gGlobalSettings.IsHostedSite)
            {
                if (TWSharedLibrary.Variables.Statics.IntUserID == -1)
                {
                    allowSiteUpdate = true;
                }
                else
                {
                    var user = userQueryHandler.ExecuteQuery(new UserSearchCriteria
                    {
                        UserId = TWSharedLibrary.Variables.Statics.IntUserID
                    }).FirstOrDefault();

                    allowSiteUpdate = user?.CanUpdateSite ?? false;
                }
            }
            else
            {
                allowSiteUpdate = false;
            }


            settingsContextMenuMain.Clear();
            if (permissionSystemMaintenance)
            {
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Customize", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Printer Setup",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Password Setup",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Signatures",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Edit Operators",
                    permissionSystemMaintenance));
            }

            settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Central Parties", true));
            if (permissionEliminate)
            {
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Eliminate Duplicate Parties[+]",
                    permissionEliminate));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Manual Merge", true));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Automatic Merge", true));
            }

            if (permissionDelete)
            {
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Delete Unused Parties",
                    permissionDelete));
            }

            if (permissionSystemMaintenance)
            {
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Audits[+]", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Internal Audit",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Audit Changes",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Audit Changes Archive",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("[--]Purge Audit Changes",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Email", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Calendar", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Process Expiration Updates",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Backup", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Restore", permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Upload Files",
                    permissionSystemMaintenance));
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Process Fix Program",
                    permissionSystemMaintenance));
            }

            if (allowSiteUpdate)
            {
                settingsContextMenuMain.Items.Add(new FCArrowContextMenuItem("Schedule Update", allowSiteUpdate));
            }

            settingsContextMenu.Clear();
            if (permissionSystemMaintenance)
            {
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Customize", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Printer Setup", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Password Setup",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Signatures", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Edit Operators",
                    permissionSystemMaintenance));
            }

            settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Central Parties", true));
            if (permissionEliminate)
            {
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Eliminate Duplicate Parties[+]",
                    permissionEliminate));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Manual Merge", true));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Automatic Merge", true));
            }

            if (permissionDelete)
            {
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Delete Unused Parties", permissionDelete));
            }

            if (permissionSystemMaintenance)
            {
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Audits[+]", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Internal Audit",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Audit Changes",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Audit Changes Archive",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("[--]Purge Audit Changes",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Email", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Calendar", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Process Expiration Updates",
                    permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Backup", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Restore", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Upload Files", permissionSystemMaintenance));
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Process Fix Program",
                    permissionSystemMaintenance));
            }

            if (allowSiteUpdate)
            {
                settingsContextMenu.Items.Add(new FCArrowContextMenuItem("Schedule Update", allowSiteUpdate));
            }
        }

        public override void OnModuleClick(FCMainFormModule module, bool hideHomePage = true)
        {
	        base.OnModuleClick(module, hideHomePage);
	        OpenModule(moduleNameToModuleKey[module.Text], false, false);
        }

        protected override void OnStatusBarText1Click()
        {
	        if (AllFormsClosed())
	        {
		        switch (currentModule)
		        {
			        case "TWPP0000":
				        var ppForm = new TWPP0000::Global.SelectArchive();
				        ppForm.ShowDialog(App.MainForm);
				        break;
			        case "TWRE0000":
						var reForm = new TWRE0000::Global.SelectArchive();
						reForm.ShowDialog(App.MainForm);
						break;
		        }
			}
        }

        private bool AllFormsClosed()
        {
	        int intCounter = 0;
	        int x;

	        if (Application.OpenForms.Count > 1)
	        {
		        intCounter = 0;
		        for (x = 0; x <= Application.OpenForms.Count - 1; x++)
		        {
			        if (Application.OpenForms[x].Visible)
			        {
				        intCounter += 1;
			        }
		        }
		        if (intCounter > 1)
		        {
			        MessageBox.Show("You must close all open forms before you may change your year", "Close Forms", MessageBoxButtons.OK, MessageBoxIcon.Information);
			        return false;
		        }
		        else
		        {
			        for (x = Application.OpenForms.Count - 1; x >= 0; x--)
			        {
				        if (Application.OpenForms[x].Visible == false)
				        {
					        Application.OpenForms[x].Close();
				        }
			        }

			        return true;
		        }
	        }
	        else
	        {
		        return true;
	        }
		}

        protected override void OnStatusBarText2Click()
        {
            if (App.MainForm.StatusBarText2.StartsWith("GL Year"))
            {
                if (AllFormsClosed())
                {
					TWBD0000.frmChangeGLYear.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
                }
            }
        }

        //FC:FINAL:IPI - #159 - save/ retrieve user's pinned modules
        public override void PinModuleClick()
        {
            base.PinModuleClick();

            RefreshModules();

            string pinnedModules = "";
            foreach (FCMainFormModule module in Modules)
            {
                if (module.IsPinned)
                {
                    pinnedModules += module.Name.Trim().ToUpper() + ";";
                }
            }

            settingsController.SaveSetting(pinnedModules, "PinnedModules", "", "User", TWSharedLibrary.Variables.Statics.OwnerID, "");
        }

        private void themeButton_Click(object sender, System.EventArgs e)
        {
            using (SelectTheme selectTheme = new SelectTheme())
            {
                selectTheme.StartPosition = FormStartPosition.CenterScreen;
                selectTheme.ShowDialog();
            }
        }

        private void NavigationUserMenuOpen_Click(object sender, System.EventArgs e)
        {
            System.Drawing.Point point = navigationUserButton.PointToScreen(System.Drawing.Point.Empty);
            userContextMenu.Direction = TabAlignment.Bottom;
            userContextMenu.Alignment = Placement.TopRight;
            userContextMenu.ShowPopup(point.X - 9, point.Y);
            navigationUserMenuOpen.Visible = false;
            navigationUserMenuClose.Visible = true;
        }

        private void userButtonMain_Click(object sender, System.EventArgs e)
        {
            var userButtonMain = sender as Control;
            System.Drawing.Point point = userButtonMain.PointToScreen(System.Drawing.Point.Empty);
            userContextMenu.Direction = TabAlignment.Top;
            userContextMenu.Alignment = Placement.BottomLeft;
            userContextMenu.ShowPopup(point.X + userButtonMain.Width + 3, point.Y + userButtonMain.Height + 5);
        }

        private void helpButton_Click(object sender, System.EventArgs e)
        {
            System.Drawing.Point point = helpButton.PointToScreen(System.Drawing.Point.Empty);
            helpContextMenu.Direction = TabAlignment.Left;
            helpContextMenu.Alignment = Placement.RightBottom;
            helpContextMenu.ShowPopup(point.X + helpButton.Width + 5, point.Y + helpButton.Height + 5);
        }

        private void helpButtonMain_Click(object sender, System.EventArgs e)
        {
            var helpButtonMain = sender as Control;
            System.Drawing.Point point = helpButtonMain.PointToScreen(System.Drawing.Point.Empty);
            helpContextMenuMain.Direction = TabAlignment.Top;
            helpContextMenuMain.Alignment = Placement.BottomLeft;
            helpContextMenuMain.ShowPopup(point.X + helpButtonMain.Width + 5, point.Y + helpButtonMain.Height + 5);
        }

        private void settingsButton_Click(object sender, System.EventArgs e)
        {
            System.Drawing.Point point = settingsButton.PointToScreen(System.Drawing.Point.Empty);
            settingsContextMenu.Direction = TabAlignment.Left;
            settingsContextMenu.Alignment = Placement.RightBottom;
            settingsContextMenu.ShowPopup(point.X + settingsButton.Width + 10, point.Y + settingsButton.Height - 5);
        }

        private void settingsButtonMain_Click(object sender, System.EventArgs e)
        {
            var settingsButtonMain = sender as Control;
            System.Drawing.Point point = settingsButtonMain.PointToScreen(System.Drawing.Point.Empty);
            settingsContextMenuMain.Direction = TabAlignment.Top;
            settingsContextMenuMain.Alignment = Placement.BottomLeft;
            settingsContextMenuMain.ShowPopup(point.X + settingsButtonMain.Width + 10, point.Y + settingsButtonMain.Height + 5);
        }

        private void NavigationUserMenuClose_Click(object sender, System.EventArgs e)
        {
            navigationUserMenuClose.Visible = false;
            navigationUserMenuOpen.Visible = true;
        }

        private void UserContextMenu_Closed(object sender, System.EventArgs e)
        {
            if (navigationUserMenuClose.Visible)
            {
                navigationUserMenuClose.Visible = false;
                navigationUserMenuOpen.Visible = true;
            }

            if (mainFormTopPanel1.userMenuClose.Visible)
            {
                mainFormTopPanel1.userMenuClose.Visible = false;
                mainFormTopPanel1.userMenuOpen.Visible = true;
            }
        }

        private void SlideLeftButton_Click(object sender, System.EventArgs e)
        {
            navigationPanel.Visible = !navigationPanel.Visible;
        }

        private void UserContextMenu_ItemClick(object sender, System.EventArgs e)
        {
            userContextMenu.Close();
            string item = sender.ToString();
            //StaticSettings.GlobalTelemetryService.TrackEvent("Menu Item Chosen", new Dictionary<string, string>()
            //{
            //    {"Menu Item Name", item},
            //    {"Menu Caption", item},
            //    {"Parent Menu","User Context"},
            //    {"Menu Origin","Home"}
            //});
            switch (item)
            {
                case "Logout":
                    Logout();
                    break;

                case "Change User Password":
                    var contextFactory = GetContextFactory();
                    var clientSettingsContext = contextFactory.GetClientSettingsContext();
                    var userDataAccess = new UserDataAccess(clientSettingsContext, StaticSettings.gGlobalSettings);
                    var authenticationService = new AuthenticationService(userDataAccess, contextFactory.GetSystemSettingsContext());

                    App.MainForm.ApplicationIcon = "trio-logo-dark";
                    var userId = TWGNENTY::Global.modGlobalConstants.Statics.clsSecurityClass.Get_UserID();

                    var user = authenticationService.GetUserByID(userId);
                    authenticationService.ChangePasswordNow(user);
                    break;
            }
        }

        private TrioContextFactory GetContextFactory()
        {
            var dataDetails = new DataContextDetails();
            dataDetails.DataSource = StaticSettings.gGlobalSettings.DataSource;
            dataDetails.DataEnvironment = StaticSettings.gGlobalSettings.DataEnvironment;
            dataDetails.Password = StaticSettings.gGlobalSettings.Password;
            dataDetails.UserID = StaticSettings.gGlobalSettings.UserName;
            var contextFactory = new TrioContextFactory(dataDetails, StaticSettings.gGlobalSettings);
            return contextFactory;
        }

        private void SettingsContextMenuMain_ItemClick(object sender, System.EventArgs e)
        {
            App.MainForm.ApplicationIcon = "trio-logo-dark";
            settingsContextMenuMain.Close();
            settingsContextMenu.Close();
            string item = sender.ToString();
            //StaticSettings.GlobalTelemetryService.TrackEvent("Menu Item Chosen", new Dictionary<string, string>()
            //{
            //    {"Menu Item Name", item},
            //    {"Menu Caption", item},
            //    {"Parent Menu","Settings"},
            //    {"Menu Origin","Home"}
            //});
            switch (item)
            {
                case "Customize":
                    TWGNENTY::TWGNENTY.frmGNCustomize.InstancePtr.Show(FormShowEnum.Modal);
                    break;

                case "Printer Setup":
                    //var currentMachine = StaticSettings.gGlobalSettings.MachineOwnerID;
                    var currentMachine = PrintingViaWebSocketsVariables.Statics.MachineName;
                    if (currentMachine.IsNullOrWhiteSpace() )
                    {
                        this.javaScript1.Eval("GetLocalMachineId('44850')");
                    }
                    else if (!PrintingViaWebSockets.IsWebSocketConnectionOpen(currentMachine))
                    {
                        this.javaScript1.Eval("CheckLocalMachineId('44850')"); //the return value isn't needed. Just make the call to trigger a connection retry
                        Task.Delay(500);
                    }

                    if (TWGNENTY::Global.ClientPrintingSetup.InstancePtr.CheckConfiguration())
                    {
                        TWGNENTY::TWGNENTY.frmPrinters.InstancePtr.Show(FormShowEnum.Modal);
                    }
                    break;

                case "Password Setup":
                    var contextFactory = GetContextFactory();
                    var clientSettingsContext = contextFactory.GetClientSettingsContext();
                    var userDataAccess = new UserDataAccess(clientSettingsContext, StaticSettings.gGlobalSettings);
                    var authenticationService = new AuthenticationService(userDataAccess, contextFactory.GetSystemSettingsContext());
                    var clstempdc = new clsDRWrapper();

                    ShowSecurityForm(authenticationService, clstempdc, userDataAccess);

                    break;
                case "Signatures":
                    TWGNENTY.frmSetupSignatures.InstancePtr.Init(
                        FCConvert.ToString(
                            TWGNENTY.modGNBas.Statics.clsGESecurity.Check_Permissions(TWGNENTY.modSecurity2.PASSWORDSETUP)) == "F");
                    break;

                case "Edit Operators":
                    TWGNENTY.frmTellerSetup.InstancePtr.Show(FormShowEnum.Modal);
                    break;

                case "Central Parties":
                    TWGNENTY::Global.frmCentralPartySearch.InstancePtr.strMod = "GN";
                    TWGNENTY::Global.frmCentralPartySearch.InstancePtr.Show(FormShowEnum.Modal);
                    break;

                case "Manual Merge":
                    TWGNENTY::Global.frmCentralPartyMerge.InstancePtr.Show(FormShowEnum.Modal);
                    break;

                case "Automatic Merge":
                    TWGNENTY.frmCentralPartiesAutoMerge.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Delete Unused Parties":
                    var dup = new TWGNENTY.frmDeleteUnusedParties();
                    dup.Show(FormShowEnum.Modal);
                    break;
                case "Internal Audit":
                    TWGNENTY.frmInternalAudit.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Audit Changes":
                    TWGNENTY::Global.frmReportViewer.InstancePtr.Init(TWGNENTY.rptAuditReport.InstancePtr, showModal: true);
                    break;
                case "Audic Changes Archive":
                    TWGNENTY.frmSetupAuditArchiveReport.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Purge Audit Changes":
                    TWGNENTY.frmPurgeAuditArchive.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Email":
                    //FC:FINAL:RPU:#1255 - Send also information if was opened from general entry
                    //TWGNENTY::Global.frmEMail.InstancePtr.Init(showAsModalForm:true);
                    TWGNENTY::Global.frmEMail.InstancePtr.Init(showAsModalForm: true, generalEntry: true);
                    break;
                case "Calendar":
                    TWGNENTY.frmScheduler.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Process Expiration Updates":
                    TWGNENTY.MDIParent.FixProcess();
                    break;
                case "Backup":
                    TWGNENTY.frmBackup.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "Restore":
                    MessageBox.Show("All database restores require our assistance. Please contact TRIO Support to restore your database.<br/><br/><b>Phone:</b><br/>888-942-6222<br/><br/><b>Email:</b></br><a href='mailto:trio@harrislocalgov.com'>trio@harrislocalgov.com</a><br/><br/><b>Support site:</b><br/><a href='https://trio-web.na2.teamsupport.com' target='_blank'>https://trio-web.na2.teamsupport.com</a>", "Contact TRIO", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    break;
                case "Program Update":
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                    TWGNENTY.frmChilkatDownload.InstancePtr.blnFromUpdateReminder = false;
                    TWGNENTY.frmChilkatDownload.InstancePtr.Show(FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = 0;
                    break;
                case "Upload Files":
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                    TWGNENTY.frmChilkatUpload.InstancePtr.Show(FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = 0;
                    break;
                case "Process Fix Program":
                    FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                    TWGNENTY.frmProcessFixProgram.InstancePtr.Show(FormShowEnum.Modal);
                    FCGlobal.Screen.MousePointer = 0;
                    break;
                case "Schedule Update":
	                if (!StaticSettings.GlobalCommandDispatcher.Send(new ScheduleUpdate()).Result)
					{
						MessageBox.Show("You are already using the most current version of the software.", "No Updates Available",
		                MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
	                break;
            }
        }

        private static void ShowSecurityForm(AuthenticationService authenticationService, clsDRWrapper clstempdc, UserDataAccess userDataAccess)
        {
            if (TWGNENTY::Global.modGlobalConstants.Statics.clsSecurityClass.Get_OpID() != "SuperUser")
            {
                TWGNENTY::TWGNENTY.modSecurity.CheckPasswords(authenticationService);
                if (!TWGNENTY::TWGNENTY.modGNBas.Statics.boolPasswordGood) return;
            }
            clstempdc.OpenRecordset("select * from permissionstable", "systemsettings");
            var securityForm = new frmSecurity(authenticationService, userDataAccess);
            securityForm.Show(FormShowEnum.Modal);
        }

        private void helpContextMenu_ItemClick(object sender, System.EventArgs e)
        {
            App.MainForm.ApplicationIcon = "trio-logo-dark";
            helpContextMenuMain.Close();
            helpContextMenu.Close();
            string item = sender.ToString();
            switch (item)
            {
                case "Start WebEx Support Session":
                    Application.Navigate("http://harriscomputer.webex.com", "_blank");
                    break;
                case "Check Database Structure":
                    if (TWGNENTY.modTrioStart.CheckVersion())
                    {
                        MessageBox.Show("Database Structure Check successful", "Check Database Structure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Database Structure Check/Update failed.", "Check Database Structure", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    break;
                case "Program Information":
                    TWGNENTY.frmProgramDates.InstancePtr.Show(FormShowEnum.Modal);
                    break;
                case "About":
                    TWGNENTY::Global.frmAbout.InstancePtr.Show(FormShowEnum.Modal);
                    break;
            }
        }

        //FC:FINAL:CHN - issue #1516: Add showing archive image after running archive.
        private void HomePagePanel_VisibleChanged(object sender, System.EventArgs e)
        {
            if (HomePageVisible)
            {
                PicArchive.Visible = false;
            }
        }

        //FC:FINAL:AM:#1876 - add expiration extension functionality
        private void ExpirationButton_Click(object sender, System.EventArgs e)
        {
            if (HomePageVisible)
            {
                TWGNENTY.MDIParent.InstancePtr.UpdateDatesOneMonth();
            }
        }

        private void Logout()
        {
            //FC:FINAL:AM:#3481 - set the exiting flag for RE
            TWRE0000.modGNBas.Statics.gboolExiting = true;
            this.ExitingApplication = true;
            FCUtils.CloseChildForms(formName: "");

            StaticSettings.CurrentUserPermissionSet = new UserPermissionSet(0, new List<PermissionItem>());
            Application.Exit();
        }

        public override int OpenModule(string module, bool addExitMenu = true, bool checkIfModuleIsAlreadyOpened = false)
        {
            base.OpenModule(module, addExitMenu, checkIfModuleIsAlreadyOpened);
			System.Diagnostics.Debug.Print("OpenModule(module='" + module + "', addExitMenu='" + addExitMenu.ToString() + "', checkIfModuleIsAlreadyOpened='" + checkIfModuleIsAlreadyOpened.ToString() + "')");
            module = module.ToUpper();
            currentModule = module;
            currentModuleHasExit = addExitMenu;
            if (checkIfModuleIsAlreadyOpened)
            {
                var openForms = string.Empty;
                openForms = GetAllOpenForms(module, openForms);
                if (!string.IsNullOrEmpty(openForms))
                {
                    MessageBox.Show(moduleKeyToModuleName[module] + " is already started" + Environment.NewLine + "Please close the following forms:" + Environment.NewLine + openForms);
                    return 0;
                }
                if (module == "TWMV0000")
                {
                    TWMV0000.MotorVehicle.Statics.moduleInitialized = false;
                    TWMV0000.MotorVehicle.Statics.OpID = string.Empty;
                }
            }

            try
            {
                ResetNavigationAndStatusBars();

                HomePageVisible = false;

                switch (module)
                {
                    case "TWCL0001":
                        TWCL0001::Global.modStatusPayments.Statics.boolRE = false;
                        if (TWCL0001::TWCL0000.modGlobal.Statics.moduleInitialized)
                        {
                            TWCL0001::TWCL0000.MDIParent.InstancePtr.Init(diContainer);
                        }
                        else
                        {
                            TWCL0001::TWCL0000.modGlobal.Main(diContainer);
                            TWCL0001::TWCL0000.modGlobal.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWCL0000":
                        TWCL0000::Global.modStatusPayments.Statics.boolRE = true;
                        if (TWCL0000::TWCL0000.modGlobal.Statics.moduleInitialized)
                        {
                            TWCL0000::TWCL0000.MDIParent.InstancePtr.Init(diContainer);
                        }
                        else
                        {
                            TWCL0000::TWCL0000.modGlobal.Main(diContainer);
                            TWCL0000::TWCL0000.modGlobal.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWFA0000":
                        if (TWFA0000.modStartup.Statics.moduleInitialized)
                        {
                            TWFA0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWFA0000.modStartup.Main();
                            TWFA0000.modStartup.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWBD0000":
                        if (TWBD0000.modBudgetaryMaster.Statics.moduleInitialized)
                        {
                            TWBD0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWBD0000.modBudgetaryMaster.Main();
                            TWBD0000.modBudgetaryMaster.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWCR0000":
                        if (TWCR0000.modGlobal.Statics.moduleInitialized)
                        {
                            TWCR0000.MDIParent.InstancePtr.Init(diContainer);
                        }
                        else
                        {
                            TWCR0000.modGlobal.Main(diContainer);
                            TWCR0000.modGlobal.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWAR0000":
                        if (TWAR0000.modGlobal.Statics.moduleInitialized)
                        {
                            TWAR0000.MDIParent.InstancePtr.Init(diContainer.Resolve<CommandDispatcher>());
                        }
                        else
                        {
                            TWAR0000.modGlobal.Main(diContainer.Resolve<CommandDispatcher>());
                            TWAR0000.modGlobal.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWBL0000":
                        if (TWBL0000.modGlobalVariables.Statics.moduleInitialized)
                        {
                            TWBL0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWBL0000.modMain.Main();
                            TWBL0000.modGlobalVariables.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWPP0000":
                        if (TWPP0000.modPPGN.Statics.moduleInitialized)
                        {
                            TWPP0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWPP0000.modPPGN.Main();
                            TWPP0000.modPPGN.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWUT0000":
                        if (TWUT0000.modMain.Statics.moduleInitialized)
                        {
                            TWUT0000.MDIParent.InstancePtr.Init(diContainer.Resolve<CommandDispatcher>());
                        }
                        else
                        {
                            TWUT0000.modMain.Main(diContainer.Resolve<CommandDispatcher>());
                            TWUT0000.modMain.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWRE0000":
                        if (TWRE0000.modREMain.Statics.moduleInitialized)
                        {
                            TWRE0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWRE0000.modREMain.Main();
                            TWRE0000.modREMain.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWCE0000":
                        if (TWCE0000.modGlobalVariables.Statics.moduleInitialized)
                        {
                            TWCE0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWCE0000.modMain.Main(diContainer.Resolve<CommandDispatcher>());
                            TWCE0000.modGlobalVariables.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWRB0000":
                        if (TWRB0000.modRedBook.Statics.moduleInitialized)
                        {
                            TWRB0000.MDIParent.InstancePtr.Init(addExitMenu);
                        }
                        else
                        {
                            TWRB0000.modRedBook.Main(addExitMenu, diContainer.Resolve<CommandDispatcher>());
                            TWRB0000.modRedBook.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWMV0000":
                        if (TWMV0000.MotorVehicle.Statics.moduleInitialized)
                        {

                            addExitMenu = TWMV0000.MotorVehicle.Statics.bolFromWindowsCR;

                            TWMV0000.MDIParent.InstancePtr.Init(addExitMenu);
                        }
                        else
                        {
                            var loaded = TWMV0000.MotorVehicle.Main(addExitMenu);
                            switch (loaded)
                            {
                                case MotorVehicleReturnValue.Success:
                                    TWMV0000.MotorVehicle.Statics.moduleInitialized = true;
                                    break;
                                case MotorVehicleReturnValue.Failure:
                                    TWMV0000.MotorVehicle.Statics.startingMotorVehicle = false;
                                    TWMV0000.MotorVehicle.Statics.moduleInitialized = false;
                                    return 0;
                            }
                        }
                        break;

                    case "TWCK0000":
                        if (TWCK0000.modStartup.Statics.moduleInitialized)
                        {
                            TWCK0000.MDIParent.InstancePtr.Init(addExitMenu, diContainer.Resolve<CommandDispatcher>());
                        }
                        else
                        {
                            TWCK0000.modStartup.Main(addExitMenu, diContainer.Resolve<CommandDispatcher>());
                            TWCK0000.modStartup.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWPY0000":
                        if (TWPY0000.modSubMain.Statics.moduleInitialized)
                        {
                            TWPY0000.MDIParent.InstancePtr.Init();
                        }
                        else
                        {
                            TWPY0000.modSubMain.Main();
                            TWPY0000.modSubMain.Statics.moduleInitialized = true;
                        }
                        break;

                    case "TWGNENTY":
                        HomePageVisible = true;
                        break;
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public override void ReloadNavigationMenu()
        {
            base.ReloadNavigationMenu();
            OpenModule(currentModule, currentModuleHasExit);
        }

        private void ResetNavigationAndStatusBars()
        {
            NavigationMenu.Clear();

            //reset status bar text
            App.MainForm.StatusBarText1 = "";
            App.MainForm.StatusBarText2 = "";
            App.MainForm.StatusBarText3 = "";
        }

        private static string GetAllOpenForms(string module, string openForms)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType()
                        .Assembly.FullName.StartsWith(module))
                {
                    openForms += string.IsNullOrEmpty(form.Text) ? form.Name : form.Text + Environment.NewLine;
                }
            }

            return openForms;
        }

        private string ModuleCodeToModuleName(string moduleCode)
        {
            return "";
        }

        public override void WaitForMVModule()
        {
            FCUtils.ApplicationUpdate(this);
            base.WaitForMVModule();
            TWSharedLibrary.Variables.Statics.MVModuleWait = true;
            lock (TWSharedLibrary.Variables.Statics.MVModuleLocker)
            {
                while (TWSharedLibrary.Variables.Statics.MVModuleWait)
                {
                    System.Threading.Monitor.Wait(TWSharedLibrary.Variables.Statics.MVModuleLocker, 10000);
                }
            }
        }


        public override void EndWaitMVModule()
        {
            base.EndWaitMVModule();
            if (TWSharedLibrary.Variables.Statics.MVModuleWait)
            {
                lock (TWSharedLibrary.Variables.Statics.MVModuleLocker)
                {
                    TWSharedLibrary.Variables.Statics.MVModuleWait = false;
                    System.Threading.Monitor.Pulse(TWSharedLibrary.Variables.Statics.MVModuleLocker);
                }
            }
            //force reinitialize of the module
            TWMV0000.MotorVehicle.Statics.moduleInitialized = false;
            TWMV0000.MotorVehicle.Statics.startingMotorVehicle = false;
        }

		public override void WaitForRBModule()
        {
            base.WaitForRBModule();
            TWSharedLibrary.Variables.Statics.RBModuleWait = true;
            lock (TWSharedLibrary.Variables.Statics.RBModuleLocker)
            {
                while (TWSharedLibrary.Variables.Statics.RBModuleWait)
                {
                    System.Threading.Monitor.Wait(TWSharedLibrary.Variables.Statics.RBModuleLocker, 10000);
                }
            }
        }

        public override void EndWaitRBModule()
        {
            base.EndWaitRBModule();
            if (TWSharedLibrary.Variables.Statics.RBModuleWait)
            {
                lock (TWSharedLibrary.Variables.Statics.RBModuleLocker)
                {
                    TWSharedLibrary.Variables.Statics.RBModuleWait = false;
                    System.Threading.Monitor.Pulse(TWSharedLibrary.Variables.Statics.RBModuleLocker);
                }
            }
        }

        public override void WaitForCKModule()
        {
            base.WaitForCKModule();
            FCUtils.ApplicationUpdate(this);
            TWSharedLibrary.Variables.Statics.CKModuleWait = true;
            lock (TWSharedLibrary.Variables.Statics.CKModuleLocker)
            {
                while (TWSharedLibrary.Variables.Statics.CKModuleWait)
                {
                    System.Threading.Monitor.Wait(TWSharedLibrary.Variables.Statics.CKModuleLocker, 10000);
                }
            }
        }

        public override void EndWaitCKModule()
        {
            base.EndWaitCKModule();
            if (TWSharedLibrary.Variables.Statics.CKModuleWait)
            {
                lock (TWSharedLibrary.Variables.Statics.CKModuleLocker)
                {
                    TWSharedLibrary.Variables.Statics.CKModuleWait = false;
                    System.Threading.Monitor.Pulse(TWSharedLibrary.Variables.Statics.CKModuleLocker);
                }
            }
        }

        private void WireUpDependencies()
        {
            var builder = new ContainerBuilder();

            builder.Register(f => StaticSettings.gGlobalActiveModuleSettings).As<IGlobalActiveModuleSettings>();
            builder.Register(f => StaticSettings.gGlobalCashReceiptSettings).As<GlobalCashReceiptSettings>();
            builder.Register(f => StaticSettings.gGlobalBudgetaryAccountSettings).As<IGlobalBudgetaryAccountSettings>();
            builder.Register(f => StaticSettings.gGlobalAccountsReceivableSettings).As<GlobalAccountsReceivableSettings>();
            builder.Register(f => StaticSettings.RealEstateSettings).As<GlobalRealEstateSettings>();
            builder.Register(f => StaticSettings.TrioColorSettings).As<GlobalColorSettings>();
            builder.RegisterInstance(StaticSettings.gGlobalSettings).As<cGlobalSettings>();

            builder.Register(f => StaticSettings.gGlobalBudgetaryAccountSettings)
                .As<IGlobalBudgetaryAccountSettings>();
            builder.Register(f => StaticSettings.TrioColorSettings)
                .As<GlobalColorSettings>();
            builder.Register(f => StaticSettings.UserInformation)
                .As<UserInformation>();
            builder.RegisterInstance(StaticSettings.EnvironmentSettings).As<DataEnvironmentSettings>();
            builder.RegisterInstance(StaticSettings.gGlobalSettings.ToDataContextDetails()).As<DataContextDetails>();
            builder.Register(f => StaticSettings.CurrentUserPermissionSet).As<IUserPermissionSet>();
            builder.Register(f => StaticSettings.UtilityBillingSettings).As<GlobalUtilityBillingSettings>();
            builder.Register(f =>
            {
                var usr = TWSharedLibrary.Variables.Statics;
                //FC:FINAL:SBE - HttpContext.Current is null when WebSocket protocol is enabled on web server
                //var req = HttpContext.Current.Request;

                var telemInit = new TelemetryInitialization
                {
                    DeviceType = Application.Browser.Device,
                    //FC:FINAL:SBE - HttpContext.Current is null when WebSocket protocol is enabled on web server
                    //DeviceId = HttpContext.Current.Server.MachineName,
                    DeviceId = Application.ServerName,
                    WiseJSessionId = Application.SessionId.ToUpper(),
                    Environment = usr.DataGroup,
                    Username = usr.UserID,
                    //FC:FINAL:SBE - HttpContext.Current is null when WebSocket protocol is enabled on web server
                    //UserAgent = req.UserAgent,
                    UserAgent = Application.UserAgent,
                    AppVersion = $"{App.Major}.{App.Minor}.{App.Revision}",
                    Browser = $"{Application.Browser.Type} {Application.Browser.Version}",
                    UserHostAddress = Application.UserHostAddress,
                    UserHostName = Application.UserHostName,
                    IsHarrisStaffEnvironment = StaticSettings.gGlobalSettings.IsHarrisStaffComputer ? "True":"False",
                    IsHosted = StaticSettings.gGlobalSettings.IsHostedSite ? "True":"False"
                };
                return new TelemetryService(telemInit);
            }).As<ITelemetryService>().SingleInstance();
            builder.Register(f => StaticSettings.TaxCollectionValues).As<GlobalTaxCollectionValues>();
            builder.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            builder.RegisterAssemblyModules(AppDomain.CurrentDomain.GetAssemblies());
            //builder.Register(f =>
            //{
            //    var gs = f.Resolve<cGlobalSettings>();
            //    return (IBlueBookRepository)f.Resolve<TrioPriceDigestsAPIBlueBookRepository>();
            //}
            //    ).As<IBlueBookRepository>();
            diContainer = builder.Build();
        }

        private void WireUpGlobalHacks()
        {
            StaticSettings.GlobalCommandDispatcher = diContainer.Resolve<CommandDispatcher>();
            StaticSettings.GlobalEventPublisher = diContainer.Resolve<EventPublisher>();
            StaticSettings.GlobalTelemetryService = diContainer.Resolve<ITelemetryService>();
            StaticSettings.GlobalSettingService = diContainer.Resolve<ISettingService>();
        }


        private void LoadEnvironmentStatics()
        {
            var queryHandler = diContainer.Resolve<IQueryHandler<GlobalVariableQuery, GlobalVariable>>();
            var globalVariable = queryHandler.ExecuteQuery(new GlobalVariableQuery());
            StaticSettings.EnvironmentSettings.IsMultiTown = globalVariable.UseMultipleTown.GetValueOrDefault();
            StaticSettings.EnvironmentSettings.ClientName = globalVariable.MuniName;
            StaticSettings.EnvironmentSettings.CityTown = globalVariable.CityTown;

            string url = Application.Url;

            if (url.ToUpper().Contains(".TRIO-WEB.COM"))
            {
                StaticSettings.gGlobalSettings.IsHostedSite = true;
            }
        }

        private void LoadUserStatics()
        {
            var queryHandler = diContainer.Resolve<IQueryHandler<GetUserPermissions, IEnumerable<PermissionItem>>>();
            var userQueryHandler = diContainer.Resolve<IQueryHandler<UserSearchCriteria, IEnumerable<User>>>();
            if (TWSharedLibrary.Variables.Statics.IntUserID == -1)
            {
                StaticSettings.CurrentUserPermissionSet = new SuperUserPermissionSet();
                StaticSettings.UserInformation = new UserInformation
                {
                    UserId = "SuperUser",
                    Id = -1,
                    AllowSiteUpdate = true
                };
            }
            else
            {
                StaticSettings.CurrentUserPermissionSet = new UserPermissionSet(
                    TWSharedLibrary.Variables.Statics.IntUserID,
                    queryHandler.ExecuteQuery(new GetUserPermissions(TWSharedLibrary.Variables.Statics.IntUserID)));
                
                var user = userQueryHandler.ExecuteQuery(new UserSearchCriteria
                {
	                UserId = TWSharedLibrary.Variables.Statics.IntUserID
                }).FirstOrDefault();

                StaticSettings.UserInformation = new UserInformation
                {
                    UserId = TWSharedLibrary.Variables.Statics.UserID,
                    Id = TWSharedLibrary.Variables.Statics.IntUserID,
                    AllowSiteUpdate = user?.CanUpdateSite ?? false
                };
            }
            StaticSettings.gGlobalSettings.MachineOwnerID = Application.ClientFingerprint.ToString();
            this.javaScript1.Eval("GetLocalMachineId('44850')"); // .Call("GetLocalMachineId('44850')");
            //this.javaScript1.Call("GetLocalMachineId", new Action<string>((s) =>
            //{
            //    SetTheMachineId(s);
            //}), "44850");
           
            // try to get the machine id by talking to the trioassistant miniserver
            //if it isn't running then just use the client fingerprint
            //var downloadedId = StaticSettings.GlobalCommandDispatcher.Send(new GetMachineId()).Result.Trim();
            //if (!downloadedId.IsNullOrWhiteSpace())
            //{
            //    StaticSettings.gGlobalSettings.MachineOwnerID = downloadedId;
            //}
            Console.WriteLine($"application.userhostaddress: {Application.UserHostAddress}");
            Console.WriteLine($"application.userhostname: {Application.UserHostName}");
            Console.WriteLine($"application.clientfingerprint: {Application.ClientFingerprint.ToString()}");
        }

        private void LoadModuleSettings()
        {
            var activeModuleSettings = diContainer.Resolve<IGlobalActiveModuleSettings>();
            var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
            LoadUtilityBillingSettings(activeModuleSettings, commandDispatcher);
            LoadTrioDataAPIUri(commandDispatcher);
        }

        private void LoadUtilityBillingSettings(IGlobalActiveModuleSettings activeModules, CommandDispatcher commandDispatcher)
        {
            if (!activeModules.UtilityBillingIsActive) return;
            var utSetting = commandDispatcher.Send(new GetUtilityBillingSettings()).Result;
            if (utSetting != null)
            {
                StaticSettings.UtilityBillingSettings = utSetting.ToGlobalUtilityBillingSettings();
            }
        }

        [Wisej.Core.WebMethod]
        public static void SetTheMachineId(string machineId)
        {
           // MessageBox.Show("machine id is " + machineId, "MachineId", MessageBoxButtons.OK, MessageBoxIcon.None);
            if (!machineId.IsNullOrWhiteSpace())
            {
                StaticSettings.gGlobalSettings.MachineOwnerID = machineId;
                PrintingViaWebSocketsVariables.Statics.MachineName = machineId;
                LoadPrinters();
            }
        }

        private static void LoadPrinters()
        {
            //string printingKey = TWGNENTY::Global.clsPrinterFunctions.GetApplicationKey() ?? "";
            //if (!string.IsNullOrEmpty(printingKey))
            //{
            //    PrintingViaWebSocketsVariables.Statics.MachineName = printingKey;
            //}

            TWGNENTY::Global.clsPrinterFunctions.LoadMachinePrinters();
        }

        private void LoadTrioDataAPIUri(CommandDispatcher commandDispatcher)
        {
            var apiUri = commandDispatcher
                .Send(new GetSystemSettingValue("TrioDataApiUri", "System", "", SettingOwnerType.Global)).Result;
            if (apiUri.HasText())
            {
                StaticSettings.gGlobalSettings.TrioDataAPIUri = apiUri;
            }
            else
            {
                commandDispatcher.Send(new SaveSystemSetting("TrioDataApiUri", "System", "",SettingOwnerType.Global,
                    StaticSettings.gGlobalSettings.TrioDataAPIUri));
            }
        }

        private void SendStartupAnalytics()
        {
            try
            {
                var service = diContainer.Resolve<ITelemetryService>();
                if (service != null)
                {
                    var info = RuntimeInformation.FrameworkDescription;
                    service.TrackTrace("Running " + info);
                }
            }
            catch
            {

            }
        }
    }
}
