﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Controllers;
using SharedApplication.Authorization;
using Owin;
using SharedApplication;
using SharedApplication.AppConfiguration;
using SharedApplication.Extensions;
using SharedDataAccess;

namespace Main.Controllers
{
    public class TrioAuthorize : AuthorizeAttribute
    {
        private readonly string category;
        private readonly int permission;
        public TrioAuthorize(string category, int permission)
        {
            this.category = category;
            this.permission = permission;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var dataEnvironment = (actionContext.RequestContext.RouteData.Values["dataEnvironment"] ?? "None") as string;
            if (!string.IsNullOrWhiteSpace(dataEnvironment))
            {                
                //haven't figured out how to make a dynamic token path so can't log in to an environment yet
                var environmentRole = dataEnvironment + "_user";
                
                if (!actionContext.RequestContext.Principal.Identity.IsAuthenticated)
                {
                    base.OnAuthorization(actionContext);
                    return;
                }

                var clientName = "";
                var host = actionContext.Request.RequestUri.Host;
                if (host.ToUpper().Contains(".TRIO-WEB.COM"))
                {
                    clientName = host.Left(host.ToUpper().IndexOf(".TRIO-WEB.COM"));
                    clientName = clientName.ToUpper().Replace("HTTPS://", "");
                }
                else
                {
                    clientName = "Default";
                }
                
                TrioWebConfiguration config;               
                if (HttpRuntime.Cache.Get("ClientSettings") == null)
                {
                    config = LoadClientConfiguration();
                    if (config != null)
                    {
                        HttpRuntime.Cache.Add("ClientSettings", config, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 30, 0), CacheItemPriority.Normal, null);
                    }
                }
                else
                {
                    config = (TrioWebConfiguration)HttpRuntime.Cache.Get("ClientSettings");
                }

                var trioContextFactory = GetTrioContextFactory(clientName, dataEnvironment, config);
                var systemSettingsContext = trioContextFactory.GetSystemSettingsContext();
                var userName = actionContext.RequestContext.Principal.Identity.Name;
                var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;
                var idClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                var id = idClaim.Value.ToIntegerValue();
                var categoryToUse = TranslateCategoryForPermissionsTable(category);
                var permissionRecord = systemSettingsContext.UserPermissions.FirstOrDefault(p => p.UserID == id && p.ModuleName == categoryToUse && p.FunctionID == permission);
                if (permissionRecord != null)
                {
                    if (permissionRecord.Permission == "F")
                    {
                        actionContext.Request.CreateResponse(HttpStatusCode.Accepted);
                        return;
                    }
                }
                actionContext.Response =
                    actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");

            }
            else
            {
                actionContext.Response =
                    actionContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Path not found");
            }

            base.OnAuthorization(actionContext);
        }

        private TrioWebConfiguration LoadClientConfiguration()
        {
            var configLoader = new TrioConfigLoader();
            var config = configLoader.LoadConfiguration(System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath,
                "TRIOClientConfig.xml"));
            return config;
        }

        private ITrioContextFactory GetTrioContextFactory(string clientName,string dataEnvironment,TrioWebConfiguration webConfiguration)
        {
            var trioClient = webConfiguration.Clients.FirstOrDefault(c => c.Name == clientName ||(c.Name == "" && clientName == "Default"));
            return new TrioContextFactory(new DataContextDetails()
            {
                DataEnvironment = dataEnvironment,
                DataSource =  trioClient.DataSource,
                EnvironmentGroup = clientName,
                Password = trioClient.Password,
                UserID = trioClient.User
            }, new cGlobalSettings()
            {
                ClientEnvironment = clientName,
                EnvironmentGroup = "Live",
                DataEnvironment = dataEnvironment,
                Password = trioClient.Password,
                UserName = trioClient.User
                
            });

        }

        private string TranslateCategoryForPermissionsTable(string originalCategory)
        {
            switch (originalCategory.ToLower())
            {
                case "realestate":
                    return "RE";
                case "personalproperty":
                    return "PP";
                case "budgetary":
                    return "BD";
                case "motorvehicle":
                    return "MV";
                case "clerk":
                    return "CK";
                case "collections":
                    return "CL";
                case "taxcollections":
                    return "CL";
                case "utilitybilling":
                    return "UT";
                case "cashreceipting":
                    return "CR";
                case "codeenforcement":
                    return "CE";
                case "taxbilling":
                    return "BL";
                default:
                    return originalCategory;
            }
        }
    }

}