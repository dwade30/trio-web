﻿using System;
using System.Linq;
using SharedDataAccess;
using System.Web.Http;
using System.Web.Http.Controllers;
using SharedApplication;
using SharedApplication.AppConfiguration;
using SharedApplication.Extensions;
using System.Web;
using System.Web.Caching;

namespace Main.Controllers
{
    public abstract partial class BaseAuthenticatedApiController : ApiController
    {

        
        protected string GetClientName(HttpActionContext actionContext)
        {
            var clientName = "";
            var host = actionContext.Request.RequestUri.Host;
            if (host.ToUpper().Contains(".TRIO-WEB.COM"))
            {
                clientName = host.Left(host.ToUpper().IndexOf(".TRIO-WEB.COM"));
                clientName = clientName.ToUpper().Replace("HTTPS://", "");
            }
            else
            {
                clientName = "Default";
            }
            
            return clientName;
        }

        protected TrioWebConfiguration _trioConfiguration = null;
        protected TrioWebConfiguration TrioConfiguration {
            get
            {
                if (_trioConfiguration == null)
                {
                    if (HttpRuntime.Cache.Get("ClientSettings") == null)
                    {
                        _trioConfiguration = LoadClientConfiguration();
                        if (_trioConfiguration != null)
                        {
                            HttpRuntime.Cache.Add("ClientSettings", _trioConfiguration, null,
                                System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 30, 0),
                                CacheItemPriority.Normal, null);
                        }
                    }
                    else
                    {
                        _trioConfiguration = (TrioWebConfiguration) HttpRuntime.Cache.Get("ClientSettings");
                    }
                }

                return _trioConfiguration;
            }
        }
        

        protected TrioWebConfiguration LoadClientConfiguration()
        {
            var configLoader = new TrioConfigLoader();
            var config = configLoader.LoadConfiguration(System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath,
                "TRIOClientConfig.xml"));
            return config;
        }

        protected ITrioContextFactory GetTrioContextFactory(string clientName, string dataEnvironment)
        {
            var trioClient = TrioConfiguration.Clients.FirstOrDefault(c => c.Name == clientName || (c.Name == "" && clientName == "Default"));
            return new TrioContextFactory(new DataContextDetails()
            {
                DataEnvironment = dataEnvironment,
                DataSource = trioClient.DataSource,
                EnvironmentGroup = clientName,
                Password = trioClient.Password,
                UserID = trioClient.User
            }, new cGlobalSettings()
            {
                ClientEnvironment = clientName,
                EnvironmentGroup = "Live",
                DataEnvironment = dataEnvironment,
                Password = trioClient.Password,
                UserName = trioClient.User

            });

        }

    }
}