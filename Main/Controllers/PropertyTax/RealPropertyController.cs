﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication.ExtendedProtection;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using SharedAPI.PropertyTax.RealProperty;
using SharedApplication.RealEstate.Models;
using SharedApplication.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Main.Controllers.PropertyTax
{
    [TrioAuthorize("RealEstate", 15)]
    public class RealPropertyController : BaseAuthenticatedApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Post(string dataEnvironment,[FromBody] RealPropertyQueryItem queryItem)
        {
            if (queryItem != null)
            {
                var clientName = GetClientName(this.ActionContext);
                var trioContextFactory = GetTrioContextFactory(clientName, dataEnvironment);
                var reContext = trioContextFactory.GetRealEstateContext();
                IQueryable<REMaster> query;
                
                query = reContext.REMasters.Where(m => m.Rscard == 1);
                if (!queryItem.IncludedDeleted)
                {
                    query = query.Where(m => m.Rsdeleted != true);
                }
                
                switch(RealPropertyEnumUtility.ToRangeType(queryItem.RangeType))
                {
                    case RealPropertyRangeType.Individual:
                        switch(RealPropertyEnumUtility.ToRangeByType(queryItem.RangeBy))
                        {
                            case RealPropertyRangeByType.Maplot:
                                query = query.Where(m => m.RsmapLot == queryItem.RangeStart);
                                break;
                            case RealPropertyRangeByType.DeedName:
                                query = query.Where(m => m.DeedName1 == queryItem.RangeStart);
                                break;
                            default:
                                query = query.Where(m => m.Rsaccount == queryItem.RangeStart.ToIntegerValue());
                                break;
                        }
                        break;
                    case RealPropertyRangeType.Range:
                        switch (RealPropertyEnumUtility.ToRangeByType(queryItem.RangeBy))
                        {
                            case RealPropertyRangeByType.Maplot:
                                query = query.Where(m => String.Compare(m.RsmapLot, queryItem.RangeStart) >= 0 && String.Compare(m.RsmapLot, queryItem.RangeEnd + "ZZ") <= 0);
                                break;
                            case RealPropertyRangeByType.DeedName:
                                query = query.Where(m => String.Compare(m.DeedName1, queryItem.RangeStart) >= 0 && String.Compare(m.DeedName1, queryItem.RangeEnd + "zz") <= 0);
                                break;
                            default:
                                query = query.Where(m => m.Rsaccount >= queryItem.RangeStart.ToIntegerValue() && m.Rsaccount <= queryItem.RangeEnd.ToIntegerValue());
                                break;
                        }
                        break;
                }
                switch(RealPropertyEnumUtility.ToOrderByType(queryItem.OrderBy))
                {
                    case RealPropertyOrderByType.MapLot:
                        if (queryItem.OrderAscending)
                        {
                            query = query.OrderBy(o => o.RsmapLot);
                        }
                        else
                        {
                            query = query.OrderByDescending(o => o.RsmapLot);
                        }
                        break;
                    case RealPropertyOrderByType.DeedName:
                        if (queryItem.OrderAscending)
                        {
                            query = query.OrderBy(o => o.DeedName1);
                        }
                        else
                        {
                            query = query.OrderByDescending(o => o.DeedName1);
                        }
                        break;
                    default:
                        if (queryItem.OrderAscending)
                        {

                            query = query.OrderBy(o => o.Rsaccount);
                        }
                        else
                        {
                            query = query.OrderByDescending(o => o.Rsaccount);
                        }
                        break;
                }
                var reply = await query.Select(m => new RealProperty
                {
                    Account = m.Rsaccount.GetValueOrDefault(),
                    AccountIdentifier = m.AccountId.ToGuid(),
                    Name = m.DeedName1,
                    ParcelIdentifier = m.RsmapLot,
                    StreetName  = m.RslocStreet,
                    StreetNumber = m.RslocNumAlph.ToIntegerValue()
                }).ToListAsync();

                return  Ok(reply);
            }

            return BadRequest();
        }
    }
}