﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication.ExtendedProtection;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using SharedApplication.CentralDocuments;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using SharedDataAccess.CentralDocuments;
using SharedDataAccess.RealEstate;

namespace Main.Controllers //.PropertyTax
{
    // [Authorize(Roles = "REAddPictures")]
     
   //[Route("api/PropertyTax/AddImageFileToProperty")]
   [TrioAuthorize("RealEstate",34)]
    public class AddImageFileToPropertyController : BaseAuthenticatedApiController
    {

        // [Route("api/{dataEnvironment}/AddImageFileToProperty")]

        [HttpPost]
         public async Task<IHttpActionResult> Post(string dataEnvironment,[FromBody] RealEstateImageFileImport imageImport)
        {
            
            if (imageImport != null)
            {
                if (imageImport.Account > 0 || !imageImport.MapLot.IsNullOrWhiteSpace())
                {
                    var clientName = GetClientName(this.ActionContext);
                    var trioContextFactory = GetTrioContextFactory(clientName, dataEnvironment);
                    var reContext = trioContextFactory.GetRealEstateContext();
                    var reCard = imageImport.Card;
                    if (reCard == 0)
                    {
                        reCard = 1;
                    }

                    var account = imageImport.Account;
                    var cardIdentifier = "";
                    if (imageImport.Account > 0)
                    {
                        var propertyCards = reContext.REMasters.Where(m => m.Rsaccount == imageImport.Account).Select(m => new { m.Rsaccount, m.Rscard, m.CardId });
                        if (propertyCards == null || !propertyCards.Any())
                        {
                            return BadRequest("Account not found");
                        }
                        var propertyCard = propertyCards.Where(p => p.Rscard == reCard).FirstOrDefault();
                        if (propertyCard == null)
                        {
                            propertyCard = propertyCards.FirstOrDefault(p => p.Rscard == 1);
                            reCard = 1;
                        }
                        if (propertyCard == null)
                        {
                            return BadRequest("Account not found");
                        }
                        cardIdentifier = propertyCard?.CardId ?? "";
                    }
                    else
                    {
                        var propertyCards = reContext.REMasters.Where(m => m.RsmapLot == imageImport.MapLot && m.Rscard == reCard).Select(m => new { m.Rsaccount, m.Rscard,m.CardId});

                        if (propertyCards == null || !propertyCards.Any())
                        {
                            return BadRequest("Account not found");
                        }
                        var propertyCard = propertyCards.Where(p => p.Rscard == reCard).FirstOrDefault();
                        if (propertyCard == null)
                        {
                            propertyCard = propertyCards.FirstOrDefault(p => p.Rscard == 1);
                            reCard = 1;
                        }
                        if (propertyCard == null)
                        {
                            return BadRequest("Account not found");
                        }
                        account = propertyCard?.Rsaccount ?? 0;
                        cardIdentifier = propertyCard?.CardId ?? "";
                    }

                    var centralDocumentsContext = trioContextFactory.GetCentralDocumentsContext();

                    var sha256 = new SHA256Managed();
                    var hashedValue = sha256.ComputeHash(imageImport.ImageData);
                    
                    var matchedDocument = centralDocumentsContext.Documents.Any(d => 
                        d.AltReference == cardIdentifier && d.HashedValue == hashedValue);
                    if (!matchedDocument )
                    {
                        var sequenceNumber = reContext.PictureRecords
                            .Where(p => p.Account == account && p.Card == reCard)
                            .Max(p => p.PicNum).GetValueOrDefault() + 1;

                        var docUtility = new CentralDocumentUtility(centralDocumentsContext);
                        var documentIdentifier = Guid.NewGuid();
                        var createDocumentCommand = new CreateCentralDocument(imageImport.MediaType,
                            imageImport.Description, "PropertyPicture", account,
                            cardIdentifier, "RealEstate", documentIdentifier, imageImport.ImageData,
                            imageImport.Filename,
                            DateTime.Now);
                        var documentId = docUtility.CreateCentralDocument(createDocumentCommand);
                        if (documentId > 0)
                        {
                            var createPictureCommand = new CreatePictureRecord(account, reCard, sequenceNumber,
                                documentIdentifier, imageImport.Filename, imageImport.Description);
                            var pictureHandler = new CreatePictureRecordHandler(reContext);
                            var pictureId = pictureHandler.Handle(createPictureCommand, new CancellationToken()).Result;
                            if (pictureId == 0)
                            {
                                return InternalServerError();
                            }
                        }
                        else
                        {
                            return InternalServerError();
                        }
                    }
                    return Ok();
                }                
            }
           
            return BadRequest();
        }



    }
}