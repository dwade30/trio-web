﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Main.Controllers
{
    public class TestController : ApiController
    {
        [Authorize(Roles = "user")]
        public IEnumerable<string> Get()
        {
            var identity = (ClaimsIdentity) User.Identity;
            return new string[] { identity.Name, "hello" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}