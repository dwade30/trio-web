﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Main.Controllers
{
    public class TrioAuthenticationActionFilter : ActionFilterAttribute
    {
        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (actionContext.ActionArguments.ContainsKey("dataEnvironment"))
            {
                //HttpContext.Current.User.
            }
            else
            {
                actionContext.Response =
                    actionContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Path not found");
            }

            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}