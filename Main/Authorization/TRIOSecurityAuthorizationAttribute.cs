﻿using Microsoft.Owin.Security.Authorization.WebApi;

namespace Main.Authorization
{
    public class TRIOSecurityAuthorizeAttribute : ResourceAuthorizeAttribute
    {
        private const string POLICY_PREFIX = "TRIOSecurity";

        public TRIOSecurityAuthorizeAttribute(string permissionString)
        {

        }

        public string PermissionString
        {
            get
            {
                return Policy.Substring(POLICY_PREFIX.Length);
            }
            set
            {
                Policy = $"{POLICY_PREFIX}{value}";
            }
        }
    }
}