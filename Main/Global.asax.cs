﻿using System;
using System.Net.Http.Headers;
using Autofac;
using TWSharedLibrary;
using System.Web.Http;
using System.Web.Routing;
using GrapeCity.ActiveReports.PageReportModel;

namespace Main
{
    public class Global : System.Web.HttpApplication
    {
        // Provider that holds the application container.
        static IContainer _container;

        // Instance property that will be used by Autofac HttpModules
        // to resolve and inject dependencies.
        public IContainer DIContainer => _container;

        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();
                config.Routes.MapHttpRoute(name: "DefaultApi", routeTemplate: "api/{dataEnvironment}/{controller}/{id}",
                    defaults: new { id = System.Web.Http.RouteParameter.Optional });
                config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            });
            //RouteTable.Routes.MapHttpRoute(name: "DefaultApi", routeTemplate: "api/{category}/{controller}/{id}",
            //    defaults: new {id = System.Web.Http.RouteParameter.Optional});   
            //RouteTable.Routes.MapHttpRoute(name: "DefaultApi", routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = System.Web.Http.RouteParameter.Optional });
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception == null) return;

            var telem = StaticSettings.GlobalTelemetryService;
            telem.TrackException(exception);
            Server.ClearError();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
 

    }
}