﻿extern alias TWGNENTY;

using fecherFoundation;
using System;
using Wisej.Web;

namespace Main
{
    public partial class SelectTheme : Form
    {
        public SelectTheme()
        {
            InitializeComponent();
        }

        private void colorScheme_Click(object sender, EventArgs e)
        {
            PictureBox selectedThemeControl = sender as PictureBox;
            if (selectedThemeControl != null)
            {
                CheckSelectedTheme(selectedThemeControl);
                string selectedtheme = selectedThemeControl.Name;
                TWGNENTY::Global.modRegistry.SaveRegistryKey("UserColorScheme", selectedtheme);
                (App.MainForm as MainForm).ApplyColorScheme(selectedtheme);
            }
        }

        private void SelectTheme_Load(object sender, EventArgs e)
        {
            string colorScheme = TWGNENTY::Global.modRegistry.GetRegistryKey("UserColorScheme");
            if (string.IsNullOrEmpty(colorScheme))
            {
                colorScheme = MainForm.DEFAULT_COLOR_SCHEME;
            }
            PictureBox selectedThemeControl =  this.Controls[colorScheme] as PictureBox;
            CheckSelectedTheme(selectedThemeControl);
        }

        private void CheckSelectedTheme(PictureBox selectedThemeControl)
        {
            if (selectedThemeControl != null)
            {
                pictureBox1.Left = selectedThemeControl.Left + 110;
                pictureBox1.Top = selectedThemeControl.Top + 60;
            }
        }
}
}
