﻿using System.Collections.Generic;
using fecherFoundation;
using SharedApplication.Telemetry;

namespace Main
{
    public class TelemetryMenuLogger : IMenuLogger
    {
        private ITelemetryService telemetryService;
        public TelemetryMenuLogger(ITelemetryService telemetryService)
        {
            this.telemetryService = telemetryService;
        }
        public void LogMenuItemChoice(string itemName, string origin, string caption, string parentMenuName)
        {
            this.telemetryService.TrackEvent("Menu Item Chosen", new Dictionary<string, string>()
            {
                {"Menu Item Name",itemName},
                {"Menu Origin",origin},
                {"Menu Caption", caption},
                {"Parent Menu",parentMenuName}
            });
        }
    }
}