﻿using System.IO;
using fecherFoundation;
using SharedApplication;
using SharedApplication.Messaging;
using Wisej.Web;

namespace Main
{
    public class GetUploadFileHandler : CommandHandler<GetUploadFile,string>
    {
        private ITrioFileSystem fileSystem;
        public GetUploadFileHandler(ITrioFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }
        protected override string Handle(GetUploadFile command)
        {
            var ofd = new StreamOpenFileDialog();
            ofd.Text = command.Title;
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return "";
            }
            var files = ofd.UploadedFiles;
            var fileName = ofd.FileName;
            var fileStream = fileSystem.OpenFileStream(fileName, FileMode.Create, FileAccess.Write);
            files[0].InputStream.CopyTo(fileStream, 1024);
            fileStream.Flush();
            fileStream.Close();
            return fileName;
        }
    }
}