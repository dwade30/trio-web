﻿using System.Collections.ObjectModel;
using System.Web.Services;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using TWSharedLibrary.Data;

namespace Main
{
    /// <summary>
    /// Summary description for TRIOAPI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TRIOAPI : System.Web.Services.WebService
    {
        private readonly CommandDispatcher _commandDispatcher;

        public TRIOAPI()
        {
            DataConfigurator.LoadSQLConfig();
            //WireUpDependencies();
            //WireUpGlobalHacks(); 
        }
 

        [WebMethod]
        public CentralDocument GetCentralDocument(int documentId, string dataGroup)
        {
            var docService = new CentralDocumentService(_commandDispatcher);

            return docService.GetCentralDocument(new GetCommand() { Id = documentId });
        }

        [WebMethod]
        public CentralDocument GetCentralDocument(int id)
        {
            var docService = new CentralDocumentService(_commandDispatcher);

            return docService.GetCentralDocument(new GetCommand() { Id = id});
        }


        [WebMethod]
        public Collection<CentralDocumentHeader> GetCentralDocuments(int propertyId)
        {
            var docService = new CentralDocumentService(_commandDispatcher);

            return docService.GetHeadersByReference(new GetHeadersByReferenceCommand() { DataGroup = "RealEstate", ReferenceId = propertyId, ReferenceType = "0" });
        }
    }
}
