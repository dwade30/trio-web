﻿var webbrowserid = "";
var browserParentId = "";
window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
    webbrowserid = event.data.browserId;
    browserParentId = event.data.parentPageId;  
    var urlParams = new URLSearchParams(window.location.search);
    var tran = urlParams.get('tranresult');
    sendData({ tranResult: tran });
}

function sendData(payload) {
    var data = { wisejID: webbrowserid, parentPageId: browserParentId, value: payload };
    window.parent.parent.postMessage(data, "*");
}