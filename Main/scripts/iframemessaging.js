﻿function AttachToIframe(handle) {
    var webbrowser = Wisej.Core.getComponent("id_" + handle);
    if (webbrowser) {
        var iframe = webbrowser.getChildControl("iframe");
        if (iframe) {
            var domElement = iframe.getContentElement().getDomElement();
            if (domElement) {
                domElement.contentWindow.postMessage(handle, "*");
            }
        }
    }
}

window.addEventListener("message", receiveMessageFCWebBrowser, false);
function receiveMessageFCWebBrowser(event) {
    try {
        Wisej.Core.getComponent("id_" + event.data.wisejID).fireDataEvent('SendMessage', event.data.value);
    } catch (e) {
        console.log(e);
    }

}