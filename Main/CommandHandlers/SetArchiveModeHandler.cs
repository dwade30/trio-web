﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SharedApplication;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Commands;
using SharedApplication.Messaging;
using SharedDataAccess;
using SharedDataAccess.CashReceipts;
using TWSharedLibrary;

namespace Main.CommandHandlers
{
	public class SetArchiveModeHandler : CommandHandler<SetArchiveMode>
	{
		private cGlobalSettings globalSettings;
		public SetArchiveModeHandler(cGlobalSettings globalSettings)
		{
			this.globalSettings = globalSettings;
		}

		protected override void Handle(SetArchiveMode command)
		{
			MainForm.InstancePtr.favouritesPanel.Enabled = !command.ArchiveMode;
			MainForm.InstancePtr.LoadColorScheme(command.ArchiveMode);
			StaticSettings.gGlobalSettings.EnvironmentGroup = command.ArchiveTag;
		}
	}
}