﻿using System;
using System.Collections.Generic;
using SharedApplication.Commands;
using SharedApplication.Messaging;
using Wisej.Web;

namespace Main.CommandHandlers
{
    public class GetOpenFormsForModuleHandler : CommandHandler<GetOpenFormsForModule,IEnumerable<string>>
    {
        protected override IEnumerable<string> Handle(GetOpenFormsForModule command)
        {
            var openForms = new List<string>();
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType()
                    .Assembly.FullName.StartsWith(command.Module))
                {
                    openForms.Add(string.IsNullOrEmpty(form.Text) ? form.Name : form.Text);
                }
            }
            return openForms;
        }
    }
}