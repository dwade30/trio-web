﻿namespace Main
{
    partial class MainFormTopPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox2 = new Wisej.Web.PictureBox();
            this.label1 = new Wisej.Web.Label();
            this.UserUserName = new Wisej.Web.Label();
            this.userMenuOpen = new Wisej.Web.PictureBox();
            this.userMenuClose = new Wisej.Web.PictureBox();
            this.settingsButtonMain = new Wisej.Web.PictureBox();
            this.newsButtonMain = new Wisej.Web.PictureBox();
            this.userButtonMain = new Wisej.Web.PictureBox();
            this.themeButtonMain = new Wisej.Web.PictureBox();
            this.helpButtonMain = new Wisej.Web.PictureBox();
            this.Userlabel3 = new Wisej.Web.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userMenuOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userMenuClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButtonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newsButtonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userButtonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themeButtonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButtonMain)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = Wisej.Web.ImageLayout.BestFit;
            this.pictureBox2.ImageSource = "trio-logo-dark";
            this.pictureBox2.Location = new System.Drawing.Point(46, 40);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(36, 36);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("semibold", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(40, 40, 40);
            this.label1.Location = new System.Drawing.Point(94, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "TRIO";
            // 
            // UserUserName
            // 
            this.UserUserName.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.UserUserName.Font = new System.Drawing.Font("semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.UserUserName.Location = new System.Drawing.Point(510, 55);
            this.UserUserName.Name = "UserUserName";
            this.UserUserName.Size = new System.Drawing.Size(100, 38);
            this.UserUserName.TabIndex = 5;
            this.UserUserName.Text = "User";
            this.UserUserName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // userMenuOpen
            // 
            this.userMenuOpen.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.userMenuOpen.ImageSource = "icon-caret-down";
            this.userMenuOpen.Location = new System.Drawing.Point(526, 53);
            this.userMenuOpen.Name = "userMenuOpen";
            this.userMenuOpen.Size = new System.Drawing.Size(10, 6);
            this.userMenuOpen.Visible = false;
            // 
            // userMenuClose
            // 
            this.userMenuClose.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.userMenuClose.ForeColor = System.Drawing.Color.FromArgb(197, 197, 197);
            this.userMenuClose.ImageSource = "icon-close-menu";
            this.userMenuClose.Location = new System.Drawing.Point(526, 53);
            this.userMenuClose.Name = "userMenuClose";
            this.userMenuClose.Size = new System.Drawing.Size(10, 10);
            this.userMenuClose.Visible = false;
            // 
            // settingsButtonMain
            // 
            this.settingsButtonMain.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.settingsButtonMain.ForeColor = System.Drawing.Color.FromArgb(112, 122, 138);
            this.settingsButtonMain.ImageSource = "icon-settings-btn?color=app-mainform-top-icon";
            this.settingsButtonMain.Location = new System.Drawing.Point(330, 38);
            this.settingsButtonMain.Name = "settingsButtonMain";
            this.settingsButtonMain.Size = new System.Drawing.Size(37, 37);
            this.settingsButtonMain.Click += new System.EventHandler(this.settingsButtonMain_Click);
            // 
            // newsButtonMain
            // 
            this.newsButtonMain.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.newsButtonMain.ImageSource = "icon-news?color=app-mainform-top-icon";
            this.newsButtonMain.Location = new System.Drawing.Point(265, 38);
            this.newsButtonMain.Name = "newsButtonMain";
            this.newsButtonMain.Size = new System.Drawing.Size(37, 37);
            // 
            // userButtonMain
            // 
            this.userButtonMain.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.userButtonMain.AppearanceKey = "roundedPictureBox";
            this.userButtonMain.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.userButtonMain.ImageSource = "icon-user?color=app-mainform-top-icon";
            this.userButtonMain.Location = new System.Drawing.Point(460, 38);
            this.userButtonMain.Name = "userButtonMain";
            this.userButtonMain.Size = new System.Drawing.Size(37, 37);
            this.userButtonMain.Click += new System.EventHandler(this.userButtonMain_Click);
            // 
            // themeButtonMain
            // 
            this.themeButtonMain.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.themeButtonMain.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.themeButtonMain.ImageSource = "icon-themes?color=app-mainform-top-icon";
            this.themeButtonMain.Location = new System.Drawing.Point(200, 38);
            this.themeButtonMain.Name = "themeButtonMain";
            this.themeButtonMain.Size = new System.Drawing.Size(37, 37);
            this.themeButtonMain.Click += new System.EventHandler(this.themeButton_Click);
            // 
            // helpButtonMain
            // 
            this.helpButtonMain.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.helpButtonMain.AppearanceKey = "roundedPictureBox";
            this.helpButtonMain.ForeColor = System.Drawing.Color.FromArgb(180, 187, 198);
            this.helpButtonMain.ImageSource = "icon-help-btn?color=app-mainform-top-icon";
            this.helpButtonMain.Location = new System.Drawing.Point(395, 38);
            this.helpButtonMain.Name = "helpButtonMain";
            this.helpButtonMain.Size = new System.Drawing.Size(37, 37);
            this.helpButtonMain.Click += new System.EventHandler(this.helpButtonMain_Click);
            // 
            // Userlabel3
            // 
            this.Userlabel3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.Userlabel3.AutoSize = true;
            this.Userlabel3.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Userlabel3.Location = new System.Drawing.Point(530, 35);
            this.Userlabel3.Name = "Userlabel3";
            this.Userlabel3.Size = new System.Drawing.Size(90, 17);
            this.Userlabel3.TabIndex = 6;
            this.Userlabel3.Text = "Logged in as";
            // 
            // MainFormTopPanel
            // 
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userMenuOpen);
            this.Controls.Add(this.userMenuClose);
            this.Controls.Add(this.settingsButtonMain);
            this.Controls.Add(this.newsButtonMain);
            this.Controls.Add(this.userButtonMain);
            this.Controls.Add(this.helpButtonMain);
            this.Controls.Add(this.Userlabel3);
            this.Controls.Add(this.UserUserName);
            this.Controls.Add(this.themeButtonMain);
            this.Name = "MainFormTopPanel";
            this.Size = new System.Drawing.Size(630, 84);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userMenuOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userMenuClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsButtonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newsButtonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userButtonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themeButtonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.helpButtonMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Wisej.Web.PictureBox pictureBox2;
        public Wisej.Web.PictureBox userMenuOpen;
        public Wisej.Web.PictureBox userMenuClose;
        public Wisej.Web.PictureBox settingsButtonMain;
        public Wisej.Web.PictureBox newsButtonMain;
        public Wisej.Web.PictureBox userButtonMain;
        public Wisej.Web.PictureBox themeButtonMain;
        public Wisej.Web.PictureBox helpButtonMain;
        public Wisej.Web.Label label1;
        public Wisej.Web.Label UserUserName;
        public Wisej.Web.Label Userlabel3;
    }
}
