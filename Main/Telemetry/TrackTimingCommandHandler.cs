﻿using System;
using MediatR;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using System.Threading;
using System.Threading.Tasks;

namespace Main.Telemetry
{
    public class TrackTimingCommandHandler : ICommandHandler<TrackTimingCommand>
    {
        private readonly ITelemetryService _telemetryService;

        public TrackTimingCommandHandler(ITelemetryService telemetryService)
        {
            _telemetryService = telemetryService;
        }

        #region Implementation of IRequestHandler<in TrackTimingCommand,Unit>

        /// <inheritdoc />
        public Task<Unit> Handle(TrackTimingCommand request, CancellationToken cancellationToken)
        {
            if (request.Milliseconds == 0)
            {
                if (request.EndDateTime == DateTime.MinValue
                    || request.EndDateTime == DateTime.MaxValue
                    || request.StartDateTime == DateTime.MinValue
                    || request.StartDateTime == DateTime.MaxValue)
                    throw new ArgumentOutOfRangeException(nameof(request), "If you do not provide milliseconds, you must provide non-default start date and end date");

                request.Milliseconds = (request.EndDateTime - request.StartDateTime).Milliseconds;
            }
            
            _telemetryService.TrackTiming(request.Title, request.Milliseconds);

            return Task.FromResult(Unit.Value);
        }

        #endregion
    }
}