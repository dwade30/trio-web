﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MediatR;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;

namespace Main.Telemetry
{
    public class TrackEventCommandHandler : ICommandHandler<TrackEventCommand>
    {
        private readonly ITelemetryService _telemetryService;

        public TrackEventCommandHandler(ITelemetryService telemetryService)
        {
            _telemetryService = telemetryService;
        }

        #region Implementation of IRequestHandler<in TrackEventCommand,Unit>

        /// <inheritdoc />
        public Task<Unit> Handle(TrackEventCommand request, CancellationToken cancellationToken)
        {
            _telemetryService.TrackEvent(request.EventName);

            return Task.FromResult(Unit.Value);
        }

        #endregion
    }
}