﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using Microsoft.ApplicationInsights.Extensibility;
using Newtonsoft.Json;
using SharedApplication.Telemetry;
using UAParser;

namespace Main.Telemetry
{
    public class TelemetryService : ITelemetryService
    {
        private TelemetryClient _telemetry;

        //public TelemetryService()
        //{
        //    _telemetry = new TelemetryClient();
        //}

        public TelemetryService(TelemetryInitialization initialization)
        {
           _telemetry = new TelemetryClient();
            Initialize(initialization);
        }
        private void Initialize(TelemetryInitialization initialization)
        {
           _telemetry.Context.User.Id = initialization.Username;
           _telemetry.Context.Device.Id = initialization.DeviceId;

           var uaParser = Parser.GetDefault();
           ClientInfo ua = uaParser.Parse(initialization.UserAgent);

           _telemetry.Context.Device.OperatingSystem = $"{ua.OS}";
           _telemetry.Context.Device.Model = $"{ua.Device.Model}";
           _telemetry.Context.Device.Type = initialization.DeviceType;

           _telemetry.Context.Location.Ip = initialization.UserHostAddress;
           _telemetry.Context.User.AuthenticatedUserId = initialization.UserHostName;

           //_telemetry.Context.Device.Type = $"{ua.Device.Family}";
           _telemetry.Context.User.UserAgent = initialization.UserAgent;
           _telemetry.Context.Component.Version = initialization.AppVersion;
           _telemetry.Context.Session.Id = initialization.WiseJSessionId;
           _telemetry.Context.Device.OemName = $"{ua.UA.Family}";
           
           //
           

           Type type = typeof(TelemetryInitialization);
           PropertyInfo[] properties = type.GetProperties();
           foreach (PropertyInfo property in properties)
           {
               _telemetry.Context.GlobalProperties.Add(property.Name, property.GetValue(initialization, null)?.ToString());
           }
            
           var appStartedMsg = $"Application telemetry started.";
            _telemetry.TrackTrace(appStartedMsg, SeverityLevel.Information);
        }

        public void TrackHit(string pageName)
        {
            _telemetry.TrackPageView(pageName);
        }

        public  void TrackException(Exception exception)
        {
            try
            {
                _telemetry.TrackException(exception);
            }
            catch (Exception ex)
            {

            }
        }

        public void TrackException(Exception exception, IDictionary<string, string> extraProperties)
        {
            try
            {
                _telemetry.TrackException(exception, extraProperties);
            }
            catch (Exception ex)
            {

            }
        }

        public  void TrackTrace(string traceText)
        {
            _telemetry.TrackTrace(traceText);
        }

        public void TrackTrace(string traceText, IDictionary<string, string> extraProperties)
        {
            _telemetry.TrackTrace(traceText, extraProperties);
        }

        public  void TrackEvent(string eventName)
        {
            _telemetry.TrackEvent(eventName);
        }

        public void TrackEvent(string eventName, IDictionary<string, string> extraProperties)
        {
            _telemetry.TrackEvent(eventName, extraProperties);
        }

        public  void TrackTiming(string title, long milliseconds)
        {
            _telemetry.GetMetric(title).TrackValue(milliseconds);
        }

        public void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime)
        {
            _telemetry.GetMetric(title).TrackValue((endDateTime - startDateTime).Milliseconds);
        }
    }
}
