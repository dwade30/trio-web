﻿using System;
using System.Collections.Generic;
using SharedApplication.Telemetry;

namespace Main.Telemetry
{
    public class TelemetryServiceMock : ITelemetryService
    {
        
        public TelemetryServiceMock()
        {
           
        }
        public void Initialize(TelemetryInitialization initialization)
        {
            var appStartedMsg = $"Application started. Mocked session.";
            Console.WriteLine($"{appStartedMsg} -- {initialization.Username}, {initialization.WiseJSessionId},{initialization.DeviceId},{initialization.LocationIp},");
        }

        public  void TrackException(Exception exception)
        {
            Console.WriteLine($"*** Exception: {exception.GetBaseException().Message}");
        }

        public  void TrackTrace(string traceText)
        {
            Console.WriteLine($"*** Trace: {traceText}");

        }

        public  void TrackEvent(string eventName)
        {
            Console.WriteLine($"*** Event: {eventName}");

        }

        public  void TrackTiming(string title, long milliseconds)
        {
            Console.WriteLine($"*** Timing: {title} took {milliseconds} ms");

        }

        /// <inheritdoc />
        public void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public void TrackHit(string pageName)
        {
            throw new NotImplementedException();
        }

        public void TrackException(Exception exception, IDictionary<string, string> extraProperties)
        {
            Console.WriteLine($"*** Exception: {exception.GetBaseException().Message}");
        }

        public void TrackTrace(string traceText, IDictionary<string, string> extraProperties)
        {
            Console.WriteLine($"*** Trace: {traceText}");
        }

        public void TrackEvent(string eventName, IDictionary<string, string> extraProperties)
        {
            Console.WriteLine($"*** Event: {eventName}");
        }
    }
}
