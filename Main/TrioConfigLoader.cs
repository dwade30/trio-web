﻿using System;
using System.Xml.Linq;
using SharedApplication;
using SharedApplication.AppConfiguration;

namespace Main
{
    public class TrioConfigLoader
    {
        public string LastError { get; private set; } = "";
        private const string strToUse =  "B4A45E16-2828-4157-908E-F2266FA1810A";
        private const string sToUse = "O72SajdzgdE=";
        public TrioWebConfiguration LoadConfiguration(string fileName)
        {
            var trioConfig = new TrioWebConfiguration();
            LastError = "";
            XDocument SettingsDoc = null;
            if (!System.IO.File.Exists(fileName))
            {
                LastError = "File not found";
                return null;
            }

            try
            {
                SettingsDoc = XDocument.Load(fileName);
                var tRoot = SettingsDoc.Element("Setup");
                //tRoot = tRoot.Element("Clients")
                if (tRoot != null)
                {
                    foreach (XElement tClient in tRoot.Elements("Clients").Elements("Client"))
                    {
                        var client = new TrioClientSetup();
                        client.DataSource = tClient.Element("Server").Element("DataSource").Value;
                        client.User =
                            TWSharedLibrary.CryptoUtility.DecryptByPassword(
                                tClient.Element("Server").Element("UserName").Value, strToUse, sToUse);
                        client.Password =
                            TWSharedLibrary.CryptoUtility.DecryptByPassword(
                                tClient.Element("Server").Element("Password").Value, strToUse, sToUse);
                        client.IsHarrisStaff = tClient.Element("Global").Element("HarrisStaffUser")?.Value.ToLower().Trim() == "true";
                        

                        foreach (XElement tEnvironment in tClient.Elements("Envs").Elements("Env"))
                        {
                            var dataEnvironment = new TrioDataEnvironment();
                            dataEnvironment.Name = tEnvironment.Element("Name").Value;
                            dataEnvironment.DataDirectory = tEnvironment.Element("DataDirectory").Value;


                            client.AddDataEnvironment(dataEnvironment);
                        }
                        trioConfig.AddClient(client);
                    }

                    return trioConfig;
                }
                else
                {
                    LastError = "Invalid file format";
                }
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }

            return null;
        }
    }
}