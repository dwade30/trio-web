﻿//GRID7Light keydown
function AccountInput_KeyDown(accountInput, e)
{
    if (accountInput.customProperties.intAcctCol == -1 || accountInput.customProperties.intAcctCol == accountInput.customProperties.gRID7Light_Col) {
        if (e._identifier == 'F2' || e._identifier == 'Tab' || e._identifier == 'Shift') return;
        var input = accountInput.__contentElement.__element.children[0];
        var text = input.value;
        var selection = accountInput.getSelection();
        var allowKey = false;
        if (selection.start == 0) {

            if (e._identifier == 'Right') {
                selection.start = 2;
                selection.length = 1;
                accountInput.setSelection(selection);
                e.preventDefault();
                return;
            }
            else if (e._identifier == 'End') {
                selection.start = text.length;
                selection.length = 0;
                accountInput.setSelection(selection);
                e.preventDefault();
                return;
            }

            selection.start = 0;
            selection.length = 1;
            accountInput.setSelection(selection);

            var accountStart = e._identifier;

            if (
                (accountInput.customProperties.gboolTownAccounts && (accountStart == 'E' || accountStart == 'R' || accountStart == 'G')) ||
                (accountInput.customProperties.gboolSchoolAccounts && (accountStart == 'P' || accountStart == 'L' || accountStart == 'V')) ||
                (accountStart == 'M' || accountStart == 'A')
            )
                allowKey = true;
            if (!allowKey) { e.preventDefault(); return; }

            var segmentSize = '';
            if (accountStart == 'A') accountStart = 'M';
            if (accountStart == 'E') segmentSize = accountInput.customProperties.eSegmentSize;
            if (accountStart == 'R') segmentSize = accountInput.customProperties.rSegmentSize;
            if (accountStart == 'G') segmentSize = accountInput.customProperties.gSegmentSize;
            if (accountStart == 'P') segmentSize = accountInput.customProperties.pSegmentSize;
            if (accountStart == 'V') segmentSize = accountInput.customProperties.vSegmentSize;
            if (accountStart == 'L') segmentSize = accountInput.customProperties.lSegmentSize;
            if (accountStart == 'M') segmentSize = '230000';

            if (segmentSize != '') {
                var holder = '';
                for (counter = 2; counter <= segmentSize.length; counter += 2) {
                    var segmentLength = parseInt(segmentSize.substring(counter - 2, counter));
                    if (segmentLength > 0) {
                        holder += Array(segmentLength + 1).join('_') + '-';
                    }
                }
                if (holder.length > 0) {
                    holder = holder.substring(0, holder.length - 1);
                }
                input.value = accountStart + ' ' + holder;
                selection = accountInput.getSelection();
                selection.start = 2;
                selection.length = 1;
                accountInput.setSelection(selection);
                e.preventDefault();
            }
        }
        else if (selection.start == 1) {
            if (e._identifier == 'Left' || e._identifier == 'Backspace' || e._identifier == 'Home') {
                selection.start = 0;
                selection.length = 1;
            }
            else if (e._identifier == 'Right') {
                selection.start = 2;
                selection.length = 1;
            }
            accountInput.setSelection(selection);
            e.preventDefault();
        }
        else if (selection.start > 1) {
            selection = accountInput.getSelection();
            if (selection.start == text.length && e._identifier != 'Home' && e._identifier != 'Left' && e._identifier != 'Backspace') {
                selection.length = 0;
                accountInput.setSelection(selection);
                e.preventDefault();
                return;
            }
            var allowedKeysArray = accountInput.customProperties.allowedKeys.split(';');
            if ((text[0] != 'M' && allowedKeysArray.indexOf(e._identifier) >= 0) || (text[0] == 'M' && e._identifier.length == 1)) {
                input.value = text.substring(0, selection.start) + e._identifier + text.slice(selection.start + 1);
                selection.start = selection.start + 1;
                if (text[selection.start] == '-') {
                    selection.start = selection.start + 1;
                }
                if (selection.start == text.length) {
                    selection.length = 0;
                }
                else {
                    selection.length = 1;
                }
                accountInput.setSelection(selection);
                e.preventDefault();
            }
            else if (e._identifier == 'Home') {
                selection.start = 0;
                selection.length = 1;
                accountInput.setSelection(selection);
                e.preventDefault();
            }
            else if (e._identifier == 'Left') {
                selection.start = selection.start - 1;
                if (text[selection.start] == '-' || text[selection.start] == ' ') {
                    selection.start = selection.start - 1;
                }
                selection.length = 1;
                accountInput.setSelection(selection);
               // e.preventDefault(); // enabling this stops scrolling when the account is larger than the text area
            }
            else if (e._identifier == 'Right') {
                if (selection.start < text.length) {
                    selection.start = selection.start + 1;
                    selection.length = 0;
                }
                if (selection.start < text.length) {
                    if (text[selection.start] == '-' || text[selection.start] == ' ') {
                        selection.start = selection.start + 1;
                    }
                    selection.length = 1;
                }
                accountInput.setSelection(selection);
                //e.preventDefault(); // enabling this stops scrolling when the account is larger than the text area
            }
            else if (e._identifier == 'Backspace') {
                if (selection.start == 2) {
                    selection.start = 0;
                    selection.length = 1;
                    accountInput.setSelection(selection);
                    e.preventDefault();
                    return;
                }

                if (text[selection.start - 1] != '-') {
                    selection.start = selection.start - 1;
                }
                else if (text[selection.start - 1] == '-') {
                    selection.start = selection.start - 2;
                }
                input.value = text.substring(0, selection.start) + '_' + text.slice(selection.start + 1);
                selection.length = 1;
                accountInput.setSelection(selection);
                e.preventDefault();
            }
            else if (e._identifier == 'Delete') {
                input.value = text.substring(0, selection.start) + '_' + text.slice(selection.start + 1);
                selection.length = 1;
                accountInput.setSelection(selection);
                e.preventDefault();
            }
            else {
                e.preventDefault();
            }
        }
    }
}

//GRID7Light keyup
function AccountInput_KeyUp(accountInput, e)
{
    if (accountInput.customProperties.intAcctCol == -1 || accountInput.customProperties.intAcctCol == accountInput.customProperties.gRID7Light_Col) {
        if (e._identifier == 'F2' || e._identifier == 'Tab' || e._identifier == 'Shift') return;
        var input = accountInput.__contentElement.__element.children[0];
        var text = input.value;
        var selection = accountInput.getSelection();
        var allowKey = false;
        if (e._identifier == 'Backspace') {
            if (text.length > 0) {
                var accountStart = text[0];
                if (
                    (accountInput.customProperties.gboolTownAccounts && (accountStart == 'E' || accountStart == 'R' || accountStart == 'G')) ||
                    (accountInput.customProperties.gboolSchoolAccounts && (accountStart == 'P' || accountStart == 'L' || accountStart == 'V')) ||
                    (accountStart == 'M' || accountStart == 'A')
                ) {
                    var segmentSize = '';
                    if (accountStart == 'A') accountStart = 'M';
                    if (accountStart == 'E') segmentSize = accountInput.customProperties.eSegmentSize;
                    if (accountStart == 'R') segmentSize = accountInput.customProperties.rSegmentSize;
                    if (accountStart == 'G') segmentSize = accountInput.customProperties.gSegmentSize;
                    if (accountStart == 'P') segmentSize = accountInput.customProperties.pSegmentSize;
                    if (accountStart == 'V') segmentSize = accountInput.customProperties.vSegmentSize;
                    if (accountStart == 'L') segmentSize = accountInput.customProperties.lSegmentSize;
                    if (accountStart == 'M') segmentSize = '230000';

                    if (segmentSize != '') {
                        var holder = '';
                        for (counter = 2; counter <= segmentSize.length; counter += 2) {
                            var segmentLength = parseInt(segmentSize.substring(counter - 2, counter));
                            if (segmentLength > 0) {
                                holder += Array(segmentLength + 1).join('_') + '-';
                            }
                        }
                        if (holder.length > 0) {
                            holder = holder.substring(0, holder.length - 1);
                        }
                        var newText = accountStart + ' ' + holder;
                        if (newText.length > text.length) {
                            newText = text.substring(0, selection.start) + newText.substring(selection.start, newText.length - text.length + selection.start) + text.substring(selection.start, text.length);
                            input.value = newText;
                            if (input.value[selection.start] == '-') {
                                selection.start = selection.start - 1;
                            }
                            selection.length = 1;
                            accountInput.setSelection(selection);
                        }
                    }
                    else {
                        input.value = '';
                    }
                }
                else {
                    input.value = '';
                }
            }
        }
    }
}

function PrintGoogleChrome(exportUrl) {
    var printingFrameId = "gc-viewer-print-frame";
    var iframe = document.getElementById(printingFrameId);
    iframe || (iframe = document.createElement("iframe"), iframe.setAttribute("id", printingFrameId), iframe.setAttribute("name", "printFrame"));
    iframe.setAttribute("src", exportUrl);
    iframe.setAttribute("onload", "PrintFrame()");
    document.body.appendChild(iframe);
}

function PrintFrame() {
    setTimeout(this.printFrame.print(), 1000);
}