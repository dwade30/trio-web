﻿/// <reference path="../scripts/paymentportalresponse.js" />
function T2KBackFillDecimal_KeyDown(T2KBackFillDecimal, e) {
    if (e._keyCode === 8 || //BackSpace
        e._keyCode === 9 || //Tab
        e._keyCode === 13 || //Enter
        (e._keyCode >= 37 && e._keyCode <= 40 && e._identifier !== "'" && e._identifier !== "&" && e._identifier !== "(" && e._identifier !== "%") || //arrow keys
        e._identifier === 'Delete' ||
        e._identifier === 'Insert' ||
        e._identifier === 'Home' ||
        e._identifier === 'End' ||
        (e._keyCode >= 48 && e._keyCode <= 57)) { //0 -> 9
        //allowkey
    }
    else {
        //block non numeric input
        e._preventDefault = true;
    }
}

function T2KBackFillDecimal_Tap(T2KBackFillDecimal) {
    var selection = T2KBackFillDecimal.getSelection();
    var input = T2KBackFillDecimal.__contentElement.__element.children[0];
    selection.start = input.value.length;
    T2KBackFillDecimal.setSelection(selection);
}

function T2KBackFillDecimal_KeyUp(T2KBackFillDecimal, e) {
    var input = T2KBackFillDecimal.__contentElement.__element.children[0];
    var text = input.value.replace(/\./g, '').replace(/,/g, '');
    text = text.trim();
    if (text.length > 3) {
        text = text.replace(/^0?/, '');
    }
    while (text.length < 3) { text = "0" + text; }
    var newText = '';
    for (i = text.length - 1; i >= 0; i--) {
        var step = text.length - i;
        newText = text[i] + newText;
        if (step === 2) {
            newText = '.' + newText;
        } else if ((step > 2) && ((step - 2) % 3 === 0) && (step < text.length)) {
            newText = ',' + newText;
        }
    }
    input.value = newText;
}

//TWBD0000 - frmWarrantMessage rtfWarrant_KeyPress
function WarrantInput_KeyPress(warantInput, e) {
    var currentInputId = parseInt(warantInput.$$user_name.replace('rtfWarrant_', ''));
    var selection = warantInput.getSelection();
    if (e._identifier === 'Up' && currentInputId > 0)
    {
        var newInput = warantInput.$$parent.__widgetChildren[currentInputId - 1];
        var newSelection = newInput.getSelection();
        newSelection.start = selection.start;
        if (newInput.customProperties.blnOverType) {
            newSelection.length = 1;
        }
        newInput.setSelection(newSelection);
        newInput.focus();
        e.preventDefault();
    }
    else if (e._identifier === 'Left' && currentInputId > 0 && selection.start == 0)
    {
        var newInput = warantInput.$$parent.__widgetChildren[currentInputId - 1];
        var newSelection = newInput.getSelection();
        newSelection.start = 80;
        if (newInput.customProperties.blnOverType) {
            newSelection.length = 1;
        }
        newInput.setSelection(newSelection);
        newInput.focus();
        e.preventDefault();
    }
    else if (e._identifier === 'Down' && currentInputId < 9)
    {
        var newInput = warantInput.$$parent.__widgetChildren[currentInputId + 1];
        var newSelection = newInput.getSelection();
        newSelection.start = selection.start;
        if (newInput.customProperties.blnOverType) {
            newSelection.length = 1;
        }
        newInput.setSelection(newSelection);
        newInput.focus();
        e.preventDefault();
    }
    else if (e._identifier === 'Right' && currentInputId < 9 && selection.start == 80)
    {
        var newInput = warantInput.$$parent.__widgetChildren[currentInputId + 1];
        var newSelection = newInput.getSelection();
        newSelection.start = 0;
        if (newInput.customProperties.blnOverType) {
            newSelection.length = 1;
        }
        newInput.setSelection(newSelection);
        newInput.focus();
        e.preventDefault();
    }
    else
    {
        if (warantInput.customProperties.blnOverType) {
            if (selection.start < 80) {
                selection.length = 1;
                warantInput.setSelection(selection);
            }
            else if (selection.start === 80 && currentInputId < 9) {
                var newInput = warantInput.$$parent.__widgetChildren[currentInputId + 1];
                //newInput.focus();
                var newSelection = newInput.getSelection();
                newSelection.start = 0;
                newSelection.length = 1;
                newInput.setSelection(newSelection);
            }
        }
        else {
            var input = warantInput.__contentElement.__element.children[0];
            var text = input.value;
            if (text.length === 80 && (e._identifier.length === 1 || e._identifier === 'Space')) {
                var allowKey = confirm('You only have so much space for your warrant message.  By inserting this text you will be deleteing text as the bottom of your warrant message.  Do you wish to continue?');
                if (allowKey) {
                    input.value = text.substring(0, text.length - 1);
                    warantInput.setSelection(selection);
                }
            }
        }
    }
    
}

//TWBD0000 - frmWarrantMessage rtfWarrant_KeyUp
function WarrantInput_KeyUp(warantInput, e) {
    if (warantInput.customProperties.blnOverType) {
        var selection = warantInput.getSelection();
        if (e._identifier == 'Backspace' || e._identifier == 'Left') {
            selection.start = selection.start - 1;
        }
        selection.length = 1;
        warantInput.setSelection(selection);
    }
    if (e._identifier == 'Insert') {
        for (var i = 0; i < warantInput.$$parent.__widgetChildren.length; i++) {
            warantInputControl = warantInput.$$parent.__widgetChildren[i];
            warantInputControl.customProperties.blnOverType = !warantInputControl.customProperties.blnOverType;
        }
    }

}

//FC:FINAL:MSH - issue #987: TWUT0000: cancel entering value in textbox with appropriate service value
function Sewer_Water_CheckService(txtField, e) {
    if (txtField.$$user_name.toUpperCase() == "TXTOVERRIDESA" || txtField.$$user_name.toUpperCase() == "TXTOVERRIDESC") {
        if (txtField.customProperties.serviceValue.toUpperCase() == "W") {
            e._preventDefault = true;
        }
    }
    if (txtField.$$user_name.toUpperCase() == "TXTOVERRIDEWA" || txtField.$$user_name.toUpperCase() == "TXTOVERRIDEWC") {
        if (txtField.customProperties.serviceValue.toUpperCase() == "S") {
            e._preventDefault = true;
        }
    }
}

//Allow entering only numeric values
function TextBox_KeyPress_AllowOnlyNumbers(txtField, e) {
    switch (e.getKeyIdentifier()) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
        case "Backspace":
        case "Delete":
        case "Tab":
        case "Left":
        case "Right":
        case "Up":
        case "Down":
        case "Insert":
        case "Home":
        case "End":
            break;
        default:
            e.preventDefault();
            break;
    }
}

//Allow entering only numeric values and dot
function TextBox_KeyPress_AllowNumbersAndOneDot(txtField, e) {
    switch (e.getKeyIdentifier()) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
        case "Backspace":
        case "Delete":
        case "Tab":
        case "Left":
        case "Right":
        case "Up":
        case "Down":
        case "Insert":
        case "Home":
        case "End":
            break;
        case ".":
            var str = txtField.getValue();
            if (str.indexOf(".") > -1) {
                e.preventDefault();
            }
            break;
        default:
            e.preventDefault();
            break;
    }
}

function TextBox_OnPaste(e) {
    if (e._native && e._native.clipboardData && e._native.clipboardData.types && e._native.clipboardData.getData) {
        types = e._native.clipboardData.types;
        if ((types instanceof DOMStringList) && types.contains("text/plain")) {

            pastedData = e._native.clipboardData.getData('text/plain');
            for (i = 0; i < pastedData.length; i++) {
                switch (pastedData[i]) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "Backspace":
                    case "Delete":
                    case "Tab":
                    case "Left":
                    case "Right":
                    case "Up":
                    case "Down":
                    case "Insert":
                    case "Home":
                    case "End":
                        break;                    
                    default:
                        e._native.preventDefault();
                        return false;
                }
            }
        }
        else {
            e._native.preventDefault();
            return false;
        }
    }
}

function TextBox_OnPasteWithDot(e) {
    if (e._native && e._native.clipboardData && e._native.clipboardData.types && e._native.clipboardData.getData) {
        types = e._native.clipboardData.types;
        if ((types instanceof DOMStringList) && types.contains("text/plain")) {

            pastedData = e._native.clipboardData.getData('text/plain');
            for (i = 0; i < pastedData.length; i++) {
                switch (pastedData[i]) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "Backspace":
                    case "Delete":
                    case "Tab":
                    case "Left":
                    case "Right":
                    case "Up":
                    case "Down":
                    case "Insert":
                    case "Home":
                    case "End":
                        break;
                    case ".":
                        if (pastedData.indexOf(".") > -1) {
                            e.preventDefault();
                            return false;
                        }
                        break;
                    default:
                        e._native.preventDefault();
                        return false;
                }
            }
        }
        else {
            e._native.preventDefault();
            return false;
        }
    }
}

function RemoveAlphaCharacters(txtField, e) {
    var value = txtField.getValue();
    txtField.getChildControl("textfield").setValue(value.replace(/[^0-9.,]/, ""));
}

function PrintDiv(divId) {
    var divToPrint = document.getElementById(divId);
    var divToPrintWithStyle = divToPrint.cloneNode(true);
    var popupWin = window.open('', '_blank', 'width=800,height=600');
    
    cloneComputedStyle(divToPrint, divToPrintWithStyle);

    popupWin.document.open();
    //FC:FINAL:SBE - #4468 - use appendChild() instead of using innerHTML, because with innerHTML input values are lost
    //popupWin.document.write('<html><body onload="window.print()">' + divToPrintWithStyle.innerHTML + '</html>');
    popupWin.document.write('<html><body onload="window.print()"></body></html>');
    popupWin.document.body.appendChild(divToPrintWithStyle);
    popupWin.document.close();
    setTimeout(function () { popupWin.close(); }, 10);
}

function PrintRTB(divId, text) {
	var divToPrint = document.getElementById(divId);
	var popupWin = window.open('', '_blank', 'width=800,height=800');
	var div = createDiv(divToPrint, text);
	popupWin.document.open();
	popupWin.document.writeln('<html><body onload="window.print()">' + div + '</html>');
	popupWin.document.close();
	setTimeout(function () { popupWin.close(); }, 10);
}

function createDiv(divToPrint, text) {
    var content = '<div width=\"100%\"; height=\"100%\";>';
	//var computedStyle = false;
	//computedStyle = divToPrint.currentStyle || document.defaultView.getComputedStyle(divToPrint, null);
	//if (computedStyle) {
	//	var stylePropertyValid = function (name, value) {
	//		return typeof value !== 'undefined' &&
	//			typeof value !== 'object' &&
	//			typeof value !== 'function' &&
	//			value.length > 0 &&
	//			value != parseInt(value);
 //       };
 //       content += " style=\"";
	//	for (property in computedStyle) {
	//		if (property != 'width' && property != 'height' &&
	//			stylePropertyValid(property, computedStyle[property])) {
	//				content += property + ": " + computedStyle[property] + ";";
	//		}
 //       }
 //       content += "\"";
 //   }
	//content += 'width=\"100%\"; height=\"100%\";>';
	content += text + '</div>';

	return content;
}

var cloneComputedStyle = function (sourceElement, targetElement) {
    var computedStyle = false;
    computedStyle = sourceElement.currentStyle || document.defaultView.getComputedStyle(sourceElement, null);
    if (!computedStyle) return null;

    var stylePropertyValid = function (name, value) {
        return typeof value !== 'undefined' &&
            typeof value !== 'object' &&
            typeof value !== 'function' &&
            value.length > 0 &&
            value != parseInt(value);
    };

    for (property in computedStyle) {
        if (stylePropertyValid(property, computedStyle[property])) {
            targetElement.style[property] = computedStyle[property];

        }
    }

    for (var i = 0; i < sourceElement.children.length; i++) {
        cloneComputedStyle(sourceElement.children[i], targetElement.children[i]);
    }

};

function frmPayrollAccountsSetup_vsAccounts_KeyDown(vsAccountsEditor, e) {
    var col = vsAccountsEditor.customProperties.vsAccounts_Col;
    var gboolTownAccounts = vsAccountsEditor.customProperties.gboolTownAccounts;
    var gboolSchoolAccounts = vsAccountsEditor.customProperties.gboolSchoolAccounts;
    var gboolBudgetary = vsAccountsEditor.customProperties.gboolBudgetary;
    var AccountCol = vsAccountsEditor.customProperties.vsAccounts_AccountCol;
    var AccountTypeCol = vsAccountsEditor.customProperties.vsAccounts_AccountTypeCol;
    var accountTypeColValue = vsAccountsEditor.customProperties.accountTypeCol_Value;
    var descriptionColValue = vsAccountsEditor.customProperties.descriptionCol_Value;
    var identifier = e.getKeyIdentifier();
    if (col === AccountCol) {
        if (accountTypeColValue !== 'M' && (descriptionColValue === 'EMPLOYERS FICA' || descriptionColValue === 'EMPLOYERS MEDICARE')) {
            //allow x character and numbers
            if (identifier === 'x' || identifier === 'X' || IdentifierIsNumber(identifier) || IdentifierIsSpecialCharacter(identifier)) {
                //do nothing (allow the key)
            }
            else {
                //prevent the key
                if (gboolBudgetary) {
                    e.preventDefault();
                }
            }
        } else {
            if (IdentifierIsNumber(identifier) || IdentifierIsSpecialCharacter(identifier)) {
                //do nothing (allow the key)
            }
            else {
                //prevent the key
                if (gboolBudgetary) {
                    e.preventDefault();
                }
            }
        }
    }
    else if (col === AccountTypeCol) {
        if (gboolTownAccounts
            && (identifier === 'e' || identifier === 'E'
                || identifier === 'g' || identifier === 'G'
                || identifier === 'm' || identifier === 'M'
                || identifier === 'r' || identifier === 'R')) {
            //do nothing (allow the key)
        }
        else if (gboolSchoolAccounts
            && (identifier === 'l' || identifier === 'L'
                || identifier === 'p' || identifier === 'P'
                || identifier === 'v' || identifier === 'V')) {
            //do nothing (allow the key)
        }
        else if (identifier === 'm' || identifier === 'M') {
            //do nothing (allow the key)
        }
        else if (IdentifierIsSpecialCharacter(identifier)) {
            //do nothing (allow the key)
        }
        else {
            //prevent the key
            e.preventDefault();
        }
    }
}

function IdentifierIsNumber(identifier) {
    switch (identifier) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
            return true;
        default:
            return false;
    }
}

function IdentifierIsSpecialCharacter(identifier) {
    switch (identifier) {
        case "Backspace":
        case "Delete":
        case "Tab":
        case "Left":
        case "Right":
        case "Up":
        case "Down":
        case "Insert":
        case "Home":
        case "End":
            return true;
        default:
            return false;
    }
}

function FitStringToControlWidth(passedControl,originalString,useEllipses) {
    //var myid = passedControl.getContentElement().getDomElement();
    //var otherId = document.getElementById(passedControl.$$user_id);
    return FitStringToPixelWidth(originalString, passedControl.$state.width, '', useEllipses);
}


function FitStringToPixelWidth(originalString, maxWidth, className, addEllipses) {
    var adjustedString = originalString;
    var span = document.createElement("span");
    //span.className = className; 
    span.style.display = 'inline';
    span.style.visibility = 'hidden';
    //span.style.padding = '0px';
    document.body.appendChild(span);
    span.innerHTML = adjustedString;
    if (span.offsetWidth > maxWidth) {
        var startPosition = 0;
        var endPosition = adjustedString.length;
        var midPosition = 0;
        var calcPosition = 0;

        while (calcPosition = (endPosition - startPosition) >> 1) {
            midPosition = startPosition + calcPosition;
            if (addEllipses) {
                span.innerHTML = adjustedString.substring(0, midPosition) + '&hellip;';
            } else {
                span.innerHTML = adjustedString.substring(0, midPosition);
            }
            if (span.offsetWidth > maxWidth) {
                endPosition = midPosition;
            } else {
                startPosition = midPosition;
            }
        }
        if (addEllipses) {
            adjustedString = originalString.substring(0, startPosition) + '...';
        } else {
            adjustedString = originalString.substring(0, startPosition);
        }
    }
    document.body.removeChild(span);
    return adjustedString;
}

 function GetLocalMachineId(portToUse) {
    if (!portToUse) {
        portToUse = '44850';
    }
    if (portToUse == '') {
        portToUse = '44850';
    }
     var requestUri = 'http://localhost:' + portToUse + '/machineid';
     try {
         var oReq = new XMLHttpRequest();
         //oReq.timeout = 2000;
        // oReq.responseType = 'json';
         oReq.onload = function() {
             if (oReq.status == 200) {
                 var respobj = JSON.parse(oReq.response);
                 App.SetMachineIdentifier(respobj.machineId);
             }
         };
         oReq.onerror = function() {
         }
         oReq.open("GET", requestUri, false);
         oReq.send();
     } catch (err) {
         
     }
     //fetch(requestUri).then(response =>
    //    response.json().then(data => ({
    //            data: data,
    //            status: response.status
    //        })
    //    ).then(res => {
    //        if (res.status === 200) {
    //            App.SetMachineIdentifier(data.toString());
    //        }
    //    }))
    //    .catch(error => alert(error.toString()));

    //fetch(requestUri)
    //    .then(function (response) {
    //        alert(response.status);
    //        if (response.status === 200) {
    //            alert('hello');
    //           // let data = response.json();
               
    //            let data =  Promise.resolve(response.json().then(function (data) {
    //                alert('Data is ' + data.toString());
                   
    //            }));
    //            alert(data.toString());
               
    //            App.SetMachineIdentifier(data.toString());
    //            //response.json().then(function (data) {
    //            //    alert('Data is ' + data.toString());
    //            //    App.SetMachineIdentifier(data.toString());
    //            //});
    //            // App.SetMachineIdentifier(response.json().toString());
    //        } else {
    //            alert('good bye');
    //        }
    //    })
    //     .catch(error => alert(error.toString()));


    //fetch(requestUri)
    //    .then(response => response.json())
    //    .then(data => App.SetMachineIdentifier(data));
    return '';
}

function CheckLocalMachineId(portToUse) {
    if (!portToUse) {
        portToUse = '44850';
    }
    if (portToUse == '') {
        portToUse = '44850';
    }
    var requestUri = 'http://localhost:' + portToUse + '/machineid';
    try {
        var oReq = new XMLHttpRequest();
        oReq.onload = function () {
        };
        oReq.onerror = function () {
        }
        oReq.open("GET", requestUri, false);
        oReq.send();
    } catch (err) {

    }
    return '';
}

