﻿/**
 * app.MessageBoxPatch
 *
 * Adds keyboard support to wisej.web.MessageBox.
 */
qx.Mixin.define("app.MessageBoxPatch", {

	construct: function () {

		this.addListener("keydown", this.__onKeyDown, this);

	},

	members: {

		__onKeyDown: function (e) {

			if (e.getModifiers() !== 0)
				return;

			switch (e.getKeyIdentifier()) {

				case "Down":
				case "Right":
					qx.ui.core.FocusHandler.getInstance().focusNext();
					break;

				case "Up":
				case "Left":
					qx.ui.core.FocusHandler.getInstance().focusPrev();
					break;
			}
		}
	}
});

qx.Class.patch(wisej.web.MessageBox, app.MessageBoxPatch);
