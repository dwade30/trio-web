﻿

/**
 * app.RightTreeNode
 *
 * Extends the wisej.web.TreeNode in order to move
 * the open/close button to the far right.
 */
qx.Class.define("app.RightTreeNode", {

	extend: wisej.web.TreeNode,

	properties:
	 {
	 	appearance: { refine: true, init: "right-tree-folder" }
	 },

	members: {

		// overridden
		_applyCanExpand: function (value, old) {

			if (value) {
				this.setOpenSymbolMode("always");
				this.setAppearance("right-tree-folder");
			}
			else {
				this.setOpenSymbolMode("auto");
				this.setAppearance("right-tree-file");

				// remove the ajax loader, in case the node was loading.
				this.__hideLoader();
			}
		},

		// overridden
		_addWidgets: function () {
			this.addSpacer();
			this.addCheckBox();
			this.addIconState();
			this.addIcon();
			this.addLabel();
			this.addOpenButton();
		},

		// overridden
		addLabel: function (text) {

			this.base(arguments, text);

			var label = this.getChildControl("label");
			label.setAllowGrowX(true);
			label.setLayoutProperties({ flex: 1 });
		},

		/**
		 * Adds the spacer used to render the indentation to the item's horizontal
		 * box layout. If the spacer has been added before, it is removed from its
		 * old position and added to the end of the layout.
		 */
		addSpacer: function () {

			if (!this.__spacer)
				this.__spacer = this.getChildControl("spacer");

			this._add(this.__spacer);
		},

		// overridden
		_createChildControlImpl: function (id, hash) {
			var control;

			switch (id) {
				case "spacer":
					control = new qx.ui.core.Widget().set({
						width: 0,
						height: 0
					});
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * Update the indentation of the tree item.
		 */
		_updateIndent: function () {

			if (!this.getTree()) {
				return;
			}

			var openWidth = 0;
			var open = this.getChildControl("open", true);

			if (open) {
				if (this._shouldShowOpenSymbol()) {
					open.show();
				}
				else {
					open.exclude();
				}
			}

			//if (this.__spacer) {
			//    this.__spacer.setWidth((this.getLevel()) * this.getIndent() - openWidth);
			//}
			this.setMarginLeft(10 + this.getLevel() * this.getIndent() - openWidth);
			this.__contentElement.__styleValues.overflowX = "none";
			this.__contentElement.__styleValues.overflowY = "none";

			var label = this.getChildControl("label", true);
			if (label && this.getLevel() > 0) {
			    label.setMarginTop(6);
			    label.setMarginBottom(5);
			}

            //SBE - vertical line for subnodes
			if (this.__spacer && this.getLevel() > 0) {
			    this.__spacer.setMarginLeft(-30 + (-2 * this.getLevel()));
			    this.__spacer.setWidth(30);

			    var border = new qx.ui.decoration.Decorator();
			    border.setRight(2, "solid", "#51555B");

			    if (this.getLevel() > 1) {
			        border.setLeft(2, "solid", "#51555B");
			    }
			    this.__spacer.set({
			        decorator: border
			    });
			}
		},
	}

});