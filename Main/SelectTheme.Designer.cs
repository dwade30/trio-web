﻿namespace Main
{
    partial class SelectTheme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.berry = new Wisej.Web.PictureBox();
            this.purple = new Wisej.Web.PictureBox();
            this.orange = new Wisej.Web.PictureBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.fcLabel4 = new fecherFoundation.FCLabel();
            this.green = new Wisej.Web.PictureBox();
            this.fcLabel5 = new fecherFoundation.FCLabel();
            this.slate = new Wisej.Web.PictureBox();
            this.fcLabel6 = new fecherFoundation.FCLabel();
            this.light = new Wisej.Web.PictureBox();
            this.pictureBox1 = new Wisej.Web.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.berry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // berry
            // 
            this.berry.Cursor = Wisej.Web.Cursors.Hand;
            this.berry.ImageSource = "color-scheme-berry";
            this.berry.Location = new System.Drawing.Point(20, 20);
            this.berry.Name = "berry";
            this.berry.Size = new System.Drawing.Size(250, 150);
            this.berry.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // purple
            // 
            this.purple.Cursor = Wisej.Web.Cursors.Hand;
            this.purple.ImageSource = "color-scheme-purple";
            this.purple.Location = new System.Drawing.Point(290, 20);
            this.purple.Name = "purple";
            this.purple.Size = new System.Drawing.Size(250, 150);
            this.purple.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // orange
            // 
            this.orange.Cursor = Wisej.Web.Cursors.Hand;
            this.orange.ImageSource = "color-scheme-orange";
            this.orange.Location = new System.Drawing.Point(560, 20);
            this.orange.Name = "orange";
            this.orange.Size = new System.Drawing.Size(250, 150);
            this.orange.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // fcLabel1
            // 
            this.fcLabel1.Capitalize = false;
            this.fcLabel1.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel1.Location = new System.Drawing.Point(20, 176);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(250, 17);
            this.fcLabel1.TabIndex = 5;
            this.fcLabel1.Text = "Berry";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fcLabel2
            // 
            this.fcLabel2.Capitalize = false;
            this.fcLabel2.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel2.Location = new System.Drawing.Point(290, 176);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(250, 17);
            this.fcLabel2.TabIndex = 6;
            this.fcLabel2.Text = "Purple";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fcLabel3
            // 
            this.fcLabel3.Capitalize = false;
            this.fcLabel3.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel3.Location = new System.Drawing.Point(560, 176);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(250, 17);
            this.fcLabel3.TabIndex = 7;
            this.fcLabel3.Text = "Orange";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fcLabel4
            // 
            this.fcLabel4.Capitalize = false;
            this.fcLabel4.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel4.Location = new System.Drawing.Point(20, 376);
            this.fcLabel4.Name = "fcLabel4";
            this.fcLabel4.Size = new System.Drawing.Size(250, 17);
            this.fcLabel4.TabIndex = 12;
            this.fcLabel4.Text = "Green";
            this.fcLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // green
            // 
            this.green.Cursor = Wisej.Web.Cursors.Hand;
            this.green.ImageSource = "color-scheme-green";
            this.green.Location = new System.Drawing.Point(20, 220);
            this.green.Name = "green";
            this.green.Size = new System.Drawing.Size(250, 150);
            this.green.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // fcLabel5
            // 
            this.fcLabel5.Capitalize = false;
            this.fcLabel5.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel5.Location = new System.Drawing.Point(290, 376);
            this.fcLabel5.Name = "fcLabel5";
            this.fcLabel5.Size = new System.Drawing.Size(250, 17);
            this.fcLabel5.TabIndex = 15;
            this.fcLabel5.Text = "Slate";
            this.fcLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // slate
            // 
            this.slate.Cursor = Wisej.Web.Cursors.Hand;
            this.slate.ImageSource = "color-scheme-slate";
            this.slate.Location = new System.Drawing.Point(290, 220);
            this.slate.Name = "slate";
            this.slate.Size = new System.Drawing.Size(250, 150);
            this.slate.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // fcLabel6
            // 
            this.fcLabel6.Capitalize = false;
            this.fcLabel6.Font = new System.Drawing.Font("semibold", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcLabel6.Location = new System.Drawing.Point(560, 376);
            this.fcLabel6.Name = "fcLabel6";
            this.fcLabel6.Size = new System.Drawing.Size(250, 17);
            this.fcLabel6.TabIndex = 18;
            this.fcLabel6.Text = "Light";
            this.fcLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // light
            // 
            this.light.Cursor = Wisej.Web.Cursors.Hand;
            this.light.ImageSource = "color-scheme-light";
            this.light.Location = new System.Drawing.Point(560, 220);
            this.light.Name = "light";
            this.light.Size = new System.Drawing.Size(250, 150);
            this.light.Click += new System.EventHandler(this.colorScheme_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageSource = "icon - process complete - active?color=#05CC47";
            this.pictureBox1.Location = new System.Drawing.Point(762, 189);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            // 
            // SelectTheme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 408);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.fcLabel6);
            this.Controls.Add(this.light);
            this.Controls.Add(this.fcLabel5);
            this.Controls.Add(this.slate);
            this.Controls.Add(this.fcLabel4);
            this.Controls.Add(this.green);
            this.Controls.Add(this.fcLabel3);
            this.Controls.Add(this.fcLabel2);
            this.Controls.Add(this.fcLabel1);
            this.Controls.Add(this.orange);
            this.Controls.Add(this.purple);
            this.Controls.Add(this.berry);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectTheme";
            this.Text = "SELECT A THEME";
            this.Load += new System.EventHandler(this.SelectTheme_Load);
            ((System.ComponentModel.ISupportInitialize)(this.berry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.PictureBox berry;
        private Wisej.Web.PictureBox purple;
        private Wisej.Web.PictureBox orange;
        private fecherFoundation.FCLabel fcLabel1;
        private fecherFoundation.FCLabel fcLabel2;
        private fecherFoundation.FCLabel fcLabel3;
        private fecherFoundation.FCLabel fcLabel4;
        private Wisej.Web.PictureBox green;
        private fecherFoundation.FCLabel fcLabel5;
        private Wisej.Web.PictureBox slate;
        private fecherFoundation.FCLabel fcLabel6;
        private Wisej.Web.PictureBox light;
        private Wisej.Web.PictureBox pictureBox1;
    }
}