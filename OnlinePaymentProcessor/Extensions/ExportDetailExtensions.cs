﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Extensions
{
    public static class ExportDetailExtensions
    {
        public static IEnumerable<BillException> GetBillExceptions(this IEnumerable<ExportDetail> exportDetails)
        {
            return exportDetails.Where(ed => ed.CostDue < 0 || ed.SalesTaxDue < 0 || ed.InterestDue < 0).Select(ed => ed.ToBillException()).ToList();
        }

        public static BillException ToBillException(this ExportDetail exportDetail)
        {
            var billException = new BillException();
            if (exportDetail.CostDue < 0)
            {
                billException.Reason = "Negative cost due";
            }

            if (exportDetail.SalesTaxDue < 0)
            {
                billException.Reason = "Negative tax due";
            }

            if (exportDetail.InterestDue < 0)
            {
                billException.Reason = "Negative interest due";
            }

            if (!String.IsNullOrWhiteSpace(billException.Reason))
            {
                billException.Account = exportDetail.AccountNumber;
                billException.BillType = exportDetail.BillType;
                billException.BillIdentifier = exportDetail.BillNumber;
            }
            return billException;
        }
        
        public static IEnumerable<ExportDetail> WithoutExceptions(this IEnumerable<ExportDetail> exportDetails)
        {
            return exportDetails.Where(ed => ed.CostDue >= 0 && ed.InterestDue >= 0 && ed.SalesTaxDue >= 0);
        }

        public static IEnumerable<int> MapToAccountNumbers(this IEnumerable<ExportDetail> exportDetails)
        {
            return exportDetails.Select(ed => ed.AccountNumber).ToList();
        }
    }
}
