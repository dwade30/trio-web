﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Extensions
{
    public static partial class UTBillRecordExtensions
    {
        public static IEnumerable<Bill> MapToCombinedBill(this IEnumerable<UTBillRecord> billRecords)
        {
            return billRecords.Select(BillFromCombinedBill);
        }

        public static Bill BillFromCombinedBill(this UTBillRecord billRecord)
        {

            UTBill bill;
            if (billRecord.IsSewerLien || billRecord.IsWaterLien)
            {
                var sewerLien = new UTLien();
                sewerLien = billRecord.SewerLien ?? new UTLien();
                var combinedLien = sewerLien.CombineWith(billRecord.WaterLien);
                bill = combinedLien.ToUTBill();

                bill.AccountNumber = billRecord.AccountNumber;
                bill.BillType = BillType.UtilityBilling;
                bill.InterestPaidDate = billRecord.SewerInterestPaidDate ?? (billRecord.WaterInterestPaidDate ?? DateTime.MinValue);
                bill.DaysPerYear = billRecord.DaysPerYear;
                bill.BillKey = billRecord.SewerLienID != 0 ? billRecord.SewerLienID : billRecord.WaterLienID;
                bill.Periods.Add(new UTPeriod
                {
                    DueDate = combinedLien.DueDate ?? DateTime.MinValue,
                    InterestDate = combinedLien.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = combinedLien.Principal,
                    PrincipalDue = combinedLien.PrincipalDue,
                    WaterPrincipalDue = billRecord.WaterLien?.PrincipalDue ?? 0,
                    SewerPrincipalDue = sewerLien?.PrincipalDue ?? 0
                });
            }
            else
            {
                var combinedUtilityAmount = billRecord.SewerUtilityAmount.CombineWith(billRecord.WaterUtilityAmount);
                bill = new UTBill()
                {
                    IsLien = false,
                    AccountNumber = billRecord.AccountNumber,
                    BillType = BillType.UtilityBilling,
                    BillNumber = billRecord.BillNumber.ToString(),
                    BillKey = billRecord.ID,
                    CostAdded = combinedUtilityAmount.CostAdded,
                    CostBilled = combinedUtilityAmount.CostBilled,
                    CostPaid = combinedUtilityAmount.CostPaid,
                    DaysPerYear = billRecord.DaysPerYear,
                    DiscountAmount = 0,
                    InterestAdded = combinedUtilityAmount.InterestAdded,
                    InterestBilled = combinedUtilityAmount.InterestBilled,
                    InterestPaid = combinedUtilityAmount.InterestPaid,
                    InterestPaidDate = billRecord.SewerInterestPaidDate ?? (billRecord.WaterInterestPaidDate ?? DateTime.MinValue),
                    SewerInterestPaidDate = billRecord.SewerInterestPaidDate ?? DateTime.MinValue,
                    WaterInterestPaidDate = billRecord.WaterInterestPaidDate ?? DateTime.MinValue,
                    InterestRate = billRecord.SewerInterestRate != 0 ? billRecord.SewerInterestRate : billRecord.WaterInterestRate,
                    SewerInterestRate = billRecord.SewerInterestRate,
                    WaterInterestRate = billRecord.WaterInterestRate,
                    SalesTaxPaid = combinedUtilityAmount.SalesTaxPaid,
                    SalesTaxBilled = combinedUtilityAmount.SalesTaxBilled,
                    PrincipalPaid = combinedUtilityAmount.PrincipalPaid,
                    Owner1FName = "",
                    Owner1LName = billRecord.OwnerName,
                    Owner2FName = "",
                    Owner2LName = billRecord.OwnerName2,
                    OwnerAddress1 = billRecord.OwnerAddress1,
                    OwnerAddress2 = billRecord.OwnerAddress2,
                    OwnerAddress3 = billRecord.OwnerAddress3,
                    OwnerCity = billRecord.OwnerCity,
                    OwnerCountry = "",
                    OwnerState = billRecord.OwnerState,
                    OwnerZip = billRecord.OwnerZip + (string.IsNullOrWhiteSpace(billRecord.OwnerZip4) ? string.Empty : "-" + billRecord.OwnerZip4),
                    Tenant1FName = "",
                    Tenant1LName = billRecord.TenantName,
                    Tenant2FName = "",
                    Tenant2LName = billRecord.TenantName2,
                    TenantAddress1 = billRecord.TenantAddress1,
                    TenantAddress2 = billRecord.TenantAddress2,
                    TenantAddress3 = billRecord.TenantAddress3,
                    TenantCity = billRecord.TenantCity,
                    TenantState = billRecord.TenantState,
                    TenantCountry = "",
                    TenantZip = billRecord.TenantZip + (string.IsNullOrWhiteSpace(billRecord.TenantZip4) ? string.Empty : "-" + billRecord.TenantZip4),
                };
                UTPeriod p = new UTPeriod()
                {
                    DueDate = billRecord.DueDate ?? DateTime.MinValue,
                    InterestDate = billRecord.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = combinedUtilityAmount.PrincipalOwed,
                    PrincipalDue = billRecord.TotalPrincipalBalance,
                    WaterPrincipalDue = billRecord.WaterPrincipalBalance,
                    SewerPrincipalDue = billRecord.SewerPrincipalBalance
                };
                bill.Periods.Add(p);
            }

            return bill;
        }

        public static UTLien CombineWith(this UTLien sourceLien, UTLien addOnLien)
        {
            var combinedLien = new UTLien();
            if (sourceLien != null)
            {
                combinedLien.PrincipalPaid = sourceLien.PrincipalPaid;
                combinedLien.InterestAdded = sourceLien.InterestAdded;
                combinedLien.InterestPaid = sourceLien.InterestPaid;
                combinedLien.InterestPaidDate = sourceLien.InterestPaidDate;
                combinedLien.Principal = sourceLien.Principal;
                combinedLien.Costs = sourceLien.Costs;
                combinedLien.SewerInterestRate = sourceLien.SewerInterestRate;
                combinedLien.CostsPaid = sourceLien.CostsPaid;
                combinedLien.PreLienInterestPaid = sourceLien.PreLienInterestPaid;
                combinedLien.DueDate = sourceLien.DueDate;
                combinedLien.InterestStartDate = sourceLien.InterestStartDate;
                combinedLien.WaterInterestRate = sourceLien.WaterInterestRate;
                combinedLien.InterestPaidDate = sourceLien.InterestPaidDate;
                combinedLien.ID = sourceLien.ID;
                combinedLien.TaxPaid = sourceLien.TaxPaid;
                combinedLien.MaturityFee = sourceLien.MaturityFee;
                combinedLien.Interest = sourceLien.Interest;
                combinedLien.Tax = sourceLien.Tax;
            }

            if (addOnLien != null)
            {
                combinedLien.PrincipalPaid += addOnLien.PrincipalPaid;
                combinedLien.InterestAdded += addOnLien.InterestAdded;
                combinedLien.InterestPaid += addOnLien.InterestPaid;
                combinedLien.Principal += addOnLien.Principal;
                combinedLien.Costs += addOnLien.Costs;
                if (combinedLien.SewerInterestRate == 0)
                {
                    combinedLien.SewerInterestRate = addOnLien.SewerInterestRate;
                }
                combinedLien.CostsPaid += addOnLien.CostsPaid;
                combinedLien.PreLienInterestPaid += addOnLien.PreLienInterestPaid;
                combinedLien.DueDate = combinedLien.DueDate ?? addOnLien.DueDate;
                combinedLien.InterestStartDate = combinedLien.InterestStartDate ?? addOnLien.InterestStartDate;
                if (combinedLien.WaterInterestRate == 0)
                {
                    combinedLien.WaterInterestRate = addOnLien.WaterInterestRate;
                }

                combinedLien.InterestPaidDate = combinedLien.InterestPaidDate ?? addOnLien.InterestPaidDate;
                combinedLien.ID = addOnLien.ID;
                combinedLien.TaxPaid += addOnLien.TaxPaid;
                combinedLien.MaturityFee += addOnLien.MaturityFee;
                combinedLien.Interest += addOnLien.Interest;
                combinedLien.Tax += addOnLien.Tax;
            }

            return combinedLien;
        }

        public static UTBill ToUTBill(this UTLien utLien)
        {
            if (utLien == null)
            {
                return new UTBill();
            }

            return new UTBill()
            {
                IsLien = true,
                BillType = BillType.Sewer,
                PrincipalPaid = utLien.PrincipalPaid,
                BillNumber = utLien.ID.ToString(),
                InterestBilled = utLien.Interest,
                CostAdded = utLien.MaturityFee * -1m,
                SalesTaxPaid = utLien.TaxPaid,
                CostBilled = utLien.Costs,
                SalesTaxBilled = utLien.Tax,
                InterestPaid = utLien.PreLienInterestPaid + utLien.InterestPaid,
                CostPaid = utLien.CostsPaid,
                InterestAdded = utLien.InterestAdded * -1m,
                SewerInterestRate = utLien.SewerInterestRate,
                WaterInterestRate = utLien.WaterInterestRate,
                SewerInterestPaidDate = utLien.InterestPaidDate ?? DateTime.MinValue,
                WaterInterestPaidDate = utLien.InterestPaidDate ?? DateTime.MinValue
            };
        }

        public static IEnumerable<Bill> MapToSewerBill(this IEnumerable<UTBillRecord> billRecords)
        {
            return billRecords.Select(BillFromSewerBill);
        }

        public static IEnumerable<Bill> MapToWaterBill(this IEnumerable<UTBillRecord> billRecords)
        {
            return billRecords.Select(BillFromWaterBill);
        }

        public static Bill BillFromSewerBill(this UTBillRecord billRecord)
        {
            var bill = new Bill();
            if (billRecord.IsSewerLien)
            {
                bill = billRecord.SewerLien.ToBill();
                bill.AccountNumber = billRecord.AccountNumber;
                bill.BillType = BillType.Sewer;
                bill.DaysPerYear = billRecord.DaysPerYear;
                bill.BillKey = billRecord.SewerLienID;
                bill.Periods.Add(new Period
                {
                    DueDate = billRecord.SewerLien.DueDate ?? DateTime.MinValue,
                    InterestDate = billRecord.SewerLien.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = billRecord.SewerLien.Principal,
                    PrincipalDue = billRecord.SewerLien.PrincipalDue
                }
                );
            }
            else
            {
                bill = new Bill()
                {
                    IsLien = false,
                    AccountNumber = billRecord.AccountNumber,
                    BillType = BillType.Sewer,
                    BillNumber = billRecord.BillNumber.ToString(),
                    BillKey = billRecord.ID,
                    CostAdded = billRecord.SewerUtilityAmount.CostAdded,
                    CostBilled = billRecord.SewerUtilityAmount.CostBilled,
                    CostPaid = billRecord.SewerUtilityAmount.CostPaid,
                    DaysPerYear = billRecord.DaysPerYear,
                    DiscountAmount = 0,
                    InterestAdded = billRecord.SewerUtilityAmount.InterestAdded,
                    InterestBilled = billRecord.SewerUtilityAmount.InterestBilled,
                    InterestPaid = billRecord.SewerUtilityAmount.InterestPaid,
                    InterestPaidDate = billRecord.SewerInterestPaidDate ?? DateTime.MinValue,
                    InterestRate = billRecord.SewerInterestRate,
                    SalesTaxPaid = billRecord.SewerUtilityAmount.SalesTaxPaid,
                    SalesTaxBilled = billRecord.SewerUtilityAmount.SalesTaxBilled,
                    PrincipalPaid = billRecord.SewerUtilityAmount.PrincipalPaid,
                    Owner1FName = "",
                    Owner1LName = billRecord.OwnerName,
                    Owner2FName = "",
                    Owner2LName = billRecord.OwnerName2,
                    OwnerAddress1 = billRecord.OwnerAddress1,
                    OwnerAddress2 = billRecord.OwnerAddress2,
                    OwnerAddress3 = billRecord.OwnerAddress3,
                    OwnerCity = billRecord.OwnerCity,
                    OwnerCountry = "",
                    OwnerState = billRecord.OwnerState,
                    OwnerZip = billRecord.OwnerZip + (string.IsNullOrWhiteSpace(billRecord.OwnerZip4) ? string.Empty : "-" + billRecord.OwnerZip4),
                    Tenant1FName = "",
                    Tenant1LName = billRecord.TenantName,
                    Tenant2FName = "",
                    Tenant2LName = billRecord.TenantName2,
                    TenantAddress1 = billRecord.TenantAddress1,
                    TenantAddress2 = billRecord.TenantAddress2,
                    TenantAddress3 = billRecord.TenantAddress3,
                    TenantCity = billRecord.TenantCity,
                    TenantState = billRecord.TenantState,
                    TenantCountry = "",
                    TenantZip = billRecord.TenantZip + (string.IsNullOrWhiteSpace(billRecord.TenantZip4) ? string.Empty : "-" + billRecord.TenantZip4),
                };
                Period p = new Period()
                {
                    DueDate = billRecord.DueDate ?? DateTime.MinValue,
                    InterestDate = billRecord.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = billRecord.SewerUtilityAmount.PrincipalOwed,
                    PrincipalDue = billRecord.SewerPrincipalBalance

                };
                bill.Periods.Add(p);
            }
            return bill;
        }

        public static Bill BillFromWaterBill(this UTBillRecord billRecord)
        {
            var bill = new Bill();
            if (billRecord.IsWaterLien)
            {
                bill = billRecord.WaterLien.ToBill();
                bill.AccountNumber = billRecord.AccountNumber;
                bill.BillType = BillType.Water;
                bill.InterestPaidDate = billRecord.SewerInterestPaidDate ?? DateTime.MinValue;
                bill.DaysPerYear = billRecord.DaysPerYear;
                bill.BillKey = billRecord.WaterLienID;
                bill.Periods.Add(new Period
                {
                    DueDate = billRecord.WaterLien.DueDate ?? DateTime.MinValue,
                    InterestDate = billRecord.WaterLien.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = billRecord.WaterLien.Principal,
                    PrincipalDue = billRecord.WaterLien.PrincipalDue
                }
                );
            }
            else
            {
                bill = new Bill()
                {
                    IsLien = false,
                    AccountNumber = billRecord.AccountNumber,
                    BillType = BillType.Water,
                    BillNumber = billRecord.BillNumber.ToString(),
                    BillKey = billRecord.ID,
                    CostAdded = billRecord.WaterUtilityAmount.CostAdded,
                    CostBilled = billRecord.WaterUtilityAmount.CostBilled,
                    CostPaid = billRecord.WaterUtilityAmount.CostPaid,
                    DaysPerYear = billRecord.DaysPerYear,
                    DiscountAmount = 0,
                    InterestAdded = billRecord.WaterUtilityAmount.InterestAdded,
                    InterestBilled = billRecord.WaterUtilityAmount.InterestBilled,
                    InterestPaid = billRecord.WaterUtilityAmount.InterestPaid,
                    InterestPaidDate = billRecord.WaterInterestPaidDate ?? DateTime.MinValue,
                    InterestRate = billRecord.WaterInterestRate,
                    SalesTaxPaid = billRecord.WaterUtilityAmount.SalesTaxPaid,
                    SalesTaxBilled = billRecord.WaterUtilityAmount.SalesTaxBilled,
                    PrincipalPaid = billRecord.WaterUtilityAmount.PrincipalPaid,
                    Owner1FName = "",
                    Owner1LName = billRecord.OwnerName,
                    Owner2FName = "",
                    Owner2LName = billRecord.OwnerName2,
                    OwnerAddress1 = billRecord.OwnerAddress1,
                    OwnerAddress2 = billRecord.OwnerAddress2,
                    OwnerAddress3 = billRecord.OwnerAddress3,
                    OwnerCity = billRecord.OwnerCity,
                    OwnerCountry = "",
                    OwnerState = billRecord.OwnerState,
                    OwnerZip = billRecord.OwnerZip + (string.IsNullOrWhiteSpace(billRecord.OwnerZip4) ? string.Empty : "-" + billRecord.OwnerZip4),
                    Tenant1FName = "",
                    Tenant1LName = billRecord.TenantName,
                    Tenant2FName = "",
                    Tenant2LName = billRecord.TenantName2,
                    TenantAddress1 = billRecord.TenantAddress1,
                    TenantAddress2 = billRecord.TenantAddress2,
                    TenantAddress3 = billRecord.TenantAddress3,
                    TenantCity = billRecord.TenantCity,
                    TenantState = billRecord.TenantState,
                    TenantCountry = "",
                    TenantZip = billRecord.TenantZip + (string.IsNullOrWhiteSpace(billRecord.TenantZip4) ? string.Empty : "-" + billRecord.TenantZip4),
                };
                Period p = new Period()
                {
                    DueDate = billRecord.DueDate ?? DateTime.MinValue,
                    InterestDate = billRecord.InterestStartDate ?? DateTime.MinValue,
                    PrincipalAmount = billRecord.WaterUtilityAmount.PrincipalOwed,
                    PrincipalDue = billRecord.WaterPrincipalBalance

                };
                bill.Periods.Add(p);
            }
            return bill;
        }

        public static Bill ToBill(this UTLien utLien)
        {
            if (utLien == null)
            {
                return new Bill();
            }

            return new Bill()
            {
                IsLien = true,
                BillType = BillType.Sewer,
                PrincipalPaid = utLien.PrincipalPaid,
                BillNumber = utLien.ID.ToString(),
                InterestBilled = utLien.Interest,
                InterestPaidDate = utLien.InterestPaidDate ?? DateTime.MinValue,
                CostAdded = utLien.MaturityFee * -1m,
                SalesTaxPaid = utLien.TaxPaid,
                CostBilled = utLien.Costs,
                SalesTaxBilled = utLien.Tax,
                InterestPaid = utLien.PreLienInterestPaid + utLien.InterestPaid,
                CostPaid = utLien.CostsPaid,
                InterestAdded = utLien.InterestAdded * -1m,
                InterestRate = utLien.SewerInterestRate
            };


        }
    }
}
