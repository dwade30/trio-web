﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Extensions
{
	public static class BillTypeExtensions
	{
		public static string ToBillTypeCode(this BillType billType)
		{
			var result = "";
			switch (billType)
			{
				case BillType.RealEstate:
					result = "RE";
					break;
				case BillType.PersonalPropery:
					result = "PP";
					break;
				case BillType.Water:
					result = "WA";
					break;
				case BillType.Sewer:
					result = "SW";
					break;
				case BillType.UtilityBilling:
					result = "UT";
					break;
				default:
					result = "  ";
					break;
			}
			
			return result;
		}
    }
}
