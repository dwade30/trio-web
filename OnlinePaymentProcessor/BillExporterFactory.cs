﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using OnlinePaymentProcessor.PersonalProperty;
using OnlinePaymentProcessor.RealEstate;
using OnlinePaymentProcessor.UtilityBilling;

namespace OnlinePaymentProcessor
{
    public class BillExporterFactory
    {
        private Settings payportSettings;
        private Autofac.IContainer diContainer;

        public BillExporterFactory(Settings config, Autofac.IContainer container)
        {
	        payportSettings = config;
	        diContainer = container;
        }

        public BillExporter<UTBillRecord> GetUtilityBillExporter()
        {
            return new BillExporter<UTBillRecord>(CreateUTDataLayer(), CreateUTGenerator());
        }

        private IDataLayer<UTBillRecord> CreateUTDataLayer()
        {
            return new UTSQLDataLayer(diContainer);
        }

        private IBillGenerator<UTBillRecord> CreateUTGenerator()
        {
            if (payportSettings.CombineUtilityServices)
            {
                return new UTCombinedBillGenerator(payportSettings);
            }
            else
            {
                return new UTSeparateBillGenerator(payportSettings);
            }
        }

        public BillExporter<REBillRecord> GetReBillExporter()
        {
            return new BillExporter<REBillRecord>(CreateREDataLayer(), CreateREGenerator());
        }

        public IDataLayer<REBillRecord> CreateREDataLayer()
        {
            return new RESQLDataLayer(diContainer);
        }

        public IBillGenerator<REBillRecord> CreateREGenerator()
        {
            return new REBillGenerator();
        }

        public BillExporter<PPBillRecord> GetPPBillExporter()
        {
            return new BillExporter<PPBillRecord>(CreatePPDataLayer(), CreatePPGenerator());
        }

        public IDataLayer<PPBillRecord> CreatePPDataLayer()
        {
            return new PPSQLDataLayer(diContainer);
        }

        public IBillGenerator<PPBillRecord> CreatePPGenerator()
        {
            return new PPBillGenerator();
        }
    }
}
