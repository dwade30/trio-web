﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Extensions;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.SystemSettings.Extensions;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using SharedDataAccess;
using SharedDataAccess.CentralData;
using TWSharedLibrary;

namespace OnlinePaymentProcessor
{
    public class PaymentImporter
    {
        private INetSync paymentSync;
        private ITrioContextFactory contextFactory;
        private IUtilityBillingPaymentProcessor utilityBillingPaymentProcessor;
        private IPropertyTaxImportedPaymentProcessor propertyTaxPaymentProcessor;
        private Settings settings;
        private CommandDispatcher commandDispatcher;
        private IGlobalActiveModuleSettings activeModuleSettings;
        private IGlobalBudgetaryAccountSettings budgetaryAccountSettings;
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleQueryHandler;
		private IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler;
		private IBudgetaryJournalService budgetaryJournalService;
		public PaymentImporter(INetSync netSync, ITrioContextFactory contextFactory, Settings settings, IUtilityBillingPaymentProcessor utilityBillingPaymentProcessor, CommandDispatcher commandDispatcher, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, 
			IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleQueryHandler, IBudgetaryJournalService budgetaryJournalService, IPropertyTaxImportedPaymentProcessor propertyTaxPaymentProcessor, IGlobalActiveModuleSettings activeModuleSettings, IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler)
		{
            paymentSync = netSync;
            this.contextFactory = contextFactory;
            this.settings = settings;
            this.utilityBillingPaymentProcessor = utilityBillingPaymentProcessor;
            this.commandDispatcher = commandDispatcher;
            this.receiptTypeQueryHandler = receiptTypeQueryHandler;
            this.deptDivTitleQueryHandler = deptDivTitleQueryHandler;
            this.ledgerTitleQueryHandler = ledgerTitleQueryHandler;
            this.activeModuleSettings = activeModuleSettings;
			this.budgetaryAccountSettings = activeModuleSettings.BudgetaryIsActive ? commandDispatcher.Send(new GetBudgetarySettings()).Result.ToGlobalBudgetaryAccountSettings() : null;
			this.budgetaryJournalService = budgetaryJournalService;
			this.propertyTaxPaymentProcessor = propertyTaxPaymentProcessor;
		}

        public void ImportPayments()
        {
            List<Payment> payments = (List<Payment>)paymentSync.DownloadPayments("geocode=" + settings.GeoCode);
            Logger.Log("Downloaded " + payments.Count.ToString() + " payments", EventLogEntryType.Information);

            var centralDataContext = contextFactory.GetCentralDataContext();

            centralDataContext.ImportedPayments.AddRange(payments.MapToImportedPayments());
            centralDataContext.SaveChanges();
        }

        public void ProcessImportedPayments()
        {
	        var centralDataContext = contextFactory.GetCentralDataContext();
            var taxPayments = new List<PropertyTaxTransaction>();
            var unappliedPayments = centralDataContext.ImportedPayments.Where(x => !(x.Processed ?? false)).ToList();
            var correlationId = Guid.NewGuid();
            var ubBaseTransactions = new List<UtilityBillingTransactionBase>();

			if (unappliedPayments != null)
            {
	            foreach (var payment in unappliedPayments)
	            {
		            switch ((BillType) payment.BillType.ToIntegerValue())
		            {
			            case BillType.Sewer:
			            case BillType.Water:
			            case BillType.UtilityBilling:
							var trans = ProcessUtilityPayment(payment, correlationId);
				            ubBaseTransactions.AddRange(trans.ToList());
							payment.AppliedDateTime = DateTime.Today;
				            payment.Processed = true;
				            break;
			            case BillType.RealEstate:
			            case BillType.PersonalPropery:
                            var propertyBillType = (BillType) payment.BillType.ToIntegerValue() == BillType.PersonalPropery ? PropertyTaxBillType.Personal : PropertyTaxBillType.Real;
                            var previousPayments = taxPayments.Where(t =>
					            t.Account == payment.AccountNumber && t.BillType == propertyBillType);
				            var newTaxTransactions = propertyTaxPaymentProcessor
					            .ProcessPayment(payment, previousPayments,correlationId).ToList();
				            if (newTaxTransactions.Any())
				            {
					            taxPayments.AddRange(newTaxTransactions);
				            }
                            payment.AppliedDateTime = DateTime.Today;
                            payment.Processed = true;

							break;
		            }
	            }

				commandDispatcher.Send(new FinalizeTransactions<PropertyTaxTransaction>()
	            {
		            CorrelationIdentifier = correlationId,
		            ReceiptId = 0,
		            TellerId = "",
		            Transactions = taxPayments,
                    AllowUIInteraction = false
	            });

				foreach (var transaction in taxPayments)
				{
					var amountItems = transaction.Payments.ToTransactionAmountItems();
					var lastPayment = transaction.Payments.GetAll().LastOrDefault();
					if (lastPayment != null)
					{
						transaction.Controls.Add(new ControlItem()
						{
							Name = "Control1",
							Description = "Control1",
							Value = lastPayment.Period
						});
						transaction.Controls.Add(new ControlItem()
						{
							Name = "Control2",
							Description = "Control2",
							Value = lastPayment.Code
						});
						
						transaction.OverrideAccount = lastPayment.BudgetaryAccountNumber;
					}
					transaction.AddTransactionAmounts(amountItems);

				}


				if (activeModuleSettings.BudgetaryIsActive && activeModuleSettings.CashReceiptingIsActive)
	            {
		            CreateBudgetaryJournal(ubBaseTransactions, taxPayments);
	            }

	            centralDataContext.SaveChanges();

            }
        }

        private IEnumerable<UtilityBillingTransactionBase> ProcessUtilityPayment(ImportedPayment payment, Guid correlationId)
        {
	        var trans = utilityBillingPaymentProcessor.ProcessPayment(payment,
		        (BillType)payment.BillType.ToIntegerValue() == BillType.Sewer ? UtilityType.Sewer :
		        (BillType)payment.BillType.ToIntegerValue() == BillType.Water ? UtilityType.Water :
		        UtilityType.All);

	        var individualBaseTransactions =
		        ConvertTransactionToUtilityBillingTransactionBase(trans, correlationId);

	        commandDispatcher.Send(new FinalizeTransactions<UtilityBillingTransactionBase>
	        {
		        CorrelationIdentifier = correlationId,
		        ReceiptId = 0,
		        TellerId = "",
		        Transactions = individualBaseTransactions,
				AllowUIInteraction = false
	        });

	        return individualBaseTransactions;
        }

        private void CreateBudgetaryJournal(List<UtilityBillingTransactionBase> ubBaseTransactions, List<PropertyTaxTransaction> taxTransactions)
        {
			var entries = new List<JournalAccountInfo>();
			var budgetaryAccountingService = new BudgetaryAccountingService(budgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);
			var cashCalEntries = new List<JournalAccountInfo>();

			var standardCashAccount = commandDispatcher.Send(new GetStandardCashAccount
			{
				CashAccountType = CashAccountType.MiscellaneousCash
			}).Result;


			foreach (var trans in taxTransactions)
			{
				var receiptType = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
				{
					ReceiptType = GetReceiptTypeCodeForTaxPayment(trans)
				}).FirstOrDefault();

				if (standardCashAccount != "")
				{
					//var defaultCashAccount = budgetaryAccountSettings.BuildLedgerAccount(
					//	budgetaryAccountingService.GetFundFromAccount(receiptType.Account1),
					//	standardCashAccount.ToIntegerValue());

					decimal total = 0;
					foreach (var amount in trans.TransactionAmounts)
					{
						var entry = new JournalAccountInfo();

						switch (amount.Description)
						{
							case "Principal":
								 entry = new JournalAccountInfo
								 {
									BudgetaryAccountNumber = BuildCorrectGLAccount(receiptType.Account1, trans.YearBill, receiptType.Year1 ?? false),
									Amount = amount.Total * -1
								};
								break;
							case "Interest":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = BuildCorrectGLAccount(receiptType.Account2, trans.YearBill, receiptType.Year2 ?? false),
									Amount = amount.Total * -1
								};
								break;
							case "Tax":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = BuildCorrectGLAccount(receiptType.Account5, trans.YearBill, receiptType.Year5 ?? false),
									Amount = amount.Total * -1
								};
								break;
							case "Costs":
							case "Lien Costs":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = BuildCorrectGLAccount(receiptType.Account4, trans.YearBill, receiptType.Year4 ?? false),
									Amount = amount.Total * -1
								};
								break;
							default:
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = BuildCorrectGLAccount(receiptType.Account3, trans.YearBill, receiptType.Year3 ?? false),
									Amount = amount.Total * -1
								};
								break;
						}

						entries.Add(entry);
						if ((receiptType.DefaultAccount ?? "") == "")
						{
							cashCalEntries.Add(entry);
						}

						total = total + amount.Total;
					}

					if ((receiptType.DefaultAccount ?? "") != "")
					{
						entries.Add(new JournalAccountInfo
						{
							BudgetaryAccountNumber = receiptType.DefaultAccount,
							Amount = total
						});
					}
				}
			}

			foreach (var trans in ubBaseTransactions)
			{
				var receiptType = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
				{
					ReceiptType = trans.TransactionTypeCode.ToIntegerValue()
				}).FirstOrDefault();

				if (standardCashAccount != "")
				{
					//var defaultCashAccount = budgetaryAccountSettings.BuildLedgerAccount(
					//	budgetaryAccountingService.GetFundFromAccount(receiptType.Account1),
					//	standardCashAccount.ToIntegerValue());

					decimal total = 0;
					foreach (var amount in trans.TransactionAmounts.Where(x => x.Total != 0))
					{
						var entry = new JournalAccountInfo();

						switch (amount.Description)
						{
							case "Principal":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = receiptType.Account1,
									Amount = amount.Total * -1
								};
								break;
							case "Interest":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = receiptType.Account2,
									Amount = amount.Total * -1
								};
								break;
							case "Tax":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = receiptType.Account5,
									Amount = amount.Total * -1
								};
								break;
							case "Costs":
							case "Lien Costs":
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = receiptType.Account4,
									Amount = amount.Total * -1
								};
								break;
							default:
								entry = new JournalAccountInfo
								{
									BudgetaryAccountNumber = receiptType.Account3,
									Amount = amount.Total * -1
								};
								break;
						}

						entries.Add(entry);
						if ((receiptType.DefaultAccount ?? "") == "")
						{
							cashCalEntries.Add(entry);
						}

						total = total + amount.Total;
					}

					if ((receiptType.DefaultAccount ?? "") != "")
					{
						entries.Add(new JournalAccountInfo
						{
							BudgetaryAccountNumber = receiptType.DefaultAccount,
							Amount = total
						});
					}
				}
			}

			if (cashCalEntries.Count > 0)
			{
				var cashEntries =
					budgetaryAccountingService.CreateCashEntries(cashCalEntries, JournalType.GeneralJournal, CashAccountType.MiscellaneousCash);

				entries.AddRange(cashEntries);
			}
			
			var summaryEntries = entries.GroupBy(x => x.BudgetaryAccountNumber).Select(y => new JournalEntryInfo
			{
				BudgetaryAccountNumber = y.Key,
				Amount = y.Sum(z => z.Amount),
				Description = "PAYPORT " + DateTime.Today.FormatAndPadShortDate(),
				Period = DateTime.Today.Month,
				Date = DateTime.Today,
				Type = "G",
				Rcb = "R",
				Status = "E"
			});

			if (summaryEntries != null && summaryEntries?.Count() > 0)
			{
				budgetaryJournalService.CreateGeneralJournal(DateTime.Today.Month,
					"PAYPORT " + DateTime.Today.FormatAndPadShortDate(), summaryEntries);
			}
        }

        private string BuildCorrectGLAccount(string account, int billYear, bool useYear)
        {
	        if (useYear)
	        {
		        string suffix = billYear.ToString().Substring(2, 2);
		        return account.Left(account.Length - 2) + suffix;
	        }
	        else
	        {
		        return account;
	        }
        }

		private IEnumerable<UtilityBillingTransactionBase> ConvertTransactionToUtilityBillingTransactionBase(
			UtilityBillingPayPortTransaction transaction, Guid correlationId)
		{
			var result = new List<UtilityBillingTransactionBase>();
			foreach (var bill in transaction.Bills.Where(x =>
				x.Pending || x.BillPayments(UtilityType.All).Any(y => y.Pending)))
			{
				foreach (var utility in bill.UtilityDetails.Where(x => x.Payments.Any(y => y.Pending)))
				{
					var controls = new List<ControlItem>();

					controls.Add(new ControlItem
					{
						Name = "Control1",
						Description = "Period",
						Value = "1"
					});

					controls.Add(new ControlItem
					{
						Name = "Control2",
						Description = "Code",
						Value = "P"
					});


					var payment = utility.Payments.Where(x => x.Pending).FirstOrDefault();
					var receiptType = GetReceiptTypeCodeForPayment(bill, utility, transaction.TaxAcquired);

					var cashReceiptsTransaction = new UtilityBillingTransactionBase
					{
						ActualDateTime = (DateTime)payment.ActualSystemDate,
						EffectiveDateTime = (DateTime)payment.EffectiveInterestDate,
						RecordedTransactionDate = (DateTime)payment.RecordedTransactionDate,
						AccountNumber = transaction.Customer.AccountNumber,
						Comment = payment.Comments,
						Controls = controls,
						CorrelationIdentifier = correlationId,
						Id = Guid.NewGuid(),
						PayerName = transaction.Customer.FullNameLF,
						PayerPartyId = transaction.Customer.PartyId,
						AffectCashDrawer = payment.CashDrawer == "Y" ? true : false,
						IncludeInCashDrawerTotal = payment.CashDrawer == "Y" ? true : false,
						AffectCash = payment.GeneralLedger == "Y" ? true : false,
						Code = payment.Code,
						BudgetaryAccountNumber = payment.BudgetaryAccountNumber,
						DefaultCashAccount = "",
						EPmtAccountID = transaction.Id.ToString(),
						Reference = transaction.Customer.AccountNumber + "-" + bill.BillNumber,
						TransactionTypeCode = receiptType.ToString(),
						TransactionDetails = new UtilityBillingTransaction()
						{
							Id = transaction.Id,
							CorrelationId = correlationId,
							AccountNumber = transaction.Customer.AccountNumber,
							Bills = new List<UBBill>
							{
								bill
							}
						},
						TransactionTypeDescription = "",
					};

					cashReceiptsTransaction.TransactionDetails.Bills.FirstOrDefault().UtilityDetails =
					new List<UBBillUtilityDetails>
					{
						utility
					};

					cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
					{
						new TransactionAmountItem
						{
							Name = "Principal",
							Description = "Principal",
							Total = utility.PendingPrincipalAmount - utility.PendingAbatementTotal
						},
						new TransactionAmountItem
						{
							Name = "Interest",
							Description = "Interest",
							Total = utility.PendingCurrentInterestAmount
						},
						new TransactionAmountItem
						{
							Name = bill.IsLien ? "Lien Costs" : "Costs",
							Description = bill.IsLien ? "Lien Costs" : "Costs",
							Total = utility.PendingCostAmount
						},
						new TransactionAmountItem
						{
							Name = "Tax",
							Description = "Tax",
							Total = utility.PendingTaxAmount
						},
						new TransactionAmountItem
						{
							Name = "Abatement",
							Description = "Abatement",
							Total = utility.PendingAbatementTotal
						}
					});

					if (bill.IsLien)
					{
						cashReceiptsTransaction.AddTransactionAmount(
							new TransactionAmountItem
							{
								Name = "Pre Lien Interest",
								Description = "Pre Lien Interest",
								Total = utility.PendingPreLienInterestAmount
							}
						);
					}

					result.Add(cashReceiptsTransaction);
				}
			}

			return result;
		}

		private int GetReceiptTypeCodeForTaxPayment(PropertyTaxTransaction transaction)
		{
			int typeCode;

			if (transaction.IsTaxAcquired)
			{
				if (transaction.IsLien)
				{
					typeCode = 891;
				}
				else
				{
					typeCode = 890;
				}
			}
			else
			{
				if (transaction.BillType == PropertyTaxBillType.Real)
				{
					if (transaction.IsLien)
					{
						typeCode = 91;
					}
					else
					{
						typeCode = 90;
					}
				}
				else
				{
					typeCode = 92;
				}
			}

			return typeCode;
		}

		
		private int GetReceiptTypeCodeForPayment(UBBill bill, UBBillUtilityDetails utility, bool isTaxAcquired)
		{
			int typeCode;

			if (isTaxAcquired)
			{
				if (bill.IsLien)
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 896;
					}
					else
					{
						typeCode = 895;
					}
				}
				else
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 893;
					}
					else
					{
						typeCode = 894;
					}
				}
			}
			else
			{
				if (bill.IsLien)
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 96;
					}
					else
					{
						typeCode = 95;
					}
				}
				else
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 93;
					}
					else
					{
						typeCode = 94;
					}
				}
			}

			return typeCode;
		}
	}

    public static class PaymentExtensions
    {
        public static IEnumerable<ImportedPayment> MapToImportedPayments(this IEnumerable<Payment> payments)
        {
            if (payments == null)
            {
                return new List<ImportedPayment>();
            }

            return payments.Select(payment => new ImportedPayment()
            {
                AccountNumber = payment.AccountNumber,
                BillId = payment.BillId,
                PaidBy = payment.PaidBy,
                PaymentAmount = payment.PaymentAmount,
                PaymentDate = payment.PaymentDate,
                BillNumber = payment.BillNumber,
                BillType = Convert.ToInt32(payment.BillType).ToString(),
                Processed = false,
                AppliedDateTime = null,
                PaymentIdentifier = Guid.NewGuid().ToString()
            });
        }

    }

}
