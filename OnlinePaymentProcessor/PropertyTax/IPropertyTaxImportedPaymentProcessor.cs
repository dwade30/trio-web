﻿using System;
using System.Collections.Generic;
using SharedApplication.CentralData.Models;
using SharedApplication.TaxCollections.Receipting;

namespace OnlinePaymentProcessor.Interfaces
{
    public interface IPropertyTaxImportedPaymentProcessor
    {
         IEnumerable<PropertyTaxTransaction> ProcessPayment(ImportedPayment importedPayment,IEnumerable<PropertyTaxTransaction> previousTransactions, Guid correlationIdentifier);                 
    }
}