﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.Receipting;

namespace OnlinePaymentProcessor
{
    public class PropertyTaxImportedPaymentProcessor : IPropertyTaxImportedPaymentProcessor
    {
        private IPropertyTaxBillQueryHandler billQueryHandler;
        private IPropertyTaxBillCalculator billCalculator;
        private IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler;
        private IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler;
        public PropertyTaxImportedPaymentProcessor(IPropertyTaxBillQueryHandler billQueryHandler, IPropertyTaxBillCalculator billCalculator, IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler, IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler)
        {
            this.billQueryHandler = billQueryHandler;
            this.billCalculator = billCalculator;
            this.reBriefHandler = reBriefHandler;
            this.ppBriefHandler = ppBriefHandler;
        }
        public IEnumerable<PropertyTaxTransaction> ProcessPayment(ImportedPayment importedPayment, IEnumerable<PropertyTaxTransaction> previousTransactions,Guid correlationIdentifier)
        {
            //throw new NotImplementedException();
            PropertyTaxBillType billType;
            switch (importedPayment.BillType.ToIntegerValue())
            {
                case (int)BillType.PersonalPropery:
                    billType = PropertyTaxBillType.Personal;
                    break;
                default:
                    billType = PropertyTaxBillType.Real;
                    break;
            }
           var taxAccount = LoadAccount(billType,importedPayment.AccountNumber,previousTransactions);
           var summaries = CalculateBills(taxAccount, importedPayment.PaymentDate);
           var setupInfo = SetupInfoFromPayment(importedPayment);
           setupInfo.CorrelationIdentifier = correlationIdentifier;
           var payments = AutoCreatePayments(setupInfo, taxAccount, summaries);
           var transactions = new List<PropertyTaxTransaction>();
            foreach (var payment in payments)
            {
                var bill = taxAccount.Bills.FirstOrDefault(b => b.YearBill() == payment.Year.GetValueOrDefault());
                if (bill != null)
                {
                    PropertyTaxTransaction transaction;
                    transaction = transactions.FirstOrDefault(t => t.Id == payment.TransactionIdentifier);
                    if (transaction == null)
                    {
                        transaction = CreateTransactionForBill(bill,correlationIdentifier,taxAccount);
                        transaction.Id = payment.TransactionIdentifier.GetValueOrDefault();
                        transactions.Add(transaction);
                    }

                    if (bill.HasLien())
                    {
                        var lienEvents = bill.Lien.NewEvents.GetEvents().Where(ne => ne.EventType == TaxBillEventType.PropertyTaxLienPaid || ne.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid);
                        foreach (var lienEvent in lienEvents)
                        {
                            if (lienEvent.EventType == TaxBillEventType.PropertyTaxLienPaid)
                            {
                                if (((PropertyTaxLienPaid)lienEvent).PaymentIdentifier == payment.PaymentIdentifier)
                                {
                                    transaction.NewEvents.AddEvent(lienEvent);
                                }
                            }
                            else if (lienEvent.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid)
                            {
                                if (((PropertyTaxLienPartiallyPaid)lienEvent).PaymentIdentifier == payment.PaymentIdentifier)
                                {
                                    transaction.NewEvents.AddEvent(lienEvent);
                                }
                            }
                        }
                    }
                    transaction.EffectiveDateTime = payment.RecordedTransactionDate.GetValueOrDefault();
                    transaction.Payments.AddPayment(payment);
                    transaction.ActualDateTime = DateTime.Now;
                    transaction.Reference = bill.TaxYear.ToString();
                    transaction.IncludeInCashDrawerTotal = payment.CashDrawer == "Y";
                    transaction.AffectCash = payment.GeneralLedger == "Y";
                }
            }            

            return transactions;
        }

        private PaymentSetupInfo SetupInfoFromPayment(ImportedPayment importedPayment)
        {
            var paymentInfo = new PaymentSetupInfo()
            {
                Account = importedPayment.AccountNumber.ToString(),
                Reference = "EPYMT",
                PayCode = PropertyTaxPaymentCode.RegularPayment,
                Principal = importedPayment.PaymentAmount,
                Interest = 0,
                Costs = 0,
                TransactionDate = importedPayment.PaymentDate,
                AffectCash = "Y",
                AffectCashDrawer = "Y",
                YearBill = 0,
                Comment = "",
                PaidBy = importedPayment.PaidBy,
                ExternalPaymentIdentifier = importedPayment.PaymentIdentifier
            };

            return paymentInfo;
        }
        private PropertyTaxAccount LoadAccount(PropertyTaxBillType billType, int accountNumber,IEnumerable<PropertyTaxTransaction> previousTransactions)
        {
            //throw new NotImplementedException();
            switch (billType)
            {
                case PropertyTaxBillType.Personal:
                    return LoadPPAccount(accountNumber, previousTransactions);
                    break;
                case PropertyTaxBillType.Real:
                    return LoadREAccount(accountNumber, previousTransactions);
                    break;
            }

            return null;
        }

        private PersonalPropertyTaxAccount LoadPPAccount(int accountNumber,
            IEnumerable<PropertyTaxTransaction> previousTransactions)
        {
            var bills = billQueryHandler.Find(b => b.Account == accountNumber && b.BillingType == "PP")
                .OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber);
            var accountInfo = ppBriefHandler.ExecuteQuery(new PersonalPropertyAccountBriefQuery(accountNumber));
            var taxAccount = new PersonalPropertyTaxAccount();
            taxAccount.Account = accountNumber;
            taxAccount.FillBriefInfo(accountInfo);
            taxAccount.AddBills(bills);
            foreach (var transaction in previousTransactions)
            {
                taxAccount.AddNewPayments(transaction.Payments);
            }

            return taxAccount;
        }
        private RealEstateTaxAccount LoadREAccount(int accountNumber, IEnumerable<PropertyTaxTransaction> previousTransactions)
        {
           
            var bills = billQueryHandler.Find(b => b.Account == accountNumber && b.BillingType == "RE")
                .OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber);
            var accountInfo = reBriefHandler.ExecuteQuery(new RealEstateAccountBriefQuery() { Account = accountNumber });
            var taxAccount = new RealEstateTaxAccount();
            taxAccount.Account = accountNumber;
            taxAccount.FillBriefInfo(accountInfo);
            taxAccount.AddBills(bills);
            foreach (var transaction in previousTransactions)
            {
                taxAccount.AddNewPayments(transaction.Payments);
            }

            return taxAccount;
        }

        private PropertyTaxAccountCalculationSummaries CalculateBills(PropertyTaxAccount taxAccount,DateTime effectiveDate)
        {
            var accountSummaries = new PropertyTaxAccountCalculationSummaries(taxAccount.Account,taxAccount.BillType);                        
            List<PropertyTaxBill> bills;
            bills = taxAccount.Bills.OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber).ToList();
            foreach (var bill in bills)
            {
                var summary = billCalculator.CalculateBill(bill, effectiveDate);
                accountSummaries.AccountSummary.AddTo(summary);
                accountSummaries.CalculationSummaries.Add(bill.ID, summary);
            }

            return accountSummaries;
        }

        private IEnumerable<PaymentRec> AutoCreatePayments(PaymentSetupInfo paymentInfo,PropertyTaxAccount taxAccount, PropertyTaxAccountCalculationSummaries calculationSummaries)
        {
            var paymentSetup = paymentInfo.GetCopy();
            var payments = new List<PaymentRec>();
            var bills = taxAccount.Bills.Where(b => b.BillSummary.GetNetOwed() > 0).OrderBy(b => b.YearBill()).ToList();
            var totalOwed = calculationSummaries.AccountSummary.Balance;
            decimal amountOver = paymentInfo.GetTotal();
            PropertyTaxBill lastBill;
            lastBill = taxAccount.Bills.Where(b => b.RateId > 0).OrderByDescending(b => b.YearBill()).FirstOrDefault();

            foreach (var bill in bills)
            {
                paymentSetup.TransactionIdentifier = Guid.NewGuid();
                var paymentResult = CreatePendingPayment(bill, paymentSetup, false,calculationSummaries);
                payments.AddRange(paymentResult.Payments);
                amountOver = paymentResult.AmountLeft;
                paymentSetup.Principal = amountOver;
                if (paymentSetup.Principal == 0)
                {
                    break;
                }
            }

            if (amountOver > 0)
            {
                var paymentResult = CreatePendingPayment(lastBill, paymentSetup, true,calculationSummaries);
                payments.AddRange(paymentResult.Payments);
            }

            return payments;
        }

        private (decimal AmountLeft,IEnumerable<PaymentRec> Payments) CreatePendingPayment(PropertyTaxBill bill, PaymentSetupInfo paymentInfo, bool applyExtraAsOverpay,  PropertyTaxAccountCalculationSummaries calculationSummaries)
        {
            decimal amountLeft = 0;
            var paymentRecs = new List<PaymentRec>();
            if (calculationSummaries.CalculationSummaries.ContainsKey(bill.ID))
            {
                PaymentRec paymentRec;
               
                var calcSummary = calculationSummaries.CalculationSummaries[bill.ID];
                if ((paymentInfo.PayCode == PropertyTaxPaymentCode.RegularPayment || paymentInfo.PayCode == PropertyTaxPaymentCode.Correction ||
                     paymentInfo.PayCode == PropertyTaxPaymentCode.NonBudgetary) && calcSummary.Interest > 0 && !paymentInfo.ForcePaymentDistributionAsIs)
                {
                    var chargedInterestPayment = CreateChargedInterestPayment(bill, paymentInfo, calcSummary.Interest);
                    paymentRecs.Add(chargedInterestPayment);
                }
                if (paymentInfo.PayCode == PropertyTaxPaymentCode.RegularPayment && !paymentInfo.ForcePaymentDistributionAsIs)
                {
                    var newInfo = paymentInfo.GetCopy();
                    if (bill.HasLien())
                    {
                        var paidAmounts = MakePaymentInfoForLien(bill.Lien.LienSummary, paymentInfo.Principal);
                        newInfo.Costs = paidAmounts.costs;
                        newInfo.Interest = paidAmounts.interest;
                        newInfo.Principal = paidAmounts.principal;
                        newInfo.PrelienInterest = paidAmounts.preLienInterest;
                       
                        if (paidAmounts.remaining > 0 && applyExtraAsOverpay)
                        {
                            newInfo.Principal += paidAmounts.remaining;
                        }
                        else
                        {
                            amountLeft = paidAmounts.remaining;
                        }
                    }
                    else
                    {
                        var paidAmounts = MakePaymentInfoForBill(bill.BillSummary, paymentInfo.Principal);
                        newInfo.Costs = paidAmounts.costs;
                        newInfo.Interest = paidAmounts.interest;
                        newInfo.Principal = paidAmounts.principal;
                        if (paidAmounts.remaining > 0 && applyExtraAsOverpay)
                        {
                            newInfo.Principal += paidAmounts.remaining;
                        }
                        else
                        {
                            amountLeft = paidAmounts.remaining;
                        }
                        newInfo.PrelienInterest = 0;
                    }

                    paymentRec = CreatePaymentRecForBill(bill, newInfo);                    
                }
                else
                {
                    paymentRec = PaymentFromPaymentInfo(paymentInfo);
                }
                 
                if (paymentRec.Principal != 0 || paymentRec.CurrentInterest != 0 || paymentRec.PreLienInterest != 0 ||
                    paymentRec.LienCost != 0)
                {
                    bill.AddNewPayments(paymentRec.ToPayments());
                    paymentRecs.Add(paymentRec);
                }
            }
            else if (bill.ID == 0 && paymentInfo.PayCode == PropertyTaxPaymentCode.PrePayment)
            {
                var paymentRec = PaymentFromPaymentInfo(paymentInfo);
                if (paymentRec.Principal != 0 || paymentRec.CurrentInterest != 0 || paymentRec.PreLienInterest != 0 ||
                    paymentRec.LienCost != 0)
                {
                    bill.AddNewPayments(paymentRec.ToPayments());
                    paymentRecs.Add(paymentRec);
                }
            }

            return (AmountLeft: amountLeft, Payments: paymentRecs);
        }

        private PaymentRec PaymentFromPaymentInfo(PaymentSetupInfo paymentInfo)
        {
            if (paymentInfo == null)
            {
                return null;
            }
            var payment = new PaymentRec()
            {
                ActualSystemDate = DateTime.Now,
                Account = Convert.ToInt32(paymentInfo.Account),
                BudgetaryAccountNumber = paymentInfo.Account,
                GeneralLedger = paymentInfo.AffectCash,
                CashDrawer = paymentInfo.AffectCashDrawer,
                Code = paymentInfo.PayCode.ToCode(),
                CorrelationIdentifier = paymentInfo.CorrelationIdentifier,
                EffectiveInterestDate = paymentInfo.TransactionDate,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                Principal = paymentInfo.Principal,
                Period = paymentInfo.Period == 0 ? "A" : paymentInfo.Period.ToString(),
                Reference = paymentInfo.Reference,
                CurrentInterest = paymentInfo.Interest,
                PreLienInterest = paymentInfo.PrelienInterest,
                LienCost = paymentInfo.Costs,
                PaymentIdentifier = Guid.NewGuid(),
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                ChargedInterestId = 0,
                Comments = "",
                Teller = "",
                PaidBy = paymentInfo.PaidBy,
                ExternalPaymentIdentifier = paymentInfo.ExternalPaymentIdentifier,
                TransactionIdentifier = paymentInfo.TransactionIdentifier,
                Year = paymentInfo.YearBill
            };
            return payment;
        }

        private PaymentRec CreateChargedInterestPayment(PropertyTaxBill bill, PaymentSetupInfo paymentInfo, decimal interest)
        {
            var paymentRec = new PaymentRec()
            {
                Account = bill.Account.GetValueOrDefault(),
                ActualSystemDate = DateTime.Now,
                BillCode = bill.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                BillId = bill.HasLien() ? bill.LienID : bill.ID,
                BudgetaryAccountNumber = paymentInfo.Account,
                CashDrawer = paymentInfo.AffectCashDrawer,
                ChargedInterestdate = paymentInfo.TransactionDate,
                ChargedInterestId = 0,
                Code = PropertyTaxPaymentCode.Interest.ToCode(),
                Reference = "CHGINT",
                PaymentIdentifier = Guid.NewGuid(),
                CorrelationIdentifier = paymentInfo.CorrelationIdentifier,
                //ChargedInterestIdentifier = null,
                Comments = "",
                CurrentInterest = -1 * interest,
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                DailyCloseOut = 0,
                EffectiveInterestDate = paymentInfo.TransactionDate,
                ExternalPaymentIdentifier = "",
                GeneralLedger = "Y",
                IsReversal = false,
                LienCost = 0,
                PaidBy = "",
                Period = "1",
                PreLienInterest = 0,
                Principal = 0,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                SetEidate = new DateTime(1899, 12, 30),
                Teller = "",
                Year = bill.YearBill(),
                TransactionIdentifier = paymentInfo.TransactionIdentifier

            };

            if (bill.HasLien())
            {
                paymentRec.BillCode = "L";
                paymentRec.OldInterestDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.PreviousInterestAppliedDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
            }
            else
            {
                paymentRec.OldInterestDate = bill.InterestAppliedThroughDate;
                bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                bill.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
            }
            bill.AddNewPayments(paymentRec.ToPayments());
            return paymentRec;
        }

        private PropertyTaxTransaction CreateTransactionForBill(PropertyTaxBill bill,Guid correlationIdentifier,PropertyTaxAccount taxAccount)
        {
            var transaction = new PropertyTaxTransaction()
            {
                Account = bill.Account.GetValueOrDefault(),
                Id = Guid.NewGuid(),
                BillType = bill.BillType,
                BillId = bill.ID,
                CorrelationIdentifier = correlationIdentifier,
                IsLien = bill.HasLien(),
                TownId = taxAccount.TownId,
                ActualDateTime = DateTime.Now,
                YearBill = bill.YearBill(),
            };

            transaction.Controls.Add(new ControlItem()
            {
                Name = "Control3",
                Description = "Control3",
                Value = (taxAccount.LocationNumber().ToString() + " " + taxAccount.LocationStreet()).Trim()
            });

            if (transaction.BillType == PropertyTaxBillType.Personal)
            {
                var accountInfo = ppBriefHandler.ExecuteQuery(new PersonalPropertyAccountBriefQuery(taxAccount.Account));
                transaction.PayerPartyId = accountInfo.PartyId;
                transaction.PayerName = accountInfo.Name;
            }
            else
            {
                var accountInfo = reBriefHandler.ExecuteQuery(new RealEstateAccountBriefQuery() { Account = taxAccount.Account });
                transaction.PayerPartyId = accountInfo.PartyId1;
                transaction.PayerName = taxAccount.OwnerNames();
            }

            if (bill.BillType == PropertyTaxBillType.Real)
            {
                transaction.IsTaxAcquired = ((RealEstateTaxAccount)taxAccount).AccountBriefInfo.IsTaxAcquired;
            }

            if (bill.BillType == PropertyTaxBillType.Personal)
            {
                // "92";
                transaction.TransactionTypeCode = "PPTAX";
            }
            else
            {
                if (bill.HasLien())
                {
                    if (transaction.IsTaxAcquired)
                    {
                        //"891";
                        transaction.TransactionTypeCode = "TARETAXLIEN";
                    }
                    else
                    {
                        // "91";
                        transaction.TransactionTypeCode = "RETAXLIEN";
                    }
                }
                else
                {
                    if (transaction.IsTaxAcquired)
                    {
                        //"890";
                        transaction.TransactionTypeCode = "TARETAX";
                    }
                    else
                    {
                        //"90";
                        transaction.TransactionTypeCode = "RETAX";
                    }
                }
            }
            return transaction;
        }

        private (decimal principal, decimal preLienInterest, decimal interest, decimal costs, decimal remaining) MakePaymentInfoForLien(PropertyTaxLienSummary lienSummary, decimal amountToPay)
        {
            var amountLeft = amountToPay;
            decimal paidInterest = 0;
            decimal paidPrincipal = 0;
            decimal paidCosts = 0;
            decimal paidPreLienInterest = 0;
            if (amountLeft >= lienSummary.GetNetOwed())
            {
                paidPreLienInterest = lienSummary.GetNetPreLienInterest();
                paidCosts = lienSummary.GetNetCosts();
                paidInterest = lienSummary.GetNetPostLienInterest();
                paidPrincipal = lienSummary.GetNetPrincipal();
                amountLeft -= paidPreLienInterest;
                amountLeft -= paidCosts;
                amountLeft -= paidInterest;
                amountLeft -= paidPrincipal;
            }
            else
            {
                if (lienSummary.GetNetPreLienInterest() > 0)
                {
                    if (amountLeft > lienSummary.GetNetPreLienInterest())
                    {
                        paidPreLienInterest = lienSummary.GetNetPreLienInterest();
                        amountLeft -= paidPreLienInterest;
                    }
                    else
                    {
                        paidPreLienInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (lienSummary.GetNetPostLienInterest() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetPostLienInterest())
                    {
                        paidInterest = lienSummary.GetNetPostLienInterest();
                        amountLeft -= paidInterest;
                    }
                    else
                    {
                        paidInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (lienSummary.GetNetCosts() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetCosts())
                    {
                        paidCosts = lienSummary.GetNetCosts();
                        amountLeft -= paidCosts;
                    }
                    else
                    {
                        paidCosts = amountLeft;
                        amountLeft = 0;

                    }
                }

                if (lienSummary.GetNetPrincipal() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetPrincipal())
                    {
                        paidPrincipal = lienSummary.GetNetPrincipal();
                        amountLeft -= paidPrincipal;
                    }
                    else
                    {
                        paidPrincipal = amountLeft;
                        amountLeft = 0;
                    }
                }
            }
            return (principal: paidPrincipal, preLienInterest: paidPreLienInterest, interest: paidInterest, costs: paidCosts, remaining: amountLeft);
        }

        private (decimal principal, decimal interest, decimal costs, decimal remaining) MakePaymentInfoForBill(PropertyTaxBillSummary billSummary, decimal amountToPay)
        {
            var amountLeft = amountToPay;
            decimal paidInterest = 0;
            decimal paidPrincipal = 0;
            decimal paidCosts = 0;
            if (amountToPay >= billSummary.GetNetOwed())
            {
                paidPrincipal = billSummary.GetNetPrincipal();
                paidCosts = billSummary.GetNetCosts();
                paidInterest = billSummary.GetNetInterest();
                amountLeft = amountToPay - billSummary.GetNetOwed();
            }
            else
            {
                if (billSummary.GetNetInterest() > 0)
                {
                    if (amountLeft > billSummary.GetNetInterest())
                    {
                        paidInterest = billSummary.GetNetInterest();
                        amountLeft -= paidInterest;
                    }
                    else
                    {
                        paidInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (billSummary.GetNetCosts() > 0 && amountLeft > 0)
                {
                    if (amountLeft > billSummary.GetNetCosts())
                    {
                        paidCosts = billSummary.GetNetCosts();
                        amountLeft -= paidCosts;
                    }
                    else
                    {
                        paidCosts = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (billSummary.GetNetPrincipal() > 0 && amountLeft > 0)
                {
                    if (amountLeft > billSummary.GetNetPrincipal())
                    {
                        paidPrincipal = billSummary.GetNetPrincipal();
                        amountLeft -= paidPrincipal;
                    }
                    else
                    {
                        paidPrincipal = amountLeft;
                        amountLeft = 0;
                    }
                }

            }

            return (principal: paidPrincipal, interest: paidInterest, costs: paidCosts, remaining: amountLeft);
        }

        private PaymentRec CreatePaymentRecForBill(PropertyTaxBill bill, PaymentSetupInfo paymentInfo)
        {
            var paymentRec = new PaymentRec()
            {
                Account = bill.Account.GetValueOrDefault(),
                ActualSystemDate = DateTime.Now,
                BillCode = bill.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                BillId = bill.HasLien() ? bill.LienID : bill.ID,
                BudgetaryAccountNumber = paymentInfo.Account,
                CashDrawer = paymentInfo.AffectCashDrawer,
                ChargedInterestdate = paymentInfo.TransactionDate,
                ChargedInterestId = 0,
                Code = paymentInfo.PayCode.ToCode(),
                Reference = paymentInfo.Reference,
                PaymentIdentifier = Guid.NewGuid(),
                CorrelationIdentifier = paymentInfo.CorrelationIdentifier,
                //ChargedInterestIdentifier = null,
                Comments = paymentInfo.Comment,
                CurrentInterest = paymentInfo.Interest,
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                DailyCloseOut = 0,
                EffectiveInterestDate = paymentInfo.TransactionDate,
                ExternalPaymentIdentifier = paymentInfo.ExternalPaymentIdentifier,
                GeneralLedger = paymentInfo.AffectCash,
                IsReversal = paymentInfo.PayCode == PropertyTaxPaymentCode.Correction && paymentInfo.Reference == "REVERSE",
                LienCost = paymentInfo.Costs,
                PaidBy = paymentInfo.PaidBy,
                Period = paymentInfo.Period == 0 ? "A" : paymentInfo.Period.ToString(),
                PreLienInterest = paymentInfo.PrelienInterest,
                Principal = paymentInfo.Principal,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                SetEidate = new DateTime(1899, 12, 30),
                Teller = "",
                Year = bill.YearBill(),
                TransactionNumber = "",
                TransactionIdentifier = paymentInfo.TransactionIdentifier
            };

            if (bill.HasLien())
            {
                paymentRec.BillCode = "L";
                paymentRec.OldInterestDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.PreviousInterestAppliedDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
                // bill.Lien.AddNewPayments(paymentRec.ToPayments());
            }
            else
            {
                paymentRec.OldInterestDate = bill.InterestAppliedThroughDate;
                bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                bill.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
                // bill.AddNewPayments(paymentRec.ToPayments());
            }

            return paymentRec;
        }

    }
}