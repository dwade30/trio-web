﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.EntityFrameworkCore;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess;

namespace OnlinePaymentProcessor.PersonalProperty
{
    public class PPSQLDataLayer : IDataLayer<PPBillRecord>
    {
	    private int daysPerYear = 0;
	    private Autofac.IContainer diContainer;
	    private ITaxCollectionsContext collectionsContext;
	    private IPersonalPropertyContext personalPropertyContext;

	    public PPSQLDataLayer(Autofac.IContainer diContainer)
	    {
		    this.diContainer = diContainer;

		    var factory = diContainer.Resolve<ITrioContextFactory>();

		    collectionsContext = factory.GetTaxCollectionsContext();
		    personalPropertyContext = factory.GetPersonalPropertyContext();

		    var settings = collectionsContext.CollectionSettings.FirstOrDefault();

		    if (settings != null)
		    {
			    daysPerYear = (settings.DaysInYear ?? 0) == 0 ? 365 : settings.DaysInYear ?? 0;
		    }
		    else
		    {
			    daysPerYear = 365;
		    }
	    }

        public IEnumerable<Account> GetAccounts()
        {
	        var accounts = personalPropertyContext.PPAccountPartyAddressViews
		        .OrderBy(y => y.Account)
		        .Select(z => new Account
		        {
			        AccountType = AccountType.PersonalProperty,
			        AccountNumber = z.Account ?? 0,
			        LocationNumber = z.StreetNumber == null ? "" : z.StreetNumber.ToString(),
			        LocationStreet = z.Street ?? "",
			        MapLot = "",
			        Owner1FName = z.FullNameLastFirst(),
			        Owner1LName = "",
			        Owner2FName = "",
			        Owner2LName = "",
			        OwnerAddress1 = z.Address1 ?? "",
			        OwnerAddress2 = z.Address2 ?? "",
			        OwnerAddress3 = z.Address3 ?? "",
			        OwnerCity = z.City ?? "",
			        OwnerState = z.State ?? "",
			        OwnerCountry = z.Country ?? "",
			        OwnerZip = z.Zip ?? "",
			        Tenant1FName = "",
			        Tenant1LName = "",
			        Tenant2FName = "",
			        Tenant2LName = "",
			        TenantAddress1 = "",
			        TenantAddress2 = "",
			        TenantAddress3 = "",
			        TenantCity = "",
			        TenantState = "",
			        TenantCountry = "",
			        TenantZip = ""
		        });


	        return accounts.ToList();
        }

        public IEnumerable<PPBillRecord> GetUnpaidBillsForAccount(int accountNumber)
        {
            var bills = GetUnpaidBillRecordsForAccount(accountNumber);
            var ppBills = MapToPPBillRecords(ref bills).ToList();
            return ppBills;
        }

        private IEnumerable<PPBillRecord> MapToPPBillRecords(ref IEnumerable<BillingMaster> bills)
        {
            return bills.Select(bm => CreateBill(BillType.PersonalPropery, bm));
        }

        private PPBillRecord CreateBill(BillType btype, BillingMaster brec)
        {
            var _b = new PPBillRecord();

            _b.BillType = btype;
            _b.BillKey = brec.ID;
            _b.AccountNumber = brec.Account ?? 0;
            int tmpBillNo = brec.BillingYear ?? 0;
            _b.BillNumber = tmpBillNo == 0 ? "" : tmpBillNo.ToString("0000-0");

            _b.Owner1FName = "";
            _b.Owner1LName = brec.Name1 ?? "";
            _b.Owner2FName = "";
            _b.Owner2LName = brec.Name2 ?? "";
            _b.OwnerAddress1 = brec.Address1 ?? "";
            _b.OwnerAddress2 = brec.Address2 ?? "";
            _b.OwnerAddress3 = "";
            _b.OwnerCity = brec.Address3 ?? "";
            _b.OwnerState = brec.Address3 ?? "";
            _b.OwnerZip = brec.Zip ?? "";
            _b.OwnerCountry = String.Empty;
            _b.Tenant1FName = String.Empty;
            _b.Tenant1LName = String.Empty;
            _b.Tenant2FName = String.Empty;
            _b.Tenant2LName = String.Empty;
            _b.TenantAddress1 = String.Empty;
            _b.TenantAddress2 = String.Empty;
            _b.TenantAddress3 = String.Empty;
            _b.TenantCity = String.Empty;
            _b.TenantState = String.Empty;
            _b.TenantZip = String.Empty;
            _b.TenantCountry = String.Empty;

            _b.IsLien = false;
            _b.PrincipalPaid = brec.PrincipalPaid ?? 0;
            _b.CostBilled = 0m;
            _b.CostAdded = brec.DemandFees ?? 0;
            _b.CostPaid = brec.DemandFeesPaid ?? 0;
            _b.InterestBilled = 0m;
            _b.InterestAdded = (brec.InterestCharged ?? 0) * -1m;
            _b.InterestPaid = brec.InterestPaid ?? 0;
            _b.SalesTaxBilled = 0m;
            _b.SalesTaxPaid = 0;
            _b.InterestPaidDate = brec.InterestAppliedThroughDate ?? DateTime.MinValue;
            _b.InterestRate = (brec.RateRec.InterestRate ?? 0).ToString().ToDecimalValue();
            _b.DaysPerYear = daysPerYear;

            _b.Periods.AddRange(CreatePeriods(ref brec));

            return _b;
        }

        private class Abatement
        {
            public decimal Amount { get; set; }
            public int Period { get; set; }
        }

        private IEnumerable<Period> CreatePeriods(ref BillingMaster brec)
        {
            var billId = brec.ID;
            var numberOfPeriods = brec.RateRec.NumberOfPeriods.ToString().ToIntegerValue();
            var periods = new List<Period>();
            decimal prinOffsetAmount = brec.PrincipalPaid ?? 0;
            var abatements = GetAbatementsForBill(billId, numberOfPeriods);
            prinOffsetAmount = Math.Round(prinOffsetAmount - abatements.Sum(a => a.Amount), 2);


            for (int i = 1; i <= numberOfPeriods; i++)
            {
                Period _p = new Period();

                switch (i)
                {
                    case 1:
                        _p.DueDate = (DateTime)brec.RateRec.DueDate1;
                        _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate1;
                        _p.PrincipalAmount = brec.TaxDue1 ?? 0;
                        break;
                    case 2:
                        _p.DueDate = (DateTime)brec.RateRec.DueDate2;
                        _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate2;
                        _p.PrincipalAmount = brec.TaxDue2 ?? 0;
                        break;
                    case 3:
                        _p.DueDate = (DateTime)brec.RateRec.DueDate3;
                        _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate3;
                        _p.PrincipalAmount = brec.TaxDue3 ?? 0;
                        break;
                    case 4:
                        _p.DueDate = (DateTime)brec.RateRec.DueDate4;
                        _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate4;
                        _p.PrincipalAmount = brec.TaxDue4 ?? 0;
                        break;
                }

                decimal periodAbateAmount = abatements.Where(a => a.Period == i).Sum(a => a.Amount);

                if (prinOffsetAmount > _p.PrincipalAmount - periodAbateAmount)
                {
                    _p.PrincipalDue = 0m;
                    prinOffsetAmount -= _p.PrincipalAmount - periodAbateAmount;
                }
                else
                {
                    _p.PrincipalDue = _p.PrincipalAmount - periodAbateAmount - prinOffsetAmount;
                    prinOffsetAmount = 0m;
                }
                periods.Add(_p);
            }
            return periods;
        }

        private IEnumerable<Abatement> GetAbatementsForBill(int billKey, int numPeriods)
        {
	        List<Abatement> abatements = new List<Abatement>();

	        var abateRecords = collectionsContext.PaymentRecords
		        .Where(x => x.BillCode == "P" && x.Code == "A" && x.BillId == billKey);

	        // Abatements to a specific period
	        abatements = abateRecords
		        .Where(a => a.Period != "A")
		        .Select(a =>
			        new Abatement
			        {
				        Period = a.Period.ToIntegerValue(),
				        Amount = a.Principal ?? 0
			        }).ToList();

	        // Split abatements with Period = Auto
	        foreach (PaymentRec pr in abateRecords.Where(a => a.Period == "A"))
	        {
		        decimal abateAmt = pr.Principal ?? 0;
		        abatements.AddRange(SplitAbatement(abateAmt, numPeriods));
	        }

	        return abatements;
        }

        /// Return a list of Abatement that splits the abatement amount evenly between the number of periods
        private IEnumerable<Abatement> SplitAbatement(decimal abateAmount, int numPeriods)
        {
            List<Abatement> abatements = new List<Abatement>();
            decimal remainder = abateAmount % (numPeriods / 100m);
            decimal splitAmount = Math.Round((abateAmount - remainder) / numPeriods, 2);

            for (int i = numPeriods; i > 1; i--)
            {
                abatements.Add(new Abatement { Amount = splitAmount, Period = i });
            }
            abatements.Add(new Abatement { Amount = Math.Round(abateAmount - (splitAmount * (numPeriods - 1)), 2), Period = 1 });
            return abatements;
        }

        private IEnumerable<BillingMaster> GetUnpaidBillRecordsForAccount(int accountNumber)
        {
	        var bills = collectionsContext.BillingMasters
		        .Where(x => x.BillingType == "PP" && x.Account == accountNumber && x.RateRecId > 0 &&
		                    (x.TaxDue1 + x.TaxDue2 + x.TaxDue3 + x.TaxDue4 + x.DemandFees - x.InterestCharged) > (x.PrincipalPaid + x.InterestPaid + x.DemandFeesPaid))
		        .Include(z => z.RateRec)
		        .OrderBy(y => y.BillingYear);

	        return bills;
        }
    }
}
