﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.UtilityBilling.Receipting;

namespace OnlinePaymentProcessor.Models
{
	public class BillUtilityInfo
	{
		public int BillId { get; set; }
		public int BillNumber { get; set; }
		public DateTime DueDate { get; set; }
		public IEnumerable<UBBillUtilityDetails> UtilityInfo { get; set; }
	}
}
