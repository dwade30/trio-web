﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class UtilityAmount
	{
		public Decimal PrincipalPaid { get; set; }
		public Decimal CostBilled { get; set; }
		public Decimal CostAdded { get; set; }
		public Decimal CostPaid { get; set; }
		public Decimal InterestBilled { get; set; }
		public Decimal InterestAdded { get; set; }
		public Decimal InterestPaid { get; set; }
		public Decimal SalesTaxBilled { get; set; }
		public Decimal SalesTaxPaid { get; set; }
		public DateTime InterestPaidDate { get; set; }
		public Decimal InterestRate { get; set; }
		public Decimal PrincipalOwed { get; set; }

		public Decimal PrincipalBalance => PrincipalOwed - PrincipalPaid;
		public Decimal SalesTaxBalance => SalesTaxBilled - SalesTaxPaid;
		public Decimal InterestBalance => InterestBilled + InterestAdded - InterestPaid;
		public Decimal CostBalance => CostBilled + CostAdded - CostPaid;
		public UtilityAmount CombineWith(UtilityAmount toCombineWith)
		{
			var utAmount = new UtilityAmount();
			utAmount.CostAdded = CostAdded + toCombineWith.CostAdded;
			utAmount.CostBilled = CostBilled + toCombineWith.CostBilled;
			utAmount.CostPaid = CostPaid + toCombineWith.CostPaid;
			utAmount.InterestAdded = InterestAdded + toCombineWith.InterestAdded;
			utAmount.InterestBilled = InterestBilled + toCombineWith.InterestBilled;
			utAmount.InterestPaid = InterestPaid + toCombineWith.InterestPaid;
			utAmount.PrincipalOwed = PrincipalOwed + toCombineWith.PrincipalOwed;
			utAmount.PrincipalPaid = PrincipalPaid + toCombineWith.PrincipalPaid;
			utAmount.SalesTaxBilled = SalesTaxBilled + toCombineWith.SalesTaxBilled;
			utAmount.SalesTaxPaid = SalesTaxPaid + toCombineWith.SalesTaxPaid;
			return utAmount;
		}
	}
}
