﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;

namespace OnlinePaymentProcessor.Models
{
	public class Account
	{
		public AccountType AccountType { get; set; }
		public int AccountNumber { get; set; }
		public string LocationNumber { get; set; }
		public string LocationStreet { get; set; }
		public string MapLot { get; set; }
		public string Owner1FName { get; set; }
		public string Owner1LName { get; set; }
		public string Owner2FName { get; set; }
		public string Owner2LName { get; set; }
		public string OwnerAddress1 { get; set; }
		public string OwnerAddress2 { get; set; }
		public string OwnerAddress3 { get; set; }
		public string OwnerCity { get; set; }
		public string OwnerState { get; set; }
		public string OwnerCountry { get; set; }
		public string OwnerZip { get; set; }
		public string Tenant1FName { get; set; }
		public string Tenant1LName { get; set; }
		public string Tenant2FName { get; set; }
		public string Tenant2LName { get; set; }
		public string TenantAddress1 { get; set; }
		public string TenantAddress2 { get; set; }
		public string TenantAddress3 { get; set; }
		public string TenantCity { get; set; }
		public string TenantState { get; set; }
		public string TenantCountry { get; set; }
		public string TenantZip { get; set; }
		public DateTime ConveyDate { get; set; }
	}
}
