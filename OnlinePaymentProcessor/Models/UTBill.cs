﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class UTBill : Bill
	{
		public decimal WaterPrincipal { get; set; }
		public DateTime WaterInterestPaidDate { get; set; }
		public decimal WaterInterestRate { get; set; }
		public decimal SewerPrincipal { get; set; }
		public DateTime SewerInterestPaidDate { get; set; }
		public decimal SewerInterestRate { get; set; }

		protected override decimal CalcCurrentInterest()
		{
			decimal CurInt = 0m;

			foreach (UTPeriod p in Periods)
			{
				DateTime WaterInterestStartDate = GetInterestAccrualStartDate(p.InterestDate, WaterInterestPaidDate);
				DateTime SewerInterestStartDate = GetInterestAccrualStartDate(p.InterestDate, SewerInterestPaidDate);
				CurInt += CalculateInterest(p.WaterPrincipalDue, WaterInterestRate, WaterInterestStartDate, DateTime.Now, DaysPerYear) +
				          CalculateInterest(p.SewerPrincipalDue, SewerInterestRate, SewerInterestStartDate, DateTime.Now, DaysPerYear);
			}
			return CurInt;
		}
	}
}
