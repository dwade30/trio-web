﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;

namespace OnlinePaymentProcessor.Models
{
	public class Payment
	{
		public int BillId { get; set; }
		public BillType BillType { get; set; }
		public string BillNumber { get; set; }
		public int AccountNumber { get; set; }
		public DateTime PaymentDate { get; set; }
		public decimal PaymentAmount { get; set; }
		public string PaidBy { get; set; }
	}
}
