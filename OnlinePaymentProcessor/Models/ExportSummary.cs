﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class ExportSummary
	{
		public string GEOCode { get; set; }
		public DateTime FileDate { get; set; }
		public int BillCount { get; set; }
		public decimal TotalAmount { get; set; }

		// Write the header - Will we create a zero invoice file just to have the record of it?
		// Record Type  - H
		// GEO Cod      - 5 digit number
		// File Date    - MMDDYYYY 
		// Bill Count   - 5 digits (Is that enough? 99999 Bills)
		// Total Amount - 11 (9.2) 
		// H12345MMDDYYYYNNNNNXXXXXXXXXXX
		public string BuildHeaderLine()
		{
			string retVal = "";
			using (StringWriter dl = new StringWriter())
			{
				dl.Write("H");
				dl.Write(GEOCode);
				dl.Write(FileDate.ToString("MMddyyyy"));
				dl.Write(BillCount.ToString("00000"));
				dl.Write((TotalAmount * 100).ToString("00000000000"));
				retVal = dl.ToString();
			}

			return retVal;
		}
	}
}
