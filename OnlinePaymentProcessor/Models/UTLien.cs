﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class UTLien
	{
		public int ID { get; set; }
		public DateTime? InterestPaidDate { get; set; }
		public decimal Principal { get; set; }
		public decimal PrincipalPaid { get; set; }
		public decimal Costs { get; set; }
		public decimal MaturityFee { get; set; }
		public decimal CostsPaid { get; set; }
		public decimal Interest { get; set; }
		public decimal InterestAdded { get; set; }
		public decimal InterestPaid { get; set; }
		public decimal PreLienInterestPaid { get; set; }
		public decimal Tax { get; set; }
		public decimal TaxPaid { get; set; }
		public DateTime? InterestStartDate { get; set; }
		public DateTime? DueDate { get; set; }
		public decimal WaterInterestRate { get; set; }
		public decimal SewerInterestRate { get; set; }

		public decimal PrincipalDue => Principal - PrincipalPaid;

		public decimal TotalDue => Principal - PrincipalPaid + Interest - PreLienInterestPaid - InterestAdded -
			InterestPaid + Costs - CostsPaid - MaturityFee;
	}
}
