﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
    public class UTBillRecord
    {
        public int ID { get; set; }
        public int AccountNumber { get; set; }
        public int BillNumber { get; set; }
        public int DaysPerYear { get; set; }
        public string ServiceCode { get; set; }
        public string OwnerName { get; set; }
        public string OwnerName2 { get; set; }
        public string OwnerAddress1 { get; set; }
        public string OwnerAddress2 { get; set; }
        public string OwnerAddress3 { get; set; }
        public string OwnerCity { get; set; }
        public string OwnerState { get; set; }
        public string OwnerZip { get; set; }
        public string OwnerZip4 { get; set; }
        public string TenantName { get; set; }
        public string TenantName2 { get; set; }
        public string TenantAddress1 { get; set; }
        public string TenantAddress2 { get; set; }
        public string TenantAddress3 { get; set; }
        public string TenantCity { get; set; }
        public string TenantState { get; set; }
        public string TenantZip { get; set; }
        public string TenantZip4 { get; set; }
        public int SewerLienID { get; set; }
        public UTLien SewerLien { get; set; }
        public String SewerCombinationLienKey { get; set; }
        public DateTime? SewerInterestPaidDate { get; set; }
        public UtilityAmount SewerUtilityAmount { get; set; }
        //public decimal SewerPrincipalOwed { get; set; }
        //public decimal SewerPrincipalPaid { get; set; }
        //public decimal SewerCostOwed { get; set; }
        //public decimal SewerCostAdded { get; set; }
        //public decimal SewerCostPaid { get; set; }
        //public decimal SewerInterestOwed { get; set; }
        //public decimal SewerInterestAdded { get; set; }
        //public decimal SewerInterestPaid { get; set; }
        //public decimal SewerTaxOwed { get; set; }
        //public decimal SewerTaxPaid { get; set; }
        public int WaterLienID { get; set; }
        public UTLien WaterLien { get; set; }
        public DateTime? WaterInterestPaidDate { get; set; }
        //public decimal WaterPrincipalOwed { get; set; }
        //public decimal WaterPrincipalPaid { get; set; }
        //public decimal WaterCostOwed { get; set; }
        //public decimal WaterCostAdded { get; set; }
        //public decimal WaterCostPaid { get; set; }
        //public decimal WaterInterestOwed { get; set; }
        //public decimal WaterInterestAdded { get; set; }
        //public decimal WaterInterestPaid { get; set; }
        //public decimal WaterTaxOwed { get; set; }
        //public decimal WaterTaxPaid { get; set; }
        public UtilityAmount WaterUtilityAmount { get; set; }
        public decimal SewerInterestRate { get; set; }
        public decimal WaterInterestRate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? InterestStartDate { get; set; }
        public bool IsSewerLien => SewerLienID > 0;
        public bool IsWaterLien => WaterLienID > 0;
        public decimal SewerPrincipalBalance => SewerUtilityAmount.PrincipalBalance;
        public decimal WaterPrincipalBalance => WaterUtilityAmount.PrincipalBalance;

        public decimal TotalPrincipalBalance => SewerPrincipalBalance + WaterPrincipalBalance;

    }
}
