﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class BillException
	{
		public int Account { get; set; } = 0;
		public string Reason { get; set; } = "";
		public string BillIdentifier { get; set; } = "";
		public string BillType { get; set; } = "";
	}
}
