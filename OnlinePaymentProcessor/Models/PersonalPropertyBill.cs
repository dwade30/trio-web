﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;

namespace OnlinePaymentProcessor.Models
{
	public class PersonalPropertyBill : Bill
	{
		public PersonalPropertyBill()
		{
			base.BillType = BillType.PersonalPropery;
		}

	}
}
