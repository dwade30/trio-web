﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class Period
	{
		public DateTime DueDate { get; set; }
		public DateTime InterestDate { get; set; }
		public decimal PrincipalAmount { get; set; }
		public decimal PrincipalDue { get; set; }
	}
}
