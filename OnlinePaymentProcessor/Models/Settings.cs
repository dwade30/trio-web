﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
	public class Settings
	{
		public bool CombineUtilityServices { get; set; }
		public string CombinedUtilityType { get; set; } = "Utility";
		public bool SyncRealEstate { get; set; }
		public bool SyncPersonalProperty { get; set; }
		public bool SyncUtilityBilling { get; set; }
		public string GeoCode { get; set; } = "";
		public string DataPath { get; set; } = "";
		public string PayportPath { get; set; } = "";
		public string LogPathFileName { get; set; } = "";
		public string DownLoadURL { get; set; } = "";
		public string UploadURL { get; set; } = "";
		public string Username { get; set; } = "";
		public string Password { get; set; } = "";
		public bool TestMode { get; set; } = false;
	}
}
