﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using SharedApplication.Extensions;

namespace OnlinePaymentProcessor.Models
{
    public class Bill
    {
        public Bill()
        {
            Periods = new List<Period>();
        }

        public int DaysPerYear { get; set; }

        public BillType BillType { get; set; }
        public int AccountNumber { get; set; }
        public string BillNumber { get; set; }
        public int BillKey { get; set; }
        public bool IsLien { get; set; }
        public DateTime InterestPaidDate { get; set; }
        public decimal InterestRate { get; set; }

        public decimal TotalPrincipalBilled
        {
            get { return (Periods.Sum(x => x.PrincipalAmount)); }
        }
        public decimal TotalCostBilled
        {
            get { return (CostBilled); }
        }
        public decimal TotalInterestBilled
        {
            get { return (InterestBilled); }
        }
        public decimal TotalSalesTaxBilled
        {
            get { return (SalesTaxBilled); }
        }
        public decimal TotalPrincipalDue
        {
            get { return (TotalPrincipalBilled - PrincipalPaid); }
        }
        public decimal TotalCostDue
        {
            get { return (CostBilled + CostAdded - CostPaid); }
        }
        public decimal TotalSalesTaxDue
        {
            get { return (SalesTaxBilled - SalesTaxPaid); }
        }
        public decimal TotalInterestDue
        {
            get { return (InterestBilled + InterestAdded + CalcCurrentInterest() - InterestPaid); }
        }

        public decimal TotalAmountDue
        {
            get { return (TotalPrincipalDue + TotalInterestDue + TotalCostDue + TotalSalesTaxDue); }
        }

        public decimal PrincipalPaid { get; set; }
        public decimal SalesTaxBilled { get; set; }
        public decimal SalesTaxPaid { get; set; }
        public decimal InterestBilled { get; set; }
        public decimal InterestAdded { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal CostBilled { get; set; }
        public decimal CostAdded { get; set; }
        public decimal CostPaid { get; set; }

        public decimal DiscountAmount { get; set; }

        public List<Period> Periods { get; set; }

        //REPEAT THESE IN THE BILL TO KNOW IF NEW OWNERS OR PARSE OUT IN GetBills methods
        public string Owner1FName { get; set; }
        public string Owner1LName { get; set; }
        public string Owner2FName { get; set; }
        public string Owner2LName { get; set; }
        public string OwnerAddress1 { get; set; }
        public string OwnerAddress2 { get; set; }
        public string OwnerAddress3 { get; set; }
        public string OwnerCity { get; set; }
        public string OwnerState { get; set; }
        public string OwnerCountry { get; set; }
        public string OwnerZip { get; set; }

        public string Tenant1FName { get; set; }
        public string Tenant1LName { get; set; }
        public string Tenant2FName { get; set; }
        public string Tenant2LName { get; set; }
        public string TenantAddress1 { get; set; }
        public string TenantAddress2 { get; set; }
        public string TenantAddress3 { get; set; }
        public string TenantCity { get; set; }
        public string TenantState { get; set; }
        public string TenantCountry { get; set; }
        public string TenantZip { get; set; }

        public static decimal CalculateInterest(decimal PrinAmt, decimal AnnualRate, DateTime StartDate, DateTime AsOfDate, int DaysPerYear)
        {
            if (PrinAmt == 0 || AnnualRate == 0) return 0;

            // What else do we need to add?

            double PeriodDays = AsOfDate.Subtract(StartDate).Days;
            if (PeriodDays <= 0) return 0;

            double PerDiem = -1d * Microsoft.VisualBasic.Financial.IPmt(Convert.ToDouble(AnnualRate) / DaysPerYear, 1, PeriodDays, Convert.ToDouble(PrinAmt));
            return Convert.ToDecimal(Math.Round((PeriodDays * PerDiem), 2));
        }

        /// <summary>
        /// Returns the date that the interest accrual period starts.
        /// If using the InterestStartDate then include the InterestStartDate in the accrual period.
        /// </summary>
        /// <param name="InterestStartDate">The interest start date from the bill.</param>
        /// <param name="InterestPaidDate">The date of the most recent interest accrual.</param>
        /// <returns>The date that the interest accrual period starts</returns>
        public static DateTime GetInterestAccrualStartDate(DateTime InterestStartDate, DateTime InterestPaidDate)
        {
            return (InterestStartDate > InterestPaidDate) ? InterestStartDate.AddDays(-1) : InterestPaidDate;
        }

        protected virtual decimal CalcCurrentInterest()
        {
            decimal CurInt = 0m;

            foreach (Period p in Periods)
            {
                DateTime InterestStartDate = GetInterestAccrualStartDate(p.InterestDate, InterestPaidDate);
                CurInt += CalculateInterest(p.PrincipalDue, InterestRate, InterestStartDate, DateTime.Now, DaysPerYear);
            }
            return CurInt;
        }

        private decimal CalculateDiscount(decimal PrinAmt, decimal DiscRate)
        {
            /*  'this function returns the amount of discount for the principal passed in dblPrin
                'if dblPrin is passed a value of 0 and dblDiscPay has a value passed in, this is treated as
                'how much the user wants to pay in cash and the program will calculate the discount for just
                'the amount that is to be paid  */

            decimal disc = (PrinAmt * DiscRate);
            return decimal.Round(disc, 2);
        }
    }
}
