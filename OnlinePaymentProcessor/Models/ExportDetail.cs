﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Models
{
    public class ExportDetail
    {
        public string BillKey { get; set; }
        public int AccountNumber { get; set; }
        public string MapLot { get; set; }
        public string LocationNumber { get; set; }
        public string LocationStreet { get; set; }
        public string BillNumber { get; set; }
        public string BillType { get; set; }
        public Boolean IsLien { get; set; }
        public decimal InterestRate { get; set; }

        public DateTime Period1DueDate { get; set; }
        public DateTime Period1InterestDate { get; set; }
        public decimal Period1PrincipalDue { get; set; }
        public DateTime Period2DueDate { get; set; }
        public DateTime Period2InterestDate { get; set; }
        public decimal Period2PrincipalDue { get; set; }
        public DateTime Period3DueDate { get; set; }
        public DateTime Period3InterestDate { get; set; }
        public decimal Period3PrincipalDue { get; set; }
        public DateTime Period4DueDate { get; set; }
        public DateTime Period4InterestDate { get; set; }
        public decimal Period4PrincipalDue { get; set; }

        public decimal InterestDue { get; set; }
        public decimal CostDue { get; set; }
        public decimal SalesTaxDue { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TotalCurrentDue { get; set; }
        public decimal PrevBalDue { get; set; }

        public string Owner1FName { get; set; }
        public string Owner1LName { get; set; }
        public string Owner2FName { get; set; }
        public string Owner2LName { get; set; }
        public string OwnerMailing1 { get; set; }
        public string OwnerMailing2 { get; set; }
        public string OwnerMailing3 { get; set; }
        public string OwnerMailingCity { get; set; }
        public string OwnerMailingState { get; set; }
        public string OwnerMailingCountry { get; set; }
        public string OwnerMailingZip { get; set; }
        public string Tenant1FName { get; set; }
        public string Tenant1LName { get; set; }
        public string Tenant2FName { get; set; }
        public string Tenant2LName { get; set; }
        public string TenantMailing1 { get; set; }
        public string TenantMailing2 { get; set; }
        public string TenantMailing3 { get; set; }
        public string TenantMailingCity { get; set; }
        public string TenantMailingState { get; set; }
        public string TenantMailingCountry { get; set; }
        public string TenantMailingZip { get; set; }

        public string BuildDetailLine()
        {
            string retVal = "";
            using (StringWriter dl = new StringWriter())
            {
                dl.Write("D");
                dl.Write(BillKey.PadRight(24)); //Unique id ?    - Pad to 24 Char
                dl.Write(AccountNumber.ToString("0000000000"));
                dl.Write(MapLot.Length > 20 ? MapLot.Remove(20) : MapLot.PadRight(20));
                dl.Write(LocationNumber.Length > 5 ? LocationNumber.Remove(5) : LocationNumber.PadRight(5));
                dl.Write(LocationStreet.Length > 30 ? LocationStreet.Remove(30) : LocationStreet.PadRight(30));
                dl.Write(BillNumber.PadRight(10, Convert.ToChar(" ")));
                dl.Write(BillType);
                dl.Write("N");   //dl.Write(IsLien ? "Y" : "N");
                dl.Write((InterestRate * 100000m).ToString("00000"));
                dl.Write(Period1DueDate == DateTime.MinValue ? "00000000" : Period1DueDate.ToString("MMddyyyy"));
                dl.Write(Period1InterestDate == DateTime.MinValue
                    ? "00000000"
                    : Period1InterestDate.ToString("MMddyyyy"));
                dl.Write((Period1PrincipalDue * 100m).ToString("00000000000"));
                dl.Write(Period2DueDate == DateTime.MinValue ? "00000000" : Period2DueDate.ToString("MMddyyyy"));
                dl.Write(Period2InterestDate == DateTime.MinValue
                    ? "00000000"
                    : Period2InterestDate.ToString("MMddyyyy"));
                dl.Write((Period2PrincipalDue * 100m).ToString("00000000000"));
                dl.Write(Period3DueDate == DateTime.MinValue ? "00000000" : Period3DueDate.ToString("MMddyyyy"));
                dl.Write(Period3InterestDate == DateTime.MinValue
                    ? "00000000"
                    : Period3InterestDate.ToString("MMddyyyy"));
                dl.Write((Period3PrincipalDue * 100m).ToString("00000000000"));
                dl.Write(Period4DueDate == DateTime.MinValue ? "00000000" : Period4DueDate.ToString("MMddyyyy"));
                dl.Write(Period4InterestDate == DateTime.MinValue
                    ? "00000000"
                    : Period4InterestDate.ToString("MMddyyyy"));
                dl.Write((Period4PrincipalDue * 100m).ToString("00000000000"));
                dl.Write((InterestDue * 100m).ToString("00000000000"));
                dl.Write((CostDue * 100m).ToString("00000000000"));
                dl.Write((SalesTaxDue * 100m).ToString("00000000000"));
                dl.Write((DiscountAmount * 100m).ToString("00000000000"));
                dl.Write(((TotalCurrentDue + PrevBalDue) * 100m).ToString("00000000000"));
                dl.Write((PrevBalDue * 100m).ToString("00000000000"));
                dl.Write(Owner1FName.Length > 40 ? Owner1FName.Remove(40) : Owner1FName.PadRight(40));
                dl.Write(Owner1LName.Length > 40 ? Owner1LName.Remove(40) : Owner1LName.PadRight(40));
                dl.Write(Owner2FName.Length > 40 ? Owner2FName.Remove(40) : Owner2FName.PadRight(40));
                dl.Write(Owner2LName.Length > 40 ? Owner2LName.Remove(40) : Owner2LName.PadRight(40));
                dl.Write(OwnerMailing1.Length > 40 ? OwnerMailing1.Remove(40) : OwnerMailing1.PadRight(40));
                dl.Write(OwnerMailing2.Length > 40 ? OwnerMailing2.Remove(40) : OwnerMailing2.PadRight(40));
                dl.Write(OwnerMailing3.Length > 40 ? OwnerMailing3.Remove(40) : OwnerMailing3.PadRight(40));
                dl.Write(OwnerMailingCity.PadRight(40));
                dl.Write(OwnerMailingState.PadRight(2));
                dl.Write(OwnerMailingCountry.PadRight(40));
                dl.Write(OwnerMailingZip.PadRight(10));
                dl.Write(Tenant1FName.Length > 40 ? Tenant1FName.Remove(40) : Tenant1FName.PadRight(40));
                dl.Write(Tenant1LName.Length > 40 ? Tenant1LName.Remove(40) : Tenant1LName.PadRight(40));
                dl.Write(Tenant2FName.Length > 40 ? Tenant2FName.Remove(40) : Tenant2FName.PadRight(40));
                dl.Write(Tenant2LName.Length > 40 ? Tenant2LName.Remove(40) : Tenant2LName.PadRight(40));
                dl.Write(TenantMailing1.Length > 40 ? TenantMailing1.Remove(40) : TenantMailing1.PadRight(40));
                dl.Write(TenantMailing2.Length > 40 ? TenantMailing2.Remove(40) : TenantMailing2.PadRight(40));
                dl.Write(TenantMailing3.Length > 40 ? TenantMailing3.Remove(40) : TenantMailing3.PadRight(40));
                dl.Write(TenantMailingCity.PadRight(40));
                dl.Write(TenantMailingState.PadRight(2));
                dl.Write(TenantMailingCountry.PadRight(40));
                dl.Write(TenantMailingZip.PadRight(10));
                retVal = dl.ToString();
            }

            return retVal;
        }
    }
}
