﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor
{
    public class NetSync : INetSync
    {
        private string uploadURL = "";
        private string downloadURL = "";
        private string username = "";
        private string password = "";

        public NetSync(Settings settings)
        {
            uploadURL = settings.UploadURL;
            downloadURL = settings.DownLoadURL;
            username = settings.Username;
            password = settings.Password;
        }

        public int UploadFile(string filename)
        {
            System.Net.ServicePointManager.Expect100Continue = false;
            Uri url = new Uri(uploadURL);
            string resp = "";
            try
            {
                using (WebClient wc = new WebClient())
                {
                    string authInfo = username + ":" + password;
                    authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    wc.Headers["Authorization"] = "Basic " + authInfo;
                    byte[] respBytes = wc.UploadFile(url, "POST", filename);
                    resp = System.Text.Encoding.ASCII.GetString(respBytes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Logger.Log(ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            if (resp == "OK")
            {
                Console.WriteLine(filename + " uploaded successfully.");
            }
            else
            {
                // InforME returned an application error
                Console.WriteLine(resp);
            }
            return 0;
        }

        public IEnumerable<Payment> DownloadPayments(string queryString)
        {
            //            "https://www3.maine.gov/cgi-bin/bpadmin/rtvbpay.pl"

            System.Net.ServicePointManager.Expect100Continue = false;
            System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12) & ~(SecurityProtocolType.Tls | SecurityProtocolType.Ssl3);
            string urlString = downloadURL;
            if (!String.IsNullOrWhiteSpace(queryString))
                urlString += "?" + queryString;
            Uri url = new Uri(urlString);
            string resp = "";
            try
            {
                using (WebClient wc = new WebClient())
                {
                    string authInfo = username + ":" + password;
                    authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    wc.Headers["Authorization"] = "Basic " + authInfo;
                    resp = wc.DownloadString(url);
                }

                using (System.IO.StringReader sr = new System.IO.StringReader(resp))
                {
                    System.Data.DataSet ds = new System.Data.DataSet();
                    ds.ReadXml(sr);

                    // Get the reported payment sum and count
                    System.Data.DataRow dr0 = ds.Tables["PaymentDataSet"].Rows[0];
                    decimal pmtTotal = Convert.ToDecimal(dr0["Total"]);
                    int pmtCount = Convert.ToInt32(dr0["Count"]);

                    List<Payment> pmts = new List<Payment>();

                    if (pmtCount > 0)
                    {
                        //TODO: Utility type bill?
                        foreach (System.Data.DataRow dr in ds.Tables["Payment"].Rows)
                        {
                            Payment p = new Payment();
                            switch (dr["BillType"].ToString())
                            {
                                case "RE":
                                    p.BillType = BillType.RealEstate;
                                    break;
                                case "PP":
                                    p.BillType = BillType.PersonalPropery;
                                    break;
                                case "SW":
                                    p.BillType = BillType.Sewer;
                                    break;
                                case "WA":
                                    p.BillType = BillType.Water;
                                    break;
                                case "UT":
                                    p.BillType = BillType.UtilityBilling;
                                    break;
                                default:
                                    p.BillType = BillType.Unknown;
                                    break;
                            }
                            p.BillId = Convert.ToInt32(dr["BillId"].ToString().Remove(0, 2));
                            p.BillNumber = dr["BillNumber"].ToString();
                            p.AccountNumber = Convert.ToInt32(dr["AccountNumber"]);
                            p.PaidBy = dr["Name"].ToString();
                            p.PaymentAmount = Convert.ToDecimal(dr["PaymentAmount"]);
                            DateTime pdDt;
                            if (DateTime.TryParseExact(dr["PaymentDate"].ToString(), "MMddyyyy", System.Globalization.CultureInfo.InvariantCulture,
                                                       System.Globalization.DateTimeStyles.None, out pdDt))
                            {
                                p.PaymentDate = pdDt;
                            }
                            else
                            {
                                p.PaymentDate = DateTime.Now;
                            }
                            pmts.Add(p);
                        }
                        if (pmtCount != pmts.Count())
                        {
                            // Payment Count doesn't match number of Payment records
                            Console.WriteLine("Payment count mismatch.");
                        }
                        if (pmtTotal != pmts.Sum(x => x.PaymentAmount))
                        {
                            // Payment Total doesn't match sum of Payment records
                            Console.WriteLine("Payment total mismatch.");
                        }
                    }
                    return pmts;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Logger.Log(ex.Message, System.Diagnostics.EventLogEntryType.Error);
                return new List<Payment>();
            }
        }
    }
}
