﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Extensions;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralParties;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Extensions;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.Telemetry;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Extensions;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using SharedDataAccess;
using SharedDataAccess.Extensions;
using SharedDataAccess.TaxCollections;
using SharedDataAccess.UtilityBilling;

namespace OnlinePaymentProcessor
{
	public class PaymentProcessing
	{
		private Autofac.IContainer diContainer;
		private cGlobalSettings gGlobalSettings;
		private Settings payPortSettings = new Settings();
		private IGlobalActiveModuleSettings activeModuleSettings;

		private string _logFile = @"TRIO_InforME_Sync.log";
		private string _payportRelativePath = "PayPortOnline";
		private string _paymentRelativePath = "PaymentFiles";
		private string _billRelativePath = "BillFiles";
		private static BillExporterFactory billExporterFactory;

		public void ProcessPaymentFiles(cGlobalSettings gGlobalSettings, bool testMode)
		{
			Logger.Log("Sync started", System.Diagnostics.EventLogEntryType.Information);
			Console.WriteLine("Sync Started");
			if (gGlobalSettings == null)
			{
				Console.WriteLine("Global Settings Object is NULL");
			}
			this.gGlobalSettings = gGlobalSettings;
			Console.WriteLine("Wire Up Dependencies");
			WireUpDependencies();
			Console.WriteLine("Set Test Mode");
			payPortSettings.TestMode = testMode;
			Console.WriteLine("Load Payport Settings");
			LoadSettings();
			Console.WriteLine("Set up Directories");
			SetupDirectories();

			Console.WriteLine("Prepare for Import");
			billExporterFactory = new BillExporterFactory(payPortSettings, diContainer);

			var netSync = new NetSync(payPortSettings);
			var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
			this.activeModuleSettings = commandDispatcher.Send(new GetModules()).Result.ToGlobalActiveModuleSettings();
			var contextFactory = diContainer.Resolve<ITrioContextFactory>();
			var utilityBillingPaymentProcessor = activeModuleSettings.UtilityBillingIsActive ? diContainer.Resolve<IUtilityBillingPaymentProcessor>() : null;
			var propertyTaxPaymentProcessor = activeModuleSettings.TaxCollectionsIsActive ? diContainer.Resolve<IPropertyTaxImportedPaymentProcessor>() : null;
			
			var receiptTypeQueryHandler = activeModuleSettings.CashReceiptingIsActive ? diContainer.Resolve<IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>>>() : null;
			var deptDivTitleQueryHandler = activeModuleSettings.BudgetaryIsActive ? diContainer.Resolve<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>() : null;
			var ledgerTitleQueryHandler = activeModuleSettings.BudgetaryIsActive ? diContainer.Resolve<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>() : null;
			var budgetaryJournalService = activeModuleSettings.BudgetaryIsActive ? diContainer.Resolve<IBudgetaryJournalService>() : null;

			var systemSettingsContext = contextFactory.GetSystemSettingsContext();
			var payportSetting =
				systemSettingsContext.Settings.FirstOrDefault(x => x.SettingName == "PayPortInterface");

			if ((payportSetting?.SettingValue.ToLower() ?? "false") == "true")
			{
				var paymentImporter = new PaymentImporter(netSync, contextFactory, payPortSettings, utilityBillingPaymentProcessor, commandDispatcher, receiptTypeQueryHandler, deptDivTitleQueryHandler, budgetaryJournalService, propertyTaxPaymentProcessor, activeModuleSettings, ledgerTitleQueryHandler);
				Console.WriteLine("Importing Payments");
				paymentImporter.ImportPayments();
				Console.WriteLine("Processing Imported Payments");
				paymentImporter.ProcessImportedPayments();

				List<ExportDetail> expRecords = new List<ExportDetail>();
				List<BillException> billExceptions = new List<BillException>();

				Console.WriteLine("Calculating Bills");
				if (payPortSettings.SyncPersonalProperty)
				{
					var ppBillExports = ExportPPBills();
					billExceptions.AddRange(ppBillExports.billExceptions);
					expRecords.AddRange(ppBillExports.exportDetails);
				}

				if (payPortSettings.SyncRealEstate)
				{
					var reBillExports = ExportREBills();
					billExceptions.AddRange(reBillExports.billExceptions);
					expRecords.AddRange(reBillExports.exportDetails);
				}

				if (payPortSettings.SyncUtilityBilling)
				{
					var utBillExports = ExportUTBills();
					billExceptions.AddRange(utBillExports.billExceptions);
					expRecords.AddRange(utBillExports.exportDetails);
				}

				ExportSummary eh = new ExportSummary();
				eh.GEOCode = payPortSettings.GeoCode;
				eh.FileDate = DateTime.Now;
				eh.BillCount = expRecords.Count;
				eh.TotalAmount = expRecords.Sum(x => x.TotalCurrentDue + x.PrevBalDue);

				Console.WriteLine("Logging Bill Exceptions");
				var logFilename = Path.Combine(payPortSettings.PayportPath, DateTime.Now.ToString("MM-dd-yyyy-") + _logFile);
				LogBillExceptions(logFilename, billExceptions);

				Console.WriteLine("Creating Bill Export File");
				string filename = Path.Combine(payPortSettings.PayportPath, _billRelativePath,
					payPortSettings.GeoCode + "_" + DateTime.Now.ToString("yyyyMMdd") + ".dat");
				CreateBillExportFile(filename, eh, expRecords);

				Console.WriteLine("Uploading Bills");
				if (expRecords.Count > 0)
				{
					netSync.UploadFile(filename);
				}
			}
			else
			{
				Console.WriteLine("Sync stopped.  PayPort Interface not active.");
				Logger.Log("Sync stopped.  PayPort Interface not active.", System.Diagnostics.EventLogEntryType.Information);
			}
		}

		
		private void CreateBillExportFile(string fileName, ExportSummary eh, IEnumerable<ExportDetail> expRecords)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(fileName, false))
				{
					sw.WriteLine(eh.BuildHeaderLine());

					foreach (ExportDetail ed in expRecords)
					{
						sw.WriteLine(ed.BuildDetailLine());
					}

					sw.Flush();
					sw.Close();
				}
			}
			catch (Exception e)
			{
				// We need an error logger since this may be running as a background process
				Console.WriteLine(e.Message);
				Logger.Log(e.Message, System.Diagnostics.EventLogEntryType.Error);
				Console.ReadKey();
			}
		}

		private static (IEnumerable<ExportDetail> exportDetails, IEnumerable<BillException> billExceptions)
			ExportPPBills()
		{
			var ppBillExporter = billExporterFactory.GetPPBillExporter();
			return ppBillExporter.GetExportDetails();
		}

		private static (IEnumerable<ExportDetail> exportDetails, IEnumerable<BillException> billExceptions)
			ExportREBills()
		{
			var reBillExporter = billExporterFactory.GetReBillExporter();
			return reBillExporter.GetExportDetails();
		}

		private  (IEnumerable<ExportDetail> exportDetails, IEnumerable<BillException> billExceptions) ExportUTBills()
		{
			var utilityBillExporter = billExporterFactory.GetUtilityBillExporter();
			return utilityBillExporter.GetExportDetails();
		}

		private void SetupDirectories()
		{
			if (!Directory.Exists(payPortSettings.PayportPath))
			{
				Directory.CreateDirectory(payPortSettings.PayportPath);
			}

			if (!Directory.Exists(Path.Combine(payPortSettings.PayportPath, _paymentRelativePath)))
			{
				Directory.CreateDirectory(Path.Combine(payPortSettings.PayportPath, _paymentRelativePath));
			}

			if (!Directory.Exists(Path.Combine(payPortSettings.PayportPath, _billRelativePath)))
			{
				Directory.CreateDirectory(Path.Combine(payPortSettings.PayportPath, _billRelativePath));
			}
		}

		private void LoadSettings()
		{
			var settingsService = diContainer.Resolve<ISettingService>();

			payPortSettings.SyncRealEstate = settingsService.GetSettingValue(SettingOwnerType.Global, "SyncRE").SettingValue.ToLower() == "true";
			payPortSettings.SyncPersonalProperty = settingsService.GetSettingValue(SettingOwnerType.Global, "SyncPP").SettingValue.ToLower() == "true";
			payPortSettings.SyncUtilityBilling = settingsService.GetSettingValue(SettingOwnerType.Global, "SyncUB").SettingValue.ToLower() == "true";
			payPortSettings.CombineUtilityServices = settingsService.GetSettingValue(SettingOwnerType.Global, "CombineUBBills").SettingValue.ToLower() == "true";
			payPortSettings.CombinedUtilityType = settingsService.GetSettingValue(SettingOwnerType.Global, "CombinedUBBillType").SettingValue.Trim();
			payPortSettings.DataPath = gGlobalSettings.GlobalDataDirectory;
			payPortSettings.GeoCode = settingsService.GetSettingValue(SettingOwnerType.Global, "GeoCode").SettingValue.Trim();

			if (payPortSettings.TestMode)
			{
				payPortSettings.Username = "harris";
				payPortSettings.Password = "knox";
				payPortSettings.DownLoadURL = "https://www3.maine.gov/cgi-bin/bpadmin/rtvbpay.pl";
				payPortSettings.UploadURL = "https://www3.maine.gov/cgi-bin/bpadmin/process.pl";
			}
			else
			{
				payPortSettings.Username = "harris";
				payPortSettings.Password = "H@rr15!";
				payPortSettings.DownLoadURL = "https://www1.maine.gov/cgi-bin/bpadmin/process.pl";
				payPortSettings.UploadURL = "https://www1.maine.gov/cgi-bin/bpadmin/process.pl";
			}

			payPortSettings.PayportPath = Path.Combine(payPortSettings.DataPath, _payportRelativePath);
			payPortSettings.LogPathFileName = Path.Combine(payPortSettings.DataPath, DateTime.Now.ToString("MM-dd-yyyy-") + _logFile);

		}

		private void WireUpDependencies()
		{
			try
			{
				var builder = new ContainerBuilder();
				Console.WriteLine("Wire Up Global Settings");
				builder.RegisterInstance(gGlobalSettings).As<cGlobalSettings>();
				Console.WriteLine("Wire Up Data Context Details");
				builder.RegisterInstance(gGlobalSettings.ToDataContextDetails()).As<DataContextDetails>();
				Console.WriteLine("Wire Up Module Settings");
				RegisterBudgetaryGlobalAccountSettings(ref builder);
				RegisterGlobalActiveModuleSettings(ref builder);
				RegisterUtilityBillingSettings(ref builder);
				RegisterEnvironmentSettings(ref builder);
				RegisterUBContext(ref builder);
				RegisterCLContext(ref builder);
				RegisterCentralPartyContext(ref builder);
				builder.RegisterType<PropertyTaxImportedPaymentProcessor>().As<IPropertyTaxImportedPaymentProcessor>();
				Console.WriteLine("Wire Up Telemetry");
				builder.RegisterType<TelemetryDummy>().As<ITelemetryService>();
				Console.WriteLine("Wire Up User Info");
				builder.Register(f => new UserInformation
				{
					UserId = "SuperUser",
					Id = -1
				}).As<UserInformation>();
				Console.WriteLine("Wire Up Mediatr");
				builder.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
				Console.WriteLine("Wire Up Assembly Objects");
				builder.RegisterAssemblyModules(AppDomain.CurrentDomain.GetAssemblies());
				Console.WriteLine("Build Dependencies");
				diContainer = builder.Build();
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		private void RegisterUBContext(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetUtilityBillingContext();
			}).As<UtilityBillingContext>();
		}

		private void RegisterCLContext(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetTaxCollectionsContext();
			}).As<TaxCollectionsContext>();
		}

		private void RegisterCentralPartyContext(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var tcf = f.Resolve<ITrioContextFactory>();
				return tcf.GetCentralPartyContext();
			}).As<ICentralPartyContext>();
		}

		private void RegisterBudgetaryGlobalAccountSettings(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var commandDispatcher = diContainer.Resolve<CommandDispatcher>();
				
				return commandDispatcher.Send(new GetBudgetarySettings()).Result.ToGlobalBudgetaryAccountSettings();
			}).As<IGlobalBudgetaryAccountSettings>();
		}

		private void RegisterUtilityBillingSettings(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var commandDispatcher = diContainer.Resolve<CommandDispatcher>();

				return commandDispatcher.Send(new GetUtilityBillingSettings()).Result.ToGlobalUtilityBillingSettings();
			}).As<GlobalUtilityBillingSettings>();
		}

		private void RegisterEnvironmentSettings(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var commandDispatcher = diContainer.Resolve<CommandDispatcher>();

				return commandDispatcher.Send(new GetEnvironmentSettings()).Result;
			}).As<DataEnvironmentSettings>();
		}

		private void RegisterGlobalActiveModuleSettings(ref ContainerBuilder builder)
		{
			builder.Register(f =>
			{
				var commandDispatcher = diContainer.Resolve<CommandDispatcher>();

				return commandDispatcher.Send(new GetModules()).Result.ToGlobalActiveModuleSettings();
			}).As<IGlobalActiveModuleSettings>();
		}

		private static void LogBillExceptions(string filename, IEnumerable<BillException> billExceptions)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(filename, false))
				{
					foreach (BillException billException in billExceptions)
					{
						sw.WriteLine(BillTypeCodeToDescription(billException.BillType) + " bill for account " +
						             billException.Account + " " + billException.BillIdentifier + " has " +
						             billException.Reason.ToLower());
					}

					sw.Flush();
					sw.Close();
				}
			}
			catch
			{

			}
		}

		private static string BillTypeCodeToDescription(string billType)
		{
			switch (billType.ToLower())
			{
				case "sw":
					return "Sewer";
				case "wa":
					return "Water";
				case "ut":
					return "Utility";
				case "re":
					return "Real Estate";
				case "pp":
					return "Personal Property";
			}

			return "";
		}

	}
}
