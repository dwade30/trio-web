﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;

namespace OnlinePaymentProcessor
{
	public static class CryptoUtility
	{
		private static string strToUse = "B4A45E16-2828-4157-908E-F2266FA1810A";
		private static string sToUse = "O72SajdzgdE=";

		public static string DecryptByPassword(string strToDecrypt)
		{
			return DecryptByPassword(strToDecrypt, strToUse, sToUse, "", 2, KeySize.Bits256);
		}

		private static string Decrypt(byte[] ToDecryptBytes, byte[] keybytes, byte[] InitialVectorBytes)
		{
			string strReturn = null;
			if (ToDecryptBytes == null)
			{
				return strReturn;
			}
			if (ToDecryptBytes.LongLength < 1)
			{
				return strReturn;
			}
			try
			{
				using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
				{
					aesAlg.Key = keybytes;
					aesAlg.IV = InitialVectorBytes;
					ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
					using (MemoryStream msDecrypt = new MemoryStream(ToDecryptBytes))
					{
						using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
						{
							using (StreamReader srDecrypt = new StreamReader(csDecrypt))
							{
								strReturn = srDecrypt.ReadToEnd();
							}
						}
					}
				}
				return strReturn;
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		private static string DecryptByPassword(string strToDecrypt, string strPassword, string strSalt, string strInitialVector, int intPasswordIterations, KeySize intKeySize)
		{
			string strReturn = null;
			if (string.IsNullOrEmpty(strToDecrypt))
			{
				return strReturn;
			}
			try
			{
				byte[] InitialVectorBytes;
				byte[] SaltBytes = Convert.FromBase64String(strSalt);
				Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
				byte[] ToDecryptBytes;
				ToDecryptBytes = Convert.FromBase64String(strToDecrypt);

				byte[] KeyBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, intKeySize);
				if (strInitialVector != "")
				{
					InitialVectorBytes = Convert.FromBase64String(strInitialVector);
				}
				else
				{
					InitialVectorBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, KeySize.Bits128);
					// initial vector must be 128 bits
				}
				return Decrypt(ToDecryptBytes, KeyBytes, InitialVectorBytes);
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		private static byte[] KeyFromPassword(string strPassword, string strSalt, int intPasswordIterations, KeySize intKeySize)
		{
			if (string.IsNullOrEmpty(strPassword))
			{
				return null;
			}
			byte[] SaltBytes = Convert.FromBase64String(strSalt);
			Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
			int intSize = 256;
			switch (intKeySize)
			{
				case KeySize.Bits128:
					intSize = 128;
					break;
				case KeySize.Bits192:
					intSize = 192;
					break;
				case KeySize.Bits256:
					intSize = 256;
					break;
				default:
					intSize = 256;
					break;
			}
			byte[] KeyBytes = DerivedPassword.GetBytes(intSize / 8);
			return KeyBytes;
		}

		
	}
}
