﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication.Extensions;

namespace OnlinePaymentProcessor.RealEstate
{
    public class REBillGenerator : IBillGenerator<REBillRecord>
    {
        public List<ExportDetail> GetExportDetailsForAccount(Account account, ref IEnumerable<Bill> bills)
        {
            var billAndPrevBalance = GetCurrentBillAndPrevBalance(ref bills);
            var exportDetails = new List<ExportDetail>();
            var detail = GetExportDetail(billAndPrevBalance.bill, account);
            if (detail != null)
            {
                detail.PrevBalDue = billAndPrevBalance.previousBalance;
                exportDetails.Add(detail);
            }
            return exportDetails;
        }

        public IEnumerable<Bill> GetOutstandingBills(ref IEnumerable<REBillRecord> billRecords)
        {
            List<Bill> bills = new List<Bill>();
            bills.AddRange(billRecords.Where(b => b.TotalAmountDue > 0)
                .ToList());
            return bills;
        }

        public int DaysPerYear { get; set; }

        protected ExportDetail GetExportDetail(Bill bill, Account reAccount)
        {
            if (bill.BillKey != 0)
            {
                var exportDetail = new ExportDetail();
                exportDetail.AccountNumber = bill.AccountNumber;
                exportDetail.BillKey = bill.BillType.ToBillTypeCode() + bill.BillKey;
                exportDetail.AccountNumber = reAccount.AccountNumber;
                exportDetail.Owner1FName = reAccount.Owner1FName;
                exportDetail.Owner1LName = reAccount.Owner1LName;
                exportDetail.Owner2FName = reAccount.Owner2FName;
                exportDetail.Owner2LName = reAccount.Owner2LName;
                exportDetail.OwnerMailing1 = reAccount.OwnerAddress1;
                exportDetail.OwnerMailing2 = reAccount.OwnerAddress2;
                exportDetail.OwnerMailing3 = reAccount.OwnerAddress3;
                exportDetail.OwnerMailingCity = reAccount.OwnerCity;
                exportDetail.OwnerMailingState = reAccount.OwnerState.Length > 2 ? reAccount.OwnerState.Left(2) : reAccount.OwnerState;
                exportDetail.OwnerMailingCountry = reAccount.OwnerCountry;
                exportDetail.OwnerMailingZip = reAccount.OwnerZip;
                exportDetail.Tenant1FName = reAccount.Tenant1FName;
                exportDetail.Tenant1LName = reAccount.Tenant1LName;
                exportDetail.Tenant2FName = reAccount.Tenant2FName;
                exportDetail.Tenant2LName = reAccount.Tenant2LName;
                exportDetail.TenantMailing1 = reAccount.TenantAddress1;
                exportDetail.TenantMailing2 = reAccount.TenantAddress2;
                exportDetail.TenantMailing3 = reAccount.TenantAddress3;
                exportDetail.TenantMailingCity = reAccount.TenantCity;
                exportDetail.TenantMailingState = reAccount.TenantState.Length > 2 ? reAccount.TenantState.Left(2) : reAccount.TenantState;
                exportDetail.TenantMailingCountry = reAccount.TenantCountry;
                exportDetail.TenantMailingZip = reAccount.TenantZip;
                exportDetail.MapLot = reAccount.MapLot;
                exportDetail.LocationNumber = reAccount.LocationNumber;
                exportDetail.LocationStreet = reAccount.LocationStreet;
                exportDetail.BillNumber = bill.BillNumber.ToString();
                exportDetail.BillType = bill.BillType.ToBillTypeCode();
                exportDetail.IsLien = bill.IsLien;
                exportDetail.InterestRate = bill.InterestRate;
                exportDetail.InterestDue = bill.TotalInterestDue;
                exportDetail.CostDue = bill.TotalCostDue;
                exportDetail.SalesTaxDue = bill.TotalSalesTaxDue;
                exportDetail.DiscountAmount = bill.DiscountAmount;
                exportDetail.TotalCurrentDue = bill.TotalAmountDue;
                Period p = bill.Periods[0];
                if (p != null)
                {
                    exportDetail.Period1DueDate = p.DueDate;
                    exportDetail.Period1InterestDate = p.InterestDate;
                    exportDetail.Period1PrincipalDue = p.PrincipalDue;
                }
                else
                {
                    exportDetail.Period1DueDate = DateTime.MinValue;
                    exportDetail.Period1InterestDate = DateTime.MinValue;
                    exportDetail.Period1PrincipalDue = 0m;
                }
                p = bill.Periods.Count > 1 ? p = bill.Periods[1] : null;
                if (p != null)
                {
                    exportDetail.Period2DueDate = p.DueDate;
                    exportDetail.Period2InterestDate = p.InterestDate;
                    exportDetail.Period2PrincipalDue = p.PrincipalDue;
                }
                else
                {
                    exportDetail.Period2DueDate = DateTime.MinValue;
                    exportDetail.Period2InterestDate = DateTime.MinValue;
                    exportDetail.Period2PrincipalDue = 0m;
                }

                p = bill.Periods.Count > 2 ? p = bill.Periods[2] : null;
                if (p != null)
                {
                    exportDetail.Period3DueDate = p.DueDate;
                    exportDetail.Period3InterestDate = p.InterestDate;
                    exportDetail.Period3PrincipalDue = p.PrincipalDue;
                }
                else
                {
                    exportDetail.Period3DueDate = DateTime.MinValue;
                    exportDetail.Period3InterestDate = DateTime.MinValue;
                    exportDetail.Period3PrincipalDue = 0m;
                }
                p = bill.Periods.Count > 3 ? p = bill.Periods[3] : null;
                if (p != null)
                {
                    exportDetail.Period4DueDate = p.DueDate;
                    exportDetail.Period4InterestDate = p.InterestDate;
                    exportDetail.Period4PrincipalDue = p.PrincipalDue;
                }
                else
                {
                    exportDetail.Period4DueDate = DateTime.MinValue;
                    exportDetail.Period4InterestDate = DateTime.MinValue;
                    exportDetail.Period4PrincipalDue = 0m;
                }

                return exportDetail;
            }

            return null;
        }

        protected (Bill bill, Decimal previousBalance) GetCurrentBillAndPrevBalance(ref IEnumerable<Bill> bills)
        {
            if (bills.Any())
            {
                Bill bcur = bills.OrderByDescending(x => x.BillNumber).First();
                decimal prevBal = bills.Where(x => x.BillNumber != bcur.BillNumber)
                    .Sum(x => x.TotalAmountDue);
                return (bcur, prevBal);
            }

            return (new Bill(), 0);
        }
    }
}
