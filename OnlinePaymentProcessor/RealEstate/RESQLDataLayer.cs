﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.EntityFrameworkCore;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication.Extensions;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess;

namespace OnlinePaymentProcessor.RealEstate
{
    public class RESQLDataLayer : IDataLayer<REBillRecord>
    {
        private int daysPerYear = 0;
        private Autofac.IContainer diContainer;
        private ITaxCollectionsContext collectionsContext;
        private IRealEstateContext realEstateContext;

        public RESQLDataLayer(Autofac.IContainer diContainer)
        {
	        this.diContainer = diContainer;

	        var factory = diContainer.Resolve<ITrioContextFactory>();

            collectionsContext = factory.GetTaxCollectionsContext();
            realEstateContext = factory.GetRealEstateContext();

            var settings = collectionsContext.CollectionSettings.FirstOrDefault();

            if (settings != null)
            {
	            daysPerYear = (settings.DaysInYear ?? 0) == 0 ? 365 : settings.DaysInYear ?? 0;
            }
            else
            {
	            daysPerYear = 365;
            }
        }

        public IEnumerable<Account> GetAccounts()
        {
	        var accounts = realEstateContext.REAccountPartyAddressViews.Where(x => (x.Rscard ?? 0) == 1)
		        .OrderBy(y => y.Rsaccount)
		        .Select(z => new Account
		        {
                    AccountType = AccountType.RealEstate,
                    AccountNumber = z.Rsaccount ?? 0,
                    LocationNumber = z.RslocNumAlph ?? "",
                    LocationStreet = z.RslocStreet ?? "",
                    MapLot = z.RsmapLot ?? "",
                    Owner1FName = z.DeedName1 ?? "",
                    Owner1LName = "",
                    Owner2FName = z.DeedName2 ?? "",
                    Owner2LName = "",
                    OwnerAddress1 = z.Address1 ?? "",
                    OwnerAddress2 = z.Address2 ?? "",
                    OwnerAddress3 = z.Address3 ?? "",
                    OwnerCity = z.City ?? "",
                    OwnerState = z.State ?? "",
                    OwnerCountry = "",
                    OwnerZip = z.Zip ?? "",
                    Tenant1FName = "",
                    Tenant1LName = "",
                    Tenant2FName = "",
                    Tenant2LName = "",
                    TenantAddress1 = "",
                    TenantAddress2 = "",
                    TenantAddress3 = "",
                    TenantCity = "",
                    TenantState = "",
                    TenantCountry = "",
                    TenantZip = ""
                });

           
            return accounts.ToList();
        }

        public IEnumerable<REBillRecord> GetUnpaidBillsForAccount(int accountNumber)
        {
            var bills = GetUnpaidBillRecordsForAccount(accountNumber);
            var reBills = MapToREBillRecords(ref bills).Where(b => b.IsLien == false).ToList();

            //TODO: possibly add liens to export
            IEnumerable<Bill> ln = 
                from _b in bills
                where _b.LienRecordNumber != 0
                select (CreateBill(BillType.RealEstate, _b));
            List<Bill> LnBills = ln.ToList();
            IEnumerable<Bill> liens =
                from _b in LnBills
                where _b.TotalAmountDue > 0
                select (_b);
            reBills.AddRange(MapToREBillRecords(ref liens).ToList());
            return reBills;
        }

        private IEnumerable<BillingMaster> GetUnpaidBillRecordsForAccount(int accountNumber)
        {
	        var bills = collectionsContext.BillingMasters
		        .Where(x => x.BillingType == "RE" && x.Account == accountNumber && x.RateRecId > 0 &&
		                    (x.TaxDue1 + x.TaxDue2 + x.TaxDue3 + x.TaxDue4 + x.DemandFees - x.InterestCharged) > (x.PrincipalPaid + x.InterestPaid + x.DemandFeesPaid))
		        .Include(z => z.RateRec)
		        .OrderBy(y => y.BillingYear);

	        return bills;
        }

        private IEnumerable<REBillRecord> MapToREBillRecords(ref IEnumerable<BillingMaster> bills)
        {
            return bills.Select(dr => CreateBill(BillType.RealEstate, dr));
        }

        private REBillRecord CreateBill(BillType btype, BillingMaster brec)
        {
            REBillRecord _b = new REBillRecord();

            _b.BillType = btype;
            _b.BillKey = brec.ID;
            _b.AccountNumber = brec.Account ?? 0;
            int tmpBillNo = brec.BillingYear ?? 0;
            _b.BillNumber = tmpBillNo == 0 ? "" : tmpBillNo.ToString("0000-0");

            _b.Owner1FName = "";
            _b.Owner1LName = brec.Name1 ?? "";
            _b.Owner2FName = "";
            _b.Owner2LName = brec.Name2 ?? "";
            _b.OwnerAddress1 = brec.Address1 ?? "";
            _b.OwnerAddress2 = brec.Address2 ?? "";
            _b.OwnerAddress3 = "";
            _b.OwnerCity = brec.Address3 ?? "" ;
            _b.OwnerState = brec.Address3 ?? "";
            _b.OwnerZip = brec.Zip ?? "";
            _b.OwnerCountry = String.Empty;
            _b.Tenant1FName = String.Empty;
            _b.Tenant1LName = String.Empty;
            _b.Tenant2FName = String.Empty;
            _b.Tenant2LName = String.Empty;
            _b.TenantAddress1 = String.Empty;
            _b.TenantAddress2 = String.Empty;
            _b.TenantAddress3 = String.Empty;
            _b.TenantCity = String.Empty;
            _b.TenantState = String.Empty;
            _b.TenantZip = String.Empty;
            _b.TenantCountry = String.Empty;

            if (brec.LienRecordNumber == 0)
            {
                // Process regular bill
                _b.IsLien = false;
                _b.PrincipalPaid = brec.PrincipalPaid ?? 0;
                _b.CostBilled = 0m;
                _b.CostAdded = brec.DemandFees ?? 0;
                _b.CostPaid = brec.DemandFeesPaid ?? 0;
                _b.InterestBilled = 0m;
                _b.InterestAdded = (brec.InterestCharged ?? 0) * -1m;
                _b.InterestPaid = brec.InterestPaid ?? 0;
                _b.SalesTaxBilled = 0m;
                _b.SalesTaxPaid = 0;
                _b.InterestPaidDate = brec.InterestAppliedThroughDate ?? DateTime.MinValue;
                _b.InterestRate = (brec.RateRec.InterestRate ?? 0).ToString().ToDecimalValue();
                _b.DaysPerYear = daysPerYear;

                _b.Periods.AddRange(CreatePeriods(ref brec));
            }
            else
            {
                // Process lien
                _b.IsLien = true;

                var lien = collectionsContext.LienRecords.Include(y => y.RateRec).FirstOrDefault(x => x.Id == brec.LienRecordNumber);

                _b.PrincipalPaid = lien.PrincipalPaid ?? 0;
                _b.CostBilled = lien.Costs ?? 0;
                _b.CostAdded = (lien.MaturityFee ?? 0) * -1m;
                _b.CostPaid = lien.CostsPaid ?? 0;
                _b.InterestBilled = lien.Interest ?? 0;
                _b.InterestAdded = (lien.InterestCharged ?? 0) * -1m;
                _b.InterestPaid = ((lien.Plipaid ?? 0) + (lien.InterestPaid ?? 0));
                _b.SalesTaxBilled = 0m;
                _b.SalesTaxPaid = 0m;

                _b.InterestPaidDate = lien.InterestAppliedThroughDate ?? DateTime.MinValue;
                _b.InterestRate = (lien.RateRec.InterestRate ?? 0).ToString().ToDecimalValue();
                _b.DaysPerYear = daysPerYear;

                Period _p = new Period
                {
                    DueDate = lien.RateRec.DueDate1 ?? DateTime.MinValue,
                    InterestDate = lien.RateRec.InterestStartDate1 ?? DateTime.MinValue,
                    PrincipalAmount = lien.Principal ?? 0,
                    PrincipalDue = (lien.Principal ?? 0) - _b.PrincipalPaid
                };
                _b.Periods.Add(_p);
            }

            return _b;
        }

        private class Abatement
        {
            public decimal Amount { get; set; }
            public int Period { get; set; }
        }

        private IEnumerable<Period> CreatePeriods(ref BillingMaster brec)
        {
            var billId = brec.ID;
            var numberOfPeriods = brec.RateRec.NumberOfPeriods.ToString().ToIntegerValue();
            var periods = new List<Period>();
            decimal prinOffsetAmount = brec.PrincipalPaid ?? 0;
            var abatements = GetAbatementsForBill(billId, numberOfPeriods);
            prinOffsetAmount = Math.Round(prinOffsetAmount - abatements.Sum(a => a.Amount), 2);


            for (int i = 1; i <= numberOfPeriods; i++)
            {
                Period _p = new Period();

                switch (i)
                {
                    case 1:
	                    _p.DueDate = (DateTime)brec.RateRec.DueDate1;
	                    _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate1;
	                    _p.PrincipalAmount = brec.TaxDue1 ?? 0;
                        break;
                    case 2:
                        _p.DueDate = (DateTime)brec.RateRec.DueDate2;
                        _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate2;
                        _p.PrincipalAmount = brec.TaxDue2 ?? 0;
                        break;
                    case 3:
	                    _p.DueDate = (DateTime)brec.RateRec.DueDate3;
	                    _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate3;
	                    _p.PrincipalAmount = brec.TaxDue3 ?? 0;
                        break;
                    case 4:
	                    _p.DueDate = (DateTime)brec.RateRec.DueDate4;
	                    _p.InterestDate = (DateTime)brec.RateRec.InterestStartDate4;
	                    _p.PrincipalAmount = brec.TaxDue4 ?? 0;
                        break;
                }
                
                decimal periodAbateAmount = abatements.Where(a => a.Period == i).Sum(a => a.Amount);

                if (prinOffsetAmount > _p.PrincipalAmount - periodAbateAmount)
                {
                    _p.PrincipalDue = 0m;
                    prinOffsetAmount -= _p.PrincipalAmount - periodAbateAmount;
                }
                else
                {
                    _p.PrincipalDue = _p.PrincipalAmount - periodAbateAmount - prinOffsetAmount;
                    prinOffsetAmount = 0m;
                }
                periods.Add(_p);
            }
            return periods;
        }

        private IEnumerable<Abatement> GetAbatementsForBill(int billKey, int numPeriods)
        {
            List<Abatement> abatements = new List<Abatement>();

            var abateRecords = collectionsContext.PaymentRecords
	            .Where(x => x.BillCode == "R" && x.Code == "A" && x.BillId == billKey);

            // Abatements to a specific period
            abatements = abateRecords
                .Where(a => a.Period != "A")
                .Select(a =>
                    new Abatement
                    {
                        Period = a.Period.ToIntegerValue(),
                        Amount = a.Principal ?? 0
                    }).ToList();

            // Split abatements with Period = Auto
            foreach (PaymentRec pr in abateRecords.Where(a => a.Period == "A"))
            {
                decimal abateAmt = pr.Principal ?? 0;
                abatements.AddRange(SplitAbatement(abateAmt, numPeriods));
            }

            return abatements;
        }

        /// Return a list of Abatement that splits the abatement amount evenly between the number of periods
        private IEnumerable<Abatement> SplitAbatement(decimal abateAmount, int numPeriods)
        {
            List<Abatement> abatements = new List<Abatement>();
            decimal remainder = abateAmount % (numPeriods / 100m);
            decimal splitAmount = Math.Round((abateAmount - remainder) / numPeriods, 2);

            for (int i = numPeriods; i > 1; i--)
            {
                abatements.Add(new Abatement { Amount = splitAmount, Period = i });
            }
            abatements.Add(new Abatement { Amount = Math.Round(abateAmount - (splitAmount * (numPeriods - 1)), 2), Period = 1 });
            return abatements;
        }

        private IEnumerable<REBillRecord> MapToREBillRecords(ref IEnumerable<Bill> bills)
        {
            return bills.Select(b => new REBillRecord()
            {
                BillType = b.BillType,
                BillNumber = b.BillNumber,
                AccountNumber = b.AccountNumber,
                InterestPaid = b.InterestPaid,
                InterestBilled = b.InterestBilled,
                CostAdded = b.CostAdded,
                SalesTaxPaid = b.SalesTaxPaid,
                InterestAdded = b.InterestAdded,
                SalesTaxBilled = b.SalesTaxBilled,
                CostPaid = b.CostPaid,
                CostBilled = b.CostBilled,
                IsLien = b.IsLien,
                BillKey = b.BillKey,
                DaysPerYear = b.DaysPerYear,
                DiscountAmount = b.DiscountAmount,
                InterestPaidDate = b.InterestPaidDate,
                InterestRate = b.InterestRate,
                Owner1FName = b.Owner1FName,
                Owner1LName = b.Owner1LName,
                Owner2FName = b.Owner2FName,
                Owner2LName = b.Owner2LName,
                OwnerAddress1 = b.OwnerAddress1,
                OwnerAddress2 = b.OwnerAddress2,
                OwnerAddress3 = b.OwnerAddress3,
                OwnerCity = b.OwnerCity,
                OwnerCountry = b.OwnerCountry,
                OwnerState = b.OwnerState,
                OwnerZip = b.OwnerZip,
                Periods = b.Periods,
                PrincipalPaid = b.PrincipalPaid,
                Tenant1FName = b.Tenant1FName,
                Tenant1LName = b.Tenant1LName,
                Tenant2FName = b.Tenant2FName,
                Tenant2LName = b.Tenant2LName,
                TenantAddress1 = b.TenantAddress1,
                TenantAddress2 = b.TenantAddress2,
                TenantAddress3 = b.TenantAddress3,
                TenantCity = b.TenantCity,
                TenantCountry = b.TenantCountry,
                TenantState = b.TenantState,
                TenantZip = b.TenantZip
            });
        }

        public void TestAbatements()
        {
            List<Abatement> abatements = new List<Abatement>();

            Console.WriteLine("Test 1 Period");
            abatements = SplitAbatement(1888.79m, 1).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 2 Periods Remainder 1");
            abatements = SplitAbatement(1888.79m, 2).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 2 Periods Remainder 0");
            abatements = SplitAbatement(1888.78m, 2).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 3 Periods Remainder 2");
            abatements = SplitAbatement(1888.79m, 3).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 3 Periods Remainder 1");
            abatements = SplitAbatement(1888.78m, 3).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 3 Periods Remainder 0");
            abatements = SplitAbatement(1888.77m, 3).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 4 Periods Remainder 3");
            abatements = SplitAbatement(1888.79m, 4).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 4 Periods Remainder 2");
            abatements = SplitAbatement(1888.78m, 4).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 4 Periods Remainder 1");
            abatements = SplitAbatement(1888.77m, 4).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

            Console.WriteLine("Test 4 Periods Remainder 0");
            abatements = SplitAbatement(1888.76m, 4).ToList();
            foreach (Abatement a in abatements)
                Console.WriteLine("Period " + a.Period.ToString() + " Amount = " + a.Amount);
            Console.WriteLine();

        }
    }
}
