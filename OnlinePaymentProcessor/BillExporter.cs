﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor
{
	public class BillExporter<TRecordType>
	{
		private IDataLayer<TRecordType> dataLayer;
		private IBillGenerator<TRecordType> billGenerator;
		public BillExporter(IDataLayer<TRecordType> dLayer, IBillGenerator<TRecordType> generator)
		{
			dataLayer = dLayer;
			billGenerator = generator;
		}

		public (IEnumerable<ExportDetail> exportDetails, IEnumerable<BillException> billExceptions) GetExportDetails()
		{
			List<ExportDetail> expDetails = new List<ExportDetail>();
			IEnumerable<Account> accounts = dataLayer.GetAccounts();
			foreach (Account acct in accounts)
			{
                IEnumerable<TRecordType> bills = dataLayer.GetUnpaidBillsForAccount(acct.AccountNumber);

				var outstandingBills = billGenerator.GetOutstandingBills(ref bills);
				expDetails.AddRange(billGenerator.GetExportDetailsForAccount(acct, ref outstandingBills));
			}
			var badDetails = expDetails.GetBillExceptions().ToList();
			var goodDetails = expDetails.WithoutExceptions().ToList();
			return (goodDetails, badDetails);
		}
	}
}
