﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.EntityFrameworkCore;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using SharedDataAccess;
using Bill = SharedApplication.UtilityBilling.Models.Bill;

namespace OnlinePaymentProcessor.UtilityBilling
{
    public class UTSQLDataLayer : IDataLayer<UTBillRecord>
    {
        private Autofac.IContainer diContainer;
        private int daysPerYear;
        private IUtilityBillingContext utilityBillingContext;
        private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>> customerPartyQueryHandler;

        public UTSQLDataLayer(Autofac.IContainer diContainer)
        {
	        this.diContainer = diContainer;
	        utilityBillingContext = diContainer.Resolve<ITrioContextFactory>().GetUtilityBillingContext();
	        customerPartyQueryHandler = diContainer.Resolve<IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>>();
        }

        private IEnumerable<Bill> GetUnpaidBillRecordsForAccount(int accountNumber)
        {
	        var bills = utilityBillingContext.Bills.Where(x => x.ActualAccountNumber == accountNumber && x.BillNumber > 0 && ((x.SPrinOwed + x.SIntOwed + x.SCostAdded + x.STaxOwed - x.SIntAdded) > (x.SPrinPaid + x.SCostPaid + x.SIntPaid + x.STaxPaid) || (x.WPrinOwed + x.WIntOwed + x.WCostAdded + x.WTaxOwed - x.WIntAdded) > (x.WPrinPaid + x.WCostPaid + x.WIntPaid + x.WTaxPaid) || (x.SLienRecordNumber != 0 && x.SCombinationLienKey == x.ID) || (x.WLienRecordNumber != 0 && x.WCombinationLienKey == x.ID)))
		        .Include(z => z.RateKey)
		        .OrderBy(y => y.BillNumber);

	        return bills.ToList();
        }

        private Lien GetLienByID(int lienId)
        {
	        var lien = utilityBillingContext.Liens.Include(y => y.RateKey).FirstOrDefault(x => x.ID == lienId);
            
            return lien;
        }

        public IEnumerable<Account> GetAccounts()
        {
	        var accounts = customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
	        {

	        }).Where(x => !x.Deleted)
	        .Select(z => new Account
	        {
                AccountType = AccountType.UtilityBilling,
                AccountNumber = z.AccountNumber,
                LocationNumber = z.StreetNumber.ToString(),
                LocationStreet = z.StreetName ?? "",
                MapLot = z.MapLot ?? "",
                Owner1FName = z.FullNameLF ?? "",
                Owner1LName = "",
                Owner2FName = z.SecondOwnerFullNameLF ?? "",
                Owner2LName = "",
                OwnerAddress1 = z.OwnerAddress1 ?? "",
                OwnerAddress2 = z.OwnerAddress2 ?? "",
                OwnerAddress3 = z.OwnerAddress3 ?? "",
                OwnerCity = z.OwnerCity ?? "",
                OwnerState = z.OwnerState ?? "",
                OwnerCountry = "",
                OwnerZip = z.OwnerZip ?? "",
                Tenant1FName = z.BilledFullNameLF ?? "",
                Tenant1LName = "",
                Tenant2FName = z.Billed2FullNameLF ?? "",
                Tenant2LName = "",
                TenantAddress1 = z.BilledAddress1 ?? "",
                TenantAddress2 = z.BilledAddress2 ?? "",
                TenantAddress3 = z.BilledAddress3 ?? "",
                TenantCity = z.BilledCity ?? "",
                TenantState = z.BilledState ?? "",
                TenantCountry = String.Empty,
                TenantZip = z.BilledZip ?? "",
            });

	        return accounts.ToList();
        }

		public int DaysPerYear
        {
	        get
	        {
		        if (daysPerYear == 0)
		        {
			        var commandDispatcher = diContainer.Resolve<CommandDispatcher>();

			        var settings = commandDispatcher.Send(new GetUtilityBillingSettings()).Result;

			        if (settings != null)
			        {
				        daysPerYear = (settings.Basis ?? 0) == 0 ? 365 : (int)settings.Basis;
			        }
		        }

		        return daysPerYear;
	        }
	        set { daysPerYear = value; }
        }

        private IEnumerable<UTBillRecord> MapToUTBillRecord(ref IEnumerable<Bill> bills)
        {
            return bills.Select(bill => new UTBillRecord()
            {
                ID = bill.ID,
                AccountNumber = bill.ActualAccountNumber ?? 0,
                BillNumber = bill.BillNumber ?? 0,
                DaysPerYear = DaysPerYear,
                DueDate = bill.RateKey.DueDate ?? DateTime.MinValue,
                InterestStartDate = bill.RateKey.IntStart ?? DateTime.MinValue,
                OwnerAddress1 = bill.OAddress1,
                OwnerAddress2 = bill.OAddress2,
                OwnerAddress3 = bill.OAddress3,
                OwnerCity = bill.OCity,
                OwnerName = bill.OName,
                OwnerName2 = bill.OName2,
                OwnerState = bill.OState,
                OwnerZip = bill.OZip,
                OwnerZip4 = bill.OZip4,
                TenantAddress1 = bill.BAddress1,
                TenantAddress2 = bill.BAddress2,
                TenantAddress3 = bill.BAddress3,
                TenantCity = bill.BCity,
                TenantName = bill.BName,
                TenantName2 = bill.BName2,
                TenantState = bill.BState,
                TenantZip = bill.BZip,
                TenantZip4 = bill.BZip4,
                ServiceCode = bill.Service,
                SewerUtilityAmount = GetUtilityAmounts(bill, UtilityType.Sewer),
                SewerInterestPaidDate = bill.SIntPaidDate ?? DateTime.MinValue,
                SewerInterestRate = (bill.RateKey.SIntRate ?? 0).ToDecimal(),
                SewerLienID = bill.SLienRecordNumber ?? 0,
                SewerCombinationLienKey = bill.SCombinationLienKey.ToString(),
                WaterUtilityAmount = GetUtilityAmounts(bill, UtilityType.Water),
                WaterInterestPaidDate = bill.WIntPaidDate ?? DateTime.MinValue,
                WaterInterestRate = (bill.RateKey.WIntRate ?? 0).ToDecimal(),
                WaterLienID = bill.WLienRecordNumber ?? 0,
                SewerLien = (bill.SLienRecordNumber ?? 0) > 0 ? GetLienByID(bill.SLienRecordNumber ?? 0).MapToUTLien() : null,
                WaterLien = (bill.WLienRecordNumber ?? 0) > 0 ? GetLienByID(bill.WLienRecordNumber ?? 0).MapToUTLien() : null
            });
        }

        IEnumerable<UTBillRecord> IDataLayer<UTBillRecord>.GetUnpaidBillsForAccount(int accountNumber)
        {
            var bills = GetUnpaidBillRecordsForAccount(accountNumber);
            return MapToUTBillRecord(ref bills);
        }

        private UtilityAmount GetUtilityAmounts(Bill bill, UtilityType type)
        {
            var utilityAmount = new UtilityAmount();

            utilityAmount.PrincipalPaid = (type == UtilityType.Sewer ? bill.SPrinPaid ?? 0 : bill.WPrinPaid ?? 0).ToDecimal();
            utilityAmount.CostBilled = (type == UtilityType.Sewer ? bill.SCostOwed ?? 0 : bill.WCostOwed ?? 0).ToDecimal();
            utilityAmount.CostAdded = (type == UtilityType.Sewer ? bill.SCostAdded ?? 0 : bill.WCostAdded ?? 0).ToDecimal() * -1m;
            utilityAmount.CostPaid = (type == UtilityType.Sewer ? bill.SCostPaid ?? 0 : bill.WCostPaid ?? 0).ToDecimal();
            utilityAmount.InterestBilled = (type == UtilityType.Sewer ? bill.SIntOwed ?? 0 : bill.WIntOwed ?? 0).ToDecimal();
            utilityAmount.InterestAdded = (type == UtilityType.Sewer ? bill.SIntAdded ?? 0 : bill.WIntAdded ?? 0).ToDecimal() * -1m;
            utilityAmount.InterestPaid = (type == UtilityType.Sewer ? bill.SIntPaid ?? 0 : bill.WIntPaid ?? 0).ToDecimal();
            utilityAmount.SalesTaxBilled = (type == UtilityType.Sewer ? bill.STaxOwed ?? 0 : bill.WTaxOwed ?? 0).ToDecimal();
            utilityAmount.SalesTaxPaid = (type == UtilityType.Sewer ? bill.STaxPaid ?? 0 : bill.WTaxPaid ?? 0).ToDecimal();
            utilityAmount.PrincipalOwed = (type == UtilityType.Sewer ? bill.SPrinOwed ?? 0 : bill.WPrinOwed ?? 0).ToDecimal();
            utilityAmount.InterestPaidDate = (type == UtilityType.Sewer ? bill.SIntPaidDate ?? DateTime.MinValue : bill.WIntPaidDate ?? DateTime.MinValue); 
            utilityAmount.InterestRate = (type == UtilityType.Sewer ? bill.RateKey.SIntRate ?? 0 : bill.RateKey.WIntRate ?? 0).ToDecimal();

            return utilityAmount;
        }
    }

    public static class UTDataRowExtensions
    {
        public static UTLien MapToUTLien(this Lien lien)
        {
            return new UTLien()
            {
                ID = lien.ID,
                Costs = (lien.Costs ?? 0).ToDecimal(),
                CostsPaid = (lien.CostPaid ?? 0).ToDecimal(),
                DueDate = lien.RateKey.DueDate ?? DateTime.MinValue,
                InterestStartDate = lien.RateKey.IntStart ?? DateTime.MinValue,
                InterestAdded = (lien.IntAdded ?? 0).ToDecimal(),
                InterestPaid = (lien.IntPaid ?? 0).ToDecimal(),
                Interest = (lien.Interest ?? 0).ToDecimal(),
                InterestPaidDate = lien.IntPaidDate ?? DateTime.MinValue,
                MaturityFee = (lien.MaturityFee ?? 0).ToDecimal(),
                WaterInterestRate = (lien.RateKey.WIntRate ?? 0).ToDecimal(),
                SewerInterestRate = (lien.RateKey.SIntRate ?? 0).ToDecimal(),
                PrincipalPaid = (lien.PrinPaid ?? 0).ToDecimal(),
                PreLienInterestPaid = (lien.PLIPaid ?? 0).ToDecimal(),
                Principal = (lien.Principal ?? 0).ToDecimal(),
                Tax = (lien.Tax ?? 0).ToDecimal(),
                TaxPaid = (lien.TaxPaid ?? 0).ToDecimal()

            };
        }
    }
}
