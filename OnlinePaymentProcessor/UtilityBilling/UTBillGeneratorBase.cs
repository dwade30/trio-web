﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Models;
using SharedApplication.Extensions;

namespace OnlinePaymentProcessor.UtilityBilling
{
    public class UTBillGeneratorBase
    {
        protected int daysPerYear = 365;
        public int DaysPerYear { get; set; }
        protected ExportDetail GetExportDetail(Bill bill, Account utAccount, Settings payportSettings)
        {
            if (bill.BillKey != 0 || bill.IsLien)
            {
                var exportDetail = new ExportDetail();
                exportDetail.AccountNumber = bill.AccountNumber;
                exportDetail.BillKey = (payportSettings.CombineUtilityServices ? (payportSettings.CombinedUtilityType.ToUpper() == "WATER" ? "WA" :
	                payportSettings.CombinedUtilityType.ToUpper() == "SEWER" ? "SW" : "UT") : bill.BillType.ToBillTypeCode()) + bill.BillKey;
                 exportDetail.AccountNumber = utAccount.AccountNumber;
                exportDetail.Owner1FName = utAccount.Owner1FName;
                exportDetail.Owner1LName = utAccount.Owner1LName;
                exportDetail.Owner2FName = utAccount.Owner2FName;
                exportDetail.Owner2LName = utAccount.Owner2LName;
                exportDetail.OwnerMailing1 = utAccount.OwnerAddress1;
                exportDetail.OwnerMailing2 = utAccount.OwnerAddress2;
                exportDetail.OwnerMailing3 = utAccount.OwnerAddress3;
                exportDetail.OwnerMailingCity = utAccount.OwnerCity;
                exportDetail.OwnerMailingState = utAccount.OwnerState.Length > 2 ? utAccount.OwnerState.Left(2) : utAccount.OwnerState;
                exportDetail.OwnerMailingCountry = utAccount.OwnerCountry;
                exportDetail.OwnerMailingZip = utAccount.OwnerZip;
                exportDetail.Tenant1FName = utAccount.Tenant1FName;
                exportDetail.Tenant1LName = utAccount.Tenant1LName;
                exportDetail.Tenant2FName = utAccount.Tenant2FName;
                exportDetail.Tenant2LName = utAccount.Tenant2LName;
                exportDetail.TenantMailing1 = utAccount.TenantAddress1;
                exportDetail.TenantMailing2 = utAccount.TenantAddress2;
                exportDetail.TenantMailing3 = utAccount.TenantAddress3;
                exportDetail.TenantMailingCity = utAccount.TenantCity;
                exportDetail.TenantMailingState = utAccount.TenantState.Length > 2 ? utAccount.TenantState.Left(2) : utAccount.TenantState;
                exportDetail.TenantMailingCountry = utAccount.TenantCountry;
                exportDetail.TenantMailingZip = utAccount.TenantZip;
                exportDetail.MapLot = utAccount.MapLot;
                exportDetail.LocationNumber = utAccount.LocationNumber;
                exportDetail.LocationStreet = utAccount.LocationStreet;
                exportDetail.BillNumber = bill.BillNumber.ToString();
                exportDetail.BillType = payportSettings.CombineUtilityServices
	                ? (payportSettings.CombinedUtilityType.ToUpper() == "WATER" ? "WA" :
		                payportSettings.CombinedUtilityType.ToUpper() == "SEWER" ? "SW" : "UT")
	                : bill.BillType.ToBillTypeCode();
                exportDetail.IsLien = bill.IsLien;
                exportDetail.InterestRate = bill.InterestRate;
                exportDetail.InterestDue = bill.TotalInterestDue;
                exportDetail.CostDue = bill.TotalCostDue;
                exportDetail.SalesTaxDue = bill.TotalSalesTaxDue;
                exportDetail.DiscountAmount = bill.DiscountAmount;
                exportDetail.TotalCurrentDue = bill.TotalAmountDue;
                Period p = bill.Periods[0];
                if (p != null)
                {
                    exportDetail.Period1DueDate = p.DueDate;
                    exportDetail.Period1InterestDate = p.InterestDate;
                    exportDetail.Period1PrincipalDue = p.PrincipalDue;
                }
                else
                {
                    exportDetail.Period1DueDate = DateTime.MinValue;
                    exportDetail.Period1InterestDate = DateTime.MinValue;
                    exportDetail.Period1PrincipalDue = 0m;
                }

                exportDetail.Period2DueDate = DateTime.MinValue;
                exportDetail.Period2InterestDate = DateTime.MinValue;
                exportDetail.Period2PrincipalDue = 0m;

                exportDetail.Period3DueDate = DateTime.MinValue;
                exportDetail.Period3InterestDate = DateTime.MinValue;
                exportDetail.Period3PrincipalDue = 0m;

                exportDetail.Period4DueDate = DateTime.MinValue;
                exportDetail.Period4InterestDate = DateTime.MinValue;
                exportDetail.Period4PrincipalDue = 0m;
                return exportDetail;
            }

            return null;
        }
        protected (Bill bill, Decimal previousBalance) GetCurrentBillAndPrevBalanceForBillType(ref IEnumerable<Bill> bills, BillType billType)
        {
            if (bills.Count(x => x.BillType == billType) > 0)
            {
                Bill bcur = bills.Where(x => x.BillType == billType).OrderBy(y => y.IsLien ? 1 : 0).ThenByDescending(x => x.BillNumber.ToIntegerValue()).First();
                decimal prevBal = bills.Where(x => x.BillNumber != bcur.BillNumber && x.BillType == billType)
                    .Sum(x => x.TotalAmountDue);
                return (bcur, prevBal);
            }

            return (new Bill(), 0);
        }
    }
}
