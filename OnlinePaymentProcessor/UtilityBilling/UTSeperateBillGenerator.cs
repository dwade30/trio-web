﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;
using SharedApplication.Extensions;

namespace OnlinePaymentProcessor.UtilityBilling
{
	public class UTSeparateBillGenerator : UTBillGeneratorBase, IBillGenerator<UTBillRecord>
	{
		private Settings payportSettings;

		public UTSeparateBillGenerator(Settings payportSettings)
		{
			this.payportSettings = payportSettings;
		}

		public List<ExportDetail> GetExportDetailsForAccount(Account utAccount, ref IEnumerable<Bill> bills)
		{
			var sewerBill = GetCurrentBillAndPrevBalanceForBillType(ref bills, BillType.Sewer);
			var waterBill = GetCurrentBillAndPrevBalanceForBillType(ref bills, BillType.Water);
			var exportDetails = new List<ExportDetail>();
			var sewerDetail = GetExportDetail(sewerBill.bill, utAccount, payportSettings);
			var waterDetail = GetExportDetail(waterBill.bill, utAccount, payportSettings);
			if (sewerDetail != null)
			{
				sewerDetail.PrevBalDue = sewerBill.previousBalance;
				exportDetails.Add(sewerDetail);
			}

			if (waterDetail != null)
			{
				waterDetail.PrevBalDue = waterBill.previousBalance;
				exportDetails.Add(waterDetail);
			}
			return exportDetails;
		}

		IEnumerable<Bill> IBillGenerator<UTBillRecord>.GetOutstandingBills(ref IEnumerable<UTBillRecord> billRecords)
		{
			List<Bill> bills = new List<Bill>();
			var distinctSewerBills = billRecords.Where(b => b.ServiceCode != "W" && (b.SewerPrincipalBalance > 0 || (b.IsSewerLien && b.SewerCombinationLienKey.ToIntegerValue() == b.ID)))
				.MapToSewerBill().Where(b => b.TotalAmountDue > 0).GroupBy(bill => bill.BillNumber)
				.Select(grp => grp.First())
				.ToList();
			bills.AddRange(distinctSewerBills);

			var distinctWaterBills = billRecords.Where(b => b.ServiceCode != "S" && !b.IsWaterLien && b.WaterPrincipalBalance > 0)
				.MapToWaterBill().Where(b => b.TotalAmountDue > 0).GroupBy(bill => bill.BillNumber)
				.Select(grp => grp.First())
				.ToList();
			bills.AddRange(distinctWaterBills);

			return bills;
		}

	}
}
