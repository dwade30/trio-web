﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Enums;
using OnlinePaymentProcessor.Extensions;
using OnlinePaymentProcessor.Interfaces;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.UtilityBilling
{
	public class UTCombinedBillGenerator : UTBillGeneratorBase, IBillGenerator<UTBillRecord>
	{
		private Settings payportSettings;

		public UTCombinedBillGenerator(Settings payportSettings)
		{
			this.payportSettings = payportSettings;
		}

		public List<ExportDetail> GetExportDetailsForAccount(Account account, ref IEnumerable<Bill> bills)
		{
			var combinedBill = GetCurrentBillAndPrevBalanceForBillType(ref bills, BillType.UtilityBilling);
			var exportDetails = new List<ExportDetail>();
			var combinedDetail = GetExportDetail(combinedBill.bill, account, payportSettings);
			if (combinedDetail != null)
			{
				combinedDetail.PrevBalDue = combinedBill.previousBalance;
				exportDetails.Add(combinedDetail);
			}
			return exportDetails;
		}

		public IEnumerable<Bill> GetOutstandingBills(ref IEnumerable<UTBillRecord> billRecords)
		{
			List<Bill> bills = new List<Bill>();

			var distinctBills = billRecords.Where(b => b.TotalPrincipalBalance > 0)
				.MapToCombinedBill().Where(b => b.TotalAmountDue > 0).GroupBy(bill => bill.BillNumber)
				.Select(grp => grp.First())
				.ToList();

			bills.AddRange(distinctBills);
			return bills;
		}
	}
}
