﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;
using SharedApplication;
using SharedDataAccess;

namespace OnlinePaymentProcessor
{
	class Program
	{
		public static void Main(string[] args)
		{
			string client = "";
			string dataEnvironment = "";
			bool testMode = false;
			string configPath = System.IO.Directory.GetCurrentDirectory();

			if (args.Length > 0)
			{
				int x = 0;

				do
				{
					switch (args[x])
					{
						case "-de":
							x += 1;
							if (x < args.Length)
							{
								dataEnvironment = args[x].Trim();
							}

							break;
						case "-client":
							x += 1;
							if (x < args.Length)
							{
								client = args[x].Trim();
							}

							break;
						case "-cp":
							x += 1;
							if (x < args.Length)
							{
								configPath = args[x].Trim();
							}

							break;
						case "-test":
							testMode = true;
							break;
					}

					x += 1;
				} while (x < args.Length);

			}
			
			var webSetup = new WebEnvironmentSetup();
			var processor = new PaymentProcessing();

			webSetup.WebConfigLocation = configPath;

			var result = webSetup.VerifyConfigFileLocation();

			if (result.success)
			{
				processor.ProcessPaymentFiles(webSetup.LoadGlobalSettings(dataEnvironment, client), testMode);
			}
			else
			{
				Logger.Log(result.issueDescription, EventLogEntryType.Error);
			}
			
		}
    }
}
