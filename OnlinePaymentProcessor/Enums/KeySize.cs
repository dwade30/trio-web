﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Enums
{
	public enum KeySize
	{
		Bits128,
		Bits192,
		Bits256
	}
}
