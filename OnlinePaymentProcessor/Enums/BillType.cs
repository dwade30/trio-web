﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinePaymentProcessor.Enums
{
	public enum BillType
	{
		Unknown,
		Sewer,
		Water,
		RealEstate,
		PersonalPropery,
		UtilityBilling
	}
}
