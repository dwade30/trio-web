﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Interfaces
{
	public interface INetSync
	{
		int UploadFile(string filename);
		IEnumerable<Payment> DownloadPayments(string queryString);
	}
}
