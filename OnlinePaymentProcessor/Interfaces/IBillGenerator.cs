﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Interfaces
{
	public interface IBillGenerator<TBillType>
	{
		List<ExportDetail> GetExportDetailsForAccount(Account account, ref IEnumerable<Bill> bills);
		IEnumerable<Bill> GetOutstandingBills(ref IEnumerable<TBillType> billRecords);
		int DaysPerYear { get; set; }
	}
}
