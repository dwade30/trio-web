﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlinePaymentProcessor.Models;

namespace OnlinePaymentProcessor.Interfaces
{
	public interface IDataLayer<out TRecordType>
	{
		IEnumerable<TRecordType> GetUnpaidBillsForAccount(int accountNumber);
		IEnumerable<Account> GetAccounts();
	}
}
