﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.Extensions.Logging;
using SharedApplication;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Enums;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using SharedDataAccess;
using SharedDataAccess.Extensions;
using TRIOWebUpdateService;

namespace TRIOWebUpdateWindowsService
{
	public partial class Scheduler : ServiceBase
	{
		private Timer checkTimer = new Timer();
		private Autofac.IContainer diContainer;
		private cGlobalSettings gGlobalSettings;
		private IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>> scheduledUpdatesQueryHandler;
		private CommandDispatcher commandDispatcher;
		private string downloadPath = "";

		public Scheduler()
		{
			InitializeComponent();
		}

		internal void TestStartupAndStop(string[] args)
		{
			this.OnStart(args);
			Console.ReadLine();
			this.OnStop();
		}

		protected override void OnStart(string[] args)
		{
			var webSetup = new WebEnvironmentSetup();

			webSetup.WebConfigLocation = ConfigurationManager.AppSettings["ConfigLocation"];

			var result = webSetup.VerifyConfigFileLocation();
			Logger.Log("Verify Config Info Result - " + result.success + "   " + result.issueDescription, EventLogEntryType.Information);
			if (result.success)
			{
				Logger.Log("Load Global Settings", EventLogEntryType.Information);
				gGlobalSettings = webSetup.LoadGlobalSettings("", ConfigurationManager.AppSettings["client"]);

				Logger.Log("Wire Up Dependencies", EventLogEntryType.Information);
				WireUpDependencies();

				downloadPath = Path.Combine(ConfigurationManager.AppSettings["ConfigLocation"], "SiteUpdate");
				Logger.Log("Set Download Path - " + downloadPath, EventLogEntryType.Information);
				
				Logger.Log("Create SiteUpdate Directory", EventLogEntryType.Information);
				CreateUpdateDirectory(downloadPath);

				Logger.Log("Get Scheudled Updates", EventLogEntryType.Information);
				scheduledUpdatesQueryHandler = diContainer.Resolve<IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>>>();
				Logger.Log("Get Command Dispatcher", EventLogEntryType.Information);
				commandDispatcher = diContainer.Resolve<CommandDispatcher>();

				checkTimer.Interval = 60000;
				checkTimer.Elapsed += OnTimedEvent;
				checkTimer.AutoReset = true;
				checkTimer.Enabled = true;
				checkTimer.Start();
				Logger.Log("Start Timer", EventLogEntryType.Information);
			}
			else
			{
				Logger.Log(result.issueDescription, EventLogEntryType.Error);
				Stop();
			}
		}

		private void CreateUpdateDirectory(string updateFolder)
		{
			//string updateFolder = "SiteUpdate";
			if (!Directory.Exists(updateFolder))
			{
				Directory.CreateDirectory(updateFolder);
			}
		}

		private void WireUpDependencies()
		{
			var builder = new ContainerBuilder();

			builder.RegisterInstance(gGlobalSettings).As<cGlobalSettings>();
			builder.RegisterInstance(gGlobalSettings.ToDataContextDetails()).As<DataContextDetails>();
			builder.RegisterType<TelemetryDummy>().As<ITelemetryService>();
			builder.Register(f => new UserInformation
			{
				UserId = "SuperUser",
				Id = -1
			}).As<UserInformation>();
			builder.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
			builder.RegisterAssemblyModules(AppDomain.CurrentDomain.GetAssemblies());
			diContainer = builder.Build();
		}

		private void OnTimedEvent(Object source, ElapsedEventArgs e)
		{
			Logger.Log("Timer Triggered", EventLogEntryType.Information);
			var upcomingUpdate = scheduledUpdatesQueryHandler.ExecuteQuery(new ScheduledUpdateSearchCriteria
			{
				CompletedSelection = ScheduledUpdateCompletedOptions.NotCompletedOnly
			}).FirstOrDefault();

			Logger.Log("Scheduled Update Records Found - " + (upcomingUpdate != null ? "True" : "False"), EventLogEntryType.Information);
			if (upcomingUpdate != null)
			{

				Logger.Log("Check to see if Update is in the future", EventLogEntryType.Information);
				if (DateTime.Compare(upcomingUpdate.ScheduledDateTime, DateTime.Now) < 0)
				{
					Logger.Log("Create New UpdateAgent", EventLogEntryType.Information);
					var agent = new UpdateAgent(downloadPath, commandDispatcher);

					Logger.Log("Update Site", EventLogEntryType.Information);
					var updateStatus = agent.UpdateSite();

					Logger.Log("Update Scheduled Update to show Processed", EventLogEntryType.Information);
					if (updateStatus.result)
					{
						var command = new SaveScheduledUpdate();

						command.updateToSave = upcomingUpdate;
						command.updateToSave.Completed = true;
						command.updateToSave.UpdateResult = updateStatus.status;
						command.Version = updateStatus.status;
						commandDispatcher.Send(command);
					}
					Logger.Log("End Update Process", EventLogEntryType.Information);
				}
			}
		}

		protected override void OnStop()
		{
			checkTimer.Stop();
			Logger.Log("Stop Timer", EventLogEntryType.Information);
		}
	}
}
