﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Timers;
using Autofac;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using MediatR.Extensions.Autofac.DependencyInjection;
using SharedApplication;
using SharedApplication.ClientSettings;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Enums;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using SharedDataAccess;
using SharedDataAccess.Extensions;

namespace TRIOWebUpdateService
{
	public class UpdateAgent
	{
		private CommandDispatcher commandDispatcher;
		private string downloadPath = "";
		public UpdateAgent(string downloadPath, CommandDispatcher commandDispatcher)
		{
			this.commandDispatcher = commandDispatcher;
			this.downloadPath = downloadPath;
		}

		private void ClearUpdateDirectory()
		{
			DirectoryInfo directory = new DirectoryInfo(downloadPath);

			foreach (FileInfo file in directory.GetFiles())
			{
				file.Delete();
			}

			foreach (DirectoryInfo dir in directory.GetDirectories())
			{
				dir.Delete(true);
			}
		}

		public (bool result, string status) UpdateSite()
		{
			try
			{
				BlobServiceClient blobServiceClient = new BlobServiceClient(ConfigurationManager.AppSettings["StorageConnectionString"]);
				BlobContainerClient container = blobServiceClient.GetBlobContainerClient("triowebupdates");

				BlobItem blobItem = container.GetBlobs().OrderBy(x => x.Name).FirstOrDefault();
				BlobClient blobClient = container.GetBlobClient(blobItem.Name);

				MemoryStream memStream = new MemoryStream();

				string downloadFilePath = Path.Combine(downloadPath, blobItem.Name);
				string version = blobItem.Name.Left(blobItem.Name.Length - 4);
				BlobDownloadInfo download = blobClient.Download();

				ClearUpdateDirectory();

				using (FileStream downloadFileStream = File.OpenWrite(downloadFilePath))
				{
					download.Content.CopyTo(downloadFileStream);
					downloadFileStream.Close();
				}
                
				ZipFile.ExtractToDirectory(downloadFilePath, downloadPath);

				var command = Path.Combine(downloadPath, "Main.Deploy.cmd");

				Process p = new Process();
				p.StartInfo.FileName = command;
				p.StartInfo.Arguments = "/Y";
				p.StartInfo.UseShellExecute = false;
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.RedirectStandardOutput = true;
				p.StartInfo.Verb = "runas";
				p.Start();
				p.WaitForExit(10000);
				string result = p.StandardOutput.ReadToEnd();
				// Display the command output.
				Console.WriteLine(result);
				Logger.Log(result, EventLogEntryType.Information);

				return (true, version);
			}
			catch (Exception e)
			{
				Logger.Log(e.Message, EventLogEntryType.Error);
				return (false, e.Message);
			}
		}
	}
}
