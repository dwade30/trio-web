﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using NSubstitute;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedDataAccess;
using SharedDataAccess.Budgetary;
using SharedDataAccess.CentralData;
using SharedDataAccess.CentralParties;
using SharedDataAccess.Clerk;
using TWSharedLibrary;
using Xunit;

namespace TWSharedTests
{
	public class AccountValidationServiceTests
	{
		[Fact]
		public void CheckValidAccountGivenValidAccountWithValidAccountCheckOnlyAcceptngValidAccounts()
		{
			//var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-9999-00";

			
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "9999",
				FourthAccountField = "00",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = true,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "9999",
					FourthAccountField = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.CheckValidAccount(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void CheckValidAccountGivenInvalidValidAccountWithValidAccountCheckOnlyAcceptngValidAccounts()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-9999-01";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = true,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "9999",
					FourthAccountField = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.CheckValidAccount(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void CheckValidAccountGivenInvalidValidAccountWithValidAccountCheckAcceptngAnyExistingAccounts()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-9999-00";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "9999",
				FourthAccountField = "00",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "9999",
					FourthAccountField = "00",
					Account = testAccount
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.CheckValidAccount(testAccount);
				Assert.True(result == true);

				var record = budgetaryContext.AccountMasters.FirstOrDefault();
				Assert.True(record.Account == testAccount);
				Assert.True(record.Valid == true);
			}
		}

		[Fact]
		public void CheckValidAccountGivenAccountNotInAccountMasterWithValidAccountCheckAcceptngAnyExistingAccounts()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-9999-00";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "9999",
				FourthAccountField = "00",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.CheckValidAccount(testAccount);
				Assert.True(result == true);

				var record = budgetaryContext.AccountMasters.FirstOrDefault();
				Assert.True(budgetaryContext.AccountMasters.Count() == 1);
				Assert.True(record.Account == testAccount);
				Assert.True(record.AccountType == "E");
				Assert.True(record.FirstAccountField == "10");
				Assert.True(record.SecondAccountField == "100");
				Assert.True(record.ThirdAccountField == "9999");
				Assert.True(record.FourthAccountField == "00");
				Assert.True(record.FifthAccountField == "");
				Assert.True(record.Valid == true);
			}
		}

		[Fact]
		public void ValidLedgerReturnsTrueWithValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "G 10-100-00";
			
			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "G",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "00",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "G",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "00",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidLedger(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidLedgerReturnsFalseWithInValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "G 10-100-00";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "G",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "00",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidLedger(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidLedgerReturnsFalseWithNonExistantAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "G 10-100-00";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = true,
					AccountType = "G",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "00",
					FourthAccountField = "",
					Account = testAccount
				});
				
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidLedger(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidRevenueReturnsTrueWithValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "R 10-100-29";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidRevenueReturnsFalseWithInValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "R 10-100-29";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidRevenueReturnsFalseWithNonExistantAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "R 10-100-29";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = true,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidRevenueReturnsFalseWithNoDivisionExisting()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "R 10-100-29";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidRevenueReturnsFalseWithNoDepartmentExisting()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.RevenueZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "R 10-29";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "29",
					ThirdAccountField = "",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "",
					Revenue = "29"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidRevenueReturnsTrueWithNoDivisionExistingNoDivInRevenues()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "R 10-29";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.RevenueZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = true,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "29",
					ThirdAccountField = "",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidRevenue(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidExpenseReturnsTrueWithValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidExpenseNoDivReturnsTrueWithValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("0");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "10",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "29",
					ThirdAccountField = "10",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "0"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidExpenseNoDivObjReturnsTrueWithValidAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-29";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("0");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("0");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "29",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "29",
					ThirdAccountField = "",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "0"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "0"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void ValidExpenseReturnsFalseWithInvalidObject()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-29-10";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "11"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidExpenseReturnsFalseWithInvalidExpense()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-29-10";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "28",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "28",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidExpenseReturnsFalseWithInvalidDivision()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-29-10";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "999"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void ValidExpenseReturnsFalseWithInvalidDepartment()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-29-10";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "20",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "20",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.ValidExpense(testAccount);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidExpenseAccountNotRegional()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidRevenueAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "R 10-100-29";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);
			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "R",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidLedgerAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "G 10-100-00";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "G",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "00",
				FourthAccountField = "",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "G",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "00",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidMAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			regionalTownService.IsRegionalTown().Returns(true);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "M TESTING";
				
				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == true);
			}
		}

		[Fact]
		public void AccountValidateReturnsFalseWithInvalidExpenseAccountNotRegional()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			regionalTownService.IsRegionalTown().Returns(true);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "E 10-100-29-10";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void AccountValidateReturnsFalseWithInvalidRevenueAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);
			regionalTownService.IsRegionalTown().Returns(true);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "R 10-100-29";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "R",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.RevTitles.Add(new RevTitle
				{
					Department = "10",
					Division = "100",
					Revenue = "29"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void AccountValidateReturnsFalseWithInvalidLedgerAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.ValidAccountsOnly);
			regionalTownService.IsRegionalTown().Returns(true);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "G 10-100-00";
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "G",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "00",
					FourthAccountField = "",
					Account = testAccount
				});

				budgetaryContext.LedgerTitles.Add(new LedgerTitle
				{
					Fund = "10",
					Account = "100",
					Year = "00"
				});
				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void AccountValidateReturnsFalseWithInvalidMAccount()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			regionalTownService.IsRegionalTown().Returns(true);

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				var testAccount = "M";

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, false);
				Assert.True(result == false);
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidExpenseAccountIsRegionalDepartmentBreakdownCode()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 010-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(3);

			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetDepartment(testAccount).Returns("010");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "010",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});
			budgetaryAccountingService.GetAccountBreakDownInformation("E 210-100-29-10").Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "210",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});
			budgetaryAccountingService.GetAccountBreakDownInformation("E 310-100-29-10").Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "310",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});
			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				
				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "110",
					Division = "000",
					BreakdownCode = 2
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "210",
					Division = "100"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "310",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
				{
					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 3,
						Percentage = 20
					});

					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 2,
						Percentage = 80
					});

					centralDataContext.SaveChanges();

					trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);
					trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

					var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);

					var result = service.AccountValidate(testAccount, true);
					Assert.True(result == true);
				}
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidExpenseAccountIsRegionalExpenseBreakdownCode()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 010-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(3);

			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetDepartment(testAccount).Returns("010");
			budgetaryAccountingService.GetExpense(testAccount).Returns("29");
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "010",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});
			budgetaryAccountingService.GetAccountBreakDownInformation("E 210-100-29-10").Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "210",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});
			budgetaryAccountingService.GetAccountBreakDownInformation("E 310-100-29-10").Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "310",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "210",
					Division = "100"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "310",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00",
					BreakdownCode = 2
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
				{
					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 3,
						Percentage = 20
					});

					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 2,
						Percentage = 80
					});

					centralDataContext.SaveChanges();

					trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);
					trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

					var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);

					var result = service.AccountValidate(testAccount, true);
					Assert.True(result == true);
				}
			}
		}

		[Fact]
		public void AccountValidateReturnsFalseWithInvalidExpenseAccountIsRegionalExpenseBreakdownCode()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 010-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(3);

			regionalTownService.IsRegionalTown().Returns(true);
			budgetaryAccountingService.GetDepartment(testAccount).Returns("010");
			budgetaryAccountingService.GetExpense(testAccount).Returns("29");

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "210",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00",
					BreakdownCode = 2
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				using (var centralDataContext = new CentralDataContext(GetDBOptions<CentralDataContext>()))
				{
					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 3,
						Percentage = 20
					});

					centralDataContext.RegionalTownBreakdowns.Add(new RegionalTownBreakdown
					{
						Code = 2,
						TownNumber = 2,
						Percentage = 80
					});

					centralDataContext.SaveChanges();

					trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);
					trioContextFactory.GetCentralDataContext().Returns(centralDataContext);

					var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);

					var result = service.AccountValidate(testAccount, true);
					Assert.True(result == false);
				}
			}
		}

		[Fact]
		public void AccountValidateReturnsTrueWithValidExpenseAccountNotRegionalMultiAccountCheck()
		{
			var contextFactoryService = Substitute.For<IContextFactoryService>();
			var regionalTownService = Substitute.For<IRegionalTownService>();
			var budgetaryAccountingService = Substitute.For<IBudgetaryAccountingService>();
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var trioContextFactory = Substitute.For<ITrioContextFactory>();

			var testAccount = "E 10-100-29-10";

			contextFactoryService.GetContextFactory().Returns(trioContextFactory);
			globalBudgetaryAccountSettings.ValidAcctCheck.Returns(ValidAccountSetting.AnyExistingAccount);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ExpenseZeroDivisionString().Returns("000");
			globalBudgetaryAccountSettings.ObjectZeroString().Returns("00");
			regionalTownService.IsRegionalTown().Returns(false);
			budgetaryAccountingService.GetAccountBreakDownInformation(testAccount).Returns(new AccountBreakdownInformation
			{
				Account = testAccount,
				AccountType = "E",
				FirstAccountField = "10",
				SecondAccountField = "100",
				ThirdAccountField = "29",
				FourthAccountField = "10",
				FifthAccountField = ""
			});

			using (var budgetaryContext = new BudgetaryContext(GetDBOptions<BudgetaryContext>()))
			{
				budgetaryContext.AccountMasters.Add(new AccountMaster
				{
					Valid = false,
					AccountType = "E",
					FifthAccountField = "",
					FirstAccountField = "10",
					SecondAccountField = "100",
					ThirdAccountField = "29",
					FourthAccountField = "10",
					Account = testAccount
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "000"
				});

				budgetaryContext.DeptDivTitles.Add(new DeptDivTitle
				{
					Department = "10",
					Division = "100"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "00"
				});

				budgetaryContext.ExpObjTitles.Add(new ExpObjTitle
				{
					Expense = "29",
					Object = "10"
				});

				budgetaryContext.SaveChanges();

				trioContextFactory.GetBudgetaryContext().Returns(budgetaryContext);

				var service = new AccountValidationService(trioContextFactory, regionalTownService, budgetaryAccountingService, globalBudgetaryAccountSettings);
				var result = service.AccountValidate(testAccount, true);
				Assert.True(result == true);
			}
		}

		private DbContextOptions<TContext> GetDBOptions<TContext>() where TContext : DbContext
		{
			var builder = new DbContextOptionsBuilder<TContext>();
			builder.UseInMemoryDatabase(Guid.NewGuid().ToString()); //the database name is set to a unique Guid


			return builder.Options;
		}
	}
}
