﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using SharedApplication;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Messaging;
using TWSharedLibrary;
using Xunit;

namespace TWSharedTests
{
	public class BudgetaryAccountingServiceTests
	{
		[Fact]
		public void GetDepartmentReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
		    var commandDispatcher = Substitute.For<CommandDispatcher>();

		var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);

			var result = service.GetDepartment("E 01-002-9999-00");

			Assert.True(result == "01");
		}

		[Fact]
		public void GetExpenseWithDivisionReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);

			var result = service.GetExpense("E 01-100-9999-00");

			Assert.True(result == "9999");
		}

		[Fact]
		public void GetExpenseWithNoDivisionReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(0);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);

			var result = service.GetExpense("E 01-9999-00");

			Assert.True(result == "9999");
		}

		[Fact]
		public void GetAccountBreakdownReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.ExpenseObjectLength().Returns(2);

			string testAccout = "E 01-002-9999-00";

			var result = service.GetAccountBreakDownInformation(testAccout);

			Assert.True(result.AccountType == "E");
			Assert.True(result.Account == testAccout);
			Assert.True(result.FirstAccountField == "01");
			Assert.True(result.SecondAccountField == "002");
			Assert.True(result.ThirdAccountField == "9999");
			Assert.True(result.FourthAccountField == "00");

		}

		[Fact]
		public void ConvertAccountBreakdownToExpenseBreakdownReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.ExpenseObjectLength().Returns(2);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(true);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);

			string testAccout = "E 01-002-9999-00";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "E",
				FirstAccountField = "01",
				SecondAccountField = "002",
				ThirdAccountField = "9999",
				FourthAccountField = "00",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToExpenseAccountBreakdown(accountBreakdown);

			Assert.True(result.Department == "01");
			Assert.True(result.Division == "002");
			Assert.True(result.Expense == "9999");
			Assert.True(result.Object == "00");

		}

		[Fact]
		public void ConvertAccountBreakdownToExpenseBreakdownNoDivisionReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.ExpenseObjectLength().Returns(2);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(true);

			string testAccout = "E 01-9999-00";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "E",
				FirstAccountField = "01",
				SecondAccountField = "9999",
				ThirdAccountField = "00",
				FourthAccountField = "",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToExpenseAccountBreakdown(accountBreakdown);

			Assert.True(result.Department == "01");
			Assert.True(result.Division == "");
			Assert.True(result.Expense == "9999");
			Assert.True(result.Object == "00");

		}

		[Fact]
		public void ConvertAccountBreakdownToExpenseBreakdownNoDivisionNoObjectReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.ExpenseDepartmentLength().Returns(2);
			globalBudgetaryAccountSettings.ExpenseDivisionLength().Returns(3);
			globalBudgetaryAccountSettings.ExpenseExpenseLength().Returns(4);
			globalBudgetaryAccountSettings.ExpenseObjectLength().Returns(2);
			globalBudgetaryAccountSettings.DivisionExistsInExpense().Returns(false);
			globalBudgetaryAccountSettings.ObjectExistsInExpense().Returns(false);

			string testAccout = "E 01-9999";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "E",
				FirstAccountField = "01",
				SecondAccountField = "9999",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToExpenseAccountBreakdown(accountBreakdown);

			Assert.True(result.Department == "01");
			Assert.True(result.Division == "");
			Assert.True(result.Expense == "9999");
			Assert.True(result.Object == "");

		}

		[Fact]
		public void ConvertAccountBreakdownToRevenueBreakdownReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(true);

			string testAccout = "R 01-002-9999";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "R",
				FirstAccountField = "01",
				SecondAccountField = "002",
				ThirdAccountField = "9999",
				FourthAccountField = "",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToRevenueAccountBreakdown(accountBreakdown);

			Assert.True(result.Department == "01");
			Assert.True(result.Division == "002");
			Assert.True(result.Revenue == "9999");
		}

		[Fact]
		public void ConvertAccountBreakdownToRevenueBreakdownNoDivisionReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.DivisionExistsInRevenue().Returns(false);

			string testAccout = "R 01-9999";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "R",
				FirstAccountField = "01",
				SecondAccountField = "9999",
				ThirdAccountField = "",
				FourthAccountField = "",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToRevenueAccountBreakdown(accountBreakdown);

			Assert.True(result.Department == "01");
			Assert.True(result.Division == "");
			Assert.True(result.Revenue == "9999");
		}

		[Fact]
		public void ConvertAccountBreakdownToLedgerBreakdownReturnCorrectValue()
		{
			var globalBudgetaryAccountSettings = Substitute.For<IGlobalBudgetaryAccountSettings>();
			var deptDivTitleQueryHandler = Substitute.For<IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>>>();
			var ledgerTitleQueryHandler = Substitute.For<IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>>>();
			var commandDispatcher = Substitute.For<CommandDispatcher>();

			var service = new BudgetaryAccountingService(globalBudgetaryAccountSettings, deptDivTitleQueryHandler, ledgerTitleQueryHandler, commandDispatcher);

			globalBudgetaryAccountSettings.SuffixExistsInLedger().Returns(true);

			string testAccout = "G 01-9999-00";
			AccountBreakdownInformation accountBreakdown = new AccountBreakdownInformation
			{
				Account = testAccout,
				AccountType = "G",
				FirstAccountField = "01",
				SecondAccountField = "9999",
				ThirdAccountField = "00",
				FourthAccountField = "",
				FifthAccountField = "",
				Valid = false
			};

			var result = service.ConvertAccountBreakdownToLedgerAccountBreakdown(accountBreakdown);

			Assert.True(result.Fund == "01");
			Assert.True(result.Account == "9999");
			Assert.True(result.Suffix == "00");
		}

	}
}
