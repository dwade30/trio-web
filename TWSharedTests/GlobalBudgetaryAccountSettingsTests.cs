﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using TWSharedLibrary;
using Xunit;

namespace TWSharedTests
{
    public class GlobalBudgetaryAccountSettingsTests
    {
	    [Fact]
	    public void TestHandlingInvalidValidAccountSetting()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;
		    
			service.ValidAcctCheck = (ValidAccountSetting) 5;

		    Assert.True(service.ValidAcctCheck == ValidAccountSetting.NotSet);
	    }

	    [Fact]
	    public void TestHandlingValidValidAccountSetting()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.ValidAcctCheck = (ValidAccountSetting)3;

		    Assert.True(service.ValidAcctCheck == ValidAccountSetting.AnyExistingAccount);
	    }

	    [Fact]
	    public void TestExpenseLengthsForValidValues()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.Exp = "03000204";

		    Assert.True(service.ExpenseDepartmentLength() == 3);
		    Assert.True(service.ExpenseDivisionLength() == 0);
		    Assert.True(service.ExpenseExpenseLength() == 2);
		    Assert.True(service.ExpenseObjectLength() == 4);
		    Assert.True(service.ExpenseTotalLength() == 9);
		}

	    [Fact]
	    public void TestRevenueLengthsForValidValues()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.Rev = "040203";

		    Assert.True(service.RevenueDepartmentLength() == 4);
		    Assert.True(service.RevenueDivisionLength() == 2);
		    Assert.True(service.RevenueRevenueLength() == 3);
		    Assert.True(service.RevenueTotalLength() == 9);
	    }

	    [Fact]
	    public void TestLedgerLengthsForValidValues()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.Ledger = "010300";

		    Assert.True(service.LedgerFundLength() == 1);
		    Assert.True(service.LedgerAccountLength() == 3);
		    Assert.True(service.LedgerSuffixLength() == 0);
		    Assert.True(service.LedgerTotalLength() == 4);
	    }

	    [Fact]
	    public void TestDivisionObjectYearFlags()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.ExpDivFlag = true;
		    service.RevDivFlag = false;
		    service.ObjFlag = true;
		    service.YearFlag = false;

		    Assert.True(service.DivisionExistsInExpense() == false);
		    Assert.True(service.DivisionExistsInRevenue() == true);
		    Assert.True(service.ObjectExistsInExpense() == false);
		    Assert.True(service.SuffixExistsInLedger() == true);
	    }

	    [Fact]
	    public void TestZeroDivisionWhenDivisionExistsInExpenseNoRevDivision()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.ExpDivFlag = false;
		    service.RevDivFlag = true;
			service.Exp = "02050101";
		    service.Rev = "010302";

			Assert.True(service.RevenueZeroDivisionString() == "00000");
	    }

	    [Fact]
	    public void TestZeroDivisionWhenDivisionExistsInExpenseRevDivisionExists()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.ExpDivFlag = false;
		    service.RevDivFlag = false;
		    service.Exp = "02050101";
		    service.Rev = "010302";

		    Assert.True(service.RevenueZeroDivisionString() == "000");
	    }

	    [Fact]
	    public void TestZeroDivisionWhenDivisionDoesNotExistInExpenseRevDivisionExists()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

		    service.ExpDivFlag = true;
		    service.RevDivFlag = false;
		    service.Exp = "02000101";
		    service.Rev = "010302";

		    Assert.True(service.RevenueZeroDivisionString() == "000");
	    }

		[Fact]
	    public void TestZeroDivisionWhenDivisionDoesNotExistInExpenseNoRevDivision()
	    {
		    var service = StaticSettings.gGlobalBudgetaryAccountSettings;

			service.ExpDivFlag = true;
			service.RevDivFlag = true;
			service.Exp = "02000101";
			service.Rev = "010002";

			Assert.True(service.RevenueZeroDivisionString() == "0");
	    }
	}
}
