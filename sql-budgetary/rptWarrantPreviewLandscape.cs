﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantPreviewLandscape.
	/// </summary>
	public partial class rptWarrantPreviewLandscape : BaseSectionReport
	{
		public static rptWarrantPreviewLandscape InstancePtr
		{
			get
			{
				return (rptWarrantPreviewLandscape)Sys.GetInstance(typeof(rptWarrantPreviewLandscape));
			}
		}

		protected rptWarrantPreviewLandscape _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
				rsJournalInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantPreviewLandscape	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		// vbPorter upgrade warning: curJournalTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalTotal;
		// vbPorter upgrade warning: curJournalEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalEncTotal;
		// vbPorter upgrade warning: curVendorTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorTotal;
		// vbPorter upgrade warning: curVendorEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorEncTotal;
		Decimal curWarrantTotal;
		Decimal curWarrantEncTotal;
		Decimal curPrepaidTotal;
		Decimal curCurrentTotal;
		bool blnFirstRecord;
		bool blnTempVendors;
		//clsDRWrapper rsTempVendorInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		int intTempVendorCounter;
		string strJournalSQL;
		string strSQL = "";
        private WarrantPreviewReportOptions reportOptions;
        private IAccountTitleService accountTitleService;

		public rptWarrantPreviewLandscape()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rptWarrantPreviewLandscape(WarrantPreviewReportOptions options) : this()
        {
            reportOptions = options;
            accountTitleService = StaticSettings.GlobalCommandDispatcher.Send(new GetAccountTitleService()).Result;
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Warrant Preview";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (!reportOptions.EnteredOrder)
				{
					if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						intTempVendorCounter = 0;
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY ID");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY ID");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
				}
				else
				{
					if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						intTempVendorCounter = 0;
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND ID = " + rsVendorInfo.Get_Fields_Int32("ID") + " AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY ID");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND ID = " + rsVendorInfo.Get_Fields_Int32("ID") + " AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY ID");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
				}
				strJournalSQL = rsJournalInfo.Name();
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() == true)
				{
					rsJournalInfo.MoveNext();
					bool executeCheckNextVendor = false;
					bool executeCheckNextJournal = false;
					bool executeGetNextDetail = false;
					if (rsJournalInfo.EndOfFile() == true)
					{
						rsVendorInfo.MoveNext();
						executeCheckNextVendor = true;
						goto CheckNextVendor;
					}
					else
					{
						executeGetNextDetail = true;
						goto GetNextDetail;
					}
					CheckNextVendor:
					;
					if (executeCheckNextVendor)
					{
						if (rsVendorInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							GetFirstVendor:
							;
							if (!reportOptions.EnteredOrder)
							{
								if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY ID");
								}
								else
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY ID");
								}
							}
							else
							{
								if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND ID = " + rsVendorInfo.Get_Fields_Int32("ID") + " AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY ID");
								}
								else
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND ID = " + rsVendorInfo.Get_Fields_Int32("ID") + " AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY ID");
								}
							}
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
					CheckNextJournal:
					;
					if (executeCheckNextJournal)
					{
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							executeGetNextDetail = true;
							goto GetNextDetail;
						}
						else
						{
							rsVendorInfo.MoveNext();
							executeCheckNextVendor = true;
							goto CheckNextVendor;
						}
					}
					GetNextDetail:
					;
					if (executeGetNextDetail)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
						{
							this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
							this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
							eArgs.EOF = false;
							return;
						}
						else
						{
							rsJournalInfo.MoveNext();
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
				}
				else
				{
					this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
					this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					eArgs.EOF = false;
					return;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
            using (clsDRWrapper rsMasterJournal = new clsDRWrapper())
            {
                int counter;
                string strMasterSQL;
                strMasterSQL = "Status = 'E' AND JournalNumber IN (";

                foreach (var journalNumber in reportOptions.SelectedJournals)
                {
                    strMasterSQL += journalNumber + ", ";
                }

                strMasterSQL = Strings.Left(strMasterSQL, strMasterSQL.Length - 2) + ")";

                rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE " + strMasterSQL);
                if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
                {
                    do
                    {
                        rsMasterJournal.Edit();
                        rsMasterJournal.Set_Fields("Status", "V");
                        rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                        rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
                        rsMasterJournal.Update(true);
                        rsMasterJournal.MoveNext();
                    } while (rsMasterJournal.EndOfFile() != true);
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			//FC:FINAL:CHN - issue #744: Incorrect export to PDF.
			// this.PrintWidth = 15100 / 1440;
			// Line1.X2 = this.PrintWidth;
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			Label19.Text = "Pay Date: " + reportOptions.PayDate.ToString("MM/dd/yyyy");
			blnFirstRecord = true;
			blnTempVendors = false;
			strSQL = "(APJournal.Status = 'E' or APJournal.Status = 'V') AND APJournal.JournalNumber IN (";

            foreach (var journalNumber in reportOptions.SelectedJournals)
            {
                strSQL += journalNumber + ", ";
            }

			strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") AND APJournal.Payable <= '" + reportOptions.PayDate + "' ";
			FillTemp();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
            if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("WarrantPrevDeptSummary")))
            {
                var summaryReport = new rptWarrantPreviewDeptSummary(reportOptions);
                var preview = new frmReportViewer();

                preview.Init(summaryReport);
            }
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDetailDescription.Text = rsDetailInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetailInfo.Get_Fields_String("Account");
			fldProject.Text = rsDetailInfo.Get_Fields_String("Project");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
			fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			ShowTitle();
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curJournalTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curJournalEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curVendorTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curVendorEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curWarrantTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curWarrantEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("PrepaidCheck")))
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curPrepaidTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curCurrentTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldVendorAmountTotal.Text = Strings.Format(curVendorTotal, "#,##0.00");
			fldVendorEncumbranceTotal.Text = "";
			// format(curVendorEncTotal, "#,##0.00")
			curVendorEncTotal = 0;
			curVendorTotal = 0;
			if (rsVendorInfo.EndOfFile())
			{
				Line2.Visible = false;
			}
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			if (ShowJournalTotal())
			{
				Line3.Visible = true;
				fldJournalTitle.Visible = true;
				fldJournalAmountTotal.Text = Strings.Format(curJournalTotal, "#,##0.00");
				fldJournalEncumbranceTotal.Text = "";
				// format(curJournalEncTotal, "#,##0.00")
			}
			else
			{
				Line3.Visible = false;
				fldJournalTitle.Visible = false;
				fldJournalAmountTotal.Text = "";
				fldJournalEncumbranceTotal.Text = "";
			}
			curJournalEncTotal = 0;
			curJournalTotal = 0;
			strJournalSQL = rsJournalInfo.Name();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			//clsDRWrapper rsVendorName = new clsDRWrapper();
			fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsVendorInfo.Get_Fields_String("VendorName");
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("JournalNumber"), 4);
			fldDescription.Text = rsJournalInfo.Get_Fields_String("Description");
			fldReference.Text = rsJournalInfo.Get_Fields_String("Reference");
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsJournalInfo.Get_Fields("CheckNumber")) != "")
			{
				fldPaid.Visible = true;
				fldCheck.Visible = true;
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheckNumber.Text = FCConvert.ToString(rsJournalInfo.Get_Fields("CheckNumber"));
				fldSeperate.Visible = false;
			}
			else
			{
				fldPaid.Visible = false;
				fldCheck.Visible = false;
				fldCheckNumber.Text = "";
				if (rsJournalInfo.Get_Fields_Boolean("Seperate") == true)
				{
					fldSeperate.Visible = true;
				}
				else
				{
					fldSeperate.Visible = false;
				}
			}
			if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) == "E")
			{
				rsJournalInfo.Edit();
				rsJournalInfo.Set_Fields("Status", "V");
				rsJournalInfo.Update(true);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("VendorBinder");
			this.Fields.Add("JournalBinder");
		}

		private void ShowTitle()
		{
			int intHolder = 0;
            string accountTitle = "";

			if (reportOptions.AccountTitleOption == WarrantAccountTitleOption.NoAccountTitles)
			{
				fldAccount.Text += "  " + "";
				return;
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                accountTitle = accountTitleService.ReturnAccountDescription(rsDetailInfo.Get_Fields("Account"));
			}
			if (reportOptions.AccountTitleOption == WarrantAccountTitleOption.PrintFullTitles)
			{
                fldAccount.Text = accountTitle;
			}
			else if (reportOptions.AccountTitleOption == WarrantAccountTitleOption.PrintDeptDivFundOnly)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
				{
					intHolder = Strings.InStr(1, accountTitle, "/", CompareConstants.vbBinaryCompare);
					fldAccount.Text += "  " + Strings.Right(accountTitle, accountTitle.Length - intHolder);
				}
				else
				{
					intHolder = Strings.InStr(1, accountTitle, "-", CompareConstants.vbBinaryCompare);
					fldAccount.Text += "  " + Strings.Left(accountTitle, intHolder - 1);
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
				{
					intHolder = Strings.InStr(1, accountTitle, "/", CompareConstants.vbBinaryCompare);
					fldAccount.Text += "  " + Strings.Right(accountTitle, accountTitle.Length - intHolder);
				}
				else
				{
					intHolder = Strings.InStr(1, accountTitle, "-", CompareConstants.vbBinaryCompare);
					fldAccount.Text += "  " + Strings.Right(accountTitle, accountTitle.Length - intHolder - 1);
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsSignatureInfo = new clsDRWrapper())
            {
                int counter;
                clsDRWrapper rsJournalInfo = new clsDRWrapper();
                fldPrepaidAmountTotal.Text = Strings.Format(curPrepaidTotal, "#,##0.00");
                fldCurrentAmountTotal.Text = Strings.Format(curCurrentTotal, "#,##0.00");
                fldWarrantAmountTotal.Text = Strings.Format(curWarrantTotal, "#,##0.00");
                fldWarrantEncumbranceTotal.Text = "";

                foreach (var journalNumber in reportOptions.SelectedJournals)
                {
                    rsJournalInfo.OpenRecordset(
                        "SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' or Status = 'V') AND JournalNumber = " +
                        journalNumber);
                    if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                    {
                        if (rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo") != 0)
                        {
                            subCreditMemoSummary.Report = new srptCredMemSummary(reportOptions.SelectedJournals);
                            break;
                        }
                    }
                }

                if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("WarrantPrevSigLine")) == "N")
                {
                    fldWarrantSignature1.Visible = false;
                    fldWarrantSignature2.Visible = false;
                    fldWarrantSignature3.Visible = false;
                    fldWarrantSignature4.Visible = false;
                    fldWarrantSignature5.Visible = false;
                    fldWarrantSignature6.Visible = false;
                    fldWarrantSignature7.Visible = false;
                    fldWarrantSignature8.Visible = false;
                    fldWarrantSignature9.Visible = false;
                    fldWarrantSignature10.Visible = false;
                }
                else
                {
                    rsSignatureInfo.OpenRecordset("SELECT * FROM WarrantMessage");
                    if (rsSignatureInfo.EndOfFile() != true && rsSignatureInfo.BeginningOfFile() != true)
                    {
                        fldWarrantSignature1.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 1, 80);
                        fldWarrantSignature2.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 81, 80);
                        fldWarrantSignature3.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 161, 80);
                        fldWarrantSignature4.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 241, 80);
                        fldWarrantSignature5.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 321, 80);
                        fldWarrantSignature6.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 401, 80);
                        fldWarrantSignature7.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 481, 80);
                        fldWarrantSignature8.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 561, 80);
                        fldWarrantSignature9.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 641, 80);
                        fldWarrantSignature10.Text =
                            Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 721, 80);
                        if (Strings.Trim(fldWarrantSignature1.Text) == "")
                        {
                            fldWarrantSignature1.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature2.Text) == "")
                        {
                            fldWarrantSignature2.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature3.Text) == "")
                        {
                            fldWarrantSignature3.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature4.Text) == "")
                        {
                            fldWarrantSignature4.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature5.Text) == "")
                        {
                            fldWarrantSignature5.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature6.Text) == "")
                        {
                            fldWarrantSignature6.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature7.Text) == "")
                        {
                            fldWarrantSignature7.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature8.Text) == "")
                        {
                            fldWarrantSignature8.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature9.Text) == "")
                        {
                            fldWarrantSignature9.Visible = false;
                        }

                        if (Strings.Trim(fldWarrantSignature10.Text) == "")
                        {
                            fldWarrantSignature10.Visible = false;
                        }
                    }
                    else
                    {
                        fldWarrantSignature1.Visible = false;
                        fldWarrantSignature2.Visible = false;
                        fldWarrantSignature3.Visible = false;
                        fldWarrantSignature4.Visible = false;
                        fldWarrantSignature5.Visible = false;
                        fldWarrantSignature6.Visible = false;
                        fldWarrantSignature7.Visible = false;
                        fldWarrantSignature8.Visible = false;
                        fldWarrantSignature9.Visible = false;
                        fldWarrantSignature10.Visible = false;
                    }
                }
            }
        }

		private bool ShowJournalTotal()
		{
			bool ShowJournalTotal = false;
            using (clsDRWrapper rsCheck = new clsDRWrapper())
            {
                int intCheck;
                intCheck = Strings.InStr(1, strJournalSQL, "WHERE", CompareConstants.vbBinaryCompare);
                rsCheck.OpenRecordset("SELECT DISTINCT ID FROM APJournal " +
                                      Strings.Mid(strJournalSQL, intCheck, strJournalSQL.Length - (intCheck - 1)));
                if (rsCheck.RecordCount() > 1)
                {
                    ShowJournalTotal = true;
                }
                else
                {
                    ShowJournalTotal = false;
                }
            }

            return ShowJournalTotal;
		}

		private void FillTemp()
		{
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			if (!reportOptions.EnteredOrder)
			{
				strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
				// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
				strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries' ORDER BY TempVendorName";
				// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
				{
					if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
					{
						strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorNumber";
					}
					else
					{
						strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorName";
					}
				}
				else
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorNumber";
				}
			}
			else
			{
				strTemp = "SELECT DISTINCT APJournal.ID AS [ID], APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
				// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
				strTemp = "SELECT DISTINCT ID, VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries' ORDER BY TempVendorName";
				// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.ID AS [ID], APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT ID, VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY ID";
			}
			rsVendorInfo.OpenRecordset(strTemp);
		}

		
	}
}
