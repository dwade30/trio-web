﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmClearBudgetAmounts.
	/// </summary>
	partial class frmClearBudgetAmounts : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCComboBox cmbAllAmounts;
		public fecherFoundation.FCLabel lblAllAmounts;
		public fecherFoundation.FCComboBox cmbPercents;
		public fecherFoundation.FCLabel lblPercents;
		public fecherFoundation.FCCheckBox chkComments;
		public fecherFoundation.FCFrame fraAmounts;
		public fecherFoundation.FCFrame fraSelected;
		public fecherFoundation.FCCheckBox chkElected;
		public fecherFoundation.FCCheckBox chkCommittee;
		public fecherFoundation.FCCheckBox chkInitial;
		public fecherFoundation.FCCheckBox chkManager;
		public fecherFoundation.FCCheckBox chkApproved;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClearBudgetAmounts));
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.lblAll = new fecherFoundation.FCLabel();
			this.cmbAllAmounts = new fecherFoundation.FCComboBox();
			this.lblAllAmounts = new fecherFoundation.FCLabel();
			this.cmbPercents = new fecherFoundation.FCComboBox();
			this.lblPercents = new fecherFoundation.FCLabel();
			this.chkComments = new fecherFoundation.FCCheckBox();
			this.fraAmounts = new fecherFoundation.FCFrame();
			this.fraSelected = new fecherFoundation.FCFrame();
			this.chkElected = new fecherFoundation.FCCheckBox();
			this.chkCommittee = new fecherFoundation.FCCheckBox();
			this.chkInitial = new fecherFoundation.FCCheckBox();
			this.chkManager = new fecherFoundation.FCCheckBox();
			this.chkApproved = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAmounts)).BeginInit();
			this.fraAmounts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelected)).BeginInit();
			this.fraSelected.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkElected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCommittee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInitial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManager)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApproved)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnClear);
			this.BottomPanel.Location = new System.Drawing.Point(0, 429);
			this.BottomPanel.Size = new System.Drawing.Size(696, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbAll);
			this.ClientArea.Controls.Add(this.lblAll);
			this.ClientArea.Controls.Add(this.chkComments);
			this.ClientArea.Controls.Add(this.fraAmounts);
			this.ClientArea.Size = new System.Drawing.Size(696, 369);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(696, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(286, 30);
			this.HeaderText.Text = "Clear Budget Information";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All Accounts",
				"Expense Accounts",
				"Revenue Accounts"
			});
			this.cmbAll.Location = new System.Drawing.Point(218, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(180, 40);
			this.cmbAll.TabIndex = 0;
			// 
			// lblAll
			// 
			this.lblAll.Location = new System.Drawing.Point(30, 44);
			this.lblAll.Name = "lblAll";
			this.lblAll.Size = new System.Drawing.Size(118, 16);
			this.lblAll.TabIndex = 1;
			this.lblAll.Text = "AMOUNT CATEGORY";
			// 
			// cmbAllAmounts
			// 
			this.cmbAllAmounts.AutoSize = false;
			this.cmbAllAmounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllAmounts.FormattingEnabled = true;
			this.cmbAllAmounts.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbAllAmounts.Location = new System.Drawing.Point(188, 30);
			this.cmbAllAmounts.Name = "cmbAllAmounts";
			this.cmbAllAmounts.Size = new System.Drawing.Size(110, 40);
			this.cmbAllAmounts.TabIndex = 4;
			this.cmbAllAmounts.SelectedIndexChanged += new System.EventHandler(this.optAllAmounts_CheckedChanged);
			// 
			// lblAllAmounts
			// 
			this.lblAllAmounts.Location = new System.Drawing.Point(20, 44);
			this.lblAllAmounts.Name = "lblAllAmounts";
			this.lblAllAmounts.Size = new System.Drawing.Size(118, 16);
			this.lblAllAmounts.TabIndex = 5;
			this.lblAllAmounts.Text = "AMOUNTS TO CLEAR";
			// 
			// cmbPercents
			// 
			this.cmbPercents.AutoSize = false;
			this.cmbPercents.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPercents.FormattingEnabled = true;
			this.cmbPercents.Items.AddRange(new object[] {
				"Percents",
				"Amounts",
				"Both"
			});
			this.cmbPercents.Location = new System.Drawing.Point(188, 200);
			this.cmbPercents.Name = "cmbPercents";
			this.cmbPercents.Size = new System.Drawing.Size(110, 40);
			this.cmbPercents.TabIndex = 20;
			// 
			// lblPercents
			// 
			this.lblPercents.Location = new System.Drawing.Point(20, 214);
			this.lblPercents.Name = "lblPercents";
			this.lblPercents.Size = new System.Drawing.Size(110, 16);
			this.lblPercents.TabIndex = 21;
			this.lblPercents.Text = "MONTHLY BUDGET";
			// 
			// chkComments
			// 
			this.chkComments.AutoSize = false;
			this.chkComments.Location = new System.Drawing.Point(30, 440);
			this.chkComments.Name = "chkComments";
			this.chkComments.Size = new System.Drawing.Size(200, 24);
			this.chkComments.TabIndex = 10;
			this.chkComments.Text = "Clear Budget Comments";
			// 
			// fraAmounts
			// 
			this.fraAmounts.Controls.Add(this.fraSelected);
			this.fraAmounts.Controls.Add(this.cmbAllAmounts);
			this.fraAmounts.Controls.Add(this.lblAllAmounts);
			this.fraAmounts.Location = new System.Drawing.Point(30, 90);
			this.fraAmounts.Name = "fraAmounts";
			this.fraAmounts.Size = new System.Drawing.Size(318, 330);
			this.fraAmounts.TabIndex = 0;
			this.fraAmounts.Text = "Amounts To Clear";
			// 
			// fraSelected
			// 
			this.fraSelected.AppearanceKey = "groupBoxNoBorders";
			this.fraSelected.Controls.Add(this.chkElected);
			this.fraSelected.Controls.Add(this.cmbPercents);
			this.fraSelected.Controls.Add(this.lblPercents);
			this.fraSelected.Controls.Add(this.chkCommittee);
			this.fraSelected.Controls.Add(this.chkInitial);
			this.fraSelected.Controls.Add(this.chkManager);
			this.fraSelected.Controls.Add(this.chkApproved);
			this.fraSelected.Enabled = false;
			this.fraSelected.Location = new System.Drawing.Point(0, 70);
			this.fraSelected.Name = "fraSelected";
			this.fraSelected.Size = new System.Drawing.Size(318, 260);
			this.fraSelected.TabIndex = 3;
			// 
			// chkElected
			// 
			this.chkElected.AutoSize = false;
			this.chkElected.Enabled = false;
			this.chkElected.Location = new System.Drawing.Point(20, 122);
			this.chkElected.Name = "chkElected";
			this.chkElected.Size = new System.Drawing.Size(143, 24);
			this.chkElected.TabIndex = 19;
			this.chkElected.Text = "Elected Request";
			// 
			// chkCommittee
			// 
			this.chkCommittee.AutoSize = false;
			this.chkCommittee.Enabled = false;
			this.chkCommittee.Location = new System.Drawing.Point(20, 88);
			this.chkCommittee.Name = "chkCommittee";
			this.chkCommittee.Size = new System.Drawing.Size(233, 24);
			this.chkCommittee.TabIndex = 7;
			this.chkCommittee.Text = "Budget Committee\'s Request";
			// 
			// chkInitial
			// 
			this.chkInitial.AutoSize = false;
			this.chkInitial.Enabled = false;
			this.chkInitial.Location = new System.Drawing.Point(20, 20);
			this.chkInitial.Name = "chkInitial";
			this.chkInitial.Size = new System.Drawing.Size(126, 24);
			this.chkInitial.TabIndex = 6;
			this.chkInitial.Text = "Initial Request";
			// 
			// chkManager
			// 
			this.chkManager.AutoSize = false;
			this.chkManager.Enabled = false;
			this.chkManager.Location = new System.Drawing.Point(20, 54);
			this.chkManager.Name = "chkManager";
			this.chkManager.Size = new System.Drawing.Size(163, 24);
			this.chkManager.TabIndex = 5;
			this.chkManager.Text = "Manager\'s Request";
			// 
			// chkApproved
			// 
			this.chkApproved.AutoSize = false;
			this.chkApproved.Enabled = false;
			this.chkApproved.Location = new System.Drawing.Point(20, 156);
			this.chkApproved.Name = "chkApproved";
			this.chkApproved.Size = new System.Drawing.Size(153, 24);
			this.chkApproved.TabIndex = 4;
			this.chkApproved.Text = "Approved Budget";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Text = "Clear Amounts";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnClear
			// 
			this.btnClear.AppearanceKey = "acceptButton";
			this.btnClear.Location = new System.Drawing.Point(267, 30);
			this.btnClear.Name = "btnClear";
			this.btnClear.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnClear.Size = new System.Drawing.Size(160, 48);
			this.btnClear.TabIndex = 2;
			this.btnClear.Text = "Clear Amounts";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// frmClearBudgetAmounts
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(696, 537);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmClearBudgetAmounts";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Clear Budget Information";
			this.Load += new System.EventHandler(this.frmClearBudgetAmounts_Load);
			this.Activated += new System.EventHandler(this.frmClearBudgetAmounts_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmClearBudgetAmounts_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAmounts)).EndInit();
			this.fraAmounts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelected)).EndInit();
			this.fraSelected.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkElected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCommittee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInitial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManager)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApproved)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnClear;
	}
}
