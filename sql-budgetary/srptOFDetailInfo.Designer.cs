﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOFDetailInfo.
	/// </summary>
	partial class srptOFDetailInfo
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptOFDetailInfo));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFund = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.subOFSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAmount,
				this.fldFund
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Field13,
				this.Line1,
				this.Field29,
				this.lblPeriod,
				this.Field35,
				this.Field36
			});
			this.GroupHeader1.Height = 0.7916667F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subOFSummary
			});
			this.GroupFooter1.Height = 0.1041667F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDescription,
				this.fldType,
				this.Binder
			});
			this.GroupHeader2.DataField = "Binder";
			this.GroupHeader2.Height = 0.1875F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 11pt; font-weight: bold; text-align: center";
			this.Label22.Text = "OF Detail Information";
			this.Label22.Top = 0.28125F;
			this.Label22.Width = 7.53125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0.71875F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field13.Tag = " ";
			this.Field13.Text = "Jrnl";
			this.Field13.Top = 0.59375F;
			this.Field13.Width = 0.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.78125F;
			this.Line1.Width = 7.5625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5625F;
			this.Line1.Y1 = 0.78125F;
			this.Line1.Y2 = 0.78125F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 1.15625F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Description / Fund";
			this.Field29.Top = 0.59375F;
			this.Field29.Width = 1.25F;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Height = 0.1875F;
			this.lblPeriod.Left = 0F;
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblPeriod.Tag = " ";
			this.lblPeriod.Text = "Per";
			this.lblPeriod.Top = 0.59375F;
			this.lblPeriod.Width = 0.25F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 0.3125F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field35.Tag = " ";
			this.Field35.Text = "Type";
			this.Field35.Top = 0.59375F;
			this.Field35.Width = 0.34375F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.1875F;
			this.Field36.Left = 2.5F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field36.Tag = " ";
			this.Field36.Text = "Amount";
			this.Field36.Top = 0.59375F;
			this.Field36.Width = 0.875F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0F;
			this.fldPeriod.MultiLine = false;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldPeriod.Text = "Field36";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.71875F;
			this.fldJournal.MultiLine = false;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldJournal.Text = "Field36";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.34375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 1.15625F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; white-space: nowrap; ddo" + "-char-set: 1";
			this.fldDescription.Text = "Field36";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 3.125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.3125F;
			this.fldType.MultiLine = false;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldType.Text = "Field36";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 4.5F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field37";
			this.Binder.Top = 0.03125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.19F;
			this.fldAmount.Left = 2.21875F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldAmount.Text = "Field36";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.15625F;
			// 
			// fldFund
			// 
			this.fldFund.Height = 0.19F;
			this.fldFund.Left = 1.625F;
			this.fldFund.MultiLine = false;
			this.fldFund.Name = "fldFund";
			this.fldFund.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldFund.Text = "Field36";
			this.fldFund.Top = 0F;
			this.fldFund.Width = 0.53125F;
			// 
			// subOFSummary
			// 
			this.subOFSummary.CloseBorder = false;
			this.subOFSummary.Height = 0.09375F;
			this.subOFSummary.Left = 0F;
			this.subOFSummary.Name = "subOFSummary";
			this.subOFSummary.Report = null;
			this.subOFSummary.Top = 0F;
			this.subOFSummary.Width = 7.5F;
			// 
			// srptOFDetailInfo
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFund;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subOFSummary;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
	}
}
