﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptVendorDefaultAccountInfo.
	/// </summary>
	public partial class rptVendorDefaultAccountInfo : BaseSectionReport
	{
		public static rptVendorDefaultAccountInfo InstancePtr
		{
			get
			{
				return (rptVendorDefaultAccountInfo)Sys.GetInstance(typeof(rptVendorDefaultAccountInfo));
			}
		}

		protected rptVendorDefaultAccountInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVendorDefaultAccountInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();

		public rptVendorDefaultAccountInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Vendor Default Account List";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsVendorInfo.MoveNext();
					if (rsVendorInfo.EndOfFile() != true)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY DescriptionNumber");
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = Strings.Format(rsVendorInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber IN (SELECT DISTINCT VendorNumber FROM DefaultInfo) ORDER BY CheckName");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				rsDetailInfo.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY DescriptionNumber");
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - can't implicitly convert from int to string
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("AccountNumber"));
			fldDescription.Text = FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description"));
			// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
			fldTax.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("Tax"));
			fldProject.Text = FCConvert.ToString(rsDetailInfo.Get_Fields_String("Project"));
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldVendor.Text = this.Fields["Binder"].Value.ToString();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		

		private void rptVendorDefaultAccountInfo_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
