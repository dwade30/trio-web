﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Web.Ext.CustomProperties;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineDeptQuery.
	/// </summary>
	public partial class frmOutlineDeptQuery : BaseForm
	{
		public frmOutlineDeptQuery()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOutlineDeptQuery InstancePtr
		{
			get
			{
				return (frmOutlineDeptQuery)Sys.GetInstance(typeof(frmOutlineDeptQuery));
			}
		}

		protected frmOutlineDeptQuery _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		[DllImport("user32")]
		private static extern int GetCursorPos(ref POINTAPI lpPoint);

		[StructLayout(LayoutKind.Sequential)]
		private struct POINTAPI
		{
			public int x;
			public int y;
		};

		string length;
		// holds format account lengths
		string length2;
		int NumberOfDepartments;
		string[] DeletedDepartments = new string[20 + 1];
		int FundLength;
		bool CollapsedFlag;
		string OldDepartment = "";
		int DivLength;
		// holds division length
		int DeptLength;
		// holds department length
		int NumberCol;
		// column in the flex grid that has the database key
		int DeptCol;
		int FundCol;
		int DivCol;
		// column in the grid that holds the division number
		int ShortCol;
		// column in the grid that holds the short description
		int LongCol;
		// column in the grid that holds the long description
		int AccountCol;
		int BreakdownCol;
		bool EditFlag;
		float DeptPercent;
		float DivPercent;
		bool blnUnload;
		bool blnDirty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = false;
			vs1.Enabled = true;
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			string temp = "";
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDelete.Text == "")
			{
				MessageBox.Show("You must enter the number of the department you wish to delete", "Invalid Department Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			answer = MessageBox.Show("Are you sure you wish to delete department " + txtDelete.Text + "?", "Delete Department?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.Yes)
			{
				if (CheckDeptUsed_2(Strings.Trim(txtDelete.Text)) == false)
				{
					vs1.Enabled = true;
					counter = 1;
					temp = Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength);
					while (temp != txtDelete.Text)
					{
						counter += 1;
						if (counter == vs1.Rows)
						{
							break;
						}
						temp = Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength);
					}
					if (counter == vs1.Rows)
					{
						MessageBox.Show("No department found with the department number of " + txtDelete.Text + ".", "No Matches Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtDelete.Text = "";
						Frame1.Visible = false;
						return;
					}
					else
					{
						vs1.RemoveItem(counter);
						if (vs1.Rows == 1)
						{
							counter2 = 0;
							while (DeletedDepartments[counter2] != "")
							{
								counter2 += 1;
								if (counter2 == 20)
								{
									return;
								}
							}
							DeletedDepartments[counter2] = txtDelete.Text;
							txtDelete.Text = "";
							Frame1.Visible = false;
							cmdDepartmentDelete.Enabled = false;
							vs1_Collapsed();
							return;
						}
						else
						{
							if (counter == vs1.Rows)
							{
								counter2 = 0;
								while (DeletedDepartments[counter2] != "")
								{
									counter2 += 1;
									if (counter2 == 20)
									{
										return;
									}
								}
								DeletedDepartments[counter2] = txtDelete.Text;
								txtDelete.Text = "";
								Frame1.Visible = false;
								vs1_Collapsed();
								return;
							}
							//FC:FINAL:DSE:#891 Top outline level is 1, not 0
							while (vs1.RowOutlineLevel(counter) != 1)
							{
								vs1.RemoveItem(counter);
								if (vs1.Rows == 1)
								{
									cmdDepartmentDelete.Enabled = false;
									break;
								}
								else if (counter == vs1.Rows)
								{
									break;
								}
							}
						}
						counter2 = 0;
						while (DeletedDepartments[counter2] != "")
						{
							counter2 += 1;
							if (counter2 == 20)
							{
								return;
							}
						}
						DeletedDepartments[counter2] = txtDelete.Text;
						txtDelete.Text = "";
						Frame1.Visible = false;
						vs1_Collapsed();
					}
				}
				else
				{
					MessageBox.Show("You may not delete this Department because it it has a budget associated with it, is used in one or more journal entries,  or is used in one or more Revenue accounts.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				txtDelete.Focus();
				txtDelete.SelectionStart = 0;
				txtDelete.SelectionLength = 1;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void frmOutlineDeptQuery_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1.Focus();
			if (vs1.Rows > 1)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vs1.Rows - 1, 0, Color.White);
			}
			vs1.ColAlignment(DivCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DeptCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(FundCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(NumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ShortCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(LongCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(BreakdownCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			this.Refresh();
		}

        private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
                e.Control.KeyDown -= vs1_KeyDownEdit;
				e.Control.KeyDown += vs1_KeyDownEdit;
                e.Control.KeyUp -= vs1_KeyUpEdit;
                e.Control.KeyUp += vs1_KeyUpEdit;
                JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void frmOutlineDeptQuery_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Tab)
			{
				if (vs1.Col == AccountCol && vs1.RowOutlineLevel(vs1.Row) == 2)
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						KeyCode = (Keys)0;
						if (vs1.RowOutlineLevel(vs1.Row + 1) == 1)
						{
							if (vs1.TextMatrix(vs1.Row + 1, DeptCol) == "")
							{
								EditFlag = true;
								vs1.Row += 1;
								vs1.Col = DeptCol;
								EditFlag = false;
							}
							else
							{
								EditFlag = true;
								vs1.Row += 1;
								vs1.Col = ShortCol;
								EditFlag = false;
							}
						}
						else
						{
							if (vs1.TextMatrix(vs1.Row + 1, DivCol) == "")
							{
								EditFlag = true;
								vs1.Row += 1;
								vs1.Col = DivCol;
								EditFlag = false;
							}
							else
							{
								EditFlag = true;
								vs1.Row += 1;
								vs1.Col = ShortCol;
								EditFlag = false;
							}
						}
					}
				}
				else if (vs1.RowOutlineLevel(vs1.Row) == 2 && vs1.Col == DivCol)
				{
					if (vs1.TextMatrix(vs1.Row, DivCol) == "" && vs1.EditText == "")
					{
						if (vs1.Row < vs1.Rows - 1)
						{
							KeyCode = (Keys)0;
							if (vs1.RowOutlineLevel(vs1.Row + 1) == 1)
							{
								if (vs1.TextMatrix(vs1.Row + 1, DeptCol) == "")
								{
									EditFlag = true;
									vs1.Row += 1;
									vs1.Col = DeptCol;
									vs1.EditCell();
									EditFlag = false;
								}
								else
								{
									EditFlag = true;
									vs1.Row += 1;
									vs1.Col = ShortCol;
									vs1.EditCell();
									EditFlag = false;
								}
							}
							else
							{
								if (vs1.TextMatrix(vs1.Row + 1, DivCol) == "")
								{
									EditFlag = true;
									vs1.Row += 1;
									vs1.Col = DivCol;
									vs1.EditCell();
									EditFlag = false;
								}
								else
								{
									EditFlag = true;
									vs1.Row += 1;
									vs1.Col = ShortCol;
									vs1.EditCell();
									EditFlag = false;
								}
							}
						}
					}
				}
			}
			else
			{
				if (vs1.Col == AccountCol)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
				}
			}
			if (KeyCode == Keys.F10)
			{
				// if the user saves
				KeyCode = (Keys)0;
			}
		}

		private void frmOutlineDeptQuery_Load(object sender, System.EventArgs e)
		{
			int temp;
			int temp2;
			int temp3;
			string tempDivision;
			string tempFund = "";
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rs2 = new clsDRWrapper();
			string strList = "";
			length = modAccountTitle.Statics.Exp;
			length2 = modAccountTitle.Statics.Ledger;
			modValidateAccount.SetBDFormats();
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modNewAccountBox.GetFormats();
			NumberCol = 1;
			DeptCol = 2;
			DivCol = 3;
			// initialize the columns
			ShortCol = 4;
			LongCol = 5;
			FundCol = 6;
			AccountCol = 8;
			BreakdownCol = 7;
			FCUtils.EraseSafe(DeletedDepartments);

			DeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 1, 2)))));
			FundLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length2, 1, 2)))));
			vs1.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			DivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 3, 2)))));
			vs1.ColWidth(0, 300);
			vs1.ColWidth(NumberCol, 0);
			if (DeptLength < 4)
			{
				vs1.ColWidth(DeptCol, 600);
				DeptPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DeptCol)) / vs1.WidthOriginal);
			}
			else
			{
				vs1.ColWidth(DeptCol, DeptLength * 140 + 50);
				DeptPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DeptCol)) / vs1.WidthOriginal);
				txtDelete.Width = DeptLength * 140 + 50;
				txtDelete.Left = (Frame1.Width / 2) - (txtDelete.Width / 2);
			}
			if (DivLength == 0)
			{
				vs1.ColWidth(DivCol, 0);
			}
			else
			{
				if (DivLength < 4)
				{
					vs1.ColWidth(DivCol, 500);
					// set column widths
					DivPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DivCol)) / vs1.WidthOriginal);
				}
				else
				{
					vs1.ColWidth(DivCol, DivLength * 140 + 50);
					DivPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DivCol)) / vs1.WidthOriginal);
				}
			}
			if (!modRegionalTown.IsRegionalTown())
			{
				vs1.ColHidden(BreakdownCol, true);
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.18 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, FCConvert.ToInt32(0.38 * vs1.WidthOriginal));
				vs1.ColWidth(FundCol, FCConvert.ToInt32(0.06 * vs1.WidthOriginal));
				vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(BreakdownCol, 0);
			}
			else
			{
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.16 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, FCConvert.ToInt32(0.34 * vs1.WidthOriginal));
				vs1.ColWidth(FundCol, FCConvert.ToInt32(0.06 * vs1.WidthOriginal));
				vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(BreakdownCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.06));
			}
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.TextMatrix(0, DeptCol, "Dept");
			vs1.TextMatrix(0, DivCol, "Div");
			// set column headings
			vs1.TextMatrix(0, ShortCol, "Short Desc");
			vs1.TextMatrix(0, LongCol, "Long Desc");
			vs1.TextMatrix(0, FundCol, "Fund");
			vs1.TextMatrix(0, AccountCol, "Closeout Acct");
			vs1.TextMatrix(0, BreakdownCol, "M/T");
			rs.OpenRecordset("SELECT * FROM RegionalBreakdownCodes ORDER BY Code", "CentralData");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				strList = "#0;0" + "\t" + "None|";
				do
				{
					strList += "'" + rs.Get_Fields("Code") + ";" + rs.Get_Fields("Code") + "\t" + rs.Get_Fields_String("Description") + "|";
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			if (strList != "")
			{
				strList = Strings.Left(strList, strList.Length - 1);
			}
			vs1.ColComboList(BreakdownCol, strList);
			rs.OpenRecordset("SELECT * FROM DeptDivTitles");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs1.Rows = rs.RecordCount() + 1;
			}
			else
			{
				cmdDepartmentDelete.Enabled = false;
				vs1.Rows = 1;
			}
			for (temp = 1; temp <= vs1.Rows - 1; temp++)
			{
				vs1.TextMatrix(temp, NumberCol, FCConvert.ToString(0));
			}
			vs1.Visible = true;
			temp2 = 1;
			tempDivision = modValidateAccount.GetFormat("0", ref DivLength);
			rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + tempDivision + "' ORDER BY Department");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				NumberOfDepartments = rs.RecordCount();
				for (temp = 1; temp <= rs.RecordCount(); temp++)
				{
					vs1.TextMatrix(temp2, DeptCol, rs.Get_Fields_String("Department") + " -");
					vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
					vs1.TextMatrix(temp2, LongCol, (rs.Get_Fields_String("LongDescription") == null ? "" : rs.Get_Fields_String("LongDescription")));
					vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					tempFund = FCConvert.ToString(rs.Get_Fields("Fund"));
					vs1.TextMatrix(temp2, FundCol, modValidateAccount.GetFormat(tempFund, ref FundLength));
					vs1.TextMatrix(temp2, AccountCol, FCConvert.ToString(rs.Get_Fields_String("CloseoutAccount")));
					if (modRegionalTown.IsRegionalTown())
					{
						vs1.TextMatrix(temp2, BreakdownCol, FCConvert.ToString(rs.Get_Fields_Int32("BreakdownCode")));
					}
					vs1.RowOutlineLevel(temp2, 1);
					vs1.IsSubtotal(temp2, true);
					temp2 += 1;
					rs2.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rs.Get_Fields_String("Department") + "' AND NOT Division = '" + tempDivision + "' ORDER BY Division");
					if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
					{
						rs2.MoveLast();
						rs2.MoveFirst();
						for (temp3 = 1; temp3 <= rs2.RecordCount(); temp3++)
						{
							vs1.RowOutlineLevel(temp2, 2);
							vs1.TextMatrix(temp2, DivCol, FCConvert.ToString(rs2.Get_Fields_String("Division")));
							vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs2.Get_Fields_String("ShortDescription")));
							vs1.TextMatrix(temp2, LongCol, FCConvert.ToString(rs2.Get_Fields_String("LongDescription")));
							vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
							vs1.TextMatrix(temp2, AccountCol, FCConvert.ToString(rs2.Get_Fields_String("CloseoutAccount")));
							temp2 += 1;
							rs2.MoveNext();
						}
					}
					rs.MoveNext();
				}
			}
			for (temp = 1; temp <= temp2 - 1; temp++)
			{
				if (vs1.RowOutlineLevel(temp) == 2)
				{
					vs1.IsCollapsed(temp, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			vs1_Collapsed();
            modColorScheme.ColorGrid(vs1);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			blnDirty = false;
			if (!modRegionalTown.IsRegionalTown())
			{
				cmdFileDuplicate.Enabled = false;
			}
			else
			{
				cmdFileDuplicate.Enabled = true;
			}

            if (DivLength > 0)
            {
                vs1.AddExpandButton(1);
			}
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DialogResult ans;
			if (e.CloseReason != FCCloseReason.FormCode)
			{
				if (blnDirty)
				{
					ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmOutlineDeptQuery_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(DeptCol, FCConvert.ToInt32(vs1.WidthOriginal * DeptPercent + 250));
			vs1.ColWidth(DivCol, FCConvert.ToInt32(vs1.WidthOriginal * DivPercent));
			if (!modRegionalTown.IsRegionalTown())
			{
				vs1.ColHidden(BreakdownCol, true);
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.18 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, FCConvert.ToInt32(0.38 * vs1.WidthOriginal));
				vs1.ColWidth(FundCol, FCConvert.ToInt32(0.06 * vs1.WidthOriginal));
				vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(BreakdownCol, 0);
			}
			else
			{
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.16 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, FCConvert.ToInt32(0.34 * vs1.WidthOriginal));
				vs1.ColWidth(FundCol, FCConvert.ToInt32(0.06 * vs1.WidthOriginal));
				vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
				vs1.ColWidth(BreakdownCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.06));
			}
			vs1_Collapsed();
		}

		private void frmOutlineDeptQuery_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuDepartmentAdd_Click(object sender, System.EventArgs e)
		{
			string tempFund;
			vs1.Rows += 1;
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
			tempFund = "1";
			vs1.TextMatrix(vs1.Rows - 1, FundCol, modValidateAccount.GetFormat(tempFund, ref FundLength));
			vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
			cmdDepartmentDelete.Enabled = true;
			vs1_Collapsed();
			vs1.Focus();
			vs1.Select(vs1.Rows - 1, DeptCol);
			vs1.EditCell();
			vs1.EditMaxLength = 0;
		}

		private void mnuDepartmentDelete_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = true;
			txtDelete.MaxLength = DeptLength;
			txtDelete.Focus();
			vs1.Enabled = false;
		}

		private void mnuFileDuplicate_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsRegionalInfo = new clsDRWrapper();
			clsDRWrapper rsDeptInfo = new clsDRWrapper();
			string[,] strSearch = new string[1000 + 1, 3 + 1];
			int counter;
			string strDept = "";
			string strDiv = "";
			int intMax;
			int intDupCheck;
			bool blnDupFound = false;
			bool blnSkip;
			int intDeptStartRow = 0;
			string strCurDept = "";
			for (counter = 0; counter <= 1000; counter++)
			{
				strSearch[counter, 0] = "";
				strSearch[counter, 1] = "";
			}
			blnSkip = false;
			intMax = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, DeptCol), 1) == "1")
					{
						blnSkip = false;
						strDept = Strings.Mid(vs1.TextMatrix(counter, DeptCol), 2, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) - 1);
						if (modAccountTitle.Statics.ExpDivFlag)
						{
							strDiv = "0";
						}
						else
						{
							strDiv = Strings.StrDup(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), "0");
						}
					}
					else
					{
						blnSkip = true;
					}
				}
				else
				{
					strDiv = vs1.TextMatrix(counter, DivCol);
				}
				if (!blnSkip)
				{
					blnDupFound = false;
					for (intDupCheck = 0; intDupCheck <= intMax; intDupCheck++)
					{
						if (strSearch[intDupCheck, 0] == strDept && strSearch[intDupCheck, 1] == strDiv)
						{
							blnDupFound = true;
							break;
						}
					}
					if (!blnDupFound)
					{
						strSearch[intMax, 0] = strDept;
						strSearch[intMax, 1] = strDiv;
						strSearch[intMax, 2] = vs1.TextMatrix(counter, ShortCol);
						strSearch[intMax, 3] = vs1.TextMatrix(counter, LongCol);
						intMax += 1;
					}
				}
			}
			rsRegionalInfo.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber > 1 ORDER BY TownNumber", "CentralData");
			if (rsRegionalInfo.EndOfFile() != true && rsRegionalInfo.BeginningOfFile() != true)
			{
				do
				{
					for (counter = 0; counter <= intMax - 1; counter++)
					{
						// TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
						strDept = rsRegionalInfo.Get_Fields("TownNumber") + strSearch[counter, 0];
						if (Conversion.Val(strSearch[counter, 1]) == 0)
						{
							strDiv = "";
						}
						else
						{
							strDiv = strSearch[counter, 1];
						}
						blnDupFound = false;
						intDeptStartRow = -1;
						for (intDupCheck = 1; intDupCheck <= vs1.Rows - 1; intDupCheck++)
						{
							if (vs1.RowOutlineLevel(intDupCheck) == 1)
							{
								strCurDept = Strings.Left(vs1.TextMatrix(intDupCheck, DeptCol), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
								if (strCurDept == strDept)
								{
									intDeptStartRow = intDupCheck;
								}
								if (strCurDept == strDept && vs1.TextMatrix(intDupCheck, DivCol) == strDiv)
								{
									blnDupFound = true;
									break;
								}
							}
							else
							{
								if (strCurDept == strDept && vs1.TextMatrix(intDupCheck, DivCol) == strDiv)
								{
									blnDupFound = true;
									break;
								}
							}
						}
						if (!blnDupFound)
						{
							if (intDeptStartRow == -1)
							{
								vs1.Rows += 1;
								intDeptStartRow = vs1.Rows - 1;
								vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, DeptCol, vs1.Rows - 1, vs1.Cols - 1, 0x80000017);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, DeptCol, vs1.Rows - 1, vs1.Cols - 1, 0x80000018);
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.IsSubtotal(vs1.Rows - 1, true);
								vs1.TextMatrix(vs1.Rows - 1, DeptCol, strDept + " -");
								vs1.TextMatrix(vs1.Rows - 1, ShortCol, strSearch[counter, 2]);
								vs1.TextMatrix(vs1.Rows - 1, LongCol, strSearch[counter, 3]);
								vs1.TextMatrix(vs1.Rows - 1, FundCol, modValidateAccount.GetFormat(Strings.Left(strDept, 1), ref FundLength));
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, Color.White);
								vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
								cmdDepartmentDelete.Enabled = true;
							}
							else
							{
								vs1.AddItem("", intDeptStartRow + 1);
								vs1.RowOutlineLevel(intDeptStartRow + 1, 2);
								vs1.TextMatrix(intDeptStartRow + 1, DivCol, strSearch[counter, 1]);
								vs1.TextMatrix(intDeptStartRow + 1, ShortCol, strSearch[counter, 2]);
								vs1.TextMatrix(intDeptStartRow + 1, LongCol, strSearch[counter, 3]);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intDeptStartRow + 1, 0, Color.White);
								vs1.TextMatrix(intDeptStartRow + 1, NumberCol, FCConvert.ToString(0));
							}
						}
					}
					rsRegionalInfo.MoveNext();
				}
				while (rsRegionalInfo.EndOfFile() != true);
			}
			vs1_Collapsed();
			vs1.Focus();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtDelete_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtDelete_Validate(false);
				cmdDelete_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDelete_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp;
			temp = txtDelete.Text;
			txtDelete.Text = modValidateAccount.GetFormat(temp, ref DeptLength);
		}

		public void txtDelete_Validate(bool Cancel)
		{
			txtDelete_Validating(txtDelete, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), false);
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), true);
		}

		private void vs1_AfterCollapse(int row, bool isCollapsed)
		{
			int temp;
			int counter;
			int rows = 0;
			int height;
			bool DeptFlag = false;
			bool DivisionFlag;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						DeptFlag = true;
					}
					else
					{
						rows += 1;
						DeptFlag = false;
					}
				}
				else
				{
					if (DeptFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			height = (rows + 1) * vs1.RowHeight(0);
			if (height < frmOutlineDeptQuery.InstancePtr.Height * 0.7375)
			{
				if (vs1.Height != height)
				{
					//FC:FINAL:ASZ - use anchors: vs1.Height = height + 75;
					// vs1.ColWidth(LongCol) = vs1.Width - (vs1.ColWidth(DeptCol) + vs1.ColWidth(DivCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 700)
				}
			}
			else
			{
				// vs1.ColWidth(LongCol) = vs1.Width - (vs1.ColWidth(DeptCol) + vs1.ColWidth(DivCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 1000)
				if (vs1.Height < frmOutlineDeptQuery.InstancePtr.Height * 0.7375)
				{
					//FC:FINAL:ASZ - use anchors: vs1.Height = FCConvert.ToInt32(frmOutlineDeptQuery.InstancePtr.Height * 0.7375 + 75);
				}
			}
			counter = vs1.Row;
			while (vs1.RowOutlineLevel(counter) != 0)
			{
				counter -= 1;
			}
			if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				vs1.Row = counter;
			}
			else
			{
				// do nothing
			}
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.Col == AccountCol)
			{
				vs1.EditMaxLength = 17;
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnDirty = true;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			POINTAPI MousePosition = new POINTAPI();
			// vbPorter upgrade warning: PassFail As bool	OnWriteFCConvert.ToInt32(
			bool PassFail;
			PassFail = FCConvert.ToBoolean(GetCursorPos(ref MousePosition));
			if (MousePosition.x * FCScreen.TwipsPerPixelX < vs1.Left + vs1.ColWidth(0))
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				return;
			}
			if (vs1.Col == NumberCol)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				return;
			}
			if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col == DivCol && vs1.TextMatrix(vs1.Row, DeptCol) == "")
				{
					vs1.Select(vs1.Row, DeptCol);
				}
				else if (vs1.Col == DivCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col == DeptCol && vs1.TextMatrix(vs1.Row, DeptCol) == "")
				{
					// do nothing
				}
				else if (vs1.Col == DeptCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col == FundCol)
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = FundLength;
					vs1.EditMaxLength = FundLength;
					return;
				}
				if (vs1.Col == BreakdownCol && Strings.Left(vs1.TextMatrix(vs1.Row, DeptCol), 1) != "1")
				{
					vs1.Select(vs1.Row, LongCol);
				}
			}
			else
			{
				if (vs1.Col == DeptCol)
				{
					if (vs1.TextMatrix(vs1.Row, DivCol) == "")
					{
						vs1.Select(vs1.Row, DivCol);
					}
					else
					{
						vs1.Select(vs1.Row, ShortCol);
					}
				}
				if (vs1.Col == FundCol || vs1.Col == BreakdownCol)
				{
					vs1.Select(vs1.Row, LongCol);
				}
				if (vs1.Col == DivCol)
				{
					if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
					{
						vs1.Select(vs1.Row, ShortCol);
					}
				}
			}
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			vs1.Select(vs1.Row, vs1.Col);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			// give the cell focus
			vs1.EditSelStart = 0;
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height;
			bool DeptFlag = false;
			bool DivisionFlag;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						DeptFlag = true;
					}
					else
					{
						rows += 1;
						DeptFlag = false;
					}
				}
				else
				{
					if (DeptFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			height = (rows + 1) * vs1.RowHeight(0);
			if (height < frmOutlineDeptQuery.InstancePtr.Height * 0.7375)
			{
				if (vs1.Height != height)
				{
					//FC:FINAL:ASZ: vs1.Height = height + 75;
					// vs1.ColWidth(LongCol) = vs1.Width - (vs1.ColWidth(DeptCol) + vs1.ColWidth(DivCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 700)
				}
			}
			else
			{
				// vs1.ColWidth(LongCol) = vs1.Width - (vs1.ColWidth(DeptCol) + vs1.ColWidth(DivCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 1000)
				//FC:FINAL:ASZ: vs1.Height = FCConvert.ToInt32(frmOutlineDeptQuery.InstancePtr.Height * 0.7375 + 75);
			}
			counter = vs1.Row;
			if (counter != -1)
			{
				while (vs1.RowOutlineLevel(counter) != 0)
				{
					counter -= 1;
				}
				if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					vs1.Row = counter;
				}
				else
				{
					// do nothing
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (vs1.Col == AccountCol)
			{
				modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, false, "", "G");
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// vbPorter upgrade warning: Check As string	OnWrite(DialogResult)
			int Check = 0;
			int counter = 0;
			string tempDepartment = "";
			string tempDivision = "";
			clsDRWrapper rs = new clsDRWrapper();
			if (KeyCode == Keys.Left)
			{
				if (vs1.Col < ShortCol)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Insert)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (DivLength > 0)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							MessageBox.Show("You must open up a Department to add a Division to it", "Unable to Create a Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else if (vs1.TextMatrix(vs1.Row, DeptCol) == "" || vs1.TextMatrix(vs1.Row, ShortCol) == "")
						{
							MessageBox.Show("You must have a Short Description and a ID for a Department before you can add a Division", "Unable to Create a Division ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else
						{
							if (vs1.Row == vs1.Rows - 1)
							{
								vs1.Rows += 1;
								vs1.Select(vs1.Row + 1, DivCol);
								vs1.EditText = "";
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
								vs1.RowOutlineLevel(vs1.Row, 2);
								vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
								vs1_Collapsed();
								vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
								vs1.Select(vs1.Row, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
								return;
							}
						}

                        vs1.AddItem("", vs1.Row + 1);
                        // add a row in
                        vs1.EditText = "";
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row + 1, 0, Color.White);
                        vs1.RowOutlineLevel(vs1.Row + 1, 2);
                        vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
                        vs1_Collapsed();
                        if (vs1.RowOutlineLevel(vs1.Row) != 1)
                        {
                            counter = vs1.Row - 1;
                            while (vs1.RowOutlineLevel(counter) != 0)
                            {
                                counter -= 1;
                            }
                        }
                        else
                        {
                            counter = vs1.Row;
                        }
                        vs1.Select(vs1.Row + 1, DivCol);
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                        vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					}
				}
            }
			else if (KeyCode == Keys.Delete)
			{
				// if they hit the delete key
				if (vs1.TextMatrix(vs1.Row, DivCol) != "" && vs1.TextMatrix(vs1.Row, ShortCol) != "")
				{
					if (vs1.RowOutlineLevel(vs1.Row) != 1)
					{
						Check = FCConvert.ToInt32(MessageBox.Show("Are You Sure You Want To Delete This Record?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
						// make sure they want to delete
						if (Check == FCConvert.ToInt32(DialogResult.Yes))
						{
							// if they do
							if (CheckDivUsed_2(Strings.Trim(vs1.TextMatrix(vs1.Row, DivCol))) == false)
							{
								if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, NumberCol)) != 0)
								{
									rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, NumberCol))));
									tempDepartment = FCConvert.ToString(rs.Get_Fields_String("Department"));
									tempDivision = FCConvert.ToString(rs.Get_Fields_String("Division"));
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										rs.Delete();
										rs.Update();
									}
									rs.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + tempDepartment + "' AND Division = '" + tempDivision + "'");
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										rs.MoveLast();
										rs.MoveFirst();
										for (counter = 1; counter <= rs.RecordCount(); counter++)
										{
											rs.Delete();
											rs.Update();
										}
									}
									rs.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' or AccountType = 'R') and FirstAccountField = '" + tempDepartment + "' AND SecondAccountField = '" + tempDivision + "'");
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										rs.MoveLast();
										rs.MoveFirst();
										for (counter = 1; counter <= rs.RecordCount(); counter++)
										{
											rs.Delete();
											rs.Update();
										}
									}
								}
								vs1.RemoveItem(vs1.Row);
								vs1_Collapsed();
								counter = vs1.Row;
								while (vs1.RowOutlineLevel(counter) != 0)
								{
									counter -= 1;
								}
							}
							else
							{
								MessageBox.Show("You may not delete this Division because it has a budget associated with it, is used in one or more journal entries,  or is used in one or more Revenue accounts.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row) != 1)
					{
						vs1.RemoveItem(vs1.Row);
						vs1_Collapsed();
						counter = vs1.Row - 1;
						while (vs1.RowOutlineLevel(counter) != 0)
						{
							counter -= 1;
						}
					}
				}
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int counter = 0;
			int NextOpenRow;
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == DivCol)
			{
				// if they are in the division field
				if (vs1.EditText.Length == DivLength)
				{
					// if the cell is the length of the division number
					if (vs1.EditSelStart == DivLength)
					{
						// dont move the cursor
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							// dont send the key through
							vs1.Col = ShortCol;
							// send the user to the next column
							return;
						}
					}
				}
			}
			if (vs1.Col == DeptCol)
			{
				if (vs1.EditText.Length == DeptLength)
				{
					if (vs1.EditSelStart == DeptLength)
					{
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							// dont send the key through
							vs1.Col = ShortCol;
							// send the user to the next column
							return;
						}
					}
				}
			}
			if (vs1.Col == FundCol)
			{
				if (vs1.EditText.Length == FundLength)
				{
					if (vs1.EditSelStart == FundLength)
					{
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Insert)
						{
							KeyCode = 0;
							// If vs1.Row < vs1.rows - 1 Then
							// If vs1.RowOutlineLevel(vs1.Row + 1) = 0 Then
							// If vs1.TextMatrix(vs1.Row + 1, DeptCol) = "" Then
							// vs1.Select vs1.Row + 1, DeptCol
							// Else
							// vs1.Select vs1.Row + 1, ShortCol
							// End If
							// Else
							// If vs1.TextMatrix(vs1.Row + 1, DivCol) = "" Then
							// vs1.Select vs1.Row + 1, DivCol
							// Else
							// vs1.Select vs1.Row + 1, ShortCol
							// End If
							// End If
							// End If
							vs1.Col = AccountCol;
						}
					}
				}
			}
			//if (vs1.Col == AccountCol)
			//{
			//	// this is for the left and right arrow key
			//	if (KeyCode == Keys.Return)
			//	{
			//		KeyCode = 0;
			//		Support.SendKeys("{TAB}", false);
			//	}
			//	else
			//	{
			//		string temp = vs1.EditText;
			//		modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp, vs1.EditSelLength);
			//		vs1.EditText = temp;
			//		return;
			//	}
			//}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{TAB}", false);
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					counter = vs1.Row + 1;
					while (vs1.RowOutlineLevel(counter) != 0)
					{
						counter += 1;
						if (counter < vs1.Rows - 1)
						{
							// do nothing
						}
						else
						{
							break;
						}
					}
				}
				else
				{
					counter = vs1.Row + 1;
				}
				if (counter > vs1.Rows - 1)
				{
					return;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == DeptCol || vs1.Col == DivCol)
					{
						if (vs1.RowOutlineLevel(counter) != 1)
						{
							if (vs1.TextMatrix(counter, DivCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						else
						{
							if (vs1.TextMatrix(counter, DeptCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DeptCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
					else
					{
						if (vs1.RowOutlineLevel(counter) != 1)
						{
							if (vs1.TextMatrix(counter, DivCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, vs1.Col);
								if (vs1.Col != FundCol && vs1.Col != BreakdownCol)
								{
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						else
						{
							if (vs1.TextMatrix(counter, DeptCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DeptCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
				else
				{
					// if the row is not a heading and the column is department or division
					if (vs1.Col == DivCol || vs1.Col == DeptCol)
					{
						if (vs1.RowOutlineLevel(counter) != 2)
						{
							if (vs1.TextMatrix(counter, DeptCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DeptCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						else
						{
							if (vs1.TextMatrix(counter, DivCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
					else
					{
						if (vs1.Row < vs1.Rows - 1)
						{
							if (vs1.TextMatrix(counter, ShortCol) == "" && vs1.TextMatrix(vs1.Row + 1, DivCol) == "")
							{
								KeyCode = 0;
								vs1.Select(counter, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == DivCol + 1)
					{
						if (vs1.TextMatrix(vs1.Row, DeptCol) != "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Col = DeptCol;
						}
					}
				}
				else
				{
					if (vs1.Col == DivCol + 1)
					{
						if (vs1.TextMatrix(vs1.Row, DivCol) != "")
						{
							KeyCode = 0;
						}
					}
					else if (vs1.Col == DivCol)
					{
						KeyCode = 0;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row > 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row) == 1)
					{
						counter = vs1.Row - 1;
						while (vs1.RowOutlineLevel(counter) != 0)
						{
							counter -= 1;
						}
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							// do nothing
						}
						else
						{
							counter = vs1.Row - 1;
						}
						if (counter == vs1.Row - 1)
						{
							if (vs1.Col == DivCol || vs1.Col == DeptCol)
							{
								if (vs1.RowOutlineLevel(vs1.Row - 1) == 1)
								{
									if (vs1.TextMatrix(vs1.Row - 1, DeptCol) != "")
									{
										KeyCode = 0;
										vs1.Select(counter, ShortCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										vs1.Select(counter, DeptCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
								else
								{
									if (vs1.TextMatrix(vs1.Row - 1, DivCol) != "")
									{
										KeyCode = 0;
										vs1.Select(counter, ShortCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										vs1.Select(counter, DivCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
							}
						}
						else
						{
							if (vs1.RowOutlineLevel(counter) != 1)
							{
								if (vs1.TextMatrix(counter, DivCol) != "")
								{
									KeyCode = 0;
									vs1.Select(counter, ShortCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									vs1.Select(counter, DivCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								if (vs1.TextMatrix(counter, DeptCol) != "")
								{
									KeyCode = 0;
									vs1.Select(counter, ShortCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									vs1.Select(counter, DeptCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
						}
					}
					else
					{
						if (vs1.Col == DivCol || vs1.Col == DeptCol)
						{
							if (vs1.TextMatrix(vs1.Row - 1, DivCol) != "")
							{
								KeyCode = 0;
								vs1.Select(vs1.Row - 1, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(vs1.Row - 1, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == DeptCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Select(vs1.Row, ShortCol);
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
				}
				else
				{
					if (vs1.Col == DivCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
					}
				}
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				Close();
			}
			else if (KeyCode == Keys.Insert)
			{
				if (vs1.EditText != "")
				{
					vs1.TextMatrix(vs1.Row, vs1.Col, vs1.EditText);
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (DivLength > 0)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							MessageBox.Show("You must open up a Department to add a Division to it", "Unable to Create a Division ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else if (vs1.TextMatrix(vs1.Row, DeptCol) == "" || vs1.TextMatrix(vs1.Row, ShortCol) == "")
						{
							MessageBox.Show("You must have a Short Description and a Number for a Department before you can add a Division", "Unable to Create a Division ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else
						{
							if (vs1.Row == vs1.Rows - 1)
							{
								vs1.Rows += 1;
								vs1.Select(vs1.Row + 1, DivCol);
								vs1.EditText = "";
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
								vs1.RowOutlineLevel(vs1.Row, 2);
								vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
								vs1_Collapsed();
								vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
								vs1.Select(vs1.Row, DivCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
								return;
							}
						}
					}
					else
					{
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
				}
				if (vs1.Row == vs1.Rows - 1)
				{
					vs1.Rows += 1;
				}
				else
				{
					vs1.AddItem("", vs1.Row + 1);
					// add a row in
				}
				vs1.Select(vs1.Row + 1, DivCol);
				vs1.EditText = "";
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
				vs1.RowOutlineLevel(vs1.Row, 2);
				vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
				vs1_Collapsed();
				counter = vs1.Row - 1;
				while (vs1.RowOutlineLevel(counter) != 0)
				{
					counter -= 1;
				}
				vs1.Select(vs1.Row, DivCol);
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vs1.Col == AccountCol)
			{
				modNewAccountBox.CheckAccountKeyPress(vs1, vs1.Row, vs1.Col, FCConvert.ToInt16(keyAscii), vs1.EditSelStart, vs1.EditText);
			}
		}

		private void vs1_KeyUpEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
            if (vs1.Col == AccountCol)
            {
                // this is for the left and right arrow key
                if (KeyCode == Keys.Return)
                {
                    KeyCode = 0;
                    Support.SendKeys("{TAB}", false);
                }
                else
                {
                    string temp = vs1.EditText;
                    modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp, vs1.EditSelLength);
                    vs1.EditText = temp;
                    return;
                }
            }
   //         if (vs1.Col == AccountCol)
			//{
			//	if (FCConvert.ToInt32(KeyCode) != 40 && FCConvert.ToInt32(KeyCode) != 38)
			//	{
			//		// up and down arrows
			//		modNewAccountBox.CheckAccountKeyCode(vs1, vs1.Row, vs1.Col, FCConvert.ToInt32(KeyCode), 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
			//	}
			//}
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
            if (vs1.GetFlexRowIndex(e.RowIndex) > 0)
			{
				if (vs1.RowOutlineLevel(vs1.GetFlexRowIndex(e.RowIndex)) == 1 && !modAccountTitle.Statics.ExpDivFlag)
				{
					//ToolTip1.SetToolTip(vs1, "Click on the Department you wish to add a division to then press Insert to add a division.");
					cell.ToolTipText =  "Click on the Department you wish to add a division to then press Insert to add a division.";
				}
				else
				{
                    //ToolTip1.SetToolTip(vs1, "");
                    cell.ToolTipText = "";
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			else if (vs1.Col == DeptCol)
			{
				vs1.EditMaxLength = DeptLength;
			}
			else if (vs1.Col == DivCol)
			{
				vs1.EditMaxLength = DivLength;
			}
			if (!EditFlag)
			{
				if (vs1.Col == NumberCol)
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
					return;
				}
				if (vs1.Col == AccountCol)
				{
					modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, true, "", "G");
					return;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == DivCol && vs1.TextMatrix(vs1.Row, DeptCol) == "")
					{
						EditFlag = true;
						vs1.Col = DeptCol;
						EditFlag = false;
					}
					else if (vs1.Col == DivCol)
					{
						EditFlag = true;
						vs1.Col = ShortCol;
						EditFlag = false;
					}
					if (vs1.Col == DeptCol && vs1.TextMatrix(vs1.Row, DeptCol) == "")
					{
						// do nothing
					}
					else if (vs1.Col == DeptCol)
					{
						EditFlag = true;
						vs1.Col = ShortCol;
						EditFlag = false;
					}
					if (vs1.Col == FundCol)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = FundLength;
						vs1.EditMaxLength = FundLength;
						return;
					}
					if (vs1.Col == BreakdownCol && Strings.Left(vs1.TextMatrix(vs1.Row, DeptCol), 1) != "1")
					{
						vs1.Editable = FCGrid.EditableSettings.flexEDNone;
						return;
					}
				}
				else
				{
					if (vs1.Col == DeptCol)
					{
						if (vs1.TextMatrix(vs1.Row, DivCol) == "")
						{
							EditFlag = true;
							vs1.Col = DivCol;
							EditFlag = false;
						}
						else
						{
							EditFlag = true;
							vs1.Col = ShortCol;
							EditFlag = false;
						}
					}
					if (vs1.Col == FundCol || vs1.Col == BreakdownCol)
					{
						// EditFlag = True
						// vs1.Col = LongCol
						// EditFlag = False
						vs1.Editable = FCGrid.EditableSettings.flexEDNone;
						return;
					}
					if (vs1.Col == DivCol)
					{
						if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
						{
							EditFlag = true;
							vs1.Col = ShortCol;
							EditFlag = false;
						}
					}
					else
					{
						if (vs1.TextMatrix(vs1.Row, DivCol) == "")
						{
							EditFlag = true;
							vs1.Col = DivCol;
							EditFlag = false;
						}
					}
				}
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			// give the cell focus
			vs1.EditSelStart = 0;
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			bool flag = false;
			//FC:FINAL:MSH - issue #888: saving correct cell indexes
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == DivCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref DivLength);
					}
				}
				if (vs1.EditText.Length == DivLength && DivLength != 0)
				{
					// if there is something in the field
					counter = row - 1;
					while (vs1.RowOutlineLevel(counter) != 0)
					{
						counter -= 1;
					}
					if (row == vs1.Rows - 1)
					{
						counter2 = row;
					}
					else
					{
						counter2 = row + 1;
					}
					while (vs1.RowOutlineLevel(counter2) != 0)
					{
						counter2 += 1;
						if (counter2 == vs1.Rows)
						{
							counter2 -= 1;
							break;
						}
					}
					for (counter = counter; counter <= counter2 - 1; counter++)
					{
						// check to see if it is a duplicate division number
						if (counter == row)
						{
							// do nothing
						}
						else
						{
							if (vs1.EditText == vs1.TextMatrix(counter, col))
							{
								// if there is a match give an error
								MessageBox.Show("This Division Number is Not Allowable Because it Matches a Previously Existing Division Number", "Invalid Division Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								// cancel their action
								vs1.EditSelStart = 0;
								// highlight the field
								vs1.EditSelLength = vs1.EditText.Length;
								return;
							}
						}
					}
				}
			}
			if (col == DeptCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref DeptLength);
						vs1.EditText = vs1.EditText + " -";
					}
				}
				if (vs1.EditText.Length == DeptLength + 2)
				{
					// if there is something in the field
					for (counter = 1; counter <= vs1.Rows - 1; counter++)
					{
						// check to see if it is a duplicate division number
						if (vs1.RowOutlineLevel(counter) == 1)
						{
							if (counter == row)
							{
								// do nothing
							}
							else
							{
								if (vs1.EditText == vs1.TextMatrix(counter, col))
								{
									// if there is a match give an error
									MessageBox.Show("This department number is not allowable because it matches a previously existing department number.", "Invalid Department Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									// cancel their action
									vs1.EditSelStart = 0;
									// highlight the field
									vs1.EditSelLength = vs1.EditText.Length;
									return;
								}
							}
						}
					}
				}
			}
			if (col == FundCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText))
					{
						MessageBox.Show("You may only enter a number in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
				}
			}
			if (col == AccountCol)
			{
				flag = modNewAccountBox.CheckAccountValidate(vs1, row, col, e.Cancel);
				if (flag)
				{
					vs1.EditText = "";
					return;
				}
				if (modAccountTitle.Statics.YearFlag)
				{
					if (modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strRevCtrAccount || modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strExpCtrAccount)
					{
						MessageBox.Show("You may not enter the Revenue or Expense Control Account as a closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vs1.EditText = "";
						return;
					}
				}
				else
				{
					if ((modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strRevCtrAccount || modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strExpCtrAccount) && Strings.Right(vs1.EditText, 2) == "00")
					{
						MessageBox.Show("You may not enter the Revenue or Expense Control Account as a closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vs1.EditText = "";
						return;
					}
				}
			}
		}

		private bool CheckDeptUsed_2(string strDept)
		{
			return CheckDeptUsed(ref strDept);
		}

		private bool CheckDeptUsed(ref string strDept)
		{
			bool CheckDeptUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			CheckDeptUsed = false;
			rs.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + strDept + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "' AND CurrentBudget <> 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckDeptUsed = true;
				return CheckDeptUsed;
			}
			return CheckDeptUsed;
		}

		private bool CheckDivUsed_2(string strDiv)
		{
			return CheckDivUsed(ref strDiv);
		}

		private bool CheckDivUsed(ref string strDiv)
		{
			bool CheckDivUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			string strDept = "";
			int counter;
			CheckDivUsed = false;
			for (counter = vs1.Row; counter >= 1; counter--)
			{
				if (vs1.TextMatrix(counter, DeptCol) != "")
				{
					strDept = Strings.Left(vs1.TextMatrix(counter, DeptCol), DeptLength);
					break;
				}
			}
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE Left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "' AND CurrentBudget <> 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE Left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + strDept + "' AND Division = '" + strDiv + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "' AND CurrentBudget <> 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
				rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE (Left(Account, 1) = 'E' OR Left(Account, 1) = 'R') AND substring(Account, 4 + " + FCConvert.ToString(DeptLength) + " , " + FCConvert.ToString(DivLength) + ") = '" + strDiv + "' AND substring(Account, 3, " + FCConvert.ToString(DeptLength) + ") = '" + strDept + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					CheckDivUsed = true;
					return CheckDivUsed;
				}
			}
			return CheckDivUsed;
		}

		private void SaveInfo()
		{
			int counter;
			bool flag = false;
			string tempDepartment = "";
			clsDRWrapper rsTesting = new clsDRWrapper();
			clsDRWrapper rs = new clsDRWrapper();
			string strFund = "";
			vs1.Select(0, 1);
			//FC:FINAL:MSH - Issue #580: vs1.Rows replaced by vs1.RowCount, because the value of vs1.Rows is greater then number of rows in table (vs1.Rows is greater by (RowsCountBeforeDeleting - NumberOfRowsToDelete) then vs1.RowCount and vs1.Rows.Count)
			//FC:FINAL:DSE:#884 Add last (added) row
			//for (counter = 1; counter <= vs1.Rows - 1; counter++)
			for (counter = 1; counter <= vs1.RowCount; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (modRegionalTown.IsRegionalTown() && Conversion.Val(Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength)) < 100)
					{
						MessageBox.Show("You may not set up a department which is less than 100 for a regional town.", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(vs1.TextMatrix(counter, DeptCol)) == 0)
					{
						MessageBox.Show("There must be a department number for each department before you can save.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
					{
						MessageBox.Show("There must be a short description for each department before you can save.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(vs1.TextMatrix(counter, FundCol)) == 0)
					{
						MessageBox.Show("There must be a fund for each department before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Row = counter;
						vs1.Col = FundCol;
						vs1.EditCell();
						return;
					}
					else
					{
						strFund = vs1.TextMatrix(counter, FundCol);
					}
					if (vs1.TextMatrix(counter, AccountCol) != "")
					{
						if (Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) != "G")
						{
							MessageBox.Show("You may only use general ledger accounts for an alternate closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vs1.Row = counter;
							vs1.Col = AccountCol;
							vs1.EditCell();
							return;
						}
						else if (FCConvert.ToDouble(strFund) != modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(counter, AccountCol)))
						{
							MessageBox.Show("The fund your department uses must be the fund of the closeout account you are using for the department or any of it's divisions.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vs1.Row = counter;
							vs1.Col = AccountCol;
							vs1.EditCell();
							return;
						}
					}
				}
				else
				{
					if (vs1.TextMatrix(counter, DivCol) != "")
					{
						if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
						{
							flag = true;
							break;
						}
					}
					if (vs1.TextMatrix(counter, AccountCol) != "")
					{
						if (Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) != "G")
						{
							MessageBox.Show("You may only use general ledger accounts for an alternate closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vs1.Row = counter;
							vs1.Col = AccountCol;
							vs1.EditCell();
							return;
						}
						else if (FCConvert.ToDouble(strFund) != modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(counter, AccountCol)))
						{
							MessageBox.Show("The fund your department uses must be the fund of the closeout account you are using for the department or any of it's divisions.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vs1.Row = counter;
							vs1.Col = AccountCol;
							vs1.EditCell();
							return;
						}
					}
				}
			}
			if (flag)
			{
				MessageBox.Show("There must be a short description for every division before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			counter = 0;
			while (DeletedDepartments[counter] != "")
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + DeletedDepartments[counter] + "'");
				while (rs.EndOfFile() != true)
				{
					rs.Delete();
					rs.Update();
				}
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' or AccountType = 'R') and FirstAccountField = '" + DeletedDepartments[counter] + "'");
				while (rs.EndOfFile() != true)
				{
					rs.Delete();
					rs.Update();
				}
				counter += 1;
			}
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//FC:FINAL:MSH - Issue #580: vs1.Rows replaced by vs1.RowCount, because the value of vs1.Rows is greater then number of rows in table (vs1.Rows is greater by (RowsCountBeforeDeleting - NumberOfRowsToDelete) then vs1.RowCount and vs1.Rows.Count)
			//for (counter = 1; counter <= vs1.Rows - 1; counter++)
			//for (counter = 1; counter <= vs1.RowCount - 1; counter++)
			// FC:FINAL:VGE - #884 Fixing upper bound to include last added row.
			for (counter = 1; counter <= vs1.RowCount; counter++)
			{
				//Application.DoEvents();
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
					{
						// is this a new record
						rsTesting.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength) + "' AND Division = '" + modValidateAccount.GetFormat("0", ref DivLength) + "'");
						if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
						{
							MessageBox.Show("Saving this information would create duplicate entries for department " + Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength) + ".", "Duplicate Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							frmWait.InstancePtr.Unload();
							return;
						}
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE ID = 0");
						rs.AddNew();
						// if so add it
						tempDepartment = Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength);
						rs.Set_Fields("Department", tempDepartment);
						rs.Set_Fields("Division", modValidateAccount.GetFormat("0", ref DivLength));
						rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
						if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
						}
						else
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
						}
						rs.Set_Fields("Fund", vs1.TextMatrix(counter, FundCol));
						rs.Set_Fields("CloseoutAccount", vs1.TextMatrix(counter, AccountCol));
						if (modRegionalTown.IsRegionalTown())
						{
							rs.Set_Fields("BreakdownCode", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, BreakdownCol))));
						}
						else
						{
							rs.Set_Fields("BreakdownCode", 0);
						}
						rs.Update();
						// update the database
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
						rs.Edit();
						// if not a new record edit the existing record
						tempDepartment = Strings.Mid(vs1.TextMatrix(counter, DeptCol), 1, DeptLength);
						rs.Set_Fields("Department", tempDepartment);
						rs.Set_Fields("Division", modValidateAccount.GetFormat("0", ref DivLength));
						rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
						if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
						}
						else
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
						}
						rs.Set_Fields("Fund", vs1.TextMatrix(counter, FundCol));
						rs.Set_Fields("CloseoutAccount", vs1.TextMatrix(counter, AccountCol));
						if (modRegionalTown.IsRegionalTown())
						{
							rs.Set_Fields("BreakdownCode", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, BreakdownCol))));
						}
						else
						{
							rs.Set_Fields("BreakdownCode", 0);
						}
						rs.Update();
						// update the database
					}
				}
				else
				{
					if (vs1.TextMatrix(counter, DivCol) == "")
					{
						// if the row contains no data
						// do nothing
					}
					else
					{
						if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
						{
							// is this a new record
							rsTesting.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + tempDepartment + "' AND Division = '" + vs1.TextMatrix(counter, DivCol) + "'");
							if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
							{
								MessageBox.Show("Saving this information would create duplicate entries for division " + tempDepartment + "-" + vs1.TextMatrix(counter, DivCol) + ".", "Duplicate Division", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								frmWait.InstancePtr.Unload();
								return;
							}
							rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE ID = 0");
							rs.AddNew();
							// if so add it
							rs.Set_Fields("Department", tempDepartment);
							rs.Set_Fields("Division", vs1.TextMatrix(counter, DivCol));
							// put information in the database
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							rs.Set_Fields("Fund", vs1.TextMatrix(counter, FundCol));
							rs.Set_Fields("CloseoutAccount", vs1.TextMatrix(counter, AccountCol));
							rs.Update();
							// update the database
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
							rs.Edit();
							// if not a new record edit the existing record
							rs.Set_Fields("Department", tempDepartment);
							rs.Set_Fields("Division", vs1.TextMatrix(counter, DivCol));
							// put information in the database
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							rs.Set_Fields("Fund", vs1.TextMatrix(counter, FundCol));
							rs.Set_Fields("CloseoutAccount", vs1.TextMatrix(counter, AccountCol));
							rs.Update();
							// update the database
						}
					}
				}
			}
			frmWait.InstancePtr.Unload();
			blnDirty = false;
			//Application.DoEvents();
			MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				Close();
			}
		}
	}
}
