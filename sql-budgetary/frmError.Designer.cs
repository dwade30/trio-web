﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmError.
	/// </summary>
	partial class frmError : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCButton cmdContinue;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1_0;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmError));
			this.components = new System.ComponentModel.Container();
			//this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Text1 = new fecherFoundation.FCTextBox();
			this.Picture1 = new fecherFoundation.FCPictureBox();
			this.cmdExit = new fecherFoundation.FCButton();
			this.cmdContinue = new fecherFoundation.FCButton();
			this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			//
			// Text1
			//
			this.Text1.Name = "Text1";
			this.Text1.TabIndex = 9;
			this.Text1.Location = new System.Drawing.Point(0, 0);
			this.Text1.Size = new System.Drawing.Size(406, 17);
			this.Text1.Text = "  Error Message";
			this.Text1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(255)));
			this.Text1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(0)));
			this.Text1.BorderStyle = Wisej.Web.BorderStyle.None;
			//this.Text1.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Picture1
			//
			this.Picture1.Controls.Add(this.cmdExit);
			this.Picture1.Controls.Add(this.cmdContinue);
			this.Picture1.Controls.Add(this.RichTextBox1);
			this.Picture1.Controls.Add(this.Label1_4);
			this.Picture1.Controls.Add(this.Label1_3);
			this.Picture1.Controls.Add(this.Label1_2);
			this.Picture1.Controls.Add(this.Label1_1);
			this.Picture1.Controls.Add(this.Label2);
			this.Picture1.Controls.Add(this.Label1_0);
			this.Picture1.Name = "Picture1";
			this.Picture1.TabIndex = 0;
			this.Picture1.Location = new System.Drawing.Point(0, 16);
			this.Picture1.Size = new System.Drawing.Size(406, 325);
			this.Picture1.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.Picture1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// cmdExit
			//
			this.cmdExit.Name = "cmdExit";
			this.cmdExit.TabIndex = 8;
			this.cmdExit.Location = new System.Drawing.Point(316, 40);
			this.cmdExit.Size = new System.Drawing.Size(82, 25);
			this.cmdExit.Text = "Exit";
			this.cmdExit.BackColor = System.Drawing.SystemColors.Control;
			this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
			//
			// cmdContinue
			//
			this.cmdContinue.Name = "cmdContinue";
			this.cmdContinue.TabIndex = 7;
			this.cmdContinue.Location = new System.Drawing.Point(316, 8);
			this.cmdContinue.Size = new System.Drawing.Size(82, 25);
			this.cmdContinue.Text = "Continue";
			this.cmdContinue.BackColor = System.Drawing.SystemColors.Control;
			this.cmdContinue.Click += new System.EventHandler(this.cmdContinue_Click);
			//
			// RichTextBox1
			//
			this.RichTextBox1.Name = "RichTextBox1";
			this.RichTextBox1.Enabled = true;
			this.RichTextBox1.TabIndex = 1;
			this.RichTextBox1.Location = new System.Drawing.Point(0, 121);
			this.RichTextBox1.Size = new System.Drawing.Size(406, 203);
			this.RichTextBox1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// Label1_4
			//
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.TabIndex = 10;
			this.Label1_4.Location = new System.Drawing.Point(8, 63);
			this.Label1_4.Size = new System.Drawing.Size(373, 25);
			this.Label1_4.Text = "Label1";
			this.Label1_4.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// Label1_3
			//
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.TabIndex = 2;
			this.Label1_3.Location = new System.Drawing.Point(8, 49);
			this.Label1_3.Size = new System.Drawing.Size(373, 25);
			this.Label1_3.Text = "Label1";
			this.Label1_3.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// Label1_2
			//
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.TabIndex = 3;
			this.Label1_2.Location = new System.Drawing.Point(8, 32);
			this.Label1_2.Size = new System.Drawing.Size(300, 25);
			this.Label1_2.Text = "Label1";
			this.Label1_2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// Label1_1
			//
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.TabIndex = 6;
			this.Label1_1.Location = new System.Drawing.Point(8, 18);
			this.Label1_1.Size = new System.Drawing.Size(292, 25);
			this.Label1_1.Text = "Label1";
			this.Label1_1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 5;
			this.Label2.Location = new System.Drawing.Point(2, 105);
			this.Label2.Size = new System.Drawing.Size(122, 17);
			this.Label2.Text = "Call Stack";
			this.Label2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(255)));
			//this.Label2.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label1_0
			//
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.TabIndex = 4;
			this.Label1_0.Location = new System.Drawing.Point(8, 4);
			this.Label1_0.Size = new System.Drawing.Size(300, 25);
			this.Label1_0.Text = "Label1";
			this.Label1_0.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			//
			// frmError
			//
			this.ClientSize = new System.Drawing.Size(414, 347);
			this.ClientArea.Controls.Add(this.Text1);
			this.ClientArea.Controls.Add(this.Picture1);
			this.Name = "frmError";
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(64)), ((System.Byte)(64)), ((System.Byte)(64)));
			this.ShowInTaskbar = false;
			this.MinimizeBox = false;
			this.MaximizeBox = false;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.DefaultLocation;
			this.Load += new System.EventHandler(this.frmError_Load);
			this.Text = "Error";
			//this.Label1.SetIndex(Label1_4, FCConvert.ToInt16(4));
			//this.Label1.SetIndex(Label1_3, FCConvert.ToInt16(3));
			//this.Label1.SetIndex(Label1_2, FCConvert.ToInt16(2));
			//this.Label1.SetIndex(Label1_1, FCConvert.ToInt16(1));
			//this.Label1.SetIndex(Label1_0, FCConvert.ToInt16(0));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			this.Text1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
			this.Label2.ResumeLayout(false);
			this.Picture1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
