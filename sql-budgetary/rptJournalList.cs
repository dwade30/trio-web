﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournalList.
	/// </summary>
	public partial class rptJournalList : BaseSectionReport
	{
		public static rptJournalList InstancePtr
		{
			get
			{
				return (rptJournalList)Sys.GetInstance(typeof(rptJournalList));
			}
		}

		protected rptJournalList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptJournalList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		// keeps track of page for report
		bool blnAP;
		bool blnCR;
		bool blnCD;
		// booleans to let the program know what types of journals we are listing
		bool blnGJ;
		bool blnPY;
		bool blnEnc;
		bool blnFirstRecord;
		// boolean for if this is the first time through the fetchdata procedure
		string strDateRange = "";
		// A - All Months  R - Range of Months  S - Single Month
		int intLowDate;
		// date range for reporting
		int intHighDate;
		clsDRWrapper rsMasterInfo = new clsDRWrapper();
		string strPeriodCheck = "";
		// vbPorter upgrade warning: curExpTotal As Decimal	OnWrite(short, Decimal)
		Decimal curExpTotal;
		// vbPorter upgrade warning: curRevTotal As Decimal	OnWrite(short, Decimal)
		Decimal curRevTotal;
		// vbPorter upgrade warning: curGLTotal As Decimal	OnWrite(short, Decimal)
		Decimal curGLTotal;
		// vbPorter upgrade warning: curCashTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCashTotal;
		// vbPorter upgrade warning: curEncTotal As Decimal	OnWrite(short, Decimal)
		Decimal curEncTotal;
		string strCashAcct = "";
		string strCashAcct2 = "";
		string strCashAcct3 = "";
		string strEncOffsetAcct = "";
		string strExpOffsetAcct = "";
		string strRevOffsetAcct = "";
		string strPastYearEncAcct = "";
		int intJournalCount;
		int intOOBCount;
		int intExpErrorCount;
		int intRevErrorCount;
		int intEncErrorCount;
		bool blnShowOFInfo;
		bool blnOFInfoExists;

		public rptJournalList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Journal Summary List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsMasterInfo.MoveNext();
				eArgs.EOF = rsMasterInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsCashInfo = new clsDRWrapper();
			string strJournalType;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			blnOFInfoExists = false;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (rsMasterInfo.EndOfFile() != true && rsMasterInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CA'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strCashAcct = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CM'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strCashAcct2 = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'CP'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strCashAcct3 = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup a cash account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EC'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strExpOffsetAcct = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup an expense control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup an expense control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RC'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strRevOffsetAcct = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup a revenue control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup a revenue control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'EO'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strEncOffsetAcct = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup an Encumbrance Control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup an Encumbrance Control account before you may run this report", "No Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			rsCashInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PE'");
			if (rsCashInfo.EndOfFile() != true && rsCashInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strPastYearEncAcct = Strings.Trim(FCConvert.ToString(rsCashInfo.Get_Fields("Account")));
				}
				else
				{
					MessageBox.Show("You must setup a Past Year Encumbrance Control account before you may run this report", "No Past Year Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Cancel();
					this.Close();
					return;
				}
			}
			else
			{
				MessageBox.Show("You must setup a Past Year Encumbrance Control account before you may run this report", "No Past Year Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Cancel();
				this.Close();
				return;
			}
			strJournalType = "";
			if (blnAP && blnCR && blnCD && blnGJ && blnEnc)
			{
				lblDeptRange.Text = "All Journal Types";
			}
			else
			{
				if (blnAP)
				{
					strJournalType += "AP, ";
				}
				if (blnCR)
				{
					strJournalType += "CR, ";
				}
				if (blnCD)
				{
					strJournalType += "CD, ";
				}
				if (blnGJ)
				{
					strJournalType += "GJ, ";
				}
				if (blnPY)
				{
					strJournalType += "PY, ";
				}
				if (blnEnc)
				{
					strJournalType += "EN, ";
				}
				strJournalType = Strings.Left(strJournalType, strJournalType.Length - 2);
				if (strJournalType.Length == 2)
				{
					strJournalType += " Journal Type";
				}
				else
				{
					strJournalType = Strings.Left(strJournalType, strJournalType.Length - 2) + " And " + Strings.Right(strJournalType, 2) + " Journal Types";
				}
				lblDeptRange.Text = strJournalType;
			}
			if (strDateRange == "S")
			{
				lblDateRange.Text = MonthCalc(intLowDate);
			}
			else if (strDateRange == "M")
			{
				lblDateRange.Text = MonthCalc(intLowDate) + " to " + MonthCalc(intHighDate);
			}
			else
			{
				lblDateRange.Text = "ALL Months";
			}
			curExpTotal = 0;
			curRevTotal = 0;
			curGLTotal = 0;
			curCashTotal = 0;
			curEncTotal = 0;
			intJournalCount = 0;
			intOOBCount = 0;
			intExpErrorCount = 0;
			intRevErrorCount = 0;
			intEncErrorCount = 0;
			blnFirstRecord = true;
			return;
		}

		//private void ActiveReport_Terminate(object sender, EventArgs e)
		//{
		//	frmJournalListing.InstancePtr.Show(App.MainForm);
		//}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldOF.Visible = false;
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC")
			{
				FillDetailWithAP();
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsMasterInfo.Get_Fields("Type") == "EN")
			{
				FillDetailWithEnc();
			}
			else
			{
				FillDetailWithOtherJournalType();
			}
			if (fldOB.Visible == false && FCConvert.ToString(rsMasterInfo.Get_Fields_String("Status")) == "P")
			{
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!CheckOBF_8(FCConvert.ToInt32(rsMasterInfo.Get_Fields("JournalNumber")), rsMasterInfo.Get_Fields("Type")))
				{
					fldOF.Visible = true;
					blnOFInfoExists = true;
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldExpTotal.Text = Strings.Format(curExpTotal, "#,##0.00");
			fldRevTotal.Text = Strings.Format(curRevTotal, "#,##0.00");
			fldLedgerTotal.Text = Strings.Format(curGLTotal, "#,##0.00");
			fldCashTotal.Text = Strings.Format(curCashTotal, "#,##0.00");
			fldEncTotal.Text = Strings.Format(curEncTotal, "#,##0.00");
			fldJournalCount.Text = intJournalCount.ToString();
			fldOOBCount.Text = intOOBCount.ToString();
			fldExpErrorCount.Text = intExpErrorCount.ToString();
			fldRevErrorCount.Text = intRevErrorCount.ToString();
			fldEncErrorCount.Text = intEncErrorCount.ToString();
			if (blnShowOFInfo && blnOFInfoExists)
			{
				this.UserData = rsMasterInfo.Name();
				subOFDetail.Report = new srptOFDetailInfo();
				GroupFooter1.KeepTogether = false;
			}
			else
			{
				GroupFooter1.KeepTogether = true;
				subOFDetail = null;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}
		// vbPorter upgrade warning: intLowMonth As short	OnWrite(string)
		// vbPorter upgrade warning: intHighMonth As short	OnWrite(string)
		public void Init(bool blnAPJournals, bool blnCRJournals, bool blnCDJournals, bool blnGJJournals, bool blnPYJournals, bool blnEncJournals, string strMonthRange, string strShowOFDetail, short intLowMonth = 0, short intHighMonth = 0)
		{
			string strJournalTypeSQL;
			// initialize variables used in report
			blnAP = blnAPJournals;
			blnCR = blnCRJournals;
			blnCD = blnCDJournals;
			blnGJ = blnGJJournals;
			blnPY = blnPYJournals;
			blnEnc = blnEncJournals;
			strDateRange = strMonthRange;
			blnShowOFInfo = strShowOFDetail == "Y";
			intLowDate = intLowMonth;
			intHighDate = intHighMonth;
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			// create sql statement for journal types based on what the user selected on the frmJournalListing form
			strJournalTypeSQL = "(";
			if (blnAP)
			{
				strJournalTypeSQL += "Type = 'AP' OR Type = 'AC' OR ";
			}
			if (blnCR)
			{
				strJournalTypeSQL += "Type = 'CR' OR Type = 'CW' OR ";
			}
			if (blnCD)
			{
				strJournalTypeSQL += "Type = 'CD' OR ";
			}
			if (blnGJ)
			{
				strJournalTypeSQL += "Type = 'GJ' OR ";
			}
			if (blnPY)
			{
				strJournalTypeSQL += "Type = 'PY' OR ";
			}
			if (blnEnc)
			{
				strJournalTypeSQL += "Type = 'EN' OR ";
			}
			strJournalTypeSQL = Strings.Left(strJournalTypeSQL, strJournalTypeSQL.Length - 4) + ") AND (Status = 'P' OR Status = 'D')";
			// get information on any journals that match the search criteria
			if (strDateRange == "A")
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE (" + strJournalTypeSQL + ") ORDER BY JournalNumber");
			}
			else
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE (" + strJournalTypeSQL + ") AND (Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + ") ORDER BY JournalNumber");
			}
			// depending on what the user selected print the report or preview it
			if (frmJournalListing.InstancePtr.blnPrint)
			{
				rptJournalList.InstancePtr.PrintReport();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			//Application.DoEvents();
		}

		private void FillDetailWithAP()
		{
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curExpenseMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpenseMoney;
			// vbPorter upgrade warning: curRevenueMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevenueMoney;
			// vbPorter upgrade warning: curLedgerMoney As Decimal	OnWrite(short, Decimal)
			Decimal curLedgerMoney;
			// vbPorter upgrade warning: curCashMoney As Decimal	OnWrite(short, Decimal)
			Decimal curCashMoney;
			// vbPorter upgrade warning: curEncumbranceMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncumbranceMoney;
			// vbPorter upgrade warning: curExpCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpCtrlMoney;
			// vbPorter upgrade warning: curRevCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevCtrlMoney;
			// vbPorter upgrade warning: curEncOffMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncOffMoney;
			// vbPorter upgrade warning: curOOBTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOOBTotal;
			string strTempAccount = "";
			curExpenseMoney = 0;
			curRevenueMoney = 0;
			curLedgerMoney = 0;
			curCashMoney = 0;
			curEncumbranceMoney = 0;
			curRevCtrlMoney = 0;
			curExpCtrlMoney = 0;
			curEncOffMoney = 0;
			curOOBTotal = 0;
			intJournalCount += 1;
			if (FCConvert.ToString(rsMasterInfo.Get_Fields_String("Status")) != "D")
			{
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " + rsMasterInfo.Get_Fields("JournalNumber") + ")");
				if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
				{
					do
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							curOOBTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
						}
						if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Project")) != "CTRL")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								curExpenseMoney += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
								curEncumbranceMoney += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
								curRevenueMoney += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
								curEncumbranceMoney += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
							}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct2 || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct3)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCashMoney += rsDetailInfo.Get_Fields("Amount");
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
									curLedgerMoney += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
									curEncumbranceMoney += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
								}
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								strTempAccount = modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account"));
								if (strTempAccount == strCashAcct || strTempAccount == strCashAcct2 || strTempAccount == strCashAcct3)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCashMoney += rsDetailInfo.Get_Fields("Amount");
								}
								else if (strTempAccount == strExpOffsetAcct)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curExpCtrlMoney += rsDetailInfo.Get_Fields("Amount");
								}
								else if (strTempAccount == strRevOffsetAcct)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curRevCtrlMoney += rsDetailInfo.Get_Fields("Amount");
								}
								else if (strTempAccount == strEncOffsetAcct || strTempAccount == modBudgetaryAccounting.Statics.strPastYearEncControlAccount)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curEncOffMoney += rsDetailInfo.Get_Fields("Amount");
								}
							}
						}
						rsDetailInfo.MoveNext();
					}
					while (rsDetailInfo.EndOfFile() != true);
				}
			}
			if (curOOBTotal != 0)
			{
				fldOB.Visible = true;
				intOOBCount += 1;
			}
			else
			{
				fldOB.Visible = false;
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("JournalNumber"), 4);
			fldDate.Text = Strings.Format(rsMasterInfo.Get_Fields_DateTime("StatusChangeDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC")
			{
				fldType.Text = "AP";
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsMasterInfo.Get_Fields("Type") == "CW")
			{
				fldType.Text = "CR";
			}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsMasterInfo.Get_Fields("Type") == "PY")
			{
				fldType.Text = "GJ";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldType.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
			}
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("Period"), 2);
			if (curExpenseMoney != curExpCtrlMoney)
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = Strings.Format(curExpCtrlMoney, "#,##0.00");
				fldExpWrong.Visible = true;
				intExpErrorCount += 1;
			}
			else
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = "";
				fldExpWrong.Visible = false;
			}
			if (curRevenueMoney != curRevCtrlMoney)
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = Strings.Format(curRevCtrlMoney, "#,##0.00");
				fldRevWrong.Visible = true;
				intRevErrorCount += 1;
			}
			else
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = "";
				fldRevWrong.Visible = false;
			}
			if (curEncumbranceMoney != curEncOffMoney)
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = Strings.Format(curEncOffMoney, "#,##0.00");
				fldEncWrong.Visible = true;
				intEncErrorCount += 1;
			}
			else
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = "";
				fldEncWrong.Visible = false;
			}
			fldLedger.Text = Strings.Format(curLedgerMoney, "#,##0.00");
			fldCash.Text = Strings.Format(curCashMoney, "#,##0.00");
			if (FCConvert.ToString(rsMasterInfo.Get_Fields_String("Status")) == "D")
			{
				fldDescription.Text = "** DELETED **";
			}
			else
			{
				fldDescription.Text = Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description")));
			}
			curExpTotal += curExpenseMoney;
			curRevTotal += curRevenueMoney;
			curGLTotal += curLedgerMoney;
			curCashTotal += curCashMoney;
			curEncTotal += curEncumbranceMoney;
		}

		private void FillDetailWithEnc()
		{
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curExpenseMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpenseMoney;
			// vbPorter upgrade warning: curRevenueMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevenueMoney;
			// vbPorter upgrade warning: curLedgerMoney As Decimal	OnWrite(short, Decimal)
			Decimal curLedgerMoney;
			// vbPorter upgrade warning: curCashMoney As Decimal	OnWrite(short, Decimal)
			Decimal curCashMoney;
			// vbPorter upgrade warning: curEncumbranceMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncumbranceMoney;
			// vbPorter upgrade warning: curExpCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpCtrlMoney;
			// vbPorter upgrade warning: curRevCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevCtrlMoney;
			// vbPorter upgrade warning: curEncOffMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncOffMoney;
			// vbPorter upgrade warning: curOOBTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOOBTotal;
			string strTempAccount = "";
			curExpenseMoney = 0;
			curRevenueMoney = 0;
			curLedgerMoney = 0;
			curCashMoney = 0;
			curEncumbranceMoney = 0;
			curRevCtrlMoney = 0;
			curExpCtrlMoney = 0;
			curEncOffMoney = 0;
			curOOBTotal = 0;
			intJournalCount += 1;
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsDetailInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID IN (SELECT ID FROM Encumbrances WHERE JournalNumber = " + rsMasterInfo.Get_Fields("JournalNumber") + ")");
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "L")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curOOBTotal += rsDetailInfo.Get_Fields("Amount");
					}
					if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Project")) != "CTRL")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curExpenseMoney += rsDetailInfo.Get_Fields("Amount");
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curRevenueMoney += rsDetailInfo.Get_Fields("Amount");
						}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct2 || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct3)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCashMoney += rsDetailInfo.Get_Fields("Amount");
							}
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									else if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strPastYearEncAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curLedgerMoney += rsDetailInfo.Get_Fields("Amount");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curLedgerMoney += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields_Decimal("Liquidated") + rsDetailInfo.Get_Fields_Decimal("Adjustments");
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTempAccount = modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account"));
							if (strTempAccount == strCashAcct || strTempAccount == strCashAcct2 || strTempAccount == strCashAcct3)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCashMoney += rsDetailInfo.Get_Fields("Amount");
							}
							else if (strTempAccount == strExpOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curExpCtrlMoney += rsDetailInfo.Get_Fields("Amount");
							}
							else if (strTempAccount == strRevOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curRevCtrlMoney += rsDetailInfo.Get_Fields("Amount");
							}
							else if (strTempAccount == strEncOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curEncOffMoney += rsDetailInfo.Get_Fields("Amount");
							}
						}
					}
					rsDetailInfo.MoveNext();
				}
				while (rsDetailInfo.EndOfFile() != true);
			}
			if (curOOBTotal != 0)
			{
				fldOB.Visible = true;
				intOOBCount += 1;
			}
			else
			{
				fldOB.Visible = false;
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("JournalNumber"), 4);
			fldDate.Text = Strings.Format(rsMasterInfo.Get_Fields_DateTime("StatusChangeDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC")
			{
				fldType.Text = "AP";
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsMasterInfo.Get_Fields("Type") == "CW")
			{
				fldType.Text = "CR";
			}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsMasterInfo.Get_Fields("Type") == "PY")
			{
				fldType.Text = "GJ";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldType.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
			}
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("Period"), 2);
			curEncumbranceMoney = (curExpenseMoney + curRevenueMoney + curLedgerMoney) * -1;
			if (curExpenseMoney != curExpCtrlMoney)
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = Strings.Format(curExpCtrlMoney, "#,##0.00");
				fldExpWrong.Visible = true;
				intExpErrorCount += 1;
			}
			else
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = "";
				fldExpWrong.Visible = false;
			}
			if (curRevenueMoney != curRevCtrlMoney)
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = Strings.Format(curRevCtrlMoney, "#,##0.00");
				fldRevWrong.Visible = true;
				intRevErrorCount += 1;
			}
			else
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = "";
				fldRevWrong.Visible = false;
			}
			if (curEncumbranceMoney != curEncOffMoney)
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = Strings.Format(curEncOffMoney, "#,##0.00");
				fldEncWrong.Visible = true;
				intEncErrorCount += 1;
			}
			else
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = "";
				fldEncWrong.Visible = false;
			}
			fldLedger.Text = Strings.Format(curLedgerMoney, "#,##0.00");
			fldCash.Text = Strings.Format(curCashMoney, "#,##0.00");
			if (FCConvert.ToString(rsMasterInfo.Get_Fields_String("Status")) == "D")
			{
				fldDescription.Text = "** DELETED **";
			}
			else
			{
				fldDescription.Text = Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description")));
			}
			curExpTotal += curExpenseMoney;
			curRevTotal += curRevenueMoney;
			curGLTotal += curLedgerMoney;
			curCashTotal += curCashMoney;
			curEncTotal += curEncumbranceMoney;
		}

		private void FillDetailWithOtherJournalType()
		{
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curExpenseMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpenseMoney;
			// vbPorter upgrade warning: curRevenueMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevenueMoney;
			// vbPorter upgrade warning: curLedgerMoney As Decimal	OnWrite(short, Decimal)
			Decimal curLedgerMoney;
			// vbPorter upgrade warning: curCashMoney As Decimal	OnWrite(short, Decimal)
			Decimal curCashMoney;
			// vbPorter upgrade warning: curEncumbranceMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncumbranceMoney;
			// vbPorter upgrade warning: curExpCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curExpCtrlMoney;
			// vbPorter upgrade warning: curRevCtrlMoney As Decimal	OnWrite(short, Decimal)
			Decimal curRevCtrlMoney;
			// vbPorter upgrade warning: curEncOffMoney As Decimal	OnWrite(short, Decimal)
			Decimal curEncOffMoney;
			// vbPorter upgrade warning: curOOBTotal As Decimal	OnWrite(short, Decimal)
			Decimal curOOBTotal;
			curExpenseMoney = 0;
			curRevenueMoney = 0;
			curLedgerMoney = 0;
			curCashMoney = 0;
			curEncumbranceMoney = 0;
			curRevCtrlMoney = 0;
			curExpCtrlMoney = 0;
			curEncOffMoney = 0;
			curOOBTotal = 0;
			intJournalCount += 1;
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsDetailInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsMasterInfo.Get_Fields("JournalNumber"));
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G" && Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "L")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curOOBTotal += rsDetailInfo.Get_Fields("Amount");
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "L" || (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R"))
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "E" && FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curEncumbranceMoney += rsDetailInfo.Get_Fields("Amount") * -1;
						}
						else
						{
							if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "E")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curEncumbranceMoney += rsDetailInfo.Get_Fields("Amount") * -1;
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curExpenseMoney += rsDetailInfo.Get_Fields("Amount");
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curRevenueMoney += rsDetailInfo.Get_Fields("Amount");
							}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct2 || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct3)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCashMoney += rsDetailInfo.Get_Fields("Amount");
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curLedgerMoney += rsDetailInfo.Get_Fields("Amount");
								}
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct2 || modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strCashAcct3)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCashMoney += rsDetailInfo.Get_Fields("Amount");
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strExpOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curExpCtrlMoney += rsDetailInfo.Get_Fields("Amount");
							}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								else if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strRevOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curRevCtrlMoney += rsDetailInfo.Get_Fields("Amount");
							}
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									else if (modBudgetaryAccounting.GetAccount(rsDetailInfo.Get_Fields("Account")) == strEncOffsetAcct)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curEncOffMoney += rsDetailInfo.Get_Fields("Amount");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curLedgerMoney += rsDetailInfo.Get_Fields("Amount");
							}
						}
					}
					rsDetailInfo.MoveNext();
				}
				while (rsDetailInfo.EndOfFile() != true);
			}
			if (curOOBTotal != 0)
			{
				fldOB.Visible = true;
				intOOBCount += 1;
			}
			else
			{
				fldOB.Visible = false;
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("JournalNumber"), 4);
			fldDate.Text = Strings.Format(rsMasterInfo.Get_Fields_DateTime("StatusChangeDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC")
			{
				fldType.Text = "AP";
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsMasterInfo.Get_Fields("Type") == "CW")
			{
				fldType.Text = "CR";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldType.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
			}
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("Period"), 2);
			if (curExpenseMoney != curExpCtrlMoney)
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = Strings.Format(curExpCtrlMoney, "#,##0.00");
				fldExpWrong.Visible = true;
				intExpErrorCount += 1;
			}
			else
			{
				fldExpense.Text = Strings.Format(curExpenseMoney, "#,##0.00");
				fldExpControl.Text = "";
				fldExpWrong.Visible = false;
			}
			if (curRevenueMoney != curRevCtrlMoney)
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = Strings.Format(curRevCtrlMoney, "#,##0.00");
				fldRevWrong.Visible = true;
				intRevErrorCount += 1;
			}
			else
			{
				fldRevenue.Text = Strings.Format(curRevenueMoney, "#,##0.00");
				fldRevControl.Text = "";
				fldRevWrong.Visible = false;
			}
			if (curEncumbranceMoney != curEncOffMoney)
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = Strings.Format(curEncOffMoney, "#,##0.00");
				fldEncWrong.Visible = true;
				intEncErrorCount += 1;
			}
			else
			{
				fldEncumbrance.Text = Strings.Format(curEncumbranceMoney, "#,##0.00");
				fldEncControl.Text = "";
				fldEncWrong.Visible = false;
			}
			fldLedger.Text = Strings.Format(curLedgerMoney, "#,##0.00");
			fldCash.Text = Strings.Format(curCashMoney, "#,##0.00");
			if (FCConvert.ToString(rsMasterInfo.Get_Fields_String("Status")) == "D")
			{
				fldDescription.Text = "** DELETED **";
			}
			else
			{
				fldDescription.Text = Strings.Trim(FCConvert.ToString(rsMasterInfo.Get_Fields_String("Description")));
			}
			curExpTotal += curExpenseMoney;
			curRevTotal += curRevenueMoney;
			curGLTotal += curLedgerMoney;
			curCashTotal += curCashMoney;
			curEncTotal += curEncumbranceMoney;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private bool CheckOBF_8(int lngJournal, string strJournalType)
		{
			return CheckOBF(ref lngJournal, ref strJournalType);
		}

		private bool CheckOBF(ref int lngJournal, ref string strJournalType)
		{
			bool CheckOBF = false;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			modBudgetaryAccounting.FundType[] RegularAccountInfo = null;
			modBudgetaryAccounting.FundType[] ControlAccountInfo = null;
			modBudgetaryAccounting.FundType[] RegularFundInfo = null;
			modBudgetaryAccounting.FundType[] ControlFundInfo = null;
			int counter = 0;
			CheckOBF = true;
			if (strJournalType == "AP" || strJournalType == "AC")
			{
                rsJournalInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + ")");
                if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                {
                    RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                    counter = 0;
                    do
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Account = FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Amount = FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount") - rsJournalInfo.Get_Fields("Discount") - rsJournalInfo.Get_Fields_Decimal("Encumbrance"));
                        RegularAccountInfo[counter].Description = FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                        rsJournalInfo.MoveNext();
                        counter += 1;
                    }
                    while (rsJournalInfo.EndOfFile() != true);
                    RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                }
                else
                {
                    return CheckOBF;
                }
                for (counter = 1; counter <= 999; counter++)
                {
                    if (RegularFundInfo[counter].Amount != 0)
                    {
                        CheckOBF = false;
                        return CheckOBF;
                    }
                }
			}
			else if (strJournalType == "EN")
			{
                rsJournalInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + ")");
                if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                {
                    RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                    counter = 0;
                    do
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Account = FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Amount = FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
                        RegularAccountInfo[counter].Description = FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                        rsJournalInfo.MoveNext();
                        counter += 1;
                    }
                    while (rsJournalInfo.EndOfFile() != true);
                    RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                }
                else
                {
                    return CheckOBF;
                }
                for (counter = 1; counter <= 999; counter++)
                {
                    if (RegularFundInfo[counter].Amount != 0)
                    {
                        CheckOBF = false;
                        return CheckOBF;
                    }
                }
			}
			else
			{
                rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Left(Account, 1) = 'G'");
                if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                {
                    RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                    counter = 0;
                    do
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Account = FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsJournalInfo.Get_Fields("Type"));
                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        RegularAccountInfo[counter].Amount = FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
                        RegularAccountInfo[counter].Description = FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                        rsJournalInfo.MoveNext();
                        counter += 1;
                    }
                    while (rsJournalInfo.EndOfFile() != true);
                    RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                }
                else
                {
                    return CheckOBF;
                }
                for (counter = 1; counter <= 999; counter++)
                {
                    if (RegularFundInfo[counter].Amount != 0)
                    {
                        CheckOBF = false;
                        return CheckOBF;
                    }
                }
			}
			return CheckOBF;
		}

		private void rptJournalList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptJournalList.Caption	= "Journal Summary List";
			//rptJournalList.Icon	= "rptJournalList.dsx":0000";
			//rptJournalList.Left	= 0;
			//rptJournalList.Top	= 0;
			//rptJournalList.Width	= 11880;
			//rptJournalList.Height	= 8595;
			//rptJournalList.StartUpPosition	= 3;
			//rptJournalList.SectionData	= "rptJournalList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
