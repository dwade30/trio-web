﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptExpRevSummaryExpLevel.
	/// </summary>
	partial class srptExpRevSummaryExpLevel
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptExpRevSummaryExpLevel));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.subLowerLevelReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSpent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subLowerLevelReport
			});
			this.Detail.Height = 0.1354167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// subLowerLevelReport
			// 
			this.subLowerLevelReport.CloseBorder = false;
			this.subLowerLevelReport.Height = 0.125F;
			this.subLowerLevelReport.Left = 0F;
			this.subLowerLevelReport.Name = "subLowerLevelReport";
			this.subLowerLevelReport.Report = null;
			this.subLowerLevelReport.Top = 0F;
			this.subLowerLevelReport.Width = 7.96875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldBudget,
				this.fldCurrentMonth,
				this.fldYTD,
				this.fldBalance,
				this.fldSpent,
				this.Binder
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.19F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.CanShrink = true;
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 0.3125F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field36";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 2.09375F;
			// 
			// fldBudget
			// 
			this.fldBudget.CanShrink = true;
			this.fldBudget.Height = 0.19F;
			this.fldBudget.Left = 2.78125F;
			this.fldBudget.Name = "fldBudget";
			this.fldBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldBudget.Text = "Field36";
			this.fldBudget.Top = 0F;
			this.fldBudget.Width = 1F;
			// 
			// fldCurrentMonth
			// 
			this.fldCurrentMonth.CanShrink = true;
			this.fldCurrentMonth.Height = 0.19F;
			this.fldCurrentMonth.Left = 3.84375F;
			this.fldCurrentMonth.Name = "fldCurrentMonth";
			this.fldCurrentMonth.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCurrentMonth.Text = "Field36";
			this.fldCurrentMonth.Top = 0F;
			this.fldCurrentMonth.Width = 1F;
			// 
			// fldYTD
			// 
			this.fldYTD.CanShrink = true;
			this.fldYTD.Height = 0.19F;
			this.fldYTD.Left = 4.90625F;
			this.fldYTD.Name = "fldYTD";
			this.fldYTD.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldYTD.Text = "Field36";
			this.fldYTD.Top = 0F;
			this.fldYTD.Width = 1F;
			// 
			// fldBalance
			// 
			this.fldBalance.CanShrink = true;
			this.fldBalance.Height = 0.19F;
			this.fldBalance.Left = 5.96875F;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldBalance.Text = "Field36";
			this.fldBalance.Top = 0F;
			this.fldBalance.Width = 1F;
			// 
			// fldSpent
			// 
			this.fldSpent.CanShrink = true;
			this.fldSpent.Height = 0.19F;
			this.fldSpent.Left = 7.03125F;
			this.fldSpent.Name = "fldSpent";
			this.fldSpent.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSpent.Text = "Field36";
			this.fldSpent.Top = 0F;
			this.fldSpent.Width = 0.75F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 2.375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Binder";
			this.Binder.Top = 0.03125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.40625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptExpRevSummaryExpLevel
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.srptExpRevSummaryExpLevel_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subLowerLevelReport;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
