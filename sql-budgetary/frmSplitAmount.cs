﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSplitAmount.
	/// </summary>
	public partial class frmSplitAmount : BaseForm
	{
		public frmSplitAmount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSplitAmount InstancePtr
		{
			get
			{
				return (frmSplitAmount)Sys.GetInstance(typeof(frmSplitAmount));
			}
		}

		protected frmSplitAmount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int TempCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs_AutoInitialized;

		clsDRWrapper rs
		{
			get
			{
				if (rs_AutoInitialized == null)
				{
					rs_AutoInitialized = new clsDRWrapper();
				}
				return rs_AutoInitialized;
			}
			set
			{
				rs_AutoInitialized = value;
			}
		}

		bool EditFlag;
		bool DeleteFlag;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper Master = new clsDRWrapper();
		clsDRWrapper Master_AutoInitialized;

		clsDRWrapper Master
		{
			get
			{
				if (Master_AutoInitialized == null)
				{
					Master_AutoInitialized = new clsDRWrapper();
				}
				return Master_AutoInitialized;
			}
			set
			{
				Master_AutoInitialized = value;
			}
		}

		string ErrorString = "";
		bool BadAccountFlag;
		int DescriptionCol;
		int AccountCol;
		int AmountCol;
		int DiscountCol;
		int EncumbranceCol;
		int ProjectCol;
		int TaxCol;
		int CorrectedCol;
		int NumberCol;
		int NewAccountCol;
		int SplitNumberCol;
		int SplitTaxCol;
		int SplitAccountCol;
		int SplitAmountCol;
		public int CorrRecordNumber;
		int VendorNumber;
		bool blnStopSave;
		bool blnJournalLocked;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsGrid = new clsGridAccount();
		private clsGridAccount vsGrid_AutoInitialized;

		private clsGridAccount vsGrid
		{
			get
			{
				if (vsGrid_AutoInitialized == null)
				{
					vsGrid_AutoInitialized = new clsGridAccount();
				}
				return vsGrid_AutoInitialized;
			}
			set
			{
				vsGrid_AutoInitialized = value;
			}
		}
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(string)
		Decimal curTotalAmount;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsBudgetaryPosting clsPostInfo_AutoInitialized;

		clsBudgetaryPosting clsPostInfo
		{
			get
			{
				if (clsPostInfo_AutoInitialized == null)
				{
					clsPostInfo_AutoInitialized = new clsBudgetaryPosting();
				}
				return clsPostInfo_AutoInitialized;
			}
			set
			{
				clsPostInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		clsPostingJournalInfo clsJournalInfo_AutoInitialized;

		clsPostingJournalInfo clsJournalInfo
		{
			get
			{
				if (clsJournalInfo_AutoInitialized == null)
				{
					clsJournalInfo_AutoInitialized = new clsPostingJournalInfo();
				}
				return clsJournalInfo_AutoInitialized;
			}
			set
			{
				clsJournalInfo_AutoInitialized = value;
			}
		}

		bool blnPostJournal;

		private void cboPeriod_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

        private void frmSplitAmount_Activated(object sender, System.EventArgs e)
        {
            int counter;
            int counter2;
            string strLabel = "";
            if (modGlobal.FormExist(this))
            {
                return;
            }
            FillJournalCombo();
            cboJournal.SelectedIndex = 0;
            cboSaveJournal.SelectedIndex = 0;
            //if (vs1.Rows <= 10)
            if (vs1.Rows <= 6)
            {
                //FC:FINAL:DSE:#535 Top row header is smaller than the rest of rows
                //vs1.HeightOriginal = (vs1.RowHeight(0) * vs1.Rows) + 75;
                vs1.HeightOriginal = (vs1.RowHeight(0) + vs1.RowHeight(1) * (vs1.Rows - 1)) + 75;
            }
            else
            {
                //FC:FINAL:DSE:#535 Top row header is smaller than the rest of rows
                //vs1.HeightOriginal = (vs1.RowHeight(0) * 10) + 75;
                vs1.HeightOriginal = (vs1.RowHeight(0) + vs1.RowHeight(1) * 5) + 75;
            }
            vs1.Enabled = true;
            vs1.Focus();
            //Application.DoEvents();
            this.Refresh();
        }

		private void frmSplitAmount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSSPLIT")
			{
				if (vsSplit.Col == SplitAccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F2 && KeyCode != Keys.Insert)
				{
					modNewAccountBox.CheckFormKeyDown(vsSplit, vsSplit.Row, vsSplit.Col, KeyCode, Shift, vsSplit.EditSelStart, vsSplit.EditText, vsSplit.EditSelLength);
				}
			}
		}

		private void frmSplitAmount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSplitAmount.FillStyle	= 0;
			//frmSplitAmount.ScaleWidth	= 9300;
			//frmSplitAmount.ScaleHeight	= 7455;
			//frmSplitAmount.LinkTopic	= "Form2";
			//frmSplitAmount.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			blnJournalLocked = false;
			vs1.Cols = 9;
			vs1.ExtendLastCol = false;
			CorrectedCol = 0;
			NumberCol = 1;
			ProjectCol = 2;
			TaxCol = 3;
			DescriptionCol = 4;
			AccountCol = 5;
			AmountCol = 6;
			DiscountCol = 7;
			EncumbranceCol = 8;
			SplitNumberCol = 0;
			SplitTaxCol = 1;
			SplitAccountCol = 2;
			SplitAmountCol = 3;
			vsGrid.GRID7Light = vsSplit;
			vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = FCConvert.ToInt16(SplitAccountCol);
			vsSplit.ColWidth(SplitNumberCol, 0);
			vsSplit.ColWidth(SplitTaxCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.15));
			vsSplit.ColWidth(SplitAccountCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.5));
			vsSplit.ColWidth(SplitAmountCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.3));
			vsSplit.TextMatrix(0, SplitTaxCol, "Tax");
			vsSplit.TextMatrix(0, SplitAccountCol, "Account");
			vsSplit.TextMatrix(0, SplitAmountCol, "Amount");
			vsSplit.ColFormat(SplitAmountCol, "#,###.00");
            vsSplit.ColAlignment(SplitAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsSplit.ColComboList(SplitTaxCol, "D|N|1|2|3|4|5|6|7|8|9");
			lblRemAmount.Text = "0.00";
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2878979));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1723939));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, 4);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, DiscountCol, "Disc");
			vs1.TextMatrix(0, EncumbranceCol, "Enc");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(DiscountCol, "#,###.00");
			// set column formats
			vs1.ColFormat(EncumbranceCol, "#,###.00");
            vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			blnPostJournal = false;
		}

		private void frmSplitAmount_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(CorrectedCol, 0);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(TaxCol, 0);
			vs1.ColWidth(ProjectCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2878979));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1723939));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vs1.ColWidth(EncumbranceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1034363));
			vsSplit.ColWidth(SplitNumberCol, 0);
			vsSplit.ColWidth(SplitTaxCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.15));
			vsSplit.ColWidth(SplitAccountCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.5));
			vsSplit.ColWidth(SplitAmountCol, FCConvert.ToInt32(vsSplit.WidthOriginal * 0.3));
			fraJournalSave.CenterToContainer(this.ClientArea);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			frmGetGJCorrDataEntry.InstancePtr.Show(App.MainForm);
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmSplitAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int counter;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vs1")
				{
					if (this.ActiveControl.GetName() == "cboJournal")
					{
						KeyAscii = (Keys)0;
						vs1.Focus();
					}
					else
					{
						KeyAscii = (Keys)0;
						Support.SendKeys("{TAB}", false);
					}
				}
				else
				{
					KeyAscii = (Keys)0;
					for (counter = 2; counter <= vsSplit.Rows - 1; counter++)
					{
						if (Conversion.Val(vsSplit.TextMatrix(counter, SplitAmountCol)) != 0)
						{
							ans = MessageBox.Show("You will lose any information you have entered if you proceed.  Do you wish to split this amount?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.No)
							{
								return;
							}
							else
							{
								break;
							}
						}
					}
					vsSplit.Enabled = true;
					vsSplit.Rows = 1;
					vsSplit.Rows = 3;
					vsSplit.TextMatrix(1, SplitNumberCol, vs1.TextMatrix(vs1.Row, NumberCol));
					vsSplit.TextMatrix(1, SplitTaxCol, vs1.TextMatrix(vs1.Row, TaxCol));
					vsSplit.TextMatrix(1, SplitAccountCol, vs1.TextMatrix(vs1.Row, AccountCol));
					vsSplit.TextMatrix(1, SplitAmountCol, vs1.TextMatrix(vs1.Row, AmountCol));
					vsSplit.TextMatrix(2, SplitAmountCol, "0.00");
					vsSplit.TextMatrix(2, SplitAmountCol, "0.00");
					curTotalAmount = FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol));
					lblRemAmount.Text = "0.00";
					vsSplit.Select(1, SplitAmountCol);
					vsSplit.Focus();
					vsSplit.EditSelStart = 0;
					vsSplit.EditCell();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			bool blnShouldSave;
			clsDRWrapper rsAltCashInfo = new clsDRWrapper();
			bool blnWrongFund;
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			bool blnCreditMemoUsed = false;
			Support.SendKeys("{TAB}", false);
			//Application.DoEvents();
			rsAltCashInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(CorrRecordNumber));
			blnShouldSave = false;
			blnWrongFund = false;
			if (FCConvert.ToDecimal(vsSplit.TextMatrix(2, SplitAmountCol)) == 0)
			{
				MessageBox.Show("You must enter another line of information before you may save.", "Invalid Split Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (FCConvert.ToDecimal(lblRemAmount.Text) != 0)
			{
				MessageBox.Show("You must use the entire amount before you may continue.", "Invalid Split Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			for (counter = 1; counter <= vsSplit.Rows - 1; counter++)
			{
				if (Strings.InStr(1, vsSplit.TextMatrix(counter, SplitAccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Trim(vsSplit.TextMatrix(counter, SplitAccountCol)) != "")
					{
						blnShouldSave = true;
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCreditMemo.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsAltCashInfo.Get_Fields("JournalNumber") + " AND VendorNumber = " + rsAltCashInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = '" + rsAltCashInfo.Get_Fields("CheckNumber") + "' AND IsNull(CreditMemoRecord, 0) <> 0");
						if (rsCreditMemo.EndOfFile() != true && rsCreditMemo.BeginningOfFile() != true)
						{
							blnCreditMemoUsed = true;
						}
						else
						{
							blnCreditMemoUsed = false;
						}
						if (FCConvert.ToBoolean(rsAltCashInfo.Get_Fields_Boolean("UseAlternateCash")) || blnCreditMemoUsed)
						{
							if (modBudgetaryMaster.GetAccountFund(vsSplit.TextMatrix(counter, SplitAccountCol)) != modBudgetaryMaster.GetAccountFund(vs1.TextMatrix(vs1.Row, AccountCol)))
							{
								blnShouldSave = false;
								blnWrongFund = true;
								break;
							}
						}
					}
				}
				if (modBudgetaryMaster.CheckIfControlAccount(vsSplit.TextMatrix(counter, SplitAccountCol)))
				{
					answer = MessageBox.Show("Account " + vsSplit.TextMatrix(counter, SplitAccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (answer == DialogResult.No)
					{
						vsSplit.Select(counter, SplitAccountCol);
						vsSplit.Focus();
						return;
					}
					else
					{
						modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vsSplit.TextMatrix(counter, SplitAccountCol), "AP Cortrection Data Entry");
					}
				}
			}
			if (!blnShouldSave)
			{
				if (blnWrongFund)
				{
					if (blnCreditMemoUsed)
					{
						MessageBox.Show("Because the AP entry you are trying to correct used a credit memo the fund of the new account must match the fund of the old account.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Because the AP entry you are trying to correct used an alternate cash account the fund of the new account must match the fund of the old account.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("You must change at least one account before you may save.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the journal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
					cboSaveJournal.Focus();
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					txtJournalDescription.Focus();
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//cboSaveJournal.Width = 825;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsEncData = new clsDRWrapper();
			txtVendor.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(cboPeriod.Text))
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2)) == Conversion.Val(cboPeriod.Text))
								{
									cboSaveJournal.SelectedIndex = counter;
									cboJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "")
							{
								executeAddTag = true;
								goto AddTag;
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								txtJournalDescription.Focus();
								return;
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.AddNew();
					Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("Description", txtJournalDescription.Text);
					Master.Set_Fields("Type", "AC");
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(cboPeriod.Text)));
					Master.Update();
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "AC";
			clsJournalInfo.Period = cboPeriod.Text;
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
			rs.AddNew();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				rs.Set_Fields("JournalNumber", TempJournal);
			}
			rs.Set_Fields("VendorNumber", txtVendor.Text);
			if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
			{
				rs.Set_Fields("TempVendorName", txtAddress[0].Text);
				rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
				rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
				rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
				rs.Set_Fields("TempVendorCity", txtCity.Text);
				rs.Set_Fields("TempVendorState", txtState.Text);
				rs.Set_Fields("TempVendorZip", txtZip.Text);
				rs.Set_Fields("TempVendorZip4", txtZip4.Text);
			}
			rs.Set_Fields("Period", cboPeriod.Text);
			rs.Set_Fields("Description", txtDescription.Text);
			rs.Set_Fields("Reference", txtReference.Text);
			rs.Set_Fields("Amount", 0);
			rs.Set_Fields("CheckNumber", txtCheck.Text);
			rs.Set_Fields("Status", "E");
			rsEncData.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(CorrRecordNumber));
			if (rsEncData.EndOfFile() != true && rsEncData.BeginningOfFile() != true)
			{
				rs.Set_Fields("UseAlternateCash", rsEncData.Get_Fields_Boolean("UseAlternateCash"));
				rs.Set_Fields("AlternateCashAccount", rsEncData.Get_Fields_String("AlternateCashAccount"));
				rs.Set_Fields("CheckDate", rsEncData.Get_Fields_DateTime("CheckDate"));
				rs.Set_Fields("Payable", rsEncData.Get_Fields_DateTime("CheckDate"));
			}
			rs.Update();
			VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			SaveDetails();
			if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptAccountsPayableCorrections))
			{
				if (MessageBox.Show("Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					blnPostJournal = true;
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
			}
			else
			{
				blnPostJournal = false;
				if (cboSaveJournal.SelectedIndex != 0)
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
				}
			}
			Close();
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SaveDetails()
		{
			int counter;
			Decimal TotalAmount;
			clsDRWrapper rsCorrect = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsEncInfo = new clsDRWrapper();
			int intCorrRow;
			rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
			for (intCorrRow = 1; intCorrRow <= vs1.Rows - 1; intCorrRow++)
			{
				if (vs1.TextMatrix(intCorrRow, NumberCol) == vsSplit.TextMatrix(1, SplitNumberCol))
				{
					break;
				}
			}
			rsDetailInfo.OmitNullsOnInsert = true;
			rsDetailInfo.AddNew();
			// if so add it
			rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
			rsDetailInfo.Set_Fields("Description", vs1.TextMatrix(intCorrRow, DescriptionCol));
			rsDetailInfo.Set_Fields("account", vs1.TextMatrix(intCorrRow, AccountCol));
			rsDetailInfo.Set_Fields("Amount", ((FCConvert.ToDecimal(vs1.TextMatrix(intCorrRow, AmountCol)) - FCConvert.ToDecimal(vsSplit.TextMatrix(1, SplitAmountCol))) * -1));
			rsDetailInfo.Set_Fields("Discount", 0);
			rsDetailInfo.Set_Fields("Encumbrance", 0);
			rsDetailInfo.Set_Fields("Project", vs1.TextMatrix(intCorrRow, ProjectCol));
			rsDetailInfo.Set_Fields("1099", vs1.TextMatrix(intCorrRow, TaxCol));
			// TextMatrix(counter, TaxCol)
			rsDetailInfo.Set_Fields("RCB", "C");
			rsDetailInfo.Set_Fields("Corrected", true);
			rsDetailInfo.Update();
			// update the database
			for (counter = 2; counter <= vsSplit.Rows - 1; counter++)
			{
				// save the grid information
				if (vsSplit.TextMatrix(counter, SplitAccountCol) != "" && FCConvert.ToDecimal(vsSplit.TextMatrix(counter, SplitAmountCol)) != 0)
				{
					if (modValidateAccount.AccountValidate(vsSplit.TextMatrix(counter, SplitAccountCol)))
					{
						rsDetailInfo.AddNew();
						// if so add it
						rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
						rsDetailInfo.Set_Fields("Description", vs1.TextMatrix(intCorrRow, DescriptionCol));
						rsDetailInfo.Set_Fields("account", vsSplit.TextMatrix(counter, SplitAccountCol));
						rsDetailInfo.Set_Fields("Amount", vsSplit.TextMatrix(counter, SplitAmountCol));
						rsDetailInfo.Set_Fields("Discount", 0);
						rsDetailInfo.Set_Fields("Encumbrance", 0);
						rsDetailInfo.Set_Fields("Project", vs1.TextMatrix(intCorrRow, ProjectCol));
						rsDetailInfo.Set_Fields("1099", vsSplit.TextMatrix(counter, SplitTaxCol));
						// TextMatrix(counter, TaxCol)
						rsDetailInfo.Set_Fields("RCB", "R");
						rsDetailInfo.Set_Fields("Corrected", false);
						rsDetailInfo.Update();
						// update the database
					}
				}
			}
			rsCorrect.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(intCorrRow, NumberCol))));
			if (rsCorrect.EndOfFile() != true && rsCorrect.BeginningOfFile() != true)
			{
				rsCorrect.Edit();
				rsCorrect.Set_Fields("Corrected", true);
				rsCorrect.Set_Fields("CorrectedAmount", FCConvert.ToDecimal(Conversion.Val(rsCorrect.Get_Fields_Decimal("CorrectedAmount"))) + ((FCConvert.ToDecimal(vs1.TextMatrix(intCorrRow, AmountCol)) - FCConvert.ToDecimal(vsSplit.TextMatrix(1, SplitAmountCol))) * -1));
				rsCorrect.Update();
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			for (counter = 2; counter <= vsSplit.Rows - 1; counter++)
			{
				if (Conversion.Val(vsSplit.TextMatrix(counter, SplitAmountCol)) != 0)
				{
					ans = MessageBox.Show("You will lose any information you have entered if you proceed.  Do you wish to split this amount?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						return;
					}
					else
					{
						break;
					}
				}
			}
			vsSplit.Enabled = true;
			vsSplit.Rows = 1;
			vsSplit.Rows = 3;
			vsSplit.TextMatrix(1, SplitNumberCol, vs1.TextMatrix(vs1.Row, NumberCol));
			vsSplit.TextMatrix(1, SplitTaxCol, vs1.TextMatrix(vs1.Row, TaxCol));
			vsSplit.TextMatrix(1, SplitAccountCol, vs1.TextMatrix(vs1.Row, AccountCol));
			vsSplit.TextMatrix(1, SplitAmountCol, vs1.TextMatrix(vs1.Row, AmountCol));
			vsSplit.TextMatrix(2, SplitAmountCol, "0.00");
			curTotalAmount = FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol));
			lblRemAmount.Text = "0.00";
			vsSplit.Select(1, SplitAmountCol);
			vsSplit.Focus();
			vsSplit.EditSelStart = 0;
			vsSplit.EditCell();
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int counter;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				for (counter = 2; counter <= vsSplit.Rows - 1; counter++)
				{
					if (Conversion.Val(vsSplit.TextMatrix(counter, SplitAmountCol)) != 0)
					{
						ans = MessageBox.Show("You will lose any information you have entered if you proceed.  Do you wish to split this amount?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							return;
						}
						else
						{
							break;
						}
					}
				}
				vsSplit.Rows = 1;
				vsSplit.Rows = 3;
				vsSplit.TextMatrix(1, SplitNumberCol, vs1.TextMatrix(vs1.Row, NumberCol));
				vsSplit.TextMatrix(1, SplitTaxCol, vs1.TextMatrix(vs1.Row, TaxCol));
				vsSplit.TextMatrix(1, SplitAccountCol, vs1.TextMatrix(vs1.Row, AccountCol));
				vsSplit.TextMatrix(1, SplitAmountCol, vs1.TextMatrix(vs1.Row, AmountCol));
				curTotalAmount = FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol));
				lblRemAmount.Text = "0.00";
				vsSplit.Select(1, SplitAmountCol);
				vsSplit.Focus();
				vsSplit.EditSelStart = 0;
				vsSplit.EditCell();
			}
		}

		private void vsSplit_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsSplit.IsCurrentCellInEditMode)
			{
				if (vsSplit.Col == SplitAccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsSplit.EditText);
				}
			}
		}

		private void vsSplit_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				// if enter is hit
				KeyCode = 0;
				if (vsSplit.Col < SplitAmountCol)
				{
					// if we are not in the last column
					// do nothing
				}
				else
				{
					if (vsSplit.Row < vsSplit.Rows - 1)
					{
						// if we are not on the last row
						vsSplit.Row += 1;
						// move to the first column of the next row
						vsSplit.Col = SplitTaxCol;
					}
					else
					{
						vsSplit.AddItem("");
						// if we are at the end then create a new line
						vsSplit.TextMatrix(vsSplit.Rows - 1, SplitAmountCol, "0.00");
						vsSplit.Row += 1;
						// move to the next line in the first column
						vsSplit.Col = SplitTaxCol;
					}
				}
			}
		}

		private void vsSplit_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Insert)
			{
				if (vsSplit.Col == SplitAmountCol)
				{
					vsSplit.EditText = lblRemAmount.Text;
				}
			}
		}

		private void vsSplit_RowColChange(object sender, System.EventArgs e)
		{
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsSplit.TextMatrix(vsSplit.Row, SplitAccountCol));
			if (vsSplit.Row == 1 && vsSplit.Col != SplitAmountCol)
			{
				vsSplit.Col = SplitAmountCol;
			}
			vsSplit.EditCell();
		}

		private void vsSplit_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsSplit.GetFlexRowIndex(e.RowIndex);
			int col = vsSplit.GetFlexColIndex(e.ColumnIndex);
			if (col == SplitAmountCol)
			{
				if (!Information.IsNumeric(vsSplit.EditText))
				{
					if (Strings.Trim(vsSplit.EditText) != "")
					{
						MessageBox.Show("You must enter a numeric amount in this field.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
					else
					{
						vsSplit.EditText = "0.00";
					}
				}
				CalculateRemaining();
			}
		}

		private void CalculateRemaining()
		{
			int counter;
			Decimal curAmt = 0.0M;
			for (counter = 1; counter <= vsSplit.Rows - 1; counter++)
			{
				if (counter == vsSplit.Row)
				{
					if (vsSplit.EditText.Trim() != "")
					{
						curAmt += FCConvert.ToDecimal(vsSplit.EditText);
					}
				}
				else
				{
					if (vsSplit.TextMatrix(counter, SplitAmountCol) != "")
					{
						curAmt += FCConvert.ToDecimal(vsSplit.TextMatrix(counter, SplitAmountCol));
					}
				}
			}
			lblRemAmount.Text = Strings.Format(curTotalAmount - curAmt, "#,##0.00");
		}
	}
}
