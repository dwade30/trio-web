﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary.Enums;
using Wisej.Web;

namespace TWBD0000
{
	public class cJournalsView
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cJournalService jService = new cJournalService();
		private cJournalService jService_AutoInitialized;

		private cJournalService jService
		{
			get
			{
				if (jService_AutoInitialized == null)
				{
					jService_AutoInitialized = new cJournalService();
				}
				return jService_AutoInitialized;
			}
			set
			{
				jService_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEncumbranceController encController = new cEncumbranceController();
		private cEncumbranceController encController_AutoInitialized;

		private cEncumbranceController encController
		{
			get
			{
				if (encController_AutoInitialized == null)
				{
					encController_AutoInitialized = new cEncumbranceController();
				}
				return encController_AutoInitialized;
			}
			set
			{
				encController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection unpostedJournalsColl = new cGenericCollection();
		private cGenericCollection unpostedJournalsColl_AutoInitialized;

		private cGenericCollection unpostedJournalsColl
		{
			get
			{
				if (unpostedJournalsColl_AutoInitialized == null)
				{
					unpostedJournalsColl_AutoInitialized = new cGenericCollection();
				}
				return unpostedJournalsColl_AutoInitialized;
			}
			set
			{
				unpostedJournalsColl_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection postedJournalsColl = new cGenericCollection();
		private cGenericCollection postedJournalsColl_AutoInitialized;

		private cGenericCollection postedJournalsColl
		{
			get
			{
				if (postedJournalsColl_AutoInitialized == null)
				{
					postedJournalsColl_AutoInitialized = new cGenericCollection();
				}
				return postedJournalsColl_AutoInitialized;
			}
			set
			{
				postedJournalsColl_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cAPJournalController apController = new cAPJournalController();
		private cAPJournalController apController_AutoInitialized;

		private cAPJournalController apController
		{
			get
			{
				if (apController_AutoInitialized == null)
				{
					apController_AutoInitialized = new cAPJournalController();
				}
				return apController_AutoInitialized;
			}
			set
			{
				apController_AutoInitialized = value;
			}
		}

		public delegate void PostedJournalsChangedEventHandler();

		public event PostedJournalsChangedEventHandler PostedJournalsChanged;

		public delegate void UnpostedJournalsChangedEventHandler();

		public event UnpostedJournalsChangedEventHandler UnpostedJournalsChanged;

		private bool boolEditAP;
		private bool boolEditCR;
		private bool boolEditCD;
		private bool boolCanReverseJournals;
		private bool boolEditJournalDescriptions;
		private bool boolEditGJ;
		private bool boolDeleteUnpostedJournals;
		private bool boolCanPost;
		private bool boolCanPrint;
		private bool boolEditEN;

		public bool CanEditAP
		{
			get
			{
				bool CanEditAP = false;
				CanEditAP = boolEditAP;
				return CanEditAP;
			}
		}

		public bool CanEditCR
		{
			get
			{
				bool CanEditCR = false;
				CanEditCR = boolEditCR;
				return CanEditCR;
			}
		}

		public bool CanEditCD
		{
			get
			{
				bool CanEditCD = false;
				CanEditCD = boolEditCD;
				return CanEditCD;
			}
		}

		public bool CanCreateReverseJournals
		{
			get
			{
				bool CanCreateReverseJournals = false;
				CanCreateReverseJournals = boolCanReverseJournals;
				return CanCreateReverseJournals;
			}
		}

		public bool CanEditJournalDescriptions
		{
			get
			{
				bool CanEditJournalDescriptions = false;
				CanEditJournalDescriptions = boolEditJournalDescriptions;
				return CanEditJournalDescriptions;
			}
		}

		public bool CanEditGJ
		{
			get
			{
				bool CanEditGJ = false;
				CanEditGJ = boolEditGJ;
				return CanEditGJ;
			}
		}

		public bool CanDeleteUnpostedJournals
		{
			get
			{
				bool CanDeleteUnpostedJournals = false;
				CanDeleteUnpostedJournals = boolDeleteUnpostedJournals;
				return CanDeleteUnpostedJournals;
			}
		}

		public bool CanCreateEditJournals
		{
			get
			{
				bool CanCreateEditJournals = false;
				CanCreateEditJournals = boolEditAP || boolEditCD || boolEditCR || boolEditEN || boolEditGJ;
				return CanCreateEditJournals;
			}
		}

		public bool CanPost
		{
			get
			{
				bool CanPost = false;
				CanPost = boolCanPost;
				return CanPost;
			}
		}

		public bool CanPrint
		{
			get
			{
				bool CanPrint = false;
				CanPrint = boolCanPrint;
				return CanPrint;
			}
		}

		private void LoadSecurity()
		{
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.Posting)) == "F")
			{
				boolCanPost = true;
			}
			else
			{
				boolCanPost = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.Printing)) != "N")
			{
				boolCanPrint = true;
			}
			else
			{
				boolCanPrint = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.AccountsPayable)) == "F")
			{
				boolEditAP = true;
			}
			else
			{
				boolEditAP = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.CashReceipts)) == "F")
			{
				boolEditCR = true;
			}
			else
			{
				boolEditCR = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.CashDisbursements)) == "F")
			{
				boolEditCD = true;
			}
			else
			{
				boolEditCD = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.CreateReversingEntries)) == "F")
			{
				boolCanReverseJournals = true;
			}
			else
			{
				boolCanReverseJournals = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.Encumbrances)) == "F")
			{
				boolEditEN = true;
			}
			else
			{
				boolEditEN = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.EditJournalDescriptions)) == "F")
			{
				boolEditJournalDescriptions = true;
			}
			else
			{
				boolEditJournalDescriptions = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.GeneralJournal)) == "F")
			{
				boolEditGJ = true;
			}
			else
			{
				boolEditGJ = false;
			}
			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.DeleteUnpostedJournal)) == "F")
			{
				boolDeleteUnpostedJournals = true;
			}
			else
			{
				boolDeleteUnpostedJournals = false;
			}
		}

		public cGenericCollection PostedJournals
		{
			get
			{
				cGenericCollection PostedJournals = null;
				PostedJournals = postedJournalsColl;
				return PostedJournals;
			}
		}

		public cGenericCollection UnpostedJournals
		{
			get
			{
				cGenericCollection UnpostedJournals = null;
				UnpostedJournals = unpostedJournalsColl;
				return UnpostedJournals;
			}
		}

		public void Refresh()
		{
			RefreshUnpostedJournals();
			RefreshPostedJournals();
		}

		private void RefreshUnpostedJournals()
		{
			try
			{
				// On Error GoTo ErrorHandler
				cJournalInfo jInfo;
				unpostedJournalsColl = jService.GetUnpostedJournalInfos();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RefreshPostedJournals()
		{
			try
			{
				// On Error GoTo ErrorHandler
				cJournalInfo jInfo;
				postedJournalsColl = jService.GetPostedJournalInfos();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void PostJournals(bool boolPageBreakAfterJournals)
		{
			try
			{
				// On Error GoTo ErrorHandler
				unpostedJournalsColl.MoveFirst();
				cJournalInfo jInfo;
				cGenericCollection postList = new cGenericCollection();
				while (unpostedJournalsColl.IsCurrent())
				{
					//Application.DoEvents();
					jInfo = (cJournalInfo)unpostedJournalsColl.GetCurrentItem();
					if (jInfo.IsSelected)
					{
						if (Strings.LCase(jInfo.JournalType) == "ap")
						{
							if (Strings.LCase(jInfo.Status) == "w" || Strings.LCase(jInfo.Status) == "x")
							{
								postList.AddItem(jInfo);
							}
							else
							{
								MessageBox.Show("Journal " + FCConvert.ToString(jInfo.JournalNumber) + " is not ready to post.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						else
						{
							if (Strings.LCase(jInfo.Status) == "e")
							{
								postList.AddItem(jInfo);
							}
							else
							{
								MessageBox.Show("Journal " + FCConvert.ToString(jInfo.JournalNumber) + " is not ready to post.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						if (jInfo.ContainsBadAccount)
						{
							MessageBox.Show("There are one or more bad accounts in journal " + FCConvert.ToString(jInfo.JournalNumber) + ".  Please fix the problem with the accounts and try to post again.", "Invalid Accounts In Journal", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}
					unpostedJournalsColl.MoveNext();
				}
				if (postList.ItemCount() > 0)
				{
					jService.PostJournals(ref postList, boolPageBreakAfterJournals);
					Refresh();
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void PrintUnposted(bool boolShowLiquidatedEncumbranceActivity)
		{
			try
			{
				// On Error GoTo ErrorHandler
				unpostedJournalsColl.MoveFirst();
				string strSQL = "";
				cJournalInfo jInfo;
				cGenericCollection listColl = new cGenericCollection();
				int lngJournalNumber = 0;
				while (unpostedJournalsColl.IsCurrent())
				{
					jInfo = (cJournalInfo)unpostedJournalsColl.GetCurrentItem();
					if (jInfo.IsSelected)
					{
						listColl.AddItem(jInfo.JournalNumber);
					}
					unpostedJournalsColl.MoveNext();
				}
				if (listColl.ItemCount() == 0)
				{
					MessageBox.Show("You must select at least 1 journal number before you may proceed.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					listColl.MoveFirst();
					while (listColl.IsCurrent())
					{
						lngJournalNumber = FCConvert.ToInt32(listColl.GetCurrentItem());
						if (!jService.UnpostedJournalExists(lngJournalNumber))
						{
							MessageBox.Show("There are no unposted journals with the number " + FCConvert.ToString(lngJournalNumber), "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						if (strSQL != "")
						{
							strSQL += " or journalnumber = " + FCConvert.ToString(lngJournalNumber);
						}
						else
						{
							strSQL = "JournalNumber = " + FCConvert.ToString(lngJournalNumber);
						}
						listColl.MoveNext();
					}
					strSQL = "SELECT DISTINCT JournalNumber FROM JournalMaster WHERE Status <> 'P' AND status <> 'D' AND (" + strSQL + ") ORDER BY JournalNumber";
				}
				rptMultipleJournals.InstancePtr.Init("U", boolShowLiquidatedEncumbranceActivity, strSQL);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void PrintPosted(bool boolShowLiquidatedEncumbranceActivity)
		{
			try
			{
				// On Error GoTo ErrorHandler
				postedJournalsColl.MoveFirst();
				string strSQL = "";
				cJournalInfo jInfo;
				cGenericCollection listColl = new cGenericCollection();
				int lngJournalNumber = 0;
				while (postedJournalsColl.IsCurrent())
				{
					jInfo = (cJournalInfo)postedJournalsColl.GetCurrentItem();
					if (jInfo.IsSelected)
					{
						listColl.AddItem(jInfo.JournalNumber);
					}
					postedJournalsColl.MoveNext();
				}
				if (listColl.ItemCount() == 0)
				{
					MessageBox.Show("You must select at least 1 journal number before you may proceed.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					listColl.MoveFirst();
					while (listColl.IsCurrent())
					{
						lngJournalNumber = FCConvert.ToInt32(listColl.GetCurrentItem());
						if (!jService.PostedJournalExists(lngJournalNumber))
						{
							MessageBox.Show("There are no posted journals with the number " + FCConvert.ToString(lngJournalNumber), "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						if (strSQL != "")
						{
							strSQL += " or journalnumber = " + FCConvert.ToString(lngJournalNumber);
						}
						else
						{
							strSQL = "JournalNumber = " + FCConvert.ToString(lngJournalNumber);
						}
						listColl.MoveNext();
					}
					strSQL = "SELECT DISTINCT JournalNumber FROM JournalMaster WHERE Status = 'P' AND (" + strSQL + ") ORDER BY JournalNumber";
				}
				rptMultipleJournals.InstancePtr.Init("P", boolShowLiquidatedEncumbranceActivity, strSQL);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void SelectUnpostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				UnpostedJournals.MoveFirst();
				cJournalInfo jInfo;
				while (UnpostedJournals.IsCurrent())
				{
					jInfo = (cJournalInfo)UnpostedJournals.GetCurrentItem();
					if (jInfo.Id == lngID)
					{
						jInfo.IsSelected = true;
						return;
					}
					UnpostedJournals.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void UpdateUnpostedDescription(int lngID, string strDescription)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (boolEditJournalDescriptions)
				{
					UnpostedJournals.MoveFirst();
					cJournalInfo jInfo;
					while (UnpostedJournals.IsCurrent())
					{
						jInfo = (cJournalInfo)UnpostedJournals.GetCurrentItem();
						if (jInfo.Id == lngID)
						{
							jInfo.Description = strDescription;
							return;
						}
						UnpostedJournals.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void SaveUnpostedDescriptions()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (boolEditJournalDescriptions)
				{
					UnpostedJournals.MoveFirst();
					cJournalInfo jInfo;
					while (UnpostedJournals.IsCurrent())
					{
						jInfo = (cJournalInfo)UnpostedJournals.GetCurrentItem();
						jService.UpdateJournalDescription(jInfo.Id, jInfo.Description);
						UnpostedJournals.MoveNext();
					}
				}
				if (jService.HadError)
				{
					ShowLastServiceError();
				}
				else
				{
					MessageBox.Show("Description(s) were updated", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void UnSelectUnpostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				UnpostedJournals.MoveFirst();
				cJournalInfo jInfo;
				while (UnpostedJournals.IsCurrent())
				{
					jInfo = (cJournalInfo)UnpostedJournals.GetCurrentItem();
					if (jInfo.Id == lngID)
					{
						jInfo.IsSelected = false;
						return;
					}
					UnpostedJournals.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void SelectPostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				PostedJournals.MoveFirst();
				cJournalInfo jInfo;
				while (PostedJournals.IsCurrent())
				{
					jInfo = (cJournalInfo)PostedJournals.GetCurrentItem();
					if (jInfo.Id == lngID)
					{
						jInfo.IsSelected = true;
						return;
					}
					PostedJournals.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void UnSelectPostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				PostedJournals.MoveFirst();
				cJournalInfo jInfo;
				while (PostedJournals.IsCurrent())
				{
					jInfo = (cJournalInfo)PostedJournals.GetCurrentItem();
					if (jInfo.Id == lngID)
					{
						jInfo.IsSelected = false;
						return;
					}
					PostedJournals.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool JournalCanBeDeleted(int journalId)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (journalId > 0)
				{
					unpostedJournalsColl.MoveFirst();
					cJournalInfo jInfo;
					jInfo = null;
					while (unpostedJournalsColl.IsCurrent())
					{
						jInfo = (cJournalInfo) unpostedJournalsColl.GetCurrentItem();
						if (jInfo.Id == journalId)
						{
							break;
						}

						unpostedJournalsColl.MoveNext();
					}

					if (!(jInfo == null))
					{
						if (jInfo.Id == journalId)
						{
							if (jInfo.Status.ToLower() == "e")
							{
								return true;
							}
						}
					}
				}

				return false;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

		public void DeleteAnUnpostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (lngID > 0)
				{
					jService.DeleteUnpostedJournal(lngID);
					if (jService.LastErrorNumber != 0)
					{
						ShowLastServiceError();
					}
					else
					{
						MessageBox.Show("Journal Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
						RefreshUnpostedJournals();
					}
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowLastServiceError()
		{
			if (jService.LastErrorNumber != 0)
			{
				MessageBox.Show("Error " + FCConvert.ToString(jService.LastErrorNumber) + " " + jService.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void EditUnpostedJournal(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (lngID > 0)
				{
					unpostedJournalsColl.MoveFirst();
					cJournalInfo jInfo;
					jInfo = null;
					while (unpostedJournalsColl.IsCurrent())
					{
						jInfo = (cJournalInfo)unpostedJournalsColl.GetCurrentItem();
						if (jInfo.Id == lngID)
						{
							break;
						}
						unpostedJournalsColl.MoveNext();
					}
					if (!(jInfo == null))
					{
						if (jInfo.Id == lngID)
						{
							if (Strings.LCase(jInfo.JournalType) == "ap")
							{
								if (boolEditAP)
								{
									EditAPJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else if (Strings.LCase(jInfo.JournalType) == "ac")
							{
								if (!jInfo.IsCreditMemo)
								{
									MessageBox.Show("This type of journal cannot be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
								if (boolEditAP)
								{
									EditACJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else if ((Strings.LCase(jInfo.JournalType) == "cw") || (Strings.LCase(jInfo.JournalType) == "cr"))
							{
								if (boolEditCR)
								{
									EditCRJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else if ((Strings.LCase(jInfo.JournalType) == "cd"))
							{
								if (boolEditCD)
								{
									EditCDJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else if ((Strings.LCase(jInfo.JournalType) == "gj") || (Strings.LCase(jInfo.JournalType) == "py"))
							{
								if (boolEditGJ)
								{
									EditGJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else if (Strings.LCase(jInfo.JournalType) == "en")
							{
								if (boolEditEN)
								{
									EditENJournal(ref jInfo);
								}
								else
								{
									MessageBox.Show("You do not have permissions to edit this type of journal", "Not Allowed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
						}
						else
						{
							Information.Err().Raise(-9000, null, "Journal not found", null, null);
							return;
						}
					}
					else
					{
						Information.Err().Raise(-9000, null, "Journal not found", null, null);
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub UpdateUnpostedJournal(ByVal lngID As Long)
		// On Error GoTo ErrorHandler
		// If lngID > 0 Then
		// unpostedJournalsColl.MoveFirst
		// Dim jInfo As cJournalInfo
		// Set jInfo = Nothing
		// Do While unpostedJournalsColl.IsCurrent
		// Set jInfo = unpostedJournalsColl.GetCurrentItem
		// If jInfo.ID = lngID Then
		// Exit Do
		// End If
		// unpostedJournalsColl.MoveNext
		// Loop
		// If Not jInfo Is Nothing Then
		// Select Case LCase(jInfo.JournalType)
		// Case "ap"
		//
		// Case "ac"
		//
		// Case "cw"
		// Case "gj", "py"
		// Call UpdateGJJournal(jInfo)
		// End Select
		// Else
		// Call Err.Raise(-9000, , "Journal not found")
		// Exit Sub
		// End If
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error " & Err.Number & " " & Err.Description, vbCritical, "Error"
		//
		// End Sub
		private void EditENJournal(ref cJournalInfo jInfo)
		{
			if (jInfo.Status != "E")
			{
				MessageBox.Show("Only encumbrance journals in a status of entered may be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			cGenericCollection vList = new cGenericCollection();
			vList = jService.GetVendorInfosByEncumbranceJournal(jInfo.JournalNumber);
			cGenericCollection voList = new cGenericCollection();
			cVendorInfo vInfo;
			int lngID;
			cEncumbrance enc = new cEncumbrance();
			lngID = 0;
			if (vList.ItemCount() > 0)
			{
				cVendorOption vOption = new cVendorOption();
				vOption.VendorName = "New";
				vOption.Amount = 0;
				vOption.Description = "New";
				vOption.JournalNumber = jInfo.JournalNumber;
				vOption.VendorName = "New";
				vOption.VendorNumber = 0;
				voList.AddItem(vOption);
				vList.MoveFirst();
				while (vList.IsCurrent())
				{
					vInfo = (cVendorInfo)vList.GetCurrentItem();
					vOption = new cVendorOption();
					vOption.Amount = vInfo.Amount;
					vOption.Description = vInfo.Description;
					vOption.ID = vInfo.ID;
					vOption.JournalNumber = jInfo.JournalNumber;
					vOption.VendorName = vInfo.CheckName;
					vOption.VendorNumber = vInfo.VendorNumber;
					voList.AddItem(vOption);
					vList.MoveNext();
				}
				frmJournalVendorChoice jvc = new frmJournalVendorChoice();
				int intReturn = 0;
				jvc.Entries = voList;
				intReturn = jvc.ShowDialog();
				if (intReturn == FCConvert.ToInt32(DialogResult.OK))
				{
					lngID = jvc.ChosenID;
					if (lngID > 0)
					{
						enc = jService.GetFullEncumbrance(lngID);
					}
				}
				else
				{
					return;
				}
			}
			if (lngID > 0)
			{
				frmEncumbranceDataEntry.InstancePtr.Init(ref enc);
			}
			else
			{
				enc = new cEncumbrance();
				enc.JournalNumber = jInfo.JournalNumber;
				enc.Period = jInfo.Period;
				frmEncumbranceDataEntry.InstancePtr.Init(ref enc);
			}
			// Dim journ As cJournal
			// Set journ = jService.GetFullJournal(jInfo.ID)
			// Call frmEncumbranceDataEntry.Init(journ)
		}

		private void EditAPJournal(ref cJournalInfo jInfo)
		{
            cGenericCollection vList = new cGenericCollection();
            cGenericCollection voList = new cGenericCollection();
            cVendorInfo vInfo;
            cAPJournal apJourn = new cAPJournal();
            int lngID;

			if (jInfo.Status != "E" && jInfo.Status != "V")
			{
				MessageBox.Show("Only AP Journals in a status of Entered or Void can be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

            vList = jService.GetVendorInfosByAPJournal(jInfo.JournalNumber);
            lngID = 0;

            cEditAPViewModel apEditViewModel = new cEditAPViewModel();
            apEditViewModel.LoadAPJournalMaster(jInfo.JournalNumber);
            if (!apEditViewModel.AllValidated())
            {
                frmEditAPJournal EditAPJournal = new frmEditAPJournal();
                EditAPJournal.InitializeScreen(ref apEditViewModel);
                if (!apEditViewModel.Cancelled)
                {
                    apJourn.JournalNumber = apEditViewModel.APJournalInfo.JournalNumber;
                    apJourn.Period = (short)apEditViewModel.APJournalInfo.Period;
                    apJourn.PayableDate = apEditViewModel.APJournalInfo.PayableDate;
                }
                else
                {
                    return;
                }
            }


   //         // SHOW THE Journal Master (AP) Form Here
			//if (ShowJournalMasterAP(jInfo.JournalNumber))
   //         {
   //             return;
   //         }
			
			if (vList.ItemCount() > 0)
			{
				cVendorOption vOption = new cVendorOption();
				vOption.VendorName = "New";
				vOption.Amount = 0;
				vOption.Description = "New";
				vOption.JournalNumber = jInfo.JournalNumber;
				vOption.VendorName = "New";
				vOption.VendorNumber = 0;
				voList.AddItem(vOption);
				vList.MoveFirst();
				while (vList.IsCurrent())
				{
					vInfo = (cVendorInfo)vList.GetCurrentItem();
					vOption = new cVendorOption();
					vOption.Amount = vInfo.Amount;
					vOption.Description = vInfo.Description;
					vOption.ID = vInfo.ID;
					vOption.JournalNumber = jInfo.JournalNumber;
					vOption.VendorName = vInfo.CheckName;
					vOption.VendorNumber = vInfo.VendorNumber;
					voList.AddItem(vOption);
					vList.MoveNext();
				}
				frmJournalVendorChoice jvc = new frmJournalVendorChoice();
				int intReturn = 0;
				jvc.Entries = voList;
				intReturn = jvc.ShowDialog();
				if (intReturn == FCConvert.ToInt32(DialogResult.OK))
				{
					lngID = jvc.ChosenID;
					if (lngID > 0)
					{
						apJourn = jService.GetFullAPJournal(lngID);
					}
				}
				else
				{
					return;
				}
			}
			else
			{
			}
			if (lngID > 0)
			{
				jService.UpdateJournalStatus(jInfo.JournalNumber, "V", "E");
				jService.UpdateAPJournalStatus(jInfo.JournalNumber, "V", "E");
				modBudgetaryAccounting.Statics.SearchResults.Reset();
				// frmAPDataEntry.InstancePtr.Init(ref apJourn);
                ShowAPJournal(ref apJourn);
            }
			else
			{
				jService.UpdateJournalStatus(jInfo.JournalNumber, "V", "E");
				jService.UpdateAPJournalStatus(jInfo.JournalNumber, "V", "E");
				apJourn = new cAPJournal();
				apJourn.JournalNumber = jInfo.JournalNumber;
                apJourn.Period = jInfo.Period;
                apJourn.PayableDate = apEditViewModel.APJournalInfo.PayableDate;
				// frmAPDataEntry.InstancePtr.Init(ref apJourn);
				ShowAPJournal(ref apJourn);
            }
		}

        private bool ShowJournalMasterAP(int journalNumber)
        {
            cEditAPViewModel apView = new cEditAPViewModel();
            apView.LoadAPJournalMaster(journalNumber);
            apView.AllowEditing();
            frmEditAPJournal EditAPJournal = new frmEditAPJournal();
            EditAPJournal.InitializeScreen(ref apView);
            return apView.Cancelled;
        }

        private void ShowAPJournal(ref cAPJournal apJourn)
        {
			/* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            cEditAPViewModel apEditViewModel = new cEditAPViewModel();
            apEditViewModel.LoadAPJournalMaster(apJourn.JournalNumber);
            if (!apEditViewModel.AllValidated())
            {
                frmEditAPJournal EditAPJournal = new frmEditAPJournal();
                EditAPJournal.InitializeScreen(ref apEditViewModel);
                if (!apEditViewModel.Cancelled)
                {
                    apJourn.JournalNumber = apEditViewModel.APJournalInfo.JournalNumber;
                    apJourn.Period = (short)apEditViewModel.APJournalInfo.Period;
                    apJourn.PayableDate = apEditViewModel.APJournalInfo.PayableDate;
                }
            }
            if (!apEditViewModel.Cancelled)
            {
                frmAPDataEntry.InstancePtr.Init(ref apJourn);
                // DoEvents
            }
            XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */
			Sys.ClearInstanceOfType(typeof(frmAPDataEntry));
			frmAPDataEntry.InstancePtr.Init(ref apJourn);
        }

        private void EditACJournal(ref cJournalInfo jInfo)
		{
			if (!jInfo.IsCreditMemo || jInfo.IsCreditMemoCorrection)
			{
				MessageBox.Show("This type of journal may not be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			cGenericCollection vList = new cGenericCollection();
			vList = jService.GetVendorInfosByAPJournal(jInfo.JournalNumber);
			cGenericCollection voList = new cGenericCollection();
			cVendorInfo vInfo;
			cAPJournal apJourn = new cAPJournal();
			int lngID;
			lngID = 0;
			if (vList.ItemCount() > 0)
			{
				cVendorOption vOption = new cVendorOption();
				vOption.VendorName = "New";
				vOption.Amount = 0;
				vOption.Description = "New";
				vOption.JournalNumber = jInfo.JournalNumber;
				vOption.VendorName = "New";
				vOption.VendorNumber = 0;
				voList.AddItem(vOption);
				vList.MoveFirst();
				while (vList.IsCurrent())
				{
					vInfo = (cVendorInfo)vList.GetCurrentItem();
					vOption = new cVendorOption();
					vOption.Amount = vInfo.Amount;
					vOption.Description = vInfo.Description;
					vOption.ID = vInfo.ID;
					vOption.JournalNumber = jInfo.JournalNumber;
					vOption.VendorName = vInfo.CheckName;
					vOption.VendorNumber = vInfo.VendorNumber;
					voList.AddItem(vOption);
					vList.MoveNext();
				}
				frmJournalVendorChoice jvc = new frmJournalVendorChoice();
				int intReturn = 0;
				jvc.Entries = voList;
				intReturn = jvc.ShowDialog();
				if (intReturn == FCConvert.ToInt32(DialogResult.OK))
				{
					lngID = jvc.ChosenID;
					if (lngID > 0)
					{
						apJourn = jService.GetFullAPJournal(lngID);
						if (jService.HadError)
						{
							MessageBox.Show("Error " + FCConvert.ToString(jService.LastErrorNumber) + " " + jService.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
                    else
                    {
                        modBudgetaryMaster.Statics.CurrentCredMemoEntry = apJourn.JournalNumber;
						apJourn.JournalNumber = jInfo.JournalNumber;
                        apJourn.Period = jInfo.Period;
                    }
				}
				else
				{
					return;
				}
			}
			else
			{
			}
			//if (lngID > 0)
			//{
				frmCreditMemoDataEntry.InstancePtr.Init(ref apJourn);
			//}
			//else
			//{
			//}
		}

		private void EditCRJournal(ref cJournalInfo jInfo)
		{
			if (jInfo.Status != "E")
			{
				MessageBox.Show("Only Cash Receipts journals in a status of Entered may be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			modBudgetaryMaster.Statics.CurrentCREntry = jInfo.JournalNumber;
			modBudgetaryMaster.Statics.blnCREdit = true;
			cJournal journ;
			journ = jService.GetFullJournal(jInfo.Id);
			modRegistry.SaveRegistryKey("CURRCRJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry));
			frmCRDataEntry.InstancePtr.Init(ref journ);
		}

		private void EditCDJournal(ref cJournalInfo jInfo)
		{
			if (jInfo.Status != "E")
			{
				MessageBox.Show("Only Cash Disbursements journals in a status of Entered may be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			modBudgetaryMaster.Statics.CurrentCDEntry = jInfo.JournalNumber;
			modBudgetaryMaster.Statics.blnCDEdit = true;
			cJournal journ;
			journ = jService.GetFullJournal(jInfo.Id);
			modRegistry.SaveRegistryKey("CURRCDJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry));
			frmCDDataEntry.InstancePtr.Init(ref journ);
		}

		private void EditGJournal(ref cJournalInfo jInfo)
		{
			if (jInfo.Status != "E")
			{
				MessageBox.Show("Only general journals in a status of Entered may be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.LCase(Strings.Left(jInfo.Description + Strings.StrDup(14, " "), 14)) == "enc adjust for")
			{
				MessageBox.Show("Encumbrance adjustment journals may not be edited", "Invalid Journal Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			bool blnPayrollJournal;
			modBudgetaryMaster.Statics.CurrentGJEntry = jInfo.JournalNumber;
			if (jInfo.JournalType == "PY")
			{
				blnPayrollJournal = true;
			}
			else
			{
				blnPayrollJournal = false;
			}
			cJournal journ;
			journ = jService.GetFullJournal(jInfo.Id);
			modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
			frmGJDataEntry.InstancePtr.Init(journ);
		}

		public void CreateJournal(string strJType)
		{
			if (Strings.LCase(strJType) == "ap")
			{
				CreateAPJournal();
			}
			else if (Strings.LCase(strJType) == "ac")
			{
				CreateACJournal();
			}
			else if (Strings.LCase(strJType) == "gj")
			{
				CreateGJJournal();
			}
			else if (Strings.LCase(strJType) == "cr")
			{
				CreateCRJournal();
			}
			else if (Strings.LCase(strJType) == "cd")
			{
				CreateCDJournal();
			}
			else if (Strings.LCase(strJType) == "en")
			{
				CreateENJournal();
			}
		}

		private void CreateENJournal()
		{
			frmEncumbranceDataEntry.InstancePtr.OldJournal = false;
			modBudgetaryMaster.Statics.blnEncEdit = false;
			frmEncumbranceDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
			frmEncumbranceDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
			frmEncumbranceDataEntry.InstancePtr.VendorNumber = 0;
			modBudgetaryAccounting.Statics.SearchResults.Reset();
			frmEncumbranceDataEntry.InstancePtr.Show(App.MainForm);
			// show t
		}

		private void CreateAPJournal()
		{
            Sys.ClearInstanceOfType(typeof(frmAPDataEntry));
			frmAPDataEntry.InstancePtr.OldJournal = false;
            frmAPDataEntry.InstancePtr.OldJournalNumber = 0;
			modBudgetaryMaster.Statics.blnAPEdit = false;
			frmAPDataEntry.InstancePtr.EncumbranceFlag = false;
			frmAPDataEntry.InstancePtr.PurchaseOrderFlag = false;
			frmAPDataEntry.InstancePtr.FromVendorMaster = false;
			frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
			frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
			frmAPDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = false;
			frmAPDataEntry.InstancePtr.apJournalId = 0;
			frmAPDataEntry.InstancePtr.blnJournalEdit = false;
            modBudgetaryAccounting.Statics.SearchResults.Reset();

            cEditAPViewModel apEditViewModel = new cEditAPViewModel();
            apEditViewModel.LoadAPJournalMaster(frmAPDataEntry.InstancePtr.OldJournalNumber);
            if (!apEditViewModel.AllValidated())
            {
                frmEditAPJournal EditAPJournal = new frmEditAPJournal();
                EditAPJournal.InitializeScreen(ref apEditViewModel);
            }

            if (!apEditViewModel.Cancelled)
            {
                cAPJournal apJourn = new cAPJournal();
                apJourn.Description = "";
                apJourn.JournalNumber = apEditViewModel.APJournalInfo.JournalNumber;
                apJourn.Period = apEditViewModel.APJournalInfo.Period;
                apJourn.PayableDate = apEditViewModel.APJournalInfo.PayableDate;

				//            Load frmWait     'shwo the wait form
				//            frmWait.lblMessage.Caption = "Please Wait...." & vbNewLine & "Loading New Journal"
				//            frmWait.Refresh
				//            frmWait.Show
				
				frmAPDataEntry.InstancePtr.Init(ref apJourn);

                //            frmWait.ZOrder 0
                //            frmWait.Refresh
                //            DoEvents
                // Unload frmWait
                // Unload Me
            }


			// frmAPDataEntry.InstancePtr.Show(App.MainForm);
			// show the blankform
		}

		private void CreateACJournal()
		{
			modBudgetaryMaster.Statics.blnCredMemoEdit = false;
			frmCreditMemoDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
			frmCreditMemoDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
			frmCreditMemoDataEntry.InstancePtr.OldJournal = false;
			frmCreditMemoDataEntry.InstancePtr.OldJournalNumber = 0;
			frmCreditMemoDataEntry.InstancePtr.VendorNumber = 0;
			modBudgetaryAccounting.Statics.SearchResults.Reset();
			frmCreditMemoDataEntry.InstancePtr.Show(App.MainForm);
		}

		private void CreateGJJournal()
		{
			bool blnPayrollJournal;
			modBudgetaryMaster.Statics.CurrentGJEntry = 0;
			frmGJDataEntry.InstancePtr.Init(null);
		}

		private void CreateCRJournal()
		{
			modBudgetaryMaster.Statics.blnCREdit = false;
			frmCRDataEntry.InstancePtr.ChosenJournal = 0;
			frmCRDataEntry.InstancePtr.ChosenPeriod = 0;
			frmCRDataEntry.InstancePtr.Show(App.MainForm);
		}

		private void CreateCDJournal()
		{
			modBudgetaryMaster.Statics.blnCDEdit = false;
			frmCDDataEntry.InstancePtr.ChosenJournal = 0;
			frmCDDataEntry.InstancePtr.ChosenPeriod = 0;
			frmCDDataEntry.InstancePtr.Show(App.MainForm);
		}

		public void ReverseJournal(int lngID, bool boolCorrectingEntries)
		{
			if (lngID > 0)
			{
				postedJournalsColl.MoveFirst();
				cJournalInfo jInfo;
				jInfo = null;
				while (postedJournalsColl.IsCurrent())
				{
					jInfo = (cJournalInfo)postedJournalsColl.GetCurrentItem();
					if (jInfo.Id == lngID)
					{
						CreateAReversingJournal(ref jInfo, boolCorrectingEntries);
						this.Refresh();
						if (this.PostedJournalsChanged != null)
							this.PostedJournalsChanged();
						if (this.UnpostedJournalsChanged != null)
							this.UnpostedJournalsChanged();
						return;
					}
					postedJournalsColl.MoveNext();
				}
				Information.Err().Raise(-9000, null, "Journal not found", null, null);
				return;
			}
		}
		// vbPorter upgrade warning: boolCorrectingEntries As Variant --> As bool
		private void CreateAReversingJournal(ref cJournalInfo jInfo, bool boolCorrectingEntries)
		{
			if (!(jInfo == null))
			{
				if ((Strings.LCase(jInfo.JournalType) == "gj") || (Strings.LCase(jInfo.JournalType) == "cr") || (Strings.LCase(jInfo.JournalType) == "cd"))
				{
					jService.CreateAReversingJournal(ref jInfo, boolCorrectingEntries);
					ShowLastServiceError();
				}
				else
				{
					MessageBox.Show("The process may only be used for General Journal, Cash Receipts and Cash Disbursements journal types", "Invalid Journal Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		public cJournalsView() : base()
		{
			LoadSecurity();
		}
	}
}
