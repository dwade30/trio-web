﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.ComponentModel;
using System.Drawing;
using SharedApplication;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmVendorMaster.
    /// </summary>
    public partial class frmVendorMaster : BaseForm
    {
        public frmVendorMaster()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            txtAddress = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            txtCorAddress = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            cboCode = new System.Collections.Generic.List<fecherFoundation.FCComboBox>();
            txtAdjustment = new System.Collections.Generic.List<T2KBackFillDecimal>();
            lblPosNegValue = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            lblAdjNumber = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
            txtAddress.AddControlArrayElement(txtAddress_5, FCConvert.ToInt16(5));
            txtAddress.AddControlArrayElement(txtAddress_4, FCConvert.ToInt16(4));
            txtAddress.AddControlArrayElement(txtAddress_3, FCConvert.ToInt16(3));
            txtAddress.AddControlArrayElement(txtAddress_0, FCConvert.ToInt16(0));
            txtAddress.AddControlArrayElement(txtAddress_1, FCConvert.ToInt16(1));
            txtAddress.AddControlArrayElement(txtAddress_2, FCConvert.ToInt16(2));
            txtCorAddress.AddControlArrayElement(txtCorAddress_0, FCConvert.ToInt16(0));
            txtCorAddress.AddControlArrayElement(txtCorAddress_1, FCConvert.ToInt16(1));
            txtCorAddress.AddControlArrayElement(txtCorAddress_2, FCConvert.ToInt16(2));
            cboCode.AddControlArrayElement(cboCode_4, FCConvert.ToInt16(4));
            cboCode.AddControlArrayElement(cboCode_3, FCConvert.ToInt16(3));
            cboCode.AddControlArrayElement(cboCode_2, FCConvert.ToInt16(2));
            cboCode.AddControlArrayElement(cboCode_1, FCConvert.ToInt16(1));
            cboCode.AddControlArrayElement(cboCode_0, FCConvert.ToInt16(0));
            txtAdjustment.AddControlArrayElement(txtAdjustment_0, FCConvert.ToInt16(0));
            txtAdjustment.AddControlArrayElement(txtAdjustment_1, FCConvert.ToInt16(1));
            txtAdjustment.AddControlArrayElement(txtAdjustment_2, FCConvert.ToInt16(2));
            txtAdjustment.AddControlArrayElement(txtAdjustment_3, FCConvert.ToInt16(3));
            txtAdjustment.AddControlArrayElement(txtAdjustment_4, FCConvert.ToInt16(4));
            lblPosNegValue.AddControlArrayElement(lblPosNegValue_4, FCConvert.ToInt16(4));
            lblPosNegValue.AddControlArrayElement(lblPosNegValue_3, FCConvert.ToInt16(3));
            lblPosNegValue.AddControlArrayElement(lblPosNegValue_2, FCConvert.ToInt16(2));
            lblPosNegValue.AddControlArrayElement(lblPosNegValue_1, FCConvert.ToInt16(1));
            lblPosNegValue.AddControlArrayElement(lblPosNegValue_0, FCConvert.ToInt16(0));
            lblAdjNumber.AddControlArrayElement(lblAdjNumber_4, FCConvert.ToInt16(4));
            lblAdjNumber.AddControlArrayElement(lblAdjNumber_3, FCConvert.ToInt16(3));
            lblAdjNumber.AddControlArrayElement(lblAdjNumber_2, FCConvert.ToInt16(2));
            lblAdjNumber.AddControlArrayElement(lblAdjNumber_1, FCConvert.ToInt16(1));
            lblAdjNumber.AddControlArrayElement(lblAdjNumber_0, FCConvert.ToInt16(0));
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            cmbCorrespondenceName.SelectedIndex = 1;
            cboOverrideCode.SelectedIndex = 0;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmVendorMaster InstancePtr
        {
            get
            {
                return (frmVendorMaster)Sys.GetInstance(typeof(frmVendorMaster));
            }
        }

        protected frmVendorMaster _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        bool TaxFlag;
        bool BadDataFlag;
        bool EditFlag;
        // vbPorter upgrade warning: TempKey As string	OnWriteFCConvert.ToInt32(
        string TempKey = "";
        bool blnDirty;
        bool blnUnload;
        int AccountCol;
        bool blnMenuFlag;
        clsGridAccount vsGrid = new clsGridAccount();

        private void cboCode_SelectedIndexChanged(int Index, object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void cboCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int index = cboCode.IndexOf((FCComboBox)sender);
            cboCode_SelectedIndexChanged(index, sender, e);
        }

        private void cboCode_DropDown(int Index, object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboCode[Index].Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
        }

        private void cboCode_DropDown(object sender, System.EventArgs e)
        {
            int index = cboCode.IndexOf((FCComboBox)sender);
            cboCode_DropDown(index, sender, e);
        }

        private void chk1099_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chk1099.CheckState == CheckState.Unchecked)
            {
                if (chkAttorney.CheckState == CheckState.Checked)
                {
                    chk1099.CheckState = CheckState.Checked;
                    MessageBox.Show("Any vendor that is an attorney must be sent 1099 information", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void chkAddress_CheckedChanged(object sender, System.EventArgs e)
        {
            int counter;
            if (chkAddress.CheckState == CheckState.Checked)
            {
                // if the address checkbox is clicked
                txtCorName.Text = txtCheckName.Text;
                // save information from check to correspondence
                for (counter = 0; counter <= 2; counter++)
                {
                    txtCorAddress[FCConvert.ToInt16(counter)].Text = txtAddress[FCConvert.ToInt16(counter)].Text;
                    txtCorAddress[FCConvert.ToInt16(counter)].Enabled = false;
                    // disable all the controls in correspondence frame
                }
                txtCorrespondCity.Text = Strings.Trim(txtCheckCity.Text);
                txtCorrespondState.Text = Strings.Trim(txtCheckState.Text);
                txtCorrespondZip.Text = Strings.Trim(txtCheckZip.Text);
                txtCorrespondZip4.Text = Strings.Trim(txtCheckZip4.Text);
                txtCorrespondCity.Enabled = false;
                txtCorrespondState.Enabled = false;
                txtCorrespondZip.Enabled = false;
                txtCorrespondZip4.Enabled = false;
                txtCorName.Enabled = false;
            }
            else
            {
                txtCorName.Enabled = true;
                // else enable all the controls
                txtCorName.Text = "";
                // and make the text boxes blank
                for (counter = 0; counter <= 2; counter++)
                {
                    txtCorAddress[FCConvert.ToInt16(counter)].Enabled = true;
                    txtCorAddress[FCConvert.ToInt16(counter)].Text = "";
                }
                txtCorrespondCity.Text = "";
                txtCorrespondState.Text = "";
                txtCorrespondZip.Text = "";
                txtCorrespondZip4.Text = "";
                txtCorrespondCity.Enabled = true;
                txtCorrespondState.Enabled = true;
                txtCorrespondZip.Enabled = true;
                txtCorrespondZip4.Enabled = true;
            }
        }

        private void chkAttorney_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkAttorney.CheckState == CheckState.Checked)
            {
                chk1099.CheckState = CheckState.Checked;
            }
        }

        private void chkEFT_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkEFT.CheckState == CheckState.Checked)
            {
                fraEFT.Enabled = true;
                lblAccountType.Enabled = true;
                lblRoutingNumber.Enabled = true;
                lblBankAccountNumber.Enabled = true;
                chkPrenote.Enabled = true;
            }
            else
            {
                fraEFT.Enabled = false;
                lblAccountType.Enabled = false;
                lblRoutingNumber.Enabled = false;
                lblBankAccountNumber.Enabled = false;
                chkPrenote.Enabled = false;
            }
        }

        private void chkInsuranceRequired_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkInsuranceRequired.CheckState == CheckState.Checked)
            {
                lblInsuranceVerifiedDate.Enabled = true;
                txtInsuranceVerifiedDate.Enabled = true;
            }
            else
            {
                lblInsuranceVerifiedDate.Enabled = false;
                txtInsuranceVerifiedDate.Enabled = false;
                txtInsuranceVerifiedDate.Text = "";
            }
        }

        private void frmVendorMaster_Activated(object sender, System.EventArgs e)
        {
            int counter;
            if (modGlobal.FormExist(this))
            {
                return;
            }
            vs1.TextMatrix(0, 1, "Account #");
            vs1.TextMatrix(0, 2, "Description");
            vs1.TextMatrix(0, 3, "1099");
            vs1.TextMatrix(0, 4, "Proj");
            vs1.TextMatrix(0, 5, "Amount");
            vs1.ColWidth(0, FCConvert.ToInt32(0.044 * vs1.WidthOriginal));
            vs1.ColWidth(1, FCConvert.ToInt32(0.32 * vs1.WidthOriginal));
            // initialize flexgrid
            vs1.ColWidth(2, FCConvert.ToInt32(0.28 * vs1.WidthOriginal));
            vs1.ColWidth(3, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
            vs1.ColWidth(4, FCConvert.ToInt32(0.078 * vs1.WidthOriginal));
            vs1.ColWidth(5, FCConvert.ToInt32(0.069 * vs1.WidthOriginal));
            vs1.ColFormat(5, "#,##0.00");
            vs1.ColComboList(3, "D|N|1|2|3|4|5|6|7|8|9");
            vs1.ExtendLastCol = true;
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            //FC:FINAL:DDU:#2902 - aligned columns
            //FC:FINAL:BBE:#528 - font size used in original for samll row height
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, 1, vs1.Rows - 1, 5, 8);
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 5, 4);
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vs1.Rows - 1, 3, 4);
            vs1.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
                // number the textgrid
            }
            if (chkAddress.CheckState == CheckState.Checked)
            {
                // handle the address checkbox
                txtCorName.Text = txtCheckName.Text;
                for (counter = 0; counter <= 2; counter++)
                {
                    txtCorAddress[FCConvert.ToInt16(counter)].Text = txtAddress[FCConvert.ToInt16(counter)].Text;
                    txtCorAddress[FCConvert.ToInt16(counter)].Enabled = false;
                }
                txtCorrespondCity.Text = Strings.Trim(txtCheckCity.Text);
                txtCorrespondState.Text = Strings.Trim(txtCheckState.Text);
                txtCorrespondZip.Text = Strings.Trim(txtCheckZip.Text);
                txtCorrespondZip4.Text = Strings.Trim(txtCheckZip4.Text);
                txtCorrespondCity.Enabled = false;
                txtCorrespondState.Enabled = false;
                txtCorrespondZip.Enabled = false;
                txtCorrespondZip4.Enabled = false;
                txtCorName.Enabled = false;
            }
            // If UCase(MuniName) <> "TRIOVILLE" Then
            // SSTab1.TabEnabled(1) = False
            // End If
            //Application.DoEvents();
            txtCheckName.Focus();
            blnDirty = false;
            frmVendorMaster.InstancePtr.Refresh();
            if (modBudgetaryMaster.Statics.blnEdit)
            {
                mnuFilePrintLabel.Enabled = true;
                cmdPrintMailingLabel.Enabled = true;
            }
            else
            {
                mnuFilePrintLabel.Enabled = false;
                cmdPrintMailingLabel.Enabled = false;
            }
        }

        private void frmVendorMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F10)
            {
                KeyCode = (Keys)0;
            }
            if (Strings.UCase(ActiveControl.GetName()) == "VS1")
            {
                if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2 && KeyCode != Keys.F12)
                {
                    modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
                    vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
                }
            }
        }

        private void frmVendorMaster_Load(object sender, System.EventArgs e)
        {

            mnuProcessDelete.Enabled = modBudgetaryMaster.Statics.blnEdit;
            mnuFileViewLinkedDocument.Enabled = modBudgetaryMaster.Statics.blnEdit;
            cmdDeleteVendor.Enabled = modBudgetaryMaster.Statics.blnEdit;
            cmdAttachedDocuments.Enabled = modBudgetaryMaster.Statics.blnEdit;

            Fill1099CodeCombo();
            FillAccountTypeCombo();
            AccountCol = 1;
            vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType =  "E";
            vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this, false);
            SetCustomFormColors();
        }

        private void frmVendorMaster_Resize(object sender, System.EventArgs e)
        {
            vs1.ColWidth(0, FCConvert.ToInt32(0.044 * vs1.WidthOriginal));
            vs1.ColWidth(1, FCConvert.ToInt32(0.32 * vs1.WidthOriginal));
            // initialize flexgrid
            vs1.ColWidth(2, FCConvert.ToInt32(0.28 * vs1.WidthOriginal));
            vs1.ColWidth(3, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
            vs1.ColWidth(4, FCConvert.ToInt32(0.078 * vs1.WidthOriginal));
            vs1.ColWidth(5, FCConvert.ToInt32(0.069 * vs1.WidthOriginal));
        }

        internal void FillAccountTypeCombo()
        {
            cboAccountType.Clear();
            cboAccountType.AddItem("Checking");
            cboAccountType.AddItem("Savings");
            cboAccountType.SelectedIndex = 0;
        }
        // vbPorter upgrade warning: Cancel As short	OnWrite(bool)
        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            if (blnDirty)
            {
                ans = MessageBox.Show("Do you wish to exit without saving?", "Quit without Saving?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (ans == DialogResult.Yes)
                {
                    // do nothing
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            

            if (!modBudgetaryMaster.Statics.blnFromAP && !modBudgetaryMaster.Statics.blnFromEnc && !modBudgetaryMaster.Statics.blnFromCredMemo && !modBudgetaryMaster.Statics.blnFromPO)
                frmGetAccount.InstancePtr.Show(App.MainForm);
            else
            {
                var hasTempKey = TempKey != "";

                if (modBudgetaryMaster.Statics.blnFromAP)
                {
                    modBudgetaryMaster.Statics.blnFromAP = false;
                    frmAPDataEntry.InstancePtr.FromVendorMaster = true;
                    frmAPDataEntry.InstancePtr.txtVendor.Text = TempKey;
                    frmAPDataEntry.InstancePtr.Show(App.MainForm);
                    if (hasTempKey)
                    {
                        frmAPDataEntry.InstancePtr.blnFromVendor = true;
                        frmAPDataEntry.InstancePtr.txtVendor_Validate(false);
                    }
                }
                else
                {
                    if (modBudgetaryMaster.Statics.blnFromEnc)
                    {
                        modBudgetaryMaster.Statics.blnFromEnc = false;
                        frmEncumbranceDataEntry.InstancePtr.blnFromVendorMaster = true;
                        frmEncumbranceDataEntry.InstancePtr.txtVendor.Text = TempKey;
                        frmEncumbranceDataEntry.InstancePtr.Show(App.MainForm);
                        if (hasTempKey)
                        {
                            frmEncumbranceDataEntry.InstancePtr.blnFromVendor = true;
                            frmEncumbranceDataEntry.InstancePtr.txtVendor_Validate(false);
                        }
                    }
                    else
                    {
                        if (modBudgetaryMaster.Statics.blnFromCredMemo)
                        {
                            modBudgetaryMaster.Statics.blnFromCredMemo = false;
                            frmCreditMemoDataEntry.InstancePtr.blnFromVendorMaster = true;
                            frmCreditMemoDataEntry.InstancePtr.txtVendor.Text = TempKey;
                            frmCreditMemoDataEntry.InstancePtr.Show(App.MainForm);
                            if (hasTempKey)
                            {
                                frmCreditMemoDataEntry.InstancePtr.blnFromVendor = true;
                                frmCreditMemoDataEntry.InstancePtr.txtVendor_Validate_2(false);
                            }
                        }
                        else
                        {
                            modBudgetaryMaster.Statics.blnFromPO = false;
                            frmPurchaseOrderDataEntry.InstancePtr.blnFromVendorMaster = true;
                            frmPurchaseOrderDataEntry.InstancePtr.txtVendor.Text = TempKey;
                            frmPurchaseOrderDataEntry.InstancePtr.Show(App.MainForm);
                            if (hasTempKey)
                            {
                                frmPurchaseOrderDataEntry.InstancePtr.blnFromVendor = true;
                                frmPurchaseOrderDataEntry.InstancePtr.txtVendor_Validate(false);
                            }
                        }
                    }
                }
            }
        }

        private void frmVendorMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            switch (KeyAscii)
            {
                // catches the escape and enter keys
                case Keys.Escape:
                    KeyAscii = (Keys)0;
                    Close();

                    break;
                case Keys.Return when frmVendorMaster.InstancePtr.ActiveControl is FCGrid:
                    // Do Nothing
                    break;
                case Keys.Return:
                    KeyAscii = (Keys)0;
                    Support.SendKeys("{TAB}", false);

                    break;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void lblPosNegValue_TextChanged(int Index, object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void lblPosNegValue_TextChanged(object sender, System.EventArgs e)
        {
            int index = lblPosNegValue.IndexOf((FCLabel)sender);
            lblPosNegValue_TextChanged(index, sender, e);
        }

        private void mnuFilePrintLabel_Click(object sender, System.EventArgs e)
        {
            frmVendorLabels.InstancePtr.Init(true, modBudgetaryMaster.Statics.lngCurrentAccount);
        }

        private void mnuFileSave_Click(object sender, System.EventArgs e)
        {
            blnUnload = false;
            SaveInfo();
        }

        private void mnuFileViewLinkedDocument_Click(object sender, System.EventArgs e)
        {
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer(
                "Vendor " + Strings.Format(modBudgetaryMaster.Statics.lngCurrentAccount, "0000") + "  " +
                txtCheckName.Text, "Budgetary", "Vendor", modBudgetaryMaster.Statics.lngCurrentAccount, "",
                false, false));
        }

        private void mnuProcessDelete_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: answer As short, int --> As DialogResult
            DialogResult answer;
            answer = MessageBox.Show("Are you sure you wish to delete this Vendor?", "Delete Vendor?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (answer != DialogResult.Yes) return;

            var rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset($"SELECT * FROM VendorMaster WHERE VendorNumber = {modBudgetaryMaster.Statics.lngCurrentAccount}");
                rs.Edit();
                rs.Set_Fields("Status", "D");
                rs.Update();
            }
            finally
            {
                rs.DisposeOf();
            }
            Close();
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            blnUnload = true;
            SaveInfo();
        }

        private void mnuRowsDeleteRow_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: Check As short, int --> As DialogResult
            int counter;
            var check = MessageBox.Show("Are You Sure You Want To Delete This Entry?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (check != DialogResult.Yes) return;

            vs1.RemoveItem(vs1.Row);
            vs1.Rows += 1;
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
            }
            counter = vs1.Row;
            if (counter > 1)
            {
                counter -= 1;
            }
            vs1.Row = counter;
            vs1.Col = AccountCol;
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
            vs1_RowColChange(sender, e);
            vs1.EditCell();
        }

        private void mnuViewInvoices_Click(object sender, System.EventArgs e)
        {
            ViewInvoices();
        }

        private void txtAddress_TextChanged(int Index, object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtAddress_TextChanged(object sender, System.EventArgs e)
        {
            int index = txtAddress.IndexOf((FCTextBox)sender);
            txtAddress_TextChanged(index, sender, e);
        }

        private void txtAdjustment_Change(int Index, object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtAdjustment_Change(object sender, System.EventArgs e)
        {
            int index = txtAdjustment.IndexOf((Global.T2KBackFillDecimal)sender);
            txtAdjustment_Change(index, sender, e);
        }

        private void txtCheckMessage_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtCheckName_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtCheckZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            var KeyAscii = HandleKeyPress(e);
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private static Keys HandleKeyPress(KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys) Strings.Asc(e.KeyChar);

            if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
            {
                KeyAscii = KeyAscii - 32;
            }

            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back && (KeyAscii < Keys.A || KeyAscii > Keys.Z))
            {
                KeyAscii = (Keys) 0;
            }

            return KeyAscii;
        }

        private void txtCheckZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            var KeyAscii = HandleKeyPress(e);

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtClass_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtClass_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            int counter;
            string temp = "";
            if (txtClass.Text != "")
            {
                temp = txtClass.Text;
                for (counter = 1; counter <= txtClass.Text.Length; counter++)
                {
                    if (!Information.IsNumeric(Strings.Mid(txtClass.Text, counter, 1)))
                    {
                        fecherFoundation.Strings.MidSet(ref temp, counter, 1, Strings.UCase(Strings.Mid(temp, counter, 1)));
                    }
                }
                txtClass.Text = temp;
                for (counter = 1; counter <= txtClass.Text.Length; counter++)
                {
                    if (Information.IsNumeric(Strings.Mid(txtClass.Text, counter, 1)))
                    {
                        if (Conversion.Val(Strings.Mid(txtClass.Text, counter, 1)) == 0)
                        {
                            MessageBox.Show("You may only enter numbers between 1 and 9 or letters between A and K in this field", "Invalid Class Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            DoCancel(e, counter);

                            return;
                        }
                    }
                    else
                    {
                        if (!IsLetterA_to_K(counter))
                        {
                            MessageBox.Show("You may only enter numbers between 1 and 9 or letters between A and K in this field", "Invalid Class Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            DoCancel(e, counter);

                            return;
                        }
                    }
                }
            }
        }

        private bool IsLetterA_to_K(int counter)
        {
            return (Strings.Mid(txtClass.Text, counter, 1) == "A") || (Strings.Mid(txtClass, counter, 1) == "B") || (Strings.Mid(txtClass.Text, counter, 1) == "C") || (Strings.Mid(txtClass, counter, 1) == "D") || (Strings.Mid(txtClass.Text, counter, 1) == "E") || (Strings.Mid(txtClass, counter, 1) == "F") || (Strings.Mid(txtClass.Text, counter, 1) == "G") || (Strings.Mid(txtClass, counter, 1) == "H") || (Strings.Mid(txtClass.Text, counter, 1) == "I") || (Strings.Mid(txtClass, counter, 1) == "J") || (Strings.Mid(txtClass.Text, counter, 1) == "K");
        }

        private void DoCancel(CancelEventArgs e, int counter)
        {
            e.Cancel = true;
            txtClass.SelectionStart = counter - 1;
            txtClass.SelectionLength = 1;
        }

        private void lblPosNegValue_Click(int Index, object sender, System.EventArgs e)
        {
            lblPosNegValue[Index].Text = lblPosNegValue[Index].Text == "-" ? "+" : "-";
        }

        private void lblPosNegValue_Click(object sender, System.EventArgs e)
        {
            int index = lblPosNegValue.IndexOf((FCLabel)sender);
            lblPosNegValue_Click(index, sender, e);
        }

        private void txtContact_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtCorAddress_TextChanged(int Index, object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtCorAddress_TextChanged(object sender, System.EventArgs e)
        {
            int index = txtCorAddress.IndexOf((FCTextBox)sender);
            txtCorAddress_TextChanged(index, sender, e);
        }

        private void txtCorName_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtCorrespondZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            var KeyAscii = HandleKeyPress(e);

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtCorrespondZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            var KeyAscii = HandleKeyPress(e);

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtDataMessage_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtDefaultDescription_Change(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtExtension_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtFax_Change(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtMessage_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtPhone_Change(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtStatus_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            switch (KeyAscii)
            {
                case Keys.NumPad1:
                    KeyAscii = Keys.A;
                    // capitalize any valid data entered into the field
                    break;
                case Keys.NumPad4:
                    KeyAscii = Keys.D;

                    break;
                case Keys.F4:
                    KeyAscii = Keys.S;

                    break;
                case Keys.Back:
                case Keys.A:
                case Keys.D:
                case Keys.S:
                    // do nothing
                    break;
                default:
                    KeyAscii = (Keys)0;

                    break;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtStatus_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // if there is not a valid letter entered show an error message
            if (txtStatus.Text == "A" || txtStatus.Text == "D" || txtStatus.Text == "S") return;

            MessageBox.Show("You may only enter an A, D, or an S", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            e.Cancel = true;
            // cancel the action
            txtStatus.SelectionStart = 0;
            // highlight the incorrect field
            txtStatus.SelectionLength = 1;
        }

        private void txtTaxNumber_TextChanged(object sender, System.EventArgs e)
        {
            blnDirty = true;
        }

        private void txtTaxNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // vbPorter upgrade warning: Check As short, int --> As DialogResult
            DialogResult Check;
            if (!TaxFlag && chk1099.CheckState == CheckState.Checked && txtTaxNumber.Text == "")
            {
                // if the user wants to enter tax data
                // but no number is enterted make sure they know
                Check = MessageBox.Show("Are you sure you don't want to enter a tax number?", "Invalid Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Check == DialogResult.Yes)
                {
                    TaxFlag = true;
                }
            }
        }

        private void vs1_ChangeEdit(object sender, System.EventArgs e)
        {
            if (!vs1.IsCurrentCellInEditMode) return;

            blnDirty = true;
            if (vs1.Col == AccountCol)
            {
                lblTitle.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
            }
        }

        private void vs1_ClickEvent(object sender, System.EventArgs e)
        {
            if (vs1.Row > 0 && vs1.Col > AccountCol)
            {
                vs1.EditCell();
                vs1.EditSelStart = 0;
            }
        }

        private void vs1_Enter(object sender, System.EventArgs e)
        {
            if (frmVendorMaster.InstancePtr.ActiveControl.GetName() != "vs1")
            {
                return;
            }
            if (vs1.Col != 1)
            {
                vs1.EditCell();
                vs1.EditSelStart = 0;
            }
        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (vs1.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F2)
            {
                if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
                {
                    KeyCode = 0;
                    vs1.Col -= 1;
                }
                else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
                {
                    KeyCode = 0;
                    Support.SendKeys("{TAB}", false);
                }
            }
            if (e.KeyCode == Keys.Return)
            {
                KeyCode = 0;
                if (vs1.Col < 5)
                {
                    vs1.Col += 1;

                    if (!BadDataFlag) return;

                    BadDataFlag = false;
                    if (vs1.Col == 5)
                    {
                        if (!modBudgetaryMaster.Statics.ProjectFlag)
                        {
                            vs1.Col -= 2;
                            vs1.TextMatrix(vs1.Row, 5, "");
                        }
                        else
                        {
                            vs1.Col -= 1;
                            vs1.TextMatrix(vs1.Row, 5, "");
                        }
                    }
                    else
                    {
                        vs1.Col -= 1;
                        vs1.TextMatrix(vs1.Row, 5, "");
                    }
                }
                else
                {
                    if (vs1.Row < vs1.Rows - 1)
                    {
                        vs1.Select(vs1.Row + 1, 1);
                        if (BadDataFlag)
                        {
                            BadDataFlag = false;
                            vs1.Select(vs1.Row - 1, 5);
                        }
                    }
                }
            }
            else
            {
                if (KeyCode != Keys.Left) return;

                if (vs1.Col != 5 || modBudgetaryMaster.Statics.ProjectFlag) return;

                KeyCode = 0;
                vs1.Col -= 2;
            }
        }

        private void vs1_RowColChange(object sender, System.EventArgs e)
        {
            int counter;
            modAccountTitle.DetermineAccountTitle(vs1.TextMatrix(vs1.Row, 1), ref lblTitle);
            if (!blnMenuFlag)
            {
                vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
                if (vs1.Col < AccountCol)
                {
                    vs1.Col = AccountCol;
                }
                switch (vs1.Col)
                {
                    case 2:
                        vs1.EditMaxLength = 25;
                        vs1.EditCell();
                        vs1.EditSelStart = 0;

                        break;
                    case 3:
                    {
                        vs1.EditMaxLength = 1;
                        if (vs1.TextMatrix(vs1.Row, vs1.Col) == "")
                        {
                            vs1.TextMatrix(vs1.Row, vs1.Col, "D");
                        }
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                        vs1.EditSelLength = 1;

                        break;
                    }
                    case 4 when modBudgetaryMaster.Statics.ProjectFlag:
                        vs1.EditMaxLength = 4;
                        vs1.EditCell();
                        vs1.EditSelStart = 0;

                        break;
                    case 4:
                        vs1.Col += 1;

                        break;
                    default:
                    {
                        if (vs1.Col > 4)
                        {
                            vs1.EditMaxLength = 10;
                            vs1.EditCell();
                            vs1.EditSelStart = 0;
                            if (vs1.TextMatrix(vs1.Row, vs1.Col) != "" && FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
                            {
                                vs1.EditSelLength = 1;
                            }
                        }

                        break;
                    }
                }
            }
        }

        private void SaveInfo()
        {
            bool flag = false;
            // vbPorter upgrade warning: counter As short --> As int	OnRead(int, string)
            int counter = 0;
            int counter2;
            string temp = "";
            clsDRWrapper rsClassInfo = new clsDRWrapper();
            clsDRWrapper rsFixedAssetsInfo = new clsDRWrapper();
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsCheck = new clsDRWrapper();
            txtStatus.Focus();
            if (Strings.Trim(txtCheckName.Text) == "")
            {
                MessageBox.Show("You must enter a vendor name before you may proceed", "Invalid Vendor Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (IncludeAllPayments() && cboOverrideCode.SelectedIndex < 1)
            {
                MessageBox.Show("If you choose to include all payments for 1099s then you must choose a 1099 code to use for them.", "Invalid Override Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboOverrideCode.Focus();
                return;
            }
            if (InsuranceRequired() && !Information.IsDate(txtInsuranceVerifiedDate.Text))
            {
                MessageBox.Show("If you choose to require insurance for this vendor then you must enter the date that the insurance was last verified.", "Invalid Insurance Verification Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtInsuranceVerifiedDate.Focus();
                return;
            }
            if (IsEFTVendor())
            {
                MessageBox.Show("If you choose to set this vendor as EFT then you must enter the account type, routing number, and bank account before you may continue.", "Invalid EFT Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            while (txtAdjustment[counter].Text != "")
            {
                // if there is somethign in the adjustment box
                if (cboCode[FCConvert.ToInt16(counter)].SelectedIndex < 1)
                {
                    // if the code is 0
                    for (counter2 = 1; counter2 <= txtAdjustment[counter].Text.Length; counter2++)
                    {
                        // go through the adjustment to see if it equals 0
                        temp = Strings.Mid(txtAdjustment[counter].Text, counter2, 1);
                        if (temp != "0" && temp != "," && temp != ".")
                        {
                            flag = true;
                            // if not set the flag
                            break;
                            // and exit the loop
                        }
                    }
                    txtAdjustment[counter].Text = "";
                    // if it is set the adjustment to nothing
                }
                if (counter < 4)
                {
                    // if we are not at the last possible adjustment increment the counter
                    counter += 1;
                }
                else
                {
                    break;
                    // else exit the loop
                }
            }
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                if (IsValidAccountId(counter)) continue;

                MessageBox.Show("One or more of the accounts you entered are bad accounts.  Please correct the accounts and try again.", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                vs1.Select(counter, AccountCol);
                return;
            }
            if (flag)
            {
                // if the flag is set give an error message
                MessageBox.Show("You must have a code between 1 and 9 for each adjustment", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
                // and dont save the data
            }

            if (modBudgetaryMaster.Statics.blnEdit)
            {
                // if this is an existing record retrieve it from the database
                rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount));
                rs.Edit();
                // edit the record
            }
            else
            {
                
                rs.OpenRecordset("SELECT * FROM VendorMaster ORDER BY VendorNumber");
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    counter = 1;
                    do
                    {
                        if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) != counter)
                        {
                            break;
                        }
                        counter += 1;
                        rs.MoveNext();
                    }
                    while (rs.EndOfFile() != true);
                    modBudgetaryMaster.Statics.lngCurrentAccount = counter;
                    rsCheck.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount));
                    if (rsCheck.BeginningOfFile() != true && rsCheck.EndOfFile() != true)
                    {
                        MessageBox.Show("There is an issue with your vendor data.  You will need to contact TRIO to resolve this before you may save a new vendor.", "Vendor Issues", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    modRegistry.SaveRegistryKey("CURRVNDR", modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount), 5));
                    if (IsFromAPOrEncOrCredMemoOrPO())
                    {
                        TempKey = FCConvert.ToString(counter);
                    }
                }
                else
                {
                    modBudgetaryMaster.Statics.lngCurrentAccount = 1;
                    modRegistry.SaveRegistryKey("CURRVNDR", modValidateAccount.GetFormat_6(FCConvert.ToString(1), 5));
                    if (IsFromAPOrEncOrCredMemoOrPO())
                    {
                        TempKey = FCConvert.ToString(1);
                    }
                }
                rs.AddNew();
                rs.Set_Fields("VendorNumber", modBudgetaryMaster.Statics.lngCurrentAccount);
            }
            rs.Set_Fields("CheckName", txtCheckName.Text);
            rs.Set_Fields("CheckAddress1", txtAddress[0].Text);
            rs.Set_Fields("CheckAddress2", txtAddress[1].Text);
            rs.Set_Fields("CheckAddress3", txtAddress[2].Text);
            rs.Set_Fields("CheckCity", Strings.Trim(txtCheckCity.Text));
            rs.Set_Fields("CheckState", Strings.Trim(txtCheckState.Text));
            rs.Set_Fields("CheckZip", Strings.Trim(txtCheckZip.Text));
            rs.Set_Fields("CheckZip4", Strings.Trim(txtCheckZip4.Text));
            rs.Set_Fields("CorrespondCity", Strings.Trim(txtCorrespondCity.Text));
            rs.Set_Fields("CorrespondState", Strings.Trim(txtCorrespondState.Text));
            rs.Set_Fields("CorrespondZip", Strings.Trim(txtCorrespondZip.Text));
            rs.Set_Fields("CorrespondZip4", Strings.Trim(txtCorrespondZip4.Text));
            rs.Set_Fields("CheckAddress4", "");
            rs.Set_Fields("CorrespondAddress4", "");
            rs.Set_Fields("Same", chkAddress.CheckState == CheckState.Checked);
            rs.Set_Fields("CorrespondName", txtCorName.Text);
            rs.Set_Fields("CorrespondAddress1", txtCorAddress[0].Text);
            rs.Set_Fields("CorrespondAddress2", txtCorAddress[1].Text);
            // save all fields to the database
            rs.Set_Fields("CorrespondAddress3", txtCorAddress[2].Text);
            rs.Set_Fields("InsuranceRequired", chkInsuranceRequired.CheckState == CheckState.Checked);
            if (Information.IsDate(txtInsuranceVerifiedDate.Text))
            {
                rs.Set_Fields("InsuranceVerifiedDate", txtInsuranceVerifiedDate.Text);
            }
            else
            {
                rs.Set_Fields("InsuranceVerifiedDate", null);
            }
            rs.Set_Fields("Contact", Strings.Trim(txtContact.Text));
            rs.Set_Fields("TelephoneNumber", txtPhone.Text);
            rs.Set_Fields("FaxNumber", txtFax.Text);
            rs.Set_Fields("Extension", Strings.Trim(txtExtension.Text));
            rs.Set_Fields("Status", Strings.Trim(txtStatus.Text));
            rs.Set_Fields("Class", Strings.Trim(txtClass.Text));
            rs.Set_Fields("Email", Strings.Trim(txtEmail.Text));
            if (chkEFT.CheckState == CheckState.Checked)
            {
                rs.Set_Fields("EFT", true);
                rs.Set_Fields("Prenote", chkPrenote.CheckState == CheckState.Checked);
                rs.Set_Fields("AccountType", Strings.Left(cboAccountType.Text, 1));
                rs.Set_Fields("RoutingNumber", Strings.Trim(txtRoutingNumber.Text));
                rs.Set_Fields("BankAccountNumber", Strings.Trim(txtBankAccountNumber.Text));
            }
            else
            {
                rs.Set_Fields("EFT", false);
                rs.Set_Fields("Prenote", false);
                rs.Set_Fields("AccountType", "");
                rs.Set_Fields("RoutingNumber", "");
                rs.Set_Fields("BankAccountNumber", "");
            }
            rsClassInfo.Execute("DELETE FROM VendorClass WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount), "Budgetary");
            if (Strings.Trim(txtClass.Text) != "")
            {
                rsClassInfo.OpenRecordset("SELECT * FROM VendorClass");
                for (counter = 1; counter <= Strings.Trim(txtClass.Text).Length; counter++)
                {
                    rsClassInfo.AddNew();
                    rsClassInfo.Set_Fields("Class", Strings.Mid(txtClass.Text, counter, 1));
                    rsClassInfo.Set_Fields("VendorNumber", modBudgetaryMaster.Statics.lngCurrentAccount);
                    rsClassInfo.Update(true);
                }
            }
            rs.Set_Fields("1099", chk1099.CheckState == CheckState.Checked);
            rs.Set_Fields("Attorney", chkAttorney.CheckState == CheckState.Checked);
            rs.Set_Fields("IncludeAllPaymentsFor1099", chkIncludeAllPayments.CheckState == CheckState.Checked);
            rs.Set_Fields("1099Code", Strings.Left(cboOverrideCode.Text, 1));
            rs.Set_Fields("1099UseCorrName", cmbCorrespondenceName.SelectedIndex == 0);
            rs.Set_Fields("TaxNumber", Strings.Trim(txtTaxNumber.Text));
            rs.Set_Fields("Message", Strings.Trim(txtMessage.Text));
            if (Strings.Trim(txtCheckMessage.Text) != "")
            {
                if (chkOneTime.CheckState == CheckState.Checked)
                {
                    rs.Set_Fields("OneTimeCheckMessage", true);
                }
                else
                {
                    rs.Set_Fields("OneTimeCheckMessage", false);
                }
            }
            else
            {
                rs.Set_Fields("OneTimeCheckMessage", false);
            }
            rs.Set_Fields("CheckMessage", Strings.Trim(txtCheckMessage.Text));
            rs.Set_Fields("DataEntryMessage", Strings.Trim(txtDataMessage.Text));
            rs.Set_Fields("UpdatedBy", modBudgetaryAccounting.Statics.User);
            rs.Set_Fields("LastUpdated", DateTime.Now);
            rs.Set_Fields("DefaultDescription", Strings.Trim(txtDefaultDescription.Text));
            rs.Set_Fields("DefaultCheckNumber", Strings.Trim(txtDefaultCheckNumber.Text));
            rs.Update();
            // update the database
            rs.Reset();
            SaveAdjustments();
            // save the adjustments
            SaveDefaultInfo();
            // save the default information
            if (!modBudgetaryMaster.Statics.blnEdit)
            {
                MessageBox.Show("Your vendor information has been saved under Vendor #" + modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount), 4), "New Vendor", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Save Successful", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (modBudgetaryMaster.Statics.blnFixedAssetsExists && modBudgetaryMaster.Statics.blnFixedAssetVendorsFromBud)
            {
                rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount));
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "D")
                    {
                        rsFixedAssetsInfo.OpenRecordset("SELECT * FROM tblMaster WHERE VendorCode = '" + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount) + "'", "TWFA0000.vb1");
                        if (rsFixedAssetsInfo.EndOfFile() != true && rsFixedAssetsInfo.BeginningOfFile() != true)
                        {
                            do
                            {
                                rsFixedAssetsInfo.Edit();
                                rsFixedAssetsInfo.Set_Fields("VendorCode", 0);
                                rsFixedAssetsInfo.Set_Fields("ManualVendor", Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckName"))));
                                rsFixedAssetsInfo.Update(false);
                                rsFixedAssetsInfo.MoveNext();
                            }
                            while (rsFixedAssetsInfo.EndOfFile() != true);
                        }
                    }
                }
            }
            blnDirty = false;
            if (blnUnload)
            {
                Close();
            }
            else
            {
                if (modBudgetaryMaster.Statics.blnEdit)
                {
                    // blnEdit = True
                }
                else
                {
                    Close();
                    frmVendorMaster.InstancePtr.Show(App.MainForm);
                }
            }
        }

        private static bool IsFromAPOrEncOrCredMemoOrPO()
        {
            return modBudgetaryMaster.Statics.blnFromAP || modBudgetaryMaster.Statics.blnFromEnc || modBudgetaryMaster.Statics.blnFromCredMemo || modBudgetaryMaster.Statics.blnFromPO;
        }

        private bool IsValidAccountId(int counter)
        {
            return Strings.Trim(vs1.TextMatrix(counter, AccountCol)) == "" || modValidateAccount.AccountValidate(Strings.Trim(vs1.TextMatrix(counter, AccountCol)));
        }

        private bool IsEFTVendor()
        {
            return chkEFT.CheckState == CheckState.Checked && (cboAccountType.SelectedIndex < 0 || Strings.Trim(txtRoutingNumber.Text) == "" || Strings.Trim(txtBankAccountNumber.Text) == "");
        }

        private bool InsuranceRequired()
        {
            return chkInsuranceRequired.CheckState == CheckState.Checked;
        }

        private bool IncludeAllPayments()
        {
            return chkIncludeAllPayments.CheckState == CheckState.Checked;
        }

        private void SaveAdjustments()
        {
            int counter;
            clsDRWrapper rs = new clsDRWrapper();
            counter = 0;
            // initialize the counter
            rs.Execute(("DELETE FROM Adjustments WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount)), "Budgetary");
            while (txtAdjustment[counter].Text != "")
            {
                // if there is an adjustment to save
                rs.OpenRecordset("SELECT * FROM Adjustments WHERE ID = 0");
                rs.AddNew();
                rs.Set_Fields("Adjustment", FCConvert.ToDecimal(txtAdjustment[counter].Text));
                rs.Set_Fields("Positive", (lblPosNegValue[FCConvert.ToInt16(counter)].Text == "-" ? false : true));
                rs.Set_Fields("Code", cboCode[FCConvert.ToInt16(counter)].SelectedIndex);
                rs.Set_Fields("AdjustmentNumber", counter + 1);
                // save data to the database
                rs.Set_Fields("VendorNumber", modBudgetaryMaster.Statics.lngCurrentAccount);
                rs.Update();
                // then update
                rs.Reset();
                counter += 1;
                // increment the counter
                if (counter > 4)
                {
                    break;
                }
            }
        }

        private void SaveDefaultInfo()
        {
            int counter;
            clsDRWrapper rs = new clsDRWrapper();
            counter = 0;
            rs.Execute(("DELETE FROM DefaultInfo WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount)), "Budgetary");
            while (vs1.TextMatrix(counter + 1, 1) != "" && Strings.InStr(1, vs1.TextMatrix(counter + 1, 1), "_", CompareConstants.vbBinaryCompare) == 0)
            {
                // while there is default information to save
                if (modBudgetaryMaster.Statics.blnEdit == true)
                {
                    // if this is an existing record then
                    rs.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentAccount) + "AND DescriptionNumber = " + FCConvert.ToString(counter + 1));
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        rs.Edit();
                        // if the record exists edit it
                    }
                    else
                    {
                        rs.OpenRecordset("SELECT * FROM DefaultInfo");
                        rs.AddNew();
                        // else add a new record to the recordset
                    }
                }
                else
                {
                    // if it is a new vendor record then make a new default information record
                    rs.OpenRecordset("SELECT * FROM DefaultInfo");
                    rs.AddNew();
                }
                rs.Set_Fields("AccountNumber", vs1.TextMatrix(counter + 1, 1));
                rs.Set_Fields("Description", vs1.TextMatrix(counter + 1, 2));
                rs.Set_Fields("Tax", vs1.TextMatrix(counter + 1, 3));
                rs.Set_Fields("Project", vs1.TextMatrix(counter + 1, 4));
                rs.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter + 1, 5))));
                // save the information to the recordset
                rs.Set_Fields("DescriptionNumber", counter + 1);
                rs.Set_Fields("VendorNumber", modBudgetaryMaster.Statics.lngCurrentAccount);
                rs.Update();
                // update the database
                rs.Reset();
                counter += 1;
                // increment the counter
                if (counter >= vs1.Rows - 1)
                {
                    break;
                }
            }
        }

        private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string temp = "";
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = vs1.GetFlexRowIndex(e.RowIndex);
            int col = vs1.GetFlexColIndex(e.ColumnIndex);
            if (col == 3)
            {
                if (vs1.EditText == "")
                {
                    vs1.EditText = "N";
                }
                if (!Information.IsNumeric(vs1.EditText))
                {
                    if (Strings.UCase(vs1.EditText) != "N" && Strings.UCase(vs1.EditText) != "D")
                    {
                        MessageBox.Show("You may only enter an N, D, or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        vs1.EditText = "";
                        vs1.EditCell();
                        BadDataFlag = true;
                    }
                    else
                    {
                        temp = vs1.EditText;
                        vs1.EditText = Strings.UCase(temp);
                    }
                }
                else
                {
                    if (Conversion.Val(vs1.EditText) < 1)
                    {
                        MessageBox.Show("You may only enter an N or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        vs1.EditText = "";
                        vs1.EditCell();
                        BadDataFlag = true;
                    }
                }
            }
            else if (col == 4)
            {
                if (vs1.EditText != "")
                {
                    if (!Information.IsNumeric(vs1.EditText))
                    {
                        MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        vs1.EditText = "";
                        vs1.EditCell();
                        BadDataFlag = true;
                    }
                }
            }
            else if (col == 5)
            {
                if (vs1.EditText != "")
                {
                    if (!Information.IsNumeric(vs1.EditText))
                    {
                        MessageBox.Show("You may only enter a number in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        vs1.EditText = "";
                        vs1.EditCell();
                        BadDataFlag = true;
                    }
                }
                else
                {
                    vs1.EditText = "0";
                }
                // ElseIf vs1.Col = AccountCol Then
                // Cancel = CheckAccountValidate(vs1, Row, Col, Cancel)
            }
        }

        private void SetCustomFormColors()
        {
            lblTitle.ForeColor = Color.Blue;
            chk1099.ForeColor = Color.Blue;
            chkInsuranceRequired.ForeColor = Color.Blue;
        }

        private void vs1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && vs1.MouseRow > 0)
            {
                blnMenuFlag = true;
                vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
                vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionByRow;
                vs1.Row = vs1.MouseRow;
                //this.PopupMenu(mnuRows, this, e.Location);
                mnuRows.ShowDropDown(this, e.X, e.Y);
                //Application.DoEvents();
                blnMenuFlag = false;
            }
        }

        public void Set1099CodeCombo(string strSearch)
        {
            int counter;
            cboOverrideCode.SelectedIndex = -1;
            for (counter = 0; counter <= cboOverrideCode.Items.Count - 1; counter++)
            {
                if (strSearch == Strings.Left(cboOverrideCode.Items[counter].ToString(), 1))
                {
                    cboOverrideCode.SelectedIndex = counter;
                    break;
                }
            }
        }

        private void Fill1099CodeCombo()
        {
            clsDRWrapper rs = new clsDRWrapper();
            int counter;
            cboOverrideCode.Clear();
            cboOverrideCode.AddItem("");
            for (counter = 0; counter <= 4; counter++)
            {
                cboCode[FCConvert.ToInt16(counter)].Clear();
                cboCode[FCConvert.ToInt16(counter)].AddItem("");
            }
            rs.OpenRecordset("SELECT * FROM TaxTitles ORDER BY TaxCode");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                do
                {
                    cboOverrideCode.AddItem(rs.Get_Fields_Int32("TaxCode") + " - " + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("TaxDescription"))));
                    for (counter = 0; counter <= 4; counter++)
                    {
                        cboCode[FCConvert.ToInt16(counter)].AddItem(rs.Get_Fields_Int32("TaxCode") + " - " + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("TaxDescription"))));
                    }
                    rs.MoveNext();
                }
                while (rs.EndOfFile() != true);
            }
        }

        private void cboOverrideCode_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboOverrideCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
        }

        private void ViewInvoices()
        {
            frmVendorInvoices.InstancePtr.InitByVendorNumber(modBudgetaryMaster.Statics.lngCurrentAccount);
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            mnuProcessSave_Click(sender, e);
        }

        private void cmdDeleteVendor_Click(object sender, EventArgs e)
        {
            mnuProcessDelete_Click(sender, e);
        }

        private void cmdAttachedDocuments_Click(object sender, EventArgs e)
        {
            mnuFileViewLinkedDocument_Click(sender, e);
        }

        private void cmdInvoices_Click(object sender, EventArgs e)
        {
            mnuViewInvoices_Click(sender, e);
        }

        private void cmdPrintMailingLabel_Click(object sender, EventArgs e)
        {
            mnuFilePrintLabel_Click(sender, e);
        }
    }
}
