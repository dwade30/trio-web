﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptLaserChecksTop.
	/// </summary>
	partial class rptLaserChecksTop
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLaserChecksTop));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldTextAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTextAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomWarrant1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomWarrant10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDescription10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomReference10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomDiscount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomAmount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgIcon = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomCredit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCredit10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblVoid = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblWarrant = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCredit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Image1 = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBottomAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTextAmount2,
				this.fldTextAmount1,
				this.fldDate2,
				this.fldAmount2,
				this.fldVendorName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.Label11,
				this.Label12,
				this.fldBottomDate,
				this.Label13,
				this.fldBottomVendor,
				this.Label14,
				this.fldBottomCheck,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.fldBottomWarrant1,
				this.fldBottomWarrant2,
				this.fldBottomWarrant3,
				this.fldBottomWarrant4,
				this.fldBottomWarrant5,
				this.fldBottomWarrant6,
				this.fldBottomWarrant7,
				this.fldBottomWarrant8,
				this.fldBottomWarrant9,
				this.fldBottomWarrant10,
				this.fldBottomDescription1,
				this.fldBottomDescription2,
				this.fldBottomDescription3,
				this.fldBottomDescription4,
				this.fldBottomDescription5,
				this.fldBottomDescription6,
				this.fldBottomDescription7,
				this.fldBottomDescription8,
				this.fldBottomDescription9,
				this.fldBottomDescription10,
				this.fldBottomReference1,
				this.fldBottomReference2,
				this.fldBottomReference3,
				this.fldBottomReference4,
				this.fldBottomReference5,
				this.fldBottomReference6,
				this.fldBottomReference7,
				this.fldBottomReference8,
				this.fldBottomReference9,
				this.fldBottomReference10,
				this.fldBottomDiscount1,
				this.fldBottomDiscount2,
				this.fldBottomDiscount3,
				this.fldBottomDiscount4,
				this.fldBottomDiscount5,
				this.fldBottomDiscount6,
				this.fldBottomDiscount7,
				this.fldBottomDiscount8,
				this.fldBottomDiscount9,
				this.fldBottomDiscount10,
				this.fldBottomAmount1,
				this.fldBottomAmount2,
				this.fldBottomAmount3,
				this.fldBottomAmount4,
				this.fldBottomAmount5,
				this.fldBottomAmount6,
				this.fldBottomAmount7,
				this.fldBottomAmount8,
				this.fldBottomAmount9,
				this.fldBottomAmount10,
				this.imgIcon,
				this.Label21,
				this.fldBottomCredit1,
				this.fldBottomCredit2,
				this.fldBottomCredit3,
				this.fldBottomCredit4,
				this.fldBottomCredit5,
				this.fldBottomCredit6,
				this.fldBottomCredit7,
				this.fldBottomCredit8,
				this.fldBottomCredit9,
				this.fldBottomCredit10,
				this.lblVoid,
				this.Label1,
				this.Label2,
				this.fldDate,
				this.Label3,
				this.fldVendor,
				this.Label4,
				this.fldCheck,
				this.lblWarrant,
				this.lblDescription,
				this.lblReference,
				this.lblDiscount,
				this.lblAmount,
				this.fldMuniName,
				this.Label10,
				this.fldAmount,
				this.fldWarrant1,
				this.fldWarrant2,
				this.fldWarrant3,
				this.fldWarrant4,
				this.fldWarrant5,
				this.fldWarrant6,
				this.fldWarrant7,
				this.fldWarrant8,
				this.fldWarrant9,
				this.fldWarrant10,
				this.fldDescription1,
				this.fldDescription2,
				this.fldDescription3,
				this.fldDescription4,
				this.fldDescription5,
				this.fldDescription6,
				this.fldDescription7,
				this.fldDescription8,
				this.fldDescription9,
				this.fldDescription10,
				this.fldReference1,
				this.fldReference2,
				this.fldReference3,
				this.fldReference4,
				this.fldReference5,
				this.fldReference6,
				this.fldReference7,
				this.fldReference8,
				this.fldReference9,
				this.fldReference10,
				this.fldDiscount1,
				this.fldDiscount2,
				this.fldDiscount3,
				this.fldDiscount4,
				this.fldDiscount5,
				this.fldDiscount6,
				this.fldDiscount7,
				this.fldDiscount8,
				this.fldDiscount9,
				this.fldDiscount10,
				this.fldAmt1,
				this.fldAmt2,
				this.fldAmt3,
				this.fldAmt4,
				this.fldAmt5,
				this.fldAmt6,
				this.fldAmt7,
				this.fldAmt8,
				this.fldAmt9,
				this.fldAmt10,
				this.Label20,
				this.fldCredit1,
				this.fldCredit2,
				this.fldCredit3,
				this.fldCredit4,
				this.fldCredit5,
				this.fldCredit6,
				this.fldCredit7,
				this.fldCredit8,
				this.fldCredit9,
				this.fldCredit10,
				this.Image1,
				this.fldCheckMessage,
				this.fldBottomMuniName,
				this.Label22,
				this.fldBottomAmount,
				this.fldBottomCheckMessage
			});
			this.Detail.Height = 10.21875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanShrink = true;
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldTextAmount2
			// 
			this.fldTextAmount2.Height = 0.1875F;
			this.fldTextAmount2.Left = 0.53125F;
			this.fldTextAmount2.Name = "fldTextAmount2";
			this.fldTextAmount2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldTextAmount2.Text = "Field2";
			this.fldTextAmount2.Top = 1.270833F;
			this.fldTextAmount2.Width = 5.875F;
			// 
			// fldTextAmount1
			// 
			this.fldTextAmount1.Height = 0.1875F;
			this.fldTextAmount1.Left = 0.53125F;
			this.fldTextAmount1.Name = "fldTextAmount1";
			this.fldTextAmount1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldTextAmount1.Text = "1234567890123456789012345678901234567890123456789012345678901234567890";
			this.fldTextAmount1.Top = 1.083333F;
			this.fldTextAmount1.Width = 5.875F;
			// 
			// fldDate2
			// 
			this.fldDate2.Height = 0.1875F;
			this.fldDate2.Left = 4.93125F;
			this.fldDate2.Name = "fldDate2";
			this.fldDate2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDate2.Text = "Field2";
			this.fldDate2.Top = 1.583333F;
			this.fldDate2.Width = 0.78125F;
			// 
			// fldAmount2
			// 
			this.fldAmount2.Height = 0.1875F;
			this.fldAmount2.Left = 6.340972F;
			this.fldAmount2.Name = "fldAmount2";
			this.fldAmount2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmount2.Text = "12345678901234";
			this.fldAmount2.Top = 1.583333F;
			this.fldAmount2.Width = 1.21875F;
			// 
			// fldVendorName
			// 
			this.fldVendorName.Height = 0.1875F;
			this.fldVendorName.Left = 0.53125F;
			this.fldVendorName.Name = "fldVendorName";
			this.fldVendorName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldVendorName.Text = "Field2";
			this.fldVendorName.Top = 1.916667F;
			this.fldVendorName.Width = 3.3125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.53125F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress1.Text = "Field2";
			this.fldAddress1.Top = 2.09375F;
			this.fldAddress1.Width = 3.3125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.53125F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress2.Text = "Field2";
			this.fldAddress2.Top = 2.28125F;
			this.fldAddress2.Width = 3.3125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.53125F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress3.Text = "Field2";
			this.fldAddress3.Top = 2.46875F;
			this.fldAddress3.Width = 3.3125F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 0.53125F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress4.Text = "Field2";
			this.fldAddress4.Top = 2.65625F;
			this.fldAddress4.Width = 3.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.53125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label11.Text = "-- A/P CHECK --";
			this.Label11.Top = 3.6875F;
			this.Label11.Width = 1.1875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 4.875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label12.Text = "DATE";
			this.Label12.Top = 3.6875F;
			this.Label12.Width = 0.4375F;
			// 
			// fldBottomDate
			// 
			this.fldBottomDate.Height = 0.19F;
			this.fldBottomDate.Left = 5.34375F;
			this.fldBottomDate.Name = "fldBottomDate";
			this.fldBottomDate.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBottomDate.Text = "Field2";
			this.fldBottomDate.Top = 3.6875F;
			this.fldBottomDate.Width = 0.78125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.65625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label13.Text = "PAYEE:";
			this.Label13.Top = 4.03125F;
			this.Label13.Width = 0.59375F;
			// 
			// fldBottomVendor
			// 
			this.fldBottomVendor.Height = 0.19F;
			this.fldBottomVendor.Left = 2.28125F;
			this.fldBottomVendor.Name = "fldBottomVendor";
			this.fldBottomVendor.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBottomVendor.Text = "Field2";
			this.fldBottomVendor.Top = 4.03125F;
			this.fldBottomVendor.Width = 3.75F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.28125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label14.Text = "CHECK";
			this.Label14.Top = 4.03125F;
			this.Label14.Width = 0.59375F;
			// 
			// fldBottomCheck
			// 
			this.fldBottomCheck.Height = 0.19F;
			this.fldBottomCheck.Left = 6.90625F;
			this.fldBottomCheck.Name = "fldBottomCheck";
			this.fldBottomCheck.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldBottomCheck.Text = "Field2";
			this.fldBottomCheck.Top = 4.03125F;
			this.fldBottomCheck.Width = 0.625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.09375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label15.Text = "WRNT";
			this.Label15.Top = 4.28125F;
			this.Label15.Width = 0.46875F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.65625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label16.Text = "DESCRIPTION----------";
			this.Label16.Top = 4.28125F;
			this.Label16.Width = 1.65625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.78125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label17.Text = "REFERENCE-----";
			this.Label17.Top = 4.28125F;
			this.Label17.Width = 1.1875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 5.75F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.Label18.Text = "-DISC-";
			this.Label18.Top = 4.28125F;
			this.Label18.Width = 0.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 6.75F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.Label19.Text = "-AMOUNT-";
			this.Label19.Top = 4.28125F;
			this.Label19.Width = 0.78125F;
			// 
			// fldBottomWarrant1
			// 
			this.fldBottomWarrant1.CanGrow = false;
			this.fldBottomWarrant1.Height = 0.19F;
			this.fldBottomWarrant1.Left = 0.09375F;
			this.fldBottomWarrant1.MultiLine = false;
			this.fldBottomWarrant1.Name = "fldBottomWarrant1";
			this.fldBottomWarrant1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant1.Text = "Field1";
			this.fldBottomWarrant1.Top = 4.5F;
			this.fldBottomWarrant1.Width = 0.46875F;
			// 
			// fldBottomWarrant2
			// 
			this.fldBottomWarrant2.CanGrow = false;
			this.fldBottomWarrant2.Height = 0.19F;
			this.fldBottomWarrant2.Left = 0.09375F;
			this.fldBottomWarrant2.MultiLine = false;
			this.fldBottomWarrant2.Name = "fldBottomWarrant2";
			this.fldBottomWarrant2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant2.Text = "Field1";
			this.fldBottomWarrant2.Top = 4.6875F;
			this.fldBottomWarrant2.Width = 0.46875F;
			// 
			// fldBottomWarrant3
			// 
			this.fldBottomWarrant3.CanGrow = false;
			this.fldBottomWarrant3.Height = 0.19F;
			this.fldBottomWarrant3.Left = 0.09375F;
			this.fldBottomWarrant3.MultiLine = false;
			this.fldBottomWarrant3.Name = "fldBottomWarrant3";
			this.fldBottomWarrant3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant3.Text = "Field1";
			this.fldBottomWarrant3.Top = 4.875F;
			this.fldBottomWarrant3.Width = 0.46875F;
			// 
			// fldBottomWarrant4
			// 
			this.fldBottomWarrant4.CanGrow = false;
			this.fldBottomWarrant4.Height = 0.19F;
			this.fldBottomWarrant4.Left = 0.09375F;
			this.fldBottomWarrant4.MultiLine = false;
			this.fldBottomWarrant4.Name = "fldBottomWarrant4";
			this.fldBottomWarrant4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant4.Text = "Field1";
			this.fldBottomWarrant4.Top = 5.0625F;
			this.fldBottomWarrant4.Width = 0.46875F;
			// 
			// fldBottomWarrant5
			// 
			this.fldBottomWarrant5.CanGrow = false;
			this.fldBottomWarrant5.Height = 0.19F;
			this.fldBottomWarrant5.Left = 0.09375F;
			this.fldBottomWarrant5.MultiLine = false;
			this.fldBottomWarrant5.Name = "fldBottomWarrant5";
			this.fldBottomWarrant5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant5.Text = "Field1";
			this.fldBottomWarrant5.Top = 5.25F;
			this.fldBottomWarrant5.Width = 0.46875F;
			// 
			// fldBottomWarrant6
			// 
			this.fldBottomWarrant6.CanGrow = false;
			this.fldBottomWarrant6.Height = 0.19F;
			this.fldBottomWarrant6.Left = 0.09375F;
			this.fldBottomWarrant6.MultiLine = false;
			this.fldBottomWarrant6.Name = "fldBottomWarrant6";
			this.fldBottomWarrant6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant6.Text = "Field1";
			this.fldBottomWarrant6.Top = 5.4375F;
			this.fldBottomWarrant6.Width = 0.46875F;
			// 
			// fldBottomWarrant7
			// 
			this.fldBottomWarrant7.CanGrow = false;
			this.fldBottomWarrant7.Height = 0.19F;
			this.fldBottomWarrant7.Left = 0.09375F;
			this.fldBottomWarrant7.MultiLine = false;
			this.fldBottomWarrant7.Name = "fldBottomWarrant7";
			this.fldBottomWarrant7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant7.Text = "Field1";
			this.fldBottomWarrant7.Top = 5.625F;
			this.fldBottomWarrant7.Width = 0.46875F;
			// 
			// fldBottomWarrant8
			// 
			this.fldBottomWarrant8.CanGrow = false;
			this.fldBottomWarrant8.Height = 0.19F;
			this.fldBottomWarrant8.Left = 0.09375F;
			this.fldBottomWarrant8.MultiLine = false;
			this.fldBottomWarrant8.Name = "fldBottomWarrant8";
			this.fldBottomWarrant8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant8.Text = "Field1";
			this.fldBottomWarrant8.Top = 5.8125F;
			this.fldBottomWarrant8.Width = 0.46875F;
			// 
			// fldBottomWarrant9
			// 
			this.fldBottomWarrant9.CanGrow = false;
			this.fldBottomWarrant9.Height = 0.19F;
			this.fldBottomWarrant9.Left = 0.09375F;
			this.fldBottomWarrant9.MultiLine = false;
			this.fldBottomWarrant9.Name = "fldBottomWarrant9";
			this.fldBottomWarrant9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant9.Text = "Field1";
			this.fldBottomWarrant9.Top = 6F;
			this.fldBottomWarrant9.Width = 0.46875F;
			// 
			// fldBottomWarrant10
			// 
			this.fldBottomWarrant10.CanGrow = false;
			this.fldBottomWarrant10.Height = 0.19F;
			this.fldBottomWarrant10.Left = 0.09375F;
			this.fldBottomWarrant10.MultiLine = false;
			this.fldBottomWarrant10.Name = "fldBottomWarrant10";
			this.fldBottomWarrant10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomWarrant10.Text = "Field1";
			this.fldBottomWarrant10.Top = 6.1875F;
			this.fldBottomWarrant10.Width = 0.46875F;
			// 
			// fldBottomDescription1
			// 
			this.fldBottomDescription1.CanGrow = false;
			this.fldBottomDescription1.Height = 0.19F;
			this.fldBottomDescription1.Left = 0.65625F;
			this.fldBottomDescription1.MultiLine = false;
			this.fldBottomDescription1.Name = "fldBottomDescription1";
			this.fldBottomDescription1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription1.Text = "Field1";
			this.fldBottomDescription1.Top = 4.5F;
			this.fldBottomDescription1.Width = 2F;
			// 
			// fldBottomDescription2
			// 
			this.fldBottomDescription2.CanGrow = false;
			this.fldBottomDescription2.Height = 0.19F;
			this.fldBottomDescription2.Left = 0.65625F;
			this.fldBottomDescription2.MultiLine = false;
			this.fldBottomDescription2.Name = "fldBottomDescription2";
			this.fldBottomDescription2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription2.Text = "Field1";
			this.fldBottomDescription2.Top = 4.6875F;
			this.fldBottomDescription2.Width = 2F;
			// 
			// fldBottomDescription3
			// 
			this.fldBottomDescription3.CanGrow = false;
			this.fldBottomDescription3.Height = 0.19F;
			this.fldBottomDescription3.Left = 0.65625F;
			this.fldBottomDescription3.MultiLine = false;
			this.fldBottomDescription3.Name = "fldBottomDescription3";
			this.fldBottomDescription3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription3.Text = "Field1";
			this.fldBottomDescription3.Top = 4.875F;
			this.fldBottomDescription3.Width = 2F;
			// 
			// fldBottomDescription4
			// 
			this.fldBottomDescription4.CanGrow = false;
			this.fldBottomDescription4.Height = 0.19F;
			this.fldBottomDescription4.Left = 0.65625F;
			this.fldBottomDescription4.MultiLine = false;
			this.fldBottomDescription4.Name = "fldBottomDescription4";
			this.fldBottomDescription4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription4.Text = "Field1";
			this.fldBottomDescription4.Top = 5.0625F;
			this.fldBottomDescription4.Width = 2F;
			// 
			// fldBottomDescription5
			// 
			this.fldBottomDescription5.CanGrow = false;
			this.fldBottomDescription5.Height = 0.19F;
			this.fldBottomDescription5.Left = 0.65625F;
			this.fldBottomDescription5.MultiLine = false;
			this.fldBottomDescription5.Name = "fldBottomDescription5";
			this.fldBottomDescription5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription5.Text = "Field1";
			this.fldBottomDescription5.Top = 5.25F;
			this.fldBottomDescription5.Width = 2F;
			// 
			// fldBottomDescription6
			// 
			this.fldBottomDescription6.CanGrow = false;
			this.fldBottomDescription6.Height = 0.19F;
			this.fldBottomDescription6.Left = 0.65625F;
			this.fldBottomDescription6.MultiLine = false;
			this.fldBottomDescription6.Name = "fldBottomDescription6";
			this.fldBottomDescription6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription6.Text = "Field1";
			this.fldBottomDescription6.Top = 5.4375F;
			this.fldBottomDescription6.Width = 2F;
			// 
			// fldBottomDescription7
			// 
			this.fldBottomDescription7.CanGrow = false;
			this.fldBottomDescription7.Height = 0.19F;
			this.fldBottomDescription7.Left = 0.65625F;
			this.fldBottomDescription7.MultiLine = false;
			this.fldBottomDescription7.Name = "fldBottomDescription7";
			this.fldBottomDescription7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription7.Text = "Field1";
			this.fldBottomDescription7.Top = 5.625F;
			this.fldBottomDescription7.Width = 2F;
			// 
			// fldBottomDescription8
			// 
			this.fldBottomDescription8.CanGrow = false;
			this.fldBottomDescription8.Height = 0.19F;
			this.fldBottomDescription8.Left = 0.65625F;
			this.fldBottomDescription8.MultiLine = false;
			this.fldBottomDescription8.Name = "fldBottomDescription8";
			this.fldBottomDescription8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription8.Text = "Field1";
			this.fldBottomDescription8.Top = 5.8125F;
			this.fldBottomDescription8.Width = 2F;
			// 
			// fldBottomDescription9
			// 
			this.fldBottomDescription9.CanGrow = false;
			this.fldBottomDescription9.Height = 0.19F;
			this.fldBottomDescription9.Left = 0.65625F;
			this.fldBottomDescription9.MultiLine = false;
			this.fldBottomDescription9.Name = "fldBottomDescription9";
			this.fldBottomDescription9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription9.Text = "Field1";
			this.fldBottomDescription9.Top = 6F;
			this.fldBottomDescription9.Width = 2F;
			// 
			// fldBottomDescription10
			// 
			this.fldBottomDescription10.CanGrow = false;
			this.fldBottomDescription10.Height = 0.19F;
			this.fldBottomDescription10.Left = 0.65625F;
			this.fldBottomDescription10.MultiLine = false;
			this.fldBottomDescription10.Name = "fldBottomDescription10";
			this.fldBottomDescription10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomDescription10.Text = "Field1";
			this.fldBottomDescription10.Top = 6.1875F;
			this.fldBottomDescription10.Width = 2F;
			// 
			// fldBottomReference1
			// 
			this.fldBottomReference1.CanGrow = false;
			this.fldBottomReference1.Height = 0.19F;
			this.fldBottomReference1.Left = 2.78125F;
			this.fldBottomReference1.MultiLine = false;
			this.fldBottomReference1.Name = "fldBottomReference1";
			this.fldBottomReference1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference1.Text = "Field1";
			this.fldBottomReference1.Top = 4.5F;
			this.fldBottomReference1.Width = 1.34375F;
			// 
			// fldBottomReference2
			// 
			this.fldBottomReference2.CanGrow = false;
			this.fldBottomReference2.Height = 0.19F;
			this.fldBottomReference2.Left = 2.78125F;
			this.fldBottomReference2.MultiLine = false;
			this.fldBottomReference2.Name = "fldBottomReference2";
			this.fldBottomReference2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference2.Text = "Field1";
			this.fldBottomReference2.Top = 4.6875F;
			this.fldBottomReference2.Width = 1.34375F;
			// 
			// fldBottomReference3
			// 
			this.fldBottomReference3.CanGrow = false;
			this.fldBottomReference3.Height = 0.19F;
			this.fldBottomReference3.Left = 2.78125F;
			this.fldBottomReference3.MultiLine = false;
			this.fldBottomReference3.Name = "fldBottomReference3";
			this.fldBottomReference3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference3.Text = "Field1";
			this.fldBottomReference3.Top = 4.875F;
			this.fldBottomReference3.Width = 1.34375F;
			// 
			// fldBottomReference4
			// 
			this.fldBottomReference4.CanGrow = false;
			this.fldBottomReference4.Height = 0.19F;
			this.fldBottomReference4.Left = 2.78125F;
			this.fldBottomReference4.MultiLine = false;
			this.fldBottomReference4.Name = "fldBottomReference4";
			this.fldBottomReference4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference4.Text = "Field1";
			this.fldBottomReference4.Top = 5.0625F;
			this.fldBottomReference4.Width = 1.34375F;
			// 
			// fldBottomReference5
			// 
			this.fldBottomReference5.CanGrow = false;
			this.fldBottomReference5.Height = 0.19F;
			this.fldBottomReference5.Left = 2.78125F;
			this.fldBottomReference5.MultiLine = false;
			this.fldBottomReference5.Name = "fldBottomReference5";
			this.fldBottomReference5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference5.Text = "Field1";
			this.fldBottomReference5.Top = 5.25F;
			this.fldBottomReference5.Width = 1.34375F;
			// 
			// fldBottomReference6
			// 
			this.fldBottomReference6.CanGrow = false;
			this.fldBottomReference6.Height = 0.19F;
			this.fldBottomReference6.Left = 2.78125F;
			this.fldBottomReference6.MultiLine = false;
			this.fldBottomReference6.Name = "fldBottomReference6";
			this.fldBottomReference6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference6.Text = "Field1";
			this.fldBottomReference6.Top = 5.4375F;
			this.fldBottomReference6.Width = 1.34375F;
			// 
			// fldBottomReference7
			// 
			this.fldBottomReference7.CanGrow = false;
			this.fldBottomReference7.Height = 0.19F;
			this.fldBottomReference7.Left = 2.78125F;
			this.fldBottomReference7.MultiLine = false;
			this.fldBottomReference7.Name = "fldBottomReference7";
			this.fldBottomReference7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference7.Text = "Field1";
			this.fldBottomReference7.Top = 5.625F;
			this.fldBottomReference7.Width = 1.34375F;
			// 
			// fldBottomReference8
			// 
			this.fldBottomReference8.CanGrow = false;
			this.fldBottomReference8.Height = 0.19F;
			this.fldBottomReference8.Left = 2.78125F;
			this.fldBottomReference8.MultiLine = false;
			this.fldBottomReference8.Name = "fldBottomReference8";
			this.fldBottomReference8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference8.Text = "Field1";
			this.fldBottomReference8.Top = 5.8125F;
			this.fldBottomReference8.Width = 1.34375F;
			// 
			// fldBottomReference9
			// 
			this.fldBottomReference9.CanGrow = false;
			this.fldBottomReference9.Height = 0.19F;
			this.fldBottomReference9.Left = 2.78125F;
			this.fldBottomReference9.MultiLine = false;
			this.fldBottomReference9.Name = "fldBottomReference9";
			this.fldBottomReference9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference9.Text = "Field1";
			this.fldBottomReference9.Top = 6F;
			this.fldBottomReference9.Width = 1.34375F;
			// 
			// fldBottomReference10
			// 
			this.fldBottomReference10.CanGrow = false;
			this.fldBottomReference10.Height = 0.19F;
			this.fldBottomReference10.Left = 2.78125F;
			this.fldBottomReference10.MultiLine = false;
			this.fldBottomReference10.Name = "fldBottomReference10";
			this.fldBottomReference10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomReference10.Text = "Field1";
			this.fldBottomReference10.Top = 6.1875F;
			this.fldBottomReference10.Width = 1.34375F;
			// 
			// fldBottomDiscount1
			// 
			this.fldBottomDiscount1.CanGrow = false;
			this.fldBottomDiscount1.Height = 0.19F;
			this.fldBottomDiscount1.Left = 5.34375F;
			this.fldBottomDiscount1.MultiLine = false;
			this.fldBottomDiscount1.Name = "fldBottomDiscount1";
			this.fldBottomDiscount1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount1.Text = "Field1";
			this.fldBottomDiscount1.Top = 4.5F;
			this.fldBottomDiscount1.Width = 1.03125F;
			// 
			// fldBottomDiscount2
			// 
			this.fldBottomDiscount2.CanGrow = false;
			this.fldBottomDiscount2.Height = 0.19F;
			this.fldBottomDiscount2.Left = 5.34375F;
			this.fldBottomDiscount2.MultiLine = false;
			this.fldBottomDiscount2.Name = "fldBottomDiscount2";
			this.fldBottomDiscount2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount2.Text = "Field1";
			this.fldBottomDiscount2.Top = 4.6875F;
			this.fldBottomDiscount2.Width = 1.03125F;
			// 
			// fldBottomDiscount3
			// 
			this.fldBottomDiscount3.CanGrow = false;
			this.fldBottomDiscount3.Height = 0.19F;
			this.fldBottomDiscount3.Left = 5.34375F;
			this.fldBottomDiscount3.MultiLine = false;
			this.fldBottomDiscount3.Name = "fldBottomDiscount3";
			this.fldBottomDiscount3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount3.Text = "Field1";
			this.fldBottomDiscount3.Top = 4.875F;
			this.fldBottomDiscount3.Width = 1.03125F;
			// 
			// fldBottomDiscount4
			// 
			this.fldBottomDiscount4.CanGrow = false;
			this.fldBottomDiscount4.Height = 0.19F;
			this.fldBottomDiscount4.Left = 5.34375F;
			this.fldBottomDiscount4.MultiLine = false;
			this.fldBottomDiscount4.Name = "fldBottomDiscount4";
			this.fldBottomDiscount4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount4.Text = "Field1";
			this.fldBottomDiscount4.Top = 5.0625F;
			this.fldBottomDiscount4.Width = 1.03125F;
			// 
			// fldBottomDiscount5
			// 
			this.fldBottomDiscount5.CanGrow = false;
			this.fldBottomDiscount5.Height = 0.19F;
			this.fldBottomDiscount5.Left = 5.34375F;
			this.fldBottomDiscount5.MultiLine = false;
			this.fldBottomDiscount5.Name = "fldBottomDiscount5";
			this.fldBottomDiscount5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount5.Text = "Field1";
			this.fldBottomDiscount5.Top = 5.25F;
			this.fldBottomDiscount5.Width = 1.03125F;
			// 
			// fldBottomDiscount6
			// 
			this.fldBottomDiscount6.CanGrow = false;
			this.fldBottomDiscount6.Height = 0.19F;
			this.fldBottomDiscount6.Left = 5.34375F;
			this.fldBottomDiscount6.MultiLine = false;
			this.fldBottomDiscount6.Name = "fldBottomDiscount6";
			this.fldBottomDiscount6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount6.Text = "Field1";
			this.fldBottomDiscount6.Top = 5.4375F;
			this.fldBottomDiscount6.Width = 1.03125F;
			// 
			// fldBottomDiscount7
			// 
			this.fldBottomDiscount7.CanGrow = false;
			this.fldBottomDiscount7.Height = 0.19F;
			this.fldBottomDiscount7.Left = 5.34375F;
			this.fldBottomDiscount7.MultiLine = false;
			this.fldBottomDiscount7.Name = "fldBottomDiscount7";
			this.fldBottomDiscount7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount7.Text = "Field1";
			this.fldBottomDiscount7.Top = 5.625F;
			this.fldBottomDiscount7.Width = 1.03125F;
			// 
			// fldBottomDiscount8
			// 
			this.fldBottomDiscount8.CanGrow = false;
			this.fldBottomDiscount8.Height = 0.19F;
			this.fldBottomDiscount8.Left = 5.34375F;
			this.fldBottomDiscount8.MultiLine = false;
			this.fldBottomDiscount8.Name = "fldBottomDiscount8";
			this.fldBottomDiscount8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount8.Text = "Field1";
			this.fldBottomDiscount8.Top = 5.8125F;
			this.fldBottomDiscount8.Width = 1.03125F;
			// 
			// fldBottomDiscount9
			// 
			this.fldBottomDiscount9.CanGrow = false;
			this.fldBottomDiscount9.Height = 0.19F;
			this.fldBottomDiscount9.Left = 5.34375F;
			this.fldBottomDiscount9.MultiLine = false;
			this.fldBottomDiscount9.Name = "fldBottomDiscount9";
			this.fldBottomDiscount9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount9.Text = "Field1";
			this.fldBottomDiscount9.Top = 6F;
			this.fldBottomDiscount9.Width = 1.03125F;
			// 
			// fldBottomDiscount10
			// 
			this.fldBottomDiscount10.CanGrow = false;
			this.fldBottomDiscount10.Height = 0.19F;
			this.fldBottomDiscount10.Left = 5.34375F;
			this.fldBottomDiscount10.MultiLine = false;
			this.fldBottomDiscount10.Name = "fldBottomDiscount10";
			this.fldBottomDiscount10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomDiscount10.Text = "Field1";
			this.fldBottomDiscount10.Top = 6.1875F;
			this.fldBottomDiscount10.Width = 1.03125F;
			// 
			// fldBottomAmount1
			// 
			this.fldBottomAmount1.CanGrow = false;
			this.fldBottomAmount1.Height = 0.19F;
			this.fldBottomAmount1.Left = 6.5F;
			this.fldBottomAmount1.MultiLine = false;
			this.fldBottomAmount1.Name = "fldBottomAmount1";
			this.fldBottomAmount1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount1.Text = "Field1";
			this.fldBottomAmount1.Top = 4.5F;
			this.fldBottomAmount1.Width = 1.03125F;
			// 
			// fldBottomAmount2
			// 
			this.fldBottomAmount2.CanGrow = false;
			this.fldBottomAmount2.Height = 0.19F;
			this.fldBottomAmount2.Left = 6.5F;
			this.fldBottomAmount2.MultiLine = false;
			this.fldBottomAmount2.Name = "fldBottomAmount2";
			this.fldBottomAmount2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount2.Text = "Field1";
			this.fldBottomAmount2.Top = 4.6875F;
			this.fldBottomAmount2.Width = 1.03125F;
			// 
			// fldBottomAmount3
			// 
			this.fldBottomAmount3.CanGrow = false;
			this.fldBottomAmount3.Height = 0.19F;
			this.fldBottomAmount3.Left = 6.5F;
			this.fldBottomAmount3.MultiLine = false;
			this.fldBottomAmount3.Name = "fldBottomAmount3";
			this.fldBottomAmount3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount3.Text = "Field1";
			this.fldBottomAmount3.Top = 4.875F;
			this.fldBottomAmount3.Width = 1.03125F;
			// 
			// fldBottomAmount4
			// 
			this.fldBottomAmount4.CanGrow = false;
			this.fldBottomAmount4.Height = 0.19F;
			this.fldBottomAmount4.Left = 6.5F;
			this.fldBottomAmount4.MultiLine = false;
			this.fldBottomAmount4.Name = "fldBottomAmount4";
			this.fldBottomAmount4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount4.Text = "Field1";
			this.fldBottomAmount4.Top = 5.0625F;
			this.fldBottomAmount4.Width = 1.03125F;
			// 
			// fldBottomAmount5
			// 
			this.fldBottomAmount5.CanGrow = false;
			this.fldBottomAmount5.Height = 0.19F;
			this.fldBottomAmount5.Left = 6.5F;
			this.fldBottomAmount5.MultiLine = false;
			this.fldBottomAmount5.Name = "fldBottomAmount5";
			this.fldBottomAmount5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount5.Text = "Field1";
			this.fldBottomAmount5.Top = 5.25F;
			this.fldBottomAmount5.Width = 1.03125F;
			// 
			// fldBottomAmount6
			// 
			this.fldBottomAmount6.CanGrow = false;
			this.fldBottomAmount6.Height = 0.19F;
			this.fldBottomAmount6.Left = 6.5F;
			this.fldBottomAmount6.MultiLine = false;
			this.fldBottomAmount6.Name = "fldBottomAmount6";
			this.fldBottomAmount6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount6.Text = "Field1";
			this.fldBottomAmount6.Top = 5.4375F;
			this.fldBottomAmount6.Width = 1.03125F;
			// 
			// fldBottomAmount7
			// 
			this.fldBottomAmount7.CanGrow = false;
			this.fldBottomAmount7.Height = 0.19F;
			this.fldBottomAmount7.Left = 6.5F;
			this.fldBottomAmount7.MultiLine = false;
			this.fldBottomAmount7.Name = "fldBottomAmount7";
			this.fldBottomAmount7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount7.Text = "Field1";
			this.fldBottomAmount7.Top = 5.625F;
			this.fldBottomAmount7.Width = 1.03125F;
			// 
			// fldBottomAmount8
			// 
			this.fldBottomAmount8.CanGrow = false;
			this.fldBottomAmount8.Height = 0.19F;
			this.fldBottomAmount8.Left = 6.5F;
			this.fldBottomAmount8.MultiLine = false;
			this.fldBottomAmount8.Name = "fldBottomAmount8";
			this.fldBottomAmount8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount8.Text = "Field1";
			this.fldBottomAmount8.Top = 5.8125F;
			this.fldBottomAmount8.Width = 1.03125F;
			// 
			// fldBottomAmount9
			// 
			this.fldBottomAmount9.CanGrow = false;
			this.fldBottomAmount9.Height = 0.19F;
			this.fldBottomAmount9.Left = 6.5F;
			this.fldBottomAmount9.MultiLine = false;
			this.fldBottomAmount9.Name = "fldBottomAmount9";
			this.fldBottomAmount9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount9.Text = "Field1";
			this.fldBottomAmount9.Top = 6F;
			this.fldBottomAmount9.Width = 1.03125F;
			// 
			// fldBottomAmount10
			// 
			this.fldBottomAmount10.CanGrow = false;
			this.fldBottomAmount10.Height = 0.19F;
			this.fldBottomAmount10.Left = 6.5F;
			this.fldBottomAmount10.MultiLine = false;
			this.fldBottomAmount10.Name = "fldBottomAmount10";
			this.fldBottomAmount10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount10.Text = "Field1";
			this.fldBottomAmount10.Top = 6.1875F;
			this.fldBottomAmount10.Width = 1.03125F;
			// 
			// imgIcon
			// 
			this.imgIcon.Height = 0.96875F;
			this.imgIcon.HyperLink = null;
			this.imgIcon.ImageData = null;
			this.imgIcon.Left = 0.03125F;
			this.imgIcon.LineWeight = 1F;
			this.imgIcon.Name = "imgIcon";
			this.imgIcon.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.imgIcon.Top = 0F;
			this.imgIcon.Width = 0.96875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.59375F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.Label21.Text = "CREDIT";
			this.Label21.Top = 4.28125F;
			this.Label21.Width = 0.625F;
			// 
			// fldBottomCredit1
			// 
			this.fldBottomCredit1.CanGrow = false;
			this.fldBottomCredit1.Height = 0.19F;
			this.fldBottomCredit1.Left = 4.1875F;
			this.fldBottomCredit1.MultiLine = false;
			this.fldBottomCredit1.Name = "fldBottomCredit1";
			this.fldBottomCredit1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit1.Text = "Field1";
			this.fldBottomCredit1.Top = 4.5F;
			this.fldBottomCredit1.Width = 1.03125F;
			// 
			// fldBottomCredit2
			// 
			this.fldBottomCredit2.CanGrow = false;
			this.fldBottomCredit2.Height = 0.19F;
			this.fldBottomCredit2.Left = 4.1875F;
			this.fldBottomCredit2.MultiLine = false;
			this.fldBottomCredit2.Name = "fldBottomCredit2";
			this.fldBottomCredit2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit2.Text = "Field1";
			this.fldBottomCredit2.Top = 4.6875F;
			this.fldBottomCredit2.Width = 1.03125F;
			// 
			// fldBottomCredit3
			// 
			this.fldBottomCredit3.CanGrow = false;
			this.fldBottomCredit3.Height = 0.19F;
			this.fldBottomCredit3.Left = 4.1875F;
			this.fldBottomCredit3.MultiLine = false;
			this.fldBottomCredit3.Name = "fldBottomCredit3";
			this.fldBottomCredit3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit3.Text = "Field1";
			this.fldBottomCredit3.Top = 4.875F;
			this.fldBottomCredit3.Width = 1.03125F;
			// 
			// fldBottomCredit4
			// 
			this.fldBottomCredit4.CanGrow = false;
			this.fldBottomCredit4.Height = 0.19F;
			this.fldBottomCredit4.Left = 4.1875F;
			this.fldBottomCredit4.MultiLine = false;
			this.fldBottomCredit4.Name = "fldBottomCredit4";
			this.fldBottomCredit4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit4.Text = "Field1";
			this.fldBottomCredit4.Top = 5.0625F;
			this.fldBottomCredit4.Width = 1.03125F;
			// 
			// fldBottomCredit5
			// 
			this.fldBottomCredit5.CanGrow = false;
			this.fldBottomCredit5.Height = 0.19F;
			this.fldBottomCredit5.Left = 4.1875F;
			this.fldBottomCredit5.MultiLine = false;
			this.fldBottomCredit5.Name = "fldBottomCredit5";
			this.fldBottomCredit5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit5.Text = "Field1";
			this.fldBottomCredit5.Top = 5.25F;
			this.fldBottomCredit5.Width = 1.03125F;
			// 
			// fldBottomCredit6
			// 
			this.fldBottomCredit6.CanGrow = false;
			this.fldBottomCredit6.Height = 0.19F;
			this.fldBottomCredit6.Left = 4.1875F;
			this.fldBottomCredit6.MultiLine = false;
			this.fldBottomCredit6.Name = "fldBottomCredit6";
			this.fldBottomCredit6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit6.Text = "Field1";
			this.fldBottomCredit6.Top = 5.4375F;
			this.fldBottomCredit6.Width = 1.03125F;
			// 
			// fldBottomCredit7
			// 
			this.fldBottomCredit7.CanGrow = false;
			this.fldBottomCredit7.Height = 0.19F;
			this.fldBottomCredit7.Left = 4.1875F;
			this.fldBottomCredit7.MultiLine = false;
			this.fldBottomCredit7.Name = "fldBottomCredit7";
			this.fldBottomCredit7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit7.Text = "Field1";
			this.fldBottomCredit7.Top = 5.625F;
			this.fldBottomCredit7.Width = 1.03125F;
			// 
			// fldBottomCredit8
			// 
			this.fldBottomCredit8.CanGrow = false;
			this.fldBottomCredit8.Height = 0.19F;
			this.fldBottomCredit8.Left = 4.1875F;
			this.fldBottomCredit8.MultiLine = false;
			this.fldBottomCredit8.Name = "fldBottomCredit8";
			this.fldBottomCredit8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit8.Text = "Field1";
			this.fldBottomCredit8.Top = 5.8125F;
			this.fldBottomCredit8.Width = 1.03125F;
			// 
			// fldBottomCredit9
			// 
			this.fldBottomCredit9.CanGrow = false;
			this.fldBottomCredit9.Height = 0.19F;
			this.fldBottomCredit9.Left = 4.1875F;
			this.fldBottomCredit9.MultiLine = false;
			this.fldBottomCredit9.Name = "fldBottomCredit9";
			this.fldBottomCredit9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit9.Text = "Field1";
			this.fldBottomCredit9.Top = 6F;
			this.fldBottomCredit9.Width = 1.03125F;
			// 
			// fldBottomCredit10
			// 
			this.fldBottomCredit10.CanGrow = false;
			this.fldBottomCredit10.Height = 0.19F;
			this.fldBottomCredit10.Left = 4.1875F;
			this.fldBottomCredit10.MultiLine = false;
			this.fldBottomCredit10.Name = "fldBottomCredit10";
			this.fldBottomCredit10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomCredit10.Text = "Field1";
			this.fldBottomCredit10.Top = 6.1875F;
			this.fldBottomCredit10.Width = 1.03125F;
			// 
			// lblVoid
			// 
			this.lblVoid.Height = 0.65625F;
			this.lblVoid.HyperLink = null;
			this.lblVoid.Left = 5.0625F;
			this.lblVoid.Name = "lblVoid";
			this.lblVoid.Style = "font-family: \'Courier New\'; font-size: 48pt; text-align: center; ddo-char-set: 1";
			this.lblVoid.Text = "VOID";
			this.lblVoid.Top = 1.90625F;
			this.lblVoid.Visible = false;
			this.lblVoid.Width = 1.84375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.53125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label1.Text = "-- A/P CHECK --";
			this.Label1.Top = 7F;
			this.Label1.Width = 1.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label2.Text = "DATE";
			this.Label2.Top = 7F;
			this.Label2.Width = 0.4375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 5.34375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldDate.Text = "Field2";
			this.fldDate.Top = 7F;
			this.fldDate.Width = 0.78125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label3.Text = "PAYEE:";
			this.Label3.Top = 7.34375F;
			this.Label3.Width = 0.59375F;
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.19F;
			this.fldVendor.Left = 2.28125F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldVendor.Text = "Field2";
			this.fldVendor.Top = 7.34375F;
			this.fldVendor.Width = 3.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.28125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label4.Text = "CHECK";
			this.Label4.Top = 7.34375F;
			this.Label4.Width = 0.59375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.19F;
			this.fldCheck.Left = 6.90625F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldCheck.Text = "Field2";
			this.fldCheck.Top = 7.34375F;
			this.fldCheck.Width = 0.625F;
			// 
			// lblWarrant
			// 
			this.lblWarrant.Height = 0.1875F;
			this.lblWarrant.HyperLink = null;
			this.lblWarrant.Left = 0.09375F;
			this.lblWarrant.Name = "lblWarrant";
			this.lblWarrant.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblWarrant.Text = "WRNT";
			this.lblWarrant.Top = 7.59375F;
			this.lblWarrant.Width = 0.46875F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 0.65625F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblDescription.Text = "DESCRIPTION----------";
			this.lblDescription.Top = 7.59375F;
			this.lblDescription.Width = 1.65625F;
			// 
			// lblReference
			// 
			this.lblReference.Height = 0.1875F;
			this.lblReference.HyperLink = null;
			this.lblReference.Left = 2.8125F;
			this.lblReference.Name = "lblReference";
			this.lblReference.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblReference.Text = "REFERENCE-----";
			this.lblReference.Top = 7.59375F;
			this.lblReference.Width = 1.1875F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.1875F;
			this.lblDiscount.HyperLink = null;
			this.lblDiscount.Left = 5.75F;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.lblDiscount.Text = "-DISC-";
			this.lblDiscount.Top = 7.59375F;
			this.lblDiscount.Width = 0.625F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.75F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.lblAmount.Text = "-AMOUNT-";
			this.lblAmount.Top = 7.59375F;
			this.lblAmount.Width = 0.78125F;
			// 
			// fldMuniName
			// 
			this.fldMuniName.Height = 0.1875F;
			this.fldMuniName.Left = 1.84375F;
			this.fldMuniName.Name = "fldMuniName";
			this.fldMuniName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldMuniName.Text = "Field2";
			this.fldMuniName.Top = 6.40625F;
			this.fldMuniName.Width = 1.1875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.5625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Label10.Text = "AMOUNT-";
			this.Label10.Top = 6.40625F;
			this.Label10.Width = 0.59375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 4.15625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmount.Text = "1234567890123";
			this.fldAmount.Top = 6.40625F;
			this.fldAmount.Width = 1.125F;
			// 
			// fldWarrant1
			// 
			this.fldWarrant1.CanGrow = false;
			this.fldWarrant1.Height = 0.19F;
			this.fldWarrant1.Left = 0.09375F;
			this.fldWarrant1.MultiLine = false;
			this.fldWarrant1.Name = "fldWarrant1";
			this.fldWarrant1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant1.Text = "Field1";
			this.fldWarrant1.Top = 7.8125F;
			this.fldWarrant1.Width = 0.46875F;
			// 
			// fldWarrant2
			// 
			this.fldWarrant2.CanGrow = false;
			this.fldWarrant2.Height = 0.19F;
			this.fldWarrant2.Left = 0.09375F;
			this.fldWarrant2.MultiLine = false;
			this.fldWarrant2.Name = "fldWarrant2";
			this.fldWarrant2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant2.Text = "Field1";
			this.fldWarrant2.Top = 8F;
			this.fldWarrant2.Width = 0.46875F;
			// 
			// fldWarrant3
			// 
			this.fldWarrant3.CanGrow = false;
			this.fldWarrant3.Height = 0.19F;
			this.fldWarrant3.Left = 0.09375F;
			this.fldWarrant3.MultiLine = false;
			this.fldWarrant3.Name = "fldWarrant3";
			this.fldWarrant3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant3.Text = "Field1";
			this.fldWarrant3.Top = 8.1875F;
			this.fldWarrant3.Width = 0.46875F;
			// 
			// fldWarrant4
			// 
			this.fldWarrant4.CanGrow = false;
			this.fldWarrant4.Height = 0.19F;
			this.fldWarrant4.Left = 0.09375F;
			this.fldWarrant4.MultiLine = false;
			this.fldWarrant4.Name = "fldWarrant4";
			this.fldWarrant4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant4.Text = "Field1";
			this.fldWarrant4.Top = 8.375F;
			this.fldWarrant4.Width = 0.46875F;
			// 
			// fldWarrant5
			// 
			this.fldWarrant5.CanGrow = false;
			this.fldWarrant5.Height = 0.19F;
			this.fldWarrant5.Left = 0.09375F;
			this.fldWarrant5.MultiLine = false;
			this.fldWarrant5.Name = "fldWarrant5";
			this.fldWarrant5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant5.Text = "Field1";
			this.fldWarrant5.Top = 8.5625F;
			this.fldWarrant5.Width = 0.46875F;
			// 
			// fldWarrant6
			// 
			this.fldWarrant6.CanGrow = false;
			this.fldWarrant6.Height = 0.19F;
			this.fldWarrant6.Left = 0.09375F;
			this.fldWarrant6.MultiLine = false;
			this.fldWarrant6.Name = "fldWarrant6";
			this.fldWarrant6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant6.Text = "Field1";
			this.fldWarrant6.Top = 8.75F;
			this.fldWarrant6.Width = 0.46875F;
			// 
			// fldWarrant7
			// 
			this.fldWarrant7.CanGrow = false;
			this.fldWarrant7.Height = 0.19F;
			this.fldWarrant7.Left = 0.09375F;
			this.fldWarrant7.MultiLine = false;
			this.fldWarrant7.Name = "fldWarrant7";
			this.fldWarrant7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant7.Text = "Field1";
			this.fldWarrant7.Top = 8.9375F;
			this.fldWarrant7.Width = 0.46875F;
			// 
			// fldWarrant8
			// 
			this.fldWarrant8.CanGrow = false;
			this.fldWarrant8.Height = 0.19F;
			this.fldWarrant8.Left = 0.09375F;
			this.fldWarrant8.MultiLine = false;
			this.fldWarrant8.Name = "fldWarrant8";
			this.fldWarrant8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant8.Text = "Field1";
			this.fldWarrant8.Top = 9.125F;
			this.fldWarrant8.Width = 0.46875F;
			// 
			// fldWarrant9
			// 
			this.fldWarrant9.CanGrow = false;
			this.fldWarrant9.Height = 0.19F;
			this.fldWarrant9.Left = 0.09375F;
			this.fldWarrant9.MultiLine = false;
			this.fldWarrant9.Name = "fldWarrant9";
			this.fldWarrant9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant9.Text = "Field1";
			this.fldWarrant9.Top = 9.3125F;
			this.fldWarrant9.Width = 0.46875F;
			// 
			// fldWarrant10
			// 
			this.fldWarrant10.CanGrow = false;
			this.fldWarrant10.Height = 0.19F;
			this.fldWarrant10.Left = 0.09375F;
			this.fldWarrant10.MultiLine = false;
			this.fldWarrant10.Name = "fldWarrant10";
			this.fldWarrant10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant10.Text = "Field1";
			this.fldWarrant10.Top = 9.5F;
			this.fldWarrant10.Width = 0.46875F;
			// 
			// fldDescription1
			// 
			this.fldDescription1.CanGrow = false;
			this.fldDescription1.Height = 0.19F;
			this.fldDescription1.Left = 0.65625F;
			this.fldDescription1.MultiLine = false;
			this.fldDescription1.Name = "fldDescription1";
			this.fldDescription1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription1.Text = "Field1";
			this.fldDescription1.Top = 7.8125F;
			this.fldDescription1.Width = 2F;
			// 
			// fldDescription2
			// 
			this.fldDescription2.CanGrow = false;
			this.fldDescription2.Height = 0.19F;
			this.fldDescription2.Left = 0.65625F;
			this.fldDescription2.MultiLine = false;
			this.fldDescription2.Name = "fldDescription2";
			this.fldDescription2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription2.Text = "Field1";
			this.fldDescription2.Top = 8F;
			this.fldDescription2.Width = 2F;
			// 
			// fldDescription3
			// 
			this.fldDescription3.CanGrow = false;
			this.fldDescription3.Height = 0.19F;
			this.fldDescription3.Left = 0.65625F;
			this.fldDescription3.MultiLine = false;
			this.fldDescription3.Name = "fldDescription3";
			this.fldDescription3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription3.Text = "Field1";
			this.fldDescription3.Top = 8.1875F;
			this.fldDescription3.Width = 2F;
			// 
			// fldDescription4
			// 
			this.fldDescription4.CanGrow = false;
			this.fldDescription4.Height = 0.19F;
			this.fldDescription4.Left = 0.65625F;
			this.fldDescription4.MultiLine = false;
			this.fldDescription4.Name = "fldDescription4";
			this.fldDescription4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription4.Text = "Field1";
			this.fldDescription4.Top = 8.375F;
			this.fldDescription4.Width = 2F;
			// 
			// fldDescription5
			// 
			this.fldDescription5.CanGrow = false;
			this.fldDescription5.Height = 0.19F;
			this.fldDescription5.Left = 0.65625F;
			this.fldDescription5.MultiLine = false;
			this.fldDescription5.Name = "fldDescription5";
			this.fldDescription5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription5.Text = "Field1";
			this.fldDescription5.Top = 8.5625F;
			this.fldDescription5.Width = 2F;
			// 
			// fldDescription6
			// 
			this.fldDescription6.CanGrow = false;
			this.fldDescription6.Height = 0.19F;
			this.fldDescription6.Left = 0.65625F;
			this.fldDescription6.MultiLine = false;
			this.fldDescription6.Name = "fldDescription6";
			this.fldDescription6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription6.Text = "Field1";
			this.fldDescription6.Top = 8.75F;
			this.fldDescription6.Width = 2F;
			// 
			// fldDescription7
			// 
			this.fldDescription7.CanGrow = false;
			this.fldDescription7.Height = 0.19F;
			this.fldDescription7.Left = 0.65625F;
			this.fldDescription7.MultiLine = false;
			this.fldDescription7.Name = "fldDescription7";
			this.fldDescription7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription7.Text = "Field1";
			this.fldDescription7.Top = 8.9375F;
			this.fldDescription7.Width = 2F;
			// 
			// fldDescription8
			// 
			this.fldDescription8.CanGrow = false;
			this.fldDescription8.Height = 0.19F;
			this.fldDescription8.Left = 0.65625F;
			this.fldDescription8.MultiLine = false;
			this.fldDescription8.Name = "fldDescription8";
			this.fldDescription8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription8.Text = "Field1";
			this.fldDescription8.Top = 9.125F;
			this.fldDescription8.Width = 2F;
			// 
			// fldDescription9
			// 
			this.fldDescription9.CanGrow = false;
			this.fldDescription9.Height = 0.19F;
			this.fldDescription9.Left = 0.65625F;
			this.fldDescription9.MultiLine = false;
			this.fldDescription9.Name = "fldDescription9";
			this.fldDescription9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription9.Text = "Field1";
			this.fldDescription9.Top = 9.3125F;
			this.fldDescription9.Width = 2F;
			// 
			// fldDescription10
			// 
			this.fldDescription10.CanGrow = false;
			this.fldDescription10.Height = 0.19F;
			this.fldDescription10.Left = 0.65625F;
			this.fldDescription10.MultiLine = false;
			this.fldDescription10.Name = "fldDescription10";
			this.fldDescription10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription10.Text = "Field1";
			this.fldDescription10.Top = 9.5F;
			this.fldDescription10.Width = 2F;
			// 
			// fldReference1
			// 
			this.fldReference1.CanGrow = false;
			this.fldReference1.Height = 0.19F;
			this.fldReference1.Left = 2.78125F;
			this.fldReference1.MultiLine = false;
			this.fldReference1.Name = "fldReference1";
			this.fldReference1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference1.Text = "Field1";
			this.fldReference1.Top = 7.8125F;
			this.fldReference1.Width = 1.34375F;
			// 
			// fldReference2
			// 
			this.fldReference2.CanGrow = false;
			this.fldReference2.Height = 0.19F;
			this.fldReference2.Left = 2.78125F;
			this.fldReference2.MultiLine = false;
			this.fldReference2.Name = "fldReference2";
			this.fldReference2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference2.Text = "Field1";
			this.fldReference2.Top = 8F;
			this.fldReference2.Width = 1.34375F;
			// 
			// fldReference3
			// 
			this.fldReference3.CanGrow = false;
			this.fldReference3.Height = 0.19F;
			this.fldReference3.Left = 2.78125F;
			this.fldReference3.MultiLine = false;
			this.fldReference3.Name = "fldReference3";
			this.fldReference3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference3.Text = "Field1";
			this.fldReference3.Top = 8.1875F;
			this.fldReference3.Width = 1.34375F;
			// 
			// fldReference4
			// 
			this.fldReference4.CanGrow = false;
			this.fldReference4.Height = 0.19F;
			this.fldReference4.Left = 2.78125F;
			this.fldReference4.MultiLine = false;
			this.fldReference4.Name = "fldReference4";
			this.fldReference4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference4.Text = "Field1";
			this.fldReference4.Top = 8.375F;
			this.fldReference4.Width = 1.34375F;
			// 
			// fldReference5
			// 
			this.fldReference5.CanGrow = false;
			this.fldReference5.Height = 0.19F;
			this.fldReference5.Left = 2.78125F;
			this.fldReference5.MultiLine = false;
			this.fldReference5.Name = "fldReference5";
			this.fldReference5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference5.Text = "Field1";
			this.fldReference5.Top = 8.5625F;
			this.fldReference5.Width = 1.34375F;
			// 
			// fldReference6
			// 
			this.fldReference6.CanGrow = false;
			this.fldReference6.Height = 0.19F;
			this.fldReference6.Left = 2.78125F;
			this.fldReference6.MultiLine = false;
			this.fldReference6.Name = "fldReference6";
			this.fldReference6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference6.Text = "Field1";
			this.fldReference6.Top = 8.75F;
			this.fldReference6.Width = 1.34375F;
			// 
			// fldReference7
			// 
			this.fldReference7.CanGrow = false;
			this.fldReference7.Height = 0.19F;
			this.fldReference7.Left = 2.78125F;
			this.fldReference7.MultiLine = false;
			this.fldReference7.Name = "fldReference7";
			this.fldReference7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference7.Text = "Field1";
			this.fldReference7.Top = 8.9375F;
			this.fldReference7.Width = 1.34375F;
			// 
			// fldReference8
			// 
			this.fldReference8.CanGrow = false;
			this.fldReference8.Height = 0.19F;
			this.fldReference8.Left = 2.78125F;
			this.fldReference8.MultiLine = false;
			this.fldReference8.Name = "fldReference8";
			this.fldReference8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference8.Text = "Field1";
			this.fldReference8.Top = 9.125F;
			this.fldReference8.Width = 1.34375F;
			// 
			// fldReference9
			// 
			this.fldReference9.CanGrow = false;
			this.fldReference9.Height = 0.19F;
			this.fldReference9.Left = 2.78125F;
			this.fldReference9.MultiLine = false;
			this.fldReference9.Name = "fldReference9";
			this.fldReference9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference9.Text = "Field1";
			this.fldReference9.Top = 9.3125F;
			this.fldReference9.Width = 1.34375F;
			// 
			// fldReference10
			// 
			this.fldReference10.CanGrow = false;
			this.fldReference10.Height = 0.19F;
			this.fldReference10.Left = 2.78125F;
			this.fldReference10.MultiLine = false;
			this.fldReference10.Name = "fldReference10";
			this.fldReference10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference10.Text = "Field1";
			this.fldReference10.Top = 9.5F;
			this.fldReference10.Width = 1.34375F;
			// 
			// fldDiscount1
			// 
			this.fldDiscount1.CanGrow = false;
			this.fldDiscount1.Height = 0.19F;
			this.fldDiscount1.Left = 5.34375F;
			this.fldDiscount1.MultiLine = false;
			this.fldDiscount1.Name = "fldDiscount1";
			this.fldDiscount1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount1.Text = "Field1";
			this.fldDiscount1.Top = 7.8125F;
			this.fldDiscount1.Width = 1.03125F;
			// 
			// fldDiscount2
			// 
			this.fldDiscount2.CanGrow = false;
			this.fldDiscount2.Height = 0.19F;
			this.fldDiscount2.Left = 5.34375F;
			this.fldDiscount2.MultiLine = false;
			this.fldDiscount2.Name = "fldDiscount2";
			this.fldDiscount2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount2.Text = "Field1";
			this.fldDiscount2.Top = 8F;
			this.fldDiscount2.Width = 1.03125F;
			// 
			// fldDiscount3
			// 
			this.fldDiscount3.CanGrow = false;
			this.fldDiscount3.Height = 0.19F;
			this.fldDiscount3.Left = 5.34375F;
			this.fldDiscount3.MultiLine = false;
			this.fldDiscount3.Name = "fldDiscount3";
			this.fldDiscount3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount3.Text = "Field1";
			this.fldDiscount3.Top = 8.1875F;
			this.fldDiscount3.Width = 1.03125F;
			// 
			// fldDiscount4
			// 
			this.fldDiscount4.CanGrow = false;
			this.fldDiscount4.Height = 0.19F;
			this.fldDiscount4.Left = 5.34375F;
			this.fldDiscount4.MultiLine = false;
			this.fldDiscount4.Name = "fldDiscount4";
			this.fldDiscount4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount4.Text = "Field1";
			this.fldDiscount4.Top = 8.375F;
			this.fldDiscount4.Width = 1.03125F;
			// 
			// fldDiscount5
			// 
			this.fldDiscount5.CanGrow = false;
			this.fldDiscount5.Height = 0.19F;
			this.fldDiscount5.Left = 5.34375F;
			this.fldDiscount5.MultiLine = false;
			this.fldDiscount5.Name = "fldDiscount5";
			this.fldDiscount5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount5.Text = "Field1";
			this.fldDiscount5.Top = 8.5625F;
			this.fldDiscount5.Width = 1.03125F;
			// 
			// fldDiscount6
			// 
			this.fldDiscount6.CanGrow = false;
			this.fldDiscount6.Height = 0.19F;
			this.fldDiscount6.Left = 5.34375F;
			this.fldDiscount6.MultiLine = false;
			this.fldDiscount6.Name = "fldDiscount6";
			this.fldDiscount6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount6.Text = "Field1";
			this.fldDiscount6.Top = 8.75F;
			this.fldDiscount6.Width = 1.03125F;
			// 
			// fldDiscount7
			// 
			this.fldDiscount7.CanGrow = false;
			this.fldDiscount7.Height = 0.19F;
			this.fldDiscount7.Left = 5.34375F;
			this.fldDiscount7.MultiLine = false;
			this.fldDiscount7.Name = "fldDiscount7";
			this.fldDiscount7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount7.Text = "Field1";
			this.fldDiscount7.Top = 8.9375F;
			this.fldDiscount7.Width = 1.03125F;
			// 
			// fldDiscount8
			// 
			this.fldDiscount8.CanGrow = false;
			this.fldDiscount8.Height = 0.19F;
			this.fldDiscount8.Left = 5.34375F;
			this.fldDiscount8.MultiLine = false;
			this.fldDiscount8.Name = "fldDiscount8";
			this.fldDiscount8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount8.Text = "Field1";
			this.fldDiscount8.Top = 9.125F;
			this.fldDiscount8.Width = 1.03125F;
			// 
			// fldDiscount9
			// 
			this.fldDiscount9.CanGrow = false;
			this.fldDiscount9.Height = 0.19F;
			this.fldDiscount9.Left = 5.34375F;
			this.fldDiscount9.MultiLine = false;
			this.fldDiscount9.Name = "fldDiscount9";
			this.fldDiscount9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount9.Text = "Field1";
			this.fldDiscount9.Top = 9.3125F;
			this.fldDiscount9.Width = 1.03125F;
			// 
			// fldDiscount10
			// 
			this.fldDiscount10.CanGrow = false;
			this.fldDiscount10.Height = 0.19F;
			this.fldDiscount10.Left = 5.34375F;
			this.fldDiscount10.MultiLine = false;
			this.fldDiscount10.Name = "fldDiscount10";
			this.fldDiscount10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount10.Text = "Field1";
			this.fldDiscount10.Top = 9.5F;
			this.fldDiscount10.Width = 1.03125F;
			// 
			// fldAmt1
			// 
			this.fldAmt1.CanGrow = false;
			this.fldAmt1.Height = 0.19F;
			this.fldAmt1.Left = 6.5F;
			this.fldAmt1.MultiLine = false;
			this.fldAmt1.Name = "fldAmt1";
			this.fldAmt1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt1.Text = "Field1";
			this.fldAmt1.Top = 7.8125F;
			this.fldAmt1.Width = 1.03125F;
			// 
			// fldAmt2
			// 
			this.fldAmt2.CanGrow = false;
			this.fldAmt2.Height = 0.19F;
			this.fldAmt2.Left = 6.5F;
			this.fldAmt2.MultiLine = false;
			this.fldAmt2.Name = "fldAmt2";
			this.fldAmt2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt2.Text = "Field1";
			this.fldAmt2.Top = 8F;
			this.fldAmt2.Width = 1.03125F;
			// 
			// fldAmt3
			// 
			this.fldAmt3.CanGrow = false;
			this.fldAmt3.Height = 0.19F;
			this.fldAmt3.Left = 6.5F;
			this.fldAmt3.MultiLine = false;
			this.fldAmt3.Name = "fldAmt3";
			this.fldAmt3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt3.Text = "Field1";
			this.fldAmt3.Top = 8.1875F;
			this.fldAmt3.Width = 1.03125F;
			// 
			// fldAmt4
			// 
			this.fldAmt4.CanGrow = false;
			this.fldAmt4.Height = 0.19F;
			this.fldAmt4.Left = 6.5F;
			this.fldAmt4.MultiLine = false;
			this.fldAmt4.Name = "fldAmt4";
			this.fldAmt4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt4.Text = "Field1";
			this.fldAmt4.Top = 8.375F;
			this.fldAmt4.Width = 1.03125F;
			// 
			// fldAmt5
			// 
			this.fldAmt5.CanGrow = false;
			this.fldAmt5.Height = 0.19F;
			this.fldAmt5.Left = 6.5F;
			this.fldAmt5.MultiLine = false;
			this.fldAmt5.Name = "fldAmt5";
			this.fldAmt5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt5.Text = "Field1";
			this.fldAmt5.Top = 8.5625F;
			this.fldAmt5.Width = 1.03125F;
			// 
			// fldAmt6
			// 
			this.fldAmt6.CanGrow = false;
			this.fldAmt6.Height = 0.19F;
			this.fldAmt6.Left = 6.5F;
			this.fldAmt6.MultiLine = false;
			this.fldAmt6.Name = "fldAmt6";
			this.fldAmt6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt6.Text = "Field1";
			this.fldAmt6.Top = 8.75F;
			this.fldAmt6.Width = 1.03125F;
			// 
			// fldAmt7
			// 
			this.fldAmt7.CanGrow = false;
			this.fldAmt7.Height = 0.19F;
			this.fldAmt7.Left = 6.5F;
			this.fldAmt7.MultiLine = false;
			this.fldAmt7.Name = "fldAmt7";
			this.fldAmt7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt7.Text = "Field1";
			this.fldAmt7.Top = 8.9375F;
			this.fldAmt7.Width = 1.03125F;
			// 
			// fldAmt8
			// 
			this.fldAmt8.CanGrow = false;
			this.fldAmt8.Height = 0.19F;
			this.fldAmt8.Left = 6.5F;
			this.fldAmt8.MultiLine = false;
			this.fldAmt8.Name = "fldAmt8";
			this.fldAmt8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt8.Text = "Field1";
			this.fldAmt8.Top = 9.125F;
			this.fldAmt8.Width = 1.03125F;
			// 
			// fldAmt9
			// 
			this.fldAmt9.CanGrow = false;
			this.fldAmt9.Height = 0.19F;
			this.fldAmt9.Left = 6.5F;
			this.fldAmt9.MultiLine = false;
			this.fldAmt9.Name = "fldAmt9";
			this.fldAmt9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt9.Text = "Field1";
			this.fldAmt9.Top = 9.3125F;
			this.fldAmt9.Width = 1.03125F;
			// 
			// fldAmt10
			// 
			this.fldAmt10.CanGrow = false;
			this.fldAmt10.Height = 0.19F;
			this.fldAmt10.Left = 6.5F;
			this.fldAmt10.MultiLine = false;
			this.fldAmt10.Name = "fldAmt10";
			this.fldAmt10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt10.Text = "Field1";
			this.fldAmt10.Top = 9.5F;
			this.fldAmt10.Width = 1.03125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.59375F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.Label20.Text = "CREDIT";
			this.Label20.Top = 7.59375F;
			this.Label20.Width = 0.625F;
			// 
			// fldCredit1
			// 
			this.fldCredit1.CanGrow = false;
			this.fldCredit1.Height = 0.19F;
			this.fldCredit1.Left = 4.1875F;
			this.fldCredit1.MultiLine = false;
			this.fldCredit1.Name = "fldCredit1";
			this.fldCredit1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit1.Text = "Field1";
			this.fldCredit1.Top = 7.8125F;
			this.fldCredit1.Width = 1.03125F;
			// 
			// fldCredit2
			// 
			this.fldCredit2.CanGrow = false;
			this.fldCredit2.Height = 0.19F;
			this.fldCredit2.Left = 4.1875F;
			this.fldCredit2.MultiLine = false;
			this.fldCredit2.Name = "fldCredit2";
			this.fldCredit2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit2.Text = "Field1";
			this.fldCredit2.Top = 8F;
			this.fldCredit2.Width = 1.03125F;
			// 
			// fldCredit3
			// 
			this.fldCredit3.CanGrow = false;
			this.fldCredit3.Height = 0.19F;
			this.fldCredit3.Left = 4.1875F;
			this.fldCredit3.MultiLine = false;
			this.fldCredit3.Name = "fldCredit3";
			this.fldCredit3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit3.Text = "Field1";
			this.fldCredit3.Top = 8.1875F;
			this.fldCredit3.Width = 1.03125F;
			// 
			// fldCredit4
			// 
			this.fldCredit4.CanGrow = false;
			this.fldCredit4.Height = 0.19F;
			this.fldCredit4.Left = 4.1875F;
			this.fldCredit4.MultiLine = false;
			this.fldCredit4.Name = "fldCredit4";
			this.fldCredit4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit4.Text = "Field1";
			this.fldCredit4.Top = 8.375F;
			this.fldCredit4.Width = 1.03125F;
			// 
			// fldCredit5
			// 
			this.fldCredit5.CanGrow = false;
			this.fldCredit5.Height = 0.19F;
			this.fldCredit5.Left = 4.1875F;
			this.fldCredit5.MultiLine = false;
			this.fldCredit5.Name = "fldCredit5";
			this.fldCredit5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit5.Text = "Field1";
			this.fldCredit5.Top = 8.5625F;
			this.fldCredit5.Width = 1.03125F;
			// 
			// fldCredit6
			// 
			this.fldCredit6.CanGrow = false;
			this.fldCredit6.Height = 0.19F;
			this.fldCredit6.Left = 4.1875F;
			this.fldCredit6.MultiLine = false;
			this.fldCredit6.Name = "fldCredit6";
			this.fldCredit6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit6.Text = "Field1";
			this.fldCredit6.Top = 8.75F;
			this.fldCredit6.Width = 1.03125F;
			// 
			// fldCredit7
			// 
			this.fldCredit7.CanGrow = false;
			this.fldCredit7.Height = 0.19F;
			this.fldCredit7.Left = 4.1875F;
			this.fldCredit7.MultiLine = false;
			this.fldCredit7.Name = "fldCredit7";
			this.fldCredit7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit7.Text = "Field1";
			this.fldCredit7.Top = 8.9375F;
			this.fldCredit7.Width = 1.03125F;
			// 
			// fldCredit8
			// 
			this.fldCredit8.CanGrow = false;
			this.fldCredit8.Height = 0.19F;
			this.fldCredit8.Left = 4.1875F;
			this.fldCredit8.MultiLine = false;
			this.fldCredit8.Name = "fldCredit8";
			this.fldCredit8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit8.Text = "Field1";
			this.fldCredit8.Top = 9.125F;
			this.fldCredit8.Width = 1.03125F;
			// 
			// fldCredit9
			// 
			this.fldCredit9.CanGrow = false;
			this.fldCredit9.Height = 0.19F;
			this.fldCredit9.Left = 4.1875F;
			this.fldCredit9.MultiLine = false;
			this.fldCredit9.Name = "fldCredit9";
			this.fldCredit9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit9.Text = "Field1";
			this.fldCredit9.Top = 9.3125F;
			this.fldCredit9.Width = 1.03125F;
			// 
			// fldCredit10
			// 
			this.fldCredit10.CanGrow = false;
			this.fldCredit10.Height = 0.19F;
			this.fldCredit10.Left = 4.1875F;
			this.fldCredit10.MultiLine = false;
			this.fldCredit10.Name = "fldCredit10";
			this.fldCredit10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit10.Text = "Field1";
			this.fldCredit10.Top = 9.5F;
			this.fldCredit10.Width = 1.03125F;
			// 
			// Image1
			// 
			this.Image1.Height = 0.5625F;
			this.Image1.HyperLink = null;
			this.Image1.ImageData = null;
			this.Image1.Left = 5.23125F;
			this.Image1.LineWeight = 1F;
			this.Image1.Name = "Image1";
			this.Image1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.Image1.Top = 2.416667F;
			this.Image1.Width = 2.4375F;
			// 
			// fldCheckMessage
			// 
			this.fldCheckMessage.Height = 0.19F;
			this.fldCheckMessage.Left = 0.0625F;
			this.fldCheckMessage.Name = "fldCheckMessage";
			this.fldCheckMessage.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldCheckMessage.Text = "happy";
			this.fldCheckMessage.Top = 3.84375F;
			this.fldCheckMessage.Width = 7.3125F;
			// 
			// fldBottomMuniName
			// 
			this.fldBottomMuniName.Height = 0.1875F;
			this.fldBottomMuniName.Left = 1.84375F;
			this.fldBottomMuniName.Name = "fldBottomMuniName";
			this.fldBottomMuniName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldBottomMuniName.Text = "Field2";
			this.fldBottomMuniName.Top = 9.71875F;
			this.fldBottomMuniName.Width = 1.1875F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.5625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.Label22.Text = "AMOUNT-";
			this.Label22.Top = 9.71875F;
			this.Label22.Width = 0.59375F;
			// 
			// fldBottomAmount
			// 
			this.fldBottomAmount.Height = 0.1875F;
			this.fldBottomAmount.Left = 4.15625F;
			this.fldBottomAmount.Name = "fldBottomAmount";
			this.fldBottomAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldBottomAmount.Text = "1234567890123";
			this.fldBottomAmount.Top = 9.71875F;
			this.fldBottomAmount.Width = 1.125F;
			// 
			// fldBottomCheckMessage
			// 
			this.fldBottomCheckMessage.Height = 0.19F;
			this.fldBottomCheckMessage.Left = 0.0625F;
			this.fldBottomCheckMessage.Name = "fldBottomCheckMessage";
			this.fldBottomCheckMessage.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBottomCheckMessage.Text = "happy";
			this.fldBottomCheckMessage.Top = 7.15625F;
			this.fldBottomCheckMessage.Width = 7.3125F;
			// 
			// rptLaserChecksTop
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.3F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.3F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.947917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomWarrant10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDescription10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomReference10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomDiscount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCredit10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTextAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTextAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomVendor;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomWarrant10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDescription10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomReference10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomDiscount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount10;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgIcon;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCredit10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVoid;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReference;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit10;
		private GrapeCity.ActiveReports.SectionReportModel.Picture Image1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
