//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJDataEntry.
	/// </summary>
	partial class frmGJDataEntry : BaseForm
	{
		public fecherFoundation.FCFrame fraDefaultType;
		public fecherFoundation.FCTextBox txtTypeDescription;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCComboBox cboDefaultType;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCTextBox txtPeriod;
		public fecherFoundation.FCComboBox cboJournal;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblReadOnly;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblNet;
		public fecherFoundation.FCLabel lblNetTotal;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGJDataEntry));
			this.fraDefaultType = new fecherFoundation.FCFrame();
			this.txtTypeDescription = new fecherFoundation.FCTextBox();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cboDefaultType = new fecherFoundation.FCComboBox();
			this.fraJournalSave = new fecherFoundation.FCFrame();
			this.cboSaveJournal = new fecherFoundation.FCComboBox();
			this.cmdOKSave = new fecherFoundation.FCButton();
			this.cmdCancelSave = new fecherFoundation.FCButton();
			this.txtJournalDescription = new fecherFoundation.FCTextBox();
			this.lblSaveInstructions = new fecherFoundation.FCLabel();
			this.lblJournalSave = new fecherFoundation.FCLabel();
			this.lblJournalDescription = new fecherFoundation.FCLabel();
			this.txtPeriod = new fecherFoundation.FCTextBox();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblReadOnly = new fecherFoundation.FCLabel();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblNet = new fecherFoundation.FCLabel();
			this.lblNetTotal = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnFileSaveType = new fecherFoundation.FCButton();
			this.btnFileLoadType = new fecherFoundation.FCButton();
			this.btnProcessDelete = new fecherFoundation.FCButton();
			this.btnFileSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultType)).BeginInit();
			this.fraDefaultType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
			this.fraJournalSave.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSaveType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileLoadType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 720);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraDefaultType);
			this.ClientArea.Controls.Add(this.fraJournalSave);
			this.ClientArea.Controls.Add(this.txtPeriod);
			this.ClientArea.Controls.Add(this.cboJournal);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblReadOnly);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblJournal);
			this.ClientArea.Controls.Add(this.lblNet);
			this.ClientArea.Controls.Add(this.lblNetTotal);
			this.ClientArea.Size = new System.Drawing.Size(1078, 660);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessDelete);
			this.TopPanel.Controls.Add(this.btnFileLoadType);
			this.TopPanel.Controls.Add(this.btnFileSaveType);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileSaveType, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileLoadType, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(187, 30);
			this.HeaderText.Text = "General Journal";
			// 
			// fraDefaultType
			// 
			this.fraDefaultType.BackColor = System.Drawing.Color.White;
			this.fraDefaultType.Controls.Add(this.txtTypeDescription);
			this.fraDefaultType.Controls.Add(this.cmdCancel);
			this.fraDefaultType.Controls.Add(this.cmdProcess);
			this.fraDefaultType.Controls.Add(this.cboDefaultType);
			this.fraDefaultType.Location = new System.Drawing.Point(1062, 30);
			this.fraDefaultType.Name = "fraDefaultType";
			this.fraDefaultType.Size = new System.Drawing.Size(475, 136);
			this.fraDefaultType.TabIndex = 17;
			this.fraDefaultType.Text = "Select Default Type";
			this.fraDefaultType.Visible = false;
			// 
			// txtTypeDescription
			// 
			this.txtTypeDescription.AutoSize = false;
			this.txtTypeDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtTypeDescription.LinkItem = null;
			this.txtTypeDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTypeDescription.LinkTopic = null;
			this.txtTypeDescription.Location = new System.Drawing.Point(20, 30);
			this.txtTypeDescription.Name = "txtTypeDescription";
			this.txtTypeDescription.Size = new System.Drawing.Size(435, 40);
			this.txtTypeDescription.TabIndex = 21;
			this.txtTypeDescription.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(121, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(80, 40);
			this.cmdCancel.TabIndex = 20;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "actionButton";
			this.cmdProcess.ForeColor = System.Drawing.Color.White;
			this.cmdProcess.Location = new System.Drawing.Point(20, 90);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(87, 40);
			this.cmdProcess.TabIndex = 19;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// cboDefaultType
			// 
			this.cboDefaultType.AutoSize = false;
			this.cboDefaultType.BackColor = System.Drawing.SystemColors.Window;
			this.cboDefaultType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDefaultType.FormattingEnabled = true;
			this.cboDefaultType.Location = new System.Drawing.Point(20, 30);
			this.cboDefaultType.Name = "cboDefaultType";
			this.cboDefaultType.Size = new System.Drawing.Size(435, 40);
			this.cboDefaultType.Sorted = true;
			this.cboDefaultType.TabIndex = 18;
			// 
			// fraJournalSave
			// 
			this.fraJournalSave.BackColor = System.Drawing.Color.White;
			this.fraJournalSave.Controls.Add(this.cboSaveJournal);
			this.fraJournalSave.Controls.Add(this.cmdOKSave);
			this.fraJournalSave.Controls.Add(this.cmdCancelSave);
			this.fraJournalSave.Controls.Add(this.txtJournalDescription);
			this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
			this.fraJournalSave.Controls.Add(this.lblJournalSave);
			this.fraJournalSave.Controls.Add(this.lblJournalDescription);
			this.fraJournalSave.Location = new System.Drawing.Point(249, 200);
			this.fraJournalSave.Name = "fraJournalSave";
			this.fraJournalSave.Size = new System.Drawing.Size(826, 230);
			this.fraJournalSave.TabIndex = 7;
			this.fraJournalSave.Text = "Save Journal";
			this.fraJournalSave.Visible = false;
			// 
			// cboSaveJournal
			// 
			this.cboSaveJournal.AutoSize = false;
			this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboSaveJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSaveJournal.FormattingEnabled = true;
			this.cboSaveJournal.Location = new System.Drawing.Point(166, 70);
			this.cboSaveJournal.Name = "cboSaveJournal";
			this.cboSaveJournal.Size = new System.Drawing.Size(417, 40);
			this.cboSaveJournal.TabIndex = 11;
			this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
			this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
			//FC:FINAL:MSH - Issue #760: Handler, which check value of 'SelectedIndex' when drop-down portion will be closed. 'SelectedIndexChanged' will invoke only when selected another value from comboBox, not current.
			this.cboSaveJournal.DropDownClosed += new EventHandler(this.cboSaveJournal_DropDownClosed);
			// 
			// cmdOKSave
			// 
			this.cmdOKSave.AppearanceKey = "actionButton";
			this.cmdOKSave.ForeColor = System.Drawing.Color.White;
			this.cmdOKSave.Location = new System.Drawing.Point(324, 186);
			this.cmdOKSave.Name = "cmdOKSave";
			this.cmdOKSave.Size = new System.Drawing.Size(67, 40);
			this.cmdOKSave.TabIndex = 9;
			this.cmdOKSave.Text = "OK";
			this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
			// 
			// cmdCancelSave
			// 
			this.cmdCancelSave.AppearanceKey = "actionButton";
			this.cmdCancelSave.ForeColor = System.Drawing.Color.White;
			this.cmdCancelSave.Location = new System.Drawing.Point(399, 186);
			this.cmdCancelSave.Name = "cmdCancelSave";
			this.cmdCancelSave.Size = new System.Drawing.Size(69, 40);
			this.cmdCancelSave.TabIndex = 10;
			this.cmdCancelSave.Text = "Cancel";
			this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
			// 
			// txtJournalDescription
			// 
			this.txtJournalDescription.AutoSize = false;
			this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournalDescription.LinkItem = null;
			this.txtJournalDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtJournalDescription.LinkTopic = null;
			this.txtJournalDescription.Location = new System.Drawing.Point(166, 126);
			this.txtJournalDescription.MaxLength = 100;
			this.txtJournalDescription.Name = "txtJournalDescription";
			this.txtJournalDescription.Size = new System.Drawing.Size(417, 40);
			this.txtJournalDescription.TabIndex = 8;
			// 
			// lblSaveInstructions
			// 
			this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
			this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblSaveInstructions.Name = "lblSaveInstructions";
			this.lblSaveInstructions.Size = new System.Drawing.Size(784, 16);
			this.lblSaveInstructions.TabIndex = 14;
			this.lblSaveInstructions.Text = "PLEASE SELECT THE JOURNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION F" + "OR THE JOURNAL, AND CLICK THE OK BUTTON";
			// 
			// lblJournalSave
			// 
			this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
			this.lblJournalSave.Name = "lblJournalSave";
			this.lblJournalSave.Size = new System.Drawing.Size(62, 16);
			this.lblJournalSave.TabIndex = 13;
			this.lblJournalSave.Text = "JOURNAL";
			// 
			// lblJournalDescription
			// 
			this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalDescription.Location = new System.Drawing.Point(20, 140);
			this.lblJournalDescription.Name = "lblJournalDescription";
			this.lblJournalDescription.Size = new System.Drawing.Size(79, 16);
			this.lblJournalDescription.TabIndex = 12;
			this.lblJournalDescription.Text = "DESCRIPTION";
			// 
			// txtPeriod
			// 
			this.txtPeriod.AutoSize = false;
			this.txtPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.txtPeriod.LinkItem = null;
			this.txtPeriod.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPeriod.LinkTopic = null;
			this.txtPeriod.Location = new System.Drawing.Point(223, 66);
			this.txtPeriod.MaxLength = 2;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Size = new System.Drawing.Size(54, 40);
			this.txtPeriod.TabIndex = 2;
			this.txtPeriod.Enter += new System.EventHandler(this.txtPeriod_Enter);
			this.txtPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.txtPeriod_Validating);
			// 
			// cboJournal
			// 
			this.cboJournal.AutoSize = false;
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournal.Enabled = false;
			this.cboJournal.FormattingEnabled = true;
			this.cboJournal.Location = new System.Drawing.Point(491, 66);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(91, 40);
			this.cboJournal.TabIndex = 4;
			this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
			this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
			this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 1;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 162);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 16;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1018, 313);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 6;
			//FC:FINAL:MSH - Issue #756: KeyDownEdit doesn't work. In VB6 this event handles pressing of Keys in EditControl of the table
			//this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vs1_MouseMoveEvent);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// lblReadOnly
			// 
			this.lblReadOnly.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblReadOnly.Location = new System.Drawing.Point(30, 30);
			this.lblReadOnly.Name = "lblReadOnly";
			this.lblReadOnly.Size = new System.Drawing.Size(136, 16);
			this.lblReadOnly.TabIndex = 0;
			this.lblReadOnly.Text = "VIEW ONLY";
			this.lblReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblReadOnly.Visible = false;
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 126);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(583, 16);
			this.lblExpense.TabIndex = 5;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(30, 80);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(136, 16);
			this.lblPeriod.TabIndex = 1;
			this.lblPeriod.Text = "ACCOUNTING PERIOD";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(347, 78);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(82, 16);
			this.lblJournal.TabIndex = 3;
			this.lblJournal.Text = "JOURNAL NO";
			// 
			// lblNet
			// 
			this.lblNet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblNet.Location = new System.Drawing.Point(182, 495);
			this.lblNet.Name = "lblNet";
			this.lblNet.Size = new System.Drawing.Size(106, 16);
			this.lblNet.TabIndex = 3;
			this.lblNet.Text = "= NET ENTRY";
			// 
			// lblNetTotal
			// 
			this.lblNetTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblNetTotal.Location = new System.Drawing.Point(68, 495);
			this.lblNetTotal.Name = "lblNetTotal";
			this.lblNetTotal.Size = new System.Drawing.Size(110, 16);
			this.lblNetTotal.TabIndex = 0;
			this.lblNetTotal.Text = "$0.00";
			this.lblNetTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnFileSaveType
			// 
			this.btnFileSaveType.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileSaveType.AppearanceKey = "toolbarButton";
			this.btnFileSaveType.Location = new System.Drawing.Point(849, 28);
			this.btnFileSaveType.Name = "btnFileSaveType";
			this.btnFileSaveType.Shortcut = Wisej.Web.Shortcut.F6;
			this.btnFileSaveType.Size = new System.Drawing.Size(192, 24);
			this.btnFileSaveType.TabIndex = 2;
			this.btnFileSaveType.Text = "Save as Default Journal Type";
			this.btnFileSaveType.Click += new System.EventHandler(this.mnuFileSaveType_Click);
			// 
			// btnFileLoadType
			// 
			this.btnFileLoadType.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileLoadType.AppearanceKey = "toolbarButton";
			this.btnFileLoadType.Location = new System.Drawing.Point(668, 28);
			this.btnFileLoadType.Name = "btnFileLoadType";
			this.btnFileLoadType.Shortcut = Wisej.Web.Shortcut.F5;
			this.btnFileLoadType.Size = new System.Drawing.Size(176, 24);
			this.btnFileLoadType.TabIndex = 1;
			this.btnFileLoadType.Text = "Load Default Journal Type";
			this.btnFileLoadType.Click += new System.EventHandler(this.mnuFileLoadType_Click);
			// 
			// btnProcessDelete
			// 
			this.btnProcessDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessDelete.AppearanceKey = "toolbarButton";
			this.btnProcessDelete.Location = new System.Drawing.Point(573, 28);
			this.btnProcessDelete.Name = "btnProcessDelete";
			this.btnProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
			this.btnProcessDelete.Size = new System.Drawing.Size(91, 24);
			this.btnProcessDelete.TabIndex = 0;
			this.btnProcessDelete.Text = "Delete Entry";
			this.btnProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// btnFileSave
			// 
			this.btnFileSave.AppearanceKey = "acceptButton";
			this.btnFileSave.Location = new System.Drawing.Point(421, 30);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileSave.Size = new System.Drawing.Size(74, 48);
			this.btnFileSave.TabIndex = 0;
			this.btnFileSave.Text = "Save";
			//FC:FINAL:MSH - Issue #818: replaced to function with posting functionality
			//this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			this.btnFileSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmGJDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 828);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGJDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "General Journal";
			this.Load += new System.EventHandler(this.frmGJDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmGJDataEntry_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormClosed += FrmGJDataEntry_FormClosed;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGJDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGJDataEntry_KeyPress);
			this.Resize += new System.EventHandler(this.frmGJDataEntry_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDefaultType)).EndInit();
			this.fraDefaultType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
			this.fraJournalSave.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSaveType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileLoadType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnFileSaveType;
		internal FCButton btnFileLoadType;
		private FCButton btnProcessDelete;
		private FCButton btnFileSave;
	}
}