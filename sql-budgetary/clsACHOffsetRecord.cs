﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class clsACHOffsetRecord
	{
		//=========================================================
		private string strLastError = string.Empty;
		private string strAccountType = string.Empty;
		private string strEmployerAccount = string.Empty;
		private string strEmployerRT = string.Empty;
		private double dblTotalAmount;
		private string strEmployerID = string.Empty;
		private string strEmployerName = string.Empty;
		private int intAddendaIndicator;
		private string strOriginatingDFI = string.Empty;
		private bool boolPreNote;
		private int intRecordNumber;

		public string LastError
		{
			set
			{
				strLastError = value;
			}
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public string AccountType
		{
			set
			{
				strAccountType = value;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}

		public string EmployerAccount
		{
			set
			{
				strEmployerAccount = value;
			}
			get
			{
				string EmployerAccount = "";
				EmployerAccount = strEmployerAccount;
				return EmployerAccount;
			}
		}

		public string EmployerRT
		{
			set
			{
				strEmployerRT = value;
			}
			get
			{
				string EmployerRT = "";
				EmployerRT = strEmployerRT;
				return EmployerRT;
			}
		}

		public double TotalAmount
		{
			set
			{
				dblTotalAmount = value;
			}
			get
			{
				double TotalAmount = 0;
				TotalAmount = dblTotalAmount;
				return TotalAmount;
			}
		}

		public string EmployerID
		{
			set
			{
				strEmployerID = value;
			}
			get
			{
				string EmployerID = "";
				EmployerID = strEmployerID;
				return EmployerID;
			}
		}

		public string Name
		{
			set
			{
				strEmployerName = value;
			}
			get
			{
				string Name = "";
				Name = strEmployerName;
				return Name;
			}
		}

		public short AddendaIndicator
		{
			set
			{
				intAddendaIndicator = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short AddendaIndicator = 0;
				AddendaIndicator = FCConvert.ToInt16(intAddendaIndicator);
				return AddendaIndicator;
			}
		}

		public string OriginatingDFI
		{
			set
			{
				strOriginatingDFI = value;
			}
			get
			{
				string OriginatingDFI = "";
				OriginatingDFI = strOriginatingDFI;
				return OriginatingDFI;
			}
		}

		public bool PreNote
		{
			set
			{
				boolPreNote = value;
			}
			get
			{
				bool PreNote = false;
				PreNote = boolPreNote;
				return PreNote;
			}
		}

		public short VendorNumber
		{
			set
			{
				intRecordNumber = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short VendorNumber = 0;
				VendorNumber = FCConvert.ToInt16(intRecordNumber);
				return VendorNumber;
			}
		}

		public string RecordType
		{
			get
			{
				string RecordType = "";
				RecordType = "6";
				return RecordType;
			}
		}

		public string TransactionByAccountType()
		{
			string TransactionByAccountType = "";
			strLastError = "";
			string strReturn = "";
			if (Strings.LCase(Strings.Left(strAccountType + " ", 1)) == "2")
			{
				strReturn = "2";
			}
			else if (Strings.LCase(Strings.Left(strAccountType + " ", 1)) == "3")
			{
				strReturn = "3";
			}
			else if (Strings.LCase(Strings.Left(strAccountType + " ", 1)) == "5")
			{
				strReturn = "5";
			}
			else
			{
				strLastError = "Invalid account type.";
			}
			if (!boolPreNote)
			{
				TransactionByAccountType = strReturn + "7";
			}
			else
			{
				TransactionByAccountType = strReturn + "8";
			}
			return TransactionByAccountType;
		}

		public int HashNumber
		{
			get
			{
				int HashNumber = 0;
				HashNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(Strings.Right(Strings.StrDup(9, "0") + strEmployerRT, 9), 8))));
				return HashNumber;
			}
		}

		public string TraceNumber
		{
			get
			{
				string TraceNumber = "";
				string strTemp;
				strTemp = Strings.Left(strOriginatingDFI + Strings.StrDup(8, "0"), 8);
				strTemp += Strings.Right(Strings.StrDup(7, "0") + FCConvert.ToString(intRecordNumber), 7);
				TraceNumber = strTemp;
				return TraceNumber;
			}
		}

		public bool CheckData()
		{
			bool CheckData = false;
			if (strEmployerName == "")
			{
				strLastError = "Name is blank.";
				return CheckData;
			}
			if (strEmployerRT == "")
			{
				strLastError = "RT is blank.";
				return CheckData;
			}
			if (intRecordNumber <= 0)
			{
				strLastError = "The trace number is invalid.";
				return CheckData;
			}
			if (strEmployerAccount == "")
			{
				strLastError = "The account number is blank.";
				return CheckData;
			}
			if (strAccountType == "")
			{
				strLastError = "The account type is blank.";
				return CheckData;
			}
			if (strEmployerID == "")
			{
				strLastError = "The identifier is blank.";
				return CheckData;
			}
			CheckData = true;
			return CheckData;
		}

		public string OutputLineByRec(int intRecNum)
		{
			string OutputLineByRec = "";
			intRecordNumber = intRecNum;
			OutputLineByRec = OutputLine();
			return OutputLineByRec;
		}

		public string OutputLine()
		{
			string OutputLine = "";
			strLastError = "";
			OutputLine = "";
			string strLine = "";
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (CheckData())
				{
					strLine = RecordType;
					strTemp = TransactionByAccountType();
					if (strTemp != "")
					{
						strLine += strTemp;
					}
					else
					{
						return OutputLine;
					}
					strLine += Strings.Right(Strings.StrDup(9, "0") + strEmployerRT, 9);
					strTemp = Strings.Left(strEmployerAccount + Strings.StrDup(17, " "), 17);
					strLine += strTemp;
					strTemp = Strings.Right(Strings.StrDup(10, "0") + FCConvert.ToString(dblTotalAmount * 100), 10);
					strLine += strTemp;
					strTemp = Strings.Left(strEmployerID + Strings.StrDup(15, " "), 15);
					strLine += strTemp;
					strTemp = Strings.Left(strEmployerName + Strings.StrDup(22, " "), 22);
					strLine += strTemp;
					strLine += Strings.StrDup(2, " ");
					strLine += FCConvert.ToString(intAddendaIndicator);
					strLine += TraceNumber;
					OutputLine = strLine;
				}
				else
				{
					strLastError = "Could not build offset record." + "\r\n" + strLastError;
					return OutputLine;
				}
				return OutputLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = "Could not build offset record." + "\r\n" + Information.Err(ex).Description;
			}
			return OutputLine;
		}
	}
}
