﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class modCustomReport
	{
		// vbPorter upgrade warning: GRIDTEXT As short --> As int	OnRead(string)
		public const int GRIDTEXT = 1;
		// vbPorter upgrade warning: GRIDDATE As short --> As int	OnRead(string)
		public const int GRIDDATE = 2;
		// vbPorter upgrade warning: GRIDCOMBOIDTEXT As short --> As int	OnRead(string)
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		// vbPorter upgrade warning: GRIDCOMBOTEXT As short --> As int	OnRead(string)
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const string CUSTOMREPORTDATABASE = "Twbd0000.vb1";
		// vbPorter upgrade warning: strValue As string	OnWrite(string, VB.TextBox)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{

			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		public static void GetNumberOfFields(string strSql)
		{

			string[] strTemp = null;
			object strTemp2;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSql) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSql, "from", -1, CompareConstants.vbBinaryCompare);
			strSql = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSql, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound((object[])strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS_6(string strFieldName, short intSegment = 2)
		{
			return CheckForAS(strFieldName, intSegment);
		}

		public static object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 50; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmJournalListing, frmJournalAccountReports, frmVendorDetail, frmBudgetAdjustments, frmExpRevSummary, frmCustomBudgetReport)
		public static void SetFormFieldCaptions(dynamic FormName, string ReportType)
		{
			// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
			// 
			// ****************************************************************
			// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
			// TO ADD A NEW REPORT TYPE.
			// ****************************************************************
			Statics.strReportType = Strings.UCase(ReportType);
			// CLEAR THE COMBO LIST ARRAY
			ClearComboListArray();
			if (Strings.UCase(ReportType) == "CUSTOMBUDGET")
			{
				SetCustomBudgetParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "EXPREVSUM")
			{
				SetExpRevSumParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "JOURNALLIST")
			{
				SetJournalListParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "JOURNALACCOUNTSUMMARY")
			{
				SetJournalAccountSummaryParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "JOURNALACCOUNTDETAIL")
			{
				SetJournalAccountDetailParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "VENDORDETAIL")
			{
				SetVendorDetailParameters(FormName);
			}
			else if (Strings.UCase(ReportType) == "BUDGETADJUSTMENTS")
			{
				SetBudgetAdjustmentsParameters(FormName);
			}
			// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
			LoadSortList(ref FormName);
			// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
			LoadBudgetWhereGrid(ref FormName);
			// LOAD THE SAVED REPORT COMBO ON THE FORM
			FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetCustomBudgetParameters(dynamic FormName)
		{
			int counter;
			string strTemp;
			Statics.strCustomTitle = "**** Custom Budget Report ****";
			FormName.lstFields.AddItem("3 Years Ago Budget");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("3 Years Ago Actual");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("2 Years Ago Budget");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("2 Years Ago Actual");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("1 Year Ago Budget");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("1 Year Ago Actual");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Current Year Budget");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Current Year YTD");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Current Year Balance");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem("Initial Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem("Manager Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem("Committee Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem("Elected Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem("Approved Amounts");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem("Init Req VS Cur Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem("Man Req VS Cur Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem("Comm Req VS Cur Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Elec Req VS Cur Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("App Amt VS Cur Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Init Req VS Cur Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Man Req VS Cur Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Comm Req VS Cur Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("Elec Req VS Cur Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("App Amt VS Cur Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Init Req VS Last Yr Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Man Req VS Last Yr Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Comm Req VS Last Yr Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			FormName.lstFields.AddItem("Elec Req VS Last Yr Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			FormName.lstFields.AddItem("App Amt VS Last Yr Bud $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 28);
			FormName.lstFields.AddItem("Init Req VS Last Yr Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 29);
			FormName.lstFields.AddItem("Man Req VS Last Yr Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 30);
			FormName.lstFields.AddItem("Comm Req VS Last Yr Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 31);
			FormName.lstFields.AddItem("Elec Req VS Last Yr Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 32);
			FormName.lstFields.AddItem("App Amt VS Last Yr Bud %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 33);
			FormName.lstFields.AddItem("Blank Initial Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 34);
			FormName.lstFields.AddItem("Blank Manager Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 35);
			FormName.lstFields.AddItem("Blank Committee Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 36);
			FormName.lstFields.AddItem("Blank Elected Request");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 37);
			FormName.lstFields.AddItem("Blank Approved Amounts");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 38);
			FormName.lstFields.AddItem("3 Yrs VS 2 Yrs Ago Act $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 39);
			FormName.lstFields.AddItem("3 Yrs VS 2 Yrs Ago Act %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 40);
			FormName.lstFields.AddItem("2 Yrs VS 1 Yr Ago Act $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 41);
			FormName.lstFields.AddItem("2 Yrs VS 1 Yr Ago Act %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 42);
			FormName.lstFields.AddItem("1 Yr Ago VS Current Act $");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 43);
			FormName.lstFields.AddItem("1 Yr Ago VS Current Act %");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 44);
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			FormName.Text1.Text = "Please enter a choice for each selection criteria." + "\r\n" + "\r\n" + "You may choose up to 9 fields to display on this report.  When you have made all your selections click the Print Report option from the File menu to print the report." + "\r\n" + "\r\n" + "You may clear all of your selection criteria by double clicking on the Selection Criteria label.";
			LoadGridCellAsCombo(FormName, 0, "#0;T" + "\t" + "Town");
			LoadGridCellAsCombo(FormName, 1, "#0;B" + "\t" + "Both|#1;E" + "\t" + "Expense|#2;R" + "\t" + "Revenue");
			LoadGridCellAsCombo(FormName, 2, "#0;A" + "\t" + "All|#1;D" + "\t" + "Department Range|#2;S" + "\t" + "Single Department|#3;F" + "\t" + "Single Fund");
			LoadGridCellAsCombo_1701(FormName, 3, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'", "ShortDescription", "Department", true);
			strTemp = "";
			for (counter = DateTime.Today.Year - 4; counter <= DateTime.Today.Year + 4; counter++)
			{
				strTemp += FCConvert.ToString(counter) + "|";
			}
			strTemp = Strings.Left(strTemp, strTemp.Length - 1);
			LoadGridCellAsCombo(FormName, 4, strTemp);
			LoadGridCellAsCombo(FormName, 5, strTemp);
			LoadGridCellAsCombo(FormName, 6, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 7, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 8, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 9, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 10, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 11, "#0;B" + "\t" + "Print Below|#1;S" + "\t" + "Print on Same Line|#2;P" + "\t" + "Print Below Across Entire Page|#3;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 13, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 15, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			if (modAccountTitle.Statics.ExpDivFlag && modAccountTitle.Statics.ObjFlag)
			{
				LoadGridCellAsCombo(FormName, 16, "#0;De" + "\t" + "Department|#1;Ex" + "\t" + "Expense");
			}
			else if (modAccountTitle.Statics.ExpDivFlag)
			{
				LoadGridCellAsCombo(FormName, 16, "#0;De" + "\t" + "Department|#1;Ex" + "\t" + "Expense|#2;Ob" + "\t" + "Object");
			}
			else if (modAccountTitle.Statics.ObjFlag)
			{
				LoadGridCellAsCombo(FormName, 16, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Ex" + "\t" + "Expense");
			}
			else
			{
				LoadGridCellAsCombo(FormName, 16, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Ex" + "\t" + "Expense|#3;Ob" + "\t" + "Object");
			}
			if (modAccountTitle.Statics.RevDivFlag)
			{
				LoadGridCellAsCombo(FormName, 17, "#0;De" + "\t" + "Department|#1;Re" + "\t" + "Revenue");
			}
			else
			{
				LoadGridCellAsCombo(FormName, 17, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Re" + "\t" + "Revenue");
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetExpRevSumParameters(dynamic FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Exp / Rev Report ****";
			FormName.lstFields.AddItem("Current Budget");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("Current Month Net");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("YTD Net");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Balance");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Percent Spent");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDCOMBOTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;A" + "\t" + "All|#1;D" + "\t" + "Department Range|#2;SD" + "\t" + "Single Department|#3;SF" + "\t" + "Single Fund|#4;F" + "\t" + "Fund Range|#5;FD" + "\t" + "Departments in a Single Fund");
			LoadGridCellAsCombo_1701(FormName, 1, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'", "ShortDescription", "Department", true);
			LoadGridCellAsCombo(FormName, 2, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Ex" + "\t" + "Expense / Revenue|#3;Ob" + "\t" + "Object / Revenue");
			LoadGridCellAsCombo(FormName, 3, "#0;A" + "\t" + "All|#1;M" + "\t" + "Month Range|#2;S" + "\t" + "Single Month");
			LoadGridCellAsCombo(FormName, 4, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
			LoadGridCellAsCombo(FormName, 5, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 6, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			LoadGridCellAsCombo(FormName, 7, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
			if (modAccountTitle.Statics.ExpDivFlag || modAccountTitle.Statics.RevDivFlag)
			{
				LoadGridCellAsCombo(FormName, 8, "#0;De" + "\t" + "Department|#1;Fu" + "\t" + "Fund");
			}
			else
			{
				LoadGridCellAsCombo(FormName, 8, "#0;De" + "\t" + "Department|#1;Di" + "\t" + "Division|#2;Fu" + "\t" + "Fund");
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetJournalListParameters(dynamic FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Journal Summary List ****";
			FormName.lstFields.AddItem("AP Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("CR Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("CD Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("GJ Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("PY Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Enc Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;A" + "\t" + "All|#1;M" + "\t" + "Month Range|#2;S" + "\t" + "Single Month");
			LoadGridCellAsCombo(FormName, 1, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
			LoadGridCellAsCombo(FormName, 2, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetJournalAccountSummaryParameters(dynamic FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Journal Account Summary ****";
			FormName.lstFields.AddItem("AP Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("CR Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("CD Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("GJ Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("PY Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Enc Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;A" + "\t" + "All|#1;M" + "\t" + "Month Range|#2;S" + "\t" + "Single Month|#3;D" + "\t" + "Date Range");
			LoadGridCellAsCombo(FormName, 1, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetJournalAccountDetailParameters(dynamic FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Journal Account Detail ****";
			FormName.lstFields.AddItem("AP Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			FormName.lstFields.AddItem("CR Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("CD Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("GJ Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("PY Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Enc Journals");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;A" + "\t" + "All|#1;M" + "\t" + "Month Range|#2;S" + "\t" + "Single Month|#3;D" + "\t" + "Date Range");
			LoadGridCellAsCombo(FormName, 1, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetVendorDetailParameters(Form FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Vendor Detail ****";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;1" + "\t" + "Single Vendor|#1;2" + "\t" + "All Vendors by Name|#2;3" + "\t" + "All Vendors by Vendor Number|#3;4" + "\t" + "All Vendors by Accounting Number / Posted Date|#4;5" + "\t" + "All Vendors by Accounting Number / Vendor Name|#5;6" + "\t" + "All 1099 Eligible Vendors by Name");
			LoadGridCellAsCombo(FormName, 1, "#0;A" + "\t" + "All|#1;R" + "\t" + "Range of Accounts|#2;S" + "\t" + "Single Account");
			LoadGridCellAsCombo(FormName, 2, "#0;A" + "\t" + "All|#1;M" + "\t" + "Month Range|#2;S" + "\t" + "Single Month|#3;D" + "\t" + "Date Range");
			LoadGridCellAsCombo(FormName, 3, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
			LoadGridCellAsCombo(FormName, 4, "#0;Y" + "\t" + "Yes|#1;N" + "\t" + "No");
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetBudgetAdjustmentsParameters(dynamic FormName)
		{
			int counter;
			string strTemp = "";
			Statics.strCustomTitle = "**** Budget Adjustments ****";
			Statics.strWhereType[0] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[1] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDCOMBOIDTEXT);
			LoadGridCellAsCombo(FormName, 0, "#0;A" + "\t" + "All Accounts|#1;E" + "\t" + "Town Expense Accounts|#2;R" + "\t" + "Town Revenue Accounts|#3;G" + "\t" + "Town G/L Accounts|#4;P" + "\t" + "School Expense Accounts|#5;V" + "\t" + "School Revenue Accounts|#6;L" + "\t" + "School G/L Accounts");
			LoadGridCellAsCombo(FormName, 1, "#0;A" + "\t" + "All Departments|#1;R" + "\t" + "Range of Departments|#2;S" + "\t" + "Single Department");
			LoadGridCellAsCombo_1701(FormName, 2, "Select * from DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'", "ShortDescription", "Department", true);
		}
		
		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (Strings.UCase(Statics.strReportType))
			{

				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}

		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (Strings.UCase(Statics.strReportType) == "DOGS")
			{
			}
			else if (Strings.UCase(Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadSortList(ref dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			for (intCounter = 0; intCounter <= FormName.lstFields.Items.Count - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.lstFields.List(intCounter));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, FormName.lstFields.ItemData(intCounter));
			}
		}

		private static void LoadWhereGrid(ref dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int lngWid;
			int intCounter;
			for (intCounter = 0; intCounter <= FormName.lstFields.Items.Count - 1; intCounter++)
			{
				FormName.vsWhere.AddItem(FormName.lstFields.List(intCounter));
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
				{
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			lngWid = FormName.vsWhere.Width;
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, lngWid * 0.35);
			FormName.vsWhere.ColWidth(1, lngWid * 0.3);
			FormName.vsWhere.ColWidth(2, lngWid * 0.3);
			FormName.vsWhere.ColWidth(3, 0);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form, frmCustomBudgetReport, frmJournalAccountReports, frmVendorDetail, frmExpRevSummary)
		public static void LoadGridCellAsCombo_1701(Form FormName, int intRowNumber, string strSql, string FieldName = "", string IDField = "", bool ShowMore = false)
		{
			LoadGridCellAsCombo(FormName, intRowNumber, strSql, FieldName, IDField, "", ShowMore);
		}

		public static void LoadGridCellAsCombo(Form FormName, int intRowNumber, string strSql, string FieldName = "", string IDField = "", string DatabaseFieldName = "", bool ShowMore = false)
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSql;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSql, modGlobal.DEFAULTDATABASE);
				if (!ShowMore)
				{
					Statics.strComboList[intRowNumber, 0] = "";
					Statics.strComboList[intRowNumber, 1] = "";
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "'" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
						Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
						rsCombo.MoveNext();
					}
				}
				else
				{
					Statics.strComboList[intRowNumber, 0] = "";
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "'" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(IDField) + "\t" + rsCombo.Get_Fields(FieldName);
						rsCombo.MoveNext();
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadBudgetWhereGrid(ref dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			string vbPorterVar = FormName.Name;
			if (vbPorterVar == "frmCustomBudgetReport")
			{
				FormName.vsWhere.AddItem("Account Type(s)");
				FormName.vsWhere.AddItem("");
				FormName.vsWhere.AddItem("Reporting Range");
				FormName.vsWhere.AddItem("Department(s)");
				FormName.vsWhere.AddItem("Current Year");
				FormName.vsWhere.AddItem("Budget Year");
				FormName.vsWhere.AddItem("Dept Page Break");
				FormName.vsWhere.AddItem("Div Page Break");
				FormName.vsWhere.AddItem("Department Totals");
				FormName.vsWhere.AddItem("Division Totals");
				FormName.vsWhere.AddItem("Expense Totals");
				FormName.vsWhere.AddItem("Comments");
				FormName.vsWhere.AddItem("Report Title");
				FormName.vsWhere.AddItem("Include Budget Adjustments");
				FormName.vsWhere.AddItem("Starting Page");
				FormName.vsWhere.AddItem("Include Carry Forwards");
				FormName.vsWhere.AddItem("Exp Lowest Detail Level");
				FormName.vsWhere.AddItem("Rev Lowest Detail Level");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 2)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			else if (vbPorterVar == "frmExpRevSummary")
			{
				FormName.vsWhere.Rows = 0;
				FormName.vsWhere.AddItem("Reporting Range");
				FormName.vsWhere.AddItem("Department(s)");
				FormName.vsWhere.AddItem("Lowest Level");
				FormName.vsWhere.AddItem("Date Range");
				FormName.vsWhere.AddItem("Month(s)");
				FormName.vsWhere.AddItem("Include Encumbrances in YTD Totals");
				FormName.vsWhere.AddItem("Include Pending Activity in YTD Totals");
				FormName.vsWhere.AddItem("Dept Page Break");
				FormName.vsWhere.AddItem("Show P/L By");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 1 || intCounter == 4)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			else if (vbPorterVar == "frmJournalListing")
			{
				FormName.vsWhere.AddItem("Date Range");
				FormName.vsWhere.AddItem("Month(s)");
				FormName.vsWhere.AddItem("Show OF Detail Information");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 1)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			else if (vbPorterVar == "frmJournalAccountReports")
			{
				FormName.vsWhere.AddItem("Date Range");
				FormName.vsWhere.AddItem("Month(s)");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 1)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			else if (vbPorterVar == "frmVendorDetail")
			{
				FormName.vsWhere.AddItem("Report Type");
				FormName.vsWhere.AddItem("Account Range");
				FormName.vsWhere.AddItem("Date Range");
				FormName.vsWhere.AddItem("Month(s)");
				FormName.vsWhere.AddItem("Show OS Encumbrances");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 3 || intCounter == 1)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			else if (vbPorterVar == "frmBudgetAdjustments")
			{
				FormName.vsWhere.AddItem("Account Type");
				FormName.vsWhere.AddItem("Department Range");
				FormName.vsWhere.AddItem("Department(s)");
				for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
				{
					// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
					// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
					// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
					if (intCounter == 2)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
					if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE)
					{
						FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					}
				}
			}
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, 5100);
			FormName.vsWhere.ColWidth(1, 2500);
			FormName.vsWhere.ColWidth(2, 2500);
			FormName.vsWhere.ColWidth(3, 0);
		}

		public class StaticVariables
		{
			//=========================================================
			public int intNumberOfSQLFields;

			public string strCustomSQL = string.Empty;

			public string[] strFields = new string[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = string.Empty;
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			// vbPorter upgrade warning: strWhereType As string	OnWriteFCConvert.ToInt32(
			public string[] strWhereType = new string[50 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
