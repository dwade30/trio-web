﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCustomBudgetExpenseReport.
	/// </summary>
	partial class rptCustomBudgetExpenseReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomBudgetExpenseReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.vsHeadings = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTitle1Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle3Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle4Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle5Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle5Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle5Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle6Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle6Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle6Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle7Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle7Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle7Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle8Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle8Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle8Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle9Line1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle9Line2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle9Line3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDeptDivTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpenseTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldComments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetail9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpenseName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptDivTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpenseTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpenseName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldDetail1,
				this.fldComments,
				this.fldDetail2,
				this.fldDetail3,
				this.fldDetail4,
				this.fldDetail5,
				this.fldDetail6,
				this.fldDetail7,
				this.fldDetail8,
				this.fldDetail9
			});
			this.Detail.Height = 0.3854167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.vsHeadings,
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label14,
				this.Line2,
				this.lblTitle1Line1,
				this.lblTitle1Line2,
				this.lblTitle1Line3,
				this.lblTitle2Line1,
				this.lblTitle2Line2,
				this.lblTitle2Line3,
				this.lblTitle3Line1,
				this.lblTitle3Line2,
				this.lblTitle3Line3,
				this.lblTitle4Line1,
				this.lblTitle4Line2,
				this.lblTitle4Line3,
				this.lblTitle5Line1,
				this.lblTitle5Line2,
				this.lblTitle5Line3,
				this.lblTitle6Line1,
				this.lblTitle6Line2,
				this.lblTitle6Line3,
				this.lblTitle7Line1,
				this.lblTitle7Line2,
				this.lblTitle7Line3,
				this.lblTitle8Line1,
				this.lblTitle8Line2,
				this.lblTitle8Line3,
				this.lblTitle9Line1,
				this.lblTitle9Line2,
				this.lblTitle9Line3
			});
			this.PageHeader.Height = 1.364583F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDeptDivTitle,
				this.Field1
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.3020833F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDivisionName,
				this.fldDepartmentName,
				this.fldDivTotal1,
				this.fldDeptTotal1,
				this.fldDivTotal2,
				this.fldDeptTotal2,
				this.fldDivTotal3,
				this.fldDeptTotal3,
				this.fldDivTotal4,
				this.fldDeptTotal4,
				this.fldDivTotal5,
				this.fldDeptTotal5,
				this.fldDivTotal6,
				this.fldDivTotal7,
				this.fldDivTotal8,
				this.fldDivTotal9,
				this.fldDeptTotal6,
				this.fldDeptTotal7,
				this.fldDeptTotal8,
				this.fldDeptTotal9,
				this.fldTotal7,
				this.fldTotalName,
				this.Line1,
				this.fldTotal1,
				this.fldTotal2,
				this.fldTotal3,
				this.fldTotal4,
				this.fldTotal5,
				this.fldTotal6,
				this.fldTotal8,
				this.fldTotal9
			});
			this.GroupFooter1.Height = 0.6458333F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.CanShrink = true;
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldExpenseTitle,
				this.Field2
			});
			this.GroupHeader2.DataField = "ExpBinder";
			this.GroupHeader2.Height = 0.1979167F;
			this.GroupHeader2.KeepTogether = true;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.CanShrink = true;
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldExpenseName,
				this.fldExpTotal1,
				this.fldExpTotal2,
				this.fldExpTotal3,
				this.fldExpTotal4,
				this.fldExpTotal5,
				this.fldExpTotal6,
				this.fldExpTotal7,
				this.fldExpTotal8,
				this.fldExpTotal9
			});
			this.GroupFooter2.Height = 0.1875F;
			this.GroupFooter2.KeepTogether = true;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// vsHeadings
			// 
			this.vsHeadings.Height = 0.65625F;
			this.vsHeadings.Left = 2.5625F;
			this.vsHeadings.Name = "vsHeadings";
			this.vsHeadings.Top = 0F;
			this.vsHeadings.Visible = false;
			this.vsHeadings.Width = 4.96875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Custom Budget Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.21875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label14.Text = "Expense";
			this.Label14.Top = 0.375F;
			this.Label14.Width = 7.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.34375F;
			this.Line2.Width = 7.5625F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5625F;
			this.Line2.Y1 = 1.34375F;
			this.Line2.Y2 = 1.34375F;
			// 
			// lblTitle1Line1
			// 
			this.lblTitle1Line1.Height = 0.1875F;
			this.lblTitle1Line1.HyperLink = null;
			this.lblTitle1Line1.Left = 0.3125F;
			this.lblTitle1Line1.Name = "lblTitle1Line1";
			this.lblTitle1Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle1Line1.Text = "Label15";
			this.lblTitle1Line1.Top = 0.78125F;
			this.lblTitle1Line1.Visible = false;
			this.lblTitle1Line1.Width = 1F;
			// 
			// lblTitle1Line2
			// 
			this.lblTitle1Line2.Height = 0.1875F;
			this.lblTitle1Line2.HyperLink = null;
			this.lblTitle1Line2.Left = 0.3125F;
			this.lblTitle1Line2.Name = "lblTitle1Line2";
			this.lblTitle1Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle1Line2.Text = "Label16";
			this.lblTitle1Line2.Top = 0.96875F;
			this.lblTitle1Line2.Visible = false;
			this.lblTitle1Line2.Width = 1F;
			// 
			// lblTitle1Line3
			// 
			this.lblTitle1Line3.Height = 0.1875F;
			this.lblTitle1Line3.HyperLink = null;
			this.lblTitle1Line3.Left = 0.3125F;
			this.lblTitle1Line3.Name = "lblTitle1Line3";
			this.lblTitle1Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle1Line3.Text = "Label17";
			this.lblTitle1Line3.Top = 1.15625F;
			this.lblTitle1Line3.Visible = false;
			this.lblTitle1Line3.Width = 1F;
			// 
			// lblTitle2Line1
			// 
			this.lblTitle2Line1.Height = 0.1875F;
			this.lblTitle2Line1.HyperLink = null;
			this.lblTitle2Line1.Left = 1F;
			this.lblTitle2Line1.Name = "lblTitle2Line1";
			this.lblTitle2Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle2Line1.Text = "Label15";
			this.lblTitle2Line1.Top = 0.78125F;
			this.lblTitle2Line1.Visible = false;
			this.lblTitle2Line1.Width = 1F;
			// 
			// lblTitle2Line2
			// 
			this.lblTitle2Line2.Height = 0.1875F;
			this.lblTitle2Line2.HyperLink = null;
			this.lblTitle2Line2.Left = 1F;
			this.lblTitle2Line2.Name = "lblTitle2Line2";
			this.lblTitle2Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle2Line2.Text = "Label16";
			this.lblTitle2Line2.Top = 0.96875F;
			this.lblTitle2Line2.Visible = false;
			this.lblTitle2Line2.Width = 1F;
			// 
			// lblTitle2Line3
			// 
			this.lblTitle2Line3.Height = 0.1875F;
			this.lblTitle2Line3.HyperLink = null;
			this.lblTitle2Line3.Left = 1F;
			this.lblTitle2Line3.Name = "lblTitle2Line3";
			this.lblTitle2Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle2Line3.Text = "Label17";
			this.lblTitle2Line3.Top = 1.15625F;
			this.lblTitle2Line3.Visible = false;
			this.lblTitle2Line3.Width = 1F;
			// 
			// lblTitle3Line1
			// 
			this.lblTitle3Line1.Height = 0.1875F;
			this.lblTitle3Line1.HyperLink = null;
			this.lblTitle3Line1.Left = 1.625F;
			this.lblTitle3Line1.Name = "lblTitle3Line1";
			this.lblTitle3Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle3Line1.Text = "Label15";
			this.lblTitle3Line1.Top = 0.78125F;
			this.lblTitle3Line1.Visible = false;
			this.lblTitle3Line1.Width = 1F;
			// 
			// lblTitle3Line2
			// 
			this.lblTitle3Line2.Height = 0.1875F;
			this.lblTitle3Line2.HyperLink = null;
			this.lblTitle3Line2.Left = 1.625F;
			this.lblTitle3Line2.Name = "lblTitle3Line2";
			this.lblTitle3Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle3Line2.Text = "Label16";
			this.lblTitle3Line2.Top = 0.96875F;
			this.lblTitle3Line2.Visible = false;
			this.lblTitle3Line2.Width = 1F;
			// 
			// lblTitle3Line3
			// 
			this.lblTitle3Line3.Height = 0.1875F;
			this.lblTitle3Line3.HyperLink = null;
			this.lblTitle3Line3.Left = 1.625F;
			this.lblTitle3Line3.Name = "lblTitle3Line3";
			this.lblTitle3Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle3Line3.Text = "Label17";
			this.lblTitle3Line3.Top = 1.15625F;
			this.lblTitle3Line3.Visible = false;
			this.lblTitle3Line3.Width = 1F;
			// 
			// lblTitle4Line1
			// 
			this.lblTitle4Line1.Height = 0.1875F;
			this.lblTitle4Line1.HyperLink = null;
			this.lblTitle4Line1.Left = 2.3125F;
			this.lblTitle4Line1.Name = "lblTitle4Line1";
			this.lblTitle4Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle4Line1.Text = "Label15";
			this.lblTitle4Line1.Top = 0.78125F;
			this.lblTitle4Line1.Visible = false;
			this.lblTitle4Line1.Width = 1F;
			// 
			// lblTitle4Line2
			// 
			this.lblTitle4Line2.Height = 0.1875F;
			this.lblTitle4Line2.HyperLink = null;
			this.lblTitle4Line2.Left = 2.3125F;
			this.lblTitle4Line2.Name = "lblTitle4Line2";
			this.lblTitle4Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle4Line2.Text = "Label16";
			this.lblTitle4Line2.Top = 0.96875F;
			this.lblTitle4Line2.Visible = false;
			this.lblTitle4Line2.Width = 1F;
			// 
			// lblTitle4Line3
			// 
			this.lblTitle4Line3.Height = 0.1875F;
			this.lblTitle4Line3.HyperLink = null;
			this.lblTitle4Line3.Left = 2.3125F;
			this.lblTitle4Line3.Name = "lblTitle4Line3";
			this.lblTitle4Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle4Line3.Text = "Label17";
			this.lblTitle4Line3.Top = 1.15625F;
			this.lblTitle4Line3.Visible = false;
			this.lblTitle4Line3.Width = 1F;
			// 
			// lblTitle5Line1
			// 
			this.lblTitle5Line1.Height = 0.1875F;
			this.lblTitle5Line1.HyperLink = null;
			this.lblTitle5Line1.Left = 3F;
			this.lblTitle5Line1.Name = "lblTitle5Line1";
			this.lblTitle5Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle5Line1.Text = "Label24";
			this.lblTitle5Line1.Top = 0.78125F;
			this.lblTitle5Line1.Visible = false;
			this.lblTitle5Line1.Width = 1F;
			// 
			// lblTitle5Line2
			// 
			this.lblTitle5Line2.Height = 0.1875F;
			this.lblTitle5Line2.HyperLink = null;
			this.lblTitle5Line2.Left = 3F;
			this.lblTitle5Line2.Name = "lblTitle5Line2";
			this.lblTitle5Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle5Line2.Text = "Label25";
			this.lblTitle5Line2.Top = 0.96875F;
			this.lblTitle5Line2.Visible = false;
			this.lblTitle5Line2.Width = 1F;
			// 
			// lblTitle5Line3
			// 
			this.lblTitle5Line3.Height = 0.1875F;
			this.lblTitle5Line3.HyperLink = null;
			this.lblTitle5Line3.Left = 3F;
			this.lblTitle5Line3.Name = "lblTitle5Line3";
			this.lblTitle5Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle5Line3.Text = "Label26";
			this.lblTitle5Line3.Top = 1.15625F;
			this.lblTitle5Line3.Visible = false;
			this.lblTitle5Line3.Width = 1F;
			// 
			// lblTitle6Line1
			// 
			this.lblTitle6Line1.Height = 0.1875F;
			this.lblTitle6Line1.HyperLink = null;
			this.lblTitle6Line1.Left = 3.625F;
			this.lblTitle6Line1.Name = "lblTitle6Line1";
			this.lblTitle6Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle6Line1.Text = "Label15";
			this.lblTitle6Line1.Top = 0.78125F;
			this.lblTitle6Line1.Visible = false;
			this.lblTitle6Line1.Width = 1F;
			// 
			// lblTitle6Line2
			// 
			this.lblTitle6Line2.Height = 0.1875F;
			this.lblTitle6Line2.HyperLink = null;
			this.lblTitle6Line2.Left = 3.625F;
			this.lblTitle6Line2.Name = "lblTitle6Line2";
			this.lblTitle6Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle6Line2.Text = "Label16";
			this.lblTitle6Line2.Top = 0.96875F;
			this.lblTitle6Line2.Visible = false;
			this.lblTitle6Line2.Width = 1F;
			// 
			// lblTitle6Line3
			// 
			this.lblTitle6Line3.Height = 0.1875F;
			this.lblTitle6Line3.HyperLink = null;
			this.lblTitle6Line3.Left = 3.625F;
			this.lblTitle6Line3.Name = "lblTitle6Line3";
			this.lblTitle6Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle6Line3.Text = "Label17";
			this.lblTitle6Line3.Top = 1.15625F;
			this.lblTitle6Line3.Visible = false;
			this.lblTitle6Line3.Width = 1F;
			// 
			// lblTitle7Line1
			// 
			this.lblTitle7Line1.Height = 0.1875F;
			this.lblTitle7Line1.HyperLink = null;
			this.lblTitle7Line1.Left = 4.375F;
			this.lblTitle7Line1.Name = "lblTitle7Line1";
			this.lblTitle7Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle7Line1.Text = "Label15";
			this.lblTitle7Line1.Top = 0.78125F;
			this.lblTitle7Line1.Visible = false;
			this.lblTitle7Line1.Width = 1F;
			// 
			// lblTitle7Line2
			// 
			this.lblTitle7Line2.Height = 0.1875F;
			this.lblTitle7Line2.HyperLink = null;
			this.lblTitle7Line2.Left = 4.375F;
			this.lblTitle7Line2.Name = "lblTitle7Line2";
			this.lblTitle7Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle7Line2.Text = "Label16";
			this.lblTitle7Line2.Top = 0.96875F;
			this.lblTitle7Line2.Visible = false;
			this.lblTitle7Line2.Width = 1F;
			// 
			// lblTitle7Line3
			// 
			this.lblTitle7Line3.Height = 0.1875F;
			this.lblTitle7Line3.HyperLink = null;
			this.lblTitle7Line3.Left = 4.375F;
			this.lblTitle7Line3.Name = "lblTitle7Line3";
			this.lblTitle7Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle7Line3.Text = "Label17";
			this.lblTitle7Line3.Top = 1.15625F;
			this.lblTitle7Line3.Visible = false;
			this.lblTitle7Line3.Width = 1F;
			// 
			// lblTitle8Line1
			// 
			this.lblTitle8Line1.Height = 0.1875F;
			this.lblTitle8Line1.HyperLink = null;
			this.lblTitle8Line1.Left = 5.0625F;
			this.lblTitle8Line1.Name = "lblTitle8Line1";
			this.lblTitle8Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle8Line1.Text = "Label33";
			this.lblTitle8Line1.Top = 0.78125F;
			this.lblTitle8Line1.Visible = false;
			this.lblTitle8Line1.Width = 1F;
			// 
			// lblTitle8Line2
			// 
			this.lblTitle8Line2.Height = 0.1875F;
			this.lblTitle8Line2.HyperLink = null;
			this.lblTitle8Line2.Left = 5.0625F;
			this.lblTitle8Line2.Name = "lblTitle8Line2";
			this.lblTitle8Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle8Line2.Text = "Label34";
			this.lblTitle8Line2.Top = 0.96875F;
			this.lblTitle8Line2.Visible = false;
			this.lblTitle8Line2.Width = 1F;
			// 
			// lblTitle8Line3
			// 
			this.lblTitle8Line3.Height = 0.1875F;
			this.lblTitle8Line3.HyperLink = null;
			this.lblTitle8Line3.Left = 5.0625F;
			this.lblTitle8Line3.Name = "lblTitle8Line3";
			this.lblTitle8Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle8Line3.Text = "Label35";
			this.lblTitle8Line3.Top = 1.15625F;
			this.lblTitle8Line3.Visible = false;
			this.lblTitle8Line3.Width = 1F;
			// 
			// lblTitle9Line1
			// 
			this.lblTitle9Line1.Height = 0.1875F;
			this.lblTitle9Line1.HyperLink = null;
			this.lblTitle9Line1.Left = 5.6875F;
			this.lblTitle9Line1.Name = "lblTitle9Line1";
			this.lblTitle9Line1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle9Line1.Text = "Label15";
			this.lblTitle9Line1.Top = 0.78125F;
			this.lblTitle9Line1.Visible = false;
			this.lblTitle9Line1.Width = 1F;
			// 
			// lblTitle9Line2
			// 
			this.lblTitle9Line2.Height = 0.1875F;
			this.lblTitle9Line2.HyperLink = null;
			this.lblTitle9Line2.Left = 5.6875F;
			this.lblTitle9Line2.Name = "lblTitle9Line2";
			this.lblTitle9Line2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle9Line2.Text = "Label16";
			this.lblTitle9Line2.Top = 0.96875F;
			this.lblTitle9Line2.Visible = false;
			this.lblTitle9Line2.Width = 1F;
			// 
			// lblTitle9Line3
			// 
			this.lblTitle9Line3.Height = 0.1875F;
			this.lblTitle9Line3.HyperLink = null;
			this.lblTitle9Line3.Left = 5.6875F;
			this.lblTitle9Line3.Name = "lblTitle9Line3";
			this.lblTitle9Line3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblTitle9Line3.Text = "Label17";
			this.lblTitle9Line3.Top = 1.15625F;
			this.lblTitle9Line3.Visible = false;
			this.lblTitle9Line3.Width = 1F;
			// 
			// fldDeptDivTitle
			// 
			this.fldDeptDivTitle.DataField = "GroupTitle";
			this.fldDeptDivTitle.Height = 0.1875F;
			this.fldDeptDivTitle.Left = 0.03125F;
			this.fldDeptDivTitle.Name = "fldDeptDivTitle";
			this.fldDeptDivTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDeptDivTitle.Text = "Field1";
			this.fldDeptDivTitle.Top = 0.09375F;
			this.fldDeptDivTitle.Width = 4.25F;
			// 
			// Field1
			// 
			this.Field1.DataField = "Binder";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 4.9375F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Binder";
			this.Field1.Top = 0.03125F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.4375F;
			// 
			// fldExpenseTitle
			// 
			this.fldExpenseTitle.CanShrink = true;
			this.fldExpenseTitle.Height = 0.1875F;
			this.fldExpenseTitle.Left = 0.03125F;
			this.fldExpenseTitle.Name = "fldExpenseTitle";
			this.fldExpenseTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldExpenseTitle.Text = "Field1";
			this.fldExpenseTitle.Top = 0F;
			this.fldExpenseTitle.Width = 2.40625F;
			// 
			// Field2
			// 
			this.Field2.DataField = "ExpBinder";
			this.Field2.Height = 0.125F;
			this.Field2.Left = 4.5625F;
			this.Field2.Name = "Field2";
			this.Field2.Text = "ExpBinder";
			this.Field2.Top = 0F;
			this.Field2.Visible = false;
			this.Field2.Width = 0.5625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.125F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 2.3125F;
			// 
			// fldDetail1
			// 
			this.fldDetail1.Height = 0.1875F;
			this.fldDetail1.Left = 2.53125F;
			this.fldDetail1.Name = "fldDetail1";
			this.fldDetail1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail1.Text = "7";
			this.fldDetail1.Top = 0F;
			this.fldDetail1.Visible = false;
			this.fldDetail1.Width = 0.8819444F;
			// 
			// fldComments
			// 
			this.fldComments.CanShrink = true;
			this.fldComments.Height = 0.1875F;
			this.fldComments.Left = 0.25F;
			this.fldComments.Name = "fldComments";
			this.fldComments.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldComments.Text = "Field1";
			this.fldComments.Top = 0.1875F;
			this.fldComments.Width = 2.1875F;
			// 
			// fldDetail2
			// 
			this.fldDetail2.Height = 0.1875F;
			this.fldDetail2.Left = 3.1875F;
			this.fldDetail2.Name = "fldDetail2";
			this.fldDetail2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail2.Text = "7";
			this.fldDetail2.Top = 0F;
			this.fldDetail2.Visible = false;
			this.fldDetail2.Width = 0.8819444F;
			// 
			// fldDetail3
			// 
			this.fldDetail3.Height = 0.1875F;
			this.fldDetail3.Left = 3.65625F;
			this.fldDetail3.Name = "fldDetail3";
			this.fldDetail3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail3.Text = "7";
			this.fldDetail3.Top = 0F;
			this.fldDetail3.Visible = false;
			this.fldDetail3.Width = 0.8819444F;
			// 
			// fldDetail4
			// 
			this.fldDetail4.Height = 0.1875F;
			this.fldDetail4.Left = 4.21875F;
			this.fldDetail4.Name = "fldDetail4";
			this.fldDetail4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail4.Text = "7";
			this.fldDetail4.Top = 0F;
			this.fldDetail4.Visible = false;
			this.fldDetail4.Width = 0.8819444F;
			// 
			// fldDetail5
			// 
			this.fldDetail5.Height = 0.1875F;
			this.fldDetail5.Left = 4.6875F;
			this.fldDetail5.Name = "fldDetail5";
			this.fldDetail5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail5.Text = "7";
			this.fldDetail5.Top = 0F;
			this.fldDetail5.Visible = false;
			this.fldDetail5.Width = 0.8819444F;
			// 
			// fldDetail6
			// 
			this.fldDetail6.Height = 0.1875F;
			this.fldDetail6.Left = 5.1875F;
			this.fldDetail6.Name = "fldDetail6";
			this.fldDetail6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail6.Text = "7";
			this.fldDetail6.Top = 0F;
			this.fldDetail6.Visible = false;
			this.fldDetail6.Width = 0.8819444F;
			// 
			// fldDetail7
			// 
			this.fldDetail7.Height = 0.1875F;
			this.fldDetail7.Left = 5.71875F;
			this.fldDetail7.Name = "fldDetail7";
			this.fldDetail7.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail7.Text = "7";
			this.fldDetail7.Top = 0F;
			this.fldDetail7.Visible = false;
			this.fldDetail7.Width = 0.8819444F;
			// 
			// fldDetail8
			// 
			this.fldDetail8.Height = 0.1875F;
			this.fldDetail8.Left = 6.25F;
			this.fldDetail8.Name = "fldDetail8";
			this.fldDetail8.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail8.Text = "7";
			this.fldDetail8.Top = 0F;
			this.fldDetail8.Visible = false;
			this.fldDetail8.Width = 0.8819444F;
			// 
			// fldDetail9
			// 
			this.fldDetail9.Height = 0.1875F;
			this.fldDetail9.Left = 6.71875F;
			this.fldDetail9.Name = "fldDetail9";
			this.fldDetail9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDetail9.Text = "7";
			this.fldDetail9.Top = 0F;
			this.fldDetail9.Visible = false;
			this.fldDetail9.Width = 0.8819444F;
			// 
			// fldExpenseName
			// 
			this.fldExpenseName.CanShrink = true;
			this.fldExpenseName.Height = 0.1875F;
			this.fldExpenseName.Left = 1.28125F;
			this.fldExpenseName.Name = "fldExpenseName";
			this.fldExpenseName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldExpenseName.Text = "Field1";
			this.fldExpenseName.Top = 0F;
			this.fldExpenseName.Width = 1.15625F;
			// 
			// fldExpTotal1
			// 
			this.fldExpTotal1.CanShrink = true;
			this.fldExpTotal1.Height = 0.1875F;
			this.fldExpTotal1.Left = 2.46875F;
			this.fldExpTotal1.Name = "fldExpTotal1";
			this.fldExpTotal1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal1.Text = "Field1";
			this.fldExpTotal1.Top = 0F;
			this.fldExpTotal1.Visible = false;
			this.fldExpTotal1.Width = 0.8819444F;
			// 
			// fldExpTotal2
			// 
			this.fldExpTotal2.CanShrink = true;
			this.fldExpTotal2.Height = 0.1875F;
			this.fldExpTotal2.Left = 2.96875F;
			this.fldExpTotal2.Name = "fldExpTotal2";
			this.fldExpTotal2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal2.Text = "Field1";
			this.fldExpTotal2.Top = 0F;
			this.fldExpTotal2.Visible = false;
			this.fldExpTotal2.Width = 0.8819444F;
			// 
			// fldExpTotal3
			// 
			this.fldExpTotal3.CanShrink = true;
			this.fldExpTotal3.Height = 0.1875F;
			this.fldExpTotal3.Left = 3.25F;
			this.fldExpTotal3.Name = "fldExpTotal3";
			this.fldExpTotal3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal3.Text = "Field1";
			this.fldExpTotal3.Top = 0F;
			this.fldExpTotal3.Visible = false;
			this.fldExpTotal3.Width = 0.8819444F;
			// 
			// fldExpTotal4
			// 
			this.fldExpTotal4.CanShrink = true;
			this.fldExpTotal4.Height = 0.1875F;
			this.fldExpTotal4.Left = 3.59375F;
			this.fldExpTotal4.Name = "fldExpTotal4";
			this.fldExpTotal4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal4.Text = "Field1";
			this.fldExpTotal4.Top = 0F;
			this.fldExpTotal4.Visible = false;
			this.fldExpTotal4.Width = 0.8819444F;
			// 
			// fldExpTotal5
			// 
			this.fldExpTotal5.CanShrink = true;
			this.fldExpTotal5.Height = 0.1875F;
			this.fldExpTotal5.Left = 4F;
			this.fldExpTotal5.Name = "fldExpTotal5";
			this.fldExpTotal5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal5.Text = "Field1";
			this.fldExpTotal5.Top = 0F;
			this.fldExpTotal5.Visible = false;
			this.fldExpTotal5.Width = 0.8819444F;
			// 
			// fldExpTotal6
			// 
			this.fldExpTotal6.CanShrink = true;
			this.fldExpTotal6.Height = 0.1875F;
			this.fldExpTotal6.Left = 4.5F;
			this.fldExpTotal6.Name = "fldExpTotal6";
			this.fldExpTotal6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal6.Text = "Field1";
			this.fldExpTotal6.Top = 0F;
			this.fldExpTotal6.Visible = false;
			this.fldExpTotal6.Width = 0.8819444F;
			// 
			// fldExpTotal7
			// 
			this.fldExpTotal7.CanShrink = true;
			this.fldExpTotal7.Height = 0.1875F;
			this.fldExpTotal7.Left = 4.78125F;
			this.fldExpTotal7.Name = "fldExpTotal7";
			this.fldExpTotal7.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal7.Text = "Field1";
			this.fldExpTotal7.Top = 0F;
			this.fldExpTotal7.Visible = false;
			this.fldExpTotal7.Width = 0.8819444F;
			// 
			// fldExpTotal8
			// 
			this.fldExpTotal8.CanShrink = true;
			this.fldExpTotal8.Height = 0.1875F;
			this.fldExpTotal8.Left = 5.125F;
			this.fldExpTotal8.Name = "fldExpTotal8";
			this.fldExpTotal8.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal8.Text = "Field1";
			this.fldExpTotal8.Top = 0F;
			this.fldExpTotal8.Visible = false;
			this.fldExpTotal8.Width = 0.8819444F;
			// 
			// fldExpTotal9
			// 
			this.fldExpTotal9.CanShrink = true;
			this.fldExpTotal9.Height = 0.1875F;
			this.fldExpTotal9.Left = 5.53125F;
			this.fldExpTotal9.Name = "fldExpTotal9";
			this.fldExpTotal9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpTotal9.Text = "Field1";
			this.fldExpTotal9.Top = 0F;
			this.fldExpTotal9.Visible = false;
			this.fldExpTotal9.Width = 0.8819444F;
			// 
			// fldDivisionName
			// 
			this.fldDivisionName.CanShrink = true;
			this.fldDivisionName.DataField = "DivGroupFooter";
			this.fldDivisionName.Height = 0.1875F;
			this.fldDivisionName.Left = 1.28125F;
			this.fldDivisionName.Name = "fldDivisionName";
			this.fldDivisionName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDivisionName.Text = "Field1";
			this.fldDivisionName.Top = 0F;
			this.fldDivisionName.Width = 1.15625F;
			// 
			// fldDepartmentName
			// 
			this.fldDepartmentName.CanShrink = true;
			this.fldDepartmentName.DataField = "DeptGroupFooter";
			this.fldDepartmentName.Height = 0.1875F;
			this.fldDepartmentName.Left = 1.28125F;
			this.fldDepartmentName.Name = "fldDepartmentName";
			this.fldDepartmentName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDepartmentName.Text = "Field2";
			this.fldDepartmentName.Top = 0.1875F;
			this.fldDepartmentName.Width = 1.15625F;
			// 
			// fldDivTotal1
			// 
			this.fldDivTotal1.CanShrink = true;
			this.fldDivTotal1.Height = 0.1875F;
			this.fldDivTotal1.Left = 2.46875F;
			this.fldDivTotal1.Name = "fldDivTotal1";
			this.fldDivTotal1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal1.Text = "Field1";
			this.fldDivTotal1.Top = 0F;
			this.fldDivTotal1.Visible = false;
			this.fldDivTotal1.Width = 0.8819444F;
			// 
			// fldDeptTotal1
			// 
			this.fldDeptTotal1.CanShrink = true;
			this.fldDeptTotal1.Height = 0.1875F;
			this.fldDeptTotal1.Left = 2.46875F;
			this.fldDeptTotal1.Name = "fldDeptTotal1";
			this.fldDeptTotal1.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal1.Text = "Field2";
			this.fldDeptTotal1.Top = 0.1875F;
			this.fldDeptTotal1.Visible = false;
			this.fldDeptTotal1.Width = 0.8819444F;
			// 
			// fldDivTotal2
			// 
			this.fldDivTotal2.CanShrink = true;
			this.fldDivTotal2.Height = 0.1875F;
			this.fldDivTotal2.Left = 2.96875F;
			this.fldDivTotal2.Name = "fldDivTotal2";
			this.fldDivTotal2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal2.Text = "Field1";
			this.fldDivTotal2.Top = 0F;
			this.fldDivTotal2.Visible = false;
			this.fldDivTotal2.Width = 0.8819444F;
			// 
			// fldDeptTotal2
			// 
			this.fldDeptTotal2.CanShrink = true;
			this.fldDeptTotal2.Height = 0.1875F;
			this.fldDeptTotal2.Left = 2.96875F;
			this.fldDeptTotal2.Name = "fldDeptTotal2";
			this.fldDeptTotal2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal2.Text = "Field2";
			this.fldDeptTotal2.Top = 0.1875F;
			this.fldDeptTotal2.Visible = false;
			this.fldDeptTotal2.Width = 0.8819444F;
			// 
			// fldDivTotal3
			// 
			this.fldDivTotal3.CanShrink = true;
			this.fldDivTotal3.Height = 0.1875F;
			this.fldDivTotal3.Left = 3.40625F;
			this.fldDivTotal3.Name = "fldDivTotal3";
			this.fldDivTotal3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal3.Text = "Field1";
			this.fldDivTotal3.Top = 0F;
			this.fldDivTotal3.Visible = false;
			this.fldDivTotal3.Width = 0.8819444F;
			// 
			// fldDeptTotal3
			// 
			this.fldDeptTotal3.CanShrink = true;
			this.fldDeptTotal3.Height = 0.1875F;
			this.fldDeptTotal3.Left = 3.40625F;
			this.fldDeptTotal3.Name = "fldDeptTotal3";
			this.fldDeptTotal3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal3.Text = "Field2";
			this.fldDeptTotal3.Top = 0.1875F;
			this.fldDeptTotal3.Visible = false;
			this.fldDeptTotal3.Width = 0.8819444F;
			// 
			// fldDivTotal4
			// 
			this.fldDivTotal4.CanShrink = true;
			this.fldDivTotal4.Height = 0.1875F;
			this.fldDivTotal4.Left = 3.84375F;
			this.fldDivTotal4.Name = "fldDivTotal4";
			this.fldDivTotal4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal4.Text = "Field1";
			this.fldDivTotal4.Top = 0F;
			this.fldDivTotal4.Visible = false;
			this.fldDivTotal4.Width = 0.8819444F;
			// 
			// fldDeptTotal4
			// 
			this.fldDeptTotal4.CanShrink = true;
			this.fldDeptTotal4.Height = 0.1875F;
			this.fldDeptTotal4.Left = 3.8125F;
			this.fldDeptTotal4.Name = "fldDeptTotal4";
			this.fldDeptTotal4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal4.Text = "Field2";
			this.fldDeptTotal4.Top = 0.1875F;
			this.fldDeptTotal4.Visible = false;
			this.fldDeptTotal4.Width = 0.8819444F;
			// 
			// fldDivTotal5
			// 
			this.fldDivTotal5.CanShrink = true;
			this.fldDivTotal5.Height = 0.1875F;
			this.fldDivTotal5.Left = 4.3125F;
			this.fldDivTotal5.Name = "fldDivTotal5";
			this.fldDivTotal5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal5.Text = "Field1";
			this.fldDivTotal5.Top = 0F;
			this.fldDivTotal5.Visible = false;
			this.fldDivTotal5.Width = 0.8819444F;
			// 
			// fldDeptTotal5
			// 
			this.fldDeptTotal5.CanShrink = true;
			this.fldDeptTotal5.Height = 0.1875F;
			this.fldDeptTotal5.Left = 4.28125F;
			this.fldDeptTotal5.Name = "fldDeptTotal5";
			this.fldDeptTotal5.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal5.Text = "Field2";
			this.fldDeptTotal5.Top = 0.1875F;
			this.fldDeptTotal5.Visible = false;
			this.fldDeptTotal5.Width = 0.8819444F;
			// 
			// fldDivTotal6
			// 
			this.fldDivTotal6.CanShrink = true;
			this.fldDivTotal6.Height = 0.1875F;
			this.fldDivTotal6.Left = 4.78125F;
			this.fldDivTotal6.Name = "fldDivTotal6";
			this.fldDivTotal6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal6.Text = "Field1";
			this.fldDivTotal6.Top = 0F;
			this.fldDivTotal6.Visible = false;
			this.fldDivTotal6.Width = 0.8819444F;
			// 
			// fldDivTotal7
			// 
			this.fldDivTotal7.CanShrink = true;
			this.fldDivTotal7.Height = 0.1875F;
			this.fldDivTotal7.Left = 5.21875F;
			this.fldDivTotal7.Name = "fldDivTotal7";
			this.fldDivTotal7.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal7.Text = "Field1";
			this.fldDivTotal7.Top = 0F;
			this.fldDivTotal7.Visible = false;
			this.fldDivTotal7.Width = 0.8819444F;
			// 
			// fldDivTotal8
			// 
			this.fldDivTotal8.CanShrink = true;
			this.fldDivTotal8.Height = 0.1875F;
			this.fldDivTotal8.Left = 5.65625F;
			this.fldDivTotal8.Name = "fldDivTotal8";
			this.fldDivTotal8.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal8.Text = "Field1";
			this.fldDivTotal8.Top = 0F;
			this.fldDivTotal8.Visible = false;
			this.fldDivTotal8.Width = 0.8819444F;
			// 
			// fldDivTotal9
			// 
			this.fldDivTotal9.CanShrink = true;
			this.fldDivTotal9.Height = 0.1875F;
			this.fldDivTotal9.Left = 6.125F;
			this.fldDivTotal9.Name = "fldDivTotal9";
			this.fldDivTotal9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDivTotal9.Text = "Field1";
			this.fldDivTotal9.Top = 0F;
			this.fldDivTotal9.Visible = false;
			this.fldDivTotal9.Width = 0.8819444F;
			// 
			// fldDeptTotal6
			// 
			this.fldDeptTotal6.CanShrink = true;
			this.fldDeptTotal6.Height = 0.1875F;
			this.fldDeptTotal6.Left = 4.8125F;
			this.fldDeptTotal6.Name = "fldDeptTotal6";
			this.fldDeptTotal6.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal6.Text = "Field2";
			this.fldDeptTotal6.Top = 0.1875F;
			this.fldDeptTotal6.Visible = false;
			this.fldDeptTotal6.Width = 0.8819444F;
			// 
			// fldDeptTotal7
			// 
			this.fldDeptTotal7.CanShrink = true;
			this.fldDeptTotal7.Height = 0.1875F;
			this.fldDeptTotal7.Left = 5.25F;
			this.fldDeptTotal7.Name = "fldDeptTotal7";
			this.fldDeptTotal7.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal7.Text = "Field2";
			this.fldDeptTotal7.Top = 0.1875F;
			this.fldDeptTotal7.Visible = false;
			this.fldDeptTotal7.Width = 0.8819444F;
			// 
			// fldDeptTotal8
			// 
			this.fldDeptTotal8.CanShrink = true;
			this.fldDeptTotal8.Height = 0.1875F;
			this.fldDeptTotal8.Left = 5.65625F;
			this.fldDeptTotal8.Name = "fldDeptTotal8";
			this.fldDeptTotal8.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal8.Text = "Field2";
			this.fldDeptTotal8.Top = 0.1875F;
			this.fldDeptTotal8.Visible = false;
			this.fldDeptTotal8.Width = 0.8819444F;
			// 
			// fldDeptTotal9
			// 
			this.fldDeptTotal9.CanShrink = true;
			this.fldDeptTotal9.Height = 0.1875F;
			this.fldDeptTotal9.Left = 6.125F;
			this.fldDeptTotal9.Name = "fldDeptTotal9";
			this.fldDeptTotal9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDeptTotal9.Text = "Field2";
			this.fldDeptTotal9.Top = 0.1875F;
			this.fldDeptTotal9.Visible = false;
			this.fldDeptTotal9.Width = 0.8819444F;
			// 
			// fldTotal7
			// 
			this.fldTotal7.CanShrink = true;
			this.fldTotal7.Height = 0.1875F;
			this.fldTotal7.Left = 5.25F;
			this.fldTotal7.Name = "fldTotal7";
			this.fldTotal7.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal7.Text = "7";
			this.fldTotal7.Top = 0.4375F;
			this.fldTotal7.Visible = false;
			this.fldTotal7.Width = 0.875F;
			// 
			// fldTotalName
			// 
			this.fldTotalName.CanShrink = true;
			this.fldTotalName.Height = 0.1875F;
			this.fldTotalName.Left = 1.28125F;
			this.fldTotalName.Name = "fldTotalName";
			this.fldTotalName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldTotalName.Text = "Expense Totals:";
			this.fldTotalName.Top = 0.4375F;
			this.fldTotalName.Width = 1.15625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.28125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.375F;
			this.Line1.Width = 5.71875F;
			this.Line1.X1 = 1.28125F;
			this.Line1.X2 = 7F;
			this.Line1.Y1 = 0.375F;
			this.Line1.Y2 = 0.375F;
			// 
			// fldTotal1
			// 
			this.fldTotal1.CanShrink = true;
			this.fldTotal1.Height = 0.1875F;
			this.fldTotal1.Left = 2.46875F;
			this.fldTotal1.Name = "fldTotal1";
			this.fldTotal1.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal1.Text = "7";
			this.fldTotal1.Top = 0.4375F;
			this.fldTotal1.Visible = false;
			this.fldTotal1.Width = 0.875F;
			// 
			// fldTotal2
			// 
			this.fldTotal2.CanShrink = true;
			this.fldTotal2.Height = 0.1875F;
			this.fldTotal2.Left = 2.96875F;
			this.fldTotal2.Name = "fldTotal2";
			this.fldTotal2.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal2.Text = "7";
			this.fldTotal2.Top = 0.4375F;
			this.fldTotal2.Visible = false;
			this.fldTotal2.Width = 0.875F;
			// 
			// fldTotal3
			// 
			this.fldTotal3.CanShrink = true;
			this.fldTotal3.Height = 0.1875F;
			this.fldTotal3.Left = 3.40625F;
			this.fldTotal3.Name = "fldTotal3";
			this.fldTotal3.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal3.Text = "7";
			this.fldTotal3.Top = 0.4375F;
			this.fldTotal3.Visible = false;
			this.fldTotal3.Width = 0.875F;
			// 
			// fldTotal4
			// 
			this.fldTotal4.CanShrink = true;
			this.fldTotal4.Height = 0.1875F;
			this.fldTotal4.Left = 3.8125F;
			this.fldTotal4.Name = "fldTotal4";
			this.fldTotal4.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal4.Text = "7";
			this.fldTotal4.Top = 0.4375F;
			this.fldTotal4.Visible = false;
			this.fldTotal4.Width = 0.875F;
			// 
			// fldTotal5
			// 
			this.fldTotal5.CanShrink = true;
			this.fldTotal5.Height = 0.1875F;
			this.fldTotal5.Left = 4.28125F;
			this.fldTotal5.Name = "fldTotal5";
			this.fldTotal5.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal5.Text = "7";
			this.fldTotal5.Top = 0.4375F;
			this.fldTotal5.Visible = false;
			this.fldTotal5.Width = 0.875F;
			// 
			// fldTotal6
			// 
			this.fldTotal6.CanShrink = true;
			this.fldTotal6.Height = 0.1875F;
			this.fldTotal6.Left = 4.8125F;
			this.fldTotal6.Name = "fldTotal6";
			this.fldTotal6.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal6.Text = "7";
			this.fldTotal6.Top = 0.4375F;
			this.fldTotal6.Visible = false;
			this.fldTotal6.Width = 0.875F;
			// 
			// fldTotal8
			// 
			this.fldTotal8.CanShrink = true;
			this.fldTotal8.Height = 0.1875F;
			this.fldTotal8.Left = 5.65625F;
			this.fldTotal8.Name = "fldTotal8";
			this.fldTotal8.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal8.Text = "7";
			this.fldTotal8.Top = 0.4375F;
			this.fldTotal8.Visible = false;
			this.fldTotal8.Width = 0.875F;
			// 
			// fldTotal9
			// 
			this.fldTotal9.CanShrink = true;
			this.fldTotal9.Height = 0.1875F;
			this.fldTotal9.Left = 6.125F;
			this.fldTotal9.Name = "fldTotal9";
			this.fldTotal9.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotal9.Text = "7";
			this.fldTotal9.Top = 0.4375F;
			this.fldTotal9.Visible = false;
			this.fldTotal9.Width = 0.875F;
			// 
			// rptCustomBudgetExpenseReport
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.1597222F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle3Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle4Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle5Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle6Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle7Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle8Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle9Line3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptDivTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpenseTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetail9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpenseName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldComments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetail9;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl vsHeadings;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle3Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle4Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle5Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle5Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle5Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle6Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle6Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle6Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle7Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle7Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle7Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle8Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle8Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle8Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle9Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle9Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle9Line3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptDivTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalName;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpenseTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpenseName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal9;
	}
}
