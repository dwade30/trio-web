﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOFSummaryInfo.
	/// </summary>
	public partial class srptOFSummaryInfo : FCSectionReport
	{
		public static srptOFSummaryInfo InstancePtr
		{
			get
			{
				return (srptOFSummaryInfo)Sys.GetInstance(typeof(srptOFSummaryInfo));
			}
		}

		protected srptOFSummaryInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOFSummaryInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int intFundCounter;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;

		public srptOFSummaryInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int counter;
			// - "AutoDim"
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				for (counter = intFundCounter; counter <= 99; counter++)
				{
					if (modBudgetaryMaster.Statics.curOFFundTotals[counter] != 0)
					{
						intFundCounter = counter;
						eArgs.EOF = false;
						return;
					}
				}
				eArgs.EOF = true;
			}
			else
			{
				intFundCounter += 1;
				if (intFundCounter <= 99)
				{
					for (counter = intFundCounter; counter <= 99; counter++)
					{
						if (modBudgetaryMaster.Statics.curOFFundTotals[counter] != 0)
						{
							intFundCounter = counter;
							eArgs.EOF = false;
							return;
						}
					}
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			intFundCounter = 1;
			curTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldFund.Text = intFundCounter.ToString();
			fldAmount.Text = Strings.Format(modBudgetaryMaster.Statics.curOFFundTotals[intFundCounter], "#,##0.00");
			curTotal += modBudgetaryMaster.Statics.curOFFundTotals[intFundCounter];
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		
	}
}
