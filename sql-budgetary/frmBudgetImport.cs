﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetImport.
	/// </summary>
	public partial class frmBudgetImport : BaseForm
	{
		private string uploadPath = "";

		public frmBudgetImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBudgetImport InstancePtr
		{
			get
			{
				return (frmBudgetImport)Sys.GetInstance(typeof(frmBudgetImport));
			}
		}

		protected frmBudgetImport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         9/15/02
		// This form will be used to load budget data that was extracted
		// The data will be in a comma delimited file format
		// ********************************************************
		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FileUpload_Uploaded(object sender, UploadedEventArgs e)
		{
			uploadPath = string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMdd_HHmmss"), e.Files[0].FileName);
			uploadPath = Wisej.Web.Application.MapPath(uploadPath);
			e.Files[0].SaveAs(uploadPath);
		}

		private void cmdImport_Click(object sender, System.EventArgs e)
		{
			//string strPath = "";
			string strPath = uploadPath;
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			string strLine = "";
			int TempCheck = 0;
			string[] strData = null;
			bool blnErrors;
			double dblTotal = 0;
			double[] periodAmounts = new double[12 + 1];
			int x;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			//if (filFile.SelectedIndex == -1)
			if (string.IsNullOrEmpty(uploadPath))
			{
				MessageBox.Show("You must select a file before you may proceed.", "No File Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				ans = MessageBox.Show("This process will write over all of your current budget information.  Are you sure you wish to import this budget information?");
				if (ans == DialogResult.No)
				{
					return;
				}
			}
			blnErrors = false;
			//if (Strings.Right(dirDirectory.Path, 1) != "\\")
			//{
			//    strPath = dirDirectory.Path + "\\" + filFile.FileName;
			//}
			//else
			//{
			//    strPath = dirDirectory.Path + filFile.FileName;
			//}
			//strPath = upload1.
			FCFileSystem.FileClose(1);
			FCFileSystem.FileClose(2);
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Importing Data";
			frmWait.InstancePtr.Refresh();
			FCFileSystem.FileOpen(1, strPath, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
			FCFileSystem.FileOpen(2, "ImprtErr.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
			BackupBudgetTable();
			rsAccountInfo.Execute("DELETE FROM Budget", "Budgetary");
			while (FCFileSystem.EOF(1) != true)
			{
				//Application.DoEvents();
				strLine = FCFileSystem.LineInput(1);
				FCUtils.EraseSafe(strData);
				strData = Strings.Split(strLine, ",", -1, CompareConstants.vbBinaryCompare);
				if (Strings.Trim(strData[0].Replace("\"", " ")) == "Account Number")
				{
					goto ErrOrDup;
				}
				if (modValidateAccount.Statics.ValidAcctCheck == 3)
				{
					// do nothing
				}
				else
				{
					TempCheck = modValidateAccount.Statics.ValidAcctCheck;
					modValidateAccount.Statics.ValidAcctCheck = 1;
					if (modValidateAccount.AccountValidate(strData[0]))
					{
						modValidateAccount.Statics.ValidAcctCheck = TempCheck;
					}
					else
					{
						modValidateAccount.Statics.ValidAcctCheck = TempCheck;
						FCFileSystem.WriteLine(2, strData[0], "Invalid Account");
						blnErrors = true;
						goto ErrOrDup;
					}
				}
				rsAccountInfo.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strData[0].Replace(FCConvert.ToString(Convert.ToChar(34)), "") + "'");
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					FCFileSystem.WriteLine(2, strData[0], "Duplicate Entry");
					blnErrors = true;
					goto ErrOrDup;
				}
				if (Strings.Trim(strData[7].Replace("\"", " ")) == "A")
				{
					for (x = 0; x <= 11; x++)
					{
						periodAmounts[x] = modGlobal.Round_8(Conversion.Val(strData[(x + 4) * 2]), 0);
					}
					dblTotal = 0;
					dblTotal += periodAmounts[0];
					dblTotal += periodAmounts[1];
					dblTotal += periodAmounts[2];
					dblTotal += periodAmounts[3];
					dblTotal += periodAmounts[4];
					dblTotal += periodAmounts[5];
					dblTotal += periodAmounts[6];
					dblTotal += periodAmounts[7];
					dblTotal += periodAmounts[8];
					dblTotal += periodAmounts[9];
					dblTotal += periodAmounts[10];
					dblTotal += periodAmounts[11];
					if (dblTotal != 0 && Conversion.Val(dblTotal) != modGlobal.Round_8(Conversion.Val(strData[6]), 0))
					{
						FCFileSystem.WriteLine(2, strData[0], "Monthly BreakDown Error");
						blnErrors = true;
						goto ErrOrDup;
					}
				}
				else
				{
					for (x = 0; x <= 11; x++)
					{
						periodAmounts[x] = modGlobal.Round_8(Conversion.Val(strData[((x + 4) * 2) + 1]), 0);
					}
					dblTotal = 0;
					dblTotal += periodAmounts[0];
					dblTotal += periodAmounts[1];
					dblTotal += periodAmounts[2];
					dblTotal += periodAmounts[3];
					dblTotal += periodAmounts[4];
					dblTotal += periodAmounts[5];
					dblTotal += periodAmounts[6];
					dblTotal += periodAmounts[7];
					dblTotal += periodAmounts[8];
					dblTotal += periodAmounts[9];
					dblTotal += periodAmounts[10];
					dblTotal += periodAmounts[11];
					if (dblTotal != 0 && dblTotal != 100)
					{
						FCFileSystem.WriteLine(2, strData[0], "Monthly BreakDown Error");
						blnErrors = true;
						goto ErrOrDup;
					}
				}
				rsAccountInfo.OmitNullsOnInsert = true;
				rsAccountInfo.AddNew();
				rsAccountInfo.Set_Fields("Account", Strings.Trim(strData[0].Replace("\"", " ")));
				rsAccountInfo.Set_Fields("InitialRequest", modGlobal.Round_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(strData[2]))), 0));
				rsAccountInfo.Set_Fields("ManagerRequest", modGlobal.Round_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(strData[3]))), 0));
				rsAccountInfo.Set_Fields("CommitteeRequest", modGlobal.Round_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(strData[4]))), 0));
				double dblElectedRequest = 0;
				double dblApprovedAmount = 0;
				if (!Information.IsNumeric(strData[5]))
				{
					rsAccountInfo.Set_Fields("ElectedRequest", 0);
				}
				else
				{
					rsAccountInfo.Set_Fields("ElectedRequest", modGlobal.Round_8(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(strData[5]))), 0));
					dblElectedRequest = modGlobal.Round_8(Conversion.Val(strData[5]), 0);
				}
				dblApprovedAmount = modGlobal.Round_8(Conversion.Val(strData[6]), 0);
				rsAccountInfo.Set_Fields("ApprovedAmount", dblApprovedAmount);
				if (Information.UBound(strData, 1) >= 32)
				{
					rsAccountInfo.Set_Fields("Comments", Strings.Trim(strData[32].Replace("\"", " ")));
				}
				else
				{
					rsAccountInfo.Set_Fields("Comments", "");
				}
				if (Strings.Trim(strData[7].Replace("\"", " ")) == "P")
				{
					rsAccountInfo.Set_Fields("PercentFlag", true);
					rsAccountInfo.Set_Fields("JanPercent", periodAmounts[0]);
					rsAccountInfo.Set_Fields("JanBudget", (periodAmounts[0] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("FebPercent", periodAmounts[1]);
					rsAccountInfo.Set_Fields("FebBudget", (periodAmounts[1] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("MarPercent", periodAmounts[2]);
					rsAccountInfo.Set_Fields("MarBudget", (periodAmounts[2] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("AprPercent", periodAmounts[3]);
					rsAccountInfo.Set_Fields("AprBudget", (periodAmounts[3] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("MayPercent", periodAmounts[4]);
					rsAccountInfo.Set_Fields("MayBudget", (periodAmounts[4] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("JunePercent", periodAmounts[5]);
					rsAccountInfo.Set_Fields("JuneBudget", (periodAmounts[5] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("JulyPercent", periodAmounts[6]);
					rsAccountInfo.Set_Fields("JulyBudget", (periodAmounts[6] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("AugPercent", periodAmounts[7]);
					rsAccountInfo.Set_Fields("AugBudget", (periodAmounts[7] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("SeptPercent", periodAmounts[8]);
					rsAccountInfo.Set_Fields("SeptBudget", (periodAmounts[8] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("OctPercent", periodAmounts[9]);
					rsAccountInfo.Set_Fields("OctBudget", (periodAmounts[9] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("NovPercent", periodAmounts[10]);
					rsAccountInfo.Set_Fields("NovBudget", (periodAmounts[10] / 100) * dblElectedRequest);
					rsAccountInfo.Set_Fields("DecPercent", periodAmounts[11]);
					rsAccountInfo.Set_Fields("DecBudget", (periodAmounts[11] / 100) * dblElectedRequest);
				}
				else
				{
					rsAccountInfo.Set_Fields("PercentFlag", false);
					if (Conversion.Val(strData[6]) == 0)
					{
						rsAccountInfo.Set_Fields("JanPercent", 0);
						rsAccountInfo.Set_Fields("JanBudget", periodAmounts[0]);
						rsAccountInfo.Set_Fields("FebPercent", 0);
						rsAccountInfo.Set_Fields("FebBudget", periodAmounts[1]);
						rsAccountInfo.Set_Fields("MarPercent", 0);
						rsAccountInfo.Set_Fields("MarBudget", periodAmounts[2]);
						rsAccountInfo.Set_Fields("AprPercent", 0);
						rsAccountInfo.Set_Fields("AprBudget", periodAmounts[3]);
						rsAccountInfo.Set_Fields("MayPercent", 0);
						rsAccountInfo.Set_Fields("MayBudget", periodAmounts[4]);
						rsAccountInfo.Set_Fields("JunePercent", 0);
						rsAccountInfo.Set_Fields("JuneBudget", periodAmounts[5]);
						rsAccountInfo.Set_Fields("JulyPercent", 0);
						rsAccountInfo.Set_Fields("JulyBudget", periodAmounts[6]);
						rsAccountInfo.Set_Fields("AugPercent", 0);
						rsAccountInfo.Set_Fields("AugBudget", periodAmounts[7]);
						rsAccountInfo.Set_Fields("SeptPercent", 0);
						rsAccountInfo.Set_Fields("SeptBudget", periodAmounts[8]);
						rsAccountInfo.Set_Fields("OctPercent", 0);
						rsAccountInfo.Set_Fields("OctBudget", periodAmounts[9]);
						rsAccountInfo.Set_Fields("NovPercent", 0);
						rsAccountInfo.Set_Fields("NovBudget", periodAmounts[10]);
						rsAccountInfo.Set_Fields("DecPercent", 0);
						rsAccountInfo.Set_Fields("DecBudget", periodAmounts[11]);
					}
					else
					{
						rsAccountInfo.Set_Fields("JanPercent", modGlobal.Round_8((periodAmounts[0] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("JanBudget", periodAmounts[0]);
						rsAccountInfo.Set_Fields("FebPercent", modGlobal.Round_8((periodAmounts[1] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("FebBudget", periodAmounts[1]);
						rsAccountInfo.Set_Fields("MarPercent", modGlobal.Round_8((periodAmounts[2] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("MarBudget", periodAmounts[2]);
						rsAccountInfo.Set_Fields("AprPercent", modGlobal.Round_8((periodAmounts[3] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("AprBudget", periodAmounts[3]);
						rsAccountInfo.Set_Fields("MayPercent", modGlobal.Round_8((periodAmounts[4] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("MayBudget", periodAmounts[4]);
						rsAccountInfo.Set_Fields("JunePercent", modGlobal.Round_8((periodAmounts[5] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("JuneBudget", periodAmounts[5]);
						rsAccountInfo.Set_Fields("JulyPercent", modGlobal.Round_8((periodAmounts[6] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("JulyBudget", periodAmounts[6]);
						rsAccountInfo.Set_Fields("AugPercent", modGlobal.Round_8((periodAmounts[7] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("AugBudget", periodAmounts[7]);
						rsAccountInfo.Set_Fields("SeptPercent", modGlobal.Round_8((periodAmounts[8] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("SeptBudget", periodAmounts[8]);
						rsAccountInfo.Set_Fields("OctPercent", modGlobal.Round_8((periodAmounts[9] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("OctBudget", periodAmounts[9]);
						rsAccountInfo.Set_Fields("NovPercent", modGlobal.Round_8((periodAmounts[10] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("NovBudget", periodAmounts[10]);
						rsAccountInfo.Set_Fields("DecPercent", modGlobal.Round_8((periodAmounts[11] / dblApprovedAmount) * 100, 2));
						rsAccountInfo.Set_Fields("DecBudget", periodAmounts[11]);
					}
				}
				rsAccountInfo.Update(true);
				ErrOrDup:
				;
			}
			FCFileSystem.FileClose(1);
			FCFileSystem.FileClose(2);
			frmWait.InstancePtr.Unload();
			//Application.DoEvents();
			if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) == "\\")
			{
				strLine = FCFileSystem.Statics.UserDataFolder;
			}
			else
			{
				strLine = FCFileSystem.Statics.UserDataFolder + "\\";
			}
			if (!blnErrors)
			{
				MessageBox.Show("Import Completed Successfully!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Some information was not able to be imported.  The file " + strLine + "ImprtErr.txt contains a list of accounts and the errors encountered.", "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
		//private void dirDirectory_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    filFile.Path = dirDirectory.Path;
		//}
		//private void drvDrive_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    DriveInfo drv;
		//    string strDrive = "";
		//    drv = new DriveInfo(Strings.Left(drvDrive.Drive, 1));
		//    if (drv.IsReady)
		//    {
		//        dirDirectory.Path = drvDrive.Drive;
		//    }
		//    else
		//    {
		//        MessageBox.Show("No disk in the drive", "No Disk", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//        drvDrive.Drive = "c:";
		//    }
		//}
		private void frmBudgetImport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			//dirDirectory.Path = drvDrive.Drive;
			//filFile.Path = dirDirectory.Path;
			this.Refresh();
		}

		private void frmBudgetImport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBudgetImport.FillStyle	= 0;
			//frmBudgetImport.ScaleWidth	= 9045;
			//frmBudgetImport.ScaleHeight	= 7200;
			//frmBudgetImport.LinkTopic	= "Form2";
			//frmBudgetImport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmBudgetImport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void BackupBudgetTable()
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			if (File.Exists("BDBckup8.txt"))
			{
				File.Copy("BDBckup8.txt", "BDBckup9.txt", true);
			}
			if (File.Exists("BDBckup7.txt"))
			{
				File.Copy("BDBckup7.txt", "BDBckup8.txt", true);
			}
			if (File.Exists("BDBckup6.txt"))
			{
				File.Copy("BDBckup6.txt", "BDBckup7.txt", true);
			}
			if (File.Exists("BDBckup5.txt"))
			{
				File.Copy("BDBckup5.txt", "BDBckup6.txt", true);
			}
			if (File.Exists("BDBckup4.txt"))
			{
				File.Copy("BDBckup4.txt", "BDBckup5.txt", true);
			}
			if (File.Exists("BDBckup3.txt"))
			{
				File.Copy("BDBckup3.txt", "BDBckup4.txt", true);
			}
			if (File.Exists("BDBckup2.txt"))
			{
				File.Copy("BDBckup2.txt", "BDBckup3.txt", true);
			}
			if (File.Exists("BDBckup1.txt"))
			{
				File.Copy("BDBckup1.txt", "BDBckup2.txt", true);
			}
			FCFileSystem.FileOpen(3, "BDBckup1.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
			rsAccountInfo.OpenRecordset("SELECT * FROM Budget ORDER BY Account");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				FCFileSystem.WriteLine(3, "Account Number", "Account Description", "Last Year Budget", "Last Year Actual", "Current Year Budget", "Current Year Actual", "Initial Request", "Manager Request", "Committee Request", "Elected Request", "Approved Budget", "Breakdown By Amount or Percent", "January Amount", "January Percent", "February Amount", "February Percent", "March Amount", "March Percent", "April Amount", "April Percent", "May Amount", "May Percent", "June Amount", "June Percent", "July Amount", "July Percent", "August Amount", "August Percent", "September Amount", "September Percent", "October Amount", "October Percent", "November Amount", "November Percent", "December Amount", "December Percent", "Comments");
				System.Collections.Generic.List<object> tempObjList = null;
				do
				{
					//FC:FINAL:MSH - replaced to list of objects, which after we can save, because 'Write' and 'WriteLine' have the same
					// functionality (the methods add '\r\n' to the file after of each item) (same with issue #800)
					tempObjList = new System.Collections.Generic.List<object>();
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("Account"));
					//FCFileSystem.Write(3, modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields("Account")));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("InitialRequest"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("ManagerRequest"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("CommitteeRequest"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("ElectedRequest"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("ApprovedAmount"));
					//if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields("PercentFlag")))
					//{
					//    FCFileSystem.Write(3, "P");
					//}
					//else
					//{
					//    FCFileSystem.Write(3, "A");
					//}
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("JanBudget"), rsAccountInfo.Get_Fields("JanPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("FebBudget"), rsAccountInfo.Get_Fields("FebPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("MarBudget"), rsAccountInfo.Get_Fields("MarPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("AprBudget"), rsAccountInfo.Get_Fields("AprPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("MayBudget"), rsAccountInfo.Get_Fields("MayPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("JuneBudget"), rsAccountInfo.Get_Fields("JunePercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("JulyBudget"), rsAccountInfo.Get_Fields("JulyPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("AugBudget"), rsAccountInfo.Get_Fields("AugPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("SeptBudget"), rsAccountInfo.Get_Fields("SeptPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("OctBudget"), rsAccountInfo.Get_Fields("OctPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("NovBudget"), rsAccountInfo.Get_Fields("NovPercent"));
					//FCFileSystem.Write(3, rsAccountInfo.Get_Fields("DecBudget"), rsAccountInfo.Get_Fields("DecPercent"));
					//FCFileSystem.WriteLine(3, rsAccountInfo.Get_Fields("Comments"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					tempObjList.Add(rsAccountInfo.Get_Fields("Account"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					tempObjList.Add(modAccountTitle.ReturnAccountDescription(rsAccountInfo.Get_Fields("Account")));
					// TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
					tempObjList.Add(rsAccountInfo.Get_Fields("InitialRequest"));
					// TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
					tempObjList.Add(rsAccountInfo.Get_Fields("ManagerRequest"));
					// TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
					tempObjList.Add(rsAccountInfo.Get_Fields("CommitteeRequest"));
					// TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
					tempObjList.Add(rsAccountInfo.Get_Fields("ElectedRequest"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("ApprovedAmount"));
					if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("PercentFlag")))
					{
						tempObjList.Add("P");
					}
					else
					{
						tempObjList.Add("A");
					}
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JanBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("JanPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("FebBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("FebPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("MarBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("MarPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("AprBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("AprPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("MayBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("MayPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JuneBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("JunePercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("JulyBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("JulyPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("AugBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("AugPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("SeptBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("SeptPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("OctBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("OctPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("NovBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("NovPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Decimal("DecBudget"));
					tempObjList.Add(rsAccountInfo.Get_Fields_Double("DecPercent"));
					tempObjList.Add(rsAccountInfo.Get_Fields_String("Comments"));
					FCFileSystem.Write(3, tempObjList.ToArray());
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
				FCFileSystem.FileClose(3);
			}
			else
			{
				FCFileSystem.FileClose(3);
			}
		}

		private void SetCustomFormColors()
		{
			Label3.ForeColor = Color.Red;
		}
	}
}
