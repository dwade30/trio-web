﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptHancockCountyBadAccounts.
	/// </summary>
	public partial class rptHancockCountyBadAccounts : BaseSectionReport
	{
		public static rptHancockCountyBadAccounts InstancePtr
		{
			get
			{
				return (rptHancockCountyBadAccounts)Sys.GetInstance(typeof(rptHancockCountyBadAccounts));
			}
		}

		protected rptHancockCountyBadAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptHancockCountyBadAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public string strBadAccts = "";

		public rptHancockCountyBadAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bad Accounts List";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldAccounts.Text = strBadAccts;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		
	}
}
