//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetRequestEdit.
	/// </summary>
	partial class frmBudgetRequestEdit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRevene;
		public fecherFoundation.FCLabel lblRevene;
		public fecherFoundation.FCFrame fraAccountType;
		public fecherFoundation.FCFrame fraComments;
		public fecherFoundation.FCButton cmdCancelComment;
		public fecherFoundation.FCTextBox txtComment;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCGrid accountGrid;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBudgetRequestEdit));
            this.cmbRevene = new fecherFoundation.FCComboBox();
            this.lblRevene = new fecherFoundation.FCLabel();
            this.fraAccountType = new fecherFoundation.FCFrame();
            this.fraComments = new fecherFoundation.FCFrame();
            this.cmdCancelComment = new fecherFoundation.FCButton();
            this.txtComment = new fecherFoundation.FCTextBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.accountGrid = new fecherFoundation.FCGrid();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdAddComment = new fecherFoundation.FCButton();
            this.cmdAddAccount = new fecherFoundation.FCButton();
            this.cmdDeleteAccount = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).BeginInit();
            this.fraAccountType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraComments)).BeginInit();
            this.fraComments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 505);
            this.BottomPanel.Size = new System.Drawing.Size(607, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraComments);
            this.ClientArea.Controls.Add(this.accountGrid);
            this.ClientArea.Controls.Add(this.lblTitle);
            this.ClientArea.Controls.Add(this.fraAccountType);
            this.ClientArea.Size = new System.Drawing.Size(627, 508);
            this.ClientArea.Controls.SetChildIndex(this.fraAccountType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.accountGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraComments, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteAccount);
            this.TopPanel.Controls.Add(this.cmdAddAccount);
            this.TopPanel.Controls.Add(this.cmdAddComment);
            this.TopPanel.Size = new System.Drawing.Size(627, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteAccount, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(223, 28);
            this.HeaderText.Text = "Budget Request Edit";
            // 
            // cmbRevene
            // 
            this.cmbRevene.Items.AddRange(new object[] {
            "Expense Accounts",
            "Revenue Accounts"});
            this.cmbRevene.Location = new System.Drawing.Point(160, 30);
            this.cmbRevene.Name = "cmbRevene";
            this.cmbRevene.Size = new System.Drawing.Size(185, 40);
            this.cmbRevene.TabIndex = 0;
            // 
            // lblRevene
            // 
            this.lblRevene.Location = new System.Drawing.Point(20, 44);
            this.lblRevene.Name = "lblRevene";
            this.lblRevene.TabIndex = 1;
            this.lblRevene.Text = "ACCOUNTS TYPE";
            // 
            // fraAccountType
            // 
            this.fraAccountType.Controls.Add(this.cmbRevene);
            this.fraAccountType.Controls.Add(this.lblRevene);
            this.fraAccountType.Location = new System.Drawing.Point(20, 30);
            this.fraAccountType.Name = "fraAccountType";
            this.fraAccountType.Size = new System.Drawing.Size(365, 98);
            this.fraAccountType.TabIndex = 0;
            this.fraAccountType.Text = "Accounts To Update";
            // 
            // fraComments
            // 
            this.fraComments.BackColor = System.Drawing.Color.FromName("@window");
            this.fraComments.Controls.Add(this.cmdCancelComment);
            this.fraComments.Controls.Add(this.txtComment);
            this.fraComments.Controls.Add(this.lblAccount);
            this.fraComments.Location = new System.Drawing.Point(30, 30);
            this.fraComments.Name = "fraComments";
            this.fraComments.Size = new System.Drawing.Size(456, 246);
            this.fraComments.TabIndex = 8;
            this.fraComments.Text = "Budget Comments";
            this.fraComments.Visible = false;
            // 
            // cmdCancelComment
            // 
            this.cmdCancelComment.AppearanceKey = "actionButton";
            this.cmdCancelComment.Location = new System.Drawing.Point(20, 186);
            this.cmdCancelComment.Name = "cmdCancelComment";
            this.cmdCancelComment.Size = new System.Drawing.Size(94, 40);
            this.cmdCancelComment.TabIndex = 12;
            this.cmdCancelComment.Text = "Cancel";
            this.cmdCancelComment.Click += new System.EventHandler(this.cmdCancelComment_Click);
            // 
            // txtComment
            // 
            this.txtComment.AcceptsReturn = true;
            this.txtComment.BackColor = System.Drawing.SystemColors.Window;
            this.txtComment.Location = new System.Drawing.Point(20, 66);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(416, 100);
            this.txtComment.TabIndex = 9;
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(20, 30);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(218, 16);
            this.lblAccount.TabIndex = 10;
            this.lblAccount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // accountGrid
            // 
            this.accountGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.accountGrid.FixedCols = 0;
            this.accountGrid.Location = new System.Drawing.Point(30, 56);
            this.accountGrid.Name = "accountGrid";
            this.accountGrid.RowHeadersVisible = false;
            this.accountGrid.Rows = 50;
            this.accountGrid.Size = new System.Drawing.Size(567, 449);
            this.accountGrid.StandardTab = false;
            this.accountGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.accountGrid.TabIndex = 6;
            this.accountGrid.Visible = false;
            this.accountGrid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.accountGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
            this.accountGrid.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyUpEdit);
            this.accountGrid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
            this.accountGrid.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.Vs1_EditingControlShowing);
            this.accountGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.accountGrid.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.accountGrid.Enter += new System.EventHandler(this.vs1_Enter);
            this.accountGrid.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            this.accountGrid.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(30, 30);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(563, 16);
            this.lblTitle.TabIndex = 7;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessDelete,
            this.mnuProcessAdd,
            this.mnuFileSeperator2,
            this.mnuFileComment,
            this.mnuFileSeperator3,
            this.mnuFileSave,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessDelete
            // 
            this.mnuProcessDelete.Enabled = false;
            this.mnuProcessDelete.Index = 0;
            this.mnuProcessDelete.Name = "mnuProcessDelete";
            this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuProcessDelete.Text = "Delete Account";
            this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
            // 
            // mnuProcessAdd
            // 
            this.mnuProcessAdd.Enabled = false;
            this.mnuProcessAdd.Index = 1;
            this.mnuProcessAdd.Name = "mnuProcessAdd";
            this.mnuProcessAdd.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuProcessAdd.Text = "Add Account";
            this.mnuProcessAdd.Click += new System.EventHandler(this.mnuProcessAdd_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 2;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFileComment
            // 
            this.mnuFileComment.Index = 3;
            this.mnuFileComment.Name = "mnuFileComment";
            this.mnuFileComment.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileComment.Text = "Add Comment";
            this.mnuFileComment.Click += new System.EventHandler(this.mnuFileComment_Click);
            // 
            // mnuFileSeperator3
            // 
            this.mnuFileSeperator3.Index = 4;
            this.mnuFileSeperator3.Name = "mnuFileSeperator3";
            this.mnuFileSeperator3.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 5;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 6;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 7;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 8;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(264, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(104, 48);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdAddComment
            // 
            this.cmdAddComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddComment.Location = new System.Drawing.Point(494, 29);
            this.cmdAddComment.Name = "cmdAddComment";
            this.cmdAddComment.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdAddComment.Size = new System.Drawing.Size(103, 24);
            this.cmdAddComment.TabIndex = 13;
            this.cmdAddComment.Text = "Add Comment";
            this.cmdAddComment.Visible = false;
            this.cmdAddComment.Click += new System.EventHandler(this.cmdAddComment_Click);
            // 
            // cmdAddAccount
            // 
            this.cmdAddAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddAccount.Location = new System.Drawing.Point(394, 29);
            this.cmdAddAccount.Name = "cmdAddAccount";
            this.cmdAddAccount.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdAddAccount.Size = new System.Drawing.Size(96, 24);
            this.cmdAddAccount.TabIndex = 14;
            this.cmdAddAccount.Text = "Add Account";
            this.cmdAddAccount.Visible = false;
            this.cmdAddAccount.Click += new System.EventHandler(this.cmdAddAccount_Click);
            // 
            // cmdDeleteAccount
            // 
            this.cmdDeleteAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteAccount.Location = new System.Drawing.Point(280, 29);
            this.cmdDeleteAccount.Name = "cmdDeleteAccount";
            this.cmdDeleteAccount.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdDeleteAccount.Size = new System.Drawing.Size(110, 24);
            this.cmdDeleteAccount.TabIndex = 15;
            this.cmdDeleteAccount.Text = "Delete Account";
            this.cmdDeleteAccount.Visible = false;
            this.cmdDeleteAccount.Click += new System.EventHandler(this.cmdDeleteAccount_Click);
            // 
            // frmBudgetRequestEdit
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(627, 568);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBudgetRequestEdit";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmBudgetRequestEdit_Load);
            this.Activated += new System.EventHandler(this.frmBudgetRequestEdit_Activated);
            this.Resize += new System.EventHandler(this.frmBudgetRequestEdit_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBudgetRequestEdit_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBudgetRequestEdit_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).EndInit();
            this.fraAccountType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraComments)).EndInit();
            this.fraComments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteAccount)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdAddComment;
		public FCButton cmdAddAccount;
		public FCButton cmdDeleteAccount;
    }
}