﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetImport.
	/// </summary>
	partial class frmBudgetImport : BaseForm
	{
		//public fecherFoundation.FCDriveListBox drvDrive;
		//public fecherFoundation.FCDirListBox dirDirectory;
		//public fecherFoundation.FCFileListBox filFile;
		public fecherFoundation.FCButton cmdImport;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBudgetImport));
			this.cmdImport = new fecherFoundation.FCButton();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.fileUpload = new Wisej.Web.Upload();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdImport);
			this.BottomPanel.Location = new System.Drawing.Point(0, 435);
			this.BottomPanel.Size = new System.Drawing.Size(1163, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fileUpload);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(1163, 375);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1163, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(199, 30);
			this.HeaderText.Text = "Load Budget Info";
			// 
			// cmdImport
			// 
			this.cmdImport.AppearanceKey = "acceptButton";
			this.cmdImport.Location = new System.Drawing.Point(333, 30);
			this.cmdImport.Name = "cmdImport";
			this.cmdImport.Size = new System.Drawing.Size(93, 48);
			this.cmdImport.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdImport.TabIndex = 1;
			this.cmdImport.Text = "Import";
			this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 125);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(60, 16);
			this.Label3.TabIndex = 7;
			this.Label3.Text = "WARNING";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(658, 15);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "SELECT THE EXTRACT FILE TO INPUT BUDGET INFORMATION FROM AND THEN CLICK THE IMPOR" + "T BUTTON";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(126, 125);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(599, 32);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "THIS IMPORT WILL ONLY WORK ON A COMMA-DELIMITED FORMAT.  ATTEMPTING TO IMPORT FIL" + "ES IN OTHER FORMATS COULD CORRUPT YOUR BUDGET DATA";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// fileUpload
			// 
			this.fileUpload.Location = new System.Drawing.Point(30, 65);
			this.fileUpload.Name = "fileUpload";
			this.fileUpload.Size = new System.Drawing.Size(400, 40);
			this.fileUpload.TabIndex = 8;
			this.fileUpload.Text = "Select file";
			this.fileUpload.Uploaded += new Wisej.Web.UploadedEventHandler(this.FileUpload_Uploaded);
			// 
			// frmBudgetImport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1163, 543);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBudgetImport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Load Budget Info";
			this.Load += new System.EventHandler(this.frmBudgetImport_Load);
			this.Activated += new System.EventHandler(this.frmBudgetImport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBudgetImport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdImport)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private Upload fileUpload;
	}
}
