﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseDetailVendorSummarySelect.
	/// </summary>
	partial class frmExpenseDetailVendorSummarySelect : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraRangeSelection;
		public fecherFoundation.FCButton cmdCancelRange;
		public fecherFoundation.FCFrame fraAccountRange;
		public fecherFoundation.FCComboBox cboBeginningExpense;
		public fecherFoundation.FCComboBox cboEndingExpense;
		public fecherFoundation.FCComboBox cboSingleExpense;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraReportSelection;
		public fecherFoundation.FCButton cmdEditFormat;
		public fecherFoundation.FCButton cmdEditCriteria;
		public fecherFoundation.FCButton cmdProcessSave;
		public fecherFoundation.FCButton cmdCreateFormat;
		public fecherFoundation.FCButton cmdCreateCriteria;
		public fecherFoundation.FCTextBox txtReportTitle;
		public fecherFoundation.FCButton cmdCancelSelection;
		public fecherFoundation.FCComboBox cboFormat;
		public fecherFoundation.FCComboBox cboCriteria;
		public fecherFoundation.FCLabel lblReportTitle;
		public fecherFoundation.FCLabel lblFormat;
		public fecherFoundation.FCLabel lblCriteria;
		public fecherFoundation.FCCheckBox chkDefault;
		public fecherFoundation.FCComboBox cboReports;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCButton cmdFileDelete;
		public fecherFoundation.FCButton cmdFileSave;
		public fecherFoundation.FCButton cmdFileSaveExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpenseDetailVendorSummarySelect));
			this.fraRangeSelection = new fecherFoundation.FCFrame();
			this.cmdCancelRange = new fecherFoundation.FCButton();
			this.fraAccountRange = new fecherFoundation.FCFrame();
			this.cboBeginningExpense = new fecherFoundation.FCComboBox();
			this.cboEndingExpense = new fecherFoundation.FCComboBox();
			this.cboSingleExpense = new fecherFoundation.FCComboBox();
			this.cboBeginningDept = new fecherFoundation.FCComboBox();
			this.cboEndingDept = new fecherFoundation.FCComboBox();
			this.cboSingleDept = new fecherFoundation.FCComboBox();
			this.vsLowAccount = new fecherFoundation.FCGrid();
			this.vsHighAccount = new fecherFoundation.FCGrid();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.cboEndingMonth = new fecherFoundation.FCComboBox();
			this.cboBeginningMonth = new fecherFoundation.FCComboBox();
			this.cboSingleMonth = new fecherFoundation.FCComboBox();
			this.lblTo_0 = new fecherFoundation.FCLabel();
			this.fraReportSelection = new fecherFoundation.FCFrame();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdEditFormat = new fecherFoundation.FCButton();
			this.cmdEditCriteria = new fecherFoundation.FCButton();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.cmdCreateFormat = new fecherFoundation.FCButton();
			this.cmdCreateCriteria = new fecherFoundation.FCButton();
			this.txtReportTitle = new fecherFoundation.FCTextBox();
			this.cmdCancelSelection = new fecherFoundation.FCButton();
			this.cboFormat = new fecherFoundation.FCComboBox();
			this.cboCriteria = new fecherFoundation.FCComboBox();
			this.lblReportTitle = new fecherFoundation.FCLabel();
			this.lblFormat = new fecherFoundation.FCLabel();
			this.lblCriteria = new fecherFoundation.FCLabel();
			this.chkDefault = new fecherFoundation.FCCheckBox();
			this.cboReports = new fecherFoundation.FCComboBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.cmdFileDelete = new fecherFoundation.FCButton();
			this.cmdFileSaveExit = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).BeginInit();
			this.fraRangeSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).BeginInit();
			this.fraAccountRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).BeginInit();
			this.fraReportSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditFormat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditCriteria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreateFormat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreateCriteria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDefault)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(626, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdFileSaveExit);
			this.ClientArea.Controls.Add(this.fraReportSelection);
			this.ClientArea.Controls.Add(this.fraRangeSelection);
			this.ClientArea.Controls.Add(this.chkDefault);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.cboReports);
			this.ClientArea.Size = new System.Drawing.Size(626, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileDelete);
			this.TopPanel.Size = new System.Drawing.Size(626, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(402, 30);
			this.HeaderText.Text = "Expense - Vendor Summary Report";
			// 
			// fraRangeSelection
			// 
			this.fraRangeSelection.AppearanceKey = "groupBoxLeftBorder";
			this.fraRangeSelection.BackColor = System.Drawing.Color.White;
			this.fraRangeSelection.Controls.Add(this.cmdCancelRange);
			this.fraRangeSelection.Controls.Add(this.fraAccountRange);
			this.fraRangeSelection.Controls.Add(this.fraDateRange);
			this.fraRangeSelection.Location = new System.Drawing.Point(30, 56);
			this.fraRangeSelection.Name = "fraRangeSelection";
			this.fraRangeSelection.Size = new System.Drawing.Size(570, 310);
			this.fraRangeSelection.TabIndex = 18;
			this.fraRangeSelection.Text = "Criteria Selection";
			this.fraRangeSelection.Visible = false;
			// 
			// cmdCancelRange
			// 
			this.cmdCancelRange.AppearanceKey = "actionButton";
			this.cmdCancelRange.Location = new System.Drawing.Point(20, 250);
			this.cmdCancelRange.Name = "cmdCancelRange";
			this.cmdCancelRange.Size = new System.Drawing.Size(90, 40);
			this.cmdCancelRange.TabIndex = 33;
			this.cmdCancelRange.Text = "Cancel";
			this.cmdCancelRange.Click += new System.EventHandler(this.cmdCancelRange_Click);
			// 
			// fraAccountRange
			// 
			this.fraAccountRange.Controls.Add(this.cboBeginningExpense);
			this.fraAccountRange.Controls.Add(this.cboEndingExpense);
			this.fraAccountRange.Controls.Add(this.cboSingleExpense);
			this.fraAccountRange.Controls.Add(this.cboBeginningDept);
			this.fraAccountRange.Controls.Add(this.cboEndingDept);
			this.fraAccountRange.Controls.Add(this.cboSingleDept);
			this.fraAccountRange.Controls.Add(this.vsLowAccount);
			this.fraAccountRange.Controls.Add(this.vsHighAccount);
			this.fraAccountRange.Controls.Add(this.lblTo_2);
			this.fraAccountRange.Location = new System.Drawing.Point(20, 140);
			this.fraAccountRange.Name = "fraAccountRange";
			this.fraAccountRange.Size = new System.Drawing.Size(480, 90);
			this.fraAccountRange.TabIndex = 24;
			this.fraAccountRange.Text = "Account Range";
			// 
			// cboBeginningExpense
			// 
			this.cboBeginningExpense.AutoSize = false;
			this.cboBeginningExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningExpense.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningExpense.FormattingEnabled = true;
			this.cboBeginningExpense.Location = new System.Drawing.Point(20, 30);
			this.cboBeginningExpense.Name = "cboBeginningExpense";
			this.cboBeginningExpense.Size = new System.Drawing.Size(190, 40);
			this.cboBeginningExpense.TabIndex = 30;
			this.cboBeginningExpense.Visible = false;
			this.cboBeginningExpense.DropDown += new System.EventHandler(this.cboBeginningExpense_DropDown);
			// 
			// cboEndingExpense
			// 
			this.cboEndingExpense.AutoSize = false;
			this.cboEndingExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingExpense.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingExpense.FormattingEnabled = true;
			this.cboEndingExpense.Location = new System.Drawing.Point(270, 30);
			this.cboEndingExpense.Name = "cboEndingExpense";
			this.cboEndingExpense.Size = new System.Drawing.Size(190, 40);
			this.cboEndingExpense.TabIndex = 32;
			this.cboEndingExpense.Visible = false;
			this.cboEndingExpense.DropDown += new System.EventHandler(this.cboEndingExpense_DropDown);
			// 
			// cboSingleExpense
			// 
			this.cboSingleExpense.AutoSize = false;
			this.cboSingleExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleExpense.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleExpense.FormattingEnabled = true;
			this.cboSingleExpense.Location = new System.Drawing.Point(20, 30);
			this.cboSingleExpense.Name = "cboSingleExpense";
			this.cboSingleExpense.Size = new System.Drawing.Size(440, 40);
			this.cboSingleExpense.TabIndex = 28;
			this.cboSingleExpense.Visible = false;
			this.cboSingleExpense.DropDown += new System.EventHandler(this.cboSingleExpense_DropDown);
			// 
			// cboBeginningDept
			// 
			this.cboBeginningDept.AutoSize = false;
			this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningDept.FormattingEnabled = true;
			this.cboBeginningDept.Location = new System.Drawing.Point(20, 30);
			this.cboBeginningDept.Name = "cboBeginningDept";
			this.cboBeginningDept.Size = new System.Drawing.Size(190, 40);
			this.cboBeginningDept.TabIndex = 27;
			this.cboBeginningDept.Visible = false;
			this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
			// 
			// cboEndingDept
			// 
			this.cboEndingDept.AutoSize = false;
			this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingDept.FormattingEnabled = true;
			this.cboEndingDept.Location = new System.Drawing.Point(270, 30);
			this.cboEndingDept.Name = "cboEndingDept";
			this.cboEndingDept.Size = new System.Drawing.Size(190, 40);
			this.cboEndingDept.TabIndex = 29;
			this.cboEndingDept.Visible = false;
			this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
			// 
			// cboSingleDept
			// 
			this.cboSingleDept.AutoSize = false;
			this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleDept.FormattingEnabled = true;
			this.cboSingleDept.Location = new System.Drawing.Point(20, 30);
			this.cboSingleDept.Name = "cboSingleDept";
			this.cboSingleDept.Size = new System.Drawing.Size(440, 40);
			this.cboSingleDept.TabIndex = 26;
			this.cboSingleDept.Visible = false;
			this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
			// 
			// vsLowAccount
			// 
			this.vsLowAccount.AllowSelection = false;
			this.vsLowAccount.AllowUserToResizeColumns = false;
			this.vsLowAccount.AllowUserToResizeRows = false;
			this.vsLowAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLowAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsLowAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsLowAccount.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLowAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsLowAccount.ColumnHeadersHeight = 30;
			this.vsLowAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsLowAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLowAccount.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsLowAccount.DragIcon = null;
			this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLowAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLowAccount.FixedCols = 0;
			this.vsLowAccount.FixedRows = 0;
			this.vsLowAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.FrozenCols = 0;
			this.vsLowAccount.GridColor = System.Drawing.Color.Empty;
			this.vsLowAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.Location = new System.Drawing.Point(20, 30);
			this.vsLowAccount.Name = "vsLowAccount";
			this.vsLowAccount.RowHeadersVisible = false;
			this.vsLowAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLowAccount.RowHeightMin = 0;
			this.vsLowAccount.Rows = 1;
			this.vsLowAccount.ScrollTipText = null;
			this.vsLowAccount.ShowColumnVisibilityMenu = false;
			this.vsLowAccount.Size = new System.Drawing.Size(190, 42);
			this.vsLowAccount.StandardTab = true;
			this.vsLowAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLowAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLowAccount.TabIndex = 36;
			this.vsLowAccount.Visible = false;
			// 
			// vsHighAccount
			// 
			this.vsHighAccount.AllowSelection = false;
			this.vsHighAccount.AllowUserToResizeColumns = false;
			this.vsHighAccount.AllowUserToResizeRows = false;
			this.vsHighAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsHighAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsHighAccount.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsHighAccount.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsHighAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsHighAccount.ColumnHeadersHeight = 30;
			this.vsHighAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsHighAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsHighAccount.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsHighAccount.DragIcon = null;
			this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsHighAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsHighAccount.FixedCols = 0;
			this.vsHighAccount.FixedRows = 0;
			this.vsHighAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.FrozenCols = 0;
			this.vsHighAccount.GridColor = System.Drawing.Color.Empty;
			this.vsHighAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.Location = new System.Drawing.Point(270, 30);
			this.vsHighAccount.Name = "vsHighAccount";
			this.vsHighAccount.RowHeadersVisible = false;
			this.vsHighAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsHighAccount.RowHeightMin = 0;
			this.vsHighAccount.Rows = 1;
			this.vsHighAccount.ScrollTipText = null;
			this.vsHighAccount.ShowColumnVisibilityMenu = false;
			this.vsHighAccount.Size = new System.Drawing.Size(190, 42);
			this.vsHighAccount.StandardTab = true;
			this.vsHighAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsHighAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsHighAccount.TabIndex = 37;
			this.vsHighAccount.Visible = false;
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_2.Location = new System.Drawing.Point(230, 44);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(20, 20);
			this.lblTo_2.TabIndex = 25;
			this.lblTo_2.Text = "TO";
			this.lblTo_2.Visible = false;
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.cboEndingMonth);
			this.fraDateRange.Controls.Add(this.cboBeginningMonth);
			this.fraDateRange.Controls.Add(this.cboSingleMonth);
			this.fraDateRange.Controls.Add(this.lblTo_0);
			this.fraDateRange.Location = new System.Drawing.Point(20, 30);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(480, 90);
			this.fraDateRange.TabIndex = 19;
			this.fraDateRange.Text = "Date Range";
			// 
			// cboEndingMonth
			// 
			this.cboEndingMonth.AutoSize = false;
			this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingMonth.FormattingEnabled = true;
			this.cboEndingMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboEndingMonth.Location = new System.Drawing.Point(270, 30);
			this.cboEndingMonth.Name = "cboEndingMonth";
			this.cboEndingMonth.Size = new System.Drawing.Size(190, 40);
			this.cboEndingMonth.TabIndex = 22;
			this.cboEndingMonth.Visible = false;
			// 
			// cboBeginningMonth
			// 
			this.cboBeginningMonth.AutoSize = false;
			this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningMonth.FormattingEnabled = true;
			this.cboBeginningMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboBeginningMonth.Location = new System.Drawing.Point(20, 30);
			this.cboBeginningMonth.Name = "cboBeginningMonth";
			this.cboBeginningMonth.Size = new System.Drawing.Size(190, 40);
			this.cboBeginningMonth.TabIndex = 21;
			this.cboBeginningMonth.Visible = false;
			// 
			// cboSingleMonth
			// 
			this.cboSingleMonth.AutoSize = false;
			this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleMonth.FormattingEnabled = true;
			this.cboSingleMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboSingleMonth.Location = new System.Drawing.Point(20, 30);
			this.cboSingleMonth.Name = "cboSingleMonth";
			this.cboSingleMonth.Size = new System.Drawing.Size(190, 40);
			this.cboSingleMonth.TabIndex = 20;
			this.cboSingleMonth.Visible = false;
			// 
			// lblTo_0
			// 
			this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_0.Location = new System.Drawing.Point(230, 44);
			this.lblTo_0.Name = "lblTo_0";
			this.lblTo_0.Size = new System.Drawing.Size(20, 20);
			this.lblTo_0.TabIndex = 23;
			this.lblTo_0.Text = "TO";
			this.lblTo_0.Visible = false;
			// 
			// fraReportSelection
			// 
			this.fraReportSelection.BackColor = System.Drawing.Color.White;
			this.fraReportSelection.Controls.Add(this.cmdFileSave);
			this.fraReportSelection.Controls.Add(this.cmdEditFormat);
			this.fraReportSelection.Controls.Add(this.cmdEditCriteria);
			this.fraReportSelection.Controls.Add(this.cmdProcessSave);
			this.fraReportSelection.Controls.Add(this.cmdCreateFormat);
			this.fraReportSelection.Controls.Add(this.cmdCreateCriteria);
			this.fraReportSelection.Controls.Add(this.txtReportTitle);
			this.fraReportSelection.Controls.Add(this.cmdCancelSelection);
			this.fraReportSelection.Controls.Add(this.cboFormat);
			this.fraReportSelection.Controls.Add(this.cboCriteria);
			this.fraReportSelection.Controls.Add(this.lblReportTitle);
			this.fraReportSelection.Controls.Add(this.lblFormat);
			this.fraReportSelection.Controls.Add(this.lblCriteria);
			this.fraReportSelection.Location = new System.Drawing.Point(30, 56);
			this.fraReportSelection.Name = "fraReportSelection";
			this.fraReportSelection.Size = new System.Drawing.Size(570, 390);
			this.fraReportSelection.TabIndex = 14;
			this.fraReportSelection.Text = "Report Selections";
			this.fraReportSelection.Visible = false;
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "actionButton";
			this.cmdFileSave.Location = new System.Drawing.Point(20, 330);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Size = new System.Drawing.Size(120, 40);
			this.cmdFileSave.TabIndex = 18;
			this.cmdFileSave.Text = "Save Report";
			// 
			// cmdEditFormat
			// 
			this.cmdEditFormat.AppearanceKey = "actionButton";
			this.cmdEditFormat.Location = new System.Drawing.Point(319, 270);
			this.cmdEditFormat.Name = "cmdEditFormat";
			this.cmdEditFormat.Size = new System.Drawing.Size(75, 40);
			this.cmdEditFormat.TabIndex = 7;
			this.cmdEditFormat.Text = "Edit";
			this.cmdEditFormat.Click += new System.EventHandler(this.cmdEditFormat_Click);
			// 
			// cmdEditCriteria
			// 
			this.cmdEditCriteria.AppearanceKey = "actionButton";
			this.cmdEditCriteria.Location = new System.Drawing.Point(319, 150);
			this.cmdEditCriteria.Name = "cmdEditCriteria";
			this.cmdEditCriteria.Size = new System.Drawing.Size(75, 40);
			this.cmdEditCriteria.TabIndex = 4;
			this.cmdEditCriteria.Text = "Edit";
			this.cmdEditCriteria.Click += new System.EventHandler(this.cmdEditCriteria_Click);
			// 
			// cmdProcessNoSave
			// 
			this.cmdProcessSave.AppearanceKey = "actionButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(160, 330);
			this.cmdProcessSave.Name = "cmdProcessNoSave";
			this.cmdProcessSave.Size = new System.Drawing.Size(150, 40);
			this.cmdProcessSave.TabIndex = 10;
			this.cmdProcessSave.Text = "Save & Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.cmdProcessSave_Click);
			// 
			// cmdCreateFormat
			// 
			this.cmdCreateFormat.AppearanceKey = "actionButton";
			this.cmdCreateFormat.Location = new System.Drawing.Point(177, 270);
			this.cmdCreateFormat.Name = "cmdCreateFormat";
			this.cmdCreateFormat.Size = new System.Drawing.Size(125, 40);
			this.cmdCreateFormat.TabIndex = 6;
			this.cmdCreateFormat.Text = "Create New";
			this.cmdCreateFormat.Click += new System.EventHandler(this.cmdCreateFormat_Click);
			// 
			// cmdCreateCriteria
			// 
			this.cmdCreateCriteria.AppearanceKey = "actionButton";
			this.cmdCreateCriteria.Location = new System.Drawing.Point(177, 150);
			this.cmdCreateCriteria.Name = "cmdCreateCriteria";
			this.cmdCreateCriteria.Size = new System.Drawing.Size(125, 40);
			this.cmdCreateCriteria.TabIndex = 3;
			this.cmdCreateCriteria.Text = "Create New";
			this.cmdCreateCriteria.Click += new System.EventHandler(this.cmdCreateCriteria_Click);
			// 
			// txtReportTitle
			// 
			this.txtReportTitle.AutoSize = false;
			this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportTitle.LinkItem = null;
			this.txtReportTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReportTitle.LinkTopic = null;
			this.txtReportTitle.Location = new System.Drawing.Point(177, 30);
			this.txtReportTitle.Name = "txtReportTitle";
			this.txtReportTitle.Size = new System.Drawing.Size(373, 40);
			this.txtReportTitle.TabIndex = 1;
			// 
			// cmdCancelSelection
			// 
			this.cmdCancelSelection.AppearanceKey = "actionButton";
			this.cmdCancelSelection.Location = new System.Drawing.Point(330, 330);
			this.cmdCancelSelection.Name = "cmdCancelSelection";
			this.cmdCancelSelection.Size = new System.Drawing.Size(90, 40);
			this.cmdCancelSelection.TabIndex = 11;
			this.cmdCancelSelection.Text = "Cancel";
			this.cmdCancelSelection.Click += new System.EventHandler(this.cmdCancelSelection_Click);
			// 
			// cboFormat
			// 
			this.cboFormat.AutoSize = false;
			this.cboFormat.BackColor = System.Drawing.SystemColors.Window;
			this.cboFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboFormat.FormattingEnabled = true;
			this.cboFormat.Location = new System.Drawing.Point(177, 210);
			this.cboFormat.Name = "cboFormat";
			this.cboFormat.Size = new System.Drawing.Size(373, 40);
			this.cboFormat.TabIndex = 5;
			// 
			// cboCriteria
			// 
			this.cboCriteria.AutoSize = false;
			this.cboCriteria.BackColor = System.Drawing.SystemColors.Window;
			this.cboCriteria.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCriteria.FormattingEnabled = true;
			this.cboCriteria.Location = new System.Drawing.Point(177, 90);
			this.cboCriteria.Name = "cboCriteria";
			this.cboCriteria.Size = new System.Drawing.Size(373, 40);
			this.cboCriteria.TabIndex = 2;
			// 
			// lblReportTitle
			// 
			this.lblReportTitle.Location = new System.Drawing.Point(20, 44);
			this.lblReportTitle.Name = "lblReportTitle";
			this.lblReportTitle.Size = new System.Drawing.Size(86, 16);
			this.lblReportTitle.TabIndex = 17;
			this.lblReportTitle.Text = "REPORT TITLE";
			this.lblReportTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFormat
			// 
			this.lblFormat.Location = new System.Drawing.Point(20, 224);
			this.lblFormat.Name = "lblFormat";
			this.lblFormat.Size = new System.Drawing.Size(116, 16);
			this.lblFormat.TabIndex = 16;
			this.lblFormat.Text = "REPORT FORMAT";
			this.lblFormat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCriteria
			// 
			this.lblCriteria.Location = new System.Drawing.Point(20, 104);
			this.lblCriteria.Name = "lblCriteria";
			this.lblCriteria.Size = new System.Drawing.Size(128, 16);
			this.lblCriteria.TabIndex = 15;
			this.lblCriteria.Text = "SELECTION CRITERIA";
			this.lblCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkDefault
			// 
			this.chkDefault.Location = new System.Drawing.Point(30, 66);
			this.chkDefault.Name = "chkDefault";
			this.chkDefault.Size = new System.Drawing.Size(226, 27);
			this.chkDefault.TabIndex = 13;
			this.chkDefault.Text = "Make this the default report";
			this.chkDefault.CheckedChanged += new System.EventHandler(this.chkDefault_CheckedChanged);
			// 
			// cboReports
			// 
			this.cboReports.AutoSize = false;
			this.cboReports.BackColor = System.Drawing.SystemColors.Window;
			this.cboReports.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReports.FormattingEnabled = true;
			this.cboReports.Location = new System.Drawing.Point(30, 112);
			this.cboReports.Name = "cboReports";
			this.cboReports.Size = new System.Drawing.Size(449, 40);
			this.cboReports.TabIndex = 0;
			this.cboReports.SelectedIndexChanged += new System.EventHandler(this.cboReports_SelectedIndexChanged);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(449, 15);
			this.lblInstructions.TabIndex = 12;
			this.lblInstructions.Text = "PLEASE SELECT THE REPORT YOU WISH TO PRINT AND CLICK PROCESS";
			// 
			// cmdFileDelete
			// 
			this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDelete.AppearanceKey = "toolbarButton";
			this.cmdFileDelete.Location = new System.Drawing.Point(552, 29);
			this.cmdFileDelete.Name = "cmdFileDelete";
			this.cmdFileDelete.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdFileDelete.Size = new System.Drawing.Size(54, 24);
			this.cmdFileDelete.TabIndex = 2;
			this.cmdFileDelete.Text = "Delete";
			this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdFileSaveExit
			// 
			this.cmdFileSaveExit.AppearanceKey = "acceptButton";
			this.cmdFileSaveExit.Location = new System.Drawing.Point(30, 465);
			this.cmdFileSaveExit.Name = "cmdFileSaveExit";
			this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSaveExit.Size = new System.Drawing.Size(110, 48);
			this.cmdFileSaveExit.TabIndex = 0;
			this.cmdFileSaveExit.Text = "Process";
			this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveProcess_Click);
			// 
			// frmExpenseDetailVendorSummarySelect
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(626, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmExpenseDetailVendorSummarySelect";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Expense - Vendor Summary Report";
			this.Load += new System.EventHandler(this.frmExpenseDetailVendorSummarySelect_Load);
			this.Activated += new System.EventHandler(this.frmExpenseDetailVendorSummarySelect_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExpenseDetailVendorSummarySelect_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExpenseDetailVendorSummarySelect_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRangeSelection)).EndInit();
			this.fraRangeSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).EndInit();
			this.fraAccountRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraReportSelection)).EndInit();
			this.fraReportSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditFormat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditCriteria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreateFormat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreateCriteria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSelection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDefault)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
