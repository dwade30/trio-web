﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournalAccountReports.
	/// </summary>
	public partial class rptJournalAccountReports : BaseSectionReport
	{
		public static rptJournalAccountReports InstancePtr
		{
			get
			{
				return (rptJournalAccountReports)Sys.GetInstance(typeof(rptJournalAccountReports));
			}
		}

		protected rptJournalAccountReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMasterInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptJournalAccountReports	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		string strDateRange = "";
		// reporting range A - All, M - Month Range, S - Single Month
		int intLowDate;
		int intHighDate;
		DateTime datStart;
		DateTime datEnd;
		// vbPorter upgrade warning: curDebitsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitsTotal;
		// vbPorter upgrade warning: curCreditsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditsTotal;
		// vbPorter upgrade warning: curDebitsGrandTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitsGrandTotal;
		// vbPorter upgrade warning: curCreditsGrandTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditsGrandTotal;
		bool blnAP;
		bool blnCR;
		bool blnCD;
		// booleans to let the program know what types of journals we are listing
		bool blnGJ;
		bool blnPY;
		bool blnEnc;
		string strJournalTypeSQL = "";
		string strJournalType;
		string strPeriodCheck = "";
		clsDRWrapper rsMasterInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		bool blnDetail;

		public rptJournalAccountReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Journal Account Summary Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("AccountBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "E")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "R")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
				}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "G")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
				}
				eArgs.EOF = false;
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile())
				{
					rsMasterInfo.MoveNext();
					if (rsMasterInfo.EndOfFile())
					{
						eArgs.EOF = true;
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "E")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsDetailInfo.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
						}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "G")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' ORDER BY TransDate, JournalNumber");
						}
						eArgs.EOF = false;
					}
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			if (!eArgs.EOF)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				this.Fields["AccountBinder"].Value = rsMasterInfo.Get_Fields("Account");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			if (strDateRange == "S")
			{
				lblDateRange.Text = MonthCalc(intLowDate);
			}
			else if (strDateRange == "M")
			{
				lblDateRange.Text = MonthCalc(intLowDate) + " to " + MonthCalc(intHighDate);
			}
			else if (strDateRange == "D")
			{
				lblDateRange.Text = Strings.Format(datStart, "MM/dd/yyyy") + " to " + Strings.Format(datEnd, "MM/dd/yyyy");
			}
			else
			{
				lblDateRange.Text = "ALL Months";
			}
			blnDetail = frmJournalAccountReports.InstancePtr.blnDetail;
			if (blnDetail)
			{
				fldSummaryCredits.Visible = false;
				fldSummaryDebits.Visible = false;
				fldPeriod.Visible = true;
				fldJournal.Visible = true;
				fldType.Visible = true;
				fldRCB.Visible = true;
				fldDate.Visible = true;
				fldDescription.Visible = true;
				fldDebit.Visible = true;
				fldCredit.Visible = true;
				Line3.Visible = true;
				fldTotalCredit.Visible = true;
				fldTotalDebit.Visible = true;
				lblAccount.Font = new Font(lblAccount.Font, FontStyle.Bold);
				Label8.Visible = true;
				this.GroupHeader1.CanShrink = false;
				this.GroupHeader1.Height = 0.344f;
				lblAccount.Width = 10000 / 1440f;
				lblPeriod.Visible = true;
				lblJournal.Visible = true;
				lblType.Visible = true;
				lblRCB.Visible = true;
				lblDate.Visible = true;
				lblDescription.Visible = true;
				fldAccount.Visible = false;
				lblAccount.Top = 180 / 1440f;
				fldSummaryCredits.Top = 180 / 1440f;
				fldSummaryDebits.Top = 180 / 1440f;
			}
			else
			{
				fldSummaryCredits.Visible = true;
				fldSummaryDebits.Visible = true;
				fldPeriod.Visible = false;
				fldJournal.Visible = false;
				fldType.Visible = false;
				fldRCB.Visible = false;
				fldDate.Visible = false;
				fldDescription.Visible = false;
				fldDebit.Visible = false;
				fldCredit.Visible = false;
				Line3.Visible = false;
				fldTotalCredit.Visible = false;
				fldTotalDebit.Visible = false;
				lblAccount.Font = new Font(lblAccount.Font, FontStyle.Regular);
				Label8.Visible = false;
				//FC:FINAL:SBE: - #514 - lines are being cut off at the bottom of pages after exporting it to PDF. Shrink group header manually, by setting the Height
				this.GroupHeader1.CanShrink = false;
				this.GroupHeader1.Height = 0.2f;
				lblAccount.Width = 7470 / 1440F;
				lblPeriod.Visible = false;
				lblJournal.Visible = false;
				lblType.Visible = false;
				lblRCB.Visible = false;
				lblDate.Visible = false;
				lblDescription.Visible = false;
				fldAccount.Visible = true;
				lblAccount.Top = 0;
				fldSummaryCredits.Top = 0;
				fldSummaryDebits.Top = 0;
			}
			strJournalType = "";
			if (blnAP && blnCR && blnCD && blnGJ && blnEnc)
			{
				lblDeptRange.Text = "All Journal Types";
			}
			else
			{
				if (blnAP)
				{
					strJournalType += "AP, ";
				}
				if (blnCR)
				{
					strJournalType += "CR, ";
				}
				if (blnCD)
				{
					strJournalType += "CD, ";
				}
				if (blnGJ)
				{
					strJournalType += "GJ, ";
				}
				if (blnPY)
				{
					strJournalType += "PY, ";
				}
				if (blnEnc)
				{
					strJournalType += "EN, ";
				}
				strJournalType = Strings.Left(strJournalType, strJournalType.Length - 2);
				if (strJournalType.Length == 2)
				{
					strJournalType += " Journal Type";
				}
				else
				{
					strJournalType = Strings.Left(strJournalType, strJournalType.Length - 2) + " And " + Strings.Right(strJournalType, 2) + " Journal Types";
				}
				lblDeptRange.Text = strJournalType;
			}
			curDebitsGrandTotal = 0;
			curCreditsGrandTotal = 0;
			curDebitsTotal = 0;
			curCreditsTotal = 0;
			frmJournalAccountReports.InstancePtr.Unload();
			if (rsMasterInfo.EndOfFile() != true && rsMasterInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No info found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000");
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = Strings.Format(rsDetailInfo.Get_Fields("Period"), "00");
			fldRCB.Text = rsDetailInfo.Get_Fields_String("RCB");
			fldDate.Text = Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + rsDetailInfo.Get_Fields("JournalNumber"));
			if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldType.Text = FCConvert.ToString(rsJournalInfo.Get_Fields("Type"));
			}
			else
			{
				fldType.Text = "UN";
			}
			fldDescription.Text = rsDetailInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") > 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				fldDebit.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curDebitsTotal += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curDebitsGrandTotal += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
				fldCredit.Text = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				fldCredit.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1, "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curCreditsTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1);
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curCreditsGrandTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1);
				fldDebit.Text = "";
			}
			rsJournalInfo.Dispose();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (blnDetail)
			{
				fldTotalDebit.Text = Strings.Format(curDebitsTotal, "#,##0.00");
				fldTotalCredit.Text = Strings.Format(curCreditsTotal, "#,##0.00");
				curDebitsTotal = 0;
				curCreditsTotal = 0;
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldGrandTotalDebit.Text = Strings.Format(curDebitsGrandTotal, "#,##0.00");
			fldGrandTotalCredit.Text = Strings.Format(curCreditsGrandTotal, "#,##0.00");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsSummaryDebits = new clsDRWrapper();
			clsDRWrapper rsSummaryCredits = new clsDRWrapper();
			if (rsMasterInfo.EndOfFile())
			{
				rsMasterInfo.MovePrevious();
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccount.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Account")) + "  " + modAccountTitle.ReturnAccountDescription(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")));
			if (!blnDetail)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "E")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryDebits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM ExpenseDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) >= 0  GROUP BY Account");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryCredits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM ExpenseDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) < 0  GROUP BY Account");
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "R")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryDebits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM RevenueDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) >= 0  GROUP BY Account");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryCredits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM RevenueDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) < 0  GROUP BY Account");
				}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					else if (Strings.Left(FCConvert.ToString(rsMasterInfo.Get_Fields("Account")), 1) == "G")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryDebits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM LedgerDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) >= 0  GROUP BY Account");
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsSummaryCredits.OpenRecordset("SELECT Account, SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM LedgerDetailInfo WHERE " + strJournalTypeSQL + "AND Account = '" + rsMasterInfo.Get_Fields("Account") + "' AND Amount - isnull(Discount, 0) < 0  GROUP BY Account");
				}
				if (rsSummaryDebits.EndOfFile() != true && rsSummaryDebits.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
					fldSummaryDebits.Text = Strings.Format(Conversion.Val(rsSummaryDebits.Get_Fields_Decimal("TotalAmount")) - Conversion.Val(rsSummaryDebits.Get_Fields("TotalDiscount")), "#,##0.00");
				}
				else
				{
					fldSummaryDebits.Text = "0.00";
				}
				if (rsSummaryCredits.EndOfFile() != true && rsSummaryCredits.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
					fldSummaryCredits.Text = Strings.Format((Conversion.Val(rsSummaryCredits.Get_Fields_Decimal("TotalAmount")) - Conversion.Val(rsSummaryCredits.Get_Fields("TotalDiscount"))) * -1, "#,##0.00");
				}
				else
				{
					fldSummaryCredits.Text = "0.00";
				}
				curDebitsTotal = 0;
				curCreditsTotal = 0;
			}
			rsSummaryCredits.Dispose();
			rsSummaryDebits.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}
		// vbPorter upgrade warning: intLowMonth As short	OnWrite(string)
		// vbPorter upgrade warning: intHighMonth As short	OnWrite(string)
		// vbPorter upgrade warning: datStartDate As DateTime	OnWrite(string)
		// vbPorter upgrade warning: datEndDate As DateTime	OnWrite(string)
		public void Init(bool blnAPJournals, bool blnCRJournals, bool blnCDJournals, bool blnGJJournals, bool blnPYJournals, bool blnEncJournals, string strMonthRange, short intLowMonth = 0, short intHighMonth = 0, DateTime? datStartDateTemp = null, DateTime? datEndDateTemp = null)
		{
			//FC:FINAL:PJ initialize Date-Values correctly
			DateTime datStartDate = datStartDateTemp ?? FCConvert.ToDateTime("1/1/1899");
			DateTime datEndDate = datEndDateTemp ?? FCConvert.ToDateTime("1/1/1899");
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			blnAP = blnAPJournals;
			blnCR = blnCRJournals;
			blnCD = blnCDJournals;
			blnGJ = blnGJJournals;
			blnPY = blnPYJournals;
			blnEnc = blnEncJournals;
			strDateRange = strMonthRange;
			intLowDate = intLowMonth;
			intHighDate = intHighMonth;
			datStart = datStartDate;
			datEnd = datEndDate;
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo False, True, True, "E"
			// CalculateAccountInfo False, True, True, "R"
			// CalculateAccountInfo False, True, True, "G"
			//Application.DoEvents();
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			// create sql statement for journal types based on what the user selected on the frmJournalListing form
			strJournalTypeSQL = "(";
			if (blnAP)
			{
				strJournalTypeSQL += "Type = 'A' OR ";
			}
			if (blnCR)
			{
				strJournalTypeSQL += "Type = 'C' OR Type = 'W' OR ";
			}
			if (blnCD)
			{
				strJournalTypeSQL += "Type = 'D' OR ";
			}
			if (blnGJ)
			{
				strJournalTypeSQL += "Type = 'G' OR ";
			}
			if (blnPY)
			{
				strJournalTypeSQL += "Type = 'P' OR ";
			}
			if (blnEnc)
			{
				strJournalTypeSQL += "Type = 'E' OR ";
			}
			strJournalTypeSQL = Strings.Left(strJournalTypeSQL, strJournalTypeSQL.Length - 4) + ") AND (Status = 'P')";
			// get information on any journals that match the search criteria
			if (strDateRange == "A")
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM (SELECT DISTINCT Account FROM ExpenseDetailInfo WHERE (" + strJournalTypeSQL + ") UNION ALL SELECT DISTINCT Account FROM LedgerDetailInfo WHERE (" + strJournalTypeSQL + ") UNION ALL SELECT DISTINCT Account FROM RevenueDetailInfo WHERE (" + strJournalTypeSQL + ") ) as temp ORDER BY Account");
			}
			else if (strDateRange != "D")
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM (SELECT DISTINCT Account FROM ExpenseDetailInfo WHERE (" + strJournalTypeSQL + ") AND (Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + ") UNION ALL SELECT DISTINCT Account FROM LedgerDetailInfo WHERE (" + strJournalTypeSQL + ") AND (Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + ") UNION ALL SELECT DISTINCT Account FROM RevenueDetailInfo WHERE (" + strJournalTypeSQL + ") AND (Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + ") ) as temp ORDER BY Account");
				strJournalTypeSQL = "(" + strJournalTypeSQL + ") AND (Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + ")";
			}
			else
			{
				rsMasterInfo.OpenRecordset("SELECT * FROM (SELECT DISTINCT Account FROM ExpenseDetailInfo WHERE (" + strJournalTypeSQL + ") AND (TransDate >= '" + FCConvert.ToString(datStart) + "' AND TransDate <= '" + FCConvert.ToString(datEnd) + "') UNION ALL SELECT DISTINCT Account FROM LedgerDetailInfo WHERE (" + strJournalTypeSQL + ") AND (TransDate >= '" + FCConvert.ToString(datStart) + "' AND TransDate <= '" + FCConvert.ToString(datEnd) + "') UNION ALL SELECT DISTINCT Account FROM RevenueDetailInfo WHERE (" + strJournalTypeSQL + ") AND (TransDate >= '" + FCConvert.ToString(datStart) + "' AND TransDate <= '" + FCConvert.ToString(datEnd) + "') ) as temp ORDER BY Account");
				strJournalTypeSQL = "(" + strJournalTypeSQL + ") AND (TransDate >= '" + FCConvert.ToString(datStart) + "' AND TransDate <= '" + FCConvert.ToString(datEnd) + "') ";
			}
			// depending on what the user selected print the report or preview it
			if (frmJournalAccountReports.InstancePtr.blnPrint)
			{
				this.Document.Print(false);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			//Application.DoEvents();
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private void rptJournalAccountReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptJournalAccountReports.Caption	= "Journal Account Summary Report";
			//rptJournalAccountReports.Icon	= "rptJournalAccountReports.dsx":0000";
			//rptJournalAccountReports.Left	= 0;
			//rptJournalAccountReports.Top	= 0;
			//rptJournalAccountReports.Width	= 11880;
			//rptJournalAccountReports.Height	= 8595;
			//rptJournalAccountReports.StartUpPosition	= 3;
			//rptJournalAccountReports.SectionData	= "rptJournalAccountReports.dsx":058A;
			//End Unmaped Properties
		}

		private void rptJournalAccountReports_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
			frmReportViewer.InstancePtr.Unload();
		}
	}
}
