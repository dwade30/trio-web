﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantLandscape.
	/// </summary>
	public partial class rptWarrantLandscape : BaseSectionReport
	{
		public static rptWarrantLandscape InstancePtr
		{
			get
			{
				return (rptWarrantLandscape)Sys.GetInstance(typeof(rptWarrantLandscape));
			}
		}

		protected rptWarrantLandscape _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
				rsJournalInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantLandscape	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		// vbPorter upgrade warning: curJournalTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalTotal;
		// vbPorter upgrade warning: curJournalEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalEncTotal;
		// vbPorter upgrade warning: curVendorTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorTotal;
		// vbPorter upgrade warning: curVendorEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorEncTotal;
		Decimal curWarrantTotal;
		Decimal curWarrantEncTotal;
		Decimal curPrepaidTotal;
		Decimal curEFTTotal;
		Decimal curCurrentTotal;
		bool blnFirstRecord;
		bool blnTempVendors;
		//clsDRWrapper rsTempVendorInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		int intTempVendorCounter;
		string strJournalSQL;
		string strSQL = "";

		public rptWarrantLandscape()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "AP Warrant";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
				{
					intTempVendorCounter = 0;
					rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY CheckNumber, CreditMemoRecord");
					rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
					this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
					this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
				}
				else
				{
					rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY CheckNumber, CreditMemoRecord");
					rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
					this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
					this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
				}
				strJournalSQL = rsJournalInfo.Name();
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() == true)
				{
					rsJournalInfo.MoveNext();
					bool executeCheckNextVendor = false;
					bool executeGetNextDetails = false;
					bool executeCheckNextJournal = false;
					if (rsJournalInfo.EndOfFile() == true)
					{
						rsVendorInfo.MoveNext();
						executeCheckNextVendor = true;
						goto CheckNextVendor;
					}
					else
					{
						executeGetNextDetails = true;
						goto GetNextDetail;
					}
					CheckNextVendor:
					;
					if (executeCheckNextVendor)
					{
						if (rsVendorInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							GetFirstVendor:
							;
							if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY CheckNumber, CreditMemoRecord");
							}
							else
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY CheckNumber, CreditMemoRecord");
							}
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
					GetNextDetail:
					;
					if (executeGetNextDetails)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
						{
							this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
							this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
							eArgs.EOF = false;
							return;
						}
						else
						{
							rsJournalInfo.MoveNext();
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
					CheckNextJournal:
					;
					if (executeCheckNextJournal)
					{
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							executeGetNextDetails = true;
							goto GetNextDetail;
						}
						else
						{
							rsVendorInfo.MoveNext();
							executeCheckNextVendor = true;
							goto CheckNextVendor;
						}
					}
				}
				else
				{
					this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
					this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					eArgs.EOF = false;
					return;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
			if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Reprint WHERE Type = 'W' ORDER BY ReportOrder");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ReportOrder")) < 9)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReportOrder", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportOrder")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						else
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("Type", "W");
				rsInfo.Set_Fields("ReportOrder", 1);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsInfo.Set_Fields("WarrantNumber", frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant"));
				rsInfo.Update(true);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'R' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant") + "')");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					do
					{
						frmWarrant.InstancePtr.clsJournalInfo = new clsPostingJournalInfo();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						frmWarrant.InstancePtr.clsJournalInfo.JournalNumber = FCConvert.ToInt32(rsMasterJournal.Get_Fields("JournalNumber"));
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						frmWarrant.InstancePtr.clsJournalInfo.JournalType = FCConvert.ToString(rsMasterJournal.Get_Fields("Type"));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						frmWarrant.InstancePtr.clsJournalInfo.Period = FCConvert.ToString(rsMasterJournal.Get_Fields("Period"));
						frmWarrant.InstancePtr.clsJournalInfo.CheckDate = FCConvert.ToString(rsMasterJournal.Get_Fields_DateTime("CheckDate"));
						frmWarrant.InstancePtr.clsPostInfo.AddJournal(frmWarrant.InstancePtr.clsJournalInfo);
						rsMasterJournal.Edit();
						rsMasterJournal.Set_Fields("Status", "W");
						rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
						rsMasterJournal.Update(true);
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
				}
			}
            //FC:FINAL:SBE - #844 - if only one warrant record was printed, then we have to close also the form.
            if (frmWarrant.InstancePtr.rsInfo.RecordCount() <= 1)
            {
                //FC:FINAL:MSH - Issue #721: 'Close()' replaced by 'Hide()', so we need to close 'frmWarrant' to display it correctly next time.
                frmWarrant.InstancePtr.Close();
            }
			rsInfo.Dispose();
			rsMasterJournal.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 15100 / 1440F;
			Line1.X2 = this.PrintWidth - 400 / 1440F;
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
			if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
			{
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				Label6.Text = "Warrant " + frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant");
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				strSQL = "APJournal.Returned <> 'R' AND APJournal.Returned <> 'D' AND APJournal.Warrant = '" + frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant") + "' ";
				Label21.Visible = false;
			}
			else
			{
				Label6.Text = "Warrant " + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrant.InstancePtr.cboReport.Text, 9, 4)));
				strSQL = "APJournal.Returned = 'N' AND APJournal.Warrant = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrant.InstancePtr.cboReport.Text, 9, 4))) + "' ";
				Label21.Visible = true;
			}
			blnFirstRecord = true;
			blnTempVendors = false;
			// Me.Document.Printer.PrinterName = frmWarrant.strPrinterName
			FillTemp();
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Cancel();
				return;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptAccountsPayable))
			{
				if (!frmWarrant.InstancePtr.blnAsked)
				{
					if (MessageBox.Show("Would you like to post the journal(s)?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						frmWarrant.InstancePtr.blnPostJournal = true;
					}
					else
					{
						frmWarrant.InstancePtr.blnPostJournal = false;
					}
					frmWarrant.InstancePtr.blnAsked = true;
				}
			}
		}

		public void Init()
		{
			//FC:FINAL:BBE:#i679 - show report viewer Modeless
			//frmReportViewer.InstancePtr.Init(this, frmWarrant.InstancePtr.strPrinterName, FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true, true);
			frmReportViewer.InstancePtr.Init(this, frmWarrant.InstancePtr.strPrinterName, FCConvert.ToInt16(FCForm.FormShowEnum.Modeless), false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true, true);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDetailDescription.Text = rsDetailInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetailInfo.Get_Fields_String("Account");
			fldProject.Text = rsDetailInfo.Get_Fields_String("Project");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
			fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			ShowTitle();
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curJournalTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curJournalEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curVendorTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curVendorEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curWarrantTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			curWarrantEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
			if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("PrepaidCheck")))
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curPrepaidTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
			else if (rsJournalInfo.Get_Fields_Boolean("EFTCheck"))
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curEFTTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curCurrentTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldVendorAmountTotal.Text = Strings.Format(curVendorTotal, "#,##0.00");
			fldVendorEncumbranceTotal.Text = "";
			// format(curVendorEncTotal, "#,##0.00")
			curVendorEncTotal = 0;
			curVendorTotal = 0;
			if (rsVendorInfo.EndOfFile())
			{
				Line2.Visible = false;
			}
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			if (ShowJournalTotal())
			{
				Line3.Visible = true;
				fldJournalTitle.Visible = true;
				fldJournalAmountTotal.Text = Strings.Format(curJournalTotal, "#,##0.00");
				fldJournalEncumbranceTotal.Text = "";
				// format(curJournalEncTotal, "#,##0.00")
			}
			else
			{
				Line3.Visible = false;
				fldJournalTitle.Visible = false;
				fldJournalAmountTotal.Text = "";
				fldJournalEncumbranceTotal.Text = "";
			}
			curJournalEncTotal = 0;
			curJournalTotal = 0;
			strJournalSQL = rsJournalInfo.Name();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			//clsDRWrapper rsVendorName = new clsDRWrapper();
			fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsVendorInfo.Get_Fields_String("VendorName");
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("JournalNumber"), 4);
			fldDescription.Text = rsJournalInfo.Get_Fields_String("Description");
			fldReference.Text = rsJournalInfo.Get_Fields_String("Reference");
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			fldCheckNumber.Text = FCConvert.ToString(rsJournalInfo.Get_Fields("CheckNumber"));
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldMonth.Text = Strings.Format(rsJournalInfo.Get_Fields("Period"), "00");
			if (rsJournalInfo.Get_Fields_Boolean("Seperate") == true)
			{
				fldSeperate.Visible = true;
			}
			else
			{
				fldSeperate.Visible = false;
			}
			//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
			if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
			{
				rsJournalInfo.Edit();
				if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
				{
					rsJournalInfo.Set_Fields("Status", "W");
				}
				rsJournalInfo.Update(true);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("VendorBinder");
			this.Fields.Add("JournalBinder");
		}

		private void ShowTitle()
		{
			int intHolder = 0;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
			{
				if (frmWarrant.InstancePtr.intPreviewChoice == 1)
				{
					fldAccount.Text += "  " + "";
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modAccountTitle.DetermineAccountTitle(rsDetailInfo.Get_Fields("Account"), ref frmWarrant.InstancePtr.lblTitle);
				}
				if (frmWarrant.InstancePtr.intPreviewChoice == 2)
				{
					fldAccount.Text += "  " + frmWarrant.InstancePtr.lblTitle.Text;
				}
				else if (frmWarrant.InstancePtr.intPreviewChoice == 3)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldAccount.Text += "  " + Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldAccount.Text += "  " + Strings.Left(frmWarrant.InstancePtr.lblTitle.Text, intHolder - 1);
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldAccount.Text += "  " + Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldAccount.Text += "  " + Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder - 1);
					}
				}
			}
			else
			{
				if (frmWarrant.InstancePtr.intPreviewChoice == 1)
				{
					fldAccount.Text += "  " + "";
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modAccountTitle.DetermineAccountTitle(rsDetailInfo.Get_Fields("Account"), ref frmWarrant.InstancePtr.lblTitle);
					fldAccount.Text += "  " + frmWarrant.InstancePtr.lblTitle.Text;
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsSignatureInfo = new clsDRWrapper())
            {
                fldPrepaidAmountTotal.Text = Strings.Format(curPrepaidTotal, "#,##0.00");
                fldCurrentAmountTotal.Text = Strings.Format(curCurrentTotal, "#,##0.00");
                fldEFTAmountTotal.Text = Strings.Format(curEFTTotal, "#,##0.00");
                fldWarrantAmountTotal.Text = Strings.Format(curWarrantTotal, "#,##0.00");
                fldWarrantEncumbranceTotal.Text = "";
                // format(curWarrantEncTotal, "#,##0.00")
                rsSignatureInfo.OpenRecordset("SELECT * FROM WarrantMessage");
                if (rsSignatureInfo.EndOfFile() != true && rsSignatureInfo.BeginningOfFile() != true)
                {
                    fldWarrantSignature1.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 1, 80);
                    fldWarrantSignature2.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 81, 80);
                    fldWarrantSignature3.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 161, 80);
                    fldWarrantSignature4.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 241, 80);
                    fldWarrantSignature5.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 321, 80);
                    fldWarrantSignature6.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 401, 80);
                    fldWarrantSignature7.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 481, 80);
                    fldWarrantSignature8.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 561, 80);
                    fldWarrantSignature9.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 641, 80);
                    fldWarrantSignature10.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 721, 80);
                    if (Strings.Trim(fldWarrantSignature1.Text) == "")
                    {
                        fldWarrantSignature1.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature2.Text) == "")
                    {
                        fldWarrantSignature2.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature3.Text) == "")
                    {
                        fldWarrantSignature3.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature4.Text) == "")
                    {
                        fldWarrantSignature4.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature5.Text) == "")
                    {
                        fldWarrantSignature5.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature6.Text) == "")
                    {
                        fldWarrantSignature6.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature7.Text) == "")
                    {
                        fldWarrantSignature7.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature8.Text) == "")
                    {
                        fldWarrantSignature8.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature9.Text) == "")
                    {
                        fldWarrantSignature9.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature10.Text) == "")
                    {
                        fldWarrantSignature10.Visible = false;
                    }
                }
                else
                {
                    fldWarrantSignature1.Visible = false;
                    fldWarrantSignature2.Visible = false;
                    fldWarrantSignature3.Visible = false;
                    fldWarrantSignature4.Visible = false;
                    fldWarrantSignature5.Visible = false;
                    fldWarrantSignature6.Visible = false;
                    fldWarrantSignature7.Visible = false;
                    fldWarrantSignature8.Visible = false;
                    fldWarrantSignature9.Visible = false;
                    fldWarrantSignature10.Visible = false;
                }
            }
        }

		private bool ShowJournalTotal()
		{
			bool ShowJournalTotal = false;
            using (clsDRWrapper rsCheck = new clsDRWrapper())
            {
                int intCheck;
                intCheck = Strings.InStr(1, strJournalSQL, "WHERE", CompareConstants.vbBinaryCompare);
                rsCheck.OpenRecordset("SELECT DISTINCT ID FROM APJournal " +
                                      Strings.Mid(strJournalSQL, intCheck, strJournalSQL.Length - (intCheck + 38)));
                if (rsCheck.RecordCount() > 1)
                {
                    ShowJournalTotal = true;
                }
                else
                {
                    ShowJournalTotal = false;
                }
            }

            return ShowJournalTotal;
		}

		private void FillTemp()
		{
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
			// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
			strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries' ORDER BY TempVendorName";
			// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorNumber";
				}
				else
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorName";
				}
			}
			else
			{
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo ORDER BY VendorNumber";
			}
			rsVendorInfo.OpenRecordset(strTemp);
		}

		

		private void rptWarrantLandscape_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
	}
}
