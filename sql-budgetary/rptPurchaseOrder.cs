﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptPurchaseOrder.
	/// </summary>
	public partial class rptPurchaseOrder : BaseSectionReport
	{
		public static rptPurchaseOrder InstancePtr
		{
			get
			{
				return (rptPurchaseOrder)Sys.GetInstance(typeof(rptPurchaseOrder));
			}
		}

		protected rptPurchaseOrder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				rsDetail.Dispose();
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPurchaseOrder	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLineCounter;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		bool blnFirstRecord;
		// vbPorter upgrade warning: lngID As int	OnWrite(string)
		public int lngID;
		public bool blnIncludeAdjustments;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rsDetail = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();

		public rptPurchaseOrder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Purchase Order";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngID == 0)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 4) != "0")
					{
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				CheckNext:
				;
				lngLineCounter += 1;
				if (lngLineCounter <= frmEncumbranceDataEntry.InstancePtr.vs1.Rows - 1)
				{
					if (frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 4) != "0")
					{
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				if (blnFirstRecord)
				{
					eArgs.EOF = false;
					blnFirstRecord = false;
				}
				else
				{
					rsDetail.MoveNext();
					eArgs.EOF = rsDetail.EndOfFile();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				Label2.Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
			}
			else
			{
				Label2.Text = modGlobalConstants.Statics.MuniName;
			}
			if (lngID == 0)
			{
				fldVendorNumber.Text = frmEncumbranceDataEntry.InstancePtr.txtVendor.Text;
				fldVendorName.Text = frmEncumbranceDataEntry.InstancePtr.txtAddress[0].Text;
				fldVendorAddress1.Text = frmEncumbranceDataEntry.InstancePtr.txtAddress[1].Text;
				fldVendorAddress2.Text = frmEncumbranceDataEntry.InstancePtr.txtAddress[2].Text;
				fldVendorAddress3.Text = frmEncumbranceDataEntry.InstancePtr.txtAddress[3].Text;
				if (Strings.Trim(frmEncumbranceDataEntry.InstancePtr.txtZip4.Text) != "")
				{
					fldVendorAddress4.Text = frmEncumbranceDataEntry.InstancePtr.txtCity.Text + ", " + frmEncumbranceDataEntry.InstancePtr.txtState.Text + " " + frmEncumbranceDataEntry.InstancePtr.txtZip.Text + "-" + frmEncumbranceDataEntry.InstancePtr.txtZip4.Text;
				}
				else
				{
					fldVendorAddress4.Text = frmEncumbranceDataEntry.InstancePtr.txtCity.Text + ", " + frmEncumbranceDataEntry.InstancePtr.txtState.Text + " " + frmEncumbranceDataEntry.InstancePtr.txtZip.Text;
				}
				if (Strings.Trim(fldVendorAddress3.Text) == "")
				{
					fldVendorAddress3.Text = fldVendorAddress4.Text;
					fldVendorAddress4.Text = "";
				}
				if (Strings.Trim(fldVendorAddress2.Text) == "")
				{
					fldVendorAddress2.Text = fldVendorAddress3.Text;
					fldVendorAddress3.Text = fldVendorAddress4.Text;
					fldVendorAddress4.Text = "";
				}
				if (Strings.Trim(fldVendorAddress1.Text) == "")
				{
					fldVendorAddress1.Text = fldVendorAddress2.Text;
					fldVendorAddress2.Text = fldVendorAddress3.Text;
					fldVendorAddress3.Text = fldVendorAddress4.Text;
					fldVendorAddress4.Text = "";
				}
				fldName.Text = frmEncumbranceDataEntry.InstancePtr.txtTownName.Text;
				fldAddress1.Text = frmEncumbranceDataEntry.InstancePtr.txtTownAddress[0].Text;
				fldAddress2.Text = frmEncumbranceDataEntry.InstancePtr.txtTownAddress[1].Text;
				fldAddress3.Text = frmEncumbranceDataEntry.InstancePtr.txtTownAddress[2].Text;
				fldAddress4.Text = frmEncumbranceDataEntry.InstancePtr.txtTownAddress[3].Text;
				fldPO.Text = frmEncumbranceDataEntry.InstancePtr.txtPO.Text;
				fldDescription.Text = frmEncumbranceDataEntry.InstancePtr.txtDescription.Text;
				fldDate.Text = frmEncumbranceDataEntry.InstancePtr.txtDate.Text;
				lngLineCounter = 0;
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(lngID));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					fldVendorNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
					if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
					{
						fldVendorName.Text = rs.Get_Fields_String("TempVendorName");
						fldVendorAddress1.Text = rs.Get_Fields_String("TempVendorAddress1");
						fldVendorAddress2.Text = rs.Get_Fields_String("TempVendorAddress2");
						fldVendorAddress3.Text = rs.Get_Fields_String("TempVendorAddress3");
						if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"))) != "")
						{
							fldVendorAddress4.Text = rs.Get_Fields_String("TempVendorCity") + ", " + rs.Get_Fields_String("TempVendorState") + " " + rs.Get_Fields_String("TempVendorZip") + "-" + rs.Get_Fields_String("TempVendorZip4");
						}
						else
						{
							fldVendorAddress4.Text = rs.Get_Fields_String("TempVendorCity") + ", " + rs.Get_Fields_String("TempVendorState") + " " + rs.Get_Fields_String("TempVendorZip");
						}
					}
					else
					{
						rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
						if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
						{
							fldVendorName.Text = rsVendorInfo.Get_Fields_String("CheckName");
							fldVendorAddress1.Text = rsVendorInfo.Get_Fields_String("CheckAddress1");
							fldVendorAddress2.Text = rsVendorInfo.Get_Fields_String("CheckAddress2");
							fldVendorAddress3.Text = rsVendorInfo.Get_Fields_String("CheckAddress3");
							if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
							{
								fldVendorAddress4.Text = rsVendorInfo.Get_Fields_String("CheckCity") + ", " + rsVendorInfo.Get_Fields_String("CheckState") + " " + rsVendorInfo.Get_Fields_String("CheckZip") + "-" + rsVendorInfo.Get_Fields_String("CheckZip4");
							}
							else
							{
								fldVendorAddress4.Text = rsVendorInfo.Get_Fields_String("CheckCity") + ", " + rsVendorInfo.Get_Fields_String("CheckState") + " " + rsVendorInfo.Get_Fields_String("CheckZip");
							}
						}
						else
						{
							fldVendorName.Text = "UNKNOWN";
							fldVendorAddress1.Text = "";
							fldVendorAddress2.Text = "";
							fldVendorAddress3.Text = "";
							fldVendorAddress4.Text = "";
						}
					}
					if (Strings.Trim(fldVendorAddress3.Text) == "")
					{
						fldVendorAddress3.Text = fldVendorAddress4.Text;
						fldVendorAddress4.Text = "";
					}
					if (Strings.Trim(fldVendorAddress2.Text) == "")
					{
						fldVendorAddress2.Text = fldVendorAddress3.Text;
						fldVendorAddress3.Text = fldVendorAddress4.Text;
						fldVendorAddress4.Text = "";
					}
					if (Strings.Trim(fldVendorAddress1.Text) == "")
					{
						fldVendorAddress1.Text = fldVendorAddress2.Text;
						fldVendorAddress2.Text = fldVendorAddress3.Text;
						fldVendorAddress3.Text = fldVendorAddress4.Text;
						fldVendorAddress4.Text = "";
					}
					fldName.Text = frmPrintPurchaseOrder.InstancePtr.txtTownName.Text;
					fldAddress1.Text = frmPrintPurchaseOrder.InstancePtr.txtTownAddress[0].Text;
					fldAddress2.Text = frmPrintPurchaseOrder.InstancePtr.txtTownAddress[1].Text;
					fldAddress3.Text = frmPrintPurchaseOrder.InstancePtr.txtTownAddress[2].Text;
					fldAddress4.Text = frmPrintPurchaseOrder.InstancePtr.txtTownAddress[3].Text;
					// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					fldPO.Text = FCConvert.ToString(rs.Get_Fields("PO"));
					fldDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
					fldDate.Text = Strings.Format(rs.Get_Fields_DateTime("EncumbrancesDate"), "MM/dd/yyyy");
					rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(lngID));
				}
				else
				{
					MessageBox.Show("No information found for that PO.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Cancel();
					return;
				}
			}
			blnFirstRecord = true;
			curTotal = 0;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (lngID == 0)
			{
				frmEncumbranceDataEntry.InstancePtr.Visible = true;
			}
			else
			{
				frmPrintPurchaseOrder.InstancePtr.Visible = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngID == 0)
			{
				fldDetailDescription.Text = frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 1);
				fldAccount.Text = frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 2);
				fldAmount.Text = Strings.Format(frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 4), "$#,##0.00");
			}
			else
			{
				fldDetailDescription.Text = rsDetail.Get_Fields_String("Description");
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = rsDetail.Get_Fields_String("Account");
				if (blnIncludeAdjustments)
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					fldAmount.Text = Strings.Format(rsDetail.Get_Fields("Amount") + rsDetail.Get_Fields_Decimal("Adjustments"), "$#,##0.00");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					fldAmount.Text = Strings.Format(rsDetail.Get_Fields("Amount"), "$#,##0.00");
				}
			}
			curTotal += FCConvert.ToDecimal(fldAmount.Text.Trim('$'));
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotal, "$#,##0.00");
		}

		

		private void rptPurchaseOrder_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
