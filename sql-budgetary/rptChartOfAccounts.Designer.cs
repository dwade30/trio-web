﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptChartOfAccounts.
	/// </summary>
	partial class rptChartOfAccounts
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptChartOfAccounts));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccountType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExtraInfo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccoutType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblExtraInfo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldHeadingAccountType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingHeading1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingExtraInfo1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingAccountType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingHeading2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldHeadingExtraInfo2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtraInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccoutType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingAccountType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingHeading1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingExtraInfo1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingAccountType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingHeading2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingExtraInfo2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccountType,
				this.fldDescription,
				this.fldHeading,
				this.fldExtraInfo
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccountType
			// 
			this.fldAccountType.Height = 0.1875F;
			this.fldAccountType.Left = 0.09375F;
			this.fldAccountType.Name = "fldAccountType";
			this.fldAccountType.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccountType.Text = "Field1";
			this.fldAccountType.Top = 0F;
			this.fldAccountType.Width = 1.0625F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 1.28125F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field2";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.75F;
			// 
			// fldHeading
			// 
			this.fldHeading.Height = 0.1875F;
			this.fldHeading.Left = 3.1875F;
			this.fldHeading.Name = "fldHeading";
			this.fldHeading.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldHeading.Text = "Field3";
			this.fldHeading.Top = 0F;
			this.fldHeading.Width = 3.03125F;
			// 
			// fldExtraInfo
			// 
			this.fldExtraInfo.Height = 0.1875F;
			this.fldExtraInfo.Left = 6.34375F;
			this.fldExtraInfo.Name = "fldExtraInfo";
			this.fldExtraInfo.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldExtraInfo.Text = "Field4";
			this.fldExtraInfo.Top = 0F;
			this.fldExtraInfo.Width = 0.90625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblTitle1,
				this.lblAccoutType,
				this.Label16,
				this.Label17,
				this.lblExtraInfo,
				this.Line1
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Chart of Accounts";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.1875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 1.5F;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblTitle1.Text = "Expense";
			this.lblTitle1.Top = 0.1875F;
			this.lblTitle1.Width = 4.6875F;
			// 
			// lblAccoutType
			// 
			this.lblAccoutType.Height = 0.1875F;
			this.lblAccoutType.HyperLink = null;
			this.lblAccoutType.Left = 0.09375F;
			this.lblAccoutType.Name = "lblAccoutType";
			this.lblAccoutType.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblAccoutType.Text = null;
			this.lblAccoutType.Top = 0.53125F;
			this.lblAccoutType.Width = 1.0625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1.28125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "Description------";
			this.Label16.Top = 0.53125F;
			this.Label16.Width = 1.75F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.1875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "Heading---------------------------------";
			this.Label17.Top = 0.53125F;
			this.Label17.Width = 3.03125F;
			// 
			// lblExtraInfo
			// 
			this.lblExtraInfo.Height = 0.1875F;
			this.lblExtraInfo.HyperLink = null;
			this.lblExtraInfo.Left = 6.34375F;
			this.lblExtraInfo.Name = "lblExtraInfo";
			this.lblExtraInfo.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblExtraInfo.Text = null;
			this.lblExtraInfo.Top = 0.53125F;
			this.lblExtraInfo.Width = 0.90625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.71875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 7.46875F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 0.71875F;
			this.Line1.Y2 = 0.71875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldHeadingAccountType1,
				this.fldHeadingDescription1,
				this.fldHeadingHeading1,
				this.fldHeadingExtraInfo1,
				this.fldHeadingAccountType2,
				this.fldHeadingDescription2,
				this.fldHeadingHeading2,
				this.fldHeadingExtraInfo2,
				this.Binder
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.4166667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldHeadingAccountType1
			// 
			this.fldHeadingAccountType1.CanShrink = true;
			this.fldHeadingAccountType1.Height = 0.1875F;
			this.fldHeadingAccountType1.Left = 0.09375F;
			this.fldHeadingAccountType1.Name = "fldHeadingAccountType1";
			this.fldHeadingAccountType1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldHeadingAccountType1.Text = "Field1";
			this.fldHeadingAccountType1.Top = 0F;
			this.fldHeadingAccountType1.Width = 1.0625F;
			// 
			// fldHeadingDescription1
			// 
			this.fldHeadingDescription1.CanShrink = true;
			this.fldHeadingDescription1.Height = 0.1875F;
			this.fldHeadingDescription1.Left = 1.28125F;
			this.fldHeadingDescription1.Name = "fldHeadingDescription1";
			this.fldHeadingDescription1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldHeadingDescription1.Text = "Field2";
			this.fldHeadingDescription1.Top = 0F;
			this.fldHeadingDescription1.Width = 1.75F;
			// 
			// fldHeadingHeading1
			// 
			this.fldHeadingHeading1.CanShrink = true;
			this.fldHeadingHeading1.Height = 0.1875F;
			this.fldHeadingHeading1.Left = 3.1875F;
			this.fldHeadingHeading1.Name = "fldHeadingHeading1";
			this.fldHeadingHeading1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldHeadingHeading1.Text = "Field3";
			this.fldHeadingHeading1.Top = 0F;
			this.fldHeadingHeading1.Width = 3.03125F;
			// 
			// fldHeadingExtraInfo1
			// 
			this.fldHeadingExtraInfo1.CanShrink = true;
			this.fldHeadingExtraInfo1.Height = 0.1875F;
			this.fldHeadingExtraInfo1.Left = 6.34375F;
			this.fldHeadingExtraInfo1.Name = "fldHeadingExtraInfo1";
			this.fldHeadingExtraInfo1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.fldHeadingExtraInfo1.Text = "Field4";
			this.fldHeadingExtraInfo1.Top = 0F;
			this.fldHeadingExtraInfo1.Width = 0.90625F;
			// 
			// fldHeadingAccountType2
			// 
			this.fldHeadingAccountType2.CanShrink = true;
			this.fldHeadingAccountType2.Height = 0.1875F;
			this.fldHeadingAccountType2.Left = 0.09375F;
			this.fldHeadingAccountType2.Name = "fldHeadingAccountType2";
			this.fldHeadingAccountType2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldHeadingAccountType2.Text = "Field1";
			this.fldHeadingAccountType2.Top = 0.21875F;
			this.fldHeadingAccountType2.Width = 1.0625F;
			// 
			// fldHeadingDescription2
			// 
			this.fldHeadingDescription2.CanShrink = true;
			this.fldHeadingDescription2.Height = 0.1875F;
			this.fldHeadingDescription2.Left = 1.28125F;
			this.fldHeadingDescription2.Name = "fldHeadingDescription2";
			this.fldHeadingDescription2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldHeadingDescription2.Text = "Field2";
			this.fldHeadingDescription2.Top = 0.21875F;
			this.fldHeadingDescription2.Width = 1.75F;
			// 
			// fldHeadingHeading2
			// 
			this.fldHeadingHeading2.CanShrink = true;
			this.fldHeadingHeading2.Height = 0.1875F;
			this.fldHeadingHeading2.Left = 3.1875F;
			this.fldHeadingHeading2.Name = "fldHeadingHeading2";
			this.fldHeadingHeading2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldHeadingHeading2.Text = "Field3";
			this.fldHeadingHeading2.Top = 0.21875F;
			this.fldHeadingHeading2.Width = 3.03125F;
			// 
			// fldHeadingExtraInfo2
			// 
			this.fldHeadingExtraInfo2.CanShrink = true;
			this.fldHeadingExtraInfo2.Height = 0.1875F;
			this.fldHeadingExtraInfo2.Left = 6.34375F;
			this.fldHeadingExtraInfo2.Name = "fldHeadingExtraInfo2";
			this.fldHeadingExtraInfo2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldHeadingExtraInfo2.Text = "Field4";
			this.fldHeadingExtraInfo2.Top = 0.21875F;
			this.fldHeadingExtraInfo2.Width = 0.90625F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.19F;
			this.Binder.Left = 0F;
			this.Binder.Name = "Binder";
			this.Binder.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.78125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// rptChartOfAccounts
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptChartOfAccounts_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldAccountType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExtraInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccoutType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingAccountType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingHeading1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingExtraInfo1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingAccountType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingHeading2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldHeadingExtraInfo2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExtraInfo;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccoutType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExtraInfo;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingAccountType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingHeading1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingExtraInfo1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingAccountType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingHeading2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeadingExtraInfo2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
