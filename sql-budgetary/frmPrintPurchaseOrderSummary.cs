﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintPurchaseOrderSummary.
	/// </summary>
	public partial class frmPrintPurchaseOrderSummary : BaseForm
	{
		public frmPrintPurchaseOrderSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbBoth.SelectedIndex = 2;
			cmbVendor.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintPurchaseOrderSummary InstancePtr
		{
			get
			{
				return (frmPrintPurchaseOrderSummary)Sys.GetInstance(typeof(frmPrintPurchaseOrderSummary));
			}
		}

		protected frmPrintPurchaseOrderSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmPrintPurchaseOrderSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			//FC:FINAL:BSE #3119 set focus on correct control
            this.cmbBoth.Focus();
		}

		private void frmPrintPurchaseOrderSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintPurchaseOrderSummary.FillStyle	= 0;
			//frmPrintPurchaseOrderSummary.ScaleWidth	= 3885;
			//frmPrintPurchaseOrderSummary.ScaleHeight	= 1995;
			//frmPrintPurchaseOrderSummary.LinkTopic	= "Form2";
			//frmPrintPurchaseOrderSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPrintPurchaseOrderSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			rptPurchaseOrderSummary.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptPurchaseOrderSummary.InstancePtr);
		}
	}
}
