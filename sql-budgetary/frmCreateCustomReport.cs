﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateCustomReport.
	/// </summary>
	public partial class frmCreateCustomReport : BaseForm
	{
		public frmCreateCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:DDU:#i693 - Added initial index to combobox
			cmbRange.SelectedIndex = 2;
            lblNote.Capitalize = false;
            lblNote.Text = "NOTE:  You may only select 5 fields to display on a report.  Debit / Credit fields count as 2 fields";

        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreateCustomReport InstancePtr
		{
			get
			{
				return (frmCreateCustomReport)Sys.GetInstance(typeof(frmCreateCustomReport));
			}
		}

		protected frmCreateCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/2/02
		// This form will be used to create customizable forms in
		// the budgetary ccounting system
		// ********************************************************
		int KeyCol;
		int RowCol;
		int TypeCol;
		int AccountTypeCol;
		int AccountCol;
		int DSCol;
		int TitleCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool blnEdit;
		bool blnUnload;

		private void chkBalance_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkBalance.CheckState = CheckState.Unchecked;
			}
		}

		private void chkBudget_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkBudget.CheckState = CheckState.Unchecked;
			}
		}

		private void chkCurrent_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkCurrent.CheckState = CheckState.Unchecked;
			}
		}

		private void chkCurrentDebCred_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkCurrentDebCred.CheckState = CheckState.Unchecked;
			}
		}

		private void chkPercent_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkPercent.CheckState = CheckState.Unchecked;
			}
		}

		private void chkYTD_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkYTD.CheckState = CheckState.Unchecked;
			}
		}

		private void chkYTDDebCred_CheckedChanged(object sender, System.EventArgs e)
		{
			if (GetFieldCount() > 5)
			{
				MessageBox.Show("Selecting this field would put you over the limit for fields on this report.  You must unselect another option before you may select this one.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				chkYTDDebCred.CheckState = CheckState.Unchecked;
			}
		}

		private void frmCreateCustomReport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
		}

		private void frmCreateCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateCustomReport.FillStyle	= 0;
			//frmCreateCustomReport.ScaleWidth	= 9045;
			//frmCreateCustomReport.ScaleHeight	= 7560;
			//frmCreateCustomReport.LinkTopic	= "Form2";
			//frmCreateCustomReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			clsDRWrapper rsReportInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			KeyCol = 0;
			RowCol = 1;
			TypeCol = 2;
			AccountTypeCol = 3;
			AccountCol = 4;
			DSCol = 5;
			TitleCol = 6;
			vsCustomFormat.TextMatrix(0, RowCol, "Row");
			vsCustomFormat.TextMatrix(0, TypeCol, "Type");
			vsCustomFormat.TextMatrix(0, AccountTypeCol, "Acct Type");
			vsCustomFormat.TextMatrix(0, AccountCol, "Account(s)");
			vsCustomFormat.TextMatrix(0, DSCol, "D / S");
			vsCustomFormat.TextMatrix(0, TitleCol, "Title");
			vsCustomFormat.ColWidth(KeyCol, 0);
			vsCustomFormat.ColWidth(RowCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.0597014));
			vsCustomFormat.ColWidth(TypeCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.0955233));
			vsCustomFormat.ColWidth(AccountTypeCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.1194029));
			vsCustomFormat.ColWidth(AccountCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.2746268));
			vsCustomFormat.ColWidth(DSCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.083582));
			vsCustomFormat.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCustomFormat.ColAlignment(DSCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCustomFormat.ColAlignment(AccountTypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCustomFormat.ColAlignment(RowCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCustomFormat.ColComboList(TypeCol, "#A;A" + "\t" + "Account|#H;H" + "\t" + "Heading|#S;S" + "\t" + "Subtotal 1|#S2;S2" + "\t" + "Subtotal 2|#S3;S3" + "\t" + "Subtotal 3|#S4;S4" + "\t" + "Subtotal 4|#F;F" + "\t" + "Final Total");
			vsCustomFormat.ColComboList(DSCol, "#D;D" + "\t" + "Detail|#S;S" + "\t" + "Summary");
			vsCustomFormat.ColComboList(AccountTypeCol, "#E;E" + "\t" + "Town Expense|#R;R" + "\t" + "Town Revenue|#G;G" + "\t" + "Town General Ledger");
			if (frmCustomReportSelection.InstancePtr.cboReports.SelectedIndex == 0)
			{
				vsCustomFormat.Rows = 50;
				for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
				{
					vsCustomFormat.TextMatrix(counter, KeyCol, "0");
					vsCustomFormat.TextMatrix(counter, RowCol, FCConvert.ToString(counter));
					vsCustomFormat.TextMatrix(counter, TypeCol, "A");
                    vsCustomFormat.TextMatrix(counter, AccountTypeCol, "E");
					vsCustomFormat.TextMatrix(counter, AccountCol, "");
					vsCustomFormat.TextMatrix(counter, DSCol, "D");
					vsCustomFormat.TextMatrix(counter, TitleCol, "");
				}
				txtDescription.Text = "";
			}
			else
			{
				blnEdit = true;
				rsReportInfo.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(frmCustomReportSelection.InstancePtr.cboReports.Text) + "'");
				rsDetailInfo.OpenRecordset("SELECT * FROM CustomReportsDetail WHERE ReportKey = " + rsReportInfo.Get_Fields_Int32("ID") + " ORDER BY Row");
				txtDescription.Text = FCConvert.ToString(rsReportInfo.Get_Fields_String("Title"));
				if (rsReportInfo.Get_Fields_Boolean("IncludePending") == true)
				{
					chkPending.CheckState = CheckState.Checked;
				}
				else
				{
					chkPending.CheckState = CheckState.Unchecked;
				}
				if (FCConvert.ToString(rsReportInfo.Get_Fields_String("AcctInfo")) == "N")
				{
					chkAccountNumber.CheckState = CheckState.Checked;
					chkAccountDescription.CheckState = CheckState.Unchecked;
				}
				else if (rsReportInfo.Get_Fields_String("AcctInfo") == "D")
				{
					chkAccountDescription.CheckState = CheckState.Checked;
					chkAccountNumber.CheckState = CheckState.Unchecked;
				}
				else
				{
					chkAccountNumber.CheckState = CheckState.Checked;
					chkAccountDescription.CheckState = CheckState.Checked;
				}
				if (rsReportInfo.Get_Fields_Boolean("IncludeEnc") == true)
				{
					chkEncumbrances.CheckState = CheckState.Checked;
				}
				else
				{
					chkEncumbrances.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("Budget") == true)
				{
					chkBudget.CheckState = CheckState.Checked;
				}
				else
				{
					chkBudget.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("CurrentActivity") == true)
				{
					chkCurrent.CheckState = CheckState.Checked;
				}
				else
				{
					chkCurrent.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("YTDActivity") == true)
				{
					chkYTD.CheckState = CheckState.Checked;
				}
				else
				{
					chkYTD.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("YTDDebCred") == true)
				{
					chkYTDDebCred.CheckState = CheckState.Checked;
				}
				else
				{
					chkYTDDebCred.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("CurrentDebCred") == true)
				{
					chkCurrentDebCred.CheckState = CheckState.Checked;
				}
				else
				{
					chkCurrentDebCred.CheckState = CheckState.Unchecked;
				}
				// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
				if (rsReportInfo.Get_Fields("Balance") == true)
				{
					chkBalance.CheckState = CheckState.Checked;
				}
				else
				{
					chkBalance.CheckState = CheckState.Unchecked;
				}
				if (rsReportInfo.Get_Fields_Boolean("PercentSpent") == true)
				{
					chkPercent.CheckState = CheckState.Checked;
				}
				else
				{
					chkPercent.CheckState = CheckState.Unchecked;
				}
				if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "A")
				{
					cmbRange.SelectedIndex = 2;
				}
				else if (rsReportInfo.Get_Fields_String("DateRange") == "R")
				{
					cmbRange.SelectedIndex = 0;
					if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CheckMonthRange")))
					{
						chkCheckDateRange.CheckState = CheckState.Checked;
					}
					else
					{
						chkCheckDateRange.CheckState = CheckState.Unchecked;
						cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate")) - 1;
						cboEndingMonth.SelectedIndex = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("HighDate")) - 1;
					}
				}
				else
				{
					cmbRange.SelectedIndex = 1;
					if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CheckMonthRange")))
					{
						chkCheckDateRange.CheckState = CheckState.Checked;
					}
					else
					{
						chkCheckDateRange.CheckState = CheckState.Unchecked;
						cboSingleMonth.SelectedIndex = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate")) - 1;
					}
				}
				vsCustomFormat.Rows = rsDetailInfo.RecordCount() + 1;
				for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
				{
					vsCustomFormat.TextMatrix(counter, KeyCol, FCConvert.ToString(rsDetailInfo.Get_Fields_Int32("ID")));
					vsCustomFormat.TextMatrix(counter, RowCol, FCConvert.ToString(counter));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					vsCustomFormat.TextMatrix(counter, TypeCol, FCConvert.ToString(rsDetailInfo.Get_Fields("Type")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) != "")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						vsCustomFormat.TextMatrix(counter, AccountTypeCol, Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						vsCustomFormat.TextMatrix(counter, AccountCol, Strings.Trim(Strings.Right(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), FCConvert.ToString(rsDetailInfo.Get_Fields("Account")).Length - 1)));
					}
					vsCustomFormat.TextMatrix(counter, DSCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("DetailSummary")));
					vsCustomFormat.TextMatrix(counter, TitleCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Title")));
					rsDetailInfo.MoveNext();
				}
				blnEdit = false;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			frmCustomReportSelection.InstancePtr.Unload();
			//FC:FINAL:AM: attach event handlers
			this.vsCustomFormat.EditingControlShowing += vsCustomFormat_EditingControlShowing;
		}

		private void frmCreateCustomReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
				frmCustomReportSelection.InstancePtr.Show(App.MainForm);
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void frmCreateCustomReport_Resize(object sender, System.EventArgs e)
		{
			vsCustomFormat.ColWidth(KeyCol, 0);
			vsCustomFormat.ColWidth(RowCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.0597014));
			vsCustomFormat.ColWidth(TypeCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.0955233));
			vsCustomFormat.ColWidth(AccountTypeCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.1194029));
			vsCustomFormat.ColWidth(AccountCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.2746268));
			vsCustomFormat.ColWidth(DSCol, FCConvert.ToInt32(vsCustomFormat.WidthOriginal * 0.083582));
			//FC:FINAL:DDU - if by anchoring the grid is very small, on small resolutions, make it 4-5 rows height visible
			if (vsCustomFormat.Height < 150)
			{
				vsCustomFormat.Height += 150;
			}
		}

		private void mnuFileAccounts_Click(object sender, System.EventArgs e)
		{
			string strAcct;
			strAcct = frmLoadValidAccounts.InstancePtr.Init("");
			if (vsCustomFormat.Col == AccountTypeCol)
			{
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol, Strings.Left(strAcct, 1));
				vsCustomFormat.EditText = Strings.Left(strAcct, 1);
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol, Strings.Right(strAcct, strAcct.Length - 2));
			}
			else if (vsCustomFormat.Col == AccountCol)
			{
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol, Strings.Left(strAcct, 1));
				//FC:FINAL:AM:#i747 - fix wrong code
				//vsCustomFormat.EditText = Strings.Right(modNewAccountBox.Statics.strAccount, strAcct.Length - 2);
				vsCustomFormat.EditText = Strings.Right(strAcct, strAcct.Length - 2);
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol, Strings.Right(strAcct, strAcct.Length - 2));
			}
			else
			{
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol, Strings.Left(strAcct, 1));
				vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol, Strings.Right(strAcct, strAcct.Length - 2));
			}
			vsCustomFormat.Col = TitleCol;
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			if (vsCustomFormat.Row > 0)
			{
				if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, KeyCol) == "0" && vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A" && (vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol) == "E" || vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol) == "P") && vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol) == "" && vsCustomFormat.TextMatrix(vsCustomFormat.Row, DSCol) == "D" && vsCustomFormat.TextMatrix(vsCustomFormat.Row, TitleCol) == "")
				{
					vsCustomFormat.RemoveItem(vsCustomFormat.Row);
				}
				else
				{
					ans = MessageBox.Show("Are you sure you wish to delete this item?", "Delete Row?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						vsCustomFormat.RemoveItem(vsCustomFormat.Row);
					}
					else
					{
						return;
					}
				}
				for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
				{
					vsCustomFormat.TextMatrix(counter, RowCol, FCConvert.ToString(counter));
				}
			}
			else
			{
				MessageBox.Show("You must select a row to delete before you may continue with this process", "Select Row", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			int counter;
			vsCustomFormat.AddItem("", vsCustomFormat.Row);
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, KeyCol, "0");
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, RowCol, "");
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol, "A");
            vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol, "E");
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol, "");
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, DSCol, "D");
			vsCustomFormat.TextMatrix(vsCustomFormat.Row, TitleCol, "");
			for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
			{
				vsCustomFormat.TextMatrix(counter, RowCol, FCConvert.ToString(counter));
			}
			vsCustomFormat.Select(vsCustomFormat.Row, TypeCol);
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnNoData;
			txtDescription.Focus();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("There is data missing in your report that must be filled in before you may continue with this process.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (chkBudget.CheckState == CheckState.Unchecked && chkCurrent.CheckState == CheckState.Unchecked && chkYTD.CheckState == CheckState.Unchecked && chkBalance.CheckState == CheckState.Unchecked && chkPercent.CheckState == CheckState.Unchecked)
			{
				MessageBox.Show("You must select 1 or more items to be included in your report before you may continue.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkAccountNumber.CheckState == CheckState.Unchecked && chkAccountDescription.CheckState == CheckState.Unchecked)
			{
				MessageBox.Show("You must select account information to view in the report before you may continue.", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbRange.SelectedIndex == 0 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.SelectedIndex == 1 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			blnNoData = true;
			for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
			{
				if (vsCustomFormat.TextMatrix(counter, TypeCol) == "H" || vsCustomFormat.TextMatrix(counter, TypeCol) == "S" || vsCustomFormat.TextMatrix(counter, TypeCol) == "F")
				{
					if (Strings.Trim(vsCustomFormat.TextMatrix(counter, TitleCol)) == "")
					{
						// do nothing
					}
					else
					{
						blnNoData = false;
					}
				}
				else
				{
					if (vsCustomFormat.TextMatrix(counter, AccountCol) == "")
					{
						// do nothing
					}
					else
					{
						blnNoData = false;
						if (Strings.InStr(1, vsCustomFormat.TextMatrix(counter, AccountCol), "_", CompareConstants.vbBinaryCompare) != 0)
						{
							MessageBox.Show("There is data missing in your report that must be filled in before you may continue with this process.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsCustomFormat.Select(counter, AccountCol);
							return;
						}
					}
				}
			}
			if (blnNoData)
			{
				MessageBox.Show("You must enter at least 1 line item to show on the report before you may save it.", "Invalid Report Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsCustomFormat.Select(1, TypeCol);
				return;
			}
			//rptShowCustomReportSetup.InstancePtr.Show(App.MainForm);
			frmReportViewer.InstancePtr.Init(rptShowCustomReportSetup.InstancePtr);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private string CreateFormat(string strType)
		{
			string CreateFormat = "";
			string strTemp = "";
			int counter = 0;
			if (strType == "E")
			{
				strTemp = modAccountTitle.Statics.Exp;
			}
			else if (strType == "R")
			{
				strTemp = modAccountTitle.Statics.Rev;
			}
			else if (strType == "G")
			{
				strTemp = modAccountTitle.Statics.Ledger;
			}
			do
			{
				counter = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, 2))));
				strTemp = Strings.Right(strTemp, strTemp.Length - 2);
				if (counter > 0)
				{
					for (counter = counter; counter >= 1; counter--)
					{
						//FC:FINAL:SBE:#4358 - allow only numeric input (in VB6 i is done in KeyDown event, but Wisej doesn't support the same functionality)
                        //CreateFormat = CreateFormat + "A";
                        CreateFormat = CreateFormat + "0";
                    }
					CreateFormat = CreateFormat + "-";
				}
			}
			while (strTemp != "");
			CreateFormat = Strings.Left(CreateFormat, CreateFormat.Length - 1);
			return CreateFormat;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vsCustomFormat_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
            //FC:FINAL:SBE - #4358 - moved same functionality to validateEdit event
            //if (vsCustomFormat.Col == AccountCol)
            //{
            //    //FC:FINAL:SBE:#4358 - check if all prompt chars was replaced or not
            //    string editedText = vsCustomFormat.TextMatrix(vsCustomFormat.Row, vsCustomFormat.Col);
            //    FCMaskedTextBox maskedEditor = vsCustomFormat.EditingControl as FCMaskedTextBox;
            //    //if (Strings.InStr(1, vsCustomFormat.TextMatrix(vsCustomFormat.Row, vsCustomFormat.Col), "_", CompareConstants.vbBinaryCompare) != 0)
            //    if (maskedEditor != null && !maskedEditor.MaskedTextProvider.MaskFull)
            //    {
            //        vsCustomFormat.TextMatrix(vsCustomFormat.Row, vsCustomFormat.Col, "");
            //    }
            //}
            //vsCustomFormat.EditMask = "";
        }

		private void vsCustomFormat_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsCustomFormat.Col == AccountCol)
			{
				vsCustomFormat.EditMask = CreateFormat(vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol));
			}
			else
			{
				vsCustomFormat.EditMask = "";
			}
		}

		private void vsCustomFormat_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsCustomFormat.IsCurrentCellInEditMode)
			{
				if (vsCustomFormat.Col == AccountTypeCol)
				{
					vsCustomFormat.Col = AccountCol;
				}
				else if (vsCustomFormat.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountTypeCol) + " " + vsCustomFormat.EditText);
				}
			}
		}

		private void vsCustomFormat_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vsCustomFormat.Row < vsCustomFormat.Rows - 1)
				{
					if (vsCustomFormat.Col < TitleCol)
					{
						vsCustomFormat.Col = vsCustomFormat.Col + 1;
					}
					else
					{
						vsCustomFormat.Row += 1;
						vsCustomFormat.Col = TypeCol;
					}
				}
				else
				{
					if (vsCustomFormat.Col < TitleCol)
					{
						vsCustomFormat.Col = vsCustomFormat.Col + 1;
					}
					else
					{
						vsCustomFormat.AddItem("");
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, KeyCol, "0");
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, RowCol, FCConvert.ToString(FCConvert.ToInt32(vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 2, RowCol)) + 1));
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, TypeCol, "A");
                        vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, AccountTypeCol, "E");
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, AccountCol, "");
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, DSCol, "D");
						vsCustomFormat.TextMatrix(vsCustomFormat.Row + 1, TitleCol, "");
						vsCustomFormat.Select(vsCustomFormat.Row + 1, TypeCol);
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vsCustomFormat.EditSelStart == 0 && vsCustomFormat.EditSelLength == 0)
				{
					if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A")
					{
						// do nothing
					}
					else
					{
						KeyCode = 0;
						vsCustomFormat.Col = TypeCol;
					}
				}
			}
		}

		//private void vsCustomFormat_KeyPressEdit(object sender, KeyPressEventArgs e)
		//{
		//	//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
		//	if (e.KeyChar == 9)
		//	{
		//		return;
		//	}
		//	int keyAscii = Strings.Asc(e.KeyChar);
		//	if (vsCustomFormat.Col == AccountCol)
		//	{
		//		if (keyAscii == 120)
		//		{
		//			keyAscii -= 32;
		//		}
		//		else if (keyAscii == 88 || keyAscii == 8 || (keyAscii >= 48 && keyAscii <= 57))
		//		{
		//			// do nothing
		//		}
		//		else
		//		{
		//			keyAscii = 0;
		//		}
		//	}
		//}

		private void vsCustomFormat_RowColChange(object sender, System.EventArgs e)
		{
			if (!blnEdit)
			{
				cmdFileAccounts.Enabled = false;
				if (vsCustomFormat.Col < TypeCol)
				{
					vsCustomFormat.Col = TypeCol;
				}
				else if (vsCustomFormat.Col == TypeCol)
				{
					//vsCustomFormat.EditCell();
				}
				else if (vsCustomFormat.Col == AccountTypeCol)
				{
					cmdFileAccounts.Enabled = true;
					if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A")
					{
						//vsCustomFormat.EditCell();
					}
					else
					{
						vsCustomFormat.Col = TitleCol;
					}
				}
				else if (vsCustomFormat.Col == AccountCol)
				{
					cmdFileAccounts.Enabled = true;
					if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A")
					{
						//vsCustomFormat.EditCell();
                        //vsCustomFormat.EditSelStart = 0;
						//vsCustomFormat.EditSelLength = 0;
					}
					else
					{
						vsCustomFormat.Col = TitleCol;
					}
				}
				else if (vsCustomFormat.Col == DSCol)
				{
					if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A")
					{
						//vsCustomFormat.EditCell();
					}
					else
					{
						vsCustomFormat.Col = TitleCol;
					}
				}
				else
				{
					if (vsCustomFormat.TextMatrix(vsCustomFormat.Row, TypeCol) == "A" && vsCustomFormat.TextMatrix(vsCustomFormat.Row, DSCol) == "D" && Strings.InStr(1, vsCustomFormat.TextMatrix(vsCustomFormat.Row, AccountCol), "X", CompareConstants.vbBinaryCompare) != 0)
					{
						if (vsCustomFormat.Row < vsCustomFormat.Rows - 1)
						{
							vsCustomFormat.Select(vsCustomFormat.Row + 1, TypeCol);
						}
						else
						{
							vsCustomFormat.AddItem("");
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, KeyCol, "0");
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, RowCol, FCConvert.ToString(FCConvert.ToInt32(vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 2, RowCol)) + 1));
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, TypeCol, "A");
                            vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, AccountTypeCol, "E");
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, AccountCol, "");
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, DSCol, "D");
							vsCustomFormat.TextMatrix(vsCustomFormat.Rows - 1, TitleCol, "");
							vsCustomFormat.Select(vsCustomFormat.Row + 1, TypeCol);
						}
					}
					else
					{
						//vsCustomFormat.EditCell();
						//vsCustomFormat.EditSelStart = 0;
						//vsCustomFormat.EditSelLength = 0;
					}
				}
			}
		}

		private void vsCustomFormat_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsCustomFormat.GetFlexRowIndex(e.RowIndex);
			int col = vsCustomFormat.GetFlexColIndex(e.ColumnIndex);
			if (col == AccountCol)
			{
                //FC:FINAL:SBE:#4358 - use ClipText to check if there is something entered for account. EditMask can be different
                //FC:FINAL:AM:#3131 - EditText doesn't contain the prompt characters
                //if (Strings.InStr(1, vsCustomFormat.EditText, "X", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsCustomFormat.EditText, "_", CompareConstants.vbBinaryCompare) == 0)
                //if (Strings.InStr(1, vsCustomFormat.EditText, "X", CompareConstants.vbBinaryCompare) == 0 && vsCustomFormat.EditText != "  -  -")
                FCMaskedTextBox maskedEditor = vsCustomFormat.EditingControl as FCMaskedTextBox;
                if (Strings.InStr(1, vsCustomFormat.EditText, "X", CompareConstants.vbBinaryCompare) == 0 && (maskedEditor != null && maskedEditor.MaskedTextProvider.MaskFull))
                {
					if (modValidateAccount.AccountValidate(vsCustomFormat.TextMatrix(row, AccountTypeCol) + " " + vsCustomFormat.EditText))
					{
						lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsCustomFormat.TextMatrix(row, AccountTypeCol) + " " + vsCustomFormat.EditText);
						e.Cancel = false;
					}
					else
					{
						MessageBox.Show("This is in invalid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
					}
				}
				else
				{
                    //FC:FINAL:SBE - #4358 - moved logic from AfterEdit here
                    vsCustomFormat.EditMask = "";
                    vsCustomFormat.EditText = "";
                    e.FormattedValue = "";
                    e.Cancel = false;
                    //FC:FINAL:SBE - #4358 - update client side value for cell, after Mask was changed, and cell value was cleared
                    vsCustomFormat[e.ColumnIndex, e.RowIndex].Update();
				}
			}
			else if (col == TypeCol)
			{
				if (vsCustomFormat.EditText == "A")
				{
					if (vsCustomFormat.TextMatrix(row, AccountTypeCol) == "")
					{
                        vsCustomFormat.TextMatrix(row, AccountTypeCol, "E");
					}
					if (vsCustomFormat.TextMatrix(row, DSCol) == "")
					{
						vsCustomFormat.TextMatrix(row, DSCol, "D");
					}
				}
				else
				{
					vsCustomFormat.TextMatrix(row, AccountTypeCol, "");
					vsCustomFormat.TextMatrix(row, AccountCol, "");
					vsCustomFormat.TextMatrix(row, DSCol, "");
				}
			}
			else if (col == DSCol)
			{
				if (vsCustomFormat.EditText == "D" && Strings.InStr(1, vsCustomFormat.TextMatrix(row, AccountCol), "X", CompareConstants.vbBinaryCompare) != 0 && Strings.InStr(1, vsCustomFormat.TextMatrix(row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					vsCustomFormat.TextMatrix(row, TitleCol, "");
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
			//lblNote.ForeColor = Color.Blue;
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				cboBeginningMonth.Visible = true;
				cboEndingMonth.Visible = true;
				lblTo[0].Visible = true;
				cboSingleMonth.Visible = false;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboBeginningMonth.SelectedIndex = modBudgetaryMaster.Statics.FirstMonth - 1;
				cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.Visible = true;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			else if (cmbRange.SelectedIndex == 2)
			{
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.SelectedIndex = -1;
				cboSingleMonth.Visible = false;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = false;
			}
		}

		private void chkCheckDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckDateRange.CheckState == CheckState.Checked)
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = false;
					cboSingleMonth.SelectedIndex = -1;
				}
				else
				{
					cboBeginningMonth.Visible = false;
					cboEndingMonth.Visible = false;
					lblTo[0].Visible = false;
					cboBeginningMonth.SelectedIndex = -1;
					cboEndingMonth.SelectedIndex = -1;
				}
			}
			else
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = true;
					cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
				else
				{
					cboBeginningMonth.Visible = true;
					cboEndingMonth.Visible = true;
					lblTo[0].Visible = true;
					cboBeginningMonth.SelectedIndex = modBudgetaryMaster.Statics.FirstMonth - 1;
					cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
			}
		}

		private void SaveInfo()
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			int counter;
			clsDRWrapper rsSaveInfo = new clsDRWrapper();
			int lngReportKey = 0;
			bool blnNoData;
			txtDescription.Focus();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("There is data missing in your report that must be filled in before you may continue with this process.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (chkBudget.CheckState == CheckState.Unchecked && chkCurrent.CheckState == CheckState.Unchecked && chkYTD.CheckState == CheckState.Unchecked && chkBalance.CheckState == CheckState.Unchecked && chkPercent.CheckState == CheckState.Unchecked && chkYTDDebCred.CheckState == CheckState.Unchecked && chkCurrentDebCred.CheckState == CheckState.Unchecked)
			{
				MessageBox.Show("You must select 1 or more items to be included in your report before you may continue.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkAccountNumber.CheckState == CheckState.Unchecked && chkAccountDescription.CheckState == CheckState.Unchecked)
			{
				MessageBox.Show("You must select account information to view in the report before you may continue.", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbRange.SelectedIndex == 0 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.SelectedIndex == 1 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			blnNoData = true;
			for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
			{
				if (vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TypeCol) == "H" || vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TypeCol) == "S" || vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TypeCol) == "F")
				{
					if (Strings.Trim(vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TitleCol)) == "")
					{
						// do nothing
					}
					else
					{
						blnNoData = false;
					}
				}
				else
				{
					if (vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), AccountCol) == "")
					{
						// do nothing
					}
					else
					{
						blnNoData = false;
						if (Strings.InStr(1, vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), AccountCol), "_", CompareConstants.vbBinaryCompare) != 0)
						{
							MessageBox.Show("There is data missing in your report that must be filled in before you may continue with this process.", "Invalid Report Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsCustomFormat.Select(FCConvert.ToInt32(counter), AccountCol);
							return;
						}
					}
				}
			}
			if (blnNoData)
			{
				MessageBox.Show("You must enter at least 1 line item to show on the report before you may save it.", "Invalid Report Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsCustomFormat.Select(1, TypeCol);
				return;
			}
			rsSaveInfo.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(Strings.Trim(txtDescription.Text)) + "'");
			if (rsSaveInfo.EndOfFile() != true && rsSaveInfo.BeginningOfFile() != true)
			{
				counter = FCConvert.ToInt32(MessageBox.Show("There is a custom report that already exists with this title.  Do you wish to overwrite this report?", "Overwrite Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
				if (counter == FCConvert.ToInt32(DialogResult.Yes))
				{
					lngReportKey = FCConvert.ToInt32(rsSaveInfo.Get_Fields_Int32("ID"));
					rsSaveInfo.Edit();
					rsSaveInfo.Set_Fields("Title", Strings.Trim(txtDescription.Text));
					if (chkEncumbrances.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("IncludeEnc", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("IncludeEnc", false);
					}
					if (chkPending.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("IncludePending", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("IncludePending", false);
					}
					if (chkBudget.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("Budget", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("Budget", false);
					}
					if (chkAccountNumber.CheckState == CheckState.Checked && chkAccountDescription.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("AcctInfo", "B");
					}
					else if (chkAccountNumber.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("AcctInfo", "N");
					}
					else
					{
						rsSaveInfo.Set_Fields("AcctInfo", "D");
					}
					if (chkCurrent.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("CurrentActivity", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("CurrentActivity", false);
					}
					if (chkYTD.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("YTDActivity", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("YTDActivity", false);
					}
					if (chkCurrentDebCred.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("CurrentDebCred", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("CurrentDebCred", false);
					}
					if (chkYTDDebCred.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("YTDDebCred", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("YTDDebCred", false);
					}
					if (chkBalance.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("Balance", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("Balance", false);
					}
					if (chkPercent.CheckState == CheckState.Checked)
					{
						rsSaveInfo.Set_Fields("PercentSpent", true);
					}
					else
					{
						rsSaveInfo.Set_Fields("PercentSpent", false);
					}
					if (cmbRange.SelectedIndex == 1)
					{
						rsSaveInfo.Set_Fields("DateRange", "S");
						if (chkCheckDateRange.CheckState == CheckState.Unchecked)
						{
							rsSaveInfo.Set_Fields("LowDate", cboSingleMonth.SelectedIndex + 1);
							rsSaveInfo.Set_Fields("CheckMonthRange", false);
						}
						else
						{
							rsSaveInfo.Set_Fields("CheckMonthRange", true);
						}
					}
					else if (cmbRange.SelectedIndex == 0)
					{
						rsSaveInfo.Set_Fields("DateRange", "R");
						if (chkCheckDateRange.CheckState == CheckState.Unchecked)
						{
							rsSaveInfo.Set_Fields("LowDate", cboBeginningMonth.SelectedIndex + 1);
							rsSaveInfo.Set_Fields("HighDate", cboEndingMonth.SelectedIndex + 1);
							rsSaveInfo.Set_Fields("CheckMonthRange", false);
						}
						else
						{
							rsSaveInfo.Set_Fields("CheckMonthRange", true);
						}
					}
					else
					{
						rsSaveInfo.Set_Fields("DateRange", "A");
						rsSaveInfo.Set_Fields("CheckMonthRange", false);
					}
					rsSaveInfo.Update(true);
				}
				else
				{
					return;
				}
			}
			else
			{
				rsSaveInfo.AddNew();
				rsSaveInfo.Set_Fields("Title", Strings.Trim(txtDescription.Text));
				if (chkEncumbrances.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("IncludeEnc", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("IncludeEnc", false);
				}
				if (chkPending.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("IncludePending", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("IncludePending", false);
				}
				if (chkBudget.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("Budget", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("Budget", false);
				}
				if (chkCurrent.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("CurrentActivity", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("CurrentActivity", false);
				}
				if (chkYTD.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("YTDActivity", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("YTDActivity", false);
				}
				if (chkCurrentDebCred.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("CurrentDebCred", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("CurrentDebCred", false);
				}
				if (chkYTDDebCred.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("YTDDebCred", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("YTDDebCred", false);
				}
				if (chkBalance.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("Balance", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("Balance", false);
				}
				if (chkPercent.CheckState == CheckState.Checked)
				{
					rsSaveInfo.Set_Fields("PercentSpent", true);
				}
				else
				{
					rsSaveInfo.Set_Fields("PercentSpent", false);
				}
				if (cmbRange.SelectedIndex == 1)
				{
					rsSaveInfo.Set_Fields("DateRange", "S");
					if (chkCheckDateRange.CheckState == CheckState.Unchecked)
					{
						rsSaveInfo.Set_Fields("LowDate", cboSingleMonth.SelectedIndex + 1);
						rsSaveInfo.Set_Fields("CheckMonthRange", false);
					}
					else
					{
						rsSaveInfo.Set_Fields("CheckMonthRange", true);
					}
				}
				else if (cmbRange.SelectedIndex == 0)
				{
					rsSaveInfo.Set_Fields("DateRange", "R");
					if (chkCheckDateRange.CheckState == CheckState.Unchecked)
					{
						rsSaveInfo.Set_Fields("LowDate", cboBeginningMonth.SelectedIndex + 1);
						rsSaveInfo.Set_Fields("HighDate", cboEndingMonth.SelectedIndex + 1);
						rsSaveInfo.Set_Fields("CheckMonthRange", false);
					}
					else
					{
						rsSaveInfo.Set_Fields("CheckMonthRange", true);
					}
				}
				else
				{
					rsSaveInfo.Set_Fields("DateRange", "A");
					rsSaveInfo.Set_Fields("CheckMonthRange", false);
				}
				rsSaveInfo.Update(true);
				lngReportKey = FCConvert.ToInt32(rsSaveInfo.Get_Fields_Int32("ID"));
			}
			rsSaveInfo.Execute("DELETE FROM CustomReportsDetail WHERE ReportKey = " + FCConvert.ToString(lngReportKey), "Budgetary");
			rsSaveInfo.OpenRecordset("SELECT * FROM CustomReportsDetail WHERE ID = 0");
			for (counter = 1; counter <= vsCustomFormat.Rows - 1; counter++)
			{
				if (vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TypeCol) == "A" && vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), AccountCol) == "")
				{
					// do nothing
				}
				else
				{
					rsSaveInfo.AddNew();
					rsSaveInfo.Set_Fields("ReportKey", lngReportKey);
					rsSaveInfo.Set_Fields("Row", vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), RowCol));
					rsSaveInfo.Set_Fields("Type", vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TypeCol));
					rsSaveInfo.Set_Fields("Account", vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), AccountTypeCol) + " " + vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), AccountCol));
					rsSaveInfo.Set_Fields("DetailSummary", vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), DSCol));
					rsSaveInfo.Set_Fields("Title", vsCustomFormat.TextMatrix(FCConvert.ToInt32(counter), TitleCol));
					rsSaveInfo.Update(true);
				}
			}
			if (blnUnload)
			{
				Close();
				frmCustomReportSelection.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MessageBox.Show("Save Successful!", "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetFieldCount()
		{
			short GetFieldCount = 0;
			int counter;
			counter = 0;
			if (chkBudget.CheckState == CheckState.Checked)
			{
				counter += 1;
			}
			if (chkCurrent.CheckState == CheckState.Checked)
			{
				counter += 1;
			}
			if (chkYTD.CheckState == CheckState.Checked)
			{
				counter += 1;
			}
			if (chkYTDDebCred.CheckState == CheckState.Checked)
			{
				counter += 2;
			}
			if (chkCurrentDebCred.CheckState == CheckState.Checked)
			{
				counter += 2;
			}
			if (chkBalance.CheckState == CheckState.Checked)
			{
				counter += 1;
			}
			if (chkPercent.CheckState == CheckState.Checked)
			{
				counter += 1;
			}
			GetFieldCount = FCConvert.ToInt16(counter);
			return GetFieldCount;
		}
		//FC:FINAL:AM:#i735 - attach event handlers
		private void vsCustomFormat_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyDown -= new KeyEventHandler(vsCustomFormat_KeyDownEdit);
				//e.Control.KeyPress -= new KeyPressEventHandler(vsCustomFormat_KeyPressEdit);
				//e.Control.Appear -= Control_Appear;
				e.Control.KeyDown += new KeyEventHandler(vsCustomFormat_KeyDownEdit);
				//e.Control.KeyPress += new KeyPressEventHandler(vsCustomFormat_KeyPressEdit);
				//e.Control.Appear += Control_Appear;
			}
		}

		private void Control_Appear(object sender, EventArgs e)
		{
			var editor = sender as TextBox;
			if (editor != null)
			{
				editor.SelectionStart = 0;
				editor.SelectionLength = 1;
			}
		}
	}
}
