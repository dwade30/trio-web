﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptPosting.
	/// </summary>
	public partial class rptPosting : BaseSectionReport
	{
		public static rptPosting InstancePtr
		{
			get
			{
				return (rptPosting)Sys.GetInstance(typeof(rptPosting));
			}
		}

		protected rptPosting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPosting	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public int PageCounter;
		int counter;
		int CountHolder;
		public int intRowHolder;
		public bool blnClosingEntry;
		public bool blnSaveClosingEntry;
		public string strClosingEntryType = "";
		bool blnPrintedClosing;

		public rptPosting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Posted Journals";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!blnClosingEntry)
				{
					if (CountHolder == 0)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
				else
				{
					if (strClosingEntryType == "EN")
					{
						if (blnPrintedClosing)
						{
							eArgs.EOF = true;
						}
						else
						{
							eArgs.EOF = false;
							CountHolder += 1;
							if (CountHolder == 2)
							{
								blnPrintedClosing = true;
							}
						}
					}
					else
					{
						if (blnPrintedClosing)
						{
							eArgs.EOF = true;
						}
						else
						{
							blnPrintedClosing = true;
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fetch Data Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{

				if (!rptPosting.InstancePtr.blnSaveClosingEntry)
				{
					frmPosting.InstancePtr.Unload();
				}
				else
				{
					//this.Document.Save("EOYClosingJournal.RDF");
                    this.Document.Save(System.IO.Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, "EOYClosingJournal.RDF"));
                }
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Ending Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				PageCounter = 0;
				Label2.Text = modGlobalConstants.Statics.MuniName;
				Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
				CountHolder = 1;
				if (!blnClosingEntry)
				{
					if (frmPosting.InstancePtr.chkBreak.CheckState == CheckState.Checked)
					{
						rptPosting.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
					}
					else
					{
						rptPosting.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
					}
				}
				else
				{
					if (strClosingEntryType == "EN")
					{
						rptPosting.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						blnPrintedClosing = false;
						CountHolder = 0;
					}
					else
					{
						rptPosting.InstancePtr.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
						blnPrintedClosing = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//clsDRWrapper rsInfo = new clsDRWrapper();
			// troges126
			frmBusy bWait = new frmBusy();
			FCUtils.StartTask(frmReportViewer.InstancePtr, () =>
			{
				bWait.Message = "Recalculating Account Summary Information";
				bWait.StartBusy();
				try
				{
					// On Error GoTo ErrorHandler
					modBudgetaryAccounting.CalculateAccountInfo();
					bWait.StopBusy();
					//bWait.Unload();
					bWait.Hide();
					/*- bWait = null; */
					//return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					bWait.StopBusy();
					//bWait.Unload();
					bWait.Hide();
				}
				FCUtils.UnlockUserInterface();
			});
			bWait.Show(FCForm.FormShowEnum.Modal);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngErrorCode;
				int intCounter;
				if (!blnClosingEntry)
				{
					for (counter = CountHolder; counter <= frmPosting.InstancePtr.vsJournals.Rows - 1; counter++)
					{
						if (FCUtils.CBool(frmPosting.InstancePtr.vsJournals.TextMatrix(counter, 0)) == true)
						{
							if (frmPosting.InstancePtr.vsJournals.TextMatrix(counter, 3) == "AP" || frmPosting.InstancePtr.vsJournals.TextMatrix(counter, 3) == "AC")
							{
								intRowHolder = counter;
								rptJournalSubReport.Report = rptAPJournals.InstancePtr;
							}
							else if (frmPosting.InstancePtr.vsJournals.TextMatrix(counter, 3) == "EN")
							{
								intRowHolder = counter;
								rptJournalSubReport.Report = rptEncJournals.InstancePtr;
							}
							else
							{
								intRowHolder = counter;
								rptJournalSubReport.Report = rptJournals.InstancePtr;
							}
							rptJournalSubReport.Report.UserData = frmPosting.InstancePtr.vsJournals.TextMatrix(counter, 1);
							CountHolder = counter + 1;
							for (intCounter = CountHolder; intCounter <= frmPosting.InstancePtr.vsJournals.Rows - 1; intCounter++)
							{
								if (FCUtils.CBool(frmPosting.InstancePtr.vsJournals.TextMatrix(intCounter, 0)) == true)
								{
									return;
								}
							}
							this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
							return;
						}
					}
					if (counter >= frmPosting.InstancePtr.vsJournals.Rows - 1)
					{
						CountHolder = 0;
						rptJournalSubReport.Report = null;
					}
				}
				else
				{
					if (strClosingEntryType == "EN")
					{
						if (CountHolder == 2)
						{
							rptJournalSubReport.Report = rptEncJournals.InstancePtr;
							rptJournalSubReport.Report.UserData = frmEOYProcessing.InstancePtr.lngClosingJournal + 1;
						}
						else
						{
							rptJournalSubReport.Report = rptJournals.InstancePtr;
							rptJournalSubReport.Report.UserData = frmEOYProcessing.InstancePtr.lngClosingJournal;
						}
					}
					else
					{
						rptJournalSubReport.Report = rptJournals.InstancePtr;
						rptJournalSubReport.Report.UserData = frmEOYProcessing.InstancePtr.lngClosingJournal;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Detail Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptPosting_Load(object sender, System.EventArgs e)
		{

		}
	}
}
