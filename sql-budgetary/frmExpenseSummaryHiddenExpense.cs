﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseSummaryHiddenExpense.
	/// </summary>
	public partial class frmExpenseSummaryHiddenExpense : BaseForm
	{
		public frmExpenseSummaryHiddenExpense()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExpenseSummaryHiddenExpense InstancePtr
		{
			get
			{
				return (frmExpenseSummaryHiddenExpense)Sys.GetInstance(typeof(frmExpenseSummaryHiddenExpense));
			}
		}

		protected frmExpenseSummaryHiddenExpense _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		clsDRWrapper rs4 = new clsDRWrapper();
		clsDRWrapper rs5 = new clsDRWrapper();
		clsDRWrapper rs6 = new clsDRWrapper();
		clsDRWrapper rs7 = new clsDRWrapper();
		clsDRWrapper UsedAccounts = new clsDRWrapper();
		bool BudgetFlag;
		bool AdjustmentFlag;
		bool NetBudgetFlag;
		bool CurrentDCFlag;
		bool CurrentNetFlag;
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool BalanceFlag;
		bool SpentFlag;
		bool EncumbranceFlag;
		bool PendingFlag;
		int CurrentRow;
		string CurrentDepartment = "";
		string CurrentExpense = "";
		string CurrentDivision = "";
		string CurrentObject = "";
		int CurrentCol;
		int OriginalBudgetCol;
		int BudgetAdjustmentCol;
		int NetBudgetCol;
		int CurrentDebitCol;
		int CurrentCreditCol;
		int CurrentNetCol;
		int YTDDebitCol;
		int YTDCreditCol;
		int YTDNetCol;
		int BalanceCol;
		int SpentCol;
		int EncumbranceCol;
		int PendingCol;
		string strPeriodCheck;
		bool blnRunCollapseEvent;
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsBudgetInfo = new clsDRWrapper();
		public string strTitle = "";

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrintPreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptExpenseSummaryHiddenExpense.InstancePtr);
		}

		private void frmExpenseSummaryHiddenExpense_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper Descriptions = new clsDRWrapper();
			double dblSpent = 0;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rs.OpenRecordset("SELECT * FROM ExpenseSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			lblTitle.Text = "Department(s)";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
			{
				lblTitle.Text = "Accounts";
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount"));
			}
			else
			{
				lblRangeDept.Text = "ALL";
			}
			strPeriodCheck = "AND";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnFromPreview)
			{
				FormatGrid();
				frmWait.InstancePtr.prgProgress.Value = 30;
				frmWait.InstancePtr.Refresh();
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo();
				// If rs.Fields["IncludePending"] Or rs.Fields["SeperatePending"] Then
				// CalculateAccountInfo True, True, True, "E"
				// Else
				// CalculateAccountInfo True, True, False, "E"
				// End If
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Retrieving Information";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				RetrieveInfo();
				frmWait.InstancePtr.prgProgress.Value = 100;
				vs1.Rows += 1;
				vs1.TextMatrix(vs1.Rows - 1, 1, "-");
				CurrentRow = vs1.Rows - 1;
				PrepareTemp();
				vs1.Rows -= 1;
				CurrentRow = 0;
				frmWait.InstancePtr.prgProgress.Value = 60;
				frmWait.InstancePtr.Refresh();
				DepartmentReport();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Calculating Totals";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				FillInInformation();
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				GetSubTotals();
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				GetCompleteTotals();
				if (SpentFlag)
				{
					CurrentCol = SpentCol;
					for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
					{
						dblSpent = GetSpent();
						if (dblSpent != -999)
						{
							if ((dblSpent * 100) < -999.99)
							{
								vs1.TextMatrix(CurrentRow, vs1.Cols - 1, "-999.99");
							}
							else if ((dblSpent * 100) > 999.99)
							{
								vs1.TextMatrix(CurrentRow, vs1.Cols - 1, "999.99");
							}
							else
							{
								vs1.TextMatrix(CurrentRow, vs1.Cols - 1, Strings.Format(dblSpent * 100, "0.00"));
							}
						}
						else
						{
							vs1.TextMatrix(CurrentRow, vs1.Cols - 1, "----");
						}
					}
				}
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
				{
					ClearMonthlyYTDAmounts();
				}
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Formatting Report";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				SetColors();
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.RowHeightMax = 250;
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 8);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 10);
				}
				else
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_String("FontName"));
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields("FontSize"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Bold"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontItalic, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Italic"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontStrikethru, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("StrikeThru"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontUnderline, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Underline"));
					vs1.AutoSize(1, vs1.Cols - 1, false, 300);
					vs1.ExtendLastCol = false;
				}
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				blnRunCollapseEvent = false;
				for (counter = 5; counter >= 1; counter--)
				{
					for (counter2 = 2; counter2 <= vs1.Rows - 1; counter2++)
					{
						if (vs1.RowOutlineLevel(counter2) == counter)
						{
							if (vs1.IsSubtotal(counter2))
							{
								vs1.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
							}
						}
					}
				}
				blnRunCollapseEvent = true;
				vs1_Collapsed();
			}
			else
			{
				modBudgetaryMaster.Statics.blnFromPreview = false;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Use")) == "P")
			{
				cmdPrintPreview.Enabled = true;
			}
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
			vs1.Visible = true;
			lblTitle.Visible = true;
			lblRangeDept.Visible = true;
			lblMonthLabel.Visible = true;
			lblMonths.Visible = true;
		}

		private void frmExpenseSummaryHiddenExpense_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExpenseSummaryHiddenExpense.FillStyle	= 0;
			//frmExpenseSummaryHiddenExpense.ScaleWidth	= 9480;
			//frmExpenseSummaryHiddenExpense.ScaleHeight	= 7500;
			//frmExpenseSummaryHiddenExpense.AutoRedraw	= -1  'True;
			//frmExpenseSummaryHiddenExpense.LinkTopic	= "Form2";
			//frmExpenseSummaryHiddenExpense.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DeleteTemp();
			frmExpenseSummarySelect.InstancePtr.Show(App.MainForm);
		}

		private void frmExpenseSummaryHiddenExpense_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmdPrintPreview.Enabled == true)
			{
				frmReportViewer.InstancePtr.Init(rptExpenseSummaryHiddenExpense.InstancePtr);
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrintPreview.Enabled == true)
			{
				//rptExpenseSummaryHiddenExpense.InstancePtr.Hide();
				modDuplexPrinting.DuplexPrintReport(rptExpenseSummaryHiddenExpense.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool FirstFlag = false;
			bool SecondFlag = false;
			bool ThirdFlag = false;
			bool FourthFlag = false;
			bool FifthFlag;
			if (blnRunCollapseEvent)
			{
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FirstFlag = true;
						}
						else
						{
							rows += 1;
							FirstFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 2)
					{
						if (FirstFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							SecondFlag = true;
						}
						else
						{
							rows += 1;
							SecondFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 3)
					{
						if (FirstFlag == true || SecondFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							ThirdFlag = true;
						}
						else
						{
							rows += 1;
							ThirdFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 4)
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FourthFlag = true;
						}
						else
						{
							rows += 1;
							FourthFlag = false;
						}
					}
					else
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true || FourthFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				//FC:FINAL:BBE - fixed row height in web version
				//if (FCConvert.ToString(rs.Get_Fields("Font")) == "S")
				//{
				//    height = (rows + 2) * 250 + 300;
				//    if (height < frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645)
				//    {
				//        if (vs1.Height != height)
				//        {
				//            vs1.Height = height + 75;
				//        }
				//    }
				//    else
				//    {
				//        if (vs1.Height < frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645)
				//        {
				//            vs1.Height = FCConvert.ToInt32(frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645 + 75);
				//        }
				//    }
				//}
				//else
				//{
				//    height = (rows + 2) * vs1.RowHeight(0);
				//    if (height < frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645)
				//    {
				//        if (vs1.Height != height)
				//        {
				//            vs1.Height = height + 75;
				//        }
				//    }
				//    else
				//    {
				//        if (vs1.Height < frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645)
				//        {
				//            vs1.Height = FCConvert.ToInt32(frmExpenseSummaryHiddenExpense.InstancePtr.Height * 0.6645 + 75);
				//        }
				//    }
				//}
			}
		}

		private void FormatGrid()
		{
			int counter;
			counter = 2;
			//FC:FINAL:BBE:#i721 - merge the cells
			vs1.Cols = 20;
			vs1.MergeRow(0, true);
			//FC:FINAL:BBE:#i721 - add the expand button
			vs1.AddExpandButton();
			vs1.RowHeadersWidth = 10;
			vs1.TextMatrix(1, 1, "Account");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "L")
				{
					vs1.ColWidth(1, 5435);
				}
				else
				{
					vs1.ColWidth(1, 4375);
				}
			}
			else if (rs.Get_Fields_String("Font") == "L")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "L")
				{
					vs1.ColWidth(1, 5800);
				}
				else
				{
					vs1.ColWidth(1, 4200);
				}
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Budget")))
			{
				vs1.TextMatrix(0, counter, " Budget");
				vs1.TextMatrix(1, counter, "Original");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                BudgetFlag = true;
				counter += 1;
			}
			else
			{
				BudgetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BudgetAdjustment")))
			{
				vs1.TextMatrix(0, counter, "  Budget");
				vs1.TextMatrix(1, counter, "Adjustments");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                AdjustmentFlag = true;
				counter += 1;
			}
			else
			{
				AdjustmentFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("NetBudget")))
			{
				vs1.TextMatrix(0, counter, "Budget");
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                NetBudgetFlag = true;
				counter += 1;
			}
			else
			{
				NetBudgetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentDebitCredit")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentNet")))
				{
					vs1.TextMatrix(0, counter, "Current Month");
					vs1.TextMatrix(0, counter + 1, "Current Month");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Current Month");
					vs1.TextMatrix(0, counter + 1, "Current Month");
				}
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1750);
				}
				else
				{
					vs1.ColWidth(counter + 1, 2000);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
				CurrentDCFlag = true;
				counter += 2;
			}
			else
			{
				CurrentDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "Current Month");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Current Month");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                CurrentNetFlag = true;
				counter += 1;
			}
			else
			{
				CurrentNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
				{
					vs1.TextMatrix(0, counter, "Year To Date                      ");
					//vs1.TextMatrix(0, counter + 1, "Year To Date                      ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Year To Date          ");
					//vs1.TextMatrix(0, counter + 1, "Year To Date          ");
				}
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1750);
				}
				else
				{
					vs1.ColWidth(counter + 1, 2000);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDDCFlag = true;
				counter += 2;
			}
			else
			{
				YTDDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "Year To Date                      ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Year To Date");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDNetFlag = true;
				counter += 1;
			}
			else
			{
				YTDNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("OSEncumbrance")))
			{
				vs1.TextMatrix(0, counter, "Outstanding");
				vs1.TextMatrix(1, counter, "Encumbrances");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                EncumbranceFlag = true;
				counter += 1;
			}
			else
			{
				EncumbranceFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("SeperatePending")))
			{
				vs1.TextMatrix(0, counter, "Pending");
				vs1.TextMatrix(1, counter, "Activity");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                PendingFlag = true;
				counter += 1;
			}
			else
			{
				PendingFlag = false;
			}
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			if (FCConvert.ToBoolean(rs.Get_Fields("Balance")))
			{
				vs1.TextMatrix(0, counter, "Unexpended");
				vs1.TextMatrix(1, counter, "Balance");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                BalanceFlag = true;
				counter += 1;
			}
			else
			{
				BalanceFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Spent")))
			{
				vs1.TextMatrix(0, counter, "Percent");
				vs1.TextMatrix(1, counter, "Spent");
				vs1.ColWidth(counter, 1600);
				vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// vs1.ColFormat(counter) = "0.00"
				SpentFlag = true;
				counter += 1;
			}
			else
			{
				SpentFlag = false;
			}
			vs1.Cols = counter;
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 1, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, vs1.Cols - 1, true);
		}

		private string Expense(ref string x)
		{
			string Expense = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Expense = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			else
			{
				Expense = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
			}
			return Expense;
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private string Division(ref string x)
		{
			string Division = "";
			Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			return Division;
		}

		private string Object(ref string x)
		{
			string Object = "";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Object = Strings.Mid(x, 6 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			else
			{
				Object = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			}
			return Object;
		}

		private void ShowMonths()
		{
			// add rows for months if monthly totals are selected
			int Level = 0;
			int counter;
			string TempStr;
			int counter2;
			// vbPorter upgrade warning: intCounterLimit As short --> As int	OnWriteFCConvert.ToDouble(
			int intCounterLimit = 0;
			int intMonth = 0;
			TempStr = "";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				Level = vs1.RowOutlineLevel(vs1.Rows - 1) + 1;
				vs1.Rows += 1;
				for (counter = 1; counter <= Level; counter++)
				{
					TempStr += "    ";
				}
				TempStr += "    " + CalculateMonth(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
				vs1.TextMatrix(vs1.Rows - 1, 1, TempStr);
				vs1.RowOutlineLevel(vs1.Rows - 1, FCConvert.ToInt16(Level));
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xE0E0E0);
			}
			else
			{
				Level = vs1.RowOutlineLevel(vs1.Rows - 1) + 1;
				if (Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) > Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))
				{
					intCounterLimit = FCConvert.ToInt32((12 - Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) + 1);
				}
				else
				{
					intCounterLimit = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) - Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) + 1);
				}
				intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
				for (counter2 = 1; counter2 <= intCounterLimit; counter2++)
				{
					TempStr = "";
					vs1.Rows += 1;
					for (counter = 1; counter <= Level; counter++)
					{
						TempStr += "    ";
					}
					TempStr += "    " + CalculateMonth(intMonth);
					vs1.TextMatrix(vs1.Rows - 1, 1, TempStr);
					vs1.RowOutlineLevel(vs1.Rows - 1, FCConvert.ToInt16(Level));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xE0E0E0);
					if (intMonth < 12)
					{
						intMonth += 1;
					}
					else
					{
						intMonth = 1;
					}
				}
			}
		}
		// vbPorter upgrade warning: x As short --> As int	OnWrite(double, int)
		private string CalculateMonth(int x)
		{
			string CalculateMonth = "";
			switch (x)
			{
				case 1:
					{
						CalculateMonth = "January";
						break;
					}
				case 2:
					{
						CalculateMonth = "February";
						break;
					}
				case 3:
					{
						CalculateMonth = "March";
						break;
					}
				case 4:
					{
						CalculateMonth = "April";
						break;
					}
				case 5:
					{
						CalculateMonth = "May";
						break;
					}
				case 6:
					{
						CalculateMonth = "June";
						break;
					}
				case 7:
					{
						CalculateMonth = "July";
						break;
					}
				case 8:
					{
						CalculateMonth = "August";
						break;
					}
				case 9:
					{
						CalculateMonth = "September";
						break;
					}
				case 10:
					{
						CalculateMonth = "October";
						break;
					}
				case 11:
					{
						CalculateMonth = "November";
						break;
					}
				case 12:
					{
						CalculateMonth = "December";
						break;
					}
			}
			//end switch
			return CalculateMonth;
		}

		private void DepartmentReport()
		{
			clsDRWrapper ExpObjDescriptions = new clsDRWrapper();
			clsDRWrapper DeptDivDescriptions = new clsDRWrapper();
			string strExpDescription = "";
			ExpObjDescriptions.OpenRecordset("SELECT * FROM ExpObjTitles ORDER BY Expense, Object");
			DeptDivDescriptions.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department, Division");
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				rs2.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "'");
				if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
				{
					rs2.MoveLast();
					rs2.MoveFirst();
					vs1.Rows += 1;
					if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
					{
						if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + "UNKNOWN");
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
					{
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
							if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
							{
								rs2.MoveLast();
								rs2.MoveFirst();
								while (rs2.EndOfFile() != true)
								{
									vs1.Rows += 1;
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Di")
									{
										vs1.IsSubtotal(vs1.Rows - 1, true);
										rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
										if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
										{
											rs3.MoveLast();
											rs3.MoveFirst();
											while (rs3.EndOfFile() != true)
											{
												if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
												{
													if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
													{
														strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
													}
													else
													{
														strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
													}
												}
												else
												{
													strExpDescription = "UNKNOWN";
												}
												if (!modAccountTitle.Statics.ObjFlag)
												{
													if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
													{
														rs4.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rs3.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
														if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
														{
															rs4.MoveLast();
															rs4.MoveFirst();
															while (rs4.EndOfFile() != true)
															{
																vs1.Rows += 1;
																if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + rs4.Get_Fields_String("FourthAccountField"), ","))
																{
																	if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
																	{
																		vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
																	}
																	else
																	{
																		vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
																	}
																}
																else
																{
																	vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
																}
																vs1.RowOutlineLevel(vs1.Rows - 1, 3);
																vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
																if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
																{
																	vs1.IsSubtotal(vs1.Rows - 1, true);
																	ShowMonths();
																}
																rs4.MoveNext();
															}
														}
													}
													else
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
													}
												}
												else
												{
													if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
													{
														vs1.IsSubtotal(vs1.Rows - 1, true);
														ShowMonths();
													}
												}
												rs3.MoveNext();
											}
										}
									}
									else
									{
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
										{
											vs1.IsSubtotal(vs1.Rows - 1, true);
											ShowMonths();
										}
									}
									rs2.MoveNext();
								}
							}
						}
						else
						{
							rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
							if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
							{
								rs3.MoveLast();
								rs3.MoveFirst();
								while (rs3.EndOfFile() != true)
								{
									if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										strExpDescription = "UNKNOWN";
									}
									if (!modAccountTitle.Statics.ObjFlag)
									{
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
										{
											rs4.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rs3.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
											if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
											{
												rs4.MoveLast();
												rs4.MoveFirst();
												while (rs4.EndOfFile() != true)
												{
													vs1.Rows += 1;
													if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + rs4.Get_Fields_String("ThirdAccountField"), ","))
													{
														if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
														}
													}
													else
													{
														vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
													}
													vs1.RowOutlineLevel(vs1.Rows - 1, 2);
													vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
													if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
													{
														vs1.IsSubtotal(vs1.Rows - 1, true);
														ShowMonths();
													}
													rs4.MoveNext();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
									}
									else
									{
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
										{
											vs1.IsSubtotal(vs1.Rows - 1, true);
											ShowMonths();
										}
									}
									rs3.MoveNext();
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
						{
							vs1.IsSubtotal(vs1.Rows - 1, true);
							ShowMonths();
						}
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				// more than 1 department
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE FirstAccountField >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND FirstAccountField <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						vs1.Rows += 1;
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
						{
							vs1.IsSubtotal(vs1.Rows - 1, true);
							if (!modAccountTitle.Statics.ExpDivFlag)
							{
								rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
								if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
								{
									rs2.MoveLast();
									rs2.MoveFirst();
									while (rs2.EndOfFile() != true)
									{
										vs1.Rows += 1;
										if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
										}
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Di")
										{
											vs1.IsSubtotal(vs1.Rows - 1, true);
											rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
											if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
											{
												rs3.MoveLast();
												rs3.MoveFirst();
												while (rs3.EndOfFile() != true)
												{
													if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
													{
														if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
														}
														else
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
														}
													}
													else
													{
														strExpDescription = "UNKNOWN";
													}
													if (!modAccountTitle.Statics.ObjFlag)
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
														{
															rs4.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rs3.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
															if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
															{
																rs4.MoveLast();
																rs4.MoveFirst();
																while (rs4.EndOfFile() != true)
																{
																	vs1.Rows += 1;
																	if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + rs4.Get_Fields_String("FourthAccountField"), ","))
																	{
																		if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
																		}
																		else
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
																		}
																	}
																	else
																	{
																		vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
																	}
																	vs1.RowOutlineLevel(vs1.Rows - 1, 3);
																	vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
																	if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
																	{
																		vs1.IsSubtotal(vs1.Rows - 1, true);
																		ShowMonths();
																	}
																	rs4.MoveNext();
																}
															}
														}
														else
														{
															if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
															{
																vs1.IsSubtotal(vs1.Rows - 1, true);
																ShowMonths();
															}
														}
													}
													else
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
													}
													rs3.MoveNext();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs2.MoveNext();
									}
								}
							}
							else
							{
								rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
								if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
								{
									rs3.MoveLast();
									rs3.MoveFirst();
									while (rs3.EndOfFile() != true)
									{
										if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											strExpDescription = "UNKNOWN";
										}
										if (!modAccountTitle.Statics.ObjFlag)
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
											{
												rs4.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs3.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
												if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
												{
													rs4.MoveLast();
													rs4.MoveFirst();
													while (rs4.EndOfFile() != true)
													{
														vs1.Rows += 1;
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + rs4.Get_Fields_String("ThirdAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
														rs4.MoveNext();
													}
												}
											}
											else
											{
												if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
												{
													vs1.IsSubtotal(vs1.Rows - 1, true);
													ShowMonths();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs3.MoveNext();
									}
								}
							}
						}
						else
						{
							if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
							{
								vs1.IsSubtotal(vs1.Rows - 1, true);
								ShowMonths();
							}
						}
						rs5.MoveNext();
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "A")
			{
				// All Departments
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						vs1.Rows += 1;
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
						{
							vs1.IsSubtotal(vs1.Rows - 1, true);
							if (!modAccountTitle.Statics.ExpDivFlag)
							{
								rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
								if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
								{
									rs2.MoveLast();
									rs2.MoveFirst();
									while (rs2.EndOfFile() != true)
									{
										vs1.Rows += 1;
										if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
										}
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Di")
										{
											vs1.IsSubtotal(vs1.Rows - 1, true);
											rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
											if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
											{
												rs3.MoveLast();
												rs3.MoveFirst();
												while (rs3.EndOfFile() != true)
												{
													if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
													{
														if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
														}
														else
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
														}
													}
													else
													{
														strExpDescription = "UNKNOWN";
													}
													if (!modAccountTitle.Statics.ObjFlag)
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
														{
															rs4.OpenRecordset("SELECT Distinct FourthAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rs3.Get_Fields_String("ThirdAccountField") + "'ORDER BY FourthAccountField");
															if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
															{
																rs4.MoveLast();
																rs4.MoveFirst();
																while (rs4.EndOfFile() != true)
																{
																	vs1.Rows += 1;
																	if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + rs4.Get_Fields_String("FourthAccountField"), ","))
																	{
																		if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
																		}
																		else
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
																		}
																	}
																	else
																	{
																		vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
																	}
																	vs1.RowOutlineLevel(vs1.Rows - 1, 3);
																	vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
																	if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
																	{
																		vs1.IsSubtotal(vs1.Rows - 1, true);
																		ShowMonths();
																	}
																	rs4.MoveNext();
																}
															}
														}
														else
														{
															if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
															{
																vs1.IsSubtotal(vs1.Rows - 1, true);
																ShowMonths();
															}
														}
													}
													else
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
													}
													rs3.MoveNext();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs2.MoveNext();
									}
								}
							}
							else
							{
								rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
								if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
								{
									rs3.MoveLast();
									rs3.MoveFirst();
									while (rs3.EndOfFile() != true)
									{
										if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											strExpDescription = "UNKNOWN";
										}
										if (!modAccountTitle.Statics.ObjFlag)
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
											{
												rs4.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs3.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
												if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
												{
													rs4.MoveLast();
													rs4.MoveFirst();
													while (rs4.EndOfFile() != true)
													{
														vs1.Rows += 1;
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + rs4.Get_Fields_String("ThirdAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
														rs4.MoveNext();
													}
												}
											}
											else
											{
												if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
												{
													vs1.IsSubtotal(vs1.Rows - 1, true);
													ShowMonths();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs3.MoveNext();
									}
								}
							}
						}
						else
						{
							if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
							{
								vs1.IsSubtotal(vs1.Rows - 1, true);
								ShowMonths();
							}
						}
						rs5.MoveNext();
					}
				}
			}
			else
			{
				// range of accounts
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						vs1.Rows += 1;
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
						{
							vs1.IsSubtotal(vs1.Rows - 1, true);
							if (!modAccountTitle.Statics.ExpDivFlag)
							{
								rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
								if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
								{
									rs2.MoveLast();
									rs2.MoveFirst();
									while (rs2.EndOfFile() != true)
									{
										vs1.Rows += 1;
										if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
										}
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
										if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Di")
										{
											vs1.IsSubtotal(vs1.Rows - 1, true);
											rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY ThirdAccountField");
											if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
											{
												rs3.MoveLast();
												rs3.MoveFirst();
												while (rs3.EndOfFile() != true)
												{
													if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
													{
														if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
														}
														else
														{
															strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
														}
													}
													else
													{
														strExpDescription = "UNKNOWN";
													}
													if (!modAccountTitle.Statics.ObjFlag)
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
														{
															rs4.OpenRecordset("SELECT DISTINCT FourthAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND ThirdAccountField = '" + rs3.Get_Fields_String("ThirdAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY FourthAccountField");
															if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
															{
																rs4.MoveLast();
																rs4.MoveFirst();
																while (rs4.EndOfFile() != true)
																{
																	vs1.Rows += 1;
																	if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("ThirdAccountField") + "," + rs4.Get_Fields_String("FourthAccountField"), ","))
																	{
																		if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
																		}
																		else
																		{
																			vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
																		}
																	}
																	else
																	{
																		vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + "-" + rs4.Get_Fields_String("FourthAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
																	}
																	vs1.RowOutlineLevel(vs1.Rows - 1, 3);
																	vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
																	if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
																	{
																		vs1.IsSubtotal(vs1.Rows - 1, true);
																		ShowMonths();
																	}
																	rs4.MoveNext();
																}
															}
														}
														else
														{
															if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
															{
																vs1.IsSubtotal(vs1.Rows - 1, true);
																ShowMonths();
															}
														}
													}
													else
													{
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
													}
													rs3.MoveNext();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs2.MoveNext();
									}
								}
							}
							else
							{
								rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
								if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
								{
									rs3.MoveLast();
									rs3.MoveFirst();
									while (rs3.EndOfFile() != true)
									{
										if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												strExpDescription = FCConvert.ToString(ExpObjDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											strExpDescription = "UNKNOWN";
										}
										if (!modAccountTitle.Statics.ObjFlag)
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Ex")
											{
												rs4.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs3.Get_Fields_String("SecondAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY ThirdAccountField");
												if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
												{
													rs4.MoveLast();
													rs4.MoveFirst();
													while (rs4.EndOfFile() != true)
													{
														vs1.Rows += 1;
														if (ExpObjDescriptions.FindFirstRecord2("Expense, Object", rs3.Get_Fields_String("SecondAccountField") + "," + rs4.Get_Fields_String("ThirdAccountField"), ","))
														{
															if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("ShortDescription"));
															}
															else
															{
																vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + ExpObjDescriptions.Get_Fields_String("LongDescription"));
															}
														}
														else
														{
															vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + "-" + rs4.Get_Fields_String("ThirdAccountField") + " - " + strExpDescription + " / " + "UNKNOWN");
														}
														vs1.RowOutlineLevel(vs1.Rows - 1, 2);
														vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
														if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
														{
															vs1.IsSubtotal(vs1.Rows - 1, true);
															ShowMonths();
														}
														rs4.MoveNext();
													}
												}
											}
											else
											{
												if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
												{
													vs1.IsSubtotal(vs1.Rows - 1, true);
													ShowMonths();
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
											{
												vs1.IsSubtotal(vs1.Rows - 1, true);
												ShowMonths();
											}
										}
										rs3.MoveNext();
									}
								}
							}
						}
						else
						{
							if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
							{
								vs1.IsSubtotal(vs1.Rows - 1, true);
								ShowMonths();
							}
						}
						rs5.MoveNext();
					}
				}
			}
		}

		private void PrepareTemp()
		{
			string TempFirst = "";
			string TempSecond = "";
			string TempThird = "";
			string TempFourth = "";
			string strSQL1;
			string strSQL2;
			string strSQL3 = "";
			string strSQL4 = "";
			string strSQL5 = "";
			string strSQL6;
			string strTotalSQL;
			string strTotalSQL2;
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, double)
			Decimal curTotal;
			bool blnDataToShow = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strCurrentAccount;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			DeleteTemp();
			strSQL1 = "AccountType, Account, ";
			strSQL2 = "substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) AS FirstAccountField, ";
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2))) AS SecondAccountField, ";
				strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2))) AS ThirdAccountField, ";
				if (!modAccountTitle.Statics.ObjFlag)
				{
					strSQL5 = "substring(Account, 6 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 7, 2))) AS FourthAccountField, ";
				}
				else
				{
					strSQL5 = "";
				}
			}
			else
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2))) AS SecondAccountField, ";
				if (!modAccountTitle.Statics.ObjFlag)
				{
					strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Exp + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 3, 2)) + convert(int, substring('" + modAccountTitle.Statics.Exp + "', 5, 2)), convert(int, substring('" + modAccountTitle.Statics.Exp + "', 7, 2))) AS ThirdAccountField, ";
				}
				else
				{
					strSQL4 = "";
				}
				strSQL5 = "";
			}
			strSQL6 = "left(Account, 1) AS AccountType, Account, ";
			strTotalSQL = "SELECT DISTINCT " + strSQL1 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL = Strings.Left(strTotalSQL, strTotalSQL.Length - 2);
			strTotalSQL2 = "SELECT DISTINCT " + strSQL6 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL2 = Strings.Left(strTotalSQL2, strTotalSQL2.Length - 2);
			// rs3.CreateStoredProcedure "ExpenseAccounts", strTotalSQL2 & " FROM APJournalDetail WHERE left(Account, 1) = 'E' UNION " & strTotalSQL2 & " FROM EncumbranceDetail WHERE left(Account, 1) = 'E' UNION " & strTotalSQL2 & " FROM JournalEntries WHERE left(Account, 1) = 'E' UNION " & strTotalSQL & " FROM AccountMaster WHERE CurrentBudget <> 0 AND left(Account, 1) = 'E' ORDER BY Account"
			strCurrentAccount = "";
			rs3.OpenRecordset("SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'E' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'E' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'E' UNION " + strTotalSQL + " FROM AccountMaster WHERE CurrentBudget <> 0 AND left(Account, 1) = 'E' ) as ExpenseAccounts ORDER BY Account");
			if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
			{
				rsTemp.OpenRecordset("SELECT * FROM Temp WHERE ID = 0");
				do
				{
					curTotal = 0;
					blnDataToShow = false;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (strCurrentAccount != FCConvert.ToString(rs3.Get_Fields("Account")))
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strCurrentAccount = FCConvert.ToString(rs3.Get_Fields("Account"));
					}
					else
					{
						goto CheckNextAccount;
					}
					CurrentDepartment = FCConvert.ToString(rs3.Get_Fields_String("FirstAccountField"));
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						CurrentDivision = FCConvert.ToString(rs3.Get_Fields_String("SecondAccountField"));
						CurrentExpense = FCConvert.ToString(rs3.Get_Fields_String("ThirdAccountField"));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							CurrentObject = FCConvert.ToString(rs3.Get_Fields_String("FourthAccountField"));
						}
						else
						{
							CurrentObject = "";
						}
					}
					else
					{
						CurrentDivision = "";
						CurrentExpense = FCConvert.ToString(rs3.Get_Fields_String("SecondAccountField"));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							CurrentObject = FCConvert.ToString(rs3.Get_Fields_String("ThirdAccountField"));
						}
						else
						{
							CurrentObject = "";
						}
					}
					if (BudgetFlag || NetBudgetFlag)
					{
						curTotal = FCConvert.ToDecimal(GetOriginalBudget());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (AdjustmentFlag || NetBudgetFlag)
					{
						curTotal = FCConvert.ToDecimal(GetBudgetAdjustments());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (CurrentDCFlag)
					{
						curTotal = FCConvert.ToDecimal(GetCurrentDebits());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
						curTotal = FCConvert.ToDecimal(GetCurrentCredits());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (CurrentNetFlag)
					{
						curTotal = FCConvert.ToDecimal(GetCurrentNet());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (YTDDCFlag)
					{
						curTotal = FCConvert.ToDecimal(GetYTDDebit());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
						curTotal = FCConvert.ToDecimal(GetYTDCredit());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (YTDNetFlag)
					{
						curTotal = FCConvert.ToDecimal(GetYTDNet());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (EncumbranceFlag)
					{
						curTotal = FCConvert.ToDecimal(GetEncumbrance());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (PendingFlag)
					{
						curTotal = FCConvert.ToDecimal(GetPending());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					if (BalanceFlag)
					{
						curTotal = FCConvert.ToDecimal(GetBalance());
						if (curTotal != 0)
						{
							blnDataToShow = true;
							goto SaveAccount;
						}
					}
					SaveAccount:
					;
					if (blnDataToShow)
					{
						rsTemp.AddNew();
						rsTemp.Set_Fields("AccountType", rs3.Get_Fields_String("AccountType"));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsTemp.Set_Fields("Account", rs3.Get_Fields("Account"));
						rsTemp.Set_Fields("FirstAccountField", rs3.Get_Fields_String("FirstAccountField"));
						rsTemp.Set_Fields("SecondAccountField", rs3.Get_Fields_String("SecondAccountField"));
						if (!modAccountTitle.Statics.ExpDivFlag || !modAccountTitle.Statics.ObjFlag)
						{
							rsTemp.Set_Fields("ThirdAccountField", rs3.Get_Fields_String("ThirdAccountField"));
							if (!modAccountTitle.Statics.ExpDivFlag && !modAccountTitle.Statics.ObjFlag)
							{
								rsTemp.Set_Fields("FourthAccountField", rs3.Get_Fields_String("FourthAccountField"));
							}
						}
						rsTemp.Update();
					}
					CheckNextAccount:
					;
					rs3.MoveNext();
				}
				while (rs3.EndOfFile() != true);
			}
			// rs3.Execute "INSERT INTO Temp SELECT * FROM ExpenseAccounts"
			strPeriodCheck = strPeriodCheckHolder;
		}

		private void DeleteTemp()
		{
			rs3.Execute("DELETE FROM Temp", "Budgetary");
		}

		private void FillInInformation()
		{
			double temp = 0;
			int temp2;
			bool RemoveFlag = false;
			double dblSpent;
			int lngCounter;
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Refresh();
			//FC:FINAL:BBE - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 7);
			lngCounter = 2;
			for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
			{
				//Application.DoEvents();
				if (CurrentRow == vs1.Rows)
				{
					break;
				}
				if (CurrentRow >= lngCounter + 20)
				{
					lngCounter = CurrentRow;
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CurrentRow) / (vs1.Rows - 1)) * 100);
					frmWait.InstancePtr.Refresh();
				}
				if (vs1.RowOutlineLevel(CurrentRow) == 1)
				{
					CurrentDepartment = Strings.Left(vs1.TextMatrix(CurrentRow, 1), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
					CurrentDivision = "";
					CurrentExpense = "";
					CurrentObject = "";
				}
				else if (vs1.RowOutlineLevel(CurrentRow) == 2 && FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						CurrentDivision = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
						CurrentExpense = "";
						CurrentObject = "";
					}
					else
					{
						CurrentExpense = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
						CurrentObject = Strings.Mid(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + 2, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
					}
				}
				else if (vs1.RowOutlineLevel(CurrentRow) == 3)
				{
					CurrentExpense = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))));
					CurrentObject = Strings.Mid(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + 2, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
				}
				if (IsLowLevel(ref CurrentRow))
				{
					vs1.TextMatrix(CurrentRow, 2, "E " + CurrentDepartment + "-" + CurrentDivision + "-" + CurrentExpense + "-" + CurrentObject);
					CurrentCol = 2;
					if (BudgetFlag)
					{
						OriginalBudgetCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetOriginalBudget()));
						CurrentCol += 1;
					}
					if (AdjustmentFlag)
					{
						BudgetAdjustmentCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetBudgetAdjustments()));
						CurrentCol += 1;
					}
					if (NetBudgetFlag)
					{
						NetBudgetCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetNetBudget()));
						CurrentCol += 1;
					}
					if (CurrentDCFlag)
					{
						CurrentDebitCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentDebits()));
						CurrentCol += 1;
						CurrentCreditCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentCredits()));
						CurrentCol += 1;
					}
					if (CurrentNetFlag)
					{
						CurrentNetCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentNet()));
						CurrentCol += 1;
					}
					if (YTDDCFlag)
					{
						YTDDebitCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDDebit()));
						CurrentCol += 1;
						YTDCreditCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDCredit()));
						CurrentCol += 1;
					}
					if (YTDNetFlag)
					{
						YTDNetCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDNet()));
						CurrentCol += 1;
					}
					if (EncumbranceFlag)
					{
						EncumbranceCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetEncumbrance()));
						CurrentCol += 1;
					}
					if (PendingFlag)
					{
						PendingCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetPending()));
						CurrentCol += 1;
					}
					if (BalanceFlag)
					{
						BalanceCol = CurrentCol;
						temp = GetBalance();
						if (temp < 0)
						{
							vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, CurrentCol, Color.Red);
						}
						if (temp > GetOriginalBudget())
						{
							vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, CurrentCol, Color.Blue);
						}
						vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(temp));
						CurrentCol += 1;
					}
					if (SpentFlag)
					{
						SpentCol = CurrentCol;
						vs1.TextMatrix(CurrentRow, CurrentCol, "----");
						CurrentCol += 1;
					}
					temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
					if (temp == 0)
					{
						RemoveFlag = true;
						for (temp = 2; temp <= vs1.Cols - 1; temp++)
						{
							//Application.DoEvents();
							if (Conversion.Val(vs1.TextMatrix(CurrentRow, FCConvert.ToInt32(temp))) != 0)
							{
								RemoveFlag = false;
							}
						}
						if (RemoveFlag)
						{
							vs1.RemoveItem(CurrentRow);
							if (CurrentRow >= vs1.Rows)
							{
								break;
							}
							else
							{
								CurrentRow -= 1;
							}
						}
					}
				}
				else if (SpentFlag)
				{
					vs1.TextMatrix(CurrentRow, vs1.Cols - 1, "----");
					temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
					if (temp == 0)
					{
						RemoveFlag = true;
						for (temp = 2; temp <= vs1.Cols - 1; temp++)
						{
							//Application.DoEvents();
							if (FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, FCConvert.ToInt32(temp))) != 0)
							{
								RemoveFlag = false;
							}
						}
						if (RemoveFlag)
						{
							vs1.RemoveItem(CurrentRow);
							if (CurrentRow >= vs1.Rows)
							{
								break;
							}
							else
							{
								CurrentRow -= 1;
							}
						}
					}
					vs1.TextMatrix(CurrentRow, 2, "");
				}
				if (CurrentRow == vs1.Rows - 1)
				{
					break;
				}
			}
		}

		private double GetOriginalBudget()
		{
			double GetOriginalBudget = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [MonthlyBudgetTotal] not found!! (maybe it is an alias?)
									GetOriginalBudget = Conversion.Val(rsActivityDetail.Get_Fields("MonthlyBudgetTotal"));
								}
								else
								{
									GetOriginalBudget = 0;
								}
							}
						}
					}
				}
				else
				{
					GetOriginalBudget = 0;
				}
				return GetOriginalBudget;
			}
			if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							if (rsBudgetInfo.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
								GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
							}
							else
							{
								GetOriginalBudget = 0;
							}
						}
					}
				}
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments()
		{
			double GetBudgetAdjustments = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
									GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
								}
								else
								{
									GetBudgetAdjustments = 0;
								}
							}
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
				GetBudgetAdjustments *= -1;
				return GetBudgetAdjustments;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
				}
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			GetBudgetAdjustments *= -1;
			return GetBudgetAdjustments;
		}

		private double GetNetBudget()
		{
			double GetNetBudget = 0;
			// Dim temp As Integer
			// 
			// temp = InStr(1, vs1.TextMatrix(CurrentRow, 1), "-")
			// If temp = 0 Then
			// GetNetBudget = 0
			// Exit Function
			// End If
			if (BudgetFlag && AdjustmentFlag)
			{
				GetNetBudget = Conversion.Val(vs1.TextMatrix(CurrentRow, OriginalBudgetCol)) + Conversion.Val(vs1.TextMatrix(CurrentRow, BudgetAdjustmentCol));
			}
			else
			{
				GetNetBudget = GetOriginalBudget() + GetBudgetAdjustments();
			}
			return GetNetBudget;
		}

		private double GetCurrentDebits()
		{
			double GetCurrentDebits = 0;
			GetCurrentDebits = GetDepartmentCurrentDebits();
			return GetCurrentDebits;
		}

		private double GetCurrentCredits()
		{
			double GetCurrentCredits = 0;
			GetCurrentCredits = GetDepartmentCurrentCredits();
			return GetCurrentCredits;
		}

		private double GetCurrentNet()
		{
			double GetCurrentNet = 0;
			if (CurrentDCFlag)
			{
				GetCurrentNet = Conversion.Val(vs1.TextMatrix(CurrentRow, CurrentDebitCol)) - Conversion.Val(vs1.TextMatrix(CurrentRow, CurrentCreditCol));
			}
			else
			{
				GetCurrentNet = GetCurrentDebits() - GetCurrentCredits();
			}
			return GetCurrentNet;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetYTDDebit = 0;
								}
							}
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
				if (!EncumbranceFlag)
				{
					GetYTDDebit += GetEncumbrance();
				}
				if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
				{
					GetYTDDebit += GetPendingDebits();
				}
				return GetYTDDebit;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
				}
			}
			else
			{
				GetYTDDebit = 0;
			}
			if (!EncumbranceFlag)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetYTDCredit = 0;
								}
							}
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
				if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
				{
					GetYTDCredit += GetPendingCredits();
				}
				return GetYTDCredit;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
				}
			}
			else
			{
				GetYTDCredit = 0;
			}
			if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			// Dim temp As Integer
			// 
			// temp = InStr(1, vs1.TextMatrix(CurrentRow, 1), "-")
			// If temp = 0 Then
			// GetCurrentNet = 0
			// Exit Function
			// End If
			if (YTDDCFlag)
			{
				GetYTDNet = Conversion.Val(vs1.TextMatrix(CurrentRow, YTDDebitCol)) - Conversion.Val(vs1.TextMatrix(CurrentRow, YTDCreditCol));
			}
			else
			{
				GetYTDNet = GetYTDDebit() - GetYTDCredit();
			}
			return GetYTDNet;
		}

		private short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		private short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}
		// vbPorter upgrade warning: x As short --> As int	OnWrite(int, double)
		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private double GetEncumbrance()
		{
			double GetEncumbrance = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
									GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
								}
								else
								{
									GetEncumbrance = 0;
								}
							}
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
				return GetEncumbrance;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsYTDActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
				}
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private double GetPending()
		{
			double GetPending = 0;
			GetPending = GetPendingDebits() - GetPendingCredits();
			return GetPending;
		}

		private double GetBalance()
		{
			double GetBalance = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (GetNetBudget() == 0)
				{
					GetBalance = 0;
					return GetBalance;
				}
			}
			if (EncumbranceFlag)
			{
				GetBalance = GetNetBudget() - GetYTDDebit() + GetYTDCredit() - GetEncumbrance();
			}
			else
			{
				GetBalance = GetNetBudget() - GetYTDDebit() + GetYTDCredit();
			}
			if (PendingFlag)
			{
				GetBalance -= GetPending();
			}
			return GetBalance;
		}

		private double GetSpent()
		{
			double GetSpent = 0;
			int temp;
			// vbPorter upgrade warning: temp2 As double	OnWrite(string, Decimal)
			double temp2 = 0;
			// vbPorter upgrade warning: temp3 As double	OnWrite(string, Decimal)
			double temp3 = 0;
			if (NetBudgetFlag)
			{
				if (Strings.Trim(vs1.TextMatrix(CurrentRow, NetBudgetCol)) == "")
				{
					GetSpent = -999;
					return GetSpent;
				}
				else
				{
					temp3 = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, NetBudgetCol));
					if (temp3 == 0)
					{
						GetSpent = -999;
						return GetSpent;
					}
				}
			}
			else
			{
				if (vs1.TextMatrix(CurrentRow, OriginalBudgetCol) == "" || vs1.TextMatrix(CurrentRow, BudgetAdjustmentCol) == "")
				{
					GetSpent = -999;
					return GetSpent;
				}
				else
				{
					temp3 = FCConvert.ToDouble(FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, OriginalBudgetCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, BudgetAdjustmentCol)));
					if (temp3 == 0)
					{
						GetSpent = -999;
						return GetSpent;
					}
				}
			}
			if (BalanceFlag)
			{
				temp2 = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, BalanceCol));
			}
			else if (YTDDCFlag)
			{
				temp2 = FCConvert.ToDouble(temp3 - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDDebitCol)) + FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDCreditCol)));
				if (EncumbranceFlag)
				{
					temp2 -= FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, EncumbranceCol));
				}
				if (PendingFlag)
				{
					temp2 -= FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, PendingCol));
				}
			}
			else
			{
				temp2 = FCConvert.ToDouble(temp3 - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDNetCol)));
				if (EncumbranceFlag)
				{
					temp2 -= FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, EncumbranceCol));
				}
				if (PendingFlag)
				{
					temp2 -= FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, PendingCol));
				}
			}
			if (temp2 > temp3)
			{
				GetSpent = (temp3 - temp2) / temp3;
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, vs1.Cols - 1, Color.Blue);
				return GetSpent;
			}
			GetSpent = (temp3 - temp2) / temp3;
			if (GetSpent > 1)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, vs1.Cols - 1, Color.Red);
			}
			return GetSpent;
		}

		private double GetPendingCredits()
		{
			double GetPendingCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
									GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
								}
								else
								{
									GetPendingCredits = 0;
								}
							}
						}
					}
				}
				else
				{
					GetPendingCredits = 0;
				}
				return GetPendingCredits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			return GetPendingCredits;
		}

		private double GetPendingDebits()
		{
			double GetPendingDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
									GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
								}
								else
								{
									GetPendingDebits = 0;
								}
							}
						}
					}
				}
				else
				{
					GetPendingDebits = 0;
				}
				return GetPendingDebits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private void GetCompleteTotals()
		{
			int counter;
			int counter2;
			double total = 0;
			vs1.Rows += 1;
			vs1.TextMatrix(vs1.Rows - 1, 1, "Final Totals");
			for (counter = 2; counter <= vs1.Cols - 1; counter++)
			{
				total = 0;
				for (counter2 = 2; counter2 <= vs1.Rows - 2; counter2++)
				{
					if (vs1.RowOutlineLevel(counter2) == 1 && vs1.TextMatrix(counter2, counter) != "----")
					{
						total += FCConvert.ToDouble(vs1.TextMatrix(counter2, counter));
					}
				}
				if (counter == SpentCol && SpentFlag)
				{
					vs1.TextMatrix(vs1.Rows - 1, counter, Strings.Format(total, "0.00"));
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, counter, FCConvert.ToString(total));
				}
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.Blue);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void SetColors()
		{
			int counter;
			//FC:FINAL:BBE - first Level for RowOutlineLevel corrected to 1. Therefore color adjusted directly
			//modColorScheme.ColorGrid(vs1, 2, -1, 0, -1, true);
			//for (counter = 2; counter <= vs1.Rows - 2; counter++)
			//{
			//	vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(vs1.RowOutlineLevel(counter) - 1));
			//}
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(99));
			modColorScheme.ColorGrid(vs1, 2, vs1.Rows - 2, 0, vs1.Cols - 1);
			modColorScheme.ColorGrid(vs1, vs1.Rows - 1, vs1.Rows - 1, 0, vs1.Cols - 1, true);
			// For counter = 2 To vs1.rows - 2
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1) = SetGridColor(vs1.RowOutlineLevel(counter))
			// Next
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1) = SetGridColor(99)
		}

		private double GetDepartmentCurrentDebits()
		{
			double GetDepartmentCurrentDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
								}
								else
								{
									GetDepartmentCurrentDebits = 0;
								}
							}
						}
					}
				}
				else
				{
					GetDepartmentCurrentDebits = 0;
				}
				// If Not EncumbranceFlag Then
				// GetDepartmentCurrentDebits = GetDepartmentCurrentDebits + GetEncumbrance
				// End If
				return GetDepartmentCurrentDebits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetDepartmentCurrentDebits = 0;
							}
						}
					}
				}
			}
			else
			{
				GetDepartmentCurrentDebits = 0;
			}
			// If Not EncumbranceFlag Then
			// GetDepartmentCurrentDebits = GetDepartmentCurrentDebits + GetEncumbrance
			// End If
			return GetDepartmentCurrentDebits;
		}

		private double GetDepartmentCurrentCredits()
		{
			double GetDepartmentCurrentCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Di")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Period", CurrentDepartment + "," + CurrentDivision + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Division, Expense, Object, Period", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
					}
					else
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "De")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Period", CurrentDepartment + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ex")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Period", CurrentDepartment + "," + CurrentExpense + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
						else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ob")
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsActivityDetail.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Object")) == CurrentObject && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								if (rsActivityDetail.FindFirstRecord2("Department, Expense, Object, Period", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
								{
									// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
									GetDepartmentCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
								}
								else
								{
									GetDepartmentCurrentCredits = 0;
								}
							}
						}
					}
				}
				else
				{
					GetDepartmentCurrentCredits = 0;
				}
				return GetDepartmentCurrentCredits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					if (CurrentDivision == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
					else if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Division, Expense, Object", CurrentDepartment + "," + CurrentDivision + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
				}
				else
				{
					if (CurrentExpense == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord("Department", CurrentDepartment))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
					else if (CurrentObject == "")
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense", CurrentDepartment + "," + CurrentExpense, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
					else
					{
						if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Expense")) == CurrentExpense && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Object")) == CurrentObject)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							if (rsCurrentActivity.FindFirstRecord2("Department, Expense, Object", CurrentDepartment + "," + CurrentExpense + "," + CurrentObject, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetDepartmentCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
							}
							else
							{
								GetDepartmentCurrentCredits = 0;
							}
						}
					}
				}
			}
			else
			{
				GetDepartmentCurrentCredits = 0;
			}
			return GetDepartmentCurrentCredits;
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private bool IsLowLevel(ref int intRow)
		{
			bool IsLowLevel = false;
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				if (vs1.RowOutlineLevel(intRow) >= 2)
				{
					IsLowLevel = true;
				}
				else
				{
					IsLowLevel = false;
				}
			}
			else
			{
				if (vs1.RowOutlineLevel(intRow) >= 3)
				{
					IsLowLevel = true;
				}
				else
				{
					IsLowLevel = false;
				}
			}
			return IsLowLevel;
		}

		private void GetSubTotals()
		{
			// vbPorter upgrade warning: curTotals As Decimal	OnWrite(short, Decimal)
			Decimal[,] curTotals = new Decimal[3 + 1, 13 + 1];
			int[] intRows = new int[3 + 1];
			int counter;
			int counter2;
			int counter3;
			int temp;
			int intLowLevel = 0;
			if (vs1.Rows > 2)
			{
				for (counter = 0; counter <= 3; counter++)
				{
					for (counter2 = 0; counter2 <= 13; counter2++)
					{
						curTotals[counter, counter2] = 0;
					}
				}
				for (counter = 0; counter <= 3; counter++)
				{
					intRows[counter] = -1;
				}
				intLowLevel = GetLowLevel();
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == intLowLevel + 1)
					{
						for (counter3 = 0; counter3 <= intLowLevel - 1; counter3++)
						{
							if (SpentFlag)
							{
								for (counter2 = 2; counter2 <= vs1.Cols - 2; counter2++)
								{
									curTotals[counter3, counter2] += FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
								}
							}
							else
							{
								for (counter2 = 2; counter2 <= vs1.Cols - 1; counter2++)
								{
									curTotals[counter3, counter2] += FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
								}
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) < intLowLevel + 1)
					{
						if (intRows[vs1.RowOutlineLevel(counter) - 1] < 0)
						{
							intRows[vs1.RowOutlineLevel(counter) - 1] = counter;
						}
						else
						{
							if (SpentFlag)
							{
								for (counter2 = 2; counter2 <= vs1.Cols - 2; counter2++)
								{
									vs1.TextMatrix(intRows[vs1.RowOutlineLevel(counter) - 1], counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter) - 1, counter2]));
									curTotals[vs1.RowOutlineLevel(counter) - 1, counter2] = 0;
								}
							}
							else
							{
								for (counter2 = 2; counter2 <= vs1.Cols - 1; counter2++)
								{
									vs1.TextMatrix(intRows[vs1.RowOutlineLevel(counter) - 1], counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter) - 1, counter2]));
									curTotals[vs1.RowOutlineLevel(counter) - 1, counter2] = 0;
								}
							}
							intRows[vs1.RowOutlineLevel(counter) - 1] = counter;
						}
					}
				}
				for (counter = intLowLevel - 1; counter >= 0; counter--)
				{
					if (vs1.TextMatrix(intRows[counter], 2) == "")
					{
						if (SpentFlag)
						{
							for (counter2 = 2; counter2 <= vs1.Cols - 2; counter2++)
							{
								vs1.TextMatrix(intRows[counter], counter2, FCConvert.ToString(curTotals[counter, counter2]));
							}
						}
						else
						{
							for (counter2 = 2; counter2 <= vs1.Cols - 1; counter2++)
							{
								vs1.TextMatrix(intRows[counter], counter2, FCConvert.ToString(curTotals[counter, counter2]));
							}
						}
					}
				}
			}
		}

		private short GetLowLevel()
		{
			short GetLowLevel = 0;
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				GetLowLevel = 1;
			}
			else
			{
				GetLowLevel = 2;
			}
			return GetLowLevel;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strEncumbData = "";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")) || FCConvert.ToBoolean(rs.Get_Fields_Boolean("SeperatePending")))
			{
				strEncumbData = "SUM(EncumbActivity) + SUM(PendingEncumbActivity) as EncumbActivityTotal";
			}
			else
			{
				strEncumbData = "SUM(EncumbActivity) as EncumbActivityTotal";
			}
			rsBudgetInfo.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division, Expense, Object");
			rsYTDActivity.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(MonthlyBudget) AS MonthlyBudgetTotal, SUM(MonthlyBudgetAdjustments) AS MonthlyBudgetAdjustmentsTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division, Expense, Object");
			strPeriodCheck = strPeriodCheckHolder;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				rsCurrentActivity.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(MonthlyBudget) AS MonthlyBudgetTotal, SUM(MonthlyBudgetAdjustments) AS MonthlyBudgetAdjustmentsTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + " GROUP BY Department, Division, Expense, Object");
				rsActivityDetail.OpenRecordset("SELECT Period, Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(MonthlyBudget) AS MonthlyBudgetTotal, SUM(MonthlyBudgetAdjustments) AS MonthlyBudgetAdjustmentsTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + " GROUP BY Department, Division, Expense, Object, Period");
			}
			else
			{
				rsCurrentActivity.OpenRecordset("SELECT Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(MonthlyBudget) AS MonthlyBudgetTotal, SUM(MonthlyBudgetAdjustments) AS MonthlyBudgetAdjustmentsTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + " GROUP BY Department, Division, Expense, Object");
				rsActivityDetail.OpenRecordset("SELECT Period, Department, Division, Expense, Object, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(MonthlyBudget) AS MonthlyBudgetTotal, SUM(MonthlyBudgetAdjustments) AS MonthlyBudgetAdjustmentsTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + " GROUP BY Department, Division, Expense, Object, Period");
			}
		}

		private void ClearMonthlyYTDAmounts()
		{
			int counter;
			int temp = 0;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				temp = Strings.InStr(1, vs1.TextMatrix(counter, 1), "-", CompareConstants.vbBinaryCompare);
				if (temp == 0)
				{
					if (YTDDCFlag)
					{
						vs1.TextMatrix(counter, YTDDebitCol, "0.00");
						vs1.TextMatrix(counter, YTDCreditCol, "0.00");
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(counter, YTDNetCol, "0.00");
					}
				}
			}
		}
	}
}
