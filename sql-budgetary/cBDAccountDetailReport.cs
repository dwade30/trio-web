﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWBD0000
{
	public class cBDAccountDetailReport
	{
		//=========================================================
		private int intStartPeriod;
		private int intEndPeriod;
		private bool boolIncludeNoActivity;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collAccounts = new cGenericCollection();
		private cGenericCollection collAccounts_AutoInitialized;

		private cGenericCollection collAccounts
		{
			get
			{
				if (collAccounts_AutoInitialized == null)
				{
					collAccounts_AutoInitialized = new cGenericCollection();
				}
				return collAccounts_AutoInitialized;
			}
			set
			{
				collAccounts_AutoInitialized = value;
			}
		}

		private bool boolIncludePosted;
		private bool boolIncludeUnposted;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collSummaries = new cGenericCollection();
		private cGenericCollection collSummaries_AutoInitialized;

		private cGenericCollection collSummaries
		{
			get
			{
				if (collSummaries_AutoInitialized == null)
				{
					collSummaries_AutoInitialized = new cGenericCollection();
				}
				return collSummaries_AutoInitialized;
			}
			set
			{
				collSummaries_AutoInitialized = value;
			}
		}

		public cGenericCollection AccountSummaries
		{
			get
			{
				cGenericCollection AccountSummaries = null;
				AccountSummaries = collSummaries;
				return AccountSummaries;
			}
		}

		public bool IncludePosted
		{
			set
			{
				boolIncludePosted = value;
			}
			get
			{
				bool IncludePosted = false;
				IncludePosted = boolIncludePosted;
				return IncludePosted;
			}
		}

		public bool IncludeUnposted
		{
			set
			{
				boolIncludeUnposted = value;
			}
			get
			{
				bool IncludeUnposted = false;
				IncludeUnposted = boolIncludeUnposted;
				return IncludeUnposted;
			}
		}

		public cGenericCollection Accounts
		{
			get
			{
				cGenericCollection Accounts = null;
				Accounts = collAccounts;
				return Accounts;
			}
		}

		public short StartPeriod
		{
			set
			{
				intStartPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short StartPeriod = 0;
				StartPeriod = FCConvert.ToInt16(intStartPeriod);
				return StartPeriod;
			}
		}

		public short EndPeriod
		{
			set
			{
				intEndPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EndPeriod = 0;
				EndPeriod = FCConvert.ToInt16(intEndPeriod);
				return EndPeriod;
			}
		}

		public cBDAccountDetailReport() : base()
		{
			boolIncludePosted = true;
			boolIncludeUnposted = false;
		}
	}
}
