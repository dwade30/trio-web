﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevenueRanges.
	/// </summary>
	public partial class frmRevenueRanges : BaseForm
	{
		public frmRevenueRanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtLevel1Total = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtLevel1Description = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtLevel2Range = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtLevel2Description = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_0, FCConvert.ToInt16(0));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_1, FCConvert.ToInt16(1));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_2, FCConvert.ToInt16(2));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_3, FCConvert.ToInt16(3));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_4, FCConvert.ToInt16(4));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_5, FCConvert.ToInt16(5));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_6, FCConvert.ToInt16(6));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_7, FCConvert.ToInt16(7));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_8, FCConvert.ToInt16(8));
			this.txtLevel1Total.AddControlArrayElement(txtLevel1Total_9, FCConvert.ToInt16(9));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_0, FCConvert.ToInt16(0));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_1, FCConvert.ToInt16(1));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_2, FCConvert.ToInt16(2));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_3, FCConvert.ToInt16(3));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_4, FCConvert.ToInt16(4));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_5, FCConvert.ToInt16(5));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_6, FCConvert.ToInt16(6));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_7, FCConvert.ToInt16(7));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_8, FCConvert.ToInt16(8));
			this.txtLevel1Description.AddControlArrayElement(txtLevel1Description_9, FCConvert.ToInt16(9));
			this.txtLevel2Range.AddControlArrayElement(txtLevel2Range_0, FCConvert.ToInt16(0));
			this.txtLevel2Range.AddControlArrayElement(txtLevel2Range_1, FCConvert.ToInt16(1));
			this.txtLevel2Range.AddControlArrayElement(txtLevel2Range_2, FCConvert.ToInt16(2));
			this.txtLevel2Range.AddControlArrayElement(txtLevel2Range_3, FCConvert.ToInt16(3));
			this.txtLevel2Range.AddControlArrayElement(txtLevel2Range_4, FCConvert.ToInt16(4));
			this.txtLevel2Description.AddControlArrayElement(txtLevel2Description_0, FCConvert.ToInt16(0));
			this.txtLevel2Description.AddControlArrayElement(txtLevel2Description_1, FCConvert.ToInt16(1));
			this.txtLevel2Description.AddControlArrayElement(txtLevel2Description_2, FCConvert.ToInt16(2));
			this.txtLevel2Description.AddControlArrayElement(txtLevel2Description_3, FCConvert.ToInt16(3));
			this.txtLevel2Description.AddControlArrayElement(txtLevel2Description_4, FCConvert.ToInt16(4));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRevenueRanges InstancePtr
		{
			get
			{
				return (frmRevenueRanges)Sys.GetInstance(typeof(frmRevenueRanges));
			}
		}

		protected frmRevenueRanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// vbPorter upgrade warning: DeptLength As short --> As int	OnWrite(string)
		int DeptLength;
		// vbPorter upgrade warning: RevLength As short --> As int	OnWrite(string)
		int RevLength;
		clsDRWrapper rs = new clsDRWrapper();
		int CurrIndex;
		int CurrControl;
		bool ValidateFlag;
		string[,] Level2Ranges = new string[9 + 1, 4 + 1];
		string[,] Level2Descriptions = new string[9 + 1, 4 + 1];
		bool CheckFlag;
		bool[] Subtotals = new bool[9 + 1];
		bool FirstDepartment;
		string CurrDepartment = "";
		string CurrDivision = "";
		bool SavedAlready;
		bool ChangeFlag;
		bool InsertFlag;
		// vbPorter upgrade warning: DivLength As short --> As int	OnWrite(string)
		int DivLength;
		int DeptCurrIndex;
		int DivCurrIndex;
		bool MoveFlag;
		bool blnUnload;

		private void cmbDepartment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			bool falseTemp = false;
			cmbDepartment_Validate(ref falseTemp);
		}

		private void cmbDepartment_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cmbDepartment.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbDivision_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cmbDivision.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbDepartment_Enter(object sender, System.EventArgs e)
		{
			MoveFlag = false;
		}

		private void cmbDepartment_Validating_2(bool Cancel)
		{
			cmbDepartment_Validating(cmbDepartment, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cmbDepartment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			string temp = "";
			lblDivisionName.Visible = false;
			lblDepartmentName.Visible = false;
			//Line1.Visible = false;
			lblLevel1.Visible = false;
			cmdCreate.Visible = false;
			cmdDelete.Visible = false;
			//Line2.Visible = false;
			lblLevel2.Visible = false;
			lblLevel2Range.Visible = false;
			lblLevel2Name.Visible = false;
			for (counter = 0; counter <= 9; counter++)
			{
				txtLevel1Total[FCConvert.ToInt16(counter)].Visible = false;
				txtLevel1Description[FCConvert.ToInt16(counter)].Visible = false;
				if (counter < 5)
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].Visible = false;
					txtLevel2Description[FCConvert.ToInt16(counter)].Visible = false;
				}
			}
			if (modAccountTitle.Statics.RevDivFlag)
			{
				temp = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				if (SavedAlready)
				{
					SavedAlready = false;
				}
				else
				{
					if (FirstDepartment)
					{
						FirstDepartment = false;
						CurrDepartment = Strings.Left(cmbDepartment.Text, DeptLength);
						DeptCurrIndex = cmbDepartment.SelectedIndex;
					}
					else
					{
						if (lblLevel2.Visible == true)
						{
							SaveInfo(ref CurrIndex);
						}
						if (lblLevel1.Visible == true)
						{
							if (!ValidateData())
							{
								cmbDepartment.SelectedIndex = DeptCurrIndex;
								return;
							}
							SaveAllData();
						}
						CurrDepartment = Strings.Left(cmbDepartment.Text, DeptLength);
						DeptCurrIndex = cmbDepartment.SelectedIndex;
					}
					rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "' AND Division = '" + temp + "'");
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rs.Get_Fields_String("LongDescription")) == "")
						{
							lblDepartmentName.Text = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
						}
						else
						{
							lblDepartmentName.Text = FCConvert.ToString(rs.Get_Fields_String("LongDescription"));
						}
						DeleteAllData();
						rs.OpenRecordset("SELECT * FROM RevenueRanges WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "'");
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							GetAllData();
						}
						else
						{
							FCUtils.EraseSafe(Level2Ranges);
							FCUtils.EraseSafe(Level2Descriptions);
						}
						lblDepartmentName.Visible = true;
						lblLevel1.Visible = true;
						//Line1.Visible = true;
						for (counter = 0; counter <= 4; counter++)
						{
							txtLevel2Range[FCConvert.ToInt16(counter)].Text = Level2Ranges[0, counter];
							txtLevel2Description[FCConvert.ToInt16(counter)].Text = Level2Descriptions[0, counter];
						}
						CurrIndex = 0;
						if (Subtotals[0] == false)
						{
							HideSubrange();
							cmdDelete.Visible = false;
							cmdCreate.Visible = true;
						}
						else
						{
							ShowSubrange();
							cmdCreate.Visible = false;
							cmdDelete.Visible = true;
						}
						for (counter = 0; counter <= 9; counter++)
						{
							txtLevel1Total[FCConvert.ToInt16(counter)].Visible = true;
							txtLevel1Description[FCConvert.ToInt16(counter)].Visible = true;
						}
						if (MoveFlag)
						{
							MoveFlag = false;
							return;
						}
						txtLevel1Total[0].Focus();
					}
					else
					{
						MessageBox.Show("There is no such Department in the database", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
			}
			else
			{
				if (txtLevel2Range[0].Text != "")
				{
					SaveInfo(ref CurrIndex);
				}
				if (txtLevel1Total[0].Text != "")
				{
					if (!ValidateData())
					{
						cmbDepartment.SelectedIndex = DeptCurrIndex;
						return;
					}
					SaveAllData();
				}
				CurrDepartment = Strings.Left(cmbDepartment.Text, DeptLength);
				DeptCurrIndex = cmbDepartment.SelectedIndex;
				FillDivBox();
				if (MoveFlag)
				{
					MoveFlag = false;
					return;
				}
				if (cmbDivision.Visible == true)
				{
					cmbDivision.Focus();
				}
			}
		}

		public void cmbDepartment_Validate(ref bool Cancel)
		{
			cmbDepartment_Validating(cmbDepartment, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cmbDivision_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			bool falseTemp = false;
			cmbDivision_Validate(ref falseTemp);
		}

		private void cmbDivision_Enter(object sender, System.EventArgs e)
		{
			MoveFlag = false;
		}

		private void cmbDivision_Validating_2(bool Cancel)
		{
			cmbDivision_Validating(cmbDivision, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cmbDivision_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			string temp;
			if (cmbDivision.Text == "")
			{
				return;
			}
			temp = modValidateAccount.GetFormat("0", ref DivLength);
			if (SavedAlready)
			{
				SavedAlready = false;
			}
			else
			{
				if (FirstDepartment)
				{
					FirstDepartment = false;
					CurrDivision = Strings.Left(cmbDivision.Text, DivLength);
					DivCurrIndex = cmbDivision.SelectedIndex;
				}
				else
				{
					if (lblLevel2.Visible == true)
					{
						SaveInfo(ref CurrIndex);
					}
					if (lblLevel1.Visible == true)
					{
						if (!ValidateData())
						{
							cmbDepartment.SelectedIndex = DeptCurrIndex;
							return;
						}
						SaveAllData();
					}
					CurrDivision = Strings.Left(cmbDivision.Text, DivLength);
					DivCurrIndex = cmbDivision.SelectedIndex;
				}
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "' AND Division = '" + temp + "'");
				if (FCConvert.ToString(rs.Get_Fields_String("LongDescription")) == "")
				{
					lblDepartmentName.Text = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
				}
				else
				{
					lblDepartmentName.Text = FCConvert.ToString(rs.Get_Fields_String("LongDescription"));
				}
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "' AND Division = '" + Strings.Left(cmbDivision.Text, DivLength) + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rs.Get_Fields_String("LongDescription")) == "")
					{
						lblDivisionName.Text = FCConvert.ToString(rs.Get_Fields_String("ShortDescription"));
					}
					else
					{
						lblDivisionName.Text = FCConvert.ToString(rs.Get_Fields_String("LongDescription"));
					}
					DeleteAllData();
					rs.OpenRecordset("SELECT * FROM RevenueRanges WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "' AND Division = '" + Strings.Left(cmbDivision.Text, DivLength) + "'");
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						GetAllData();
					}
					else
					{
						FCUtils.EraseSafe(Level2Ranges);
						FCUtils.EraseSafe(Level2Descriptions);
						for (counter = 0; counter <= 9; counter++)
						{
							Subtotals[counter] = false;
						}
					}
					lblDepartmentName.Visible = true;
					lblDivisionName.Visible = true;
					lblLevel1.Visible = true;
					//Line1.Visible = true;
					for (counter = 0; counter <= 4; counter++)
					{
						txtLevel2Range[FCConvert.ToInt16(counter)].Text = Level2Ranges[0, counter];
						txtLevel2Description[FCConvert.ToInt16(counter)].Text = Level2Descriptions[0, counter];
					}
					CurrIndex = 0;
					if (Subtotals[0] == false)
					{
						HideSubrange();
						cmdDelete.Visible = false;
						cmdCreate.Visible = true;
					}
					else
					{
						ShowSubrange();
						cmdCreate.Visible = false;
						cmdDelete.Visible = true;
					}
					for (counter = 0; counter <= 9; counter++)
					{
						txtLevel1Total[FCConvert.ToInt16(counter)].Visible = true;
						txtLevel1Description[FCConvert.ToInt16(counter)].Visible = true;
					}
					if (MoveFlag)
					{
						MoveFlag = false;
						return;
					}
					txtLevel1Total[0].Focus();
				}
				else
				{
					MessageBox.Show("There is no such Department in the database", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
				}
			}
		}

		public void cmbDivision_Validate(ref bool Cancel)
		{
			cmbDivision_Validating(cmbDivision, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cmdCreate_Click(object sender, System.EventArgs e)
		{
			int temp;
			string LowBound = "";
			string HighBound = "";
			int counter;
			if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text == "" || txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text == "")
			{
				MessageBox.Show("You must enter a Range and a Description before you can create subranges", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text == "")
				{
					txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Focus();
				}
				else
				{
					txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Focus();
				}
				return;
			}
			else
			{
				HighBound = txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text;
				if (CurrIndex == 0)
				{
					LowBound = "0";
				}
				else
				{
					LowBound = txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Text;
				}
				LowBound = modValidateAccount.GetFormat(Conversion.Str(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(LowBound))) + 1), ref RevLength);
				lblLevel2Range.Text = LowBound + " - " + HighBound;
				lblLevel2Name.Text = txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text;
				for (counter = 0; counter <= 4; counter++)
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].Text = "";
					txtLevel2Description[FCConvert.ToInt16(counter)].Text = "";
				}
				ShowSubrange();
				Subtotals[CurrIndex] = true;
				cmdCreate.Visible = false;
				cmdDelete.Visible = true;
				txtLevel2Range[0].Focus();
			}
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			int counter;
			Subtotals[CurrIndex] = false;
			for (counter = 0; counter <= 4; counter++)
			{
				Level2Ranges[CurrIndex, counter] = "";
				txtLevel2Range[FCConvert.ToInt16(counter)].Text = "";
				Level2Descriptions[CurrIndex, counter] = "";
				txtLevel2Description[FCConvert.ToInt16(counter)].Text = "";
			}
			SaveInfo(ref CurrIndex);
			HideSubrange();
			txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Focus();
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void frmRevenueRanges_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			DeptLength = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 1, 2));
			RevLength = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2));
			DivLength = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2));
			if (DivLength == 0)
			{
				lblDepartment.Left = FCConvert.ToInt32(frmRevenueRanges.InstancePtr.WidthOriginal * 0.36);
				cmbDepartment.Left = lblDepartment.Left + lblDepartment.WidthOriginal + 30;
				cmbDivision.Visible = false;
				lblDivision.Visible = false;
			}
			else
			{
				cmbDivision.Visible = true;
				lblDivision.Visible = true;
			}
			FillBox();
			for (counter = 0; counter <= 9; counter++)
			{
				txtLevel1Total[FCConvert.ToInt16(counter)].MaxLength = RevLength;
				txtLevel1Description[FCConvert.ToInt16(counter)].MaxLength = 20;
				if (counter < 5)
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].MaxLength = RevLength;
					txtLevel2Description[FCConvert.ToInt16(counter)].MaxLength = 20;
				}
			}
			FirstDepartment = true;
			CurrIndex = 0;
			lblDepartment.Visible = true;
			cmbDepartment.Visible = true;
			this.Refresh();
		}

		private void frmRevenueRanges_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int counter = 0;
			int counter2;
			int temp = 0;
			string holder = "";
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			else if (KeyCode == Keys.Insert)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					// do nothing
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Description")
				{
					if (txtLevel1Total[9].Text != "")
					{
						MessageBox.Show("You must delete a Range before you can add a new one", "Too Many Ranges", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						InsertFlag = true;
						counter = CurrIndex;
						while (txtLevel1Total[FCConvert.ToInt16(counter)].Text != "")
						{
							counter += 1;
						}
						for (counter = counter - 1; counter >= CurrIndex; counter--)
						{
							txtLevel1Total[FCConvert.ToInt16(counter + 1)].Text = txtLevel1Total[FCConvert.ToInt16(counter)].Text;
							txtLevel1Description[FCConvert.ToInt16(counter + 1)].Text = txtLevel1Description[FCConvert.ToInt16(counter)].Text;
							Subtotals[counter + 1] = Subtotals[counter];
							for (counter2 = 0; counter2 <= 4; counter2++)
							{
								Level2Ranges[counter + 1, counter2] = Level2Ranges[counter, counter2];
								Level2Descriptions[counter + 1, counter2] = Level2Descriptions[counter, counter2];
							}
						}
						Subtotals[CurrIndex] = false;
						txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text = "";
						txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text = "";
						for (counter2 = 0; counter2 <= 4; counter2++)
						{
							Level2Ranges[CurrIndex, counter2] = "";
							Level2Descriptions[CurrIndex, counter2] = "";
						}
						HideSubrange();
						txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Focus();
					}
				}
				else
				{
					if (txtLevel2Range[4].Text != "")
					{
						MessageBox.Show("You must delete a Subrange before you can add a new one", "Too Many Subranges", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						temp = (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex());
						counter = temp;
						while (txtLevel2Range[FCConvert.ToInt16(counter)].Text != "")
						{
							counter += 1;
						}
						for (counter = counter - 1; counter >= temp; counter--)
						{
							txtLevel2Range[FCConvert.ToInt16(counter + 1)].Text = txtLevel2Range[FCConvert.ToInt16(counter)].Text;
							txtLevel2Description[FCConvert.ToInt16(counter + 1)].Text = txtLevel2Description[FCConvert.ToInt16(counter)].Text;
						}
						txtLevel2Range[FCConvert.ToInt16(temp)].Text = "";
						txtLevel2Description[FCConvert.ToInt16(temp)].Text = "";
						txtLevel2Range[FCConvert.ToInt16(temp)].Focus();
					}
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					MoveFlag = true;
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total")
				{
					KeyCode = (Keys)0;
					if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text != "" || txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text != "")
					{
						if (CheckLevel1Total(CurrIndex))
						{
							if (CurrIndex == 9)
							{
								txtLevel1Total[0].Focus();
								txtLevel1Total[0].SelectionStart = 0;
							}
							else
							{
								txtLevel1Total[FCConvert.ToInt16(CurrIndex + 1)].Focus();
								txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
							}
						}
					}
					else
					{
						if (CurrIndex == 9)
						{
							txtLevel1Total[0].Focus();
							txtLevel1Total[0].SelectionStart = 0;
						}
						else
						{
							txtLevel1Total[FCConvert.ToInt16(CurrIndex + 1)].Focus();
							txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
						}
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Description")
				{
					KeyCode = (Keys)0;
					if (CurrIndex == 9)
					{
						txtLevel1Description[0].Focus();
						txtLevel1Description[0].SelectionStart = 0;
					}
					else
					{
						txtLevel1Description[FCConvert.ToInt16(CurrIndex + 1)].Focus();
						txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel2Range")
				{
					KeyCode = (Keys)0;
					if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "" || txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "")
					{
						if (CheckLevel2Total(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex()))
						{
							if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 4)
							{
								txtLevel2Range[0].Focus();
								txtLevel2Range[0].SelectionStart = 0;
							}
							else
							{
								txtLevel2Range[FCConvert.ToInt16((frmRevenueRanges.InstancePtr.ActiveControl).GetIndex() + 1)].Focus();
								txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
							}
						}
					}
					else
					{
						if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 4)
						{
							txtLevel2Range[0].Focus();
							txtLevel2Range[0].SelectionStart = 0;
						}
						else
						{
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() + 1)].Focus();
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
						}
					}
				}
				else
				{
					KeyCode = (Keys)0;
					if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 4)
					{
						txtLevel2Description[0].Focus();
						txtLevel2Description[0].SelectionStart = 0;
					}
					else
					{
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() + 1)].Focus();
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					MoveFlag = true;
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total")
				{
					KeyCode = (Keys)0;
					if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text != "" || txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text != "")
					{
						if (CheckLevel1Total(CurrIndex) && txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text != "")
						{
							if (CurrIndex == 0)
							{
								txtLevel1Total[9].Focus();
								txtLevel1Total[9].SelectionStart = 0;
							}
							else
							{
								txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Focus();
								txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
							}
						}
						else
						{
							txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Focus();
						}
					}
					else
					{
						if (CurrIndex == 0)
						{
							txtLevel1Total[9].Focus();
							txtLevel1Total[9].SelectionStart = 0;
						}
						else
						{
							txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Focus();
							txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
						}
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Description")
				{
					KeyCode = (Keys)0;
					if (CurrIndex == 0)
					{
						txtLevel1Description[9].Focus();
						txtLevel1Description[9].SelectionStart = 0;
					}
					else
					{
						txtLevel1Description[FCConvert.ToInt16(CurrIndex - 1)].Focus();
						txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel2Range")
				{
					KeyCode = (Keys)0;
					if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "" || txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "")
					{
						if (CheckLevel2Total(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex()))
						{
							if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 0)
							{
								txtLevel2Range[4].Focus();
								txtLevel2Range[4].SelectionStart = 0;
							}
							else
							{
								txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() - 1)].Focus();
								txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
							}
						}
					}
					else
					{
						if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 0)
						{
							txtLevel2Range[4].Focus();
							txtLevel2Range[4].SelectionStart = 0;
						}
						else
						{
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() - 1)].Focus();
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
						}
					}
				}
				else
				{
					KeyCode = (Keys)0;
					if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 0)
					{
						txtLevel2Description[4].Focus();
						txtLevel2Description[4].SelectionStart = 0;
					}
					else
					{
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() - 1)].Focus();
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					MoveFlag = true;
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total")
				{
					if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text != "" || txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text != "")
					{
						if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart == 0)
						{
							if (CheckLevel1Total(CurrIndex))
							{
								KeyCode = (Keys)0;
								if (CurrIndex == 0)
								{
									txtLevel1Description[9].Focus();
									txtLevel1Description[9].SelectionStart = 0;
								}
								else
								{
									txtLevel1Description[FCConvert.ToInt16(CurrIndex - 1)].Focus();
									txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
								}
							}
						}
					}
					else
					{
						KeyCode = (Keys)0;
						if (CurrIndex == 0)
						{
							txtLevel1Description[9].Focus();
							txtLevel1Description[9].SelectionStart = 0;
						}
						else
						{
							txtLevel1Description[FCConvert.ToInt16(CurrIndex - 1)].Focus();
							txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
						}
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Description")
				{
					if (txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart == 0)
					{
						KeyCode = (Keys)0;
						txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Focus();
						txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel2Range")
				{
					if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "" || txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "")
					{
						if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart == 0)
						{
							if (CheckLevel2Total(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex()))
							{
								KeyCode = (Keys)0;
								if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 0)
								{
									txtLevel2Description[4].Focus();
									txtLevel2Description[4].SelectionStart = 0;
								}
								else
								{
									txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() - 1)].Focus();
									txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
								}
							}
						}
					}
					else
					{
						KeyCode = (Keys)0;
						if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 0)
						{
							txtLevel2Description[4].Focus();
							txtLevel2Description[4].SelectionStart = 0;
						}
						else
						{
							txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() - 1)].Focus();
							txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
						}
					}
				}
				else
				{
					if (txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart == 0)
					{
						KeyCode = (Keys)0;
						txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Focus();
						txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment" || frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					MoveFlag = true;
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total")
				{
					if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text != "" || txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text != "")
					{
						if (txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart == txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text.Length)
						{
							if (CheckLevel1Total(CurrIndex))
							{
								KeyCode = (Keys)0;
								txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Focus();
								txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
							}
						}
					}
					else
					{
						KeyCode = (Keys)0;
						txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Focus();
						txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Description")
				{
					if (txtLevel1Description[FCConvert.ToInt16(CurrIndex)].SelectionStart == txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text.Length)
					{
						KeyCode = (Keys)0;
						if (CurrIndex == 9)
						{
							txtLevel1Total[0].Focus();
							txtLevel1Total[0].SelectionStart = 0;
						}
						else
						{
							txtLevel1Total[FCConvert.ToInt16(CurrIndex + 1)].Focus();
							txtLevel1Total[FCConvert.ToInt16(CurrIndex)].SelectionStart = 0;
						}
					}
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel2Range")
				{
					if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "" || txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text != "")
					{
						if (txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart == txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text.Length)
						{
							if (CheckLevel2Total(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex()))
							{
								KeyCode = (Keys)0;
								txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Focus();
								txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
							}
						}
					}
					else
					{
						KeyCode = (Keys)0;
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Focus();
						txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
					}
				}
				else
				{
					if (txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart == txtLevel2Description[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].Text.Length)
					{
						KeyCode = (Keys)0;
						if (frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() == 4)
						{
							txtLevel2Range[0].Focus();
							txtLevel2Range[0].SelectionStart = 0;
						}
						else
						{
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex() + 1)].Focus();
							txtLevel2Range[FCConvert.ToInt16(frmRevenueRanges.InstancePtr.ActiveControl.GetIndex())].SelectionStart = 0;
						}
					}
				}
			}
		}

		private void frmRevenueRanges_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRevenueRanges.FillStyle	= 0;
			//frmRevenueRanges.ScaleWidth	= 9045;
			//frmRevenueRanges.ScaleHeight	= 7620;
			//frmRevenueRanges.LinkTopic	= "Form2";
			//frmRevenueRanges.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmRevenueRanges_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			int counter;
			string temp = "";
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "txtLevel1Total" && txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text == "")
				{
					txtLevel1Total_Validate(FCConvert.ToInt16(CurrIndex), false);
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDivision")
				{
					MoveFlag = false;
					bool falseTemp = false;
					cmbDivision_Validate(ref falseTemp);
				}
				else if (frmRevenueRanges.InstancePtr.ActiveControl.GetName() == "cmbDepartment")
				{
					MoveFlag = false;
					bool falseTemp = false;
					cmbDepartment_Validate(ref falseTemp);
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveTotalInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveTotalInfo();
		}

		private void btnProcessView_Click(object sender, System.EventArgs e)
		{
			// FC: FINAL: KV: IIT807 + FC - 8697
			this.Hide();
			frmRevView.InstancePtr.Show(App.MainForm);
		}

		private void txtLevel1Description_TextChanged(int Index, object sender, System.EventArgs e)
		{
			int temp = 0;
			if (!ChangeFlag)
			{
				if (Index > 0)
				{
					if (txtLevel1Total[FCConvert.ToInt16(Index - 1)].Text == "")
					{
						MessageBox.Show("You must enter ranges in order", "Invalid Range Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						ChangeFlag = true;
						txtLevel1Description[Index].Text = "";
						ChangeFlag = false;
						temp = Index - 1;
						while (txtLevel1Total[FCConvert.ToInt16(temp)].Text == "")
						{
							temp -= 1;
							if (temp == 0)
							{
								break;
							}
						}
						if (temp == 0)
						{
							if (txtLevel1Total[0].Text == "")
							{
								txtLevel1Total[0].Focus();
							}
							else
							{
								txtLevel1Total[1].Focus();
							}
						}
					}
				}
				if (txtLevel1Description[Index].SelectionStart == 20)
				{
					if (CheckLevel1Description(Index))
					{
						if (Index < 9)
						{
							txtLevel1Total[FCConvert.ToInt16(Index + 1)].Focus();
						}
						else
						{
							cmbDepartment.Focus();
						}
					}
				}
				else
				{
					if (txtLevel1Description[Index].SelectionStart > 0)
					{
						CurrIndex = Index;
					}
				}
			}
		}

		private void txtLevel1Description_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtLevel1Description.IndexOf((FCTextBox)sender);
			txtLevel1Description_TextChanged(index, sender, e);
		}

		private void txtLevel1Description_Enter(int Index, object sender, System.EventArgs e)
		{
			int temp;
			if (Index != CurrIndex)
			{
				if (lblLevel2Name.Visible == true)
				{
					SaveInfo(ref CurrIndex);
					HideSubrange();
					if (Subtotals[Index] == true)
					{
						cmdDelete.Visible = true;
						cmdCreate.Visible = false;
					}
					else
					{
						cmdCreate.Visible = true;
						cmdDelete.Visible = false;
					}
				}
				CurrIndex = Index;
			}
			else
			{
				if (lblLevel2Name.Visible == true)
				{
					SaveInfo(ref CurrIndex);
				}
			}
			if (Subtotals[CurrIndex] == true)
			{
				GetInfo(ref CurrIndex);
				ShowSubrange();
				cmdDelete.Visible = true;
				cmdCreate.Visible = false;
			}
			else
			{
				cmdCreate.Visible = true;
				cmdDelete.Visible = false;
			}
		}

		private void txtLevel1Description_Enter(object sender, System.EventArgs e)
		{
			int index = txtLevel1Description.IndexOf((FCTextBox)sender);
			txtLevel1Description_Enter(index, sender, e);
		}

		private void txtLevel1Description_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtLevel1Description[Index].SelectionStart > 0)
			{
				if (txtLevel1Total[Index].Text == "")
				{
					MessageBox.Show("You must enter a Range before you create the Description", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtLevel1Description[Index].Text = "";
					txtLevel1Total[Index].Focus();
					return;
				}
			}
			if (Subtotals[Index] == true)
			{
				if (txtLevel1Description[Index].Text != lblLevel2Name.Text)
				{
					lblLevel2Name.Text = txtLevel1Description[Index].Text;
				}
			}
		}

		private void txtLevel1Description_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtLevel1Description.IndexOf((FCTextBox)sender);
			txtLevel1Description_Validating(index, sender, e);
		}

		private void txtLevel1Total_TextChanged(int Index, object sender, System.EventArgs e)
		{
			int temp = 0;
			if (!ChangeFlag)
			{
				if (Index > 0)
				{
					if (txtLevel1Total[FCConvert.ToInt16(Index - 1)].Text == "")
					{
						MessageBox.Show("You must enter ranges in order", "Invalid Range Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						ChangeFlag = true;
						txtLevel1Total[Index].Text = "";
						ChangeFlag = false;
						temp = Index - 1;
						while (txtLevel1Total[FCConvert.ToInt16(temp)].Text == "")
						{
							temp -= 1;
							if (temp <= 0)
							{
								break;
							}
						}
						if (temp == 0)
						{
							if (txtLevel1Total[0].Text == "")
							{
								txtLevel1Total[0].Focus();
							}
							else
							{
								txtLevel1Total[1].Focus();
							}
						}
					}
				}
				if (txtLevel1Total[Index].SelectionStart == RevLength)
				{
					if (CheckLevel1Total(Index))
					{
						txtLevel1Description[Index].Focus();
					}
				}
				else
				{
					if (txtLevel1Total[Index].SelectionStart > 0)
					{
						CurrIndex = Index;
					}
				}
			}
		}

		private void txtLevel1Total_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtLevel1Total.IndexOf((FCTextBox)sender);
			txtLevel1Total_TextChanged(index, sender, e);
		}

		private void txtLevel1Total_Enter(int Index, object sender, System.EventArgs e)
		{
			int temp;
			if (lblLevel2Name.Visible == true)
			{
				SaveInfo(ref CurrIndex);
			}
			CurrIndex = Index;
			if (Subtotals[CurrIndex] == true)
			{
				GetInfo(ref CurrIndex);
				ShowSubrange();
				cmdDelete.Visible = true;
				cmdCreate.Visible = false;
			}
			else
			{
				HideSubrange();
				cmdCreate.Visible = true;
				cmdDelete.Visible = false;
			}
		}

		private void txtLevel1Total_Enter(object sender, System.EventArgs e)
		{
			int index = txtLevel1Total.IndexOf((FCTextBox)sender);
			txtLevel1Total_Enter(index, sender, e);
		}

		private void txtLevel1Total_Validating_6(short Index, bool Cancel)
		{
			txtLevel1Total_Validating(Index, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtLevel1Total_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: temp As string	OnWrite(string, DialogResult)
			string temp = "";
			int counter = 0;
			int counter2;
			if (txtLevel1Total[Index].SelectionStart > 0)
			{
				temp = txtLevel1Total[Index].Text;
				txtLevel1Total[Index].Text = modValidateAccount.GetFormat(temp, ref RevLength);
				if (!Information.IsNumeric(txtLevel1Total[Index].Text) || Conversion.Val(txtLevel1Total[Index].Text) < 1)
				{
					MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtLevel1Total[Index].SelectionStart = 0;
					txtLevel1Total[Index].SelectionLength = txtLevel1Total[Index].Text.Length;
				}
			}
			else if (txtLevel1Total[Index].Text == "")
			{
				Subtotals[Index] = false;
				cmdDelete_Click();
				txtLevel1Description[Index].Text = "";
				if (CurrIndex < 9)
				{
					counter = CurrIndex;
				}
				else
				{
					return;
				}
				while (txtLevel1Total[FCConvert.ToInt16(counter + 1)].Text != "")
				{
					txtLevel1Total[FCConvert.ToInt16(counter)].Text = txtLevel1Total[FCConvert.ToInt16(counter + 1)].Text;
					txtLevel1Description[FCConvert.ToInt16(counter)].Text = txtLevel1Description[FCConvert.ToInt16(counter + 1)].Text;
					Subtotals[counter] = Subtotals[counter + 1];
					for (counter2 = 0; counter2 <= 4; counter2++)
					{
						Level2Ranges[counter, counter2] = Level2Ranges[counter + 1, counter2];
						Level2Descriptions[counter, counter2] = Level2Descriptions[counter + 1, counter2];
					}
					counter += 1;
					if (counter == 9)
					{
						break;
					}
				}
				txtLevel1Total[FCConvert.ToInt16(counter)].Text = "";
				txtLevel1Description[FCConvert.ToInt16(counter)].Text = "";
				Subtotals[counter] = false;
				for (counter2 = 0; counter2 <= 4; counter2++)
				{
					Level2Ranges[counter, counter2] = "";
					Level2Descriptions[counter, counter2] = "";
				}
				txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Focus();
				if (Subtotals[CurrIndex] == true)
				{
					GetInfo(ref CurrIndex);
					ShowSubrange();
				}
				else
				{
					HideSubrange();
				}
			}
			if (Index > 0)
			{
				if (txtLevel1Total[Index].Text != "")
				{
					if (fecherFoundation.Strings.CompareString(txtLevel1Total[Index].Text, txtLevel1Total[FCConvert.ToInt16(Index - 1)].Text, true) <= 0)
					{
						MessageBox.Show("Your Ranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						txtLevel1Total[Index].Text = "";
						txtLevel1Total[Index].SelectionStart = 0;
						return;
					}
				}
			}
			if (Index < 9)
			{
				if (txtLevel1Total[FCConvert.ToInt16(Index + 1)].Text != "")
				{
					if (fecherFoundation.Strings.CompareString(txtLevel1Total[Index].Text, txtLevel1Total[FCConvert.ToInt16(Index + 1)].Text, true) >= 0)
					{
						MessageBox.Show("Your Ranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						txtLevel1Total[Index].Text = "";
						txtLevel1Total[Index].SelectionStart = 0;
						return;
					}
				}
			}
			if (InsertFlag)
			{
				if (Subtotals[Index + 1] == true)
				{
					if (FCConvert.ToDouble(Level2Ranges[Index + 1, 0]) <= Conversion.Val(txtLevel1Total[Index].Text))
					{
						DialogResult ans = MessageBox.Show("Inserting this new range will delete some of the Subranges in the " + txtLevel1Description[FCConvert.ToInt16(Index + 1)].Text + " Range.  Do you wish to do this?", "Range Overlap Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							InsertFlag = false;
							for (counter = 0; counter <= 4; counter++)
							{
								if (Conversion.Val(Level2Ranges[Index + 1, counter]) <= Conversion.Val(txtLevel1Total[Index].Text))
								{
									Level2Ranges[Index + 1, counter] = "";
									Level2Descriptions[Index + 1, counter] = "";
								}
								else
								{
									break;
								}
							}
							counter = 0;
							counter2 = 0;
							while (Level2Ranges[Index + 1, counter2] == "")
							{
								counter2 += 1;
								if (counter2 == 5)
								{
									counter2 = 0;
									break;
								}
							}
							if (counter2 == 0)
							{
								return;
							}
							else
							{
								while (counter2 < 5)
								{
									Level2Ranges[Index + 1, counter] = Level2Ranges[Index + 1, counter2];
									Level2Descriptions[Index + 1, counter] = Level2Descriptions[Index + 1, counter2];
									Level2Ranges[Index + 1, counter2] = "";
									Level2Descriptions[Index + 1, counter2] = "";
									counter += 1;
									counter2 += 1;
									if (counter2 < 5)
									{
										if (Level2Ranges[Index + 1, counter2] == "")
										{
											break;
										}
									}
								}
							}
						}
						else
						{
							e.Cancel = true;
							txtLevel1Total[Index].Text = "";
							txtLevel1Total[Index].Focus();
							return;
						}
					}
					else
					{
						InsertFlag = false;
					}
				}
			}
			if (Subtotals[Index] == true)
			{
				if (txtLevel1Total[Index].Text != Strings.Right(lblLevel2Range.Text, RevLength))
				{
					lblLevel2Range.Text = Strings.Left(lblLevel2Range.Text, lblLevel2Range.Text.Length - RevLength) + txtLevel1Total[Index].Text;
				}
			}
		}

		public void txtLevel1Total_Validate(short Index, bool Cancel)
		{
			txtLevel1Total_Validating(Index, txtLevel1Total[Index], new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtLevel1Total_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtLevel1Total.IndexOf((FCTextBox)sender);
			txtLevel1Total_Validating(index, sender, e);
		}

		private void txtLevel2Description_TextChanged(int Index, object sender, System.EventArgs e)
		{
			if (txtLevel2Description[Index].SelectionStart == 20)
			{
				if (CheckLevel2Description(Index))
				{
					if (Index < 4)
					{
						txtLevel2Range[FCConvert.ToInt16(Index + 1)].Focus();
					}
					else
					{
						txtLevel2Range[0].Focus();
					}
				}
			}
		}

		private void txtLevel2Description_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtLevel2Description.IndexOf((FCTextBox)sender);
			txtLevel2Description_TextChanged(index, sender, e);
		}

		private void txtLevel2Description_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtLevel2Description[Index].SelectionStart > 0)
			{
				if (txtLevel2Range[Index].Text == "")
				{
					MessageBox.Show("You must enter a Range before you create the Description", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtLevel2Range[Index].Focus();
					return;
				}
			}
		}

		private void txtLevel2Description_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtLevel2Description.IndexOf((FCTextBox)sender);
			txtLevel2Description_Validating(index, sender, e);
		}

		private void txtLevel2Range_TextChanged(int Index, object sender, System.EventArgs e)
		{
			if (txtLevel2Range[Index].SelectionStart == RevLength)
			{
				txtLevel2Range_Validating_6(Index, false);
			}
		}

		private void txtLevel2Range_TextChanged(object sender, System.EventArgs e)
		{
			int index = txtLevel2Range.IndexOf((FCTextBox)sender);
			txtLevel2Range_TextChanged(index, sender, e);
		}

		private void txtLevel2Range_Validating_6(int Index, bool Cancel)
		{
			txtLevel2Range_Validating(Index, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtLevel2Range_Validating(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			int counter = 0;
			if (txtLevel2Range[Index].SelectionStart > 0)
			{
				if (CurrIndex == 0)
				{
					temp = "1";
				}
				else
				{
					temp = Conversion.Str(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Text))) + 1);
				}
				if (txtLevel2Range[Index].Text != "")
				{
					if (!Information.IsNumeric(txtLevel2Range[Index].Text) || Conversion.Val(txtLevel2Range[Index].Text) < FCConvert.ToDouble(temp) || Conversion.Val(txtLevel2Range[Index].Text) > Conversion.Val(txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text))
					{
						MessageBox.Show("You must enter a number greater than the upper bound of the preceeding Range and less than or equal to the upper bound of this Range in this field", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						txtLevel2Range[Index].SelectionStart = 0;
						txtLevel2Range[Index].SelectionLength = txtLevel2Range[Index].Text.Length;
						return;
					}
					else
					{
						temp = txtLevel2Range[Index].Text;
						txtLevel2Range[Index].Text = modValidateAccount.GetFormat(temp, ref RevLength);
					}
				}
			}
			else if (txtLevel2Range[Index].Text == "")
			{
				txtLevel2Description[Index].Text = "";
				if (Index < 4)
				{
					counter = Index;
				}
				else
				{
					return;
				}
				while (txtLevel2Range[FCConvert.ToInt16(counter + 1)].Text != "")
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].Text = txtLevel2Range[FCConvert.ToInt16(counter + 1)].Text;
					txtLevel2Description[FCConvert.ToInt16(counter)].Text = txtLevel2Description[FCConvert.ToInt16(counter + 1)].Text;
					counter += 1;
					if (counter == 4)
					{
						break;
					}
				}
				txtLevel2Range[FCConvert.ToInt16(counter)].Text = "";
				txtLevel2Description[FCConvert.ToInt16(counter)].Text = "";
				Support.SendKeys("{LEFT}", false);
			}
			if (Index > 0)
			{
				if (txtLevel2Range[FCConvert.ToInt16(Index - 1)].Text != "")
				{
					if (Conversion.Val(txtLevel2Range[Index].Text) <= Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index - 1)].Text))
					{
						MessageBox.Show("Your Subranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						txtLevel2Range[Index].Text = "";
						txtLevel2Range[Index].SelectionStart = 0;
						return;
					}
				}
			}
			if (Index < 4)
			{
				if (txtLevel2Range[FCConvert.ToInt16(Index + 1)].Text != "")
				{
					if (Conversion.Val(txtLevel2Range[Index].Text) >= Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index + 1)].Text))
					{
						MessageBox.Show("Your Subranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						txtLevel2Range[Index].Text = "";
						txtLevel2Range[Index].SelectionStart = 0;
						return;
					}
				}
			}
			txtLevel2Description[Index].Focus();
		}

		public void txtLevel2Range_Validate(short Index, ref bool Cancel)
		{
			txtLevel2Range_Validating(Index, txtLevel2Range[Index], new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtLevel2Range_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int index = txtLevel2Range.IndexOf((FCTextBox)sender);
			txtLevel2Range_Validating(index, sender, e);
		}
		// vbPorter upgrade warning: x As short	OnWriteFCConvert.ToInt32(
		private void SaveInfo(ref int x)
		{
			int counter;
			for (counter = 0; counter <= 4; counter++)
			{
				Level2Ranges[x, counter] = txtLevel2Range[FCConvert.ToInt16(counter)].Text;
				Level2Descriptions[x, counter] = txtLevel2Description[FCConvert.ToInt16(counter)].Text;
			}
		}
		// vbPorter upgrade warning: x As short	OnWriteFCConvert.ToInt32(
		private void GetInfo(ref int x)
		{
			int counter;
			for (counter = 0; counter <= 4; counter++)
			{
				txtLevel2Range[FCConvert.ToInt16(counter)].Text = Level2Ranges[x, counter];
				txtLevel2Description[FCConvert.ToInt16(counter)].Text = Level2Descriptions[x, counter];
			}
		}

		private void DeleteInfo(ref short x)
		{
			int counter;
			for (counter = 0; counter <= 4; counter++)
			{
				Level2Ranges[x, counter] = "";
				Level2Descriptions[x, counter] = "";
			}
		}

		private void ShowSubrange()
		{
			int temp;
			string HighBound;
			string LowBound = "";
			HighBound = txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text;
			if (CurrIndex == 0)
			{
				LowBound = "0";
			}
			else
			{
				LowBound = txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Text;
			}
			LowBound = modValidateAccount.GetFormat(Conversion.Str(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(LowBound))) + 1), ref RevLength);
			lblLevel2Range.Text = LowBound + " - " + HighBound;
			lblLevel2Name.Text = txtLevel1Description[FCConvert.ToInt16(CurrIndex)].Text;
			lblLevel2Range.Visible = true;
			lblLevel2Name.Visible = true;
			lblLevel2.Visible = true;
			Label5.Visible = true;
			Label6.Visible = true;
			//Line2.Visible = true;
			for (temp = 0; temp <= 4; temp++)
			{
				txtLevel2Range[FCConvert.ToInt16(temp)].Visible = true;
				txtLevel2Description[FCConvert.ToInt16(temp)].Visible = true;
			}
		}

		private void HideSubrange()
		{
			int temp;
			lblLevel2Range.Text = "";
			lblLevel2Name.Text = "";
			lblLevel2Range.Visible = false;
			lblLevel2Name.Visible = false;
			lblLevel2.Visible = false;
			Label5.Visible = false;
			Label6.Visible = false;
			//Line2.Visible = false;
			for (temp = 0; temp <= 4; temp++)
			{
				txtLevel2Range[FCConvert.ToInt16(temp)].Visible = false;
				txtLevel2Description[FCConvert.ToInt16(temp)].Visible = false;
			}
		}

		private void SaveAllData()
		{
			string SQLValues;
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rsTemp.Execute("DELETE FROM RevenueRanges WHERE Department = '" + CurrDepartment + "'", "Budgetary");
			}
			else
			{
				rsTemp.Execute("DELETE FROM RevenueRanges WHERE Department = '" + CurrDepartment + "' AND Division = '" + CurrDivision + "'", "Budgetary");
			}
			SQLValues = "('" + CurrDepartment + "', '" + CurrDivision + "', ";
			for (counter = 0; counter <= 9; counter++)
			{
				SQLValues += "'" + txtLevel1Total[FCConvert.ToInt16(counter)].Text + "', '" + txtLevel1Description[FCConvert.ToInt16(counter)].Text + "', ";
				if (Subtotals[counter] == true)
				{
					SQLValues += "1, ";
				}
				else
				{
					SQLValues += "0, ";
				}
				SQLValues += "'" + Level2Ranges[counter, 0] + "', '" + Level2Descriptions[counter, 0] + "', '" + Level2Ranges[counter, 1] + "', '" + Level2Descriptions[counter, 1] + "', '" + Level2Ranges[counter, 2] + "', '" + Level2Descriptions[counter, 2] + "', '" + Level2Ranges[counter, 3] + "', '" + Level2Descriptions[counter, 3] + "', '" + Level2Ranges[counter, 4] + "', '" + Level2Descriptions[counter, 4] + "', ";
			}
			SQLValues = Strings.Mid(SQLValues, 1, SQLValues.Length - 2);
			SQLValues += ")";
			rsTemp.Execute("INSERT INTO RevenueRanges VALUES " + SQLValues, "Budgetary");
		}

		private void GetAllData()
		{
			rs.MoveFirst();
			ChangeFlag = true;
			Subtotals[0] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal1"));
			Subtotals[1] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal2"));
			Subtotals[2] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal3"));
			Subtotals[3] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal4"));
			Subtotals[4] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal5"));
			Subtotals[5] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal6"));
			Subtotals[6] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal7"));
			Subtotals[7] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal8"));
			Subtotals[8] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal9"));
			Subtotals[9] = FCConvert.ToBoolean(rs.Get_Fields_Boolean("Subtotal10"));
			txtLevel1Total[0].Text = FCConvert.ToString(rs.Get_Fields_String("Range1"));
			txtLevel1Description[0].Text = FCConvert.ToString(rs.Get_Fields_String("Description1"));
			txtLevel1Total[1].Text = FCConvert.ToString(rs.Get_Fields_String("Range2"));
			txtLevel1Description[1].Text = FCConvert.ToString(rs.Get_Fields_String("Description2"));
			txtLevel1Total[2].Text = FCConvert.ToString(rs.Get_Fields_String("Range3"));
			txtLevel1Description[2].Text = FCConvert.ToString(rs.Get_Fields_String("Description3"));
			txtLevel1Total[3].Text = FCConvert.ToString(rs.Get_Fields_String("Range4"));
			txtLevel1Description[3].Text = FCConvert.ToString(rs.Get_Fields_String("Description4"));
			txtLevel1Total[4].Text = FCConvert.ToString(rs.Get_Fields_String("Range5"));
			txtLevel1Description[4].Text = FCConvert.ToString(rs.Get_Fields_String("Description5"));
			txtLevel1Total[5].Text = FCConvert.ToString(rs.Get_Fields_String("Range6"));
			txtLevel1Description[5].Text = FCConvert.ToString(rs.Get_Fields_String("Description6"));
			txtLevel1Total[6].Text = FCConvert.ToString(rs.Get_Fields_String("Range7"));
			txtLevel1Description[6].Text = FCConvert.ToString(rs.Get_Fields_String("Description7"));
			txtLevel1Total[7].Text = FCConvert.ToString(rs.Get_Fields_String("Range8"));
			txtLevel1Description[7].Text = FCConvert.ToString(rs.Get_Fields_String("Description8"));
			txtLevel1Total[8].Text = FCConvert.ToString(rs.Get_Fields_String("Range9"));
			txtLevel1Description[8].Text = FCConvert.ToString(rs.Get_Fields_String("Description9"));
			txtLevel1Total[9].Text = FCConvert.ToString(rs.Get_Fields_String("Range10"));
			txtLevel1Description[9].Text = FCConvert.ToString(rs.Get_Fields_String("Description10"));
			Level2Ranges[0, 0] = FCConvert.ToString(rs.Get_Fields_String("Range1Sub1"));
			Level2Descriptions[0, 0] = FCConvert.ToString(rs.Get_Fields_String("Range1Description1"));
			Level2Ranges[0, 1] = FCConvert.ToString(rs.Get_Fields_String("Range1Sub2"));
			Level2Descriptions[0, 1] = FCConvert.ToString(rs.Get_Fields_String("Range1Description2"));
			Level2Ranges[0, 2] = FCConvert.ToString(rs.Get_Fields_String("Range1Sub3"));
			Level2Descriptions[0, 2] = FCConvert.ToString(rs.Get_Fields_String("Range1Description3"));
			Level2Ranges[0, 3] = FCConvert.ToString(rs.Get_Fields_String("Range1Sub4"));
			Level2Descriptions[0, 3] = FCConvert.ToString(rs.Get_Fields_String("Range1Description4"));
			Level2Ranges[0, 4] = FCConvert.ToString(rs.Get_Fields_String("Range1Sub5"));
			Level2Descriptions[0, 4] = FCConvert.ToString(rs.Get_Fields_String("Range1Description5"));
			Level2Ranges[1, 0] = FCConvert.ToString(rs.Get_Fields_String("Range2Sub1"));
			Level2Descriptions[1, 0] = FCConvert.ToString(rs.Get_Fields_String("Range2Description1"));
			Level2Ranges[1, 1] = FCConvert.ToString(rs.Get_Fields_String("Range2Sub2"));
			Level2Descriptions[1, 1] = FCConvert.ToString(rs.Get_Fields_String("Range2Description2"));
			Level2Ranges[1, 2] = FCConvert.ToString(rs.Get_Fields_String("Range2Sub3"));
			Level2Descriptions[1, 2] = FCConvert.ToString(rs.Get_Fields_String("Range2Description3"));
			Level2Ranges[1, 3] = FCConvert.ToString(rs.Get_Fields_String("Range2Sub4"));
			Level2Descriptions[1, 3] = FCConvert.ToString(rs.Get_Fields_String("Range2Description4"));
			Level2Ranges[1, 4] = FCConvert.ToString(rs.Get_Fields_String("Range2Sub5"));
			Level2Descriptions[1, 4] = FCConvert.ToString(rs.Get_Fields_String("Range2Description5"));
			Level2Ranges[2, 0] = FCConvert.ToString(rs.Get_Fields_String("Range3Sub1"));
			Level2Descriptions[2, 0] = FCConvert.ToString(rs.Get_Fields_String("Range3Description1"));
			Level2Ranges[2, 1] = FCConvert.ToString(rs.Get_Fields_String("Range3Sub2"));
			Level2Descriptions[2, 1] = FCConvert.ToString(rs.Get_Fields_String("Range3Description2"));
			Level2Ranges[2, 2] = FCConvert.ToString(rs.Get_Fields_String("Range3Sub3"));
			Level2Descriptions[2, 2] = FCConvert.ToString(rs.Get_Fields_String("Range3Description3"));
			Level2Ranges[2, 3] = FCConvert.ToString(rs.Get_Fields_String("Range3Sub4"));
			Level2Descriptions[2, 3] = FCConvert.ToString(rs.Get_Fields_String("Range3Description4"));
			Level2Ranges[2, 4] = FCConvert.ToString(rs.Get_Fields_String("Range3Sub5"));
			Level2Descriptions[2, 4] = FCConvert.ToString(rs.Get_Fields_String("Range3Description5"));
			Level2Ranges[3, 0] = FCConvert.ToString(rs.Get_Fields_String("Range4Sub1"));
			Level2Descriptions[3, 0] = FCConvert.ToString(rs.Get_Fields_String("Range4Description1"));
			Level2Ranges[3, 1] = FCConvert.ToString(rs.Get_Fields_String("Range4Sub2"));
			Level2Descriptions[3, 1] = FCConvert.ToString(rs.Get_Fields_String("Range4Description2"));
			Level2Ranges[3, 2] = FCConvert.ToString(rs.Get_Fields_String("Range4Sub3"));
			Level2Descriptions[3, 2] = FCConvert.ToString(rs.Get_Fields_String("Range4Description3"));
			Level2Ranges[3, 3] = FCConvert.ToString(rs.Get_Fields_String("Range4Sub4"));
			Level2Descriptions[3, 3] = FCConvert.ToString(rs.Get_Fields_String("Range4Description4"));
			Level2Ranges[3, 4] = FCConvert.ToString(rs.Get_Fields_String("Range4Sub5"));
			Level2Descriptions[3, 4] = FCConvert.ToString(rs.Get_Fields_String("Range4Description5"));
			Level2Ranges[4, 0] = FCConvert.ToString(rs.Get_Fields_String("Range5Sub1"));
			Level2Descriptions[4, 0] = FCConvert.ToString(rs.Get_Fields_String("Range5Description1"));
			Level2Ranges[4, 1] = FCConvert.ToString(rs.Get_Fields_String("Range5Sub2"));
			Level2Descriptions[4, 1] = FCConvert.ToString(rs.Get_Fields_String("Range5Description2"));
			Level2Ranges[4, 2] = FCConvert.ToString(rs.Get_Fields_String("Range5Sub3"));
			Level2Descriptions[4, 2] = FCConvert.ToString(rs.Get_Fields_String("Range5Description3"));
			Level2Ranges[4, 3] = FCConvert.ToString(rs.Get_Fields_String("Range5Sub4"));
			Level2Descriptions[4, 3] = FCConvert.ToString(rs.Get_Fields_String("Range5Description4"));
			Level2Ranges[4, 4] = FCConvert.ToString(rs.Get_Fields_String("Range5Sub5"));
			Level2Descriptions[4, 4] = FCConvert.ToString(rs.Get_Fields_String("Range5Description5"));
			Level2Ranges[5, 0] = FCConvert.ToString(rs.Get_Fields_String("Range6Sub1"));
			Level2Descriptions[5, 0] = FCConvert.ToString(rs.Get_Fields_String("Range6Description1"));
			Level2Ranges[5, 1] = FCConvert.ToString(rs.Get_Fields_String("Range6Sub2"));
			Level2Descriptions[5, 1] = FCConvert.ToString(rs.Get_Fields_String("Range6Description2"));
			Level2Ranges[5, 2] = FCConvert.ToString(rs.Get_Fields_String("Range6Sub3"));
			Level2Descriptions[5, 2] = FCConvert.ToString(rs.Get_Fields_String("Range6Description3"));
			Level2Ranges[5, 3] = FCConvert.ToString(rs.Get_Fields_String("Range6Sub4"));
			Level2Descriptions[5, 3] = FCConvert.ToString(rs.Get_Fields_String("Range6Description4"));
			Level2Ranges[5, 4] = FCConvert.ToString(rs.Get_Fields_String("Range6Sub5"));
			Level2Descriptions[5, 4] = FCConvert.ToString(rs.Get_Fields_String("Range6Description5"));
			Level2Ranges[6, 0] = FCConvert.ToString(rs.Get_Fields_String("Range7Sub1"));
			Level2Descriptions[6, 0] = FCConvert.ToString(rs.Get_Fields_String("Range7Description1"));
			Level2Ranges[6, 1] = FCConvert.ToString(rs.Get_Fields_String("Range7Sub2"));
			Level2Descriptions[6, 1] = FCConvert.ToString(rs.Get_Fields_String("Range7Description2"));
			Level2Ranges[6, 2] = FCConvert.ToString(rs.Get_Fields_String("Range7Sub3"));
			Level2Descriptions[6, 2] = FCConvert.ToString(rs.Get_Fields_String("Range7Description3"));
			Level2Ranges[6, 3] = FCConvert.ToString(rs.Get_Fields_String("Range7Sub4"));
			Level2Descriptions[6, 3] = FCConvert.ToString(rs.Get_Fields_String("Range7Description4"));
			Level2Ranges[6, 4] = FCConvert.ToString(rs.Get_Fields_String("Range7Sub5"));
			Level2Descriptions[6, 4] = FCConvert.ToString(rs.Get_Fields_String("Range7Description5"));
			Level2Ranges[7, 0] = FCConvert.ToString(rs.Get_Fields_String("Range8Sub1"));
			Level2Descriptions[7, 0] = FCConvert.ToString(rs.Get_Fields_String("Range8Description1"));
			Level2Ranges[7, 1] = FCConvert.ToString(rs.Get_Fields_String("Range8Sub2"));
			Level2Descriptions[7, 1] = FCConvert.ToString(rs.Get_Fields_String("Range8Description2"));
			Level2Ranges[7, 2] = FCConvert.ToString(rs.Get_Fields_String("Range8Sub3"));
			Level2Descriptions[7, 2] = FCConvert.ToString(rs.Get_Fields_String("Range8Description3"));
			Level2Ranges[7, 3] = FCConvert.ToString(rs.Get_Fields_String("Range8Sub4"));
			Level2Descriptions[7, 3] = FCConvert.ToString(rs.Get_Fields_String("Range8Description4"));
			Level2Ranges[7, 4] = FCConvert.ToString(rs.Get_Fields_String("Range8Sub5"));
			Level2Descriptions[7, 4] = FCConvert.ToString(rs.Get_Fields_String("Range8Description5"));
			Level2Ranges[8, 0] = FCConvert.ToString(rs.Get_Fields_String("Range9Sub1"));
			Level2Descriptions[8, 0] = FCConvert.ToString(rs.Get_Fields_String("Range9Description1"));
			Level2Ranges[8, 1] = FCConvert.ToString(rs.Get_Fields_String("Range9Sub2"));
			Level2Descriptions[8, 1] = FCConvert.ToString(rs.Get_Fields_String("Range9Description2"));
			Level2Ranges[8, 2] = FCConvert.ToString(rs.Get_Fields_String("Range9Sub3"));
			Level2Descriptions[8, 2] = FCConvert.ToString(rs.Get_Fields_String("Range9Description3"));
			Level2Ranges[8, 3] = FCConvert.ToString(rs.Get_Fields_String("Range9Sub4"));
			Level2Descriptions[8, 3] = FCConvert.ToString(rs.Get_Fields_String("Range9Description4"));
			Level2Ranges[8, 4] = FCConvert.ToString(rs.Get_Fields_String("Range9Sub5"));
			Level2Descriptions[8, 4] = FCConvert.ToString(rs.Get_Fields_String("Range9Description5"));
			Level2Ranges[9, 0] = FCConvert.ToString(rs.Get_Fields_String("Range10Sub1"));
			Level2Descriptions[9, 0] = FCConvert.ToString(rs.Get_Fields_String("Range10Description1"));
			Level2Ranges[9, 1] = FCConvert.ToString(rs.Get_Fields_String("Range10Sub2"));
			Level2Descriptions[9, 1] = FCConvert.ToString(rs.Get_Fields_String("Range10Description2"));
			Level2Ranges[9, 2] = FCConvert.ToString(rs.Get_Fields_String("Range10Sub3"));
			Level2Descriptions[9, 2] = FCConvert.ToString(rs.Get_Fields_String("Range10Description3"));
			Level2Ranges[9, 3] = FCConvert.ToString(rs.Get_Fields_String("Range10Sub4"));
			Level2Descriptions[9, 3] = FCConvert.ToString(rs.Get_Fields_String("Range10Description4"));
			Level2Ranges[9, 4] = FCConvert.ToString(rs.Get_Fields_String("Range10Sub5"));
			Level2Descriptions[9, 4] = FCConvert.ToString(rs.Get_Fields_String("Range10Description5"));
			ChangeFlag = false;
		}

		private void DeleteAllData()
		{
			int counter;
			ChangeFlag = true;
			for (counter = 0; counter <= 9; counter++)
			{
				txtLevel1Total[FCConvert.ToInt16(counter)].Text = "";
				txtLevel1Description[FCConvert.ToInt16(counter)].Text = "";
				if (counter < 5)
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].Text = "";
					txtLevel2Description[FCConvert.ToInt16(counter)].Text = "";
				}
			}
			ChangeFlag = false;
		}

		private bool ValidateData()
		{
			bool ValidateData = false;
			int counter;
			int counter2 = 0;
			bool blnUpperRange;
			blnUpperRange = false;
			for (counter = 0; counter <= 9; counter++)
			{
				if (txtLevel1Total[FCConvert.ToInt16(counter)].Text != "" && txtLevel1Description[FCConvert.ToInt16(counter)].Text == "")
				{
					MessageBox.Show("You must have a Description associated with every Range", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					ValidateData = false;
					return ValidateData;
				}
				if (txtLevel1Total[FCConvert.ToInt16(counter)].Text == Strings.StrDup(RevLength, "9"))
				{
					blnUpperRange = true;
				}
				if (Subtotals[counter] == true)
				{
					counter2 = 0;
					while (Level2Ranges[counter, counter2] != "")
					{
						counter2 += 1;
						if (counter2 == 5)
						{
							break;
						}
					}
					if (counter2 > 0)
					{
						counter2 -= 1;
					}
					if (Conversion.Val(Level2Ranges[counter, counter2]) < Conversion.Val(txtLevel1Total[FCConvert.ToInt16(counter)].Text))
					{
						MessageBox.Show("The high bound of your last Subrange in each Range must match the high bound of that Range", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						ValidateData = false;
						return ValidateData;
					}
					for (counter2 = 0; counter2 <= 4; counter2++)
					{
						if (Level2Ranges[counter, counter2] != "" && Level2Descriptions[counter, counter2] == "")
						{
							MessageBox.Show("You must have a Description associated with every Subrange", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							ValidateData = false;
							return ValidateData;
						}
					}
				}
			}
			if (!blnUpperRange)
			{
				MessageBox.Show("Your ranges must go up to the highest revenue account possible.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				ValidateData = false;
				return ValidateData;
			}
			ValidateData = true;
			return ValidateData;
		}

		private bool CheckLevel1Total(int Index)
		{
			bool CheckLevel1Total = false;
			// vbPorter upgrade warning: temp As string	OnWrite(string, DialogResult)
			int temp = 0;
			int counter = 0;
			int counter2;
			temp = FCConvert.ToInt32(txtLevel1Total[FCConvert.ToInt16(Index)].Text);
			txtLevel1Total[FCConvert.ToInt16(Index)].Text = modValidateAccount.GetFormat(temp.ToString(), ref RevLength);
			if (txtLevel1Total[FCConvert.ToInt16(Index)].Text != "")
			{
				if (!Information.IsNumeric(txtLevel1Total[FCConvert.ToInt16(Index)].Text) || Conversion.Val(txtLevel1Total[FCConvert.ToInt16(Index)].Text) < 1)
				{
					MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckLevel1Total = false;
					txtLevel1Total[FCConvert.ToInt16(Index)].SelectionStart = 0;
					txtLevel1Total[FCConvert.ToInt16(Index)].SelectionLength = txtLevel1Total[FCConvert.ToInt16(Index)].Text.Length;
					return CheckLevel1Total;
				}
			}
			else if (txtLevel1Total[FCConvert.ToInt16(Index)].Text == "")
			{
				Subtotals[Index] = false;
				cmdDelete_Click();
				txtLevel1Description[FCConvert.ToInt16(Index)].Text = "";
				if (CurrIndex < 9)
				{
					counter = CurrIndex;
				}
				else
				{
					return CheckLevel1Total;
				}
				while (txtLevel1Total[FCConvert.ToInt16(counter + 1)].Text != "")
				{
					txtLevel1Total[FCConvert.ToInt16(counter)].Text = txtLevel1Total[FCConvert.ToInt16(counter + 1)].Text;
					txtLevel1Description[FCConvert.ToInt16(counter)].Text = txtLevel1Description[FCConvert.ToInt16(counter + 1)].Text;
					Subtotals[counter] = Subtotals[counter + 1];
					for (counter2 = 0; counter2 <= 4; counter2++)
					{
						Level2Ranges[counter, counter2] = Level2Ranges[counter + 1, counter2];
						Level2Descriptions[counter, counter2] = Level2Descriptions[counter + 1, counter2];
					}
					counter += 1;
					if (counter == 9)
					{
						break;
					}
				}
				txtLevel1Total[FCConvert.ToInt16(counter)].Text = "";
				txtLevel1Description[FCConvert.ToInt16(counter)].Text = "";
				Subtotals[counter] = false;
				for (counter2 = 0; counter2 <= 4; counter2++)
				{
					Level2Ranges[counter, counter2] = "";
					Level2Descriptions[counter, counter2] = "";
				}
				// SendKeys "{LEFT}"
				if (Subtotals[CurrIndex] == true)
				{
					GetInfo(ref CurrIndex);
					ShowSubrange();
				}
				else
				{
					HideSubrange();
				}
			}
			if (Index > 0)
			{
				if (txtLevel1Total[FCConvert.ToInt16(Index)].Text != "")
				{
					if (fecherFoundation.Strings.CompareString(txtLevel1Total[FCConvert.ToInt16(Index)].Text, txtLevel1Total[FCConvert.ToInt16(Index - 1)].Text, true) <= 0)
					{
						MessageBox.Show("Your Ranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLevel1Total = false;
						txtLevel1Total[FCConvert.ToInt16(Index)].Text = "";
						txtLevel1Total[FCConvert.ToInt16(Index)].SelectionStart = 0;
						return CheckLevel1Total;
					}
				}
			}
			if (Index < 9)
			{
				if (txtLevel1Total[FCConvert.ToInt16(Index + 1)].Text != "")
				{
					if (fecherFoundation.Strings.CompareString(txtLevel1Total[FCConvert.ToInt16(Index)].Text, txtLevel1Total[FCConvert.ToInt16(Index + 1)].Text, true) >= 0)
					{
						MessageBox.Show("Your Ranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLevel1Total = false;
						txtLevel1Total[FCConvert.ToInt16(Index)].Text = "";
						txtLevel1Total[FCConvert.ToInt16(Index)].SelectionStart = 0;
						return CheckLevel1Total;
					}
				}
			}
			if (InsertFlag)
			{
				if (Subtotals[Index + 1] == true)
				{
					if (FCConvert.ToDouble(Level2Ranges[Index + 1, 0]) <= Conversion.Val(txtLevel1Total[FCConvert.ToInt16(Index)].Text))
					{
						temp = FCConvert.ToInt32(MessageBox.Show("Inserting this new Range will delete some of the Subranges in the " + txtLevel1Description[FCConvert.ToInt16(Index + 1)].Text + " Range.  Do you wish to do this?", "Range Overlap Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
						if (temp == FCConvert.ToInt32(DialogResult.Yes))
						{
							InsertFlag = false;
							for (counter = 0; counter <= 4; counter++)
							{
								if (Conversion.Val(Level2Ranges[Index + 1, counter]) <= Conversion.Val(txtLevel1Total[FCConvert.ToInt16(Index)].Text))
								{
									Level2Ranges[Index + 1, counter] = "";
									Level2Descriptions[Index + 1, counter] = "";
								}
								else
								{
									break;
								}
							}
							counter = 0;
							counter2 = 0;
							while (Level2Ranges[Index + 1, counter2] == "")
							{
								counter2 += 1;
								if (counter2 == 5)
								{
									counter2 = 0;
									break;
								}
							}
							if (counter2 == 0)
							{
								return CheckLevel1Total;
							}
							else
							{
								while (counter2 < 5)
								{
									Level2Ranges[Index + 1, counter] = Level2Ranges[Index + 1, counter2];
									Level2Descriptions[Index + 1, counter] = Level2Descriptions[Index + 1, counter2];
									Level2Ranges[Index + 1, counter2] = "";
									Level2Descriptions[Index + 1, counter2] = "";
									counter += 1;
									counter2 += 1;
									if (counter2 < 5)
									{
										if (Level2Ranges[Index + 1, counter2] == "")
										{
											break;
										}
									}
								}
							}
						}
						else
						{
							CheckLevel1Total = false;
							txtLevel1Total[FCConvert.ToInt16(Index)].Text = "";
							txtLevel1Total[FCConvert.ToInt16(Index)].Focus();
							return CheckLevel1Total;
						}
					}
					else
					{
						InsertFlag = false;
					}
				}
			}
			if (Subtotals[Index] == true)
			{
				if (txtLevel1Total[FCConvert.ToInt16(Index)].Text != Strings.Right(lblLevel2Range.Text, RevLength))
				{
					lblLevel2Range.Text = Strings.Left(lblLevel2Range.Text, lblLevel2Range.Text.Length - RevLength) + txtLevel1Total[FCConvert.ToInt16(Index)].Text;
				}
			}
			CheckLevel1Total = true;
			return CheckLevel1Total;
		}

		private bool CheckLevel2Total(int Index)
		{
			bool CheckLevel2Total = false;
			string temp = "";
			int counter = 0;
			if (txtLevel2Range[FCConvert.ToInt16(Index)].Text != "")
			{
				if (CurrIndex == 0)
				{
					temp = "1";
				}
				else
				{
					temp = Conversion.Str(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(txtLevel1Total[FCConvert.ToInt16(CurrIndex - 1)].Text))) + 1);
				}
				if (!Information.IsNumeric(txtLevel2Range[FCConvert.ToInt16(Index)].Text) || Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index)].Text) < FCConvert.ToDouble(temp) || Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index)].Text) > Conversion.Val(txtLevel1Total[FCConvert.ToInt16(CurrIndex)].Text))
				{
					MessageBox.Show("You must enter a number greater than the upper bound of the preceeding Range and less than or equal to the upper bound of this Range in this field", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckLevel2Total = false;
					txtLevel2Range[FCConvert.ToInt16(Index)].SelectionStart = 0;
					txtLevel2Range[FCConvert.ToInt16(Index)].SelectionLength = txtLevel2Range[FCConvert.ToInt16(Index)].Text.Length;
					return CheckLevel2Total;
				}
				else
				{
					temp = txtLevel2Range[FCConvert.ToInt16(Index)].Text;
					txtLevel2Range[FCConvert.ToInt16(Index)].Text = modValidateAccount.GetFormat(temp, ref RevLength);
				}
			}
			else if (txtLevel2Range[FCConvert.ToInt16(Index)].Text == "")
			{
				txtLevel2Description[FCConvert.ToInt16(Index)].Text = "";
				if (Index < 4)
				{
					counter = Index;
				}
				else
				{
					return CheckLevel2Total;
				}
				while (txtLevel2Range[FCConvert.ToInt16(counter + 1)].Text != "")
				{
					txtLevel2Range[FCConvert.ToInt16(counter)].Text = txtLevel2Range[FCConvert.ToInt16(counter + 1)].Text;
					txtLevel2Description[FCConvert.ToInt16(counter)].Text = txtLevel2Description[FCConvert.ToInt16(counter + 1)].Text;
					counter += 1;
					if (counter == 4)
					{
						break;
					}
				}
				txtLevel2Range[FCConvert.ToInt16(counter)].Text = "";
				txtLevel2Description[FCConvert.ToInt16(counter)].Text = "";
				Support.SendKeys("{LEFT}", false);
			}
			if (Index > 0)
			{
				if (txtLevel2Range[FCConvert.ToInt16(Index - 1)].Text != "")
				{
					if (Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index)].Text) <= Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index - 1)].Text))
					{
						MessageBox.Show("Your Subranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLevel2Total = false;
						txtLevel2Range[FCConvert.ToInt16(Index)].Text = "";
						txtLevel2Range[FCConvert.ToInt16(Index)].SelectionStart = 0;
						return CheckLevel2Total;
					}
				}
			}
			if (Index < 4)
			{
				if (txtLevel2Range[FCConvert.ToInt16(Index + 1)].Text != "")
				{
					if (Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index)].Text) >= Conversion.Val(txtLevel2Range[FCConvert.ToInt16(Index + 1)].Text))
					{
						MessageBox.Show("Your Subranges must be in ascending order", "Invalid Subrange", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLevel2Total = false;
						txtLevel2Range[FCConvert.ToInt16(Index)].Text = "";
						txtLevel2Range[FCConvert.ToInt16(Index)].SelectionStart = 0;
						return CheckLevel2Total;
					}
				}
			}
			CheckLevel2Total = true;
			return CheckLevel2Total;
		}

		private void FillBox()
		{
			string temp;
			int counter;
			temp = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
			rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + temp + "' ORDER BY Department");
			if (rs.RecordCount() > 0)
			{
				rs.MoveLast();
				// populates the recordset
				rs.MoveFirst();
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					temp = rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription");
					cmbDepartment.AddItem(temp);
					// enters the book number and description into the combo box
					rs.MoveNext();
				}
			}
			if (cmbDepartment.Items.Count > 0)
			{
				cmbDepartment.SelectedIndex = 0;
			}
		}

		private void FillDivBox()
		{
			string temp;
			int counter;
			cmbDivision.Clear();
			temp = modValidateAccount.GetFormat("0", ref DivLength);
			rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Left(cmbDepartment.Text, DeptLength) + "' AND Division <> '" + temp + "' ORDER BY Division");
			if (rs.RecordCount() > 0)
			{
				rs.MoveLast();
				// populates the recordset
				rs.MoveFirst();
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					temp = rs.Get_Fields_String("Division") + " - " + rs.Get_Fields_String("ShortDescription");
					cmbDivision.AddItem(temp);
					// enters the book number and description into the combo box
					rs.MoveNext();
				}
			}
			if (cmbDivision.Items.Count > 0)
			{
				cmbDivision.SelectedIndex = 0;
			}
		}

		private bool CheckLevel1Description(int Index)
		{
			bool CheckLevel1Description = false;
			if (txtLevel1Description[FCConvert.ToInt16(Index)].SelectionStart > 0)
			{
				if (txtLevel1Total[FCConvert.ToInt16(Index)].Text == "")
				{
					MessageBox.Show("You must enter a Range before you create the Description", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckLevel1Description = false;
					txtLevel1Description[FCConvert.ToInt16(Index)].Text = "";
					txtLevel1Total[FCConvert.ToInt16(Index)].Focus();
					return CheckLevel1Description;
				}
			}
			if (Subtotals[Index] == true)
			{
				if (txtLevel1Description[FCConvert.ToInt16(Index)].Text != lblLevel2Name.Text)
				{
					lblLevel2Name.Text = txtLevel1Description[FCConvert.ToInt16(Index)].Text;
				}
			}
			CheckLevel1Description = true;
			return CheckLevel1Description;
		}

		private bool CheckLevel2Description(int Index)
		{
			bool CheckLevel2Description = false;
			if (txtLevel2Description[FCConvert.ToInt16(Index)].SelectionStart > 0)
			{
				if (txtLevel2Range[FCConvert.ToInt16(Index)].Text == "")
				{
					MessageBox.Show("You must enter a Range before you create the Description", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckLevel2Description = false;
					txtLevel2Range[FCConvert.ToInt16(Index)].Focus();
					return CheckLevel2Description;
				}
			}
			CheckLevel2Description = true;
			return CheckLevel2Description;
		}

		private void SetCustomFormColors()
		{
			//Line1.LineColor = Color.Blue;
			//Line2.LineColor = Color.Blue;
		}

		private void SaveTotalInfo()
		{
			int counter;
			string holder = "";
			if (lblLevel2Name.Visible == true)
			{
				for (counter = 0; counter <= 4; counter++)
				{
					if (txtLevel2Range[FCConvert.ToInt16(counter)].Text != "")
					{
						if (txtLevel2Range[FCConvert.ToInt16(counter)].Text.Length < RevLength)
						{
							holder = txtLevel2Range[FCConvert.ToInt16(counter)].Text;
							txtLevel2Range[FCConvert.ToInt16(counter)].Text = modValidateAccount.GetFormat(holder, ref RevLength);
						}
					}
					else
					{
						break;
					}
				}
				SaveInfo(ref CurrIndex);
			}
			if (ValidateData())
			{
				if (lblLevel2.Visible == true)
				{
					SaveInfo(ref CurrIndex);
				}
				SaveAllData();
			}
			else
			{
				cmbDepartment.SelectedIndex = DeptCurrIndex;
				return;
			}
			if (blnUnload)
			{
				Close();
			}
			else
			{
				MessageBox.Show("Save Completed !!");
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
