﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCloseoutAccountsTotals.
	/// </summary>
	public partial class srptCloseoutAccountsTotals : FCSectionReport
	{
		public static srptCloseoutAccountsTotals InstancePtr
		{
			get
			{
				return (srptCloseoutAccountsTotals)Sys.GetInstance(typeof(srptCloseoutAccountsTotals));
			}
		}

		protected srptCloseoutAccountsTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCloseoutAccountsTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		int lngCounter;

		public srptCloseoutAccountsTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				lngCounter += 1;
				if (lngCounter < modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			curTotal = 0;
			lngCounter = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldAccount.Text = modBudgetaryMaster.Statics.sumCloseoutTotals[lngCounter].strAccount;
			fldAmount.Text = Strings.Format(modBudgetaryMaster.Statics.sumCloseoutTotals[lngCounter].curAmount, "#,##0.00");
			curTotal += modBudgetaryMaster.Statics.sumCloseoutTotals[lngCounter].curAmount;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotal, "#,##0.00");
		}

		
	}
}
