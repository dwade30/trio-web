﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeGraphItem.
	/// </summary>
	partial class frmPurgeGraphItem : BaseForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeGraphItem));
			this.cboGraphItem = new fecherFoundation.FCComboBox();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.cboGraphItem);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(207, 30);
			this.HeaderText.Text = "Purge Graph Item";
			// 
			// cboGraphItem
			// 
			this.cboGraphItem.AutoSize = false;
			this.cboGraphItem.BackColor = System.Drawing.SystemColors.Window;
			this.cboGraphItem.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboGraphItem.FormattingEnabled = true;
			this.cboGraphItem.Location = new System.Drawing.Point(30, 30);
			this.cboGraphItem.Name = "cboGraphItem";
			this.cboGraphItem.Size = new System.Drawing.Size(300, 40);
			this.cboGraphItem.TabIndex = 0;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 100);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(110, 48);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Process";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmPurgeGraphItem
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgeGraphItem";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Graph Item";
			this.Load += new System.EventHandler(this.frmPurgeGraphItem_Load);
			this.Activated += new System.EventHandler(this.frmPurgeGraphItem_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeGraphItem_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public fecherFoundation.FCComboBox cboGraphItem;
		private FCButton cmdSave;
	}
}
