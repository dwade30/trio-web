﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeProjectSummary.
	/// </summary>
	public partial class frmCustomizeProjectSummary : BaseForm
	{
		public frmCustomizeProjectSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//FC:FINAL:BBE - set the correct default selectedIndex. (The default value is not always the SelectedIndex 0) and Corrected logic for CheckedChanged events as in original.
			this.cmbShowOutstandingEncumbrance.SelectedIndex = 1;
			this.cmbShowPending.SelectedIndex = 1;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeProjectSummary InstancePtr
		{
			get
			{
				return (frmCustomizeProjectSummary)Sys.GetInstance(typeof(frmCustomizeProjectSummary));
			}
		}

		protected frmCustomizeProjectSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int Choices;
		bool FormatFlag;
		bool PendingFlag;
		bool OutstandingFlag;
		int PendingChoice;
		int MaxItems;
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		bool UseMax;
		bool LargeFont;
		bool SpecificFontFlag;
		public bool FromProj;
		bool blnSavedFormat;
		string strWidth;

		private void chkSelectedDebitsCredits_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkSelectedDebitsCredits.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems - 1)
						{
							Choices += 2;
						}
						else
						{
							FormatFlag = true;
							chkSelectedDebitsCredits.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 2;
					}
				}
				else
				{
					Choices -= 2;
				}
			}
			ShowItems();
		}

		private void chkSelectedNet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkSelectedNet.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkSelectedNet.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}

		private void chkYTDDebitsCredits_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkYTDDebitsCredits.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems - 1)
						{
							Choices += 2;
						}
						else
						{
							FormatFlag = true;
							chkYTDDebitsCredits.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 2;
					}
				}
				else
				{
					Choices -= 2;
				}
			}
			ShowItems();
		}

		private void chkYTDNet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!FormatFlag)
			{
				if (chkYTDNet.CheckState == CheckState.Checked)
				{
					if (UseMax)
					{
						if (Choices < MaxItems)
						{
							Choices += 1;
						}
						else
						{
							FormatFlag = true;
							chkYTDNet.CheckState = CheckState.Unchecked;
							FormatFlag = false;
						}
					}
					else
					{
						Choices += 1;
					}
				}
				else
				{
					Choices -= 1;
				}
			}
			ShowItems();
		}		

		private void frmCustomizeProjectSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			Choices = 0;
			OutstandingFlag = false;
			PendingFlag = false;
			PendingChoice = 2;
			txtDescription.Focus();
			LargeFont = false;
			UseMax = true;
			SpecificFontFlag = false;
			MaxItems = 8;
			strWidth = "L";
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnProjectSummaryEdit && !modBudgetaryMaster.Statics.blnProjectSummaryReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					
					
					strWidth = "L";
					
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					bool optShowOutstandingEncumbrance = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("OSEncumbrance"));
					cmbShowOutstandingEncumbrance.SelectedIndex = optShowOutstandingEncumbrance ? 0 : 1;
					bool optShowPending = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("SeperatePending"));
					bool optIncludePending = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("IncludePending"));
					if (!optShowPending && !optIncludePending)
					{
						cmbShowPending.SelectedIndex = 1;
					}
					else if (optShowPending)
					{
						cmbShowPending.SelectedIndex = 0;
					}
					else 
					{
						cmbShowPending.SelectedIndex = 2;
					}
					chkSelectedDebitsCredits.CheckState =
                        FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CurrentDebitCredit"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					chkSelectedNet.CheckState =
                        FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CurrentNet"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					chkYTDDebitsCredits.CheckState =
                        FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDDebitCredit"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					chkYTDNet.CheckState =
                        FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDNet"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
				}
			}
		}

		private void frmCustomizeProjectSummary_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeProjectSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeProjectSummary.FillStyle	= 0;
			//frmCustomizeProjectSummary.ScaleWidth	= 9300;
			//frmCustomizeProjectSummary.ScaleHeight	= 7470;
			//frmCustomizeProjectSummary.LinkTopic	= "Form2";
			//frmCustomizeProjectSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromProj)
			{
				FromProj = false;
				frmProjectSummarySelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnProjectSummaryReportEdit)
					{
						modBudgetaryMaster.Statics.blnProjectSummaryReportEdit = false;
						frmProjectSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmProjectSummarySelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnProjectSummaryReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnProjectSummaryEdit)
			{
				modBudgetaryMaster.Statics.blnProjectSummaryEdit = false;
				frmGetProjectSummary.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeProjectSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Choices == 0)
			{
				MessageBox.Show("You must make at least one selection before you can save this format", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rs.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("CurrentDebitCredit", chkSelectedDebitsCredits.CheckState == CheckState.Checked);
					rs.Set_Fields("CurrentNet", chkSelectedNet.CheckState == CheckState.Checked);
					rs.Set_Fields("YTDDebitCredit", chkYTDDebitsCredits.CheckState == CheckState.Checked);
					rs.Set_Fields("YTDNet", chkYTDNet.CheckState == CheckState.Checked);
					rs.Set_Fields("Printer", "O");					
					rs.Set_Fields("OSEncumbrance", cmbShowOutstandingEncumbrance.SelectedIndex == 0);
					rs.Set_Fields("SeperatePending", cmbShowPending.SelectedIndex == 0);
					rs.Set_Fields("IncludePending", cmbShowPending.SelectedIndex == 1);
					rs.Set_Fields("Font", "S");					
					rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("CurrentDebitCredit", chkSelectedDebitsCredits.CheckState == CheckState.Checked);
				rs.Set_Fields("CurrentNet", chkSelectedNet.CheckState == CheckState.Checked);
				rs.Set_Fields("YTDDebitCredit", chkYTDDebitsCredits.CheckState == CheckState.Checked);
				rs.Set_Fields("YTDNet", chkYTDNet.CheckState == CheckState.Checked);
				rs.Set_Fields("Printer", "O");				
				rs.Set_Fields("OSEncumbrance", cmbShowOutstandingEncumbrance.SelectedIndex == 0);
				rs.Set_Fields("SeperatePending", cmbShowPending.SelectedIndex == 0);
				rs.Set_Fields("IncludePending", cmbShowPending.SelectedIndex == 1);
				rs.Set_Fields("Font", "S");				
				rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}		

		private void optIncludeOS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (OutstandingFlag)
			{
				OutstandingFlag = false;
				Choices -= 1;
			}
			ShowItems();
		}

		private void optIncludePending_CheckedChanged(object sender, System.EventArgs e)
		{
			if (PendingFlag)
			{
				PendingFlag = false;
				Choices -= 1;
			}
			PendingChoice = 2;
			ShowItems();
		}		

		private void optShowOutstanding_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!OutstandingFlag)
			{
				if (UseMax)
				{
					if (Choices < MaxItems)
					{
						OutstandingFlag = true;
						Choices += 1;
					}
					else
					{
						cmbShowOutstandingEncumbrance.SelectedIndex = 0;
					}
				}
				else
				{
					Choices += 1;
				}
			}
			ShowItems();
		}

		private void optShowPending_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!PendingFlag)
			{
				if (UseMax)
				{
					if (Choices < MaxItems)
					{
						PendingFlag = true;
						Choices += 1;
						PendingChoice = 1;
					}
					else
					{
						if (PendingChoice == 2)
						{
							cmbShowPending.SelectedIndex = 1;
						}
						else
						{
							cmbShowPending.SelectedIndex = 2;
						}
					}
				}
				else
				{
					PendingFlag = true;
					Choices += 1;
					PendingChoice = 1;
				}
			}
			ShowItems();
		}		

		private void optUsePending_CheckedChanged(object sender, System.EventArgs e)
		{
			if (PendingFlag)
			{
				PendingFlag = false;
				Choices -= 1;
			}
			PendingChoice = 3;
			ShowItems();
		}

		private void ShowItems()
		{			
			lblNumber.Text = FCConvert.ToString(MaxItems - Choices);			
		}

		private void SetCustomFormColors()
		{
			lblNumber.ForeColor = ColorTranslator.FromOle(0x800080);
		}

		private void cmbShowPending_CheckedChanged(object sender, EventArgs e)
		{
			if (cmbShowPending.SelectedIndex == 0)
			{
				optShowPending_CheckedChanged(sender, e);
			}
			else if (cmbShowPending.SelectedIndex == 1)
			{
				optIncludePending_CheckedChanged(sender, e);
			}
			else if (cmbShowPending.SelectedIndex == 2)
			{
				optUsePending_CheckedChanged(sender, e);
			}
		}

		private void cmbShowOutstanding_CheckedChanged(object sender, EventArgs e)
		{
			if (cmbShowOutstandingEncumbrance.SelectedIndex == 0)
			{
				optShowOutstanding_CheckedChanged(sender, e);
			}
			else if (cmbShowOutstandingEncumbrance.SelectedIndex == 1)
			{
				optIncludeOS_CheckedChanged(sender, e);
			}
		}

	}
}
