﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class cAcctActItemComparer : cIComparator
	{
		//=========================================================
		private int intFiscalStart;
		private int[] periodRank = new int[12 + 1];

		public short FiscalStartPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					intFiscalStart = value;
					ReRankPeriods();
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FiscalStartPeriod = 0;
				FiscalStartPeriod = FCConvert.ToInt16(intFiscalStart);
				return FiscalStartPeriod;
			}
		}

		private object ReRankPeriods()
		{
			object ReRankPeriods = null;
			int x;
			int counter;
			counter = intFiscalStart;
			for (x = 1; x <= 12; x++)
			{
				if (counter > 12)
				{
					counter = 1;
				}
				periodRank[counter - 1] = x;
				counter += 1;
			}
			return ReRankPeriods;
		}
		// -1 is less than, 0 is equal and 1 is greater than
		// vbPorter upgrade warning: compareA As object	OnRead(cAccountActivityItem)
		// vbPorter upgrade warning: compareB As object	OnRead(cAccountActivityItem)
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Compare(ref object compareA, ref object compareB)
		{
			short cIComparator_Compare = 0;
			cAccountActivityItem itemA;
			cAccountActivityItem itemB;
			itemA = (cAccountActivityItem)compareA;
			itemB = (cAccountActivityItem)compareB;
			int intReturn;
			int intRankA;
			int intRankB;
			intRankA = periodRank[itemA.Period - 1];
			intRankB = periodRank[itemB.Period - 1];
			intReturn = 0;
			if (intRankA < intRankB)
			{
				intReturn = -1;
			}
			else
			{
				if (intRankA > intRankB)
				{
					intReturn = 1;
				}
				else
				{
					DateTime dtA;
					DateTime dtB;
					// vbPorter upgrade warning: intDiff As short, int --> As long
					long intDiff;
					dtA = DateAndTime.DateValue(itemA.ActivityDate);
					dtB = DateAndTime.DateValue(itemB.ActivityDate);
					intDiff = DateAndTime.DateDiff("d", dtA, dtB);
					if (intDiff > 0)
					{
						intReturn = -1;
					}
					else
					{
						if (intDiff < 0)
						{
							intReturn = 1;
						}
						else
						{
							if (itemA.JournalNumber < itemB.JournalNumber)
							{
								intReturn = -1;
							}
							else if (itemA.JournalNumber > itemB.JournalNumber)
							{
								intReturn = 1;
							}
						}
					}
				}
			}
			cIComparator_Compare = FCConvert.ToInt16(intReturn);
			return cIComparator_Compare;
		}

		public cAcctActItemComparer() : base()
		{
			FiscalStartPeriod = 1;
		}
	}
}
