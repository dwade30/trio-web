﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReversingJournal.
	/// </summary>
	partial class frmReversingJournal : BaseForm
	{
		public fecherFoundation.FCCheckBox chkCorrection;
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReversingJournal));
			this.chkCorrection = new fecherFoundation.FCCheckBox();
			this.cboJournals = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcessSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 303);
			this.BottomPanel.Size = new System.Drawing.Size(821, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcessSave);
			this.ClientArea.Controls.Add(this.chkCorrection);
			this.ClientArea.Controls.Add(this.cboJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(821, 243);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(821, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(291, 30);
			this.HeaderText.Text = "Create Reversing Journal";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// chkCorrection
			// 
			this.chkCorrection.Location = new System.Drawing.Point(30, 126);
			this.chkCorrection.Name = "chkCorrection";
			this.chkCorrection.Size = new System.Drawing.Size(367, 27);
			this.chkCorrection.TabIndex = 2;
			this.chkCorrection.Text = "Create reversing journal with correcting entries";
			this.ToolTip1.SetToolTip(this.chkCorrection, "Entries in the reversing journal will have an RCB setting of C");
			// 
			// cboJournals
			// 
			this.cboJournals.AutoSize = false;
			this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournals.FormattingEnabled = true;
			this.cboJournals.Location = new System.Drawing.Point(30, 66);
			this.cboJournals.Name = "cboJournals";
			this.cboJournals.Size = new System.Drawing.Size(713, 40);
			this.cboJournals.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cboJournals, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(713, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "THIS PROCESS MAY ONLY BE USED FOR GENERAL JOURNAL, CASH RECEIPTS, AND CASH DISBUR" + "SEMENTS JOURNAL TYPES";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnProcessSave
			// 
			this.btnProcessSave.AppearanceKey = "acceptButton";
			this.btnProcessSave.Location = new System.Drawing.Point(30, 170);
			this.btnProcessSave.Name = "btnProcessSave";
			this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcessSave.Size = new System.Drawing.Size(100, 48);
			this.btnProcessSave.TabIndex = 0;
			this.btnProcessSave.Text = "Process";
			this.ToolTip1.SetToolTip(this.btnProcessSave, null);
			this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmReversingJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(821, 411);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReversingJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Create Reversing Journal";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmReversingJournal_Load);
			this.Activated += new System.EventHandler(this.frmReversingJournal_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReversingJournal_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcessSave;
	}
}
