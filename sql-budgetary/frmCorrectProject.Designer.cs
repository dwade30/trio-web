//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCorrectProject.
	/// </summary>
	partial class frmCorrectProject : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtCheck;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtReference;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
        public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCTextBox txtZip4;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblCheck;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCorrectProject));
            this.txtAddress_0 = new Global.T2KOverTypeBox();
            this.txtVendor = new Global.T2KOverTypeBox();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.txtDescription = new Global.T2KOverTypeBox();
            this.txtReference = new Global.T2KOverTypeBox();
            this.txtAmount = new Global.T2KBackFillDecimal();
            this.txtAddress_1 = new Global.T2KOverTypeBox();
            this.txtAddress_2 = new Global.T2KOverTypeBox();
            this.txtAddress_3 = new Global.T2KOverTypeBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.lblReference = new fecherFoundation.FCLabel();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.lblCheck = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            lblExpense = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Size = new System.Drawing.Size(1051, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtAddress_0);
            this.ClientArea.Controls.Add(this.txtVendor);
            this.ClientArea.Controls.Add(this.txtCheck);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtReference);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtAddress_1);
            this.ClientArea.Controls.Add(this.txtAddress_2);
            this.ClientArea.Controls.Add(this.txtAddress_3);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(lblExpense);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.lblReference);
            this.ClientArea.Controls.Add(this.lblAmount);
            this.ClientArea.Controls.Add(this.lblCheck);
            this.ClientArea.Size = new System.Drawing.Size(1051, 410);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1051, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(172, 30);
            this.HeaderText.Text = "AP Correction";
            // 
            // txtAddress_0
            // 
            this.txtAddress_0.Enabled = false;
            this.txtAddress_0.Location = new System.Drawing.Point(661, 30);
            this.txtAddress_0.MaxLength = 35;
            this.txtAddress_0.Name = "txtAddress_0";
            this.txtAddress_0.Size = new System.Drawing.Size(360, 40);
            // 
            // txtVendor
            // 
            this.txtVendor.Enabled = false;
            this.txtVendor.Location = new System.Drawing.Point(570, 30);
            this.txtVendor.MaxLength = 4;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new System.Drawing.Size(88, 40);
            this.txtVendor.TabIndex = 1;
            // 
            // txtCheck
            // 
            this.txtCheck.Enabled = false;
            this.txtCheck.Location = new System.Drawing.Point(142, 180);
            this.txtCheck.MaxLength = 6;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(300, 40);
            this.txtCheck.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.Enabled = false;
            this.txtDescription.Location = new System.Drawing.Point(142, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(300, 40);
            this.txtDescription.TabIndex = 3;
            // 
            // txtReference
            // 
            this.txtReference.Enabled = false;
            this.txtReference.Location = new System.Drawing.Point(142, 80);
            this.txtReference.MaxLength = 15;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(300, 40);
            this.txtReference.TabIndex = 4;
            // 
            // txtAmount
            // 
            this.txtAmount.Enabled = false;
            this.txtAmount.Location = new System.Drawing.Point(142, 130);
            this.txtAmount.MaxLength = 14;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(300, 40);
            this.txtAmount.TabIndex = 6;
            // 
            // txtAddress_1
            // 
            this.txtAddress_1.Enabled = false;
            this.txtAddress_1.Location = new System.Drawing.Point(661, 80);
            this.txtAddress_1.MaxLength = 35;
            this.txtAddress_1.Name = "txtAddress_1";
            this.txtAddress_1.Size = new System.Drawing.Size(360, 40);
            this.txtAddress_1.TabIndex = 7;
            // 
            // txtAddress_2
            // 
            this.txtAddress_2.Enabled = false;
            this.txtAddress_2.Location = new System.Drawing.Point(661, 130);
            this.txtAddress_2.MaxLength = 35;
            this.txtAddress_2.Name = "txtAddress_2";
            this.txtAddress_2.Size = new System.Drawing.Size(360, 40);
            this.txtAddress_2.TabIndex = 8;
            // 
            // txtAddress_3
            // 
            this.txtAddress_3.Enabled = false;
            this.txtAddress_3.Location = new System.Drawing.Point(661, 180);
            this.txtAddress_3.MaxLength = 35;
            this.txtAddress_3.Name = "txtAddress_3";
            this.txtAddress_3.Size = new System.Drawing.Size(360, 40);
            this.txtAddress_3.TabIndex = 9;
            // 
            // txtCity
            // 
            this.txtCity.Enabled = false;
            this.txtCity.ForeColor = System.Drawing.Color.FromName("@windowText");
            this.txtCity.Location = new System.Drawing.Point(661, 230);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(156, 40);
            this.txtCity.TabIndex = 16;
            // 
            // txtState
            // 
            this.txtState.Enabled = false;
            this.txtState.Location = new System.Drawing.Point(820, 230);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(55, 40);
            this.txtState.TabIndex = 17;
            // 
            // txtZip
            // 
            this.txtZip.Enabled = false;
            this.txtZip.Location = new System.Drawing.Point(878, 230);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(70, 40);
            this.txtZip.TabIndex = 18;
            // 
            // txtZip4
            // 
            this.txtZip4.Enabled = false;
            this.txtZip4.Location = new System.Drawing.Point(951, 230);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(70, 40);
            this.txtZip4.TabIndex = 19;
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 10;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(30, 306);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 16;
            this.vs1.Size = new System.Drawing.Size(991, 74);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 5;
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            // 
            // lblVendor
            // 
            this.lblVendor.Location = new System.Drawing.Point(485, 44);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(50, 16);
            this.lblVendor.TabIndex = 14;
            this.lblVendor.Text = "VENDOR";
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(77, 16);
            this.lblDescription.TabIndex = 13;
            this.lblDescription.Text = "DESCRIPTION";
            // 
            // lblReference
            // 
            this.lblReference.Location = new System.Drawing.Point(30, 94);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(87, 16);
            this.lblReference.TabIndex = 12;
            this.lblReference.Text = "REFERENCE";
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(30, 144);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(75, 16);
            this.lblAmount.TabIndex = 11;
            this.lblAmount.Text = "AMOUNT";
            // 
            // lblCheck
            // 
            this.lblCheck.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblCheck.Location = new System.Drawing.Point(30, 194);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(58, 15);
            this.lblCheck.TabIndex = 10;
            this.lblCheck.Text = "CHECK#";
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(262, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
            this.cmdProcessSave.TabIndex = 2;
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // lblExpense
            // 
            lblExpense.Location = new System.Drawing.Point(30, 280);
            lblExpense.Name = "lblExpense";
            lblExpense.Size = new System.Drawing.Size(373, 16);
            lblExpense.TabIndex = 15;
            // 
            // frmCorrectProject
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1051, 578);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCorrectProject";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "AP Correction";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCorrectProject_Load);
            this.Activated += new System.EventHandler(this.frmCorrectProject_Activated);
            this.Resize += new System.EventHandler(this.frmCorrectProject_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCorrectProject_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCorrectProject_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
		private System.ComponentModel.IContainer components;
	}
}