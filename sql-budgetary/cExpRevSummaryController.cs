﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWBD0000
{
	public class cExpRevSummaryController : cReportDataExporter
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;
		// 0 = CSV, 1 = Tab
		private bool boolUseExpDivision;
		private bool boolUseExpObject;
		private bool boolUseRevDivision;

		public cExpRevSummaryController() : base()
		{
			boolUseExpDivision = !modAccountTitle.Statics.ExpDivFlag;
			boolUseExpObject = !modAccountTitle.Statics.ObjFlag;
			boolUseRevDivision = !modAccountTitle.Statics.RevDivFlag;
		}

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theReport As cDetailsReport	OnRead(cExpRevSummaryReport)
		public void ExportData(ref cDetailsReport theReport, string strFileName)
		{
			StreamWriter ts = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				string strSeparator = "";
				string strQ = "";
				// vbPorter upgrade warning: erReport As cExpRevSummaryReport	OnWrite(cDetailsReport)
				cExpRevSummaryReport erReport;
				erReport = (cExpRevSummaryReport)theReport;
				strPath = Path.GetDirectoryName(strFileName);
				if (!Directory.Exists(strPath))
				{
					Information.Err().Raise(9999, null, "Path not found", null, null);
				}
				if (intFormatType == 1)
				{
					strSeparator = "\t";
					strQ = "";
				}
				else
				{
					strSeparator = ",";
					strQ = FCConvert.ToString(Convert.ToChar(34));
				}
				cExpRevSummaryItem erItem;
				string strLine;
				ts = new StreamWriter(strFileName);
				strLine = strQ + "Account" + strQ;
				strLine += strSeparator + strQ + "Account Type" + strQ;
				strLine += strSeparator + strQ + "Department" + strQ;
				if (boolUseExpDivision || boolUseRevDivision)
				{
					strLine += strSeparator + strQ + "Division" + strQ;
				}
				strLine += strSeparator + strQ + "Revenue" + strQ;
				strLine += strSeparator + strQ + "Expense" + strQ;
				if (boolUseExpObject)
				{
					strLine += strSeparator + strQ + "Object" + strQ;
				}
				strLine += strSeparator + strQ + "Fund" + strQ;
				strLine += strSeparator + strQ + "Budget" + strQ;
				strLine += strSeparator + strQ + "Current Month" + strQ;
				strLine += strSeparator + strQ + "Year To Date" + strQ;
				strLine += strSeparator + strQ + "Balance" + strQ;
				strLine += strSeparator + strQ + "Percent" + strQ;
				ts.WriteLine(strLine);
				theReport.Details.MoveFirst();
				while (theReport.Details.IsCurrent())
				{
					strLine = "";
					erItem = (cExpRevSummaryItem)theReport.Details.GetCurrentItem();
					strLine = strQ + erItem.Account + strQ;
					strLine += strSeparator + strQ + erItem.AccountType + strQ;
					strLine += strSeparator + strQ + erItem.Department + strQ;
					if (boolUseExpDivision || boolUseRevDivision)
					{
						strLine += strSeparator + strQ + erItem.Division + strQ;
					}
					strLine += strSeparator + strQ + erItem.Revenue + strQ;
					strLine += strSeparator + strQ + erItem.Expense + strQ;
					if (boolUseExpObject)
					{
						strLine += strSeparator + strQ + erItem.ExpenseObject + strQ;
					}
					strLine += strSeparator + strQ + erItem.Fund + strQ;
					strLine += strSeparator + FCConvert.ToString(erItem.Budget);
					strLine += strSeparator + FCConvert.ToString(erItem.CurrentMonth);
					strLine += strSeparator + FCConvert.ToString(erItem.YTD);
					strLine += strSeparator + FCConvert.ToString(erItem.Balance);
					strLine += strSeparator + FCConvert.ToString(erItem.PercentUsed);
					ts.WriteLine(strLine);
					theReport.Details.MoveNext();
				}
				ts.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (!(ts == null))
				{
					ts.Close();
				}
			}
		}
	}
}
