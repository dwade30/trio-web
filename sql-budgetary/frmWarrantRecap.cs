﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrantRecap.
	/// </summary>
	public partial class frmWarrantRecap : BaseForm
	{
		public frmWarrantRecap()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbInitial.SelectedIndex = 0;
			this.cboReport.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWarrantRecap InstancePtr
		{
			get
			{
				return (frmWarrantRecap)Sys.GetInstance(typeof(frmWarrantRecap));
			}
		}

		protected frmWarrantRecap _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/29/02
		// This form will be used to select which warrant recap to
		// print whether it be a new one or reprinting an old one
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//public clsDRWrapper rsInfo = new clsDRWrapper();
		public clsDRWrapper rsInfo_AutoInitialized;

		public clsDRWrapper rsInfo
		{
			get
			{
				if (rsInfo_AutoInitialized == null)
				{
					rsInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsInfo_AutoInitialized;
			}
			set
			{
				rsInfo_AutoInitialized = value;
			}
		}

		public int intPreviewChoice;
		public string strPrinterName = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingUtility setUtil = new cSettingUtility();
		private cSettingUtility setUtil_AutoInitialized;

		private cSettingUtility setUtil
		{
			get
			{
				if (setUtil_AutoInitialized == null)
				{
					setUtil_AutoInitialized = new cSettingUtility();
				}
				return setUtil_AutoInitialized;
			}
			set
			{
				setUtil_AutoInitialized = value;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdCancel2_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (cmbInitial.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT DISTINCT Warrant FROM APJournal WHERE Status = 'W' ORDER BY Warrant");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					// If GetRegistryKey("APProcessReportsDefaultPrinter", , False) Then
					if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
					{
						strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim(((modRegistry.GetRegistryKey("PrinterDeviceName")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
					}
					else
					{
                        //FC:FINAL:SBE - #844 - do not select the printer. The report will be exported to PDF and printed on client side
                        //Information.Err().Clear();
                        //dlg1_Save.Flags = 0;
                        //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                        //dlg1.CancelError = true;
                        ///*? On Error Resume Next  *///FC:FINAL:DSE:#667 Upon cancelling the print dialog, there should be no errors thrown.
                        //try
                        //{
                        //	dlg1.ShowPrinter();
                        //}
                        //catch
                        //{
                        //}
                        //if (Information.Err().Number == 0)
                        //{
                        //	strPrinterName = FCGlobal.Printer.DeviceName;
                        //}
                        //else
                        //{
                        //	// User Clicked Cancel   **** DO NOT PRINT ****
                        //	Information.Err().Clear();
                        //	return;
                        //}
                        ///*? On Error GoTo 0 */
                    }
                    this.Hide();
					if (rsInfo.RecordCount() > 1)
					{
						do
						{
							// rptWarrantRecap.Show , MDIParent
							if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("RecapOrderByAccounts")))
							{
								modDuplexPrinting.DuplexPrintReport(rptWarrantRecapAccountOrder.InstancePtr, strPrinterName);
								//rptWarrantRecapAccountOrder.InstancePtr.Hide();
							}
							else
							{
								modDuplexPrinting.DuplexPrintReport(rptWarrantRecap.InstancePtr, strPrinterName);
								//rptWarrantRecap.InstancePtr.Hide();
							}
							CheckAgain:
							;
							if (JobComplete())
							{
								rsInfo.MoveNext();
							}
							else
							{
								goto CheckAgain;
							}
						}
						while (rsInfo.EndOfFile() != true);
					}
					else
					{
						// rptWarrantRecap.PrintReport False
						if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("RecapOrderByAccounts")))
						{
							frmReportViewer.InstancePtr.Init(rptWarrantRecapAccountOrder.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
						}
						else
						{
							frmReportViewer.InstancePtr.Init(rptWarrantRecap.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
						}
					}
				}
				else
				{
					MessageBox.Show("There is no warrant information ready to be put on a Warrant Recap.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (cboReport.Items.Count > 0)
				{
					fraSelectReport.Visible = true;
					cmbInitial.Visible = false;
					lblInitial.Visible = false;
					//cmdCancel.Visible = false;
					//cmdProcess.Visible = false;
				}
				else
				{
					MessageBox.Show("There are no reports to reprint.", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			if (cboReport.SelectedIndex == -1)
			{
				MessageBox.Show("You must select which report you wish to reprint before you may continue.", "No Report Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				// If GetRegistryKey("APProcessReportsDefaultPrinter", , False) Then
				if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
				{
					strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim(((modRegistry.GetRegistryKey("PrinterDeviceName")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
				}
				else
				{
                    //FC:FINAL:SBE - #844 - do not select the printer. The report will be exported to PDF and printed on client side
                    //Information.Err().Clear();
                    //dlg1.Flags = 0;
                    //// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                    //dlg1.CancelError = true;
                    ///*? On Error Resume Next  *///FC:FINAL:DSE:#667 Upon cancelling the print dialog, there should be no errors thrown.
                    //try
                    //{
                    //	dlg1.ShowPrinter();
                    //}
                    //catch
                    //{
                    //}
                    //if (Information.Err().Number == 0)
                    //{
                    //	strPrinterName = FCGlobal.Printer.DeviceName;
                    //}
                    //else
                    //{
                    //	// User Clicked Cancel   **** DO NOT PRINT ****
                    //	Information.Err().Clear();
                    //	return;
                    //}
                    ///*? On Error GoTo 0 */
                }
                // FC: FINAL: KV: IIT807 + FC - 8697
                this.Hide();
				// rptWarrantRecap.PrintReport False
				if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("RecapOrderByAccounts")))
				{
					frmReportViewer.InstancePtr.Init(rptWarrantRecapAccountOrder.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(rptWarrantRecap.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
				}
			}
		}

		private void Command2_Click()
		{
			Close();
		}

		private void frmWarrantRecap_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsReportInfo = new clsDRWrapper();
			clsDRWrapper rsWarrrantInfo = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rsReportInfo.OpenRecordset("SELECT DISTINCT Convert(int, IsNull(Warrant, 0)) as WarrantNumber FROM APJournal WHERE Convert(int, IsNull(Warrant, 0)) <> 0 ORDER BY Convert(int, IsNull(Warrant, 0)) DESC");
			cboReport.Clear();
			if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
			{
				do
				{
					rsWarrrantInfo.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + rsReportInfo.Get_Fields_Int32("WarrantNumber"));
					if (rsWarrrantInfo.EndOfFile() != true && rsWarrrantInfo.BeginningOfFile() != true)
					{
						cboReport.AddItem("Warrant " + modValidateAccount.GetFormat_6(FCConvert.ToString(rsReportInfo.Get_Fields_Int32("WarrantNumber")), 4) + "  Date: " + Strings.Format(rsWarrrantInfo.Get_Fields_DateTime("WarrantDate"), "MM/dd/yyyy"));
					}
					else
					{
						cboReport.AddItem("Warrant " + modValidateAccount.GetFormat_6(FCConvert.ToString(rsReportInfo.Get_Fields_Int32("WarrantNumber")), 4) + "  Date: UNKNOWN");
					}
					rsReportInfo.MoveNext();
				}
				while (rsReportInfo.EndOfFile() != true);
			}
			if (cboReport.Items.Count > 0)
			{
				cboReport.SelectedIndex = 0;
			}
			this.Refresh();
		}

		private void frmWarrantRecap_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWarrantRecap.FillStyle	= 0;
			//frmWarrantRecap.ScaleWidth	= 3885;
			//frmWarrantRecap.ScaleHeight	= 2580;
			//frmWarrantRecap.LinkTopic	= "Form2";
			//frmWarrantRecap.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			intPreviewChoice = 2;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmWarrantRecap_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private bool JobComplete()
		{
			bool JobComplete = false;
			string strName = "";
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("RecapOrderByAccounts")))
			{
				strName = "rptWarrantRecapAccountOrder";
			}
			else
			{
				strName = "rptWarrantRecap";
			}
			foreach (Form ff in FCGlobal.Statics.Forms)
			{
				if (ff.Name == strName)
				{
					JobComplete = false;
					return JobComplete;
				}
			}
			JobComplete = true;
			return JobComplete;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (fraSelectReport.Visible == true)
			{
				Command1_Click(btnProcess, EventArgs.Empty);
			}
			else
			{
				cmdProcess_Click(btnProcess, EventArgs.Empty);
			}
		}

		private void frmWarrantRecap_Resize(object sender, EventArgs e)
		{
			fraSelectReport.CenterToContainer(this.ClientArea);
		}
	}
}
