﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cAccountReportGroup
	{
		//=========================================================
		private string strGroupValue = string.Empty;
		private string strDescription = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collAccounts = new cGenericCollection();
		private cGenericCollection collAccounts_AutoInitialized;

		private cGenericCollection collAccounts
		{
			get
			{
				if (collAccounts_AutoInitialized == null)
				{
					collAccounts_AutoInitialized = new cGenericCollection();
				}
				return collAccounts_AutoInitialized;
			}
			set
			{
				collAccounts_AutoInitialized = value;
			}
		}

		private double dblTotalCredits;
		private double dblTotalDebits;

		public string GroupValue
		{
			set
			{
				strGroupValue = value;
			}
			get
			{
				string GroupValue = "";
				GroupValue = strGroupValue;
				return GroupValue;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public cGenericCollection Accounts
		{
			get
			{
				cGenericCollection Accounts = null;
				Accounts = collAccounts;
				return Accounts;
			}
		}

		public double Credits
		{
			set
			{
				dblTotalCredits = value;
			}
			get
			{
				double Credits = 0;
				Credits = dblTotalCredits;
				return Credits;
			}
		}

		public double Debits
		{
			set
			{
				dblTotalDebits = value;
			}
			get
			{
				double Debits = 0;
				Debits = dblTotalDebits;
				return Debits;
			}
		}
	}
}
