﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJDefault.
	/// </summary>
	public partial class frmGetGJDefault : BaseForm
	{
		public frmGetGJDefault()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetGJDefault InstancePtr
		{
			get
			{
				return (frmGetGJDefault)Sys.GetInstance(typeof(frmGetGJDefault));
			}
		}

		protected frmGetGJDefault _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           1/5/04
		// This form will be used to select GJ default types to edit
		// or delete or it will be used to create new default types
		// ********************************************************
		bool blnShowEditScreen;

		private void cboDefaultType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboDefaultType.SelectedIndex != 0)
			{
				btnFileDelete.Enabled = true;
			}
			else
			{
				btnFileDelete.Enabled = false;
			}
		}

		private void frmGetGJDefault_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmGetGJDefault_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetGJDefault.FillStyle	= 0;
			//frmGetGJDefault.ScaleWidth	= 3885;
			//frmGetGJDefault.ScaleHeight	= 1995;
			//frmGetGJDefault.LinkTopic	= "Form2";
			//frmGetGJDefault.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			LoadDefaultCombo();
			cboDefaultType.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			blnShowEditScreen = false;
		}

		private void frmGetGJDefault_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!blnShowEditScreen)
			{
				//MDIParent.InstancePtr.Focus();
			}
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			clsDRWrapper rsDeleteDetail = new clsDRWrapper();
			ans = MessageBox.Show("Once you delete this type you will not be able to recover this information.  Do you wish to continue?", "Delete Default Type?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				rsDelete.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + modCustomReport.FixQuotes(cboDefaultType.Text) + "'");
				if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
				{
					rsDeleteDetail.Execute("DELETE FROM GJDefaultDetail WHERE GJDefaultMasterID = " + rsDelete.Get_Fields_Int32("ID"), "Budgetary");
					rsDelete.Delete();
					rsDelete.Update();
				}
			}
			cboDefaultType.Items.RemoveAt(cboDefaultType.SelectedIndex);
			cboDefaultType.SelectedIndex = 0;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadDefaultCombo()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			cboDefaultType.Clear();
			cboDefaultType.AddItem(" Create New Default Type");
			rsDefaultInfo.OpenRecordset("SELECT * FROM GJDefaultMaster");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				do
				{
					cboDefaultType.AddItem(rsDefaultInfo.Get_Fields_String("Description"));
					rsDefaultInfo.MoveNext();
				}
				while (rsDefaultInfo.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter = 0;
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			if (cboDefaultType.SelectedIndex == 0)
			{
				frmGJDefaultDataEntry.InstancePtr.lngChosenType = 0;
				frmGJDefaultDataEntry.InstancePtr.Show(App.MainForm);
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + modCustomReport.FixQuotes(cboDefaultType.Text) + "'");
				frmGJDefaultDataEntry.InstancePtr.lngChosenType = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
				frmGJDefaultDataEntry.InstancePtr.txtDescription.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Description"));
				rsDetailInfo.OpenRecordset("SELECT * FROM GJDefaultDetail WHERE GJDefaultMasterID = " + rsInfo.Get_Fields_Int32("ID") + " ORDER BY ID");
				if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
				{
					frmGJDefaultDataEntry.InstancePtr.vs1.Rows = rsDetailInfo.RecordCount() + 1;
					counter = 1;
					do
					{
						frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rsDetailInfo.Get_Fields_Int32("ID")));
						frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
						frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rsDetailInfo.Get_Fields("account")));
						frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Project")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0)
						{
							frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, "0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, Strings.Format(rsDetailInfo.Get_Fields("Amount") * -1, "#,##0.00"));
						}
						else
						{
							frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, "0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							frmGJDefaultDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
						}
						counter += 1;
						rsDetailInfo.MoveNext();
					}
					while (rsDetailInfo.EndOfFile() != true);
				}
				frmGJDefaultDataEntry.InstancePtr.Show(App.MainForm);
			}
			blnShowEditScreen = true;
			Close();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, EventArgs.Empty);
		}
	}
}
