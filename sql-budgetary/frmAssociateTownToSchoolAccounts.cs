﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAssociateTownToSchoolAccounts.
	/// </summary>
	public partial class frmAssociateTownToSchoolAccounts : BaseForm
	{
		public frmAssociateTownToSchoolAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int TownCol;
		int SchoolCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsGrid = new clsGridAccount();
		private clsGridAccount vsGrid_AutoInitialized;

		private clsGridAccount vsGrid
		{
			get
			{
				if (vsGrid_AutoInitialized == null)
				{
					vsGrid_AutoInitialized = new clsGridAccount();
				}
				return vsGrid_AutoInitialized;
			}
			set
			{
				vsGrid_AutoInitialized = value;
			}
		}

		private void frmAssociateTownToSchoolAccounts_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmAssociateTownToSchoolAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAssociateTownToSchoolAccounts.FillStyle	= 0;
			//frmAssociateTownToSchoolAccounts.ScaleWidth	= 9045;
			//frmAssociateTownToSchoolAccounts.ScaleHeight	= 7290;
			//frmAssociateTownToSchoolAccounts.LinkTopic	= "Form2";
			//frmAssociateTownToSchoolAccounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			TownCol = 0;
			SchoolCol = 1;
			vs1.ColWidth(TownCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.5));
			vs1.TextMatrix(0, TownCol, "Town Acct");
			vs1.TextMatrix(0, SchoolCol, "School Acct");
			vsGrid.GRID7Light = vs1;
			vsGrid.DefaultAccountType = "L";
			vsGrid.AccountCol = FCConvert.ToInt16(SchoolCol);
			LoadGrid();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmAssociateTownToSchoolAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void LoadGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			vs1.Col = TownCol;
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vs1.Rows += 1;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vs1.TextMatrix(vs1.Rows - 1, TownCol, FCConvert.ToString(rsInfo.Get_Fields("Account")));
					vs1.TextMatrix(vs1.Rows - 1, SchoolCol, FCConvert.ToString(rsInfo.Get_Fields_String("SchoolReportingAccount")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				vs1.Row = 1;
			}
			vs1.Col = SchoolCol;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			if (vs1.Row > 1)
			{
				vs1.Row = 1;
			}
			else if (vs1.Rows > 2)
			{
				vs1.Rows = vs1.Row + 1;
			}
			else
			{
				vs1.Col = 0;
				vs1.Col = 1;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vs1.TextMatrix(counter, TownCol) + "'");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
					if (Strings.Trim(vs1.TextMatrix(counter, SchoolCol)) != "" && Strings.InStr(1, vs1.TextMatrix(counter, SchoolCol), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						rsInfo.Set_Fields("SchoolReportingAccount", Strings.Trim(vs1.TextMatrix(counter, SchoolCol)));
					}
					else
					{
						rsInfo.Set_Fields("SchoolReportingAccount", "");
					}
					rsInfo.Update();
				}
			}
			MessageBox.Show("Information Saved Successfully!!", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
		}
	}
}
