﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJDefault.
	/// </summary>
	partial class frmGetGJDefault : BaseForm
	{
		public fecherFoundation.FCComboBox cboDefaultType;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetGJDefault));
			this.cboDefaultType = new fecherFoundation.FCComboBox();
			this.btnFileDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.cboDefaultType);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFileDelete);
			this.TopPanel.Controls.SetChildIndex(this.btnFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Select GJ Default Type";
			// 
			// cboDefaultType
			// 
			this.cboDefaultType.AutoSize = false;
			this.cboDefaultType.BackColor = System.Drawing.SystemColors.Window;
			this.cboDefaultType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDefaultType.FormattingEnabled = true;
			this.cboDefaultType.Location = new System.Drawing.Point(30, 30);
			this.cboDefaultType.Name = "cboDefaultType";
			this.cboDefaultType.Size = new System.Drawing.Size(400, 40);
			this.cboDefaultType.Sorted = true;
			this.cboDefaultType.TabIndex = 0;
			this.cboDefaultType.SelectedIndexChanged += new System.EventHandler(this.cboDefaultType_SelectedIndexChanged);
			// 
			// btnFileDelete
			// 
			this.btnFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileDelete.AppearanceKey = "toolbarButton";
			this.btnFileDelete.Enabled = false;
			this.btnFileDelete.Location = new System.Drawing.Point(914, 28);
			this.btnFileDelete.Name = "btnFileDelete";
			this.btnFileDelete.Size = new System.Drawing.Size(134, 24);
			this.btnFileDelete.TabIndex = 0;
			this.btnFileDelete.Text = "Delete Default Type";
			this.btnFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 100);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Process";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmGetGJDefault
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGetGJDefault";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select GJ Default Type";
			this.Load += new System.EventHandler(this.frmGetGJDefault_Load);
			this.Activated += new System.EventHandler(this.frmGetGJDefault_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetGJDefault_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSave;
		private FCButton btnFileDelete;
	}
}
