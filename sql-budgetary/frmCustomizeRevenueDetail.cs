﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeRevenueDetail.
	/// </summary>
	public partial class frmCustomizeRevenueDetail : BaseForm
	{
		public frmCustomizeRevenueDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbLong.SelectedIndex = 0;
			this.cmbDisplay.SelectedIndex = 0;
			this.cmbYTDNet.SelectedIndex = 1;
			this.cmbPendingSummary.SelectedIndex = 2;
            this.cmbWide.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeRevenueDetail InstancePtr
		{
			get
			{
				return (frmCustomizeRevenueDetail)Sys.GetInstance(typeof(frmCustomizeRevenueDetail));
			}
		}

		protected frmCustomizeRevenueDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		public bool FromRev;
		bool blnSavedFormat;

		private void chkShowLiquidatedEncumbranceActivity_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked)
			{
				cmbYTDNet.SelectedIndex = 0;
			}
		}

		private void frmCustomizeRevenueDetail_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnRevenueDetailEdit && !modBudgetaryMaster.Statics.blnRevenueDetailReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					cmbDisplay.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Use")) == "P" ? 0 : 1;
					chkShowLiquidatedEncumbranceActivity.CheckState =
                        FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean(
                                "ShowLiquidatedEncumbranceActivity"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					cmbWide.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("PaperWidth")) == "N" ? 0 : 1;
					cmbLong.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("DescriptionLength")) == "L" ? 0 : 1;
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					cmbYTDNet.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDDebitCredit")) ? 0 : 1;
					//cmbYTDNet.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDNet")) ? 0 : 1;
					bool optPendingDetail = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingDetail"));
					bool optPendingSummary = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingSummary"));
					if (!optPendingDetail && !optPendingSummary)
					{
						cmbPendingSummary.SelectedIndex = 2;
					}
					else if (optPendingDetail)
					{
						cmbPendingSummary.SelectedIndex = 0;
					}
					else 
					{
						cmbPendingSummary.SelectedIndex = 1;
					}
				}
			}
		}

		private void frmCustomizeRevenueDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeRevenueDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeRevenueDetail.FillStyle	= 0;
			//frmCustomizeRevenueDetail.ScaleWidth	= 9300;
			//frmCustomizeRevenueDetail.ScaleHeight	= 7350;
			//frmCustomizeRevenueDetail.LinkTopic	= "Form2";
			//frmCustomizeRevenueDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromRev)
			{
				FromRev = false;
				frmRevenueDetailSelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnRevenueDetailReportEdit)
					{
						modBudgetaryMaster.Statics.blnRevenueDetailReportEdit = false;
						frmRevenueDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmRevenueDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnRevenueDetailReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnRevenueDetailEdit)
			{
				modBudgetaryMaster.Statics.blnRevenueDetailEdit = false;
				frmGetRevenueDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeRevenueDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM RevenueDetailFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("YTDDebitCredit", cmbYTDNet.SelectedIndex == 0);
					rs.Set_Fields("YTDNet", cmbYTDNet.SelectedIndex == 1);
                    rs.Set_Fields("DescriptionLength", cmbLong.SelectedIndex == 0 ? "L" : "S");
                    rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                        chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                    rs.Set_Fields("PendingDetail", cmbPendingSummary.SelectedIndex == 0);
					rs.Set_Fields("PendingSummary", cmbPendingSummary.SelectedIndex == 1);
					rs.Set_Fields("Printer", "O");
					rs.Set_Fields("Font", "S");
                    rs.Set_Fields("PaperWidth", cmbWide.SelectedIndex == 0 ? "N" : "L");
                    rs.Set_Fields("Use", cmbDisplay.SelectedIndex == 0 ? "P" : "D");
                    rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM RevenueDetailFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("YTDDebitCredit", cmbYTDNet.SelectedIndex == 0);
				rs.Set_Fields("YTDNet", cmbYTDNet.SelectedIndex == 1);
                rs.Set_Fields("DescriptionLength", cmbLong.SelectedIndex == 0 ? "L" : "S");
                rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                    chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                rs.Set_Fields("PendingDetail", cmbPendingSummary.SelectedIndex == 0);
				rs.Set_Fields("PendingSummary", cmbPendingSummary.SelectedIndex == 1);
				rs.Set_Fields("Printer", "O");
				rs.Set_Fields("Font", "S");
				rs.Set_Fields("PaperWidth", cmbWide.SelectedIndex == 0 ? "N" : "L");
                rs.Set_Fields("Use", cmbDisplay.SelectedIndex == 0 ? "P" : "D");
                rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}

		private void cmbYTDNet_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbYTDNet.SelectedIndex == 1 && chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked)
			{
                MessageBox.Show("You may only view Debits / Credits if you want to show liquidated encumbrance activity.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbYTDNet.SelectedIndex = 0;
            }
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
