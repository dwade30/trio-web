﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmError.
	/// </summary>
	public partial class frmError : BaseForm
	{
		public frmError()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_4, FCConvert.ToInt16(4));
			this.Label1.AddControlArrayElement(Label1_3, FCConvert.ToInt16(3));
			this.Label1.AddControlArrayElement(Label1_2, FCConvert.ToInt16(2));
			this.Label1.AddControlArrayElement(Label1_1, FCConvert.ToInt16(1));
			this.Label1.AddControlArrayElement(Label1_0, FCConvert.ToInt16(0));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmError InstancePtr
		{
			get
			{
				return (frmError)Sys.GetInstance(typeof(frmError));
			}
		}

		protected frmError _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void cmdContinue_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void frmError_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmError.Appearance	= 0;
			//frmError.ScaleWidth	= 6135;
			//frmError.ScaleHeight	= 5145;
			//frmError.LinkTopic	= "Form2";
			//End Unmaped Properties
			frmError.InstancePtr.Left = FCConvert.ToInt32((FCGlobal.Screen.Width / 2) - (frmError.InstancePtr.Width / 2.0));
			frmError.InstancePtr.Top = FCConvert.ToInt32((FCGlobal.Screen.Height / 2) - (frmError.InstancePtr.Height / 2.0));
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}
	}
}
