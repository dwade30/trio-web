﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptRevenueSummary.
	/// </summary>
	public partial class rptRevenueSummary : BaseSectionReport
	{
		public static rptRevenueSummary InstancePtr
		{
			get
			{
				return (rptRevenueSummary)Sys.GetInstance(typeof(rptRevenueSummary));
			}
		}

		protected rptRevenueSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRevenueSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool DeptBreakFlag;
		bool DivBreakFlag;
		clsDRWrapper rs = new clsDRWrapper();
		int counter;
		int counter2;
		int TotRows;
		int counter3;
		float lngTotalWidth;
		int temp;
		int lngRowCounter;
		bool blnFirstRow;
		int lngColumnCounter;
		bool blnShade = true;
		bool blnReportShading;
		bool blnFirstDivision;
		string strTitleToShow = "";
		bool IsColorChanged = false;

		public rptRevenueSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Revenue Summary";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngTempCounter;
			lngTempCounter = 0;
			if (lngRowCounter > frmRevenueSummary.InstancePtr.vs1.Rows - 1)
			{
				eArgs.EOF = true;
				return;
			}
			if (blnFirstRow)
			{
				blnFirstRow = false;
				return;
			}
			if (frmRevenueSummary.InstancePtr.vs1.IsCollapsed(lngRowCounter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				do
				{
					lngTempCounter += 1;
					if (lngRowCounter + lngTempCounter > frmRevenueSummary.InstancePtr.vs1.Rows)
					{
						eArgs.EOF = true;
						return;
					}
				}
				while (!(frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) >= frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter + lngTempCounter)));
				lngRowCounter += lngTempCounter;
			}
			else
			{
				lngRowCounter += 1;
				if (lngRowCounter > frmRevenueSummary.InstancePtr.vs1.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
			}
			eArgs.EOF = false;
			if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
			{
				blnFirstDivision = true;
				if (lngRowCounter < frmRevenueSummary.InstancePtr.vs1.Rows - 1 && DeptBreakFlag)
				{
					this.Fields["Binder"].Value = lngRowCounter;
				}
			}
			if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1 && lngRowCounter < frmRevenueSummary.InstancePtr.vs1.Rows - 1 && DivBreakFlag)
			{
				if (blnFirstDivision)
				{
					blnFirstDivision = false;
				}
				else
				{
					this.Fields["Binder"].Value = lngRowCounter;
				}
			}
			// If .RowOutlineLevel(lngRowCounter) = 0 And lngRowCounter < .rows - 1 And DeptBreakFlag Then
			// rptRevenueSummary.Fields["Binder"].Value = lngRowCounter
			// End If
			// If .RowOutlineLevel(lngRowCounter) = 1 And lngRowCounter < .rows - 1 And DivBreakFlag Then
			// rptRevenueSummary.Fields["Binder"].Value = lngRowCounter
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intTotalCols;
			double dblAdjust;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			lngRowCounter = 0 + frmRevenueSummary.InstancePtr.vs1.FixedRows;
			blnFirstRow = true;
			dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("REPORTADJUSTMENT"));
			this.PageSettings.Margins.Top += FCConvert.ToSingle(200 * dblAdjust / 1440f);
			if (frmRevenueSummary.InstancePtr.strTitle != "")
			{
				Label1.Text = frmRevenueSummary.InstancePtr.strTitle;
			}
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ShadeReports")) == "Y")
			{
				blnReportShading = true;
			}
			else
			{
				blnReportShading = false;
			}
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DepartmentBreak") == true)
			{
				DeptBreakFlag = true;
			}
			else
			{
				DeptBreakFlag = false;
			}
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DivisionBreak") == true && !modAccountTitle.Statics.ExpDivFlag)
			{
				DivBreakFlag = true;
			}
			else
			{
				DivBreakFlag = false;
			}
			if (!DeptBreakFlag && !DivBreakFlag)
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			// get the report format for the report we want printed
			rs.OpenRecordset("SELECT * FROM RevenueSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			
			// if we are using wide paper let the print object know what type we are using
			if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				rptRevenueSummary.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 15100 / 1440f;
				Line1.X2 = this.PrintWidth - 400 / 1440f;
				Label1.Width = this.PrintWidth - (Label3.Width * 2);
				Label5.Width = this.PrintWidth - (Label3.Width * 2);
				Label6.Width = this.PrintWidth - (Label3.Width * 2);
				Label3.Left = this.PrintWidth - (Label3.Width + 400 / 1440F);
				Label4.Left = this.PrintWidth - (Label4.Width + 400 / 1440F);
			}
			fldDeptTitle.Width = this.PrintWidth - 400 / 1440f;
			// show information about what departments are being reported for which months
			// If frmRevenueSummary.lblRangeDept <> "ALL" Then
			Label6.Text = frmRevenueSummary.InstancePtr.lblTitle.Text + ": " + frmRevenueSummary.InstancePtr.lblRangeDept.Text;
			Label5.Text = frmRevenueSummary.InstancePtr.lblMonths;
			// Else
			// Label6 = ""
			// Label5 = frmRevenueSummary.lblMonths
			// End If
			lngTotalWidth = 0;
			lngColumnCounter = 1;
			intTotalCols = 0;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 2)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field1, 0);
					FormatFixedFields(Field14, 1);
				}
				else
				{
					FormatFixedFields(Field14, 1);
				}
				FormatFields(ref fld1);
				intTotalCols += 1;
			}
			lngColumnCounter = 2;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 3)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field2, 0);
					FormatFixedFields(Field15, 1);
				}
				else
				{
					FormatFixedFields(Field15, 1);
				}
				FormatFields(ref fld2);
				intTotalCols += 1;
			}
			lngColumnCounter = 3;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 4)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field3, 0);
					FormatFixedFields(Field16, 1);
				}
				else
				{
					FormatFixedFields(Field16, 1);
				}
				FormatFields(ref fld3);
				intTotalCols += 1;
			}
			lngColumnCounter = 4;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 5)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field4, 0);
					FormatFixedFields(Field17, 1);
				}
				else
				{
					FormatFixedFields(Field17, 1);
				}
				FormatFields(ref fld4);
				intTotalCols += 1;
			}
			lngColumnCounter = 5;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 6)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field5, 0);
					FormatFixedFields(Field18, 1);
				}
				else
				{
					FormatFixedFields(Field18, 1);
				}
				FormatFields(ref fld5);
				intTotalCols += 1;
			}
			lngColumnCounter = 6;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 7)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field6, 0);
					FormatFixedFields(Field19, 1);
				}
				else
				{
					FormatFixedFields(Field19, 1);
				}
				FormatFields(ref fld6);
				intTotalCols += 1;
			}
			lngColumnCounter = 7;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 8)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field7, 0);
					FormatFixedFields(Field20, 1);
				}
				else
				{
					FormatFixedFields(Field20, 1);
				}
				FormatFields(ref fld7);
				intTotalCols += 1;
			}
			lngColumnCounter = 8;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 9)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field8, 0);
					FormatFixedFields(Field21, 1);
				}
				else
				{
					FormatFixedFields(Field21, 1);
				}
				FormatFields(ref fld8);
				intTotalCols += 1;
			}
			lngColumnCounter = 9;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 10)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field9, 0);
					FormatFixedFields(Field22, 1);
				}
				else
				{
					FormatFixedFields(Field22, 1);
				}
				FormatFields(ref fld9);
				intTotalCols += 1;
			}
			lngColumnCounter = 10;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 11)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field10, 0);
					FormatFixedFields(Field23, 1);
				}
				else
				{
					FormatFixedFields(Field23, 1);
				}
				FormatFields(ref fld10);
				intTotalCols += 1;
			}
			lngColumnCounter = 11;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 12)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field11, 0);
					FormatFixedFields(Field24, 1);
				}
				else
				{
					FormatFixedFields(Field24, 1);
				}
				FormatFields(ref fld11);
				intTotalCols += 1;
			}
			lngColumnCounter = 12;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 13)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field12, 0);
					FormatFixedFields(Field25, 1);
				}
				else
				{
					FormatFixedFields(Field25, 1);
				}
				FormatFields(ref fld12);
				intTotalCols += 1;
			}
			lngColumnCounter = 13;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 14)
			{
				if (frmRevenueSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields(Field13, 0);
					FormatFixedFields(Field26, 1);
				}
				else
				{
					FormatFixedFields(Field26, 1);
				}
				FormatFields(ref fld13);
				intTotalCols += 1;
			}
            CenterYTDHeading(intTotalCols);
            CenterCurrentHeading(intTotalCols);			
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngColumnCounter = 1;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 2)
			{
				PrintFields(ref fld1);
			}
			lngColumnCounter = 2;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 3)
			{
				PrintFields(ref fld2);
			}
			lngColumnCounter = 3;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 4)
			{
				PrintFields(ref fld3);
			}
			lngColumnCounter = 4;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 5)
			{
				PrintFields(ref fld4);
			}
			lngColumnCounter = 5;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 6)
			{
				PrintFields(ref fld5);
			}
			lngColumnCounter = 6;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 7)
			{
				PrintFields(ref fld6);
			}
			lngColumnCounter = 7;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 8)
			{
				PrintFields(ref fld7);
			}
			lngColumnCounter = 8;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 9)
			{
				PrintFields(ref fld8);
			}
			lngColumnCounter = 9;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 10)
			{
				PrintFields(ref fld9);
			}
			lngColumnCounter = 10;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 11)
			{
				PrintFields(ref fld10);
			}
			lngColumnCounter = 11;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 12)
			{
				PrintFields(ref fld11);
			}
			lngColumnCounter = 12;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 13)
			{
				PrintFields(ref fld12);
			}
			lngColumnCounter = 13;
			if (frmRevenueSummary.InstancePtr.vs1.Cols >= 14)
			{
				PrintFields(ref fld13);
			}
			//FC:FINAL:MSH - Issue #685 - moved for correct colors changing
			if (blnReportShading && !IsColorChanged)
			{
				//this.Detail.BackStyle = ddBKNormal;
				if (blnShade)
				{
					blnShade = false;
					this.Detail.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld1.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld2.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld3.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld4.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld5.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld6.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld7.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld8.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld9.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld10.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld11.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld12.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld13.BackColor = ColorTranslator.FromOle(0xE0E0E0);
				}
				else
				{
					blnShade = true;
					this.Detail.BackColor = Color.White;
					fld1.BackColor = Color.White;
					fld2.BackColor = Color.White;
					fld3.BackColor = Color.White;
					fld4.BackColor = Color.White;
					fld5.BackColor = Color.White;
					fld6.BackColor = Color.White;
					fld7.BackColor = Color.White;
					fld8.BackColor = Color.White;
					fld9.BackColor = Color.White;
					fld10.BackColor = Color.White;
					fld11.BackColor = Color.White;
					fld12.BackColor = Color.White;
					fld13.BackColor = Color.White;
				}
			}
		}

		private void FormatFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			clsDRWrapper rsFormat = new clsDRWrapper();
			rsFormat.OpenRecordset("SELECT * FROM RevenueSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			x.Visible = true;
			x.Left = lngTotalWidth;
			x.Width = (frmRevenueSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f) / 1440f;
			x.OutputFormat = frmRevenueSummary.InstancePtr.vs1.ColFormat(lngColumnCounter);
			lngTotalWidth += x.Width;
			if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
			{
				fld1.Height = 290 / 1440f;
				rptRevenueSummary.InstancePtr.Detail.Height = fld1.Height;
				fld1.Font = new Font(fld1.Font, FontStyle.Bold);
			}
			else
			{
				fld1.Height = 240 / 1440f;
				rptRevenueSummary.InstancePtr.Detail.Height = fld1.Height;
				fld1.Font = new Font(fld1.Font, FontStyle.Regular);
			}
			x.Font = new Font(x.Font.Name, 10);
			rsFormat.Dispose();
		}

		private void PrintFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
			{
                strTitleToShow = frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, 1);
                //fldDeptTitle.BackStyle = 1;
                fldDeptTitle.Height = 290 / 1440f;
                fldDeptTitle.Font = new Font(fldDeptTitle.Font, FontStyle.Regular);
                fldDeptTitle.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
                fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 8);
                fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, fldDeptTitle.Font.Size + 1);
			}
            //FC:FINAL:MSH - Issue #685: in VB6 we have BackStyle property, which can change opacity of textbox background.
            // In web we can't change opacity of backround, so we must change color of TextBox background.
            if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
            {
                //x.BackStyle = 1;
                this.Detail.BackColor = Color.FromArgb(192, 192, 192);
                x.BackColor = Color.FromArgb(192, 192, 192);
                IsColorChanged = true;
            }
            else
            {
                //x.BackStyle = 0;
                this.Detail.BackColor = Color.White;
                x.BackColor = Color.White;
                IsColorChanged = false;
            }
			
			if (lngRowCounter < frmRevenueSummary.InstancePtr.vs1.Rows - 1)
			{
				if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter + 1) == 1)
				{
					x.Height = 290 / 1440f;
				}
				else
				{
					x.Height = 240 / 1440f;
				}
			}
			else
			{
				x.Height = 240 / 1440f;
			}
			if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
			{
				x.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
			}
			else
			{
				x.VerticalAlignment = 0;
			}
			if (lngRowCounter < frmRevenueSummary.InstancePtr.vs1.Rows - 1 && Strings.Left(frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter + 1, 1), 1) != " ")
			{
                if (Strings.Left(frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, 1), 1) == " ")
                {
                    x.Height *= 2;
                }
            }

            x.ForeColor =
                ColorTranslator.FromOle(frmRevenueSummary.InstancePtr.vs1.Cell(
                    FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.Black ||
                ColorTranslator.FromOle(frmRevenueSummary.InstancePtr.vs1.Cell(
                    FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.White ||
                frmRevenueSummary.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter) == 0x05CC47
					? Color.Black
                    : (Color) ColorTranslator.FromOle(frmRevenueSummary.InstancePtr.vs1.Cell(
                        FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter));

            x.Font = new Font(x.Font.Name, 8);
            if (frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0 && lngColumnCounter == 1)
            {
                x.Font = new Font(x.Font.Name, x.Font.Size + 1);
            }

			//FC:FINAL:MSH - Issue #685: moved to the end, because previously the FontStyle was redefined in the end
			x.Font = frmRevenueSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1 ? new Font(x.Font, FontStyle.Bold) : new Font(x.Font, FontStyle.Regular);
			if (Information.IsNumeric(frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter)) &&
                lngColumnCounter == frmRevenueSummary.InstancePtr.vs1.Cols - 1 &&
                frmRevenueSummary.InstancePtr.vs1.ColFormat(lngColumnCounter) == "")
			{
				x.Text = Strings.Format(frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter), "0.00");
			}
			else
            {
                x.Text = frmRevenueSummary.InstancePtr.vs1.ColFormat(lngColumnCounter) == ""
                    ? frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter)
                    : Strings.Format(frmRevenueSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter),
                        frmRevenueSummary.InstancePtr.vs1.ColFormat(lngColumnCounter));
            }
		}

		private void FormatFixedFields(GrapeCity.ActiveReports.SectionReportModel.TextBox x, short TempRow)
		{
            x.Visible = true;
            x.Left = lngTotalWidth;
            //if (frmRevenueSummary.InstancePtr.vs1.TextMatrix(0, lngColumnCounter).ToLower() == "percent" && frmRevenueSummary.InstancePtr.vs1.TextMatrix(1, lngColumnCounter).ToLower() == "collected")
            //{
            //    x.Width = .5f;
            //}
            //else
            //{
                x.Width = (frmRevenueSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f) / 1440f;
//}
            x.Font = new Font(x.Font.Name, 10);
            var caption =frmRevenueSummary.InstancePtr.vs1.TextMatrix(TempRow, lngColumnCounter).Trim();
			switch  (caption.ToLower())
            {
				case "current month":
                    caption = "CURR MONTH";
                    break;
				case "year to date":
                    caption = "YTD";
                    break;
				case "encumbrance":
                    caption = "ENCUM";
                    break;
				case "percent":
                    caption = "PCT";
                    break;
				case "collected":
                    caption = "COLL";
                    break;
				case "uncollected":
                    caption = "UNCOLL";
                    break;
                case "adjustments":
                    caption = "ADJUSTMENT";
                    break;
                case "outstanding":
                    caption = "OUTSTAND";
                    break;
			}

            x.Text = caption;

        }

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}
		//FC:FINAL:BBE:#i721 - because the cells are merged the text in some header cells is empty
		private void CenterYTDHeading(int intFields)
		{
			//int intYTDFields;
			if (Field1.Text == "YTD")
			{
				if (intFields >= 3)
				{
					if (Field2.Text == "" && Field3.Text == "")
					{
						Field2.Visible = false;
						Field3.Visible = false;
						Field1.Width = Field3.Left - Field1.Left + Field3.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "------------ Y T D ------------";
					}
					else if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 2)
				{
					if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field2.Text == "YTD")
			{
				if (intFields >= 4)
				{
					if (Field3.Text == "" && Field4.Text == "")
					{
						Field3.Visible = false;
						Field4.Visible = false;
						Field2.Width = Field4.Left - Field2.Left + Field4.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "------------ Y T D ------------";
					}
					else if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 3)
				{
					if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field3.Text == "YTD")
			{
				if (intFields >= 5)
				{
					if (Field4.Text == "" && Field5.Text == "")
					{
						Field4.Visible = false;
						Field5.Visible = false;
						Field3.Width = Field5.Left - Field3.Left + Field5.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "------------ Y T D ------------";
					}
					else if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 4)
				{
					if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field4.Text == "YTD")
			{
				if (intFields >= 6)
				{
					if (Field5.Text == "" && Field6.Text == "")
					{
						Field5.Visible = false;
						Field6.Visible = false;
						Field4.Width = Field6.Left - Field4.Left + Field6.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "------------ Y T D ------------";
					}
					else if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 5)
				{
					if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field5.Text == "YTD")
			{
				if (intFields >= 7)
				{
					if (Field6.Text == "" && Field7.Text == "")
					{
						Field6.Visible = false;
						Field7.Visible = false;
						Field5.Width = Field7.Left - Field5.Left + Field7.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "------------ Y T D ------------";
					}
					else if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 6)
				{
					if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field6.Text == "YTD")
			{
				if (intFields >= 8)
				{
					if (Field7.Text == "" && Field8.Text == "")
					{
						Field7.Visible = false;
						Field8.Visible = false;
						Field6.Width = Field8.Left - Field6.Left + Field8.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "------------ Y T D ------------";
					}
					else if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 7)
				{
					if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field7.Text == "YTD")
			{
				if (intFields >= 9)
				{
					if (Field8.Text == "" && Field9.Text == "")
					{
						Field8.Visible = false;
						Field9.Visible = false;
						Field7.Width = Field9.Left - Field7.Left + Field9.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "------------ Y T D ------------";
					}
					else if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 8)
				{
					if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field8.Text == "YTD")
			{
				if (intFields >= 10)
				{
					if (Field9.Text == "" && Field10.Text == "")
					{
						Field9.Visible = false;
						Field10.Visible = false;
						Field8.Width = Field10.Left - Field8.Left + Field10.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "------------ Y T D ------------";
					}
					else if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 9)
				{
					if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field9.Text == "YTD")
			{
				if (intFields >= 11)
				{
					if (Field10.Text == "" && Field11.Text == "")
					{
						Field10.Visible = false;
						Field11.Visible = false;
						Field9.Width = Field11.Left - Field9.Left + Field11.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "------------ Y T D ------------";
					}
					else if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 10)
				{
					if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field10.Text == "YTD")
			{
				if (intFields >= 12)
				{
					if (Field11.Text == "" && Field12.Text == "")
					{
						Field11.Visible = false;
						Field12.Visible = false;
						Field10.Width = Field12.Left - Field10.Left + Field12.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "------------ Y T D ------------";
					}
					else if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 11)
				{
					if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field11.Text == "YTD")
			{
				if (intFields >= 13)
				{
					if (Field12.Text == "" && Field13.Text == "")
					{
						Field12.Visible = false;
						Field13.Visible = false;
						Field11.Width = Field13.Left - Field11.Left + Field13.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "------------ Y T D ------------";
					}
					else if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 12)
				{
					if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field12.Text == "YTD")
			{
				if (intFields >= 13)
				{
					if (Field13.Text == "")
					{
						Field13.Visible = false;
						Field12.Width = Field13.Left - Field12.Left + Field13.Width;
						Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field12.Text = "---- Y T D ----";
					}
				}
			}
		}
		//FC:FINAL:BBE:#i721 - because the cells are merged the text in some header cells is empty
		private void CenterCurrentHeading(int intFields)
		{
			//int intYTDFields;
			if (Field1.Text == "Curr Mnth")
			{
				if (intFields >= 3)
				{
					if (Field2.Text == "" && Field3.Text == "")
					{
						Field2.Visible = false;
						Field3.Visible = false;
						Field1.Width = Field3.Left - Field1.Left + Field3.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 2)
				{
					if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field2.Text == "Curr Mnth")
			{
				if (intFields >= 4)
				{
					if (Field3.Text == "" && Field4.Text == "")
					{
						Field3.Visible = false;
						Field4.Visible = false;
						Field2.Width = Field4.Left - Field2.Left + Field4.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 3)
				{
					if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field3.Text == "Curr Mnth")
			{
				if (intFields >= 5)
				{
					if (Field4.Text == "" && Field5.Text == "")
					{
						Field4.Visible = false;
						Field5.Visible = false;
						Field3.Width = Field5.Left - Field3.Left + Field5.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 4)
				{
					if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field4.Text == "Curr Mnth")
			{
				if (intFields >= 6)
				{
					if (Field5.Text == "" && Field6.Text == "")
					{
						Field5.Visible = false;
						Field6.Visible = false;
						Field4.Width = Field6.Left - Field4.Left + Field6.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 5)
				{
					if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field5.Text == "Curr Mnth")
			{
				if (intFields >= 7)
				{
					if (Field6.Text == "" && Field7.Text == "")
					{
						Field6.Visible = false;
						Field7.Visible = false;
						Field5.Width = Field7.Left - Field5.Left + Field7.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 6)
				{
					if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field6.Text == "Curr Mnth")
			{
				if (intFields >= 8)
				{
					if (Field7.Text == "" && Field8.Text == "")
					{
						Field7.Visible = false;
						Field8.Visible = false;
						Field6.Width = Field8.Left - Field6.Left + Field8.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 7)
				{
					if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field7.Text == "Curr Mnth")
			{
				if (intFields >= 9)
				{
					if (Field8.Text == "" && Field9.Text == "")
					{
						Field8.Visible = false;
						Field9.Visible = false;
						Field7.Width = Field9.Left - Field7.Left + Field9.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 8)
				{
					if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field8.Text == "Curr Mnth")
			{
				if (intFields >= 10)
				{
					if (Field9.Text == "" && Field10.Text == "")
					{
						Field9.Visible = false;
						Field10.Visible = false;
						Field8.Width = Field10.Left - Field8.Left + Field10.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 9)
				{
					if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field9.Text == "Curr Mnth")
			{
				if (intFields >= 11)
				{
					if (Field10.Text == "" && Field11.Text == "")
					{
						Field10.Visible = false;
						Field11.Visible = false;
						Field9.Width = Field11.Left - Field9.Left + Field11.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 10)
				{
					if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field10.Text == "Curr Mnth")
			{
				if (intFields >= 12)
				{
					if (Field11.Text == "" && Field12.Text == "")
					{
						Field11.Visible = false;
						Field12.Visible = false;
						Field10.Width = Field12.Left - Field10.Left + Field12.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 11)
				{
					if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field11.Text == "Curr Mnth")
			{
				if (intFields >= 13)
				{
					if (Field12.Text == "" && Field13.Text == "")
					{
						Field12.Visible = false;
						Field13.Visible = false;
						Field11.Width = Field13.Left - Field11.Left + Field13.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "------- C U R R   M O N T H -------";
					}
					else if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 12)
				{
					if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field12.Text == "Curr Mnth")
			{
				if (intFields >= 13)
				{
					if (Field13.Text == "")
					{
						Field13.Visible = false;
						Field12.Width = Field13.Left - Field12.Left + Field13.Width;
						Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field12.Text = "- C U R R   M O N T H -";
					}
				}
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			if (this.PageNumber == 1)
			{
				fldDeptTitle.Text = "";
				fldDeptTitle.Visible = false;
			}
			else
			{
				fldDeptTitle.Text = strTitleToShow + " CONT'D";
				fldDeptTitle.Visible = true;
			}
		}

		private void rptRevenueSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRevenueSummary.Caption	= "Revenue Summary";
			//rptRevenueSummary.Icon	= "rptRevenueSummary.dsx":0000";
			//rptRevenueSummary.Left	= 0;
			//rptRevenueSummary.Top	= 0;
			//rptRevenueSummary.Width	= 11880;
			//rptRevenueSummary.Height	= 8595;
			//rptRevenueSummary.StartUpPosition	= 3;
			//rptRevenueSummary.SectionData	= "rptRevenueSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
