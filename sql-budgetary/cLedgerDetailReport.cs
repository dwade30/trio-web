﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cLedgerDetailReport : cDetailsReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection listDetails = new cGenericCollection();
		private cGenericCollection listDetails_AutoInitialized;

		private cGenericCollection listDetails
		{
			get
			{
				if (listDetails_AutoInitialized == null)
				{
					listDetails_AutoInitialized = new cGenericCollection();
				}
				return listDetails_AutoInitialized;
			}
			set
			{
				listDetails_AutoInitialized = value;
			}
		}

		private bool boolShowJournalDetail;
		private bool boolShowLiquidatedEncActivity;
		private bool boolShowPendingActivity;

		public bool ShowLiquidatedEncumbranceActivity
		{
			set
			{
				boolShowLiquidatedEncActivity = value;
			}
			get
			{
				bool ShowLiquidatedEncumbranceActivity = false;
				ShowLiquidatedEncumbranceActivity = boolShowLiquidatedEncActivity;
				return ShowLiquidatedEncumbranceActivity;
			}
		}

		public bool ShowPendingActivity
		{
			set
			{
				boolShowPendingActivity = value;
			}
			get
			{
				bool ShowPendingActivity = false;
				ShowPendingActivity = boolShowPendingActivity;
				return ShowPendingActivity;
			}
		}

		public bool ShowJournalDetail
		{
			set
			{
				boolShowJournalDetail = value;
			}
			get
			{
				bool ShowJournalDetail = false;
				ShowJournalDetail = boolShowJournalDetail;
				return ShowJournalDetail;
			}
		}

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				Details = listDetails;
				return Details;
			}
		}
	}
}
