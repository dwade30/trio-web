﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for rptCustomLabels.
    /// </summary>
    public partial class rptCustomLabels : BaseSectionReport
    {
        public static rptCustomLabels InstancePtr
        {
            get
            {
                return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
            }
        }

        protected rptCustomLabels _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // Date           :               09/12/2002              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // Last Updated   :               08/27/2003              *
        // ********************************************************
        // THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
        clsDRWrapper rsData = new clsDRWrapper();
        //clsDRWrapper rsMort = new clsDRWrapper();
        // vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
        float intLabelWidth;
        bool boolDifferentPageSize;
        string strFont;
        bool boolPP;
        bool boolMort;
        bool boolEmpty;
        string strLeftAdjustment;
        // vbPorter upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
        int lngVertAdjust;
        int intTypeOfLabel;
        string strReport;
        clsPrintLabel labLabels = new clsPrintLabel();
        // vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
        float lngPrintWidth;
        bool blnNameOnly;
        string strOldDefaultPrinter;

        public rptCustomLabels()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            Name = "Custom Labels";
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            // IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
            eArgs.EOF = rsData.EndOfFile();
        }
        // vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
        public void Init(string strSQL, string strReportType, short intLabelType, ref string strPrinterName, string strFonttoUse)
        {
            string strDBName = "";

            double dblLabelsAdjustment;
            clsDRWrapper rsAdjustInfo = new clsDRWrapper();
            rsAdjustInfo.OpenRecordset("SELECT * FROM Budgetary");
            dblLabelsAdjustment = Conversion.Val(modRegistry.GetRegistryKey("LABELLASERADJUSTMENT"));
            strReport = strReportType;
            strFont = strFonttoUse;
            Document.Printer.PrinterName = strPrinterName;
            rsData.OpenRecordset(strSQL);
            intTypeOfLabel = intLabelType;
            // make them choose the printer first if you have to use a custom form
            // RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
            if (rsData.RecordCount() != 0)
                rsData.MoveFirst();
            boolDifferentPageSize = false;
            strLeftAdjustment = "";
            lngVertAdjust = FCConvert.ToInt32(240 * dblLabelsAdjustment);
            blnNameOnly = intTypeOfLabel == modLabels.CNSTLBLTYPE5066;
            int intindex = labLabels.Get_IndexFromID(intTypeOfLabel);

            if (labLabels.Get_IsDymoLabel(intindex))
            {
                switch (labLabels.Get_ID(intindex))
                {
                    case modLabels.CNSTLBLTYPEDYMO30256:
                        {

                            strPrinterName = Document.Printer.PrinterName;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Margins.Top = FCConvert.ToSingle(1.128);
                            PageSettings.Margins.Bottom = 0;
                            PageSettings.Margins.Right = 0;
                            PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
                            PrintWidth = FCConvert.ToSingle((4) - PageSettings.Margins.Left - PageSettings.Margins.Right);
                            intLabelWidth = 4F;
                            lngPrintWidth = 4F - PageSettings.Margins.Left - PageSettings.Margins.Right;
                            Detail.Height = 2.3125F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440F;
                            // 2 5/16"
                            Detail.ColumnCount = 1;
                            PageSettings.PaperHeight = 4;
                            PageSettings.PaperWidth = FCConvert.ToSingle(2.31);
                            Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            break;
                        }
                    case modLabels.CNSTLBLTYPEDYMO30252:
                        {

                            strPrinterName = Document.Printer.PrinterName;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Margins.Top = FCConvert.ToSingle(0.064);
                            PageSettings.Margins.Bottom = 0;
                            PageSettings.Margins.Right = 0;
                            PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
                            // 
                            PrintWidth = FCConvert.ToSingle((3.5) - PageSettings.Margins.Left - PageSettings.Margins.Right);
                            intLabelWidth = 3.5F;
                            lngPrintWidth = 3.5F - PageSettings.Margins.Left - PageSettings.Margins.Right;
                            Detail.Height = 1.1F - PageSettings.Margins.Top - PageSettings.Margins.Bottom - 10 / 1440F;
                            Detail.ColumnCount = 1;
                            PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
                            PageSettings.PaperWidth = FCConvert.ToSingle(1.1);
                            Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                            break;
                        }
                }
            }
            else if (labLabels.Get_IsLaserLabel(intindex))
            {
                PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
                PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment * 270 / 1440f);
                PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
                PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
                PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
                intLabelWidth = labLabels.Get_LabelWidth(intindex);
                Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
                if (labLabels.Get_LabelsWide(intindex) > 0)
                {
                    Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
                    Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
                }
                lngPrintWidth = PrintWidth;
            }
            CreateDataFields();
            rsAdjustInfo.Dispose();
            frmReportViewer.InstancePtr.Init(this, strPrinterName);
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            int intReturn = 0;

            if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
            {
                int cnt;

                for (cnt = 0; cnt <= Document.Printer.PaperSizes.Count - 1; cnt++)
                {
                    switch (intTypeOfLabel)
                    {
                        case modLabels.CNSTLBLTYPEDYMO30252:
                            {
                                if (Strings.UCase(Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
                                {
                                    Document.Printer.PaperSize = Document.Printer.PaperSizes[cnt];
                                    PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                                    break;
                                }
                                break;
                            }
                        case modLabels.CNSTLBLTYPEDYMO30256:
                            {
                                if (Strings.UCase(Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
                                {
                                    Document.Printer.PaperSize = Document.Printer.PaperSizes[cnt];
                                    PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                                    break;
                                }
                                break;
                            }
                    }
                    //end switch
                }
                // cnt
            }
            else
            {
                Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomLabel", FCConvert.ToInt32(PageSettings.PaperWidth * 100), FCConvert.ToInt32(PageSettings.PaperHeight * 100));
            }
        }

        private void CreateDataFields()
        {
            GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
            int intRow;
            int intNumber = 0;
            // CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
            intNumber = blnNameOnly ? 1 : 5;
            for (intRow = 1; intRow <= intNumber; intRow++)
            {
                NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow));
                NewField.CanGrow = false;
                //NewField.Name = "txtData" + FCConvert.ToString(intRow);
                NewField.Top = (((intRow - 1) * 240) + lngVertAdjust + 240) / 1440f;
                NewField.Left = 144 / 1440f;
                // one space
                if (intRow == 1)
                {
                    NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1144 / 1440f;
                }
                else
                {
                    NewField.Width = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
                }
                NewField.Height = 240 / 1440f;
                NewField.Text = string.Empty;
                NewField.MultiLine = false;
                NewField.WrapMode = GrapeCity.ActiveReports.Document.Section.WrapMode.WordWrap;
                NewField.Font = Strings.Trim(strFont) != string.Empty ? new Font(strFont, NewField.Font.Size) : new Font(lblFont.Font.Name, NewField.Font.Size);

                if (intRow != 1) continue;
                NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtAcct");
                //Detail.Controls.Add(NewField);
                NewField.CanGrow = false;
                //NewField.Name = "txtAcct";
                NewField.Top = (((intRow - 1) * 240) + lngVertAdjust + 240) / 1440f;
                NewField.Left = (PrintWidth / Detail.ColumnCount - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1145 / 1440f;
                NewField.Width = 999 / 1440f;
                NewField.Height = 240 / 1440f;
                NewField.Text = string.Empty;
                NewField.MultiLine = false;
                NewField.WrapMode = GrapeCity.ActiveReports.Document.Section.WrapMode.WordWrap;
                NewField.Font = Strings.Trim(strFont) != string.Empty ? new Font(strFont, NewField.Font.Size) : new Font(lblFont.Font.Name, NewField.Font.Size);
            }
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {

            if (frmVendorLabels.InstancePtr.blnFromVendorMaster)
            {
                frmVendorMaster.InstancePtr.Show(App.MainForm);
            }
        }

        private void PrintLabels()
        {
            // this will fill in the fields for labels
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string str4 = "";
            string str5 = "";
            if (rsData.EndOfFile() != true)
            {
                if (strReport == "VENDORS")
                {
                    str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckName")));
                    str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckAddress1")));
                    str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckAddress2")));
                    str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckAddress3")));
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckState"))) != "")
                    {
                        if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip4"))) != "")
                        {
                            str5 = Strings.Trim(rsData.Get_Fields_String("CheckCity") + ", " +
                                                Strings.Trim(
                                                    FCConvert.ToString(rsData.Get_Fields_String("CheckState"))) + " " +
                                                Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip"))) +
                                                "-" + Strings.Trim(
                                                    FCConvert.ToString(rsData.Get_Fields_String("CheckZip4"))));
                        }
                        else
                        {
                            str5 = Strings.Trim(rsData.Get_Fields_String("CheckCity") + ", " +
                                                Strings.Trim(
                                                    FCConvert.ToString(rsData.Get_Fields_String("CheckState"))) + " " +
                                                Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip"))));
                        }
                    }
                    else
                    {
                        if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip4"))) != "")
                        {
                            str5 = Strings.Trim(rsData.Get_Fields_String("CheckCity") + " " +
                                                Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip"))) +
                                                "-" + Strings.Trim(
                                                    FCConvert.ToString(rsData.Get_Fields_String("CheckZip4"))));
                        }
                        else
                        {
                            str5 = Strings.Trim(rsData.Get_Fields_String("CheckCity") + " " +
                                                Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckZip"))));
                        }
                    }
                }
                // condense the labels if some are blank
                // If boolMort Then
                if (Strings.Trim(str4) == string.Empty)
                {
                    str4 = str5;
                    str5 = "";
                }
                else
                {
                }

                // End If
                if (Strings.Trim(str3) == string.Empty)
                {
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }
                if (Strings.Trim(str2) == string.Empty)
                {
                    str2 = str3;
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }
                if (Strings.Trim(str1) == string.Empty)
                {
                    str1 = str2;
                    str2 = str3;
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }

                int intControl;
                for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
                {
                    if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
                    {
                        var intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
                        switch (intRow)
                        {
                            case 1:
                                {
                                    // If Len(str1) > 18 Then
                                    // Detail.Controls(intControl).Text = strLeftAdjustment & Left(str1, 18)
                                    // Else
                                    (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
                                    // End If
                                    break;
                                }
                            case 2:
                                {
                                    (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
                                    break;
                                }
                            case 3:
                                {
                                    (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
                                    break;
                                }
                            case 4:
                                {
                                    (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
                                    break;
                                }
                            case 5:
                                {
                                    (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
                                    break;
                                }
                        }
                        //end switch
                    }
                    else if (Detail.Controls[intControl].Name == "txtAcct")
                    {
                        (Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields_Int32("VendorNumber"), "0000");
                    }
                }
                // intControl
                rsData.MoveNext();
            }
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            PrintLabels();
        }

        private void rptCustomLabels_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //rptCustomLabels.Caption	= "Custom Labels";
            //rptCustomLabels.Icon	= "rptCustomLabels.dsx":0000";
            //rptCustomLabels.Left	= 0;
            //rptCustomLabels.Top	= 0;
            //rptCustomLabels.Width	= 11880;
            //rptCustomLabels.Height	= 8595;
            //rptCustomLabels.StartUpPosition	= 3;
            //rptCustomLabels.SectionData	= "rptCustomLabels.dsx":058A;
            //End Unmaped Properties
        }
    }
}
