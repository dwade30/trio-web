﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1099EditReport.
	/// </summary>
	partial class rpt1099EditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1099EditReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDBA = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader6 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter6 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.subVendorErrors = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.GroupHeader4 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter4 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldFederalID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress1Info = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress2Info = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldContact = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFilingElectronically = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader5 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter5 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCode1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.SpaceBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.VendorBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.fldVendorTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode2Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode2GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode1Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode1GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode3Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode3GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode5Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode5GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode4Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode4GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode6Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode6GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode7Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode7GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode8Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode8GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode9Title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode9GroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.FormTypeBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFormTypeLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDBA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMunicipality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1Info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2Info)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFilingElectronically)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpaceBinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendorBinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9GroupTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormTypeBinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormTypeLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldVendor,
            this.fldTaxID,
            this.fldJournal,
            this.fldDate,
            this.fldDescription,
            this.fldCode,
            this.fldAmount,
            this.fldDBA});
            this.Detail.Height = 0.375F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldVendor
            // 
            this.fldVendor.CanShrink = true;
            this.fldVendor.Height = 0.1875F;
            this.fldVendor.Left = 0F;
            this.fldVendor.MultiLine = false;
            this.fldVendor.Name = "fldVendor";
            this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 1";
            this.fldVendor.Text = "Field1";
            this.fldVendor.Top = 0F;
            this.fldVendor.Width = 2.1875F;
            // 
            // fldTaxID
            // 
            this.fldTaxID.CanShrink = true;
            this.fldTaxID.Height = 0.1875F;
            this.fldTaxID.Left = 2.25F;
            this.fldTaxID.Name = "fldTaxID";
            this.fldTaxID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldTaxID.Text = "Field1";
            this.fldTaxID.Top = 0F;
            this.fldTaxID.Width = 0.75F;
            // 
            // fldJournal
            // 
            this.fldJournal.CanShrink = true;
            this.fldJournal.Height = 0.1875F;
            this.fldJournal.Left = 3.0625F;
            this.fldJournal.Name = "fldJournal";
            this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.fldJournal.Text = "Field1";
            this.fldJournal.Top = 0F;
            this.fldJournal.Width = 0.375F;
            // 
            // fldDate
            // 
            this.fldDate.CanShrink = true;
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 3.5F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.fldDate.Text = "Field1";
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.625F;
            // 
            // fldDescription
            // 
            this.fldDescription.CanShrink = true;
            this.fldDescription.Height = 0.1875F;
            this.fldDescription.Left = 4.15625F;
            this.fldDescription.Name = "fldDescription";
            this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldDescription.Text = "Field1";
            this.fldDescription.Top = 0F;
            this.fldDescription.Width = 2F;
            // 
            // fldCode
            // 
            this.fldCode.CanShrink = true;
            this.fldCode.Height = 0.1875F;
            this.fldCode.Left = 6.21875F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode.Text = "Field1";
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.375F;
            // 
            // fldAmount
            // 
            this.fldAmount.CanShrink = true;
            this.fldAmount.Height = 0.1875F;
            this.fldAmount.Left = 6.625F;
            this.fldAmount.Name = "fldAmount";
            this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAmount.Text = "Field1";
            this.fldAmount.Top = 0F;
            this.fldAmount.Width = 0.875F;
            // 
            // fldDBA
            // 
            this.fldDBA.CanShrink = true;
            this.fldDBA.Height = 0.1875F;
            this.fldDBA.Left = 0F;
            this.fldDBA.MultiLine = false;
            this.fldDBA.Name = "fldDBA";
            this.fldDBA.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 1";
            this.fldDBA.Text = "Field1";
            this.fldDBA.Top = 0.1875F;
            this.fldDBA.Width = 2.1875F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanShrink = true;
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.lblTitle1,
            this.lblTitle2,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Line1,
            this.Label36});
            this.PageHeader.Height = 1.260417F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.375F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label1.Text = "1099 Report";
            this.Label1.Top = 0F;
            this.Label1.Width = 7.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label7.Text = "Label7";
            this.Label7.Top = 0.1875F;
            this.Label7.Width = 1.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label2.Text = "Label2";
            this.Label2.Top = 0F;
            this.Label2.Width = 1.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 6.1875F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "Label4";
            this.Label4.Top = 0.1875F;
            this.Label4.Width = 1.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 6.1875F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "Label3";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.3125F;
            // 
            // lblTitle1
            // 
            this.lblTitle1.Height = 0.21875F;
            this.lblTitle1.HyperLink = null;
            this.lblTitle1.Left = 0F;
            this.lblTitle1.Name = "lblTitle1";
            this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
            this.lblTitle1.Text = "Expense";
            this.lblTitle1.Top = 0.375F;
            this.lblTitle1.Width = 7.5F;
            // 
            // lblTitle2
            // 
            this.lblTitle2.Height = 0.21875F;
            this.lblTitle2.HyperLink = null;
            this.lblTitle2.Left = 0F;
            this.lblTitle2.Name = "lblTitle2";
            this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
            this.lblTitle2.Text = "Expense";
            this.lblTitle2.Top = 0.59375F;
            this.lblTitle2.Width = 7.5F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 0F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
            this.Label15.Text = "Vendor-----";
            this.Label15.Top = 1.0625F;
            this.Label15.Width = 2.1875F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 2.25F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label16.Text = "Tax ID #";
            this.Label16.Top = 1.0625F;
            this.Label16.Width = 0.75F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 3.0625F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.Label17.Text = "Jrnl";
            this.Label17.Top = 1.0625F;
            this.Label17.Width = 0.375F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 3.5F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.Label18.Text = "Date";
            this.Label18.Top = 1.0625F;
            this.Label18.Width = 0.625F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 4.15625F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label19.Text = "Description---------";
            this.Label19.Top = 1.0625F;
            this.Label19.Width = 2F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 6.21875F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.Label20.Text = "Code";
            this.Label20.Top = 1.0625F;
            this.Label20.Width = 0.375F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 6.625F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.Label21.Text = "Amount";
            this.Label21.Top = 1.0625F;
            this.Label21.Width = 0.84375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.25F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 1.25F;
            this.Line1.Y2 = 1.25F;
            // 
            // Label36
            // 
            this.Label36.Height = 0.1875F;
            this.Label36.HyperLink = null;
            this.Label36.Left = 0F;
            this.Label36.Name = "Label36";
            this.Label36.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Label36.Text = "* = 1099 Required";
            this.Label36.Top = 0.84375F;
            this.Label36.Width = 7.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader6
            // 
            this.GroupHeader6.Height = 0F;
            this.GroupHeader6.Name = "GroupHeader6";
            // 
            // GroupFooter6
            // 
            this.GroupFooter6.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.subVendorErrors});
            this.GroupFooter6.Height = 0.1666667F;
            this.GroupFooter6.Name = "GroupFooter6";
            this.GroupFooter6.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.GroupFooter6.Format += new System.EventHandler(this.GroupFooter6_Format);
            // 
            // subVendorErrors
            // 
            this.subVendorErrors.CloseBorder = false;
            this.subVendorErrors.Height = 0.19F;
            this.subVendorErrors.Left = 0F;
            this.subVendorErrors.Name = "subVendorErrors";
            this.subVendorErrors.Report = null;
            this.subVendorErrors.Top = 0F;
            this.subVendorErrors.Width = 7.5F;
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Height = 0F;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // GroupFooter4
            // 
            this.GroupFooter4.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label23,
            this.Line3,
            this.Label24,
            this.Label25,
            this.Label26,
            this.Label27,
            this.Label28,
            this.Label29,
            this.Label30,
            this.Label31,
            this.Label33,
            this.Label34,
            this.Label35,
            this.fldFederalID,
            this.fldStateID,
            this.fldMunicipality,
            this.fldAddress1Info,
            this.fldAddress2Info,
            this.fldCity,
            this.fldState,
            this.fldZip,
            this.fldContact,
            this.fldPhone,
            this.fldFilingElectronically});
            this.GroupFooter4.Height = 2.84375F;
            this.GroupFooter4.Name = "GroupFooter4";
            this.GroupFooter4.Format += new System.EventHandler(this.GroupFooter4_Format);
            // 
            // Label23
            // 
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = null;
            this.Label23.Left = 3.03125F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.Label23.Text = "Town Information";
            this.Label23.Top = 0.3125F;
            this.Label23.Width = 1.46875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 2.46875F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.5F;
            this.Line3.Width = 2.53125F;
            this.Line3.X1 = 2.46875F;
            this.Line3.X2 = 5F;
            this.Line3.Y1 = 0.5F;
            this.Line3.Y2 = 0.5F;
            // 
            // Label24
            // 
            this.Label24.Height = 0.1875F;
            this.Label24.HyperLink = null;
            this.Label24.Left = 2.5F;
            this.Label24.Name = "Label24";
            this.Label24.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label24.Text = "Federal ID";
            this.Label24.Top = 0.53125F;
            this.Label24.Width = 1.3125F;
            // 
            // Label25
            // 
            this.Label25.Height = 0.1875F;
            this.Label25.HyperLink = null;
            this.Label25.Left = 2.5F;
            this.Label25.Name = "Label25";
            this.Label25.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label25.Text = "State ID Code";
            this.Label25.Top = 0.71875F;
            this.Label25.Width = 1.3125F;
            // 
            // Label26
            // 
            this.Label26.Height = 0.1875F;
            this.Label26.HyperLink = null;
            this.Label26.Left = 2.5F;
            this.Label26.Name = "Label26";
            this.Label26.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label26.Text = "Municipality";
            this.Label26.Top = 0.90625F;
            this.Label26.Width = 1.3125F;
            // 
            // Label27
            // 
            this.Label27.Height = 0.1875F;
            this.Label27.HyperLink = null;
            this.Label27.Left = 2.5F;
            this.Label27.Name = "Label27";
            this.Label27.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label27.Text = "Address 1";
            this.Label27.Top = 1.09375F;
            this.Label27.Width = 1.3125F;
            // 
            // Label28
            // 
            this.Label28.Height = 0.1875F;
            this.Label28.HyperLink = null;
            this.Label28.Left = 2.5F;
            this.Label28.Name = "Label28";
            this.Label28.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label28.Text = "Address 2";
            this.Label28.Top = 1.28125F;
            this.Label28.Width = 1.3125F;
            // 
            // Label29
            // 
            this.Label29.Height = 0.1875F;
            this.Label29.HyperLink = null;
            this.Label29.Left = 2.5F;
            this.Label29.Name = "Label29";
            this.Label29.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label29.Text = "City";
            this.Label29.Top = 1.46875F;
            this.Label29.Width = 1.3125F;
            // 
            // Label30
            // 
            this.Label30.Height = 0.1875F;
            this.Label30.HyperLink = null;
            this.Label30.Left = 2.5F;
            this.Label30.Name = "Label30";
            this.Label30.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label30.Text = "State";
            this.Label30.Top = 1.65625F;
            this.Label30.Width = 1.3125F;
            // 
            // Label31
            // 
            this.Label31.Height = 0.1875F;
            this.Label31.HyperLink = null;
            this.Label31.Left = 2.5F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label31.Text = "Zip";
            this.Label31.Top = 1.84375F;
            this.Label31.Width = 1.3125F;
            // 
            // Label33
            // 
            this.Label33.Height = 0.1875F;
            this.Label33.HyperLink = null;
            this.Label33.Left = 2.5F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label33.Text = "Contact";
            this.Label33.Top = 2.03125F;
            this.Label33.Width = 1.3125F;
            // 
            // Label34
            // 
            this.Label34.Height = 0.1875F;
            this.Label34.HyperLink = null;
            this.Label34.Left = 2.5F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label34.Text = "Phone";
            this.Label34.Top = 2.21875F;
            this.Label34.Width = 1.3125F;
            // 
            // Label35
            // 
            this.Label35.Height = 0.1875F;
            this.Label35.HyperLink = null;
            this.Label35.Left = 2.5F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.Label35.Text = "File Electronically";
            this.Label35.Top = 2.40625F;
            this.Label35.Width = 1.3125F;
            // 
            // fldFederalID
            // 
            this.fldFederalID.Height = 0.1875F;
            this.fldFederalID.Left = 3.90625F;
            this.fldFederalID.Name = "fldFederalID";
            this.fldFederalID.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldFederalID.Text = "Field2";
            this.fldFederalID.Top = 0.53125F;
            this.fldFederalID.Width = 2.59375F;
            // 
            // fldStateID
            // 
            this.fldStateID.Height = 0.1875F;
            this.fldStateID.Left = 3.90625F;
            this.fldStateID.Name = "fldStateID";
            this.fldStateID.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldStateID.Text = "Field2";
            this.fldStateID.Top = 0.71875F;
            this.fldStateID.Width = 2.59375F;
            // 
            // fldMunicipality
            // 
            this.fldMunicipality.Height = 0.1875F;
            this.fldMunicipality.Left = 3.90625F;
            this.fldMunicipality.Name = "fldMunicipality";
            this.fldMunicipality.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldMunicipality.Text = "Field2";
            this.fldMunicipality.Top = 0.90625F;
            this.fldMunicipality.Width = 2.59375F;
            // 
            // fldAddress1Info
            // 
            this.fldAddress1Info.Height = 0.1875F;
            this.fldAddress1Info.Left = 3.90625F;
            this.fldAddress1Info.Name = "fldAddress1Info";
            this.fldAddress1Info.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldAddress1Info.Text = "Field2";
            this.fldAddress1Info.Top = 1.09375F;
            this.fldAddress1Info.Width = 2.59375F;
            // 
            // fldAddress2Info
            // 
            this.fldAddress2Info.Height = 0.1875F;
            this.fldAddress2Info.Left = 3.90625F;
            this.fldAddress2Info.Name = "fldAddress2Info";
            this.fldAddress2Info.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldAddress2Info.Text = "Field2";
            this.fldAddress2Info.Top = 1.28125F;
            this.fldAddress2Info.Width = 2.59375F;
            // 
            // fldCity
            // 
            this.fldCity.Height = 0.1875F;
            this.fldCity.Left = 3.90625F;
            this.fldCity.Name = "fldCity";
            this.fldCity.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldCity.Text = "Field2";
            this.fldCity.Top = 1.46875F;
            this.fldCity.Width = 2.59375F;
            // 
            // fldState
            // 
            this.fldState.Height = 0.1875F;
            this.fldState.Left = 3.90625F;
            this.fldState.Name = "fldState";
            this.fldState.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldState.Text = "Field2";
            this.fldState.Top = 1.65625F;
            this.fldState.Width = 2.59375F;
            // 
            // fldZip
            // 
            this.fldZip.Height = 0.1875F;
            this.fldZip.Left = 3.90625F;
            this.fldZip.Name = "fldZip";
            this.fldZip.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldZip.Text = "Field2";
            this.fldZip.Top = 1.84375F;
            this.fldZip.Width = 2.59375F;
            // 
            // fldContact
            // 
            this.fldContact.Height = 0.1875F;
            this.fldContact.Left = 3.90625F;
            this.fldContact.Name = "fldContact";
            this.fldContact.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldContact.Text = "Field2";
            this.fldContact.Top = 2.03125F;
            this.fldContact.Width = 2.59375F;
            // 
            // fldPhone
            // 
            this.fldPhone.Height = 0.1875F;
            this.fldPhone.Left = 3.90625F;
            this.fldPhone.Name = "fldPhone";
            this.fldPhone.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldPhone.Text = "Field2";
            this.fldPhone.Top = 2.21875F;
            this.fldPhone.Width = 2.59375F;
            // 
            // fldFilingElectronically
            // 
            this.fldFilingElectronically.Height = 0.1875F;
            this.fldFilingElectronically.Left = 3.90625F;
            this.fldFilingElectronically.Name = "fldFilingElectronically";
            this.fldFilingElectronically.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.fldFilingElectronically.Text = "Field2";
            this.fldFilingElectronically.Top = 2.40625F;
            this.fldFilingElectronically.Width = 2.59375F;
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.FormTypeBinder,
            this.lblFormTypeLabel});
            this.GroupHeader5.DataField = "FormTypeBinder";
            this.GroupHeader5.Height = 0.4999999F;
            this.GroupHeader5.Name = "GroupHeader5";
            this.GroupHeader5.Format += new System.EventHandler(this.GroupHeader5_Format);
            // 
            // GroupFooter5
            // 
            this.GroupFooter5.CanShrink = true;
            this.GroupFooter5.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.fldTotal,
            this.lblCode1,
            this.lblCode2,
            this.lblCode3,
            this.lblCode4,
            this.lblCode5,
            this.lblCode6,
            this.lblCode7,
            this.lblCode8,
            this.lblCode9,
            this.fldCode1,
            this.fldCode2,
            this.fldCode3,
            this.fldCode4,
            this.fldCode5,
            this.fldCode6,
            this.fldCode7,
            this.fldCode8,
            this.fldCode9,
            this.Label22,
            this.Line2});
            this.GroupFooter5.Height = 2.3125F;
            this.GroupFooter5.KeepTogether = true;
            this.GroupFooter5.Name = "GroupFooter5";
            this.GroupFooter5.Format += new System.EventHandler(this.GroupFooter5_Format);
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 5.65625F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-size: 9pt";
            this.Field1.Text = "TOTAL";
            this.Field1.Top = 0.03125F;
            this.Field1.Width = 0.90625F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.59375F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-size: 9pt; text-align: right";
            this.fldTotal.Text = "Field1";
            this.fldTotal.Top = 0.03125F;
            this.fldTotal.Width = 0.90625F;
            // 
            // lblCode1
            // 
            this.lblCode1.Height = 0.1875F;
            this.lblCode1.HyperLink = null;
            this.lblCode1.Left = 2.125F;
            this.lblCode1.Name = "lblCode1";
            this.lblCode1.Style = "font-size: 9pt";
            this.lblCode1.Text = "Label22";
            this.lblCode1.Top = 0.625F;
            this.lblCode1.Width = 2.28125F;
            // 
            // lblCode2
            // 
            this.lblCode2.Height = 0.1875F;
            this.lblCode2.HyperLink = null;
            this.lblCode2.Left = 2.125F;
            this.lblCode2.Name = "lblCode2";
            this.lblCode2.Style = "font-size: 9pt";
            this.lblCode2.Text = "Label23";
            this.lblCode2.Top = 0.8125F;
            this.lblCode2.Width = 2.28125F;
            // 
            // lblCode3
            // 
            this.lblCode3.Height = 0.1875F;
            this.lblCode3.HyperLink = null;
            this.lblCode3.Left = 2.125F;
            this.lblCode3.Name = "lblCode3";
            this.lblCode3.Style = "font-size: 9pt";
            this.lblCode3.Text = "Label24";
            this.lblCode3.Top = 1F;
            this.lblCode3.Width = 2.28125F;
            // 
            // lblCode4
            // 
            this.lblCode4.Height = 0.1875F;
            this.lblCode4.HyperLink = null;
            this.lblCode4.Left = 2.125F;
            this.lblCode4.Name = "lblCode4";
            this.lblCode4.Style = "font-size: 9pt";
            this.lblCode4.Text = "Label25";
            this.lblCode4.Top = 1.1875F;
            this.lblCode4.Width = 2.28125F;
            // 
            // lblCode5
            // 
            this.lblCode5.Height = 0.1875F;
            this.lblCode5.HyperLink = null;
            this.lblCode5.Left = 2.125F;
            this.lblCode5.Name = "lblCode5";
            this.lblCode5.Style = "font-size: 9pt";
            this.lblCode5.Text = "Label26";
            this.lblCode5.Top = 1.375F;
            this.lblCode5.Width = 2.28125F;
            // 
            // lblCode6
            // 
            this.lblCode6.Height = 0.1875F;
            this.lblCode6.HyperLink = null;
            this.lblCode6.Left = 2.125F;
            this.lblCode6.Name = "lblCode6";
            this.lblCode6.Style = "font-size: 9pt";
            this.lblCode6.Text = "Label27";
            this.lblCode6.Top = 1.5625F;
            this.lblCode6.Width = 2.28125F;
            // 
            // lblCode7
            // 
            this.lblCode7.Height = 0.1875F;
            this.lblCode7.HyperLink = null;
            this.lblCode7.Left = 2.125F;
            this.lblCode7.Name = "lblCode7";
            this.lblCode7.Style = "font-size: 9pt";
            this.lblCode7.Text = "Label28";
            this.lblCode7.Top = 1.75F;
            this.lblCode7.Width = 2.28125F;
            // 
            // lblCode8
            // 
            this.lblCode8.Height = 0.1875F;
            this.lblCode8.HyperLink = null;
            this.lblCode8.Left = 2.125F;
            this.lblCode8.Name = "lblCode8";
            this.lblCode8.Style = "font-size: 9pt";
            this.lblCode8.Text = "Label29";
            this.lblCode8.Top = 1.9375F;
            this.lblCode8.Width = 2.28125F;
            // 
            // lblCode9
            // 
            this.lblCode9.Height = 0.1875F;
            this.lblCode9.HyperLink = null;
            this.lblCode9.Left = 2.125F;
            this.lblCode9.Name = "lblCode9";
            this.lblCode9.Style = "font-size: 9pt";
            this.lblCode9.Text = "Label30";
            this.lblCode9.Top = 2.125F;
            this.lblCode9.Width = 2.28125F;
            // 
            // fldCode1
            // 
            this.fldCode1.CanShrink = true;
            this.fldCode1.Height = 0.1875F;
            this.fldCode1.Left = 4.46875F;
            this.fldCode1.Name = "fldCode1";
            this.fldCode1.Style = "font-size: 9pt; text-align: right";
            this.fldCode1.Text = "Field3";
            this.fldCode1.Top = 0.625F;
            this.fldCode1.Width = 0.9375F;
            // 
            // fldCode2
            // 
            this.fldCode2.CanShrink = true;
            this.fldCode2.Height = 0.1875F;
            this.fldCode2.Left = 4.46875F;
            this.fldCode2.Name = "fldCode2";
            this.fldCode2.Style = "font-size: 9pt; text-align: right";
            this.fldCode2.Text = "Field4";
            this.fldCode2.Top = 0.8125F;
            this.fldCode2.Width = 0.9375F;
            // 
            // fldCode3
            // 
            this.fldCode3.CanShrink = true;
            this.fldCode3.Height = 0.1875F;
            this.fldCode3.Left = 4.46875F;
            this.fldCode3.Name = "fldCode3";
            this.fldCode3.Style = "font-size: 9pt; text-align: right";
            this.fldCode3.Text = "Field5";
            this.fldCode3.Top = 1F;
            this.fldCode3.Width = 0.9375F;
            // 
            // fldCode4
            // 
            this.fldCode4.CanShrink = true;
            this.fldCode4.Height = 0.1875F;
            this.fldCode4.Left = 4.46875F;
            this.fldCode4.Name = "fldCode4";
            this.fldCode4.Style = "font-size: 9pt; text-align: right";
            this.fldCode4.Text = "Field6";
            this.fldCode4.Top = 1.1875F;
            this.fldCode4.Width = 0.9375F;
            // 
            // fldCode5
            // 
            this.fldCode5.CanShrink = true;
            this.fldCode5.Height = 0.1875F;
            this.fldCode5.Left = 4.46875F;
            this.fldCode5.Name = "fldCode5";
            this.fldCode5.Style = "font-size: 9pt; text-align: right";
            this.fldCode5.Text = "Field7";
            this.fldCode5.Top = 1.375F;
            this.fldCode5.Width = 0.9375F;
            // 
            // fldCode6
            // 
            this.fldCode6.CanShrink = true;
            this.fldCode6.Height = 0.1875F;
            this.fldCode6.Left = 4.46875F;
            this.fldCode6.Name = "fldCode6";
            this.fldCode6.Style = "font-size: 9pt; text-align: right";
            this.fldCode6.Text = "Field8";
            this.fldCode6.Top = 1.5625F;
            this.fldCode6.Width = 0.9375F;
            // 
            // fldCode7
            // 
            this.fldCode7.CanShrink = true;
            this.fldCode7.Height = 0.1875F;
            this.fldCode7.Left = 4.46875F;
            this.fldCode7.Name = "fldCode7";
            this.fldCode7.Style = "font-size: 9pt; text-align: right";
            this.fldCode7.Text = "Field9";
            this.fldCode7.Top = 1.75F;
            this.fldCode7.Width = 0.9375F;
            // 
            // fldCode8
            // 
            this.fldCode8.CanShrink = true;
            this.fldCode8.Height = 0.1875F;
            this.fldCode8.Left = 4.46875F;
            this.fldCode8.Name = "fldCode8";
            this.fldCode8.Style = "font-size: 9pt; text-align: right";
            this.fldCode8.Text = "Field10";
            this.fldCode8.Top = 1.9375F;
            this.fldCode8.Width = 0.9375F;
            // 
            // fldCode9
            // 
            this.fldCode9.CanShrink = true;
            this.fldCode9.Height = 0.1875F;
            this.fldCode9.Left = 4.46875F;
            this.fldCode9.Name = "fldCode9";
            this.fldCode9.Style = "font-size: 9pt; text-align: right";
            this.fldCode9.Text = "Field11";
            this.fldCode9.Top = 2.125F;
            this.fldCode9.Width = 0.9375F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.21875F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 3.15625F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-size: 10pt; font-weight: bold; text-align: center";
            this.Label22.Text = "Tax Code Totals";
            this.Label22.Top = 0.34375F;
            this.Label22.Width = 1.21875F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 2.15625F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.59375F;
            this.Line2.Width = 3.25F;
            this.Line2.X1 = 2.15625F;
            this.Line2.X2 = 5.40625F;
            this.Line2.Y1 = 0.59375F;
            this.Line2.Y2 = 0.59375F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.SpaceBinder});
            this.GroupHeader1.DataField = "SpaceBinder";
            this.GroupHeader1.Height = 0.1145833F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // SpaceBinder
            // 
            this.SpaceBinder.DataField = "SpaceBinder";
            this.SpaceBinder.Height = 0.19F;
            this.SpaceBinder.Left = 1.875F;
            this.SpaceBinder.Name = "SpaceBinder";
            this.SpaceBinder.Text = "Field1";
            this.SpaceBinder.Top = 0.0625F;
            this.SpaceBinder.Visible = false;
            this.SpaceBinder.Width = 0.78125F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.CanShrink = true;
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.VendorBinder});
            this.GroupHeader2.DataField = "VendorBinder";
            this.GroupHeader2.Height = 0F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // VendorBinder
            // 
            this.VendorBinder.DataField = "VendorBinder";
            this.VendorBinder.Height = 0.19F;
            this.VendorBinder.Left = 0F;
            this.VendorBinder.Name = "VendorBinder";
            this.VendorBinder.Text = "Field1";
            this.VendorBinder.Top = 0F;
            this.VendorBinder.Visible = false;
            this.VendorBinder.Width = 0.78125F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.CanShrink = true;
            this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldVendorTitle,
            this.fldVendorTotal,
            this.fldCode2Title,
            this.fldCode2GroupTotal,
            this.fldCode1Title,
            this.fldCode1GroupTotal,
            this.fldCode3Title,
            this.fldCode3GroupTotal,
            this.fldCode5Title,
            this.fldCode5GroupTotal,
            this.fldCode4Title,
            this.fldCode4GroupTotal,
            this.fldCode6Title,
            this.fldCode6GroupTotal,
            this.fldCode7Title,
            this.fldCode7GroupTotal,
            this.fldCode8Title,
            this.fldCode8GroupTotal,
            this.fldCode9Title,
            this.fldCode9GroupTotal});
            this.GroupFooter2.Height = 2F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
            // 
            // fldVendorTitle
            // 
            this.fldVendorTitle.CanShrink = true;
            this.fldVendorTitle.Height = 0.1875F;
            this.fldVendorTitle.Left = 5.65625F;
            this.fldVendorTitle.Name = "fldVendorTitle";
            this.fldVendorTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldVendorTitle.Text = "VENDOR";
            this.fldVendorTitle.Top = 0F;
            this.fldVendorTitle.Width = 0.90625F;
            // 
            // fldVendorTotal
            // 
            this.fldVendorTotal.CanShrink = true;
            this.fldVendorTotal.Height = 0.1875F;
            this.fldVendorTotal.Left = 6.59375F;
            this.fldVendorTotal.Name = "fldVendorTotal";
            this.fldVendorTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldVendorTotal.Text = "Field1";
            this.fldVendorTotal.Top = 0F;
            this.fldVendorTotal.Width = 0.90625F;
            // 
            // fldCode2Title
            // 
            this.fldCode2Title.CanShrink = true;
            this.fldCode2Title.Height = 0.1875F;
            this.fldCode2Title.Left = 3.96875F;
            this.fldCode2Title.Name = "fldCode2Title";
            this.fldCode2Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode2Title.Text = "VENDOR";
            this.fldCode2Title.Top = 0.5F;
            this.fldCode2Title.Width = 0.90625F;
            // 
            // fldCode2GroupTotal
            // 
            this.fldCode2GroupTotal.CanShrink = true;
            this.fldCode2GroupTotal.Height = 0.1875F;
            this.fldCode2GroupTotal.Left = 4.90625F;
            this.fldCode2GroupTotal.Name = "fldCode2GroupTotal";
            this.fldCode2GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode2GroupTotal.Text = "Field1";
            this.fldCode2GroupTotal.Top = 0.5F;
            this.fldCode2GroupTotal.Width = 0.90625F;
            // 
            // fldCode1Title
            // 
            this.fldCode1Title.CanShrink = true;
            this.fldCode1Title.Height = 0.1875F;
            this.fldCode1Title.Left = 3.96875F;
            this.fldCode1Title.Name = "fldCode1Title";
            this.fldCode1Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode1Title.Text = "VENDOR";
            this.fldCode1Title.Top = 0.3125F;
            this.fldCode1Title.Width = 0.90625F;
            // 
            // fldCode1GroupTotal
            // 
            this.fldCode1GroupTotal.CanShrink = true;
            this.fldCode1GroupTotal.Height = 0.1875F;
            this.fldCode1GroupTotal.Left = 4.90625F;
            this.fldCode1GroupTotal.Name = "fldCode1GroupTotal";
            this.fldCode1GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode1GroupTotal.Text = "Field1";
            this.fldCode1GroupTotal.Top = 0.3125F;
            this.fldCode1GroupTotal.Width = 0.90625F;
            // 
            // fldCode3Title
            // 
            this.fldCode3Title.CanShrink = true;
            this.fldCode3Title.Height = 0.1875F;
            this.fldCode3Title.Left = 3.96875F;
            this.fldCode3Title.Name = "fldCode3Title";
            this.fldCode3Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode3Title.Text = "VENDOR";
            this.fldCode3Title.Top = 0.6875F;
            this.fldCode3Title.Width = 0.90625F;
            // 
            // fldCode3GroupTotal
            // 
            this.fldCode3GroupTotal.CanShrink = true;
            this.fldCode3GroupTotal.Height = 0.1875F;
            this.fldCode3GroupTotal.Left = 4.90625F;
            this.fldCode3GroupTotal.Name = "fldCode3GroupTotal";
            this.fldCode3GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode3GroupTotal.Text = "Field1";
            this.fldCode3GroupTotal.Top = 0.6875F;
            this.fldCode3GroupTotal.Width = 0.90625F;
            // 
            // fldCode5Title
            // 
            this.fldCode5Title.CanShrink = true;
            this.fldCode5Title.Height = 0.1875F;
            this.fldCode5Title.Left = 3.96875F;
            this.fldCode5Title.Name = "fldCode5Title";
            this.fldCode5Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode5Title.Text = "VENDOR";
            this.fldCode5Title.Top = 1.0625F;
            this.fldCode5Title.Width = 0.90625F;
            // 
            // fldCode5GroupTotal
            // 
            this.fldCode5GroupTotal.CanShrink = true;
            this.fldCode5GroupTotal.Height = 0.1875F;
            this.fldCode5GroupTotal.Left = 4.90625F;
            this.fldCode5GroupTotal.Name = "fldCode5GroupTotal";
            this.fldCode5GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode5GroupTotal.Text = "Field1";
            this.fldCode5GroupTotal.Top = 1.0625F;
            this.fldCode5GroupTotal.Width = 0.90625F;
            // 
            // fldCode4Title
            // 
            this.fldCode4Title.CanShrink = true;
            this.fldCode4Title.Height = 0.1875F;
            this.fldCode4Title.Left = 3.96875F;
            this.fldCode4Title.Name = "fldCode4Title";
            this.fldCode4Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode4Title.Text = "VENDOR";
            this.fldCode4Title.Top = 0.875F;
            this.fldCode4Title.Width = 0.90625F;
            // 
            // fldCode4GroupTotal
            // 
            this.fldCode4GroupTotal.CanShrink = true;
            this.fldCode4GroupTotal.Height = 0.1875F;
            this.fldCode4GroupTotal.Left = 4.90625F;
            this.fldCode4GroupTotal.Name = "fldCode4GroupTotal";
            this.fldCode4GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode4GroupTotal.Text = "Field1";
            this.fldCode4GroupTotal.Top = 0.875F;
            this.fldCode4GroupTotal.Width = 0.90625F;
            // 
            // fldCode6Title
            // 
            this.fldCode6Title.CanShrink = true;
            this.fldCode6Title.Height = 0.1875F;
            this.fldCode6Title.Left = 3.96875F;
            this.fldCode6Title.Name = "fldCode6Title";
            this.fldCode6Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode6Title.Text = "VENDOR";
            this.fldCode6Title.Top = 1.25F;
            this.fldCode6Title.Width = 0.90625F;
            // 
            // fldCode6GroupTotal
            // 
            this.fldCode6GroupTotal.CanShrink = true;
            this.fldCode6GroupTotal.Height = 0.1875F;
            this.fldCode6GroupTotal.Left = 4.90625F;
            this.fldCode6GroupTotal.Name = "fldCode6GroupTotal";
            this.fldCode6GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode6GroupTotal.Text = "Field1";
            this.fldCode6GroupTotal.Top = 1.25F;
            this.fldCode6GroupTotal.Width = 0.90625F;
            // 
            // fldCode7Title
            // 
            this.fldCode7Title.CanShrink = true;
            this.fldCode7Title.Height = 0.1875F;
            this.fldCode7Title.Left = 3.96875F;
            this.fldCode7Title.Name = "fldCode7Title";
            this.fldCode7Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode7Title.Text = "VENDOR";
            this.fldCode7Title.Top = 1.4375F;
            this.fldCode7Title.Width = 0.90625F;
            // 
            // fldCode7GroupTotal
            // 
            this.fldCode7GroupTotal.CanShrink = true;
            this.fldCode7GroupTotal.Height = 0.1875F;
            this.fldCode7GroupTotal.Left = 4.90625F;
            this.fldCode7GroupTotal.Name = "fldCode7GroupTotal";
            this.fldCode7GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode7GroupTotal.Text = "Field1";
            this.fldCode7GroupTotal.Top = 1.4375F;
            this.fldCode7GroupTotal.Width = 0.90625F;
            // 
            // fldCode8Title
            // 
            this.fldCode8Title.CanShrink = true;
            this.fldCode8Title.Height = 0.1875F;
            this.fldCode8Title.Left = 3.96875F;
            this.fldCode8Title.Name = "fldCode8Title";
            this.fldCode8Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode8Title.Text = "VENDOR";
            this.fldCode8Title.Top = 1.625F;
            this.fldCode8Title.Width = 0.90625F;
            // 
            // fldCode8GroupTotal
            // 
            this.fldCode8GroupTotal.CanShrink = true;
            this.fldCode8GroupTotal.Height = 0.1875F;
            this.fldCode8GroupTotal.Left = 4.90625F;
            this.fldCode8GroupTotal.Name = "fldCode8GroupTotal";
            this.fldCode8GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode8GroupTotal.Text = "Field1";
            this.fldCode8GroupTotal.Top = 1.625F;
            this.fldCode8GroupTotal.Width = 0.90625F;
            // 
            // fldCode9Title
            // 
            this.fldCode9Title.CanShrink = true;
            this.fldCode9Title.Height = 0.1875F;
            this.fldCode9Title.Left = 3.96875F;
            this.fldCode9Title.Name = "fldCode9Title";
            this.fldCode9Title.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldCode9Title.Text = "VENDOR";
            this.fldCode9Title.Top = 1.8125F;
            this.fldCode9Title.Width = 0.90625F;
            // 
            // fldCode9GroupTotal
            // 
            this.fldCode9GroupTotal.CanShrink = true;
            this.fldCode9GroupTotal.Height = 0.1875F;
            this.fldCode9GroupTotal.Left = 4.90625F;
            this.fldCode9GroupTotal.Name = "fldCode9GroupTotal";
            this.fldCode9GroupTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCode9GroupTotal.Text = "Field1";
            this.fldCode9GroupTotal.Top = 1.8125F;
            this.fldCode9GroupTotal.Width = 0.90625F;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Height = 0F;
            this.GroupHeader3.Name = "GroupHeader3";
            this.GroupHeader3.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
            this.GroupHeader3.Format += new System.EventHandler(this.GroupHeader3_Format);
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Height = 0F;
            this.GroupFooter3.Name = "GroupFooter3";
            // 
            // FormTypeBinder
            // 
            this.FormTypeBinder.DataField = "FormTypeBinder";
            this.FormTypeBinder.DistinctField = "";
            this.FormTypeBinder.Height = 0.19F;
            this.FormTypeBinder.Left = 6.625F;
            this.FormTypeBinder.Name = "FormTypeBinder";
            this.FormTypeBinder.Style = "color: #000000";
            this.FormTypeBinder.SummaryGroup = "";
            this.FormTypeBinder.Text = "Field1";
            this.FormTypeBinder.Top = 0F;
            this.FormTypeBinder.Visible = false;
            this.FormTypeBinder.Width = 0.78125F;
            // 
            // lblFormTypeLabel
            // 
            this.lblFormTypeLabel.DataField = "";
            this.lblFormTypeLabel.Height = 0.21875F;
            this.lblFormTypeLabel.HyperLink = null;
            this.lblFormTypeLabel.Left = 0.005F;
            this.lblFormTypeLabel.Name = "lblFormTypeLabel";
            this.lblFormTypeLabel.Style = "color: #000000; font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-a" +
    "lign: center; ddo-char-set: 1";
            this.lblFormTypeLabel.Text = "Expense";
            this.lblFormTypeLabel.Top = 0.267F;
            this.lblFormTypeLabel.Width = 7.5F;
            // 
            // rpt1099EditReport
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader6);
            this.Sections.Add(this.GroupHeader4);
            this.Sections.Add(this.GroupHeader5);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.GroupHeader3);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter3);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.GroupFooter5);
            this.Sections.Add(this.GroupFooter4);
            this.Sections.Add(this.GroupFooter6);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDBA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMunicipality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1Info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2Info)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFilingElectronically)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpaceBinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendorBinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode2GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode1GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode3GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode5GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode4GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode6GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode7GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode8GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode9GroupTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormTypeBinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormTypeLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDBA;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader6;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter6;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subVendorErrors;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1Info;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2Info;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCity;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldState;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldContact;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFilingElectronically;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox SpaceBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox VendorBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode2Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode2GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode1Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode1GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode3Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode3GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode5Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode5GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode4Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode4GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode6Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode6GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode7Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode7GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode8Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode8GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode9Title;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode9GroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FormTypeBinder;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFormTypeLabel;
	}
}
