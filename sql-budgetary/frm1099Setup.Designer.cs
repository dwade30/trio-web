﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frm1099Setup.
	/// </summary>
	partial class frm1099Setup : BaseForm
	{
		public fecherFoundation.FCTextBox txtStateId;
		public fecherFoundation.FCTextBox txtFederalID;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtMunicipality;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtExtension;
		public fecherFoundation.FCCheckBox chkFileElectronically;
		public fecherFoundation.FCTextBox txtTCC;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtZip4;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public Global.T2KPhoneNumberBox txtPhone;
		public fecherFoundation.FCGrid vsCodeInfo;
		public Global.T2KPhoneNumberBox txtFax;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblExtension;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm1099Setup));
            this.txtStateId = new fecherFoundation.FCTextBox();
            this.txtFederalID = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtMunicipality = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtContact = new fecherFoundation.FCTextBox();
            this.txtExtension = new fecherFoundation.FCTextBox();
            this.chkFileElectronically = new fecherFoundation.FCCheckBox();
            this.txtTCC = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.dlg1_Save = new fecherFoundation.FCCommonDialog();
            this.dlg1 = new fecherFoundation.FCCommonDialog();
            this.txtPhone = new Global.T2KPhoneNumberBox();
            this.vsCodeInfo = new fecherFoundation.FCGrid();
            this.txtFax = new Global.T2KPhoneNumberBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblExtension = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFileElectronically)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCodeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 479);
            this.BottomPanel.Size = new System.Drawing.Size(994, 94);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.ClientArea.Controls.Add(this.txtStateId);
            this.ClientArea.Controls.Add(this.txtFederalID);
            this.ClientArea.Controls.Add(this.txtAddress2);
            this.ClientArea.Controls.Add(this.txtAddress1);
            this.ClientArea.Controls.Add(this.txtMunicipality);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtContact);
            this.ClientArea.Controls.Add(this.txtExtension);
            this.ClientArea.Controls.Add(this.chkFileElectronically);
            this.ClientArea.Controls.Add(this.txtTCC);
            this.ClientArea.Controls.Add(this.txtEmail);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.txtPhone);
            this.ClientArea.Controls.Add(this.vsCodeInfo);
            this.ClientArea.Controls.Add(this.txtFax);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Controls.Add(this.lblExtension);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.Label11);
            this.ClientArea.Controls.Add(this.Label13);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(1014, 591);
            this.ClientArea.Controls.SetChildIndex(this.Label13, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label11, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblExtension, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label8, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFax, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsCodeInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtPhone, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtZip4, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtEmail, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTCC, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkFileElectronically, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExtension, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtContact, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCity, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtState, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtZip, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMunicipality, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFederalID, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtStateId, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(226, 28);
            this.HeaderText.Text = "Set 1099 Information";
            // 
            // txtStateId
            // 
            this.txtStateId.BackColor = System.Drawing.SystemColors.Window;
            this.txtStateId.Location = new System.Drawing.Point(159, 80);
            this.txtStateId.Name = "txtStateId";
            this.txtStateId.Size = new System.Drawing.Size(200, 40);
            this.txtStateId.TabIndex = 1;
            // 
            // txtFederalID
            // 
            this.txtFederalID.BackColor = System.Drawing.SystemColors.Window;
            this.txtFederalID.Location = new System.Drawing.Point(159, 30);
            this.txtFederalID.MaxLength = 10;
            this.txtFederalID.Name = "txtFederalID";
            this.txtFederalID.Size = new System.Drawing.Size(200, 40);
            this.txtFederalID.TabIndex = 0;
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress2.Location = new System.Drawing.Point(159, 280);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(300, 40);
            this.txtAddress2.TabIndex = 5;
            this.txtAddress2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAddress2_KeyPress);
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAddress1.Location = new System.Drawing.Point(159, 230);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(300, 40);
            this.txtAddress1.TabIndex = 4;
            this.txtAddress1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAddress1_KeyPress);
            // 
            // txtMunicipality
            // 
            this.txtMunicipality.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipality.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtMunicipality.Location = new System.Drawing.Point(159, 180);
            this.txtMunicipality.Name = "txtMunicipality";
            this.txtMunicipality.Size = new System.Drawing.Size(300, 40);
            this.txtMunicipality.TabIndex = 3;
            this.txtMunicipality.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMunicipality_KeyPress);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtZip.Location = new System.Drawing.Point(459, 330);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(90, 40);
            this.txtZip.TabIndex = 8;
            this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(399, 330);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(50, 40);
            this.txtState.TabIndex = 7;
            this.txtState.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtState_KeyPress);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtCity.Location = new System.Drawing.Point(159, 330);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(230, 40);
            this.txtCity.TabIndex = 6;
            this.txtCity.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCity_KeyPress);
            // 
            // txtContact
            // 
            this.txtContact.BackColor = System.Drawing.SystemColors.Window;
            this.txtContact.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtContact.Location = new System.Drawing.Point(688, 30);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(300, 40);
            this.txtContact.TabIndex = 10;
            this.txtContact.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtContact_KeyPress);
            // 
            // txtExtension
            // 
            this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
            this.txtExtension.Location = new System.Drawing.Point(902, 130);
            this.txtExtension.MaxLength = 5;
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(83, 40);
            this.txtExtension.TabIndex = 13;
            // 
            // chkFileElectronically
            // 
            this.chkFileElectronically.AutoSize = false;
            this.chkFileElectronically.Location = new System.Drawing.Point(559, 230);
            this.chkFileElectronically.Name = "chkFileElectronically";
            this.chkFileElectronically.Size = new System.Drawing.Size(168, 26);
            this.chkFileElectronically.TabIndex = 15;
            this.chkFileElectronically.Text = "Filing Electronically";
            // 
            // txtTCC
            // 
            this.txtTCC.BackColor = System.Drawing.SystemColors.Window;
            this.txtTCC.Location = new System.Drawing.Point(159, 130);
            this.txtTCC.MaxLength = 5;
            this.txtTCC.Name = "txtTCC";
            this.txtTCC.Size = new System.Drawing.Size(200, 40);
            this.txtTCC.TabIndex = 2;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(688, 80);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(300, 40);
            this.txtEmail.TabIndex = 11;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtZip4.Location = new System.Drawing.Point(559, 330);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(80, 40);
            this.txtZip4.TabIndex = 9;
            this.txtZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip4_KeyPress);
            // 
            // dlg1_Save
            // 
            this.dlg1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dlg1_Save.FontName = "Microsoft Sans Serif";
            this.dlg1_Save.Name = "dlg1_Save";
            this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
            this.dlg1_Save.TabIndex = 0;
            // 
            // dlg1
            // 
            this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dlg1.FontName = "Microsoft Sans Serif";
            this.dlg1.Name = "dlg1";
            this.dlg1.Size = new System.Drawing.Size(0, 0);
            this.dlg1.TabIndex = 0;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(688, 130);
            this.txtPhone.MaxLength = 13;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(138, 22);
            this.txtPhone.TabIndex = 12;
            // 
            // vsCodeInfo
            // 
            this.vsCodeInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.vsCodeInfo.Cols = 4;
            this.vsCodeInfo.ExtendLastCol = true;
            this.vsCodeInfo.Location = new System.Drawing.Point(30, 441);
            this.vsCodeInfo.Name = "vsCodeInfo";
            this.vsCodeInfo.Rows = 1;
            this.vsCodeInfo.Size = new System.Drawing.Size(955, 38);
            this.vsCodeInfo.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.vsCodeInfo, "To update this information select the 1099 Titles option in the End of Year menu");
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(688, 180);
            this.txtFax.MaxLength = 13;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(138, 22);
            this.txtFax.TabIndex = 14;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 405);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(218, 18);
            this.Label7.TabIndex = 30;
            this.Label7.Text = "1099 CODE SUMMARY";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 344);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(35, 16);
            this.Label6.TabIndex = 29;
            this.Label6.Text = "C/S/Z";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(30, 294);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(70, 16);
            this.Label5.TabIndex = 28;
            this.Label5.Text = "ADDRESS 2";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(30, 244);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(70, 16);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "ADDRESS 1";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 194);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(85, 16);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "MUNICIPALITY";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(90, 16);
            this.Label2.TabIndex = 25;
            this.Label2.Text = "STATE ID CODE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(70, 16);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "FEDERAL ID";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(559, 144);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(68, 16);
            this.Label8.TabIndex = 23;
            this.Label8.Text = "PHONE";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(559, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(85, 16);
            this.Label9.TabIndex = 22;
            this.Label9.Text = "CONTACT";
            // 
            // lblExtension
            // 
            this.lblExtension.Location = new System.Drawing.Point(860, 144);
            this.lblExtension.Name = "lblExtension";
            this.lblExtension.Size = new System.Drawing.Size(25, 16);
            this.lblExtension.TabIndex = 21;
            this.lblExtension.Text = "EXT";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 144);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(30, 16);
            this.Label10.TabIndex = 20;
            this.Label10.Text = "TCC";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(559, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(110, 16);
            this.Label11.TabIndex = 19;
            this.Label11.Text = "EMAIL ADDRESS";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(559, 194);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(27, 16);
            this.Label13.TabIndex = 17;
            this.Label13.Text = "FAX";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(450, 23);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 31;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // frm1099Setup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1014, 651);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frm1099Setup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Set 1099 Information";
            this.Load += new System.EventHandler(this.frm1099Setup_Load);
            this.Activated += new System.EventHandler(this.frm1099Setup_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frm1099Setup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFileElectronically)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCodeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
	}
}
