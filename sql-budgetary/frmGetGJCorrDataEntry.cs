﻿//FeBOsAVEher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJCorrDataEntry.
	/// </summary>
	public partial class frmGetGJCorrDataEntry : BaseForm
	{
		public frmGetGJCorrDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetGJCorrDataEntry InstancePtr
		{
			get
			{
				return (frmGetGJCorrDataEntry)Sys.GetInstance(typeof(frmGetGJCorrDataEntry));
			}
		}

		protected frmGetGJCorrDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		int[] recordNum = null;
		bool EditFlag;
		// vbPorter upgrade warning: lngCheckNumber As int	OnWrite(string)
		int lngCheckNumber;
		// vbPorter upgrade warning: datCheckDate As DateTime	OnWrite(string)
		DateTime datCheckDate;
		// vbPorter upgrade warning: lngJournalNumber As int	OnWrite(string)
		int lngJournalNumber;
		// vbPorter upgrade warning: lngVendorNumber As int	OnWrite(string)
		int lngVendorNumber;
		int SelectCol;
		int CheckCol;
		int VendorCol;
		int AmountCol;
		int PeriodCol;
		clsDRWrapper Master = new clsDRWrapper();
		// vbPorter upgrade warning: intPeriod As short --> As int	OnWrite(short, string)
		int intPeriod;
		int VendorNumber;
		clsDRWrapper rsAPInfo = new clsDRWrapper();
		bool blnJournalLocked;

		public void StartProgram(int Record)
		{
			int counter;
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			if (cmbReturn.SelectedIndex == 0)
			{
				rs.OpenRecordset("SELECT * FROM APJournal WHERE VendorNumber = " + FCConvert.ToString(lngVendorNumber) + " AND JournalNumber = " + FCConvert.ToString(lngJournalNumber) + " AND CheckNumber = '" + FCConvert.ToString(lngCheckNumber) + "' AND CheckDate = '" + FCConvert.ToString(datCheckDate) + "' AND IsNull(Returned, '') <> 'R'");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(Record) + " AND IsNull(Returned, '') <> 'R'");
				// AND Returned <> 'D')")
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				//FC:FINAL:DSE:#524 Close the current form before opening the new one, so that the focus would be on the newly opened form
				Close();
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				if (cmbReturn.SelectedIndex == 0)
				{
					frmReturnCheck.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
					if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
					{
						frmReturnCheck.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
						frmReturnCheck.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
						frmReturnCheck.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
						frmReturnCheck.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
						frmReturnCheck.InstancePtr.txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
						frmReturnCheck.InstancePtr.txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
						frmReturnCheck.InstancePtr.txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
						frmReturnCheck.InstancePtr.txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
					}
					else
					{
						rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
						frmReturnCheck.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
						frmReturnCheck.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
						frmReturnCheck.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
						frmReturnCheck.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
						frmReturnCheck.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
						frmReturnCheck.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
						frmReturnCheck.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
						frmReturnCheck.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						frmReturnCheck.InstancePtr.txtCheck.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rs.Get_Fields("Period")) != "")
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						frmReturnCheck.InstancePtr.txtPeriod.Text = FCConvert.ToString(rs.Get_Fields("Period"));
					do
					{
						//Application.DoEvents();
						frmReturnCheck.InstancePtr.vsInfo.AddItem("");
						frmReturnCheck.InstancePtr.vsInfo.TextMatrix(frmReturnCheck.InstancePtr.vsInfo.Rows - 1, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						frmReturnCheck.InstancePtr.vsInfo.TextMatrix(frmReturnCheck.InstancePtr.vsInfo.Rows - 1, 1, FCConvert.ToString(rs.Get_Fields_String("Description")));
						frmReturnCheck.InstancePtr.vsInfo.TextMatrix(frmReturnCheck.InstancePtr.vsInfo.Rows - 1, 2, FCConvert.ToString(rs.Get_Fields_String("Reference")));
						rs2.OpenRecordset("SELECT SUM(Amount) - SUM(Discount) as TotalAmount FROM APJournalDetail WHERE APJournalID = " + rs.Get_Fields_Int32("ID"));
						frmReturnCheck.InstancePtr.vsInfo.TextMatrix(frmReturnCheck.InstancePtr.vsInfo.Rows - 1, 3, Strings.Format(rs2.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
						rs.MoveNext();
					}
					while (rs.EndOfFile() != true);
					frmReturnCheck.InstancePtr.Show(App.MainForm);
					// show the form
				}
				else
				{
					//FC:FINAL:JEI:IT533 correct wrong index
					if (cmbReturn.SelectedIndex == 2)
					{
						frmGJCorrDataEntry.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
						{
							frmGJCorrDataEntry.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
							frmGJCorrDataEntry.InstancePtr.txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
							frmGJCorrDataEntry.InstancePtr.txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
							frmGJCorrDataEntry.InstancePtr.txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
							frmGJCorrDataEntry.InstancePtr.txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
							frmGJCorrDataEntry.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
							frmGJCorrDataEntry.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
							frmGJCorrDataEntry.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
							frmGJCorrDataEntry.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
							frmGJCorrDataEntry.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
						}
						frmGJCorrDataEntry.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmGJCorrDataEntry.InstancePtr.txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("Period")) != "")
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							frmGJCorrDataEntry.InstancePtr.cboPeriod.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Period")) - 1);
						if (FCConvert.ToString(rs.Get_Fields_String("Reference")) != "")
							frmGJCorrDataEntry.InstancePtr.txtReference.Text = FCConvert.ToString(rs.Get_Fields_String("Reference"));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							frmGJCorrDataEntry.InstancePtr.txtCheck.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
						frmGJCorrDataEntry.InstancePtr.CorrRecordNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
						GetDetails();
						// put detail information into the AP Data Entry form
						frmGJCorrDataEntry.InstancePtr.Show(App.MainForm);
						// show the form
					}
					else if (cmbReturn.SelectedIndex == 3)
					{
						frmSplitAmount.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
						{
							frmSplitAmount.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
							frmSplitAmount.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
							frmSplitAmount.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
							frmSplitAmount.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
							frmSplitAmount.InstancePtr.txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
							frmSplitAmount.InstancePtr.txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
							frmSplitAmount.InstancePtr.txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
							frmSplitAmount.InstancePtr.txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
							frmSplitAmount.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
							frmSplitAmount.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
							frmSplitAmount.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
							frmSplitAmount.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
							frmSplitAmount.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
							frmSplitAmount.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
							frmSplitAmount.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
							frmSplitAmount.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
						}
						frmSplitAmount.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmSplitAmount.InstancePtr.txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("Period")) != "")
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								frmSplitAmount.InstancePtr.cboPeriod.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Period")) - 1);
						if (FCConvert.ToString(rs.Get_Fields_String("Reference")) != "")
							frmSplitAmount.InstancePtr.txtReference.Text = FCConvert.ToString(rs.Get_Fields_String("Reference"));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								frmSplitAmount.InstancePtr.txtCheck.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
						frmSplitAmount.InstancePtr.CorrRecordNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
						GetDetails();
						// put detail information into the AP Data Entry form
						frmSplitAmount.InstancePtr.Show(App.MainForm);
						// show the form
					}
					else
					{
						frmCorrectProject.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
						{
							frmCorrectProject.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
							frmCorrectProject.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
							frmCorrectProject.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
							frmCorrectProject.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
							frmCorrectProject.InstancePtr.txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
							frmCorrectProject.InstancePtr.txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
							frmCorrectProject.InstancePtr.txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
							frmCorrectProject.InstancePtr.txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
							frmCorrectProject.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
							frmCorrectProject.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
							frmCorrectProject.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
							frmCorrectProject.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
							frmCorrectProject.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
							frmCorrectProject.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
							frmCorrectProject.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
							frmCorrectProject.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
						}
						frmCorrectProject.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmCorrectProject.InstancePtr.txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,##0.00");
						if (FCConvert.ToString(rs.Get_Fields_String("Reference")) != "")
							frmCorrectProject.InstancePtr.txtReference.Text = FCConvert.ToString(rs.Get_Fields_String("Reference"));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								frmCorrectProject.InstancePtr.txtCheck.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
						frmCorrectProject.InstancePtr.CorrRecordNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
						GetDetails();
						// put detail information into the AP Data Entry form
						frmCorrectProject.InstancePtr.Show(App.MainForm);
						// show the form
					}
				}
				frmWait.InstancePtr.Unload();
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Account Found", "Non-Existent Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//FC:FINAL:DSE:#524 Close the current form before opening the new one, so that the focus would be on the newly opened form
				Close();
				return;
			}
			// Me.Hide
			//FC:FINAL:DSE:#524 Close the current form before opening the new one, so that the focus would be on the newly opened form
			//Close();
		}

		private void chkCreateNewJournal_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCreateNewJournal.CheckState == CheckState.Checked)
			{
				//fraJournalDescription.Enabled = true;
				txtJournalDesc.Enabled = true;
			}
			else
			{
				//fraJournalDescription.Enabled = false;
				txtJournalDesc.Enabled = false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraReturnMultipleChecks.Visible = false;
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsReturnMultiple.Rows - 1; counter++)
			{
				vsReturnMultiple.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsVendor = new clsDRWrapper();
			clsDRWrapper rsCheck = new clsDRWrapper();
			//FC:FINAL:JEI:IT533 correct wrong index
			if (cmbReturn.SelectedIndex == 2 || cmbReturn.SelectedIndex == 3 || cmbReturn.SelectedIndex == 4)
			{
				rsCheck.OpenRecordset("SELECT * FROM APJournal INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber WHERE Type = 'AC' AND APJournal.Status <> 'P' AND IsNull(Returned, '') <> 'R' AND CheckNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Trim(txtCheck.Text))) + "'");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					MessageBox.Show("There is currently an unposted AP correction journal for this check.  You must post this journal before you may proceed.", "Unposted Correction", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rs.OpenRecordset("SELECT * FROM APJournal WHERE IsNull(CreditMemoRecord, 0) = 0 AND Status = 'P' AND IsNull(Returned, '') <> 'R' AND CheckNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Trim(txtCheck.Text))) + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					if (rs.RecordCount() > 1)
					{
						vs1.Rows = rs.RecordCount() + 1;
						Fill_List();
						Frame3.Visible = true;
						vs1.Focus();
					}
					else
					{
						recordNum = new int[1 + 1];
						recordNum[0] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
						StartProgram(recordNum[0]);
					}
				}
				else
				{
					MessageBox.Show("No posted AP journal entries were found with this check number.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else if (cmbReturn.SelectedIndex == 0)
			{
				rsCheck.OpenRecordset("SELECT * FROM APJournal INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber WHERE Type = 'AC' AND APJournal.Status <> 'P' AND IsNull(Returned, '') <> 'R' AND CheckNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Trim(txtCheck.Text))) + "'");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					MessageBox.Show("There is currently an unposted AP correction journal for this check.  You must post this journal before you may proceed.", "Unposted Correction", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rs.OpenRecordset("SELECT APJournal.JournalNumber AS JournalNumber, CheckNumber, APJournal.CheckDate AS CheckDate, VendorNumber, SUM(APJournalDetail.Amount) - SUM(APJournalDetail.Discount) as TotalAmount FROM ((APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) INNER JOIN JournalMaster ON JournalMaster.JournalNumber = APJournal.JournalNumber) WHERE Type = 'AP' AND APJournal.Status = 'P' AND IsNull(Returned, '') <> 'R' AND CheckNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Trim(txtCheck.Text))) + "' GROUP BY APJournal.JournalNumber, CheckNumber, APJournal.CheckDate, VendorNumber");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					if (rs.RecordCount() > 1)
					{
						vsMultipleChecks.Rows = rs.RecordCount() + 1;
						Fill_Check_List();
						fraMultipleChecks.Visible = true;
						vsMultipleChecks.Focus();
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						lngCheckNumber = FCConvert.ToInt32(rs.Get_Fields("CheckNumber"));
						lngVendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber"));
						datCheckDate = (DateTime)rs.Get_Fields_DateTime("CheckDate");
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournalNumber = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
						StartProgram(0);
					}
				}
				else
				{
					MessageBox.Show("No posted AP journal entries were found for check number " + FCConvert.ToString(Conversion.Val(Strings.Trim(txtCheck.Text))) + ".", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				if (fraReturnMultipleChecks.Visible)
				{
					cmdReturnMultiple_Click();
					return;
				}
				if (!Information.IsDate(txtCheckDate.Text))
				{
					MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Check Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				vsReturnMultiple.Rows = 1;
				rs.OpenRecordset("SELECT VendorNumber, TempVendorName, CheckNumber, APJournal.Period AS Period, SUM(APJournalDetail.Amount) - SUM(APJournalDetail.Discount) as TotalAmount FROM ((APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) INNER JOIN JournalMaster ON JournalMaster.JournalNumber = APJournal.JournalNumber) WHERE JournalMaster.Status = 'P' AND Type = 'AP' AND APJournal.Status = 'P' AND IsNull(Returned, '') <> 'R' AND APJournal.CheckDate = '" + Strings.Trim(txtCheckDate.Text) + "' AND APJournal.Description <> 'Control Entries' GROUP BY CheckNumber, VendorNumber, TempVendorName, APJournal.Period");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					do
					{
						vsReturnMultiple.Rows += 1;
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, SelectCol, FCConvert.ToString(true));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, PeriodCol, FCConvert.ToString(rs.Get_Fields("Period")));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, CheckCol, FCConvert.ToString(rs.Get_Fields("CheckNumber")));
						if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
						{
							vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, VendorCol, Strings.Format(rs.Get_Fields_Int32("VendorNumber"), "00000") + " " + rs.Get_Fields_String("TempVendorName"));
						}
						else
						{
							rsVendor.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
							if (rsVendor.EndOfFile() != true)
							{
								vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, VendorCol, Strings.Format(rs.Get_Fields_Int32("VendorNumber"), "00000") + " " + rsVendor.Get_Fields_String("CheckName"));
							}
							else
							{
								vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, VendorCol, Strings.Format(rs.Get_Fields_Int32("VendorNumber"), "00000") + " UNKNOWN");
							}
						}
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Rows - 1, AmountCol, Strings.Format(rs.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
						rs.MoveNext();
					}
					while (rs.EndOfFile() != true);
					fraReturnMultipleChecks.Visible = true;
				}
				else
				{
					MessageBox.Show("No posted AP journal entries were found with this check date.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdGetCheck_Click(object sender, System.EventArgs e)
		{
			if (vsMultipleChecks.Row > 0)
			{
				lngCheckNumber = FCConvert.ToInt32(txtCheck.Text);
				datCheckDate = FCConvert.ToDateTime(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 2));
				lngVendorNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 1));
				lngJournalNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 0));
				StartProgram(0);
			}
			fraMultipleChecks.Visible = false;
			// make the listbox invisible
		}

		private void cmdQuit_Click()
		{
			Close();
			// unload this form
		}

		private void cmdReturnCheck_Click(object sender, System.EventArgs e)
		{
			vsMultipleChecks.Rows = 1;
			fraMultipleChecks.Visible = false;
			// make the listbox invisible
		}

		private void cmdReturnMultiple_Click()
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			string strTempAcct = "";
			bool blnReturn;
			clsDRWrapper rsCheck = new clsDRWrapper();
			blnReturn = false;
			intPeriod = 0;
			for (counter = 1; counter <= vsReturnMultiple.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsReturnMultiple.TextMatrix(counter, SelectCol)) == true)
				{
					rsCheck.OpenRecordset("SELECT * FROM APJournal INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber WHERE Type = 'AC' AND APJournal.Status <> 'P' AND IsNull(Returned, '') <> 'R' AND CheckNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Trim(vsReturnMultiple.TextMatrix(counter, CheckCol)))) + "'");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						MessageBox.Show("There is currently an unposted AP correction journal for check " + vsReturnMultiple.TextMatrix(counter, CheckCol) + ".  You must post this journal before you may proceed.", "Unposted Correction", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					if (intPeriod == 0)
					{
						intPeriod = FCConvert.ToInt32(vsReturnMultiple.TextMatrix(counter, PeriodCol));
					}
					blnReturn = true;
				}
			}
			if (!blnReturn)
			{
				MessageBox.Show("You must select at least one check to return before you may proceed.", "No Checks Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (modBudgetaryAccounting.LockJournal() == false)
			{
				MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			blnJournalLocked = true;
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
			{
				Master.MoveLast();
				Master.MoveFirst();
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
			}
			else
			{
				TempJournal = 1;
			}
			answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if (answer == DialogResult.Cancel)
			{
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
				return;
			}
			else if (answer == DialogResult.No)
			{
				fraJournalSave.Visible = true;
				lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
				lblJournalSave.Visible = true;
				cboSaveJournal.Visible = true;
				lblJournalDescription.Visible = false;
				txtJournalDescription.Visible = false;
				lblSavePeriod.Visible = false;
				cboSavePeriod.Visible = false;
				cboSaveJournal.Focus();
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
				return;
			}
			else
			{
				fraJournalSave.Visible = true;
				lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
				lblJournalSave.Visible = false;
				cboSaveJournal.Visible = false;
				lblJournalDescription.Visible = true;
				txtJournalDescription.Visible = true;
				lblSavePeriod.Visible = true;
				cboSavePeriod.Visible = true;
				txtJournalDescription.Focus();
				return;
			}
		}

        private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsReturnMultiple.Rows - 1; counter++)
			{
				vsReturnMultiple.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void frmGetGJCorrDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			FillJournalCombo();
			cboSaveJournal.SelectedIndex = 0;
		}

		private void frmGetGJCorrDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				if (frmGetGJCorrDataEntry.InstancePtr.ActiveControl.GetName() == "txtCheck")
				{
					KeyAscii = (Keys)0;
					cmdGetAccountNumber_Click();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				frmGetGJCorrDataEntry.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetGJCorrDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetGJCorrDataEntry.ScaleWidth	= 9045;
			//frmGetGJCorrDataEntry.ScaleHeight	= 7320;
			//frmGetGJCorrDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			int counter;
			blnJournalLocked = false;
			vs1.TextMatrix(0, 0, "Jrnl#");
			vs1.TextMatrix(0, 1, "Vendor#");
			vs1.TextMatrix(0, 2, "Vendor Name");
			vs1.TextMatrix(0, 3, "Description");
			vs1.TextMatrix(0, 4, "Reference");
			vs1.TextMatrix(0, 5, "Amount");
			vs1.ColWidth(0, 800);
			vs1.ColWidth(1, 1000);
			vs1.ColWidth(2, 3500);
			vs1.ColWidth(3, 3000);
			vs1.ColWidth(4, 1630);
			vs1.ColWidth(5, 1500);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, 4);
			vsMultipleChecks.TextMatrix(0, 0, "Journal #");
			vsMultipleChecks.TextMatrix(0, 1, "Vendor #");
			vsMultipleChecks.TextMatrix(0, 2, "Check Date");
			vsMultipleChecks.TextMatrix(0, 3, "Amount");
			vsMultipleChecks.ColWidth(0, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsMultipleChecks.ColWidth(1, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsMultipleChecks.ColWidth(2, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsMultipleChecks.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMultipleChecks.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMultipleChecks.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMultipleChecks.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			SelectCol = 1;
			CheckCol = 2;
			VendorCol = 3;
			AmountCol = 4;
			PeriodCol = 0;
			vsReturnMultiple.ColHidden(PeriodCol, true);
			vsReturnMultiple.TextMatrix(0, CheckCol, "Check #");
			vsReturnMultiple.TextMatrix(0, VendorCol, "Vendor");
			vsReturnMultiple.TextMatrix(0, AmountCol, "Amount");
			vsReturnMultiple.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsReturnMultiple.ColWidth(SelectCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.07));
			vsReturnMultiple.ColWidth(CheckCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.12));
			vsReturnMultiple.ColWidth(VendorCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.6));
			vsReturnMultiple.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsReturnMultiple.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			for (counter = 1; counter <= 12; counter++)
			{
				cboSavePeriod.AddItem(Strings.Format(counter, "00"));
			}
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				//cmbReturn.SelectedIndex = 4; optWrongProject.Enabled = true;
			}
			else
			{
				cmbReturn.Items.RemoveAt(4);
				//optWrongProject.Enabled = false;
			}
			cboSavePeriod.SelectedIndex = DateTime.Today.Month - 1;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
		}

		private void frmGetGJCorrDataEntry_Resize(object sender, System.EventArgs e)
		{
			vsMultipleChecks.ColWidth(0, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsMultipleChecks.ColWidth(1, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsMultipleChecks.ColWidth(2, FCConvert.ToInt32(vsMultipleChecks.WidthOriginal * 0.25));
			vsReturnMultiple.ColWidth(SelectCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.07));
			vsReturnMultiple.ColWidth(CheckCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.12));
			vsReturnMultiple.ColWidth(VendorCol, FCConvert.ToInt32(vsReturnMultiple.WidthOriginal * 0.6));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void optReturn_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbReturn.SelectedIndex == 0)
			{
				lblCheckNumber.Visible = true;
				txtCheck.Visible = true;
				txtCheckDate.Visible = false;
				lblCheckDate.Visible = false;
				txtCheck.Focus();
			}
			else if (cmbReturn.SelectedIndex == 2)
			{
				lblCheckNumber.Visible = true;
				txtCheck.Visible = true;
				txtCheckDate.Visible = false;
				lblCheckDate.Visible = false;
				txtCheck.Focus();
			}
			else if (cmbReturn.SelectedIndex == 1)
			{
				lblCheckNumber.Visible = false;
				txtCheck.Visible = false;
				txtCheckDate.Visible = true;
				lblCheckDate.Visible = true;
				txtCheckDate.Focus();
			}
			else if (cmbReturn.SelectedIndex == 3)
			{
				lblCheckNumber.Visible = true;
				txtCheck.Visible = true;
				txtCheckDate.Visible = false;
				lblCheckDate.Visible = false;
				txtCheck.Focus();
			}
			else if (cmbReturn.SelectedIndex == 4)
			{
				lblCheckNumber.Visible = true;
				txtCheck.Visible = true;
				txtCheckDate.Visible = false;
				lblCheckDate.Visible = false;
				txtCheck.Focus();
			}
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount;
			// if there is a record selected
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				StartProgram(recordNum[vs1.Row - 1]);
			}
			Frame3.Visible = false;
			// make the listbox invisible
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					StartProgram(recordNum[vs1.Row - 1]);
				}
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vsMultipleChecks_DblClick(object sender, System.EventArgs e)
		{
			if (vsMultipleChecks.Row > 0)
			{
				lngCheckNumber = FCConvert.ToInt32(txtCheck.Text);
				datCheckDate = FCConvert.ToDateTime(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 2));
				lngVendorNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 1));
				lngJournalNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 0));
				StartProgram(0);
			}
			fraMultipleChecks.Visible = false;
			// make the listbox invisible
		}

		private void vsMultipleChecks_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				if (vsMultipleChecks.Row > 0)
				{
					lngCheckNumber = FCConvert.ToInt32(txtCheck.Text);
					datCheckDate = FCConvert.ToDateTime(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 2));
					lngVendorNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 1));
					lngJournalNumber = FCConvert.ToInt32(vsMultipleChecks.TextMatrix(vsMultipleChecks.Row, 0));
					StartProgram(0);
				}
				fraMultipleChecks.Visible = false;
				// make the listbox invisible
			}
		}

		private void Fill_List()
		{
			int I;
			recordNum = new int[rs.RecordCount() + 1];
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				recordNum[I - 1] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) != 0)
				{
					rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("VendorNumber"))));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vs1.TextMatrix(I, 2, FCConvert.ToString(rs2.Get_Fields_String("CheckName")));
					vs1.TextMatrix(I, 3, FCConvert.ToString(rs.Get_Fields_String("Description")));
					vs1.TextMatrix(I, 4, FCConvert.ToString(rs.Get_Fields_String("Reference")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, 5, Strings.Format(rs.Get_Fields("Amount"), "#,##0.00"));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vs1.TextMatrix(I, 2, FCConvert.ToString(rs.Get_Fields_String("TempVendorName")));
					vs1.TextMatrix(I, 3, FCConvert.ToString(rs.Get_Fields_String("Description")));
					vs1.TextMatrix(I, 4, FCConvert.ToString(rs.Get_Fields_String("Reference")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, 5, Strings.Format(rs.Get_Fields("Amount"), "#,##0.00"));
				}
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
		}

		private void Fill_Check_List()
		{
			int I;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				vsMultipleChecks.TextMatrix(I, 0, Strings.Format(rs.Get_Fields("JournalNumber"), "0000"));
				vsMultipleChecks.TextMatrix(I, 1, Strings.Format(rs.Get_Fields_Int32("VendorNumber"), "00000"));
				vsMultipleChecks.TextMatrix(I, 2, Strings.Format(rs.Get_Fields_DateTime("CheckDate"), "MM/dd/yyyy"));
				vsMultipleChecks.TextMatrix(I, 3, Strings.Format(rs.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
		}

		private void GetDetails()
		{
			int counter = 0;
			int counter2;
			int ACounter;
			rs2.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				//FC:FINAL:JEI:IT533 correct wrong index
				if (cmbReturn.SelectedIndex == 2)
				{
					frmGJCorrDataEntry.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
					counter = 1;
					ACounter = 0;
					while (rs2.EndOfFile() != true)
					{
						// get all the default information there is
						//Application.DoEvents();
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToBoolean(rs2.Get_Fields_Boolean("Corrected")) && rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("CorrectedAmount") == 0)
						{
							frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, "T");
							frmGJCorrDataEntry.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, counter, frmGJCorrDataEntry.InstancePtr.vs1.Cols - 1, 0x909090);
						}
						else
						{
							frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, "F");
						}
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields_String("Project")));
						// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields("1099")));
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields("account")));
						//FC:FINAL:ASZ: no need to convert decimal field with Val
						//frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Amount") + Conversion.Val(rs2.Get_Fields("CorrectedAmount")))));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("CorrectedAmount")));
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields("Discount")));
						frmGJCorrDataEntry.InstancePtr.vs1.TextMatrix(counter, 8, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
						rs2.MoveNext();
						counter += 1;
					}
				}
				else if (cmbReturn.SelectedIndex == 3)
				{
					frmSplitAmount.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
					counter = 1;
					ACounter = 0;
					while (rs2.EndOfFile() != true)
					{
						// get all the default information there is
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToBoolean(rs2.Get_Fields_Boolean("Corrected")) && Conversion.Val(rs2.Get_Fields("Amount")) + rs2.Get_Fields_Decimal("CorrectedAmount") == 0)
						{
							frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 0, "T");
							frmSplitAmount.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, counter, frmSplitAmount.InstancePtr.vs1.Cols - 1, 0x909090);
						}
						else
						{
							frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 0, "F");
						}
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields_String("Project")));
						// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields("1099")));
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields("account")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("CorrectedAmount")));
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields("Discount")));
						frmSplitAmount.InstancePtr.vs1.TextMatrix(counter, 8, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
						rs2.MoveNext();
						counter += 1;
					}
				}
				else
				{
					frmCorrectProject.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
					counter = 1;
					ACounter = 0;
					while (rs2.EndOfFile() != true)
					{
						// get all the default information there is
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToBoolean(rs2.Get_Fields_Boolean("Corrected")) && rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("CorrectedAmount") == 0)
						{
							frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 0, "T");
							frmCorrectProject.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, 0, counter, frmCorrectProject.InstancePtr.vs1.Cols - 1, 0x909090);
						}
						else
						{
							frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 0, "F");
						}
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields_String("Project")));
						// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("1099")));
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("account")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("CorrectedAmount")));
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields("Discount")));
						frmCorrectProject.InstancePtr.vs1.TextMatrix(counter, 8, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
						rs2.MoveNext();
						counter += 1;
					}
				}
			}
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the account number of that record
			if (intAccount != 0)
			{
				// if there is a valid account number
				StartProgram(recordNum[vs1.Row - 1]);
			}
			Frame3.Visible = false;
			// make the list of records invisible
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			vs1.Clear();
			Frame3.Visible = false;
			// make the listbox invisible
		}

		private void vsReturnMultiple_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsReturnMultiple.Row > 0)
			{
				if (FCUtils.CBool(vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol)) == false)
				{
					vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsReturnMultiple_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsReturnMultiple.Row > 0)
				{
					KeyCode = 0;
					if (FCUtils.CBool(vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol)) == false)
					{
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsReturnMultiple.TextMatrix(vsReturnMultiple.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			if (cboSavePeriod.Visible == true)
			{
				intPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(cboSavePeriod.Text)));
			}
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.Width = 225;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsEncData = new clsDRWrapper();
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			clsDRWrapper rsCreditData = new clsDRWrapper();
			clsDRWrapper rsNewJournal = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsNewDetailInfo = new clsDRWrapper();
			int lngJournalNumber = 0;
			clsDRWrapper rsCorrRecords = new clsDRWrapper();
			if (vsReturnMultiple.Row > 1)
			{
				vsReturnMultiple.Row = 1;
			}
			else
			{
				vsReturnMultiple.Row = 0;
				vsReturnMultiple.Row = 1;
			}
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("Period", intPeriod);
				Master.Update();
				if (chkCreateNewJournal.CheckState == CheckState.Checked)
				{
					lngJournalNumber = TempJournal + 1;
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournalNumber);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					//Master.Set_Fields("Description", txtJournalDesc);
					// FC:FINAL:VGE - #863 Using Text property instead of field itself.
					Master.Set_Fields("Description", txtJournalDesc.Text);
					Master.Set_Fields("Type", "AP");
					Master.Set_Fields("Period", intPeriod);
					Master.Update();
				}
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				if (chkCreateNewJournal.CheckState == CheckState.Checked)
				{
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						lngJournalNumber = 1;
					}
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournalNumber);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					//Master.Set_Fields("Description", txtJournalDesc);
					// FC:FINAL:VGE - #863 Using Text property instead of field itself.
					Master.Set_Fields("Description", txtJournalDesc.Text);
					Master.Set_Fields("Type", "AP");
					Master.Set_Fields("Period", intPeriod);
					Master.Update();
					Master.Reset();
					modBudgetaryAccounting.UnlockJournal();
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(Master.Get_Fields("Period")) != intPeriod)
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(intPeriod));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							for (counter = 1; counter <= cboSaveJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2)) == intPeriod)
								{
									cboSaveJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboSaveJournal.SelectedIndex = 0;
							cmdReturnMultiple_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "")
							{
								executeAddTag = true;
								goto AddTag;
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								lblSavePeriod.Visible = true;
								cboSavePeriod.Visible = true;
								txtJournalDescription.Focus();
								return;
							}
						}
						else
						{
							cboSaveJournal.SelectedIndex = 0;
							cmdReturnMultiple_Click();
							return;
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.AddNew();
					Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("Description", txtJournalDescription.Text);
					Master.Set_Fields("Type", "AC");
					Master.Set_Fields("Period", intPeriod);
					Master.Update();
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
			if (chkCreateNewJournal.CheckState == CheckState.Checked)
			{
				rsNewJournal.OpenRecordset("SELECT * FROM APJournal");
			}
			rsNewJournal.OmitNullsOnInsert = true;
			rsNewDetailInfo.OmitNullsOnInsert = true;
			for (counter = 1; counter <= vsReturnMultiple.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsReturnMultiple.TextMatrix(counter, SelectCol)) == true)
				{
					rsAPInfo.OpenRecordset("SELECT * FROM APJournal WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(vsReturnMultiple.TextMatrix(counter, VendorCol), 5))) + " AND CheckNumber = '" + vsReturnMultiple.TextMatrix(counter, CheckCol) + "' AND CheckDate = '" + txtCheckDate.Text + "'");
					if (rsAPInfo.EndOfFile() != true)
					{
						do
						{
							if (chkCreateNewJournal.CheckState == CheckState.Checked)
							{
								rsNewJournal.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
								if (Conversion.Val(rsAPInfo.Get_Fields_Int32("CreditMemoRecord") + "") == 0)
								{
									rsNewJournal.AddNew();
									rsNewJournal.Set_Fields("JournalNumber", lngJournalNumber);
									rsNewJournal.Set_Fields("VendorNumber", rsAPInfo.Get_Fields_Int32("VendorNumber"));
									rsNewJournal.Set_Fields("TempVendorName", rsAPInfo.Get_Fields_String("TempVendorName"));
									rsNewJournal.Set_Fields("TempVendorAddress1", rsAPInfo.Get_Fields_String("TempVendorAddress1"));
									rsNewJournal.Set_Fields("TempVendorAddress2", rsAPInfo.Get_Fields_String("TempVendorAddress2"));
									rsNewJournal.Set_Fields("TempVendorAddress3", rsAPInfo.Get_Fields_String("TempVendorAddress3"));
									rsNewJournal.Set_Fields("TempVendorCity", rsAPInfo.Get_Fields_String("TempVendorCity"));
									rsNewJournal.Set_Fields("TempVendorState", rsAPInfo.Get_Fields_String("TempVendorState"));
									rsNewJournal.Set_Fields("TempVendorZip", rsAPInfo.Get_Fields_String("TempVendorZip"));
									rsNewJournal.Set_Fields("TempVendorZip4", rsAPInfo.Get_Fields_String("TempVendorZip4"));
									rsNewJournal.Set_Fields("Period", intPeriod);
									if (Information.IsDate(rsAPInfo.Get_Fields("Payable")))
									{
										rsNewJournal.Set_Fields("Payable", rsAPInfo.Get_Fields_DateTime("Payable"));
									}
									// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
									rsNewJournal.Set_Fields("Frequency", rsAPInfo.Get_Fields("Frequency"));
									rsNewJournal.Set_Fields("Seperate", rsAPInfo.Get_Fields_Boolean("Seperate"));
									rsNewJournal.Set_Fields("Description", rsAPInfo.Get_Fields_String("Description"));
									rsNewJournal.Set_Fields("Reference", rsAPInfo.Get_Fields_String("Reference"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									rsNewJournal.Set_Fields("Amount", rsAPInfo.Get_Fields("Amount"));
									if (rsAPInfo.Get_Fields_Boolean("PrepaidCheck") == false)
									{
										rsNewJournal.Set_Fields("CheckNumber", "");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
										rsNewJournal.Set_Fields("CheckNumber", rsAPInfo.Get_Fields("CheckNumber"));
										rsNewJournal.Set_Fields("PrepaidCheck", rsAPInfo.Get_Fields_Boolean("PrepaidCheck"));
									}
									rsNewJournal.Set_Fields("Status", "E");
									rsNewJournal.Set_Fields("PostedDate", null);
									rsNewJournal.Set_Fields("Warrant", "");
									rsNewJournal.Set_Fields("CheckDate", null);
									rsNewJournal.Set_Fields("Returned", "N");
									rsNewJournal.Set_Fields("TotalEncumbrance", 0);
									rsNewJournal.Set_Fields("EncumbranceRecord", 0);
									rsNewJournal.Update(true);
									rsNewDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
									rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsAPInfo.Get_Fields_Int32("ID"));
									if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
									{
										do
										{
											rsNewDetailInfo.AddNew();
											rsNewDetailInfo.Set_Fields("APJournalID", rsNewJournal.Get_Fields_Int32("ID"));
											rsNewDetailInfo.Set_Fields("Description", rsDetailInfo.Get_Fields_String("Description"));
											// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
											rsNewDetailInfo.Set_Fields("Account", rsDetailInfo.Get_Fields("Account"));
											// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
											rsNewDetailInfo.Set_Fields("Amount", rsDetailInfo.Get_Fields("Amount"));
											rsNewDetailInfo.Set_Fields("Project", rsDetailInfo.Get_Fields_String("Project"));
											// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
											rsNewDetailInfo.Set_Fields("Discount", rsDetailInfo.Get_Fields("Discount"));
											rsNewDetailInfo.Set_Fields("Encumbrance", 0);
											// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
											rsNewDetailInfo.Set_Fields("1099", rsDetailInfo.Get_Fields("1099"));
											rsNewDetailInfo.Set_Fields("RCB", "R");
											rsNewDetailInfo.Set_Fields("Corrected", false);
											rsNewDetailInfo.Set_Fields("EncumbranceDetailRecord", 0);
											rsNewDetailInfo.Update(true);
											rsDetailInfo.MoveNext();
										}
										while (rsDetailInfo.EndOfFile() != true);
									}
								}
							}
							rs.OmitNullsOnInsert = true;
							rs.AddNew();
							if (cboSaveJournal.SelectedIndex != 0)
							{
								rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
							}
							else
							{
								rs.Set_Fields("JournalNumber", TempJournal);
							}
							rs.Set_Fields("VendorNumber", rsAPInfo.Get_Fields_Int32("VendorNumber"));
							if (FCConvert.ToInt32(rsAPInfo.Get_Fields_Int32("VendorNumber")) == 0)
							{
								rs.Set_Fields("TempVendorName", rsAPInfo.Get_Fields_String("TempVendorName"));
								rs.Set_Fields("TempVendorAddress1", rsAPInfo.Get_Fields_String("TempVendorAddress1"));
								rs.Set_Fields("TempVendorAddress2", rsAPInfo.Get_Fields_String("TempVendorAddress2"));
								rs.Set_Fields("TempVendorAddress3", rsAPInfo.Get_Fields_String("TempVendorAddress3"));
								rs.Set_Fields("TempVendorCity", rsAPInfo.Get_Fields_String("TempVendorCity"));
								rs.Set_Fields("TempVendorState", rsAPInfo.Get_Fields_String("TempVendorState"));
								rs.Set_Fields("TempVendorZip", rsAPInfo.Get_Fields_String("TempVendorZip"));
								rs.Set_Fields("TempVendorZip4", rsAPInfo.Get_Fields_String("TempVendorZip4"));
							}
							rs.Set_Fields("Period", intPeriod);
							rs.Set_Fields("Description", rsAPInfo.Get_Fields_String("Description"));
							rs.Set_Fields("Reference", rsAPInfo.Get_Fields_String("Reference"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							rs.Set_Fields("Amount", rsAPInfo.Get_Fields("Amount") * -1);
							rs.Set_Fields("Returned", "D");
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							rs.Set_Fields("CheckNumber", rsAPInfo.Get_Fields("CheckNumber"));
							rs.Set_Fields("CheckDate", DateTime.Today);
							rs.Set_Fields("Status", "E");
							rs.Set_Fields("EncumbranceRecord", Conversion.Val(rsAPInfo.Get_Fields_Int32("EncumbranceRecord")));
							rs.Set_Fields("UseAlternateCash", rsAPInfo.Get_Fields_Boolean("UseAlternateCash"));
							rs.Set_Fields("AlternateCashAccount", rsAPInfo.Get_Fields_String("AlternateCashAccount"));
							// return credit memo money
							if (Conversion.Val(rsAPInfo.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
							{
								rsCreditData.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + rsAPInfo.Get_Fields_Int32("CreditMemoRecord"));
								if (rsCreditData.EndOfFile() != true && rsCreditData.BeginningOfFile() != true)
								{
									rsCreditData.Edit();
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									rsCreditData.Set_Fields("Liquidated", rsCreditData.Get_Fields_Decimal("Liquidated") + rsAPInfo.Get_Fields("Amount"));
									rsCreditData.Update();
								}
							}
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							rsCorrRecords.OpenRecordset("SELECT * FROM APJournal WHERE CheckNumber = '" + rsAPInfo.Get_Fields("CheckNumber") + "' AND VendorNumber = " + rsAPInfo.Get_Fields_Int32("VendorNumber") + " AND Period = " + rsAPInfo.Get_Fields("Period"));
							if (rsCorrRecords.EndOfFile() != true && rsCorrRecords.BeginningOfFile() != true)
							{
								do
								{
									rsCorrRecords.Edit();
									rsCorrRecords.Set_Fields("Returned", "R");
									rsCorrRecords.Update();
									rsCorrRecords.MoveNext();
								}
								while (rsCorrRecords.EndOfFile() != true);
							}
							rsAPInfo.Edit();
							rsAPInfo.Set_Fields("Returned", "R");
							rsAPInfo.Update();
							rs.Update();
							VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
							SaveDetails(ref counter);
							// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
							rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE WarrantNumber = " + FCConvert.ToString(Conversion.Val(rsAPInfo.Get_Fields("Warrant"))) + " AND CheckNumber = " + vsReturnMultiple.TextMatrix(counter, CheckCol));
							if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
							{
								rsCheckInfo.Edit();
								rsCheckInfo.Set_Fields("Status", "V");
								rsCheckInfo.Update(false);
							}
							rsAPInfo.MoveNext();
						}
						while (rsAPInfo.EndOfFile() != true);
						rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + vsReturnMultiple.TextMatrix(counter, CheckCol));
						if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
						{
							rsCheckInfo.Edit();
							rsCheckInfo.Set_Fields("Status", "V");
							rsCheckInfo.Update(false);
						}
					}
				}
			}
			modBudgetaryAccounting.UpdateCalculationNeededTable(0, true);
			if (chkCreateNewJournal.CheckState == CheckState.Checked)
			{
				MessageBox.Show("Save Successful.  Your new entries have been saved under journal " + FCConvert.ToString(lngJournalNumber), "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Save Successful", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			Close();
			//MDIParent.InstancePtr.GRID.Focus();
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void SaveDetails(ref int intRow)
		{
			int counter;
			Decimal TotalAmount;
			clsDRWrapper rsCorrect = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsEncInfo = new clsDRWrapper();
			rsDetailInfo.OmitNullsOnInsert = true;
			rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
			rsCorrect.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsAPInfo.Get_Fields_Int32("ID"));
			do
			{
				rsDetailInfo.AddNew();
				// if so add it
				rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
				if (("VOID - " + rsCorrect.Get_Fields_String("Description")).Length > 25)
				{
					rsDetailInfo.Set_Fields("Description", "VOID - " + Strings.Left(FCConvert.ToString(rsCorrect.Get_Fields_String("Description")), 18));
				}
				else
				{
					rsDetailInfo.Set_Fields("Description", "VOID - " + rsCorrect.Get_Fields_String("Description"));
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("account", rsCorrect.Get_Fields("Account"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("Amount", (FCConvert.ToDecimal(rsCorrect.Get_Fields("Amount")) * -1));
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("Discount", (FCConvert.ToDecimal(rsCorrect.Get_Fields("Discount")) * -1));
				rsDetailInfo.Set_Fields("Encumbrance", (FCConvert.ToDecimal(rsCorrect.Get_Fields_Decimal("Encumbrance")) * -1));
				rsDetailInfo.Set_Fields("Project", rsCorrect.Get_Fields_String("Project"));
				// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("1099", rsCorrect.Get_Fields("1099"));
				// TextMatrix(counter, TaxCol)
				rsDetailInfo.Set_Fields("RCB", "C");
				if (rsCorrect.EndOfFile() != true && rsCorrect.BeginningOfFile() != true)
				{
					rsDetailInfo.Set_Fields("EncumbranceDetailRecord", FCConvert.ToString(Conversion.Val(rsCorrect.Get_Fields_Int32("EncumbranceDetailRecord"))));
				}
				else
				{
					rsDetailInfo.Set_Fields("EncumbranceDetailRecord", 0);
				}
				rsDetailInfo.Update();
				// update the database
				rsCorrect.MoveNext();
			}
			while (rsCorrect.EndOfFile() != true);
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}
	}
}
