﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmPrintChecks.
    /// </summary>
    public partial class frmPrintChecks : BaseForm
    {
        public frmPrintChecks()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            //FC:FINAL:BBE - default selection - All
            cmbInitial.SelectedIndex = 0;
            cboBanks.SelectedIndex = 0;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmPrintChecks InstancePtr
        {
            get
            {
                return (frmPrintChecks)Sys.GetInstance(typeof(frmPrintChecks));
            }
        }

        protected frmPrintChecks _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By   Dave Wade
        // Date         4/22/02
        // This form will be used by towns to select Unposted AP
        // Journals to print checks for
        // ********************************************************
        int PostCol;
        int JournalCol;
        int JournalDescriptionCol;
        int PeriodCol;
        int AmountCol;
        int CountCol;
        int CheckCountCol;
        const int cnstBankCol = 7;
        double[] ColPercents = new double[5 + 1];
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //clsDRWrapper rsJournals = new clsDRWrapper();
        clsDRWrapper rsJournals_AutoInitialized;

        clsDRWrapper rsJournals
        {
            get
            {
                if (rsJournals_AutoInitialized == null)
                {
                    rsJournals_AutoInitialized = new clsDRWrapper();
                }
                return rsJournals_AutoInitialized;
            }
            set
            {
                rsJournals_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //clsDRWrapper rsAPJournal = new clsDRWrapper();
        clsDRWrapper rsAPJournal_AutoInitialized;

        clsDRWrapper rsAPJournal
        {
            get
            {
                if (rsAPJournal_AutoInitialized == null)
                {
                    rsAPJournal_AutoInitialized = new clsDRWrapper();
                }
                return rsAPJournal_AutoInitialized;
            }
            set
            {
                rsAPJournal_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //clsDRWrapper rsEntries = new clsDRWrapper();
        clsDRWrapper rsEntries_AutoInitialized;

        clsDRWrapper rsEntries
        {
            get
            {
                if (rsEntries_AutoInitialized == null)
                {
                    rsEntries_AutoInitialized = new clsDRWrapper();
                }
                return rsEntries_AutoInitialized;
            }
            set
            {
                rsEntries_AutoInitialized = value;
            }
        }

        public int intPreviewChoice;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //clsDRWrapper Master = new clsDRWrapper();
        clsDRWrapper Master_AutoInitialized;

        clsDRWrapper Master
        {
            get
            {
                if (Master_AutoInitialized == null)
                {
                    Master_AutoInitialized = new clsDRWrapper();
                }
                return Master_AutoInitialized;
            }
            set
            {
                Master_AutoInitialized = value;
            }
        }
        // vbPorter upgrade warning: TempWarrant As int	OnWrite(string, short)
        public int TempWarrant;
        public string strJournalStatus = "";
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //clsDRWrapper rsCheckInfo = new clsDRWrapper();
        clsDRWrapper rsCheckInfo_AutoInitialized;

        clsDRWrapper rsCheckInfo
        {
            get
            {
                if (rsCheckInfo_AutoInitialized == null)
                {
                    rsCheckInfo_AutoInitialized = new clsDRWrapper();
                }
                return rsCheckInfo_AutoInitialized;
            }
            set
            {
                rsCheckInfo_AutoInitialized = value;
            }
        }

        bool blnLockedByMe;
        public bool blnPrintingUnprintedChecks;
        int lngUnprintedJournal;
        bool blnLocked;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBankService bankService = new cBankService();
        private cBankService bankService_AutoInitialized;

        private cBankService bankService
        {
            get
            {
                if (bankService_AutoInitialized == null)
                {
                    bankService_AutoInitialized = new cBankService();
                }
                return bankService_AutoInitialized;
            }
            set
            {
                bankService_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cSettingUtility setUtil = new cSettingUtility();
        private cSettingUtility setUtil_AutoInitialized;

        private cSettingUtility setUtil
        {
            get
            {
                if (setUtil_AutoInitialized == null)
                {
                    setUtil_AutoInitialized = new cSettingUtility();
                }
                return setUtil_AutoInitialized;
            }
            set
            {
                setUtil_AutoInitialized = value;
            }
        }

        private void cboBanks_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int lngId = 0;
            int lngCheckNumber;
            if (cboBanks.SelectedIndex >= 0)
            {
                lngId = cboBanks.ItemData(cboBanks.SelectedIndex);
                if (lngId > 0)
                {
                    UpdateStartingCheckNumber(lngId);
                    for (int x = 1; x < vsJournals.Rows; x++)
                    {
                        if (FCConvert.ToInt32(vsJournals.TextMatrix(x, cnstBankCol)) == 0 ||
                            FCConvert.ToInt32(vsJournals.TextMatrix(x, cnstBankCol)) == lngId)
                        {
                            vsJournals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsJournals.Cols - 1,
                                Color.White);
                        }
                        else
                        {
                            vsJournals.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsJournals.Cols - 1,
                                modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
                            vsJournals.TextMatrix(x, PostCol, false);
                        }
                    }
                }
            }
        }

        private void cmdChoose_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsWarrant = new clsDRWrapper();
            clsDRWrapper rsCustomCheck = new clsDRWrapper();
            int intReturn;
            // vbPorter upgrade warning: lngLastCheck As int	OnWriteFCConvert.ToDouble(
            int lngLastCheck;
            int intChecks;
            int intBank = 0;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsMasterJournal = new clsDRWrapper();
            clsDRWrapper rsBanks = new clsDRWrapper();

            try
            {
                int lngBankID;
                var checkFormat = Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat"))));

                if (checkFormat == 3)
                {
                    MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Strings.Trim(txtCheck.Text) == "")
                {
                    MessageBox.Show("You must enter a check number before you may proceed", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (Strings.Trim(txtWarrant.Text) == "")
                {
                    MessageBox.Show("You must enter a warrant number before you may proceed", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                intChecks = 0;
                for (intReturn = 1; intReturn <= vsJournals.Rows - 1; intReturn++)
                {
                    if (FCUtils.CBool(vsJournals.TextMatrix(intReturn, 0)) == true)
                    {
                        intChecks += FCConvert.ToInt32(vsJournals.TextMatrix(intReturn, CheckCountCol));
                    }
                }
                lngLastCheck = FCConvert.ToInt32(Conversion.Val(txtCheck.Text) + intChecks);
                intBank = fraBanks.Visible == false
                    ? FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank"))))
                    : cboBanks.ItemData(cboBanks.SelectedIndex);
                lngBankID = intBank;
                rsCustomCheck.OpenRecordset("SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(intBank) + " AND CheckNumber >= " + txtCheck.Text + " AND CheckNumber <= " + FCConvert.ToString(lngLastCheck));
                if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
                {
                    // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    ans = MessageBox.Show("Check " + rsCustomCheck.Get_Fields("CheckNumber") + " has previously been used.  Are you sure you wish to print these checks?", "Print Checks?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        return;
                    }
                }
                for (intReturn = 1; intReturn <= vsJournals.Rows - 1; intReturn++)
                {
                    if (FCUtils.CBool(vsJournals.TextMatrix(intReturn, 0)) == true)
                    {
                        rsCustomCheck.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + vsJournals.TextMatrix(intReturn, JournalCol) + " AND (convert(int, IsNull(CheckNumber, 0)) >= " + txtCheck.Text + " AND convert(int, IsNull(CheckNumber, 0)) <= " + FCConvert.ToString(lngLastCheck) + ")");
                        if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            ans = MessageBox.Show("Check " + rsCustomCheck.Get_Fields("CheckNumber") + " has previously been used.  Are you sure you wish to print these checks?", "Print Checks?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.No)
                            {
                                return;
                            }
                        }
                    }
                }
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                //cmdChoose.Enabled = false;
                TempWarrant = FCConvert.ToInt32(Strings.Trim(txtWarrant.Text));
                rsWarrant.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + txtWarrant.Text);
                if (rsWarrant.EndOfFile() != true && rsWarrant.BeginningOfFile() != true)
                {
                    rsWarrant.Edit();
                }
                else
                {
                    rsWarrant.AddNew();
                }
                rsWarrant.Set_Fields("WarrantNumber", Strings.Trim(txtWarrant.Text));
                rsWarrant.Set_Fields("WarrantDate", DateTime.Today);
                rsWarrant.Set_Fields("WarrantTime", DateAndTime.TimeOfDay);
                rsWarrant.Set_Fields("WarrantOperator", modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
                rsWarrant.Update(true);
                modBudgetaryMaster.UnlockWarrant();
                blnLocked = false;

                Information.Err().Clear();
                App.MainForm.CommonDialog1.Flags = 0;
                App.MainForm.CommonDialog1.CancelError = true;

                if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 0)
                {
                    MessageBox.Show("Please insert your checks into the printer and then click OK to continue.", "Insert Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    switch (checkFormat)
                    {
                        case 2:
                            rptLaserChecks.InstancePtr.blnIndividualCheck = false;

                            Hide();
                            FCGlobal.Screen.MousePointer = 0;
                            rptLaserChecks.InstancePtr.Init("", true, lngBankID, true);

                            FCGlobal.Screen.MousePointer = 0;
                            blnPrintingUnprintedChecks = false;
                            MessageBox.Show("Please remove your checks from the printer when they are done printing and click OK to continue.", "Remove Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            break;
                        case 4:
                            {
                                frmSelectCustomCheckFormat.InstancePtr.Init();
                                rsBanks.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(lngBankID), "TWBD0000.vb1");
                                rsCustomCheck.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
                                if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
                                {
                                    rptCustomCheck.InstancePtr.blnIndividualCheck = false;

                                    Hide();
                                    FCGlobal.Screen.MousePointer = 0;
                                    if (rsBanks.EndOfFile() != true && rsBanks.BeginningOfFile() != true)
                                    {
                                        rptCustomCheck.InstancePtr.Init("", true, rsBanks.Get_Fields_String("RoutingNumber"), rsBanks.Get_Fields_String("BankAccountNumber"), rsBanks.Get_Fields_String("Name"), lngBankID, showModal: true);
                                    }
                                    else
                                    {
                                        rptCustomCheck.InstancePtr.Init("", true, string.Empty, string.Empty, string.Empty, lngBankID, showModal: true);
                                    }

                                    FCGlobal.Screen.MousePointer = 0;
                                    blnPrintingUnprintedChecks = false;

                                }
                                else
                                {
                                    MessageBox.Show("You must set up your custom check format before you may proceed", "Invalid Custom Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    FCGlobal.Screen.MousePointer = 0;
                                    return;
                                }
                                MessageBox.Show("Please remove your checks from the printer when they are done printing and click OK to continue.", "Remove Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                break;
                            }
                        default:
                            //cmdChoose.Enabled = true;
                            FCGlobal.Screen.MousePointer = 0;
                            MessageBox.Show("Unknown Check Format Used", "Invalid Check Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                    }
                }
                else
                {
                    //cmdChoose.Enabled = true;
                    FCGlobal.Screen.MousePointer = 0;
                    MessageBox.Show("You must setup a check format before you may proceed", "No Check Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                rsMasterJournal.OpenRecordset($"SELECT * FROM JournalMaster WHERE Status = 'C' AND ISNULL(BankNumber, 0) = 0 AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '{FCConvert.ToString(TempWarrant)}')");
                if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
                {
                    do
                    {
                        rsMasterJournal.Edit();
                        rsMasterJournal.Set_Fields("BankNumber", intBank);
                        rsMasterJournal.Update();
                        rsMasterJournal.MoveNext();
                    }
                    while (rsMasterJournal.EndOfFile() != true);
                }
            }
            finally
            {
                rsWarrant.DisposeOf();
                rsCustomCheck.DisposeOf();
                rsMasterJournal.DisposeOf();
                rsBanks.DisposeOf();
            }
            FCGlobal.Screen.MousePointer = 0;
        }

        public void cmdChoose_Click()
        {
            cmdChoose_Click(btnFileProcess, new System.EventArgs());
        }

        private void cmdPost_Click(object sender, System.EventArgs e)
        {
            int counter;
            string strJournals = "";
            bool blnSelected;

            int intJournalNumberToUse = 0;

            blnSelected = false;
            if (fraBanks.Visible && cboBanks.ListIndex < 0)
            {
                MessageBox.Show("You must select a bank to proceed", "No Bank Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            clsDRWrapper rsJournalInfo = new clsDRWrapper();
            clsDRWrapper rsDetailInfo = new clsDRWrapper();
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            try
            {
                for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
                {
                    if (FCUtils.CBool(vsJournals.TextMatrix(counter, 0)) != true) 
                        continue;

                    intJournalNumberToUse = FCConvert.ToInt32(vsJournals.TextMatrix(counter, JournalCol));
                    blnSelected = true;
                    rsJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND Status = 'V' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))) + " AND Period = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, PeriodCol))));
                    if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                    {
                        if ((rsJournalInfo.Get_Fields_Decimal("TotalAmount") - rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo")) != FCConvert.ToDecimal(vsJournals.TextMatrix(counter, AmountCol)))
                        {
                            MessageBox.Show("Journal " + intJournalNumberToUse + " was run through the Warrant Preview with an amount of " + Strings.Format(rsJournalInfo.Get_Fields_Decimal("TotalAmount") - rsJournalInfo.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + ".  This journal now shows an amount of " + vsJournals.TextMatrix(counter, AmountCol) + ".  You may not continue until these amounts are in balance.", "Different Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }

                    rsJournalInfo.OpenRecordset($"SELECT * FROM APJournal WHERE JournalNumber = {intJournalNumberToUse}", "Budgetary");

                    if (rsJournalInfo.EndOfFile() || rsJournalInfo.BeginningOfFile()) continue;

                    do
                    {
                        rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
                        if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
                        {
                            if (FCConvert.ToInt32(rsJournalInfo.Get_Fields_Int32("VendorNumber")) != 0)
                            {
                                rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsJournalInfo.Get_Fields_Int32("VendorNumber"));

                                if (rsVendorInfo.EndOfFile() || rsVendorInfo.BeginningOfFile())
                                {
                                    MessageBox.Show("Vendor " + modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields_Int32("VendorNumber"), 5) + " has been deleted.  You will need to go enter the vendor back into the system before you may run AP checks.  If you remember entering the journal for this vendor please call TRIO and describe the steps you went through so we can prevent this from happening in the future.", "Enter Vendor Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    return;
                                }
                            }
                        }
                        else
                        {
                            rsJournalInfo.Delete();
                            rsJournalInfo.Update();
                        }
                        rsJournalInfo.MoveNext();
                    }
                    while (rsJournalInfo.EndOfFile() != true);
                }

                if (!blnSelected)
                {
                    MessageBox.Show("You must first select 1 or more journals to run checks on before you may continue.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (modBudgetaryMaster.LockWarrant() == false)
                {
                    MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    blnLocked = true;
                }

                if (fraBanks.Visible)
                {
                    fraBanks.Enabled = false;
                }

                Label1.Visible = false;
                vsJournals.Visible = false;
                //cmdPost.Visible = false;
                cmdQuit.Visible = false;
                fraChoose.Visible = true;
                // get the next available warrant
                Master.OpenRecordset("SELECT TOP 1 * FROM WarrantMaster ORDER BY WarrantNumber DESC");
                if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                {
                    Master.MoveLast();
                    Master.MoveFirst();
                    TempWarrant = FCConvert.ToInt32(Master.Get_Fields_Int32("WarrantNumber")) + 1;
                }
                else
                {
                    TempWarrant = 1;
                }
                txtWarrant.Text = FCConvert.ToString(TempWarrant);
                txtCheck.Focus();
            }
            finally
            {
                rsJournalInfo.DisposeOf();
                rsDetailInfo.DisposeOf();
                rsVendorInfo.DisposeOf();
            }
        }

        public void cmdPost_Click()
        {
            cmdPost_Click(btnFileProcess, new System.EventArgs());
        }

        private void cmdProcess_Click(object sender, System.EventArgs e)
        {
            int intTotalCount = 0;
            // vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
            Decimal curTotalAmount;
            int counter = 0;
            rsJournals.OpenRecordset("SELECT * FROM Budgetary");
            rsJournals.Edit();
            rsJournals.Set_Fields("PayDate", txtPayDate.Text);
            rsJournals.Update(true);
            if (cmbInitial.SelectedIndex == 0)
            {
                rsJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND Status = 'V' ORDER BY JournalNumber, Period");
                if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
                {
                    rsJournals.MoveLast();
                    rsJournals.MoveFirst();
                    counter = 1;
                    do
                    {
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE Payable <= '" + txtPayDate.Text + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'V'");
                        if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                        {
                            intTotalCount = 0;
                            curTotalAmount = 0;
                            while (rsAPJournal.EndOfFile() != true)
                            {
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
                                if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                                {
                                    // TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
                                    intTotalCount += FCConvert.ToInt32(rsEntries.Get_Fields("TotalEntries"));
                                    // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                    curTotalAmount += FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields_Decimal("TotalAmount"))) - FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields("TotalDiscount")));
                                }
                            CheckNextJournal:
                                ;
                                rsAPJournal.MoveNext();
                            }
                            vsJournals.TextMatrix(counter, PostCol, FCConvert.ToString(false));
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            vsJournals.TextMatrix(counter, JournalCol, modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournals.Get_Fields("JournalNumber")), 4));
                            vsJournals.TextMatrix(counter, JournalDescriptionCol, FCConvert.ToString(rsJournals.Get_Fields_String("Description")));
                            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                            vsJournals.TextMatrix(counter, PeriodCol, FCConvert.ToString(rsJournals.Get_Fields("Period")));
                            vsJournals.TextMatrix(counter, AmountCol, Strings.Format(curTotalAmount, "#,##0.00"));
                            vsJournals.TextMatrix(counter, CountCol, FCConvert.ToString(intTotalCount));
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            vsJournals.TextMatrix(counter, CheckCountCol, FCConvert.ToString(CalculateCheckCount_8(FCConvert.ToInt32(rsJournals.Get_Fields("JournalNumber")), DateAndTime.DateValue(txtPayDate.Text))));
                            vsJournals.TextMatrix(counter, cnstBankCol, rsJournals.Get_Fields_Int32("BankNumber"));
                            counter += 1;
                        }
                        rsJournals.MoveNext();
                    }
                    while (rsJournals.EndOfFile() != true);
                    vsJournals.Rows = counter;
                }
                else
                {
                    vsJournals.Rows = 1;
                }

                Label1.Visible = true;
                vsJournals.Visible = true;
                //cmdPost.Visible = true;
                //cmdQuit.Visible = true;
                fraPayDate.Visible = false;
                int lngBankID = 0;
                lngBankID = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank"))));
                if (lngBankID == 0)
                {
                    fraBanks.Visible = true;
                    fraBanks.Enabled = true;
                    cboBanks.SelectedIndex = 0;
                }
                else
                {
                    UpdateStartingCheckNumber(lngBankID);
                }
                vsJournals.Row = -1;
                vsJournals.Focus();
            }
            else
            {
                if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
                {
                    txtLastReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("EndCheckNumber"));
                    txtFirstReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("StartCheckNumber"));
                    CheckCheckRangeForSingleBank();
                }
                fraPayDate.Visible = false;
                fraReprint.Visible = true;
                txtFirstCheck.Focus();
            }
        }

        public void cmdProcess_Click()
        {
            cmdProcess_Click(btnFileProcess, new System.EventArgs());
        }

        private void cmdQuit_Click(object sender, System.EventArgs e)
        {
            if (fraPayDate.Visible == true)
            {
                Close();
            }
            else
            {
                Label1.Visible = false;
                vsJournals.Visible = false;
                //cmdPost.Visible = false;
                cmdQuit.Visible = false;
                fraReprint.Visible = false;
                fraPayDate.Visible = true;
                fraBanks.Visible = false;
            }
        }

        private void cmdReprint_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsCheck = new clsDRWrapper();
            clsDRWrapper rsWarrants = new clsDRWrapper();
            // vbPorter upgrade warning: counter As int	OnWrite(double, short)
            int counter;
            int lngWarrant;
            clsDRWrapper rsCustomCheck = new clsDRWrapper();
            DateTime datOriginalCheckDate;
            int intReturn = 0;
            clsDRWrapper rsBanks = new clsDRWrapper();
            clsDRWrapper rsMasterJournal = new clsDRWrapper();
            int intBank;
            int lngBankID;
            cBankService bServ = new cBankService();
            if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 3)
            {
                MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (Strings.Trim(txtFirstCheck.Text) == "" || Strings.Trim(txtFirstReprintCheck.Text) == "" || Strings.Trim(txtLastReprintCheck.Text) == "")
            {
                MessageBox.Show("You must fill in all the fields before you may proceed.", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Conversion.Val(Strings.Trim(txtFirstReprintCheck.Text)) > Conversion.Val(Strings.Trim(txtLastReprintCheck.Text)))
            {
                MessageBox.Show("The last check to reprint must be greater than or equal to the first check to reprint.", "Invalid Check Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
            //cmdReprint.Enabled = false;
            rsWarrants.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE Status = 'R' AND CheckNumber >= " + txtFirstReprintCheck.Text + " AND CheckNumber <= " + txtLastReprintCheck.Text);
            if (rsWarrants.EndOfFile() != true && rsWarrants.BeginningOfFile() != true)
            {
                if (rsWarrants.RecordCount() > 1)
                {
                    MessageBox.Show("You may only reprint checks from one warrant at a time", "Invalid Check Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //cmdReprint.Enabled = true;
                    FCGlobal.Screen.MousePointer = 0;
                    return;
                }
                else
                {
                    TempWarrant = FCConvert.ToInt32(rsWarrants.Get_Fields_Int32("WarrantNumber"));
                }
            }
            // Dave 3/14/05 Lets them reprint checks as long as they have not been posted
            rsCheck.OpenRecordset("SELECT * FROM APJournal WHERE (convert(int, IsNull(CheckNumber, 0)) >= " + FCConvert.ToString(Conversion.Val(txtFirstReprintCheck.Text)) + " AND convert(int, IsNull(CheckNumber, 0)) <= " + FCConvert.ToString(Conversion.Val(txtLastReprintCheck.Text)) + ") AND Warrant = '" + FCConvert.ToString(TempWarrant) + "' AND Status = 'P'");
            if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
            {
                MessageBox.Show("One or more of the checks you are trying to reprint is in a journal that has already been posted.", "Unable to Reprint", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //cmdReprint.Enabled = true;
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            intBank = 0;
            // DJW@7252013 took out this part of sql to allow for journals in other statuses to get MICR line printed Status = 'C' AND
            rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(TempWarrant) + "')");
            if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
            {
                // TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMasterJournal.Get_Fields("BankNumber"))));
            }
            // lngBankID = bServ.GetBankIDFromBankNumber(intBank)
            lngBankID = intBank;
            // rsCheck.OpenRecordset "SELECT * FROM APJournal WHERE (val(Check) >= " & Val(txtFirstReprintCheck.Text) & " AND val(Check) <= " & Val(txtLastReprintCheck.Text) & ") AND Warrant = '" & TempWarrant & "'"
            // If rsCheck.EndOfFile <> True And rsCheck.BeginningOfFile <> True Then
            // strJournalStatus = rsCheck.Fields["Status"]
            // End If
            // DJW@12302010 Took out this clause so it can get voids to see if they are caused by carry overs or not   Status = 'R' AND
            rsCheck.OpenRecordset("SELECT * FROM TempCheckFile WHERE (CheckNumber >= " + txtFirstReprintCheck.Text + " AND CheckNumber <= " + txtLastReprintCheck.Text + ") ORDER BY CheckNumber");
            if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
            {
                lngWarrant = FCConvert.ToInt32(rsCheck.Get_Fields_Int32("WarrantNumber"));
                datOriginalCheckDate = (DateTime)rsCheck.Get_Fields_DateTime("CheckDate");
                for (counter = FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtFirstReprintCheck.Text))); counter <= FCConvert.ToInt32(Conversion.Val(Strings.Trim(txtLastReprintCheck.Text))); counter++)
                {
                    if (rsCheck.EndOfFile())
                    {
                        MessageBox.Show("One or more of the checks you are trying to reprint could not be found in the Temporary Check file.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdReprint.Enabled = true;
                        FCGlobal.Screen.MousePointer = 0;
                        return;
                    }
                    // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    else if (FCConvert.ToInt32(rsCheck.Get_Fields("CheckNumber")) != counter)
                    {
                        MessageBox.Show("Check # " + FCConvert.ToString(counter) + " could not be found in the Temporary Check file.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdReprint.Enabled = true;
                        FCGlobal.Screen.MousePointer = 0;
                        return;
                    }
                    else if (rsCheck.Get_Fields_String("Status") == "P")
                    {
                        MessageBox.Show("Check # " + FCConvert.ToString(counter) + " is a Pre-paid check so it may not be reprinted.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdReprint.Enabled = true;
                        FCGlobal.Screen.MousePointer = 0;
                        return;
                    }
                    else if (FCConvert.ToString(rsCheck.Get_Fields_String("Status")) == "V" && rsCheck.Get_Fields_Boolean("CarryOver") == false)
                    {
                        MessageBox.Show("Check # " + FCConvert.ToString(counter) + " is a voided check so it may not be reprinted.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdReprint.Enabled = true;
                        FCGlobal.Screen.MousePointer = 0;
                        return;
                    }
                    rsCheck.MoveNext();
                }
            }
            else
            {
                //cmdReprint.Enabled = true;
                FCGlobal.Screen.MousePointer = 0;
                MessageBox.Show("One or more of the checks you are trying to reprint could not be found or has already been run through the check register.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            txtPayDate.Text = Strings.Format(datOriginalCheckDate, "MM/dd/yyyy");
            if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 0)
            {
                MessageBox.Show("Please insert your checks into the printer and then click OK to continue.", "Insert Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 2)
                {
                   rptLaserChecks.InstancePtr.blnIndividualCheck = false;

                    Hide();
                    FCGlobal.Screen.MousePointer = 0;
                    rptLaserChecks.InstancePtr.Init("", true, lngBankID, Modal);

                    FCGlobal.Screen.MousePointer = 0;
                }
                else if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 4)
                {
                    frmSelectCustomCheckFormat.InstancePtr.Init();
                    rsBanks.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(intBank), "TWBD0000.vb1");
                    rsCustomCheck.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
                    if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
                    {
                        rptCustomCheck.InstancePtr.blnIndividualCheck = false;

                        Hide();
                        FCGlobal.Screen.MousePointer = 0;
                        if (rsBanks.EndOfFile() != true && rsBanks.BeginningOfFile() != true)
                        {
                            rptCustomCheck.InstancePtr.Init("", true, rsBanks.Get_Fields_String("RoutingNumber"), rsBanks.Get_Fields_String("BankAccountNumber"), rsBanks.Get_Fields_String("Name"), lngBankID, showModal: true);
                        }
                        else
                        {
                            rptCustomCheck.InstancePtr.Init("", true, string.Empty, string.Empty, string.Empty, lngBankID, showModal: true);
                        }
                        FCGlobal.Screen.MousePointer = 0;
                    }
                    else
                    {
                        MessageBox.Show("You must set up your custom check format before you may proceed", "Invalid Custom Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCGlobal.Screen.MousePointer = 0;
                        return;
                    }
                }
                else
                {
                    //cmdReprint.Enabled = true;
                    FCGlobal.Screen.MousePointer = 0;
                    MessageBox.Show("You are using an unknown Check Format.", "Invalid Check Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (blnPrintingUnprintedChecks)
                {
                    fraReprint.Visible = false;
                    Label1.Visible = false;
                    vsJournals.Visible = false;
                    //cmdPost.Visible = false;
                    cmdQuit.Visible = false;
                    fraReprint.Visible = false;
                    fraPayDate.Visible = true;
                    //rptLaserChecks.InstancePtr.Hide();
                    //rptStandardChecks.InstancePtr.Hide();
                    //rptCustomCheck.InstancePtr.Hide();
                    //srptCustomCheckBody.InstancePtr.Hide();
                    //srptCustomCheckStub.InstancePtr.Hide();
                    MessageBox.Show("You will now have to print initial checks to get the unprinted checks for this journal.", "Unprinted Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Please remove your checks from the printer when they are done printing and click OK to continue.", "Remove Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("You must setup a check format before you may proceed.", "No Check Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //cmdReprint.Enabled = true;
                FCGlobal.Screen.MousePointer = 0;
                return;
            }
            //cmdReprint.Enabled = true;
            FCGlobal.Screen.MousePointer = 0;
        }

        public void cmdReprint_Click()
        {
            cmdReprint_Click(btnFileProcess, new System.EventArgs());
        }

        private void frmPrintChecks_Activated(object sender, System.EventArgs e)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            clsDRWrapper rsCheckJournal = new clsDRWrapper();
            if (modGlobal.FormExist(this))
            {
                return;
            }
            rsTemp.OpenRecordset("SELECT * FROM Budgetary");
            if (!Information.IsDate(rsTemp.Get_Fields("PayDate")))
            {
                txtPayDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            }
            else
            {
                txtPayDate.Text = Strings.Format(rsTemp.Get_Fields_DateTime("PayDate"), "MM/dd/yyyy");
            }
            rsCheckJournal.OpenRecordset("SELECT JournalMaster.JournalNumber FROM JournalMaster INNER JOIN APJournal ON (JournalMaster.JournalNumber = APJournal.JournalNumber) WHERE JournalMaster.Type = 'AP' AND JournalMaster.Status = 'E' AND APJournal.Status = 'C'");
            if (rsCheckJournal.EndOfFile() != true && rsCheckJournal.BeginningOfFile() != true)
            {
                do
                {
                    // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    rsTemp.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + rsCheckJournal.Get_Fields("JournalNumber"));
                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                    {
                        blnPrintingUnprintedChecks = true;
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        lngUnprintedJournal = FCConvert.ToInt32(rsCheckJournal.Get_Fields("JournalNumber"));
                        break;
                    }
                    rsCheckJournal.MoveNext();
                }
                while (rsCheckJournal.EndOfFile() != true);
            }
            if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AllowManualWarrantNumbers")))
            {
                txtWarrant.Locked = false;
            }
            else
            {
                txtWarrant.Locked = true;
            }
            if (!blnPrintingUnprintedChecks)
            {
                // txtPayDate.SetFocus
            }
            else
            {
                MessageBox.Show("You have some unprinted checks which need to be printed.  You will need to reprint at least one check to complete the check process.", "Unprinted Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbInitial.SelectedIndex = 1;
                cmdProcess_Click();
                if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
                {
                    txtFirstReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("StartCheckNumber"));
                    txtLastReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("EndCheckNumber"));
                }
            }
            if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 3)
            {
                MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Refresh();
        }

        private void frmPrintChecks_Load(object sender, System.EventArgs e)
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            rsInfo.OpenRecordset("SELECT * FROM Banks WHERE Name <> ''");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                cboBanks.Clear();
                do
                {
                    cboBanks.AddItem(rsInfo.Get_Fields_Int32("ID") + " - " + rsInfo.Get_Fields_String("Name"));
                    cboBanks.ItemData(cboBanks.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
                    rsInfo.MoveNext();
                }
                while (rsInfo.EndOfFile() != true);
            }
            blnLocked = false;
            PostCol = 0;
            JournalCol = 1;
            JournalDescriptionCol = 2;
            PeriodCol = 3;
            AmountCol = 4;
            CountCol = 5;
            CheckCountCol = 6;
            blnPrintingUnprintedChecks = false;
            //vsJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsJournals.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsJournals.TextMatrix(0, PostCol, "Select");
            vsJournals.TextMatrix(0, JournalCol, "Journal");
            vsJournals.TextMatrix(0, JournalDescriptionCol, "Description");
            vsJournals.TextMatrix(0, PeriodCol, "Period");
            vsJournals.TextMatrix(0, AmountCol, "Amount");
            vsJournals.TextMatrix(0, CountCol, "Entries");
            vsJournals.TextMatrix(0, CheckCountCol, "Checks");
            vsJournals.TextMatrix(0, cnstBankCol, "Bank");
            vsJournals.ColWidth(PostCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.07));
            vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(JournalDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.36));
            vsJournals.ColWidth(PeriodCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(AmountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.17));
            vsJournals.ColWidth(CountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(CheckCountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            ColPercents[0] = 0.1015805;
            ColPercents[1] = 0.1410942;
            ColPercents[2] = 0.1410942;
            ColPercents[3] = 0.2739513;
            ColPercents[4] = 0.1623708;
            ColPercents[5] = 0.1123708;
            vsJournals.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
            vsJournals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            rsCheckInfo.OpenRecordset("SELECT * FROM LastChecksRun");
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            //rptStandardChecks.InstancePtr.Hide();
            //rptLaserChecks.InstancePtr.Hide();
            //rptCustomCheck.InstancePtr.Hide();
            //rptLaserChecksTop.InstancePtr.Hide();
            //srptCustomCheckBody.InstancePtr.Hide();
            //srptCustomCheckStub.InstancePtr.Hide();
            modGlobalFunctions.SetTRIOColors(this);
        }

        private void frmPrintChecks_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                if (fraPayDate.Visible == true)
                {
                    Close();
                }
                else if (vsJournals.Visible == true)
                {
                    vsJournals.Visible = false;
                    Label1.Visible = false;
                    vsJournals.Visible = false;
                    //cmdPost.Visible = false;
                    cmdQuit.Visible = false;
                    fraReprint.Visible = false;
                    fraPayDate.Visible = true;
                }
                else if (fraChoose.Visible == true)
                {
                    modBudgetaryMaster.UnlockWarrant();
                    blnLocked = false;
                    fraChoose.Visible = false;
                    Label1.Visible = false;
                    vsJournals.Visible = false;
                    //cmdPost.Visible = false;
                    cmdQuit.Visible = false;
                    fraReprint.Visible = false;
                    fraPayDate.Visible = true;
                }
                else
                {
                    fraReprint.Visible = false;
                    Label1.Visible = false;
                    vsJournals.Visible = false;
                    //cmdPost.Visible = false;
                    cmdQuit.Visible = false;
                    fraReprint.Visible = false;
                    fraPayDate.Visible = true;
                }
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmPrintChecks_Resize(object sender, System.EventArgs e)
        {
            vsJournals.ColWidth(PostCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.07));
            vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(JournalDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.36));
            vsJournals.ColWidth(PeriodCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(AmountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.17));
            vsJournals.ColWidth(CountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(CheckCountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            if (blnLocked)
            {
                modBudgetaryMaster.UnlockWarrant();
            }
        }

        private void btnFileProcess_Click(object sender, System.EventArgs e)
        {
            if (fraPayDate.Visible == true)
            {
                cmdProcess_Click();
            }
            else if (fraChoose.Visible == true)
            {
                cmdChoose_Click();
            }
            else if (fraReprint.Visible == true)
            {
                cmdReprint_Click();
            }
            else
            {
                cmdPost_Click();
            }
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuProcessSave_Click()
        {
            Support.SendKeys("{F10}", false);
        }

        private void txtCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtFirstCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtFirstReprintCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtFirstReprintCheck_Leave(object sender, System.EventArgs e)
        {
            if (Strings.Trim(txtFirstReprintCheck.Text) == "")
            {
                if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
                {
                    txtFirstReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("StartCheckNumber"));
                }
            }
        }

        private void txtLastReprintCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtLastReprintCheck_Leave(object sender, System.EventArgs e)
        {
            if (Strings.Trim(txtLastReprintCheck.Text) == "")
            {
                if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
                {
                    txtLastReprintCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("EndCheckNumber"));
                }
            }
        }

        private void txtLastReprintCheck_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CheckCheckRangeForSingleBank();
        }

        private void CheckCheckRangeForSingleBank()
        {
            if (txtFirstReprintCheck.Text != "" && txtLastReprintCheck.Text != "")
            {
                clsDRWrapper rsTemp = new clsDRWrapper();
                rsTemp.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE Status = 'R' AND CheckNumber >= " + txtFirstReprintCheck.Text + " AND CheckNumber <= " + txtLastReprintCheck.Text);
                if (rsTemp.RecordCount() == 1)
                {
                    int lngWarrant = 0;
                    lngWarrant = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("WarrantNumber"));
                    int lngBank = 0;
                    lngBank = 0;
                    rsTemp.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(lngWarrant) + "')", "Budgetary");
                    if (!rsTemp.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                        lngBank = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("BankNumber"))));
                        if (lngBank > 0)
                        {
                            UpdateStartingCheckNumber(lngBank);
                        }
                    }
                }
            }
        }

        private void txtWarrant_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtWarrant_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            // vbPorter upgrade warning: answer As short, int --> As DialogResult
            DialogResult answer;
            rsTemp.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + txtWarrant.Text);
            if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
            {
                answer = MessageBox.Show("This warrant number has already been used.  Are you sure you wish to use this Warrant Number?", "Use Warrant Number?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (answer == DialogResult.No)
                {
                    txtWarrant.Focus();
                }
                else
                {
                    modBudgetaryMaster.WriteAuditRecord_8("Selected Already Used Warrant Number", "Print AP Checks");
                    txtCheck.Focus();
                }
            }
        }

        private void vsJournals_ClickEvent(object sender, System.EventArgs e)
        {
            int intRow = 0;
            int intId = 0;
            if (cboBanks.ListIndex >= 0)
            {
                intRow = vsJournals.Row;
                intId = cboBanks.ItemData(cboBanks.ListIndex);
                if (intRow > 0)
                {
                    if (FCConvert.ToInt32(vsJournals.TextMatrix(intRow, cnstBankCol)) == 0 || FCConvert.ToInt32(vsJournals.TextMatrix(intRow, cnstBankCol)) == intId)
                    {
                        if (FCConvert.ToBoolean(vsJournals.TextMatrix(vsJournals.Row, PostCol)))
                        {
                            vsJournals.TextMatrix(vsJournals.Row, PostCol, false.ToString());
                        }
                        else
                        {
                            vsJournals.TextMatrix(vsJournals.Row, PostCol, true.ToString());
                        }
                    }
                }
            }
            else if (!fraBanks.Visible)
            {
                intRow = vsJournals.Row;
                if (intRow > 0)
                {
                    if (FCConvert.ToBoolean(vsJournals.TextMatrix(vsJournals.Row, PostCol)))
                    {
                        vsJournals.TextMatrix(vsJournals.Row, PostCol, false.ToString());
                    }
                    else
                    {
                        vsJournals.TextMatrix(vsJournals.Row, PostCol, true.ToString());
                    }
                }
            }
        }

        private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Space)
            {
                KeyCode = 0;
                if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, PostCol)) == true)
                {
                    vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(false));
                }
                else
                {
                    vsJournals.TextMatrix(vsJournals.Row, PostCol, FCConvert.ToString(true));
                }
            }
        }
        // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private short CalculateCheckCount_8(int lngJournal, DateTime datPayable)
        {
            return CalculateCheckCount(ref lngJournal, ref datPayable);
        }

        private short CalculateCheckCount(ref int lngJournal, ref DateTime datPayable)
        {
            short CalculateCheckCount = 0;
            clsDRWrapper rsCountInfo = new clsDRWrapper();
            int rsCheckTypeInfo;
            int intEntriesOnCheck;
            clsDRWrapper rsCustomCheck = new clsDRWrapper();
            CalculateCheckCount = 0;
            if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 4)
            {
                intEntriesOnCheck = 10;
            }
            else
            {
                rsCustomCheck.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
                if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
                {
                    if (rsCustomCheck.Get_Fields_Boolean("UseDoubleStub") == false)
                    {
                        intEntriesOnCheck = 10;
                    }
                    else
                    {
                        intEntriesOnCheck = 20;
                    }
                }
                else
                {
                    intEntriesOnCheck = 10;
                }
            }
            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
            rsCountInfo.OpenRecordset("SELECT * FROM APJournal WHERE Payable <= '" + txtPayDate.Text + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'V' AND Isnull(Seperate, 0) = 1");
            if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
            {
                CalculateCheckCount += FCConvert.ToInt16(rsCountInfo.RecordCount());
            }
            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
            rsCountInfo.OpenRecordset("SELECT COUNT(VendorNumber) as TotalEntries FROM APJournal WHERE VendorNumber <> 0 AND Payable <= '" + txtPayDate.Text + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'V' AND Isnull(Seperate, 0) = 0 GROUP BY VendorNumber");
            if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
            {
                do
                {
                    // TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
                    int vbPorterVar = FCUtils.iDiv(FCConvert.ToInt32(rsCountInfo.Get_Fields("TotalEntries")), intEntriesOnCheck);
                    if (vbPorterVar <= 1)
                    {
                        CalculateCheckCount += 1;
                    }
                    else if (vbPorterVar <= 2)
                    {
                        CalculateCheckCount += 2;
                    }
                    else if (vbPorterVar <= 3)
                    {
                        CalculateCheckCount += 3;
                    }
                    else if (vbPorterVar <= 4)
                    {
                        CalculateCheckCount += 4;
                    }
                    else if (vbPorterVar <= 5)
                    {
                        CalculateCheckCount += 5;
                    }
                    else if (vbPorterVar <= 6)
                    {
                        CalculateCheckCount += 6;
                    }
                    else if (vbPorterVar <= 7)
                    {
                        CalculateCheckCount += 7;
                    }
                    else if (vbPorterVar <= 8)
                    {
                        CalculateCheckCount += 8;
                    }
                    rsCountInfo.MoveNext();
                }
                while (rsCountInfo.EndOfFile() != true);
            }
            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
            rsCountInfo.OpenRecordset("SELECT COUNT(TempVendorName) as TotalEntries FROM APJournal WHERE VendorNumber = 0 AND Payable <= '" + txtPayDate.Text + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'V' AND Isnull(Seperate, 0) = 0 GROUP BY TempVendorName");
            if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
            {
                do
                {
                    // TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
                    int vbPorterVar1 = FCUtils.iDiv(FCConvert.ToInt32(rsCountInfo.Get_Fields("TotalEntries")), intEntriesOnCheck);
                    if (vbPorterVar1 <= 1)
                    {
                        CalculateCheckCount += 1;
                    }
                    else if (vbPorterVar1 <= 2)
                    {
                        CalculateCheckCount += 2;
                    }
                    else if (vbPorterVar1 <= 3)
                    {
                        CalculateCheckCount += 3;
                    }
                    else if (vbPorterVar1 <= 4)
                    {
                        CalculateCheckCount += 4;
                    }
                    else if (vbPorterVar1 <= 5)
                    {
                        CalculateCheckCount += 5;
                    }
                    else if (vbPorterVar1 <= 6)
                    {
                        CalculateCheckCount += 6;
                    }
                    else if (vbPorterVar1 <= 7)
                    {
                        CalculateCheckCount += 7;
                    }
                    else if (vbPorterVar1 <= 8)
                    {
                        CalculateCheckCount += 8;
                    }
                    rsCountInfo.MoveNext();
                }
                while (rsCountInfo.EndOfFile() != true);
            }
            return CalculateCheckCount;
        }

        private void UpdateStartingCheckNumber(int lngBankID)
        {
            int lngCheckNumber = 0;
            if (lngBankID > 0)
            {
                lngCheckNumber = bankService.GetLastCheckNumberForBankForCorrectCheckType(lngBankID, "AP");
                if (lngCheckNumber > 0)
                {
                    txtCheck.Text = FCConvert.ToString(lngCheckNumber + 1);
                    txtFirstCheck.Text = FCConvert.ToString(lngCheckNumber + 1);
                }
            }
        }
    }
}
