﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cGLFundReportGroup
	{
		//=========================================================
		private string strFund = string.Empty;
		private string strDescription = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collAssets = new cGenericCollection();
		private cGenericCollection collAssets_AutoInitialized;

		private cGenericCollection collAssets
		{
			get
			{
				if (collAssets_AutoInitialized == null)
				{
					collAssets_AutoInitialized = new cGenericCollection();
				}
				return collAssets_AutoInitialized;
			}
			set
			{
				collAssets_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collLiabilities = new cGenericCollection();
		private cGenericCollection collLiabilities_AutoInitialized;

		private cGenericCollection collLiabilities
		{
			get
			{
				if (collLiabilities_AutoInitialized == null)
				{
					collLiabilities_AutoInitialized = new cGenericCollection();
				}
				return collLiabilities_AutoInitialized;
			}
			set
			{
				collLiabilities_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collFundBalances = new cGenericCollection();
		private cGenericCollection collFundBalances_AutoInitialized;

		private cGenericCollection collFundBalances
		{
			get
			{
				if (collFundBalances_AutoInitialized == null)
				{
					collFundBalances_AutoInitialized = new cGenericCollection();
				}
				return collFundBalances_AutoInitialized;
			}
			set
			{
				collFundBalances_AutoInitialized = value;
			}
		}

		public cGenericCollection FundBalances
		{
			get
			{
				cGenericCollection FundBalances = null;
				FundBalances = collFundBalances;
				return FundBalances;
			}
		}

		public cGenericCollection Liabilities
		{
			get
			{
				cGenericCollection Liabilities = null;
				Liabilities = collLiabilities;
				return Liabilities;
			}
		}

		public cGenericCollection Assets
		{
			get
			{
				cGenericCollection Assets = null;
				Assets = collAssets;
				return Assets;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}
	}
}
