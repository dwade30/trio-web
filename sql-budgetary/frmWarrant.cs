﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrant.
	/// </summary>
	public partial class frmWarrant : BaseForm
	{
		public frmWarrant()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
			this.cmbReprint.SelectedIndex = 0;
			this.cboPreviewAccount.SelectedIndex = 0;
			this.cboReport.SelectedIndex = 0;
            this.lblTitle.Capitalize = false;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWarrant InstancePtr
		{
			get
			{
				return (frmWarrant)Sys.GetInstance(typeof(frmWarrant));
			}
		}

		protected frmWarrant _InstancePtr = null;

		public clsDRWrapper rsInfo_AutoInitialized;

		public clsDRWrapper rsInfo
		{
			get
			{
				if (rsInfo_AutoInitialized == null)
				{
					rsInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsInfo_AutoInitialized;
			}
			set
			{
				rsInfo_AutoInitialized = value;
			}
		}

		public int intPreviewChoice;
		public string strPrinterName = "";
		public clsBudgetaryPosting clsPostInfo_AutoInitialized;

		public clsBudgetaryPosting clsPostInfo
		{
			get
			{
				if (clsPostInfo_AutoInitialized == null)
				{
					clsPostInfo_AutoInitialized = new clsBudgetaryPosting();
				}
				return clsPostInfo_AutoInitialized;
			}
			set
			{
				clsPostInfo_AutoInitialized = value;
			}
		}

		public clsPostingJournalInfo clsJournalInfo_AutoInitialized;

		public clsPostingJournalInfo clsJournalInfo
		{
			get
			{
				if (clsJournalInfo_AutoInitialized == null)
				{
					clsJournalInfo_AutoInitialized = new clsPostingJournalInfo();
				}
				return clsJournalInfo_AutoInitialized;
			}
			set
			{
				clsJournalInfo_AutoInitialized = value;
			}
		}

		public bool blnPostJournal;
		public bool blnAsked;

		private cSettingUtility setUtil_AutoInitialized;

		private cSettingUtility setUtil
		{
			get
			{
				if (setUtil_AutoInitialized == null)
				{
					setUtil_AutoInitialized = new cSettingUtility();
				}
				return setUtil_AutoInitialized;
			}
			set
			{
				setUtil_AutoInitialized = value;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_cmdCancel_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdCancel2_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_cmdCancel2_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdChoose_Click(object sender, System.EventArgs e)
		{
			try
			{
                if (cboPreviewAccount.SelectedIndex > -1)
                {
                    intPreviewChoice = cboPreviewAccount.SelectedIndex + 1;
                    cmdProcess_Click();
                    return;
                }
                else
                {
                    MessageBox.Show("You must make a selection before you may proceed with this Report.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_cmdChoose_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			clsDRWrapper rsAPJournal = new clsDRWrapper();
			clsDRWrapper rsEntries = new clsDRWrapper();
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			try
			{
				// On Error GoTo ErrorHandler
				//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
				if (cmbReprint.SelectedIndex == 0)
				{
					rsInfo.OpenRecordset("SELECT DISTINCT Warrant FROM APJournal WHERE Status = 'R' ORDER BY Warrant");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						do
						{
							// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
							rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'R' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + rsInfo.Get_Fields("Warrant") + "')");
							if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
							{
								do
								{
									// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
									rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'R' AND JournalNumber = " + rsMasterJournal.Get_Fields("JournalNumber"));
									curTotalAmount = 0;
									if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
									{
										while (rsAPJournal.EndOfFile() != true)
										{
											rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
											if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
											{
												//FC:FINAL:MSH - Issue #733: FCConvert.ToDecimal added, because '+=' can't be applied to decimal and double
												// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
												curTotalAmount += FCConvert.ToDecimal(Conversion.Val(rsEntries.Get_Fields_Decimal("TotalAmount")) - Conversion.Val(rsEntries.Get_Fields("TotalDiscount")));
											}
											rsAPJournal.MoveNext();
										}
									}
									//FC:FINAL:MSH - Issue #733: FCConvert.ToDouble added, because '!=' can't be applied to double and decimal
									if (modGlobal.Round_8(FCConvert.ToDouble(curTotalAmount), 2) != FCConvert.ToDouble(rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo")))
									{
										// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
										MessageBox.Show("Journal " + Strings.Format(rsMasterJournal.Get_Fields("JournalNumber"), "0000") + " was run through the Warrant Preview with an amount of " + Strings.Format(rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + ".  This journal now shows an amount of " + Strings.Format(curTotalAmount, "#,##0.00") + ".  You may not continue until these amounts are in balance.", "Different Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
										return;
									}
									rsMasterJournal.MoveNext();
								}
								while (rsMasterJournal.EndOfFile() != true);
							}
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
					if (intPreviewChoice == 5)
					{
						//cmdProcess.Visible = false;
						cboPreviewAccount.SelectedIndex = 1;
						fraChoose.Visible = true;
						cmbReprint.Visible = false;
						lblReprint.Visible = false;
						//cmdCancel.Visible = false;
						return;
					}
					rsInfo.OpenRecordset("SELECT DISTINCT Warrant FROM APJournal WHERE Status = 'R' ORDER BY Warrant");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						// If GetRegistryKey("APProcessReportsDefaultPrinter", , False) Then
						if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
						{
							strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim(((modRegistry.GetRegistryKey("PrinterDeviceName")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
						}
						else
						{

						}
						this.Hide();
						clsPostInfo.ClearJournals();
						if (rsInfo.RecordCount() > 1)
						{   
                            using (var selectPrintItem = new SelectPrintItem())
                            {
                                selectPrintItem.ClearItems();
                                do
                                {
                                    selectPrintItem.AddItem(string.Format("Item {0} of {1} - {2}", rsInfo.AbsolutePosition() + 1, rsInfo.RecordCount(), "Warrant " + rsInfo.Get_Fields("Warrant")));

                                CheckAgain:
                                    ;
                                    if (JobComplete())
                                    {
                                        rsInfo.MoveNext();
                                    }
                                    else
                                    {
                                        goto CheckAgain;
                                    }
                                }
                                while (rsInfo.EndOfFile() != true);
                                selectPrintItem.CurrentItem = 0;
                                selectPrintItem.PrintItem += (s, evt) =>
                                {
                                    if (selectPrintItem.CurrentItem >= 0)
                                    {
                                        frmWarrant.InstancePtr.rsInfo.SetRow(selectPrintItem.CurrentItem);

                                        if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("LandscapeWarrant")))
                                        {
                                            modDuplexPrinting.DuplexPrintReport(rptWarrantLandscape.InstancePtr, strPrinterName);
                                            //rptWarrantLandscape.InstancePtr.Hide();
                                        }
                                        else
                                        {
                                            modDuplexPrinting.DuplexPrintReport(rptWarrant.InstancePtr, strPrinterName);
                                            //rptWarrant.InstancePtr.Hide();
                                        }
                                    }
                                };
                                selectPrintItem.ShowDialog();
                            }
                            //FC:FINAL:SBE - #844 - we can close the form, because no other report will be generated. Reports was generated from a modal dialog if record count was greater than 1
                            this.Close();
                        }
						else
						{
							if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("LandscapeWarrant")))
							{
								frmReportViewer.InstancePtr.Init(rptWarrantLandscape.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true, true);
							}
							else
							{
								// Call frmReportViewer.Init(rptWarrant, strPrinterName, vbModal, , , , , , , , , , True, True)
								rptWarrant.InstancePtr.Init();
							}
						}
						
						this.Hide();
					}
					else
					{
						MessageBox.Show("There are no journals ready to be put on a Warrant.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (cboReport.Items.Count > 0)
					{
						fraSelectReport.Visible = true;
						cmbReprint.Visible = false;
						lblReprint.Visible = false;
					}
					else
					{
						MessageBox.Show("There are no reports to reprint.", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_cmdProcess_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(btnProcess, new System.EventArgs());
		}

		private void Command1_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (cboReport.SelectedIndex == -1)
				{
					MessageBox.Show("You must select which report you wish to reprint before you may continue.", "No Report Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

                if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
                {
                    strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim(((modRegistry.GetRegistryKey("PrinterDeviceName")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
                }

                clsPostInfo.ClearJournals();
                if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("LandscapeWarrant")))
                {
                    rptWarrantLandscape.InstancePtr.Init();
                }
                else
                {
                    rptWarrant.InstancePtr.Init();
                }
                this.Hide();
            }
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_Command1_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Command2_Click()
		{
			try
			{
				// On Error GoTo ErrorHandler
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_Command2_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmWarrant_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsReportInfo = new clsDRWrapper();
			clsDRWrapper rsWarrrantInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (modGlobal.FormExist(this))
				{
					return;
				}
				// rsReportInfo.OpenRecordset "SELECT * FROM Reprint WHERE Type = 'W' ORDER BY ReportOrder"
				rsReportInfo.OpenRecordset("SELECT DISTINCT Convert(int, IsNull(Warrant, 0)) as WarrantNumber FROM (SELECT * FROM APJournal WHERE Convert(int, IsNull(Warrant, 0)) <> 0 AND (Status = 'W' OR Status = 'X' OR Status = 'P')) as temp ORDER BY Convert(int, IsNull(Warrant, 0)) DESC");
				cboReport.Clear();
				if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
				{
					do
					{
						rsWarrrantInfo.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + rsReportInfo.Get_Fields_Int32("WarrantNumber"));
						if (rsWarrrantInfo.EndOfFile() != true && rsWarrrantInfo.BeginningOfFile() != true)
						{
							cboReport.AddItem("Warrant " + modValidateAccount.GetFormat_6(FCConvert.ToString(rsReportInfo.Get_Fields_Int32("WarrantNumber")), 4) + "  Date: " + Strings.Format(rsWarrrantInfo.Get_Fields_DateTime("WarrantDate"), "MM/dd/yyyy"));
						}
						else
						{
							cboReport.AddItem("Warrant " + modValidateAccount.GetFormat_6(FCConvert.ToString(rsReportInfo.Get_Fields_Int32("WarrantNumber")), 4) + "  Date: UNKNOWN");
						}
						rsReportInfo.MoveNext();
					}
					while (rsReportInfo.EndOfFile() != true);
				}
				if (cboReport.Items.Count > 0)
				{
					cboReport.SelectedIndex = 0;
				}
				lblTownAccounts.Enabled = true;
				cboPreviewAccount.Enabled = true;
				this.Refresh();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_Form_Activate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmWarrant_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				intPreviewChoice = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("PreviewPrint"))));
				if (intPreviewChoice == 0)
				{
					intPreviewChoice = 5;
				}
				modGlobalFunctions.SetTRIOColors(this);
				blnPostJournal = false;
				blnAsked = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmWarrant_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			try
			{
				// On Error GoTo ErrorHandler
				if (KeyAscii == Keys.Escape)
				{
					KeyAscii = (Keys)0;
					Close();
				}
				else if (KeyAscii == Keys.Return)
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_Form_KeyPress", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private bool JobComplete()
		{
			bool JobComplete = false;
			try
			{
				// On Error GoTo ErrorHandler
				foreach (Form ff in FCGlobal.Statics.Forms)
				{
					if (ff.Name == "rptWarrant")
					{
						JobComplete = false;
						return JobComplete;
					}
				}
				JobComplete = true;
				return JobComplete;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In frmWarrant_JobComplete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return JobComplete;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PageBreakBetweenJournalsOnReport = true;
				clsPostInfo.PostJournals();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (fraSelectReport.Visible == true)
			{
				Command1_Click(btnProcess, EventArgs.Empty);
			}
			else if (fraChoose.Visible == true)
			{
				cmdChoose_Click(btnProcess, EventArgs.Empty);
			}
			else
			{
				cmdProcess_Click(btnProcess, EventArgs.Empty);
			}
		}

		private void frmWarrant_Resize(object sender, EventArgs e)
		{
			fraChoose.CenterToContainer(this.ClientArea);
			fraSelectReport.CenterToContainer(this.ClientArea);
		}
	}
}
