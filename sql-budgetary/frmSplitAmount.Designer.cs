//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSplitAmount.
	/// </summary>
	partial class frmSplitAmount : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public FCGrid vsSplit;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtCheck;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtReference;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public fecherFoundation.FCFrame fraBorder;
		public fecherFoundation.FCComboBox cboPeriod;
		public fecherFoundation.FCComboBox cboJournal;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblRemaining;
		public fecherFoundation.FCLabel lblRemAmount;
		public fecherFoundation.FCLabel Shape1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblCheck;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplitAmount));
            this.fraJournalSave = new fecherFoundation.FCFrame();
            this.txtJournalDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelSave = new fecherFoundation.FCButton();
            this.cmdOKSave = new fecherFoundation.FCButton();
            this.cboSaveJournal = new fecherFoundation.FCComboBox();
            this.lblJournalDescription = new fecherFoundation.FCLabel();
            this.lblJournalSave = new fecherFoundation.FCLabel();
            this.lblSaveInstructions = new fecherFoundation.FCLabel();
            this.vsSplit = new fecherFoundation.FCGrid();
            this.txtAddress_0 = new Global.T2KOverTypeBox();
            this.txtVendor = new Global.T2KOverTypeBox();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.txtDescription = new Global.T2KOverTypeBox();
            this.txtReference = new Global.T2KOverTypeBox();
            this.txtAmount = new Global.T2KBackFillDecimal();
            this.txtAddress_1 = new Global.T2KOverTypeBox();
            this.txtAddress_2 = new Global.T2KOverTypeBox();
            this.txtAddress_3 = new Global.T2KOverTypeBox();
            this.fraBorder = new fecherFoundation.FCFrame();
            this.cboPeriod = new fecherFoundation.FCComboBox();
            this.cboJournal = new fecherFoundation.FCComboBox();
            this.lblJournal = new fecherFoundation.FCLabel();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.lblRemaining = new fecherFoundation.FCLabel();
            this.lblRemAmount = new fecherFoundation.FCLabel();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblExpense = new fecherFoundation.FCLabel();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.lblReference = new fecherFoundation.FCLabel();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.lblCheck = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
            this.fraJournalSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSplit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).BeginInit();
            this.fraBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 489);
            this.BottomPanel.Size = new System.Drawing.Size(1060, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraJournalSave);
            this.ClientArea.Controls.Add(this.fraBorder);
            this.ClientArea.Controls.Add(this.vsSplit);
            this.ClientArea.Controls.Add(this.txtAddress_0);
            this.ClientArea.Controls.Add(this.txtVendor);
            this.ClientArea.Controls.Add(this.txtCheck);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtReference);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtAddress_1);
            this.ClientArea.Controls.Add(this.txtAddress_2);
            this.ClientArea.Controls.Add(this.txtAddress_3);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.lblRemaining);
            this.ClientArea.Controls.Add(this.lblRemAmount);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblExpense);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.lblReference);
            this.ClientArea.Controls.Add(this.lblAmount);
            this.ClientArea.Controls.Add(this.lblCheck);
            this.ClientArea.Size = new System.Drawing.Size(1060, 429);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1060, 60);
            // 
            // HeaderText
            // 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(172, 30);
            this.HeaderText.Text = "AP Correction";
            // 
            // fraJournalSave
            // 
            this.fraJournalSave.BackColor = System.Drawing.SystemColors.Window;
            this.fraJournalSave.Controls.Add(this.txtJournalDescription);
            this.fraJournalSave.Controls.Add(this.cmdCancelSave);
            this.fraJournalSave.Controls.Add(this.cmdOKSave);
            this.fraJournalSave.Controls.Add(this.cboSaveJournal);
            this.fraJournalSave.Controls.Add(this.lblJournalDescription);
            this.fraJournalSave.Controls.Add(this.lblJournalSave);
            this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
            this.fraJournalSave.Location = new System.Drawing.Point(30, 20);
            this.fraJournalSave.Name = "fraJournalSave";
            this.fraJournalSave.Size = new System.Drawing.Size(826, 245);
			this.fraJournalSave.TabIndex = 0;
            this.fraJournalSave.Text = "Save Journal";
            this.fraJournalSave.Visible = false;
            // 
            // txtJournalDescription
            // 
			this.txtJournalDescription.AutoSize = false;
            this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournalDescription.LinkItem = null;
			this.txtJournalDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtJournalDescription.LinkTopic = null;
            this.txtJournalDescription.Location = new System.Drawing.Point(170, 125);
            this.txtJournalDescription.MaxLength = 100;
            this.txtJournalDescription.Name = "txtJournalDescription";
            this.txtJournalDescription.Size = new System.Drawing.Size(463, 40);
            this.txtJournalDescription.TabIndex = 2;
            // 
            // cmdCancelSave
            // 
            this.cmdCancelSave.AppearanceKey = "actionButton";
            this.cmdCancelSave.Location = new System.Drawing.Point(409, 186);
            this.cmdCancelSave.Name = "cmdCancelSave";
            this.cmdCancelSave.Size = new System.Drawing.Size(80, 40);
            this.cmdCancelSave.TabIndex = 6;
            this.cmdCancelSave.Text = "Cancel";
            this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
            // 
            // cmdOKSave
            // 
            this.cmdOKSave.AppearanceKey = "actionButton";
            this.cmdOKSave.Location = new System.Drawing.Point(337, 186);
            this.cmdOKSave.Name = "cmdOKSave";
            this.cmdOKSave.Size = new System.Drawing.Size(52, 40);
            this.cmdOKSave.TabIndex = 4;
            this.cmdOKSave.Text = "OK";
            this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
            // 
            // cboSaveJournal
            // 
			this.cboSaveJournal.AutoSize = false;
            this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboSaveJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSaveJournal.FormattingEnabled = true;
            this.cboSaveJournal.Location = new System.Drawing.Point(170, 65);
            this.cboSaveJournal.Name = "cboSaveJournal";
            this.cboSaveJournal.Size = new System.Drawing.Size(463, 40);
            this.cboSaveJournal.TabIndex = 1;
            this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
            this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
            // 
            // lblJournalDescription
            // 
            this.lblJournalDescription.AutoSize = true;
            this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalDescription.Location = new System.Drawing.Point(20, 139);
            this.lblJournalDescription.Name = "lblJournalDescription";
            this.lblJournalDescription.Size = new System.Drawing.Size(92, 15);
            this.lblJournalDescription.TabIndex = 7;
            this.lblJournalDescription.Text = "DESCRIPTION";
            this.lblJournalDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJournalSave
            // 
            this.lblJournalSave.AutoSize = true;
            this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalSave.Location = new System.Drawing.Point(20, 79);
            this.lblJournalSave.Name = "lblJournalSave";
            this.lblJournalSave.Size = new System.Drawing.Size(65, 15);
            this.lblJournalSave.TabIndex = 5;
            this.lblJournalSave.Text = "JOURNAL";
            this.lblJournalSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaveInstructions
            // 
            this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
            this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblSaveInstructions.Name = "lblSaveInstructions";
            this.lblSaveInstructions.Size = new System.Drawing.Size(800, 15);
            this.lblSaveInstructions.TabIndex = 3;
            this.lblSaveInstructions.Text = "PLEASE SELECT THE JOURNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION F" +
    "OR THE JOURNAL, AND CLICK THE OK BUTTON";
            this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vsSplit
            // 
			this.vsSplit.AllowSelection = false;
			this.vsSplit.AllowUserToResizeColumns = false;
			this.vsSplit.AllowUserToResizeRows = false;
            this.vsSplit.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsSplit.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSplit.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSplit.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSplit.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSplit.BackColorSel = System.Drawing.Color.Empty;
            this.vsSplit.Cols = 4;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSplit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsSplit.ColumnHeadersHeight = 30;
			this.vsSplit.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSplit.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsSplit.DragIcon = null;
            this.vsSplit.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsSplit.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsSplit.Enabled = false;
            this.vsSplit.FixedCols = 0;
			this.vsSplit.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSplit.FrozenCols = 0;
			this.vsSplit.GridColor = System.Drawing.Color.Empty;
			this.vsSplit.GridColorFixed = System.Drawing.Color.Empty;
            this.vsSplit.Location = new System.Drawing.Point(30, 557);
            this.vsSplit.Name = "vsSplit";
            
            this.vsSplit.RowHeadersVisible = false;
			this.vsSplit.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSplit.RowHeightMin = 0;
            this.vsSplit.Rows = 3;
            this.vsSplit.Size = new System.Drawing.Size(890, 152);
            this.vsSplit.ScrollTipText = null;
			this.vsSplit.ShowColumnVisibilityMenu = false;
			this.vsSplit.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsSplit.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsSplit.TabIndex = 33;
            this.vsSplit.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsSplit_KeyDownEdit);
            this.vsSplit.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsSplit_ChangeEdit);
            this.vsSplit.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsSplit_ValidateEdit);
            this.vsSplit.CurrentCellChanged += new System.EventHandler(this.vsSplit_RowColChange);
            this.vsSplit.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSplit_KeyDownEvent);
            // 
            // txtAddress_0
            // 
			this.txtAddress_0.AutoSize = false;
            this.txtAddress_0.Enabled = false;
			this.txtAddress_0.LinkItem = null;
			this.txtAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_0.LinkTopic = null;
            this.txtAddress_0.Location = new System.Drawing.Point(640, 30);
            this.txtAddress_0.MaxLength = 35;
            this.txtAddress_0.Name = "txtAddress_0";
            this.txtAddress_0.Size = new System.Drawing.Size(362, 40);
            this.txtAddress_0.TabIndex = 12;
            // 
            // txtVendor
            // 
			this.txtVendor.AutoSize = false;
            this.txtVendor.Enabled = false;
			this.txtVendor.LinkItem = null;
			this.txtVendor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVendor.LinkTopic = null;
            this.txtVendor.Location = new System.Drawing.Point(547, 30);
            this.txtVendor.MaxLength = 5;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new System.Drawing.Size(83, 40);
            this.txtVendor.TabIndex = 13;
            // 
            // txtCheck
            // 
			this.txtCheck.AutoSize = false;
            this.txtCheck.Enabled = false;
			this.txtCheck.LinkItem = null;
			this.txtCheck.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheck.LinkTopic = null;
            this.txtCheck.Location = new System.Drawing.Point(386, 230);
            this.txtCheck.MaxLength = 6;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(133, 40);
            this.txtCheck.TabIndex = 14;
            // 
            // txtDescription
            // 
			this.txtDescription.AutoSize = false;
            this.txtDescription.Enabled = false;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(153, 130);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(366, 40);
            this.txtDescription.TabIndex = 15;
            // 
            // txtReference
            // 
			this.txtReference.AutoSize = false;
            this.txtReference.Enabled = false;
			this.txtReference.LinkItem = null;
			this.txtReference.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReference.LinkTopic = null;
            this.txtReference.Location = new System.Drawing.Point(153, 180);
            this.txtReference.MaxLength = 15;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(366, 40);
            this.txtReference.TabIndex = 16;
            // 
            // txtAmount
            // 
            this.txtAmount.Enabled = false;
            this.txtAmount.Location = new System.Drawing.Point(153, 230);
            this.txtAmount.MaxLength = 14;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(108, 40);
            this.txtAmount.TabIndex = 18;
			this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtAddress_1
            // 
			this.txtAddress_1.AutoSize = false;
            this.txtAddress_1.Enabled = false;
			this.txtAddress_1.LinkItem = null;
			this.txtAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_1.LinkTopic = null;
            this.txtAddress_1.Location = new System.Drawing.Point(640, 80);
            this.txtAddress_1.MaxLength = 35;
            this.txtAddress_1.Name = "txtAddress_1";
            this.txtAddress_1.Size = new System.Drawing.Size(362, 40);
            this.txtAddress_1.TabIndex = 19;
            // 
            // txtAddress_2
            // 
			this.txtAddress_2.AutoSize = false;
            this.txtAddress_2.Enabled = false;
			this.txtAddress_2.LinkItem = null;
			this.txtAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_2.LinkTopic = null;
            this.txtAddress_2.Location = new System.Drawing.Point(640, 130);
            this.txtAddress_2.MaxLength = 35;
            this.txtAddress_2.Name = "txtAddress_2";
            this.txtAddress_2.Size = new System.Drawing.Size(362, 40);
            this.txtAddress_2.TabIndex = 20;
            // 
            // txtAddress_3
            // 
			this.txtAddress_3.AutoSize = false;
            this.txtAddress_3.Enabled = false;
			this.txtAddress_3.LinkItem = null;
			this.txtAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_3.LinkTopic = null;
            this.txtAddress_3.Location = new System.Drawing.Point(640, 180);
            this.txtAddress_3.MaxLength = 35;
            this.txtAddress_3.Name = "txtAddress_3";
            this.txtAddress_3.Size = new System.Drawing.Size(362, 40);
            this.txtAddress_3.TabIndex = 21;
            // 
            // fraBorder
            // 
            this.fraBorder.AppearanceKey = "groupBoxNoBorders";
            this.fraBorder.Controls.Add(this.cboPeriod);
            this.fraBorder.Controls.Add(this.cboJournal);
            this.fraBorder.Controls.Add(this.lblJournal);
            this.fraBorder.Controls.Add(this.lblPeriod);
            this.fraBorder.Location = new System.Drawing.Point(30, 30);
            this.fraBorder.Name = "fraBorder";
            this.fraBorder.Size = new System.Drawing.Size(388, 90);
            this.fraBorder.TabIndex = 8;
            // 
            // cboPeriod
            // 
			this.cboPeriod.AutoSize = false;
            this.cboPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cboPeriod.Location = new System.Drawing.Point(123, 50);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(265, 40);
            this.cboPeriod.TabIndex = 36;
            this.cboPeriod.Enter += new System.EventHandler(this.cboPeriod_Enter);
            // 
            // cboJournal
            // 
			this.cboJournal.AutoSize = false;
            this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournal.FormattingEnabled = true;
            this.cboJournal.Location = new System.Drawing.Point(123, 0);
            this.cboJournal.Name = "cboJournal";
            this.cboJournal.Size = new System.Drawing.Size(265, 40);
            this.cboJournal.TabIndex = 9;
            this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
            this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
            this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
            // 
            // lblJournal
            // 
            this.lblJournal.AutoSize = true;
            this.lblJournal.Location = new System.Drawing.Point(0, 14);
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Size = new System.Drawing.Size(65, 15);
            this.lblJournal.TabIndex = 11;
            this.lblJournal.Text = "JOURNAL";
            this.lblJournal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(0, 64);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(101, 15);
            this.lblPeriod.TabIndex = 10;
            this.lblPeriod.Text = "ACCTG PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCity
            // 
			this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Enabled = false;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(641, 230);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(133, 40);
            this.txtCity.TabIndex = 28;
            // 
            // txtState
            // 
			this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Enabled = false;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(784, 230);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(55, 40);
            this.txtState.TabIndex = 29;
            // 
            // txtZip
            // 
			this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Enabled = false;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(849, 230);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(76, 40);
            this.txtZip.TabIndex = 30;
            // 
            // txtZip4
            // 
			this.txtZip4.AutoSize = false;
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Enabled = false;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
            this.txtZip4.Location = new System.Drawing.Point(935, 230);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(67, 40);
            this.txtZip4.TabIndex = 31;
            // 
            // vs1
            // 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
            this.vs1.Cols = 10;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.vs1.ColumnHeadersHeight = 35;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle8;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
	
            this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
	
            this.vs1.Location = new System.Drawing.Point(30, 280);
            this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
            this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
            this.vs1.Rows = 16;
            this.vs1.Size = new System.Drawing.Size(890, 192);
            this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 17;
            this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
            this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
            // 
            // lblRemaining
            // 
            this.lblRemaining.AutoSize = true;
            this.lblRemaining.Location = new System.Drawing.Point(39, 503);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(134, 15);
            this.lblRemaining.TabIndex = 35;
            this.lblRemaining.Text = "REMAINING AMOUNT";
            this.lblRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRemAmount
            // 
            this.lblRemAmount.Location = new System.Drawing.Point(162, 502);
            this.lblRemAmount.Name = "lblRemAmount";
            this.lblRemAmount.Size = new System.Drawing.Size(90, 16);
            this.lblRemAmount.TabIndex = 34;
            this.lblRemAmount.Text = "0.00";
            this.lblRemAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Shape1
            // 
            this.Shape1.BorderStyle = 1;
            this.Shape1.Location = new System.Drawing.Point(30, 493);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(231, 33);
            this.Shape1.TabIndex = 36;
            this.Shape1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 532);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(99, 15);
            this.Label1.TabIndex = 32;
            this.Label1.Text = "SPLIT AMOUNT";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExpense
            // 
            this.lblExpense.Location = new System.Drawing.Point(30, 720);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(500, 18);
            this.lblExpense.TabIndex = 27;
            this.lblExpense.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblVendor
            // 
            this.lblVendor.AutoSize = true;
            this.lblVendor.Location = new System.Drawing.Point(459, 44);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(60, 15);
            this.lblVendor.TabIndex = 26;
            this.lblVendor.Text = "VENDOR";
            this.lblVendor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(30, 144);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(92, 15);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "DESCRIPTION";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReference
            // 
            this.lblReference.AutoSize = true;
            this.lblReference.Location = new System.Drawing.Point(30, 194);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(84, 15);
            this.lblReference.TabIndex = 24;
            this.lblReference.Text = "REFERENCE";
            this.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(30, 244);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(61, 15);
            this.lblAmount.TabIndex = 23;
            this.lblAmount.Text = "AMOUNT";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCheck
            // 
            this.lblCheck.AutoSize = true;
            this.lblCheck.Location = new System.Drawing.Point(299, 244);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(58, 15);
            this.lblCheck.TabIndex = 22;
            this.lblCheck.Text = "CHECK#";
            this.lblCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(353, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(100, 48);
			this.btnProcessSave.TabIndex = 0;
            this.btnProcessSave.Text = "Save";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmSplitAmount
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1060, 597);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSplitAmount";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "AP Correction";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmSplitAmount_Load);
            this.Activated += new System.EventHandler(this.frmSplitAmount_Activated);
            this.Resize += new System.EventHandler(this.frmSplitAmount_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSplitAmount_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSplitAmount_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
            this.fraJournalSave.ResumeLayout(false);
            this.fraJournalSave.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSplit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).EndInit();
            this.fraBorder.ResumeLayout(false);
            this.fraBorder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcessSave;
	}
}