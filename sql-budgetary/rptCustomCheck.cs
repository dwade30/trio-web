﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCustomCheck.
	/// </summary>
	public partial class rptCustomCheck : BaseSectionReport
	{
		public static rptCustomCheck InstancePtr
		{
			get
			{
				return (rptCustomCheck)Sys.GetInstance(typeof(rptCustomCheck));
			}
		}

		protected rptCustomCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                rsVendorInfo_AutoInitialized?.Dispose();
				rsJournalInfo_AutoInitialized?.Dispose();
				rsPrePaidJournalInfo_AutoInitialized?.Dispose();
				rsCheckInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int lngCheckBank;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//public clsDRWrapper rsVendorInfo = new clsDRWrapper();
		//public clsDRWrapper rsJournalInfo = new clsDRWrapper();
		//public clsDRWrapper rsPrePaidJournalInfo = new clsDRWrapper();
		public clsDRWrapper rsVendorInfo_AutoInitialized;

		public clsDRWrapper rsVendorInfo
		{
			get
			{
				if (rsVendorInfo_AutoInitialized == null)
				{
					rsVendorInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsVendorInfo_AutoInitialized;
			}
			set
			{
				rsVendorInfo_AutoInitialized = value;
			}
		}

		public clsDRWrapper rsJournalInfo_AutoInitialized;

		public clsDRWrapper rsJournalInfo
		{
			get
			{
				if (rsJournalInfo_AutoInitialized == null)
				{
					rsJournalInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsJournalInfo_AutoInitialized;
			}
			set
			{
				rsJournalInfo_AutoInitialized = value;
			}
		}

		public clsDRWrapper rsPrePaidJournalInfo_AutoInitialized;

		public clsDRWrapper rsPrePaidJournalInfo
		{
			get
			{
				if (rsPrePaidJournalInfo_AutoInitialized == null)
				{
					rsPrePaidJournalInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsPrePaidJournalInfo_AutoInitialized;
			}
			set
			{
				rsPrePaidJournalInfo_AutoInitialized = value;
			}
		}

		bool blnFirstRecord;
		public int lngcheck;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		public Decimal curTotal;
		// vbPorter upgrade warning: curCarryOver As Decimal	OnWrite(Decimal, short)
		public Decimal curCarryOver;
		public bool blnCarryOver;
		public bool blnEFTCheck;
		int lngCheckToReprint;
		int lngLastCheckToReprint;
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		int lngLowCheck;
		int lngHighCheck;
		public string strCheckType = "";
		string strCheckPosition = "";
		public string strFooterVendorName = "";
		public string strFooterAddress1 = "";
		public string strFooterAddress2 = "";
		public string strFooterAddress3 = "";
		public string strFooterAddress4 = "";
		string strFooterCity = "";
		string strFooterState = "";
		string strFooterZip = "";
		string strFooterZip4 = "";
		public string strFooterJournals = "";
		// vbPorter upgrade warning: curFooterAmount As Decimal	OnWrite(short, Decimal)
		public Decimal curFooterAmount;
		public int lngFooterVendorNumber;
		public int lngFooterCheck;
		bool blnDoubleStub;
		bool blnTwoStubs;
		int intCounterLimit;
		string strSQL = "";
		//clsReportPrinterFunctions instReportFunctions = new clsReportPrinterFunctions();
		bool blnDonePreview;
		bool blnPrinting;
		public bool blnIndividualCheck;
		bool blnInitial;
		int lngWarrant;
		// vbPorter upgrade warning: intPeriod As short --> As int	OnWrite(int, string)
		int intPeriod;
		// vbPorter upgrade warning: datPayDate As DateTime	OnWrite(DateTime, string)
		public DateTime datPayDate;
		bool blnUnprintedChecks;
		int lngFirstCheck;
		int lngFirstReprintedCheck;
		int lngLastReprintedCheck;
		public string strCheckMessage = "";
		public string strRoutingNumber = string.Empty;
		public string strBankAccountNumber = string.Empty;
		public string strBankName = string.Empty;
		bool blnEND;
        bool IsFirstExecuting = true;

		public rptCustomCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Checks";
		}

		public int CheckBankID
		{
			set
			{
				lngCheckBank = value;
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
            //FC:FINAL:MSH - issue #1585: exit from handler if it has already executed from ReportStart (in original app first will be executed DataInitialize)
            if (!IsFirstExecuting)
            {
                return;
            }
            IsFirstExecuting = false;

            int intReturn = 0;
            using (clsDRWrapper rsCheckFormat = new clsDRWrapper())
            {
                rsCheckFormat.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " +
                                            FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
                if (rsCheckFormat.EndOfFile() != true && rsCheckFormat.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckType")) == "L" ||
                        FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckType")) == "M")
                    {
                        strCheckType = FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckType"));
                    }
                    else
                    {
                        MessageBox.Show("You must go save custom check settings before you may proceed.",
                            "Invalid Custom Check Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Cancel();
                        return;
                    }

                    if (FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckPosition")) == "T" ||
                        FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckPosition")) == "M" ||
                        FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckPosition")) == "B")
                    {
                        strCheckPosition = FCConvert.ToString(rsCheckFormat.Get_Fields_String("CheckPosition"));
                    }
                    else
                    {
                        MessageBox.Show("You must go save custom check settings before you may proceed.",
                            "Invalid Custom Check Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Cancel();
                        return;
                    }

                    if (FCConvert.ToBoolean(rsCheckFormat.Get_Fields_Boolean("UseDoubleStub")))
                    {
                        blnDoubleStub = true;
                        intCounterLimit = 19;
                    }
                    else
                    {
                        blnDoubleStub = false;
                        intCounterLimit = 8;
                    }

                    if (FCConvert.ToBoolean(rsCheckFormat.Get_Fields_Boolean("UseTwoStubs")))
                    {
                        blnTwoStubs = true;
                    }
                    else
                    {
                        blnTwoStubs = false;
                    }
                }

                if (strCheckType == "D" && !blnTwoStubs)
                {

                    this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("StandardCheckSize",
                        FCConvert.ToInt32(this.PageSettings.PaperWidth * 100),
                        FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));

                }

                modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = false;
                blnPrinting = true;
            }
        }
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void ActiveReport_QueryClose(ref short Cancel, ref short CloseMode)
		{
			if (!blnDonePreview && blnPrinting && !blnEND)
			{
				MessageBox.Show("Checks have not finished processing.", "Checks Not Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel = FCConvert.ToInt16(true);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			clsDRWrapper rsAPJournals = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsAPJournals.OmitNullsOnInsert = true;
			if (!blnIndividualCheck)
			{
				if (blnInitial)
				{
					rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'V' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(lngWarrant) + "')");
					if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
					{
						do
						{
							rsMasterJournal.Edit();
							rsMasterJournal.Set_Fields("Status", "C");
							rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
							rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
							rsMasterJournal.Set_Fields("CheckDate", datPayDate);
							rsMasterJournal.Update(true);
							rsMasterJournal.MoveNext();
						}
						while (rsMasterJournal.EndOfFile() != true);
						rsMasterJournal.MoveFirst();
						do
						{
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							rsAPJournals.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + rsMasterJournal.Get_Fields("JournalNumber"));
							if (rsAPJournals.EndOfFile() != true && rsAPJournals.BeginningOfFile() != true)
							{
								rsAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE ID = 0");
								rsAPJournals.AddNew();
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("JournalNumber", rsMasterJournal.Get_Fields("JournalNumber"));
								rsAPJournals.Set_Fields("Description", rsMasterJournal.Get_Fields_String("Description"));
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
								rsAPJournals.Set_Fields("Status", "E");
								rsAPJournals.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
								rsAPJournals.Set_Fields("StatusChangeDate", DateTime.Today);
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Period", rsMasterJournal.Get_Fields("Period"));
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
								rsAPJournals.Update(true);
							}
							rsMasterJournal.MoveNext();
						}
						while (rsMasterJournal.EndOfFile() != true);
					}
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CarryOver = 1 ORDER BY CheckNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsMasterJournal.Execute("UPDATE APJournal SET CheckNumber = '" + FCConvert.ToString(FCConvert.ToInt32(rsInfo.Get_Fields("CheckNumber")) + 1) + "' WHERE CheckNumber = '" + rsInfo.Get_Fields("CheckNumber") + "' AND CheckDate = '" + rsInfo.Get_Fields_DateTime("CheckDate") + "' AND Warrant = '" + rsInfo.Get_Fields_Int32("WarrantNumber") + "'", "Budgetary");
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			rsCheckInfo.OpenRecordset("SELECT * FROM LastChecksRun");
			if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
			{
				rsCheckInfo.Edit();
			}
			else
			{
				rsCheckInfo.OmitNullsOnInsert = true;
				rsCheckInfo.AddNew();
			}
			rsCheckInfo.Set_Fields("StartCheckNumber", lngLowCheck);
			rsCheckInfo.Set_Fields("EndCheckNumber", lngHighCheck);
			rsCheckInfo.Update(true);
			if (lngCheckBank > 0)
			{
				cBankService bServ = new cBankService();
				bServ.UpdateLastCheckNumberForBankForCorrectCheckType(lngCheckBank, lngHighCheck, "AP");
			}
			if (blnUnprintedChecks == false)
			{
				if (blnIndividualCheck)
				{
					frmPrintIndividualChecks.InstancePtr.Unload();
				}
				else
				{
					frmPrintChecks.InstancePtr.Unload();
				}
			}
			blnDonePreview = true;
			rsMasterJournal.Dispose();
			rsAPJournals.Dispose();
			rsInfo.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            //FC:FINAL:MSH - issue #1585: DataInitialize should be executed before ReportStart (as in original app)
            ActiveReport_DataInitialize(null, null);
            
            int counter;
			double dblAdjust = 0;
			clsDRWrapper rsIcon = new clsDRWrapper();
			int cnt;
			int const_PrintToolID;
			// vbPorter upgrade warning: intResponse As int --> As DialogResult
			int intResponse;
			string strSigFile = "";
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			const_PrintToolID = 9950;
			
			blnDonePreview = false;

			blnFirstRecord = true;
			rsIcon.OpenRecordset("SELECT * FROM Budgetary");
			blnEND = false;
			if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("PrintSignature")))
			{
				// must get password
				ans = MessageBox.Show("Do you want a signature to print on the checks?", "Print Signature?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					TryPassword:
					;
					intResponse = frmSignaturePassword.InstancePtr.Init(modSignatureFile.BUDGETARYSIG);
					if (intResponse > 0)
					{
						strSigFile = modSignatureFile.GetSigFileFromID(ref intResponse);
						//FC:FINAL:CHN - issue #1250: Incorrect file path. 
						//if (File.Exists(strSigFile))
						if (File.Exists(Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, strSigFile)))
						{
							modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = true;
							modBudgetaryMaster.Statics.lngSignatureID = intResponse;
						}
						else
						{
							ans = MessageBox.Show("Signature File is Missing. Print checks without signature?", "Print Checks?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.No)
							{
								blnEND = true;
								this.Cancel();
								return;
							}
							else
							{
								modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = true;
								modBudgetaryMaster.Statics.lngSignatureID = 0;
							}
						}
					}
					else
					{
						intResponse = FCConvert.ToInt32(MessageBox.Show("Incorrect Password. Re-enter password?", "Incorrect Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
						if (intResponse == FCConvert.ToInt32(DialogResult.Yes))
							goto TryPassword;
					}
				}
			}
			if (strCheckType == "D" && !blnTwoStubs)
			{
				SubReport3.Report = null;
				SubReport3.Visible = false;
			}
			else if (blnDoubleStub)
			{
				if (strCheckPosition == "T")
				{
					SubReport3.Report = null;
					SubReport3.Visible = false;
				}
				else
				{
					SubReport2.Report = null;
					SubReport2.Visible = false;
				}
			}
			if (blnIndividualCheck)
			{
				blnInitial = frmPrintIndividualChecks.InstancePtr.cmbInitial.SelectedIndex == 0;
				lngWarrant = 0;
				intPeriod = DateTime.Today.Month;
				datPayDate = DateTime.Today;
				blnUnprintedChecks = false;
				lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.txtCheck.Text));
				if (!blnInitial)
				{
					lngFirstReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 1)));
					lngLastReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 1)));
				}
				else
				{
					lngFirstReprintedCheck = 0;
					lngLastReprintedCheck = 0;
				}
				strCheckMessage = frmPrintIndividualChecks.InstancePtr.txtCheckMessage.Text;
			}
			else
			{
				blnInitial = frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0;
				lngWarrant = frmPrintChecks.InstancePtr.TempWarrant;
       			intPeriod = -1;
				datPayDate = FCConvert.ToDateTime(frmPrintChecks.InstancePtr.txtPayDate.Text);
				blnUnprintedChecks = frmPrintChecks.InstancePtr.blnPrintingUnprintedChecks;
				if (blnInitial)
				{
					lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtCheck.Text));
				}
				else
				{
					lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtFirstCheck.Text));
				}
				if (!blnInitial)
				{
					lngFirstReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtFirstReprintCheck.Text));
					lngLastReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtLastReprintCheck.Text));
				}
				else
				{
					lngFirstReprintedCheck = 0;
					lngLastReprintedCheck = 0;
				}
				strCheckMessage = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
			}
			if (blnInitial)
			{
				lngcheck = lngFirstCheck;
				lngLowCheck = lngcheck;
				if (blnIndividualCheck)
				{
					if (FCUtils.CBool(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 3)) == false)
					{
						strSQL = "APJournal.JournalNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsJournals.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsJournals.Row, 0)))) + " AND APJournal.VendorNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 1)))) + " AND Isnull(Seperate, 0) = 0 ";
					}
					else
					{
						strSQL = "APJournal.ID = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 0)))) + " ";
					}
				}
				else
				{
					strSQL = "APJournal.JournalNumber IN (";
					for (counter = 1; counter <= frmPrintChecks.InstancePtr.vsJournals.Rows - 1; counter++)
					{
						if (FCUtils.CBool(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 0)) == true)
						{
							strSQL += FCConvert.ToString(Conversion.Val(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 1))) + ", ";
						}
					}
					strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") AND APJournal.Payable <= '" + FCConvert.ToString(datPayDate) + "' ";
				}
				FillTemp();
			}
			else
			{
				lngcheck = lngFirstCheck;
				lngLowCheck = lngcheck;
				lngCheckToReprint = lngFirstReprintedCheck;
				lngLastCheckToReprint = lngLastReprintedCheck;
				if (blnIndividualCheck)
				{
					rsVendorInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + FCConvert.ToString(lngCheckToReprint) + " AND VendorNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 2)))) + " AND WarrantNumber = 0 ORDER BY CheckNumber");
				}
				else
				{
					rsVendorInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE Status = 'R' AND CheckNumber >= " + FCConvert.ToString(lngCheckToReprint) + " ORDER BY CheckNumber");
				}
			}
			if (strCheckType != "D")
			{
				dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("CHECKLASERADJUSTMENT"));
				foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
				{
					ControlName.Top += FCConvert.ToSingle(240 * dblAdjust / 1440f);
				}
			}
			rsIcon.Dispose();
		}
		
		private void FillTemp()
		{
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
			// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
			strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 ORDER BY TempVendorName";
			// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorNumber";
				}
				else
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorName";
				}
			}
			else
			{
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorNumber";
			}
			rsVendorInfo.OpenRecordset(strTemp);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            using (clsDRWrapper rsVendorData = new clsDRWrapper())
            {
                if (blnInitial)
                {
                    bool executeCheckJournal = false;
                    if (blnFirstRecord)
                    {
                        blnFirstRecord = false;
                        if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
                        {
                            rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL +
                                                        "AND VendorNumber = 0 AND TempVendorName = '" +
                                                        modCustomReport.FixQuotes(
                                                            rsVendorInfo.Get_Fields_String("VendorName")) +
                                                        "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
                            rsPrePaidJournalInfo.OpenRecordset(
                                "SELECT * FROM APJournal WHERE " + strSQL +
                                "AND VendorNumber = 0 AND TempVendorName = '" +
                                modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) +
                                "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
                        }
                        else
                        {
                            rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL +
                                                        "AND IsNull(SeperateCheckRecord, 0) = 0 AND VendorNumber = " +
                                                        rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                                        " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
                            rsPrePaidJournalInfo.OpenRecordset(
                                "SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " +
                                rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
                        }

                        executeCheckJournal = true;
                        goto CheckJournal;
                    }
                    else
                    {
                        executeCheckJournal = true;
                        goto CheckJournal;
                    }

                    CheckJournal: ;
                    if (executeCheckJournal)
                    {
                        SavePrePaidJournalInfo();
                        if (rsJournalInfo.EndOfFile() == true)
                        {
                            rsVendorInfo.MoveNext();
                            CheckNextVendor: ;
                            if (rsVendorInfo.EndOfFile() == true)
                            {
                                eArgs.EOF = true;
                                return;
                            }
                            else
                            {
                                if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
                                {
                                    rsJournalInfo.OpenRecordset(
                                        "SELECT * FROM APJournal WHERE " + strSQL +
                                        "AND VendorNumber = 0 AND TempVendorName = '" +
                                        modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) +
                                        "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
                                    rsPrePaidJournalInfo.OpenRecordset(
                                        "SELECT * FROM APJournal WHERE " + strSQL +
                                        "AND VendorNumber = 0 AND TempVendorName = '" +
                                        modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) +
                                        "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
                                }
                                else
                                {
                                    rsJournalInfo.OpenRecordset(
                                        "SELECT * FROM APJournal WHERE " + strSQL +
                                        "AND IsNull(SeperateCheckRecord, 0) = 0 AND VendorNumber = " +
                                        rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                        " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
                                    rsPrePaidJournalInfo.OpenRecordset(
                                        "SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " +
                                        rsVendorInfo.Get_Fields_Int32("VendorNumber") +
                                        " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC, ID");
                                }

                                SavePrePaidJournalInfo();
                                if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                                {
                                    eArgs.EOF = false;
                                    rsVendorData.OpenRecordset(
                                        "SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                        rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                                    if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
                                    {
                                        blnEFTCheck = FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) &&
                                                      !FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
                                    }

                                    return;
                                }
                                else
                                {
                                    rsVendorInfo.MoveNext();
                                    goto CheckNextVendor;
                                }
                            }
                        }
                        else
                        {
                            eArgs.EOF = false;
                            rsVendorData.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                                       rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                            if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
                            {
                                blnEFTCheck = FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) &&
                                              !FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
                            }

                            return;
                        }
                    }
                }
                else
                {
                    bool executeGetNextReprintVendor = false;
                    if (blnFirstRecord)
                    {
                        blnFirstRecord = false;
                        if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) != "P" ||
                            rsVendorInfo.Get_Fields_Boolean("PrintedIndividual") == true)
                        {
                            rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals"))
                                .Replace("Separate", "Seperate"));
                            if (intPeriod < 1)
                            {
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                intPeriod = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Period"));
                            }
                        }
                        else
                        {
                            executeGetNextReprintVendor = true;
                            goto GetNextReprintVendor;
                        }
                    }
                    else
                    {
                        if (rsJournalInfo.EndOfFile() == true)
                        {
                            executeGetNextReprintVendor = true;
                            goto GetNextReprintVendor;
                        }
                        else
                        {
                            eArgs.EOF = false;
                            rsVendorData.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                                       rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                            if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
                            {
                                blnEFTCheck = FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) &&
                                              !FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
                            }

                            return;
                        }
                    }

                    GetNextReprintVendor: ;
                    if (executeGetNextReprintVendor)
                    {
                        rsVendorInfo.MoveNext();
                        if (rsVendorInfo.EndOfFile() == true)
                        {
                            eArgs.EOF = true;
                            return;
                        }
                        // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                        else if (FCConvert.ToInt32(rsVendorInfo.Get_Fields("CheckNumber")) > lngLastCheckToReprint)
                        {
                            eArgs.EOF = true;
                            return;
                        }
                        else if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "P" &&
                                 FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("PrintedIndividual")) != true)
                        {
                            goto GetNextReprintVendor;
                        }
                        else
                        {
                            rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals"))
                                .Replace("Separate", "Seperate"));
                            if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                            {
                                if (intPeriod < 1)
                                {
                                    // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                    intPeriod = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Period"));
                                }

                                eArgs.EOF = false;
                                rsVendorData.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                                           rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                                if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
                                {
                                    blnEFTCheck = FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) &&
                                                  !FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
                                }

                                return;
                            }
                            else
                            {
                                goto GetNextReprintVendor;
                            }
                        }
                    }
                }
            }
        }

		private void SavePrePaidJournalInfo()
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			clsDRWrapper rsTotal = new clsDRWrapper();
			string strCheckSQL = "";
			//clsDRWrapper rsStatusChange = new clsDRWrapper();
			rsCheckInfo.OmitNullsOnInsert = true;
			while (rsPrePaidJournalInfo.EndOfFile() != true)
			{
				if (FCConvert.ToString(rsPrePaidJournalInfo.Get_Fields_String("Status")) == "V")
				{
					if (!FCConvert.ToBoolean(rsPrePaidJournalInfo.Get_Fields_Boolean("PrintedIndividual")))
					{
						bool executeAddNew = false;
						if (Information.IsDate(rsPrePaidJournalInfo.Get_Fields("CheckDate")))
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = " + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + " AND CheckDate = '" + rsPrePaidJournalInfo.Get_Fields_DateTime("CheckDate") + "'");
						}
						else
						{
							rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
							executeAddNew = true;
							goto AddNew;
						}
						if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
						{
							rsCheckInfo.Edit();
						}
						else
						{
							executeAddNew = true;
							goto AddNew;
						}
						AddNew:
						;
						if (executeAddNew)
						{
							rsCheckInfo.AddNew();
						}
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCheckInfo.Set_Fields("CheckNumber", rsPrePaidJournalInfo.Get_Fields("CheckNumber"));
						rsCheckInfo.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
						rsCheckInfo.Set_Fields("VendorName", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"))));
						rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))));
						rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"))));
						rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"))));
						rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"))));
						rsCheckInfo.Set_Fields("CheckState", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))));
						rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
						rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
						rsCheckInfo.Set_Fields("WarrantNumber", lngWarrant);
						rsTotal.OpenRecordset("SELECT SUM(Amount) as TotalAmt, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsPrePaidJournalInfo.Get_Fields_Int32("ID"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [TotalAmt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
						rsCheckInfo.Set_Fields("Amount", Conversion.Val(rsCheckInfo.Get_Fields("Amount")) + Conversion.Val(rsTotal.Get_Fields("TotalAmt")) - Conversion.Val(rsTotal.Get_Fields("TotalDiscount")));
						// rsCheckInfo.Fields["Amount"] = rsCheckInfo.Fields["Amount"] + Val(rsPrePaidJournalInfo.Fields["Amount"])
						rsCheckInfo.Set_Fields("CheckDate", datPayDate);
						strCheckSQL = rsPrePaidJournalInfo.Name();
						if (!blnInitial)
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
						}
						rsCheckInfo.Set_Fields("Journals", strCheckSQL);
						rsCheckInfo.Set_Fields("Status", "P");
						rsCheckInfo.Update(true);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = " + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = 0");
						if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
						{
							do
							{
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								UpdateIndividualCheck_8(FCConvert.ToInt32(rsCheckInfo.Get_Fields("CheckNumber")), FCConvert.ToInt32(rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber")));
								rsCheckInfo.MoveNext();
							}
							while (rsCheckInfo.EndOfFile() != true);
						}
					}
					if (!blnIndividualCheck)
					{
						rsPrePaidJournalInfo.Edit();
						rsPrePaidJournalInfo.Set_Fields("Status", "C");
						rsPrePaidJournalInfo.Set_Fields("Warrant", lngWarrant);
						rsPrePaidJournalInfo.Set_Fields("CheckDate", datPayDate);
						rsPrePaidJournalInfo.Update(true);
					}
				}
				rsPrePaidJournalInfo.MoveNext();
			}
			rsCheckInfo.Dispose();
			rsTotal.Dispose();
        }

		private void UpdateIndividualCheck_2(int lngTempCheck, int lngVendorNumber)
		{
			UpdateIndividualCheck(ref lngTempCheck, ref lngVendorNumber);
		}

		private void UpdateIndividualCheck_8(int lngTempCheck, int lngVendorNumber)
		{
			UpdateIndividualCheck(ref lngTempCheck, ref lngVendorNumber);
		}

		private void UpdateIndividualCheck(ref int lngTempCheck, ref int lngVendorNumber)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + FCConvert.ToString(lngVendorNumber) + " AND CheckNumber = " + FCConvert.ToString(lngTempCheck) + " AND WarrantNumber = 0");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsTemp.Get_Fields_String("OldCheckNumber")) != "")
				{
					if (FCConvert.ToInt32(rsTemp.Get_Fields_String("oldchecknumber")) != lngTempCheck)
					{
						UpdateIndividualCheck_2(FCConvert.ToInt32(rsTemp.Get_Fields_String("OldCheckNumber")), lngVendorNumber);
					}
				}
				rsTemp.Edit();
				rsTemp.Set_Fields("WarrantNumber", lngWarrant);
				rsTemp.Set_Fields("ReportNumber", 0);
				rsTemp.Update();
			}
			rsTemp.Dispose();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// If strCheckType = "D" Then
			// ResetDefaultPrinter
			// End If
		}
		//private void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "Print...")
		//	{
		//		if (blnDonePreview)
		//		{
		//			this.Document.Print(false);
		//		}
		//		else
		//		{
		//			MessageBox.Show("You may not print checks until they have all finished processing.", "Checks Not Finished", MessageBoxButtons.OK, MessageBoxIcon.Information, modal:false);
		//		}
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			string strCheckSQL = "";
			clsDRWrapper rsDupChecks = new clsDRWrapper();
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsCheckRecJournalInfo = new clsDRWrapper();
			int intBank = 0;
			rsCheckRec.OmitNullsOnInsert = true;
			if (rsVendorInfo.EndOfFile() != true)
			{
				curTotal = 0;
				curFooterAmount = 0;
				strFooterVendorName = "";
				lngFooterVendorNumber = 0;
				strFooterAddress1 = "";
				strFooterAddress2 = "";
				strFooterAddress3 = "";
				strFooterAddress4 = "";
				strFooterJournals = "";
				SetFieldsToPrint();
				if (strCheckPosition == "T")
				{
					SubReport1.Report = new srptCustomCheckBody();
					if (blnDoubleStub)
					{
						SubReport2.Height *= 2;
						SubReport2.Report = new srptCustomCheckDoubleStub();
					}
					else
					{
						SubReport2.Report = new srptCustomCheckStub();
					}
					if (strCheckType == "L" && !blnDoubleStub)
					{
						SubReport3.Report = new srptCustomCheckStub();
					}
				}
				else if (strCheckPosition == "M")
				{
					if (strCheckType == "M")
					{
						SubReport1.Report = new srptCustomCheckStub();
						SubReport2.Report = new srptCustomCheckBody();
						SubReport3.Report = new srptCustomCheckAddressStub();
					}
					else
					{
						SubReport1.Report = new srptCustomCheckStub();
						SubReport2.Report = new srptCustomCheckBody();
						SubReport3.Report = new srptCustomCheckStub();
					}
				}
				else
				{
					if (strCheckType == "L")
					{
						if (!blnDoubleStub)
						{
							SubReport1.Report = new srptCustomCheckStub();
							SubReport2.Report = new srptCustomCheckStub();
							SubReport3.Report = new srptCustomCheckBody();
						}
						else
						{
							SubReport1.Height *= 2;
							SubReport1.Report = new srptCustomCheckDoubleStub();
							SubReport3.Report = new srptCustomCheckBody();
						}
					}
					else
					{
						if (!blnTwoStubs)
						{
							SubReport1.Report = new srptCustomCheckStub();
							SubReport2.Report = new srptCustomCheckBody();
						}
						else
						{
							SubReport1.Report = new srptCustomCheckStub();
							SubReport2.Report = new srptCustomCheckStub();
							SubReport3.Report = new srptCustomCheckBody();
						}
					}
				}
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
				rsCheckInfo.AddNew();
				rsCheckInfo.Set_Fields("CheckNumber", lngcheck);
				rsCheckInfo.Set_Fields("VendorNumber", lngFooterVendorNumber);
				rsCheckInfo.Set_Fields("VendorName", Strings.Trim(strFooterVendorName));
				rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(strFooterAddress1));
				rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(strFooterAddress2));
				rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(strFooterAddress3));
				rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(strFooterCity));
				rsCheckInfo.Set_Fields("CheckState", Strings.Trim(strFooterState));
				rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(strFooterZip));
				rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(strFooterZip4));
				rsCheckInfo.Set_Fields("WarrantNumber", lngWarrant);
				rsCheckInfo.Set_Fields("Amount", curTotal);
				rsCheckInfo.Set_Fields("CheckDate", datPayDate);
				rsCheckInfo.Set_Fields("EFTCheck", blnEFTCheck);
				if (curCarryOver != 0)
				{
					rsCheckInfo.Set_Fields("CarryOver", true);
				}
				else
				{
					rsCheckInfo.Set_Fields("CarryOver", false);
				}
				strCheckSQL = strFooterJournals;
				if (!blnInitial)
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck) + "' ORDER BY IsNull(Seperate, 0) DESC";
					rsCheckInfo.Set_Fields("ReprintedCheck", true);
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsCheckInfo.Set_Fields("OldCheckNumber", rsVendorInfo.Get_Fields("CheckNumber"));
					rsCheckInfo.Set_Fields("ReportNumber", rsVendorInfo.Get_Fields_Int32("ReportNumber"));
					if (!blnIndividualCheck)
					{
						rsCheckRecJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE convert(int, IsNull(CheckNumber, 0)) = " + FCConvert.ToString(lngcheck) + " AND Warrant = '" + FCConvert.ToString(lngWarrant) + "'");
						if (rsCheckRecJournalInfo.EndOfFile() != true && rsCheckRecJournalInfo.BeginningOfFile() != true)
						{
							if (FCConvert.ToString(rsCheckRecJournalInfo.Get_Fields_String("Status")) != "C")
							{
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsVendorInfo.Get_Fields("CheckNumber"));
								if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
								{
									rsCheckRec.Edit();
									// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
									intBank = FCConvert.ToInt32(rsCheckRec.Get_Fields("BankNumber"));
									rsCheckRec.Set_Fields("Status", "V");
									rsCheckRec.Update();
									rsCheckRec.AddNew();
									rsCheckRec.Set_Fields("CheckNumber", lngcheck);
									rsCheckRec.Set_Fields("Type", "1");
									rsCheckRec.Set_Fields("CheckDate", datPayDate);
									rsCheckRec.Set_Fields("Name", Strings.Trim(strFooterVendorName));
									rsCheckRec.Set_Fields("Amount", curTotal);
									if (curCarryOver != 0)
									{
										rsCheckRec.Set_Fields("Status", "V");
									}
									else
									{
										rsCheckRec.Set_Fields("Status", "1");
									}
									rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
									rsCheckRec.Set_Fields("BankNumber", intBank);
									rsCheckRec.Update(true);
								}
							}
						}
					}
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck) + "' ORDER BY IsNull(Seperate, 0) DESC";
					rsCheckInfo.Set_Fields("ReprintedCheck", false);
					rsCheckInfo.Set_Fields("OldCheckNumber", "");
					if (blnIndividualCheck)
					{
						rsCheckInfo.Set_Fields("ReportNumber", -1);
					}
					else
					{
						rsCheckInfo.Set_Fields("ReportNumber", 0);
					}
				}
				rsCheckInfo.Set_Fields("Journals", strCheckSQL);
				if (curTotal != 0)
				{
					if (blnIndividualCheck)
					{
						rsCheckInfo.Set_Fields("Status", "P");
					}
					else
					{
						rsCheckInfo.Set_Fields("Status", "R");
					}
				}
				else
				{
					rsCheckInfo.Set_Fields("Status", "V");
				}
				if (blnIndividualCheck)
				{
					rsCheckInfo.Set_Fields("PrintedIndividual", true);
				}
				rsCheckInfo.Update(true);
				if (lngFooterVendorNumber != 0)
				{
					rsCheckInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(lngFooterVendorNumber));
					if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
					{
						rsCheckInfo.Edit();
						rsCheckInfo.Set_Fields("LastPayment", DateTime.Today);
						rsCheckInfo.Set_Fields("CheckNumber", lngcheck);
						rsCheckInfo.Update();
					}
				}
				if (!blnInitial)
				{
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsDupChecks.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsVendorInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = " + FCConvert.ToString(lngWarrant) + " AND ID <> " + rsVendorInfo.Get_Fields_Int32("ID"));
					if (rsDupChecks.EndOfFile() != true && rsDupChecks.BeginningOfFile() != true)
					{
						rptCustomCheck.InstancePtr.rsVendorInfo.Delete();
						rptCustomCheck.InstancePtr.rsVendorInfo.Update();
					}
					else
					{
						rptCustomCheck.InstancePtr.rsVendorInfo.Edit();
						rptCustomCheck.InstancePtr.rsVendorInfo.Set_Fields("Status", "V");
						rptCustomCheck.InstancePtr.rsVendorInfo.Update();
					}
				}
				lngHighCheck = lngcheck;
				lngcheck += 1;
			}
			rsDupChecks.Dispose();
			rsCheckRec.Dispose();
			rsCheckRecJournalInfo.Dispose();
        }

		private void SetFieldsToPrint()
		{
			int counter;
			int counter2 = 0;
			clsDRWrapper rsDiscount = new clsDRWrapper();
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			clsDRWrapper rsSepCred = new clsDRWrapper();
			//Application.DoEvents();
			curTotal = curCarryOver;
			for (counter = 0; counter <= intCounterLimit; counter++)
			{
				if (blnInitial)
				{
					if (rsJournalInfo.EndOfFile() != true)
					{
						rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						if (Conversion.Val(rsJournalInfo.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							FillData_2184(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curTotal += FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
						}
						else
						{
							rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							FillData_2184(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							curTotal += FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount")) - FCConvert.ToDecimal(rsDiscount.Get_Fields("TotalDiscount")) + FCConvert.ToDecimal(rsCreditMemo.Get_Fields("TotalCredit"));
						}
						rsJournalInfo.Edit();
						rsJournalInfo.Set_Fields("CheckNumber", rptCustomCheck.InstancePtr.lngcheck);
						if (!blnIndividualCheck)
						{
							rsJournalInfo.Set_Fields("Status", "C");
						}
						else
						{
							rsJournalInfo.Set_Fields("PrepaidCheck", true);
							rsJournalInfo.Set_Fields("PrintedIndividual", true);
						}
						rsJournalInfo.Set_Fields("Warrant", lngWarrant);
						rsJournalInfo.Set_Fields("CheckDate", datPayDate);
						rsJournalInfo.Set_Fields("EFTCheck", blnEFTCheck);
						rsJournalInfo.Update(true);
						if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
						{
							rsSepCred.OpenRecordset("SELECT * FROM APJournal WHERE SeperateCheckRecord = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (rsSepCred.EndOfFile() != true && rsSepCred.BeginningOfFile() != true)
							{
								counter2 = 1;
								do
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									FillData_2184(counter2, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsSepCred.Get_Fields_String("Description"), rsSepCred.Get_Fields_String("Reference"), Strings.Format(rsSepCred.Get_Fields("Amount") * -1, "#,##0.00"), "0.00", "0.00");
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curTotal += FCConvert.ToDecimal(rsSepCred.Get_Fields("Amount"));
									counter2 += 1;
									rsSepCred.Edit();
									rsSepCred.Set_Fields("CheckNumber", lngcheck);
									if (!blnIndividualCheck)
									{
										rsSepCred.Set_Fields("Status", "C");
									}
									else
									{
										rsSepCred.Set_Fields("PrepaidCheck", true);
										rsSepCred.Set_Fields("PrintedIndividual", true);
									}
									rsSepCred.Set_Fields("Warrant", lngWarrant);
									rsSepCred.Set_Fields("CheckDate", datPayDate);
									rsSepCred.Set_Fields("EFTCheck", blnEFTCheck);
									rsSepCred.Update(true);
									rsSepCred.MoveNext();
								}
								while (rsSepCred.EndOfFile() != true);
							}
							else
							{
								counter2 = 1;
							}
							for (counter2 = counter2; counter2 <= intCounterLimit; counter2++)
							{
								FillData_2184(counter2, "", "", "", "", "", "");
							}
							rsJournalInfo.MoveNext();
							goto GetVendorInfo;
						}
					}
					else
					{
						FillData_2184(counter, "", "", "", "", "", "");
					}
				}
				else
				{
					if (rsJournalInfo.EndOfFile() != true)
					{
						rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						if (Conversion.Val(rsJournalInfo.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
						{
							// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							FillData_2184(counter, modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("Warrant"), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curTotal += FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
						}
						else
						{
							rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							FillData_2184(counter, modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("Warrant"), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
							curTotal += FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount")) - FCConvert.ToDecimal(rsDiscount.Get_Fields("TotalDiscount")) + FCConvert.ToDecimal(rsCreditMemo.Get_Fields("TotalCredit"));
						}
						rsJournalInfo.Edit();
						rsJournalInfo.Set_Fields("CheckNumber", rptCustomCheck.InstancePtr.lngcheck);
						rsJournalInfo.Set_Fields("CheckDate", datPayDate);
						rsJournalInfo.Set_Fields("EFTCheck", blnEFTCheck);
						rsJournalInfo.Update(true);
						if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
						{
							rsSepCred.OpenRecordset("SELECT * FROM APJournal WHERE SeperateCheckRecord = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (rsSepCred.EndOfFile() != true && rsSepCred.BeginningOfFile() != true)
							{
								counter2 = 1;
								do
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									FillData_2184(counter2, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsSepCred.Get_Fields_String("Description"), rsSepCred.Get_Fields_String("Reference"), Strings.Format(rsSepCred.Get_Fields("Amount") * -1, "#,##0.00"), "0.00", "0.00");
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curTotal += FCConvert.ToDecimal(rsSepCred.Get_Fields("Amount"));
									counter2 += 1;
									rsSepCred.Edit();
									rsSepCred.Set_Fields("CheckNumber", lngcheck);
									if (!blnIndividualCheck)
									{
										rsSepCred.Set_Fields("Status", "C");
									}
									else
									{
										rsSepCred.Set_Fields("PrepaidCheck", true);
										rsSepCred.Set_Fields("PrintedIndividual", true);
									}
									rsSepCred.Set_Fields("Warrant", lngWarrant);
									rsSepCred.Set_Fields("CheckDate", datPayDate);
									rsSepCred.Set_Fields("EFTCheck", blnEFTCheck);
									rsSepCred.Update(true);
									rsSepCred.MoveNext();
								}
								while (rsSepCred.EndOfFile() != true);
							}
							else
							{
								counter2 = 1;
							}
							for (counter2 = counter2; counter2 <= intCounterLimit; counter2++)
							{
								FillData_2184(counter2, "", "", "", "", "", "");
							}
							rsJournalInfo.MoveNext();
							goto GetVendorInfo;
						}
					}
					else
					{
						FillData_2184(counter, "", "", "", "", "", "");
					}
				}
				rsJournalInfo.MoveNext();
			}
			if (rsJournalInfo.EndOfFile() != true)
			{
				curCarryOver = curTotal;
				curTotal = 0;
			}
			else
			{
				curCarryOver = 0;
			}
			GetVendorInfo:
			;
			curFooterAmount = curTotal;
			lngFooterCheck = lngcheck;
			strFooterVendorName = FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"));
			lngFooterVendorNumber = FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber"));
			strFooterAddress1 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
			strFooterAddress2 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
			strFooterAddress3 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
			if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
				{
					strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
				}
				else
				{
					strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
				{
					strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
				}
				else
				{
					strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
				}
			}
			strFooterCity = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
			strFooterState = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
			strFooterZip = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
			strFooterZip4 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
			if (!blnInitial)
			{
				strFooterJournals = rsJournalInfo.Name();
			}
			else
			{
				strFooterJournals = rsPrePaidJournalInfo.Name();
			}
			rsDiscount.Dispose();
			rsCreditMemo.Dispose();
			rsSepCred.Dispose();
        }
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void FillData_2184(int intIndex, string strWarrData, string strDescData, string strRefData, string curCreditData, string curDiscData, string curAmtData)
		{
			FillData(ref intIndex, ref strWarrData, ref strDescData, ref strRefData, ref curCreditData, ref curDiscData, ref curAmtData);
		}

		private void FillData(ref int intIndex, ref string strWarrData, ref string strDescData, ref string strRefData, ref string curCreditData, ref string curDiscData, ref string curAmtData)
		{
			modBudgetaryMaster.Statics.strWarr[intIndex] = strWarrData;
			modBudgetaryMaster.Statics.strDesc[intIndex] = strDescData;
			modBudgetaryMaster.Statics.strRef[intIndex] = strRefData;
			modBudgetaryMaster.Statics.curCredit[intIndex] = curCreditData;
			modBudgetaryMaster.Statics.curDisc[intIndex] = curDiscData;
			modBudgetaryMaster.Statics.curAmt[intIndex] = curAmtData;
		}

        //FC:FINAL:IPI - #1249 - report must be shown modal as MessageBox & frmSignaturePassword should be shown in the right order
        public void Init(string strPrinter, bool blnModal, string strRouting = "", string strBankAcct = "", string strBankN = "", int lngBankID = 0, bool showModal = false)
		{
			lngCheckBank = lngBankID;
			strRoutingNumber = strRouting;
			strBankAccountNumber = strBankAcct;
			strBankName = strBankN;
			frmReportViewer.InstancePtr.Init(this, strPrinter, 1, this.PageSettings.Duplex == System.Drawing.Printing.Duplex.Default, false, "Pages", false, string.Empty, "TRIO Software", false, false, showModal : showModal, showProgressDialog : false);
		}

		private void rptCustomCheck_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomCheck.Caption	= "Checks";
			//rptCustomCheck.Icon	= "rptCustomCheck.dsx":0000";
			//rptCustomCheck.Left	= 0;
			//rptCustomCheck.Top	= 0;
			//rptCustomCheck.Width	= 11880;
			//rptCustomCheck.Height	= 8595;
			//rptCustomCheck.StartUpPosition	= 3;
			//rptCustomCheck.SectionData	= "rptCustomCheck.dsx":058A;
			//End Unmaped Properties
		}

		private void rptCustomCheck_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
			if (blnEND)
			{
				frmReportViewer.InstancePtr.Unload();
			}
		}
	}
}
