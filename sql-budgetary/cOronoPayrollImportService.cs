﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cOronoPayrollImportService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public void OronoPayrollImport()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				FCCommonDialog ofd = new FCCommonDialog();
				string strFileName = "";
				ofd.Text = "Import Payroll File";
				//FC:TODO:PJ File-Handling (Options)
				//ofd.InitialDirectory = Application.StartupPath;
				//ofd.MultiSelect = false;
				//ofd.Options = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer;
				if (ofd.ShowOpen())
				{
					strFileName = ofd.FileName;
				}
				else
				{
					return;
				}
				cOronoPayrollImport pImport = new cOronoPayrollImport();
				if (pImport.ImportPayrollJournalEntries(strFileName))
				{
					MessageBox.Show("Process Complete.  Entries have been entered into journal " + FCConvert.ToString(pImport.LastJournal), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show(pImport.Errors, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				if (Strings.Trim(pImport.BadAccounts) != "")
				{
					rptHancockCountyBadAccounts.InstancePtr.strBadAccts = pImport.BadAccounts;
					frmReportViewer.InstancePtr.Init(rptHancockCountyBadAccounts.InstancePtr);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
