﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptTrialBalance.
	/// </summary>
	partial class rptTrialBalance
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTrialBalance));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUser = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubtitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFundDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEndBalanceBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBegBalanceBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccountDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupEndBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupBeginningBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtGroupFooterName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundEndBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtFundBeginningBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFundFooterName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndBalanceBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBegBalanceBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupEndBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupBeginningBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFooterName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundEndBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundNet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundBeginningBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundFooterName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtEndBalanceBudget,
				this.txtNet,
				this.txtCredits,
				this.txtDebits,
				this.txtBegBalanceBudget,
				this.txtAccount,
				this.txtAccountDescription
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label7,
				this.Label8,
				this.lblPage,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.lblUser,
				this.lblSubtitle1,
				this.lblSubTitle2
			});
			this.PageHeader.Height = 0.78125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.txtFundDescription
			});
			this.GroupHeader1.DataField = "GroupHeader1Binder";
			this.GroupHeader1.Height = 0.1979167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtFundEndBalance,
				this.txtFundNet,
				this.txtFundCredits,
				this.txtFundDebits,
				this.Line2,
				this.txtFundBeginningBalance,
				this.txtFundFooterName
			});
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupDescription
			});
			this.GroupHeader2.Height = 0.1569444F;
			this.GroupHeader2.DataField = "GroupHeader2Binder";
			this.GroupHeader2.KeepTogether = true;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtGroupEndBalance,
				this.txtGroupNet,
				this.txtGroupCredits,
				this.txtGroupDebits,
				this.txtGroupBeginningBalance,
				this.Line1,
				this.txtGroupFooterName
			});
			this.GroupFooter2.KeepTogether = true;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label2
			// 
			this.Label2.Height = 0.15625F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label2.Text = "Account";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 1.114583F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.15625F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.947917F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label3.Text = "Beginning Balance";
			this.Label3.Top = 0.625F;
			this.Label3.Width = 1.25F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label4.Text = "Trial Balance";
			this.Label4.Top = 0F;
			this.Label4.Width = 7.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0F;
			this.Label8.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.666667F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label9";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 8.666667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0F;
			this.Label10.Width = 1.3125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.15625F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 5.270833F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label11.Text = "Debits";
			this.Label11.Top = 0.625F;
			this.Label11.Width = 1.114583F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.15625F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6.458333F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label12.Text = "Credits";
			this.Label12.Top = 0.625F;
			this.Label12.Width = 1.114583F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.15625F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 7.645833F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label13.Text = "Net Change";
			this.Label13.Top = 0.625F;
			this.Label13.Width = 1.114583F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.15625F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 8.864583F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Label14.Text = "Balance";
			this.Label14.Top = 0.625F;
			this.Label14.Width = 1.114583F;
			// 
			// lblUser
			// 
			this.lblUser.Height = 0.1875F;
			this.lblUser.HyperLink = null;
			this.lblUser.Left = 0F;
			this.lblUser.Name = "lblUser";
			this.lblUser.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblUser.Text = "Label15";
			this.lblUser.Top = 0.375F;
			this.lblUser.Width = 1.5F;
			// 
			// lblSubtitle1
			// 
			this.lblSubtitle1.Height = 0.1875F;
			this.lblSubtitle1.HyperLink = null;
			this.lblSubtitle1.Left = 1.5F;
			this.lblSubtitle1.Name = "lblSubtitle1";
			this.lblSubtitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblSubtitle1.Text = null;
			this.lblSubtitle1.Top = 0.1875F;
			this.lblSubtitle1.Width = 7.1875F;
			// 
			// lblSubTitle2
			// 
			this.lblSubTitle2.Height = 0.1875F;
			this.lblSubTitle2.HyperLink = null;
			this.lblSubTitle2.Left = 1.5F;
			this.lblSubTitle2.Name = "lblSubTitle2";
			this.lblSubTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblSubTitle2.Text = null;
			this.lblSubTitle2.Top = 0.375F;
			this.lblSubTitle2.Width = 7.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.15625F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label1.Text = "Fund";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.4791667F;
			// 
			// txtFundDescription
			// 
			this.txtFundDescription.Height = 0.15625F;
			this.txtFundDescription.Left = 0.5416667F;
			this.txtFundDescription.Name = "txtFundDescription";
			this.txtFundDescription.Style = "font-size: 8.5pt; font-weight: bold";
			this.txtFundDescription.Text = null;
			this.txtFundDescription.Top = 0F;
			this.txtFundDescription.Width = 3.729167F;
			// 
			// txtGroupDescription
			// 
			this.txtGroupDescription.Height = 0.15625F;
			this.txtGroupDescription.Left = 0F;
			this.txtGroupDescription.Name = "txtGroupDescription";
			this.txtGroupDescription.Style = "font-size: 8.5pt; font-weight: bold";
			this.txtGroupDescription.Text = null;
			this.txtGroupDescription.Top = 0F;
			this.txtGroupDescription.Width = 3.541667F;
			// 
			// txtEndBalanceBudget
			// 
			this.txtEndBalanceBudget.Height = 0.15625F;
			this.txtEndBalanceBudget.Left = 8.84375F;
			this.txtEndBalanceBudget.Name = "txtEndBalanceBudget";
			this.txtEndBalanceBudget.Style = "font-size: 8.5pt; text-align: right";
			this.txtEndBalanceBudget.Text = null;
			this.txtEndBalanceBudget.Top = 0F;
			this.txtEndBalanceBudget.Width = 1.135417F;
			// 
			// txtNet
			// 
			this.txtNet.Height = 0.15625F;
			this.txtNet.Left = 7.625F;
			this.txtNet.Name = "txtNet";
			this.txtNet.Style = "font-size: 8.5pt; text-align: right";
			this.txtNet.Text = null;
			this.txtNet.Top = 0F;
			this.txtNet.Width = 1.135417F;
			// 
			// txtCredits
			// 
			this.txtCredits.Height = 0.15625F;
			this.txtCredits.Left = 6.4375F;
			this.txtCredits.Name = "txtCredits";
			this.txtCredits.Style = "font-size: 8.5pt; text-align: right";
			this.txtCredits.Text = null;
			this.txtCredits.Top = 0F;
			this.txtCredits.Width = 1.135417F;
			// 
			// txtDebits
			// 
			this.txtDebits.Height = 0.15625F;
			this.txtDebits.Left = 5.25F;
			this.txtDebits.Name = "txtDebits";
			this.txtDebits.Style = "font-size: 8.5pt; text-align: right";
			this.txtDebits.Text = null;
			this.txtDebits.Top = 0F;
			this.txtDebits.Width = 1.135417F;
			// 
			// txtBegBalanceBudget
			// 
			this.txtBegBalanceBudget.Height = 0.15625F;
			this.txtBegBalanceBudget.Left = 4.0625F;
			this.txtBegBalanceBudget.Name = "txtBegBalanceBudget";
			this.txtBegBalanceBudget.Style = "font-size: 8.5pt; text-align: right";
			this.txtBegBalanceBudget.Text = null;
			this.txtBegBalanceBudget.Top = 0F;
			this.txtBegBalanceBudget.Width = 1.135417F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8.5pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.354167F;
			// 
			// txtAccountDescription
			// 
			this.txtAccountDescription.Height = 0.15625F;
			this.txtAccountDescription.Left = 1.416667F;
			this.txtAccountDescription.Name = "txtAccountDescription";
			this.txtAccountDescription.Style = "font-size: 8.5pt";
			this.txtAccountDescription.Text = null;
			this.txtAccountDescription.Top = 0F;
			this.txtAccountDescription.Width = 2.604167F;
			// 
			// txtGroupEndBalance
			// 
			this.txtGroupEndBalance.Height = 0.15625F;
			this.txtGroupEndBalance.Left = 8.84375F;
			this.txtGroupEndBalance.Name = "txtGroupEndBalance";
			this.txtGroupEndBalance.Style = "font-size: 8.5pt; text-align: right";
			this.txtGroupEndBalance.Text = null;
			this.txtGroupEndBalance.Top = 0.0625F;
			this.txtGroupEndBalance.Width = 1.135417F;
			// 
			// txtGroupNet
			// 
			this.txtGroupNet.Height = 0.15625F;
			this.txtGroupNet.Left = 7.625F;
			this.txtGroupNet.Name = "txtGroupNet";
			this.txtGroupNet.Style = "font-size: 8.5pt; text-align: right";
			this.txtGroupNet.Text = null;
			this.txtGroupNet.Top = 0.0625F;
			this.txtGroupNet.Width = 1.135417F;
			// 
			// txtGroupCredits
			// 
			this.txtGroupCredits.Height = 0.15625F;
			this.txtGroupCredits.Left = 6.4375F;
			this.txtGroupCredits.Name = "txtGroupCredits";
			this.txtGroupCredits.Style = "font-size: 8.5pt; text-align: right";
			this.txtGroupCredits.Text = null;
			this.txtGroupCredits.Top = 0.0625F;
			this.txtGroupCredits.Width = 1.135417F;
			// 
			// txtGroupDebits
			// 
			this.txtGroupDebits.Height = 0.15625F;
			this.txtGroupDebits.Left = 5.25F;
			this.txtGroupDebits.Name = "txtGroupDebits";
			this.txtGroupDebits.Style = "font-size: 8.5pt; text-align: right";
			this.txtGroupDebits.Text = null;
			this.txtGroupDebits.Top = 0.0625F;
			this.txtGroupDebits.Width = 1.135417F;
			// 
			// txtGroupBeginningBalance
			// 
			this.txtGroupBeginningBalance.Height = 0.15625F;
			this.txtGroupBeginningBalance.Left = 4.0625F;
			this.txtGroupBeginningBalance.Name = "txtGroupBeginningBalance";
			this.txtGroupBeginningBalance.Style = "font-size: 8.5pt; text-align: right";
			this.txtGroupBeginningBalance.Text = null;
			this.txtGroupBeginningBalance.Top = 0.0625F;
			this.txtGroupBeginningBalance.Width = 1.135417F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 9.96875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 9.96875F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// txtGroupFooterName
			// 
			this.txtGroupFooterName.Height = 0.15625F;
			this.txtGroupFooterName.Left = 2.041667F;
			this.txtGroupFooterName.Name = "txtGroupFooterName";
			this.txtGroupFooterName.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtGroupFooterName.Text = null;
			this.txtGroupFooterName.Top = 0.0625F;
			this.txtGroupFooterName.Width = 1.979167F;
			// 
			// txtFundEndBalance
			// 
			this.txtFundEndBalance.Height = 0.15625F;
			this.txtFundEndBalance.Left = 8.84375F;
			this.txtFundEndBalance.Name = "txtFundEndBalance";
			this.txtFundEndBalance.Style = "font-size: 8.5pt; text-align: right";
			this.txtFundEndBalance.Text = null;
			this.txtFundEndBalance.Top = 0.0625F;
			this.txtFundEndBalance.Width = 1.135417F;
			// 
			// txtFundNet
			// 
			this.txtFundNet.Height = 0.15625F;
			this.txtFundNet.Left = 7.625F;
			this.txtFundNet.Name = "txtFundNet";
			this.txtFundNet.Style = "font-size: 8.5pt; text-align: right";
			this.txtFundNet.Text = null;
			this.txtFundNet.Top = 0.0625F;
			this.txtFundNet.Width = 1.135417F;
			// 
			// txtFundCredits
			// 
			this.txtFundCredits.Height = 0.15625F;
			this.txtFundCredits.Left = 6.4375F;
			this.txtFundCredits.Name = "txtFundCredits";
			this.txtFundCredits.Style = "font-size: 8.5pt; text-align: right";
			this.txtFundCredits.Text = null;
			this.txtFundCredits.Top = 0.0625F;
			this.txtFundCredits.Width = 1.135417F;
			// 
			// txtFundDebits
			// 
			this.txtFundDebits.Height = 0.15625F;
			this.txtFundDebits.Left = 5.25F;
			this.txtFundDebits.Name = "txtFundDebits";
			this.txtFundDebits.Style = "font-size: 8.5pt; text-align: right";
			this.txtFundDebits.Text = null;
			this.txtFundDebits.Top = 0.0625F;
			this.txtFundDebits.Width = 1.135417F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 9.96875F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 9.96875F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// txtFundBeginningBalance
			// 
			this.txtFundBeginningBalance.Height = 0.15625F;
			this.txtFundBeginningBalance.Left = 4.0625F;
			this.txtFundBeginningBalance.Name = "txtFundBeginningBalance";
			this.txtFundBeginningBalance.Style = "font-size: 8.5pt; text-align: right";
			this.txtFundBeginningBalance.Text = null;
			this.txtFundBeginningBalance.Top = 0.0625F;
			this.txtFundBeginningBalance.Width = 1.135417F;
			// 
			// txtFundFooterName
			// 
			this.txtFundFooterName.Height = 0.15625F;
			this.txtFundFooterName.Left = 2.041667F;
			this.txtFundFooterName.Name = "txtFundFooterName";
			this.txtFundFooterName.Style = "font-size: 8.5pt; font-weight: bold; text-align: right";
			this.txtFundFooterName.Text = null;
			this.txtFundFooterName.Top = 0.0625F;
			this.txtFundFooterName.Width = 1.979167F;
			// 
			// rptTrialBalance
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "Trial Balance";
			this.PageSettings.DefaultPaperSize = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEndBalanceBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBegBalanceBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupEndBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupBeginningBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFooterName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundEndBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundNet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundBeginningBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFundFooterName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEndBalanceBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBegBalanceBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUser;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubtitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundEndBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundDebits;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundBeginningBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFundFooterName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupEndBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupBeginningBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupFooterName;
	}
}
