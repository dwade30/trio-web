﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintChecks.
	/// </summary>
	partial class frmPrintChecks : BaseForm
	{
		public fecherFoundation.FCComboBox cmbInitial;
		public fecherFoundation.FCLabel lblInitial;
		public fecherFoundation.FCFrame fraReprint;
		public Global.T2KOverTypeBox txtFirstReprintCheck;
		public Global.T2KOverTypeBox txtLastReprintCheck;
		public Global.T2KOverTypeBox txtFirstCheck;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraChoose;
		public Global.T2KOverTypeBox txtWarrant;
		public Global.T2KOverTypeBox txtCheck;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblWarrant;
		public fecherFoundation.FCFrame fraPayDate;
		public Global.T2KDateBox txtPayDate;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCComboBox cboBanks;
		public fecherFoundation.FCTextBox txtCheckMessage;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintChecks));
            Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
            this.cmbInitial = new fecherFoundation.FCComboBox();
            this.lblInitial = new fecherFoundation.FCLabel();
            this.fraReprint = new fecherFoundation.FCFrame();
            this.txtFirstReprintCheck = new Global.T2KOverTypeBox();
            this.txtLastReprintCheck = new Global.T2KOverTypeBox();
            this.txtFirstCheck = new Global.T2KOverTypeBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.fraChoose = new fecherFoundation.FCFrame();
            this.txtWarrant = new Global.T2KOverTypeBox();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblWarrant = new fecherFoundation.FCLabel();
            this.fraPayDate = new fecherFoundation.FCFrame();
            this.txtPayDate = new Global.T2KDateBox();
            this.lblInstruct = new fecherFoundation.FCLabel();
            this.fraBanks = new fecherFoundation.FCFrame();
            this.cboBanks = new fecherFoundation.FCComboBox();
            this.txtCheckMessage = new fecherFoundation.FCTextBox();
            this.cmdQuit = new fecherFoundation.FCButton();
            this.vsJournals = new fecherFoundation.FCGrid();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.btnFileProcess = new fecherFoundation.FCButton();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).BeginInit();
            this.fraReprint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstReprintCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastReprintCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoose)).BeginInit();
            this.fraChoose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarrant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).BeginInit();
            this.fraPayDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
            this.fraBanks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdQuit);
            this.BottomPanel.Controls.Add(this.btnFileProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 592);
            this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPayDate);
            this.ClientArea.Controls.Add(this.fraChoose);
            this.ClientArea.Controls.Add(this.fraBanks);
            this.ClientArea.Controls.Add(this.fraReprint);
            this.ClientArea.Controls.Add(this.txtCheckMessage);
            this.ClientArea.Controls.Add(this.vsJournals);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.lblTitle);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1088, 532);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(140, 30);
            this.HeaderText.Text = "Checks";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbInitial
            // 
            this.cmbInitial.Items.AddRange(new object[] {
            "Initial Run",
            "Reprint Checks"});
            this.cmbInitial.Location = new System.Drawing.Point(158, 30);
            this.cmbInitial.Name = "cmbInitial";
            this.cmbInitial.Size = new System.Drawing.Size(362, 40);
            this.cmbInitial.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbInitial, null);
            // 
            // lblInitial
            // 
            this.lblInitial.Location = new System.Drawing.Point(20, 44);
            this.lblInitial.Name = "lblInitial";
            this.lblInitial.Size = new System.Drawing.Size(68, 15);
            this.lblInitial.TabIndex = 3;
            this.lblInitial.Text = "PRINT TYPE";
            this.ToolTip1.SetToolTip(this.lblInitial, null);
            // 
            // fraReprint
            // 
            this.fraReprint.Controls.Add(this.txtFirstReprintCheck);
            this.fraReprint.Controls.Add(this.txtLastReprintCheck);
            this.fraReprint.Controls.Add(this.txtFirstCheck);
            this.fraReprint.Controls.Add(this.Label5);
            this.fraReprint.Controls.Add(this.Label4);
            this.fraReprint.Controls.Add(this.Label3);
            this.fraReprint.Location = new System.Drawing.Point(30, 150);
            this.fraReprint.Name = "fraReprint";
            this.fraReprint.Size = new System.Drawing.Size(425, 210);
            this.fraReprint.TabIndex = 20;
            this.fraReprint.Text = "Reprint Checks";
            this.ToolTip1.SetToolTip(this.fraReprint, null);
            this.fraReprint.Visible = false;
            // 
            // txtFirstReprintCheck
            // 
            this.txtFirstReprintCheck.Location = new System.Drawing.Point(288, 30);
            this.txtFirstReprintCheck.MaxLength = 6;
            this.txtFirstReprintCheck.Name = "txtFirstReprintCheck";
            this.txtFirstReprintCheck.Size = new System.Drawing.Size(120, 40);
            this.txtFirstReprintCheck.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.txtFirstReprintCheck, "This is the first check that printed incorrectly.  If you wish to restart at the " +
        "beginning, reprinting all checks,  just hit enter.");
            this.txtFirstReprintCheck.Leave += new System.EventHandler(this.txtFirstReprintCheck_Leave);
            this.txtFirstReprintCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFirstReprintCheck_KeyPressEvent);
            // 
            // txtLastReprintCheck
            // 
            this.txtLastReprintCheck.Location = new System.Drawing.Point(288, 90);
            this.txtLastReprintCheck.MaxLength = 6;
            this.txtLastReprintCheck.Name = "txtLastReprintCheck";
            this.txtLastReprintCheck.Size = new System.Drawing.Size(120, 40);
            this.txtLastReprintCheck.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.txtLastReprintCheck, "This is the last check that printed incorrectly.  If you wish to reprint all the " +
        "remaining checks just hit enter");
            this.txtLastReprintCheck.Leave += new System.EventHandler(this.txtLastReprintCheck_Leave);
            this.txtLastReprintCheck.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastReprintCheck_Validate);
            this.txtLastReprintCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLastReprintCheck_KeyPressEvent);
            // 
            // txtFirstCheck
            // 
            this.txtFirstCheck.Location = new System.Drawing.Point(288, 150);
            this.txtFirstCheck.MaxLength = 6;
            this.txtFirstCheck.Name = "txtFirstCheck";
            this.txtFirstCheck.Size = new System.Drawing.Size(120, 40);
            this.txtFirstCheck.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.txtFirstCheck, "This is the number on the first check you are using to reprint checks on.");
            this.txtFirstCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFirstCheck_KeyPressEvent);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 164);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(198, 18);
            this.Label5.TabIndex = 26;
            this.Label5.Text = "FIRST CHECK NUMBER";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 104);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(198, 18);
            this.Label4.TabIndex = 24;
            this.Label4.Text = "LAST CHECK NUMBER TO REPRINT";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(198, 18);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "FIRST CHECK NUMBER TO REPRINT";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // fraChoose
            // 
            this.fraChoose.Controls.Add(this.txtWarrant);
            this.fraChoose.Controls.Add(this.txtCheck);
            this.fraChoose.Controls.Add(this.Label2);
            this.fraChoose.Controls.Add(this.lblWarrant);
            this.fraChoose.Location = new System.Drawing.Point(30, 150);
            this.fraChoose.Name = "fraChoose";
            this.fraChoose.Size = new System.Drawing.Size(380, 158);
            this.fraChoose.TabIndex = 9;
            this.fraChoose.Text = "Please Select Check Information";
            this.ToolTip1.SetToolTip(this.fraChoose, null);
            this.fraChoose.Visible = false;
            // 
            // txtWarrant
            // 
            clientEvent1.Event = "keypress";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtWarrant).Add(clientEvent1);
            this.txtWarrant.Location = new System.Drawing.Point(160, 30);
            this.txtWarrant.MaxLength = 4;
            this.txtWarrant.Name = "txtWarrant";
            this.txtWarrant.Size = new System.Drawing.Size(120, 40);
            this.txtWarrant.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.txtWarrant, null);
            this.txtWarrant.Validating += new System.ComponentModel.CancelEventHandler(this.txtWarrant_Validate);
            this.txtWarrant.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtWarrant_KeyPressEvent);
            // 
            // txtCheck
            // 
            clientEvent2.Event = "keypress";
            clientEvent2.JavaScript = resources.GetString("clientEvent2.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtCheck).Add(clientEvent2);
            this.txtCheck.Location = new System.Drawing.Point(160, 90);
            this.txtCheck.MaxLength = 6;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(120, 40);
            this.txtCheck.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtCheck, null);
            this.txtCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheck_KeyPressEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(90, 18);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "FIRST CHECK #";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // lblWarrant
            // 
            this.lblWarrant.Location = new System.Drawing.Point(20, 44);
            this.lblWarrant.Name = "lblWarrant";
            this.lblWarrant.Size = new System.Drawing.Size(72, 18);
            this.lblWarrant.TabIndex = 10;
            this.lblWarrant.Text = "WARRANT #";
            this.lblWarrant.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblWarrant, null);
            // 
            // fraPayDate
            // 
            this.fraPayDate.Controls.Add(this.cmbInitial);
            this.fraPayDate.Controls.Add(this.lblInitial);
            this.fraPayDate.Controls.Add(this.txtPayDate);
            this.fraPayDate.Controls.Add(this.lblInstruct);
            this.fraPayDate.Location = new System.Drawing.Point(30, 150);
            this.fraPayDate.Name = "fraPayDate";
            this.fraPayDate.Size = new System.Drawing.Size(540, 90);
            this.fraPayDate.Text = "Check Type";
            this.ToolTip1.SetToolTip(this.fraPayDate, null);
            // 
            // txtPayDate
            // 
            this.txtPayDate.Location = new System.Drawing.Point(260, 30);
            this.txtPayDate.Mask = "##/##/####";
            this.txtPayDate.MaxLength = 10;
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.Size = new System.Drawing.Size(115, 22);
            this.txtPayDate.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtPayDate, null);
            this.txtPayDate.Visible = false;
            // 
            // lblInstruct
            // 
            this.lblInstruct.Location = new System.Drawing.Point(20, 44);
            this.lblInstruct.Name = "lblInstruct";
            this.lblInstruct.Size = new System.Drawing.Size(170, 18);
            this.lblInstruct.TabIndex = 3;
            this.lblInstruct.Text = "PLEASE ENTER THE PAY DATE";
            this.lblInstruct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblInstruct, null);
            this.lblInstruct.Visible = false;
            // 
            // fraBanks
            // 
            this.fraBanks.Anchor = Wisej.Web.AnchorStyles.Bottom;
            this.fraBanks.Controls.Add(this.cboBanks);
            this.fraBanks.Location = new System.Drawing.Point(340, 432);
            this.fraBanks.Name = "fraBanks";
            this.fraBanks.Size = new System.Drawing.Size(448, 90);
            this.fraBanks.TabIndex = 30;
            this.fraBanks.Text = "Select A Bank";
            this.ToolTip1.SetToolTip(this.fraBanks, null);
            this.fraBanks.Visible = false;
            // 
            // cboBanks
            // 
            this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
            this.cboBanks.Items.AddRange(new object[] {
            "1 - Most Recently Run Report",
            "2 - Next Oldest Report",
            "3 - 3rd Oldest Report",
            "4 - 4th Oldest Report",
            "5 - 5th Oldest Report",
            "6 - 6th Oldest Report",
            "7 - 7th Oldest Report",
            "8 - 8th Oldest Report",
            "9 - 9th Oldest Report"});
            this.cboBanks.Location = new System.Drawing.Point(20, 30);
            this.cboBanks.Name = "cboBanks";
            this.cboBanks.Size = new System.Drawing.Size(408, 40);
            this.cboBanks.TabIndex = 31;
            this.ToolTip1.SetToolTip(this.cboBanks, null);
            this.cboBanks.SelectedIndexChanged += new System.EventHandler(this.cboBanks_SelectedIndexChanged);
            // 
            // txtCheckMessage
            // 
            this.txtCheckMessage.BackColor = System.Drawing.SystemColors.Window;
            this.txtCheckMessage.Cursor = Wisej.Web.Cursors.Default;
            this.txtCheckMessage.Location = new System.Drawing.Point(200, 90);
            this.txtCheckMessage.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.txtCheckMessage.Name = "txtCheckMessage";
            this.txtCheckMessage.Size = new System.Drawing.Size(323, 40);
            this.txtCheckMessage.TabIndex = 29;
            this.ToolTip1.SetToolTip(this.txtCheckMessage, null);
            // 
            // cmdQuit
            // 
            this.cmdQuit.AppearanceKey = "actionButton";
            this.cmdQuit.Location = new System.Drawing.Point(160, 30);
            this.cmdQuit.Name = "cmdQuit";
            this.cmdQuit.Size = new System.Drawing.Size(90, 48);
            this.cmdQuit.TabIndex = 5;
            this.cmdQuit.Text = "Quit";
            this.ToolTip1.SetToolTip(this.cmdQuit, null);
            this.cmdQuit.Visible = false;
            this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
            // 
            // vsJournals
            // 
            this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsJournals.Cols = 8;
            this.vsJournals.FixedCols = 0;
            this.vsJournals.Location = new System.Drawing.Point(30, 150);
            this.vsJournals.Name = "vsJournals";
            this.vsJournals.RowHeadersVisible = false;
            this.vsJournals.Rows = 50;
            this.vsJournals.Size = new System.Drawing.Size(1028, 252);
            this.vsJournals.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.vsJournals, null);
            this.vsJournals.Visible = false;
            this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
            this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 104);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(100, 18);
            this.Label6.TabIndex = 28;
            this.Label6.Text = "CHECK MESSAGE";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(0, 130);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(66, 24);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "LABEL2";
            this.ToolTip1.SetToolTip(this.lblTitle, null);
            this.lblTitle.Visible = false;
            // 
            // btnFileProcess
            // 
            this.btnFileProcess.AppearanceKey = "acceptButton";
            this.btnFileProcess.Location = new System.Drawing.Point(30, 30);
            this.btnFileProcess.Name = "btnFileProcess";
            this.btnFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnFileProcess.Size = new System.Drawing.Size(100, 48);
            this.btnFileProcess.TabIndex = 6;
            this.btnFileProcess.Text = "Process";
            this.ToolTip1.SetToolTip(this.btnFileProcess, null);
            this.btnFileProcess.Click += new System.EventHandler(this.btnFileProcess_Click);
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(824, 30);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "PLEASE SELECT THE JOURNAL(S) YOU WISH TO PRINT CHECKS FROM AND THEN CLICK THE \"PR" +
    "OCESS\" BUTTON TO CONTINUE";
            this.ToolTip1.SetToolTip(this.Label1, null);
            this.Label1.Visible = false;
            // 
            // frmPrintChecks
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPrintChecks";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Checks";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmPrintChecks_Load);
            this.Activated += new System.EventHandler(this.frmPrintChecks_Activated);
            this.Resize += new System.EventHandler(this.frmPrintChecks_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintChecks_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReprint)).EndInit();
            this.fraReprint.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstReprintCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastReprintCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoose)).EndInit();
            this.fraChoose.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtWarrant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).EndInit();
            this.fraPayDate.ResumeLayout(false);
            this.fraPayDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
            this.fraBanks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public fecherFoundation.FCButton btnFileProcess;
		private JavaScript javaScript1;
	}
}
