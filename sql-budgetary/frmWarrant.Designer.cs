﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrant.
	/// </summary>
	partial class frmWarrant : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReprint;
		public fecherFoundation.FCLabel lblReprint;
		public fecherFoundation.FCFrame fraSelectReport;
		public fecherFoundation.FCComboBox cboReport;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCFrame fraChoose;
		public fecherFoundation.FCComboBox cboPreviewAccount;
		public fecherFoundation.FCLabel lblSchoolAccounts;
		public fecherFoundation.FCLabel lblTownAccounts;
		public fecherFoundation.FCLabel lblTitle;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWarrant));
			this.cmbReprint = new fecherFoundation.FCComboBox();
			this.lblReprint = new fecherFoundation.FCLabel();
			this.fraSelectReport = new fecherFoundation.FCFrame();
			this.cboReport = new fecherFoundation.FCComboBox();
			this.dlg1_Save = new fecherFoundation.FCCommonDialog();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.fraChoose = new fecherFoundation.FCFrame();
			this.cboPreviewAccount = new fecherFoundation.FCComboBox();
			this.lblSchoolAccounts = new fecherFoundation.FCLabel();
			this.lblTownAccounts = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).BeginInit();
			this.fraSelectReport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraChoose)).BeginInit();
			this.fraChoose.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 415);
			this.BottomPanel.Size = new System.Drawing.Size(631, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbReprint);
			this.ClientArea.Controls.Add(this.lblReprint);
			this.ClientArea.Controls.Add(this.fraSelectReport);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Controls.Add(this.fraChoose);
			this.ClientArea.Size = new System.Drawing.Size(631, 355);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(631, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(146, 30);
			this.HeaderText.Text = "Warrant";
			// 
			// cmbReprint
			// 
			this.cmbReprint.AutoSize = false;
			this.cmbReprint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbReprint.FormattingEnabled = true;
			//FC:FINAL:MSH - Issue #720: reorder values in combobox
			this.cmbReprint.Items.AddRange(new object[] {
				"Initial Run",
				"Reprint Warrant"
			});
			this.cmbReprint.Location = new System.Drawing.Point(180, 30);
			this.cmbReprint.Name = "cmbReprint";
			this.cmbReprint.Size = new System.Drawing.Size(370, 40);
			this.cmbReprint.TabIndex = 0;
			// 
			// lblReprint
			// 
			this.lblReprint.Location = new System.Drawing.Point(30, 44);
			this.lblReprint.Name = "lblReprint";
			this.lblReprint.Size = new System.Drawing.Size(80, 15);
			this.lblReprint.TabIndex = 1;
			this.lblReprint.Text = "REPORT TYPE";
			// 
			// fraSelectReport
			// 
			this.fraSelectReport.Controls.Add(this.cboReport);
			this.fraSelectReport.Location = new System.Drawing.Point(30, 30);
			this.fraSelectReport.Name = "fraSelectReport";
			this.fraSelectReport.Size = new System.Drawing.Size(540, 90);
			this.fraSelectReport.TabIndex = 6;
			this.fraSelectReport.Text = "Select Report";
			this.fraSelectReport.Visible = false;
			// 
			// cboReport
			// 
			this.cboReport.AutoSize = false;
			this.cboReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReport.FormattingEnabled = true;
			this.cboReport.Items.AddRange(new object[] {
				"1 - Most Recently Run Report",
				"2 - Next Oldest Report",
				"3 - 3rd Oldest Report",
				"4 - 4th Oldest Report",
				"5 - 5th Oldest Report",
				"6 - 6th Oldest Report",
				"7 - 7th Oldest Report",
				"8 - 8th Oldest Report",
				"9 - 9th Oldest Report"
			});
			this.cboReport.Location = new System.Drawing.Point(20, 30);
			this.cboReport.Name = "cboReport";
			this.cboReport.Size = new System.Drawing.Size(500, 40);
			this.cboReport.TabIndex = 7;
			// 
			// dlg1_Save
			// 
			this.dlg1_Save.Color = System.Drawing.Color.Black;
			this.dlg1_Save.DefaultExt = null;
			this.dlg1_Save.FilterIndex = ((short)(0));
			this.dlg1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1_Save.FontName = "Microsoft Sans Serif";
			this.dlg1_Save.FontSize = 8.25F;
			this.dlg1_Save.ForeColor = System.Drawing.Color.Black;
			this.dlg1_Save.FromPage = 0;
			this.dlg1_Save.Location = new System.Drawing.Point(0, 0);
			this.dlg1_Save.Name = "dlg1_Save";
			this.dlg1_Save.PrinterSettings = null;
			this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
			this.dlg1_Save.TabIndex = 0;
			this.dlg1_Save.ToPage = 0;
			// 
			// dlg1
			// 
			this.dlg1.Color = System.Drawing.Color.Black;
			this.dlg1.DefaultExt = null;
			this.dlg1.FilterIndex = ((short)(0));
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.FontSize = 8.25F;
			this.dlg1.ForeColor = System.Drawing.Color.Black;
			this.dlg1.FromPage = 0;
			this.dlg1.Location = new System.Drawing.Point(0, 0);
			this.dlg1.Name = "dlg1";
			this.dlg1.PrinterSettings = null;
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			this.dlg1.TabIndex = 0;
			this.dlg1.ToPage = 0;
			// 
			// fraChoose
			// 
			this.fraChoose.Controls.Add(this.cboPreviewAccount);
			this.fraChoose.Controls.Add(this.lblSchoolAccounts);
			this.fraChoose.Controls.Add(this.lblTownAccounts);
			this.fraChoose.Location = new System.Drawing.Point(30, 30);
			this.fraChoose.Name = "fraChoose";
			this.fraChoose.Size = new System.Drawing.Size(540, 158);
			this.fraChoose.TabIndex = 3;
			this.fraChoose.Text = "Please Select A Reporting Option";
			this.fraChoose.Visible = false;
			// 
			// cboPreviewAccount
			// 
			this.cboPreviewAccount.AutoSize = false;
			this.cboPreviewAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboPreviewAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboPreviewAccount.FormattingEnabled = true;
			this.cboPreviewAccount.Items.AddRange(new object[] {
				"1 - Do Not Print Account Titles",
				"2 - Print Dept/Div - Exp/Obj // Dept/Div - Rev Titles",
				"3 - Print Dept/Div - Dept/Div Titles",
				"4 - Print Exp/Obj - Rev Titles"
			});
			this.cboPreviewAccount.Location = new System.Drawing.Point(20, 58);
			this.cboPreviewAccount.Name = "cboPreviewAccount";
			this.cboPreviewAccount.Size = new System.Drawing.Size(500, 40);
			this.cboPreviewAccount.TabIndex = 4;
			// 
			// lblSchoolAccounts
			// 
			this.lblSchoolAccounts.Location = new System.Drawing.Point(20, 118);
			this.lblSchoolAccounts.Name = "lblSchoolAccounts";
			this.lblSchoolAccounts.Size = new System.Drawing.Size(126, 18);
			this.lblSchoolAccounts.TabIndex = 14;
			this.lblSchoolAccounts.Text = "SCHOOL ACCOUNTS";
			// 
			// lblTownAccounts
			// 
			this.lblTownAccounts.Location = new System.Drawing.Point(20, 30);
			this.lblTownAccounts.Name = "lblTownAccounts";
			this.lblTownAccounts.Size = new System.Drawing.Size(124, 18);
			this.lblTownAccounts.TabIndex = 13;
			this.lblTownAccounts.Text = "TOWN ACCOUNTS";
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(6, -1);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(66, 23);
			this.lblTitle.TabIndex = 2;
			this.lblTitle.Text = "LABEL2";
			this.lblTitle.Visible = false;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(100, 48);
			this.btnProcess.TabIndex = 10;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmWarrant
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(631, 523);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmWarrant";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Warrant";
			this.Load += new System.EventHandler(this.frmWarrant_Load);
			this.Activated += new System.EventHandler(this.frmWarrant_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmWarrant_KeyPress);
			this.Resize += new System.EventHandler(this.frmWarrant_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).EndInit();
			this.fraSelectReport.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraChoose)).EndInit();
			this.fraChoose.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton btnProcess;
	}
}
