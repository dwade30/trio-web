﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCredMemSummary.
	/// </summary>
	partial class srptCredMemSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCredMemSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldJournalNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEnteredAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditMemoAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnteredAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditMemoAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.fldJournalNumber,
				this.Label3,
				this.Label4,
				this.fldEnteredAmount,
				this.fldCreditMemoAmount
			});
			this.Detail.Height = 0.625F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Line1
			});
			this.GroupHeader1.Height = 0.2291667F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 9pt; font-weight: bold; text-align: center" + "; ddo-char-set: 1";
			this.Label1.Text = "Journal Totals Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 2.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.5F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.21875F;
			this.Line1.Width = 2.46875F;
			this.Line1.X1 = 2.5F;
			this.Line1.X2 = 4.96875F;
			this.Line1.Y1 = 0.21875F;
			this.Line1.Y2 = 0.21875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.8125F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.Label2.Text = "Journal #:";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.875F;
			// 
			// fldJournalNumber
			// 
			this.fldJournalNumber.Height = 0.1875F;
			this.fldJournalNumber.Left = 3.78125F;
			this.fldJournalNumber.Name = "fldJournalNumber";
			this.fldJournalNumber.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldJournalNumber.Text = "Field1";
			this.fldJournalNumber.Top = 0F;
			this.fldJournalNumber.Width = 0.34375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 2.25F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.Label3.Text = "Entered Amount:";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 1.4375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.Label4.Text = "Credit Memo Amount:";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 1.6875F;
			// 
			// fldEnteredAmount
			// 
			this.fldEnteredAmount.Height = 0.1875F;
			this.fldEnteredAmount.Left = 3.78125F;
			this.fldEnteredAmount.Name = "fldEnteredAmount";
			this.fldEnteredAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldEnteredAmount.Text = "Field1";
			this.fldEnteredAmount.Top = 0.1875F;
			this.fldEnteredAmount.Width = 1.15625F;
			// 
			// fldCreditMemoAmount
			// 
			this.fldCreditMemoAmount.Height = 0.1875F;
			this.fldCreditMemoAmount.Left = 3.78125F;
			this.fldCreditMemoAmount.Name = "fldCreditMemoAmount";
			this.fldCreditMemoAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCreditMemoAmount.Text = "Field1";
			this.fldCreditMemoAmount.Top = 0.375F;
			this.fldCreditMemoAmount.Width = 1.15625F;
			// 
			// srptCredMemSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnteredAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditMemoAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournalNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEnteredAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditMemoAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
