﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEncumbranceUpdate.
	/// </summary>
	partial class frmEncumbranceUpdate : BaseForm
	{
		public fecherFoundation.FCFrame fraUpdate;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdAmount;
		public fecherFoundation.FCButton cmdAccount;
		public fecherFoundation.FCButton cmdDeleteUpdate;
		public fecherFoundation.FCButton cmdDeleteNoUpdate;
		public fecherFoundation.FCFrame fraChange;
		public fecherFoundation.FCTextBox txtNewValue;
		public fecherFoundation.FCButton cmdUpdate;
		public fecherFoundation.FCButton cmdCancel;
		public FCGrid vsNewAccount;
		public FCGrid vsOldValue;
		public fecherFoundation.FCLabel lblOldValue;
		public fecherFoundation.FCLabel lblNewValue;
		public fecherFoundation.FCComboBox cboPeriod;
		public fecherFoundation.FCGrid vs2;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblEntries;
		public fecherFoundation.FCLabel lblDetails;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEncumbranceUpdate));
			this.fraUpdate = new fecherFoundation.FCFrame();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdAmount = new fecherFoundation.FCButton();
			this.cmdAccount = new fecherFoundation.FCButton();
			this.cmdDeleteUpdate = new fecherFoundation.FCButton();
			this.cmdDeleteNoUpdate = new fecherFoundation.FCButton();
			this.fraChange = new fecherFoundation.FCFrame();
			this.txtNewValue = new fecherFoundation.FCTextBox();
			this.cmdUpdate = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.vsNewAccount = new fecherFoundation.FCGrid();
			this.vsOldValue = new fecherFoundation.FCGrid();
			this.lblOldValue = new fecherFoundation.FCLabel();
			this.lblNewValue = new fecherFoundation.FCLabel();
			this.cboPeriod = new fecherFoundation.FCComboBox();
			this.vs2 = new fecherFoundation.FCGrid();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblEntries = new fecherFoundation.FCLabel();
			this.lblDetails = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).BeginInit();
			this.fraUpdate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteNoUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChange)).BeginInit();
			this.fraChange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsNewAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsOldValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 523);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraUpdate);
			this.ClientArea.Controls.Add(this.fraChange);
			this.ClientArea.Controls.Add(this.cboPeriod);
			this.ClientArea.Controls.Add(this.vs2);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblInstruct);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblEntries);
			this.ClientArea.Controls.Add(this.lblDetails);
			this.ClientArea.Size = new System.Drawing.Size(1078, 463);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(312, 30);
			this.HeaderText.Text = "Encumbrance Corrections";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraUpdate
			// 
			this.fraUpdate.BackColor = System.Drawing.Color.White;
			this.fraUpdate.Controls.Add(this.cmdReturn);
			this.fraUpdate.Controls.Add(this.cmdAmount);
			this.fraUpdate.Controls.Add(this.cmdAccount);
			this.fraUpdate.Controls.Add(this.cmdDeleteUpdate);
			this.fraUpdate.Controls.Add(this.cmdDeleteNoUpdate);
			this.fraUpdate.Location = new System.Drawing.Point(760, 17);
			this.fraUpdate.Name = "fraUpdate";
			this.fraUpdate.Size = new System.Drawing.Size(274, 240);
			this.fraUpdate.TabIndex = 4;
			this.fraUpdate.Text = "Update Choices";
			this.ToolTip1.SetToolTip(this.fraUpdate, null);
			this.fraUpdate.Visible = false;
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(20, 180);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(234, 40);
			this.cmdReturn.TabIndex = 9;
			this.cmdReturn.Text = "Return to List";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// cmdAmount
			// 
			this.cmdAmount.AppearanceKey = "actionButton";
			this.cmdAmount.Location = new System.Drawing.Point(20, 130);
			this.cmdAmount.Name = "cmdAmount";
			this.cmdAmount.Size = new System.Drawing.Size(234, 40);
			this.cmdAmount.TabIndex = 8;
			this.cmdAmount.Text = "Change Amount";
			this.ToolTip1.SetToolTip(this.cmdAmount, null);
			this.cmdAmount.Click += new System.EventHandler(this.cmdAmount_Click);
			// 
			// cmdAccount
			// 
			this.cmdAccount.AppearanceKey = "actionButton";
			this.cmdAccount.Location = new System.Drawing.Point(20, 80);
			this.cmdAccount.Name = "cmdAccount";
			this.cmdAccount.Size = new System.Drawing.Size(234, 40);
			this.cmdAccount.TabIndex = 7;
			this.cmdAccount.Text = "Change Account";
			this.ToolTip1.SetToolTip(this.cmdAccount, null);
			this.cmdAccount.Click += new System.EventHandler(this.cmdAccount_Click);
			// 
			// cmdDeleteUpdate
			// 
			this.cmdDeleteUpdate.AppearanceKey = "actionButton";
			this.cmdDeleteUpdate.Location = new System.Drawing.Point(20, 30);
			this.cmdDeleteUpdate.Name = "cmdDeleteUpdate";
			this.cmdDeleteUpdate.Size = new System.Drawing.Size(232, 40);
			this.cmdDeleteUpdate.TabIndex = 5;
			this.cmdDeleteUpdate.Text = "Delete Line Item";
			this.ToolTip1.SetToolTip(this.cmdDeleteUpdate, null);
			this.cmdDeleteUpdate.Click += new System.EventHandler(this.cmdDeleteUpdate_Click);
			// 
			// cmdDeleteNoUpdate
			// 
			this.cmdDeleteNoUpdate.AppearanceKey = "actionButton";
			this.cmdDeleteNoUpdate.Location = new System.Drawing.Point(20, 77);
			this.cmdDeleteNoUpdate.Name = "cmdDeleteNoUpdate";
			this.cmdDeleteNoUpdate.Size = new System.Drawing.Size(234, 40);
			this.cmdDeleteNoUpdate.TabIndex = 6;
			this.cmdDeleteNoUpdate.Text = "Delete Line Item - No Update";
			this.ToolTip1.SetToolTip(this.cmdDeleteNoUpdate, null);
			this.cmdDeleteNoUpdate.Visible = false;
			this.cmdDeleteNoUpdate.Click += new System.EventHandler(this.cmdDeleteNoUpdate_Click);
			// 
			// fraChange
			// 
			this.fraChange.BackColor = System.Drawing.Color.White;
			this.fraChange.Controls.Add(this.txtNewValue);
			this.fraChange.Controls.Add(this.cmdUpdate);
			this.fraChange.Controls.Add(this.cmdCancel);
			this.fraChange.Controls.Add(this.vsNewAccount);
			this.fraChange.Controls.Add(this.vsOldValue);
			this.fraChange.Controls.Add(this.lblOldValue);
			this.fraChange.Controls.Add(this.lblNewValue);
			this.fraChange.Location = new System.Drawing.Point(686, 250);
			this.fraChange.Name = "fraChange";
			this.fraChange.Size = new System.Drawing.Size(375, 196);
			this.fraChange.TabIndex = 13;
			this.fraChange.Text = "Update Value";
			this.ToolTip1.SetToolTip(this.fraChange, null);
			this.fraChange.Visible = false;
			// 
			// txtNewValue
			// 
			this.txtNewValue.BackColor = System.Drawing.SystemColors.Window;
			this.txtNewValue.Location = new System.Drawing.Point(168, 90);
			this.txtNewValue.Name = "txtNewValue";
			this.txtNewValue.Size = new System.Drawing.Size(187, 40);
			this.txtNewValue.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.txtNewValue, null);
			this.txtNewValue.Visible = false;
			this.txtNewValue.Validating += new System.ComponentModel.CancelEventHandler(this.txtNewValue_Validating);
			this.txtNewValue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewValue_KeyPress);
			// 
			// cmdUpdate
			// 
			this.cmdUpdate.AppearanceKey = "actionButton";
			this.cmdUpdate.Location = new System.Drawing.Point(98, 149);
			this.cmdUpdate.Name = "cmdUpdate";
			this.cmdUpdate.Size = new System.Drawing.Size(84, 40);
			this.cmdUpdate.TabIndex = 17;
			this.cmdUpdate.Text = "Update";
			this.ToolTip1.SetToolTip(this.cmdUpdate, null);
			this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(192, 150);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(84, 40);
			this.cmdCancel.TabIndex = 16;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// vsNewAccount
			// 
			this.vsNewAccount.Cols = 1;
			this.vsNewAccount.ColumnHeadersVisible = false;
			this.vsNewAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsNewAccount.ExtendLastCol = true;
			this.vsNewAccount.FixedCols = 0;
			this.vsNewAccount.FixedRows = 0;
			this.vsNewAccount.Location = new System.Drawing.Point(168, 90);
			this.vsNewAccount.Name = "vsNewAccount";
			this.vsNewAccount.ReadOnly = false;
			this.vsNewAccount.RowHeadersVisible = false;
			this.vsNewAccount.Rows = 1;
			this.vsNewAccount.Size = new System.Drawing.Size(187, 42);
			this.vsNewAccount.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.vsNewAccount, null);
			this.vsNewAccount.Validating += new System.ComponentModel.CancelEventHandler(this.vsNewAccount_Validating);
			this.vsNewAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsNewAccount_KeyPressEvent);
			// 
			// vsOldValue
			// 
			this.vsOldValue.Cols = 1;
			this.vsOldValue.ColumnHeadersVisible = false;
			this.vsOldValue.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsOldValue.ExtendLastCol = true;
			this.vsOldValue.FixedCols = 0;
			this.vsOldValue.FixedRows = 0;
			this.vsOldValue.Location = new System.Drawing.Point(168, 30);
			this.vsOldValue.Name = "vsOldValue";
			this.vsOldValue.ReadOnly = false;
			this.vsOldValue.RowHeadersVisible = false;
			this.vsOldValue.Rows = 1;
			this.vsOldValue.Size = new System.Drawing.Size(187, 42);
			this.vsOldValue.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.vsOldValue, null);
			// 
			// lblOldValue
			// 
			this.lblOldValue.Location = new System.Drawing.Point(20, 44);
			this.lblOldValue.Name = "lblOldValue";
			this.lblOldValue.Size = new System.Drawing.Size(116, 20);
			this.lblOldValue.TabIndex = 20;
			this.lblOldValue.Text = "OLD ACCOUNT";
			this.ToolTip1.SetToolTip(this.lblOldValue, null);
			// 
			// lblNewValue
			// 
			this.lblNewValue.Location = new System.Drawing.Point(20, 104);
			this.lblNewValue.Name = "lblNewValue";
			this.lblNewValue.Size = new System.Drawing.Size(116, 20);
			this.lblNewValue.TabIndex = 19;
			this.lblNewValue.Text = "NEW ACCOUNT";
			this.ToolTip1.SetToolTip(this.lblNewValue, null);
			// 
			// cboPeriod
			// 
			this.cboPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriod.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
			this.cboPeriod.Location = new System.Drawing.Point(100, 66);
			this.cboPeriod.Name = "cboPeriod";
			this.cboPeriod.Size = new System.Drawing.Size(263, 40);
			this.cboPeriod.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.cboPeriod, null);
			// 
			// vs2
			// 
			this.vs2.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs2.Cols = 6;
			this.vs2.Location = new System.Drawing.Point(30, 370);
			this.vs2.Name = "vs2";
			this.vs2.Rows = 1;
			this.vs2.Size = new System.Drawing.Size(933, 73);
			this.vs2.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vs2, "Double click on the line you wish to make the correction to.");
			this.vs2.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
			this.vs2.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
			this.vs2.Enter += new System.EventHandler(this.vs2_Enter);
			this.vs2.DoubleClick += new System.EventHandler(this.vs2_DblClick);
			this.vs2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs2_KeyPressEvent);
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 7;
			this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vs1.Location = new System.Drawing.Point(30, 162);
			this.vs1.Name = "vs1";
			this.vs1.Rows = 1;
			this.vs1.Size = new System.Drawing.Size(933, 152);
			this.ToolTip1.SetToolTip(this.vs1, "Click on a journal to view entries");
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Sorted += new System.EventHandler(this.vs1_AfterSort);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			// 
			// lblInstruct
			// 
			this.lblInstruct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblInstruct.Location = new System.Drawing.Point(30, 30);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(454, 16);
			this.lblInstruct.TabIndex = 12;
			this.lblInstruct.Text = "ANY CHANGES MADE IN THIS SCREEN WILL TAKE EFFECT IMMEDIATELY";
			this.lblInstruct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblInstruct, null);
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(30, 80);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(64, 18);
			this.lblPeriod.TabIndex = 11;
			this.lblPeriod.Text = "PERIOD";
			this.ToolTip1.SetToolTip(this.lblPeriod, null);
			// 
			// lblEntries
			// 
			this.lblEntries.Location = new System.Drawing.Point(30, 126);
			this.lblEntries.Name = "lblEntries";
			this.lblEntries.Size = new System.Drawing.Size(111, 16);
			this.lblEntries.TabIndex = 3;
			this.lblEntries.Text = "ENCUMBRANCES";
			this.lblEntries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblEntries, null);
			// 
			// lblDetails
			// 
			this.lblDetails.Location = new System.Drawing.Point(30, 334);
			this.lblDetails.Name = "lblDetails";
			this.lblDetails.Size = new System.Drawing.Size(62, 16);
			this.lblDetails.TabIndex = 2;
			this.lblDetails.Text = "DETAILS";
			this.lblDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblDetails, null);
			// 
			// frmEncumbranceUpdate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 631);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEncumbranceUpdate";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Encumbrance Corrections";
			this.ToolTip1.SetToolTip(this, null);
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmEncumbranceUpdate_Load);
			this.Activated += new System.EventHandler(this.frmEncumbranceUpdate_Activated);
			this.Resize += new System.EventHandler(this.frmEncumbranceUpdate_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEncumbranceUpdate_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEncumbranceUpdate_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraUpdate)).EndInit();
			this.fraUpdate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteNoUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraChange)).EndInit();
			this.fraChange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdUpdate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsNewAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsOldValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
