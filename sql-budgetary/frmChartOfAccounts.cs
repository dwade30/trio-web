﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChartOfAccounts.
	/// </summary>
	public partial class frmChartOfAccounts : BaseForm
	{
		public frmChartOfAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChartOfAccounts InstancePtr
		{
			get
			{
				return (frmChartOfAccounts)Sys.GetInstance(typeof(frmChartOfAccounts));
			}
		}

		protected frmChartOfAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/3/02
		// This form will be used to choose which type of accounts
		// to list in the chart of accounts report
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			//rptChartOfAccounts.InstancePtr.Hide();
			//Application.DoEvents();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptChartOfAccounts.InstancePtr);
		}

		private void frmChartOfAccounts_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmChartOfAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmChartOfAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChartOfAccounts.FillStyle	= 0;
			//frmChartOfAccounts.ScaleWidth	= 5880;
			//frmChartOfAccounts.ScaleHeight	= 4380;
			//frmChartOfAccounts.LinkTopic	= "Form2";
			//frmChartOfAccounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			//cmbDepartment.SelectedIndex = 0;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			//rptChartOfAccounts.InstancePtr.Hide();
			//Application.DoEvents();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptChartOfAccounts.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			//rptChartOfAccounts.InstancePtr.Hide();
			//Application.DoEvents();
			modDuplexPrinting.DuplexPrintReport(rptChartOfAccounts.InstancePtr);
		}

		private void optDepartment_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbDepartment.SelectedIndex == 0)
			{
				if (modRegionalTown.IsRegionalTown())
				{
					chkRegionalOption.Visible = true;
				}
				else
				{
					chkRegionalOption.Visible = false;
				}
			}
			else if (cmbDepartment.SelectedIndex == 1)
			{
				chkRegionalOption.Visible = false;
				chkRegionalOption.CheckState = CheckState.Unchecked;
			}
			else if (cmbDepartment.SelectedIndex == 2)
			{
				chkRegionalOption.CheckState = CheckState.Unchecked;
				chkRegionalOption.Visible = false;
			}
			else if (cmbDepartment.SelectedIndex == 3)
			{
				chkRegionalOption.CheckState = CheckState.Unchecked;
				chkRegionalOption.Visible = false;
			}
		}
	}
}
