﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cJournalBriefInfo
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private string strJournalType = string.Empty;
		private int intPeriod;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}
	}
}
