﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeChart.
	/// </summary>
	public partial class frmPurgeChart : BaseForm
	{
		public frmPurgeChart()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeChart InstancePtr
		{
			get
			{
				return (frmPurgeChart)Sys.GetInstance(typeof(frmPurgeChart));
			}
		}

		protected frmPurgeChart _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmPurgeChart_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (cboCharts.Items.Count == 0)
			{
				MessageBox.Show("No charts to purge", "No Charts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			this.Refresh();
		}

		private void frmPurgeChart_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeChart.FillStyle	= 0;
			//frmPurgeChart.ScaleWidth	= 3885;
			//frmPurgeChart.ScaleHeight	= 1995;
			//frmPurgeChart.LinkTopic	= "Form2";
			//frmPurgeChart.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsChartInfo = new clsDRWrapper();
			rsChartInfo.OpenRecordset("SELECT * FROM CashCharts ORDER BY Title");
			if (rsChartInfo.EndOfFile() != true && rsChartInfo.BeginningOfFile() != true)
			{
				do
				{
					cboCharts.AddItem(rsChartInfo.Get_Fields_String("Title"));
					cboCharts.ItemData(cboCharts.ListCount - 1, FCConvert.ToInt32(rsChartInfo.Get_Fields_Int32("ID")));
					rsChartInfo.MoveNext();
				}
				while (rsChartInfo.EndOfFile() != true);
			}
			if (cboCharts.Items.Count > 0)
			{
				cboCharts.SelectedIndex = 0;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPurgeChart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rs = new clsDRWrapper();
			if (cboCharts.Items.Count > 0)
			{
				ans = MessageBox.Show("Are you sure you wish to delete this cash chart?", "Delete Chart?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rs.Execute("DELETE FROM ItemsInChart WHERE CashChartID = " + FCConvert.ToString(cboCharts.ItemData(cboCharts.SelectedIndex)), "Budgetary");
					rs.Execute("DELETE FROM CashCharts WHERE ID = " + FCConvert.ToString(cboCharts.ItemData(cboCharts.SelectedIndex)), "Budgetary");
					MessageBox.Show("Chart Deleted", "Chart Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboCharts.Items.RemoveAt(cboCharts.SelectedIndex);
					if (cboCharts.Items.Count > 0)
					{
						cboCharts.SelectedIndex = 0;
					}
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
