﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptReportBadVendors.
	/// </summary>
	public partial class rptReportBadVendors : BaseSectionReport
	{
		public static rptReportBadVendors InstancePtr
		{
			get
			{
				return (rptReportBadVendors)Sys.GetInstance(typeof(rptReportBadVendors));
			}
		}

		protected rptReportBadVendors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReportBadVendors	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intTotal;

		public rptReportBadVendors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bad Vendor List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			intTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) + "  " + rsInfo.Get_Fields_String("CheckName");
			intTotal += 1;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = intTotal.ToString();
		}

		public void Init(ref string strVendors, bool modalDialog)
		{
			rsInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber IN " + strVendors + " ORDER BY CheckName");
			//this.ShowDialog();
			frmReportViewer.InstancePtr.Init(this, "", 1, showModal: modalDialog);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
			if (this.PageNumber == 1)
			{
				Label15.Visible = true;
			}
			else
			{
				Label15.Visible = false;
			}
		}

		
	}
}
