﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptExpRevSummary.
	/// </summary>
	partial class rptExpRevSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExpRevSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader4 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter4 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDeptRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTitle2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DeptBinder2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DepartmentBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ExpRevBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblExpRev = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.subLowerLevelReport = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldExpRevTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpRevBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpRevCurrentMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpRevYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpRevBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpRevSpent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotalBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotalCurrentMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotalYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTotalBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DeptBinder2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DepartmentBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ExpRevBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpRev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevCurrentMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevSpent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalCurrentMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.subLowerLevelReport
			});
			this.Detail.Height = 0.125F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblDateRange,
				this.lblDeptRange,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Line1,
				this.Field26,
				this.lblDeptDiv,
				this.Field28,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field33,
				this.Field34
			});
			this.PageHeader.Height = 1.229167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader4
			// 
			this.GroupHeader4.Format += new System.EventHandler(this.GroupHeader4_Format);
			this.GroupHeader4.CanShrink = true;
			this.GroupHeader4.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDeptTitle2
			});
			this.GroupHeader4.Height = 0.1875F;
			this.GroupHeader4.Name = "GroupHeader4";
			this.GroupHeader4.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter4
			// 
			this.GroupFooter4.Height = 0F;
			this.GroupFooter4.Name = "GroupFooter4";
			// 
			// GroupHeader3
			// 
			this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.DeptBinder2
			});
			this.GroupHeader3.DataField = "DeptBinder2";
			this.GroupHeader3.Height = 0F;
			this.GroupHeader3.Name = "GroupHeader3";
			// 
			// GroupFooter3
			// 
			this.GroupFooter3.Format += new System.EventHandler(this.GroupFooter3_Format);
			this.GroupFooter3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field36,
				this.fldDeptTotalBudget,
				this.fldDeptTotalCurrentMonth,
				this.fldDeptTotalYTD,
				this.fldDeptTotalBalance,
				this.Line2,
				this.Line3
			});
			this.GroupFooter3.Height = 0.3020833F;
			this.GroupFooter3.Name = "GroupFooter3";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.DepartmentBinder,
				this.fldDeptTitle
			});
			this.GroupHeader1.DataField = "DepartmentBinder";
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.CanShrink = true;
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.ExpRevBinder,
				this.lblExpRev
			});
			this.GroupHeader2.DataField = "ExpRevBinder";
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldExpRevTotal,
				this.fldExpRevBudget,
				this.fldExpRevCurrentMonth,
				this.fldExpRevYTD,
				this.fldExpRevBalance,
				this.fldExpRevSpent
			});
			this.GroupFooter2.Height = 0.2604167F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Exp / Rev Summary Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.1875F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.375F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label5";
			this.lblDateRange.Top = 0.40625F;
			this.lblDateRange.Width = 5.4375F;
			// 
			// lblDeptRange
			// 
			this.lblDeptRange.Height = 0.1875F;
			this.lblDeptRange.HyperLink = null;
			this.lblDeptRange.Left = 1.5F;
			this.lblDeptRange.Name = "lblDeptRange";
			this.lblDeptRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDeptRange.Text = "Label6";
			this.lblDeptRange.Top = 0.21875F;
			this.lblDeptRange.Width = 5.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.21875F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 1.21875F;
			this.Line1.Y2 = 1.21875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 2.875F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field26.Tag = " ";
			this.Field26.Text = "Budget";
			this.Field26.Top = 1.03125F;
			this.Field26.Width = 0.9375F;
			// 
			// lblDeptDiv
			// 
			this.lblDeptDiv.Height = 0.1875F;
			this.lblDeptDiv.Left = 0F;
			this.lblDeptDiv.Name = "lblDeptDiv";
			this.lblDeptDiv.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblDeptDiv.Tag = " ";
			this.lblDeptDiv.Text = "Account";
			this.lblDeptDiv.Top = 1.03125F;
			this.lblDeptDiv.Width = 2.46875F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 3.90625F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field28.Tag = " ";
			this.Field28.Text = "Current";
			this.Field28.Top = 0.84375F;
			this.Field28.Width = 0.9375F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 3.90625F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Month";
			this.Field29.Top = 1.03125F;
			this.Field29.Width = 0.9375F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 4.96875F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field30.Tag = " ";
			this.Field30.Text = "Year";
			this.Field30.Top = 0.84375F;
			this.Field30.Width = 0.9375F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 4.96875F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field31.Tag = " ";
			this.Field31.Text = "To Date";
			this.Field31.Top = 1.03125F;
			this.Field31.Width = 0.9375F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 6.03125F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field33.Tag = " ";
			this.Field33.Text = "Balance";
			this.Field33.Top = 1.03125F;
			this.Field33.Width = 0.9375F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 7.03125F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field34.Tag = " ";
			this.Field34.Text = "Percent";
			this.Field34.Top = 1.03125F;
			this.Field34.Width = 0.75F;
			// 
			// fldDeptTitle2
			// 
			this.fldDeptTitle2.Height = 0.1875F;
			this.fldDeptTitle2.Left = 0F;
			this.fldDeptTitle2.Name = "fldDeptTitle2";
			this.fldDeptTitle2.Style = "background-color: rgb(255,255,255); font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldDeptTitle2.Text = "Field28";
			this.fldDeptTitle2.Top = 0F;
			this.fldDeptTitle2.Width = 4.59375F;
			// 
			// DeptBinder2
			// 
			this.DeptBinder2.DataField = "DeptBinder2";
			this.DeptBinder2.Height = 0.125F;
			this.DeptBinder2.Left = 4.59375F;
			this.DeptBinder2.Name = "DeptBinder2";
			this.DeptBinder2.Text = "Field42";
			this.DeptBinder2.Top = 0.0625F;
			this.DeptBinder2.Visible = false;
			this.DeptBinder2.Width = 0.75F;
			// 
			// DepartmentBinder
			// 
			this.DepartmentBinder.DataField = "DepartmentBinder";
			this.DepartmentBinder.Height = 0.125F;
			this.DepartmentBinder.Left = 4.625F;
			this.DepartmentBinder.Name = "DepartmentBinder";
			this.DepartmentBinder.Text = "DepartmentBinder";
			this.DepartmentBinder.Top = 0.0625F;
			this.DepartmentBinder.Visible = false;
			this.DepartmentBinder.Width = 0.71875F;
			// 
			// fldDeptTitle
			// 
			this.fldDeptTitle.Height = 0.1875F;
			this.fldDeptTitle.Left = 0F;
			this.fldDeptTitle.Name = "fldDeptTitle";
			this.fldDeptTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.fldDeptTitle.Text = "Field36";
			this.fldDeptTitle.Top = 0F;
			this.fldDeptTitle.Width = 2.40625F;
			// 
			// ExpRevBinder
			// 
			this.ExpRevBinder.DataField = "ExpRevBinder";
			this.ExpRevBinder.Height = 0.125F;
			this.ExpRevBinder.Left = 4.65625F;
			this.ExpRevBinder.Name = "ExpRevBinder";
			this.ExpRevBinder.Text = "ExpRevBinder";
			this.ExpRevBinder.Top = 0.0625F;
			this.ExpRevBinder.Visible = false;
			this.ExpRevBinder.Width = 0.6875F;
			// 
			// lblExpRev
			// 
			this.lblExpRev.Height = 0.1875F;
			this.lblExpRev.HyperLink = null;
			this.lblExpRev.Left = 0F;
			this.lblExpRev.Name = "lblExpRev";
			this.lblExpRev.Style = "text-align: center";
			this.lblExpRev.Text = "Label8";
			this.lblExpRev.Top = 0.0625F;
			this.lblExpRev.Width = 4.75F;
			// 
			// subLowerLevelReport
			// 
			this.subLowerLevelReport.CloseBorder = false;
			this.subLowerLevelReport.Height = 0.125F;
			this.subLowerLevelReport.Left = 0F;
			this.subLowerLevelReport.Name = "subLowerLevelReport";
			this.subLowerLevelReport.Report = null;
			this.subLowerLevelReport.Top = 0F;
			this.subLowerLevelReport.Width = 7.96875F;
			// 
			// fldExpRevTotal
			// 
			this.fldExpRevTotal.Height = 0.1875F;
			this.fldExpRevTotal.Left = 0F;
			this.fldExpRevTotal.Name = "fldExpRevTotal";
			this.fldExpRevTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevTotal.Text = "Field36";
			this.fldExpRevTotal.Top = 0.0625F;
			this.fldExpRevTotal.Width = 2.40625F;
			// 
			// fldExpRevBudget
			// 
			this.fldExpRevBudget.Height = 0.1875F;
			this.fldExpRevBudget.Left = 2.78125F;
			this.fldExpRevBudget.Name = "fldExpRevBudget";
			this.fldExpRevBudget.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevBudget.Text = "Field36";
			this.fldExpRevBudget.Top = 0.0625F;
			this.fldExpRevBudget.Width = 1F;
			// 
			// fldExpRevCurrentMonth
			// 
			this.fldExpRevCurrentMonth.Height = 0.1875F;
			this.fldExpRevCurrentMonth.Left = 3.84375F;
			this.fldExpRevCurrentMonth.Name = "fldExpRevCurrentMonth";
			this.fldExpRevCurrentMonth.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevCurrentMonth.Text = "Field36";
			this.fldExpRevCurrentMonth.Top = 0.0625F;
			this.fldExpRevCurrentMonth.Width = 1F;
			// 
			// fldExpRevYTD
			// 
			this.fldExpRevYTD.Height = 0.1875F;
			this.fldExpRevYTD.Left = 4.90625F;
			this.fldExpRevYTD.Name = "fldExpRevYTD";
			this.fldExpRevYTD.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevYTD.Text = "Field36";
			this.fldExpRevYTD.Top = 0.0625F;
			this.fldExpRevYTD.Width = 1F;
			// 
			// fldExpRevBalance
			// 
			this.fldExpRevBalance.Height = 0.1875F;
			this.fldExpRevBalance.Left = 5.96875F;
			this.fldExpRevBalance.Name = "fldExpRevBalance";
			this.fldExpRevBalance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevBalance.Text = "Field36";
			this.fldExpRevBalance.Top = 0.0625F;
			this.fldExpRevBalance.Width = 1F;
			// 
			// fldExpRevSpent
			// 
			this.fldExpRevSpent.Height = 0.1875F;
			this.fldExpRevSpent.Left = 7.03125F;
			this.fldExpRevSpent.Name = "fldExpRevSpent";
			this.fldExpRevSpent.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpRevSpent.Text = "Field36";
			this.fldExpRevSpent.Top = 0.0625F;
			this.fldExpRevSpent.Width = 0.75F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.1875F;
			this.Field36.Left = 0F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Field36.Text = "Net Profit / (Loss)";
			this.Field36.Top = 0.03125F;
			this.Field36.Width = 2.40625F;
			// 
			// fldDeptTotalBudget
			// 
			this.fldDeptTotalBudget.Height = 0.1875F;
			this.fldDeptTotalBudget.Left = 2.78125F;
			this.fldDeptTotalBudget.Name = "fldDeptTotalBudget";
			this.fldDeptTotalBudget.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDeptTotalBudget.Text = "Field36";
			this.fldDeptTotalBudget.Top = 0.03125F;
			this.fldDeptTotalBudget.Width = 1F;
			// 
			// fldDeptTotalCurrentMonth
			// 
			this.fldDeptTotalCurrentMonth.Height = 0.1875F;
			this.fldDeptTotalCurrentMonth.Left = 3.84375F;
			this.fldDeptTotalCurrentMonth.Name = "fldDeptTotalCurrentMonth";
			this.fldDeptTotalCurrentMonth.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDeptTotalCurrentMonth.Text = "Field36";
			this.fldDeptTotalCurrentMonth.Top = 0.03125F;
			this.fldDeptTotalCurrentMonth.Width = 1F;
			// 
			// fldDeptTotalYTD
			// 
			this.fldDeptTotalYTD.Height = 0.1875F;
			this.fldDeptTotalYTD.Left = 4.90625F;
			this.fldDeptTotalYTD.Name = "fldDeptTotalYTD";
			this.fldDeptTotalYTD.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDeptTotalYTD.Text = "Field36";
			this.fldDeptTotalYTD.Top = 0.03125F;
			this.fldDeptTotalYTD.Width = 1F;
			// 
			// fldDeptTotalBalance
			// 
			this.fldDeptTotalBalance.Height = 0.1875F;
			this.fldDeptTotalBalance.Left = 5.96875F;
			this.fldDeptTotalBalance.Name = "fldDeptTotalBalance";
			this.fldDeptTotalBalance.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDeptTotalBalance.Text = "Field36";
			this.fldDeptTotalBalance.Top = 0.03125F;
			this.fldDeptTotalBalance.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.5F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.5F;
			this.Line2.X1 = 1.5F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.03125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.25F;
			this.Line3.Width = 7.4375F;
			this.Line3.X1 = 0.03125F;
			this.Line3.X2 = 7.46875F;
			this.Line3.Y1 = 0.25F;
			this.Line3.Y2 = 0.25F;
			// 
			// rptExpRevSummary
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader4);
			this.Sections.Add(this.GroupHeader3);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter3);
			this.Sections.Add(this.GroupFooter4);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DeptBinder2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DepartmentBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ExpRevBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpRev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevCurrentMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpRevSpent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalCurrentMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTotalBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subLowerLevelReport;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDeptRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox DeptBinder2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotalBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotalCurrentMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotalYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTotalBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox DepartmentBinder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTitle;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox ExpRevBinder;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpRev;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevCurrentMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpRevSpent;
	}
}
