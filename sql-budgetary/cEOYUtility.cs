﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;

namespace TWBD0000
{
	public class cEOYUtility
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDReportService repServ = new cBDReportService();
		private cBDReportService repServ_AutoInitialized;

		private cBDReportService repServ
		{
			get
			{
				if (repServ_AutoInitialized == null)
				{
					repServ_AutoInitialized = new cBDReportService();
				}
				return repServ_AutoInitialized;
			}
			set
			{
				repServ_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController acctCont = new cBDAccountController();
		private cBDAccountController acctCont_AutoInitialized;

		private cBDAccountController acctCont
		{
			get
			{
				if (acctCont_AutoInitialized == null)
				{
					acctCont_AutoInitialized = new cBDAccountController();
				}
				return acctCont_AutoInitialized;
			}
			set
			{
				acctCont_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEncumbranceController encCont = new cEncumbranceController();
		private cEncumbranceController encCont_AutoInitialized;

		private cEncumbranceController encCont
		{
			get
			{
				if (encCont_AutoInitialized == null)
				{
					encCont_AutoInitialized = new cEncumbranceController();
				}
				return encCont_AutoInitialized;
			}
			set
			{
				encCont_AutoInitialized = value;
			}
		}

		private Dictionary<object, object> acctDictionary = new Dictionary<object, object>();
		private cStandardAccounts standardAccounts;
		private Dictionary<object, object> fundsDictionary = new Dictionary<object, object>();
		private int lngYearToClose;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}

		private void EndOfYearCloseBooks(int lngYear)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				lngYearToClose = lngYear;
				cGenericCollection acctSummaries;
				cGenericCollection closingJournals = new cGenericCollection();
				cJournal TempJournal;
				cAccountSummaryItem acctSumm;
				cGenericCollection eoyAdjustments = new cGenericCollection();
				standardAccounts = acctCont.GetStandardAccounts();
				fundsDictionary = acctCont.GetFundsAsDictionary();
				// calculate ending balances
				// runningFundTotals.RemoveAll
				acctSummaries = repServ.GetAccountSummaries(false, false);
				acctSummaries.MoveFirst();
				while (acctSummaries.IsCurrent())
				{
					acctSumm = (cAccountSummaryItem)acctSummaries.GetCurrentItem();
					acctDictionary.Add(acctSumm.Account, acctSumm);
					acctSummaries.MoveNext();
				}
				// close unliquidated
				TempJournal = GetClosingUnliquidatedJournal(ref eoyAdjustments);
				if (!(TempJournal == null))
				{
					closingJournals.AddItem(TempJournal);
				}
				// close control
				TempJournal = GetClosingControlsJournal();
				if (!(TempJournal == null))
				{
					closingJournals.AddItem(TempJournal);
				}
				// post closing journal
				// save 1099
				// clear budgets
				// reset numbers
				// clear detail
				// carry over
				// save all changes at once
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private cJournal GetClosingUnliquidatedJournal(ref cGenericCollection eoyAdjustments)
		{
			cJournal GetClosingUnliquidatedJournal = null;
			cGenericCollection encs;
			GetClosingUnliquidatedJournal = null;
			encs = encCont.GetFullEncumbrancesForEOY();
			if (encCont.HadError)
			{
				SetError(encCont.LastErrorNumber, encCont.LastErrorMessage);
				return GetClosingUnliquidatedJournal;
			}
			if (encs.ItemCount() == 0)
			{
				GetClosingUnliquidatedJournal = null;
				return GetClosingUnliquidatedJournal;
			}
			cJournal journ = new cJournal();
			cEncumbrance enc;
			cEncumbranceDetail encDet = new cEncumbranceDetail();
			Dictionary<object, object> Funds = new Dictionary<object, object>();
			cJournalEntry jDet;
			// vbPorter upgrade warning: ferv As cFundExpRevValue	OnWrite(object, cFundExpRevValue)
			cFundExpRevValue ferv;
			Dictionary<object, object> runningFundTotals = new Dictionary<object, object>();
			journ.Status = "E";
			journ.StatusChangeTime = "12/30/1899 " + DateTime.Today.ToShortTimeString();
			journ.StatusChangeDate = FCConvert.ToString(DateTime.Today);
			journ.Description = "EOY Enc Closing Entries";
			journ.JournalType = "GJ";
			journ.Period = FCConvert.ToInt16(DateTime.Today.Month);
			encs.MoveFirst();
			// vbPorter upgrade warning: acctSumm As cAccountSummaryItem	OnWrite(object)
			cAccountSummaryItem acctSumm;
			// vbPorter upgrade warning: tempAcctSumm As cAccountSummaryItem	OnWrite(object)
			cAccountSummaryItem tempAcctSumm;
			cEOYAdjustment eoyAdjust;
			while (encs.IsCurrent())
			{
				enc = (cEncumbrance)encs.GetCurrentItem();
				enc.EncumbranceDetails.MoveFirst();
				while (enc.EncumbranceDetails.IsCurrent())
				{
					//Application.DoEvents();
					encDet = (cEncumbranceDetail)enc.EncumbranceDetails.GetCurrentItem();
					if (encDet.Amount + encDet.Adjustments - encDet.LiquidatedAmount != 0)
					{
						// If acctDictionary.Exists(encDet.Account) Then
						acctSumm = (cAccountSummaryItem)acctDictionary[encDet.Account];
						if (!(acctSumm == null))
						{
							jDet = new cJournalEntry();
							jDet.JournalType = "G";
							jDet.JournalEntriesDate = FCConvert.ToString(DateTime.Today);
							jDet.Description = "EOY Enc Closing Entries";
							jDet.Period = FCConvert.ToInt16(DateTime.Today.Month);
							jDet.RCB = "L";
							if (!modAccountTitle.Statics.YearFlag)
							{
								jDet.Account = "G " + acctSumm.Fund + "-" + standardAccounts.EncumbranceOffset + "-00";
							}
							else
							{
								jDet.Account = "G " + acctSumm.Fund + "-" + standardAccounts.EncumbranceOffset;
							}
							jDet.Status = "E";
							eoyAdjust = new cEOYAdjustment();
							eoyAdjust.Account = jDet.Account;
							eoyAdjust.Amount = jDet.Amount;
							eoyAdjust.Year = lngYearToClose;
							eoyAdjustments.AddItem(eoyAdjust);
							journ.JournalEntries.AddItem(jDet);
							if (acctDictionary.ContainsKey(jDet.Account))
							{
								tempAcctSumm = (cAccountSummaryItem)acctDictionary[jDet.Account];
								if (jDet.IsCredit())
								{
									tempAcctSumm.Credits -= Math.Abs(jDet.Amount);
									tempAcctSumm.AppendCreditsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
								}
								else
								{
									tempAcctSumm.Debits -= Math.Abs(jDet.Amount);
									tempAcctSumm.AppendDebitsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
								}
								tempAcctSumm.CalcNet();
								tempAcctSumm.CalcEndBalance();
							}
							jDet = new cJournalEntry();
							jDet.JournalType = "G";
							jDet.JournalEntriesDate = FCConvert.ToString(DateTime.Today);
							jDet.Description = "EOY Enc Closing Entries";
							jDet.Period = FCConvert.ToInt16(DateTime.Today.Month);
							jDet.RCB = "L";
							jDet.Status = "E";
							jDet.Account = encDet.Account;
							jDet.Amount = -1 * (encDet.Amount + encDet.Adjustments - encDet.LiquidatedAmount);
							journ.JournalEntries.AddItem(jDet);
							if (jDet.IsCredit())
							{
								acctSumm.Credits -= Math.Abs(jDet.Amount);
								acctSumm.AppendCreditsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
							}
							else
							{
								acctSumm.Debits -= Math.Abs(jDet.Amount);
								acctSumm.AppendDebitsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
							}
							acctSumm.CalcNet();
							acctSumm.CalcEndBalance();
							if (runningFundTotals.ContainsKey(acctSumm.Fund))
							{
								ferv = (cFundExpRevValue)runningFundTotals[acctSumm.Fund];
							}
							else
							{
								ferv = new cFundExpRevValue();
								ferv.Fund = acctSumm.Fund;
								runningFundTotals.Add(ferv.Fund, ferv);
							}
							ferv.FundAmount += encDet.Amount;
							if (acctSumm.IsExpense)
							{
								ferv.ExpenseAmount -= encDet.Amount;
							}
							else if (acctSumm.IsRevenue)
							{
								ferv.RevenueAmount -= encDet.Amount;
							}
						}
					}
					enc.EncumbranceDetails.MoveNext();
				}
			}
			// vbPorter upgrade warning: totalArray As object	OnRead(cFundExpRevValue)
			object[] totalArray = new object[runningFundTotals.Count];
			runningFundTotals.Values.CopyTo(totalArray, 0);
			int x;
			for (x = 0; x <= Information.UBound(totalArray, 1); x++)
			{
				ferv = (cFundExpRevValue)totalArray[x];
				if (ferv.FundAmount != 0)
				{
					jDet = new cJournalEntry();
					jDet.JournalType = "G";
					jDet.JournalEntriesDate = "Date";
					jDet.Description = "EOY Enc Closing Entries";
					jDet.Period = FCConvert.ToInt16(DateTime.Today.Month);
					jDet.RCB = "L";
					jDet.Status = "E";
					jDet.Amount = ferv.FundAmount;
					if (!modAccountTitle.Statics.YearFlag)
					{
						jDet.Account = "G " + ferv.Fund + "-" + standardAccounts.FundBalance + "-00";
					}
					else
					{
						jDet.Account = "G " + ferv.Fund + "-" + standardAccounts.FundBalance;
					}
					journ.JournalEntries.AddItem(jDet);
					if (acctDictionary.ContainsKey(encDet.Account))
					{
						tempAcctSumm = (cAccountSummaryItem)acctDictionary[jDet.Account];
						if (jDet.IsCredit())
						{
							tempAcctSumm.Credits -= Math.Abs(jDet.Amount);
							tempAcctSumm.AppendCreditsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
						}
						else
						{
							tempAcctSumm.Debits -= Math.Abs(jDet.Amount);
							tempAcctSumm.AppendDebitsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
						}
						tempAcctSumm.CalcNet();
						tempAcctSumm.CalcEndBalance();
					}
					jDet = new cJournalEntry();
					jDet.JournalType = "G";
					jDet.JournalEntriesDate = "Date";
					jDet.Description = "EOY Enc Closing Entries";
					jDet.Period = FCConvert.ToInt16(DateTime.Today.Month);
					jDet.RCB = "L";
					jDet.Status = "E";
					jDet.Amount = ferv.FundAmount * -1;
					if (!modAccountTitle.Statics.YearFlag)
					{
						jDet.Account = "G " + ferv.Fund + "-" + standardAccounts.UnliquidatedEncumbrance + "-00";
					}
					else
					{
						jDet.Account = "G " + ferv.Fund + "-" + standardAccounts.UnliquidatedEncumbrance;
					}
					journ.JournalEntries.AddItem(jDet);
					if (acctDictionary.ContainsKey(encDet.Account))
					{
						tempAcctSumm = (cAccountSummaryItem)acctDictionary[jDet.Account];
						if (jDet.IsCredit())
						{
							tempAcctSumm.Credits -= Math.Abs(jDet.Amount);
							tempAcctSumm.AppendCreditsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
						}
						else
						{
							tempAcctSumm.Debits -= Math.Abs(jDet.Amount);
							tempAcctSumm.AppendDebitsForPeriod(jDet.Period, -Math.Abs(jDet.Amount));
						}
						tempAcctSumm.CalcNet();
						tempAcctSumm.CalcEndBalance();
					}
					eoyAdjust = new cEOYAdjustment();
					eoyAdjust.Account = jDet.Account;
					eoyAdjust.Amount = jDet.Amount;
					eoyAdjust.Year = lngYearToClose;
					eoyAdjustments.AddItem(eoyAdjust);
				}
			}
			if (journ.JournalEntries.ItemCount() > 0)
			{
				GetClosingUnliquidatedJournal = journ;
			}
			return GetClosingUnliquidatedJournal;
		}

		private cJournal GetClosingControlsJournal()
		{
			cJournal GetClosingControlsJournal = null;
			cJournal journ = new cJournal();
			cGenericCollection expControlAccounts;
			cGenericCollection revControlAccounts;
			cBDAccount tAcct;
			// vbPorter upgrade warning: tempAcctSumm As cAccountSummaryItem	OnWrite(object)
			cAccountSummaryItem tempAcctSumm;
			cJournalEntry jDet;
			expControlAccounts = acctCont.GetGLAccountsByAccountObject(standardAccounts.ExpenseControl);
			revControlAccounts = acctCont.GetGLAccountsByAccountObject(standardAccounts.RevenueControl);
			// for each fund get ytd net of exp ctrl account and rev ctrl accounts and make journal entries
			expControlAccounts.MoveFirst();
			revControlAccounts.MoveFirst();
			// Master.Fields["JournalNumber"] = TempJournal
			journ.Status = "E";
			journ.StatusChangeTime = "12/30/1899 " + DateTime.Today.ToShortTimeString();
			journ.StatusChangeDate = FCConvert.ToString(DateTime.Today);
			journ.Description = "EOY Closing Entries";
			journ.JournalType = "GJ";
			journ.Period = FCConvert.ToInt16(DateTime.Today.Month);
			while (expControlAccounts.IsCurrent())
			{
				tAcct = (cBDAccount)expControlAccounts.GetCurrentItem();
				if (acctDictionary.ContainsKey(tAcct.Account))
				{
					tempAcctSumm = (cAccountSummaryItem)acctDictionary[tAcct.Account];
					jDet = new cJournalEntry();
					jDet.JournalType = "G";
					jDet.JournalEntriesDate = FCConvert.ToString(DateTime.Today);
					jDet.Description = "EOY EC Closing Entries";
					jDet.Period = FCConvert.ToInt16(DateTime.Today.Month);
					jDet.Project = "";
					jDet.Account = tAcct.Account;
					jDet.Status = "E";
					jDet.RCB = "L";
					jDet.Amount = tempAcctSumm.Net * -1;
					journ.JournalEntries.AddItem(jDet);
				}
				expControlAccounts.MoveNext();
			}
			while (revControlAccounts.IsCurrent())
			{
				tAcct = (cBDAccount)revControlAccounts.GetCurrentItem();
				if (acctDictionary.ContainsKey(tAcct.Account))
				{
					tempAcctSumm = (cAccountSummaryItem)acctDictionary[tAcct.Account];
				}
				revControlAccounts.MoveNext();
			}
			return GetClosingControlsJournal;
		}
	}
}
