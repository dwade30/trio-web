﻿//Fecher vbPorter - Version 1.0.0.27

using System.Collections.Generic;
using fecherFoundation;
using iTextSharp.text;

namespace TWBD0000
{
	public class clsACHBatch
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHBatchHeader tBatchHeader = new clsACHBatchHeader();
		private clsACHBatchHeader tBatchHeader_AutoInitialized;

		private clsACHBatchHeader tBatchHeader
		{
			get
			{
				if (tBatchHeader_AutoInitialized == null)
				{
					tBatchHeader_AutoInitialized = new clsACHBatchHeader();
				}
				return tBatchHeader_AutoInitialized;
			}
			set
			{
				tBatchHeader_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHBatchControlRecord tBatchControlRecord = new clsACHBatchControlRecord();
		private clsACHBatchControlRecord tBatchControlRecord_AutoInitialized;

		private clsACHBatchControlRecord tBatchControlRecord
		{
			get
			{
				if (tBatchControlRecord_AutoInitialized == null)
				{
					tBatchControlRecord_AutoInitialized = new clsACHBatchControlRecord();
				}
				return tBatchControlRecord_AutoInitialized;
			}
			set
			{
				tBatchControlRecord_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection tDets = new FCCollection();
		private List<clsACHDetailRecord> tDets_AutoInitialized;

		private List<clsACHDetailRecord> tDets
		{
			get
			{
				if (tDets_AutoInitialized == null)
				{
					tDets_AutoInitialized = new List<clsACHDetailRecord>();
				}
				return tDets_AutoInitialized;
			}
			set
			{
				tDets_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection tOffsets = new FCCollection();
		private FCCollection tOffsets_AutoInitialized;

		private FCCollection tOffsets
		{
			get
			{
				if (tOffsets_AutoInitialized == null)
				{
					tOffsets_AutoInitialized = new FCCollection();
				}
				return tOffsets_AutoInitialized;
			}
			set
			{
				tOffsets_AutoInitialized = value;
			}
		}

		private int intBlocks;
		private int intBatchNumber;

		public void AddOffset(ref clsACHOffsetRecord TRec)
		{
			tOffsets.Add(TRec);
		}

		public void AddDetail(ref clsACHDetailRecord TRec)
		{
			tDets.Add(TRec);
		}

		public FCCollection Offsets()
		{
			FCCollection Offsets = null;
			Offsets = tOffsets;
			return Offsets;
		}

		public List<clsACHDetailRecord> Details()
		{
            List<clsACHDetailRecord> Details = null;
			Details = tDets;
			return Details;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short BatchNumber
		{
			get
			{
				short BatchNumber = 0;
				BatchNumber = FCConvert.ToInt16(intBatchNumber);
				return BatchNumber;
			}
			set
			{
				intBatchNumber = value;
			}
		}

		public double Debits
		{
			get
			{
				double Debits = 0;
				Debits = tBatchControlRecord.TotalDebit;
				return Debits;
			}
		}

		public double Credits
		{
			get
			{
				double Credits = 0;
				Credits = tBatchControlRecord.TotalCredit;
				return Credits;
			}
		}

		public double Hash
		{
			get
			{
				double Hash = 0;
				Hash = tBatchControlRecord.Hash;
				return Hash;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short BlockRecords
		{
			get
			{
				short BlockRecords = 0;
				BlockRecords = FCConvert.ToInt16(intBlocks);
				return BlockRecords;
			}
			set
			{
				intBlocks = value;
			}
		}

		public clsACHBatchHeader BatchHeader()
		{
			clsACHBatchHeader BatchHeader = null;
			BatchHeader = tBatchHeader;
			return BatchHeader;
		}

		public clsACHBatchControlRecord BatchControl()
		{
			clsACHBatchControlRecord BatchControl = null;
			BatchControl = tBatchControlRecord;
			return BatchControl;
		}

		public double CalcTotalCredits()
		{
			double CalcTotalCredits = 0;
			double dblTotalCredits;
			dblTotalCredits = 0;
			foreach (clsACHDetailRecord TRec in tDets)
			{
				dblTotalCredits += TRec.TotalAmount;
			}
			// TRec
			CalcTotalCredits = dblTotalCredits;
			return CalcTotalCredits;
		}

		public void BuildBatchControlRecord()
		{
			int intRecCount = 0;
			double dblHashNumber = 0;
			double dblTotalDebits = 0;
			double dblTotalCredits = 0;
			intBlocks = 2;
			foreach (clsACHDetailRecord TRec in tDets)
			{
				intRecCount += 1;
				TRec.VendorNumber = FCConvert.ToInt16(intRecCount);
				dblHashNumber += TRec.HashNumber;
				dblTotalCredits += TRec.TotalAmount;
			}
			// TRec
			foreach (clsACHOffsetRecord tOff in tOffsets)
			{
				intRecCount += 1;
				tOff.VendorNumber = FCConvert.ToInt16(intRecCount);
				dblHashNumber += tOff.HashNumber;
				dblTotalDebits += tOff.TotalAmount;
			}
			// tOff
			intBlocks += intRecCount;
			tBatchControlRecord.Hash = dblHashNumber;
			tBatchControlRecord.EntryAddendaCount = FCConvert.ToInt16(intRecCount);
			tBatchControlRecord.TotalCredit = dblTotalCredits;
			tBatchControlRecord.TotalDebit = dblTotalDebits;
			tBatchControlRecord.BatchNumber = FCConvert.ToInt16(intBatchNumber);
		}
	}
}
