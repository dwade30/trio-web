﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerSummary.
	/// </summary>
	partial class frmLedgerSummary : BaseForm
	{
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblMonthLabel;
		public fecherFoundation.FCLabel lblMonths;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel lblRangeDept;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLedgerSummary));
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblMonthLabel = new fecherFoundation.FCLabel();
			this.lblMonths = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.lblRangeDept = new fecherFoundation.FCLabel();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrintPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(798, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblMonthLabel);
			this.ClientArea.Controls.Add(this.lblMonths);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Controls.Add(this.lblRangeDept);
			this.ClientArea.Size = new System.Drawing.Size(798, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(798, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(282, 30);
			this.HeaderText.Text = "Ledger Summary Report";
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedRows = 2;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 78);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 2;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(738, 420);
			this.vs1.StandardTab = true;
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs1.TabIndex = 0;
			this.vs1.Visible = false;
			this.vs1.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
			this.vs1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
			// 
			// lblMonthLabel
			// 
			this.lblMonthLabel.Location = new System.Drawing.Point(30, 53);
			this.lblMonthLabel.Name = "lblMonthLabel";
			this.lblMonthLabel.Size = new System.Drawing.Size(141, 14);
			this.lblMonthLabel.TabIndex = 6;
			this.lblMonthLabel.Text = "FOR THE MONTH(S) OF";
			this.lblMonthLabel.Visible = false;
			// 
			// lblMonths
			// 
			this.lblMonths.Location = new System.Drawing.Point(192, 53);
			this.lblMonths.Name = "lblMonths";
			this.lblMonths.Size = new System.Drawing.Size(161, 14);
			this.lblMonths.TabIndex = 5;
			this.lblMonths.Visible = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(30, 30);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(104, 14);
			this.lblTitle.TabIndex = 4;
			this.lblTitle.Visible = false;
			// 
			// lblRangeDept
			// 
			this.lblRangeDept.Location = new System.Drawing.Point(192, 30);
			this.lblRangeDept.Name = "lblRangeDept";
			this.lblRangeDept.Size = new System.Drawing.Size(346, 14);
			this.lblRangeDept.TabIndex = 3;
			this.lblRangeDept.Visible = false;
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(725, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(43, 24);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Enabled = false;
			this.cmdPrintPreview.Location = new System.Drawing.Point(327, 30);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.Size = new System.Drawing.Size(145, 48);
			this.cmdPrintPreview.TabIndex = 2;
			this.cmdPrintPreview.Text = "Print / Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
			// 
			// frmLedgerSummary
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(798, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLedgerSummary";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Ledger Summary Report";
			this.Load += new System.EventHandler(this.frmLedgerSummary_Load);
			this.Activated += new System.EventHandler(this.frmLedgerSummary_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLedgerSummary_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton cmdPrintPreview;
		public FCButton cmdPrint;
	}
}
