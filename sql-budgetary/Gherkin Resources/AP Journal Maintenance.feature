﻿Feature: AP Journal Maintenance

Adding, editing and reversing AP Journals

Background: 
	Given an AP Journal with at least one invoice

@create_journal
Scenario: Creating a journal with invoices - journal and invoices must have same valid pay period
	When the journal is created
	Then the journal must have a valid pay period
	And all invoices must have the same pay period as the journal

@update_journal	
Scenario: Updating a journal with invoices - journal and invoices must have same valid pay period
	When the journal is updated
	Then the journal must have a valid pay period
	And all invoices must have the same pay period as the journal