//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateCustomReport.
	/// </summary>
	partial class frmCreateCustomReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkAccountDescription;
		public fecherFoundation.FCCheckBox chkAccountNumber;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraInclude;
		public fecherFoundation.FCCheckBox chkYTD;
		public fecherFoundation.FCCheckBox chkYTDDebCred;
		public fecherFoundation.FCCheckBox chkCurrentDebCred;
		public fecherFoundation.FCCheckBox chkBudget;
		public fecherFoundation.FCCheckBox chkCurrent;
		public fecherFoundation.FCCheckBox chkBalance;
		public fecherFoundation.FCCheckBox chkPercent;
		public fecherFoundation.FCFrame fraYTDInclude;
		public fecherFoundation.FCCheckBox chkPending;
		public fecherFoundation.FCCheckBox chkEncumbrances;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCGrid vsCustomFormat;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdFileNew;
		public fecherFoundation.FCButton cmdFileDelete;
		public fecherFoundation.FCButton cmdFileAccounts;
		public fecherFoundation.FCButton cmdFilePreview;
		public fecherFoundation.FCButton cmdFileSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateCustomReport));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.chkAccountDescription = new fecherFoundation.FCCheckBox();
            this.chkAccountNumber = new fecherFoundation.FCCheckBox();
            this.fraMonths = new fecherFoundation.FCFrame();
            this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.fraInclude = new fecherFoundation.FCFrame();
            this.lblNote = new fecherFoundation.FCLabel();
            this.chkYTD = new fecherFoundation.FCCheckBox();
            this.chkYTDDebCred = new fecherFoundation.FCCheckBox();
            this.chkCurrentDebCred = new fecherFoundation.FCCheckBox();
            this.chkBudget = new fecherFoundation.FCCheckBox();
            this.chkCurrent = new fecherFoundation.FCCheckBox();
            this.chkBalance = new fecherFoundation.FCCheckBox();
            this.chkPercent = new fecherFoundation.FCCheckBox();
            this.fraYTDInclude = new fecherFoundation.FCFrame();
            this.chkPending = new fecherFoundation.FCCheckBox();
            this.chkEncumbrances = new fecherFoundation.FCCheckBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.vsCustomFormat = new fecherFoundation.FCGrid();
            this.lblExpense = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdFileNew = new fecherFoundation.FCButton();
            this.cmdFileDelete = new fecherFoundation.FCButton();
            this.cmdFileAccounts = new fecherFoundation.FCButton();
            this.cmdFilePreview = new fecherFoundation.FCButton();
            this.cmdFileSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
            this.fraMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInclude)).BeginInit();
            this.fraInclude.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebCred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentDebCred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTDInclude)).BeginInit();
            this.fraYTDInclude.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPending)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEncumbrances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCustomFormat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraMonths);
            this.ClientArea.Controls.Add(this.fraInclude);
            this.ClientArea.Controls.Add(this.fraYTDInclude);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.vsCustomFormat);
            this.ClientArea.Controls.Add(this.lblExpense);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileAccounts);
            this.TopPanel.Controls.Add(this.cmdFileDelete);
            this.TopPanel.Controls.Add(this.cmdFileNew);
            this.TopPanel.Controls.Add(this.cmdFilePreview);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileAccounts, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(158, 30);
            this.HeaderText.Text = "Report Writer";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "Range of Months",
            "Single Month",
            "All"});
            this.cmbRange.Location = new System.Drawing.Point(148, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(208, 40);
            this.cmbRange.TabIndex = 19;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(61, 15);
            this.lblRange.TabIndex = 20;
            this.lblRange.Text = "MONTHS";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.chkAccountDescription);
            this.Frame1.Controls.Add(this.chkAccountNumber);
            this.Frame1.Location = new System.Drawing.Point(651, 75);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(204, 100);
            this.Frame1.TabIndex = 20;
            this.Frame1.Text = "Account Information";
            // 
            // chkAccountDescription
            // 
            this.chkAccountDescription.Location = new System.Drawing.Point(20, 63);
            this.chkAccountDescription.Name = "chkAccountDescription";
            this.chkAccountDescription.Size = new System.Drawing.Size(173, 27);
            this.chkAccountDescription.TabIndex = 22;
            this.chkAccountDescription.Text = "Account Description";
            // 
            // chkAccountNumber
            // 
            this.chkAccountNumber.Checked = true;
            this.chkAccountNumber.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkAccountNumber.Location = new System.Drawing.Point(20, 30);
            this.chkAccountNumber.Name = "chkAccountNumber";
            this.chkAccountNumber.Size = new System.Drawing.Size(149, 27);
            this.chkAccountNumber.TabIndex = 21;
            this.chkAccountNumber.Text = "Account Number";
            // 
            // fraMonths
            // 
            this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
            this.fraMonths.Controls.Add(this.chkCheckDateRange);
            this.fraMonths.Controls.Add(this.cmbRange);
            this.fraMonths.Controls.Add(this.lblRange);
            this.fraMonths.Controls.Add(this.cboEndingMonth);
            this.fraMonths.Controls.Add(this.cboBeginningMonth);
            this.fraMonths.Controls.Add(this.cboSingleMonth);
            this.fraMonths.Controls.Add(this.lblTo_0);
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Location = new System.Drawing.Point(30, 75);
            this.fraMonths.Name = "fraMonths";
            this.fraMonths.Size = new System.Drawing.Size(376, 200);
            this.fraMonths.TabIndex = 11;
            this.fraMonths.Text = "Month(s) To Report";
            // 
            // chkCheckDateRange
            // 
            this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckDateRange.Location = new System.Drawing.Point(20, 89);
            this.chkCheckDateRange.Name = "chkCheckDateRange";
            this.chkCheckDateRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckDateRange.TabIndex = 18;
            this.chkCheckDateRange.Text = "Select at report time";
            this.chkCheckDateRange.Visible = false;
            this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(216, 131);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(140, 40);
            this.cboEndingMonth.TabIndex = 16;
            this.cboEndingMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 131);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(140, 40);
            this.cboBeginningMonth.TabIndex = 15;
            this.cboBeginningMonth.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(20, 131);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(140, 40);
            this.cboSingleMonth.TabIndex = 12;
            this.cboSingleMonth.Visible = false;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_0.Location = new System.Drawing.Point(180, 145);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(21, 20);
            this.lblTo_0.TabIndex = 19;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.Visible = false;
            // 
            // fraInclude
            // 
            this.fraInclude.Controls.Add(this.lblNote);
            this.fraInclude.Controls.Add(this.chkYTD);
            this.fraInclude.Controls.Add(this.chkYTDDebCred);
            this.fraInclude.Controls.Add(this.chkCurrentDebCred);
            this.fraInclude.Controls.Add(this.chkBudget);
            this.fraInclude.Controls.Add(this.chkCurrent);
            this.fraInclude.Controls.Add(this.chkBalance);
            this.fraInclude.Controls.Add(this.chkPercent);
            this.fraInclude.Location = new System.Drawing.Point(436, 195);
            this.fraInclude.Name = "fraInclude";
            this.fraInclude.Size = new System.Drawing.Size(612, 160);
            this.fraInclude.TabIndex = 5;
            this.fraInclude.Text = "Include In Report";
            // 
            // lblNote
            // 
            this.lblNote.Location = new System.Drawing.Point(412, 39);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(175, 69);
            this.lblNote.TabIndex = 27;
            this.lblNote.Tag = "";
            this.lblNote.Text = "NOTE:  YOU MAY ONLY SELECT 5 FIELDS TO DISPLAY ON A REPORT.  DEBIT / CREDIT FIELD" +
    "S COUNT AS 2 FIELDS";
            // 
            // chkYTD
            // 
            this.chkYTD.Location = new System.Drawing.Point(20, 120);
            this.chkYTD.Name = "chkYTD";
            this.chkYTD.Size = new System.Drawing.Size(90, 27);
            this.chkYTD.TabIndex = 8;
            this.chkYTD.Text = "YTD Net";
            this.chkYTD.CheckedChanged += new System.EventHandler(this.chkYTD_CheckedChanged);
            // 
            // chkYTDDebCred
            // 
            this.chkYTDDebCred.Location = new System.Drawing.Point(226, 30);
            this.chkYTDDebCred.Name = "chkYTDDebCred";
            this.chkYTDDebCred.Size = new System.Drawing.Size(177, 27);
            this.chkYTDDebCred.TabIndex = 26;
            this.chkYTDDebCred.Text = "YTD Debits / Credits";
            this.chkYTDDebCred.CheckedChanged += new System.EventHandler(this.chkYTDDebCred_CheckedChanged);
            // 
            // chkCurrentDebCred
            // 
            this.chkCurrentDebCred.Location = new System.Drawing.Point(20, 90);
            this.chkCurrentDebCred.Name = "chkCurrentDebCred";
            this.chkCurrentDebCred.Size = new System.Drawing.Size(199, 27);
            this.chkCurrentDebCred.TabIndex = 25;
            this.chkCurrentDebCred.Text = "Current Debits / Credits";
            this.chkCurrentDebCred.CheckedChanged += new System.EventHandler(this.chkCurrentDebCred_CheckedChanged);
            // 
            // chkBudget
            // 
            this.chkBudget.Location = new System.Drawing.Point(20, 30);
            this.chkBudget.Name = "chkBudget";
            this.chkBudget.Size = new System.Drawing.Size(186, 27);
            this.chkBudget.TabIndex = 10;
            this.chkBudget.Text = "Budget / Beg Balance";
            this.chkBudget.CheckedChanged += new System.EventHandler(this.chkBudget_CheckedChanged);
            // 
            // chkCurrent
            // 
            this.chkCurrent.Location = new System.Drawing.Point(20, 60);
            this.chkCurrent.Name = "chkCurrent";
            this.chkCurrent.Size = new System.Drawing.Size(112, 27);
            this.chkCurrent.TabIndex = 9;
            this.chkCurrent.Text = "Current Net";
            this.chkCurrent.CheckedChanged += new System.EventHandler(this.chkCurrent_CheckedChanged);
            // 
            // chkBalance
            // 
            this.chkBalance.Location = new System.Drawing.Point(226, 60);
            this.chkBalance.Name = "chkBalance";
            this.chkBalance.Size = new System.Drawing.Size(86, 27);
            this.chkBalance.TabIndex = 7;
            this.chkBalance.Text = "Balance";
            this.chkBalance.CheckedChanged += new System.EventHandler(this.chkBalance_CheckedChanged);
            // 
            // chkPercent
            // 
            this.chkPercent.Location = new System.Drawing.Point(226, 90);
            this.chkPercent.Name = "chkPercent";
            this.chkPercent.Size = new System.Drawing.Size(84, 27);
            this.chkPercent.TabIndex = 6;
            this.chkPercent.Text = "Percent";
            this.chkPercent.CheckedChanged += new System.EventHandler(this.chkPercent_CheckedChanged);
            // 
            // fraYTDInclude
            // 
            this.fraYTDInclude.Controls.Add(this.chkPending);
            this.fraYTDInclude.Controls.Add(this.chkEncumbrances);
            this.fraYTDInclude.FormatCaption = false;
            this.fraYTDInclude.Location = new System.Drawing.Point(436, 75);
            this.fraYTDInclude.Name = "fraYTDInclude";
            this.fraYTDInclude.Size = new System.Drawing.Size(185, 100);
            this.fraYTDInclude.TabIndex = 2;
            this.fraYTDInclude.Text = "Include In YTD";
            // 
            // chkPending
            // 
            this.chkPending.Location = new System.Drawing.Point(20, 30);
            this.chkPending.Name = "chkPending";
            this.chkPending.Size = new System.Drawing.Size(157, 27);
            this.chkPending.TabIndex = 4;
            this.chkPending.Text = "Pending Amounts";
            // 
            // chkEncumbrances
            // 
            this.chkEncumbrances.Checked = true;
            this.chkEncumbrances.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkEncumbrances.Location = new System.Drawing.Point(20, 63);
            this.chkEncumbrances.Name = "chkEncumbrances";
            this.chkEncumbrances.Size = new System.Drawing.Size(137, 27);
            this.chkEncumbrances.TabIndex = 3;
            this.chkEncumbrances.Text = "Encumbrances";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(177, 30);
            this.txtDescription.MaxLength = 35;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(444, 40);
            this.txtDescription.TabIndex = 21;
            // 
            // vsCustomFormat
            // 
            this.vsCustomFormat.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsCustomFormat.Cols = 7;
            this.vsCustomFormat.ExtendLastCol = true;
            this.vsCustomFormat.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsCustomFormat.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsCustomFormat.FixedCols = 0;
            this.vsCustomFormat.Location = new System.Drawing.Point(30, 385);
            this.vsCustomFormat.Name = "vsCustomFormat";
            this.vsCustomFormat.RowHeadersVisible = false;
            this.vsCustomFormat.Rows = 50;
            this.vsCustomFormat.Size = new System.Drawing.Size(1018, 85);
            this.vsCustomFormat.StandardTab = false;
            this.vsCustomFormat.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsCustomFormat.TabIndex = 1;
            this.vsCustomFormat.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsCustomFormat_AfterEdit);
            this.vsCustomFormat.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCustomFormat_ChangeEdit);
            this.vsCustomFormat.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsCustomFormat_BeforeEdit);
            this.vsCustomFormat.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCustomFormat_ValidateEdit);
            this.vsCustomFormat.CurrentCellChanged += new System.EventHandler(this.vsCustomFormat_RowColChange);
            // 
            // lblExpense
            // 
            this.lblExpense.Location = new System.Drawing.Point(30, 305);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(373, 18);
            this.lblExpense.TabIndex = 24;
            this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(143, 18);
            this.Label1.TabIndex = 23;
            this.Label1.Text = "REPORT TITLE";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdFileNew
            // 
            this.cmdFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileNew.Location = new System.Drawing.Point(739, 29);
            this.cmdFileNew.Name = "cmdFileNew";
            this.cmdFileNew.Size = new System.Drawing.Size(45, 24);
            this.cmdFileNew.TabIndex = 3;
            this.cmdFileNew.Text = "New";
            this.cmdFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // cmdFileDelete
            // 
            this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDelete.Location = new System.Drawing.Point(788, 29);
            this.cmdFileDelete.Name = "cmdFileDelete";
            this.cmdFileDelete.Size = new System.Drawing.Size(56, 24);
            this.cmdFileDelete.TabIndex = 2;
            this.cmdFileDelete.Text = "Delete";
            this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // cmdFileAccounts
            // 
            this.cmdFileAccounts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileAccounts.Enabled = false;
            this.cmdFileAccounts.Location = new System.Drawing.Point(948, 29);
            this.cmdFileAccounts.Name = "cmdFileAccounts";
            this.cmdFileAccounts.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdFileAccounts.Size = new System.Drawing.Size(146, 24);
            this.cmdFileAccounts.TabIndex = 1;
            this.cmdFileAccounts.Text = "Show Valid Accounts";
            this.cmdFileAccounts.Click += new System.EventHandler(this.mnuFileAccounts_Click);
            // 
            // cmdFilePreview
            // 
            this.cmdFilePreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePreview.Location = new System.Drawing.Point(848, 29);
            this.cmdFilePreview.Name = "cmdFilePreview";
            this.cmdFilePreview.Size = new System.Drawing.Size(97, 24);
            this.cmdFilePreview.TabIndex = 4;
            this.cmdFilePreview.Text = "Print Preview";
            this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // cmdFileSave
            // 
            this.cmdFileSave.AppearanceKey = "acceptButton";
            this.cmdFileSave.Location = new System.Drawing.Point(274, 30);
            this.cmdFileSave.Name = "cmdFileSave";
            this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSave.Size = new System.Drawing.Size(80, 48);
            this.cmdFileSave.Text = "Save";
            this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmCreateCustomReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCreateCustomReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Report Writer";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCreateCustomReport_Load);
            this.Activated += new System.EventHandler(this.frmCreateCustomReport_Activated);
            this.Resize += new System.EventHandler(this.frmCreateCustomReport_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreateCustomReport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
            this.fraMonths.ResumeLayout(false);
            this.fraMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInclude)).EndInit();
            this.fraInclude.ResumeLayout(false);
            this.fraInclude.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebCred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentDebCred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTDInclude)).EndInit();
            this.fraYTDInclude.ResumeLayout(false);
            this.fraYTDInclude.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPending)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEncumbrances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCustomFormat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCLabel lblNote;
    }
}