﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cEOYAdjustment
	{
		//=========================================================
		int lngRecordID;
		int lngYear;
		double dblAmount;
		string strAccount;
		bool boolUpdated;
		bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public int Year
		{
			set
			{
				lngYear = value;
				IsUpdated = true;
			}
			get
			{
				int Year = 0;
				Year = lngYear;
				return Year;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
