﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptArticleList.
	/// </summary>
	partial class rptArticleList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptArticleList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSpent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldArticleTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ArticleBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldArticleBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldArticleTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldYTDTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalanceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSpentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ArticleBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalanceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldTitle,
				this.fldBudget,
				this.fldYTD,
				this.fldBalance,
				this.fldSpent
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.25F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.625F;
			// 
			// fldTitle
			// 
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 1.9375F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldTitle.Text = "Field2";
			this.fldTitle.Top = 0F;
			this.fldTitle.Width = 2.5625F;
			// 
			// fldBudget
			// 
			this.fldBudget.Height = 0.1875F;
			this.fldBudget.Left = 4.5625F;
			this.fldBudget.Name = "fldBudget";
			this.fldBudget.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldBudget.Text = "Field1";
			this.fldBudget.Top = 0F;
			this.fldBudget.Width = 0.8125F;
			// 
			// fldYTD
			// 
			this.fldYTD.Height = 0.1875F;
			this.fldYTD.Left = 5.40625F;
			this.fldYTD.Name = "fldYTD";
			this.fldYTD.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldYTD.Text = "Field1";
			this.fldYTD.Top = 0F;
			this.fldYTD.Width = 0.8125F;
			// 
			// fldBalance
			// 
			this.fldBalance.Height = 0.1875F;
			this.fldBalance.Left = 6.28125F;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldBalance.Text = "Field1";
			this.fldBalance.Top = 0F;
			this.fldBalance.Width = 0.8125F;
			// 
			// fldSpent
			// 
			this.fldSpent.Height = 0.1875F;
			this.fldSpent.Left = 7.15625F;
			this.fldSpent.Name = "fldSpent";
			this.fldSpent.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSpent.Text = "Field1";
			this.fldSpent.Top = 0F;
			this.fldSpent.Width = 0.8125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblAccount,
				this.Line1,
				this.Label8,
				this.Label10,
				this.Label11,
				this.Label12
			});
			this.PageHeader.Height = 0.6979167F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.645833F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Article List";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.25F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 3.65625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 7.90625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.90625F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.53125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label8.Text = "Budget";
			this.Label8.Top = 0.5F;
			this.Label8.Width = 0.8125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.40625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "YTD Net";
			this.Label10.Top = 0.5F;
			this.Label10.Width = 0.8125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.25F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label11.Text = "Balance";
			this.Label11.Top = 0.5F;
			this.Label11.Width = 0.8125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 7.125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "% Spent";
			this.Label12.Top = 0.5F;
			this.Label12.Width = 0.8125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldArticleTitle,
				this.ArticleBinder,
				this.fldArticleBudget
			});
			this.GroupHeader1.DataField = "ArticleBinder";
			this.GroupHeader1.Height = 0.21875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldArticleTitle
			// 
			this.fldArticleTitle.Height = 0.1875F;
			this.fldArticleTitle.Left = 0F;
			this.fldArticleTitle.Name = "fldArticleTitle";
			this.fldArticleTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldArticleTitle.Text = "Field1";
			this.fldArticleTitle.Top = 0.03125F;
			this.fldArticleTitle.Width = 3.03125F;
			// 
			// ArticleBinder
			// 
			this.ArticleBinder.DataField = "ArticleBinder";
			this.ArticleBinder.Height = 0.125F;
			this.ArticleBinder.Left = 5.40625F;
			this.ArticleBinder.Name = "ArticleBinder";
			this.ArticleBinder.Text = "Field1";
			this.ArticleBinder.Top = 0F;
			this.ArticleBinder.Visible = false;
			this.ArticleBinder.Width = 0.6875F;
			// 
			// fldArticleBudget
			// 
			this.fldArticleBudget.Height = 0.1875F;
			this.fldArticleBudget.Left = 3.15625F;
			this.fldArticleBudget.Name = "fldArticleBudget";
			this.fldArticleBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldArticleBudget.Text = "Field1";
			this.fldArticleBudget.Top = 0.03125F;
			this.fldArticleBudget.Width = 1.65625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.fldArticleTotal,
				this.Line2,
				this.fldYTDTotal,
				this.fldBalanceTotal,
				this.fldSpentTotal
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.5625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label9.Text = "Article Total:";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 0.9375F;
			// 
			// fldArticleTotal
			// 
			this.fldArticleTotal.Height = 0.1875F;
			this.fldArticleTotal.Left = 4.5625F;
			this.fldArticleTotal.Name = "fldArticleTotal";
			this.fldArticleTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldArticleTotal.Text = "Field1";
			this.fldArticleTotal.Top = 0.03125F;
			this.fldArticleTotal.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.53125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 1.84375F;
			this.Line2.X1 = 5.375F;
			this.Line2.X2 = 3.53125F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldYTDTotal
			// 
			this.fldYTDTotal.Height = 0.1875F;
			this.fldYTDTotal.Left = 5.40625F;
			this.fldYTDTotal.Name = "fldYTDTotal";
			this.fldYTDTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldYTDTotal.Text = "Field1";
			this.fldYTDTotal.Top = 0.03125F;
			this.fldYTDTotal.Width = 0.8125F;
			// 
			// fldBalanceTotal
			// 
			this.fldBalanceTotal.Height = 0.1875F;
			this.fldBalanceTotal.Left = 6.28125F;
			this.fldBalanceTotal.Name = "fldBalanceTotal";
			this.fldBalanceTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldBalanceTotal.Text = "Field1";
			this.fldBalanceTotal.Top = 0.03125F;
			this.fldBalanceTotal.Width = 0.8125F;
			// 
			// fldSpentTotal
			// 
			this.fldSpentTotal.Height = 0.1875F;
			this.fldSpentTotal.Left = 7.15625F;
			this.fldSpentTotal.Name = "fldSpentTotal";
			this.fldSpentTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldSpentTotal.Text = "Field1";
			this.fldSpentTotal.Top = 0.03125F;
			this.fldSpentTotal.Width = 0.8125F;
			// 
			// rptArticleList
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptArticleList_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ArticleBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldArticleTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalanceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpent;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldArticleTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox ArticleBinder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldArticleBudget;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldArticleTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalanceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpentTotal;
	}
}
