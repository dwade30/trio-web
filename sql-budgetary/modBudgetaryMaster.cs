﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.DataBaseLayer;
using Wisej.Core;
using System.Text;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Enums;
using TWSharedLibrary;

namespace TWBD0000
{
	public class modBudgetaryMaster
	{
		//=========================================================
		// Public global_NetHoster As cNetHoster
		
		public const string DEFAULTDATABASE = "TWBD0000.vb1";

		//[DllImport("user32")]
		//public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

		//[DllImport("advapi32.dll", EntryPoint = "GetUserNameA", SetLastError = true)]
		//public static extern bool GetUserName(System.Text.StringBuilder sb, ref Int32 length);

		//public static int GetUserNameWrp(ref string lpBuffer, ref int nSize)
		//{
		//	StringBuilder sb = new StringBuilder(nSize);
		//	bool Ok = GetUserName(sb, ref nSize);
		//	lpBuffer = sb.ToString();
		//	return nSize;
		//}

		public struct CloseoutInfo
		{
			public string strAccount;
			public Decimal curAmount;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CloseoutInfo(int unusedParam)
			{
				this.strAccount = string.Empty;
				this.curAmount = 0;
			}
		};

		public static void Main()
		{
			// kk04132015 troges-39  Change to single config file
			if (!modGlobalFunctions.LoadSQLConfig("TWBD0000.VB1"))
			{
				MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			string temp;
			clsDRWrapper rs = new clsDRWrapper();
			int lngLiability;
			int lngFundBalance;
			clsDRWrapper rsLedgerRange = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			string strDataPath = "";

			try
			{
				// On Error GoTo ErrorHandler
				modGlobalFunctions.LoadTRIOColors();
				Statics.gboolClosingProgram = false;
				modValidateAccount.SetBDFormats();
				modNewAccountBox.GetFormats();
				modReplaceWorkFiles.GetGlobalVariables();
                var sset = new cSystemSettings();
                modBudgetaryMaster.Statics.gGlobalVariable = sset.GetGlobalVariables();
				modGlobalFunctions.UpdateUsedModules();
				// MsgBox "2"
				if (!modGlobalConstants.Statics.gboolBD)
				{
					MessageBox.Show("Budgetary has expired.  Please call TRIO", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				if (Strings.Right(Application.StartupPath, 1) == "\\")
				{
					App.HelpFile = Application.StartupPath + "TWBD0000.hlp";
				}
				else
				{
					App.HelpFile = Application.StartupPath + "\\TWBD0000.hlp";
				}
				rsUpdate.Execute("DELETE FROM VendorMaster WHERE VendorNumber = 0", "Budgetary");
				CheckVersion();
				NonUpdateTag:
				;
				
				modGlobalFunctions.GetGlobalRegistrySetting();
				
				
				// MsgBox "8"
				modGlobalConstants.Statics.clsSecurityClass.Init("BD");
				// MsgBox "9"
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					modBudgetaryAccounting.Statics.strZeroDiv = "0";
				}
				else
				{
					modBudgetaryAccounting.Statics.strZeroDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				}
				CheckFixedAssets();
				Statics.FirstMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				Statics.CurrentAPEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRAPJRNL"))));
				Statics.CurrentEncEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRENCJRNL"))));
				Statics.CurrentCDEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCDJRNL"))));
				// MsgBox "10"
				temp = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("UseProjectNumber"));
				if (temp == "Y")
				{
					Statics.ProjectFlag = true;
				}
				else
				{
					Statics.ProjectFlag = false;
				}
				if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutomaticInterest")))
				{
					modBudgetaryAccounting.Statics.gboolAutoInterest = true;
				}
				else
				{
					modBudgetaryAccounting.Statics.gboolAutoInterest = false;
				}
				if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("PUCChartOfAccounts")))
				{
					modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts = true;
				}
				else
				{
					modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts = false;
				}
				// MsgBox "11"
				modBudgetaryAccounting.GetControlAccounts();
				ValidateControlAccounts();
				// MsgBox "13"
				SetInterestAccounts();
				// MsgBox "14"
				// Set global_NetHoster = New cNetHoster
				MDIParent.InstancePtr.Init();				
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description);
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private static bool CheckVersion()
		{
			bool CheckVersion = false;
			cBudgetaryDB bDB = new cBudgetaryDB();
			if (!bDB.CheckVersion())
			{
				cVersionInfo tVer;
				tVer = bDB.GetVersion();
				MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
			}
			cSystemSettings sysSet = new cSystemSettings();
			sysSet.CheckVersion();
			cCentralDocumentDB cdDB = new cCentralDocumentDB();
			cdDB.CheckVersion();

            var util = new cArchiveUtility();
            var archives = util.GetArchiveGroupNames("Archive", 0);
            var wrapper = new clsDRWrapper();
            var liveGroup = wrapper.DefaultGroup;
            try
            {
                foreach (string archive in archives)
                {
                    wrapper.DefaultGroup = archive;
                    var archiveDB = new cBudgetaryDB();
                    archiveDB.CheckVersion();
                    var archiveCentralData = new cCentralDataDB();
                    archiveCentralData.CheckVersion();
                    var archiveCDocDB = new cCentralDocumentDB();
                    archiveCDocDB.CheckVersion();
                }
			}
            catch
            {
                return false;
            }
            finally
            {
                wrapper.DefaultGroup = liveGroup;
			}

            CheckVersion = true;
			return CheckVersion;
		}

		

		public static bool UpdateBankVariable(string strVariableName, int NewValue)
		{
			bool UpdateBankVariable = false;
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				UpdateBankVariable = false;
				rsVariables.Reset();
				rsVariables.OpenRecordset("SELECT * FROM BankIndex", "CentralData", FCConvert.ToInt32(FCDatabase.RecordsetTypeSettings.dbOpenDynaset), FCConvert.ToBoolean(0), FCConvert.ToInt32(FCRecordset.LockTypeEnum.dbOptimistic));
				if (rsVariables.EndOfFile())
				{
					rsVariables.OmitNullsOnInsert = true;
					rsVariables.AddNew();
				}
				else
				{
					rsVariables.Edit();
				}
				rsVariables.Set_Fields(strVariableName, NewValue);
				rsVariables.Update();
				UpdateBankVariable = true;
				return UpdateBankVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Errors Encountered", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateBankVariable;
		}

		public static bool LockWarrant()
		{
			bool LockWarrant = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
				{
					rsTemp.Edit();
					rsTemp.Set_Fields("MasterLock", true);
					rsTemp.Set_Fields("OpID", modBudgetaryAccounting.Statics.User);
					if (rsTemp.Update(false))
					{
						LockWarrant = true;
					}
					else
					{
						LockWarrant = false;
					}
				}
				else
				{
					LockWarrant = false;
					modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
					rsTemp.Reset();
					return LockWarrant;
				}
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("MasterLock", true);
				rsTemp.Set_Fields("OpID", modBudgetaryAccounting.Statics.User);
				if (rsTemp.Update(false))
				{
					LockWarrant = true;
				}
				else
				{
					LockWarrant = false;
				}
			}
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != modBudgetaryAccounting.Statics.User)
			{
				LockWarrant = false;
				modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
			}
			return LockWarrant;
		}

		public static void UnlockWarrant()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM WarrantLock");
			rsTemp.Edit();
			rsTemp.Set_Fields("MasterLock", false);
			rsTemp.Set_Fields("OpID", "");
			rsTemp.Update(true);
		}

		public static bool LockPO()
		{
			bool LockPO = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM POLock");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (rsTemp.Get_Fields_Boolean("MasterLock") == false)
				{
					rsTemp.Edit();
					rsTemp.Set_Fields("MasterLock", true);
					rsTemp.Set_Fields("OpID", modBudgetaryAccounting.Statics.User);
					if (rsTemp.Update(false))
					{
						LockPO = true;
					}
					else
					{
						LockPO = false;
					}
				}
				else
				{
					LockPO = false;
					modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
					return LockPO;
				}
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("MasterLock", true);
				rsTemp.Set_Fields("OpID", modBudgetaryAccounting.Statics.User);
				if (rsTemp.Update(false))
				{
					LockPO = true;
				}
				else
				{
					LockPO = false;
				}
			}
			rsTemp.OpenRecordset("SELECT * FROM POLock");
			if (FCConvert.ToString(rsTemp.Get_Fields_String("OpID")) != modBudgetaryAccounting.Statics.User)
			{
				LockPO = false;
				modBudgetaryAccounting.Statics.strLockedBy = FCConvert.ToString(rsTemp.Get_Fields_String("OpID"));
			}
			return LockPO;
		}

		public static void UnlockPO()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM POLock");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				rsTemp.Edit();
			}
			else
			{
				rsTemp.AddNew();
			}
			rsTemp.Set_Fields("MasterLock", false);
			rsTemp.Set_Fields("OpID", "");
			rsTemp.Update(true);
		}

		public static int GetNextPO()
		{
			int GetNextPO = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM POLock");
			if (Conversion.Val(rsTemp.Get_Fields_Int32("NextPO")) == 0)
			{
				GetNextPO = 1;
			}
			else
			{
				GetNextPO = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("NextPO"));
			}
			return GetNextPO;
		}

		public static void IncrementPO()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM POLock");
			rsTemp.Edit();
			if (Conversion.Val(rsTemp.Get_Fields_Int32("NextPO")) == 0)
			{
				rsTemp.Set_Fields("NextPO", 2);
			}
			else
			{
				rsTemp.Set_Fields("NextPO", FCConvert.ToInt16(rsTemp.Get_Fields_Int32("NextPO")) + 1);
			}
			rsTemp.Update(true);
		}

		public static void WriteAuditRecord_8(string strOverride, string strScreen)
		{
			WriteAuditRecord(ref strOverride, ref strScreen);
		}

		public static void WriteAuditRecord(ref string strOverride, ref string strScreen)
		{
			modGlobalFunctions.AddCYAEntry_26("BD", strOverride, strScreen);
		}

		public static bool CheckReportDateRange_8(short intBegDate, short intEndDate, short intStartYear = -1, short intEndYear = -1)
		{
			return CheckReportDateRange(intBegDate, intEndDate, intStartYear, intEndYear);
		}

		public static bool CheckReportDateRange_80(short intBegDate, short intEndDate, short intStartYear = -1, short intEndYear = -1)
		{
			return CheckReportDateRange(intBegDate, intEndDate, intStartYear, intEndYear);
		}

		public static bool CheckReportDateRange(short intBegDate, short intEndDate, short intStartYear = -1, short intEndYear = -1)
		{
			bool CheckReportDateRange = false;
			CheckReportDateRange = true;
			if (intStartYear == -1 || intEndYear == -1)
			{
				if (Statics.FirstMonth == 1)
				{
					if (intEndDate < intBegDate)
					{
						CheckReportDateRange = false;
					}
				}
				else if (intBegDate < Statics.FirstMonth)
				{
					if (intEndDate > Statics.FirstMonth)
					{
						CheckReportDateRange = false;
					}
					else if (intBegDate > intEndDate)
					{
						CheckReportDateRange = false;
					}
				}
				else if (intBegDate >= Statics.FirstMonth)
				{
					if (intEndDate > Statics.FirstMonth && intEndDate < intBegDate)
					{
						CheckReportDateRange = false;
					}
				}
			}
			else
			{
				DateTime FiscalStartDate;
				DateTime StartDate;
				DateTime EndDate;
				FiscalStartDate = DateAndTime.DateValue(FCConvert.ToString(Statics.FirstMonth) + "/1/" + FCConvert.ToString(intStartYear));
				StartDate = DateAndTime.DateValue(FCConvert.ToString(intBegDate) + "/1/" + FCConvert.ToString(intStartYear));
				EndDate = DateAndTime.DateValue(FCConvert.ToString(intEndDate) + "/1/" + FCConvert.ToString(intEndYear));
				if (StartDate.ToOADate() < FiscalStartDate.ToOADate() && EndDate.ToOADate() >= FiscalStartDate.ToOADate())
				{
					CheckReportDateRange = false;
				}
			}
			return CheckReportDateRange;
		}

		public static string CreateAccount_242(string strType, string strFirst, string strSecond, string strThird, string strFourth, string strFifth = "", bool blnSearch = true)
		{
			return CreateAccount(ref strType, ref strFirst, ref strSecond, ref strThird, ref strFourth, strFifth, blnSearch);
		}

		public static string CreateAccount_2186(string strType, string strFirst, string strSecond, string strThird, string strFourth, string strFifth = "", bool blnSearch = true)
		{
			return CreateAccount(ref strType, ref strFirst, ref strSecond, ref strThird, ref strFourth, strFifth, blnSearch);
		}

		public static string CreateAccount(ref string strType, ref string strFirst, ref string strSecond, ref string strThird, ref string strFourth, string strFifth = "", bool blnSearch = true)
		{
			string CreateAccount = "";
			CreateAccount = strType + " ";
			if (Strings.Trim(strFirst) == "")
			{
				CreateAccount = CreateAccount + "*";
			}
			else if (Strings.Trim(strSecond) == "")
			{
				CreateAccount = CreateAccount + Strings.Trim(strFirst) + "*";
			}
			else if (Strings.Trim(strThird) == "")
			{
				CreateAccount = CreateAccount + Strings.Trim(strFirst) + "-" + Strings.Trim(strSecond) + "*";
			}
			else if (Strings.Trim(strFourth) == "")
			{
				CreateAccount = CreateAccount + Strings.Trim(strFirst) + "-" + Strings.Trim(strSecond) + "-" + Strings.Trim(strThird) + "*";
			}
			else if (Strings.Trim(strFifth) == "")
			{
				CreateAccount = CreateAccount + Strings.Trim(strFirst) + "-" + Strings.Trim(strSecond) + "-" + Strings.Trim(strThird) + "-" + Strings.Trim(strFourth) + "*";
			}
			else
			{
				CreateAccount = CreateAccount + Strings.Trim(strFirst) + "-" + Strings.Trim(strSecond) + "-" + Strings.Trim(strThird) + "-" + Strings.Trim(strFourth) + "-" + Strings.Trim(strFifth) + "*";
			}
			if (blnSearch == false)
			{
				CreateAccount = Strings.Left(CreateAccount, CreateAccount.Length - 1);
			}
			return CreateAccount;
		}

		public static void InitializeBudgetTable()
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsBudget = new clsDRWrapper();
			string strTempAccount = "";
			rsBudget.OpenRecordset("SELECT * FROM Budget WHERE ID = 0");
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("ValidAccountBudgetProcess")))
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' or AccountType = 'R' or AccountType = 'P' or AccountType = 'V') AND Valid = 1");
			}
			else
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' or AccountType = 'R' or AccountType = 'P' or AccountType = 'V')");
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					rsBudget.OmitNullsOnInsert = true;
					rsBudget.AddNew();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBudget.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
					rsBudget.Update(true);
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
		}

		public static bool JobComplete_2(string x)
		{
			return JobComplete(ref x);
		}

		public static bool JobComplete(ref string x)
		{
			bool JobComplete = false;
			foreach (Form ff in FCGlobal.Statics.Forms)
			{
				if (ff.Name == x)
				{
					JobComplete = false;
					return JobComplete;
				}
			}
			JobComplete = true;
			return JobComplete;
		}

		public static bool IsMonth_2(string x)
		{
			return IsMonth(ref x);
		}

		public static bool IsMonth(ref string x)
		{
			bool IsMonth = false;
			if ((Strings.UCase(x) == "JANUARY") || (Strings.UCase(x) == "FEBRUARY") || (Strings.UCase(x) == "MARCH") || (Strings.UCase(x) == "APRIL") || (Strings.UCase(x) == "MAY") || (Strings.UCase(x) == "JUNE") || (Strings.UCase(x) == "JULY") || (Strings.UCase(x) == "AUGUST") || (Strings.UCase(x) == "SEPTEMBER") || (Strings.UCase(x) == "OCTOBER") || (Strings.UCase(x) == "NOVEMBER") || (Strings.UCase(x) == "DECEMBER"))
			{
				IsMonth = true;
			}
			else
			{
				IsMonth = false;
			}
			return IsMonth;
		}

		public static void CheckFixedAssets()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (modGlobalConstants.Statics.gstrArchiveYear == "")
			{
				rsData.OpenRecordset("Select * from Modules", "SystemSettings");
				if (!rsData.EndOfFile())
				{
					Statics.blnFixedAssetsExists = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FA"));
				}
				else
				{
					Statics.blnFixedAssetsExists = false;
				}
				if (Statics.blnFixedAssetsExists)
				{
					rsData.OpenRecordset("Select * from tblSettings", "TWFA0000.vb1");
					if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
					{
						Statics.blnFixedAssetVendorsFromBud = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Vendors"));
					}
					else
					{
						Statics.blnFixedAssetVendorsFromBud = false;
					}
				}
				else
				{
					Statics.blnFixedAssetVendorsFromBud = false;
				}
			}
			else
			{
				Statics.blnFixedAssetsExists = false;
				Statics.blnFixedAssetVendorsFromBud = false;
			}
		}

		public static bool CheckIfControlAccount(string strAccount)
		{
			bool CheckIfControlAccount = false;
			string strAcct;
			string strSuffix;
			strAcct = GetAccountNumber(ref strAccount);
			strSuffix = modBudgetaryAccounting.GetSuffix(strAccount);
			if (strAcct != "")
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					if (strAcct == modBudgetaryAccounting.Statics.strRevCtrAccount || strAcct == modBudgetaryAccounting.Statics.strExpCtrAccount || strAcct == modBudgetaryAccounting.Statics.strEncOffAccount || strAcct == modBudgetaryAccounting.Statics.strUnliqEncCtrAccount || strAcct == modBudgetaryAccounting.Statics.strFundBalCtrAccount || strAccount == modBudgetaryAccounting.Statics.strDueToCtrAccount || strAccount == modBudgetaryAccounting.Statics.strDueFromCtrAccount)
					{
						CheckIfControlAccount = true;
					}
					else
					{
						CheckIfControlAccount = false;
					}
				}
				else
				{
					if ((strAcct == modBudgetaryAccounting.Statics.strRevCtrAccount && strSuffix == "00") || (strAcct == modBudgetaryAccounting.Statics.strExpCtrAccount && strSuffix == "00") || (strAcct == modBudgetaryAccounting.Statics.strEncOffAccount && strSuffix == "00") || (strAcct == modBudgetaryAccounting.Statics.strUnliqEncCtrAccount && strSuffix == "00") || (strAcct == modBudgetaryAccounting.Statics.strFundBalCtrAccount && strSuffix == "00") || strAccount == modBudgetaryAccounting.Statics.strDueToCtrAccount || strAccount == modBudgetaryAccounting.Statics.strDueFromCtrAccount)
					{
						CheckIfControlAccount = true;
					}
					else
					{
						CheckIfControlAccount = false;
					}
				}
			}
			else
			{
				CheckIfControlAccount = false;
			}
			return CheckIfControlAccount;
		}

		public static string GetAccountNumber_2(string Acct)
		{
			return GetAccountNumber(ref Acct);
		}

		public static string GetAccountNumber(ref string Acct)
		{
			string GetAccountNumber = "";
			if (Strings.Left(Acct, 1) == "G")
			{
				GetAccountNumber = Strings.Mid(Acct, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			}
			else
			{
				GetAccountNumber = "";
			}
			return GetAccountNumber;
		}

		public static void AddAccountToBudgetTable(ref string tempAccount)
		{
			clsDRWrapper rsBudget = new clsDRWrapper();
			rsBudget.OmitNullsOnInsert = true;
			rsBudget.OpenRecordset("SELECT * FROM Budget WHERE ID = 0");
			rsBudget.AddNew();
			rsBudget.Set_Fields("Account", tempAccount);
			rsBudget.Update();
		}

		public static string GetAccountFund(string strAcct)
		{
			string GetAccountFund = "";
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (Strings.Left(strAcct, 1) == "E" || Strings.Left(strAcct, 1) == "R")
			{
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Mid(strAcct, 3, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "'");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Mid(strAcct, 3, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND Division = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "0") + "'");
				}
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					GetAccountFund = FCConvert.ToString(rsInfo.Get_Fields("Fund"));
				}
				else
				{
					GetAccountFund = "-99";
				}
			}
			else if (Strings.Left(strAcct, 1) == "G")
			{
				GetAccountFund = Strings.Mid(strAcct, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
			}
			else
			{
				GetAccountFund = "-99";
			}
			return GetAccountFund;
		}

		public static void FixCustomCheckFields()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int intFormat = 0;
			rsInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = 'Name' ORDER BY FormatID, ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() > 1)
				{
					// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
					intFormat = FCConvert.ToInt32(rsInfo.Get_Fields("FormatID"));
					rsInfo.MoveNext();
					do
					{
						// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
						if (intFormat == FCConvert.ToInt32(rsInfo.Get_Fields("FormatID")))
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
							intFormat = FCConvert.ToInt32(rsInfo.Get_Fields("FormatID"));
							rsInfo.MoveNext();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = 'Name and Address' ORDER BY FormatID, ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() > 1)
				{
					// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
					intFormat = FCConvert.ToInt32(rsInfo.Get_Fields("FormatID"));
					rsInfo.MoveNext();
					do
					{
						// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
						if (intFormat == FCConvert.ToInt32(rsInfo.Get_Fields("FormatID")))
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
							intFormat = FCConvert.ToInt32(rsInfo.Get_Fields("FormatID"));
							rsInfo.MoveNext();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
			}
		}
		// vbPorter upgrade warning: lngJournal As int	OnWrite(int, double)
		public static Decimal RemoveExistingCreditMemos_2(int lngJournal)
		{
			return RemoveExistingCreditMemos(ref lngJournal);
		}

		public static Decimal RemoveExistingCreditMemos(ref int lngJournal)
		{
			Decimal RemoveExistingCreditMemos = 0;
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			clsDRWrapper rsCreditDetail = new clsDRWrapper();
			// vbPorter upgrade warning: curDetailAmount As Decimal	OnWrite(short, Decimal)
			Decimal curDetailAmount;
			rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE CreditMemoRecord <> 0 AND PrintedIndividual <> 1 AND JournalNumber = " + FCConvert.ToString(lngJournal));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					rsDetailInfo.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + rsInfo.Get_Fields_Int32("ID"), "Budgetary");
					//FC:FINAL:MSH - can't implicitly convert to decimal (same with issue #742)
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					RemoveExistingCreditMemos += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
					rsCreditMemo.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + rsInfo.Get_Fields_Int32("CreditMemoRecord"));
					if (rsCreditMemo.EndOfFile() != true && rsCreditMemo.BeginningOfFile() != true)
					{
						rsCreditMemo.Edit();
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsCreditMemo.Set_Fields("Liquidated", rsCreditMemo.Get_Fields_Decimal("Liquidated") + rsInfo.Get_Fields("Amount"));
						rsCreditMemo.Update();
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curDetailAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
					rsCreditDetail.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE Liquidated <> 0 AND CreditMemoID = " + rsCreditMemo.Get_Fields_Int32("ID"));
					if (rsCreditDetail.EndOfFile() != true && rsCreditDetail.BeginningOfFile() != true)
					{
						do
						{
							if (curDetailAmount * -1 <= rsCreditDetail.Get_Fields_Decimal("Liquidated"))
							{
								rsCreditDetail.Edit();
								rsCreditDetail.Set_Fields("Liquidated", rsCreditDetail.Get_Fields_Decimal("Liquidated") + curDetailAmount);
								curDetailAmount = 0;
								rsCreditDetail.Update();
								break;
							}
							else
							{
								rsCreditDetail.Edit();
								curDetailAmount += rsCreditDetail.Get_Fields_Decimal("Liquidated");
								rsCreditDetail.Set_Fields("Liquidated", 0);
								rsCreditDetail.Update();
							}
							rsCreditDetail.MoveNext();
						}
						while (rsCreditDetail.EndOfFile() != true);
					}
					rsInfo.Delete();
					rsInfo.Update();
				}
				while (rsInfo.EndOfFile() != true);
			}
			RemoveExistingCreditMemos *= -1;
			return RemoveExistingCreditMemos;
		}

		public static void ValidateControlAccounts()
		{
			string strMessage;
			strMessage = "";
            if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) == 0)
            {
                strMessage += "Town Encumbrance Control Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strExpCtrAccount) == 0)
            {
                strMessage += "Town Expense Control Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) == 0)
            {
                strMessage += "Town Revenue Control Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strUnliqEncCtrAccount) == 0)
            {
                strMessage += "Town Unliquidated Encumbrance Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strFundBalCtrAccount) == 0)
            {
                strMessage += "Town Fund Balance Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strCredMemRecAccount) == 0)
            {
                strMessage += "Town Credit Memo Receivable Account" + "\r\n";
            }
            if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
            {
                if (Conversion.Val(modBudgetaryAccounting.Statics.strDueToCtrAccount) == 0)
                {
                    strMessage += "Town Due To Account" + "\r\n";
                }
                if (Conversion.Val(modBudgetaryAccounting.Statics.strDueFromCtrAccount) == 0)
                {
                    strMessage += "Town Due From Account" + "\r\n";
                }
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strCRCashAccount) == 0)
            {
                strMessage += "Town Miscellaneous Cash Account" + "\r\n";
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strAPCashAccount) == 0)
            {
                strMessage += "Town Accounts Payable Cash Account" + "\r\n";
            }
			if (strMessage != "")
			{
				MessageBox.Show("The following control accounts need to be set up in your system." + "\r\n" + "\r\n" + strMessage, "Setup Control Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public static void UpdateEOYAPStandardAccounts()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'T'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() < 26)
				{
					rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row >= 13 AND Type = 'T' ORDER BY Row DESC");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						do
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("Row", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("Row")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
					rsInfo.AddNew();
					rsInfo.Set_Fields("Row", 13);
					rsInfo.Set_Fields("Account", "");
					rsInfo.Set_Fields("ShortDescription", "EOY Acct Pay");
					rsInfo.Set_Fields("LongDescription", "EOY Accounts Payable");
					rsInfo.Set_Fields("Code", "AP");
					rsInfo.Set_Fields("Type", "T");
					rsInfo.Update();
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'S'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() < 13)
				{
					rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row >= 13 AND Type = 'S' ORDER BY Row DESC");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						do
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("Row", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("Row")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
					rsInfo.AddNew();
					rsInfo.Set_Fields("Row", 12);
					rsInfo.Set_Fields("Account", "");
					rsInfo.Set_Fields("ShortDescription", "EOY Acct Pay");
					rsInfo.Set_Fields("LongDescription", "EOY Accounts Payable");
					rsInfo.Set_Fields("Code", "SAP");
					rsInfo.Set_Fields("Type", "S");
					rsInfo.Update();
				}
			}
		}

		public static void SetValidateBalanceFlag()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE ValidateBalance = 1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rsInfo.Execute("UPDATE AccountMaster SET ValidateBalance = 1", "Budgetary");
			}
		}

		public static void ChangeLaserAdjustmentsToRegistrySettings()
		{
			if (modRegistry.GetRegistryKey("CONVERTEDADJUSTMENTS") != "Y")
			{
				modRegistry.SaveRegistryKey("1099LASERADJUSTMENT", FCConvert.ToString(Conversion.Val(modBudgetaryAccounting.GetBDVariable("1099LaserAdjustment"))));
				modRegistry.SaveRegistryKey("1099LASERADJUSTMENTHORIZONTAL", FCConvert.ToString(Conversion.Val(modBudgetaryAccounting.GetBDVariable("1099LaserAdjustmentHorizontal"))));
				modRegistry.SaveRegistryKey("CHECKLASERADJUSTMENT", FCConvert.ToString(Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckLaserAdjustment"))));
				modRegistry.SaveRegistryKey("LABELLASERADJUSTMENT", FCConvert.ToString(Conversion.Val(modBudgetaryAccounting.GetBDVariable("LabelLaserAdjustment"))));
				modRegistry.SaveRegistryKey("REPORTADJUSTMENT", FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("REPORTADJUSTMENT"))));
				modRegistry.SaveRegistryKey("CONVERTEDADJUSTMENTS", "Y");
			}
		}
		// vbPorter upgrade warning: lngYear As int	OnWriteFCConvert.ToDouble(
		public static void AddEOYAdjustment_26(string strAcct, Decimal curAmount, int lngYear)
		{
			AddEOYAdjustment(ref strAcct, ref curAmount, ref lngYear);
		}

		public static void AddEOYAdjustment(ref string strAcct, ref Decimal curAmount, ref int lngYear)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM EOYAdjustments WHERE ID = 0");
			rsInfo.AddNew();
			rsInfo.Set_Fields("Account", strAcct);
			rsInfo.Set_Fields("Amount", curAmount);
			rsInfo.Set_Fields("Year", lngYear);
			rsInfo.Update();
		}

		public static void SetInterestAccounts()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (modBudgetaryAccounting.Statics.gboolAutoInterest)
				{
					// do nothing
				}
				else
				{
					modBudgetaryAccounting.UpdateBDVariable("AutomaticInterest", true);
					modBudgetaryAccounting.Statics.gboolAutoInterest = true;
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public static short RedFromRGB(int rgb)
		{
			short RedFromRGB = 0;
			// The ampersand after &HFF coerces the number as a
			// long, preventing Visual Basic from evaluating the
			// number as a negative value. The logical And is
			// used to return bit values.
			RedFromRGB = FCConvert.ToInt16(0xFF & rgb);
			return RedFromRGB;
		}

		public static short GreenFromRGB(int rgb)
		{
			short GreenFromRGB = 0;
			// The result of the And operation is divided by
			// 256, to return the value of the middle bytes.
			// Note the use of the Integer divisor.
			GreenFromRGB = FCConvert.ToInt16((0xFF00 & rgb) / 256);
			return GreenFromRGB;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public static short BlueFromRGB(int rgb)
		{
			short BlueFromRGB = 0;
			// This function works like the GreenFromRGB above,
			// except you don't need the ampersand. The
			// number is already a long. The result divided by
			// 65536 to obtain the highest bytes.
			BlueFromRGB = FCConvert.ToInt16((0xFF0000 & rgb) / 65536);
			return BlueFromRGB;
		}

		public static bool AllowAutoPostBD(modBudgetaryAccounting.AutoPostType aptType)
		{
			bool AllowAutoPostBD = false;
			if (!modSecurity.ValidPermissions_8(null, (int)BudgetarySecurityItems.Posting, false))
			{
				AllowAutoPostBD = false;
			}
			else
			{
				AllowAutoPostBD = modBudgetaryAccounting.AutoPostAllowed(aptType);
			}
			return AllowAutoPostBD;
		}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
            public CloseoutInfo[] sumCloseoutTotals = null;
			public int lngCloseoutTotalsArrayCounter;
			public string[] strWarr = new string[19 + 1];
			public string[] strDesc = new string[19 + 1];
			public string[] strRef = new string[19 + 1];
			public string[] curCredit = new string[19 + 1];
			public string[] curDisc = new string[19 + 1];
			public string[] curAmt = new string[19 + 1];
			
			
			public int lngCurrentAccount;
			public int lngCurrentPurchaseOrder;
			public int CurrentCredMemoEntry;
			public int CurrentAPEntry;
			public int CurrentEncEntry;
			public int CurrentCDEntry;
			public int CurrentCREntry;
			public int CurrentGJEntry;
            public int FirstMonth;
			public bool blnEdit;
			public bool blnAPEdit;
			public bool blnEncEdit;
			public bool blnPOEdit;
			public bool blnCDEdit;
			public bool blnCREdit;
			public bool blnGJEdit;
            public bool blnCredMemoEdit;
			public bool blnExpenseSummaryEdit;
			public bool blnRevenueSummaryEdit;
			public bool blnLedgerSummaryEdit;
			public bool blnProjectSummaryEdit;
			public bool blnExpenseSummaryReportEdit;
			public bool blnLedgerSummaryReportEdit;
			public bool blnRevenueSummaryReportEdit;
			public bool blnProjectSummaryReportEdit;
			public bool blnExpenseDetailEdit;
			public bool blnRevenueDetailEdit;
			public bool blnLedgerDetailEdit;
			public bool blnProjectDetailEdit;
			public bool blnExpenseDetailReportEdit;
			public bool blnLedgerDetailReportEdit;
			public bool blnRevenueDetailReportEdit;
			public bool blnProjectDetailReportEdit;
			public bool blnFromAP;
			public bool blnFromEnc;
			public bool blnFromPO;
			public bool blnFromCredMemo;
			public bool blnFromPreview;
			public bool ProjectFlag;
			public double[] EncumbranceDetail = null;
			// original encumbrance amounts
			public int[] EncumbranceDetailRecords = null;
			// Public clsSecurity                  As New clsTrioSecurity
			// Public strLockedBy                  As String
			public int lngReportCriteria;
			public int lngReportFormat;
			public string strBudgetFlag = "";
			public string strBudgetAccountType = "";
			public bool[] blnFields = new bool[44 + 1];
			public int intCurrentBank;
			public bool blnFixedAssetsExists;
			public bool blnFixedAssetVendorsFromBud;
			public string strReturnedValidAccount = string.Empty;
			// vbPorter upgrade warning: curOFFundTotals As Decimal	OnWrite(short, Decimal)
			public Decimal[] curOFFundTotals = new Decimal[999 + 1];
			public bool blnSigPasswordCheckedAlready;
			// vbPorter upgrade warning: intResponse As int --> As DialogResult
			public DialogResult intResponse;
			// vbPorter upgrade warning: lngSignatureID As int --> As DialogResult
			public int lngSignatureID;
			public bool gboolClosingProgram;
            public cGlobalVariable gGlobalVariable = new cGlobalVariable();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
