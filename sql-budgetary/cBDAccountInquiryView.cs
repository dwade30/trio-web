﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cBDAccountInquiryView
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDReportService repServ = new cBDReportService();
		private cBDReportService repServ_AutoInitialized;

		private cBDReportService repServ
		{
			get
			{
				if (repServ_AutoInitialized == null)
				{
					repServ_AutoInitialized = new cBDReportService();
				}
				return repServ_AutoInitialized;
			}
			set
			{
				repServ_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection listAccount = new cGenericCollection();
		private cGenericCollection listAccount_AutoInitialized;

		private cGenericCollection listAccount
		{
			get
			{
				if (listAccount_AutoInitialized == null)
				{
					listAccount_AutoInitialized = new cGenericCollection();
				}
				return listAccount_AutoInitialized;
			}
			set
			{
				listAccount_AutoInitialized = value;
			}
		}

		private string strAccountFilter = "";
		private int intAccountTypeToShow;
		private string strLedgerTemplate = "";
		private string strExpenseTemplate = "";
		private string strRevenueTemplate = "";
		private string[] strSegmentFilter = new string[5 + 1];
		private int[] SegmentLength = new int[5 + 1];
		private string strAccountTemplate = "";
		private bool boolExcludeNonActivity;
		private bool boolExcludeZeroBalance;
		private int intStartPeriod;
		private int intEndPeriod;
		private int intFirstPeriod;
		private int intLastPeriod;

		public delegate void AccountListChangedEventHandler();

		public event AccountListChangedEventHandler AccountListChanged;

		public delegate void AccountFilterChangedEventHandler();

		public event AccountFilterChangedEventHandler AccountFilterChanged;

		public int EndPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					if (value != intEndPeriod)
					{
						intEndPeriod = value;
						// FilterAccountList
					}
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int EndPeriod = 0;
				EndPeriod = intEndPeriod;
				return EndPeriod;
			}
		}

		public int StartPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					if (value != intStartPeriod)
					{
						intStartPeriod = value;
						// FilterAccountList
					}
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int StartPeriod = 0;
				StartPeriod = intStartPeriod;
				return StartPeriod;
			}
		}

		public bool ExcludeZeroBalance
		{
			set
			{
				if (value != boolExcludeZeroBalance)
				{
					boolExcludeZeroBalance = value;
					FilterAccountList();
				}
			}
			get
			{
				bool ExcludeZeroBalance = false;
				ExcludeZeroBalance = boolExcludeZeroBalance;
				return ExcludeZeroBalance;
			}
		}

		public bool ExcludeNonActivity
		{
			set
			{
				if (value != boolExcludeNonActivity)
				{
					boolExcludeNonActivity = value;
					FilterAccountList();
				}
			}
			get
			{
				bool ExcludeNonActivity = false;
				ExcludeNonActivity = boolExcludeNonActivity;
				return ExcludeNonActivity;
			}
		}

		public cGenericCollection AccountList
		{
			get
			{
				cGenericCollection AccountList = null;
				AccountList = listAccount;
				return AccountList;
			}
		}

		public string AccountTemplate
		{
			get
			{
				string AccountTemplate = "";
				AccountTemplate = strAccountTemplate;
				return AccountTemplate;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short GetSegmentLength(int intSegment)
		{
			short GetSegmentLength = 0;
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegmentLength = FCConvert.ToInt16(SegmentLength[intSegment - 1]);
			}
			return GetSegmentLength;
		}

		public string GetSegmentFilter(int intSegment)
		{
			string GetSegmentFilter = "";
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegmentFilter = strSegmentFilter[intSegment - 1];
			}
			return GetSegmentFilter;
		}

		public void SetSegmentFilter(int intSegment, string strValue)
		{
			if (intSegment > 0 && intSegment < 5)
			{
				string strTemp = "";
				// strTemp = Replace(strValue, "X", "")
				// strTemp = Replace(strTemp, "x", "")
				strTemp = strTemp.Replace(" ", "");
				strTemp = Strings.UCase(strTemp);
				strTemp = Strings.Trim(strTemp);
				if (strTemp.Length == 0 || strTemp.Length == GetSegmentLength(intSegment))
				{
					strSegmentFilter[intSegment - 1] = strValue;
					FilterAccountList();
				}
			}
		}

		public short AccountTypeToShow
		{
			set
			{
				if (intAccountTypeToShow != value)
				{
					intAccountTypeToShow = value;
					strSegmentFilter[0] = "";
					strSegmentFilter[1] = "";
					strSegmentFilter[2] = "";
					strSegmentFilter[3] = "";
					strSegmentFilter[4] = "";
					switch (intAccountTypeToShow)
					{
						case 0:
							{
								// all
								strAccountTemplate = "000000";
								break;
							}
						case 1:
							{
								strAccountTemplate = strExpenseTemplate;
								break;
							}
						case 2:
							{
								strAccountTemplate = strRevenueTemplate;
								break;
							}
						case 3:
							{
								strAccountTemplate = strLedgerTemplate;
								break;
							}
					}
					//end switch
					UpdateSegmentLengths(strAccountTemplate);
					if (this.AccountFilterChanged != null)
						this.AccountFilterChanged();
					//Application.DoEvents();
					FilterAccountList();
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short AccountTypeToShow = 0;
				AccountTypeToShow = FCConvert.ToInt16(intAccountTypeToShow);
				return AccountTypeToShow;
			}
		}

		private void UpdateSegmentLengths(string strTemplate)
		{
			string strRemainder;
			int intCount;
			int intLength = 0;
			int intSegment;
			int x;
			strRemainder = strTemplate;
			intCount = 0;
			intSegment = 0;
			for (x = 0; x <= Information.UBound(SegmentLength, 1); x++)
			{
				SegmentLength[x] = 0;
			}
			while (intCount < 5)
			{
				intCount += 1;
				if (strRemainder.Length >= 2)
				{
					intLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strRemainder, 2))));
					if (strRemainder.Length > 2)
					{
						strRemainder = Strings.Mid(strRemainder, 3);
					}
					else
					{
						strRemainder = "";
					}
				}
				else
				{
					strRemainder = "";
					intLength = 0;
				}
				if (intLength > 0)
				{
					intSegment += 1;
					SegmentLength[intSegment - 1] = intLength;
				}
			}
		}

		public void SetAccountFilter(string strFilter)
		{
			if (strFilter != strAccountFilter)
			{
				strAccountFilter = strFilter;
				if (this.AccountFilterChanged != null)
					this.AccountFilterChanged();
			}
		}

		public string AccountFilter
		{
			get
			{
				string AccountFilter = "";
				AccountFilter = strAccountFilter;
				return AccountFilter;
			}
		}

		public void Refresh()
		{
			RefreshAccountList();
			int intOldType;
			intOldType = intAccountTypeToShow;
			intAccountTypeToShow = -1;
			AccountTypeToShow = FCConvert.ToInt16(intOldType);
		}

		private void RefreshAccountList()
		{
			listAccount = repServ.GetAccountSummaries(false, false);
			if (repServ.HadError)
			{
				Information.Err().Raise(repServ.LastErrorNumber, null, repServ.LastErrorMessage, null, null);
			}
		}

		public void FilterAccountList()
		{
			listAccount.MoveFirst();
			cAccountSummaryItem summItem;
			int intCount;
			string strTemp = "";
			string strSeg = "";
			string strTempSeg = "";
			int intXloc = 0;
			while (listAccount.IsCurrent())
			{
				summItem = (cAccountSummaryItem)listAccount.GetCurrentItem();
				summItem.Include = true;
				summItem.SetTotalsByPeriodRange(intStartPeriod, intEndPeriod);
				switch (intAccountTypeToShow)
				{
					case 1:
						{
							// e
							if (!summItem.IsExpense)
							{
								summItem.Include = false;
							}
							break;
						}
					case 2:
						{
							// r
							if (!summItem.IsRevenue)
							{
								summItem.Include = false;
							}
							break;
						}
					case 3:
						{
							// g
							if (!summItem.IsGeneralLedger)
							{
								summItem.Include = false;
							}
							break;
						}
				}
				//end switch
				if (boolExcludeNonActivity)
				{
					if (summItem.Credits == 0 && summItem.Debits == 0)
					{
						summItem.Include = false;
					}
				}
				if (boolExcludeZeroBalance)
				{
					if (summItem.EndBalance == 0)
					{
						summItem.Include = false;
					}
				}
				if (summItem.Include)
				{
					for (intCount = 1; intCount <= 4; intCount++)
					{
						// strTemp = Replace(strSegmentFilter(intCount - 1), "X", "")
						// strTemp = Trim(Replace(strTemp, "x", ""))
						strTemp = Strings.UCase(strSegmentFilter[intCount - 1]);
						if (strTemp != "")
						{
							strSeg = summItem.GetSegment(intCount);
							if (strTemp != strSeg)
							{
								strTempSeg = "";
								intXloc = 1;
								while (intXloc <= strTemp.Length)
								{
									if (Strings.Mid(strTemp, intXloc, 1) == "X")
									{
										strTempSeg += "X";
									}
									else
									{
										strTempSeg += Strings.Mid(strSeg, intXloc, 1);
									}
									intXloc += 1;
								}
								if (strTempSeg != strTemp)
								{
									summItem.Include = false;
									break;
								}
							}
						}
					}
				}
				listAccount.MoveNext();
			}
			if (this.AccountListChanged != null)
				this.AccountListChanged();
		}

		public cBDAccountInquiryView() : base()
		{
			strLedgerTemplate = modAccountTitle.Statics.Ledger;
			strExpenseTemplate = modAccountTitle.Statics.Exp;
			strRevenueTemplate = modAccountTitle.Statics.Rev;
			intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (intFirstPeriod == 1)
			{
				intLastPeriod = 12;
			}
			else
			{
				intLastPeriod = intFirstPeriod - 1;
			}
			intStartPeriod = intFirstPeriod;
			intEndPeriod = intLastPeriod;
		}

		public void ShowDetails(string strAccount, FCForm parentForm)
		{
			if (strAccount != "")
			{
				cBDAccountDetailView detView = new cBDAccountDetailView();
				frmBDAccountDetail detForm = new frmBDAccountDetail();
				detForm.ShowWait();
				FCUtils.StartTask(parentForm, () =>
				{
					detForm.UpdateWait("Loading Data ...");
					//FC:FINAL:ASZ: adding event handlers
					detView.AccountChanged += detForm.theView_AccountChanged;
					detView.DetailsChanged += detForm.theView_DetailsChanged;
					//frmBusy bWait = new frmBusy();
					//bWait.StartBusy();
					//bWait.Show(FCForm.FormShowEnum.Modeless);
					detForm.DataContext = detView;
					detView.AccountNumber = strAccount;
					detView.StartPeriod = FCConvert.ToInt16(intStartPeriod);
					detView.EndPeriod = FCConvert.ToInt16(intEndPeriod);
					detView.FilterDetails();
                    detForm.EndWait();
                });
				detForm.Show(App.MainForm);
                //bWait.StopBusy();
				//FC:FINAL:BBE:#i774 - Hide frmBusy instead of Unload to reactivate this form where we are working on. Then the click event for buttons is fired correctly.
				//bWait.Unload();
				//bWait.Hide();
				/*- bWait = null; */
			}
		}

		public void PrintInquiry()
		{
			cBDAccountInquiryReport theReport = new cBDAccountInquiryReport();
			cAccountSummaryItem currAccount;
			int x;
			theReport.AccountTypeToShow = AccountTypeToShow;
			for (x = 1; x <= 4; x++)
			{
				theReport.SetSegmentFilter(x, GetSegmentFilter(x));
				theReport.SetSegmentLength(x, GetSegmentLength(x));
			}
			theReport.Accounts.ClearList();
			listAccount.MoveFirst();
			while (listAccount.IsCurrent())
			{
				currAccount = (cAccountSummaryItem)listAccount.GetCurrentItem();
				if (currAccount.Include)
				{
					theReport.Accounts.AddItem(currAccount);
				}
				listAccount.MoveNext();
			}
			if (theReport.Accounts.ItemCount() > 0)
			{
				repServ.ShowAccountInquiryReport(ref theReport);
			}
			else
			{
				MessageBox.Show("No Accounts to print", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void PrintAccountStatus(string strAccount)
		{
			if (Strings.Trim(strAccount) != "")
			{
				cBDAccountStatusReport theReport = new cBDAccountStatusReport();
				theReport.Account = strAccount;
				theReport.AccountRangeType = 1;
				// single
				if (intStartPeriod != intFirstPeriod || intEndPeriod != intLastPeriod)
				{
					theReport.ActivityRangeType = 2;
					// period range
				}
				else
				{
					theReport.ActivityRangeType = 0;
					// all
				}
				theReport.EndAccount = strAccount;
				theReport.StartPeriod = FCConvert.ToInt16(intStartPeriod);
				theReport.EndPeriod = FCConvert.ToInt16(intEndPeriod);
				repServ.FillBDAccountStatusReport(ref theReport);
				rptBDAccountStatus statReport = new rptBDAccountStatus();
				statReport.Init(theReport);
			}
			else
			{
				MessageBox.Show("No account specified", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}
