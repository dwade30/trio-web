﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frm1099Setup.
	/// </summary>
	public partial class frm1099Setup : BaseForm
	{
		public frm1099Setup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frm1099Setup InstancePtr
		{
			get
			{
				return (frm1099Setup)Sys.GetInstance(typeof(frm1099Setup));
			}
		}

		protected frm1099Setup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         8/13/02
		// This form will be used to print the actual 1099 forms for
		// vendors the towns have paid
		// ********************************************************
		public string ReportPrinter = "";
		public string OptionType = "";
        int NameCol;
        int CategoryCol;
        int FormCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private c1099Service ten99Service = new c1099Service();
		private c1099Service ten99Service_AutoInitialized;

		private c1099Service ten99Service
		{
			get
			{
				if (ten99Service_AutoInitialized == null)
				{
					ten99Service_AutoInitialized = new c1099Service();
				}
				return ten99Service_AutoInitialized;
			}
			set
			{
				ten99Service_AutoInitialized = value;
			}
		}

		private void frm1099Setup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frm1099Setup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frm1099Setup_Load(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();

            NameCol = 1;
            FormCol = 2;
            CategoryCol = 3;

            vsCodeInfo.ColWidth(0, FCConvert.ToInt32(vsCodeInfo.WidthOriginal * 0.1));
            vsCodeInfo.ColWidth(NameCol, FCConvert.ToInt32(vsCodeInfo.WidthOriginal * 0.45));
            vsCodeInfo.ColWidth(FormCol, FCConvert.ToInt32(vsCodeInfo.WidthOriginal * 0.1));
            vsCodeInfo.ColWidth(CategoryCol, FCConvert.ToInt32(vsCodeInfo.WidthOriginal * 0.01));

            vsCodeInfo.TextMatrix(0, 0, "Code");
            vsCodeInfo.TextMatrix(0, NameCol, "Tax Title");
            vsCodeInfo.TextMatrix(0, FormCol, "Form");
            vsCodeInfo.TextMatrix(0, CategoryCol, "Box");

			vsCodeInfo.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			LoadCodeInfo();

			rsDefaultInfo.OpenRecordset("SELECT * FROM [1099Information]");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				txtFederalID.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("FederalCode"));
				txtStateId.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("StateIDCode"));
				txtMunicipality.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Municipality"));
				txtAddress1.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Address1"));
				txtAddress2.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Address2"));
				txtCity.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("City"));
				txtState.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("State"));
				txtZip.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Zip"));
				txtZip4.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Zip4"));
				txtContact.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Contact"));
				txtPhone.Text = Strings.Format(rsDefaultInfo.Get_Fields_String("Phone"), "(000)###-####");
				txtExtension.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Extension"));
				txtTCC.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("TCC"));
				txtEmail.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("EMail"));
				txtFax.Text = FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Fax"));
				if (FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("ElectronicFiling")))
				{
					chkFileElectronically.CheckState = CheckState.Checked;
				}
				else
				{
					chkFileElectronically.CheckState = CheckState.Unchecked;
				}
			}
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			// check to make sure all the required information ave been input correctly
			if (Strings.Trim(txtFederalID.Text) == "")
			{
				MessageBox.Show("You must enter your Federal ID before you may continue", "No Federal ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtStateId.Text) == "")
			{
				MessageBox.Show("You must enter your State ID before you may continue", "No State ID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtMunicipality.Text) == "")
			{
				MessageBox.Show("You must enter your Municipality Name before you may continue", "No Municipality", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtAddress1.Text) == "")
			{
				MessageBox.Show("You must enter your address before you may continue", "No Address", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtCity.Text) == "")
			{
				MessageBox.Show("You must enter your city before you may continue", "No City", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtState.Text) == "")
			{
				MessageBox.Show("You must enter your state before you may continue", "No State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Strings.Trim(txtZip.Text) == "")
			{
				MessageBox.Show("You must enter your zip code before you may continue", "No Zip Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkFileElectronically.CheckState == CheckState.Checked)
			{
				if (Strings.Trim(txtContact.Text) == "")
				{
					MessageBox.Show("You must enter your contact before you may continue", "No Contact", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Strings.Trim(txtPhone.Text) == "")
				{
					MessageBox.Show("You must enter your phone number before you may continue", "No Phone Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (Strings.Trim(txtTCC.Text) == "")
				{
					MessageBox.Show("You must enter your transmitter control code before you may continue", "No TCC", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			// verify the information then if correct print the report
			ans = MessageBox.Show("Are you sure this is the information you wish to use?", "Correct Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				return;
			}
			rsDefaultInfo.OpenRecordset("SELECT * FROM [1099Information]");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				rsDefaultInfo.Edit();
			}
			else
			{
				rsDefaultInfo.AddNew();
			}
			rsDefaultInfo.Set_Fields("FederalCode", Strings.Trim(txtFederalID.Text));
			rsDefaultInfo.Set_Fields("StateIDCode", Strings.Trim(txtStateId.Text));
			rsDefaultInfo.Set_Fields("Municipality", Strings.Trim(txtMunicipality.Text));
			rsDefaultInfo.Set_Fields("Address1", Strings.Trim(txtAddress1.Text));
			rsDefaultInfo.Set_Fields("Address2", Strings.Trim(txtAddress2.Text));
			rsDefaultInfo.Set_Fields("Address3", "");
			rsDefaultInfo.Set_Fields("City", Strings.Trim(txtCity.Text));
			rsDefaultInfo.Set_Fields("State", Strings.Trim(txtState.Text));
			rsDefaultInfo.Set_Fields("Zip", Strings.Trim(txtZip.Text));
			rsDefaultInfo.Set_Fields("Zip4", Strings.Trim(txtZip4.Text));
			rsDefaultInfo.Set_Fields("Contact", Strings.Trim(txtContact.Text));
			rsDefaultInfo.Set_Fields("Phone", Strings.Trim(txtPhone.Text));
			rsDefaultInfo.Set_Fields("Extension", Strings.Trim(txtExtension.Text));
			rsDefaultInfo.Set_Fields("TCC", Strings.Trim(txtTCC.Text));
			rsDefaultInfo.Set_Fields("EMail", Strings.Trim(txtEmail.Text));
			rsDefaultInfo.Set_Fields("Fax", Strings.Trim(txtFax.Text));
			rsDefaultInfo.Set_Fields("ElectronicFiling", chkFileElectronically.CheckState == CheckState.Checked);
			rsDefaultInfo.Update(false);

			if (OptionType == "P")
            {
                var result = StaticSettings.GlobalCommandDispatcher.Send(new SelectTaxFormType
                {
                    ShowAllOption = false
                }).Result;

                if (result.Success)
                {
                    if (result.selectedType == TaxFormType.MISC1099)
                    {
                        frmReportViewer.InstancePtr.Init(rpt1099MiscLaser.InstancePtr);
					}
					else if (result.selectedType == TaxFormType.NEC1099)
                    {
                        frmReportViewer.InstancePtr.Init(rpt1099NECLaser.InstancePtr);
					}
                }
                else
                {
                    return;
                }
            }
			else if (OptionType == "E")
			{
				frmEditReportSetup.InstancePtr.Show(App.MainForm);
			}
			else
			{
                var result = StaticSettings.GlobalCommandDispatcher.Send(new SelectTaxFormType
                {
                    ShowAllOption = false
                }).Result;

                if (result.Success)
                {
					ten99Service.CreateElectronic1099File(result.selectedType);
				}
                else
                {
                    return;
                }
            }
		}

		private void txtAddress1_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAddress2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAddress3_KeyPress(ref short KeyAscii)
		{
			// capitalize letters entered
			if (KeyAscii >= 97 && KeyAscii <= 122)
			{
				KeyAscii -= 32;
			}
		}

		private void txtCity_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtContact_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMunicipality_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadCodeInfo()
		{
			clsDRWrapper rsCode = new clsDRWrapper();
			ITaxFormService taxFormService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxFormService()).Result;

			rsCode.OpenRecordset("SELECT * FROM TaxTitles ORDER BY TaxCode");
			if (rsCode.EndOfFile() != true && rsCode.BeginningOfFile() != true)
			{
				do
				{
                    if (rsCode.Get_Fields("Category") != 0)
                    {
                        vsCodeInfo.AddItem("");
                        vsCodeInfo.TextMatrix(vsCodeInfo.Rows - 1, 0, FCConvert.ToString(rsCode.Get_Fields_Int32("TaxCode")));
                        vsCodeInfo.TextMatrix(vsCodeInfo.Rows - 1, NameCol, FCConvert.ToString(rsCode.Get_Fields_String("TaxDescription")));
                        vsCodeInfo.TextMatrix(vsCodeInfo.Rows - 1, FormCol, FCConvert.ToString(rsCode.Get_Fields("FormName")));
                        vsCodeInfo.TextMatrix(vsCodeInfo.Rows - 1, CategoryCol, FCConvert.ToString(rsCode.Get_Fields("Category")) + " - " + taxFormService.GetTaxCategoryDescription(rsCode.Get_Fields("FormName") == "NEC" ? TaxFormType.NEC1099 : TaxFormType.MISC1099, rsCode.Get_Fields("Category")));
					}
                    rsCode.MoveNext();
				}
				while (rsCode.EndOfFile() != true);
			}
		}

		private void txtState_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// capitalize letters entered
			if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(mnuProcessSave, EventArgs.Empty);
		}
    }
}
