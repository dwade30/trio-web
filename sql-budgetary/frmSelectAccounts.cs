﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSelectAccounts.
	/// </summary>
	public partial class frmSelectAccounts : BaseForm
	{
		public frmSelectAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectAccounts InstancePtr
		{
			get
			{
				return (frmSelectAccounts)Sys.GetInstance(typeof(frmSelectAccounts));
			}
		}

		protected frmSelectAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int KeyCol;
		int SelectCol;
		int AccountCol;

		private void frmSelectAccounts_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSelectAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectAccounts.FillStyle	= 0;
			//frmSelectAccounts.ScaleWidth	= 5880;
			//frmSelectAccounts.ScaleHeight	= 3810;
			//frmSelectAccounts.LinkTopic	= "Form2";
			//frmSelectAccounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			KeyCol = 0;
			SelectCol = 1;
			AccountCol = 2;
			vsAccounts.ColHidden(KeyCol, true);
			vsAccounts.ColWidth(SelectCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.25));
			//FC:FINAL:RPU:#i586-Enlarge AccountCol
			vsAccounts.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.30));
			vsAccounts.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsAccounts.TextMatrix(0, AccountCol, "Account");
			vsAccounts.TextMatrix(0, SelectCol, "Select");
			vsAccounts.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' or AccountType = 'P' ORDER BY Account");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsAccounts.Rows += 1;
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("ValidateBalance")))
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Rows - 1, SelectCol, FCConvert.ToString(false));
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, FCConvert.ToString(rsInfo.Get_Fields("Account")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSelectAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE ID = " + vsAccounts.TextMatrix(counter, KeyCol));
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
					if (FCUtils.CBool(vsAccounts.TextMatrix(counter, SelectCol)) == true)
					{
						rsInfo.Set_Fields("validateBalance", true);
					}
					else
					{
						rsInfo.Set_Fields("validateBalance", false);
					}
					rsInfo.Update();
				}
			}
			Close();
		}

		private void mnuSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				vsAccounts.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void mnuUnselectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				vsAccounts.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void vsAccounts_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsAccounts.Row > 0)
			{
				if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.Row, SelectCol)) == true)
				{
					vsAccounts.TextMatrix(vsAccounts.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsAccounts.Row > 0)
				{
					if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.Row, SelectCol)) == true)
					{
						vsAccounts.TextMatrix(vsAccounts.Row, SelectCol, FCConvert.ToString(false));
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Row, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}
	}
}
