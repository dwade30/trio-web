﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseDetailVendorSummarySetup.
	/// </summary>
	partial class frmExpenseDetailVendorSummarySetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraPageBreaks;
		public fecherFoundation.FCCheckBox chkDivision;
		public fecherFoundation.FCCheckBox chkDepartment;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdCancelPrint;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCCheckBox chkCheckAccountRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpenseDetailVendorSummarySetup));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbAllAccounts = new fecherFoundation.FCComboBox();
            this.lblAllAccounts = new fecherFoundation.FCLabel();
            this.fraPageBreaks = new fecherFoundation.FCFrame();
            this.chkDivision = new fecherFoundation.FCCheckBox();
            this.chkDepartment = new fecherFoundation.FCCheckBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelPrint = new fecherFoundation.FCButton();
            this.fraMonths = new fecherFoundation.FCFrame();
            this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.fraDeptRange = new fecherFoundation.FCFrame();
            this.chkCheckAccountRange = new fecherFoundation.FCCheckBox();
            this.cboEndingDept = new fecherFoundation.FCComboBox();
            this.cboSingleDept = new fecherFoundation.FCComboBox();
            this.vsLowAccount = new fecherFoundation.FCGrid();
            this.vsHighAccount = new fecherFoundation.FCGrid();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.lblTo_1 = new fecherFoundation.FCLabel();
            this.cboBeginningDept = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).BeginInit();
            this.fraPageBreaks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDivision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
            this.fraMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
            this.fraDeptRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPageBreaks);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.fraMonths);
            this.ClientArea.Controls.Add(this.fraDeptRange);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCancelPrint);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdCancelPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(516, 30);
            this.HeaderText.Text = "Expense - Vendor Summary Selection Criteria";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Single Month",
            "Range of Months"});
            this.cmbRange.Location = new System.Drawing.Point(210, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(245, 40);
            this.cmbRange.TabIndex = 5;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(145, 15);
            this.lblRange.TabIndex = 6;
            this.lblRange.Text = "MONTH(S) TO REPORT ";
            // 
            // cmbAllAccounts
            // 
            this.cmbAllAccounts.Items.AddRange(new object[] {
            "All",
            "Account Range",
            "Single Department",
            "Department Range"});
            this.cmbAllAccounts.Location = new System.Drawing.Point(250, 30);
            this.cmbAllAccounts.Name = "cmbAllAccounts";
            this.cmbAllAccounts.Size = new System.Drawing.Size(205, 40);
            this.cmbAllAccounts.TabIndex = 13;
            this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.optAllAccounts_CheckedChanged);
            // 
            // lblAllAccounts
            // 
            this.lblAllAccounts.AutoSize = true;
            this.lblAllAccounts.Location = new System.Drawing.Point(20, 44);
            this.lblAllAccounts.Name = "lblAllAccounts";
            this.lblAllAccounts.Size = new System.Drawing.Size(189, 15);
            this.lblAllAccounts.TabIndex = 14;
            this.lblAllAccounts.Text = "ACCOUNTS TO BE REPORTED";
            this.lblAllAccounts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraPageBreaks
            // 
            this.fraPageBreaks.Controls.Add(this.chkDivision);
            this.fraPageBreaks.Controls.Add(this.chkDepartment);
            this.fraPageBreaks.Location = new System.Drawing.Point(30, 315);
            this.fraPageBreaks.Name = "fraPageBreaks";
            this.fraPageBreaks.Size = new System.Drawing.Size(154, 99);
            this.fraPageBreaks.TabIndex = 26;
            this.fraPageBreaks.Text = "Page Breaks";
            // 
            // chkDivision
            // 
            this.chkDivision.Location = new System.Drawing.Point(20, 60);
            this.chkDivision.Name = "chkDivision";
            this.chkDivision.Size = new System.Drawing.Size(85, 27);
            this.chkDivision.TabIndex = 17;
            this.chkDivision.Text = "Division";
            this.chkDivision.CheckedChanged += new System.EventHandler(this.chkDivision_CheckedChanged);
            // 
            // chkDepartment
            // 
            this.chkDepartment.Location = new System.Drawing.Point(20, 30);
            this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Size = new System.Drawing.Size(113, 27);
            this.chkDepartment.TabIndex = 16;
            this.chkDepartment.Text = "Department";
            this.chkDepartment.CheckedChanged += new System.EventHandler(this.chkDepartment_CheckedChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(216, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(269, 40);
            // 
            // cmdCancelPrint
            // 
            this.cmdCancelPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCancelPrint.Location = new System.Drawing.Point(1050, 29);
            this.cmdCancelPrint.Name = "cmdCancelPrint";
            this.cmdCancelPrint.Size = new System.Drawing.Size(57, 24);
            this.cmdCancelPrint.TabIndex = 19;
            this.cmdCancelPrint.Text = "Cancel";
            this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
            // 
            // fraMonths
            // 
            this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
            this.fraMonths.Controls.Add(this.chkCheckDateRange);
            this.fraMonths.Controls.Add(this.cmbRange);
            this.fraMonths.Controls.Add(this.lblRange);
            this.fraMonths.Controls.Add(this.cboEndingMonth);
            this.fraMonths.Controls.Add(this.lblTo_0);
            this.fraMonths.Controls.Add(this.cboSingleMonth);
            this.fraMonths.Controls.Add(this.cboBeginningMonth);
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Location = new System.Drawing.Point(30, 100);
            this.fraMonths.Name = "fraMonths";
            this.fraMonths.Size = new System.Drawing.Size(475, 185);
            this.fraMonths.TabIndex = 22;
            this.fraMonths.Text = "Month(s) To Report";
            // 
            // chkCheckDateRange
            // 
            this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckDateRange.Location = new System.Drawing.Point(20, 87);
            this.chkCheckDateRange.Name = "chkCheckDateRange";
            this.chkCheckDateRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckDateRange.TabIndex = 4;
            this.chkCheckDateRange.Text = "Select at report time";
            this.chkCheckDateRange.Visible = false;
            this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(265, 125);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(190, 40);
            this.cboEndingMonth.TabIndex = 6;
            this.cboEndingMonth.Visible = false;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_0.Location = new System.Drawing.Point(230, 139);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(21, 19);
            this.lblTo_0.TabIndex = 23;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(20, 125);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(190, 40);
            this.cboSingleMonth.TabIndex = 7;
            this.cboSingleMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 125);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(190, 40);
            this.cboBeginningMonth.TabIndex = 5;
            this.cboBeginningMonth.Visible = false;
            // 
            // fraDeptRange
            // 
            this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
            this.fraDeptRange.Controls.Add(this.chkCheckAccountRange);
            this.fraDeptRange.Controls.Add(this.cmbAllAccounts);
            this.fraDeptRange.Controls.Add(this.lblAllAccounts);
            this.fraDeptRange.Controls.Add(this.cboEndingDept);
            this.fraDeptRange.Controls.Add(this.cboSingleDept);
            this.fraDeptRange.Controls.Add(this.vsLowAccount);
            this.fraDeptRange.Controls.Add(this.vsHighAccount);
            this.fraDeptRange.Controls.Add(this.lblTo_2);
            this.fraDeptRange.Controls.Add(this.lblTo_1);
            this.fraDeptRange.Controls.Add(this.cboBeginningDept);
            this.fraDeptRange.Location = new System.Drawing.Point(535, 100);
            this.fraDeptRange.Name = "fraDeptRange";
            this.fraDeptRange.Size = new System.Drawing.Size(475, 185);
            this.fraDeptRange.TabIndex = 20;
            this.fraDeptRange.Text = "Accounts To Be Reported";
            // 
            // chkCheckAccountRange
            // 
            this.chkCheckAccountRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckAccountRange.Location = new System.Drawing.Point(20, 87);
            this.chkCheckAccountRange.Name = "chkCheckAccountRange";
            this.chkCheckAccountRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckAccountRange.TabIndex = 12;
            this.chkCheckAccountRange.Text = "Select at report time";
            this.chkCheckAccountRange.Visible = false;
            this.chkCheckAccountRange.CheckedChanged += new System.EventHandler(this.chkCheckAccountRange_CheckedChanged);
            // 
            // cboEndingDept
            // 
            this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingDept.Location = new System.Drawing.Point(265, 125);
            this.cboEndingDept.Name = "cboEndingDept";
            this.cboEndingDept.Size = new System.Drawing.Size(190, 40);
            this.cboEndingDept.TabIndex = 14;
            this.cboEndingDept.Visible = false;
            this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
            // 
            // cboSingleDept
            // 
            this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleDept.Location = new System.Drawing.Point(20, 125);
            this.cboSingleDept.Name = "cboSingleDept";
            this.cboSingleDept.Size = new System.Drawing.Size(190, 40);
            this.cboSingleDept.TabIndex = 15;
            this.cboSingleDept.Visible = false;
            this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
            // 
            // vsLowAccount
            // 
            this.vsLowAccount.Cols = 1;
            this.vsLowAccount.ColumnHeadersVisible = false;
            this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLowAccount.FixedCols = 0;
            this.vsLowAccount.FixedRows = 0;
            this.vsLowAccount.Location = new System.Drawing.Point(20, 125);
            this.vsLowAccount.Name = "vsLowAccount";
            this.vsLowAccount.ReadOnly = false;
            this.vsLowAccount.RowHeadersVisible = false;
            this.vsLowAccount.Rows = 1;
            this.vsLowAccount.Size = new System.Drawing.Size(190, 42);
            this.vsLowAccount.TabIndex = 27;
            this.vsLowAccount.Visible = false;
            // 
            // vsHighAccount
            // 
            this.vsHighAccount.Cols = 1;
            this.vsHighAccount.ColumnHeadersVisible = false;
            this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHighAccount.FixedCols = 0;
            this.vsHighAccount.FixedRows = 0;
            this.vsHighAccount.Location = new System.Drawing.Point(265, 125);
            this.vsHighAccount.Name = "vsHighAccount";
            this.vsHighAccount.ReadOnly = false;
            this.vsHighAccount.RowHeadersVisible = false;
            this.vsHighAccount.Rows = 1;
            this.vsHighAccount.Size = new System.Drawing.Size(190, 42);
            this.vsHighAccount.TabIndex = 28;
            this.vsHighAccount.Visible = false;
            // 
            // lblTo_2
            // 
            this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_2.Location = new System.Drawing.Point(230, 139);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(21, 19);
            this.lblTo_2.TabIndex = 24;
            this.lblTo_2.Text = "TO";
            this.lblTo_2.Visible = false;
            // 
            // lblTo_1
            // 
            this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_1.Location = new System.Drawing.Point(230, 139);
            this.lblTo_1.Name = "lblTo_1";
            this.lblTo_1.Size = new System.Drawing.Size(21, 19);
            this.lblTo_1.TabIndex = 21;
            this.lblTo_1.Text = "TO";
            this.lblTo_1.Visible = false;
            // 
            // cboBeginningDept
            // 
            this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningDept.Location = new System.Drawing.Point(20, 125);
            this.cboBeginningDept.Name = "cboBeginningDept";
            this.cboBeginningDept.Size = new System.Drawing.Size(190, 40);
            this.cboBeginningDept.TabIndex = 13;
            this.cboBeginningDept.Visible = false;
            this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(124, 17);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "SELECTION CRITERIA";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmExpenseDetailVendorSummarySetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmExpenseDetailVendorSummarySetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Expense - Vendor Summary Selection Criteria";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmExpenseDetailVendorSummarySetup_Load);
            this.Activated += new System.EventHandler(this.frmExpenseDetailVendorSummarySetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExpenseDetailVendorSummarySetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExpenseDetailVendorSummarySetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).EndInit();
            this.fraPageBreaks.ResumeLayout(false);
            this.fraPageBreaks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDivision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
            this.fraMonths.ResumeLayout(false);
            this.fraMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
            this.fraDeptRange.ResumeLayout(false);
            this.fraDeptRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
