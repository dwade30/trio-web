﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmFlagCashed.
	/// </summary>
	public partial class frmFlagCashed : BaseForm
	{
		public frmFlagCashed()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			KeyCol = 0;
			CashCol = 1;
			CheckCol = 2;
			AmountCol = 3;
			TypeCol = 4;
			DateCol = 5;
			PayeeCol = 6;
			vs1.ColHidden(KeyCol, true);
			vs1.ColDataType(CashCol, FCGrid.DataTypeSettings.flexDTBoolean);
			//FC:FINAL:DDU:#3000 - align columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(CashCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(PayeeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.TextMatrix(0, CashCol, "Cashed");
			vs1.TextMatrix(0, CheckCol, "Check");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, PayeeCol, "Payee");
			vs1.ColWidth(CashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			vs1.ColWidth(CheckCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1551166));
			vs1.ColWidth(TypeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0610884));
			vs1.ColWidth(DateCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1381118));
			vs1.ColWidth(PayeeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4042798));
			dblColPercents[0] = 0.0981118;
			dblColPercents[1] = 0.0981118;
			dblColPercents[2] = 0.1551166;
			dblColPercents[3] = 0.0610884;
			dblColPercents[4] = 0.1381118;
			dblColPercents[5] = 0.4042798;
			//vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// show a border between the titles and the data input section of the grid
			vs1.Select(0, 0, 0, vs1.Cols - 1);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			//FC:FINAL:MSH - Issue #850: set type of amount column
			vs1.ColDataType(AmountCol, FCGrid.DataTypeSettings.flexDTCurrency);
			//FC:FINAL:MSH - Issue #850: set format of displayed values, not of stored values
			vs1.ColFormat(AmountCol, "#,##0.00");
			lblBank.Text = App.MainForm.StatusBarText3;
			lblStatementDate.Text = "Statement:  " + Strings.Format(modBudgetaryAccounting.GetBDVariable("StatementDate"), "MM/dd/yy");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			//FC:FINAL:ASZ: default selection - All
			cmbSelected.SelectedIndex = 1;
			//FC:FINAL:DDU:#3002 - allow only numeric in textboxes
			txtLow.AllowOnlyNumericInput();
			txtHigh.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFlagCashed InstancePtr
		{
			get
			{
				return (frmFlagCashed)Sys.GetInstance(typeof(frmFlagCashed));
			}
		}

		protected frmFlagCashed _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/1/02
		// This form will be used by towns to change the status of
		// checks and deposits to cashed
		// ********************************************************
		int KeyCol;
		int CashCol;
		int CheckCol;
		int TypeCol;
		int AmountCol;
		int DateCol;
		int PayeeCol;
		clsDRWrapper rsInfo = new clsDRWrapper();
		double[] dblColPercents = new double[5 + 1];
		// Dave 12/14/2006---------------------------------------------------
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		// -------------------------------------------------------------------
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, CashCol, FCConvert.ToString(false));
			}
		}

		private void cmdGetInfo_Click(object sender, System.EventArgs e)
		{
			int counter;
			if (cmbSelected.SelectedIndex == 0)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 type before you may continue", "None Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			CreateSQL();
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				vs1.Rows = rsInfo.RecordCount() + 1;
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					vs1.TextMatrix(counter, CashCol, FCConvert.ToString(false));
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, CheckCol, FCConvert.ToString(rsInfo.Get_Fields("CheckNumber")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, AmountCol, rsInfo.Get_Fields("Amount"));
					// FC:FINAL:VGE - #851 Using var instead of object for proper comparison later.
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					var vbPorterVar = rsInfo.Get_Fields("Type");
					if (vbPorterVar == "1")
					{
						vs1.TextMatrix(counter, TypeCol, "AP");
					}
					else if (vbPorterVar == "2")
					{
						vs1.TextMatrix(counter, TypeCol, "PY");
					}
					else if (vbPorterVar == "3")
					{
						vs1.TextMatrix(counter, TypeCol, "DP");
					}
					else if (vbPorterVar == "4")
					{
						vs1.TextMatrix(counter, TypeCol, "RT");
					}
					else if (vbPorterVar == "5")
					{
						vs1.TextMatrix(counter, TypeCol, "IN");
					}
					else if (vbPorterVar == "6")
					{
						vs1.TextMatrix(counter, TypeCol, "OC");
					}
					else if (vbPorterVar == "7")
					{
						vs1.TextMatrix(counter, TypeCol, "OD");
					}
					vs1.TextMatrix(counter, DateCol, Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy"));
					vs1.TextMatrix(counter, PayeeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
					rsInfo.MoveNext();
				}
			}
			else
			{
				MessageBox.Show("There are no outstanding transactions of the chosen type(s) at this time", "No Outstanding Transactions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				// vs1.rows = 1
				// vs1.Visible = True
				// lblBank.Visible = True
				// lblStatementDate.Visible = True
				// cmdProcess.Visible = True
				// cmdCancel.Visible = True
				// lblTotalAmount.Visible = True
				// lblTotalItemsFlagged.Visible = True
				// Label1.Visible = True
				// Label2.Visible = True
				// cmdClearAll.Visible = True
				// fraAccountType.Visible = False
				return;
			}
			//FC:FINAL:ASZ: benu process become menu save & exit
			//mnuProcessSave.Text = "Save & Exit";
			cmdProcessSave.Text = "Save";
			vs1.Visible = true;
			lblBank.Visible = true;
			lblStatementDate.Visible = true;
			cmdSelect.Visible = true;
			fraAccountType.Visible = false;
			lblTotalAmount.Visible = true;
			lblTotalItemsFlagged.Visible = true;
			Label1.Visible = true;
			Label2.Visible = true;
			cmdClearAll.Visible = true;
			Form_Resize();
		}

		public void cmdGetInfo_Click()
		{
			cmdGetInfo_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int counter;
			if (!Information.IsNumeric(txtLow.Text) || !Information.IsNumeric(txtHigh.Text))
			{
				MessageBox.Show("You must enter a valid range before you may proceed.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else if (Conversion.Val(txtLow.Text) > Conversion.Val(txtHigh.Text))
			{
				MessageBox.Show("You must enter a valid range before you may proceed.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (Conversion.Val(vs1.TextMatrix(counter, CheckCol)) >= Conversion.Val(txtLow.Text) && Conversion.Val(vs1.TextMatrix(counter, CheckCol)) <= Conversion.Val(txtHigh.Text))
				{
					vs1.TextMatrix(counter, CashCol, FCConvert.ToString(true));
				}
			}
			fraRange.Visible = false;
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vs1.TextMatrix(counter, CashCol)) == true)
				{
					rsInfo.FindFirstRecord("ID", vs1.TextMatrix(counter, KeyCol));
					rsInfo.Edit();
					rsInfo.Set_Fields("Status", "3");
					rsInfo.Set_Fields("StatusDate", DateTime.Today);
					rsInfo.Update(true);
					clsReportChanges.AddChange("Check: " + vs1.TextMatrix(counter, CheckCol) + "  Date: " + vs1.TextMatrix(counter, DateCol) + "  Payee: " + vs1.TextMatrix(counter, PayeeCol) + "  Amount: " + vs1.TextMatrix(counter, AmountCol) + "  - flagged as cashed");
				}
				vs1.TextMatrix(counter, CashCol, FCConvert.ToString(false));
			}
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Check Rec Flag Cashed", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
			Close();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cmdSelect_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, CashCol, FCConvert.ToString(true));
			}
		}

		private void cndCancelRange_Click(object sender, System.EventArgs e)
		{
			fraRange.Visible = false;
		}

		private void frmFlagCashed_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			Form_Resize();
			this.Refresh();
		}

		private void frmFlagCashed_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFlagCashed.FillStyle	= 0;
			//frmFlagCashed.ScaleWidth	= 9045;
			//frmFlagCashed.ScaleHeight	= 7245;
			//frmFlagCashed.LinkTopic	= "Form2";
			//frmFlagCashed.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:ASZ: moved code to Constructor
			//KeyCol = 0;
			//CashCol = 1;
			//CheckCol = 2;
			//AmountCol = 3;
			//TypeCol = 4;
			//DateCol = 5;
			//PayeeCol = 6;
			//vs1.ColHidden(KeyCol, true);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.TextMatrix(0, CashCol, "Cashed");
			//vs1.TextMatrix(0, CheckCol, "Check");
			//vs1.TextMatrix(0, AmountCol, "Amount");
			//vs1.TextMatrix(0, TypeCol, "Type");
			//vs1.TextMatrix(0, DateCol, "Date");
			//vs1.TextMatrix(0, PayeeCol, "Payee");
			//vs1.ColWidth(CashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			//vs1.ColWidth(CheckCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			//vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1551166));
			//vs1.ColWidth(TypeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0610884));
			//vs1.ColWidth(DateCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1381118));
			//vs1.ColWidth(PayeeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4042798));
			//dblColPercents[0] = 0.0981118;
			//dblColPercents[1] = 0.0981118;
			//dblColPercents[2] = 0.1551166;
			//dblColPercents[3] = 0.0610884;
			//dblColPercents[4] = 0.1381118;
			//dblColPercents[5] = 0.4042798;
			//vs1.ColDataType(CashCol, FCGrid.DataTypeSettings.flexDTBoolean);
			//vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//// show a border between the titles and the data input section of the grid
			//vs1.Select(0, 0, 0, vs1.Cols - 1);
			//vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			//lblBank.Text = App.MainForm.StatusBarText2;
			//lblStatementDate.Text = "Statement:  " + Strings.Format(modBudgetaryAccounting.GetBDVariable("StatementDate"), "MM/dd/yy");
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			//SetCustomFormColors();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (vs1.Visible == true)
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vs1.TextMatrix(counter, CashCol)) == true)
					{
						ans = MessageBox.Show("All your work that has not been saved will be lost if you exit this screen now.  Are you sure you wish to exit this screen?", "Leave Screen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							break;
						}
						else
						{
							e.Cancel = true;
							return;
						}
					}
				}
			}
			//MDIParent.InstancePtr.Focus();
		}

		private void frmFlagCashed_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(CashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			vs1.ColWidth(CheckCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0981118));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1551166));
			vs1.ColWidth(TypeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0610884));
			vs1.ColWidth(DateCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1381118));
			vs1.ColWidth(PayeeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4042798));
			if (vs1.Rows > 17)
			{
				//FC:FINAL:ASZ - use anchors: vs1.Height = vs1.RowHeight(0) * 17 + 75;
			}
			else
			{
				//FC:FINAL:ASZ - use anchors: vs1.Height = vs1.RowHeight(0) * vs1.Rows + 75;
			}
		}

		public void Form_Resize()
		{
			frmFlagCashed_Resize(this, new System.EventArgs());
		}

		private void frmFlagCashed_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileRange_Click(object sender, System.EventArgs e)
		{
			txtLow.Text = "";
			txtHigh.Text = "";
			fraRange.Visible = true;
			txtLow.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (fraAccountType.Visible == true)
			{
				cmdGetInfo_Click();
			}
			else
			{
				cmdProcess_Click();
			}
		}

		private void CreateSQL()
		{
			string strTemp;
			strTemp = "SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND Status = '2' AND (";
			if (cmbSelected.SelectedIndex == 0)
			{
				if (chkAP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '1' OR ";
				}
				if (chkPY.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '2' OR ";
				}
				if (chkDP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '3' OR ";
				}
				if (chkRT.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '4' OR ";
				}
				if (chkIN.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '5' OR ";
				}
				if (chkOC.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '6' OR ";
				}
				if (chkOD.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '7' OR ";
				}
				strTemp = Strings.Left(strTemp, strTemp.Length - 4) + ") ORDER BY Type, CheckNumber, CheckDate";
			}
			else
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 6) + " ORDER BY Type, CheckNumber, CheckDate";
			}
			rsInfo.OpenRecordset(strTemp);
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkAP.CheckState = CheckState.Unchecked;
			chkPY.CheckState = CheckState.Unchecked;
			chkDP.CheckState = CheckState.Unchecked;
			chkRT.CheckState = CheckState.Unchecked;
			chkIN.CheckState = CheckState.Unchecked;
			chkOC.CheckState = CheckState.Unchecked;
			chkOD.CheckState = CheckState.Unchecked;
			fraOptions.Enabled = false;
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//fraOptions.Enabled = true;
			if (sender == cmbSelected)
			{
				switch (cmbSelected.SelectedIndex)
				{
					case 0:
						{
							fraOptions.Enabled = true;
							break;
						}
					case 1:
						{
							optAll_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void txtHigh_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLow_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ClickEvent(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex > -1)
			{
				if (FCUtils.CBool(vs1.TextMatrix(vs1.Row, CashCol)) == true)
				{
					vs1.TextMatrix(vs1.Row, CashCol, FCConvert.ToString(false));
				}
				else
				{
					vs1.TextMatrix(vs1.Row, CashCol, FCConvert.ToString(true));
				}
			}
			CalculateTotals();
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (FCUtils.CBool(vs1.TextMatrix(vs1.Row, CashCol)) == true)
				{
					vs1.TextMatrix(vs1.Row, CashCol, FCConvert.ToString(false));
				}
				else
				{
					vs1.TextMatrix(vs1.Row, CashCol, FCConvert.ToString(true));
				}
			}
			CalculateTotals();
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			vs1.Col = CashCol;
		}

		private void SetCustomFormColors()
		{
			lblBank.ForeColor = Color.Blue;
			lblStatementDate.ForeColor = Color.Blue;
			Label1.ForeColor = Color.Blue;
			Label2.ForeColor = Color.Blue;
			lblTotalItemsFlagged.ForeColor = Color.Blue;
			lblTotalAmount.ForeColor = Color.Blue;
		}

		private void CalculateTotals()
		{
			int lngTotalItems;
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			int counter;
			lngTotalItems = 0;
			curTotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vs1.TextMatrix(counter, CashCol)) == true)
				{
					lngTotalItems += 1;
					curTotalAmount += FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol));
				}
			}
			lblTotalItemsFlagged.Text = FCConvert.ToString(lngTotalItems);
			lblTotalAmount.Text = Strings.Format(curTotalAmount, "#,##0.00");
		}
	}
}
