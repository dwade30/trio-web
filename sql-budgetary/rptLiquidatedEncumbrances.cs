﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptLiquidatedEncumbrances.
	/// </summary>
	public partial class rptLiquidatedEncumbrances : BaseSectionReport
	{
		public static rptLiquidatedEncumbrances InstancePtr
		{
			get
			{
				return (rptLiquidatedEncumbrances)Sys.GetInstance(typeof(rptLiquidatedEncumbrances));
			}
		}

		protected rptLiquidatedEncumbrances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsVendorInfo.Dispose();
				rsEncInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLiquidatedEncumbrances	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsEncInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;

		public rptLiquidatedEncumbrances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Liquidated Encumbrance List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			PageCounter = 0;
			curTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE IsNull(Project, '') <> 'CTRL' AND Liquidated <> 0");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No encumbrance information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsEncInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + rsInfo.Get_Fields_Int32("EncumbranceID"));
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsEncInfo.Get_Fields_Int32("VendorNumber"));
			fldDate.Text = Strings.Format(rsEncInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				fldVendor.Text = Strings.Format(rsEncInfo.Get_Fields_Int32("VendorNumber"), "0000") + " " + rsVendorInfo.Get_Fields_String("CheckName");
			}
			else
			{
				fldVendor.Text = Strings.Format(rsEncInfo.Get_Fields_Int32("VendorNumber"), "0000") + " UNKNOWN";
			}
			fldDescription.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			fldReference.Text = FCConvert.ToString(rsEncInfo.Get_Fields("PO"));
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("Liquidated"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsInfo.Get_Fields_String("Account");
			curTotal += rsInfo.Get_Fields_Decimal("Liquidated");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptLiquidatedEncumbrances_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLiquidatedEncumbrances.Caption	= "Liquidated Encumbrance List";
			//rptLiquidatedEncumbrances.Icon	= "rptLiquidatedEncumbrances.dsx":0000";
			//rptLiquidatedEncumbrances.Left	= 0;
			//rptLiquidatedEncumbrances.Top	= 0;
			//rptLiquidatedEncumbrances.Width	= 11880;
			//rptLiquidatedEncumbrances.Height	= 8595;
			//rptLiquidatedEncumbrances.StartUpPosition	= 3;
			//rptLiquidatedEncumbrances.SectionData	= "rptLiquidatedEncumbrances.dsx":058A;
			//End Unmaped Properties
		}
	}
}
