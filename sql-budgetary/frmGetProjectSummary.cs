﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetProjectSummary.
	/// </summary>
	public partial class frmGetProjectSummary : BaseForm
	{
		public frmGetProjectSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.cmbEditFormat.SelectedIndex = 0;
			this.cmbEditLayout.SelectedIndex = 0;
			this.cmbFormat.SelectedIndex = 0;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetProjectSummary InstancePtr
		{
			get
			{
				return (frmGetProjectSummary)Sys.GetInstance(typeof(frmGetProjectSummary));
			}
		}

		protected frmGetProjectSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			int lngRecord = 0;
			if (cmbFormat.SelectedIndex == 1)
			{
				if (cmbEditLayout.SelectedIndex == 0)
				{
					if (cboLayout.SelectedIndex != -1)
					{
						if (cboLayout.SelectedIndex != 0)
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayout.Text + "' AND Type = 'PS'");
							modBudgetaryMaster.Statics.blnProjectSummaryEdit = true;
							frmProjectSummarySetup.InstancePtr.Show(App.MainForm);
						}
						else
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.Reset();
							modBudgetaryMaster.Statics.blnProjectSummaryEdit = true;
							frmProjectSummarySetup.InstancePtr.Show(App.MainForm);
						}
					}
				}
				else
				{
					if (cboLayoutDelete.SelectedIndex != -1)
					{
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayoutDelete.Text + "' AND Type = 'PS'");
						lngRecord = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM Reports WHERE CriteriaID = " + FCConvert.ToString(lngRecord) + " AND Type = 'PS'");
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
						{
							MessageBox.Show("You can't delete this Selection Criteria because it is used in one or more reports.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						answer = MessageBox.Show("Do you really want to delete this selection criteria?", "Delete Layout?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							// do nothing
						}
						else
						{
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayoutDelete.Text + "' AND Type = 'PS'");
							modBudgetaryAccounting.Statics.SearchResults.Delete();
							modBudgetaryAccounting.Statics.SearchResults.Update();
							FillLayoutCombo();
							cboLayoutDelete.SelectedIndex = -1;
						}
					}
				}
			}
			else
			{
				if (cmbEditFormat.SelectedIndex == 0)
				{
					if (cboFormat.SelectedIndex != -1)
					{
						if (cboFormat.SelectedIndex != 0)
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
							modBudgetaryMaster.Statics.blnProjectSummaryEdit = true;
							frmCustomizeProjectSummary.InstancePtr.Show(App.MainForm);
						}
						else
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.Reset();
							modBudgetaryMaster.Statics.blnProjectSummaryEdit = true;
							frmCustomizeProjectSummary.InstancePtr.Show(App.MainForm);
						}
					}
				}
				else
				{
					if (cboFormatDelete.SelectedIndex != -1)
					{
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE Description = '" + cboFormatDelete.Text + "'");
						lngRecord = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM Reports WHERE FormatID = " + FCConvert.ToString(lngRecord) + " AND Type = 'PS'");
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
						{
							MessageBox.Show("You can't delete this Project Report Format because it is used in one or more reports.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						answer = MessageBox.Show("Do you really want to delete this format?", "Delete Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							// do nothing
						}
						else
						{
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE Description = '" + cboFormatDelete.Text + "'");
							modBudgetaryAccounting.Statics.SearchResults.Delete();
							modBudgetaryAccounting.Statics.SearchResults.Update();
							FillFormatCombo();
							cboFormatDelete.SelectedIndex = -1;
						}
					}
				}
			}
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdFileProcess, new System.EventArgs());
		}

		private void frmGetProjectSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				FillFormatCombo();
				FillLayoutCombo();
				return;
			}
			FillFormatCombo();
			FillLayoutCombo();
			this.Refresh();
		}

		private void frmGetProjectSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetProjectSummary.FillStyle	= 0;
			//frmGetProjectSummary.ScaleWidth	= 9045;
			//frmGetProjectSummary.ScaleHeight	= 7350;
			//frmGetProjectSummary.LinkTopic	= "Form2";
			//frmGetProjectSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetProjectSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optEditFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbEditFormat.SelectedIndex == 0)
			{
				cboFormat.SelectedIndex = -1;
				cboFormat.Visible = true;
				cboFormatDelete.Visible = false;
			}
			else if (this.cmbEditFormat.SelectedIndex == 1)
			{
				cboFormatDelete.SelectedIndex = -1;
				cboFormatDelete.Visible = true;
				cboFormat.Visible = false;
			}
		}

		private void optEditLayout_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbEditLayout.SelectedIndex == 0)
			{
				cboLayout.SelectedIndex = -1;
				cboLayout.Visible = true;
				cboLayoutDelete.Visible = false;
			}
			else if (this.cmbEditLayout.SelectedIndex == 1)
			{
				cboLayoutDelete.SelectedIndex = -1;
				cboLayoutDelete.Visible = true;
				cboLayout.Visible = false;
			}
		}

		private void optFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbFormat.SelectedIndex == 0)
			{
				cboFormat.Enabled = true;
				cmbEditFormat.Enabled = true;
				cboLayout.Enabled = false;
				cmbEditLayout.Enabled = false;
			}
			else if (this.cmbFormat.SelectedIndex == 1)
			{
				cboLayout.Enabled = true;
				cmbEditLayout.Enabled = true;
				cboFormat.Enabled = false;
				cmbEditFormat.Enabled = false;
			}
		}

		private void FillLayoutCombo()
		{
			int counter = 0;
			cboLayout.Clear();
			cboLayoutDelete.Clear();
			cboLayout.AddItem("Create New Layout");
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'PS' ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboLayout.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					cboLayoutDelete.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
		}

		private void FillFormatCombo()
		{
			int counter = 0;
			cboFormat.Clear();
			cboFormatDelete.Clear();
			cboFormat.AddItem("Create New Format");
			rs.OpenRecordset("SELECT * FROM ProjectSummaryFormats ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboFormat.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					cboFormatDelete.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
		}
	}
}
