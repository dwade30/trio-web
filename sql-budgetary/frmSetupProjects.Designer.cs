﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupProjects.
	/// </summary>
	partial class frmSetupProjects : BaseForm
	{
		public fecherFoundation.FCGrid vsProjects;
		public fecherFoundation.FCButton cmdFileAdd;
		public fecherFoundation.FCButton cmdFileDelete;
		public fecherFoundation.FCButton cmdFileSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupProjects));
			this.vsProjects = new fecherFoundation.FCGrid();
			this.cmdFileAdd = new fecherFoundation.FCButton();
			this.cmdFileDelete = new fecherFoundation.FCButton();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsProjects);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileAdd);
			this.TopPanel.Controls.Add(this.cmdFileDelete);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileAdd, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(172, 30);
			this.HeaderText.Text = "Setup Projects";
			// 
			// vsProjects
			// 
			this.vsProjects.AllowSelection = false;
			this.vsProjects.AllowUserToResizeColumns = false;
			this.vsProjects.AllowUserToResizeRows = false;
			this.vsProjects.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsProjects.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsProjects.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsProjects.BackColorBkg = System.Drawing.Color.Empty;
			this.vsProjects.BackColorFixed = System.Drawing.Color.Empty;
			this.vsProjects.BackColorSel = System.Drawing.Color.Empty;
			this.vsProjects.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsProjects.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsProjects.ColumnHeadersHeight = 30;
			this.vsProjects.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsProjects.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsProjects.DragIcon = null;
			this.vsProjects.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsProjects.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsProjects.FixedCols = 0;
			this.vsProjects.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsProjects.FrozenCols = 0;
			this.vsProjects.ExtendLastCol = true;
			this.vsProjects.GridColor = System.Drawing.Color.Empty;
			this.vsProjects.GridColorFixed = System.Drawing.Color.Empty;
			this.vsProjects.Location = new System.Drawing.Point(30, 30);
			this.vsProjects.Name = "vsProjects";
			this.vsProjects.ReadOnly = true;
			this.vsProjects.RowHeadersVisible = false;
			this.vsProjects.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsProjects.RowHeightMin = 0;
			this.vsProjects.Rows = 1;
			this.vsProjects.ScrollTipText = null;
			this.vsProjects.ShowColumnVisibilityMenu = false;
			this.vsProjects.Size = new System.Drawing.Size(1018, 440);
			this.vsProjects.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsProjects.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsProjects.TabIndex = 0;
			this.vsProjects.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsProjects_ValidateEdit);
			this.vsProjects.CurrentCellChanged += new System.EventHandler(this.vsProjects_RowColChange);
			// 
			// cmdFileAdd
			// 
			this.cmdFileAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileAdd.AppearanceKey = "toolbarButton";
			this.cmdFileAdd.Location = new System.Drawing.Point(832, 29);
			this.cmdFileAdd.Name = "cmdFileAdd";
			this.cmdFileAdd.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdFileAdd.Size = new System.Drawing.Size(89, 24);
			this.cmdFileAdd.TabIndex = 1;
			this.cmdFileAdd.Text = "Add Project";
			this.cmdFileAdd.Click += new System.EventHandler(this.mnuFileAdd_Click);
			// 
			// cmdFileDelete
			// 
			this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDelete.AppearanceKey = "toolbarButton";
			this.cmdFileDelete.Location = new System.Drawing.Point(924, 29);
			this.cmdFileDelete.Name = "cmdFileDelete";
			this.cmdFileDelete.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdFileDelete.Size = new System.Drawing.Size(102, 24);
			this.cmdFileDelete.TabIndex = 2;
			this.cmdFileDelete.Text = "Delete Project";
			this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(274, 30);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(120, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmSetupProjects
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupProjects";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Setup Projects";
			this.Load += new System.EventHandler(this.frmSetupProjects_Load);
			this.Activated += new System.EventHandler(this.frmSetupProjects_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupProjects_KeyPress);
			this.Resize += new System.EventHandler(this.frmSetupProjects_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
