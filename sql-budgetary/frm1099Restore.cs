﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frm1099Restore.
	/// </summary>
	public partial class frm1099Restore : BaseForm
	{
		public frm1099Restore()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frm1099Restore InstancePtr
		{
			get
			{
				return (frm1099Restore)Sys.GetInstance(typeof(frm1099Restore));
			}
		}

		protected frm1099Restore _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         08/26/02
		// This form will be used to select and restore archived
		// 1099 information
		// ********************************************************
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdRestore_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (cboYear.SelectedIndex != -1)
			{
				ans = MessageBox.Show("Running this procedure will overwrite any tax information that is currently extracted.  Do you wish to continue?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.No)
				{
					return;
				}
				//! Load frmWait; // shwo the wait form
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Restoring Archived 1099 Data";
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.Show();
				// get all the records for the year requested
				rsArchiveInfo.OpenRecordset("SELECT * FROM VendorTaxInfoArchive WHERE Year = " + cboYear.Text);
				// clear out the information from the 1099 table
				rsInfo.Execute("DELETE FROM VendorTaxInfo", "Budgetary");
				// add all the archive records back in to live table
				rsInfo.OmitNullsOnInsert = true;
				rsInfo.OpenRecordset("SELECT * FROM VendorTaxInfo WHERE ID = 0");
				do
				{
					//Application.DoEvents();
					rsInfo.AddNew();
					rsInfo.Set_Fields("VendorNumber", rsArchiveInfo.Get_Fields_Int32("VendorNumber"));
					rsInfo.Set_Fields("Class", rsArchiveInfo.Get_Fields_String("Class"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					rsInfo.Set_Fields("Amount", rsArchiveInfo.Get_Fields("Amount"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					rsInfo.Set_Fields("Year", rsArchiveInfo.Get_Fields("Year"));
					rsInfo.Update(true);
					rsArchiveInfo.MoveNext();
				}
				while (rsArchiveInfo.EndOfFile() != true);
				frmWait.InstancePtr.Unload();
				this.Hide();
				//Application.DoEvents();
				MessageBox.Show("Restore Completed Successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
		}

		private void frm1099Restore_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboYear.Clear();
			// get all the years that we have 1099 archive information for to fill the combo box
			rsInfo.OpenRecordset("SELECT DISTINCT Year FROM VendorTaxInfoArchive ORDER BY Year");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					cboYear.AddItem(FCConvert.ToString(rsInfo.Get_Fields("Year")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				cboYear.SelectedIndex = 0;
			}
			else
			{
				// if we have no archived data tell the user that and unload the form
				MessageBox.Show("There was no archived data found to restore", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				//MDIParent.InstancePtr.Focus();
				return;
			}
			this.Refresh();
		}

		private void frm1099Restore_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frm1099Restore_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frm1099Restore.FillStyle	= 0;
			//frm1099Restore.ScaleWidth	= 3885;
			//frm1099Restore.ScaleHeight	= 2280;
			//frm1099Restore.LinkTopic	= "Form2";
			//frm1099Restore.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
		}
	}
}
