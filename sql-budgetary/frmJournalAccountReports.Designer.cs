//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournalAccountReports.
	/// </summary>
	partial class frmJournalAccountReports : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJournalAccountReports));
            Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdClear = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 512);
            this.BottomPanel.Size = new System.Drawing.Size(1042, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Size = new System.Drawing.Size(1042, 452);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1042, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(301, 30);
            this.HeaderText.Text = "Journal Account Summary";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(281, 40);
            this.cmbReport.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 540);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(861, 137);
            this.fraWhere.TabIndex = 11;
            this.fraWhere.Text = "Selection Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(19, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.ShowFocusCell = false;
            this.vsWhere.Size = new System.Drawing.Size(822, 85);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cmdAdd);
            this.Frame2.Controls.Add(this.cmbReport);
            this.Frame2.Controls.Add(this.cboSavedReport);
            this.Frame2.Enabled = false;
            this.Frame2.Location = new System.Drawing.Point(670, 197);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(321, 190);
            this.Frame2.TabIndex = 5;
            this.Frame2.Text = "Report";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(281, 40);
            this.cmdAdd.TabIndex = 10;
            this.cmdAdd.Text = "Add Custom Report to Library";
            this.ToolTip1.SetToolTip(this.cmdAdd, null);
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(281, 40);
            this.cboSavedReport.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.cboSavedReport, null);
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Enabled = false;
            this.fraSort.Location = new System.Drawing.Point(350, 197);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(300, 312);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(260, 259);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(382, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(141, 48);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "Print Preview";
            this.ToolTip1.SetToolTip(this.cmdPrint, null);
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 197);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(300, 312);
            this.fraFields.TabIndex = 12;
            this.fraFields.Text = "Fields To Display";
            this.ToolTip1.SetToolTip(this.fraFields, "Double Click to select all");
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(260, 259);
            this.lstFields.Style = 1;
            this.lstFields.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lstFields, null);
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupBoxNoBorders";
            this.Frame3.Controls.Add(this.fraMessage);
            this.Frame3.Controls.Add(this.vsLayout);
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.Image1);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(1039, 190);
            this.Frame3.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // fraMessage
            // 
            this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(78, 72);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(941, 92);
            this.fraMessage.TabIndex = 22;
            this.ToolTip1.SetToolTip(this.fraMessage, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(24, 39);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(593, 16);
            this.Label3.TabIndex = 23;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // vsLayout
            // 
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.Enabled = false;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.Location = new System.Drawing.Point(78, 72);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.ReadOnly = false;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 1;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(941, 92);
            this.vsLayout.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.vsLayout, null);
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(49, 72);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(9, 82);
            this.Label2.TabIndex = 20;
            this.Label2.Text = "M A R G I N";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 72);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(9, 58);
            this.Label1.TabIndex = 19;
            this.Label1.Text = "L E F T";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Image1
            // 
            this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(78, 30);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(941, 22);
            this.Image1.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.Image1, null);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry5});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuFileSeperator2,
            this.mnuFilePrint,
            this.mnuPreview,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 2;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuPreview
            // 
            this.mnuPreview.Index = 3;
            this.mnuPreview.Name = "mnuPreview";
            this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPreview.Text = "Print / Preview ";
            this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(912, 27);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(91, 24);
            this.cmdClear.TabIndex = 22;
            this.cmdClear.Text = "Clear Criteria";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // frmJournalAccountReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1042, 620);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmJournalAccountReports";
            this.Text = "Journal Summary List";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmJournalAccountReports_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmJournalAccountReports_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmJournalAccountReports_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdClear;
	}
}