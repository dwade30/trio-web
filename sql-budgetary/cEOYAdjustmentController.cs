﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class cEOYAdjustmentController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}

		public cGenericCollection GetEOYAdjustments()
		{
			cGenericCollection GetEOYAdjustments = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection retColl = new cGenericCollection();
				cEOYAdjustment ea;
				string strSQL;
				// Dim strStartAcct As String
				// Dim strEndAcct As String
				// Call SetFundBalancesRange(strStartAcct, strEndAcct)
				strSQL = "select * from EOYAdjustments order by id";
				rsLoad.OpenRecordset(strSQL, "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					ea = new cEOYAdjustment();
					ea.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					ea.Account = FCConvert.ToString(rsLoad.Get_Fields("account"));
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					ea.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
					// TODO Get_Fields: Check the table for the column [year] and replace with corresponding Get_Field method
					ea.Year = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("year"))));
					ea.IsUpdated = false;
					retColl.AddItem(ea);
					rsLoad.MoveNext();
				}
				GetEOYAdjustments = retColl;
				return GetEOYAdjustments;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetEOYAdjustments;
		}

		public void DeleteEOYAdjustment(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from EOYAdjustments where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void SaveEOYAdjustment(ref cEOYAdjustment eAdj)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(eAdj == null))
				{
					if (eAdj.IsDeleted)
					{
						DeleteEOYAdjustment(eAdj.ID);
						return;
					}
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from EOYAdjustments where id = " + eAdj.ID, "Budgetary");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						if (eAdj.ID > 0)
						{
							SetError(9999, "End of year adjustment record not found");
							return;
						}
						rsSave.AddNew();
						rsSave.Set_Fields("Account", eAdj.Account);
						rsSave.Set_Fields("Amount", eAdj.Amount);
						rsSave.Set_Fields("Year", eAdj.Year);
						rsSave.Update();
						eAdj.ID = rsSave.Get_Fields_Int32("id");
						eAdj.IsUpdated = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void SaveEOYAdjustments(ref cGenericCollection eAdjusts)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(eAdjusts == null))
				{
					eAdjusts.MoveFirst();
					cEOYAdjustment eAdj;
					while (eAdjusts.IsCurrent())
					{
						eAdj = (cEOYAdjustment)eAdjusts.GetCurrentItem();
						SaveEOYAdjustment(ref eAdj);
						if (HadError)
						{
							return;
						}
						eAdjusts.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
	}
}
