﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEncumbranceUpdate.
	/// </summary>
	public partial class frmEncumbranceUpdate : BaseForm
	{
		public frmEncumbranceUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEncumbranceUpdate InstancePtr
		{
			get
			{
				return (frmEncumbranceUpdate)Sys.GetInstance(typeof(frmEncumbranceUpdate));
			}
		}

		protected frmEncumbranceUpdate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int VendorCol;
		int NameCol;
		int DescriptionCol;
		int AmountCol;
		int ReferenceCol;
		float[] ColPercents = new float[6 + 1];
		float[] vs2ColPercents = new float[5 + 1];
		int AccountCol;
		int NumberCol;
		int DetailCol;
		int DetailAmountCol;
		int ProjectCol;
		int PayableCol;
		int DetailPayableCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool BadAmount;
		bool EditFlag;
		int JournalNumber;
		clsDRWrapper Master = new clsDRWrapper();
		int[] PeriodData = null;
		bool blnJournalLocked;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsOldGrid = new clsGridAccount();
		private clsGridAccount vsOldGrid_AutoInitialized;

		private clsGridAccount vsOldGrid
		{
			get
			{
				if (vsOldGrid_AutoInitialized == null)
				{
					vsOldGrid_AutoInitialized = new clsGridAccount();
				}
				return vsOldGrid_AutoInitialized;
			}
			set
			{
				vsOldGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsNewGrid = new clsGridAccount();
		private clsGridAccount vsNewGrid_AutoInitialized;

		private clsGridAccount vsNewGrid
		{
			get
			{
				if (vsNewGrid_AutoInitialized == null)
				{
					vsNewGrid_AutoInitialized = new clsGridAccount();
				}
				return vsNewGrid_AutoInitialized;
			}
			set
			{
				vsNewGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsBudgetaryPosting clsPostInfo_AutoInitialized;

		clsBudgetaryPosting clsPostInfo
		{
			get
			{
				if (clsPostInfo_AutoInitialized == null)
				{
					clsPostInfo_AutoInitialized = new clsBudgetaryPosting();
				}
				return clsPostInfo_AutoInitialized;
			}
			set
			{
				clsPostInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		clsPostingJournalInfo clsJournalInfo_AutoInitialized;

		clsPostingJournalInfo clsJournalInfo
		{
			get
			{
				if (clsJournalInfo_AutoInitialized == null)
				{
					clsJournalInfo_AutoInitialized = new clsPostingJournalInfo();
				}
				return clsJournalInfo_AutoInitialized;
			}
			set
			{
				clsJournalInfo_AutoInitialized = value;
			}
		}

		private void cmdAccount_Click(object sender, System.EventArgs e)
		{
			rs.OpenRecordset("SELECT APJournalDetail.ID AS lNumber FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE APJournal.EncumbranceRecord = " + vs1.TextMatrix(vs1.Row, NumberCol) + " AND (APJournal.Status = 'W' OR APJournal.Status = 'X') AND APJournalDetail.Account = '" + vs2.TextMatrix(vs2.Row, AccountCol) + "' ORDER BY APJournalDetail.ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				MessageBox.Show("You may not perform this update at this time because there is an Invoice Entry that has been put on a Warrant that uses this account.", "Unable To Update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cmdCancel_Click();
				return;
			}
			lblOldValue.Text = "Old Account";
			lblNewValue.Text = "New Account";
			vsOldValue.TextMatrix(0, 0, vs2.TextMatrix(vs2.Row, AccountCol));
			vsNewAccount.Visible = true;
			txtNewValue.Visible = false;
			vsNewAccount.TextMatrix(0, 0, "");
			fraChange.Visible = true;
			vsNewAccount.Focus();
			fraUpdate.Visible = false;
		}

		private void cmdAmount_Click(object sender, System.EventArgs e)
		{
			lblOldValue.Text = "Old Amount";
			lblNewValue.Text = "New Amount";
			vsOldValue.TextMatrix(0, 0, vs2.TextMatrix(vs2.Row, DetailAmountCol));
			vsNewAccount.Visible = false;
			txtNewValue.Visible = true;
			txtNewValue.Text = "";
			fraChange.Visible = true;
			txtNewValue.Focus();
			fraUpdate.Visible = false;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraUpdate.Visible = true;
			fraChange.Visible = false;
			cmdDeleteUpdate.Focus();
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdDeleteNoUpdate_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: total As double	OnWrite(string)
			decimal total = 0;
			if (vs2.TextMatrix(vs2.Row, DetailPayableCol) != "")
			{
				MessageBox.Show("You may not delete this Encumbrance item because it has a Pending A / P amount", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			answer = MessageBox.Show("Do you really want to Delete this line?", "Delete Account?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if (answer == DialogResult.Cancel)
			{
				cmdReturn_Click();
			}
			else if (answer == DialogResult.No)
			{
				// do nothing
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + vs2.TextMatrix(vs2.Row, 0));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - FCConvert.ToDecimal(vs2.TextMatrix(vs2.Row, DetailAmountCol)));
					rs.Update();
				}
				total = FCConvert.ToDecimal(vs2.TextMatrix(vs2.Row, DetailAmountCol));
				vs2.RemoveItem(vs2.Row);
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + vs1.TextMatrix(vs1.Row, 0));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - total);
					rs.Update();
				}
				if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol)) - total != 0)
				{
					vs1.TextMatrix(vs1.Row, AmountCol, FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol)) - total);
				}
				else
				{
					vs1.RemoveItem(vs1.Row);
				}
				vs1_Collapsed();
				vs2_Collapsed();
				cmdReturn_Click();
				// Dave 5/7/03
				vs1_RowColChange(sender, e);
			}
		}

		private void cmdDeleteUpdate_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: total As double	OnWrite(string)
			double total = 0;
			string AcctType = "";
			string AcctFund = "";
			string AcctDept = "";
			if (vs2.TextMatrix(vs2.Row, DetailPayableCol) != "")
			{
				MessageBox.Show("You may not delete this Encumbrance item because it has a Pending A / P amount", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			answer = MessageBox.Show("Do you really want to Delete this line?", "Delete Account?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			if (answer == DialogResult.Cancel)
			{
				cmdReturn_Click();
			}
			else if (answer == DialogResult.No)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "Enc Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "GJ";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("Type", "G");
				rs.Set_Fields("JournalEntriesDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rs.Set_Fields("RCB", "E");
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("Amount", -1 * Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol)));
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("PO", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Description", vs2.TextMatrix(vs2.Row, DetailCol));
				rs.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Update();
				modBudgetaryMaster.Statics.CurrentGJEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + vs2.TextMatrix(vs2.Row, 0));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - FCConvert.ToDecimal(vs2.TextMatrix(vs2.Row, DetailAmountCol)));
					rs.Update();
				}
				total = FCConvert.ToDouble(vs2.TextMatrix(vs2.Row, DetailAmountCol));
				vs2.RemoveItem(vs2.Row);
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + vs1.TextMatrix(vs1.Row, 0));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - FCConvert.ToDecimal(total));
					rs.Update();
				}
				if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, AmountCol)) - total != 0)
				{
					vs1.TextMatrix(vs1.Row, AmountCol, Conversion.Val(vs1.TextMatrix(vs1.Row, AmountCol)) - total);
				}
				else
				{
					vs1.RemoveItem(vs1.Row);
				}
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(JournalNumber));
				vs1_Collapsed();
				vs2_Collapsed();
				cmdReturn_Click();
				// Dave 5/7/03
				vs1_RowColChange(sender, e);
				if (!clsPostInfo.PostJournals())
				{
					MessageBox.Show("There was a problem posting journal " + FCConvert.ToString(JournalNumber) + ".  Please correct the issues and post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			fraUpdate.Visible = false;
			vs1.Enabled = true;
			vs2.Enabled = true;
			vs2.BackColorSel = SystemColors.Info;
			vs2.Focus();
		}

		public void cmdReturn_Click()
		{
			cmdReturn_Click(cmdReturn, new System.EventArgs());
		}

		private void cmdUpdate_Click(object sender, System.EventArgs e)
		{
			decimal total;
			clsDRWrapper rs2 = new clsDRWrapper();
			clsDRWrapper rs3 = new clsDRWrapper();
			int counter;
			decimal AmountChange = 0;
			string AcctType = "";
			string AcctFund = "";
			string AcctDept = "";
			string AcctType2 = "";
			string AcctFund2 = "";
			string AcctDept2 = "";
			int lngEncumbranceDetail = 0;
			int lngOldEncumbranceDetail = 0;
			if (txtNewValue.Visible == true)
			{
				if (vs2.TextMatrix(vs2.Row, DetailPayableCol) != "")
				{
					if (Conversion.Val(txtNewValue.Text) < Conversion.Val(vs2.TextMatrix(vs2.Row, DetailPayableCol)))
					{
						MessageBox.Show("You cannot change this amount to less then is curently pending in unposted journals", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "Enc Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Set_Fields("Type", "GJ");
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "GJ";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				AmountChange = FCConvert.ToDecimal(txtNewValue.Text) - FCConvert.ToDecimal(vsOldValue.TextMatrix(0, 0));
				rs.OpenRecordset("SELECT * FROM JournalEntries  WHERE ID = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("Type", "G");
				rs.Set_Fields("JournalEntriesDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rs.Set_Fields("RCB", "E");
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("Amount", AmountChange);
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("PO", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Description", vs2.TextMatrix(vs2.Row, DetailCol));
				rs.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Update();
				modBudgetaryMaster.Statics.CurrentGJEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") + AmountChange);
					rs.Update();
				}
				vs2.TextMatrix(vs2.Row, DetailAmountCol, txtNewValue.Text);
				// Dave 5/7/03
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs.Edit();
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") + AmountChange);
					rs.Update();
				}
				vs1.TextMatrix(vs1.Row, AmountCol, FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, AmountCol)) - AmountChange);
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(JournalNumber));
			}
			else
			{
				for (counter = 1; counter <= vs2.Rows - 1; counter++)
				{
					if (vs2.TextMatrix(counter, AccountCol) == vsNewAccount.TextMatrix(0, 0))
					{
						MessageBox.Show("There is another detail item in this Encumbrance with this Account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsNewAccount.TextMatrix(0, 0, "");
						vsNewAccount.EditSelLength = 1;
						return;
					}
				}
				if (vsNewAccount.Visible == true)
				{
					for (counter = 1; counter <= vsNewAccount.TextMatrix(0, 0).Length; counter++)
					{
						if (Strings.Mid(vsNewAccount.TextMatrix(0, 0), counter, 1) == "_")
						{
							MessageBox.Show("You must enter a full account number before you may proceed", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsNewAccount.Focus();
							return;
						}
					}
				}
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					JournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					JournalNumber = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", JournalNumber);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "Enc Adjust for " + Strings.Format(DateTime.Today, "MM/dd/yy"));
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", cboPeriod.Text);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalType = "GJ";
				clsJournalInfo.JournalNumber = JournalNumber;
				clsJournalInfo.Period = FCConvert.ToString(FCConvert.ToInt16(FCConvert.ToDouble(cboPeriod.Text)));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.ClearJournals();
				clsPostInfo.AddJournal(clsJournalInfo);
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0 ");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
				}
				rs.AddNew();
				rs.Set_Fields("Type", "G");
				rs.Set_Fields("JournalEntriesDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("account", vs2.TextMatrix(vs2.Row, AccountCol));
				rs.Set_Fields("RCB", "E");
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("Amount", -1 * Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol)));
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("PO", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Description", vs2.TextMatrix(vs2.Row, DetailCol));
				rs.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Update();
				rs.AddNew();
				rs.Set_Fields("Type", "G");
				rs.Set_Fields("JournalEntriesDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				rs.Set_Fields("JournalNumber", JournalNumber);
				rs.Set_Fields("account", vsNewAccount.TextMatrix(0, 0));
				rs.Set_Fields("RCB", "E");
				rs.Set_Fields("Status", "E");
				rs.Set_Fields("VendorNumber", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol))));
				rs.Set_Fields("PO", vs1.TextMatrix(vs1.Row, ReferenceCol));
				rs.Set_Fields("Amount", FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, DetailAmountCol))));
				rs.Set_Fields("Description", vs2.TextMatrix(vs2.Row, DetailCol));
				rs.Set_Fields("Project", vs2.TextMatrix(vs2.Row, ProjectCol));
				rs.Set_Fields("Period", cboPeriod.Text);
				rs.Update();
				// Dave 5/7/03
				rs2.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs2.TextMatrix(vs2.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					rs2.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = 0");
					rs2.AddNew();
					rs2.Set_Fields("account", vsNewAccount.TextMatrix(0, 0));
					rs2.Set_Fields("Adjustments", vs2.TextMatrix(vs2.Row, DetailAmountCol));
					rs2.Set_Fields("EncumbranceID", rs.Get_Fields_Int32("EncumbranceID"));
					rs2.Set_Fields("Description", rs.Get_Fields_String("Description"));
					rs2.Set_Fields("Project", rs.Get_Fields_String("Project"));
					rs.Edit();
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					rs.Set_Fields("Adjustments", rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields("Amount"));
					lngOldEncumbranceDetail = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					rs.Update();
				}
				vs2.TextMatrix(vs2.Row, AccountCol, vsNewAccount.TextMatrix(0, 0));
				// Dave 5/7/03
				rs2.Update();
				lngEncumbranceDetail = FCConvert.ToInt32(rs2.Get_Fields_Int32("ID"));
				vs2.TextMatrix(vs2.Row, 0, FCConvert.ToString(lngEncumbranceDetail));
				modBudgetaryMaster.Statics.CurrentGJEntry = JournalNumber;
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
				rs.OpenRecordset("SELECT APJournalDetail.ID AS lNumber FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE APJournal.EncumbranceRecord = " + vs1.TextMatrix(vs1.Row, NumberCol) + " AND (APJournal.Status <> 'W' AND APJournal.Status <> 'X' AND APJournal.Status <> 'P') AND APJournalDetail.EncumbranceDetailRecord = " + FCConvert.ToString(lngOldEncumbranceDetail) + " ORDER BY APJournalDetail.ID");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (rs.EndOfFile() != true)
					{
						// TODO Get_Fields: Field [lNumber] not found!! (maybe it is an alias?)
						rs2.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + rs.Get_Fields("lNumber") + " AND Encumbrance > 0");
						if (rs2.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs2.Edit();
							rs2.Set_Fields("account", vsNewAccount.TextMatrix(0, 0));
							rs2.Set_Fields("EncumbranceDetailRecord", lngEncumbranceDetail);
							rs2.Update();
						}
						rs.MoveNext();
					}
				}
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(JournalNumber));
			}
			fraChange.Visible = false;
			vs1.Enabled = true;
			vs2.Enabled = true;
			vs2.BackColorSel = SystemColors.Info;
			vs2.Focus();
			if (!clsPostInfo.PostJournals())
			{
				MessageBox.Show("There was a problem posting journal " + FCConvert.ToString(JournalNumber) + ".  Please correct the issues and post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdUpdate_Click()
		{
			cmdUpdate_Click(cmdUpdate, new System.EventArgs());
		}

		private void frmEncumbranceUpdate_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1_RowColChange(sender, e);
		}

		private void frmEncumbranceUpdate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSOLDVALUE")
			{
				modNewAccountBox.CheckFormKeyDown(vsOldValue, vsOldValue.Row, vsOldValue.Col, KeyCode, Shift, vsOldValue.EditSelStart, vsOldValue.EditText, vsOldValue.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSNEWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsNewAccount, vsNewAccount.Row, vsNewAccount.Col, KeyCode, Shift, vsNewAccount.EditSelStart, vsNewAccount.EditText, vsNewAccount.EditSelLength);
			}
		}

		private void frmEncumbranceUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEncumbranceUpdate.FillStyle	= 0;
			//frmEncumbranceUpdate.ScaleWidth	= 9045;
			//frmEncumbranceUpdate.ScaleHeight	= 7410;
			//frmEncumbranceUpdate.LinkTopic	= "Form2";
			//frmEncumbranceUpdate.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int CurrJournal;
			int counter;
			decimal temp = 0;
			decimal temp2 = 0;
			blnJournalLocked = false;
			JournalNumber = 0;
			NumberCol = 0;
			DetailCol = 1;
			AccountCol = 2;
			ProjectCol = 3;
			DetailAmountCol = 4;
			DetailPayableCol = 5;
			VendorCol = 1;
			NameCol = 2;
			DescriptionCol = 3;
			ReferenceCol = 4;
			AmountCol = 5;
			PayableCol = 6;
			vs1.TextMatrix(0, VendorCol, "Vdr #");
			vs1.TextMatrix(0, NameCol, "Vendor Name");
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, ReferenceCol, "PO");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, PayableCol, "Pending A / P");
			vs2.TextMatrix(0, DetailCol, "Description");
			vs2.TextMatrix(0, AccountCol, "Account");
			vs2.TextMatrix(0, DetailAmountCol, "Amount");
			vs2.TextMatrix(0, ProjectCol, "Project");
			vs2.TextMatrix(0, DetailPayableCol, "Pending A / P");
			//FC:FINAL:DDU:#2897 - aligned columns
			vs1.ColAlignment(VendorCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(PayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs2.ColAlignment(DetailCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(DetailAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs2.ColAlignment(DetailPayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ForeColorSel = Color.Black;
			vs2.ForeColorSel = Color.Black;
			ColPercents[0] = 0;
			ColPercents[1] = 0.07057221f;
			ColPercents[2] = 0.3002835f;
			ColPercents[3] = 0.2152247f;
			ColPercents[4] = 0.1356311f;
			ColPercents[5] = 0.1288771f;
			ColPercents[6] = 0.1288771f;
			vs2ColPercents[0] = 0;
			vs2ColPercents[1] = 0.2791759f;
			vs2ColPercents[2] = 0.2882659f;
			vs2ColPercents[3] = 0.08494805f;
			vs2ColPercents[4] = 0.1691473f;
			vs2ColPercents[5] = 0.1691473f;
			counter = 0;
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P'");
			if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveLast();
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				PeriodData = new int[modBudgetaryAccounting.Statics.SearchResults.RecordCount() + 1];
				vs1.ColFormat(AmountCol, "#,##0.00");
				vs1.ColFormat(PayableCol, "#,##0.00");
				vs2.ColFormat(DetailAmountCol, "#,##0.00");
				vs2.ColFormat(DetailPayableCol, "#,##0.00");
				while (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
				{
					if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Adjustments"))
					{
						temp = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Adjustments");
					}
					else
					{
						temp = 0;
					}
					if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Liquidated"))
					{
						temp2 = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Liquidated");
					}
					else
					{
						temp2 = 0;
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount") + temp - temp2 != 0)
					{
						vs1.Rows += 1;
						//FC:FINAL:BBE:#528 - remove colors
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, PayableCol, 0x80000017);
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, PayableCol, 0x80000018);
						vs1.TextMatrix(vs1.Rows - 1, VendorCol, modValidateAccount.GetFormat_6(Conversion.Str(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")), 5));
						if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")) == 0)
						{
							vs1.TextMatrix(vs1.Rows - 1, NameCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorName")));
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
							vs1.TextMatrix(vs1.Rows - 1, NameCol, FCConvert.ToString(rs.Get_Fields_String("CheckName")));
						}
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
						vs1.TextMatrix(vs1.Rows - 1, ReferenceCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO")));
						if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Adjustments"))
						{
							temp = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Adjustments");
						}
						else
						{
							temp = 0;
						}
						if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Liquidated"))
						{
							temp2 = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Decimal("Liquidated");
						}
						else
						{
							temp2 = 0;
						}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount") + temp - temp2));
						vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")));
						CalculatePending();
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						PeriodData[counter] = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"));
						counter += 1;
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					}
				}
				//FC:FINAL:DDU:#2897 - aligned columns
				//FC:FINAL:BBE:#528 - font size used in original for samll row height
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, VendorCol, vs1.Rows - 1, PayableCol, 8);
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, VendorCol, 0, PayableCol, 4);
				//if (vs1.Rows > 1)
				//{
				//	vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, ReferenceCol, vs1.Rows - 1, ReferenceCol, 1);
				//}
				////FC:FINAL:BBE:#528 - font size used in original for samll row height
				////vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.Rows - 1, DetailPayableCol, 8);
				//vs2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DetailCol, 0, DetailPayableCol, 4);
				vs1.ColAlignment(VendorCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs1.ColAlignment(PayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs2.ColAlignment(DetailCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs2.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs2.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs2.ColAlignment(DetailAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs2.ColAlignment(DetailPayableCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs1_RowColChange(sender, e);
				// cboPeriod.ListIndex = PeriodData(0) - 1
				cboPeriod.SelectedIndex = DateTime.Today.Month - 1;
			}
			else
			{
				cboPeriod.SelectedIndex = DateTime.Today.Month - 1;
			}
			vsOldGrid.GRID7Light = vsOldValue;
			vsOldGrid.DefaultAccountType = "E";
			vsOldGrid.AccountCol = -1;
			vsNewGrid.GRID7Light = vsNewAccount;
			vsNewGrid.DefaultAccountType = "E";
			vsNewGrid.AccountCol = -1;
			vs1_Collapsed();
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomColors();
			clsPostInfo = new clsBudgetaryPosting();
			clsPostInfo.AllowPreview = true;
		}

		private void frmEncumbranceUpdate_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 6; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * ColPercents[counter]));
			}
			for (counter = 0; counter <= 5; counter++)
			{
				vs2.ColWidth(counter, FCConvert.ToInt32(vs2.WidthOriginal * vs2ColPercents[counter]));
			}
			fraUpdate.CenterToContainer(this.ClientArea);
			fraChange.CenterToContainer(this.ClientArea);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			if (JournalNumber != 0)
			{
				MessageBox.Show("Adjusting entries in GJ Journal " + FCConvert.ToString(JournalNumber) + ".  Post the journal as soon as possible.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void frmEncumbranceUpdate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				if (fraChange.Visible == true)
				{
					KeyAscii = (Keys)0;
					cmdCancel_Click();
				}
				else if (fraUpdate.Visible == true)
				{
					KeyAscii = (Keys)0;
					cmdReturn_Click();
				}
				else
				{
					KeyAscii = (Keys)0;
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmEncumbranceUpdate.InstancePtr.ActiveControl.GetName() == "vs1")
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsNewAccount_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				vsNewAccount_Validating_2(false);
				if (!BadAmount)
				{
					cmdUpdate_Click();
				}
			}
		}

		private void vsNewAccount_Validating_2(bool Cancel)
		{
			vsNewAccount_Validating(vsNewAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vsNewAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsNewAccount.TextMatrix(0, 0).Length; counter++)
			{
				if (Strings.Mid(vsNewAccount.TextMatrix(0, 0), counter, 1) == "_")
				{
					BadAmount = true;
					return;
				}
			}
			if (modValidateAccount.AccountValidate(vsNewAccount.TextMatrix(0, 0)))
			{
				BadAmount = false;
			}
			else
			{
				MessageBox.Show("This is not a valid account", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				vsNewAccount.TextMatrix(0, 0, "");
				vsNewAccount.EditSelLength = 1;
				BadAmount = true;
			}
		}

		private void txtNewValue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtNewValue_Validating_2(false);
				if (!BadAmount)
				{
					cmdUpdate_Click();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtNewValue_Validating_2(bool Cancel)
		{
			txtNewValue_Validating(txtNewValue, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtNewValue_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int temp = 0;
			if ((!Information.IsNumeric(txtNewValue.Text) && txtNewValue.Text != "") || Information.IsNumeric(txtNewValue.Text) && Conversion.Val(txtNewValue.Text) < 0)
			{
				MessageBox.Show("You must enter a number greater then 0 in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtNewValue.Text = "";
				BadAmount = true;
			}
			else if (txtNewValue.Text == "")
			{
				txtNewValue.Text = "0";
				BadAmount = false;
			}
			else
			{
				temp = Strings.InStr(txtNewValue.Text, ".", CompareConstants.vbBinaryCompare);
				if (temp > 0)
				{
					if (txtNewValue.Text.Length > temp + 2)
					{
						txtNewValue.Text = Strings.Left(txtNewValue.Text, temp + 2);
					}
				}
				if (vs2.TextMatrix(vs2.Row, DetailPayableCol) != "")
				{
					if (fecherFoundation.Strings.CompareString(txtNewValue.Text, vs2.TextMatrix(vs2.Row, DetailPayableCol), true) < 0)
					{
						MessageBox.Show("You may not change the amount to less than what is shown for a Pending A / P amount", "Unable to Change", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						BadAmount = true;
						return;
					}
				}
				BadAmount = false;
			}
		}

		public void txtNewValue_Validate(ref bool Cancel)
		{
			txtNewValue_Validating(txtNewValue, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vs1_AfterSort(object sender, EventArgs e)
		{
			vs1_RowColChange(sender, e);
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int height;
			height = vs1.Rows * vs1.RowHeight(0);
			if (height < frmEncumbranceUpdate.InstancePtr.Height * 0.4)
			{
				if (vs1.Height != height)
				{
					//vs1.Height = height + 75;
				}
			}
			else
			{
				if (vs1.Height < frmEncumbranceUpdate.InstancePtr.Height * 0.4 + 75)
				{
					//vs1.Height = FCConvert.ToInt32(frmEncumbranceUpdate.InstancePtr.Height * 0.4 + 75);
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			vs1.BackColorSel = SystemColors.Info;
			vs2.BackColorSel = SystemColors.Control;
			// &H8000000F&
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			double temp = 0;
			double temp2 = 0;
			clsDRWrapper rsAdjustments = new clsDRWrapper();
			vs2.Rows = 1;
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				// cboPeriod.ListIndex = PeriodData(vs1.Row - 1) - 1
				while (rs.EndOfFile() != true)
				{
					if (!rs.IsFieldNull("Adjustments"))
					{
						temp = FCConvert.ToDouble(rs.Get_Fields_Decimal("Adjustments"));
					}
					else
					{
						temp = 0;
					}
					if (!rs.IsFieldNull("Liquidated"))
					{
						temp2 = FCConvert.ToDouble(rs.Get_Fields_Decimal("Liquidated"));
					}
					else
					{
						temp2 = 0;
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (FCConvert.ToDouble(rs.Get_Fields("Amount")) + temp - temp2 != 0)
					{
						vs2.Rows += 1;
						vs2.TextMatrix(vs2.Rows - 1, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						vs2.TextMatrix(vs2.Rows - 1, DetailCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, AccountCol, FCConvert.ToString(rs.Get_Fields("account")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, DetailAmountCol, FCConvert.ToString(FCConvert.ToDouble(rs.Get_Fields("Amount")) + temp - temp2));
						if (modBudgetaryMaster.Statics.ProjectFlag)
						{
							vs2.TextMatrix(vs2.Rows - 1, ProjectCol, FCConvert.ToString(rs.Get_Fields_String("Project")));
						}
						CalculateDetailPending();
						rs.MoveNext();
					}
					else
					{
						rs.MoveNext();
					}
				}
				rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				vs1.TextMatrix(vs1.Row, AmountCol, FCConvert.ToString(FCConvert.ToDouble(rs.Get_Fields("Amount")) + FCConvert.ToDouble(rs.Get_Fields_Decimal("Adjustments")) - FCConvert.ToDouble(rs.Get_Fields_Decimal("Liquidated"))));
			}
			// vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.rows - 1, DetailPayableCol) = 8
			vs2_Collapsed();
			rsAdjustments.OpenRecordset("SELECT * FROM JournalEntries WHERE Status <> 'P' AND RCB = 'E' AND PO = '" + vs1.TextMatrix(vs1.Row, ReferenceCol) + "'");
			if (rsAdjustments.EndOfFile() != true && rsAdjustments.BeginningOfFile() != true)
			{
				MessageBox.Show("Encumbrance Information is not accurate.  There are one or more Encumbrance Adjustment Journal Entries that have not been posted yet for this encumbrance.", "Encumbrance Information Not Current", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void vs2_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs2_Collapsed();
		}

		private void vs2_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs2_Collapsed();
		}

		private void vs2_Collapsed()
		{
			if (vs2.Rows >= 8)
			{
				//vs2.Height = 8 * vs2.RowHeight(0) + 75;
			}
			else
			{
				//vs2.Height = vs2.Rows * vs2.RowHeight(0) + 75;
			}
		}

		private void vs2_DblClick(object sender, System.EventArgs e)
		{
			if (vs2.Rows > 1 && vs2.MouseRow > 0)
			{
				vs2.BackColorSel = SystemColors.Control;
				fraUpdate.Visible = true;
				cmdDeleteUpdate.Focus();
				vs1.Enabled = false;
				vs2.Enabled = false;
			}
		}

		private void vs2_Enter(object sender, System.EventArgs e)
		{
			vs2.BackColorSel = SystemColors.Info;
			vs1.BackColorSel = SystemColors.Control;
		}

		private void vs2_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				vs2.BackColorSel = SystemColors.Control;
				fraUpdate.Visible = true;
				cmdDeleteUpdate.Focus();
				vs1.Enabled = false;
				vs2.Enabled = false;
			}
		}

		private decimal CalculateTotal()
		{
			decimal CalculateTotal = 0;
			int counter;
			CalculateTotal = 0;
			for (counter = 1; counter <= vs2.Rows - 1; counter++)
			{
				CalculateTotal += FCConvert.ToDecimal(vs2.TextMatrix(counter, DetailAmountCol));
			}
			return CalculateTotal;
		}

		private void CalculatePending()
		{
			decimal total = 0;
			int counter;
			rs.OpenRecordset("SELECT APJournalDetail.Account AS Account, APJournalDetail.Encumbrance AS Encumbrance FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE APJournalDetail.Encumbrance > 0 AND APJournal.EncumbranceRecord = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " AND APJournal.Status <> 'P' ORDER BY APJournalDetail.ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				total = 0;
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					total += rs.Get_Fields_Decimal("Encumbrance");
					rs.MoveNext();
				}
				vs1.TextMatrix(vs1.Rows - 1, PayableCol, FCConvert.ToString(total));
			}
		}

		private void CalculateDetailPending()
		{
			int counter;
			clsDRWrapper rs2 = new clsDRWrapper();
			rs2.OpenRecordset("SELECT APJournalDetail.Description AS Description, APJournalDetail.Account AS Account, APJournalDetail.Encumbrance AS Encumbrance FROM ((APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) INNER JOIN Encumbrances ON APJournal.EncumbranceRecord = Encumbrances.ID) WHERE APJournalDetail.Encumbrance > 0 AND APJournal.Reference = '" + vs1.TextMatrix(vs1.Row, ReferenceCol) + "' AND APJournal.Status <> 'P' ORDER BY APJournalDetail.ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				rs2.MoveLast();
				rs2.MoveFirst();
				for (counter = 1; counter <= rs2.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rs2.Get_Fields_String("Description")) == vs2.TextMatrix(vs2.Rows - 1, DetailCol) && FCConvert.ToString(rs2.Get_Fields("account")) == vs2.TextMatrix(vs2.Rows - 1, AccountCol))
					{
						vs2.TextMatrix(vs2.Rows - 1, DetailPayableCol, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
						break;
					}
					rs2.MoveNext();
				}
			}
		}

		private void SetCustomColors()
		{
			lblInstruct.ForeColor = Color.Red;
		}
	}
}
