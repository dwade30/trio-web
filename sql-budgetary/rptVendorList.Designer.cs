﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptVendorList.
	/// </summary>
	partial class rptVendorList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVendorList));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldInsuranceVerifiedDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblInsuranceVerifiedDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldContact = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldClassCode1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjustment1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClassCode2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjustment2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClassCode3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjustment3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClassCode4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjustment4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldClassCode5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAdjustment5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.linDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rtfDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
            this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.linMessage = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rtfMessage = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
            this.fldPhoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPhoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFaxNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFaxNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInsuranceRequired = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInsuranceRequired = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldInsuranceVerifiedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsuranceVerifiedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataEntryMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFaxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaxNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInsuranceRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsuranceRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldInsuranceVerifiedDate,
            this.lblInsuranceVerifiedDate,
            this.fldVendor,
            this.fldName,
            this.fldContact,
            this.fldStatus,
            this.fldClass,
            this.fldTax,
            this.fldYTDAmount,
            this.fldAddress1,
            this.fldAddress2,
            this.fldAddress3,
            this.fldAddress4,
            this.fldTaxID,
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Line2,
            this.fldClassCode1,
            this.fldAdjustment1,
            this.fldClassCode2,
            this.fldAdjustment2,
            this.fldClassCode3,
            this.fldAdjustment3,
            this.fldClassCode4,
            this.fldAdjustment4,
            this.fldClassCode5,
            this.fldAdjustment5,
            this.lblDataEntryMessage,
            this.linDataEntryMessage,
            this.rtfDataEntryMessage,
            this.lblMessage,
            this.linMessage,
            this.rtfMessage,
            this.fldPhoneNumber,
            this.lblPhoneNumber,
            this.fldFaxNumber,
            this.lblFaxNumber,
            this.fldInsuranceRequired});
            this.Detail.Height = 1.94F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldInsuranceVerifiedDate
            // 
            this.fldInsuranceVerifiedDate.CanShrink = true;
            this.fldInsuranceVerifiedDate.Height = 0.1875F;
            this.fldInsuranceVerifiedDate.Left = 5.96875F;
            this.fldInsuranceVerifiedDate.Name = "fldInsuranceVerifiedDate";
            this.fldInsuranceVerifiedDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldInsuranceVerifiedDate.Text = "Field1";
            this.fldInsuranceVerifiedDate.Top = 0.90625F;
            this.fldInsuranceVerifiedDate.Width = 0.875F;
            // 
            // lblInsuranceVerifiedDate
            // 
            this.lblInsuranceVerifiedDate.CanShrink = true;
            this.lblInsuranceVerifiedDate.Height = 0.1875F;
            this.lblInsuranceVerifiedDate.Left = 4.75F;
            this.lblInsuranceVerifiedDate.Name = "lblInsuranceVerifiedDate";
            this.lblInsuranceVerifiedDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.lblInsuranceVerifiedDate.Text = "Ins. Verified Date:";
            this.lblInsuranceVerifiedDate.Top = 0.90625F;
            this.lblInsuranceVerifiedDate.Width = 1.1875F;
            // 
            // fldVendor
            // 
            this.fldVendor.CanShrink = true;
            this.fldVendor.Height = 0.1875F;
            this.fldVendor.Left = 0.03125F;
            this.fldVendor.Name = "fldVendor";
            this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldVendor.Text = "Field1";
            this.fldVendor.Top = 0.125F;
            this.fldVendor.Width = 0.375F;
            // 
            // fldName
            // 
            this.fldName.CanShrink = true;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.46875F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldName.Text = "Field2";
            this.fldName.Top = 0.125F;
            this.fldName.Width = 2.53125F;
            // 
            // fldContact
            // 
            this.fldContact.CanShrink = true;
            this.fldContact.Height = 0.1875F;
            this.fldContact.Left = 4F;
            this.fldContact.Name = "fldContact";
            this.fldContact.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldContact.Text = "Field3";
            this.fldContact.Top = 0.125F;
            this.fldContact.Width = 1.65625F;
            // 
            // fldStatus
            // 
            this.fldStatus.CanShrink = true;
            this.fldStatus.Height = 0.1875F;
            this.fldStatus.Left = 5.71875F;
            this.fldStatus.Name = "fldStatus";
            this.fldStatus.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.fldStatus.Text = "Field4";
            this.fldStatus.Top = 0.125F;
            this.fldStatus.Width = 0.46875F;
            // 
            // fldClass
            // 
            this.fldClass.CanShrink = true;
            this.fldClass.Height = 0.1875F;
            this.fldClass.Left = 6.21875F;
            this.fldClass.Name = "fldClass";
            this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClass.Text = "Field5";
            this.fldClass.Top = 0.125F;
            this.fldClass.Width = 0.84375F;
            // 
            // fldTax
            // 
            this.fldTax.CanShrink = true;
            this.fldTax.Height = 0.1875F;
            this.fldTax.Left = 7.0625F;
            this.fldTax.Name = "fldTax";
            this.fldTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.fldTax.Text = "Field6";
            this.fldTax.Top = 0.125F;
            this.fldTax.Width = 0.375F;
            // 
            // fldYTDAmount
            // 
            this.fldYTDAmount.CanShrink = true;
            this.fldYTDAmount.Height = 0.1875F;
            this.fldYTDAmount.Left = 3.0625F;
            this.fldYTDAmount.Name = "fldYTDAmount";
            this.fldYTDAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldYTDAmount.Text = "Field1";
            this.fldYTDAmount.Top = 0.125F;
            this.fldYTDAmount.Width = 0.90625F;
            // 
            // fldAddress1
            // 
            this.fldAddress1.CanShrink = true;
            this.fldAddress1.Height = 0.1875F;
            this.fldAddress1.Left = 0.46875F;
            this.fldAddress1.Name = "fldAddress1";
            this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldAddress1.Text = "Field1";
            this.fldAddress1.Top = 0.3125F;
            this.fldAddress1.Width = 2.53125F;
            // 
            // fldAddress2
            // 
            this.fldAddress2.CanShrink = true;
            this.fldAddress2.Height = 0.1875F;
            this.fldAddress2.Left = 0.46875F;
            this.fldAddress2.Name = "fldAddress2";
            this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldAddress2.Text = "Field1";
            this.fldAddress2.Top = 0.5F;
            this.fldAddress2.Width = 2.53125F;
            // 
            // fldAddress3
            // 
            this.fldAddress3.CanShrink = true;
            this.fldAddress3.Height = 0.1875F;
            this.fldAddress3.Left = 0.46875F;
            this.fldAddress3.Name = "fldAddress3";
            this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldAddress3.Text = "Field1";
            this.fldAddress3.Top = 0.6875F;
            this.fldAddress3.Width = 2.53125F;
            // 
            // fldAddress4
            // 
            this.fldAddress4.CanShrink = true;
            this.fldAddress4.Height = 0.1875F;
            this.fldAddress4.Left = 0.46875F;
            this.fldAddress4.Name = "fldAddress4";
            this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldAddress4.Text = "Field1";
            this.fldAddress4.Top = 0.875F;
            this.fldAddress4.Width = 2.53125F;
            // 
            // fldTaxID
            // 
            this.fldTaxID.CanShrink = true;
            this.fldTaxID.Height = 0.1875F;
            this.fldTaxID.Left = 6.21875F;
            this.fldTaxID.Name = "fldTaxID";
            this.fldTaxID.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldTaxID.Text = "Field1";
            this.fldTaxID.Top = 0.3125F;
            this.fldTaxID.Width = 1.09375F;
            // 
            // Field1
            // 
            this.Field1.CanShrink = true;
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 5.875F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field1.Text = "Tax:";
            this.Field1.Top = 0.3125F;
            this.Field1.Width = 0.34375F;
            // 
            // Field2
            // 
            this.Field2.CanShrink = true;
            this.Field2.Height = 0.1875F;
            this.Field2.Left = 3.4375F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field2.Text = "Adjustments";
            this.Field2.Top = 0.375F;
            this.Field2.Width = 0.84375F;
            // 
            // Field3
            // 
            this.Field3.CanShrink = true;
            this.Field3.Height = 0.1875F;
            this.Field3.Left = 3.09375F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field3.Text = "Class Code";
            this.Field3.Top = 0.5625F;
            this.Field3.Width = 0.6875F;
            // 
            // Field4
            // 
            this.Field4.CanShrink = true;
            this.Field4.Height = 0.1875F;
            this.Field4.Left = 3.875F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field4.Text = "Amount";
            this.Field4.Top = 0.5625F;
            this.Field4.Width = 0.6875F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 3.125F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.75F;
            this.Line2.Width = 1.4375F;
            this.Line2.X1 = 3.125F;
            this.Line2.X2 = 4.5625F;
            this.Line2.Y1 = 0.75F;
            this.Line2.Y2 = 0.75F;
            // 
            // fldClassCode1
            // 
            this.fldClassCode1.CanShrink = true;
            this.fldClassCode1.Height = 0.1875F;
            this.fldClassCode1.Left = 3.09375F;
            this.fldClassCode1.Name = "fldClassCode1";
            this.fldClassCode1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClassCode1.Text = "Class Code";
            this.fldClassCode1.Top = 0.78125F;
            this.fldClassCode1.Width = 0.6875F;
            // 
            // fldAdjustment1
            // 
            this.fldAdjustment1.CanShrink = true;
            this.fldAdjustment1.Height = 0.1875F;
            this.fldAdjustment1.Left = 3.875F;
            this.fldAdjustment1.Name = "fldAdjustment1";
            this.fldAdjustment1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAdjustment1.Text = "Amount";
            this.fldAdjustment1.Top = 0.78125F;
            this.fldAdjustment1.Width = 0.6875F;
            // 
            // fldClassCode2
            // 
            this.fldClassCode2.CanShrink = true;
            this.fldClassCode2.Height = 0.1875F;
            this.fldClassCode2.Left = 3.09375F;
            this.fldClassCode2.Name = "fldClassCode2";
            this.fldClassCode2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClassCode2.Text = "Class Code";
            this.fldClassCode2.Top = 0.96875F;
            this.fldClassCode2.Width = 0.6875F;
            // 
            // fldAdjustment2
            // 
            this.fldAdjustment2.CanShrink = true;
            this.fldAdjustment2.Height = 0.1875F;
            this.fldAdjustment2.Left = 3.875F;
            this.fldAdjustment2.Name = "fldAdjustment2";
            this.fldAdjustment2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAdjustment2.Text = "Amount";
            this.fldAdjustment2.Top = 0.96875F;
            this.fldAdjustment2.Width = 0.6875F;
            // 
            // fldClassCode3
            // 
            this.fldClassCode3.CanShrink = true;
            this.fldClassCode3.Height = 0.1875F;
            this.fldClassCode3.Left = 3.09375F;
            this.fldClassCode3.Name = "fldClassCode3";
            this.fldClassCode3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClassCode3.Text = "Class Code";
            this.fldClassCode3.Top = 1.15625F;
            this.fldClassCode3.Width = 0.6875F;
            // 
            // fldAdjustment3
            // 
            this.fldAdjustment3.CanShrink = true;
            this.fldAdjustment3.Height = 0.1875F;
            this.fldAdjustment3.Left = 3.875F;
            this.fldAdjustment3.Name = "fldAdjustment3";
            this.fldAdjustment3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAdjustment3.Text = "Amount";
            this.fldAdjustment3.Top = 1.15625F;
            this.fldAdjustment3.Width = 0.6875F;
            // 
            // fldClassCode4
            // 
            this.fldClassCode4.CanShrink = true;
            this.fldClassCode4.Height = 0.1875F;
            this.fldClassCode4.Left = 3.09375F;
            this.fldClassCode4.Name = "fldClassCode4";
            this.fldClassCode4.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClassCode4.Text = "Class Code";
            this.fldClassCode4.Top = 1.34375F;
            this.fldClassCode4.Width = 0.6875F;
            // 
            // fldAdjustment4
            // 
            this.fldAdjustment4.CanShrink = true;
            this.fldAdjustment4.Height = 0.1875F;
            this.fldAdjustment4.Left = 3.875F;
            this.fldAdjustment4.Name = "fldAdjustment4";
            this.fldAdjustment4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAdjustment4.Text = "Amount";
            this.fldAdjustment4.Top = 1.34375F;
            this.fldAdjustment4.Width = 0.6875F;
            // 
            // fldClassCode5
            // 
            this.fldClassCode5.CanShrink = true;
            this.fldClassCode5.Height = 0.1875F;
            this.fldClassCode5.Left = 3.09375F;
            this.fldClassCode5.Name = "fldClassCode5";
            this.fldClassCode5.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldClassCode5.Text = "Class Code";
            this.fldClassCode5.Top = 1.53125F;
            this.fldClassCode5.Width = 0.6875F;
            // 
            // fldAdjustment5
            // 
            this.fldAdjustment5.CanShrink = true;
            this.fldAdjustment5.Height = 0.1875F;
            this.fldAdjustment5.Left = 3.875F;
            this.fldAdjustment5.Name = "fldAdjustment5";
            this.fldAdjustment5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldAdjustment5.Text = "Amount";
            this.fldAdjustment5.Top = 1.53125F;
            this.fldAdjustment5.Width = 0.6875F;
            // 
            // lblDataEntryMessage
            // 
            this.lblDataEntryMessage.CanShrink = true;
            this.lblDataEntryMessage.Height = 0.19F;
            this.lblDataEntryMessage.Left = 0.1875F;
            this.lblDataEntryMessage.Name = "lblDataEntryMessage";
            this.lblDataEntryMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.lblDataEntryMessage.Text = "Data Entry Message";
            this.lblDataEntryMessage.Top = 1.15625F;
            this.lblDataEntryMessage.Width = 1.15625F;
            // 
            // linDataEntryMessage
            // 
            this.linDataEntryMessage.Height = 0F;
            this.linDataEntryMessage.Left = 0.1875F;
            this.linDataEntryMessage.LineWeight = 1F;
            this.linDataEntryMessage.Name = "linDataEntryMessage";
            this.linDataEntryMessage.Top = 1.3125F;
            this.linDataEntryMessage.Width = 1.15625F;
            this.linDataEntryMessage.X1 = 0.1875F;
            this.linDataEntryMessage.X2 = 1.34375F;
            this.linDataEntryMessage.Y1 = 1.3125F;
            this.linDataEntryMessage.Y2 = 1.3125F;
            // 
            // rtfDataEntryMessage
            // 
            this.rtfDataEntryMessage.AutoReplaceFields = true;
            this.rtfDataEntryMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.rtfDataEntryMessage.Height = 0.1875F;
            this.rtfDataEntryMessage.Left = 0.21875F;
            this.rtfDataEntryMessage.Name = "rtfDataEntryMessage";
            this.rtfDataEntryMessage.RTF = resources.GetString("rtfDataEntryMessage.RTF");
            this.rtfDataEntryMessage.Top = 1.34375F;
            this.rtfDataEntryMessage.Width = 2.78125F;
            // 
            // lblMessage
            // 
            this.lblMessage.CanShrink = true;
            this.lblMessage.Height = 0.19F;
            this.lblMessage.Left = 4.65625F;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.lblMessage.Text = "Vendor Message";
            this.lblMessage.Top = 1.15625F;
            this.lblMessage.Width = 1.15625F;
            // 
            // linMessage
            // 
            this.linMessage.Height = 0F;
            this.linMessage.Left = 4.65625F;
            this.linMessage.LineWeight = 1F;
            this.linMessage.Name = "linMessage";
            this.linMessage.Top = 1.3125F;
            this.linMessage.Width = 1.15625F;
            this.linMessage.X1 = 4.65625F;
            this.linMessage.X2 = 5.8125F;
            this.linMessage.Y1 = 1.3125F;
            this.linMessage.Y2 = 1.3125F;
            // 
            // rtfMessage
            // 
            this.rtfMessage.AutoReplaceFields = true;
            this.rtfMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.rtfMessage.Height = 0.1875F;
            this.rtfMessage.Left = 4.6875F;
            this.rtfMessage.Name = "rtfMessage";
            this.rtfMessage.RTF = resources.GetString("rtfMessage.RTF");
            this.rtfMessage.Top = 1.34375F;
            this.rtfMessage.Width = 2.78125F;
            // 
            // fldPhoneNumber
            // 
            this.fldPhoneNumber.CanShrink = true;
            this.fldPhoneNumber.Height = 0.1875F;
            this.fldPhoneNumber.Left = 5.96875F;
            this.fldPhoneNumber.Name = "fldPhoneNumber";
            this.fldPhoneNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldPhoneNumber.Text = "Field1";
            this.fldPhoneNumber.Top = 0.53125F;
            this.fldPhoneNumber.Width = 1.375F;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.CanShrink = true;
            this.lblPhoneNumber.Height = 0.1875F;
            this.lblPhoneNumber.Left = 5.3125F;
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.lblPhoneNumber.Text = "Phone #:";
            this.lblPhoneNumber.Top = 0.53125F;
            this.lblPhoneNumber.Width = 0.625F;
            // 
            // fldFaxNumber
            // 
            this.fldFaxNumber.CanShrink = true;
            this.fldFaxNumber.Height = 0.1875F;
            this.fldFaxNumber.Left = 5.96875F;
            this.fldFaxNumber.Name = "fldFaxNumber";
            this.fldFaxNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.fldFaxNumber.Text = "Field1";
            this.fldFaxNumber.Top = 0.71875F;
            this.fldFaxNumber.Width = 1.375F;
            // 
            // lblFaxNumber
            // 
            this.lblFaxNumber.CanShrink = true;
            this.lblFaxNumber.Height = 0.1875F;
            this.lblFaxNumber.Left = 5.3125F;
            this.lblFaxNumber.Name = "lblFaxNumber";
            this.lblFaxNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.lblFaxNumber.Text = "Fax #:";
            this.lblFaxNumber.Top = 0.71875F;
            this.lblFaxNumber.Width = 0.625F;
            // 
            // fldInsuranceRequired
            // 
            this.fldInsuranceRequired.CanShrink = true;
            this.fldInsuranceRequired.Height = 0.1875F;
            this.fldInsuranceRequired.Left = 7.5F;
            this.fldInsuranceRequired.Name = "fldInsuranceRequired";
            this.fldInsuranceRequired.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.fldInsuranceRequired.Text = "Field6";
            this.fldInsuranceRequired.Top = 0.125F;
            this.fldInsuranceRequired.Width = 0.5F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Line1,
            this.Label14,
            this.lblInsuranceRequired});
            this.PageHeader.Height = 0.8F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.375F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.5F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label1.Text = "Vendor List";
            this.Label1.Top = 0F;
            this.Label1.Width = 4.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label7.Text = "Label7";
            this.Label7.Top = 0.1875F;
            this.Label7.Width = 1.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label2.Text = "Label2";
            this.Label2.Top = 0F;
            this.Label2.Width = 1.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 6.1875F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "Label4";
            this.Label4.Top = 0.1875F;
            this.Label4.Width = 1.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 6.1875F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "Label3";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.3125F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.2F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.031F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label8.Text = "Vndr";
            this.Label8.Top = 0.511F;
            this.Label8.Width = 0.375F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.2F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 0.4685F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label9.Text = "Name";
            this.Label9.Top = 0.511F;
            this.Label9.Width = 2.53125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.2F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 3.99975F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label10.Text = "Contact";
            this.Label10.Top = 0.511F;
            this.Label10.Width = 1.625F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.2F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 5.7185F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label11.Text = "Status";
            this.Label11.Top = 0.511F;
            this.Label11.Width = 0.46875F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.2F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 6.2185F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label12.Text = "Class";
            this.Label12.Top = 0.511F;
            this.Label12.Width = 0.84375F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.2F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 7.06225F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.Label13.Text = "1099";
            this.Label13.Top = 0.511F;
            this.Label13.Width = 0.375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.03125F;
            this.Line1.LineWeight = 0.001F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.75F;
            this.Line1.Width = 7.96875F;
            this.Line1.X1 = 0.03125F;
            this.Line1.X2 = 8F;
            this.Line1.Y1 = 0.75F;
            this.Line1.Y2 = 0.75F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.2F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 2.99975F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.Label14.Text = "YTD Amount";
            this.Label14.Top = 0.511F;
            this.Label14.Width = 0.9375F;
            // 
            // lblInsuranceRequired
            // 
            this.lblInsuranceRequired.Height = 0.2F;
            this.lblInsuranceRequired.HyperLink = null;
            this.lblInsuranceRequired.Left = 7.5F;
            this.lblInsuranceRequired.Name = "lblInsuranceRequired";
            this.lblInsuranceRequired.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.lblInsuranceRequired.Text = "Ins Req";
            this.lblInsuranceRequired.Top = 0.511F;
            this.lblInsuranceRequired.Width = 0.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptVendorList
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldInsuranceVerifiedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsuranceVerifiedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldClassCode5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAdjustment5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataEntryMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFaxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFaxNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInsuranceRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInsuranceRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInsuranceVerifiedDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblInsuranceVerifiedDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldContact;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCode1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustment1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCode2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustment2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCode3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustment3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCode4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustment4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCode5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustment5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line linDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Line linMessage;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox rtfMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPhoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFaxNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblFaxNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInsuranceRequired;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInsuranceRequired;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
