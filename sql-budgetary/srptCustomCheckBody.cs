﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckBody.
	/// </summary>
	public partial class srptCustomCheckBody : FCSectionReport
	{
		public static srptCustomCheckBody InstancePtr
		{
			get
			{
				return (srptCustomCheckBody)Sys.GetInstance(typeof(srptCustomCheckBody));
			}
		}

		protected srptCustomCheckBody _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCustomCheckBody	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsReportPrinterFunctions instReportFunctions = new clsReportPrinterFunctions();

		public srptCustomCheckBody()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsIcon = new clsDRWrapper())
            {
                string strLargeFonttoUse = "";
                int X;
                int y;
                string strSigFile = "";
                int lngWidth = 0;
                int lngHeight = 0;
                double dblRatio = 0;
                float lngSignatureBottom = 0;
                // vbPorter upgrade warning: intSigPassResponse As int --> As DialogResult
                int intSigPassResponse;
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                if (rptCustomCheck.InstancePtr.strCheckType == "D")
                {
                    strLargeFonttoUse =
                        instReportFunctions.GetFont(this.Document.Printer.PrinterName, 12, "Roman 12cpi");
                    if (strLargeFonttoUse != string.Empty)
                    {
                        instReportFunctions.SetReportFontsByTag(this, "Large", strLargeFonttoUse);
                    }

                    for (y = 0; y <= this.Sections.Count - 1; y++)
                    {
                        for (X = 0; X <= this.Sections[y].Controls.Count - 1; X++)
                        {
                            if (Strings.UCase(FCConvert.ToString(this.Sections[y].Controls[X].Tag)) == "LARGE" &&
                                this.Sections[y].Controls[X].Name != "lblVoid")
                            {
                                GrapeCity.ActiveReports.SectionReportModel.TextBox textbox =
                                    this.Sections[y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                                if (textbox != null)
                                {
                                    textbox.Font = new Font(textbox.Font.Name, 10);
                                }
                                else
                                {
                                    GrapeCity.ActiveReports.SectionReportModel.Label label =
                                        this.Sections[y]
                                            .Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Label;
                                    if (label != null)
                                    {
                                        label.Font = new Font(textbox.Font.Name, 10);
                                    }
                                }
                            }
                        }

                        // X
                    }

                    // y
                }

                rsIcon.OpenRecordset("SELECT * FROM Budgetary");
                if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("ShowIcon")))
                {
                    if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("UseTownSeal")))
                    {
                        if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic"))
                            {
                                imgIcon.Image =
                                    FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic");
                            }
                        }
                        else
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic"))
                            {
                                imgIcon.Image =
                                    FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic");
                            }
                        }
                    }
                    else
                    {
                        if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" +
                                            rsIcon.Get_Fields_String("CheckIcon")))
                            {
                                imgIcon.Image = FCUtils.LoadPicture(
                                    FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" +
                                    rsIcon.Get_Fields_String("CheckIcon"));
                            }
                        }
                        else
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" +
                                            rsIcon.Get_Fields_String("CheckIcon")))
                            {
                                imgIcon.Image = FCUtils.LoadPicture(
                                    FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" +
                                    rsIcon.Get_Fields_String("CheckIcon"));
                            }
                        }
                    }
                }

                lblReturnName.Text = rsIcon.Get_Fields_String("ReturnAddress1");
                lblReturnAddress1.Text = rsIcon.Get_Fields_String("ReturnAddress2");
                lblReturnAddress2.Text = rsIcon.Get_Fields_String("ReturnAddress3");
                lblReturnAddress3.Text = rsIcon.Get_Fields_String("ReturnAddress4");
                imgElectronicSignature.Visible = false;
                if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("PrintSignature")))
                {
                    // must get password
                    if (modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready != true)
                    {
                        modBudgetaryMaster.Statics.intResponse = MessageBox.Show(
                            "Do you want a signature to print on the checks?", "Print Signature?",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        // Else
                        // intResponse = vbYes
                    }

                    if (modBudgetaryMaster.Statics.intResponse == DialogResult.Yes ||
                        (modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready && lngSignatureBottom == 0))
                    {
                        TryPassword: ;
                        if (!modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready)
                        {
                            intSigPassResponse = frmSignaturePassword.InstancePtr.Init(modSignatureFile.BUDGETARYSIG);
                        }
                        else
                        {
                            intSigPassResponse = modBudgetaryMaster.Statics.lngSignatureID;
                        }

                        if (intSigPassResponse > 0)
                        {
                            strSigFile = modSignatureFile.GetSigFileFromID(ref intSigPassResponse);
                            if (!modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready)
                            {
                                modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = true;
                                modBudgetaryMaster.Statics.lngSignatureID = intSigPassResponse;
                            }

                            //FC:FINAL:MSH - issue #1585: wrong file path
                            //if (File.Exists(strSigFile))
                            if (File.Exists(Path.Combine(
                                fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, strSigFile)))
                            {
                                //FC:FINAL:MSH - issue #1585: wrong file path
                                //imgElectronicSignature.Image = FCUtils.LoadPicture(strSigFile);
                                imgElectronicSignature.Image = FCUtils.LoadPicture(
                                    Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder,
                                        strSigFile));
                                imgElectronicSignature.Visible = true;
                                lngWidth = imgElectronicSignature.Image.Width;
                                lngHeight = imgElectronicSignature.Image.Height;
                                lngSignatureBottom = imgElectronicSignature.Top + imgElectronicSignature.Height;
                                dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
                                if (dblRatio * imgElectronicSignature.Height > imgElectronicSignature.Width)
                                {
                                    // keep width, change height
                                    imgElectronicSignature.Height =
                                        FCConvert.ToSingle(imgElectronicSignature.Width / dblRatio);
                                }
                                else
                                {
                                    // keep height, change width
                                    imgElectronicSignature.Width =
                                        FCConvert.ToSingle(imgElectronicSignature.Height * dblRatio);
                                }

                                imgElectronicSignature.Top = lngSignatureBottom - imgElectronicSignature.Height;
                                // keep bottom of signature on line. This is default laser check
                                modBudgetaryMaster.WriteAuditRecord_8("Used Electronic Signature on checks",
                                    "Print AP Checks");
                            }
                        }
                        else
                        {
                            intSigPassResponse = FCConvert.ToInt32(MessageBox.Show(
                                "Incorrect Password. Re-enter password?", "Incorrect Password", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question));
                            if (intSigPassResponse == FCConvert.ToInt32(DialogResult.Yes))
                            {
                                goto TryPassword;
                            }
                            else
                            {
                                modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = true;
                            }
                        }
                    }
                    else
                    {
                        modBudgetaryMaster.Statics.blnSigPasswordCheckedAlready = true;
                    }
                }

                rsIcon.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " +
                                     FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
                if (rsIcon.EndOfFile() != true && rsIcon.BeginningOfFile() != true)
                {
                    lblVoidAfter90Days.Text =
                        Strings.Trim(FCConvert.ToString(rsIcon.Get_Fields_String("VoidAfterMessage")));
                }
            }

            SetupFieldsOnCheck();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strAmount = "";
			int intPlaceHolder = 0;
            using (clsDRWrapper rsVendorData = new clsDRWrapper())
            {
                bool blnCheck = false;
                if (rptCustomCheck.InstancePtr.curCarryOver != 0)
                {
                    fldAmount.Text = "";
                    fldDate.Text = "";
                }
                else
                {
                    fldAmount.Text = Strings.Format(rptCustomCheck.InstancePtr.curTotal, "#,##0.00");
                    fldAmount.Text = Strings.StrDup(13 - fldAmount.Text.Length, "*") + fldAmount.Text;
                    fldDate.Text = Strings.Format(rptCustomCheck.InstancePtr.datPayDate, "MM/dd/yy");
                }

                fldBankName.Text = rptCustomCheck.InstancePtr.strBankName;
                fldVendorName.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterVendorName);
                fldCheckName.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterVendorName);
                fldLargeCheckNumber.Text = rptCustomCheck.InstancePtr.lngFooterCheck.ToString();
                fldCheckNumber.Text = rptCustomCheck.InstancePtr.lngFooterCheck.ToString();
                fldAddress1.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress1);
                fldAddress2.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress2);
                fldAddress3.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress3);
                fldAddress4.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress4);
                if (Strings.Trim(fldAddress3.Text) == "")
                {
                    fldAddress3.Text = fldAddress4.Text;
                    fldAddress4.Text = "";
                }

                if (Strings.Trim(fldAddress2.Text) == "")
                {
                    fldAddress2.Text = fldAddress3.Text;
                    fldAddress3.Text = "";
                }

                if (Strings.Trim(fldAddress1.Text) == "")
                {
                    fldAddress1.Text = fldAddress2.Text;
                    fldAddress2.Text = "";
                }

                if (rptCustomCheck.InstancePtr.curTotal != 0)
                {
                    strAmount = modConvertAmountToString.ConvertAmountToString(rptCustomCheck.InstancePtr.curTotal);
                    blnCheck = false;
                    rsVendorData.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                               FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterVendorNumber));
                    if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
                    {
                        blnCheck = FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) &&
                                   !FCConvert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
                    }

                    if (!blnCheck)
                    {
                        lblVoid.Visible = false;
                        fldCarryOver.Visible = false;
                        fldEFT.Visible = false;
                    }
                    else
                    {
                        lblVoid.Visible = true;
                        fldCarryOver.Visible = false;
                        fldEFT.Visible = true;
                    }

                    if (strAmount.Length > 70)
                    {
                        intPlaceHolder = Strings.InStr(55, strAmount, " ", CompareConstants.vbBinaryCompare);
                        fldTextAmount1.Text = Strings.Left(strAmount, intPlaceHolder) +
                                              Strings.StrDup(70 - Strings.Left(strAmount, intPlaceHolder).Length, "*");
                        fldTextAmount2.Text = Strings.Right(strAmount, strAmount.Length - intPlaceHolder) +
                                              Strings.StrDup(
                                                  70 - Strings.Right(strAmount, strAmount.Length - intPlaceHolder)
                                                      .Length, "*");
                    }
                    else
                    {
                        fldTextAmount1.Text = "";
                        fldTextAmount2.Text = strAmount + Strings.StrDup(70 - strAmount.Length, "*");
                    }
                }
                else
                {
                    if (rptCustomCheck.InstancePtr.curCarryOver != 0)
                    {
                        strAmount = "";
                        lblVoid.Visible = true;
                        fldCarryOver.Visible = true;
                        imgElectronicSignature.Visible = false;
                        fldTextAmount1.Text = "";
                        fldTextAmount2.Text = "";
                    }
                    else
                    {
                        strAmount = "ZERO 00/100";
                        lblVoid.Visible = true;
                        if (strAmount.Length > 70)
                        {
                            intPlaceHolder = Strings.InStr(55, strAmount, " ", CompareConstants.vbBinaryCompare);
                            fldTextAmount1.Text = Strings.Left(strAmount, intPlaceHolder) +
                                                  Strings.StrDup(70 - Strings.Left(strAmount, intPlaceHolder).Length,
                                                      "*");
                            fldTextAmount2.Text = Strings.Right(strAmount, strAmount.Length - intPlaceHolder) +
                                                  Strings.StrDup(
                                                      70 - Strings.Right(strAmount, strAmount.Length - intPlaceHolder)
                                                          .Length, "*");
                        }
                        else
                        {
                            fldTextAmount1.Text = "";
                            fldTextAmount2.Text = strAmount + Strings.StrDup(70 - strAmount.Length, "*");
                        }
                    }
                }

                fldMICRLine.Text = "C" + FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterCheck) + "C A" +
                                   rptCustomCheck.InstancePtr.strRoutingNumber + "A  " +
                                   rptCustomCheck.InstancePtr.strBankAccountNumber + "C";
            }
        }

		private void SetupFieldsOnCheck()
		{
			clsDRWrapper rsCheckFields = new clsDRWrapper();
			string[] strFields = null;
			int counter;
			float lngHeight = 0;
			rsCheckFields.OpenRecordset("SELECT * FROM CustomCheckFields WHERE FormatID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
			if (rsCheckFields.EndOfFile() != true && rsCheckFields.BeginningOfFile() != true)
			{
				do
				{
					strFields = Strings.Split(FCConvert.ToString(rsCheckFields.Get_Fields_String("ControlName")), "@", -1, CompareConstants.vbBinaryCompare);
					lngHeight = 0;
					for (counter = 0; counter <= Information.UBound(strFields, 1); counter++)
					{
						if (FCConvert.ToString(rsCheckFields.Get_Fields_String("CheckType")) != rptCustomCheck.InstancePtr.strCheckType && FCConvert.ToString(rsCheckFields.Get_Fields_String("CheckType")) != "B" && (FCConvert.ToString(rsCheckFields.Get_Fields_String("CheckType")) != "L" || rptCustomCheck.InstancePtr.strCheckType != "M"))
						{
							this.Detail.Controls[strFields[counter]].Visible = false;
						}
						else
						{
							//FC:FINAL:MSH - Issue #813 - Positioning of fields implemented in inches, so we must convert pixels to inches. Otherwise, all data will be outside of the report area
							//this.Detail.Controls[strFields[counter]].Left = FCConvert.ToInt32(rsCheckFields.Get_Fields("XPosition")) * 144;
							this.Detail.Controls[strFields[counter]].Left = FCConvert.ToInt32(rsCheckFields.Get_Fields_Double("XPosition")) * 144 / 1440f;
							if (counter == 0)
							{
								//FC:FINAL:MSH - Issue #813 - Positioning of fields implemented in inches, so we must convert pixels to inches. Otherwise, all data will be outside of the report area
								//this.Detail.Controls[strFields[counter]].Top = lngHeight + FCConvert.ToInt32(rsCheckFields.Get_Fields("YPosition")) * 240;
								this.Detail.Controls[strFields[counter]].Top = lngHeight + FCConvert.ToInt32(rsCheckFields.Get_Fields_Double("YPosition")) * 240 / 1440f;
							}
							else
							{
								this.Detail.Controls[strFields[counter]].Top = lngHeight;
							}
							this.Detail.Controls[strFields[counter]].Visible = rsCheckFields.Get_Fields_Boolean("Include");
							if (counter == 0)
							{
								lngHeight = FCConvert.ToInt32(rsCheckFields.Get_Fields_Double("YPosition")) * 240 / 1440f + this.Detail.Controls[strFields[counter]].Height;
							}
							else
							{
								lngHeight += this.Detail.Controls[strFields[counter]].Height;
							}
						}
					}
					rsCheckFields.MoveNext();
				}
				while (rsCheckFields.EndOfFile() != true);
			}
		}

		private void srptCustomCheckBody_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptCustomCheckBody.Icon	= "srptCustomCheckBody.dsx":0000";
			//srptCustomCheckBody.Left	= 0;
			//srptCustomCheckBody.Top	= 0;
			//srptCustomCheckBody.Width	= 11880;
			//srptCustomCheckBody.Height	= 8595;
			//srptCustomCheckBody.StartUpPosition	= 3;
			//srptCustomCheckBody.SectionData	= "srptCustomCheckBody.dsx":058A;
			//End Unmaped Properties
		}
	}
}
