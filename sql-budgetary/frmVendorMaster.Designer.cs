//Fecher vbPorter - VersTabion 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorMaster.
	/// </summary>
	partial class frmVendorMaster : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCorrespondenceName;
		public fecherFoundation.FCLabel lblCorrespondenceName;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtAddress;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtCorAddress;
		public System.Collections.Generic.List<fecherFoundation.FCComboBox> cboCode;
		public System.Collections.Generic.List<T2KBackFillDecimal> txtAdjustment;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblPosNegValue;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblAdjNumber;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCFrame fraTaxInfo;
		public fecherFoundation.FCCheckBox chkAttorney;
		public fecherFoundation.FCCheckBox chkIncludeAllPayments;
		public fecherFoundation.FCComboBox cboOverrideCode;
		public fecherFoundation.FCTextBox txtTaxNumber;
		public fecherFoundation.FCLabel lblTaxNumber;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCCheckBox chk1099;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtAddress_5;
		public fecherFoundation.FCTextBox txtAddress_4;
		public fecherFoundation.FCTextBox txtAddress_3;
		public fecherFoundation.FCTextBox Text10;
		public fecherFoundation.FCTextBox Text9;
		public fecherFoundation.FCTextBox Text8;
		public fecherFoundation.FCTextBox Text7;
		public fecherFoundation.FCTextBox Text6;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public fecherFoundation.FCFrame fraEFT;
		public fecherFoundation.FCComboBox cboAccountType;
		public fecherFoundation.FCTextBox txtBankAccountNumber;
		public fecherFoundation.FCTextBox txtRoutingNumber;
		public fecherFoundation.FCCheckBox chkPrenote;
		public fecherFoundation.FCLabel lblAccountType;
		public fecherFoundation.FCLabel lblBankAccountNumber;
		public fecherFoundation.FCLabel lblRoutingNumber;
		public fecherFoundation.FCCheckBox chkEFT;
		public fecherFoundation.FCCheckBox chkInsuranceRequired;
		public fecherFoundation.FCFrame fraInsurance;
		public Global.T2KDateBox txtInsuranceVerifiedDate;
		public fecherFoundation.FCLabel lblInsuranceVerifiedDate;
		public fecherFoundation.FCTabControl tabAddress;
		public fecherFoundation.FCTabPage tabAddress_Page1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtCheckZip4;
		public fecherFoundation.FCTextBox txtCheckZip;
		public fecherFoundation.FCTextBox txtCheckState;
		public fecherFoundation.FCTextBox txtCheckCity;
		public fecherFoundation.FCTextBox txtCheckName;
		public fecherFoundation.FCTextBox txtAddress_0;
		public fecherFoundation.FCTextBox txtAddress_1;
		public fecherFoundation.FCTextBox txtAddress_2;
		public fecherFoundation.FCLabel lblCheckCityStateZip;
		public fecherFoundation.FCLabel lblCheckName;
		public fecherFoundation.FCLabel lblCheckAddress;
		public fecherFoundation.FCTabPage tabAddress_Page2;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCTextBox txtCorrespondZip4;
		public fecherFoundation.FCTextBox txtCorrespondState;
		public fecherFoundation.FCTextBox txtCorrespondCity;
		public fecherFoundation.FCTextBox txtCorName;
		public fecherFoundation.FCTextBox txtCorAddress_0;
		public fecherFoundation.FCTextBox txtCorAddress_1;
		public fecherFoundation.FCTextBox txtCorAddress_2;
		public fecherFoundation.FCTextBox txtCorrespondZip;
		public fecherFoundation.FCLabel lblCorrespondCityStateZip;
		public fecherFoundation.FCLabel lblCorName;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCCheckBox chkOneTime;
		public fecherFoundation.FCFrame Frame3;
		public FCGrid vs1;
		public Global.T2KOverTypeBox txtDefaultDescription;
		public Global.T2KOverTypeBox txtDefaultCheckNumber;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTextBox txtDataMessage;
		public fecherFoundation.FCTextBox txtCheckMessage;
		public fecherFoundation.FCTextBox txtExtension;
		public fecherFoundation.FCFrame frame4;
		public fecherFoundation.FCComboBox cboCode_4;
		public fecherFoundation.FCComboBox cboCode_3;
		public fecherFoundation.FCComboBox cboCode_2;
		public fecherFoundation.FCComboBox cboCode_1;
		public fecherFoundation.FCComboBox cboCode_0;
		public Global.T2KBackFillDecimal txtAdjustment_0;
		public Global.T2KBackFillDecimal txtAdjustment_1;
		public Global.T2KBackFillDecimal txtAdjustment_2;
		public Global.T2KBackFillDecimal txtAdjustment_3;
		public Global.T2KBackFillDecimal txtAdjustment_4;
		public fecherFoundation.FCLabel lblPosNegValue_4;
		public fecherFoundation.FCLabel lblPosNegValue_3;
		public fecherFoundation.FCLabel lblPosNegValue_2;
		public fecherFoundation.FCLabel lblPosNegValue_1;
		public fecherFoundation.FCLabel lblPosNegValue_0;
		public fecherFoundation.FCLabel lblAdjNumber_4;
		public fecherFoundation.FCLabel lblAdjNumber_3;
		public fecherFoundation.FCLabel lblAdjNumber_2;
		public fecherFoundation.FCLabel lblAdjNumber_1;
		public fecherFoundation.FCLabel lblAdjNumber_0;
		public fecherFoundation.FCLabel lblPosOrNeg;
		public fecherFoundation.FCLabel lblCode;
		public fecherFoundation.FCLabel lblAdjustment;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtStatus;
		public fecherFoundation.FCTextBox txtClass;
		public fecherFoundation.FCTextBox txtMessage;
		public Global.T2KPhoneNumberBox txtPhone;
		public Global.T2KPhoneNumberBox txtFax;
		public fecherFoundation.FCLabel lblFaxNumber;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblCheck;
		public fecherFoundation.FCLabel lblLastPayment;
		public fecherFoundation.FCLabel lblDataMessage;
		public fecherFoundation.FCLabel lblCheckMessage;
		public fecherFoundation.FCLabel lblExtension;
		public fecherFoundation.FCLabel lblContact;
		public fecherFoundation.FCLabel lblPhoneNumber;
		public fecherFoundation.FCLabel lblStatus;
		public fecherFoundation.FCLabel lblClass;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCHeader lblRecordNumber;
		public fecherFoundation.FCLabel lblLastPaymentDate;
		public fecherFoundation.FCLabel lblCheckNumber;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileViewLinkedDocument;
		public fecherFoundation.FCToolStripMenuItem mnuViewInvoices;
		public fecherFoundation.FCToolStripMenuItem mnuSep23;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintLabel;
		public fecherFoundation.FCToolStripMenuItem mnuRows;
		public fecherFoundation.FCToolStripMenuItem mnuRowsDeleteRow;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendorMaster));
			this.cmbCorrespondenceName = new fecherFoundation.FCComboBox();
			this.lblCorrespondenceName = new fecherFoundation.FCLabel();
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.fraTaxInfo = new fecherFoundation.FCFrame();
			this.chkAttorney = new fecherFoundation.FCCheckBox();
			this.chkIncludeAllPayments = new fecherFoundation.FCCheckBox();
			this.cboOverrideCode = new fecherFoundation.FCComboBox();
			this.txtTaxNumber = new fecherFoundation.FCTextBox();
			this.lblTaxNumber = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.chk1099 = new fecherFoundation.FCCheckBox();
			this.Frame7 = new fecherFoundation.FCFrame();
			this.txtAddress_5 = new fecherFoundation.FCTextBox();
			this.txtAddress_4 = new fecherFoundation.FCTextBox();
			this.txtAddress_3 = new fecherFoundation.FCTextBox();
			this.Text10 = new fecherFoundation.FCTextBox();
			this.Text9 = new fecherFoundation.FCTextBox();
			this.Text8 = new fecherFoundation.FCTextBox();
			this.Text7 = new fecherFoundation.FCTextBox();
			this.Text6 = new fecherFoundation.FCTextBox();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.fraEFT = new fecherFoundation.FCFrame();
			this.cboAccountType = new fecherFoundation.FCComboBox();
			this.txtBankAccountNumber = new fecherFoundation.FCTextBox();
			this.txtRoutingNumber = new fecherFoundation.FCTextBox();
			this.chkPrenote = new fecherFoundation.FCCheckBox();
			this.lblAccountType = new fecherFoundation.FCLabel();
			this.lblBankAccountNumber = new fecherFoundation.FCLabel();
			this.lblRoutingNumber = new fecherFoundation.FCLabel();
			this.chkEFT = new fecherFoundation.FCCheckBox();
			this.chkInsuranceRequired = new fecherFoundation.FCCheckBox();
			this.fraInsurance = new fecherFoundation.FCFrame();
			this.txtInsuranceVerifiedDate = new Global.T2KDateBox();
			this.lblInsuranceVerifiedDate = new fecherFoundation.FCLabel();
			this.tabAddress = new fecherFoundation.FCTabControl();
			this.tabAddress_Page1 = new fecherFoundation.FCTabPage();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtCheckZip4 = new fecherFoundation.FCTextBox();
			this.txtCheckZip = new fecherFoundation.FCTextBox();
			this.txtCheckState = new fecherFoundation.FCTextBox();
			this.txtCheckCity = new fecherFoundation.FCTextBox();
			this.txtCheckName = new fecherFoundation.FCTextBox();
			this.txtAddress_0 = new fecherFoundation.FCTextBox();
			this.txtAddress_1 = new fecherFoundation.FCTextBox();
			this.txtAddress_2 = new fecherFoundation.FCTextBox();
			this.lblCheckCityStateZip = new fecherFoundation.FCLabel();
			this.lblCheckName = new fecherFoundation.FCLabel();
			this.lblCheckAddress = new fecherFoundation.FCLabel();
			this.tabAddress_Page2 = new fecherFoundation.FCTabPage();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.chkAddress = new fecherFoundation.FCCheckBox();
			this.txtCorrespondZip4 = new fecherFoundation.FCTextBox();
			this.txtCorrespondState = new fecherFoundation.FCTextBox();
			this.txtCorrespondCity = new fecherFoundation.FCTextBox();
			this.txtCorName = new fecherFoundation.FCTextBox();
			this.txtCorAddress_0 = new fecherFoundation.FCTextBox();
			this.txtCorAddress_1 = new fecherFoundation.FCTextBox();
			this.txtCorAddress_2 = new fecherFoundation.FCTextBox();
			this.txtCorrespondZip = new fecherFoundation.FCTextBox();
			this.lblCorrespondCityStateZip = new fecherFoundation.FCLabel();
			this.lblCorName = new fecherFoundation.FCLabel();
			this.lblAddress = new fecherFoundation.FCLabel();
			this.txtEmail = new fecherFoundation.FCTextBox();
			this.chkOneTime = new fecherFoundation.FCCheckBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.vs1 = new fecherFoundation.FCGrid();
			this.txtDefaultDescription = new Global.T2KOverTypeBox();
			this.txtDefaultCheckNumber = new Global.T2KOverTypeBox();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.txtDataMessage = new fecherFoundation.FCTextBox();
			this.txtCheckMessage = new fecherFoundation.FCTextBox();
			this.txtExtension = new fecherFoundation.FCTextBox();
			this.frame4 = new fecherFoundation.FCFrame();
			this.cboCode_4 = new fecherFoundation.FCComboBox();
			this.cboCode_3 = new fecherFoundation.FCComboBox();
			this.cboCode_2 = new fecherFoundation.FCComboBox();
			this.cboCode_1 = new fecherFoundation.FCComboBox();
			this.cboCode_0 = new fecherFoundation.FCComboBox();
			this.txtAdjustment_0 = new Global.T2KBackFillDecimal();
			this.txtAdjustment_1 = new Global.T2KBackFillDecimal();
			this.txtAdjustment_2 = new Global.T2KBackFillDecimal();
			this.txtAdjustment_3 = new Global.T2KBackFillDecimal();
			this.txtAdjustment_4 = new Global.T2KBackFillDecimal();
			this.lblPosNegValue_4 = new fecherFoundation.FCLabel();
			this.lblPosNegValue_3 = new fecherFoundation.FCLabel();
			this.lblPosNegValue_2 = new fecherFoundation.FCLabel();
			this.lblPosNegValue_1 = new fecherFoundation.FCLabel();
			this.lblPosNegValue_0 = new fecherFoundation.FCLabel();
			this.lblAdjNumber_4 = new fecherFoundation.FCLabel();
			this.lblAdjNumber_3 = new fecherFoundation.FCLabel();
			this.lblAdjNumber_2 = new fecherFoundation.FCLabel();
			this.lblAdjNumber_1 = new fecherFoundation.FCLabel();
			this.lblAdjNumber_0 = new fecherFoundation.FCLabel();
			this.lblPosOrNeg = new fecherFoundation.FCLabel();
			this.lblCode = new fecherFoundation.FCLabel();
			this.lblAdjustment = new fecherFoundation.FCLabel();
			this.txtContact = new fecherFoundation.FCTextBox();
			this.txtStatus = new fecherFoundation.FCTextBox();
			this.txtClass = new fecherFoundation.FCTextBox();
			this.txtMessage = new fecherFoundation.FCTextBox();
			this.txtPhone = new Global.T2KPhoneNumberBox();
			this.txtFax = new Global.T2KPhoneNumberBox();
			this.lblFaxNumber = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblCheck = new fecherFoundation.FCLabel();
			this.lblLastPayment = new fecherFoundation.FCLabel();
			this.lblDataMessage = new fecherFoundation.FCLabel();
			this.lblCheckMessage = new fecherFoundation.FCLabel();
			this.lblExtension = new fecherFoundation.FCLabel();
			this.lblContact = new fecherFoundation.FCLabel();
			this.lblPhoneNumber = new fecherFoundation.FCLabel();
			this.lblStatus = new fecherFoundation.FCLabel();
			this.lblClass = new fecherFoundation.FCLabel();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.lblRecordNumber = new fecherFoundation.FCHeader();
			this.lblLastPaymentDate = new fecherFoundation.FCLabel();
			this.lblCheckNumber = new fecherFoundation.FCLabel();
			this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileViewLinkedDocument = new fecherFoundation.FCToolStripMenuItem();
			this.mnuViewInvoices = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSep23 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrintLabel = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRows = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRowsDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdDeleteVendor = new fecherFoundation.FCButton();
			this.cmdPrintMailingLabel = new fecherFoundation.FCButton();
			this.cmdAttachedDocuments = new fecherFoundation.FCButton();
			this.cmdInvoices = new fecherFoundation.FCButton();
			this.javaScript1 = new Wisej.Web.JavaScript(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTaxInfo)).BeginInit();
			this.fraTaxInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAttorney)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeAllPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk1099)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
			this.Frame7.SuspendLayout();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEFT)).BeginInit();
			this.fraEFT.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPrenote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInsuranceRequired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInsurance)).BeginInit();
			this.fraInsurance.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtInsuranceVerifiedDate)).BeginInit();
			this.tabAddress.SuspendLayout();
			this.tabAddress_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			this.tabAddress_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOneTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frame4)).BeginInit();
			this.frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintMailingLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAttachedDocuments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdInvoices)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 745);
			this.BottomPanel.Size = new System.Drawing.Size(1073, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Controls.Add(this.chkInsuranceRequired);
			this.ClientArea.Controls.Add(this.fraInsurance);
			this.ClientArea.Controls.Add(this.tabAddress);
			this.ClientArea.Controls.Add(this.txtEmail);
			this.ClientArea.Controls.Add(this.chkOneTime);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.txtDataMessage);
			this.ClientArea.Controls.Add(this.txtCheckMessage);
			this.ClientArea.Controls.Add(this.txtExtension);
			this.ClientArea.Controls.Add(this.frame4);
			this.ClientArea.Controls.Add(this.txtContact);
			this.ClientArea.Controls.Add(this.txtClass);
			this.ClientArea.Controls.Add(this.txtMessage);
			this.ClientArea.Controls.Add(this.txtPhone);
			this.ClientArea.Controls.Add(this.txtFax);
			this.ClientArea.Controls.Add(this.lblFaxNumber);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblCheck);
			this.ClientArea.Controls.Add(this.lblLastPayment);
			this.ClientArea.Controls.Add(this.lblDataMessage);
			this.ClientArea.Controls.Add(this.lblCheckMessage);
			this.ClientArea.Controls.Add(this.lblExtension);
			this.ClientArea.Controls.Add(this.lblContact);
			this.ClientArea.Controls.Add(this.lblPhoneNumber);
			this.ClientArea.Controls.Add(this.lblClass);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Controls.Add(this.lblLastPaymentDate);
			this.ClientArea.Controls.Add(this.lblCheckNumber);
			this.ClientArea.Size = new System.Drawing.Size(1073, 685);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.lblRecordNumber);
			this.TopPanel.Controls.Add(this.cmdInvoices);
			this.TopPanel.Controls.Add(this.cmdAttachedDocuments);
			this.TopPanel.Controls.Add(this.cmdPrintMailingLabel);
			this.TopPanel.Controls.Add(this.cmdDeleteVendor);
			this.TopPanel.Controls.Add(this.txtStatus);
			this.TopPanel.Controls.Add(this.lblStatus);
			this.TopPanel.Size = new System.Drawing.Size(1073, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.lblStatus, 0);
			this.TopPanel.Controls.SetChildIndex(this.txtStatus, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteVendor, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintMailingLabel, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAttachedDocuments, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdInvoices, 0);
			this.TopPanel.Controls.SetChildIndex(this.lblRecordNumber, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(112, 30);
			this.HeaderText.Text = "Vendor #";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbCorrespondenceName
			// 
			this.cmbCorrespondenceName.AutoSize = false;
			this.cmbCorrespondenceName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCorrespondenceName.FormattingEnabled = true;
			this.cmbCorrespondenceName.Items.AddRange(new object[] {
				"Correspondence Name",
				"Check Name"
			});
			this.cmbCorrespondenceName.Location = new System.Drawing.Point(174, 96);
			this.cmbCorrespondenceName.Name = "cmbCorrespondenceName";
			this.cmbCorrespondenceName.Size = new System.Drawing.Size(283, 40);
			this.cmbCorrespondenceName.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.cmbCorrespondenceName, null);
			// 
			// lblCorrespondenceName
			// 
			this.lblCorrespondenceName.Location = new System.Drawing.Point(20, 110);
			this.lblCorrespondenceName.Name = "lblCorrespondenceName";
			this.lblCorrespondenceName.Size = new System.Drawing.Size(120, 16);
			this.lblCorrespondenceName.TabIndex = 6;
			this.lblCorrespondenceName.Text = "1099 VENDOR NAME";
			this.ToolTip1.SetToolTip(this.lblCorrespondenceName, null);
			// 
			// SSTab1
			// 
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Location = new System.Drawing.Point(572, 214);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
			this.SSTab1.ShowFocusRect = false;
			this.SSTab1.Size = new System.Drawing.Size(475, 265);
			this.SSTab1.TabIndex = 13;
			this.SSTab1.TabsPerRow = 0;
			this.SSTab1.Text = "EFT Info";
			this.ToolTip1.SetToolTip(this.SSTab1, null);
			this.SSTab1.WordWrap = false;
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.Controls.Add(this.fraTaxInfo);
			this.SSTab1_Page1.Controls.Add(this.chk1099);
			this.SSTab1_Page1.Controls.Add(this.Frame7);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 50);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Text = "1099 Info";
			this.ToolTip1.SetToolTip(this.SSTab1_Page1, null);
			// 
			// fraTaxInfo
			// 
			this.fraTaxInfo.AppearanceKey = "groupBoxNoBorders";
			this.fraTaxInfo.Controls.Add(this.chkAttorney);
			this.fraTaxInfo.Controls.Add(this.lblCorrespondenceName);
			this.fraTaxInfo.Controls.Add(this.chkIncludeAllPayments);
			this.fraTaxInfo.Controls.Add(this.cmbCorrespondenceName);
			this.fraTaxInfo.Controls.Add(this.cboOverrideCode);
			this.fraTaxInfo.Controls.Add(this.txtTaxNumber);
			this.fraTaxInfo.Controls.Add(this.lblTaxNumber);
			this.fraTaxInfo.Controls.Add(this.Label3);
			this.fraTaxInfo.Location = new System.Drawing.Point(0, 54);
			this.fraTaxInfo.Name = "fraTaxInfo";
			this.fraTaxInfo.Size = new System.Drawing.Size(470, 160);
			this.fraTaxInfo.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.fraTaxInfo, null);
			// 
			// chkAttorney
			// 
			this.chkAttorney.AutoSize = false;
			this.chkAttorney.Location = new System.Drawing.Point(20, 20);
			this.chkAttorney.Name = "chkAttorney";
			this.chkAttorney.Size = new System.Drawing.Size(148, 24);
			this.chkAttorney.TabIndex = 0;
			this.chkAttorney.Text = "1099 is Required";
			this.ToolTip1.SetToolTip(this.chkAttorney, null);
			this.chkAttorney.CheckedChanged += new System.EventHandler(this.chkAttorney_CheckedChanged);
			// 
			// chkIncludeAllPayments
			// 
			this.chkIncludeAllPayments.AutoSize = false;
			this.chkIncludeAllPayments.Location = new System.Drawing.Point(20, 54);
			this.chkIncludeAllPayments.Name = "chkIncludeAllPayments";
			this.chkIncludeAllPayments.Size = new System.Drawing.Size(178, 24);
			this.chkIncludeAllPayments.TabIndex = 3;
			this.chkIncludeAllPayments.Text = "Include All Payments";
			this.ToolTip1.SetToolTip(this.chkIncludeAllPayments, null);
			// 
			// cboOverrideCode
			// 
			this.cboOverrideCode.AutoSize = false;
			this.cboOverrideCode.BackColor = System.Drawing.SystemColors.Window;
			this.cboOverrideCode.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboOverrideCode.FormattingEnabled = true;
			this.cboOverrideCode.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboOverrideCode.Location = new System.Drawing.Point(218, 46);
			this.cboOverrideCode.Name = "cboOverrideCode";
			this.cboOverrideCode.Size = new System.Drawing.Size(93, 40);
			this.cboOverrideCode.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.cboOverrideCode, null);
			this.cboOverrideCode.DropDown += new System.EventHandler(this.cboOverrideCode_DropDown);
			// 
			// txtTaxNumber
			// 
			this.txtTaxNumber.AutoSize = false;
			this.txtTaxNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtTaxNumber.LinkItem = null;
			this.txtTaxNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTaxNumber.LinkTopic = null;
			this.txtTaxNumber.Location = new System.Drawing.Point(358, 46);
			this.txtTaxNumber.MaxLength = 11;
			this.txtTaxNumber.Name = "txtTaxNumber";
			this.txtTaxNumber.Size = new System.Drawing.Size(99, 40);
			this.txtTaxNumber.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtTaxNumber, null);
			this.txtTaxNumber.TextChanged += new System.EventHandler(this.txtTaxNumber_TextChanged);
			this.txtTaxNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaxNumber_Validating);
			// 
			// lblTaxNumber
			// 
			this.lblTaxNumber.Location = new System.Drawing.Point(381, 20);
			this.lblTaxNumber.Name = "lblTaxNumber";
			this.lblTaxNumber.Size = new System.Drawing.Size(50, 16);
			this.lblTaxNumber.TabIndex = 2;
			this.lblTaxNumber.Text = "TAX ID#";
			this.ToolTip1.SetToolTip(this.lblTaxNumber, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(230, 20);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(64, 16);
			this.Label3.TabIndex = 1;
			this.Label3.Text = "1099 CODE";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// chk1099
			// 
			this.chk1099.AutoSize = false;
			this.chk1099.Location = new System.Drawing.Point(20, 30);
			this.chk1099.Name = "chk1099";
			this.chk1099.Size = new System.Drawing.Size(121, 24);
			this.chk1099.TabIndex = 0;
			this.chk1099.Text = "1099 Eligible";
			this.ToolTip1.SetToolTip(this.chk1099, null);
			this.chk1099.CheckedChanged += new System.EventHandler(this.chk1099_CheckedChanged);
			// 
			// Frame7
			// 
			this.Frame7.Controls.Add(this.txtAddress_5);
			this.Frame7.Controls.Add(this.txtAddress_4);
			this.Frame7.Controls.Add(this.txtAddress_3);
			this.Frame7.Controls.Add(this.Text10);
			this.Frame7.Controls.Add(this.Text9);
			this.Frame7.Controls.Add(this.Text8);
			this.Frame7.Controls.Add(this.Text7);
			this.Frame7.Controls.Add(this.Text6);
			this.Frame7.Controls.Add(this.Label10);
			this.Frame7.Controls.Add(this.Label9);
			this.Frame7.Controls.Add(this.Label8);
			this.Frame7.Location = new System.Drawing.Point(-5051, 3);
			this.Frame7.Name = "Frame7";
			this.Frame7.Size = new System.Drawing.Size(288, 128);
			this.Frame7.TabIndex = 89;
			this.ToolTip1.SetToolTip(this.Frame7, null);
			// 
			// txtAddress_5
			// 
			this.txtAddress_5.AutoSize = false;
			this.txtAddress_5.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_5.LinkItem = null;
			this.txtAddress_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_5.LinkTopic = null;
			this.txtAddress_5.Location = new System.Drawing.Point(49, 89);
			this.txtAddress_5.MaxLength = 35;
			this.txtAddress_5.Name = "txtAddress_5";
			this.txtAddress_5.Size = new System.Drawing.Size(236, 40);
			this.txtAddress_5.TabIndex = 97;
			this.ToolTip1.SetToolTip(this.txtAddress_5, "This line will not print on 1099 forms.");
			this.txtAddress_5.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// txtAddress_4
			// 
			this.txtAddress_4.AutoSize = false;
			this.txtAddress_4.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_4.LinkItem = null;
			this.txtAddress_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_4.LinkTopic = null;
			this.txtAddress_4.Location = new System.Drawing.Point(49, 70);
			this.txtAddress_4.MaxLength = 35;
			this.txtAddress_4.Name = "txtAddress_4";
			this.txtAddress_4.Size = new System.Drawing.Size(236, 40);
			this.txtAddress_4.TabIndex = 96;
			this.ToolTip1.SetToolTip(this.txtAddress_4, null);
			this.txtAddress_4.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// txtAddress_3
			// 
			this.txtAddress_3.AutoSize = false;
			this.txtAddress_3.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_3.LinkItem = null;
			this.txtAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_3.LinkTopic = null;
			this.txtAddress_3.Location = new System.Drawing.Point(49, 51);
			this.txtAddress_3.MaxLength = 35;
			this.txtAddress_3.Name = "txtAddress_3";
			this.txtAddress_3.Size = new System.Drawing.Size(236, 40);
			this.txtAddress_3.TabIndex = 95;
			this.ToolTip1.SetToolTip(this.txtAddress_3, null);
			this.txtAddress_3.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// Text10
			// 
			this.Text10.AutoSize = false;
			this.Text10.BackColor = System.Drawing.SystemColors.Window;
			this.Text10.LinkItem = null;
			this.Text10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text10.LinkTopic = null;
			this.Text10.Location = new System.Drawing.Point(49, 32);
			this.Text10.MaxLength = 35;
			this.Text10.Name = "Text10";
			this.Text10.Size = new System.Drawing.Size(236, 16);
			this.Text10.TabIndex = 94;
			this.ToolTip1.SetToolTip(this.Text10, null);
			// 
			// Text9
			// 
			this.Text9.AutoSize = false;
			this.Text9.BackColor = System.Drawing.SystemColors.Window;
			this.Text9.LinkItem = null;
			this.Text9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text9.LinkTopic = null;
			this.Text9.Location = new System.Drawing.Point(49, 108);
			this.Text9.MaxLength = 35;
			this.Text9.Name = "Text9";
			this.Text9.Size = new System.Drawing.Size(137, 16);
			this.Text9.TabIndex = 93;
			this.ToolTip1.SetToolTip(this.Text9, null);
			// 
			// Text8
			// 
			this.Text8.AutoSize = false;
			this.Text8.BackColor = System.Drawing.SystemColors.Window;
			this.Text8.LinkItem = null;
			this.Text8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text8.LinkTopic = null;
			this.Text8.Location = new System.Drawing.Point(188, 108);
			this.Text8.MaxLength = 2;
			this.Text8.Name = "Text8";
			this.Text8.Size = new System.Drawing.Size(19, 16);
			this.Text8.TabIndex = 92;
			this.ToolTip1.SetToolTip(this.Text8, null);
			// 
			// Text7
			// 
			this.Text7.AutoSize = false;
			this.Text7.BackColor = System.Drawing.SystemColors.Window;
			this.Text7.LinkItem = null;
			this.Text7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text7.LinkTopic = null;
			this.Text7.Location = new System.Drawing.Point(210, 108);
			this.Text7.MaxLength = 5;
			this.Text7.Name = "Text7";
			this.Text7.Size = new System.Drawing.Size(39, 16);
			this.Text7.TabIndex = 91;
			this.ToolTip1.SetToolTip(this.Text7, null);
			// 
			// Text6
			// 
			this.Text6.AutoSize = false;
			this.Text6.BackColor = System.Drawing.SystemColors.Window;
			this.Text6.LinkItem = null;
			this.Text6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.Text6.LinkTopic = null;
			this.Text6.Location = new System.Drawing.Point(253, 108);
			this.Text6.MaxLength = 4;
			this.Text6.Name = "Text6";
			this.Text6.Size = new System.Drawing.Size(31, 16);
			this.Text6.TabIndex = 90;
			this.ToolTip1.SetToolTip(this.Text6, null);
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(4, 51);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(54, 16);
			this.Label10.TabIndex = 100;
			this.Label10.Text = "ADDRESS";
			this.ToolTip1.SetToolTip(this.Label10, null);
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(4, 32);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(41, 16);
			this.Label9.TabIndex = 99;
			this.Label9.Text = "NAME";
			this.ToolTip1.SetToolTip(this.Label9, null);
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(4, 108);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(54, 16);
			this.Label8.TabIndex = 98;
			this.Label8.Text = "C/S/Z";
			this.ToolTip1.SetToolTip(this.Label8, null);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.Controls.Add(this.fraEFT);
			this.SSTab1_Page2.Controls.Add(this.chkEFT);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 50);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Text = "EFT Info";
			this.ToolTip1.SetToolTip(this.SSTab1_Page2, null);
			// 
			// fraEFT
			// 
			this.fraEFT.AppearanceKey = "groupBoxNoBorders";
			this.fraEFT.Controls.Add(this.cboAccountType);
			this.fraEFT.Controls.Add(this.txtBankAccountNumber);
			this.fraEFT.Controls.Add(this.txtRoutingNumber);
			this.fraEFT.Controls.Add(this.chkPrenote);
			this.fraEFT.Controls.Add(this.lblAccountType);
			this.fraEFT.Controls.Add(this.lblBankAccountNumber);
			this.fraEFT.Controls.Add(this.lblRoutingNumber);
			this.fraEFT.Enabled = false;
			this.fraEFT.Location = new System.Drawing.Point(0, 54);
			this.fraEFT.Name = "fraEFT";
			this.fraEFT.Size = new System.Drawing.Size(470, 160);
			this.fraEFT.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.fraEFT, null);
			// 
			// cboAccountType
			// 
			this.cboAccountType.AutoSize = false;
			this.cboAccountType.BackColor = System.Drawing.SystemColors.Window;
			this.cboAccountType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboAccountType.FormattingEnabled = true;
			this.cboAccountType.Location = new System.Drawing.Point(125, 10);
			this.cboAccountType.Name = "cboAccountType";
			this.cboAccountType.Size = new System.Drawing.Size(226, 40);
			this.cboAccountType.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cboAccountType, null);
			// 
			// txtBankAccountNumber
			// 
			this.txtBankAccountNumber.AutoSize = false;
			this.txtBankAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtBankAccountNumber.LinkItem = null;
			this.txtBankAccountNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBankAccountNumber.LinkTopic = null;
			this.txtBankAccountNumber.Location = new System.Drawing.Point(125, 60);
			this.txtBankAccountNumber.MaxLength = 35;
			this.txtBankAccountNumber.Name = "txtBankAccountNumber";
			this.txtBankAccountNumber.Size = new System.Drawing.Size(226, 40);
			this.txtBankAccountNumber.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtBankAccountNumber, null);
			// 
			// txtRoutingNumber
			// 
			this.txtRoutingNumber.AutoSize = false;
			this.txtRoutingNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtRoutingNumber.LinkItem = null;
			this.txtRoutingNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRoutingNumber.LinkTopic = null;
			this.txtRoutingNumber.Location = new System.Drawing.Point(125, 110);
			this.txtRoutingNumber.MaxLength = 35;
			this.txtRoutingNumber.Name = "txtRoutingNumber";
			this.txtRoutingNumber.Size = new System.Drawing.Size(226, 40);
			this.txtRoutingNumber.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtRoutingNumber, null);
			// 
			// chkPrenote
			// 
			this.chkPrenote.AutoSize = false;
			this.chkPrenote.Enabled = false;
			this.chkPrenote.Location = new System.Drawing.Point(378, 16);
			this.chkPrenote.Name = "chkPrenote";
			this.chkPrenote.Size = new System.Drawing.Size(85, 24);
			this.chkPrenote.TabIndex = 2;
			this.chkPrenote.Text = "Prenote";
			this.ToolTip1.SetToolTip(this.chkPrenote, null);
			// 
			// lblAccountType
			// 
			this.lblAccountType.Enabled = false;
			this.lblAccountType.Location = new System.Drawing.Point(20, 24);
			this.lblAccountType.Name = "lblAccountType";
			this.lblAccountType.Size = new System.Drawing.Size(70, 16);
			this.lblAccountType.TabIndex = 0;
			this.lblAccountType.Text = "ACCT TYPE";
			this.ToolTip1.SetToolTip(this.lblAccountType, null);
			// 
			// lblBankAccountNumber
			// 
			this.lblBankAccountNumber.Enabled = false;
			this.lblBankAccountNumber.Location = new System.Drawing.Point(20, 124);
			this.lblBankAccountNumber.Name = "lblBankAccountNumber";
			this.lblBankAccountNumber.Size = new System.Drawing.Size(70, 16);
			this.lblBankAccountNumber.TabIndex = 5;
			this.lblBankAccountNumber.Text = "ACCOUNT #";
			this.ToolTip1.SetToolTip(this.lblBankAccountNumber, null);
			// 
			// lblRoutingNumber
			// 
			this.lblRoutingNumber.Enabled = false;
			this.lblRoutingNumber.Location = new System.Drawing.Point(20, 74);
			this.lblRoutingNumber.Name = "lblRoutingNumber";
			this.lblRoutingNumber.Size = new System.Drawing.Size(68, 16);
			this.lblRoutingNumber.TabIndex = 3;
			this.lblRoutingNumber.Text = "ROUTING #";
			this.ToolTip1.SetToolTip(this.lblRoutingNumber, null);
			// 
			// chkEFT
			// 
			this.chkEFT.AutoSize = false;
			this.chkEFT.Location = new System.Drawing.Point(20, 30);
			this.chkEFT.Name = "chkEFT";
			this.chkEFT.Size = new System.Drawing.Size(116, 24);
			this.chkEFT.TabIndex = 0;
			this.chkEFT.Text = "EFT Vendor";
			this.ToolTip1.SetToolTip(this.chkEFT, null);
			this.chkEFT.CheckedChanged += new System.EventHandler(this.chkEFT_CheckedChanged);
			// 
			// chkInsuranceRequired
			// 
			this.chkInsuranceRequired.AutoSize = false;
			this.chkInsuranceRequired.Location = new System.Drawing.Point(30, 439);
			this.chkInsuranceRequired.Name = "chkInsuranceRequired";
			this.chkInsuranceRequired.Size = new System.Drawing.Size(171, 24);
			this.chkInsuranceRequired.TabIndex = 14;
			this.chkInsuranceRequired.Text = "Insurance Required";
			this.ToolTip1.SetToolTip(this.chkInsuranceRequired, null);
			this.chkInsuranceRequired.CheckedChanged += new System.EventHandler(this.chkInsuranceRequired_CheckedChanged);
			// 
			// fraInsurance
			// 
			this.fraInsurance.AppearanceKey = "groupBoxNoBorders";
			this.fraInsurance.Controls.Add(this.txtInsuranceVerifiedDate);
			this.fraInsurance.Controls.Add(this.lblInsuranceVerifiedDate);
			this.fraInsurance.Location = new System.Drawing.Point(207, 423);
			this.fraInsurance.Name = "fraInsurance";
			this.fraInsurance.Size = new System.Drawing.Size(336, 56);
			this.fraInsurance.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.fraInsurance, null);
			// 
			// txtInsuranceVerifiedDate
			// 
			this.txtInsuranceVerifiedDate.Enabled = false;
			this.txtInsuranceVerifiedDate.Location = new System.Drawing.Point(150, 10);
			this.txtInsuranceVerifiedDate.Mask = "00/00/0000";
			this.txtInsuranceVerifiedDate.Name = "txtInsuranceVerifiedDate";
			this.txtInsuranceVerifiedDate.Size = new System.Drawing.Size(111, 40);
			this.txtInsuranceVerifiedDate.TabIndex = 1;
			this.txtInsuranceVerifiedDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtInsuranceVerifiedDate, null);
			// 
			// lblInsuranceVerifiedDate
			// 
			this.lblInsuranceVerifiedDate.Enabled = false;
			this.lblInsuranceVerifiedDate.Location = new System.Drawing.Point(18, 24);
			this.lblInsuranceVerifiedDate.Name = "lblInsuranceVerifiedDate";
			this.lblInsuranceVerifiedDate.Size = new System.Drawing.Size(87, 16);
			this.lblInsuranceVerifiedDate.TabIndex = 0;
			this.lblInsuranceVerifiedDate.Text = "DATE VERIFIED";
			this.ToolTip1.SetToolTip(this.lblInsuranceVerifiedDate, null);
			// 
			// tabAddress
			// 
			this.tabAddress.Controls.Add(this.tabAddress_Page1);
			this.tabAddress.Controls.Add(this.tabAddress_Page2);
			this.tabAddress.Location = new System.Drawing.Point(30, 30);
			this.tabAddress.Name = "tabAddress";
			this.tabAddress.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
			this.tabAddress.ShowFocusRect = false;
			this.tabAddress.Size = new System.Drawing.Size(506, 380);
			this.tabAddress.TabIndex = 0;
			this.tabAddress.TabsPerRow = 0;
			this.tabAddress.Text = "Check Address";
			this.ToolTip1.SetToolTip(this.tabAddress, null);
			this.tabAddress.WordWrap = false;
			// 
			// tabAddress_Page1
			// 
			this.tabAddress_Page1.Controls.Add(this.Frame1);
			this.tabAddress_Page1.Location = new System.Drawing.Point(1, 50);
			this.tabAddress_Page1.Name = "tabAddress_Page1";
			this.tabAddress_Page1.Text = "Check Address";
			this.ToolTip1.SetToolTip(this.tabAddress_Page1, null);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.txtCheckZip4);
			this.Frame1.Controls.Add(this.txtCheckZip);
			this.Frame1.Controls.Add(this.txtCheckState);
			this.Frame1.Controls.Add(this.txtCheckCity);
			this.Frame1.Controls.Add(this.txtCheckName);
			this.Frame1.Controls.Add(this.txtAddress_0);
			this.Frame1.Controls.Add(this.txtAddress_1);
			this.Frame1.Controls.Add(this.txtAddress_2);
			this.Frame1.Controls.Add(this.lblCheckCityStateZip);
			this.Frame1.Controls.Add(this.lblCheckName);
			this.Frame1.Controls.Add(this.lblCheckAddress);
			this.Frame1.Location = new System.Drawing.Point(0, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(501, 327);
			this.Frame1.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtCheckZip4
			// 
			this.txtCheckZip4.AutoSize = false;
			this.txtCheckZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtCheckZip4.CharacterCasing = CharacterCasing.Upper;
            this.txtCheckZip4.LinkItem = null;
			this.txtCheckZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckZip4.LinkTopic = null;
			this.txtCheckZip4.Location = new System.Drawing.Point(407, 270);
			this.txtCheckZip4.MaxLength = 4;
			this.txtCheckZip4.Name = "txtCheckZip4";
			this.txtCheckZip4.Size = new System.Drawing.Size(75, 40);
			this.txtCheckZip4.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtCheckZip4, null);
			this.txtCheckZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheckZip4_KeyPress);
			// 
			// txtCheckZip
			// 
			this.txtCheckZip.AutoSize = false;
			this.txtCheckZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCheckZip.CharacterCasing = CharacterCasing.Upper;
            this.txtCheckZip.LinkItem = null;
			this.txtCheckZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckZip.LinkTopic = null;
			this.txtCheckZip.Location = new System.Drawing.Point(327, 270);
			this.txtCheckZip.MaxLength = 5;
			this.txtCheckZip.Name = "txtCheckZip";
			this.txtCheckZip.Size = new System.Drawing.Size(78, 40);
			this.txtCheckZip.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtCheckZip, null);
			this.txtCheckZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheckZip_KeyPress);
			// 
			// txtCheckState
			// 
			this.txtCheckState.AutoSize = false;
			this.txtCheckState.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckState.LinkItem = null;
			this.txtCheckState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckState.LinkTopic = null;
			this.txtCheckState.Location = new System.Drawing.Point(262, 270);
			this.txtCheckState.MaxLength = 2;
			this.txtCheckState.Name = "txtCheckState";
			this.txtCheckState.Size = new System.Drawing.Size(63, 40);
			this.txtCheckState.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtCheckState, null);
			// 
			// txtCheckCity
			// 
			this.txtCheckCity.AutoSize = false;
			this.txtCheckCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckCity.LinkItem = null;
			this.txtCheckCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckCity.LinkTopic = null;
			this.txtCheckCity.Location = new System.Drawing.Point(99, 270);
			this.txtCheckCity.MaxLength = 35;
			this.txtCheckCity.Name = "txtCheckCity";
			this.txtCheckCity.Size = new System.Drawing.Size(160, 40);
			this.txtCheckCity.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtCheckCity, null);
			// 
			// txtCheckName
			// 
			this.txtCheckName.AutoSize = false;
			this.txtCheckName.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckName.LinkItem = null;
			this.txtCheckName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckName.LinkTopic = null;
			this.txtCheckName.Location = new System.Drawing.Point(99, 30);
			this.txtCheckName.MaxLength = 50;
			this.txtCheckName.Name = "txtCheckName";
			this.txtCheckName.Size = new System.Drawing.Size(383, 40);
			this.txtCheckName.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtCheckName, null);
			this.txtCheckName.TextChanged += new System.EventHandler(this.txtCheckName_TextChanged);
			// 
			// txtAddress_0
			// 
			this.txtAddress_0.AutoSize = false;
			this.txtAddress_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_0.LinkItem = null;
			this.txtAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_0.LinkTopic = null;
			this.txtAddress_0.Location = new System.Drawing.Point(99, 90);
			this.txtAddress_0.MaxLength = 35;
			this.txtAddress_0.Name = "txtAddress_0";
			this.txtAddress_0.Size = new System.Drawing.Size(383, 40);
			this.txtAddress_0.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtAddress_0, null);
			this.txtAddress_0.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// txtAddress_1
			// 
			this.txtAddress_1.AutoSize = false;
			this.txtAddress_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_1.LinkItem = null;
			this.txtAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_1.LinkTopic = null;
			this.txtAddress_1.Location = new System.Drawing.Point(99, 150);
			this.txtAddress_1.MaxLength = 35;
			this.txtAddress_1.Name = "txtAddress_1";
			this.txtAddress_1.Size = new System.Drawing.Size(383, 40);
			this.txtAddress_1.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtAddress_1, null);
			this.txtAddress_1.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// txtAddress_2
			// 
			this.txtAddress_2.AutoSize = false;
			this.txtAddress_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress_2.LinkItem = null;
			this.txtAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_2.LinkTopic = null;
			this.txtAddress_2.Location = new System.Drawing.Point(99, 210);
			this.txtAddress_2.MaxLength = 35;
			this.txtAddress_2.Name = "txtAddress_2";
			this.txtAddress_2.Size = new System.Drawing.Size(383, 40);
			this.txtAddress_2.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtAddress_2, "This line will not print on 1099 forms.");
			this.txtAddress_2.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// lblCheckCityStateZip
			// 
			this.lblCheckCityStateZip.Location = new System.Drawing.Point(20, 284);
			this.lblCheckCityStateZip.Name = "lblCheckCityStateZip";
			this.lblCheckCityStateZip.Size = new System.Drawing.Size(35, 16);
			this.lblCheckCityStateZip.TabIndex = 6;
			this.lblCheckCityStateZip.Text = "C/S/Z";
			this.ToolTip1.SetToolTip(this.lblCheckCityStateZip, null);
			// 
			// lblCheckName
			// 
			this.lblCheckName.Location = new System.Drawing.Point(20, 44);
			this.lblCheckName.Name = "lblCheckName";
			this.lblCheckName.Size = new System.Drawing.Size(38, 16);
			this.lblCheckName.TabIndex = 0;
			this.lblCheckName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblCheckName, null);
			// 
			// lblCheckAddress
			// 
			this.lblCheckAddress.Location = new System.Drawing.Point(20, 104);
			this.lblCheckAddress.Name = "lblCheckAddress";
			this.lblCheckAddress.Size = new System.Drawing.Size(58, 16);
			this.lblCheckAddress.TabIndex = 2;
			this.lblCheckAddress.Text = "ADDRESS";
			this.ToolTip1.SetToolTip(this.lblCheckAddress, null);
			// 
			// tabAddress_Page2
			// 
			this.tabAddress_Page2.Controls.Add(this.Frame2);
			this.tabAddress_Page2.Location = new System.Drawing.Point(1, 50);
			this.tabAddress_Page2.Name = "tabAddress_Page2";
			this.tabAddress_Page2.Text = "Correspond Address";
			this.ToolTip1.SetToolTip(this.tabAddress_Page2, null);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.chkAddress);
			this.Frame2.Controls.Add(this.txtCorrespondZip4);
			this.Frame2.Controls.Add(this.txtCorrespondState);
			this.Frame2.Controls.Add(this.txtCorrespondCity);
			this.Frame2.Controls.Add(this.txtCorName);
			this.Frame2.Controls.Add(this.txtCorAddress_0);
			this.Frame2.Controls.Add(this.txtCorAddress_1);
			this.Frame2.Controls.Add(this.txtCorAddress_2);
			this.Frame2.Controls.Add(this.txtCorrespondZip);
			this.Frame2.Controls.Add(this.lblCorrespondCityStateZip);
			this.Frame2.Controls.Add(this.lblCorName);
			this.Frame2.Controls.Add(this.lblAddress);
			this.Frame2.Location = new System.Drawing.Point(0, 0);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(501, 327);
			this.Frame2.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// chkAddress
			// 
			this.chkAddress.AutoSize = false;
			this.chkAddress.Checked = true;
			this.chkAddress.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkAddress.Location = new System.Drawing.Point(20, 30);
			this.chkAddress.Name = "chkAddress";
			this.chkAddress.Size = new System.Drawing.Size(208, 24);
			this.chkAddress.TabIndex = 0;
			this.chkAddress.Text = "Same as Check Address";
			this.ToolTip1.SetToolTip(this.chkAddress, null);
			this.chkAddress.CheckedChanged += new System.EventHandler(this.chkAddress_CheckedChanged);
			// 
			// txtCorrespondZip4
			// 
			this.txtCorrespondZip4.AutoSize = false;
			this.txtCorrespondZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtCorrespondZip4.CharacterCasing = CharacterCasing.Upper;
            this.txtCorrespondZip4.LinkItem = null;
			this.txtCorrespondZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorrespondZip4.LinkTopic = null;
			this.txtCorrespondZip4.Location = new System.Drawing.Point(407, 264);
			this.txtCorrespondZip4.MaxLength = 4;
			this.txtCorrespondZip4.Name = "txtCorrespondZip4";
			this.txtCorrespondZip4.Size = new System.Drawing.Size(75, 40);
			this.txtCorrespondZip4.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtCorrespondZip4, null);
			this.txtCorrespondZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCorrespondZip4_KeyPress);
			// 
			// txtCorrespondState
			// 
			this.txtCorrespondState.AutoSize = false;
			this.txtCorrespondState.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorrespondState.LinkItem = null;
			this.txtCorrespondState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorrespondState.LinkTopic = null;
			this.txtCorrespondState.Location = new System.Drawing.Point(261, 264);
			this.txtCorrespondState.MaxLength = 2;
			this.txtCorrespondState.Name = "txtCorrespondState";
			this.txtCorrespondState.Size = new System.Drawing.Size(63, 40);
			this.txtCorrespondState.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtCorrespondState, null);
			// 
			// txtCorrespondCity
			// 
			this.txtCorrespondCity.AutoSize = false;
			this.txtCorrespondCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorrespondCity.LinkItem = null;
			this.txtCorrespondCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorrespondCity.LinkTopic = null;
			this.txtCorrespondCity.Location = new System.Drawing.Point(98, 264);
			this.txtCorrespondCity.MaxLength = 35;
			this.txtCorrespondCity.Name = "txtCorrespondCity";
			this.txtCorrespondCity.Size = new System.Drawing.Size(160, 40);
			this.txtCorrespondCity.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtCorrespondCity, null);
			// 
			// txtCorName
			// 
			this.txtCorName.AutoSize = false;
			this.txtCorName.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorName.LinkItem = null;
			this.txtCorName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorName.LinkTopic = null;
			this.txtCorName.Location = new System.Drawing.Point(98, 64);
			this.txtCorName.MaxLength = 50;
			this.txtCorName.Name = "txtCorName";
			this.txtCorName.Size = new System.Drawing.Size(384, 40);
			this.txtCorName.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtCorName, null);
			this.txtCorName.TextChanged += new System.EventHandler(this.txtCorName_TextChanged);
			// 
			// txtCorAddress_0
			// 
			this.txtCorAddress_0.AutoSize = false;
			this.txtCorAddress_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorAddress_0.LinkItem = null;
			this.txtCorAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorAddress_0.LinkTopic = null;
			this.txtCorAddress_0.Location = new System.Drawing.Point(98, 114);
			this.txtCorAddress_0.MaxLength = 35;
			this.txtCorAddress_0.Name = "txtCorAddress_0";
			this.txtCorAddress_0.Size = new System.Drawing.Size(384, 40);
			this.txtCorAddress_0.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtCorAddress_0, null);
			this.txtCorAddress_0.TextChanged += new System.EventHandler(this.txtCorAddress_TextChanged);
			// 
			// txtCorAddress_1
			// 
			this.txtCorAddress_1.AutoSize = false;
			this.txtCorAddress_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorAddress_1.LinkItem = null;
			this.txtCorAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorAddress_1.LinkTopic = null;
			this.txtCorAddress_1.Location = new System.Drawing.Point(98, 164);
			this.txtCorAddress_1.MaxLength = 35;
			this.txtCorAddress_1.Name = "txtCorAddress_1";
			this.txtCorAddress_1.Size = new System.Drawing.Size(384, 40);
			this.txtCorAddress_1.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtCorAddress_1, null);
			this.txtCorAddress_1.TextChanged += new System.EventHandler(this.txtCorAddress_TextChanged);
			// 
			// txtCorAddress_2
			// 
			this.txtCorAddress_2.AutoSize = false;
			this.txtCorAddress_2.BackColor = System.Drawing.SystemColors.Window;
			this.txtCorAddress_2.LinkItem = null;
			this.txtCorAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorAddress_2.LinkTopic = null;
			this.txtCorAddress_2.Location = new System.Drawing.Point(98, 214);
			this.txtCorAddress_2.MaxLength = 35;
			this.txtCorAddress_2.Name = "txtCorAddress_2";
			this.txtCorAddress_2.Size = new System.Drawing.Size(384, 40);
			this.txtCorAddress_2.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtCorAddress_2, "This line will not print on 1099 forms.");
			this.txtCorAddress_2.TextChanged += new System.EventHandler(this.txtCorAddress_TextChanged);
			// 
			// txtCorrespondZip
			// 
			this.txtCorrespondZip.AutoSize = false;
			this.txtCorrespondZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtCorrespondZip.CharacterCasing = CharacterCasing.Upper;
            this.txtCorrespondZip.LinkItem = null;
			this.txtCorrespondZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCorrespondZip.LinkTopic = null;
			this.txtCorrespondZip.Location = new System.Drawing.Point(327, 264);
			this.txtCorrespondZip.MaxLength = 5;
			this.txtCorrespondZip.Name = "txtCorrespondZip";
			this.txtCorrespondZip.Size = new System.Drawing.Size(78, 40);
			this.txtCorrespondZip.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtCorrespondZip, null);
			this.txtCorrespondZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCorrespondZip_KeyPress);
			// 
			// lblCorrespondCityStateZip
			// 
			this.lblCorrespondCityStateZip.Location = new System.Drawing.Point(20, 278);
			this.lblCorrespondCityStateZip.Name = "lblCorrespondCityStateZip";
			this.lblCorrespondCityStateZip.Size = new System.Drawing.Size(32, 16);
			this.lblCorrespondCityStateZip.TabIndex = 7;
			this.lblCorrespondCityStateZip.Text = "C/S/Z";
			this.ToolTip1.SetToolTip(this.lblCorrespondCityStateZip, null);
			// 
			// lblCorName
			// 
			this.lblCorName.Location = new System.Drawing.Point(20, 78);
			this.lblCorName.Name = "lblCorName";
			this.lblCorName.Size = new System.Drawing.Size(38, 16);
			this.lblCorName.TabIndex = 1;
			this.lblCorName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblCorName, null);
			// 
			// lblAddress
			// 
			this.lblAddress.Location = new System.Drawing.Point(20, 128);
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Size = new System.Drawing.Size(58, 16);
			this.lblAddress.TabIndex = 3;
			this.lblAddress.Text = "ADDRESS";
			this.ToolTip1.SetToolTip(this.lblAddress, null);
			// 
			// txtEmail
			// 
			this.txtEmail.AutoSize = false;
			this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmail.LinkItem = null;
			this.txtEmail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEmail.LinkTopic = null;
			this.txtEmail.Location = new System.Drawing.Point(199, 680);
			this.txtEmail.MaxLength = 50;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(412, 40);
			this.txtEmail.TabIndex = 24;
			this.ToolTip1.SetToolTip(this.txtEmail, null);
			// 
			// chkOneTime
			// 
			this.chkOneTime.AutoSize = false;
			this.chkOneTime.Location = new System.Drawing.Point(199, 489);
			this.chkOneTime.Name = "chkOneTime";
			this.chkOneTime.Size = new System.Drawing.Size(222, 24);
			this.chkOneTime.TabIndex = 15;
			this.chkOneTime.Text = "One Time Check Message";
			this.ToolTip1.SetToolTip(this.chkOneTime, null);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.vs1);
			this.Frame3.Controls.Add(this.txtDefaultDescription);
			this.Frame3.Controls.Add(this.txtDefaultCheckNumber);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.lblTitle);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Location = new System.Drawing.Point(339, 744);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(707, 316);
			this.Frame3.TabIndex = 29;
			this.Frame3.Text = "Default Information";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(20, 104);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 51;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(671, 192);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 47;
			this.ToolTip1.SetToolTip(this.vs1, null);
			this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vs1_MouseDownEvent);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// txtDefaultDescription
			// 
			this.txtDefaultDescription.AutoSize = false;
			this.txtDefaultDescription.LinkItem = null;
			this.txtDefaultDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDefaultDescription.LinkTopic = null;
			this.txtDefaultDescription.Location = new System.Drawing.Point(119, 30);
			this.txtDefaultDescription.MaxLength = 25;
			this.txtDefaultDescription.Name = "txtDefaultDescription";
			this.txtDefaultDescription.Size = new System.Drawing.Size(327, 40);
			this.txtDefaultDescription.TabIndex = 45;
			this.ToolTip1.SetToolTip(this.txtDefaultDescription, null);
			this.txtDefaultDescription.TextChanged += new System.EventHandler(this.txtDefaultDescription_Change);
			// 
			// txtDefaultCheckNumber
			// 
			this.txtDefaultCheckNumber.AutoSize = false;
			this.txtDefaultCheckNumber.LinkItem = null;
			this.txtDefaultCheckNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDefaultCheckNumber.LinkTopic = null;
			this.txtDefaultCheckNumber.Location = new System.Drawing.Point(546, 30);
			this.txtDefaultCheckNumber.MaxLength = 25;
			this.txtDefaultCheckNumber.Name = "txtDefaultCheckNumber";
			this.txtDefaultCheckNumber.Size = new System.Drawing.Size(134, 40);
			this.txtDefaultCheckNumber.TabIndex = 46;
			this.ToolTip1.SetToolTip(this.txtDefaultCheckNumber, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(475, 44);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(52, 16);
			this.Label4.TabIndex = 76;
			this.Label4.Text = "CHECK #";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(20, 80);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(414, 14);
			this.lblTitle.TabIndex = 73;
			this.ToolTip1.SetToolTip(this.lblTitle, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(78, 16);
			this.Label1.TabIndex = 72;
			this.Label1.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// txtDataMessage
			// 
			this.txtDataMessage.AutoSize = false;
			this.txtDataMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtDataMessage.LinkItem = null;
			this.txtDataMessage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDataMessage.LinkTopic = null;
			this.txtDataMessage.Location = new System.Drawing.Point(199, 630);
			this.txtDataMessage.MaxLength = 60;
			this.txtDataMessage.Name = "txtDataMessage";
			this.txtDataMessage.Size = new System.Drawing.Size(848, 40);
			this.txtDataMessage.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.txtDataMessage, null);
			this.txtDataMessage.TextChanged += new System.EventHandler(this.txtDataMessage_TextChanged);
			// 
			// txtCheckMessage
			// 
			this.txtCheckMessage.AutoSize = false;
			this.txtCheckMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckMessage.LinkItem = null;
			this.txtCheckMessage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheckMessage.LinkTopic = null;
			this.txtCheckMessage.Location = new System.Drawing.Point(199, 530);
			this.txtCheckMessage.MaxLength = 60;
			this.txtCheckMessage.Name = "txtCheckMessage";
			this.txtCheckMessage.Size = new System.Drawing.Size(848, 40);
			this.txtCheckMessage.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.txtCheckMessage, null);
			this.txtCheckMessage.TextChanged += new System.EventHandler(this.txtCheckMessage_TextChanged);
			// 
			// txtExtension
			// 
			this.txtExtension.AutoSize = false;
			this.txtExtension.BackColor = System.Drawing.SystemColors.Window;
			this.txtExtension.LinkItem = null;
			this.txtExtension.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtExtension.LinkTopic = null;
			this.txtExtension.Location = new System.Drawing.Point(932, 80);
			this.txtExtension.MaxLength = 7;
			this.txtExtension.Name = "txtExtension";
			this.txtExtension.Size = new System.Drawing.Size(104, 40);
			this.txtExtension.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtExtension, null);
			this.txtExtension.TextChanged += new System.EventHandler(this.txtExtension_TextChanged);
			// 
			// frame4
			// 
			this.frame4.Controls.Add(this.cboCode_4);
			this.frame4.Controls.Add(this.cboCode_3);
			this.frame4.Controls.Add(this.cboCode_2);
			this.frame4.Controls.Add(this.cboCode_1);
			this.frame4.Controls.Add(this.cboCode_0);
			this.frame4.Controls.Add(this.txtAdjustment_0);
			this.frame4.Controls.Add(this.txtAdjustment_1);
			this.frame4.Controls.Add(this.txtAdjustment_2);
			this.frame4.Controls.Add(this.txtAdjustment_3);
			this.frame4.Controls.Add(this.txtAdjustment_4);
			this.frame4.Controls.Add(this.lblPosNegValue_4);
			this.frame4.Controls.Add(this.lblPosNegValue_3);
			this.frame4.Controls.Add(this.lblPosNegValue_2);
			this.frame4.Controls.Add(this.lblPosNegValue_1);
			this.frame4.Controls.Add(this.lblPosNegValue_0);
			this.frame4.Controls.Add(this.lblAdjNumber_4);
			this.frame4.Controls.Add(this.lblAdjNumber_3);
			this.frame4.Controls.Add(this.lblAdjNumber_2);
			this.frame4.Controls.Add(this.lblAdjNumber_1);
			this.frame4.Controls.Add(this.lblAdjNumber_0);
			this.frame4.Controls.Add(this.lblPosOrNeg);
			this.frame4.Controls.Add(this.lblCode);
			this.frame4.Controls.Add(this.lblAdjustment);
			this.frame4.Location = new System.Drawing.Point(30, 744);
			this.frame4.Name = "frame4";
			this.frame4.Size = new System.Drawing.Size(291, 316);
			this.frame4.TabIndex = 29;
			this.frame4.Text = "1099 Adjustments";
			this.ToolTip1.SetToolTip(this.frame4, null);
			// 
			// cboCode_4
			// 
			this.cboCode_4.AutoSize = false;
			this.cboCode_4.BackColor = System.Drawing.SystemColors.Window;
			this.cboCode_4.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCode_4.FormattingEnabled = true;
			this.cboCode_4.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboCode_4.Location = new System.Drawing.Point(184, 256);
			this.cboCode_4.Name = "cboCode_4";
			this.cboCode_4.Size = new System.Drawing.Size(88, 40);
			this.cboCode_4.TabIndex = 22;
			this.ToolTip1.SetToolTip(this.cboCode_4, null);
			this.cboCode_4.SelectedIndexChanged += new System.EventHandler(this.cboCode_SelectedIndexChanged);
			this.cboCode_4.DropDown += new System.EventHandler(this.cboCode_DropDown);
			// 
			// cboCode_3
			// 
			this.cboCode_3.AutoSize = false;
			this.cboCode_3.BackColor = System.Drawing.SystemColors.Window;
			this.cboCode_3.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCode_3.FormattingEnabled = true;
			this.cboCode_3.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboCode_3.Location = new System.Drawing.Point(184, 206);
			this.cboCode_3.Name = "cboCode_3";
			this.cboCode_3.Size = new System.Drawing.Size(88, 40);
			this.cboCode_3.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.cboCode_3, null);
			this.cboCode_3.SelectedIndexChanged += new System.EventHandler(this.cboCode_SelectedIndexChanged);
			this.cboCode_3.DropDown += new System.EventHandler(this.cboCode_DropDown);
			// 
			// cboCode_2
			// 
			this.cboCode_2.AutoSize = false;
			this.cboCode_2.BackColor = System.Drawing.SystemColors.Window;
			this.cboCode_2.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCode_2.FormattingEnabled = true;
			this.cboCode_2.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboCode_2.Location = new System.Drawing.Point(184, 156);
			this.cboCode_2.Name = "cboCode_2";
			this.cboCode_2.Size = new System.Drawing.Size(88, 40);
			this.cboCode_2.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.cboCode_2, null);
			this.cboCode_2.SelectedIndexChanged += new System.EventHandler(this.cboCode_SelectedIndexChanged);
			this.cboCode_2.DropDown += new System.EventHandler(this.cboCode_DropDown);
			// 
			// cboCode_1
			// 
			this.cboCode_1.AutoSize = false;
			this.cboCode_1.BackColor = System.Drawing.SystemColors.Window;
			this.cboCode_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCode_1.FormattingEnabled = true;
			this.cboCode_1.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboCode_1.Location = new System.Drawing.Point(184, 106);
			this.cboCode_1.Name = "cboCode_1";
			this.cboCode_1.Size = new System.Drawing.Size(88, 40);
			this.cboCode_1.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.cboCode_1, null);
			this.cboCode_1.SelectedIndexChanged += new System.EventHandler(this.cboCode_SelectedIndexChanged);
			this.cboCode_1.DropDown += new System.EventHandler(this.cboCode_DropDown);
			// 
			// cboCode_0
			// 
			this.cboCode_0.AutoSize = false;
			this.cboCode_0.BackColor = System.Drawing.SystemColors.Window;
			this.cboCode_0.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCode_0.FormattingEnabled = true;
			this.cboCode_0.Items.AddRange(new object[] {
				" ",
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9"
			});
			this.cboCode_0.Location = new System.Drawing.Point(184, 56);
			this.cboCode_0.Name = "cboCode_0";
			this.cboCode_0.Size = new System.Drawing.Size(88, 40);
			this.cboCode_0.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.cboCode_0, null);
			this.cboCode_0.SelectedIndexChanged += new System.EventHandler(this.cboCode_SelectedIndexChanged);
			this.cboCode_0.DropDown += new System.EventHandler(this.cboCode_DropDown);
			// 
			// txtAdjustment_0
			// 
			this.txtAdjustment_0.Location = new System.Drawing.Point(40, 56);
			this.txtAdjustment_0.MaxLength = 10;
			this.txtAdjustment_0.Name = "txtAdjustment_0";
			this.txtAdjustment_0.Size = new System.Drawing.Size(117, 40);
			this.txtAdjustment_0.TabIndex = 4;
			this.txtAdjustment_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAdjustment_0, null);
			this.txtAdjustment_0.TextChanged += new System.EventHandler(this.txtAdjustment_Change);
			// 
			// txtAdjustment_1
			// 
			this.txtAdjustment_1.Location = new System.Drawing.Point(40, 106);
			this.txtAdjustment_1.MaxLength = 10;
			this.txtAdjustment_1.Name = "txtAdjustment_1";
			this.txtAdjustment_1.Size = new System.Drawing.Size(117, 40);
			this.txtAdjustment_1.TabIndex = 8;
			this.txtAdjustment_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAdjustment_1, null);
			this.txtAdjustment_1.TextChanged += new System.EventHandler(this.txtAdjustment_Change);
			// 
			// txtAdjustment_2
			// 
			this.txtAdjustment_2.Location = new System.Drawing.Point(40, 156);
			this.txtAdjustment_2.MaxLength = 10;
			this.txtAdjustment_2.Name = "txtAdjustment_2";
			this.txtAdjustment_2.Size = new System.Drawing.Size(117, 40);
			this.txtAdjustment_2.TabIndex = 12;
			this.txtAdjustment_2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAdjustment_2, null);
			this.txtAdjustment_2.TextChanged += new System.EventHandler(this.txtAdjustment_Change);
			// 
			// txtAdjustment_3
			// 
			this.txtAdjustment_3.Location = new System.Drawing.Point(40, 206);
			this.txtAdjustment_3.MaxLength = 10;
			this.txtAdjustment_3.Name = "txtAdjustment_3";
			this.txtAdjustment_3.Size = new System.Drawing.Size(117, 40);
			this.txtAdjustment_3.TabIndex = 16;
			this.txtAdjustment_3.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAdjustment_3, null);
			this.txtAdjustment_3.TextChanged += new System.EventHandler(this.txtAdjustment_Change);
			// 
			// txtAdjustment_4
			// 
			this.txtAdjustment_4.Location = new System.Drawing.Point(40, 256);
			this.txtAdjustment_4.MaxLength = 10;
			this.txtAdjustment_4.Name = "txtAdjustment_4";
			this.txtAdjustment_4.Size = new System.Drawing.Size(117, 40);
			this.txtAdjustment_4.TabIndex = 20;
			this.txtAdjustment_4.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAdjustment_4, null);
			this.txtAdjustment_4.TextChanged += new System.EventHandler(this.txtAdjustment_Change);
			// 
			// lblPosNegValue_4
			// 
			this.lblPosNegValue_4.BackColor = System.Drawing.SystemColors.HighlightText;
			this.lblPosNegValue_4.Location = new System.Drawing.Point(163, 270);
			this.lblPosNegValue_4.Name = "lblPosNegValue_4";
			this.lblPosNegValue_4.Size = new System.Drawing.Size(15, 16);
			this.lblPosNegValue_4.TabIndex = 21;
			this.lblPosNegValue_4.Text = "+";
			this.lblPosNegValue_4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPosNegValue_4, null);
			this.lblPosNegValue_4.Click += new System.EventHandler(this.lblPosNegValue_Click);
			this.lblPosNegValue_4.TextChanged += new System.EventHandler(this.lblPosNegValue_TextChanged);
			// 
			// lblPosNegValue_3
			// 
			this.lblPosNegValue_3.BackColor = System.Drawing.SystemColors.HighlightText;
			this.lblPosNegValue_3.Location = new System.Drawing.Point(163, 220);
			this.lblPosNegValue_3.Name = "lblPosNegValue_3";
			this.lblPosNegValue_3.Size = new System.Drawing.Size(15, 16);
			this.lblPosNegValue_3.TabIndex = 17;
			this.lblPosNegValue_3.Text = "+";
			this.lblPosNegValue_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPosNegValue_3, null);
			this.lblPosNegValue_3.Click += new System.EventHandler(this.lblPosNegValue_Click);
			this.lblPosNegValue_3.TextChanged += new System.EventHandler(this.lblPosNegValue_TextChanged);
			// 
			// lblPosNegValue_2
			// 
			this.lblPosNegValue_2.BackColor = System.Drawing.SystemColors.HighlightText;
			this.lblPosNegValue_2.Location = new System.Drawing.Point(163, 170);
			this.lblPosNegValue_2.Name = "lblPosNegValue_2";
			this.lblPosNegValue_2.Size = new System.Drawing.Size(15, 16);
			this.lblPosNegValue_2.TabIndex = 13;
			this.lblPosNegValue_2.Text = "+";
			this.lblPosNegValue_2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPosNegValue_2, null);
			this.lblPosNegValue_2.Click += new System.EventHandler(this.lblPosNegValue_Click);
			this.lblPosNegValue_2.TextChanged += new System.EventHandler(this.lblPosNegValue_TextChanged);
			// 
			// lblPosNegValue_1
			// 
			this.lblPosNegValue_1.BackColor = System.Drawing.SystemColors.HighlightText;
			this.lblPosNegValue_1.Location = new System.Drawing.Point(163, 120);
			this.lblPosNegValue_1.Name = "lblPosNegValue_1";
			this.lblPosNegValue_1.Size = new System.Drawing.Size(15, 16);
			this.lblPosNegValue_1.TabIndex = 9;
			this.lblPosNegValue_1.Text = "+";
			this.lblPosNegValue_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPosNegValue_1, null);
			this.lblPosNegValue_1.Click += new System.EventHandler(this.lblPosNegValue_Click);
			this.lblPosNegValue_1.TextChanged += new System.EventHandler(this.lblPosNegValue_TextChanged);
			// 
			// lblPosNegValue_0
			// 
			this.lblPosNegValue_0.BackColor = System.Drawing.SystemColors.HighlightText;
			this.lblPosNegValue_0.Location = new System.Drawing.Point(163, 70);
			this.lblPosNegValue_0.Name = "lblPosNegValue_0";
			this.lblPosNegValue_0.Size = new System.Drawing.Size(15, 16);
			this.lblPosNegValue_0.TabIndex = 5;
			this.lblPosNegValue_0.Text = "+";
			this.lblPosNegValue_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPosNegValue_0, null);
			this.lblPosNegValue_0.Click += new System.EventHandler(this.lblPosNegValue_Click);
			this.lblPosNegValue_0.TextChanged += new System.EventHandler(this.lblPosNegValue_TextChanged);
			// 
			// lblAdjNumber_4
			// 
			this.lblAdjNumber_4.Location = new System.Drawing.Point(20, 270);
			this.lblAdjNumber_4.Name = "lblAdjNumber_4";
			this.lblAdjNumber_4.Size = new System.Drawing.Size(10, 16);
			this.lblAdjNumber_4.TabIndex = 19;
			this.lblAdjNumber_4.Text = "5.";
			this.ToolTip1.SetToolTip(this.lblAdjNumber_4, null);
			// 
			// lblAdjNumber_3
			// 
			this.lblAdjNumber_3.Location = new System.Drawing.Point(20, 220);
			this.lblAdjNumber_3.Name = "lblAdjNumber_3";
			this.lblAdjNumber_3.Size = new System.Drawing.Size(10, 16);
			this.lblAdjNumber_3.TabIndex = 15;
			this.lblAdjNumber_3.Text = "4.";
			this.ToolTip1.SetToolTip(this.lblAdjNumber_3, null);
			// 
			// lblAdjNumber_2
			// 
			this.lblAdjNumber_2.Location = new System.Drawing.Point(20, 170);
			this.lblAdjNumber_2.Name = "lblAdjNumber_2";
			this.lblAdjNumber_2.Size = new System.Drawing.Size(10, 16);
			this.lblAdjNumber_2.TabIndex = 11;
			this.lblAdjNumber_2.Text = "3.";
			this.ToolTip1.SetToolTip(this.lblAdjNumber_2, null);
			// 
			// lblAdjNumber_1
			// 
			this.lblAdjNumber_1.Location = new System.Drawing.Point(20, 120);
			this.lblAdjNumber_1.Name = "lblAdjNumber_1";
			this.lblAdjNumber_1.Size = new System.Drawing.Size(10, 16);
			this.lblAdjNumber_1.TabIndex = 7;
			this.lblAdjNumber_1.Text = "2.";
			this.ToolTip1.SetToolTip(this.lblAdjNumber_1, null);
			// 
			// lblAdjNumber_0
			// 
			this.lblAdjNumber_0.Location = new System.Drawing.Point(20, 70);
			this.lblAdjNumber_0.Name = "lblAdjNumber_0";
			this.lblAdjNumber_0.Size = new System.Drawing.Size(10, 16);
			this.lblAdjNumber_0.TabIndex = 3;
			this.lblAdjNumber_0.Text = "1.";
			this.ToolTip1.SetToolTip(this.lblAdjNumber_0, null);
			// 
			// lblPosOrNeg
			// 
			this.lblPosOrNeg.Location = new System.Drawing.Point(163, 30);
			this.lblPosOrNeg.Name = "lblPosOrNeg";
			this.lblPosOrNeg.Size = new System.Drawing.Size(20, 16);
			this.lblPosOrNeg.TabIndex = 1;
			this.lblPosOrNeg.Text = "+/--";
			this.ToolTip1.SetToolTip(this.lblPosOrNeg, null);
			// 
			// lblCode
			// 
			this.lblCode.Location = new System.Drawing.Point(206, 30);
			this.lblCode.Name = "lblCode";
			this.lblCode.Size = new System.Drawing.Size(38, 16);
			this.lblCode.TabIndex = 2;
			this.lblCode.Text = "CODE";
			this.ToolTip1.SetToolTip(this.lblCode, null);
			// 
			// lblAdjustment
			// 
			this.lblAdjustment.Location = new System.Drawing.Point(43, 30);
			this.lblAdjustment.Name = "lblAdjustment";
			this.lblAdjustment.Size = new System.Drawing.Size(80, 16);
			this.lblAdjustment.TabIndex = 0;
			this.lblAdjustment.Text = "ADJUSTMENT";
			this.ToolTip1.SetToolTip(this.lblAdjustment, null);
			// 
			// txtContact
			// 
			this.txtContact.AutoSize = false;
			this.txtContact.BackColor = System.Drawing.SystemColors.Window;
			this.txtContact.LinkItem = null;
			this.txtContact.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtContact.LinkTopic = null;
			this.txtContact.Location = new System.Drawing.Point(667, 30);
			this.txtContact.MaxLength = 30;
			this.txtContact.Name = "txtContact";
			this.txtContact.Size = new System.Drawing.Size(369, 40);
			this.txtContact.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtContact, null);
			this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
			// 
			// txtStatus
			// 
			this.txtStatus.AutoSize = false;
			this.txtStatus.BackColor = System.Drawing.SystemColors.Window;
			this.txtStatus.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
			this.javaScript1.GetJavaScriptEvents(this.txtStatus).Add(clientEvent1);
			this.txtStatus.LinkItem = null;
			this.txtStatus.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStatus.LinkTopic = null;
			this.txtStatus.Location = new System.Drawing.Point(355, 17);
			this.txtStatus.MaxLength = 1;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Size = new System.Drawing.Size(40, 40);
			this.txtStatus.TabIndex = 2;
			this.txtStatus.Text = "A";
			this.ToolTip1.SetToolTip(this.txtStatus, "A - Active  D - Deleted  S - Suspended");
			this.txtStatus.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStatus_KeyPress);
			this.txtStatus.TextChanged += new System.EventHandler(this.txtStatus_TextChanged);
			this.txtStatus.Validating += new System.ComponentModel.CancelEventHandler(this.txtStatus_Validating);
			// 
			// txtClass
			// 
			this.txtClass.AutoSize = false;
			this.txtClass.BackColor = System.Drawing.SystemColors.Window;
			this.txtClass.LinkItem = null;
			this.txtClass.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtClass.LinkTopic = null;
			this.txtClass.Location = new System.Drawing.Point(932, 130);
			this.txtClass.MaxLength = 6;
			this.txtClass.Name = "txtClass";
			this.txtClass.Size = new System.Drawing.Size(104, 40);
			this.txtClass.TabIndex = 12;
			this.txtClass.Text = "1";
			this.ToolTip1.SetToolTip(this.txtClass, null);
			this.txtClass.TextChanged += new System.EventHandler(this.txtClass_TextChanged);
			this.txtClass.Validating += new System.ComponentModel.CancelEventHandler(this.txtClass_Validating);
			// 
			// txtMessage
			// 
			this.txtMessage.AutoSize = false;
			this.txtMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtMessage.LinkItem = null;
			this.txtMessage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMessage.LinkTopic = null;
			this.txtMessage.Location = new System.Drawing.Point(199, 580);
			this.txtMessage.MaxLength = 60;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(848, 40);
			this.txtMessage.TabIndex = 20;
			this.ToolTip1.SetToolTip(this.txtMessage, null);
			this.txtMessage.TextChanged += new System.EventHandler(this.txtMessage_TextChanged);
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(667, 80);
			this.txtPhone.Mask = "(999)000-0000";
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Size = new System.Drawing.Size(156, 40);
			this.txtPhone.TabIndex = 6;
			this.txtPhone.Text = "(207) 000-0000";
			this.ToolTip1.SetToolTip(this.txtPhone, null);
			this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_Change);
			// 
			// txtFax
			// 
			this.txtFax.Location = new System.Drawing.Point(667, 130);
			this.txtFax.Mask = "(999)000-0000";
			this.txtFax.Name = "txtFax";
			this.txtFax.Size = new System.Drawing.Size(156, 40);
			this.txtFax.TabIndex = 10;
			this.txtFax.Text = "(207) 000-0000";
			this.ToolTip1.SetToolTip(this.txtFax, null);
			this.txtFax.TextChanged += new System.EventHandler(this.txtFax_Change);
			// 
			// lblFaxNumber
			// 
			this.lblFaxNumber.Location = new System.Drawing.Point(572, 144);
			this.lblFaxNumber.Name = "lblFaxNumber";
			this.lblFaxNumber.Size = new System.Drawing.Size(28, 16);
			this.lblFaxNumber.TabIndex = 9;
			this.lblFaxNumber.Text = "FAX";
			this.ToolTip1.SetToolTip(this.lblFaxNumber, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 694);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(97, 16);
			this.Label2.TabIndex = 23;
			this.Label2.Text = "E-MAIL ADDRESS";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// lblCheck
			// 
			this.lblCheck.Location = new System.Drawing.Point(905, 694);
			this.lblCheck.Name = "lblCheck";
			this.lblCheck.Size = new System.Drawing.Size(53, 16);
			this.lblCheck.TabIndex = 27;
			this.lblCheck.Text = "CHECK #";
			this.ToolTip1.SetToolTip(this.lblCheck, null);
			// 
			// lblLastPayment
			// 
			this.lblLastPayment.Location = new System.Drawing.Point(638, 694);
			this.lblLastPayment.Name = "lblLastPayment";
			this.lblLastPayment.Size = new System.Drawing.Size(90, 16);
			this.lblLastPayment.TabIndex = 25;
			this.lblLastPayment.Text = "LAST PAYMENT";
			this.ToolTip1.SetToolTip(this.lblLastPayment, null);
			// 
			// lblDataMessage
			// 
			this.lblDataMessage.Location = new System.Drawing.Point(30, 644);
			this.lblDataMessage.Name = "lblDataMessage";
			this.lblDataMessage.Size = new System.Drawing.Size(134, 16);
			this.lblDataMessage.TabIndex = 21;
			this.lblDataMessage.Text = "DATA ENTRY MESSAGE";
			this.ToolTip1.SetToolTip(this.lblDataMessage, null);
			// 
			// lblCheckMessage
			// 
			this.lblCheckMessage.Location = new System.Drawing.Point(30, 544);
			this.lblCheckMessage.Name = "lblCheckMessage";
			this.lblCheckMessage.Size = new System.Drawing.Size(102, 16);
			this.lblCheckMessage.TabIndex = 17;
			this.lblCheckMessage.Text = "CHECK MESSAGE";
			this.ToolTip1.SetToolTip(this.lblCheckMessage, null);
			// 
			// lblExtension
			// 
			this.lblExtension.Location = new System.Drawing.Point(857, 94);
			this.lblExtension.Name = "lblExtension";
			this.lblExtension.Size = new System.Drawing.Size(25, 16);
			this.lblExtension.TabIndex = 7;
			this.lblExtension.Text = "EXT";
			this.ToolTip1.SetToolTip(this.lblExtension, null);
			// 
			// lblContact
			// 
			this.lblContact.Location = new System.Drawing.Point(572, 44);
			this.lblContact.Name = "lblContact";
			this.lblContact.Size = new System.Drawing.Size(60, 16);
			this.lblContact.TabIndex = 3;
			this.lblContact.Text = "CONTACT";
			this.ToolTip1.SetToolTip(this.lblContact, null);
			// 
			// lblPhoneNumber
			// 
			this.lblPhoneNumber.Location = new System.Drawing.Point(572, 94);
			this.lblPhoneNumber.Name = "lblPhoneNumber";
			this.lblPhoneNumber.Size = new System.Drawing.Size(25, 16);
			this.lblPhoneNumber.TabIndex = 5;
			this.lblPhoneNumber.Text = "TEL";
			this.ToolTip1.SetToolTip(this.lblPhoneNumber, null);
			// 
			// lblStatus
			// 
			this.lblStatus.Location = new System.Drawing.Point(293, 31);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(48, 16);
			this.lblStatus.TabIndex = 1;
			this.lblStatus.Text = "STATUS";
			this.ToolTip1.SetToolTip(this.lblStatus, null);
			// 
			// lblClass
			// 
			this.lblClass.Location = new System.Drawing.Point(857, 144);
			this.lblClass.Name = "lblClass";
			this.lblClass.Size = new System.Drawing.Size(40, 16);
			this.lblClass.TabIndex = 11;
			this.lblClass.Text = "CLASS";
			this.ToolTip1.SetToolTip(this.lblClass, null);
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(30, 594);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(60, 16);
			this.lblMessage.TabIndex = 19;
			this.lblMessage.Text = "MESSAGE";
			this.ToolTip1.SetToolTip(this.lblMessage, null);
			// 
			// lblRecordNumber
			// 
			this.lblRecordNumber.AppearanceKey = "Header";
			this.lblRecordNumber.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblRecordNumber.Location = new System.Drawing.Point(186, 26);
			this.lblRecordNumber.Name = "lblRecordNumber";
			this.lblRecordNumber.Size = new System.Drawing.Size(88, 30);
			this.lblRecordNumber.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.lblRecordNumber, null);
			// 
			// lblLastPaymentDate
			// 
			this.lblLastPaymentDate.Location = new System.Drawing.Point(741, 694);
			this.lblLastPaymentDate.Name = "lblLastPaymentDate";
			this.lblLastPaymentDate.Size = new System.Drawing.Size(125, 16);
			this.lblLastPaymentDate.TabIndex = 26;
			this.ToolTip1.SetToolTip(this.lblLastPaymentDate, null);
			// 
			// lblCheckNumber
			// 
			this.lblCheckNumber.Location = new System.Drawing.Point(971, 694);
			this.lblCheckNumber.Name = "lblCheckNumber";
			this.lblCheckNumber.Size = new System.Drawing.Size(76, 16);
			this.lblCheckNumber.TabIndex = 28;
			this.ToolTip1.SetToolTip(this.lblCheckNumber, null);
			// 
			// mnuProcessDelete
			// 
			this.mnuProcessDelete.Index = -1;
			this.mnuProcessDelete.Name = "mnuProcessDelete";
			this.mnuProcessDelete.Text = "Delete Vendor";
			this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// mnuFileViewLinkedDocument
			// 
			this.mnuFileViewLinkedDocument.Index = -1;
			this.mnuFileViewLinkedDocument.Name = "mnuFileViewLinkedDocument";
			this.mnuFileViewLinkedDocument.Text = "Attached Documents";
			this.mnuFileViewLinkedDocument.Click += new System.EventHandler(this.mnuFileViewLinkedDocument_Click);
			// 
			// mnuViewInvoices
			// 
			this.mnuViewInvoices.Index = -1;
			this.mnuViewInvoices.Name = "mnuViewInvoices";
			this.mnuViewInvoices.Text = "Invoices";
			this.mnuViewInvoices.Click += new System.EventHandler(this.mnuViewInvoices_Click);
			// 
			// mnuSep23
			// 
			this.mnuSep23.Index = -1;
			this.mnuSep23.Name = "mnuSep23";
			this.mnuSep23.Text = "-";
			// 
			// mnuFilePrintLabel
			// 
			this.mnuFilePrintLabel.Enabled = false;
			this.mnuFilePrintLabel.Index = -1;
			this.mnuFilePrintLabel.Name = "mnuFilePrintLabel";
			this.mnuFilePrintLabel.Text = "Print Mailing Label";
			this.mnuFilePrintLabel.Click += new System.EventHandler(this.mnuFilePrintLabel_Click);
			// 
			// mnuRows
			// 
			this.mnuRows.Index = -1;
			this.mnuRows.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRowsDeleteRow
			});
			this.mnuRows.Name = "mnuRows";
			this.mnuRows.Text = "Rows";
			this.mnuRows.Visible = false;
			// 
			// mnuRowsDeleteRow
			// 
			this.mnuRowsDeleteRow.Index = 0;
			this.mnuRowsDeleteRow.Name = "mnuRowsDeleteRow";
			this.mnuRowsDeleteRow.Text = "Delete Row";
			this.mnuRowsDeleteRow.Click += new System.EventHandler(this.mnuRowsDeleteRow_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(428, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(102, 48);
			this.btnProcess.TabIndex = 2;
			this.btnProcess.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdDeleteVendor
			// 
			this.cmdDeleteVendor.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteVendor.AppearanceKey = "toolbarButton";
			this.cmdDeleteVendor.Location = new System.Drawing.Point(606, 29);
			this.cmdDeleteVendor.Name = "cmdDeleteVendor";
			this.cmdDeleteVendor.Size = new System.Drawing.Size(104, 24);
			this.cmdDeleteVendor.TabIndex = 2;
			this.cmdDeleteVendor.Text = "Delete Vendor";
			this.ToolTip1.SetToolTip(this.cmdDeleteVendor, null);
			this.cmdDeleteVendor.Click += new System.EventHandler(this.cmdDeleteVendor_Click);
			// 
			// cmdPrintMailingLabel
			// 
			this.cmdPrintMailingLabel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintMailingLabel.AppearanceKey = "toolbarButton";
			this.cmdPrintMailingLabel.Location = new System.Drawing.Point(929, 29);
			this.cmdPrintMailingLabel.Name = "cmdPrintMailingLabel";
			this.cmdPrintMailingLabel.Size = new System.Drawing.Size(127, 24);
			this.cmdPrintMailingLabel.TabIndex = 5;
			this.cmdPrintMailingLabel.Text = "Print Mailing Label";
			this.ToolTip1.SetToolTip(this.cmdPrintMailingLabel, null);
			this.cmdPrintMailingLabel.Click += new System.EventHandler(this.cmdPrintMailingLabel_Click);
			// 
			// cmdAttachedDocuments
			// 
			this.cmdAttachedDocuments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAttachedDocuments.AppearanceKey = "toolbarButton";
			this.cmdAttachedDocuments.Location = new System.Drawing.Point(714, 29);
			this.cmdAttachedDocuments.Name = "cmdAttachedDocuments";
			this.cmdAttachedDocuments.Size = new System.Drawing.Size(141, 24);
			this.cmdAttachedDocuments.TabIndex = 3;
			this.cmdAttachedDocuments.Text = "Attached Documents";
			this.ToolTip1.SetToolTip(this.cmdAttachedDocuments, null);
			this.cmdAttachedDocuments.Click += new System.EventHandler(this.cmdAttachedDocuments_Click);
			// 
			// cmdInvoices
			// 
			this.cmdInvoices.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdInvoices.AppearanceKey = "toolbarButton";
			this.cmdInvoices.Location = new System.Drawing.Point(860, 29);
			this.cmdInvoices.Name = "cmdInvoices";
			this.cmdInvoices.Size = new System.Drawing.Size(65, 24);
			this.cmdInvoices.TabIndex = 4;
			this.cmdInvoices.Text = "Invoices";
			this.ToolTip1.SetToolTip(this.cmdInvoices, null);
			this.cmdInvoices.Click += new System.EventHandler(this.cmdInvoices_Click);
			// 
			// frmVendorMaster
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1073, 853);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVendorMaster";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vendor Master Update";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmVendorMaster_Load);
			this.Activated += new System.EventHandler(this.frmVendorMaster_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormClosed += new FormClosedEventHandler(this.Form_Closed);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVendorMaster_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendorMaster_KeyPress);
			this.Resize += new System.EventHandler(this.frmVendorMaster_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTaxInfo)).EndInit();
			this.fraTaxInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAttorney)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeAllPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk1099)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
			this.Frame7.ResumeLayout(false);
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraEFT)).EndInit();
			this.fraEFT.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkPrenote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInsuranceRequired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInsurance)).EndInit();
			this.fraInsurance.ResumeLayout(false);
			this.fraInsurance.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtInsuranceVerifiedDate)).EndInit();
			this.tabAddress.ResumeLayout(false);
			this.tabAddress_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.tabAddress_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOneTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frame4)).EndInit();
			this.frame4.ResumeLayout(false);
			this.frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAdjustment_4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintMailingLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAttachedDocuments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdInvoices)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		private FCButton cmdDeleteVendor;
		private FCButton cmdInvoices;
		private FCButton cmdAttachedDocuments;
		private FCButton cmdPrintMailingLabel;
		private JavaScript javaScript1;
	}
}