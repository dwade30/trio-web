﻿//Fecher vbPorter - Version 1.0.0.27

using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmPosting.
    /// </summary>
    public partial class frmPosting : BaseForm
    {
        public frmPosting()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            //FC:FINAL:DDU:#2454 - added loader
            ShowWait();
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    InitializeComponentEx();
                }
                finally
                {
                    EndWait();

                }
            });
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            if (_InstancePtr == null)
                _InstancePtr = this;
            //FC:FINAL:ASZ: add code from Load
            clsDRWrapper rsEntries = new clsDRWrapper();
            int counter = 0;
            clsDRWrapper rsStandardAccounts = new clsDRWrapper();
            string strJournalSQL = "";
            clsDRWrapper rsTemp = new clsDRWrapper();
            clsDRWrapper rsTaxCommitment = new clsDRWrapper();
            clsDRWrapper rsDiscount = new clsDRWrapper();
            clsDRWrapper rsCreditMemo = new clsDRWrapper();
            clsDRWrapper rsEncAdj = new clsDRWrapper();
            // initialize column variables and grid values
            PostCol = 0;
            JournalCol = 1;
            PeriodCol = 2;
            TypeCol = 3;
            AmountCol = 4;
            CountCol = 5;
            //vsJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsJournals.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsJournals.TextMatrix(0, PostCol, "Post");
            vsJournals.TextMatrix(0, JournalCol, "Journal");
            vsJournals.TextMatrix(0, PeriodCol, "Period");
            vsJournals.TextMatrix(0, TypeCol, "Type");
            vsJournals.TextMatrix(0, AmountCol, "Amount");
            vsJournals.TextMatrix(0, CountCol, "Entries");
            vsJournals.ColWidth(PostCol, 600);
            vsJournals.ColWidth(JournalCol, 800);
            vsJournals.ColWidth(PeriodCol, 800);
            vsJournals.ColWidth(TypeCol, 900);
            vsJournals.ColWidth(AmountCol, 1500);
            vsJournals.ColWidth(CountCol, 800);
            ColPercents[0] = 0.1038961f;
            ColPercents[1] = 0.1385281f;
            ColPercents[2] = 0.1385281f;
            ColPercents[3] = 0.1558441f;
            ColPercents[4] = 0.2597402f;
            ColPercents[5] = 0.1385281f;
            vsJournals.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
            //FC:FINAL:DDU:#2931 - aligned grid columns
            //vsJournals.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            //vsJournals.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsJournals.ColAlignment(PostCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(JournalCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(PeriodCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsJournals.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsJournals.ColAlignment(CountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            rsJournals = new clsDRWrapper();

            try
            {
                StaticSettings.GlobalTelemetryService.TrackTrace("BD Posting Initialize Try");
                // get error opening db if we don't create a new one
                rsJournals.OpenRecordset("SELECT JournalNumber, [Type], Period, CheckDate, CreditMemo, CreditMemoCorrection FROM JournalMaster WHERE ([Type] <> 'AP' AND Status = 'E') OR ([Type] = 'AP' AND (Status = 'W' OR Status = 'X')) GROUP BY JournalNumber, Period, CheckDate, [Type], CreditMemo, CreditMemoCorrection ORDER BY JournalNumber, Period, CheckDate, [Type], CreditMemo, CreditMemoCorrection", "Budgetary");
                if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
                {
                    rsJournals.MoveLast();
                    rsJournals.MoveFirst();
                    vsJournals.Rows = rsJournals.RecordCount() + 1;
                    vsCheckDates.Rows = rsJournals.RecordCount() + 1;

                    StaticSettings.GlobalTelemetryService.TrackTrace($"BD Posting Initialize {rsJournals.RecordCount()} Journals Recs");

                    counter = 1;
                    do
                    {
                        if (counter % 10 == 0)
                        {
                            UpdateWait("Loading Data ... " + counter + " out of " + vsJournals.Rows);
                        }
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
                        {
                            if (!rsJournals.IsFieldNull("CheckDate"))
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE CheckDate = '" + rsJournals.Get_Fields_DateTime("CheckDate") + "' AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'W' OR Status = 'X') AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'W' OR Status = 'X') AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                            vsCheckDates.TextMatrix(counter, 0, FCConvert.ToString(rsJournals.Get_Fields_DateTime("CheckDate")));
                        }
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        else
                        {
                            if (rsJournals.Get_Fields("Type") == "AC")
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            else if (rsJournals.Get_Fields("Type") == "EN")
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM Encumbrances WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            else if (rsJournals.Get_Fields("Type") == "PY")
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND RCB <> 'E' AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND Status = 'E' AND Period = " + rsJournals.Get_Fields("Period"));
                            }
                        }

                        vsJournals.TextMatrix(counter, PostCol, FCConvert.ToString(false));
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        vsJournals.TextMatrix(counter, JournalCol, modValidateAccount.GetFormat_6(rsJournals.Get_Fields("JournalNumber"), 4));
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        vsJournals.TextMatrix(counter, PeriodCol, Strings.Format(rsJournals.Get_Fields("Period"), "00"));
                        if (FCConvert.ToBoolean(rsJournals.Get_Fields_Boolean("CreditMemo")) || FCConvert.ToBoolean(rsJournals.Get_Fields_Boolean("CreditMemoCorrection")))
                        {
                            vsJournals.RowData(counter, true);
                        }
                        else
                        {
                            vsJournals.RowData(counter, false);
                        }
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        if (!modBudgetaryAccounting.BadAccountsInJournal(FCConvert.ToInt32(rsJournals.Get_Fields("JournalNumber"))))
                        {
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "EN" || FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AC")
                            {
                                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                vsJournals.TextMatrix(counter, TypeCol, FCConvert.ToString(rsJournals.Get_Fields("Type")));
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "GJ")
                                {
                                    if (rsEntries.Get_Fields_Decimal("TotalAmount") != 0)
                                    {
                                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        rsEncAdj.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + "AND Period = " + rsJournals.Get_Fields("Period") + " AND RCB <> 'E'");
                                        if (rsEncAdj.EndOfFile() != true && rsEncAdj.BeginningOfFile() != true)
                                        {
                                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                            vsJournals.TextMatrix(counter, TypeCol, rsJournals.Get_Fields("Type") + "  (OB)");
                                        }
                                        else
                                        {
                                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                            vsJournals.TextMatrix(counter, TypeCol, rsJournals.Get_Fields("Type") + "  (E)");
                                        }
                                    }
                                    else
                                    {
                                        // If GetBDVariable("Due") <> "Y" Then
                                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                        if (!modBudgetaryAccounting.CheckOBF(FCConvert.ToInt32(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))), rsJournals.Get_Fields("Type")))
                                        {
                                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                            vsJournals.TextMatrix(counter, TypeCol, rsJournals.Get_Fields("Type") + "  (OBF)");
                                        }
                                        else
                                        {
                                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                            vsJournals.TextMatrix(counter, TypeCol, FCConvert.ToString(rsJournals.Get_Fields("Type")));
                                        }
                                        // Else
                                        // vsJournals.TextMatrix(counter, TypeCol) = rsJournals.Fields["Type"]
                                        // End If
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                else if (rsJournals.Get_Fields("Type") == "PY")
                                {
                                    if (rsEntries.Get_Fields_Decimal("TotalAmount") != 0)
                                    {
                                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        rsEncAdj.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + "AND Period = " + rsJournals.Get_Fields("Period") + " AND RCB = 'E'");
                                        if (rsEncAdj.EndOfFile() != true && rsEncAdj.BeginningOfFile() != true)
                                        {
                                            vsJournals.TextMatrix(counter, TypeCol, "PY  (E)");
                                        }
                                        else
                                        {
                                            vsJournals.TextMatrix(counter, TypeCol, "PY  (OB)");
                                        }
                                    }
                                    else
                                    {
                                        vsJournals.TextMatrix(counter, TypeCol, "PY");
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                else if (rsJournals.Get_Fields("Type") == "CW")
                                {
                                    if (rsEntries.Get_Fields_Decimal("TotalAmount") != 0)
                                    {
                                        vsJournals.TextMatrix(counter, TypeCol, "CRW  (OB)");
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                        if (!modBudgetaryAccounting.CheckOBF(FCConvert.ToInt32(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))), rsJournals.Get_Fields("Type")))
                                        {
                                            vsJournals.TextMatrix(counter, TypeCol, "CRW  (OBF)");
                                        }
                                        else
                                        {
                                            vsJournals.TextMatrix(counter, TypeCol, "CRW");
                                        }
                                    }
                                }
                                else
                                {
                                    // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                                    vsJournals.TextMatrix(counter, TypeCol, FCConvert.ToString(rsJournals.Get_Fields("Type")));
                                }
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            vsJournals.TextMatrix(counter, TypeCol, rsJournals.Get_Fields("Type") + "  (BA)");
                        }
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "AP")
                        {
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                            rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'W' OR Status = 'X') AND Period = " + rsJournals.Get_Fields("Period") + ")");
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                            rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'W' OR Status = 'X') AND IsNull(CreditMemoRecord, 0) = 0 AND Period = " + rsJournals.Get_Fields("Period") + ")");
                            // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                            // TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
                            if (Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) != 0 || Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) != 0)
                            {
                                // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
                                vsJournals.TextMatrix(counter, AmountCol, Strings.Format(rsEntries.Get_Fields_Decimal("TotalAmount") - FCConvert.ToDecimal(rsDiscount.Get_Fields("TotalDiscount")) + FCConvert.ToDecimal(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
                            }
                            else
                            {
                                vsJournals.TextMatrix(counter, AmountCol, Strings.Format(rsEntries.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
                            }
                        }
                        else
                        {
                            vsJournals.TextMatrix(counter, AmountCol, Strings.Format(rsEntries.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
                        }
                        // TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
                        vsJournals.TextMatrix(counter, CountCol, FCConvert.ToString(rsEntries.Get_Fields("TotalEntries")));
                        // DJW 6/18/2010 Uncombined control entries for CW type journal
                        // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                        if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "PY")
                        {
                            // rsJournals.Fields["Type"] = "CW" Or
                            modBudgetaryAccounting.CombineDuplicateControlEntries_2(rsJournals.Get_Fields_Int32("JournalNumber"));
                        }
                        counter += 1;
                        rsJournals.MoveNext();
                    }
                    while (rsJournals.EndOfFile() != true);
                }
                else
                {
                    vsJournals.Rows = 1;
                }
                if (vsJournals.Rows >= 15)
                {
                    //FC:FINAL:ASZ - use anchors
                    //vsJournals.Height = 15 * vsJournals.RowHeight(0) + 75;
                }
                else
                {
                    //FC:FINAL:ASZ - use anchors
                    //vsJournals.Height = vsJournals.Rows * vsJournals.RowHeight(0) + 75;
                }
                blnUnload = false;
                modBudgetaryAccounting.GetControlAccounts();
                rsStandardAccounts.OpenRecordset("SELECT * FROM StandardAccounts");
                if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) != "")
                {
                    // If gboolTownAccounts Then
                    if (Conversion.Val(modBudgetaryAccounting.Statics.strEOYAPAccount) == 0)
                    {
                        MessageBox.Show("You must set up your EOY Accounts Payable Account before you may proceed", "Setup EOY Accounts Payable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vsJournals.Enabled = false;
                        blnUnload = true;
                        return;
                    }
                    // End If
                }
                if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y" )
                {
                    rsTemp.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'T' AND ShortDescription = 'Cash Fund'");
                    if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (Conversion.Val(rsTemp.Get_Fields("Account")) == 0)
                        {
                            MessageBox.Show("You must set up a Cash Fund before you may continue.", "No Cash Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            vsJournals.Enabled = false;
                            blnUnload = true;
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("You must set up a Cash Fund before you may continue.", "No Cash Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vsJournals.Enabled = false;
                        blnUnload = true;
                        return;
                    }
                    if (Conversion.Val(modBudgetaryAccounting.Statics.strDueToCtrAccount) == 0)
                    {
                        MessageBox.Show("You must set up your Due To Account before you may proceed", "Setup Due To Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vsJournals.Enabled = false;
                        blnUnload = true;
                        return;
                    }
                    if (Conversion.Val(modBudgetaryAccounting.Statics.strDueFromCtrAccount) == 0)
                    {
                        MessageBox.Show("You must set up your Due From Account before you may proceed", "Setup Due From Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vsJournals.Enabled = false;
                        blnUnload = true;
                        return;
                    }
                }
                if (Conversion.Val(modBudgetaryAccounting.Statics.strAPCashAccount) == 0)
                {
                    MessageBox.Show("You must set up your Town Accounts Payable Cash Account before you may proceed", "Setup Town Accounts Payable Cash Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    vsJournals.Enabled = false;
                    blnUnload = true;
                    return;
                }
                if (Conversion.Val(modBudgetaryAccounting.Statics.strExpCtrAccount) == 0)
                {
                    MessageBox.Show("You must set up your Town Expense Control Account before you may proceed", "Setup Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    vsJournals.Enabled = false;
                    blnUnload = true;
                    return;
                }
                if (Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) == 0)
                {
                    MessageBox.Show("You must set up your Town Revenue Control Account before you may proceed", "Setup Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    vsJournals.Enabled = false;
                    blnUnload = true;
                    return;
                }
                if (Conversion.Val(modBudgetaryAccounting.Statics.strCredMemRecAccount) == 0)
                {
                    MessageBox.Show("You must set up your Town Credit Memo Receivable Account before you may proceed", "Setup Town Credit Memo Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    vsJournals.Enabled = false;
                    blnUnload = true;
                    return;
                }
                for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
                {
                    if (vsJournals.TextMatrix(counter, TypeCol) == "EN")
                    {
                        if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) == 0)
                        {
                            MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            vsJournals.Enabled = false;
                            blnUnload = true;
                            return;
                        }
                    }
                    else if (vsJournals.TextMatrix(counter, TypeCol) == "AP" || vsJournals.TextMatrix(counter, TypeCol) == "AC")
                    {
                        strJournalSQL = "SELECT ID FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol)));
                        rsTemp.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Encumbrance) as JournalTotal FROM APJournalDetail WHERE APJournalID IN (" + strJournalSQL + ")");
                        // TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
                        if (Conversion.Val(rsTemp.Get_Fields("JournalTotal")) != 0)
                        {
                            if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) == 0)
                            {
                                MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                vsJournals.Enabled = false;
                                blnUnload = true;
                                return;
                            }
                        }
                    }
                    else
                    {
                        rsTemp.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))) + " AND RCB = 'E'");
                        if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(modBudgetaryAccounting.Statics.strEncOffAccount) == 0)
                            {
                                MessageBox.Show("You must set up your Town Encumbrance Control Account before you may proceed", "Setup Town Encumbrance Control Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                vsJournals.Enabled = false;
                                blnUnload = true;
                                return;
                            }
                        }
                    }
                }
                rsTaxCommitment.OpenRecordset("SELECT * FROM TaxCommitment");
                if (rsTaxCommitment.EndOfFile() != true && rsTaxCommitment.BeginningOfFile() != true)
                {
                    MessageBox.Show("You have not completed your tax commitment entry.", "Complete Tax Commitment Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                rsTaxCommitment.OpenRecordset("SELECT * FROM TempJournalEntries");
                if (rsTaxCommitment.EndOfFile() != true && rsTaxCommitment.BeginningOfFile() != true)
                {
                    MessageBox.Show("You have some incomplete journals waiting to be completed.", "Complete Uncompleted Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            finally
            {
                StaticSettings.GlobalTelemetryService.TrackTrace("BD Posting Initialize Finally");
                rsEncAdj.DisposeOf();
                rsCreditMemo.DisposeOf();
                rsDiscount.DisposeOf();
                rsEntries.DisposeOf();
                rsStandardAccounts.DisposeOf();
                rsTaxCommitment.DisposeOf();
                rsTemp.DisposeOf();
                rsJournals.DisposeOf();
            }
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmPosting InstancePtr
        {
            get
            {
                return (frmPosting)Sys.GetInstance(typeof(frmPosting));
            }
        }

        protected frmPosting _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By   Dave Wade
        // Date         11/16/2001
        // This form will be used to post journal entries.
        // ********************************************************
        int PostCol;
        int JournalCol;
        int TypeCol;
        // variables for grid columns
        int AmountCol;
        int CountCol;
        int PeriodCol;
        clsDRWrapper rsJournals = new clsDRWrapper();
        float[] ColPercents = new float[5 + 1];
        // resizing information for the grid
        int lngCloseoutKey;
        int intJournal;
        string ControlDate = "";
        // date to use for control entries
        int TempJournal;
        bool blnUnload;

        private void cmdPost_Click(object sender, System.EventArgs e)
        {
            // On Error GoTo ERROR_HANDLER
            int counter;
            int ans;
            double TotalEncumbrance;
            int UpdateRecordNumber;
            int lngErrorCode = 0;
            string strWarning = "";
            var clsPostInfo = new clsBudgetaryPosting();
            clsPostingJournalInfo clsJournalInfo;

            cmdPost.Enabled = false;
            clsPostInfo.ClearJournals();

            frmWait.InstancePtr.Init("Adding journals...", true, vsJournals.Rows - 1, true);
            frmWait.InstancePtr.Show();

            try
            {
                for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
                {
                    frmWait.InstancePtr.UpdateWait(progressBarValue: counter);

                    if (FCUtils.CBool(vsJournals.TextMatrix(counter, PostCol)) != true) continue;

                    if (Strings.Right(vsJournals.TextMatrix(counter, TypeCol), 4) == "(BA)")
                    {
                        MessageBox.Show("There are one or more bad accounts in journal " + vsJournals.TextMatrix(counter, JournalCol) + ".  Please fix the problem with the accounts and try to post again.", "Invalid Accounts In Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmdPost.Enabled = true;

                        return;
                    }

                    clsJournalInfo = new clsPostingJournalInfo();
                    clsJournalInfo.JournalNumber = FCConvert.ToInt32(vsJournals.TextMatrix(counter, JournalCol));
                    clsJournalInfo.Period = vsJournals.TextMatrix(counter, PeriodCol);
                    clsJournalInfo.JournalType = Strings.Trim(Strings.Left(vsJournals.TextMatrix(counter, TypeCol), 3)) == "CRW" ? "CW" : Strings.Trim(Strings.Left(vsJournals.TextMatrix(counter, TypeCol), 3));
                    clsJournalInfo.CheckDate = vsCheckDates.TextMatrix(counter, 0);
                    clsPostInfo.AddJournal(clsJournalInfo);
                }

                frmWait.InstancePtr.UpdateWait("Posting journals...", -1, -1);

                if (clsPostInfo.JournalCount > 0)
                {
                    clsPostInfo.PageBreakBetweenJournalsOnReport = chkBreak.CheckState == CheckState.Checked;
                    clsPostInfo.AllowPreview = true;

                    //FC:FINAL:DSE:#2057 Report not shown if current form is closed after
                    Hide();

                    if (clsPostInfo.PostJournals())
                    {
                        frmWait.InstancePtr.Unload();
                        Close();
                    }
                    else
                    {
                        //FC:FINAL:DSE:#2057 Restore form if report did not show
                        Show();
                        cmdPost.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("You must first select 1 or more journals to post before you may continue.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmdPost.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Posting Error: {ex.GetBaseException().Message}", "Posting Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                
                frmWait.InstancePtr.Unload();

            }

        }

        public void cmdPost_Click()
        {
            cmdPost_Click(cmdPost, new System.EventArgs());
        }

        private void cmdQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void frmPosting_Activated(object sender, System.EventArgs e)
        {
            if (blnUnload)
            {
                Close();
                return;
            }
            if (modGlobal.FormExist(this))
            {
                return;
            }
            Refresh();
        }

        private void frmPosting_Load(object sender, System.EventArgs e)
        {

        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            int counter;
            for (counter = 0; counter <= 4; counter++)
            {
                vsJournals.ColWidth(counter, FCConvert.ToInt32(vsJournals.WidthOriginal * ColPercents[counter]));
            }
            //MDIParent.InstancePtr.GRID.Focus();
        }

        private void frmPosting_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            switch (KeyAscii)
            {
                // catches the escape and enter keys
                case Keys.Escape:
                    KeyAscii = (Keys)0;
                    Close();

                    break;
                case Keys.Return:
                    KeyAscii = (Keys)0;
                    Support.SendKeys("{TAB}", false);

                    break;
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmPosting_Resize(object sender, System.EventArgs e)
        {
            int counter;
            for (counter = 0; counter <= 5; counter++)
            {
                vsJournals.ColWidth(counter, FCConvert.ToInt32(vsJournals.WidthOriginal * ColPercents[counter]));
            }
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            cmdPost_Click();
        }

        private void vsJournals_ClickEvent(object sender, System.EventArgs e)
        {
            if (vsJournals.Row <= 0) return;

            vsJournals.TextMatrix(vsJournals.Row, PostCol, FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, PostCol))
                                      ? FCConvert.ToString(false)
                                      : FCConvert.ToString(true));
        }

        private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;

            if (KeyCode != Keys.Space || vsJournals.Row <= 0) return;

            vsJournals.TextMatrix(vsJournals.Row, PostCol, FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, PostCol))
                                      ? FCConvert.ToString(false)
                                      : FCConvert.ToString(true));
        }
    }
}
