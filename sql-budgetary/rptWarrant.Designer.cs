﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrant.
	/// </summary>
	partial class rptWarrant
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarrant));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldProject = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncumbrance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDetailDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldWarrantSignature1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantSignature10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrantEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWarrantAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldPrepaidAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCurrentAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEFTAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldVendorAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMonth = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeperate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldJournalAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournalEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournalTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldProject)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetailDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantEncumbranceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrepaidAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEFTAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorEncumbranceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalEncumbranceTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldProject,
				this.fldAmount,
				this.fldEncumbrance,
				this.fldDetailDescription,
				this.fldAccount,
				this.fldTitle
			});
			this.Detail.Height = 0.3229167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldProject
			// 
			this.fldProject.Height = 0.19F;
			this.fldProject.Left = 4.15625F;
			this.fldProject.Name = "fldProject";
			this.fldProject.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldProject.Text = "Field3";
			this.fldProject.Top = 0F;
			this.fldProject.Width = 0.59375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.19F;
			this.fldAmount.Left = 4.96875F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Courier New\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field3";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.03125F;
			// 
			// fldEncumbrance
			// 
			this.fldEncumbrance.Height = 0.19F;
			this.fldEncumbrance.Left = 6.1875F;
			this.fldEncumbrance.Name = "fldEncumbrance";
			this.fldEncumbrance.Style = "font-family: \'Courier New\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldEncumbrance.Text = "Field4";
			this.fldEncumbrance.Top = 0F;
			this.fldEncumbrance.Width = 1.0625F;
			// 
			// fldDetailDescription
			// 
			this.fldDetailDescription.Height = 0.19F;
			this.fldDetailDescription.Left = 0.25F;
			this.fldDetailDescription.Name = "fldDetailDescription";
			this.fldDetailDescription.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDetailDescription.Text = "Field3";
			this.fldDetailDescription.Top = 0F;
			this.fldDetailDescription.Width = 2.15625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 2.5F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldAccount.Text = "P 001-0001-0001-0001-01";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.59375F;
			// 
			// fldTitle
			// 
			this.fldTitle.CanShrink = true;
			this.fldTitle.Height = 0.19F;
			this.fldTitle.Left = 1.625F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldTitle.Text = "Field3";
			this.fldTitle.Top = 0.15625F;
			this.fldTitle.Width = 4.0625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldWarrantSignature1,
				this.fldWarrantSignature2,
				this.fldWarrantSignature3,
				this.fldWarrantSignature4,
				this.fldWarrantSignature5,
				this.fldWarrantSignature6,
				this.fldWarrantSignature7,
				this.fldWarrantSignature8,
				this.fldWarrantSignature9,
				this.fldWarrantSignature10,
				this.fldWarrantEncumbranceTotal,
				this.Label18,
				this.fldWarrantAmountTotal,
				this.Line5,
				this.Label22,
				this.fldPrepaidAmountTotal,
				this.Label23,
				this.fldCurrentAmountTotal,
				this.Line6,
				this.Label24,
				this.fldEFTAmountTotal
			});
			this.ReportFooter.Height = 3.125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldWarrantSignature1
			// 
			this.fldWarrantSignature1.CanShrink = true;
			this.fldWarrantSignature1.Height = 0.1875F;
			this.fldWarrantSignature1.Left = 0.4375F;
			this.fldWarrantSignature1.Name = "fldWarrantSignature1";
			this.fldWarrantSignature1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature1.Text = "Field3";
			this.fldWarrantSignature1.Top = 1.25F;
			this.fldWarrantSignature1.Width = 6.9375F;
			// 
			// fldWarrantSignature2
			// 
			this.fldWarrantSignature2.CanShrink = true;
			this.fldWarrantSignature2.Height = 0.1875F;
			this.fldWarrantSignature2.Left = 0.4375F;
			this.fldWarrantSignature2.Name = "fldWarrantSignature2";
			this.fldWarrantSignature2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature2.Text = "Field4";
			this.fldWarrantSignature2.Top = 1.4375F;
			this.fldWarrantSignature2.Width = 6.9375F;
			// 
			// fldWarrantSignature3
			// 
			this.fldWarrantSignature3.CanShrink = true;
			this.fldWarrantSignature3.Height = 0.1875F;
			this.fldWarrantSignature3.Left = 0.4375F;
			this.fldWarrantSignature3.Name = "fldWarrantSignature3";
			this.fldWarrantSignature3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature3.Text = "Field5";
			this.fldWarrantSignature3.Top = 1.625F;
			this.fldWarrantSignature3.Width = 6.9375F;
			// 
			// fldWarrantSignature4
			// 
			this.fldWarrantSignature4.CanShrink = true;
			this.fldWarrantSignature4.Height = 0.1875F;
			this.fldWarrantSignature4.Left = 0.4375F;
			this.fldWarrantSignature4.Name = "fldWarrantSignature4";
			this.fldWarrantSignature4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature4.Text = "Field6";
			this.fldWarrantSignature4.Top = 1.8125F;
			this.fldWarrantSignature4.Width = 6.9375F;
			// 
			// fldWarrantSignature5
			// 
			this.fldWarrantSignature5.CanShrink = true;
			this.fldWarrantSignature5.Height = 0.1875F;
			this.fldWarrantSignature5.Left = 0.4375F;
			this.fldWarrantSignature5.Name = "fldWarrantSignature5";
			this.fldWarrantSignature5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature5.Text = "Field7";
			this.fldWarrantSignature5.Top = 2F;
			this.fldWarrantSignature5.Width = 6.9375F;
			// 
			// fldWarrantSignature6
			// 
			this.fldWarrantSignature6.CanShrink = true;
			this.fldWarrantSignature6.Height = 0.1875F;
			this.fldWarrantSignature6.Left = 0.4375F;
			this.fldWarrantSignature6.Name = "fldWarrantSignature6";
			this.fldWarrantSignature6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature6.Text = "Field8";
			this.fldWarrantSignature6.Top = 2.1875F;
			this.fldWarrantSignature6.Width = 6.9375F;
			// 
			// fldWarrantSignature7
			// 
			this.fldWarrantSignature7.CanShrink = true;
			this.fldWarrantSignature7.Height = 0.1875F;
			this.fldWarrantSignature7.Left = 0.4375F;
			this.fldWarrantSignature7.Name = "fldWarrantSignature7";
			this.fldWarrantSignature7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature7.Text = "Field9";
			this.fldWarrantSignature7.Top = 2.375F;
			this.fldWarrantSignature7.Width = 6.9375F;
			// 
			// fldWarrantSignature8
			// 
			this.fldWarrantSignature8.CanShrink = true;
			this.fldWarrantSignature8.Height = 0.1875F;
			this.fldWarrantSignature8.Left = 0.4375F;
			this.fldWarrantSignature8.Name = "fldWarrantSignature8";
			this.fldWarrantSignature8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature8.Text = "Field10";
			this.fldWarrantSignature8.Top = 2.5625F;
			this.fldWarrantSignature8.Width = 6.9375F;
			// 
			// fldWarrantSignature9
			// 
			this.fldWarrantSignature9.CanShrink = true;
			this.fldWarrantSignature9.Height = 0.1875F;
			this.fldWarrantSignature9.Left = 0.4375F;
			this.fldWarrantSignature9.Name = "fldWarrantSignature9";
			this.fldWarrantSignature9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature9.Text = "Field11";
			this.fldWarrantSignature9.Top = 2.75F;
			this.fldWarrantSignature9.Width = 6.9375F;
			// 
			// fldWarrantSignature10
			// 
			this.fldWarrantSignature10.CanShrink = true;
			this.fldWarrantSignature10.Height = 0.1875F;
			this.fldWarrantSignature10.Left = 0.4375F;
			this.fldWarrantSignature10.Name = "fldWarrantSignature10";
			this.fldWarrantSignature10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldWarrantSignature10.Text = "Field12";
			this.fldWarrantSignature10.Top = 2.9375F;
			this.fldWarrantSignature10.Width = 6.9375F;
			// 
			// fldWarrantEncumbranceTotal
			// 
			this.fldWarrantEncumbranceTotal.Height = 0.1875F;
			this.fldWarrantEncumbranceTotal.Left = 6.1875F;
			this.fldWarrantEncumbranceTotal.Name = "fldWarrantEncumbranceTotal";
			this.fldWarrantEncumbranceTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right;" + " ddo-char-set: 1";
			this.fldWarrantEncumbranceTotal.Text = "Field4";
			this.fldWarrantEncumbranceTotal.Top = 0.9375F;
			this.fldWarrantEncumbranceTotal.Width = 1.0625F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.6875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold";
			this.Label18.Text = "Warrant Total-";
			this.Label18.Top = 0.9375F;
			this.Label18.Width = 1.25F;
			// 
			// fldWarrantAmountTotal
			// 
			this.fldWarrantAmountTotal.Height = 0.1875F;
			this.fldWarrantAmountTotal.Left = 5F;
			this.fldWarrantAmountTotal.Name = "fldWarrantAmountTotal";
			this.fldWarrantAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right";
			this.fldWarrantAmountTotal.Text = "Field3";
			this.fldWarrantAmountTotal.Top = 0.9375F;
			this.fldWarrantAmountTotal.Width = 1F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.96875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.09375F;
			this.Line5.Width = 2.3125F;
			this.Line5.X1 = 4.96875F;
			this.Line5.X2 = 7.28125F;
			this.Line5.Y1 = 0.09375F;
			this.Line5.Y2 = 0.09375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.65625F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold";
			this.Label22.Text = "Prepaid Total-";
			this.Label22.Top = 0.15625F;
			this.Label22.Width = 1.25F;
			// 
			// fldPrepaidAmountTotal
			// 
			this.fldPrepaidAmountTotal.Height = 0.1875F;
			this.fldPrepaidAmountTotal.Left = 5F;
			this.fldPrepaidAmountTotal.Name = "fldPrepaidAmountTotal";
			this.fldPrepaidAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right";
			this.fldPrepaidAmountTotal.Text = "Field3";
			this.fldPrepaidAmountTotal.Top = 0.15625F;
			this.fldPrepaidAmountTotal.Width = 1F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 3.65625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold";
			this.Label23.Text = "Current Total-";
			this.Label23.Top = 0.3902778F;
			this.Label23.Width = 1.25F;
			// 
			// fldCurrentAmountTotal
			// 
			this.fldCurrentAmountTotal.Height = 0.1875F;
			this.fldCurrentAmountTotal.Left = 5F;
			this.fldCurrentAmountTotal.Name = "fldCurrentAmountTotal";
			this.fldCurrentAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right";
			this.fldCurrentAmountTotal.Text = "Field3";
			this.fldCurrentAmountTotal.Top = 0.3854167F;
			this.fldCurrentAmountTotal.Width = 1F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 5F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.875F;
			this.Line6.Width = 2.3125F;
			this.Line6.X1 = 5F;
			this.Line6.X2 = 7.3125F;
			this.Line6.Y1 = 0.875F;
			this.Line6.Y2 = 0.875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.65625F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold";
			this.Label24.Text = "EFT Total-";
			this.Label24.Top = 0.6243055F;
			this.Label24.Width = 1.25F;
			// 
			// fldEFTAmountTotal
			// 
			this.fldEFTAmountTotal.Height = 0.1875F;
			this.fldEFTAmountTotal.Left = 5F;
			this.fldEFTAmountTotal.Name = "fldEFTAmountTotal";
			this.fldEFTAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right";
			this.fldEFTAmountTotal.Text = "Field3";
			this.fldEFTAmountTotal.Top = 0.625F;
			this.fldEFTAmountTotal.Width = 1F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label8,
				this.Line1,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label6,
				this.Label19,
				this.Label20,
				this.Label21
			});
			this.PageHeader.Height = 1.052083F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 12pt; font-weight: bold; text-align: cente" + "r; ddo-char-set: 1";
			this.Label1.Text = "A / P Warrant";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.15625F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label8.Text = "Jrnl";
			this.Label8.Top = 0.65625F;
			this.Label8.Width = 0.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.03125F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 1.03125F;
			this.Line1.Y2 = 1.03125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label9.Text = "Invoice Description";
			this.Label9.Top = 0.65625F;
			this.Label9.Width = 2.0625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.0625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label10.Text = "Reference";
			this.Label10.Top = 0.65625F;
			this.Label10.Width = 0.84375F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.3125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label11.Text = "Description";
			this.Label11.Top = 0.84375F;
			this.Label11.Width = 1.46875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label12.Text = "Account";
			this.Label12.Top = 0.84375F;
			this.Label12.Width = 0.84375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.3125F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label13.Text = "Proj";
			this.Label13.Top = 0.84375F;
			this.Label13.Width = 0.40625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 4.96875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label14.Text = "Amount";
			this.Label14.Top = 0.84375F;
			this.Label14.Width = 1.03125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 6.15625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Encumbrance";
			this.Label15.Top = 0.84375F;
			this.Label15.Width = 1.125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.03125F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label6.Text = "Label6";
			this.Label6.Top = 0.375F;
			this.Label6.Width = 7.5F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 1.03125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Label19.Text = "Check";
			this.Label19.Top = 0.65625F;
			this.Label19.Width = 0.625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 1.875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 1";
			this.Label20.Text = "Month";
			this.Label20.Top = 0.65625F;
			this.Label20.Width = 0.5F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.5F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label21.Text = "**** REPRINT ****";
			this.Label21.Top = 0.1875F;
			this.Label21.Visible = false;
			this.Label21.Width = 4.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVendor,
				this.Field1
			});
			this.GroupHeader2.DataField = "VendorBinder";
			this.GroupHeader2.Height = 0.1979167F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.19F;
			this.fldVendor.Left = 0.03125F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldVendor.Text = "Field3";
			this.fldVendor.Top = 0.03125F;
			this.fldVendor.Width = 3.53125F;
			// 
			// Field1
			// 
			this.Field1.DataField = "VendorBinder";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.Field1.Text = "VendorBinder";
			this.Field1.Top = 0F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.84375F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.CanShrink = true;
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label17,
				this.fldVendorAmountTotal,
				this.fldVendorEncumbranceTotal,
				this.Line4,
				this.Line2
			});
			this.GroupFooter2.Height = 0.21875F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.78125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label17.Text = "Vendor Total-";
			this.Label17.Top = 0.03125F;
			this.Label17.Width = 1.125F;
			// 
			// fldVendorAmountTotal
			// 
			this.fldVendorAmountTotal.Height = 0.19F;
			this.fldVendorAmountTotal.Left = 5F;
			this.fldVendorAmountTotal.Name = "fldVendorAmountTotal";
			this.fldVendorAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right;" + " ddo-char-set: 1";
			this.fldVendorAmountTotal.Text = "Field3";
			this.fldVendorAmountTotal.Top = 0.03125F;
			this.fldVendorAmountTotal.Width = 1F;
			// 
			// fldVendorEncumbranceTotal
			// 
			this.fldVendorEncumbranceTotal.Height = 0.19F;
			this.fldVendorEncumbranceTotal.Left = 6.1875F;
			this.fldVendorEncumbranceTotal.Name = "fldVendorEncumbranceTotal";
			this.fldVendorEncumbranceTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right;" + " ddo-char-set: 1";
			this.fldVendorEncumbranceTotal.Text = "Field4";
			this.fldVendorEncumbranceTotal.Top = 0.03125F;
			this.fldVendorEncumbranceTotal.Width = 1.0625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 4.96875F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 2.3125F;
			this.Line4.X1 = 4.96875F;
			this.Line4.X2 = 7.28125F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.1875F;
			this.Line2.Width = 3.53125F;
			this.Line2.X1 = 2F;
			this.Line2.X2 = 5.53125F;
			this.Line2.Y1 = 0.1875F;
			this.Line2.Y2 = 0.1875F;
			// 
			// GroupHeader3
			// 
			this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldJournal,
				this.fldDescription,
				this.fldReference,
				this.fldCheckNumber,
				this.Field2,
				this.fldMonth,
				this.fldSeperate
			});
			this.GroupHeader3.DataField = "JournalBinder";
			this.GroupHeader3.Height = 0.1979167F;
			this.GroupHeader3.Name = "GroupHeader3";
			this.GroupHeader3.Format += new System.EventHandler(this.GroupHeader3_Format);
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.19F;
			this.fldJournal.Left = 0.15625F;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldJournal.Text = "Field3";
			this.fldJournal.Top = 0.03125F;
			this.fldJournal.Width = 0.375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.19F;
			this.fldDescription.Left = 2.8125F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field3";
			this.fldDescription.Top = 0.03125F;
			this.fldDescription.Width = 2.15625F;
			// 
			// fldReference
			// 
			this.fldReference.Height = 0.19F;
			this.fldReference.Left = 5.03125F;
			this.fldReference.Name = "fldReference";
			this.fldReference.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.fldReference.Text = null;
			this.fldReference.Top = 0.03125F;
			this.fldReference.Width = 1.21875F;
			// 
			// fldCheckNumber
			// 
			this.fldCheckNumber.Height = 0.19F;
			this.fldCheckNumber.Left = 0.78125F;
			this.fldCheckNumber.Name = "fldCheckNumber";
			this.fldCheckNumber.Style = "font-family: \'Courier New\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCheckNumber.Text = "Field3";
			this.fldCheckNumber.Top = 0.03125F;
			this.fldCheckNumber.Width = 0.875F;
			// 
			// Field2
			// 
			this.Field2.DataField = "JournalBinder";
			this.Field2.Height = 0.19F;
			this.Field2.Left = 0F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Courier New\'; font-size: 8pt; ddo-char-set: 1";
			this.Field2.Text = "JournalBinder";
			this.Field2.Top = 0F;
			this.Field2.Visible = false;
			this.Field2.Width = 0.84375F;
			// 
			// fldMonth
			// 
			this.fldMonth.Height = 0.19F;
			this.fldMonth.Left = 1.875F;
			this.fldMonth.Name = "fldMonth";
			this.fldMonth.Style = "font-family: \'Courier New\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldMonth.Text = "Field3";
			this.fldMonth.Top = 0.03125F;
			this.fldMonth.Width = 0.46875F;
			// 
			// fldSeperate
			// 
			this.fldSeperate.Height = 0.19F;
			this.fldSeperate.Left = 6.25F;
			this.fldSeperate.Name = "fldSeperate";
			this.fldSeperate.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldSeperate.Text = "*** SEPARATE ***";
			this.fldSeperate.Top = 0.03125F;
			this.fldSeperate.Width = 1.25F;
			// 
			// GroupFooter3
			// 
			this.GroupFooter3.CanShrink = true;
			this.GroupFooter3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldJournalAmountTotal,
				this.fldJournalEncumbranceTotal,
				this.fldJournalTitle,
				this.Line3
			});
			this.GroupFooter3.Height = 0.21875F;
			this.GroupFooter3.Name = "GroupFooter3";
			this.GroupFooter3.Format += new System.EventHandler(this.GroupFooter3_Format);
			// 
			// fldJournalAmountTotal
			// 
			this.fldJournalAmountTotal.CanShrink = true;
			this.fldJournalAmountTotal.Height = 0.19F;
			this.fldJournalAmountTotal.Left = 5F;
			this.fldJournalAmountTotal.Name = "fldJournalAmountTotal";
			this.fldJournalAmountTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right;" + " ddo-char-set: 1";
			this.fldJournalAmountTotal.Text = "Field3";
			this.fldJournalAmountTotal.Top = 0.03125F;
			this.fldJournalAmountTotal.Width = 1F;
			// 
			// fldJournalEncumbranceTotal
			// 
			this.fldJournalEncumbranceTotal.CanShrink = true;
			this.fldJournalEncumbranceTotal.Height = 0.19F;
			this.fldJournalEncumbranceTotal.Left = 6.1875F;
			this.fldJournalEncumbranceTotal.Name = "fldJournalEncumbranceTotal";
			this.fldJournalEncumbranceTotal.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; text-align: right;" + " ddo-char-set: 1";
			this.fldJournalEncumbranceTotal.Text = "Field4";
			this.fldJournalEncumbranceTotal.Top = 0.03125F;
			this.fldJournalEncumbranceTotal.Width = 1.0625F;
			// 
			// fldJournalTitle
			// 
			this.fldJournalTitle.CanShrink = true;
			this.fldJournalTitle.Height = 0.19F;
			this.fldJournalTitle.Left = 3.6875F;
			this.fldJournalTitle.Name = "fldJournalTitle";
			this.fldJournalTitle.Style = "font-family: \'Courier New\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldJournalTitle.Text = "Invoice Total-";
			this.fldJournalTitle.Top = 0.03125F;
			this.fldJournalTitle.Width = 1.21875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.96875F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.3125F;
			this.Line3.X1 = 4.96875F;
			this.Line3.X2 = 7.28125F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// rptWarrant
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader3);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter3);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptWarrant_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldProject)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDetailDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantSignature10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantEncumbranceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrantAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrepaidAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEFTAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorEncumbranceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalEncumbranceTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProject;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbrance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDetailDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantSignature10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantEncumbranceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrantAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrepaidAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEFTAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorEncumbranceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonth;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperate;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournalAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournalEncumbranceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournalTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
