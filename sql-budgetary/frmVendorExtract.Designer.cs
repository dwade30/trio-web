﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorExtract.
	/// </summary>
	partial class frmVendorExtract : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkEmail;
		public fecherFoundation.FCCheckBox chkComments;
		public fecherFoundation.FCCheckBox chkClass;
		public fecherFoundation.FCCheckBox chkStatus;
		public fecherFoundation.FCCheckBox chkTelephone;
		public fecherFoundation.FCCheckBox chkContact;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkCorrName;
		public fecherFoundation.FCCheckBox chkCheckName;
		public fecherFoundation.FCCheckBox chkVendorNumber;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendorExtract));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkEmail = new fecherFoundation.FCCheckBox();
			this.chkComments = new fecherFoundation.FCCheckBox();
			this.chkClass = new fecherFoundation.FCCheckBox();
			this.chkStatus = new fecherFoundation.FCCheckBox();
			this.chkTelephone = new fecherFoundation.FCCheckBox();
			this.chkContact = new fecherFoundation.FCCheckBox();
			this.chkAddress = new fecherFoundation.FCCheckBox();
			this.chkCorrName = new fecherFoundation.FCCheckBox();
			this.chkCheckName = new fecherFoundation.FCCheckBox();
			this.chkVendorNumber = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTelephone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkContact)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVendorNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 364);
			this.BottomPanel.Size = new System.Drawing.Size(551, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnProcess);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(551, 304);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(551, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(175, 30);
			this.HeaderText.Text = "Vendor Extract";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkEmail);
			this.Frame1.Controls.Add(this.chkComments);
			this.Frame1.Controls.Add(this.chkClass);
			this.Frame1.Controls.Add(this.chkStatus);
			this.Frame1.Controls.Add(this.chkTelephone);
			this.Frame1.Controls.Add(this.chkContact);
			this.Frame1.Controls.Add(this.chkAddress);
			this.Frame1.Controls.Add(this.chkCorrName);
			this.Frame1.Controls.Add(this.chkCheckName);
			this.Frame1.Controls.Add(this.chkVendorNumber);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(230, 380);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Include In Extract";
			// 
			// chkEmail
			// 
			this.chkEmail.AutoSize = false;
			this.chkEmail.Location = new System.Drawing.Point(20, 336);
			this.chkEmail.Name = "chkEmail";
			this.chkEmail.Size = new System.Drawing.Size(132, 24);
			this.chkEmail.TabIndex = 10;
			this.chkEmail.Text = "E-mail Address";
			// 
			// chkComments
			// 
			this.chkComments.AutoSize = false;
			this.chkComments.Location = new System.Drawing.Point(20, 302);
			this.chkComments.Name = "chkComments";
			this.chkComments.Size = new System.Drawing.Size(102, 24);
			this.chkComments.TabIndex = 9;
			this.chkComments.Text = "Comments";
			// 
			// chkClass
			// 
			this.chkClass.AutoSize = false;
			this.chkClass.Location = new System.Drawing.Point(20, 268);
			this.chkClass.Name = "chkClass";
			this.chkClass.Size = new System.Drawing.Size(115, 24);
			this.chkClass.TabIndex = 8;
			this.chkClass.Text = "Class Codes";
			// 
			// chkStatus
			// 
			this.chkStatus.AutoSize = false;
			this.chkStatus.Location = new System.Drawing.Point(20, 234);
			this.chkStatus.Name = "chkStatus";
			this.chkStatus.Size = new System.Drawing.Size(113, 24);
			this.chkStatus.TabIndex = 7;
			this.chkStatus.Text = "Status Code";
			// 
			// chkTelephone
			// 
			this.chkTelephone.AutoSize = false;
			this.chkTelephone.Location = new System.Drawing.Point(20, 200);
			this.chkTelephone.Name = "chkTelephone";
			this.chkTelephone.Size = new System.Drawing.Size(101, 24);
			this.chkTelephone.TabIndex = 6;
			this.chkTelephone.Text = "Telephone";
			// 
			// chkContact
			// 
			this.chkContact.AutoSize = false;
			this.chkContact.Location = new System.Drawing.Point(20, 166);
			this.chkContact.Name = "chkContact";
			this.chkContact.Size = new System.Drawing.Size(128, 24);
			this.chkContact.TabIndex = 5;
			this.chkContact.Text = "Contact Name";
			// 
			// chkAddress
			// 
			this.chkAddress.AutoSize = false;
			this.chkAddress.Location = new System.Drawing.Point(20, 132);
			this.chkAddress.Name = "chkAddress";
			this.chkAddress.Size = new System.Drawing.Size(140, 24);
			this.chkAddress.TabIndex = 4;
			this.chkAddress.Text = "Mailing Address";
			// 
			// chkCorrName
			// 
			this.chkCorrName.AutoSize = false;
			this.chkCorrName.Location = new System.Drawing.Point(20, 98);
			this.chkCorrName.Name = "chkCorrName";
			this.chkCorrName.Size = new System.Drawing.Size(190, 24);
			this.chkCorrName.TabIndex = 3;
			this.chkCorrName.Text = "Correspondence Name";
			// 
			// chkCheckName
			// 
			this.chkCheckName.AutoSize = false;
			this.chkCheckName.Location = new System.Drawing.Point(20, 64);
			this.chkCheckName.Name = "chkCheckName";
			this.chkCheckName.Size = new System.Drawing.Size(118, 24);
			this.chkCheckName.TabIndex = 2;
			this.chkCheckName.Text = "Check Name";
			// 
			// chkVendorNumber
			// 
			this.chkVendorNumber.AutoSize = false;
			this.chkVendorNumber.Location = new System.Drawing.Point(20, 30);
			this.chkVendorNumber.Name = "chkVendorNumber";
			this.chkVendorNumber.Size = new System.Drawing.Size(140, 24);
			this.chkVendorNumber.TabIndex = 1;
			this.chkVendorNumber.Text = "Vendor Number";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 430);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(85, 48);
			this.btnProcess.TabIndex = 4;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmVendorExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(551, 472);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVendorExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vendor Extract";
			this.Load += new System.EventHandler(this.frmVendorExtract_Load);
			this.Activated += new System.EventHandler(this.frmVendorExtract_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendorExtract_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkTelephone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkContact)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVendorNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
