﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptRevenueDetail.
	/// </summary>
	public partial class rptRevenueDetail : BaseSectionReport
	{
		public static rptRevenueDetail InstancePtr
		{
			get
			{
				return (rptRevenueDetail)Sys.GetInstance(typeof(rptRevenueDetail));
			}
		}

		protected rptRevenueDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRevenueDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool DeptBreakFlag;
		bool DivBreakFlag;
		clsDRWrapper rs = new clsDRWrapper();
		int counter;
		int counter2;
		int TotRows;
		int counter3;
		float lngTotalWidth;
		int temp;
		int lngRowCounter;
		bool blnFirstRow;
		int lngColumnCounter;
		bool blnShade = true;
		bool blnReportShading;
		bool blnFirstDivision;
		string strTitleToShow = "";
		bool IsColorChanged = false;

		public rptRevenueDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Revenue Detail";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngTempCounter;
			int counter;
			bool blnData = false;
			lngTempCounter = 0;
			if (lngRowCounter > frmRevenueDetail.InstancePtr.vs1.Rows - 1)
			{
				eArgs.EOF = true;
				return;
			}
			if (blnFirstRow)
			{
				blnFirstRow = false;
				return;
			}
			if (frmRevenueDetail.InstancePtr.vs1.IsCollapsed(lngRowCounter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				do
				{
					lngTempCounter += 1;
					if (lngRowCounter + lngTempCounter > frmRevenueDetail.InstancePtr.vs1.Rows)
					{
						eArgs.EOF = true;
						return;
					}
				}
				while (!(frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) >= frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter + lngTempCounter)));
				lngRowCounter += lngTempCounter;
			}
			else
			{
				lngRowCounter += 1;
				if (lngRowCounter > frmRevenueDetail.InstancePtr.vs1.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
				blnData = false;
				for (counter = 0; counter <= frmRevenueDetail.InstancePtr.vs1.Cols - 1; counter++)
				{
					if (frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, counter) != "")
					{
						blnData = true;
						break;
					}
				}
				if (!blnData)
				{
					lngRowCounter += 1;
				}
				if (lngRowCounter > frmRevenueDetail.InstancePtr.vs1.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
			}
			eArgs.EOF = false;
			if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0 && !frmRevenueDetail.InstancePtr.IsTotalRow(lngRowCounter))
			{
				blnFirstDivision = true;
				if (lngRowCounter < frmRevenueDetail.InstancePtr.vs1.Rows - 1 && DeptBreakFlag)
				{
					this.Fields["Binder"].Value = lngRowCounter;
				}
			}
			if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1 && lngRowCounter < frmRevenueDetail.InstancePtr.vs1.Rows - 1 && DivBreakFlag && !frmRevenueDetail.InstancePtr.IsTotalRow(lngRowCounter))
			{
				if (blnFirstDivision)
				{
					blnFirstDivision = false;
				}
				else
				{
					this.Fields["Binder"].Value = lngRowCounter;
				}
			}
			// If .RowOutlineLevel(lngRowCounter) = 0 And lngRowCounter < .rows - 1 And DeptBreakFlag And Not frmRevenueDetail.IsTotalRow(lngRowCounter) Then
			// rptRevenueDetail.Fields["Binder"].Value = lngRowCounter
			// End If
			// If .RowOutlineLevel(lngRowCounter) = 1 And lngRowCounter < .rows - 1 And DivBreakFlag And Not frmRevenueDetail.IsTotalRow(lngRowCounter) Then
			// rptRevenueDetail.Fields["Binder"].Value = lngRowCounter
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			double dblAdjust;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			lngRowCounter = 0 + frmRevenueDetail.InstancePtr.vs1.FixedRows;
			blnFirstRow = true;
			dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("REPORTADJUSTMENT"));
			this.PageSettings.Margins.Top += FCConvert.ToSingle(200 * dblAdjust / 1440f);
			if (frmRevenueDetail.InstancePtr.strTitle != "")
			{
				Label1.Text = frmRevenueDetail.InstancePtr.strTitle;
			}
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ShadeReports")) == "Y")
			{
				blnReportShading = true;
			}
			else
			{
				blnReportShading = false;
			}
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DepartmentBreak") == true)
			{
				DeptBreakFlag = true;
			}
			else
			{
				DeptBreakFlag = false;
			}
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DivisionBreak") == true && !modAccountTitle.Statics.ExpDivFlag)
			{
				DivBreakFlag = true;
			}
			else
			{
				DivBreakFlag = false;
			}
			if (!DeptBreakFlag && !DivBreakFlag)
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			// get the report format for the report we want printed
			rs.OpenRecordset("SELECT * FROM RevenueDetailFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				// do nothing
			}
			else
			{
				Label1.Font = new Font("Roman 10cpi", Label1.Font.Size, FontStyle.Bold);
				Label1.Font = new Font(Label1.Font.Name, 10);
				Label2.Font = new Font("Roman 12cpi", Label2.Font.Size);
				Label2.Font = new Font(Label2.Font.Name, 10);
				Label3.Font = new Font("Roman 12cpi", Label3.Font.Size);
				Label3.Font = new Font(Label3.Font.Name, 10);
				Label4.Font = new Font("Roman 12cpi", Label4.Font.Size);
				Label4.Font = new Font(Label4.Font.Name, 10);
				Label5.Font = new Font("Roman 12cpi", Label5.Font.Size);
				Label5.Font = new Font(Label5.Font.Name, 10);
				Label6.Font = new Font("Roman 12cpi", Label6.Font.Size);
				Label6.Font = new Font(Label6.Font.Name, 10);
				Label7.Font = new Font("Roman 12cpi", Label7.Font.Size);
				Label7.Font = new Font(Label7.Font.Name, 10);
			}
			// if we are using wide paper let the print object know what type we are using
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				//rptRevenueDetail.InstancePtr.Document.Printer.PaperSize = 39;
				this.PrintWidth = 20420 / 1440f;
				Line1.X2 = this.PrintWidth - 400 / 1440f;
				Line2.X2 = this.PrintWidth - 400 / 1440f;
				Label1.Width = this.PrintWidth - (Label3.Width * 2);
				Label5.Width = this.PrintWidth - (Label3.Width * 2);
				Label6.Width = this.PrintWidth - (Label3.Width * 2);
				Label3.Left = this.PrintWidth - (Label3.Width + 400 / 1440F);
				Label4.Left = this.PrintWidth - (Label4.Width + 400 / 1440F);
			}
			else if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				rptRevenueDetail.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 15100 / 1440f;
				Line1.X2 = this.PrintWidth - 400 / 1440f;
				Line2.X2 = this.PrintWidth - 400 / 1440f;
				Label1.Width = this.PrintWidth - (Label3.Width * 2);
				Label5.Width = this.PrintWidth - (Label3.Width * 2);
				Label6.Width = this.PrintWidth - (Label3.Width * 2);
				Label3.Left = this.PrintWidth - (Label3.Width + 400 / 1440F);
				Label4.Left = this.PrintWidth - (Label4.Width + 400 / 1440F);
			}
			fldDeptTitle.Width = this.PrintWidth - 400 / 1440f;
			// show information about what departments are being reported for which months
			if (frmRevenueDetail.InstancePtr.lblRangeDept.Text != "ALL")
			{
				Label6.Text = frmRevenueDetail.InstancePtr.lblTitle.Text + ": " + frmRevenueDetail.InstancePtr.lblRangeDept.Text;
				if (frmRevenueDetail.InstancePtr.lblMonths.Text == "ALL")
				{
					Label5.Text = frmRevenueDetail.InstancePtr.lblMonths.Text + " Months";
				}
				else
				{
					Label5.Text = frmRevenueDetail.InstancePtr.lblMonths;
				}
			}
			else
			{
				Label6.Text = "ALL Accounts";
				if (frmRevenueDetail.InstancePtr.lblMonths.Text == "ALL")
				{
					Label5.Text = frmRevenueDetail.InstancePtr.lblMonths.Text + " Months";
				}
				else
				{
					Label5.Text = frmRevenueDetail.InstancePtr.lblMonths;
				}
			}
			lngTotalWidth = 0;
			lngColumnCounter = 1;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 2)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field1, 0);
					FormatFixedFields_6(Field14, 1);
				}
				else
				{
					FormatFixedFields_6(Field14, 1);
				}
				FormatFields(ref fld1);
			}
			lngColumnCounter = 2;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 3)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field2, 0);
					FormatFixedFields_6(Field15, 1);
				}
				else
				{
					FormatFixedFields_6(Field15, 1);
				}
				FormatFields(ref fld2);
			}
			lngColumnCounter = 3;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 4)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field3, 0);
					FormatFixedFields_6(Field16, 1);
				}
				else
				{
					FormatFixedFields_6(Field16, 1);
				}
				FormatFields(ref fld3);
			}
			lngColumnCounter = 4;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 5)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field4, 0);
					FormatFixedFields_6(Field17, 1);
				}
				else
				{
					FormatFixedFields_6(Field17, 1);
				}
				FormatFields(ref fld4);
			}
			lngColumnCounter = 5;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 6)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field5, 0);
					FormatFixedFields_6(Field18, 1);
				}
				else
				{
					FormatFixedFields_6(Field18, 1);
				}
				FormatFields(ref fld5);
			}
			lngColumnCounter = 6;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 7)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field6, 0);
					FormatFixedFields_6(Field19, 1);
				}
				else
				{
					FormatFixedFields_6(Field19, 1);
				}
				FormatFields(ref fld6);
			}
			lngColumnCounter = 7;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 8)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field7, 0);
					FormatFixedFields_6(Field20, 1);
				}
				else
				{
					FormatFixedFields_6(Field20, 1);
				}
				FormatFields(ref fld7);
			}
			lngColumnCounter = 8;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 9)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field8, 0);
					FormatFixedFields_6(Field21, 1);
				}
				else
				{
					FormatFixedFields_6(Field21, 1);
				}
				FormatFields(ref fld8);
			}
			lngColumnCounter = 9;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 10)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field9, 0);
					FormatFixedFields_6(Field22, 1);
				}
				else
				{
					FormatFixedFields_6(Field22, 1);
				}
				FormatFields(ref fld9);
			}
			lngColumnCounter = 10;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 11)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field10, 0);
					FormatFixedFields_6(Field23, 1);
				}
				else
				{
					FormatFixedFields_6(Field23, 1);
				}
				FormatFields(ref fld10);
			}
			lngColumnCounter = 11;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 12)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field11, 0);
					FormatFixedFields_6(Field24, 1);
				}
				else
				{
					FormatFixedFields_6(Field24, 1);
				}
				FormatFields(ref fld11);
			}
			lngColumnCounter = 12;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 13)
			{
				if (frmRevenueDetail.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field12, 0);
					FormatFixedFields_6(Field25, 1);
				}
				else
				{
					FormatFixedFields_6(Field25, 1);
				}
				FormatFields(ref fld12);
			}
			lngColumnCounter = 1;
			FormatFields(ref fldDescription);
			fldDescription.Visible = false;
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				fldDescription.Left = 0;
				fldDescription.Width = fld10.Left;
				fld1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field14.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field15.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field16.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field17.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field18.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field19.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field20.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field21.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field22.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				if (fld12.Visible == true)
				{
					this.PrintWidth = fld12.Left + fld12.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
					Label1.Width = this.PrintWidth;
					Label5.Width = this.PrintWidth;
					Label6.Width = this.PrintWidth;
					Label3.Left = this.PrintWidth - Label3.Width;
					Label4.Left = this.PrintWidth - Label4.Width;
				}
				else
				{
					this.PrintWidth = fld11.Left + fld11.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
					Label1.Width = this.PrintWidth;
					Label5.Width = this.PrintWidth;
					Label6.Width = this.PrintWidth;
					Label3.Left = this.PrintWidth - Label3.Width;
					Label4.Left = this.PrintWidth - Label4.Width;
				}
			}
			else if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				fldDescription.Left = 0;
				fldDescription.Width = fld5.Left;
				fld1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field14.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field15.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field16.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field17.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				if (fld8.Visible == true)
				{
					this.PrintWidth = fld8.Left + fld8.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
				}
				else
				{
					this.PrintWidth = fld7.Left + fld7.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
				}
			}
			else
			{
				fldDescription.Left = 0;
				fldDescription.Width = fld4.Left;
				fld1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field14.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field15.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				Field16.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				fld4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fld12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				if (fld7.Visible == true)
				{
					this.PrintWidth = fld7.Left + fld7.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
				}
				else
				{
					this.PrintWidth = fld6.Left + fld6.Width;
					Line1.X2 = this.PrintWidth;
					Line2.X2 = this.PrintWidth;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!frmRevenueDetail.InstancePtr.IsTotalRow(lngRowCounter) && !frmRevenueDetail.InstancePtr.IsDetailRow(lngRowCounter))
			{
				if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
				{
					fldDescription.Visible = true;
					fld1.Visible = false;
					fld2.Visible = false;
					fld3.Visible = false;
					fld4.Visible = false;
					fld5.Visible = false;
					fld6.Visible = false;
					fld7.Visible = false;
					fld8.Visible = false;
					fld9.Visible = false;
					lngColumnCounter = 1;
					PrintFields(ref fldDescription);
				}
				else
				{
					fldDescription.Visible = true;
					fld1.Visible = false;
					fld2.Visible = false;
					fld3.Visible = false;
					lngColumnCounter = 1;
					PrintFields(ref fldDescription);
				}
			}
			else
			{
				if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
				{
					fldDescription.Visible = false;
					fld1.Visible = true;
					fld2.Visible = true;
					fld3.Visible = true;
					fld4.Visible = true;
					fld5.Visible = true;
					fld6.Visible = true;
					fld7.Visible = true;
					fld8.Visible = true;
					fld9.Visible = true;
				}
				else
				{
					fldDescription.Visible = false;
					fld1.Visible = true;
					fld2.Visible = true;
					fld3.Visible = true;
				}
			}
			// If rs.Fields["Printer"] = "D" Then
			// If .RowOutlineLevel(lngRowCounter) = 0 And lngRowCounter <> .rows - 1 Then
			// Line2.Visible = True
			// Else
			// Line2.Visible = False
			// End If
			// End If
			if (!frmRevenueDetail.InstancePtr.IsTotalRow(lngRowCounter) && !frmRevenueDetail.InstancePtr.IsDetailRow(lngRowCounter))
			{
				if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
				{
					goto WideSkip;
				}
				else
				{
					goto NarrowSkip;
				}
			}
			lngColumnCounter = 1;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 2)
			{
				PrintFields(ref fld1);
			}
			lngColumnCounter = 2;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 3)
			{
				PrintFields(ref fld2);
			}
			lngColumnCounter = 3;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 4)
			{
				PrintFields(ref fld3);
			}
			NarrowSkip:
			;
			lngColumnCounter = 4;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 5)
			{
				PrintFields(ref fld4);
			}
			lngColumnCounter = 5;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 6)
			{
				PrintFields(ref fld5);
			}
			lngColumnCounter = 6;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 7)
			{
				PrintFields(ref fld6);
			}
			lngColumnCounter = 7;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 8)
			{
				PrintFields(ref fld7);
			}
			lngColumnCounter = 8;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 9)
			{
				PrintFields(ref fld8);
			}
			lngColumnCounter = 9;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 10)
			{
				PrintFields(ref fld9);
			}
			WideSkip:
			;
			lngColumnCounter = 10;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 11)
			{
				PrintFields(ref fld10);
			}
			lngColumnCounter = 11;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 12)
			{
				PrintFields(ref fld11);
			}
			lngColumnCounter = 12;
			if (frmRevenueDetail.InstancePtr.vs1.Cols >= 13)
			{
				PrintFields(ref fld12);
			}
			//FC:FINAL:MSH - moved for correct colors changing (same with issue #685)
			if (blnReportShading && !IsColorChanged)
			{
				//this.Detail.BackStyle = ddBKNormal;
				if (blnShade)
				{
					blnShade = false;
					this.Detail.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld1.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld2.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld3.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld4.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld5.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld6.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld7.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld8.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld9.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld10.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld11.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld12.BackColor = ColorTranslator.FromOle(0xE0E0E0);
				}
				else
				{
					blnShade = true;
					this.Detail.BackColor = Color.White;
					fld1.BackColor = Color.White;
					fld2.BackColor = Color.White;
					fld3.BackColor = Color.White;
					fld4.BackColor = Color.White;
					fld5.BackColor = Color.White;
					fld6.BackColor = Color.White;
					fld7.BackColor = Color.White;
					fld8.BackColor = Color.White;
					fld9.BackColor = Color.White;
					fld10.BackColor = Color.White;
					fld11.BackColor = Color.White;
					fld12.BackColor = Color.White;
				}
			}
		}

		private void FormatFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
            using (clsDRWrapper rsFormat = new clsDRWrapper())
            {
                rsFormat.OpenRecordset("SELECT * FROM RevenueDetailFormats WHERE ID = " +
                                       FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
                x.Visible = true;
                if (modAccountTitle.Statics.RevDivFlag)
                {
                    x.Left = lngTotalWidth + 400 / 1440f;
                }
                else
                {
                    x.Left = lngTotalWidth + 600 / 1440f;
                }

                if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
                {
                    x.Width = frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) / 1440f;
                }
                else
                {
                    x.Width = (frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) - 100) / 1440f;
                }

                x.OutputFormat = frmRevenueDetail.InstancePtr.vs1.ColFormat(lngColumnCounter);
                lngTotalWidth += x.Width;
                if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
                {
                    fld1.Height = 290 / 1440f;
                    rptRevenueDetail.InstancePtr.Detail.Height = fld1.Height + 50 / 1440f;
                    fld1.Font = new Font(fld1.Font, FontStyle.Bold);
                }
                else
                {
                    fld1.Height = 240 / 1440f;
                    rptRevenueDetail.InstancePtr.Detail.Height = fld1.Height + 50 / 1440f;
                    fld1.Font = new Font(fld1.Font, FontStyle.Regular);
                }

                if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
                {
                    x.Font = new Font(x.Font.Name, 10);
                }
                else if (rsFormat.Get_Fields_String("Font") == "L")
                {
                    x.Font = new Font(x.Font.Name, 12);
                }
                else
                {
                    FontStyle style = FontStyle.Regular;
                    if (rsFormat.Get_Fields_Boolean("Bold"))
                        style = FontStyle.Bold;
                    if (rsFormat.Get_Fields_Boolean("Italic"))
                        style |= FontStyle.Italic;
                    if (rsFormat.Get_Fields_Boolean("StrikeThru"))
                        style |= FontStyle.Strikeout;
                    if (rsFormat.Get_Fields_Boolean("Underline"))
                        style |= FontStyle.Underline;
                    // TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
                    x.Font = new Font(rsFormat.Get_Fields_String("FontName"),
                        FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
                }
            }
        }

		private void PrintFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
			{
				strTitleToShow = frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, 1);
				//fldDeptTitle.BackStyle = 1;
				fldDeptTitle.Height = 290 / 1440f;
				fldDeptTitle.Font = new Font(fldDeptTitle.Font, FontStyle.Regular);
				fldDeptTitle.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
				if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
				{
					if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 8);
					}
					else if (rs.Get_Fields_String("Font") == "L")
					{
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 10);
					}
					else
					{
						FontStyle style = FontStyle.Regular;
						if (rs.Get_Fields_Boolean("Bold"))
							style = FontStyle.Bold;
						if (rs.Get_Fields_Boolean("Italic"))
							style |= FontStyle.Italic;
						if (rs.Get_Fields_Boolean("StrikeThru"))
							style |= FontStyle.Strikeout;
						if (rs.Get_Fields_Boolean("Underline"))
							style |= FontStyle.Underline;
						// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, rs.Get_Fields("FontSize"));
						fldDeptTitle.Font = new Font(rs.Get_Fields_String("FontName"), fldDeptTitle.Font.Size, style);
					}
					fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, fldDeptTitle.Font.Size + 1);
					//.Size += 1;
				}
				else
				{
					fldDeptTitle.Font = new Font("Roman 17cpi", fldDeptTitle.Font.Size);
					fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 10);
				}
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				//if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
				//{
				//	x.BackStyle = 1;
				//}
				//else
				//{
				//	x.BackStyle = 0;
				//}
				//FC:FINAL:MSH - in VB6 we have BackStyle property, which can change opacity of textbox background.
				// In web we can't change opacity of backround, so we must change color of TextBox background (same with issue #685)
				if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
				{
					//x.BackStyle = 1;
					this.Detail.BackColor = Color.FromArgb(192, 192, 192);
					x.BackColor = Color.FromArgb(192, 192, 192);
					IsColorChanged = true;
				}
				else
				{
					//x.BackStyle = 0;
					this.Detail.BackColor = Color.White;
					x.BackColor = Color.White;
					IsColorChanged = false;
				}
			}
			if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
			{
				x.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
			}
			else
			{
				x.VerticalAlignment = 0;
			}
			if (ColorTranslator.FromOle(frmRevenueDetail.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.Black || ColorTranslator.FromOle(frmRevenueDetail.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.White || frmRevenueDetail.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter) == 0x05CC47)
			{
				x.ForeColor = Color.Black;
			}
			else
			{
				x.ForeColor = ColorTranslator.FromOle(frmRevenueDetail.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter));
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					x.Font = new Font(x.Font.Name, 8);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					x.Font = new Font(x.Font.Name, 10);
				}
				else
				{
					FontStyle style = FontStyle.Regular;
					if (rs.Get_Fields_Boolean("Bold"))
						style = FontStyle.Bold;
					if (rs.Get_Fields_Boolean("Italic"))
						style |= FontStyle.Italic;
					if (rs.Get_Fields_Boolean("StrikeThru"))
						style |= FontStyle.Strikeout;
					if (rs.Get_Fields_Boolean("Underline"))
						style |= FontStyle.Underline;
					// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
					x.Font = new Font(rs.Get_Fields_String("FontName"), FCConvert.ToInt32(rs.Get_Fields("FontSize")) + 2, style);
				}
				if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 0)
				{
					if (lngColumnCounter == 1)
					{
						x.Font = new Font(x.Font.Name, x.Font.Size + 1);
					}
				}
				if (frmRevenueDetail.InstancePtr.vs1.ColFormat(lngColumnCounter) == "")
				{
					x.Text = frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter);
				}
				else
				{
					x.Text = Strings.Format(frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter), frmRevenueDetail.InstancePtr.vs1.ColFormat(lngColumnCounter));
				}
			}
			else
			{
				x.Font = new Font("Roman 17cpi", x.Font.Size);
				x.Font = new Font(x.Font.Name, 10);
				if (frmRevenueDetail.InstancePtr.vs1.ColFormat(lngColumnCounter) == "")
				{
					x.Text = frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter);
				}
				else
				{
					x.Text = Strings.Format(frmRevenueDetail.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter), frmRevenueDetail.InstancePtr.vs1.ColFormat(lngColumnCounter));
				}
			}
			//FC:FINAL:MSH - moved to the end, because previously the FontStyle was redefined in the end (same with issue #685)
			if (frmRevenueDetail.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
			{
				x.Height = 290 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Bold);
			}
			else if (frmRevenueDetail.InstancePtr.IsTotalRowToBold(lngRowCounter))
			{
				x.Height = 240 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Bold);
			}
			else if (lngRowCounter == frmRevenueDetail.InstancePtr.vs1.Rows - 1)
			{
				x.Height = 290 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Bold);
			}
			else
			{
				x.Height = 240 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Regular);
			}
		}

		private void FormatFixedFields_6(GrapeCity.ActiveReports.SectionReportModel.TextBox x, short TempRow)
		{
			FormatFixedFields(x, ref TempRow);
		}

		private void FormatFixedFields(GrapeCity.ActiveReports.SectionReportModel.TextBox x, ref short TempRow)
		{
			x.Visible = true;
			if (modAccountTitle.Statics.RevDivFlag)
			{
				x.Left = lngTotalWidth + 400 / 1440f;
			}
			else
			{
				x.Left = lngTotalWidth + 600 / 1440f;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				if (lngColumnCounter == 1 && TempRow == 0)
				{
					x.Width = (frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) + frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter + 1)) / 1440f;
				}
				else
				{
					x.Width = frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) / 1440f;
				}
			}
			else
			{
				if (lngColumnCounter == 1 && TempRow == 0)
				{
					x.Width = (frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) + frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter + 1) + frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter + 2)) / 1440f;
				}
				else
				{
					x.Width = frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) / 1440f;
				}
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					x.Font = new Font(x.Font.Name, 10);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					x.Font = new Font(x.Font.Name, 12);
				}
				else
				{
					FontStyle style = FontStyle.Regular;
					if (rs.Get_Fields_Boolean("Bold"))
						style = FontStyle.Bold;
					if (rs.Get_Fields_Boolean("Italic"))
						style |= FontStyle.Italic;
					if (rs.Get_Fields_Boolean("StrikeThru"))
						style |= FontStyle.Strikeout;
					if (rs.Get_Fields_Boolean("Underline"))
						style |= FontStyle.Underline;
					// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
					x.Font = new Font(rs.Get_Fields_String("FontName"), FCConvert.ToInt32(rs.Get_Fields("FontSize")) + 2, style);
				}
			}
			else
			{
				if (frmRevenueDetail.InstancePtr.vs1.TextMatrix(TempRow, lngColumnCounter) == "Account------------")
				{
					x.Width = (frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) + 500) / 1440f;
				}
				else
				{
					x.Width = (frmRevenueDetail.InstancePtr.vs1.ColWidth(lngColumnCounter) - 100) / 1440f;
				}
				x.Font = new Font("Roman 12cpi", x.Font.Size);
				x.Font = new Font(x.Font.Name, 10);
			}
			x.Text = frmRevenueDetail.InstancePtr.vs1.TextMatrix(TempRow, lngColumnCounter);
			if (x.Text == "CURRENT MONTH")
			{
				x.Text = "Curr Mnth";
			}
			else if (x.Text == "YEAR TO DATE")
			{
				x.Text = "YTD";
			}
			else if (x.Text == "ENCUMBRANCE")
			{
				x.Text = "Encum";
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			if (this.PageNumber == 1)
			{
				fldDeptTitle.Text = "";
				fldDeptTitle.Visible = false;
			}
			else
			{
				fldDeptTitle.Text = strTitleToShow + " CONT'D";
				fldDeptTitle.Visible = true;
			}
		}

		
	}
}
