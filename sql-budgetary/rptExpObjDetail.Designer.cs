﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptExpObjDetail.
	/// </summary>
	partial class rptExpObjDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExpObjDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDates = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccounts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblExpObj = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTransDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPostedDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldObjDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldObjCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpObj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPostedDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldObjDebitTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldObjCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalDebitTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCreditTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTransDate,
				this.fldPostedDate,
				this.fldPeriod,
				this.fldType,
				this.fldRCB,
				this.fldJournal,
				this.fldWarrant,
				this.fldCheck,
				this.fldVendor,
				this.fldAccount,
				this.fldDebits,
				this.fldCredits
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.Label24,
				this.fldFinalDebitTotal,
				this.fldFinalCreditTotal
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblDates,
				this.lblAccounts,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.Line1
			});
			this.PageHeader.Height = 1.052083F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblExpObj,
				this.Binder
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.Label23,
				this.fldObjDebitTotal,
				this.fldObjCreditTotal
			});
			this.GroupFooter1.Height = 0.1979167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Exp / Obj Detail Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.375F;
			// 
			// lblDates
			// 
			this.lblDates.Height = 0.1875F;
			this.lblDates.HyperLink = null;
			this.lblDates.Left = 1.5F;
			this.lblDates.Name = "lblDates";
			this.lblDates.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDates.Text = "Label5";
			this.lblDates.Top = 0.40625F;
			this.lblDates.Width = 7.375F;
			// 
			// lblAccounts
			// 
			this.lblAccounts.Height = 0.1875F;
			this.lblAccounts.HyperLink = null;
			this.lblAccounts.Left = 1.5F;
			this.lblAccounts.Name = "lblAccounts";
			this.lblAccounts.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblAccounts.Text = "Label6";
			this.lblAccounts.Top = 0.21875F;
			this.lblAccounts.Width = 7.375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 8.875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 8.875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.28125F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "text-align: center";
			this.Label9.Text = "Trans";
			this.Label9.Top = 0.71875F;
			this.Label9.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.28125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "text-align: center";
			this.Label10.Text = "Date";
			this.Label10.Top = 0.875F;
			this.Label10.Width = 0.625F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.96875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "text-align: center";
			this.Label11.Text = "Posted";
			this.Label11.Top = 0.71875F;
			this.Label11.Width = 0.625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.96875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "text-align: center";
			this.Label12.Text = "Date";
			this.Label12.Top = 0.875F;
			this.Label12.Width = 0.625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 1.65625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "text-align: center";
			this.Label13.Text = "Per";
			this.Label13.Top = 0.875F;
			this.Label13.Width = 0.3125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.19F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "text-align: center";
			this.Label14.Text = "Type";
			this.Label14.Top = 0.875F;
			this.Label14.Width = 0.375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.19F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.5F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "text-align: center";
			this.Label15.Text = "RCB";
			this.Label15.Top = 0.875F;
			this.Label15.Width = 0.3125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "text-align: center";
			this.Label16.Text = "Jrnl";
			this.Label16.Top = 0.875F;
			this.Label16.Width = 0.375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.3125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "text-align: center";
			this.Label17.Text = "Wrnt";
			this.Label17.Top = 0.875F;
			this.Label17.Width = 0.3125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.6875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "text-align: left";
			this.Label18.Text = "Check #";
			this.Label18.Top = 0.875F;
			this.Label18.Width = 0.5625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4.40625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "text-align: left";
			this.Label19.Text = "Vendor";
			this.Label19.Top = 0.875F;
			this.Label19.Width = 2.25F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 6.71875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "text-align: left";
			this.Label20.Text = "Account";
			this.Label20.Top = 0.875F;
			this.Label20.Width = 1.1875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 7.96875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "text-align: right";
			this.Label21.Text = "Debits";
			this.Label21.Top = 0.875F;
			this.Label21.Width = 0.96875F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 9.03125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "text-align: right";
			this.Label22.Text = "Credits";
			this.Label22.Top = 0.875F;
			this.Label22.Width = 0.96875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.03125F;
			this.Line1.Width = 10.4375F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 10.46875F;
			this.Line1.Y1 = 1.03125F;
			this.Line1.Y2 = 1.03125F;
			// 
			// lblExpObj
			// 
			this.lblExpObj.Height = 0.1875F;
			this.lblExpObj.HyperLink = null;
			this.lblExpObj.Left = 0F;
			this.lblExpObj.Name = "lblExpObj";
			this.lblExpObj.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblExpObj.Text = null;
			this.lblExpObj.Top = 0.0625F;
			this.lblExpObj.Width = 4.1875F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 4.53125F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.0625F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.625F;
			// 
			// fldTransDate
			// 
			this.fldTransDate.Height = 0.19F;
			this.fldTransDate.Left = 0.28125F;
			this.fldTransDate.Name = "fldTransDate";
			this.fldTransDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldTransDate.Text = "08/08/2006";
			this.fldTransDate.Top = 0F;
			this.fldTransDate.Width = 0.625F;
			// 
			// fldPostedDate
			// 
			this.fldPostedDate.Height = 0.19F;
			this.fldPostedDate.Left = 0.96875F;
			this.fldPostedDate.Name = "fldPostedDate";
			this.fldPostedDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPostedDate.Text = "08/08/2006";
			this.fldPostedDate.Top = 0F;
			this.fldPostedDate.Width = 0.625F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.19F;
			this.fldPeriod.Left = 1.65625F;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPeriod.Text = "08/08/2006";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.3125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.19F;
			this.fldType.Left = 2.0625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "08/08/2006";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.375F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.19F;
			this.fldRCB.Left = 2.5F;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldRCB.Text = "08/08/2006";
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.3125F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.19F;
			this.fldJournal.Left = 2.875F;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldJournal.Text = "08/08/2006";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.375F;
			// 
			// fldWarrant
			// 
			this.fldWarrant.Height = 0.19F;
			this.fldWarrant.Left = 3.3125F;
			this.fldWarrant.Name = "fldWarrant";
			this.fldWarrant.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldWarrant.Text = "08/08/2006";
			this.fldWarrant.Top = 0F;
			this.fldWarrant.Width = 0.3125F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.19F;
			this.fldCheck.Left = 3.6875F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldCheck.Text = "08/08/2006";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.5625F;
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.19F;
			this.fldVendor.Left = 4.40625F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldVendor.Text = "08/08/2006";
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 2.25F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 6.71875F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldAccount.Text = "08/08/2006";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.1875F;
			// 
			// fldDebits
			// 
			this.fldDebits.Height = 0.19F;
			this.fldDebits.Left = 7.96875F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = "99,999,999.00";
			this.fldDebits.Top = 0F;
			this.fldDebits.Width = 0.96875F;
			// 
			// fldCredits
			// 
			this.fldCredits.Height = 0.19F;
			this.fldCredits.Left = 9.03125F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = "08/08/2006";
			this.fldCredits.Top = 0F;
			this.fldCredits.Width = 0.96875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 6.78125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 3.25F;
			this.Line2.X1 = 10.03125F;
			this.Line2.X2 = 6.78125F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.19F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 6.875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right";
			this.Label23.Text = "Object Totals:";
			this.Label23.Top = 0.03125F;
			this.Label23.Width = 1.03125F;
			// 
			// fldObjDebitTotal
			// 
			this.fldObjDebitTotal.Height = 0.19F;
			this.fldObjDebitTotal.Left = 7.96875F;
			this.fldObjDebitTotal.Name = "fldObjDebitTotal";
			this.fldObjDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldObjDebitTotal.Text = "99,999,999.00";
			this.fldObjDebitTotal.Top = 0.03125F;
			this.fldObjDebitTotal.Width = 0.96875F;
			// 
			// fldObjCreditTotal
			// 
			this.fldObjCreditTotal.Height = 0.19F;
			this.fldObjCreditTotal.Left = 9.03125F;
			this.fldObjCreditTotal.Name = "fldObjCreditTotal";
			this.fldObjCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldObjCreditTotal.Text = "08/08/2006";
			this.fldObjCreditTotal.Top = 0.03125F;
			this.fldObjCreditTotal.Width = 0.96875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 6.78125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 3.25F;
			this.Line3.X1 = 10.03125F;
			this.Line3.X2 = 6.78125F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.19F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 6.875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right";
			this.Label24.Text = "Final Totals:";
			this.Label24.Top = 0.03125F;
			this.Label24.Width = 1.03125F;
			// 
			// fldFinalDebitTotal
			// 
			this.fldFinalDebitTotal.Height = 0.19F;
			this.fldFinalDebitTotal.Left = 7.96875F;
			this.fldFinalDebitTotal.Name = "fldFinalDebitTotal";
			this.fldFinalDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalDebitTotal.Text = "99,999,999.00";
			this.fldFinalDebitTotal.Top = 0.03125F;
			this.fldFinalDebitTotal.Width = 0.96875F;
			// 
			// fldFinalCreditTotal
			// 
			this.fldFinalCreditTotal.Height = 0.19F;
			this.fldFinalCreditTotal.Left = 9.03125F;
			this.fldFinalCreditTotal.Name = "fldFinalCreditTotal";
			this.fldFinalCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalCreditTotal.Text = "08/08/2006";
			this.fldFinalCreditTotal.Top = 0.03125F;
			this.fldFinalCreditTotal.Width = 0.96875F;
			// 
			// rptExpObjDetail
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.Name = "Exp / Obj Detail Report";
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.1875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExpObj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTransDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPostedDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldObjDebitTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldObjCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalDebitTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCreditTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTransDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPostedDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalDebitTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCreditTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDates;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccounts;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExpObj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldObjDebitTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldObjCreditTotal;
	}
}
