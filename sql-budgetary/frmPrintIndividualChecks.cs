﻿//Fecher vbPorter - Version 1.0.0.27
//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports;
using SharedApplication.CentralData;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintIndividualChecks.
	/// </summary>
	public partial class frmPrintIndividualChecks : BaseForm
	{
		public frmPrintIndividualChecks()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbInitial.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintIndividualChecks InstancePtr
		{
			get
			{
				return (frmPrintIndividualChecks)Sys.GetInstance(typeof(frmPrintIndividualChecks));
			}
		}

		protected frmPrintIndividualChecks _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/22/02
		// This form will be used by towns to select Unposted AP
		// Journals to print checks for
		// ********************************************************
		int JournalCol;
		int PeriodCol;
		int AmountCol;
		int CountCol;
		int CheckCountCol;
		int VendorNameCol;
		int VendorNumberCol;
		int VendorSeperateCol;
		int VendorAmountCol;
		int VendorAPIDCol;
		int CheckVendorNameCol;
		int CheckVendorNumberCol;
		int CheckNumberCol;
		int CheckAmountCol;
		int CheckAPIDCol;

        private const int cnstBankCol = 6;
        private const int cnstDescriptionCol = 1;

		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsJournals = new clsDRWrapper();
		clsDRWrapper rsJournals_AutoInitialized;

		clsDRWrapper rsJournals
		{
			get
			{
				if (rsJournals_AutoInitialized == null)
				{
					rsJournals_AutoInitialized = new clsDRWrapper();
				}
				return rsJournals_AutoInitialized;
			}
			set
			{
				rsJournals_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsAPJournal = new clsDRWrapper();
		clsDRWrapper rsAPJournal_AutoInitialized;

		clsDRWrapper rsAPJournal
		{
			get
			{
				if (rsAPJournal_AutoInitialized == null)
				{
					rsAPJournal_AutoInitialized = new clsDRWrapper();
				}
				return rsAPJournal_AutoInitialized;
			}
			set
			{
				rsAPJournal_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsEntries = new clsDRWrapper();
		clsDRWrapper rsEntries_AutoInitialized;

		clsDRWrapper rsEntries
		{
			get
			{
				if (rsEntries_AutoInitialized == null)
				{
					rsEntries_AutoInitialized = new clsDRWrapper();
				}
				return rsEntries_AutoInitialized;
			}
			set
			{
				rsEntries_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsCheckInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo_AutoInitialized;

		clsDRWrapper rsCheckInfo
		{
			get
			{
				if (rsCheckInfo_AutoInitialized == null)
				{
					rsCheckInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsCheckInfo_AutoInitialized;
			}
			set
			{
				rsCheckInfo_AutoInitialized = value;
			}
		}

		bool blnLocked;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//cBankService bankService = new cBankService();
		cBankService bankService_AutoInitialized;

		cBankService bankService
		{
			get
			{
				if (bankService_AutoInitialized == null)
				{
					bankService_AutoInitialized = new cBankService();
				}
				return bankService_AutoInitialized;
			}
			set
			{
				bankService_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//cJournalService journService = new cJournalService();
		cJournalService journService_AutoInitialized;

		cJournalService journService
		{
			get
			{
				if (journService_AutoInitialized == null)
				{
					journService_AutoInitialized = new cJournalService();
				}
				return journService_AutoInitialized;
			}
			set
			{
				journService_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingUtility setUtil = new cSettingUtility();
		private cSettingUtility setUtil_AutoInitialized;

		private cSettingUtility setUtil
		{
			get
			{
				if (setUtil_AutoInitialized == null)
				{
					setUtil_AutoInitialized = new cSettingUtility();
				}
				return setUtil_AutoInitialized;
			}
			set
			{
				setUtil_AutoInitialized = value;
			}
		}

		private void frmPrintIndividualChecks_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCheckJournal = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 3)
			{
				MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			this.Refresh();
		}

		private void frmPrintIndividualChecks_Load(object sender, System.EventArgs e)
		{
			blnLocked = false;
			JournalCol = 0;
			PeriodCol = 2;
			AmountCol =3;
			CountCol = 4;
			CheckCountCol = 5;

			VendorAPIDCol = 0;
			VendorNumberCol = 1;
			VendorNameCol = 2;
			VendorSeperateCol = 3;
			VendorAmountCol = 4;

			CheckAPIDCol = 0;
			CheckNumberCol = 1;
			CheckVendorNumberCol = 2;
			CheckVendorNameCol = 3;
			CheckAmountCol = 4;

			vsJournals.TextMatrix(0, cnstBankCol, "Bank");
            vsJournals.TextMatrix(0, cnstDescriptionCol, "Description");
			vsJournals.TextMatrix(0, JournalCol, "Journal");
			vsJournals.TextMatrix(0, PeriodCol, "Period");
			vsJournals.TextMatrix(0, AmountCol, "Amount");
			vsJournals.TextMatrix(0, CountCol, "Entries");
			vsJournals.TextMatrix(0, CheckCountCol, "Checks");
			vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
            vsJournals.ColWidth(cnstDescriptionCol, FCConvert.ToInt32(vsJournals.WidthOriginal * .36));
			vsJournals.ColWidth(PeriodCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
			vsJournals.ColWidth(AmountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.2));
			vsJournals.ColWidth(CountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.08));
			vsJournals.ColWidth(CheckCountCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1));
			vsJournals.ColAlignment(JournalCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsJournals.ColAlignment(PeriodCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsJournals.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsJournals.ColAlignment(CountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsJournals.ColAlignment(CheckCountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsVendors.TextMatrix(0, VendorNumberCol, "Vendor #");
			vsVendors.TextMatrix(0, VendorNameCol, "Vendor Name");
			vsVendors.TextMatrix(0, VendorSeperateCol, "Separate");
			vsVendors.TextMatrix(0, VendorAmountCol, "Amount");
			vsVendors.ColWidth(VendorNumberCol, FCConvert.ToInt32(vsVendors.WidthOriginal * 0.2));
			vsVendors.ColWidth(VendorNameCol, FCConvert.ToInt32(vsVendors.WidthOriginal * 0.4));
			vsVendors.ColWidth(VendorSeperateCol, FCConvert.ToInt32(vsVendors.WidthOriginal * 0.2));
			vsVendors.ColWidth(VendorAmountCol, FCConvert.ToInt32(vsVendors.WidthOriginal * 0.01));
			vsVendors.ColDataType(VendorSeperateCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsVendors.ColAlignment(VendorNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVendors.ColAlignment(VendorNameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVendors.ColAlignment(VendorSeperateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsVendors.ColAlignment(VendorAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsVendors.ColHidden(VendorAPIDCol, true);
			vsVendors.ExtendLastCol = true;

			vsChecks.TextMatrix(0, CheckNumberCol, "Check #");
			vsChecks.TextMatrix(0, CheckVendorNumberCol, "Vendor #");
			vsChecks.TextMatrix(0, CheckVendorNameCol, "Vendor Name");
			vsChecks.TextMatrix(0, CheckAmountCol, "Amount");
			vsChecks.ColWidth(CheckNumberCol, FCConvert.ToInt32(vsChecks.WidthOriginal * 0.2));
			vsChecks.ColWidth(CheckVendorNameCol, FCConvert.ToInt32(vsChecks.WidthOriginal * 0.4));
			vsChecks.ColWidth(CheckVendorNumberCol, FCConvert.ToInt32(vsChecks.WidthOriginal * 0.2));
			vsChecks.ColWidth(CheckAmountCol, FCConvert.ToInt32(vsChecks.WidthOriginal * 0.01));
			vsChecks.ColAlignment(CheckNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsChecks.ColAlignment(CheckVendorNameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsChecks.ColAlignment(CheckVendorNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsChecks.ColAlignment(CheckAmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsChecks.ColHidden(CheckAPIDCol, true);
			vsChecks.ExtendLastCol = true;
			rsCheckInfo.OpenRecordset("SELECT * FROM LastChecksRun");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);

			FormatJournalsGrid();
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmPrintIndividualChecks_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraPayDate.Visible == true)
				{
					Close();
				}
				else
				{
					fraReprint.Visible = false;
					fraVendors.Visible = false;
					Label1.Visible = false;
					fraJournals.Visible = false;
					lblCheckNumber.Visible = false;
					txtCheck.Visible = false;
					fraPayDate.Visible = true;
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPost_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsWarrant = new clsDRWrapper();
			clsDRWrapper rsCustomCheck = new clsDRWrapper();
			int intReturn = 0;
			int lngLastCheck;
			int intChecks;
			//int intBank = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			clsDRWrapper rsCheck = new clsDRWrapper();
			clsDRWrapper rsWarrants = new clsDRWrapper();
			int counter = 0;
			clsDRWrapper rsBanks = new clsDRWrapper();
			cBankService bServ = new cBankService();
			int lngBankID = 0;
            int intJournalRow = vsJournals.Row;
            int intJournalNumberToUse = FCConvert.ToInt32(vsJournals.TextMatrix(intJournalRow,JournalCol));
            lngBankID = vsJournals.TextMatrix(intJournalRow, cnstBankCol).ToIntegerValue();

			if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 3)
			{
				MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (Strings.Trim(txtCheck.Text) == "")
			{
				MessageBox.Show("You must enter a check number before you may proceed", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbInitial.SelectedIndex == 0)
			{
				if (vsVendors.Row <= 0)
				{
					MessageBox.Show("You must select a vendor before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (vsVendors.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVendors.Row, 0) == ColorTranslator.ToOle(SystemColors.GrayText))
				{
					MessageBox.Show("You may not print this check as it has been entered as a prepaid check.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(vsVendors.TextMatrix(vsVendors.Row, VendorNumberCol))));
			}
			else
			{
				if (vsChecks.Row <= 0)
				{
					MessageBox.Show("You must select a check before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(vsChecks.TextMatrix(vsChecks.Row, CheckVendorNumberCol))));
			}
			if (Conversion.Val(vsChecks.TextMatrix(vsChecks.Row, CheckVendorNumberCol)) > 0)
			{
				if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("Vendor " + modValidateAccount.GetFormat_6(vsVendors.TextMatrix(vsVendors.Row, VendorNumberCol), 5) + " has been deleted.  You will need to go enter the vendor back into the system before you may run AP checks.  If you remember entering the journal for this vendor please call TRIO and describe the steps you went through so we can prevent this from happening in the future.", "Enter Vendor Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			
			rsCustomCheck.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + intJournalNumberToUse.ToString() + " AND convert(int, IsNull(CheckNumber, 0)) = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(txtCheck.Text))));
			if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				ans = MessageBox.Show("Check " + rsCustomCheck.Get_Fields("CheckNumber") + " has previously been used.  Are you sure you wish to print these checks?", "Print Checks?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.No)
				{
					return;
				}
			}
			if (cmbInitial.SelectedIndex == 1)
			{
				// DJW@12302010 Took out this clause so it can get voids to see if they are caused by carry overs or not   Status = 'R' AND
				rsCheck.OpenRecordset("SELECT * FROM TempCheckFile WHERE PrintedIndividual = 1 AND CheckNumber = " + FCConvert.ToString(Conversion.Val(vsChecks.TextMatrix(vsChecks.Row, CheckNumberCol))) + " ORDER BY CheckNumber");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsCheck.Get_Fields_String("Status")) == "V" && rsCheck.Get_Fields_Boolean("CarryOver") == false)
					{
						MessageBox.Show("Check # " + FCConvert.ToString(counter) + " is a voided check so it may not be reprinted.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						//cmdPost.Enabled = true;
						FCGlobal.Screen.MousePointer = 0;
						return;
					}
				}
				else
				{
					//cmdPost.Enabled = true;
					FCGlobal.Screen.MousePointer = 0;
					MessageBox.Show("One or more of the checks you are trying to reprint could not be found or has already been run through the check register.", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				lngBankID = 0;
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + intJournalNumberToUse.ToString(),"Budgetary");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					lngBankID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMasterJournal.Get_Fields("BankNumber"))));
				}
				if (lngBankID == 0)
                {
                    using (frmChooseBDBank getBankForm = new frmChooseBDBank())
                    {
                        lngBankID = getBankForm.Init(StaticSettings.GlobalCommandDispatcher.Send(new GetDefaultAPBank()).Result);
                    }

                    if (lngBankID == 0)
                    {
                        MessageBox.Show("You must select a bank or add one to the journal before you can print a check");
                        return;
                    }


                    rsMasterJournal.Execute("update journalmaster set banknumber = " + lngBankID +
                                            " where journalnumber = " + intJournalNumberToUse, "Budgetary");
                    vsJournals.TextMatrix(intJournalRow, cnstBankCol, lngBankID);
                }
			}
            else
            {
                if (lngBankID == 0)
                {
                    using (frmChooseBDBank getBankForm = new frmChooseBDBank())
                    {
                        lngBankID = getBankForm.Init(StaticSettings.GlobalCommandDispatcher.Send(new GetDefaultAPBank()).Result);
                    }

                    if (lngBankID == 0)
                    {
                        MessageBox.Show("You must select a bank or add one to the journal before you can print a check");
                        return;
                    }

					
                    rsMasterJournal.Execute("update journalmaster set banknumber = " + lngBankID +
                                            " where journalnumber = " + intJournalNumberToUse,"Budgetary");
                    vsJournals.TextMatrix(intJournalRow,cnstBankCol,lngBankID);
				}
			}
			
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 0)
			{
				MessageBox.Show("Please insert your checks into the printer and then click OK to continue.", "Insert Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
				
                if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 2)
				{
					rptLaserChecks.InstancePtr.blnIndividualCheck = true;
					
					this.Hide();
					FCGlobal.Screen.MousePointer = 0;
					rptLaserChecks.InstancePtr.Init("", true, lngBankID, true);
					
					FCGlobal.Screen.MousePointer = 0;
					MessageBox.Show("Please remove your checks from the printer when they are done printing and click OK to continue.", "Remove Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else if (Conversion.Val(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckFormat")))) == 4)
				{
					frmSelectCustomCheckFormat.InstancePtr.Init();
					rsBanks.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(lngBankID), "TWBD0000.vb1");
					rsCustomCheck.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
					if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
					{
						rptCustomCheck.InstancePtr.blnIndividualCheck = true;
						
						this.Hide();
						FCGlobal.Screen.MousePointer = 0;
						if (rsBanks.EndOfFile() != true && rsBanks.BeginningOfFile() != true)
						{
                            rptCustomCheck.InstancePtr.Init("", true, rsBanks.Get_Fields_String("RoutingNumber"), rsBanks.Get_Fields_String("BankAccountNumber"), rsBanks.Get_Fields_String("Name"), lngBankID, showModal: true);
                        }
						else
						{
                            rptCustomCheck.InstancePtr.Init("", true, string.Empty, string.Empty, string.Empty, lngBankID, showModal: true);
                        }
						
						FCGlobal.Screen.MousePointer = 0;
					}
					else
					{
						MessageBox.Show("You must set up your custom check format before you may proceed", "Invalid Custom Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
						FCGlobal.Screen.MousePointer = 0;
						return;
					}
					MessageBox.Show("Please remove your checks from the printer when they are done printing and click OK to continue.", "Remove Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					FCGlobal.Screen.MousePointer = 0;
					MessageBox.Show("Unknown Check Format Used", "Invalid Check Format Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				FCGlobal.Screen.MousePointer = 0;
				MessageBox.Show("You must setup a check format before you may proceed", "No Check Format", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			
			FCGlobal.Screen.MousePointer = 0;
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			int lngBankID = 0;
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			int intJournalNumberToUse = vsJournals.Row >= 0 ? FCConvert.ToInt32(vsJournals.TextMatrix(vsJournals.Row, JournalCol)) : 0;
			
			Label1.Visible = true;
			fraJournals.Visible = true;
			lblCheckNumber.Visible = true;
			txtCheck.Visible = true;
			fraPayDate.Visible = false;
			vsJournals.Focus();

			FillDetailInfo();

			if (cmbInitial.SelectedIndex == 0)
			{
				fraVendors.Visible = true;
			}
			else
			{
				fraReprint.Visible = true;
			}
		}

		private void FormatJournalsGrid()
		{
			int counter = 0;
			int intTotalCount = 0;
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			rsJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status = 'E' OR Status = 'V') ORDER BY JournalNumber, Period");
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				rsJournals.MoveLast();
				rsJournals.MoveFirst();
				counter = 1;
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'V')");
					if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                    {
                        vsJournals.Rows = vsJournals.Rows + 1;
						intTotalCount = 0;
						curTotalAmount = 0;
						while (rsAPJournal.EndOfFile() != true)
						{
							rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
							if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
							{
								// TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
								intTotalCount += FCConvert.ToInt32(rsEntries.Get_Fields("TotalEntries"));
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								curTotalAmount += FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields_Decimal("TotalAmount"))) - FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields("TotalDiscount")));
							}
							rsAPJournal.MoveNext();
						}						
						vsJournals.TextMatrix(counter, JournalCol, modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournals.Get_Fields_Int32("JournalNumber")), 4));
                        vsJournals.TextMatrix(counter, cnstDescriptionCol, rsJournals.Get_Fields_String("Description"));
						vsJournals.TextMatrix(counter, PeriodCol, FCConvert.ToString(rsJournals.Get_Fields("Period")));
						vsJournals.TextMatrix(counter, AmountCol, Strings.Format(curTotalAmount, "#,##0.00"));
						vsJournals.TextMatrix(counter, CountCol, FCConvert.ToString(intTotalCount));
                        vsJournals.TextMatrix(counter, cnstBankCol, rsJournals.Get_Fields_Int32("BankNumber").ToString());						
						vsJournals.TextMatrix(counter, CheckCountCol, FCConvert.ToString(CalculateCheckCount_8(rsJournals.Get_Fields_Int32("JournalNumber"), DateTime.Today)));
						counter += 1;
					}
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
                //FC:FINAL:BSE:#4057 remove unnecessary rows
                vsJournals.Rows = counter;
            }

			if (vsJournals.Rows > 1)
			{
				vsJournals.Row = 1;
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			if (blnLocked)
			{
				modBudgetaryMaster.UnlockWarrant();
			}
		}

		private void btnFileProcess_Click(object sender, EventArgs e)
		{
			if (fraPayDate.Visible == true)
			{
				cmdProcess_Click(btnFileProcess, EventArgs.Empty);
			}
			else
			{
				cmdPost_Click(btnFileProcess, EventArgs.Empty);
			}
		}

		private void btnProcessSave_Click(object sender, System.EventArgs e)
		{
			Support.SendKeys("{F10}", false);
		}

		private void txtCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
			{
				keyAscii = 0;
			}
		}

		private void txtCheck_Leave(object sender, System.EventArgs e)
		{
			if (Strings.Trim(txtCheck.Text) == "")
			{
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					txtCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("StartCheckNumber"));
				}
			}
		}
		private short CalculateCheckCount_8(int lngJournal, DateTime datPayable)
		{
			return CalculateCheckCount(ref lngJournal, ref datPayable);
		}

		private short CalculateCheckCount(ref int lngJournal, ref DateTime datPayable)
		{
			short CalculateCheckCount = 0;
			clsDRWrapper rsCountInfo = new clsDRWrapper();
			int rsCheckTypeInfo;
			int intEntriesOnCheck;
			clsDRWrapper rsCustomCheck = new clsDRWrapper();
			CalculateCheckCount = 0;
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 4)
			{
				intEntriesOnCheck = 10;
			}
			else
			{
				rsCustomCheck.OpenRecordset("SELECT * FROM CustomChecks WHERE ID = " + FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse));
				if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
				{
					if (rsCustomCheck.Get_Fields_Boolean("UseDoubleStub") == false)
					{
						intEntriesOnCheck = 10;
					}
					else
					{
						intEntriesOnCheck = 20;
					}
				}
				else
				{
					intEntriesOnCheck = 10;
				}
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsCountInfo.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'V') AND Isnull(Seperate, 0) = 1");
			if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
			{
				CalculateCheckCount += FCConvert.ToInt16(rsCountInfo.RecordCount());
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsCountInfo.OpenRecordset("SELECT COUNT(VendorNumber) as TotalEntries FROM APJournal WHERE VendorNumber <> 0 AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'V') AND Isnull(Seperate, 0) = 0 GROUP BY VendorNumber");
			if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
					int vbPorterVar = FCUtils.iDiv(FCConvert.ToInt32(rsCountInfo.Get_Fields("TotalEntries")), intEntriesOnCheck);
					if (vbPorterVar <= 1)
					{
						CalculateCheckCount += 1;
					}
					else if (vbPorterVar <= 2)
					{
						CalculateCheckCount += 2;
					}
					else if (vbPorterVar <= 3)
					{
						CalculateCheckCount += 3;
					}
					else if (vbPorterVar <= 4)
					{
						CalculateCheckCount += 4;
					}
					else if (vbPorterVar <= 5)
					{
						CalculateCheckCount += 5;
					}
					else if (vbPorterVar <= 6)
					{
						CalculateCheckCount += 6;
					}
					else if (vbPorterVar <= 7)
					{
						CalculateCheckCount += 7;
					}
					else if (vbPorterVar <= 8)
					{
						CalculateCheckCount += 8;
					}
					rsCountInfo.MoveNext();
				}
				while (rsCountInfo.EndOfFile() != true);
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rsCountInfo.OpenRecordset("SELECT COUNT(TempVendorName) as TotalEntries FROM APJournal WHERE VendorNumber = 0 AND JournalNumber = " + rsJournals.Get_Fields("JournalNumber") + " AND (Status = 'E' OR Status = 'V') AND Isnull(Seperate, 0) = 0 GROUP BY TempVendorName");
			if (rsCountInfo.EndOfFile() != true && rsCountInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Field [TotalEntries] not found!! (maybe it is an alias?)
					int vbPorterVar1 = FCUtils.iDiv(FCConvert.ToInt32(rsCountInfo.Get_Fields("TotalEntries")), intEntriesOnCheck);
					if (vbPorterVar1 <= 1)
					{
						CalculateCheckCount += 1;
					}
					else if (vbPorterVar1 <= 2)
					{
						CalculateCheckCount += 2;
					}
					else if (vbPorterVar1 <= 3)
					{
						CalculateCheckCount += 3;
					}
					else if (vbPorterVar1 <= 4)
					{
						CalculateCheckCount += 4;
					}
					else if (vbPorterVar1 <= 5)
					{
						CalculateCheckCount += 5;
					}
					else if (vbPorterVar1 <= 6)
					{
						CalculateCheckCount += 6;
					}
					else if (vbPorterVar1 <= 7)
					{
						CalculateCheckCount += 7;
					}
					else if (vbPorterVar1 <= 8)
					{
						CalculateCheckCount += 8;
					}
					rsCountInfo.MoveNext();
				}
				while (rsCountInfo.EndOfFile() != true);
			}
			return CalculateCheckCount;
		}

		private void vsJournals_AfterRowColChange(object sender, EventArgs e)
		{
			FillDetailInfo();
		}

		private void FillDetailInfo()
		{
			if (cmbInitial.SelectedIndex == 0)
			{
				FillVendorsGrid();
			}
			else
			{
				if (vsJournals.Row > 0)
				{
					cJournal journ;
					journ = journService.GetJournalByNumber(FCConvert.ToInt32(Conversion.Val(vsJournals.TextMatrix(vsJournals.Row, JournalCol))));
					if (journ != null)
					{
						UpdateStartingCheckNumber(journ.BankNumber);
					}
				}
				FillChecksGrid();
			}
		}

		private void UpdateStartingCheckNumber(int lngBankID)
		{
			int lngCheckNumber = 0;
			if (lngBankID > 0)
			{
				lngCheckNumber = bankService.GetLastCheckNumberForBankForCorrectCheckType(lngBankID, "AP");
				if (lngCheckNumber > 0)
				{
					txtCheck.Text = FCConvert.ToString(lngCheckNumber + 1);
				}
			}
		}

        private void FillVendorsGrid()
        {
            clsDRWrapper rsCountInfo = new clsDRWrapper();
            clsDRWrapper rsVendors = new clsDRWrapper();
            int intCurrentVendor;
            string strCurrentVendorName = "";
            bool blnSavedSeperate = false;
            int intSavedAPID = 0;
            // vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
            Decimal curTotalAmount;
            bool blnPrePrinted = false;
            int intCurrentJournal = 0;

            vsVendors.Rows = 1;
            rsVendors.OpenRecordset("SELECT * FROM VendorMaster", "TWBD0000.vb1");
            intCurrentVendor = -1;
            intCurrentJournal = 0;
            if (vsJournals.Row > 0)
            {
                rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " +
                                          FCConvert.ToString(
                                              Conversion.Val(vsJournals.TextMatrix(vsJournals.Row, JournalCol))) +
                                          " AND IsNull(PrintedIndividual, 0) = 0 AND (Status = 'E' OR Status = 'V') ORDER BY VendorNumber, TempVendorName, Seperate");
                if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                {
                    curTotalAmount = 0;
                    intCurrentVendor = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("VendorNumber"));
                    strCurrentVendorName =
                        Strings.Trim(FCConvert.ToString(rsAPJournal.Get_Fields_String("TempVendorName")));
                    blnSavedSeperate = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("Seperate"));
                    intSavedAPID = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("ID"));
                    blnPrePrinted = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("PrepaidCheck")) &&
                                    !FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("PrintedIndividual"));
                    if (blnSavedSeperate || blnPrePrinted)
                    {
                        rsEntries.OpenRecordset(
                            "SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " +
                            rsAPJournal.Get_Fields_Int32("ID"));
                        if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                            curTotalAmount +=
                                FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields_Decimal("TotalAmount"))) -
                                FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields("TotalDiscount")));
                        }

                        rsAPJournal.MoveNext();
                    }

                    while (rsAPJournal.EndOfFile() != true)
                    {
                        bool executeCheckNext = false;
                        if (intCurrentVendor == FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("VendorNumber")) &&
                            strCurrentVendorName ==
                            Strings.Trim(FCConvert.ToString(rsAPJournal.Get_Fields_String("TempVendorName"))) &&
                            rsAPJournal.Get_Fields_Boolean("Seperate") != true)
                        {
                            executeCheckNext = true;
                            goto CheckNext;
                        }
                        else
                        {
                            vsVendors.Rows += 1;
                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNumberCol,
                                modValidateAccount.GetFormat_6(FCConvert.ToString(intCurrentVendor), 5));
                            if (intCurrentVendor != 0)
                            {
                                if (rsVendors.FindFirstRecord("VendorNumber", intCurrentVendor))
                                {
                                    vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol,
                                        FCConvert.ToString(rsVendors.Get_Fields_String("CheckName")));
                                }
                                else
                                {
                                    vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol, "UNKNOWN");
                                }
                            }
                            else
                            {
                                vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol, strCurrentVendorName);
                            }

                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAmountCol,
                                Strings.Format(curTotalAmount, "#,##0.00"));
                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorSeperateCol,
                                FCConvert.ToString(blnSavedSeperate));
                            if (blnSavedSeperate == true)
                            {
                                vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAPIDCol,
                                    FCConvert.ToString(intSavedAPID));
                            }
                            else
                            {
                                vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAPIDCol, FCConvert.ToString(-1));
                            }

                            if (blnPrePrinted)
                            {
                                vsVendors.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVendors.Rows - 1, 0,
                                    vsVendors.Rows - 1, vsVendors.Cols - 1, SystemColors.GrayText);
                            }

                            curTotalAmount = 0;
                            intCurrentVendor = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("VendorNumber"));
                            strCurrentVendorName =
                                Strings.Trim(FCConvert.ToString(rsAPJournal.Get_Fields_String("TempVendorName")));
                            blnSavedSeperate = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("Seperate"));
                            blnPrePrinted = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("PrepaidCheck")) &&
                                            !FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("PrintedIndividual"));
                            intSavedAPID = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("ID"));
                            executeCheckNext = true;
                            goto CheckNext;
                        }

                        CheckNext: ;
                        if (executeCheckNext)
                        {
                            rsEntries.OpenRecordset(
                                "SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " +
                                rsAPJournal.Get_Fields_Int32("ID"));
                            if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                            {
                                // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                curTotalAmount +=
                                    FCConvert.ToDecimal(
                                        FCConvert.ToString(rsEntries.Get_Fields_Decimal("TotalAmount"))) -
                                    FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields("TotalDiscount")));
                            }
                        }

                        rsAPJournal.MoveNext();
                    }

                    if (curTotalAmount != 0)
                    {
                        vsVendors.Rows += 1;
                        vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNumberCol,
                            modValidateAccount.GetFormat_6(FCConvert.ToString(intCurrentVendor), 5));
                        if (intCurrentVendor != 0)
                        {
                            if (rsVendors.FindFirstRecord("VendorNumber", intCurrentVendor))
                            {
                                vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol,
                                    FCConvert.ToString(rsVendors.Get_Fields_String("CheckName")));
                            }
                            else
                            {
                                vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol, "UNKNOWN");
                            }
                        }
                        else
                        {
                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorNameCol, strCurrentVendorName);
                        }

                        vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAmountCol,
                            Strings.Format(curTotalAmount, "#,##0.00"));
                        vsVendors.TextMatrix(vsVendors.Rows - 1, VendorSeperateCol,
                            FCConvert.ToString(blnSavedSeperate));
                        if (blnSavedSeperate == true)
                        {
                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAPIDCol, FCConvert.ToString(intSavedAPID));
                        }
                        else
                        {
                            vsVendors.TextMatrix(vsVendors.Rows - 1, VendorAPIDCol, FCConvert.ToString(-1));
                        }

                        if (blnPrePrinted)
                        {
                            vsVendors.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsVendors.Rows - 1, 0,
                                vsVendors.Rows - 1, vsVendors.Cols - 1, SystemColors.GrayText);
                        }
                    }
                }
            }
        }
    

		private void FillChecksGrid()
		{
			clsDRWrapper rsCountInfo = new clsDRWrapper();
			clsDRWrapper rsVendors = new clsDRWrapper();
			int intCurrentVendor;
			string strCurrentVendorName = "";
			bool blnSavedSeperate = false;
			int intSavedAPID = 0;
			// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
			Decimal curTotalAmount;
			string strCheck = "";
			rsVendors.OpenRecordset("SELECT * FROM VendorMaster", "TWBD0000.vb1");
			intCurrentVendor = -1;
			vsChecks.Rows = 1;
            if (vsJournals.Row > 0)
            {
                rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(vsJournals.Row, JournalCol))) + " AND (Status = 'E' OR Status = 'V') and PrintedIndividual = 1 ORDER BY CheckNumber");
                if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                {
                    curTotalAmount = 0;
                    intCurrentVendor = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("VendorNumber"));
                    strCurrentVendorName = Strings.Trim(FCConvert.ToString(rsAPJournal.Get_Fields_String("TempVendorName")));
                    blnSavedSeperate = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("Seperate"));
                    intSavedAPID = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("ID"));
                    // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    strCheck = FCConvert.ToString(rsAPJournal.Get_Fields("CheckNumber"));
                    while (rsAPJournal.EndOfFile() != true)
                    {
                        bool executeCheckNext = false;
                        // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                        if (strCheck == FCConvert.ToString(rsAPJournal.Get_Fields("CheckNumber")))
                        {
                            executeCheckNext = true;
                            goto CheckNext;
                        }
                        else
                        {
                            vsChecks.Rows += 1;
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNumberCol, modValidateAccount.GetFormat_6(FCConvert.ToString(intCurrentVendor), 5));
                            if (intCurrentVendor != 0)
                            {
                                if (rsVendors.FindFirstRecord("VendorNumber", intCurrentVendor))
                                {
                                    vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, FCConvert.ToString(rsVendors.Get_Fields_String("CheckName")));
                                }
                                else
                                {
                                    vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, "UNKNOWN");
                                }
                            }
                            else
                            {
                                vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, strCurrentVendorName);
                            }
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAmountCol, Strings.Format(curTotalAmount, "#,##0.00"));
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckNumberCol, strCheck);
                            if (blnSavedSeperate == true)
                            {
                                vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAPIDCol, FCConvert.ToString(intSavedAPID));
                            }
                            else
                            {
                                vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAPIDCol, FCConvert.ToString(-1));
                            }
                            curTotalAmount = 0;
                            intCurrentVendor = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("VendorNumber"));
                            strCurrentVendorName = Strings.Trim(FCConvert.ToString(rsAPJournal.Get_Fields_String("TempVendorName")));
                            blnSavedSeperate = FCConvert.ToBoolean(rsAPJournal.Get_Fields_Boolean("Seperate"));
                            intSavedAPID = FCConvert.ToInt32(rsAPJournal.Get_Fields_Int32("ID"));
                            // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            strCheck = FCConvert.ToString(rsAPJournal.Get_Fields("CheckNumber"));
                            executeCheckNext = true;
                            goto CheckNext;
                        }
                        CheckNext:
                        ;
                        if (executeCheckNext)
                        {
                            rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount, COUNT(Amount) as TotalEntries FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
                            if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                            {
                                // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                curTotalAmount += FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields_Decimal("TotalAmount"))) - FCConvert.ToDecimal(FCConvert.ToString(rsEntries.Get_Fields("TotalDiscount")));
                            }
                        }
                        rsAPJournal.MoveNext();
                    }
                    if (curTotalAmount != 0)
                    {
                        vsChecks.Rows += 1;
                        vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNumberCol, modValidateAccount.GetFormat_6(FCConvert.ToString(intCurrentVendor), 5));
                        if (intCurrentVendor != 0)
                        {
                            if (rsVendors.FindFirstRecord("VendorNumber", intCurrentVendor))
                            {
                                vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, FCConvert.ToString(rsVendors.Get_Fields_String("CheckName")));
                            }
                            else
                            {
                                vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, "UNKNOWN");
                            }
                        }
                        else
                        {
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckVendorNameCol, strCurrentVendorName);
                        }
                        vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAmountCol, Strings.Format(curTotalAmount, "#,##0.00"));
                        vsChecks.TextMatrix(vsChecks.Rows - 1, CheckNumberCol, strCheck);
                        if (blnSavedSeperate == true)
                        {
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAPIDCol, FCConvert.ToString(intSavedAPID));
                        }
                        else
                        {
                            vsChecks.TextMatrix(vsChecks.Rows - 1, CheckAPIDCol, FCConvert.ToString(-1));
                        }
                    }
                }
                
            }
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			btnProcessSave_Click(mnuProcessSave, EventArgs.Empty);
		}
	}
}
