//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreditMemoDataEntry.
	/// </summary>
	partial class frmCreditMemoDataEntry : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCFrame frmInfo;
		public fecherFoundation.FCButton cmdRetrieve;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCListBox lstRecords;
		public fecherFoundation.FCLabel lblRecordNumber;
		public fecherFoundation.FCLabel lblVendorName;
		public fecherFoundation.FCFrame frmSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdCancel;
		public Global.T2KOverTypeBox txtPeriod;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtMemoNumber;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCComboBox cboJournal;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblPO;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblDate1;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblRemAmount;
		public fecherFoundation.FCLabel lblRemaining;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteEntry;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreditMemoDataEntry));
			this.fraJournalSave = new fecherFoundation.FCFrame();
			this.cboSaveJournal = new fecherFoundation.FCComboBox();
			this.cmdOKSave = new fecherFoundation.FCButton();
			this.cmdCancelSave = new fecherFoundation.FCButton();
			this.txtJournalDescription = new fecherFoundation.FCTextBox();
			this.lblSaveInstructions = new fecherFoundation.FCLabel();
			this.lblJournalSave = new fecherFoundation.FCLabel();
			this.lblJournalDescription = new fecherFoundation.FCLabel();
			this.frmInfo = new fecherFoundation.FCFrame();
			this.cmdRetrieve = new fecherFoundation.FCButton();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.lstRecords = new fecherFoundation.FCListBox();
			this.lblRecordNumber = new fecherFoundation.FCLabel();
			this.lblVendorName = new fecherFoundation.FCLabel();
			this.frmSearch = new fecherFoundation.FCFrame();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.txtPeriod = new Global.T2KOverTypeBox();
			this.txtAddress_0 = new Global.T2KOverTypeBox();
			this.txtVendor = new Global.T2KOverTypeBox();
			this.txtDescription = new Global.T2KOverTypeBox();
			this.txtMemoNumber = new Global.T2KOverTypeBox();
			this.txtAmount = new Global.T2KBackFillDecimal();
			this.txtAddress_1 = new Global.T2KOverTypeBox();
			this.txtAddress_2 = new Global.T2KOverTypeBox();
			this.txtAddress_3 = new Global.T2KOverTypeBox();
			this.txtDate = new Global.T2KDateBox();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblPO = new fecherFoundation.FCLabel();
			this.lblAmount = new fecherFoundation.FCLabel();
			this.lblDate1 = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblRemAmount = new fecherFoundation.FCLabel();
			this.lblRemaining = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDeleteEntry = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcessNextEntry = new fecherFoundation.FCButton();
			this.btnProcessPreviousEntry = new fecherFoundation.FCButton();
			this.btnFileAddVendor = new fecherFoundation.FCButton();
			this.btnProcessSearch = new fecherFoundation.FCButton();
			this.btnFileSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
			this.fraJournalSave.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).BeginInit();
			this.frmInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).BeginInit();
			this.frmSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMemoNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileAddVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraJournalSave);
			this.ClientArea.Controls.Add(this.frmInfo);
			this.ClientArea.Controls.Add(this.frmSearch);
			this.ClientArea.Controls.Add(this.txtPeriod);
			this.ClientArea.Controls.Add(this.txtAddress_0);
			this.ClientArea.Controls.Add(this.txtVendor);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtMemoNumber);
			this.ClientArea.Controls.Add(this.txtAmount);
			this.ClientArea.Controls.Add(this.txtAddress_1);
			this.ClientArea.Controls.Add(this.txtAddress_2);
			this.ClientArea.Controls.Add(this.txtAddress_3);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.cboJournal);
			this.ClientArea.Controls.Add(this.txtZip4);
			this.ClientArea.Controls.Add(this.txtZip);
			this.ClientArea.Controls.Add(this.txtState);
			this.ClientArea.Controls.Add(this.txtCity);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblVendor);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblPO);
			this.ClientArea.Controls.Add(this.lblAmount);
			this.ClientArea.Controls.Add(this.lblDate1);
			this.ClientArea.Controls.Add(this.lblJournal);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblRemAmount);
			this.ClientArea.Controls.Add(this.lblRemaining);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessSearch);
			this.TopPanel.Controls.Add(this.btnFileAddVendor);
			this.TopPanel.Controls.Add(this.btnProcessPreviousEntry);
			this.TopPanel.Controls.Add(this.btnProcessNextEntry);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessNextEntry, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessPreviousEntry, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFileAddVendor, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(276, 30);
			this.HeaderText.Text = "Credit Memo Data Entry";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// fraJournalSave
			// 
			this.fraJournalSave.BackColor = System.Drawing.Color.White;
			this.fraJournalSave.Controls.Add(this.cboSaveJournal);
			this.fraJournalSave.Controls.Add(this.cmdOKSave);
			this.fraJournalSave.Controls.Add(this.cmdCancelSave);
			this.fraJournalSave.Controls.Add(this.txtJournalDescription);
			this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
			this.fraJournalSave.Controls.Add(this.lblJournalSave);
			this.fraJournalSave.Controls.Add(this.lblJournalDescription);
			this.fraJournalSave.Location = new System.Drawing.Point(10, 10);
			this.fraJournalSave.Name = "fraJournalSave";
			this.fraJournalSave.Size = new System.Drawing.Size(861, 620);
			this.fraJournalSave.TabIndex = 29;
			this.fraJournalSave.Text = "Save Journal";
			this.ToolTip1.SetToolTip(this.fraJournalSave, null);
			this.fraJournalSave.Visible = false;
			// 
			// cboSaveJournal
			// 
			this.cboSaveJournal.AutoSize = false;
			this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboSaveJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSaveJournal.FormattingEnabled = true;
			this.cboSaveJournal.Location = new System.Drawing.Point(165, 66);
			this.cboSaveJournal.Name = "cboSaveJournal";
			this.cboSaveJournal.Size = new System.Drawing.Size(408, 40);
			this.cboSaveJournal.TabIndex = 33;
			this.ToolTip1.SetToolTip(this.cboSaveJournal, null);
			this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
			this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
			// 
			// cmdOKSave
			// 
			this.cmdOKSave.AppearanceKey = "actionButton";
			this.cmdOKSave.Location = new System.Drawing.Point(20, 186);
			this.cmdOKSave.Name = "cmdOKSave";
			this.cmdOKSave.Size = new System.Drawing.Size(90, 40);
			this.cmdOKSave.TabIndex = 31;
			this.cmdOKSave.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdOKSave, null);
			this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
			// 
			// cmdCancelSave
			// 
			this.cmdCancelSave.AppearanceKey = "actionButton";
			this.cmdCancelSave.Location = new System.Drawing.Point(140, 186);
			this.cmdCancelSave.Name = "cmdCancelSave";
			this.cmdCancelSave.Size = new System.Drawing.Size(90, 40);
			this.cmdCancelSave.TabIndex = 32;
			this.cmdCancelSave.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancelSave, null);
			this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
			// 
			// txtJournalDescription
			// 
			this.txtJournalDescription.AutoSize = false;
			this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournalDescription.LinkItem = null;
			this.txtJournalDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtJournalDescription.LinkTopic = null;
			this.txtJournalDescription.Location = new System.Drawing.Point(165, 126);
			this.txtJournalDescription.MaxLength = 100;
			this.txtJournalDescription.Name = "txtJournalDescription";
			this.txtJournalDescription.Size = new System.Drawing.Size(408, 40);
			this.txtJournalDescription.TabIndex = 30;
			this.ToolTip1.SetToolTip(this.txtJournalDescription, null);
			// 
			// lblSaveInstructions
			// 
			this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
			this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblSaveInstructions.Name = "lblSaveInstructions";
			this.lblSaveInstructions.Size = new System.Drawing.Size(783, 16);
			this.lblSaveInstructions.TabIndex = 36;
			this.lblSaveInstructions.Text = "PLEASE SELECT THE JOUNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION FO" + "R THE JOURNAL, AND CLICK THE OK BUTTON";
			this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblSaveInstructions, null);
			// 
			// lblJournalSave
			// 
			this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
			this.lblJournalSave.Name = "lblJournalSave";
			this.lblJournalSave.Size = new System.Drawing.Size(69, 16);
			this.lblJournalSave.TabIndex = 35;
			this.lblJournalSave.Text = "JOURNAL";
			this.ToolTip1.SetToolTip(this.lblJournalSave, null);
			// 
			// lblJournalDescription
			// 
			this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalDescription.Location = new System.Drawing.Point(20, 140);
			this.lblJournalDescription.Name = "lblJournalDescription";
			this.lblJournalDescription.Size = new System.Drawing.Size(86, 21);
			this.lblJournalDescription.TabIndex = 34;
			this.lblJournalDescription.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.lblJournalDescription, null);
			// 
			// frmInfo
			// 
			this.frmInfo.BackColor = System.Drawing.Color.White;
			this.frmInfo.Controls.Add(this.cmdRetrieve);
			this.frmInfo.Controls.Add(this.cmdReturn);
			this.frmInfo.Controls.Add(this.lstRecords);
			this.frmInfo.Controls.Add(this.lblRecordNumber);
			this.frmInfo.Controls.Add(this.lblVendorName);
			this.frmInfo.Location = new System.Drawing.Point(1000, 30);
			this.frmInfo.Name = "frmInfo";
			this.frmInfo.Size = new System.Drawing.Size(861, 405);
			this.frmInfo.TabIndex = 38;
			this.frmInfo.Text = "Multiple Records";
			this.ToolTip1.SetToolTip(this.frmInfo, null);
			this.frmInfo.Visible = false;
			// 
			// cmdRetrieve
			// 
			this.cmdRetrieve.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdRetrieve.AppearanceKey = "actionButton";
			this.cmdRetrieve.Location = new System.Drawing.Point(20, 340);
			this.cmdRetrieve.Name = "cmdRetrieve";
			this.cmdRetrieve.Size = new System.Drawing.Size(145, 40);
			this.cmdRetrieve.TabIndex = 41;
			this.cmdRetrieve.Text = "Retrieve Record";
			this.ToolTip1.SetToolTip(this.cmdRetrieve, null);
			this.cmdRetrieve.Click += new System.EventHandler(this.cmdRetrieve_Click);
			// 
			// cmdReturn
			// 
			this.cmdReturn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(195, 340);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(90, 40);
			this.cmdReturn.TabIndex = 40;
			this.cmdReturn.Text = "Cancel ";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// lstRecords
			// 
			this.lstRecords.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.lstRecords.Appearance = 0;
			this.lstRecords.BackColor = System.Drawing.SystemColors.Window;
			this.lstRecords.Location = new System.Drawing.Point(20, 66);
			this.lstRecords.MultiSelect = 0;
			this.lstRecords.Name = "lstRecords";
			this.lstRecords.Size = new System.Drawing.Size(821, 255);
			this.lstRecords.Sorted = false;
			this.lstRecords.TabIndex = 39;
			this.ToolTip1.SetToolTip(this.lstRecords, null);
			this.lstRecords.DoubleClick += new System.EventHandler(this.lstRecords_DoubleClick);
			// 
			// lblRecordNumber
			// 
			this.lblRecordNumber.Location = new System.Drawing.Point(20, 30);
			this.lblRecordNumber.Name = "lblRecordNumber";
			this.lblRecordNumber.Size = new System.Drawing.Size(69, 14);
			this.lblRecordNumber.TabIndex = 43;
			this.lblRecordNumber.Text = "VENDOR #";
			this.ToolTip1.SetToolTip(this.lblRecordNumber, null);
			// 
			// lblVendorName
			// 
			this.lblVendorName.Location = new System.Drawing.Point(146, 30);
			this.lblVendorName.Name = "lblVendorName";
			this.lblVendorName.Size = new System.Drawing.Size(41, 14);
			this.lblVendorName.TabIndex = 42;
			this.lblVendorName.Text = "NAME";
			this.ToolTip1.SetToolTip(this.lblVendorName, null);
			// 
			// frmSearch
			// 
			this.frmSearch.BackColor = System.Drawing.Color.White;
			this.frmSearch.Controls.Add(this.txtSearch);
			this.frmSearch.Controls.Add(this.cmdSearch);
			this.frmSearch.Controls.Add(this.cmdCancel);
			this.frmSearch.Location = new System.Drawing.Point(1000, 30);
			this.frmSearch.Name = "frmSearch";
			this.frmSearch.Size = new System.Drawing.Size(340, 150);
			this.frmSearch.TabIndex = 25;
			this.frmSearch.Text = "Vendor Search";
			this.ToolTip1.SetToolTip(this.frmSearch, null);
			this.frmSearch.Visible = false;
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.LinkItem = null;
			this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearch.LinkTopic = null;
			this.txtSearch.Location = new System.Drawing.Point(20, 30);
			this.txtSearch.MaxLength = 35;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(300, 40);
			this.txtSearch.TabIndex = 28;
			this.ToolTip1.SetToolTip(this.txtSearch, null);
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.Location = new System.Drawing.Point(20, 90);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(90, 40);
			this.cmdSearch.TabIndex = 27;
			this.cmdSearch.Text = "Search";
			this.ToolTip1.SetToolTip(this.cmdSearch, null);
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(140, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(90, 40);
			this.cmdCancel.TabIndex = 26;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// txtPeriod
			// 
			this.txtPeriod.AutoSize = false;
			this.txtPeriod.LinkItem = null;
			this.txtPeriod.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPeriod.LinkTopic = null;
			this.txtPeriod.Location = new System.Drawing.Point(140, 280);
			this.txtPeriod.MaxLength = 2;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Size = new System.Drawing.Size(113, 40);
			this.txtPeriod.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.txtPeriod, null);
			this.txtPeriod.TextChanged += new System.EventHandler(this.txtPeriod_Change);
			// 
			// txtAddress_0
			// 
			this.txtAddress_0.AutoSize = false;
			this.txtAddress_0.LinkItem = null;
			this.txtAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_0.LinkTopic = null;
			this.txtAddress_0.Location = new System.Drawing.Point(612, 29);
			this.txtAddress_0.MaxLength = 35;
			this.txtAddress_0.Name = "txtAddress_0";
			this.txtAddress_0.Size = new System.Drawing.Size(359, 40);
			this.txtAddress_0.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.txtAddress_0, null);
			this.txtAddress_0.Enter += new System.EventHandler(this.txtAddress_Enter);
			this.txtAddress_0.TextChanged += new System.EventHandler(this.txtAddress_Change);
			this.txtAddress_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
			// 
			// txtVendor
			// 
			this.txtVendor.AutoSize = false;
			this.txtVendor.LinkItem = null;
			this.txtVendor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVendor.LinkTopic = null;
			this.txtVendor.Location = new System.Drawing.Point(529, 29);
			this.txtVendor.MaxLength = 5;
			this.txtVendor.Name = "txtVendor";
			this.txtVendor.Size = new System.Drawing.Size(77, 40);
			this.txtVendor.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtVendor, "Enter S for Vendor Search or A for Add Vendor");
			this.txtVendor.KeyDown += new Wisej.Web.KeyEventHandler(this.txtVendor_KeyDownEvent);
			this.txtVendor.Enter += new System.EventHandler(this.txtVendor_Enter);
			//this.txtVendor.Leave += new System.EventHandler(this.txtVendor_Leave);
			this.txtVendor.TextChanged += new System.EventHandler(this.txtVendor_Change);
			this.txtVendor.Validating += new System.ComponentModel.CancelEventHandler(this.txtVendor_Validate);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(140, 130);
			this.txtDescription.MaxLength = 25;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(269, 40);
			this.txtDescription.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			this.txtDescription.Enter += new System.EventHandler(this.txtDescription_Enter);
			this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_Change);
			// 
			// txtMemoNumber
			// 
			this.txtMemoNumber.AutoSize = false;
			this.txtMemoNumber.LinkItem = null;
			this.txtMemoNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMemoNumber.LinkTopic = null;
			this.txtMemoNumber.Location = new System.Drawing.Point(140, 180);
			this.txtMemoNumber.MaxLength = 15;
			this.txtMemoNumber.Name = "txtMemoNumber";
			this.txtMemoNumber.Size = new System.Drawing.Size(222, 40);
			this.txtMemoNumber.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.txtMemoNumber, null);
			this.txtMemoNumber.Enter += new System.EventHandler(this.txtMemoNumber_Enter);
			this.txtMemoNumber.TextChanged += new System.EventHandler(this.txtMemoNumber_Change);
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(140, 230);
			this.txtAmount.MaxLength = 14;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(166, 40);
			this.txtAmount.TabIndex = 15;
			this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtAmount, null);
			this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
			this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_Change);
			this.txtAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAmount_Validate);
			// 
			// txtAddress_1
			// 
			this.txtAddress_1.AutoSize = false;
			this.txtAddress_1.LinkItem = null;
			this.txtAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_1.LinkTopic = null;
			this.txtAddress_1.Location = new System.Drawing.Point(612, 80);
			this.txtAddress_1.MaxLength = 35;
			this.txtAddress_1.Name = "txtAddress_1";
			this.txtAddress_1.Size = new System.Drawing.Size(359, 40);
			this.txtAddress_1.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtAddress_1, null);
			this.txtAddress_1.Enter += new System.EventHandler(this.txtAddress_Enter);
			this.txtAddress_1.TextChanged += new System.EventHandler(this.txtAddress_Change);
			this.txtAddress_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
			// 
			// txtAddress_2
			// 
			this.txtAddress_2.AutoSize = false;
			this.txtAddress_2.LinkItem = null;
			this.txtAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_2.LinkTopic = null;
			this.txtAddress_2.Location = new System.Drawing.Point(612, 130);
			this.txtAddress_2.MaxLength = 35;
			this.txtAddress_2.Name = "txtAddress_2";
			this.txtAddress_2.Size = new System.Drawing.Size(359, 40);
			this.txtAddress_2.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtAddress_2, null);
			this.txtAddress_2.Enter += new System.EventHandler(this.txtAddress_Enter);
			this.txtAddress_2.TextChanged += new System.EventHandler(this.txtAddress_Change);
			this.txtAddress_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
			// 
			// txtAddress_3
			// 
			this.txtAddress_3.AutoSize = false;
			this.txtAddress_3.LinkItem = null;
			this.txtAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress_3.LinkTopic = null;
			this.txtAddress_3.Location = new System.Drawing.Point(612, 180);
			this.txtAddress_3.MaxLength = 35;
			this.txtAddress_3.Name = "txtAddress_3";
			this.txtAddress_3.Size = new System.Drawing.Size(359, 40);
			this.txtAddress_3.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtAddress_3, null);
			this.txtAddress_3.Enter += new System.EventHandler(this.txtAddress_Enter);
			this.txtAddress_3.TextChanged += new System.EventHandler(this.txtAddress_Change);
			this.txtAddress_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(140, 80);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.MaxLength = 10;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 3;
			this.txtDate.Text = "  .  .";
			this.ToolTip1.SetToolTip(this.txtDate, null);
			this.txtDate.Enter += new System.EventHandler(this.txtDate_Enter);
			this.txtDate.TextChanged += new System.EventHandler(this.txtDate_Change);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// cboJournal
			// 
			this.cboJournal.AutoSize = false;
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournal.FormattingEnabled = true;
			this.cboJournal.Location = new System.Drawing.Point(140, 29);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(269, 40);
			this.cboJournal.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.cboJournal, null);
			this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
			this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
			this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
			this.cboJournal.TextChanged += new System.EventHandler(this.cboJournal_TextChanged);
			// 
			// txtZip4
			// 
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
			this.txtZip4.Location = new System.Drawing.Point(897, 230);
			this.txtZip4.MaxLength = 4;
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(74, 40);
			this.txtZip4.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.txtZip4, null);
			this.txtZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip4_KeyPress);
			this.txtZip4.Enter += new System.EventHandler(this.txtZip4_Enter);
			this.txtZip4.TextChanged += new System.EventHandler(this.txtZip4_TextChanged);
			// 
			// txtZip
			// 
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(820, 230);
			this.txtZip.MaxLength = 5;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(71, 40);
			this.txtZip.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtZip, null);
			this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
			this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
			this.txtZip.TextChanged += new System.EventHandler(this.txtZip_TextChanged);
			// 
			// txtState
			// 
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(750, 230);
			this.txtState.MaxLength = 2;
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(64, 40);
			this.txtState.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtState, null);
			this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
			this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(612, 230);
			this.txtCity.MaxLength = 35;
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(132, 40);
			this.txtCity.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtCity, null);
			this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
			this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 405);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 16;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(678, 219);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vs1, null);
			this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(30, 294);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(51, 16);
			this.lblPeriod.TabIndex = 37;
			this.lblPeriod.Text = "PERIOD";
			this.ToolTip1.SetToolTip(this.lblPeriod, null);
			// 
			// lblVendor
			// 
			this.lblVendor.Location = new System.Drawing.Point(445, 44);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(64, 16);
			this.lblVendor.TabIndex = 24;
			this.lblVendor.Text = "VENDOR";
			this.ToolTip1.SetToolTip(this.lblVendor, null);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 144);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(91, 16);
			this.lblDescription.TabIndex = 23;
			this.lblDescription.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// lblPO
			// 
			this.lblPO.Location = new System.Drawing.Point(30, 194);
			this.lblPO.Name = "lblPO";
			this.lblPO.Size = new System.Drawing.Size(50, 16);
			this.lblPO.TabIndex = 22;
			this.lblPO.Text = "MEMO #";
			this.ToolTip1.SetToolTip(this.lblPO, null);
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(30, 244);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(59, 16);
			this.lblAmount.TabIndex = 21;
			this.lblAmount.Text = "AMOUNT";
			this.ToolTip1.SetToolTip(this.lblAmount, null);
			// 
			// lblDate1
			// 
			this.lblDate1.Location = new System.Drawing.Point(30, 94);
			this.lblDate1.Name = "lblDate1";
			this.lblDate1.Size = new System.Drawing.Size(31, 16);
			this.lblDate1.TabIndex = 20;
			this.lblDate1.Text = "DATE";
			this.ToolTip1.SetToolTip(this.lblDate1, null);
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(30, 44);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(61, 16);
			this.lblJournal.TabIndex = 19;
			this.lblJournal.Text = "JOURNAL";
			this.ToolTip1.SetToolTip(this.lblJournal, null);
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 376);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(485, 16);
			this.lblExpense.TabIndex = 18;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblExpense, null);
			// 
			// lblRemAmount
			// 
			this.lblRemAmount.Location = new System.Drawing.Point(165, 340);
			this.lblRemAmount.Name = "lblRemAmount";
			this.lblRemAmount.Size = new System.Drawing.Size(110, 16);
			this.lblRemAmount.TabIndex = 17;
			this.lblRemAmount.Text = "0.00";
			this.lblRemAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblRemAmount, null);
			// 
			// lblRemaining
			// 
			this.lblRemaining.Location = new System.Drawing.Point(30, 340);
			this.lblRemaining.Name = "lblRemaining";
			this.lblRemaining.Size = new System.Drawing.Size(129, 16);
			this.lblRemaining.TabIndex = 0;
			this.lblRemaining.Text = "REMAINING AMOUNT";
			this.ToolTip1.SetToolTip(this.lblRemaining, null);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessDelete,
				this.mnuProcessDeleteEntry
			});
			this.MainMenu1.Name = null;
			// 
			// mnuProcessDelete
			// 
			this.mnuProcessDelete.Enabled = false;
			this.mnuProcessDelete.Index = 0;
			this.mnuProcessDelete.Name = "mnuProcessDelete";
			this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuProcessDelete.Text = "Delete Detail Item";
			this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// mnuProcessDeleteEntry
			// 
			this.mnuProcessDeleteEntry.Enabled = false;
			this.mnuProcessDeleteEntry.Index = 1;
			this.mnuProcessDeleteEntry.Name = "mnuProcessDeleteEntry";
			this.mnuProcessDeleteEntry.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuProcessDeleteEntry.Text = "Delete Journal Entry";
			this.mnuProcessDeleteEntry.Click += new System.EventHandler(this.mnuProcessDeleteEntry_Click);
			// 
			// btnProcessNextEntry
			// 
			this.btnProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessNextEntry.AppearanceKey = "toolbarButton";
			this.btnProcessNextEntry.Location = new System.Drawing.Point(919, 29);
			this.btnProcessNextEntry.Name = "btnProcessNextEntry";
			this.btnProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
			this.btnProcessNextEntry.Size = new System.Drawing.Size(129, 24);
			this.btnProcessNextEntry.TabIndex = 1;
			this.btnProcessNextEntry.Text = "Next Journal Entry";
			this.ToolTip1.SetToolTip(this.btnProcessNextEntry, null);
			this.btnProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
			// 
			// btnProcessPreviousEntry
			// 
			this.btnProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessPreviousEntry.AppearanceKey = "toolbarButton";
			this.btnProcessPreviousEntry.Location = new System.Drawing.Point(765, 29);
			this.btnProcessPreviousEntry.Name = "btnProcessPreviousEntry";
			this.btnProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
			this.btnProcessPreviousEntry.Size = new System.Drawing.Size(151, 24);
			this.btnProcessPreviousEntry.TabIndex = 2;
			this.btnProcessPreviousEntry.Text = "Previous Journal Entry";
			this.ToolTip1.SetToolTip(this.btnProcessPreviousEntry, null);
			this.btnProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
			// 
			// btnFileAddVendor
			// 
			this.btnFileAddVendor.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileAddVendor.AppearanceKey = "toolbarButton";
			this.btnFileAddVendor.Location = new System.Drawing.Point(670, 29);
			this.btnFileAddVendor.Name = "btnFileAddVendor";
			this.btnFileAddVendor.Size = new System.Drawing.Size(91, 24);
			this.btnFileAddVendor.TabIndex = 3;
			this.btnFileAddVendor.Text = "Add Vendor";
			this.ToolTip1.SetToolTip(this.btnFileAddVendor, null);
			//this.btnFileAddVendor.MouseEnter += new System.EventHandler(this.btnFileAddVendor_MouseEnter);
			//this.btnFileAddVendor.MouseLeave += new System.EventHandler(this.btnFileAddVendor_MouseLeave);
			this.btnFileAddVendor.Click += new System.EventHandler(this.mnuFileAddVendor_Click);
			// 
			// btnProcessSearch
			// 
			this.btnProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessSearch.AppearanceKey = "toolbarButton";
			this.btnProcessSearch.Location = new System.Drawing.Point(559, 29);
			this.btnProcessSearch.Name = "btnProcessSearch";
			this.btnProcessSearch.Shortcut = Wisej.Web.Shortcut.F6;
			this.btnProcessSearch.Size = new System.Drawing.Size(108, 25);
			this.btnProcessSearch.TabIndex = 4;
			this.btnProcessSearch.Text = "Vendor Search";
			this.ToolTip1.SetToolTip(this.btnProcessSearch, null);
			//this.btnProcessSearch.MouseEnter += new System.EventHandler(this.btnProcessSearch_MouseEnter);
			//this.btnProcessSearch.MouseLeave += new System.EventHandler(this.btnProcessSearch_MouseLeave);
			this.btnProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
			// 
			// btnFileSave
			// 
			this.btnFileSave.AppearanceKey = "acceptButton";
			this.btnFileSave.Location = new System.Drawing.Point(496, 30);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileSave.Size = new System.Drawing.Size(80, 48);
			this.btnFileSave.TabIndex = 1;
			this.btnFileSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnFileSave, null);
			this.btnFileSave.Click += new System.EventHandler(this.cmdProcessSave_Click);
			// 
			// frmCreditMemoDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCreditMemoDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Credit Memo Data Entry";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmCreditMemoDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmCreditMemoDataEntry_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.FormClosed += FrmCreditMemoDataEntry_FormClosed;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreditMemoDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreditMemoDataEntry_KeyPress);
			this.Resize += new System.EventHandler(this.frmCreditMemoDataEntry_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
			this.fraJournalSave.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).EndInit();
			this.frmInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).EndInit();
			this.frmSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMemoNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileAddVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		internal FCButton btnProcessNextEntry;
		internal FCButton btnProcessPreviousEntry;
		private FCButton btnFileAddVendor;
		private FCButton btnProcessSearch;
		private FCButton btnFileSave;
	}
}