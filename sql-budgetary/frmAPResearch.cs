﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAPResearch.
	/// </summary>
	public partial class frmAPResearch : BaseForm
	{
		public frmAPResearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAPResearch InstancePtr
		{
			get
			{
				return (frmAPResearch)Sys.GetInstance(typeof(frmAPResearch));
			}
		}

		protected frmAPResearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int WarrantCol;
		int JournalCol;
		int VendorNumberCol;
		int VendorNameCol;
		int CheckNumberCol;
		int CheckDateCol;
		int ReferenceCol;
		int JournalDescriptionCol;
		int AmountCol;
		int AccountCol;
		int LineDescriptionCol;
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void frmAPResearch_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmAPResearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAPResearch.FillStyle	= 0;
			//frmAPResearch.ScaleWidth	= 9045;
			//frmAPResearch.ScaleHeight	= 7515;
			//frmAPResearch.LinkTopic	= "Form2";
			//frmAPResearch.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int counter = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			WarrantCol = 0;
			JournalCol = 1;
			VendorNumberCol = 2;
			VendorNameCol = 3;
			CheckNumberCol = 4;
			CheckDateCol = 5;
			ReferenceCol = 6;
			JournalDescriptionCol = 7;
			LineDescriptionCol = 8;
			AccountCol = 9;
			AmountCol = 10;
			vsInfo.TextMatrix(vsInfo.Rows - 1, WarrantCol, "Wrnt #");
			vsInfo.TextMatrix(vsInfo.Rows - 1, JournalCol, "Jrnl #");
			vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNumberCol, "Vndr #");
			vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNameCol, "Vendor Name");
			vsInfo.TextMatrix(vsInfo.Rows - 1, CheckNumberCol, "Check #");
			vsInfo.TextMatrix(vsInfo.Rows - 1, CheckDateCol, "Check Date");
			vsInfo.TextMatrix(vsInfo.Rows - 1, ReferenceCol, "Reference");
			vsInfo.TextMatrix(vsInfo.Rows - 1, JournalDescriptionCol, "Entry Desc");
			vsInfo.TextMatrix(vsInfo.Rows - 1, LineDescriptionCol, "Line Desc");
			vsInfo.TextMatrix(vsInfo.Rows - 1, AccountCol, "Acct #");
			vsInfo.TextMatrix(vsInfo.Rows - 1, AmountCol, "Amount");
			vsInfo.ColWidth(WarrantCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(JournalCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(VendorNumberCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.08));
			vsInfo.ColWidth(CheckNumberCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.1));
			vsInfo.ColWidth(VendorNameCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.25));
			vsInfo.ColWidth(CheckDateCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.15));
			vsInfo.ColWidth(ReferenceCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.15));
			vsInfo.ColWidth(JournalDescriptionCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.2));
			vsInfo.ColWidth(LineDescriptionCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.2));
			vsInfo.ColWidth(AccountCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.27));
			vsInfo.ColWidth(AmountCol, FCConvert.ToInt32(vsInfo.WidthOriginal * 0.12));
			vsInfo.ColAlignment(CheckNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(JournalCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(WarrantCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(VendorNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(CheckDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(JournalDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsInfo.ColAlignment(LineDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			if (frmSetupAPResearch.InstancePtr.cmbLineItem.SelectedIndex == 1)
			{
				rsInfo.OpenRecordset("SELECT APJournal.JournalNumber as JournalNumber, APJournal.Warrant as Warrant, APJournal.CheckNumber as CheckNumber, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournal.Description as JournalDescription, APJournal.Reference as Reference, SUM(APJournalDetail.Amount) as TotalAmount FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' GROUP BY APJournal.JournalNumber, APJournal.Warrant, APJournal.CheckNumber, APJournal.CheckDate, APJournal.VendorNumber, APJournal.TempVendorName, APJournal.Description, APJournal.Reference ORDER BY JournalNumber");
				vsInfo.ColHidden(AccountCol, true);
				vsInfo.ColHidden(LineDescriptionCol, true);
			}
			else
			{
				rsInfo.OpenRecordset("SELECT APJournal.JournalNumber as JournalNumber, APJournal.Warrant as Warrant, APJournal.CheckNumber as CheckNumber, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournal.Description as JournalDescription, APJournal.Reference as Reference, APJournalDetail.Account as Account, APJournalDetail.Description as LineDescription, APJournalDetail.Amount as TotalAmount FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' ORDER BY JournalNumber");
				vsInfo.ColHidden(AccountCol, false);
				vsInfo.ColHidden(LineDescriptionCol, false);
			}
			SetCustomFormColors();
			if (rsInfo.EndOfFile() != true)
			{
				counter = 1;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Retrieving Information", true, rsInfo.RecordCount());
				this.Refresh();
				do
				{
					//Application.DoEvents();
					frmWait.InstancePtr.prgProgress.Value = counter;
					vsInfo.Rows += 1;
					// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
					vsInfo.TextMatrix(vsInfo.Rows - 1, WarrantCol, Strings.Format(rsInfo.Get_Fields("Warrant"), "0000"));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vsInfo.TextMatrix(vsInfo.Rows - 1, JournalCol, Strings.Format(rsInfo.Get_Fields("JournalNumber"), "0000"));
					vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNumberCol, Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000"));
					if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNameCol, FCConvert.ToString(rsInfo.Get_Fields_String("TempVendorName")));
					}
					else
					{
						rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"));
						if (rsVendorInfo.EndOfFile() != true)
						{
							vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNameCol, FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
						}
						else
						{
							vsInfo.TextMatrix(vsInfo.Rows - 1, VendorNameCol, "UNKNOWN");
						}
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					vsInfo.TextMatrix(vsInfo.Rows - 1, CheckNumberCol, FCConvert.ToString(rsInfo.Get_Fields("CheckNumber")));
					vsInfo.TextMatrix(vsInfo.Rows - 1, CheckDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yyyy"));
					vsInfo.TextMatrix(vsInfo.Rows - 1, ReferenceCol, FCConvert.ToString(rsInfo.Get_Fields_String("Reference")));
					// TODO Get_Fields: Field [JournalDescription] not found!! (maybe it is an alias?)
					vsInfo.TextMatrix(vsInfo.Rows - 1, JournalDescriptionCol, FCConvert.ToString(rsInfo.Get_Fields("JournalDescription")));
					if (frmSetupAPResearch.InstancePtr.cmbLineItem.SelectedIndex == 0)
					{
						// TODO Get_Fields: Field [LineDescription] not found!! (maybe it is an alias?)
						vsInfo.TextMatrix(vsInfo.Rows - 1, LineDescriptionCol, FCConvert.ToString(rsInfo.Get_Fields("LineDescription")));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						vsInfo.TextMatrix(vsInfo.Rows - 1, AccountCol, FCConvert.ToString(rsInfo.Get_Fields("Account")));
					}
					vsInfo.TextMatrix(vsInfo.Rows - 1, AmountCol, Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00"));
					rsInfo.MoveNext();
					counter += 1;
				}
				while (rsInfo.EndOfFile() != true);
				rsInfo.OpenRecordset("SELECT CheckNumber FROM APJournal WHERE Status = 'P' AND Description <> 'Control Entries' GROUP BY CheckNumber");
				lblTotalAPChecks.Text = "Total AP Checks: " + Strings.Format(rsInfo.RecordCount(), "#,##0");
				rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND Description <> 'Control Entries'");
				lblTotalAPInvoices.Text = "Total AP Invoices: " + Strings.Format(rsInfo.RecordCount(), "#,##0");
				frmWait.InstancePtr.Unload();
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No information found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void frmAPResearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptAPResearch.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			rptAPResearch.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SetCustomFormColors()
		{
			//FC:FINAL:BSE:#4062 remove label color 
            //lblTotalAPChecks.ForeColor = Color.Blue;
			//lblTotalAPInvoices.ForeColor = Color.Blue;
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(sender, e);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuFilePrint_Click(sender, e);
		}
	}
}
