﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineExpQuery.
	/// </summary>
	public partial class frmOutlineExpQuery : BaseForm
	{
		public frmOutlineExpQuery()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOutlineExpQuery InstancePtr
		{
			get
			{
				return (frmOutlineExpQuery)Sys.GetInstance(typeof(frmOutlineExpQuery));
			}
		}

		protected frmOutlineExpQuery _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		[DllImport("user32")]
		private static extern int GetCursorPos(ref POINTAPI lpPoint);

		[StructLayout(LayoutKind.Sequential)]
		private struct POINTAPI
		{
			public int x;
			public int y;
		};

		string length;
		// holds format account lengths
		int NumberOfExpenses;
		string[] DeletedExpenses = new string[20 + 1];
		bool CollapsedFlag;
		int ObjLength;
		// holds division length
		int ExpLength;
		// holds department length
		int NumberCol;
		// column in the flex grid that has the database ID
		int ExpCol;
		int TaxCol;
		int ObjCol;
		// column in the grid that holds the division number
		int ShortCol;
		// column in the grid that holds the short description
		int LongCol;
		// column in the grid that holds the long description
		int BreakdownCol;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		float ExpPercent;
		float ObjPercent;
		bool EditFlag;
		bool blnUnload;
		bool blnDirty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = false;
			vs1.Enabled = true;
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			string temp = "";
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDelete.Text == "")
			{
				MessageBox.Show("You must enter the Number of the Expense you wish to delete", "Invalid Expense Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			answer = MessageBox.Show("Are you sure you wish to delete expense " + txtDelete.Text + "?", "Delete Expense?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.Yes)
			{
				if (CheckExpUsed_2(Strings.Trim(txtDelete.Text)) == false)
				{
					vs1.Enabled = true;
					counter = 1;
					temp = Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength);
					while (temp != txtDelete.Text && counter < vs1.Rows)
					{
						temp = Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength);
						counter += 1;
					}
					if (temp == txtDelete.Text)
					{
						counter -= 1;
					}
					if (counter == vs1.Rows)
					{
						MessageBox.Show("No expense found with the expense number of " + txtDelete.Text + ".", "No Matches Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						vs1.RemoveItem(counter);
						if (vs1.Rows == 1)
						{
							counter2 = 0;
							while (DeletedExpenses[counter2] != "")
							{
								counter2 += 1;
								if (counter2 == 20)
								{
									return;
								}
							}
							DeletedExpenses[counter2] = txtDelete.Text;
							txtDelete.Text = "";
							Frame1.Visible = false;
							cmdExpenseDelete.Enabled = false;
							vs1_AfterCollapse(1, 2);
							return;
						}
						else
						{
							if (counter == vs1.Rows)
							{
								counter2 = 0;
								while (DeletedExpenses[counter2] != "")
								{
									counter2 += 1;
									if (counter2 == 20)
									{
										return;
									}
								}
								DeletedExpenses[counter2] = txtDelete.Text;
								txtDelete.Text = "";
								Frame1.Visible = false;
								vs1_AfterCollapse(1, 2);
								return;
							}
							while (vs1.RowOutlineLevel(counter) > 1)
							{
								vs1.RemoveItem(counter);
								if (vs1.Rows == 1)
								{
									cmdExpenseDelete.Enabled = false;
									break;
								}
								else if (counter == vs1.Rows)
								{
									break;
								}
							}
						}
						counter2 = 0;
						while (DeletedExpenses[counter2] != "")
						{
							counter2 += 1;
							if (counter2 == 20)
							{
								return;
							}
						}
						DeletedExpenses[counter2] = txtDelete.Text;
						txtDelete.Text = "";
						Frame1.Visible = false;
						vs1_AfterCollapse(1, 2);
					}
				}
				else
				{
					MessageBox.Show("You may not delete this Expense because it it has a budget associated with it or is used in one or more journal entries.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				txtDelete.Focus();
				txtDelete.SelectionStart = 0;
				txtDelete.SelectionLength = 1;
			}
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void frmOutlineExpQuery_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1.Focus();
			vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + vs1.ColWidth(BreakdownCol) + 800));
			//FC:FINAL:DDU:#2951 - aligned columns
			if (vs1.Rows > 1)
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vs1.Rows - 1, 0, Color.White);
				//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, TaxCol, vs1.Rows - 1, TaxCol, 4);
			}
			vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ExpCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ObjCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			this.Refresh();
			// refresh the form
		}

		private void frmOutlineExpQuery_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Tab)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{ENTER}", false);
			}
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmOutlineExpQuery_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOutlineExpQuery.FillStyle	= 0;
			//frmOutlineExpQuery.ScaleWidth	= 9045;
			//frmOutlineExpQuery.ScaleHeight	= 7380;
			//frmOutlineExpQuery.LinkTopic	= "Form2";
			//frmOutlineExpQuery.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int temp;
			int temp2;
			int temp3;
			string tempObject;
			string strList = "";
			length = modAccountTitle.Statics.Exp;
			NumberCol = 1;
			ExpCol = 2;
			ObjCol = 3;
			// initialize the columns
			ShortCol = 4;
			LongCol = 5;
			BreakdownCol = 6;
			TaxCol = 7;
			FCUtils.EraseSafe(DeletedExpenses);
			ExpLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 5, 2)))));
			// initialize department length
			vs1.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
			// allow the user to resize the columns
			ObjLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 7, 2)))));
			// initialize division lngth
			vs1.ColWidth(0, 300);
			vs1.ColWidth(NumberCol, 0);
			if (ExpLength < 4)
			{
				vs1.ColWidth(ExpCol, 600);
				ExpPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(ExpCol)) / vs1.WidthOriginal);
			}
			else
			{
				vs1.ColWidth(ExpCol, ExpLength * 140 + 150);
				ExpPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(ExpCol)) / vs1.WidthOriginal);
				txtDelete.WidthOriginal = ExpLength * 140 + 50;
				//FC:FINAL:MSH - issue #886: calculating value as in original must be added to LeftOriginal
				//txtDelete.Left = (Frame1.WidthOriginal / 2) - (txtDelete.WidthOriginal / 2);
				txtDelete.LeftOriginal = (Frame1.WidthOriginal / 2) - (txtDelete.WidthOriginal / 2);
			}
			if (ObjLength == 0)
			{
				vs1.ColWidth(ObjCol, 0);
			}
			else
			{
				if (ObjLength < 4)
				{
					vs1.ColWidth(ObjCol, 500);
					// set column widths
					ObjPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(ObjCol)) / vs1.WidthOriginal);
				}
				else
				{
					vs1.ColWidth(ObjCol, ObjLength * 140 + 50);
					ObjPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(ObjCol)) / vs1.WidthOriginal);
				}
			}
			if (!modRegionalTown.IsRegionalTown())
			{
				vs1.ColHidden(BreakdownCol, true);
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.2 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 800));
				vs1.ColWidth(TaxCol, 100);
				vs1.ColWidth(BreakdownCol, 0);
			}
			else
			{
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.2 * vs1.WidthOriginal));
				vs1.ColWidth(BreakdownCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.08));
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + vs1.ColWidth(BreakdownCol) + 800));
				vs1.ColWidth(TaxCol, 100);
			}
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.TextMatrix(0, ExpCol, "Exp");
			vs1.TextMatrix(0, ObjCol, "Obj");
			// set column headings
			vs1.TextMatrix(0, ShortCol, "Short Desc");
			vs1.TextMatrix(0, LongCol, "Long Desc");
			vs1.TextMatrix(0, TaxCol, "1099?");
			vs1.TextMatrix(0, BreakdownCol, "M/T");
			rs.OpenRecordset("SELECT * FROM RegionalBreakdownCodes ORDER BY Code", "CentralData");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				strList = "#0;0" + "\t" + "None|";
				do
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					strList += "'" + rs.Get_Fields("Code") + ";" + rs.Get_Fields("Code") + "\t" + rs.Get_Fields_String("Description") + "|";
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			if (strList != "")
			{
				strList = Strings.Left(strList, strList.Length - 1);
			}
			vs1.ColComboList(BreakdownCol, strList);
			rs.OpenRecordset("SELECT * FROM ExpObjTitles");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs1.Rows = rs.RecordCount() + 1;
			}
			else
			{
				cmdExpenseDelete.Enabled = false;
				vs1.Rows = 1;
			}
			for (temp = 1; temp <= vs1.Rows - 1; temp++)
			{
				vs1.TextMatrix(temp, NumberCol, FCConvert.ToString(0));
			}
			vs1.Visible = true;
			temp2 = 1;
			tempObject = modValidateAccount.GetFormat("0", ref ObjLength);
			rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Object = '" + tempObject + "' ORDER BY Expense");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				NumberOfExpenses = rs.RecordCount();
				for (temp = 1; temp <= rs.RecordCount(); temp++)
				{
					//FC:FINAL:AAKV: title color change
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, temp2, ExpCol, temp2, TaxCol, 0x80000017);
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, temp2, ExpCol, temp2, TaxCol, 0x80000018);
					vs1.TextMatrix(temp2, ExpCol, rs.Get_Fields_String("Expense") + " -");
					vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
					vs1.TextMatrix(temp2, LongCol, (rs.Get_Fields_String("LongDescription") == null ? "" : rs.Get_Fields_String("LongDescription")));
					vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					if (modRegionalTown.IsRegionalTown())
					{
						vs1.TextMatrix(temp2, BreakdownCol, FCConvert.ToString(rs.Get_Fields_Int32("BreakdownCode")));
					}
					vs1.RowOutlineLevel(temp2, 1);
					vs1.IsSubtotal(temp2, true);
					temp2 += 1;
					rs2.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + rs.Get_Fields_String("Expense") + "' AND NOT Object = '" + tempObject + "' ORDER BY Object");
					if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
					{
						rs2.MoveLast();
						rs2.MoveFirst();
						for (temp3 = 1; temp3 <= rs2.RecordCount(); temp3++)
						{
							vs1.RowOutlineLevel(temp2, 2);
							vs1.TextMatrix(temp2, ObjCol, FCConvert.ToString(rs2.Get_Fields_String("Object")));
							vs1.TextMatrix(temp2, ShortCol, FCConvert.ToString(rs2.Get_Fields_String("ShortDescription")));
							vs1.TextMatrix(temp2, LongCol, FCConvert.ToString(rs2.Get_Fields_String("LongDescription")));
							vs1.TextMatrix(temp2, NumberCol, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							vs1.TextMatrix(temp2, TaxCol, FCConvert.ToString(rs2.Get_Fields("Tax")));
							temp2 += 1;
							rs2.MoveNext();
						}
					}
					rs.MoveNext();
				}
			}
			for (temp = 1; temp <= temp2 - 1; temp++)
			{
				if (vs1.RowOutlineLevel(temp) == 2)
				{
					vs1.IsCollapsed(temp, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			vs1_AfterCollapse(1, 2);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnDirty = false;
			modGlobalFunctions.SetTRIOColors(this);
            modColorScheme.ColorGrid(vs1);
			//FC:FINAL:SBE -  add expand/collapse all button
			vs1.AddExpandButton(1);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (e.CloseReason != FCCloseReason.FormCode)
			{
				if (blnDirty)
				{
					ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
				}
			}
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmOutlineExpQuery_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(ExpCol, FCConvert.ToInt32(vs1.WidthOriginal * ExpPercent + 350));
			vs1.ColWidth(ObjCol, FCConvert.ToInt32(vs1.WidthOriginal * ObjPercent));
			if (!modRegionalTown.IsRegionalTown())
			{
				vs1.ColHidden(BreakdownCol, true);
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.2 * vs1.WidthOriginal));
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + 800));
				vs1.ColWidth(TaxCol, 100);
				vs1.ColWidth(BreakdownCol, 0);
			}
			else
			{
				vs1.ColWidth(ShortCol, FCConvert.ToInt32(0.2 * vs1.WidthOriginal));
				vs1.ColWidth(BreakdownCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.08));
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + vs1.ColWidth(BreakdownCol) + 800));
				vs1.ColWidth(TaxCol, 100);
			}
			vs1_AfterCollapse(1, 2);
		}

		private void frmOutlineExpQuery_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmOutlineExpQuery.InstancePtr.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuExpenseAdd_Click(object sender, System.EventArgs e)
		{
			vs1.Rows += 1;
			//FC:FINAL:DDU:#2952 - removed color change on adding expense
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, ExpCol, vs1.Rows - 1, TaxCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, ExpCol, vs1.Rows - 1, TaxCol, 0x80000018);
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, Color.White);
			vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
			cmdExpenseDelete.Enabled = true;
			vs1_AfterCollapse(1, 2);
			vs1.Focus();
			vs1.Select(vs1.Rows - 1, ExpCol);
			vs1.EditCell();
		}

		private void mnuExpenseDelete_Click(object sender, System.EventArgs e)
		{
			Frame1.Visible = true;
			txtDelete.Focus();
			vs1.Enabled = false;
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void txtDelete_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				txtDelete_Validate(false);
				cmdDelete_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDelete_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp;
			temp = txtDelete.Text;
			txtDelete.Text = modValidateAccount.GetFormat(temp, ref ExpLength);
		}

		public void txtDelete_Validate(bool Cancel)
		{
			txtDelete_Validating(txtDelete, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), 2);
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), 0);
		}

		private void vs1_AfterCollapse(int row, int State)
		{
			int temp;
			int counter;
			int rows = 0;
			int height;
			bool ExpFlag = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						ExpFlag = true;
					}
					else
					{
						rows += 1;
						ExpFlag = false;
					}
				}
				else
				{
					if (ExpFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			height = (rows + 1) * vs1.RowHeight(0);
			if (height < frmOutlineExpQuery.InstancePtr.Height * 0.7375)
			{
				if (vs1.Height != height)
				{
					//FC:FINAL:ASZ - use anchors: vs1.Height = height + 75;
					vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + vs1.ColWidth(BreakdownCol) + 800));
				}
			}
			else
			{
				vs1.ColWidth(LongCol, vs1.WidthOriginal - (vs1.ColWidth(ExpCol) + vs1.ColWidth(ObjCol) + vs1.ColWidth(0) + vs1.ColWidth(ShortCol) + vs1.ColWidth(BreakdownCol) + 1000));
				//FC:FINAL:ASZ - use anchors: vs1.Height = FCConvert.ToInt32(frmOutlineExpQuery.InstancePtr.Height * 0.7375 + 75);
			}
			counter = vs1.Row;
			while (vs1.RowOutlineLevel(counter) != 0)
			{
				counter -= 1;
			}
			if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				vs1.Row = counter;
			}
			else
			{
				// do nothing
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnDirty = true;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			POINTAPI MousePosition = new POINTAPI();
			// vbPorter upgrade warning: PassFail As bool	OnWriteFCConvert.ToInt32(
			bool PassFail;
			PassFail = FCConvert.ToBoolean(GetCursorPos(ref MousePosition));
			if (MousePosition.x * FCScreen.TwipsPerPixelX < vs1.Left + vs1.ColWidth(0))
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				return;
			}
			if (vs1.Col == NumberCol)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				return;
			}
			if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col == ObjCol && vs1.TextMatrix(vs1.Row, ExpCol) == "")
				{
					vs1.Select(vs1.Row, ExpCol);
				}
				else if (vs1.Col == ObjCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col == ExpCol && vs1.TextMatrix(vs1.Row, ExpCol) == "")
				{
					// do nothing
				}
				else if (vs1.Col == ExpCol)
				{
					vs1.Select(vs1.Row, ShortCol);
				}
				if (vs1.Col == TaxCol)
				{
					vs1.Select(vs1.Row, LongCol);
				}
			}
			else
			{
				if (vs1.Col == ExpCol)
				{
					if (vs1.TextMatrix(vs1.Row, ObjCol) == "")
					{
						vs1.Select(vs1.Row, ObjCol);
					}
					else
					{
						vs1.Select(vs1.Row, ShortCol);
					}
				}
				if (vs1.Col == BreakdownCol)
				{
					vs1.Select(vs1.Row, LongCol);
				}
				if (vs1.Col == TaxCol)
				{
					if (vs1.TextMatrix(vs1.Row, ObjCol) != "")
					{
						vs1.EditCell();
						if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
						{
							vs1.EditText = "N";
						}
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
						vs1.EditMaxLength = 1;
						return;
					}
					else
					{
						vs1.Col = ObjCol;
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				if (vs1.Col == ObjCol)
				{
					if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
					{
						vs1.Select(vs1.Row, ShortCol);
					}
				}
			}
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			vs1.Select(vs1.Row, vs1.Col);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			// give the cell focus
			vs1.EditSelStart = 0;
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// vbPorter upgrade warning: Check As string	OnWrite(DialogResult)
			int Check = 0;
			int counter = 0;
			string tempExpense = "";
			string tempObject = "";
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Insert)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (ObjLength > 0)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							MessageBox.Show("You must open up an Expense to add an Object to it", "Unable to Create an Object", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else
						{
							if (vs1.Row == vs1.Rows - 1)
							{
								vs1.Rows += 1;
								vs1.Select(vs1.Row + 1, ObjCol);
								vs1.EditText = "";
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
								vs1.RowOutlineLevel(vs1.Row, 2);
								vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
								vs1_AfterCollapse(1, 2);
								vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
								vs1.Select(vs1.Row, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
								return;
							}
						}
					}
					else
					{
						MessageBox.Show("You are not set up to use objects in your expense accounts", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				vs1.AddItem("", vs1.Row + 1);
				// add a row in
				vs1.EditText = "";
				vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.TextMatrix(vs1.Row + 1, TaxCol, "N");
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row + 1, 0, Color.White);
				vs1.RowOutlineLevel(vs1.Row + 1, 2);
				vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
				vs1_AfterCollapse(1, 2);
				if (vs1.RowOutlineLevel(vs1.Row) > 1)
				{
					counter = vs1.Row - 1;
					while (vs1.RowOutlineLevel(counter) != 0)
					{
						counter -= 1;
					}
				}
				else
				{
					counter = vs1.Row;
				}
				vs1.Select(vs1.Row + 1, ObjCol);
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
			else if (KeyCode == Keys.Delete)
			{
				// if they hit the delete ID
				if (vs1.TextMatrix(vs1.Row, ObjCol) != "" && vs1.TextMatrix(vs1.Row, ShortCol) != "")
				{
					if (vs1.RowOutlineLevel(vs1.Row) > 1)
					{
						Check = FCConvert.ToInt32(MessageBox.Show("Are You Sure You Want To Delete This Record?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
						// make sure they want to delete
						if (Check == FCConvert.ToInt32(DialogResult.Yes))
						{
							// if they do
							if (CheckObjUsed_2(Strings.Trim(vs1.TextMatrix(vs1.Row, ObjCol))) == false)
							{
								if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, NumberCol)) != 0)
								{
									rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, NumberCol))));
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										tempExpense = FCConvert.ToString(rs.Get_Fields_String("Expense"));
										tempObject = FCConvert.ToString(rs.Get_Fields_String("Object"));
										rs.Delete();
										rs.Update();
									}
									if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) == 0)
									{
										rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' and SecondAccountField = '" + tempExpense + "' AND ThirdAccountField = '" + tempObject + "'");
									}
									else
									{
										rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' and ThirdAccountField = '" + tempExpense + "' AND FourthAccountField = '" + tempObject + "'");
									}
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										rs.MoveLast();
										rs.MoveFirst();
										for (counter = 1; counter <= rs.RecordCount(); counter++)
										{
											rs.Delete();
											rs.Update();
										}
									}
								}
								vs1.RemoveItem(vs1.Row);
								vs1_AfterCollapse(1, 2);
								counter = vs1.Row;
								while (vs1.RowOutlineLevel(counter) != 0)
								{
									counter -= 1;
								}
							}
							else
							{
								MessageBox.Show("You may not delete this Object because it it has a budget associated with it or is used in one or more journal entries.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(vs1.Row) > 1)
					{
						vs1.RemoveItem(vs1.Row);
						vs1_AfterCollapse(1, 2);
						counter = vs1.Row - 1;
						while (vs1.RowOutlineLevel(counter) != 0)
						{
							counter -= 1;
						}
						if (counter == 0)
						{
							counter = 1;
						}
					}
				}
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

        private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(vs1_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(vs1_KeyDownEdit);
            }
        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int counter = 0;
			int NextOpenRow = 0;
			Keys KeyCode = e.KeyCode;
            //FC:FINAL:AM:#4308 - don't change the column; in case the validation fails it will throw an exception
            //if (vs1.Col == ObjCol)
            //{
            //	// if they are in the division field
            //	if (vs1.EditText.Length == ObjLength)
            //	{
            //		// if the cell is the length of the division number
            //		if (vs1.EditSelStart == ObjLength)
            //		{
            //                     // dont move the cursor
            //                     if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
            //                     {
            //				KeyCode = 0;
            //				// dont send the ID through
            //				vs1.Col = ShortCol;
            //				// send the user to the next column
            //			}
            //		}
            //	}
            //}
            if (vs1.Col == ExpCol)
			{
				if (vs1.EditText.Length == ExpLength)
				{
					if (vs1.EditSelStart == ExpLength)
					{
						if (KeyCode != Keys.Back && KeyCode != Keys.Left && KeyCode != Keys.Up && KeyCode != Keys.Down)
						{
							KeyCode = 0;
							vs1.Col = ShortCol;
						}
					}
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < TaxCol && vs1.RowOutlineLevel(vs1.Row) == 2 || vs1.Col < BreakdownCol && vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == ObjCol)
					{
						if (vs1.TextMatrix(vs1.Row, vs1.Col) != "" || vs1.EditText != "")
						{
							EditFlag = true;
							vs1.Select(vs1.Row, vs1.Col + 1);
							EditFlag = false;
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
						else
						{
							if (vs1.Row < vs1.Rows - 1)
							{
								if (vs1.RowOutlineLevel(vs1.Row + 1) == 1)
								{
									if (vs1.TextMatrix(vs1.Row + 1, ExpCol) == "")
									{
										EditFlag = true;
										vs1.Row += 1;
										vs1.Col = ExpCol;
										vs1.EditCell();
										EditFlag = false;
									}
									else
									{
										EditFlag = true;
										vs1.Row += 1;
										vs1.Col = ShortCol;
										vs1.EditCell();
										EditFlag = false;
									}
								}
								else
								{
									if (vs1.TextMatrix(vs1.Row + 1, ObjCol) == "")
									{
										EditFlag = true;
										vs1.Row += 1;
										vs1.Col = ObjCol;
										vs1.EditCell();
										EditFlag = false;
									}
									else
									{
										EditFlag = true;
										vs1.Row += 1;
										vs1.Col = ShortCol;
										vs1.EditCell();
										EditFlag = false;
									}
								}
							}
						}
					}
					else if (vs1.Col == LongCol)
					{
						if (vs1.RowOutlineLevel(vs1.Row) == 2)
						{
							vs1.Select(vs1.Row, vs1.Col + 2);
							vs1.EditCell();
							vs1.EditSelStart = 0;
							vs1.EditSelLength = 1;
						}
						else
						{
							vs1.Select(vs1.Row, vs1.Col + 1);
							vs1.EditCell();
							vs1.EditSelStart = 0;
							vs1.EditSelLength = 1;
						}
					}
					else
					{
						vs1.Select(vs1.Row, vs1.Col + 1);
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						if (vs1.RowOutlineLevel(vs1.Row) == 1 && vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							NextOpenRow = vs1.Row + 1;
							while (vs1.RowOutlineLevel(NextOpenRow) != 0)
							{
								NextOpenRow += 1;
								if (NextOpenRow == vs1.Rows)
								{
									break;
								}
							}
						}
						else
						{
							NextOpenRow = vs1.Row + 1;
						}
						if (NextOpenRow == vs1.Rows)
						{
							return;
						}
						else
						{
							vs1.Row = NextOpenRow;
						}
						if (vs1.RowOutlineLevel(vs1.Row) == 1)
						{
							if (vs1.TextMatrix(vs1.Row, ExpCol) != "")
							{
								vs1.Select(vs1.Row, ShortCol);
							}
							else
							{
								vs1.Select(vs1.Row, ExpCol);
							}
						}
						else
						{
							if (vs1.TextMatrix(vs1.Row, ObjCol) != "")
							{
								vs1.Select(vs1.Row, ShortCol);
							}
							else
							{
								vs1.Select(vs1.Row, ObjCol);
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					counter = vs1.Row + 1;
					while (vs1.RowOutlineLevel(counter) != 0)
					{
						counter += 1;
						if (counter < vs1.Rows - 1)
						{
							// do nothing
						}
						else
						{
							break;
						}
					}
				}
				else
				{
					counter = vs1.Row + 1;
				}
				if (counter > vs1.Rows - 1)
				{
					return;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					// if its a heading and they are in the Expense column or the Object column
					if (vs1.Col == ExpCol || vs1.Col == ObjCol)
					{
						// if the column we are moving to is not a heading row
						if (vs1.RowOutlineLevel(counter) > 1)
						{
							if (vs1.TextMatrix(counter, ObjCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							// if we are moving to a heading
						}
						else
						{
							if (vs1.TextMatrix(counter, ExpCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ExpCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						// if we are in a heading and not in the Expense or Object columns
					}
					else
					{
						// if we are moving to a row that is not a heading
						if (vs1.RowOutlineLevel(counter) > 1)
						{
							if (vs1.TextMatrix(counter, ObjCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, vs1.Col);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							// else we are moving to a heading row
						}
						else
						{
							if (vs1.TextMatrix(counter, ExpCol) != "")
							{
								if (vs1.Col == TaxCol)
								{
									KeyCode = 0;
									vs1.Select(counter, LongCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									vs1.Select(counter, vs1.Col);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ExpCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
				else
				{
					// if the row is not a heading and the column is expense or object
					if (vs1.Col == ObjCol || vs1.Col == ExpCol)
					{
						// if we are moving to a heading
						if (vs1.RowOutlineLevel(counter) != 2)
						{
							if (vs1.TextMatrix(counter, ExpCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ExpCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							// if we are moving to a regular row
						}
						else
						{
							if (vs1.TextMatrix(counter, ObjCol) != "")
							{
								KeyCode = 0;
								vs1.Select(counter, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(counter, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						// if we are not in a heading and not in the expense or object columns
					}
					else
					{
						// if we are moving to a heading
						if (vs1.RowOutlineLevel(counter) != 2)
						{
							if (vs1.Col == TaxCol)
							{
								KeyCode = 0;
								vs1.Select(counter, LongCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						else
						{
							if (vs1.Col == BreakdownCol)
							{
								KeyCode = 0;
								vs1.Select(counter, LongCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
						if (vs1.Row < vs1.Rows - 1)
						{
							if (vs1.TextMatrix(counter, ShortCol) == "" && vs1.TextMatrix(vs1.Row + 1, ObjCol) == "")
							{
								KeyCode = 0;
								vs1.Select(counter, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == ShortCol)
					{
						if (vs1.TextMatrix(vs1.Row, ExpCol) != "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Col = ExpCol;
						}
					}
				}
				else
				{
					if (vs1.Col == ShortCol)
					{
						if (vs1.TextMatrix(vs1.Row, ObjCol) != "")
						{
							KeyCode = 0;
						}
					}
					else if (vs1.Col == TaxCol)
					{
						KeyCode = 0;
						vs1.Col = LongCol;
					}
					else
					{
						KeyCode = 0;
						vs1.Col = ObjCol;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row > 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row) == 1)
					{
						counter = vs1.Row - 1;
						while (vs1.RowOutlineLevel(counter) != 0)
						{
							counter -= 1;
						}
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							// do nothing
						}
						else
						{
							counter = vs1.Row - 1;
						}
						if (counter == vs1.Row - 1)
						{
							if (vs1.Col == ObjCol || vs1.Col == ExpCol)
							{
								if (vs1.RowOutlineLevel(vs1.Row - 1) == 1)
								{
									if (vs1.TextMatrix(vs1.Row - 1, ExpCol) != "")
									{
										KeyCode = 0;
										vs1.Select(counter, ShortCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										vs1.Select(counter, ExpCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
								else
								{
									if (vs1.TextMatrix(vs1.Row - 1, ObjCol) != "")
									{
										KeyCode = 0;
										vs1.Select(counter, ShortCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
									else
									{
										KeyCode = 0;
										vs1.Select(counter, ObjCol);
										vs1.EditCell();
										vs1.EditSelStart = 0;
									}
								}
							}
							else if (vs1.Col == BreakdownCol)
							{
								if (vs1.RowOutlineLevel(vs1.Row - 1) == 2)
								{
									KeyCode = 0;
									vs1.Select(vs1.Row - 1, LongCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
						}
						else
						{
							if (vs1.RowOutlineLevel(counter) > 1)
							{
								if (vs1.TextMatrix(counter, ObjCol) != "")
								{
									KeyCode = 0;
									vs1.Select(counter, ShortCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									vs1.Select(counter, ObjCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
							else
							{
								if (vs1.TextMatrix(counter, ExpCol) != "")
								{
									KeyCode = 0;
									vs1.Select(counter, ShortCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
								else
								{
									KeyCode = 0;
									vs1.Select(counter, ExpCol);
									vs1.EditCell();
									vs1.EditSelStart = 0;
								}
							}
						}
					}
					else
					{
						if (vs1.Col == ObjCol || vs1.Col == ExpCol)
						{
							if (vs1.TextMatrix(vs1.Row - 1, ObjCol) != "")
							{
								KeyCode = 0;
								vs1.Select(vs1.Row - 1, ShortCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
							else
							{
								KeyCode = 0;
								vs1.Select(vs1.Row - 1, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
							}
						}
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == ExpCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
						else
						{
							KeyCode = 0;
							vs1.Select(vs1.Row, ShortCol);
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
				}
				else
				{
					if (vs1.Col == ObjCol)
					{
						if (vs1.EditText == "")
						{
							KeyCode = 0;
						}
					}
				}
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				Close();
			}
			else if (KeyCode == Keys.Insert)
			{
				if (vs1.Col == TaxCol)
				{
					//FC:FINAL:SBE - executing EndEdit, will trigger validate if cell is in edit mode
					//vs1_ValidateEdit_18(vs1.Row, TaxCol, false);
					if (vs1.IsCurrentCellInEditMode)
					{
						vs1.EndEdit();
					}
				}
				vs1.TextMatrix(vs1.Row, vs1.Col, vs1.EditText);
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (ObjLength > 0)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							MessageBox.Show("You must open up an Expense to add an Object to it", "Unable to Create an Object ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else if (vs1.TextMatrix(vs1.Row, ExpCol) == "" || vs1.TextMatrix(vs1.Row, ShortCol) == "")
						{
							MessageBox.Show("You must have a short description and a number for an expense before you can add an object.", "Unable to Create an Object ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
							return;
						}
						else
						{
							if (vs1.Row == vs1.Rows - 1)
							{
								vs1.Rows += 1;
								vs1.Select(vs1.Row + 1, ObjCol);
								vs1.EditText = "";
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
								vs1.RowOutlineLevel(vs1.Row, 2);
								vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
								vs1_AfterCollapse(1, 2);
								vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
								vs1.Select(vs1.Row, ObjCol);
								vs1.EditCell();
								vs1.EditSelStart = 0;
								return;
							}
						}
					}
					else
					{
						vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
						return;
					}
				}
				if (vs1.Row == vs1.Rows - 1)
				{
					vs1.Rows += 1;
				}
				else
				{
					vs1.AddItem("", vs1.Row + 1);
					// add a row in
				}
				vs1.Select(vs1.Row + 1, ObjCol);
				vs1.EditText = "";
				vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vs1.TextMatrix(vs1.Row, TaxCol, "N");
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
				vs1.RowOutlineLevel(vs1.Row, 2);
				vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));
				vs1_AfterCollapse(1, 2);
				counter = vs1.Row - 1;
				while (vs1.RowOutlineLevel(counter) != 0)
				{
					counter -= 1;
				}
				vs1.Select(vs1.Row, ObjCol);
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			}
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
            if (vs1.GetFlexColIndex(e.ColumnIndex) == TaxCol)
			{
				//ToolTip1.SetToolTip(vs1, "Please enter an N or a number between 1 and 9 in this field");
				cell.ToolTipText =  "Please enter an N or a number between 1 and 9 in this field";
			}
			else
			{
				//ToolTip1.SetToolTip(vs1, "To add an object to an expense click on the expense you would like to add the object to and hit the Insert ID.");
				cell.ToolTipText = "To add an object to an expense click on the expense you would like to add the object to and hit the Insert ID.";
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.Col == ShortCol)
			{
				vs1.EditMaxLength = 12;
			}
			else if (vs1.Col == LongCol)
			{
				vs1.EditMaxLength = 30;
			}
			else if (vs1.Col == ExpCol)
			{
				vs1.EditMaxLength = ExpLength;
			}
			else if (vs1.Col == ObjCol)
			{
				vs1.EditMaxLength = ObjLength;
			}
			if (!EditFlag)
			{
				if (vs1.Col == NumberCol)
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
					return;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col == ObjCol && vs1.TextMatrix(vs1.Row, ExpCol) == "")
					{
						vs1.Select(vs1.Row, ExpCol);
					}
					else if (vs1.Col == ObjCol)
					{
						vs1.Select(vs1.Row, ShortCol);
					}
					if (vs1.Col == ExpCol && vs1.TextMatrix(vs1.Row, ExpCol) == "")
					{
						// do nothing
					}
					else if (vs1.Col == ExpCol)
					{
						vs1.Select(vs1.Row, ShortCol);
					}
					if (vs1.Col == TaxCol)
					{
						vs1.Select(vs1.Row, LongCol);
					}
				}
				else
				{
					if (vs1.Col == ExpCol)
					{
						if (vs1.TextMatrix(vs1.Row, ObjCol) == "")
						{
							vs1.Select(vs1.Row, ObjCol);
						}
						else
						{
							vs1.Select(vs1.Row, ShortCol);
						}
					}
					if (vs1.Col == BreakdownCol)
					{
						vs1.Select(vs1.Row, LongCol);
					}
					if (vs1.Col == TaxCol)
					{
						vs1.EditCell();
						if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
						{
							vs1.EditText = "N";
						}
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
						vs1.EditMaxLength = 1;
						return;
					}
					if (vs1.Col == ObjCol)
					{
						if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
						{
							vs1.Select(vs1.Row, ShortCol);
						}
					}
					else
					{
						if (vs1.TextMatrix(vs1.Row, ObjCol) == "")
						{
							vs1.Select(vs1.Row, ObjCol);
						}
					}
				}
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.Select(vs1.Row, vs1.Col);
			vs1.EditCell();
			// give the cell focus
			vs1.EditSelStart = 0;
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int counter = 0;
			int counter2 = 0;
			bool flag;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == ObjCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref ObjLength);
					}
				}
				if (vs1.EditText.Length == ObjLength)
				{
					// if there is something in the field
					counter = row - 1;
					while (vs1.RowOutlineLevel(counter) != 1)
					{
						counter -= 1;
					}
					if (row == vs1.Rows - 1)
					{
						counter2 = row;
					}
					else
					{
						counter2 = row + 1;
					}
					while (vs1.RowOutlineLevel(counter2) > 1)
					{
						counter2 += 1;
						if (counter2 == vs1.Rows)
						{
							counter2 -= 1;
							break;
						}
					}
					for (counter = counter; counter <= counter2 - 1; counter++)
					{
						// check to see if it is a duplicate division number
						if (counter == row || vs1.RowOutlineLevel(counter) != 2)
						{
							// do nothing
						}
						else
						{
							if (vs1.EditText == vs1.TextMatrix(counter, col))
							{
								// if there is a match give an error
								MessageBox.Show("This Object Number is Not Allowable Because it Matches a Previously Existing Object Number", "Invalid Object Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								// cancel their action
								vs1.EditSelStart = 0;
								// highlight the field
								vs1.EditSelLength = vs1.EditText.Length;
								return;
							}
						}
					}
				}
			}
			if (col == ExpCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref ExpLength);
						vs1.EditText = vs1.EditText + " -";
					}
				}
				if (vs1.EditText.Length == ExpLength + 2)
				{
					// if there is something in the field
					for (counter = 1; counter <= vs1.Rows - 1; counter++)
					{
						// check to see if it is a duplicate division number
						if (vs1.RowOutlineLevel(counter) == 1)
						{
							if (counter == row)
							{
								// do nothing
							}
							else
							{
								if (vs1.EditText == vs1.TextMatrix(counter, col))
								{
									// if there is a match give an error
									MessageBox.Show("This Expense Number is Not Allowable Because it Matches a Previously Existing Expense Number", "Invalid Expense Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									// cancel their action
									vs1.EditSelStart = 0;
									// highlight the field
									vs1.EditSelLength = vs1.EditText.Length;
									return;
								}
							}
						}
					}
				}
			}
			if (col == TaxCol)
			{
				// if we are in the division column
				vs1.DataRefresh();
				if (vs1.EditText.Length > 0)
				{
					// if there is something in that field
					if (!Information.IsNumeric(vs1.EditText) && Strings.UCase(vs1.EditText) != "N")
					{
						MessageBox.Show("You may only enter a number from 1 to 9 or an N in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else if (Information.IsNumeric(vs1.EditText) && Conversion.Val(vs1.EditText) == 0)
					{
						MessageBox.Show("You may only enter a number from 1 to 9 or an N in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditSelStart = 0;
						vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
						return;
					}
					else
					{
						vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						if (vs1.EditText == "n")
						{
							vs1.EditText = "N";
						}
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(row) == 1)
					{
						if (vs1.TextMatrix(row, ExpCol) != "")
						{
							vs1.EditText = "N";
							vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						}
					}
					else
					{
						if (vs1.TextMatrix(row, ObjCol) != "")
						{
							vs1.EditText = "N";
							vs1.CellAlignment = FCGrid.AlignmentSettings.flexAlignCenterBottom;
						}
					}
				}
			}
		}

		private bool CheckExpUsed_2(string strExp)
		{
			return CheckExpUsed(ref strExp);
		}

		private bool CheckExpUsed(ref string strExp)
		{
			bool CheckExpUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			int intStart = 0;
			CheckExpUsed = false;
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				intStart = 4 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2));
			}
			else
			{
				intStart = 5 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2));
			}
			rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckExpUsed = true;
				return CheckExpUsed;
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "' AND CurrentBudget <> 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckExpUsed = true;
				return CheckExpUsed;
			}
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckExpUsed = true;
				return CheckExpUsed;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckExpUsed = true;
				return CheckExpUsed;
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckExpUsed = true;
				return CheckExpUsed;
			}
			return CheckExpUsed;
		}

		private bool CheckObjUsed_2(string strObj)
		{
			return CheckObjUsed(ref strObj);
		}

		private bool CheckObjUsed(ref string strObj)
		{
			bool CheckObjUsed = false;
			clsDRWrapper rs = new clsDRWrapper();
			int intObjStart = 0;
			int intExpStart = 0;
			string strExp = "";
			int counter;
			CheckObjUsed = false;
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				intExpStart = 4 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2));
				intObjStart = 5 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) + ExpLength;
			}
			else
			{
				intExpStart = 5 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2));
				intObjStart = 6 + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 1, 2)) + FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + ExpLength;
			}
			for (counter = vs1.Row; counter >= 1; counter--)
			{
				if (vs1.TextMatrix(counter, ExpCol) != "")
				{
					strExp = Strings.Left(vs1.TextMatrix(counter, ExpCol), ExpLength);
					break;
				}
			}
			rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intObjStart) + ", " + FCConvert.ToString(ObjLength) + ") = '" + strObj + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckObjUsed = true;
				return CheckObjUsed;
			}
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intObjStart) + ", " + FCConvert.ToString(ObjLength) + ") = '" + strObj + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "' AND CurrentBudget <> 0");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckObjUsed = true;
				return CheckObjUsed;
			}
			rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intObjStart) + ", " + FCConvert.ToString(ObjLength) + ") = '" + strObj + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckObjUsed = true;
				return CheckObjUsed;
			}
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intObjStart) + ", " + FCConvert.ToString(ObjLength) + ") = '" + strObj + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckObjUsed = true;
				return CheckObjUsed;
			}
			rs.OpenRecordset("SELECT * FROM StandardAccounts WHERE Left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intObjStart) + ", " + FCConvert.ToString(ObjLength) + ") = '" + strObj + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(ExpLength) + ") = '" + strExp + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				CheckObjUsed = true;
				return CheckObjUsed;
			}
			return CheckObjUsed;
		}

		private void SaveInfo()
		{
			int counter;
			bool flag = false;
			string tempExpense = "";
			clsDRWrapper rsTesting = new clsDRWrapper();
			vs1.Select(0, 1);
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (Conversion.Val(vs1.TextMatrix(counter, ExpCol)) == 0)
					{
						MessageBox.Show("There must be an expense number for each expense before you can save.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
					{
						MessageBox.Show("There must be a short description for each expense before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (vs1.TextMatrix(counter, ObjCol) != "")
					{
						if (Strings.Trim(vs1.TextMatrix(counter, ShortCol)) == "")
						{
							flag = true;
							break;
						}
					}
				}
			}
			if (flag)
			{
				MessageBox.Show("There must be a short description for every object before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			counter = 0;
			while (DeletedExpenses[counter] != "")
			{
				//Application.DoEvents();
				rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + DeletedExpenses[counter] + "'");
				while (rs.EndOfFile() != true)
				{
					rs.Delete();
					rs.Update();
				}
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					rs.Execute("DELETE FROM AccountMaster WHERE AccountType = 'E' AND SecondAccountField = '" + DeletedExpenses[counter] + "'", "Budgetary");
					rs.Execute("DELETE FROM Budget WHERE left(Account, 1) = 'E' AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") = '" + DeletedExpenses[counter] + "'", "Budgetary");
				}
				else
				{
					rs.Execute("DELETE FROM AccountMaster WHERE AccountType = 'E' AND ThirdAccountField = '" + DeletedExpenses[counter] + "'", "Budgetary");
					rs.Execute("DELETE FROM Budget WHERE left(Account, 1) = 'E' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") = '" + DeletedExpenses[counter] + "'", "Budgetary");
				}
				counter += 1;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
					{
						// is this a new record
						rsTesting.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength) + "' AND Object = '" + modValidateAccount.GetFormat("0", ref ObjLength) + "'");
						if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
						{
							MessageBox.Show("Saving this information would create duplicate entries for expense " + Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength) + ".", "Duplicate Expense", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							frmWait.InstancePtr.Unload();
							return;
						}
						rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE ID = 0");
						rs.AddNew();
						// if so add it
						tempExpense = Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength);
						rs.Set_Fields("Expense", tempExpense);
						rs.Set_Fields("Object", modValidateAccount.GetFormat("0", ref ObjLength));
						rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
						if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
						}
						else
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
						}
						if (modRegionalTown.IsRegionalTown())
						{
							rs.Set_Fields("BreakdownCode", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, BreakdownCol))));
						}
						else
						{
							rs.Set_Fields("BreakdownCode", 0);
						}
						rs.Update();
						// update the database
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
						rs.Edit();
						// if not a new record edit the existing record
						tempExpense = Strings.Mid(vs1.TextMatrix(counter, ExpCol), 1, ExpLength);
						rs.Set_Fields("Expense", tempExpense);
						rs.Set_Fields("Object", modValidateAccount.GetFormat("0", ref ObjLength));
						rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
						if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
						}
						else
						{
							rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
						}
						if (modRegionalTown.IsRegionalTown())
						{
							rs.Set_Fields("BreakdownCode", FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, BreakdownCol))));
						}
						else
						{
							rs.Set_Fields("BreakdownCode", 0);
						}
						rs.Update();
						// update the database
					}
				}
				else
				{
					if (vs1.TextMatrix(counter, ObjCol) == "")
					{
						// if the row contains no data
						// do nothing
					}
					else
					{
						if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) == 0)
						{
							// is this a new record
							rsTesting.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + tempExpense + "' AND Object = '" + vs1.TextMatrix(counter, ObjCol) + "'");
							if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
							{
								MessageBox.Show("Saving this information would create duplicate entries for object " + tempExpense + "-" + vs1.TextMatrix(counter, ObjCol) + ".", "Duplicate Object", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								frmWait.InstancePtr.Unload();
								return;
							}
							rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE ID = 0");
							rs.AddNew();
							// if so add it
							rs.Set_Fields("Expense", tempExpense);
							rs.Set_Fields("Object", vs1.TextMatrix(counter, ObjCol));
							// put information in the database
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							if (vs1.TextMatrix(counter, TaxCol) != "")
							{
								rs.Set_Fields("Tax", vs1.TextMatrix(counter, TaxCol));
							}
							else
							{
								rs.Set_Fields("Tax", "N");
							}
							rs.Update();
							// update the database
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
							rs.Edit();
							// if not a new record edit the existing record
							rs.Set_Fields("Expense", tempExpense);
							rs.Set_Fields("Object", vs1.TextMatrix(counter, ObjCol));
							// put information in the database
							rs.Set_Fields("ShortDescription", vs1.TextMatrix(counter, ShortCol));
							if (Strings.Trim(vs1.TextMatrix(counter, LongCol)) == "")
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, ShortCol));
							}
							else
							{
								rs.Set_Fields("LongDescription", vs1.TextMatrix(counter, LongCol));
							}
							if (vs1.TextMatrix(counter, TaxCol) != "")
							{
								rs.Set_Fields("Tax", vs1.TextMatrix(counter, TaxCol));
							}
							else
							{
								rs.Set_Fields("Tax", "N");
							}
							rs.Update();
							// update the database
						}
					}
				}
			}
			frmWait.InstancePtr.Unload();
			blnDirty = false;
			//Application.DoEvents();
			MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				Close();
			}
		}
	}
}
