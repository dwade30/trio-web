﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptPurgedVendors.
	/// </summary>
	public partial class rptPurgedVendors : BaseSectionReport
	{
		public static rptPurgedVendors InstancePtr
		{
			get
			{
				return (rptPurgedVendors)Sys.GetInstance(typeof(rptPurgedVendors));
			}
		}

		protected rptPurgedVendors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPurgedVendors	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intTotal;

		public rptPurgedVendors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Purged Vendors List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            using (clsDRWrapper rsDelete = new clsDRWrapper())
            {
                if (blnFirstRecord)
                {
                    blnFirstRecord = false;
                    eArgs.EOF = false;
                }
                else
                {
                    rsDelete.Execute(
                        "DELETE FROM Adjustments WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"),
                        "Budgetary");
                    rsDelete.Execute(
                        "DELETE FROM DefaultInfo WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"),
                        "Budgetary");
                    rsDelete.Execute(
                        "DELETE FROM VendorTaxInfoArchive WHERE VendorNumber = " +
                        rsInfo.Get_Fields_Int32("VendorNumber"), "Budgetary");
                    rsInfo.Delete();
                    rsInfo.Update();
                    eArgs.EOF = rsInfo.EndOfFile();
                }
            }
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			intTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE Status = 'D' AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM APJournal) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM Encumbrances) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM APJournalArchive) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM JournalEntries) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM CDJournalArchive) AND VendorNumber NOT IN (SELECT DISTINCT VendorNumber FROM CreditMemo)");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldVendor.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("VendorNumber"), 5) + "  " + rsInfo.Get_Fields_String("CheckName");
			intTotal += 1;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldTotal As Variant, short --> As int
			//FC:FINAL:MSH - Issue #631: TextBox 'fldTotal' was blocked by local int variable with the same name 'fldTotal'.
			//int fldTotal;   // - "AutoDim"
			//fldTotal = intTotal;
			fldTotal.Text = FCConvert.ToString(intTotal);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		
	}
}
