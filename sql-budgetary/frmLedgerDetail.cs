﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerDetail.
	/// </summary>
	public partial class frmLedgerDetail : BaseForm
	{
		public frmLedgerDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            vs1.SuppressExpandCollapseEventsOnExpandCollapseAll = true;
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerDetail InstancePtr
		{
			get
			{
				return (frmLedgerDetail)Sys.GetInstance(typeof(frmLedgerDetail));
			}
		}

		protected frmLedgerDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		clsDRWrapper rs4 = new clsDRWrapper();
		clsDRWrapper rs5 = new clsDRWrapper();
		clsDRWrapper rs6 = new clsDRWrapper();
		clsDRWrapper rs7 = new clsDRWrapper();
		clsDRWrapper UsedAccounts = new clsDRWrapper();
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool EncumbranceFlag;
		bool PendingSummaryFlag;
		bool PendingDetailFlag;
		int CurrentRow;
		string currentFund = "";
		string currentAccount = "";
		string CurrentSuffix = "";
		int CurrentCol;
		int PostedCol;
		int TransCol;
		int PeriodCol;
		int RCBCol;
		int JournalCol;
		int DescriptionCol;
		int WarrantCol;
		int CheckCol;
		int VendorCol;
		int YTDDebitCol;
		int YTDCreditCol;
		int BalanceDebitCol;
		int BalanceCreditCol;
		int EncumbranceCol;
		int PendingCol;
		string strPeriodCheck = "";
		string[] strBalances = null;
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		clsDRWrapper rsFundSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsAcctSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsFundBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsAcctBudgetInfo = new clsDRWrapper();
		bool blnRunCollapse;
		public string strTitle = "";
        cLedgerDetailReport theReport = new cLedgerDetailReport();

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdExport_Click(object sender, System.EventArgs e)
		{
			ExportData();
		}

		private void ExportData()
		{
			cLedgerDetailController rdc = new cLedgerDetailController();
			rdc.FormatType = 0;
			// csv
			frmReportDataExport rdeForm = new frmReportDataExport(this.Text);
			cReportExportView reView = new cReportExportView();
			reView.SetReport(theReport);
			rdeForm.SetViewModel(ref reView);
			reView.SetExporter(rdc);
			rdeForm.Show(this);
			rdeForm.TopMost = true;
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
            if (cmdPrint.Enabled == true)
			{
                frmReportViewer.InstancePtr.Init(rptLedgerDetail.InstancePtr);
			}
		}

		private void frmLedgerDetail_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper Descriptions = new clsDRWrapper();
			int HMonth = 0;
			int LMonth = 0;
			string strPeriodCheck;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rs.OpenRecordset("SELECT * FROM LedgerDetailFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			lblTitle.Text = "Fund(s)";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
			{
				lblTitle.Text = "Accounts";
				lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount");
			}
			else
			{
				lblRangeDept.Text = "ALL";
			}
			// lblMonthLabel.Visible = True
			strPeriodCheck = "AND";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnFromPreview)
			{
				FormatGrid();
				frmWait.InstancePtr.prgProgress.Value = 20;
				frmWait.InstancePtr.Refresh();
				PrepareTemp();
				frmWait.InstancePtr.prgProgress.Value = 40;
				frmWait.InstancePtr.Refresh();
                modBudgetaryAccounting.CalculateAccountInfo();
                frmWait.InstancePtr.prgProgress.Value = 80;
				frmWait.InstancePtr.Refresh();
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
				{
                    if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Status = 'P' ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
					}
					else
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo ORDER BY Account, OrderMonth, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Status = 'P' ORDER BY Account, OrderMonth, JournalNumber");
						}
					}
				}
				else
				{
                    LMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					}
					else
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
					}
					if (LMonth > HMonth)
					{
						strPeriodCheck = "OR";
					}
					else
					{
						strPeriodCheck = "AND";
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " AND Status = 'P' ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
					}
					else
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " ORDER BY Account, OrderMonth, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM LedgerDetailInfo WHERE Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + " AND Status = 'P' ORDER BY Account, OrderMonth, JournalNumber");
						}
					}
				}
				LMonth = modBudgetaryMaster.Statics.FirstMonth;
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == LMonth)
				{
					HMonth = -1;
					LMonth = -1;
				}
				else
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == 1)
					{
						HMonth = 12;
					}
					else
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
					}
				}
				if (LMonth > HMonth)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
				{
					rsFundBudgetInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund");
					rsAcctBudgetInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund, LedgerAccount, Suffix");
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund, LedgerAccount, Suffix");
					}
					else
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Fund, LedgerAccount, Suffix");
					}
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
				{
					rsFundBudgetInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' GROUP BY Fund");
					rsAcctBudgetInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' GROUP BY Fund, LedgerAccount, Suffix");
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND (Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND (Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Fund, LedgerAccount, Suffix");
					}
					else
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Fund >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Fund <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Fund, LedgerAccount, Suffix");
					}
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
				{
					rsFundBudgetInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' GROUP BY Fund");
					rsAcctBudgetInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' GROUP BY Fund, LedgerAccount, Suffix");
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Fund, LedgerAccount, Suffix");
					}
					else
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Fund, LedgerAccount, Suffix");
					}
				}
				else
				{
					rsFundBudgetInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo GROUP BY Fund");
					rsAcctBudgetInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo GROUP BY Fund, LedgerAccount, Suffix");
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period < 0 GROUP BY Fund, LedgerAccount, Suffix");
					}
					else
					{
						rsFundSummaryInfo.OpenRecordset("SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") GROUP BY Fund");
						rsAcctSummaryInfo.OpenRecordset("SELECT Fund, LedgerAccount as Acct, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") GROUP BY Fund, LedgerAccount, Suffix");
					}
				}
				LMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				}
				else
				{
					HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
				}
				if (LMonth > HMonth)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
				FundReport();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Calculating Totals";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				FillInInformation();
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				GetRowSubtotals();
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				GetCompleteTotals();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				ClearValues();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Formatting Report";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				SetColors();
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.RowHeightMax = 220;
                }
				else if (rs.Get_Fields_String("Font") == "L")
				{
                }
				else
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_String("FontName"));
                    vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Bold"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontItalic, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Italic"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontStrikethru, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("StrikeThru"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontUnderline, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Underline"));
					vs1.AutoSize(1, vs1.Cols - 1, false, 300);
					vs1.ExtendLastCol = false;
				}
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				blnRunCollapse = false;
				for (counter = 4; counter >= 0; counter--)
				{
					for (counter2 = 2; counter2 <= vs1.Rows - 1; counter2++)
					{
                        if (vs1.RowOutlineLevel(counter2) == counter)
						{
							if (vs1.IsSubtotal(counter2))
							{
								vs1.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
							}
						}
					}
				}
				blnRunCollapse = true;
				vs1_Collapsed();
			}
			else
			{
				modBudgetaryMaster.Statics.blnFromPreview = false;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Use")) == "P")
			{
				cmdPrint.Enabled = true;
			}
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
            modColorScheme.ColorGrid(vs1);
			this.Refresh();
			vs1.Visible = true;
			lblTitle.Visible = true;
			lblRangeDept.Visible = true;
			lblMonthLabel.Visible = true;
			lblMonths.Visible = true;
		}

		private void frmLedgerDetail_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DeleteTemp();
			frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
		}

		private void frmLedgerDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled == true)
			{
                frmReportViewer.InstancePtr.Init(rptLedgerDetail.InstancePtr);
			}
		}

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled == true)
			{
                modDuplexPrinting.DuplexPrintReport(rptLedgerDetail.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

        private void vs1_Collapsed(object sender, EventArgs e)
        {
            vs1_Collapsed();
        }

        private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool FirstFlag = false;
			bool SecondFlag = false;
			bool ThirdFlag = false;
			bool FourthFlag = false;
			bool FifthFlag;
			int counter2;
            Decimal TempInfo;
			int TempRow = 0;
			string TempArrayInfo = "";
			TempInfo = 0;
			if (blnRunCollapse)
			{
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 0)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FirstFlag = true;
							if (strBalances[counter] == "B0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E0");
								for (counter2 = YTDDebitCol; counter2 <= BalanceCreditCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
										vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
										vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									}
									else
									{
										vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
										vs1.TextMatrix(TempRow, counter2, "");
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							FirstFlag = false;
							if (strBalances[counter] == "E0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B0");
								for (counter2 = YTDDebitCol; counter2 <= BalanceCreditCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
										vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
										vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									}
									else
									{
										vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
										vs1.TextMatrix(TempRow, counter2, "");
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (FirstFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							SecondFlag = true;
							if (strBalances[counter] == "B1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E1");
								if (TempRow != 0)
								{
									for (counter2 = YTDDebitCol; counter2 <= BalanceCreditCol; counter2++)
									{
										if (vs1.TextMatrix(counter, counter2) != "")
										{
											TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
											vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
											vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
										}
										else
										{
											vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
											vs1.TextMatrix(TempRow, counter2, "");
										}
									}
									TempArrayInfo = strBalances[counter];
									strBalances[counter] = strBalances[TempRow];
									strBalances[TempRow] = TempArrayInfo;
								}
							}
						}
						else
						{
							rows += 1;
							SecondFlag = false;
							if (strBalances[counter] == "E1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B1");
								if (TempRow != 0)
								{
									for (counter2 = YTDDebitCol; counter2 <= BalanceCreditCol; counter2++)
									{
										if (vs1.TextMatrix(counter, counter2) != "")
										{
											TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
											vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
											vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
										}
										else
										{
											vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
											vs1.TextMatrix(TempRow, counter2, "");
										}
									}
									TempArrayInfo = strBalances[counter];
									strBalances[counter] = strBalances[TempRow];
									strBalances[TempRow] = TempArrayInfo;
								}
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 2)
					{
						if (FirstFlag == true || SecondFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							ThirdFlag = true;
						}
						else
						{
							rows += 1;
							ThirdFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 3)
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FourthFlag = true;
						}
						else
						{
							rows += 1;
							FourthFlag = false;
						}
					}
					else
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true || FourthFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
            }
		}

		private void FormatGrid()
		{
			int counter = 0;
			//FC:FINAL:BBE:#i721 - merge the cells
			vs1.Cols = 20;
			vs1.MergeRow(0, true);
			//FC:FINAL:BBE:#i721 - add the expand button
			vs1.AddExpandButton();
			vs1.RowHeadersWidth = 10;
			//FC:FINAL:BBE:#595-#596 - correct column size with factor to show it correct on the report
			vs1.UseScaleFactor = true;
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				vs1.TextMatrix(0, 1, "----Dates----");
				vs1.TextMatrix(1, 1, "Posted");
				vs1.TextMatrix(1, 2, "Trans");
				vs1.TextMatrix(1, 3, "Per");
				vs1.TextMatrix(0, 4, "RCB/");
				vs1.TextMatrix(1, 4, "Type");
				vs1.TextMatrix(1, 5, "Jrnl");
				vs1.TextMatrix(1, 6, "Description---");
				vs1.TextMatrix(1, 7, "Wrnt");
				vs1.TextMatrix(1, 8, "Check#");
				vs1.TextMatrix(1, 9, "Vendor------");
				PostedCol = 1;
				TransCol = 2;
				PeriodCol = 3;
				RCBCol = 4;
				JournalCol = 5;
				DescriptionCol = 6;
				WarrantCol = 7;
				CheckCol = 8;
				VendorCol = 9;
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(PostedCol, 800);
				}
				else
				{
					vs1.ColWidth(PostedCol, 1100);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(TransCol, 800);
				}
				else
				{
					vs1.ColWidth(TransCol, 1100);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(PeriodCol, 500);
				}
				else
				{
					vs1.ColWidth(PeriodCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(RCBCol, 500);
				}
				else
				{
					vs1.ColWidth(RCBCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(JournalCol, 700);
				}
				else
				{
					vs1.ColWidth(JournalCol, 800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(DescriptionCol, 2400);
				}
				else
				{
					vs1.ColWidth(DescriptionCol, 2800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(WarrantCol, 500);
				}
				else
				{
					vs1.ColWidth(WarrantCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(CheckCol, 800);
				}
				else
				{
					vs1.ColWidth(CheckCol, 1000);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(VendorCol, 2400);
				}
				else
				{
					vs1.ColWidth(VendorCol, 2600);
				}
				counter = 10;
			}
			else if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowAll")))
				{
					vs1.TextMatrix(0, 1, "Trans");
					vs1.TextMatrix(1, 1, "Date");
					vs1.TextMatrix(1, 2, "Per");
					vs1.TextMatrix(0, 3, "RCB/");
					vs1.TextMatrix(1, 3, "Type");
					vs1.TextMatrix(1, 4, "Jrnl");
					vs1.TextMatrix(1, 5, "Description---");
					vs1.TextMatrix(1, 6, "Wrnt");
					vs1.TextMatrix(1, 7, "Check#");
					vs1.TextMatrix(1, 8, "Vendor------");
					TransCol = 1;
					PeriodCol = 2;
					RCBCol = 3;
					JournalCol = 4;
					DescriptionCol = 5;
					WarrantCol = 6;
					CheckCol = 7;
					VendorCol = 8;
					vs1.ColWidth(TransCol, 700);
					vs1.ColWidth(PeriodCol, 500);
					vs1.ColWidth(RCBCol, 500);
					vs1.ColWidth(JournalCol, 600);
					vs1.ColWidth(DescriptionCol, 2200);
					vs1.ColWidth(WarrantCol, 500);
					vs1.ColWidth(CheckCol, 800);
					vs1.ColWidth(VendorCol, 2200);
					counter = 9;
				}
				else
				{
					vs1.TextMatrix(0, 1, "Account------------");
					vs1.TextMatrix(1, 1, "Date");
					vs1.TextMatrix(1, 2, "Jrnl");
					vs1.TextMatrix(1, 3, "Desc---");
					vs1.TextMatrix(1, 4, "Vendor------");
					TransCol = 1;
					JournalCol = 2;
					DescriptionCol = 3;
					VendorCol = 4;
                    if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
                        vs1.ColWidth(TransCol, 1450);
					}
					else
					{
                        vs1.ColWidth(TransCol, 1450);
					}
					if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
						vs1.ColWidth(JournalCol, 700);
					}
					else
					{
						vs1.ColWidth(JournalCol, 800);
					}
					if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
						vs1.ColWidth(DescriptionCol, 2400);
					}
					else
					{
						vs1.ColWidth(DescriptionCol, 2800);
					}
					if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
						vs1.ColWidth(VendorCol, 2400);
					}
					else
					{
						vs1.ColWidth(VendorCol, 2600);
					}
					counter = 5;
				}
			}
			else
			{
				vs1.TextMatrix(0, 1, "Account------------");
				vs1.TextMatrix(1, 1, "Date");
				vs1.TextMatrix(1, 2, "Jrnl");
				vs1.TextMatrix(1, 3, "Desc---");
				TransCol = 1;
				JournalCol = 2;
				DescriptionCol = 3;
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
                    vs1.ColWidth(TransCol, 1450);
				}
				else
				{
                    vs1.ColWidth(TransCol, 1450);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(JournalCol, 700);
				}
				else
				{
					vs1.ColWidth(JournalCol, 800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(DescriptionCol, 2100);
				}
				else
				{
					vs1.ColWidth(DescriptionCol, 2500);
				}
				counter = 4;
			}
			vs1.TextMatrix(0, counter, "");
			vs1.TextMatrix(0, counter + 1, "");
			vs1.TextMatrix(1, counter, "Debits");
			vs1.TextMatrix(1, counter + 1, "Credits");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1400);
			}
			else
			{
				vs1.ColWidth(counter, 1600);
			}
			vs1.ColFormat(counter, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter + 1, 1400);
			}
			else
			{
				vs1.ColWidth(counter + 1, 1600);
			}
			vs1.ColFormat(counter + 1, "#,##0.00");
            vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            YTDDCFlag = true;
			YTDDebitCol = counter;
			YTDCreditCol = counter + 1;
			vs1.MergeCol(YTDDebitCol, false);
			vs1.MergeCol(YTDCreditCol, false);
			counter += 2;
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("PendingDetail")))
			{
				PendingDetailFlag = true;
				PendingSummaryFlag = false;
			}
			else if (rs.Get_Fields_Boolean("PendingSummary"))
			{
				PendingSummaryFlag = true;
				PendingDetailFlag = false;
			}
			else
			{
				PendingDetailFlag = false;
				PendingSummaryFlag = false;
			}
			vs1.TextMatrix(0, counter, "Balance");
			vs1.TextMatrix(1, counter, "Debit");
			vs1.TextMatrix(0, counter + 1, "Balance");
			vs1.TextMatrix(1, counter + 1, "Credit");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1400);
			}
			else
			{
				vs1.ColWidth(counter, 1600);
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter + 1, 1400);
			}
			else
			{
				vs1.ColWidth(counter + 1, 1600);
			}
			vs1.ColFormat(counter, "#,##0.00");
			vs1.ColFormat(counter + 1, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            BalanceDebitCol = counter;
			BalanceCreditCol = counter + 1;
			counter += 2;
			vs1.Cols = counter;
            if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				for (int i = 0; i <= 9; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				for (int i = 10; i < vs1.Cols - 1; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignRightCenter);
				}
            }
			else if (rs.Get_Fields_Boolean("ShowAll"))
			{
				for (int i = 0; i <= 8; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				for (int i = 9; i < vs1.Cols - 1; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignRightCenter);
				}
            }
			else
			{
				for (int i = 0; i <= 3; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				}
				for (int i = 4; i < vs1.Cols - 1; i++)
				{
					vs1.ColAlignment(i, FCGrid.AlignmentSettings.flexAlignRightCenter);
				}
            }
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, vs1.Cols - 1, true);
		}

		private string Suffix(ref string x)
		{
			string Suffix = "";
			Suffix = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
			return Suffix;
		}

		private string Fund(ref string x)
		{
			string Fund = "";
			Fund = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
			return Fund;
		}

		private string Account(ref string x)
		{
			string Account = "";
			Account = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			return Account;
		}

		private string CalculateMonth(int x)
		{
			string CalculateMonth = "";
			switch (x)
			{
				case 1:
					{
						CalculateMonth = "January";
						break;
					}
				case 2:
					{
						CalculateMonth = "February";
						break;
					}
				case 3:
					{
						CalculateMonth = "March";
						break;
					}
				case 4:
					{
						CalculateMonth = "April";
						break;
					}
				case 5:
					{
						CalculateMonth = "May";
						break;
					}
				case 6:
					{
						CalculateMonth = "June";
						break;
					}
				case 7:
					{
						CalculateMonth = "July";
						break;
					}
				case 8:
					{
						CalculateMonth = "August";
						break;
					}
				case 9:
					{
						CalculateMonth = "September";
						break;
					}
				case 10:
					{
						CalculateMonth = "October";
						break;
					}
				case 11:
					{
						CalculateMonth = "November";
						break;
					}
				case 12:
					{
						CalculateMonth = "December";
						break;
					}
			}
            return CalculateMonth;
		}

		private void FundReport()
		{
			clsDRWrapper LedgerDescriptions = new clsDRWrapper();
			clsDRWrapper rsRanges = new clsDRWrapper();
			bool blnDescFound = false;
			LedgerDescriptions.OpenRecordset("SELECT * FROM LedgerTitles");
			theReport = new cLedgerDetailReport();
			theReport.Details.ClearList();
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowLiquidatedEncumbranceActivity")))
			{
				theReport.ShowLiquidatedEncumbranceActivity = true;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("pendingdetail")) || FCConvert.ToBoolean(rs.Get_Fields_Boolean("pendingSummary")))
			{
				theReport.ShowPendingActivity = true;
			}
			if (Strings.LCase(FCConvert.ToString(rs.Get_Fields_String("PAPERWIDTH"))) == "l" || Strings.LCase(FCConvert.ToString(rs.Get_Fields_String("paperwidth"))) == "w" || rs.Get_Fields_Boolean("showall") == true)
			{
				theReport.ShowJournalDetail = true;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				rs2.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "'");
				if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
				{
					rs2.MoveLast();
					rs2.MoveFirst();
					vs1.Rows += 1;
                    vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
					if (LedgerDescriptions.FindFirstRecord2("Fund, Account", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
					{
						if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + "UNKNOWN");
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 0);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX/*0x80000009*/);
					vs1.IsSubtotal(vs1.Rows - 1, true);
					rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField, ThirdAccountField");
					if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
					{
						rs2.MoveLast();
						rs2.MoveFirst();
						while (rs2.EndOfFile() != true)
						{
							//Application.DoEvents();
							vs1.Rows += 1;
							//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
							vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
							if (!modAccountTitle.Statics.YearFlag)
							{
								blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
							}
							else
							{
								blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
							}
							if (blnDescFound)
							{
								if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
								}
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
							}
							vs1.RowOutlineLevel(vs1.Rows - 1, 1);
							vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
							if (modAccountTitle.Statics.YearFlag)
							{
								CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rs2.Get_Fields_String("SecondAccountField"), "", ""));
							}
							else
							{
								CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rs2.Get_Fields_String("SecondAccountField"), rs2.Get_Fields_String("ThirdAccountField"), ""));
							}
							rs2.MoveNext();
						}
					}
					vs1.Rows += 1;
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Fund........");
					vs1.IsSubtotal(vs1.Rows - 1, true);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				// more than 1 department
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND FirstAccountField >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND FirstAccountField <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
                        vs1.Rows += 1;
                        vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX/*0x80000009*/);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField, ThirdAccountField");
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
                                vs1.Rows += 1;
                                vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
								if (modAccountTitle.Statics.YearFlag)
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), "", ""));
								}
								else
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs2.Get_Fields_String("ThirdAccountField"), ""));
								}
								rs2.MoveNext();
							}
						}
						rs5.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Fund........");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "A")
			{
				// All Departments
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
                        vs1.Rows += 1;
                        vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX/*0x80000009*/);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField, ThirdAccountField");
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
                                vs1.Rows += 1;
                                vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
								if (modAccountTitle.Statics.YearFlag)
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), "", ""));
								}
								else
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs2.Get_Fields_String("ThirdAccountField"), ""));
								}
								rs2.MoveNext();
							}
						}
						rs5.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Fund........");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
					}
				}
			}
			else
			{
				// range of accounts
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						//Application.DoEvents();
						vs1.Rows += 1;
						//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
						vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX/*0x80000009*/);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField, ThirdAccountField");
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
								//Application.DoEvents();
								vs1.Rows += 1;
								//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
								vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
								if (modAccountTitle.Statics.YearFlag)
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), "", ""));
								}
								else
								{
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("G", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs2.Get_Fields_String("ThirdAccountField"), ""));
								}
								rs2.MoveNext();
							}
						}
						rs5.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Fund........");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT/*0x8000000F*/);
					}
				}
			}
		}

		private void PrepareTemp()
		{
			string strSQL1;
			string strSQL2;
			string strSQL3;
			string strSQL4 = "";
			string strSQL5 = "";
			string strSQL6;
			string strTotalSQL;
			string strTotalSQL2;
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strSQLFields;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			DeleteTemp();
			strSQL1 = "AccountType, Account, ";
			strSQL2 = "substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) AS FirstAccountField, ";
			strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) AS SecondAccountField, ";
			strSQLFields = "AccountType, Account, FirstAccountField, SecondAccountField";
			if (!modAccountTitle.Statics.YearFlag)
			{
				strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 5, 2))) AS ThirdAccountField, ";
				strSQL5 = "";
				strSQLFields += ", ThirdAccountField";
			}
			else
			{
				strSQL4 = "";
				strSQL5 = "";
			}
			strSQL6 = "left(Account, 1) AS AccountType, Account, ";
			strTotalSQL = "SELECT DISTINCT " + strSQL1 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL = Strings.Left(strTotalSQL, strTotalSQL.Length - 2);
			strTotalSQL2 = "SELECT DISTINCT " + strSQL6 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL2 = Strings.Left(strTotalSQL2, strTotalSQL2.Length - 2);
			if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowZeroBalance")))
			{
                rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'G' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'G' AND Valid = 1 ) as LedgerAccounts", "Budgetary");
			}
			else
			{
                rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'G' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'G' AND CurrentBudget <> 0 ) as LedgerAccounts", "Budgetary");
			}
            if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				rs3.Execute("UPDATE (SELECT * FROM Temp INNER JOIN AccountMaster ON Temp.Account = AccountMaster.Account) SET Temp.GLAccountType = AccountMaster.GLAccountType", "Budgetary");
			}
			strPeriodCheck = strPeriodCheckHolder;
		}

		private void DeleteTemp()
		{
			rs3.Execute("DELETE FROM Temp", "Budgetary");
		}

		private void FillInInformation()
		{
			double temp = 0;
			int lngCounter;
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Refresh();
            lngCounter = 2;
			for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
			{
                if (CurrentRow >= lngCounter + 20)
				{
					lngCounter = CurrentRow;
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CurrentRow) / (vs1.Rows - 1)) * 100);
					frmWait.InstancePtr.Refresh();
				}
				if (!IsTotalRow(CurrentRow) && !IsDetailRow(CurrentRow))
				{
					if (vs1.RowOutlineLevel(CurrentRow) == 0)
					{
						currentFund = Strings.Left(vs1.TextMatrix(CurrentRow, 1), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
						currentAccount = "";
						CurrentSuffix = "";
					}
					else if (Strings.Left(vs1.TextMatrix(CurrentRow, 1), 3) == "   ")
					{
						currentAccount = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
						CurrentSuffix = Strings.Mid(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), 2 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
					}

                    if (currentAccount == "570" && CurrentSuffix == "28")
                    {
                        temp = temp;
                    }
                        temp = GetBeginningBalance(ref currentFund, ref currentAccount, ref CurrentSuffix) + GetYTDNet();
					if (temp < 0)
					{
						vs1.TextMatrix(CurrentRow, BalanceCreditCol, Strings.Format(temp * -1, "#,##0.00"));
						vs1.TextMatrix(CurrentRow, BalanceDebitCol, "");
					}
					else
					{
						vs1.TextMatrix(CurrentRow, BalanceDebitCol, Strings.Format(temp, "#,##0.00"));
						vs1.TextMatrix(CurrentRow, BalanceCreditCol, "");
					}
					if (YTDDCFlag)
					{
						vs1.TextMatrix(CurrentRow, YTDDebitCol, "");
						vs1.TextMatrix(CurrentRow, YTDCreditCol, "");
					}
				}
			}
		}

		private short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		private short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private double GetEncumbrance()
		{
			double GetEncumbrance = 0;
			int temp;
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.EndOfFile() != true && rsFundSummaryInfo.BeginningOfFile() != true)
				{
					if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetEncumbrance = FCConvert.ToDouble(rsFundSummaryInfo.Get_Fields("EncumbActivityTotal"));
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetEncumbrance = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetEncumbrance = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
			}
			return GetEncumbrance;
		}

		private double GetPending()
		{
			double GetPending = 0;
			GetPending = GetPendingDebits() - GetPendingCredits();
			return GetPending;
		}

		private double GetPendingCredits()
		{
			double GetPendingCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				return GetPendingCredits;
			}
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.EndOfFile() != true && rsFundSummaryInfo.BeginningOfFile() != true)
				{
					if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetPendingCredits = FCConvert.ToDouble(rsFundSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					GetPendingCredits = 0;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetPendingCredits = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetPendingCredits = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			return GetPendingCredits;
		}

		private double GetPendingDebits()
		{
			double GetPendingDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				return GetPendingDebits;
			}
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.EndOfFile() != true && rsFundSummaryInfo.BeginningOfFile() != true)
				{
					if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetPendingDebits = FCConvert.ToDouble(rsFundSummaryInfo.Get_Fields("PendingDebitsTotal"));
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					GetPendingDebits = 0;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetPendingDebits = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetPendingDebits = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			return GetPendingDebits;
		}

		private void GetCompleteTotals()
		{
			int counter;
			int counter2;
			double total = 0;
			vs1.Rows += 1;
			//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
			vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
			vs1.TextMatrix(vs1.Rows - 1, 1, "Final Totals");
			for (counter = YTDDebitCol; counter <= vs1.Cols - 1; counter++)
			{
				total = 0;
				for (counter2 = 2; counter2 <= vs1.Rows - 2; counter2++)
				{
					if (IsTotalRow(counter2) && vs1.RowOutlineLevel(counter2) == 1)
					{
						if (vs1.TextMatrix(counter2, counter) != "")
						{
							total += FCConvert.ToDouble(vs1.TextMatrix(counter2, counter));
						}
					}
				}
				vs1.TextMatrix(vs1.Rows - 1, counter, FCConvert.ToString(total));
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.Blue);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			vs1.RowOutlineLevel(vs1.Rows - 1, 0);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void SetColors()
		{
			modColorScheme.ColorGrid(vs1, 2, -1, 0, -1, true);
		}

		private void CreateDetailInfo_2(string strAccount)
		{
			CreateDetailInfo(ref strAccount);
		}

		private void CreateDetailInfo(ref string strAccount)
		{
   
			clsDRWrapper rsAPJournal = new clsDRWrapper();
			clsDRWrapper rsJournalEntries = new clsDRWrapper();
			clsDRWrapper rsEncumbranceJournal = new clsDRWrapper();
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int HighDate = 0;
			int LowDate;
            Decimal curDebitTotal;
            Decimal curCreditTotal;
            Decimal curNetTotal;
			double dblPendingTotal = 0;
			bool blnRecordsFound;
            Decimal curDebitTotalPeriod = 0.0M;
            Decimal curCreditTotalPeriod = 0.0M;
			int intPeriod;
            Decimal curPeriodBalance;
			cLedgerDetailItem glItem = new cLedgerDetailItem();
            cDetailsReport detReport;
			string strCurrentFund = "";
			Decimal curBeginBalance;
            Decimal curTemp;
			detReport = theReport;
			intPeriod = -1;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
			}
			else
			{
				HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				if (HighDate == 1)
				{
					HighDate = 12;
				}
				else
				{
					HighDate -= 1;
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) != "A")
			{
				LowDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else
			{
				LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			}
			blnRecordsFound = false;
			curDebitTotal = 0;
			curCreditTotal = 0;
			curNetTotal = 0;
			currentFund = Fund(ref strAccount);
			currentAccount = Account(ref strAccount);
			CurrentSuffix = Suffix(ref strAccount);
			curPeriodBalance = FCConvert.ToDecimal(GetBeginningBalance(ref currentFund, ref currentAccount, ref CurrentSuffix) + GetYTDNet());
			curBeginBalance = curPeriodBalance;
			strAccount = Strings.Left(strAccount, strAccount.Length - 1);
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				vs1.IsSubtotal(vs1.Rows - 1, true);
				CheckAgain:
				;
                if (FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) == strAccount)
				{

					do
					{
                        if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "E" && rsDetailInfo.Get_Fields_Decimal("Amount") == 0)
						{
							goto CheckNext;
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
						{
							if (intPeriod == -1)
							{
                                intPeriod = FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period"));
							}
							else
							{
                                if (intPeriod != FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period")))
								{
									vs1.Rows += 1;
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, MonthCalc(intPeriod));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotalPeriod, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotalPeriod * -1, "#,##0.00"));
									curPeriodBalance += curDebitTotalPeriod + curCreditTotalPeriod;
									if (curPeriodBalance < 0)
									{
										vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, Strings.Format(curPeriodBalance * -1, "#,##0.00"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, Strings.Format(curPeriodBalance, "#,##0.00"));
									}
									curDebitTotalPeriod = 0;
									curCreditTotalPeriod = 0;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
                                    intPeriod = FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period"));
								}
							}
						}
						blnRecordsFound = true;
						vs1.Rows += 1;
						glItem = new cLedgerDetailItem();
                        glItem.Account = FCConvert.ToString(rsDetailInfo.Get_Fields("Account"));
                        glItem.JournalNumber = FCConvert.ToInt16(rsDetailInfo.Get_Fields("journalnumber"));
                       
						glItem.Description = FCConvert.ToString(rsDetailInfo.Get_Fields_String("description"));
						glItem.Fund = currentFund;
						glItem.AccountObject = currentAccount;
						glItem.Suffix = CurrentSuffix;
                        glItem.Warrant = FCConvert.ToString(rsDetailInfo.Get_Fields("warrant"));
                        glItem.JournalType = FCConvert.ToString(rsDetailInfo.Get_Fields("Type"));
						glItem.RCB = FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB"));
                        glItem.CheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDetailInfo.Get_Fields("CheckNumber"))));
                        glItem.Period = FCConvert.ToInt16(Math.Round(Conversion.Val(rsDetailInfo.Get_Fields("period"))));
						glItem.TransactionDate = Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
						if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("status")) == "P")
						{
							glItem.JournalDate = Strings.Format(rsDetailInfo.Get_Fields_DateTime("Posteddate"), "MM/dd/yyyy");
						}
                        if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
                                vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "A " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
                                vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
                                vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
									}
									glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "A " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowAll")))
									{
                                        vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
										vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
                                        vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
                                        vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
											}
											glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
										}
									}
									else
									{
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
											}
											glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
										}
									}
								}
							}
							if (rs.Get_Fields_Boolean("ShowLiquidatedEncumbranceActivity"))
							{
								// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
                                if (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields("Discount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
                                    curTemp = FCConvert.ToDecimal((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount")));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curTemp, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00"));
									glItem.Debit = FCConvert.ToDouble(curTemp);
									glItem.Credit = rsDetailInfo.Get_Fields_Decimal("encumbrance").ToDouble();
                                    curDebitTotal += rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount");
									curCreditTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                    curDebitTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount");
									curCreditTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
									// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
								}
                                else if (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount")), "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1, "#,##0.00"));
                                    curTemp = FCConvert.ToDecimal(((rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount"))));
									curDebitTotal += curTemp;
									curCreditTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
									glItem.Debit = FCConvert.ToDouble(curTemp);
									glItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("encumbrance")) * -1;
                                    curDebitTotalPeriod += ((rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount")));
									curCreditTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
									// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
								}
                                else if (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    curTemp = (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount")) * -1;
									glItem.Credit = curTemp.ToDouble();
									glItem.Debit = rsDetailInfo.Get_Fields_Decimal("encumbrance").ToDouble() * -1;
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curTemp, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1, "#,##0.00"));
                                    curCreditTotal += (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount"));
									curDebitTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                    curCreditTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount"));
									curDebitTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
									// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
								}
								else
								{
                                    curTemp = ((rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount")) * -1);
									glItem.Credit = FCConvert.ToDouble(curTemp);
									glItem.Debit = FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("encumbrance")) * -1;
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curTemp, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1, "#,##0.00"));
                                    curCreditTotal += rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount");
									curDebitTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                    curCreditTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount");
									curDebitTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
								}
							}
							else
							{
                                curTemp = (rsDetailInfo.Get_Fields_Decimal("Amount") - rsDetailInfo.Get_Fields_Decimal("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
								// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
								if (curTemp > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curTemp, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
									glItem.Debit = FCConvert.ToDouble(curTemp);
									glItem.Credit = 0;
									curDebitTotal += curTemp;
									curDebitTotalPeriod += curTemp;
									// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
								}
								else if (curTemp < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curTemp, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
									glItem.Debit = FCConvert.ToDouble(curTemp);
									glItem.Credit = 0;
									curDebitTotal += curTemp;
									curDebitTotalPeriod += curTemp;
									// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
								}
								else if (curTemp > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curTemp * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
									glItem.Credit = FCConvert.ToDouble(curTemp * -1);
									glItem.Debit = 0;
									curCreditTotal += curTemp;
									curCreditTotalPeriod += curTemp;
									// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curTemp * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
									glItem.Credit = FCConvert.ToDouble(curTemp * -1);
									glItem.Debit = 0;
									curCreditTotal += curTemp;
									curCreditTotalPeriod += curTemp;
								}
							}
						}
                        else if (rsDetailInfo.Get_Fields("Type") == "E")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
                                vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, "E");
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "E " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
									}
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "E " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowAll")))
									{
                                        vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
										vs1.TextMatrix(vs1.Rows - 1, RCBCol, "E");
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
											}
											glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
										}
									}
									else
									{
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
											}
											glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
										}
									}
								}
							}
                            if (rsDetailInfo.Get_Fields_Decimal("Amount") > 0)
							{
                                vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Double("Amount"), "#,##0.00"));
								vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
                                glItem.Debit = rsDetailInfo.Get_Fields_Decimal("amount").ToDouble();
								glItem.Credit = 0;
                                curDebitTotal += rsDetailInfo.Get_Fields_Decimal("Amount");
                                curDebitTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount");
							}
							else
							{
                                vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount") * -1, "#,##0.00"));
								vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
                                glItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("amount")) * -1;
								glItem.Debit = 0;
                                curCreditTotal += rsDetailInfo.Get_Fields("Amount");
                                curCreditTotalPeriod += rsDetailInfo.Get_Fields("Amount");
							}
						}
						else
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
                                vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, rsDetailInfo.Get_Fields("Type") + " " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
                                if (Conversion.Val(rsDetailInfo.Get_Fields("Warrant")) == 0)
								{
									vs1.TextMatrix(vs1.Rows - 1, WarrantCol, "");
								}
								else
								{
                                    vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
								}
                                vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									glItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, rsDetailInfo.Get_Fields("Type") + " " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowAll")))
									{
                                        vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
										vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
                                        if (Conversion.Val(rsDetailInfo.Get_Fields("Warrant")) == 0)
										{
											vs1.TextMatrix(vs1.Rows - 1, WarrantCol, "");
										}
										else
										{
                                            vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
										}
                                        vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
										}
									}
									else
									{
										if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
										{
											rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
											if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
											}
										}
									}
								}
							}
							// if the amount is more than 0 and it is a cash disbursement or a general journal entry with an RCB of anything but c it is a debit
                            if (rsDetailInfo.Get_Fields_Decimal("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount"), "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
                                    glItem.BalanceDebit = FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("amount"));
								}
								else
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount"), "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
									vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
									vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
								}
                                curDebitTotal += rsDetailInfo.Get_Fields_Decimal("Amount");
                                curDebitTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount");
								// if the amount is less than 0 and it is a general journal entry with an RCB of C then it is a debit
							}
                            else if (rsDetailInfo.Get_Fields_Decimal("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
							{
                                vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount"), "#,##0.00"));
								vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
								vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
								vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
                                curDebitTotal += (rsDetailInfo.Get_Fields_Decimal("Amount"));
                                curDebitTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Amount"));
								// if the amount is more than 0 and it is a cash receipt or a general journal entry with an RCB of C than it is a Credit
							}
                            else if (rsDetailInfo.Get_Fields_Decimal("Amount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
							{
                                vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount") * -1, "#,##0.00"));
								vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
								vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
								vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
                                curCreditTotal += rsDetailInfo.Get_Fields_Decimal("Amount");
                                curCreditTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount");
								// if the amount is less than 0 and it is a cash disbursement journal or a general journal entry with an RCB of anything but C then it is a Credit
							}
                            else if (rsDetailInfo.Get_Fields_Decimal("Amount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount") * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
                                    glItem.BalanceCredit = FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("amount")) * -1;
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
								}
								else
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Amount") * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
									vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
									vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
								}
                                curCreditTotal += rsDetailInfo.Get_Fields_Decimal("Amount");
                                curCreditTotalPeriod += rsDetailInfo.Get_Fields_Decimal("Amount");
							}
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
						detReport.Details.AddItem(glItem);
						CheckNext:
						;
						rsDetailInfo.MoveNext();
						if (rsDetailInfo.EndOfFile())
						{
							break;
						}
                    }
					while (rsDetailInfo.Get_Fields_String("Account") == strAccount);
				}
                else if (String.Compare(rsDetailInfo.Get_Fields_String("Account"), strAccount) < 0)
				{
					rsDetailInfo.MoveNext();
					if (rsDetailInfo.EndOfFile() != true)
					{
						goto CheckAgain;
					}
					else
					{
						goto NoDetailFound;
					}
				}
			}
			else
			{
				if (PendingSummaryFlag)
				{
					vs1.IsSubtotal(vs1.Rows - 1, true);
				}
			}
			NoDetailFound:
			;
			if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
			{
				if (intPeriod != -1)
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, MonthCalc(intPeriod));
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotalPeriod, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotalPeriod * -1, "#,##0.00"));
					curPeriodBalance += curDebitTotalPeriod + curCreditTotalPeriod;
					if (curPeriodBalance < 0)
					{
						vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, Strings.Format(curPeriodBalance * -1, "#,##0.00"));
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, Strings.Format(curPeriodBalance, "#,##0.00"));
					}
					curDebitTotalPeriod = 0;
					curCreditTotalPeriod = 0;
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				}
			}
			if (PendingSummaryFlag)
			{
				CurrentRow = vs1.Rows - 1;
				dblPendingTotal = GetPending();
				if (dblPendingTotal != 0)
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Pending Activity");
					if (dblPendingTotal > 0)
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(dblPendingTotal, "#,##0.00"));
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
						curDebitTotal += FCConvert.ToDecimal(dblPendingTotal);
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(dblPendingTotal * -1, "#,##0.00"));
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
						curCreditTotal += FCConvert.ToDecimal(dblPendingTotal);
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				}
			}
			if (!(glItem == null))
			{
				glItem.BeginningNet = FCConvert.ToDouble(curBeginBalance);
				glItem.NetBalance = FCConvert.ToDouble(curBeginBalance + curDebitTotal + curCreditTotal);
			}
			if (blnRecordsFound || PendingSummaryFlag)
			{
				vs1.Rows += 1;
				vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Account.....");
				if (curDebitTotal != 0 && curCreditTotal != 0)
				{
					if (curDebitTotal + curCreditTotal < 0)
					{
						vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, FCConvert.ToString((curDebitTotal + curCreditTotal) * -1));
						vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, FCConvert.ToString(curDebitTotal + curCreditTotal));
						vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
					}
				}
				else if (curDebitTotal != 0)
				{
					vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, Strings.Format(curDebitTotal, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
				}
				else if (curCreditTotal != 0)
				{
					vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, Strings.Format(curCreditTotal * -1, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, BalanceDebitCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, BalanceCreditCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, "");
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, "");
				}
				vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				vs1.Rows += 1;
				vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			}
		}

		public bool IsTotalRow(int lngRow)
		{
			bool IsTotalRow = false;
			if (vs1.TextMatrix(lngRow, DescriptionCol) == "Account....." || vs1.TextMatrix(lngRow, DescriptionCol) == "Fund........")
			{
				IsTotalRow = true;
			}
			else
			{
				IsTotalRow = false;
			}
			return IsTotalRow;
		}

		public bool IsTotalRowToBold(int lngRow)
		{
			bool IsTotalRowToBold = false;
			if (vs1.TextMatrix(lngRow, DescriptionCol) == "Account....." || vs1.TextMatrix(lngRow, DescriptionCol) == "Fund........" || vs1.TextMatrix(lngRow, DescriptionCol) == "January" || vs1.TextMatrix(lngRow, DescriptionCol) == "February" || vs1.TextMatrix(lngRow, DescriptionCol) == "March" || vs1.TextMatrix(lngRow, DescriptionCol) == "April" || vs1.TextMatrix(lngRow, DescriptionCol) == "May" || vs1.TextMatrix(lngRow, DescriptionCol) == "June" || vs1.TextMatrix(lngRow, DescriptionCol) == "July" || vs1.TextMatrix(lngRow, DescriptionCol) == "August" || vs1.TextMatrix(lngRow, DescriptionCol) == "September" || vs1.TextMatrix(lngRow, DescriptionCol) == "October" || vs1.TextMatrix(lngRow, DescriptionCol) == "November" || vs1.TextMatrix(lngRow, DescriptionCol) == "December")
			{
				IsTotalRowToBold = true;
			}
			else
			{
				IsTotalRowToBold = false;
			}
			return IsTotalRowToBold;
		}

		public bool IsDetailRow(int lngRow)
		{
			bool IsDetailRow = false;
			if (vs1.RowOutlineLevel(lngRow) == 2)
			{
				IsDetailRow = true;
			}
			else
			{
				IsDetailRow = false;
			}
			return IsDetailRow;
		}

		private void GetRowSubtotals()
		{
            Decimal[,] curTotals = new Decimal[3 + 1, 3 + 1];
			int counter;
			int counter2;
			int temp = 0;
			double TempAmount = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				for (counter2 = 0; counter2 <= 3; counter2++)
				{
					curTotals[counter, counter2] = 0;
				}
			}
			strBalances = new string[vs1.Rows + 1 + 1];
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (!IsTotalRow(counter) && !IsDetailRow(counter))
				{
					strBalances[counter] = "B" + FCConvert.ToString(vs1.RowOutlineLevel(counter));
				}
				else if (IsTotalRow(counter))
				{
					strBalances[counter] = "E" + FCConvert.ToString(vs1.RowOutlineLevel(counter) - 1);
				}
				else
				{
					strBalances[counter] = "";
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsTotalRow(counter) && IsDetailRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					if (vs1.TextMatrix(temp, BalanceDebitCol) != "")
					{
						TempAmount = vs1.TextMatrix(temp, BalanceDebitCol).ToDoubleValue();
					}
					else
					{
						TempAmount = vs1.TextMatrix(temp, BalanceCreditCol).ToDoubleValue() * -1;
					}
					if (vs1.TextMatrix(counter, BalanceDebitCol) != "")
					{
						if ((TempAmount + vs1.TextMatrix(counter, BalanceDebitCol).ToDoubleValue()) < 0)
						{
							vs1.TextMatrix(counter, BalanceCreditCol, FCConvert.ToString((TempAmount + vs1.TextMatrix(counter, BalanceDebitCol).ToDoubleValue()) * -1));
							vs1.TextMatrix(counter, BalanceDebitCol, "");
						}
						else
						{
							vs1.TextMatrix(counter, BalanceDebitCol, FCConvert.ToString(TempAmount + vs1.TextMatrix(counter, BalanceDebitCol).ToDoubleValue()));
							vs1.TextMatrix(counter, BalanceCreditCol, "");
						}
					}
					else
					{
						if (TempAmount - vs1.TextMatrix(counter, BalanceCreditCol).ToDoubleValue() < 0)
						{
							vs1.TextMatrix(counter, BalanceCreditCol, FCConvert.ToString((TempAmount - vs1.TextMatrix(counter, BalanceCreditCol).ToDoubleValue()) * -1));
							vs1.TextMatrix(counter, BalanceDebitCol, "");
						}
						else
						{
							vs1.TextMatrix(counter, BalanceDebitCol, FCConvert.ToString(TempAmount - vs1.TextMatrix(counter, BalanceCreditCol).ToDoubleValue()));
							vs1.TextMatrix(counter, BalanceCreditCol, "");
						}
					}
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if ((IsTotalRow(counter) && IsDetailRow(counter)) || IsTotalWithNoDetail(ref counter))
				{
					if (IsTotalWithNoDetail(ref counter))
					{
                        // Dave 3/11/04 This was not getting the correct total because it was not taking the total rows with no detail into account so I uncommented it
                        for (counter2 = BalanceDebitCol; counter2 <= BalanceCreditCol; counter2++)
                        {
                            if (vs1.TextMatrix(counter, counter2) != "")
                            {
                                curTotals[vs1.RowOutlineLevel(counter), counter2 - YTDDebitCol] += vs1.TextMatrix(counter, counter2).ToDecimalValue();
                            }
                        }
                    }
					else
					{
						for (counter2 = BalanceDebitCol; counter2 <= BalanceCreditCol; counter2++)
						{
							if (vs1.TextMatrix(counter, counter2) != "")
							{
								curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - YTDDebitCol] += vs1.TextMatrix(counter, counter2).ToDecimalValue();
							}
						}
					}
				}
				else if (IsTotalRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					for (counter2 = BalanceDebitCol; counter2 <= BalanceCreditCol; counter2++)
					{
                        vs1.TextMatrix(counter, counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter), counter2 - YTDDebitCol]));
						curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - YTDDebitCol] += curTotals[vs1.RowOutlineLevel(counter), counter2 - YTDDebitCol];
						curTotals[vs1.RowOutlineLevel(counter), counter2 - YTDDebitCol] = 0;
					}
				}
			}
		}

		private int FindBeginningBalanceRow(ref int lngRow)
		{
			int FindBeginningBalanceRow = 0;
			int tempLevel;
			int counter;
			tempLevel = vs1.RowOutlineLevel(lngRow) - 1;
			for (counter = lngRow - 1; counter >= 2; counter--)
			{
				if (vs1.RowOutlineLevel(counter) == tempLevel)
				{
					FindBeginningBalanceRow = counter;
					break;
				}
			}
			return FindBeginningBalanceRow;
		}

		private int FindEndingBalanceRow_6(int lngRow, string strBalance)
		{
			return FindEndingBalanceRow(lngRow, ref strBalance);
		}

		private int FindEndingBalanceRow(int lngRow, ref string strBalance)
		{
			int FindEndingBalanceRow = 0;
			string tempLevel;
			int counter;
			tempLevel = strBalance;
			for (counter = lngRow + 1; counter <= vs1.Rows - 1; counter++)
			{
				if (strBalances[counter] == tempLevel)
				{
					FindEndingBalanceRow = counter;
					break;
				}
			}
			return FindEndingBalanceRow;
		}

		private double GetBeginningBalance(ref string strFund, ref string strAccount, ref string strSuffix)
		{
			double GetBeginningBalance = 0;
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
				{
					if (rsFundBudgetInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetBeginningBalance = (rsFundBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal") + rsFundSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal")).ToDouble();
					}
					else
					{
                        GetBeginningBalance = rsFundSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal").ToDouble();
					}
				}
				else
				{
					if (rsFundBudgetInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetBeginningBalance = rsFundBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal").ToDouble();
					}
					else
					{
						GetBeginningBalance = 0;
					}
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
					{
						if (rsAcctBudgetInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetBeginningBalance = (rsAcctBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal") + rsAcctSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal")).ToDouble();
						}
						else
						{
                            GetBeginningBalance = rsAcctSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal").ToDouble();
						}
					}
					else
					{
						if (rsAcctBudgetInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetBeginningBalance = rsAcctBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal").ToDouble();
						}
						else
						{
							GetBeginningBalance = 0;
						}
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetBeginningBalance = (rsAcctSummaryInfo.Get_Fields_Decimal("OriginalBudgetTotal") + rsAcctSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal")).ToDouble();
						}
						else
						{
							GetBeginningBalance = 0;
						}
					}
					else
					{
						GetBeginningBalance = 0;
					}
					if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
					{
						if (rsAcctBudgetInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetBeginningBalance = (rsAcctBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal") + rsAcctSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal")).ToDouble();
						}
						else
						{
                            GetBeginningBalance = rsAcctSummaryInfo.Get_Fields_Decimal("BudgetAdjustmentsTotal").ToDouble();
						}
					}
					else
					{
						if (rsAcctBudgetInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetBeginningBalance = rsAcctBudgetInfo.Get_Fields_Decimal("OriginalBudgetTotal").ToDouble();
						}
						else
						{
							GetBeginningBalance = 0;
						}
					}
				}
			}
			return GetBeginningBalance;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			int temp;
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.EndOfFile() != true && rsFundSummaryInfo.BeginningOfFile() != true)
				{
					if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetYTDDebit = FCConvert.ToDouble(rsFundSummaryInfo.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetYTDDebit = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetYTDDebit = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			GetYTDDebit += GetEncumbrance();
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			int temp;
			if (currentAccount == "")
			{
				if (rsFundSummaryInfo.EndOfFile() != true && rsFundSummaryInfo.BeginningOfFile() != true)
				{
					if (rsFundSummaryInfo.FindFirstRecord("Fund", currentFund))
					{
                        GetYTDCredit = FCConvert.ToDouble(rsFundSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.YearFlag)
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
                            GetYTDCredit = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					if (rsAcctSummaryInfo.EndOfFile() != true && rsAcctSummaryInfo.BeginningOfFile() != true)
					{
						if (rsAcctSummaryInfo.FindFirstRecord2("Fund, Acct", currentFund + "," + currentAccount, ","))
						{
                            GetYTDCredit = FCConvert.ToDouble(rsAcctSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() - GetYTDCredit();
			return GetYTDNet;
		}

		private void ClearValues()
		{
			int counter;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsDetailRow(counter) && !IsTotalRow(counter))
				{
					if (!IsTotalRowToBold(counter))
					{
						if (vs1.TextMatrix(counter, YTDDebitCol) != "" || vs1.TextMatrix(counter, YTDCreditCol) != "")
						{
							vs1.TextMatrix(counter, BalanceDebitCol, "");
							vs1.TextMatrix(counter, BalanceCreditCol, "");
							if (vs1.TextMatrix(counter, YTDDebitCol) != "" && vs1.TextMatrix(counter, YTDCreditCol) != "" && (vs1.TextMatrix(counter, YTDDebitCol) == "0.00" || vs1.TextMatrix(counter, YTDCreditCol) == "0.00"))
							{
								if (FCConvert.ToDouble(vs1.TextMatrix(counter, YTDDebitCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, YTDCreditCol)) > 0)
								{
									vs1.TextMatrix(counter, YTDDebitCol, (vs1.TextMatrix(counter, YTDDebitCol).ToDoubleValue() - vs1.TextMatrix(counter, YTDCreditCol).ToDoubleValue()));
									vs1.TextMatrix(counter, YTDCreditCol, "");
								}
								else
								{
									vs1.TextMatrix(counter, YTDCreditCol, (vs1.TextMatrix(counter, YTDDebitCol).ToDoubleValue() - vs1.TextMatrix(counter, YTDCreditCol).ToDoubleValue()) * -1);
									vs1.TextMatrix(counter, YTDDebitCol, "");
								}
							}
						}
					}
				}
				else
				{
					vs1.TextMatrix(counter, YTDDebitCol, "");
					vs1.TextMatrix(counter, YTDCreditCol, "");
					if (vs1.TextMatrix(counter, BalanceDebitCol) != "" && vs1.TextMatrix(counter, BalanceCreditCol) != "")
					{
						if (FCConvert.ToDouble(vs1.TextMatrix(counter, BalanceDebitCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, BalanceCreditCol)) > 0)
						{
							vs1.TextMatrix(counter, BalanceDebitCol, FCConvert.ToString(FCConvert.ToDouble(vs1.TextMatrix(counter, BalanceDebitCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, BalanceCreditCol))));
							vs1.TextMatrix(counter, BalanceCreditCol, "");
						}
						else
						{
							vs1.TextMatrix(counter, BalanceCreditCol, (vs1.TextMatrix(counter, BalanceDebitCol).ToDoubleValue() - vs1.TextMatrix(counter, BalanceCreditCol).ToDoubleValue()) * -1);
							vs1.TextMatrix(counter, BalanceDebitCol, "");
						}
					}
				}
			}
		}

		private bool IsTotalWithNoDetail(ref int lngRow)
		{
			bool IsTotalWithNoDetail = false;
			IsTotalWithNoDetail = false;
			if (vs1.RowOutlineLevel(lngRow) == 1 && !IsTotalRow(lngRow))
			{
				if (lngRow == vs1.Rows - 1)
				{
					IsTotalWithNoDetail = true;
				}
				else if (lngRow < vs1.Rows - 1)
				{
					if (vs1.RowOutlineLevel(lngRow + 1) <= 1)
					{
						IsTotalWithNoDetail = true;
					}
				}
			}
			return IsTotalWithNoDetail;
		}
	}
}
