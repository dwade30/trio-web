﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevenueDetail.
	/// </summary>
	public partial class frmRevenueDetail : BaseForm
	{
		public frmRevenueDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRevenueDetail InstancePtr
		{
			get
			{
				return (frmRevenueDetail)Sys.GetInstance(typeof(frmRevenueDetail));
			}
		}

		protected frmRevenueDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		clsDRWrapper rs4 = new clsDRWrapper();
		clsDRWrapper rs5 = new clsDRWrapper();
		clsDRWrapper rs6 = new clsDRWrapper();
		clsDRWrapper rs7 = new clsDRWrapper();
		clsDRWrapper UsedAccounts = new clsDRWrapper();
		bool NetBudgetFlag;
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool BalanceFlag;
		bool PendingDetailFlag;
		bool PendingSummaryFlag;
		int CurrentRow;
		string CurrentDepartment = "";
		string CurrentRevenue = "";
		string CurrentDivision = "";
		int CurrentCol;
		int PostedCol;
		int TransCol;
		int PeriodCol;
		int RCBCol;
		int JournalCol;
		int DescriptionCol;
		int WarrantCol;
		int CheckCol;
		int VendorCol;
		int NetBudgetCol;
		int YTDDebitCol;
		int YTDCreditCol;
		int YTDNetCol;
		int BalanceCol;
		int EncumbranceCol;
		int PendingCol;
		string strPeriodCheck = "";
		string[] strBalances = null;
		clsDRWrapper rsDeptSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsDivSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsRevSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsDeptBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsDivBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsRevBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		bool blnRunCollapse;
		// vbPorter upgrade warning: curNonActivityTotals As Decimal	OnWrite(short, Decimal)
		Decimal[] curNonActivityTotals = new Decimal[3 + 1];
		public string strTitle = "";
		// vbPorter upgrade warning: theReport As cRevenueDetailReport	OnRead(cDetailsReport)
		cRevenueDetailReport theReport = new cRevenueDetailReport();
		cBDAccountController acctServ = new cBDAccountController();

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdExport_Click(object sender, System.EventArgs e)
		{
			ExportData();
		}

		private void ExportData()
		{
			cRevenueDetailController rdc = new cRevenueDetailController();
			rdc.FormatType = 0;
			// csv
			frmReportDataExport rdeForm = new frmReportDataExport(this.Text);
			cReportExportView reView = new cReportExportView();
			reView.SetReport(theReport);
			rdeForm.SetViewModel(ref reView);
			reView.SetExporter(rdc);
			rdeForm.Show(this);
			rdeForm.TopMost = true;
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			//rptRevenueDetail.InstancePtr.Hide();
			frmReportViewer.InstancePtr.Init(rptRevenueDetail.InstancePtr);
		}

		private void frmRevenueDetail_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper Descriptions = new clsDRWrapper();
			int HMonth = 0;
			int LMonth = 0;
			string strPeriodCheck;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rs.OpenRecordset("SELECT * FROM RevenueDetailFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "F")
			{
				lblTitle.Text = "Fund";
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
			}
			else
			{
				lblTitle.Text = "Department(s)";
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
				{
					lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
				{
					lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp");
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
				{
					lblTitle.Text = "Accounts";
					lblRangeDept.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount");
				}
				else
				{
					lblRangeDept.Text = "ALL";
				}
			}
			strPeriodCheck = "AND";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnFromPreview)
			{
				FormatGrid();
				frmWait.InstancePtr.prgProgress.Value = 20;
				frmWait.InstancePtr.Refresh();
				PrepareTemp();
				frmWait.InstancePtr.prgProgress.Value = 40;
				frmWait.InstancePtr.Refresh();
				modBudgetaryAccounting.CalculateAccountInfo();
				// CalculateAccountInfo False, True, True, "R"
				// frmWait.prgProgress.Value = 60
				// frmWait.Refresh
				// CalculateAccountInfo True, True, True, "R"
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
				{
					// rsDeptSummaryInfo.OpenRecordset "SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department"
					// rsDivSummaryInfo.OpenRecordset "SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department, Division"
					// rsRevSummaryInfo.OpenRecordset "SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department, Division, Revenue"
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE Status = 'P' ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
					}
					else
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo ORDER BY Account, OrderMonth, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE Status = 'P' ORDER BY Account, OrderMonth, JournalNumber");
						}
					}
				}
				else
				{
					// LMonth = FirstMonth
					// If SearchResults.Fields["BegMonth"] = LMonth Then
					// HMonth = -1
					// LMonth = -1
					// Else
					// If SearchResults.Fields["BegMonth"] = 1 Then
					// HMonth = 12
					// Else
					// HMonth = SearchResults.Fields["BegMonth"] - 1
					// End If
					// End If
					// If LMonth > HMonth Then
					// strPeriodCheck = "OR"
					// Else
					// strPeriodCheck = "AND"
					// End If
					// rsDeptSummaryInfo.OpenRecordset "SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " & LMonth & " " & strPeriodCheck & " Period <= " & HMonth & " GROUP BY Department"
					// rsDivSummaryInfo.OpenRecordset "SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " & LMonth & " " & strPeriodCheck & " Period <= " & HMonth & " GROUP BY Department, Division"
					// rsRevSummaryInfo.OpenRecordset "SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " & LMonth & " " & strPeriodCheck & " Period <= " & HMonth & " GROUP BY Department, Division, Revenue"
					LMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
					}
					else
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
					}
					if (LMonth > HMonth)
					{
						strPeriodCheck = "OR";
					}
					else
					{
						strPeriodCheck = "AND";
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Status = 'P' ORDER BY Account, OrderMonth, PostedDate, JournalNumber");
						}
					}
					else
					{
						if (PendingDetailFlag)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") ORDER BY Account, OrderMonth, JournalNumber");
						}
						else
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Status = 'P' ORDER BY Account, OrderMonth, JournalNumber");
						}
					}
				}
				LMonth = modBudgetaryMaster.Statics.FirstMonth;
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == LMonth)
				{
					HMonth = -1;
					LMonth = -1;
				}
				else
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) == 1)
					{
						HMonth = 12;
					}
					else
					{
						HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
					}
				}
				if (LMonth > HMonth)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "F")
				{
					rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department");
					rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division");
					rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division, Revenue");
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department");
						rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division");
						rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division, Revenue");
					}
					else
					{
						rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department");
						rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division");
						rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department IN (SELECT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "') GROUP BY Department, Division, Revenue");
					}
				}
				else
				{
					lblTitle.Text = "Department(s)";
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
					{
						rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department");
						rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division");
						rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division, Revenue");
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division, Revenue");
						}
						else
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND Department = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' GROUP BY Department, Division, Revenue");
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
					{
						rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' GROUP BY Department");
						rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' GROUP BY Department, Division");
						rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' GROUP BY Department, Division, Revenue");
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department, Division, Revenue");
						}
						else
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Department >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND Department <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "') GROUP BY Department, Division, Revenue");
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
					{
						rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' GROUP BY Department");
						rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' GROUP BY Department, Division");
						rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' GROUP BY Department, Division, Revenue");
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department, Division, Revenue");
						}
						else
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") AND (Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "') GROUP BY Department, Division, Revenue");
						}
					}
					else
					{
						rsDeptBudgetInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department");
						rsDivBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department, Division");
						rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department, Division, Revenue");
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period < 0 GROUP BY Department, Division, Revenue");
						}
						else
						{
							rsDeptSummaryInfo.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") GROUP BY Department");
							rsDivSummaryInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") GROUP BY Department, Division");
							rsRevSummaryInfo.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) AS EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LMonth) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HMonth) + ") GROUP BY Department, Division, Revenue");
						}
					}
				}
				LMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				}
				else
				{
					HMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
				}
				if (LMonth > HMonth)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
				frmWait.InstancePtr.prgProgress.Value = 80;
				frmWait.InstancePtr.Refresh();
				DepartmentReport();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Calculating Totals";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				FillInInformation();
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				GetRowSubtotals();
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				GetCompleteTotals();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Formatting Report";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				SetColors();
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.RowHeightMax = 220;
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 7);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 10);
				}
				else
				{
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_String("FontName"));
					//FC:FINAL:BBE - font size used in original for samll row height
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields("FontSize"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Bold"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontItalic, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Italic"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontStrikethru, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("StrikeThru"));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpFontUnderline, 0, 1, vs1.Rows - 1, vs1.Cols - 1, rs.Get_Fields_Boolean("Underline"));
					vs1.AutoSize(1, vs1.Cols - 1, false, 300);
					vs1.ExtendLastCol = false;
				}
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				blnRunCollapse = false;
				for (counter = 4; counter >= 0; counter--)
				{
					for (counter2 = 2; counter2 <= vs1.Rows - 1; counter2++)
					{
						//Application.DoEvents();
						if (vs1.RowOutlineLevel(counter2) == counter)
						{
							if (vs1.IsSubtotal(counter2))
							{
								vs1.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
							}
						}
					}
				}
				blnRunCollapse = true;
				vs1_Collapsed();
			}
			else
			{
				modBudgetaryMaster.Statics.blnFromPreview = false;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Use")) == "P")
			{
				cmdPrint.Enabled = true;
			}
			this.Refresh();
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
			vs1.Visible = true;
			lblTitle.Visible = true;
			lblRangeDept.Visible = true;
			lblMonthLabel.Visible = true;
			lblMonths.Visible = true;
		}

		private void frmRevenueDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRevenueDetail.FillStyle	= 0;
			//frmRevenueDetail.ScaleWidth	= 9480;
			//frmRevenueDetail.ScaleHeight	= 7425;
			//frmRevenueDetail.AutoRedraw	= -1  'True;
			//frmRevenueDetail.LinkTopic	= "Form2";
			//frmRevenueDetail.LockControls	= -1  'True;
			//frmRevenueDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DeleteTemp();
			frmRevenueDetailSelect.InstancePtr.Show(App.MainForm);
		}

		private void frmRevenueDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled == true)
			{
				//rptRevenueDetail.InstancePtr.Hide();
				frmReportViewer.InstancePtr.Init(rptRevenueDetail.InstancePtr);
			}
		}

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrint.Enabled == true)
			{
				//rptRevenueDetail.InstancePtr.Hide();
				modDuplexPrinting.DuplexPrintReport(rptRevenueDetail.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool FirstFlag = false;
			bool SecondFlag = false;
			bool ThirdFlag = false;
			bool FourthFlag = false;
			bool FifthFlag;
			int counter2;
			// vbPorter upgrade warning: TempInfo As Decimal	OnWrite(short, string)
			Decimal TempInfo;
			int TempRow = 0;
			string TempArrayInfo = "";
			TempInfo = 0;
			if (blnRunCollapse)
			{
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 0)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FirstFlag = true;
							if (strBalances[counter] == "B0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E0");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, TempRow, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							FirstFlag = false;
							if (strBalances[counter] == "E0")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B0");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (FirstFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							SecondFlag = true;
							if (strBalances[counter] == "B1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E1");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, TempRow, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							SecondFlag = false;
							if (strBalances[counter] == "E1")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B1");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 2)
					{
						if (FirstFlag == true || SecondFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							ThirdFlag = true;
							if (strBalances[counter] == "B2")
							{
								TempRow = FindEndingBalanceRow_6(counter, "E2");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, TempRow, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
						else
						{
							rows += 1;
							ThirdFlag = false;
							if (strBalances[counter] == "E2")
							{
								TempRow = FindEndingBalanceRow_6(counter, "B2");
								for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
								{
									if (vs1.TextMatrix(counter, counter2) != "")
									{
										TempInfo = FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
									}
									else
									{
										TempInfo = 0;
									}
									vs1.TextMatrix(counter, counter2, vs1.TextMatrix(TempRow, counter2));
									vs1.TextMatrix(TempRow, counter2, FCConvert.ToString(TempInfo));
									if (counter2 == BalanceCol && IsLowRow(counter))
									{
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) < 0)
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Blue);
										}
										if (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) > FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)))
										{
											vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Red);
										}
										vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, counter, BalanceCol, Color.Black);
									}
								}
								TempArrayInfo = strBalances[counter];
								strBalances[counter] = strBalances[TempRow];
								strBalances[TempRow] = TempArrayInfo;
							}
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 3)
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FourthFlag = true;
						}
						else
						{
							rows += 1;
							FourthFlag = false;
						}
					}
					else
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true || FourthFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					height = (rows + 2) * 220 + 300;
					if (height < frmExpenseSummary.InstancePtr.Height * 0.6645)
					{
						if (vs1.Height != height)
						{
							vs1.Height = height + 75;
						}
					}
					else
					{
						if (vs1.Height < frmExpenseSummary.InstancePtr.Height * 0.6645)
						{
							vs1.Height = FCConvert.ToInt32(frmExpenseSummary.InstancePtr.Height * 0.6645 + 75);
						}
					}
				}
				else
				{
					height = (rows + 2) * vs1.RowHeight(0);
					if (height < frmExpenseSummary.InstancePtr.Height * 0.6645)
					{
						if (vs1.Height != height)
						{
							vs1.Height = height + 75;
						}
					}
					else
					{
						if (vs1.Height < frmExpenseSummary.InstancePtr.Height * 0.6645)
						{
							vs1.Height = FCConvert.ToInt32(frmExpenseSummary.InstancePtr.Height * 0.6645 + 75);
						}
					}
				}
			}
		}

		private void FormatGrid()
		{
			int counter = 0;
			//FC:FINAL:BBE:#i721 - merge the cells
			vs1.MergeRow(0, true);
			vs1.Cols = 20;
			//FC:FINAL:BBE:#i721 - add the expand button
			vs1.AddExpandButton();
			vs1.RowHeadersWidth = 10;
			//FC:FINAL:BBE:#595-#596 - correct column size with factor to show it correct on the report
			vs1.UseScaleFactor = true;
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				vs1.TextMatrix(0, 1, "----Dates----");
				vs1.TextMatrix(1, 1, "Posted");
				vs1.TextMatrix(1, 2, "Trans");
				vs1.TextMatrix(1, 3, "Per");
				vs1.TextMatrix(0, 4, "RCB/");
				vs1.TextMatrix(1, 4, "Type");
				vs1.TextMatrix(1, 5, "Jrnl");
				vs1.TextMatrix(1, 6, "Description---");
				vs1.TextMatrix(1, 7, "Wrnt");
				vs1.TextMatrix(1, 8, "Check#");
				vs1.TextMatrix(1, 9, "Vendor------");
				PostedCol = 1;
				TransCol = 2;
				PeriodCol = 3;
				RCBCol = 4;
				JournalCol = 5;
				DescriptionCol = 6;
				WarrantCol = 7;
				CheckCol = 8;
				VendorCol = 9;
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(PostedCol, 800);
				}
				else
				{
					vs1.ColWidth(PostedCol, 1100);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(TransCol, 800);
				}
				else
				{
					vs1.ColWidth(TransCol, 1100);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(PeriodCol, 500);
				}
				else
				{
					vs1.ColWidth(PeriodCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(RCBCol, 500);
				}
				else
				{
					vs1.ColWidth(RCBCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(JournalCol, 700);
				}
				else
				{
					vs1.ColWidth(JournalCol, 800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(DescriptionCol, 2400);
				}
				else
				{
					vs1.ColWidth(DescriptionCol, 2800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(WarrantCol, 500);
				}
				else
				{
					vs1.ColWidth(WarrantCol, 600);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(CheckCol, 800);
				}
				else
				{
					vs1.ColWidth(CheckCol, 1000);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(VendorCol, 2400);
				}
				else
				{
					vs1.ColWidth(VendorCol, 2600);
				}
				counter = 10;
			}
			else if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				vs1.TextMatrix(0, 1, "Account------------");
				vs1.TextMatrix(1, 1, "Date");
				vs1.TextMatrix(1, 2, "Jrnl");
				vs1.TextMatrix(1, 3, "Desc---");
				vs1.TextMatrix(1, 4, "Vendor------");
				TransCol = 1;
				JournalCol = 2;
				DescriptionCol = 3;
				VendorCol = 4;
				//FC:FINAL:BBE:#595-#596 - correct column size
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					//vs1.ColWidth(TransCol, 1000);
					vs1.ColWidth(TransCol, 1450);
				}
				else
				{
					//vs1.ColWidth(TransCol, 1300);
					vs1.ColWidth(TransCol, 1450);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(JournalCol, 700);
				}
				else
				{
					vs1.ColWidth(JournalCol, 800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(DescriptionCol, 2400);
				}
				else
				{
					vs1.ColWidth(DescriptionCol, 2800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(VendorCol, 2400);
				}
				else
				{
					vs1.ColWidth(VendorCol, 2600);
				}
				counter = 5;
			}
			else
			{
				vs1.TextMatrix(0, 1, "Account------------");
				vs1.TextMatrix(1, 1, "Date");
				vs1.TextMatrix(1, 2, "Jrnl");
				vs1.TextMatrix(1, 3, "Desc---");
				TransCol = 1;
				JournalCol = 2;
				DescriptionCol = 3;
				//FC:FINAL:BBE:#595-#596 - correct column size
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					//vs1.ColWidth(TransCol, 1000);
					vs1.ColWidth(TransCol, 1450);
				}
				else
				{
					//vs1.ColWidth(TransCol, 1300);
					vs1.ColWidth(TransCol, 1450);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(JournalCol, 700);
				}
				else
				{
					vs1.ColWidth(JournalCol, 800);
				}
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(DescriptionCol, 2400);
				}
				else
				{
					vs1.ColWidth(DescriptionCol, 2800);
				}
				counter = 4;
			}
			vs1.TextMatrix(0, counter, "Current");
			vs1.TextMatrix(1, counter, "Budget");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1400);
			}
			else
			{
				vs1.ColWidth(counter, 1600);
			}
			vs1.ColFormat(counter, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            NetBudgetFlag = true;
			NetBudgetCol = counter;
			vs1.MergeCol(NetBudgetCol, false);
			counter += 1;
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
			{
				vs1.TextMatrix(0, counter, "");
				vs1.TextMatrix(0, counter + 1, "");
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1400);
				}
				else
				{
					vs1.ColWidth(counter, 1600);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1400);
				}
				else
				{
					vs1.ColWidth(counter + 1, 1600);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDDCFlag = true;
				YTDDebitCol = counter;
				YTDCreditCol = counter + 1;
				vs1.MergeCol(YTDDebitCol, false);
				vs1.MergeCol(YTDCreditCol, false);
				counter += 2;
			}
			else
			{
				YTDDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "");
				}
				else
				{
					vs1.TextMatrix(0, counter, "");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1400);
				}
				else
				{
					vs1.ColWidth(counter, 1600);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDNetFlag = true;
				YTDNetCol = counter;
				vs1.MergeCol(YTDNetCol, false);
				counter += 1;
			}
			else
			{
				YTDNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("PendingDetail")))
			{
				PendingDetailFlag = true;
				PendingSummaryFlag = false;
			}
			else if (rs.Get_Fields_Boolean("PendingSummary"))
			{
				PendingSummaryFlag = true;
				PendingDetailFlag = false;
			}
			else
			{
				PendingDetailFlag = false;
				PendingSummaryFlag = false;
			}
			vs1.TextMatrix(0, counter, "Uncollected");
			vs1.TextMatrix(1, counter, "Balance");
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				vs1.ColWidth(counter, 1850);
			}
			else
			{
				vs1.ColWidth(counter, 2100);
			}
			vs1.ColFormat(counter, "#,##0.00");
            vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
            BalanceFlag = true;
			BalanceCol = counter;
			vs1.MergeCol(BalanceCol, false);
			counter += 1;
			vs1.Cols = counter;
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 9, 4);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 10, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
			else
			{
				vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 3, 4);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, vs1.Cols - 1, true);
		}

		private string Revenue(ref string x)
		{
			string Revenue = "";
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				Revenue = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			else
			{
				Revenue = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			return Revenue;
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private string Division(ref string x)
		{
			string Division = "";
			Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))));
			return Division;
		}

		private string CalculateMonth(int x)
		{
			string CalculateMonth = "";
			switch (x)
			{
				case 1:
					{
						CalculateMonth = "January";
						break;
					}
				case 2:
					{
						CalculateMonth = "February";
						break;
					}
				case 3:
					{
						CalculateMonth = "March";
						break;
					}
				case 4:
					{
						CalculateMonth = "April";
						break;
					}
				case 5:
					{
						CalculateMonth = "May";
						break;
					}
				case 6:
					{
						CalculateMonth = "June";
						break;
					}
				case 7:
					{
						CalculateMonth = "July";
						break;
					}
				case 8:
					{
						CalculateMonth = "August";
						break;
					}
				case 9:
					{
						CalculateMonth = "September";
						break;
					}
				case 10:
					{
						CalculateMonth = "October";
						break;
					}
				case 11:
					{
						CalculateMonth = "November";
						break;
					}
				case 12:
					{
						CalculateMonth = "December";
						break;
					}
			}
			//end switch
			return CalculateMonth;
		}

		private void DepartmentReport()
		{
			clsDRWrapper DeptDivDescriptions = new clsDRWrapper();
			clsDRWrapper RevDescriptions = new clsDRWrapper();
			DeptDivDescriptions.OpenRecordset("SELECT * FROM DeptDivTitles");
			RevDescriptions.OpenRecordset("SELECT * FROM RevTitles");
			theReport = new cRevenueDetailReport();
			theReport.Details.ClearList();
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowLiquidatedEncumbranceActivity")))
			{
				theReport.ShowLiquidatedEncumbranceActivity = true;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("pendingdetail")) || FCConvert.ToBoolean(rs.Get_Fields_Boolean("pendingSummary")))
			{
				theReport.ShowPendingActivity = true;
			}
			if (Strings.LCase(FCConvert.ToString(rs.Get_Fields_String("PAPERWIDTH"))) == "l" || Strings.LCase(FCConvert.ToString(rs.Get_Fields_String("paperwidth"))) == "w")
			{
				theReport.ShowJournalDetail = true;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				rs2.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "'");
				if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
				{
					rs2.MoveLast();
					rs2.MoveFirst();
					vs1.Rows += 1;
					//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
					vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
					if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
					{
						if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + "UNKNOWN");
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 0);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
					vs1.IsSubtotal(vs1.Rows - 1, true);
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
								//Application.DoEvents();
								vs1.Rows += 1;
								//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
								vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
								if (DeptDivDescriptions.FindFirstRecord2("Department, Division", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								// Get Revenue Information-------------------------------
								vs1.IsSubtotal(vs1.Rows - 1, true);
								rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
								if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
								{
									rs3.MoveLast();
									rs3.MoveFirst();
									while (rs3.EndOfFile() != true)
									{
										//Application.DoEvents();
										vs1.Rows += 1;
										//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
										vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
										if (RevDescriptions.FindFirstRecord2("Department, Division, Revenue", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs3.Get_Fields_String("ThirdAccountField"), ","))
										{
											if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
											}
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
										}
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
										vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
										CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rs2.Get_Fields_String("SecondAccountField"), rs3.Get_Fields_String("ThirdAccountField"), ""));
										rs3.MoveNext();
									}
								}
								// Get Revenue Information---------------------------
								vs1.Rows += 1;
								vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
								vs1.IsSubtotal(vs1.Rows - 1, true);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								rs2.MoveNext();
							}
						}
					}
					else
					{
						// Get Revenue Information----------------------
						rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField");
						if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
						{
							rs3.MoveLast();
							rs3.MoveFirst();
							while (rs3.EndOfFile() != true)
							{
								//Application.DoEvents();
								vs1.Rows += 1;
								//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
								vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
								if (RevDescriptions.FindFirstRecord2("Department, Revenue", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs3.Get_Fields_String("SecondAccountField"), ","))
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 1);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
								CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"), rs3.Get_Fields_String("SecondAccountField"), "", ""));
								rs3.MoveNext();
							}
						}
						// Get Revenue Information------------------------
					}
					vs1.Rows += 1;
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
					vs1.IsSubtotal(vs1.Rows - 1, true);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "F")
			{
				// more than 1 department
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField IN (SELECT DISTINCT Department FROM DeptDivTitles WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "')");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						//Application.DoEvents();
						vs1.Rows += 1;
						//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
						vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
							{
								rs2.MoveLast();
								rs2.MoveFirst();
								while (rs2.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									// Get Revenue Information------------------------
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
									{
										rs3.MoveLast();
										rs3.MoveFirst();
										while (rs3.EndOfFile() != true)
										{
											//Application.DoEvents();
											vs1.Rows += 1;
											//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
											vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
											if (RevDescriptions.FindFirstRecord2("Department, Division, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs3.Get_Fields_String("ThirdAccountField"), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs3.Get_Fields_String("ThirdAccountField"), ""));
											rs3.MoveNext();
										}
									}
									// Get Revenue Information---------------------------
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									rs2.MoveNext();
								}
							}
						}
						else
						{
							// Get Revenue Information---------------------------------
							rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
							{
								rs3.MoveLast();
								rs3.MoveFirst();
								while (rs3.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (RevDescriptions.FindFirstRecord2("Department, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs3.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs3.Get_Fields_String("SecondAccountField"), "", ""));
									rs3.MoveNext();
								}
							}
							// Get Revenue Information----------------------------
						}
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
						rs5.MoveNext();
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				// more than 1 department
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND FirstAccountField <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						//Application.DoEvents();
						vs1.Rows += 1;
						//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
						vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
							{
								rs2.MoveLast();
								rs2.MoveFirst();
								while (rs2.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									// Get Revenue Information------------------------
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
									{
										rs3.MoveLast();
										rs3.MoveFirst();
										while (rs3.EndOfFile() != true)
										{
											//Application.DoEvents();
											vs1.Rows += 1;
											//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
											vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
											if (RevDescriptions.FindFirstRecord2("Department, Division, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs3.Get_Fields_String("ThirdAccountField"), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs3.Get_Fields_String("ThirdAccountField"), ""));
											rs3.MoveNext();
										}
									}
									// Get Revenue Information---------------------------
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									rs2.MoveNext();
								}
							}
						}
						else
						{
							// Get Revenue Information---------------------------------
							rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
							{
								rs3.MoveLast();
								rs3.MoveFirst();
								while (rs3.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (RevDescriptions.FindFirstRecord2("Department, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs3.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs3.Get_Fields_String("SecondAccountField"), "", ""));
									rs3.MoveNext();
								}
							}
							// Get Revenue Information----------------------------
						}
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
						rs5.MoveNext();
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "A")
			{
				// All Departments
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'R'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						//Application.DoEvents();
						vs1.Rows += 1;
						//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
						vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
							{
								rs2.MoveLast();
								rs2.MoveFirst();
								while (rs2.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									// Get Revenue Information-----------------------
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' ORDER BY ThirdAccountField");
									if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
									{
										rs3.MoveLast();
										rs3.MoveFirst();
										while (rs3.EndOfFile() != true)
										{
											//Application.DoEvents();
											vs1.Rows += 1;
											//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
											vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
											if (RevDescriptions.FindFirstRecord2("Department, Division, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs3.Get_Fields_String("ThirdAccountField"), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs3.Get_Fields_String("ThirdAccountField"), ""));
											rs3.MoveNext();
										}
									}
									// Get Revenue Information--------------------------
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									rs2.MoveNext();
								}
							}
						}
						else
						{
							// Get Revenue Information------------------------
							rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField");
							if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
							{
								rs3.MoveLast();
								rs3.MoveFirst();
								while (rs3.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (RevDescriptions.FindFirstRecord2("Department, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs3.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs3.Get_Fields_String("SecondAccountField"), "", ""));
									rs3.MoveNext();
								}
							}
							// Get Revenue Information----------------------------
						}
						rs5.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
					}
				}
			}
			else
			{
				// range of accounts
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'R' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						//Application.DoEvents();
						vs1.Rows += 1;
						//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
						vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
						if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + modBudgetaryAccounting.Statics.strZeroDiv, ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 0);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							rs2.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
							if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
							{
								rs2.MoveLast();
								rs2.MoveFirst();
								while (rs2.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (DeptDivDescriptions.FindFirstRecord2("Department, Division", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + DeptDivDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									// Get Revenue Information----------------------------
									vs1.IsSubtotal(vs1.Rows - 1, true);
									rs3.OpenRecordset("SELECT DISTINCT ThirdAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND SecondAccountField = '" + rs2.Get_Fields_String("SecondAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY ThirdAccountField");
									if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
									{
										rs3.MoveLast();
										rs3.MoveFirst();
										while (rs3.EndOfFile() != true)
										{
											//Application.DoEvents();
											vs1.Rows += 1;
											//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
											vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
											if (RevDescriptions.FindFirstRecord2("Department, Division, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs3.Get_Fields_String("ThirdAccountField"), ","))
											{
												if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
												}
												else
												{
													vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
												}
											}
											else
											{
												vs1.TextMatrix(vs1.Rows - 1, 1, "        " + rs3.Get_Fields_String("ThirdAccountField") + " - " + "UNKNOWN");
											}
											vs1.RowOutlineLevel(vs1.Rows - 1, 2);
											vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
											CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs2.Get_Fields_String("SecondAccountField"), rs3.Get_Fields_String("ThirdAccountField"), ""));
											rs3.MoveNext();
										}
									}
									// Get Revenue Information---------------------------
									vs1.Rows += 1;
									vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Division....");
									vs1.IsSubtotal(vs1.Rows - 1, true);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
									rs2.MoveNext();
								}
							}
						}
						else
						{
							// Get Revenue Information------------------------
							rs3.OpenRecordset("SELECT DISTINCT SecondAccountField FROM Temp WHERE AccountType = 'R' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField");
							if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
							{
								rs3.MoveLast();
								rs3.MoveFirst();
								while (rs3.EndOfFile() != true)
								{
									//Application.DoEvents();
									vs1.Rows += 1;
									//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
									vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
									if (RevDescriptions.FindFirstRecord2("Department, Revenue", rs5.Get_Fields_String("FirstAccountField") + "," + rs3.Get_Fields_String("SecondAccountField"), ","))
									{
										if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("ShortDescription"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + RevDescriptions.Get_Fields_String("LongDescription"));
										}
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs3.Get_Fields_String("SecondAccountField") + " - " + "UNKNOWN");
									}
									vs1.RowOutlineLevel(vs1.Rows - 1, 1);
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xC0FFFF);
									CreateDetailInfo_2(modBudgetaryMaster.CreateAccount_242("R", rs5.Get_Fields_String("FirstAccountField"), rs3.Get_Fields_String("SecondAccountField"), "", ""));
									rs3.MoveNext();
								}
							}
							// Get Revenue Information----------------------------
						}
						rs5.MoveNext();
						vs1.Rows += 1;
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Department..");
						vs1.IsSubtotal(vs1.Rows - 1, true);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
					}
				}
			}
		}

		private void PrepareTemp()
		{
			string strSQL1;
			string strSQL2;
			string strSQL3 = "";
			string strSQL4 = "";
			string strSQL5 = "";
			string strSQL6;
			string strTotalSQL;
			string strTotalSQL2;
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strSQLFields;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			DeleteTemp();
			strSQL1 = "AccountType, Account, ";
			strSQL2 = "substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Rev + "', 2))) AS FirstAccountField, ";
			strSQLFields = "AccountType, Account, FirstAccountField";
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Rev + "', 2)), convert(int, substring('" + modAccountTitle.Statics.Rev + "', 3, 2))) AS SecondAccountField, ";
				strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Rev + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Rev + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Rev + "', 5, 2))) AS ThirdAccountField, ";
				strSQL5 = "";
				strSQLFields += ", SecondAccountField, ThirdAccountField";
			}
			else
			{
				strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Rev + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Rev + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Rev + "', 5, 2))) AS SecondAccountField, ";
				strSQL4 = "";
				strSQL5 = "";
				strSQLFields += ", SecondAccountField";
			}
			strSQL6 = "left(Account, 1) AS AccountType, Account, ";
			strTotalSQL = "SELECT DISTINCT " + strSQL1 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL = Strings.Left(strTotalSQL, strTotalSQL.Length - 2);
			strTotalSQL2 = "SELECT DISTINCT " + strSQL6 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL2 = Strings.Left(strTotalSQL2, strTotalSQL2.Length - 2);
			if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowZeroBalance")))
			{
				// rs3.CreateStoredProcedure "ExpenseAccounts", strTotalSQL2 & " FROM APJournalDetail WHERE left(Account, 1) = 'R' UNION " & strTotalSQL2 & " FROM EncumbranceDetail WHERE left(Account, 1) = 'R' UNION " & strTotalSQL2 & " FROM JournalEntries WHERE left(Account, 1) = 'R' UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'R' AND Valid = 1 ORDER BY Account"  'UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'R'
				rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'R' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'R' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'R' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'R' AND Valid = 1 ) as ExpenseAccounts", "Budgetary");
			}
			else
			{
				// rs3.CreateStoredProcedure "ExpenseAccounts", strTotalSQL2 & " FROM APJournalDetail WHERE left(Account, 1) = 'R' UNION " & strTotalSQL2 & " FROM EncumbranceDetail WHERE left(Account, 1) = 'R' UNION " & strTotalSQL2 & " FROM JournalEntries WHERE left(Account, 1) = 'R' UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'R' AND CurrentBudget <> 0 ORDER BY Account"  'UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'R'
				rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'R' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'R' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'R' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'R' AND CurrentBudget <> 0 ) as ExpenseAccounts", "Budgetary");
			}
			// rs3.Execute "INSERT INTO Temp SELECT * FROM ExpenseAccounts"
			strPeriodCheck = strPeriodCheckHolder;
		}

		private void DeleteTemp()
		{
			rs3.Execute("DELETE FROM Temp", "Budgetary");
		}

		private void FillInInformation()
		{
			double temp = 0;
			int lngBeginningRow = 0;
			int lngCounter;
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Refresh();
			//FC:FINAL:BBE - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 7);
			lngCounter = 2;
			for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
			{
				//Application.DoEvents();
				if (CurrentRow >= lngCounter + 20)
				{
					lngCounter = CurrentRow;
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CurrentRow) / (vs1.Rows - 1)) * 100);
					frmWait.InstancePtr.Refresh();
				}
				if (!IsTotalRow(CurrentRow) && !IsDetailRow(CurrentRow))
				{
					if (vs1.RowOutlineLevel(CurrentRow) == 0)
					{
						CurrentDepartment = Strings.Left(vs1.TextMatrix(CurrentRow, 1), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
						CurrentDivision = "";
						CurrentRevenue = "";
					}
					else if (vs1.RowOutlineLevel(CurrentRow) == 1 && FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De")
					{
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							CurrentDivision = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
							CurrentRevenue = "";
						}
						else
						{
							CurrentRevenue = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
						}
					}
					else if (vs1.RowOutlineLevel(CurrentRow) == 2)
					{
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "De" && FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) != "Di")
						{
							CurrentRevenue = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
						}
					}
					if (NetBudgetFlag)
					{
						vs1.TextMatrix(CurrentRow, NetBudgetCol, Strings.Format(GetNetBudget(), "#,##0.00"));
					}
					if (YTDDCFlag)
					{
						vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(GetYTDDebit(), "#,##0.00"));
						vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(GetYTDCredit(), "#,##0.00"));
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(GetYTDNet() * -1, "#,##0.00"));
					}
					if (YTDDCFlag)
					{
						temp = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, NetBudgetCol)) + FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDDebitCol)) - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDCreditCol));
					}
					else
					{
						temp = FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(CurrentRow, YTDNetCol));
					}
					if (temp < 0)
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, BalanceCol, Color.Blue);
					}
					if (temp > GetNetBudget())
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurrentRow, BalanceCol, Color.Red);
					}
					vs1.TextMatrix(CurrentRow, BalanceCol, FCConvert.ToString(temp));
				}
				else if (IsTotalRow(CurrentRow))
				{
					lngBeginningRow = FindBeginningBalanceRow(ref CurrentRow);
					// If vs1.TextMatrix(CurrentRow, NetBudgetCol) <> "" Then
					// vs1.TextMatrix(CurrentRow, NetBudgetCol) = format(CCur(vs1.TextMatrix(lngBeginningRow, NetBudgetCol)) + CCur(vs1.TextMatrix(CurrentRow, NetBudgetCol)), "#,##0.00")
					// Else
					// vs1.TextMatrix(CurrentRow, NetBudgetCol) = format(CCur(vs1.TextMatrix(lngBeginningRow, NetBudgetCol)), "#,##0.00")
					// End If
					if (YTDDCFlag == true)
					{
						if (vs1.TextMatrix(CurrentRow, YTDDebitCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDDebitCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDDebitCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDDebitCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDDebitCol)), "#,##0.00"));
						}
						if (vs1.TextMatrix(CurrentRow, YTDCreditCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDCreditCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDCreditCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDCreditCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDCreditCol)), "#,##0.00"));
						}
					}
					else
					{
						if (vs1.TextMatrix(CurrentRow, YTDNetCol) != "")
						{
							vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDNetCol)) + FCConvert.ToDecimal(vs1.TextMatrix(CurrentRow, YTDNetCol)), "#,##0.00"));
						}
						else
						{
							vs1.TextMatrix(CurrentRow, YTDNetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(lngBeginningRow, YTDNetCol)), "#,##0.00"));
						}
					}
				}
			}
		}

		private double GetNetBudget()
		{
			double GetNetBudget = 0;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal") + rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
					}
					else
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
					{
						if (rsDivBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivBudgetInfo.Get_Fields("OriginalBudgetTotal") + rsDivSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
					}
					else
					{
						if (rsDivBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDivBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else
				{
					if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal") + rsRevSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
					}
					else
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal") + rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
					}
					else
					{
						if (rsDeptBudgetInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsDeptBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
				else
				{
					if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal") + rsRevSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("BudgetAdjustmentsTotal"));
						}
					}
					else
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetNetBudget = FCConvert.ToDouble(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetNetBudget = 0;
						}
					}
				}
			}
			return GetNetBudget;
		}

		private short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		private short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private double GetEncumbrance()
		{
			double GetEncumbrance = 0;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
					else
					{
						GetEncumbrance = 0;
					}
				}
			}
			return GetEncumbrance;
		}

		private double GetPending()
		{
			double GetPending = 0;
			GetPending = GetPendingCredits() - GetPendingDebits();
			return GetPending;
		}

		private double GetPendingCredits()
		{
			double GetPendingCredits = 0;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PendingCreditsTotal")) * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			return GetPendingCredits;
		}

		private double GetPendingDebits()
		{
			double GetPendingDebits = 0;
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDeptSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDivSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsRevSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsDeptSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsRevSummaryInfo.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			return GetPendingDebits;
		}

		private void GetCompleteTotals()
		{
			int counter;
			int counter2;
			double total = 0;
			vs1.Rows += 1;
			//FC:FINAL:BBE:#595-#596 - Add missing MergeRow to solve cut columns
			vs1.MergeRow(vs1.Rows - 1, true, 1, DescriptionCol);
			vs1.TextMatrix(vs1.Rows - 1, 1, "Final Totals");
			for (counter = NetBudgetCol; counter <= vs1.Cols - 1; counter++)
			{
				total = 0;
				for (counter2 = 2; counter2 <= vs1.Rows - 2; counter2++)
				{
					if (IsTotalRow(counter2) && vs1.RowOutlineLevel(counter2) == 1)
					{
						total += FCConvert.ToDouble(vs1.TextMatrix(counter2, counter));
					}
				}
				vs1.TextMatrix(vs1.Rows - 1, counter, FCConvert.ToString(total));
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.Blue);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			vs1.RowOutlineLevel(vs1.Rows - 1, 0);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void SetColors()
		{
			modColorScheme.ColorGrid(vs1, 2, -1, 0, -1, true);
		}

		private void CreateDetailInfo_2(string strAccount)
		{
			CreateDetailInfo(ref strAccount);
		}

		private void CreateDetailInfo(ref string strAccount)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int HighDate = 0;
			int LowDate;
            Decimal curDebitTotal;
            Decimal curCreditTotal;
            Decimal curNetTotal;
            Decimal curBudgetTotal;
			double dblPendingTotal = 0;
			bool blnRecordsFound;
            Decimal curDebitTotalPeriod = 0.0M;
            Decimal curCreditTotalPeriod = 0.0M;
            Decimal curNetTotalPeriod = 0.0M;
            Decimal curBudgetTotalPeriod = 0.0M;
			int intPeriod;
			Decimal curPeriodBalance;
			cRevenueDetailItem revItem = new cRevenueDetailItem();
            cDetailsReport detReport;
			string strCurrentFund = "";
            Decimal curNetBegin;
			Decimal curUncollectedBalance;
			detReport = theReport;
			intPeriod = -1;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				HighDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"));
			}
			else
			{
				HighDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				if (HighDate == 1)
				{
					HighDate = 12;
				}
				else
				{
					HighDate -= 1;
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) != "A")
			{
				LowDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			else
			{
				LowDate = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			}
			blnRecordsFound = false;
			curDebitTotal = 0;
			curCreditTotal = 0;
			curNetTotal = 0;
			curBudgetTotal = 0;
			CurrentDepartment = Department(ref strAccount);
			CurrentDivision = Division(ref strAccount);
			CurrentRevenue = Revenue(ref strAccount);
			curNetBegin = FCConvert.ToDecimal(GetNetBudget());
			curPeriodBalance = curNetBegin + FCConvert.ToDecimal(GetYTDNet());
			curNetBegin = curPeriodBalance;
			curUncollectedBalance = curNetBegin;
			strAccount = Strings.Left(strAccount, strAccount.Length - 1);
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				vs1.IsSubtotal(vs1.Rows - 1, true);
				CheckAgain:
				;
                if (FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) == strAccount)
				{
					strCurrentFund = acctServ.GetFundForAccount(strAccount);
					do
					{
                        if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "E" && FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) == 0)
						{
							goto CheckNext;
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
						{
							if (intPeriod == -1)
							{
                                intPeriod = FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period"));
							}
							else
							{
                                if (intPeriod != FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period")))
								{
									vs1.Rows += 1;
									vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, MonthCalc(intPeriod));
									vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curBudgetTotalPeriod, "#,##0.00"));
									if (YTDDCFlag)
									{
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotalPeriod, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotalPeriod * -1, "#,##0.00"));
										curPeriodBalance += curDebitTotalPeriod + curCreditTotalPeriod;
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curNetTotalPeriod, "#,##0.00"));
										curPeriodBalance -= curNetTotalPeriod;
									}
									vs1.TextMatrix(vs1.Rows - 1, BalanceCol, Strings.Format(curPeriodBalance, "#,##0.00"));
									curDebitTotalPeriod = 0;
									curCreditTotalPeriod = 0;
									curNetTotalPeriod = 0;
									curBudgetTotalPeriod = 0;
									if (modAccountTitle.Statics.RevDivFlag)
									{
										vs1.RowOutlineLevel(vs1.Rows - 1, 2);
									}
									else
									{
										vs1.RowOutlineLevel(vs1.Rows - 1, 3);
									}
									vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
                                    intPeriod = FCConvert.ToInt32(rsDetailInfo.Get_Fields("Period"));
								}
							}
						}
						blnRecordsFound = true;
						vs1.Rows += 1;
						revItem = new cRevenueDetailItem();
                        revItem.Account = FCConvert.ToString(rsDetailInfo.Get_Fields("Account"));
                        revItem.JournalNumber = FCConvert.ToInt32(rsDetailInfo.Get_Fields("journalnumber"));
						revItem.Description = FCConvert.ToString(rsDetailInfo.Get_Fields_String("description"));
						revItem.Department = CurrentDepartment;
						revItem.Division = CurrentDivision;
						revItem.Revenue = CurrentRevenue;
						revItem.Fund = strCurrentFund;
                        revItem.Warrant = FCConvert.ToString(rsDetailInfo.Get_Fields("warrant"));
                        revItem.JournalType = FCConvert.ToString(rsDetailInfo.Get_Fields("Type"));
						revItem.RCB = FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB"));
                        revItem.CheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDetailInfo.Get_Fields("CheckNumber"))));
                        revItem.Period = FCConvert.ToInt32(Math.Round(Conversion.Val(rsDetailInfo.Get_Fields("period"))));
						revItem.TransactionDate = Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
						if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("status")) == "P")
						{
							revItem.JournalDate = Strings.Format(rsDetailInfo.Get_Fields_DateTime("Posteddate"), "MM/dd/yyyy");
						}
                        if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
                                vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "A " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
                                vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
                                vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
									}
									revItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "A " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
									{
										rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
										if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
										}
										revItem.Vendor = vs1.TextMatrix(vs1.Rows - 1, VendorCol);
									}
								}
							}
							if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowLiquidatedEncumbranceActivity")))
							{
								// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
                                if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
                                    revItem.Debit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
									revItem.Credit = (FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
									// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
								}
                                else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    revItem.Debit = FCConvert.ToDouble((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")));
									revItem.Credit = Conversion.Val(rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
									// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
								}
                                else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    revItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
									revItem.Debit = (FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
									// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
								}
								else
								{
                                    revItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
									revItem.Debit = (FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
								}
							}
							else
							{
								// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
                                if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
                                    revItem.Debit = (double) rsDetailInfo.Get_Fields("Amount") - (double)rsDetailInfo.Get_Fields("Discount") - (double)rsDetailInfo.Get_Fields_Decimal("Encumbrance");
									revItem.Credit = 0;
									// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
								}
								else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									revItem.Debit = FCConvert.ToDouble((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
									revItem.Credit = 0;
									// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
								}	
								else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									revItem.Credit = (FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount")) - FCConvert.ToDouble(rsDetailInfo.Get_Fields("Discount")) - FCConvert.ToDouble(rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
									revItem.Debit = 0;
									// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
								}
								else
								{
									revItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
									revItem.Debit = 0;
								}
							}
							if (YTDDCFlag)
							{
								if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowLiquidatedEncumbranceActivity")))
								{
									// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
									if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
									{
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00"));
										curDebitTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
										curCreditTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
										curDebitTotalPeriod += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
										curCreditTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
										// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
									}
									else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
									{
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00"));
										curDebitTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")));
										curCreditTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
										curDebitTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")));
										curCreditTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance"));
										// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
									}
									else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
                                        curCreditTotal += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
										curDebitTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                        curCreditTotalPeriod += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount"));
										curDebitTotalPeriod += (FCConvert.ToDecimal(rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
										// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
									}
									else
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
                                        curCreditTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
										curDebitTotal += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                        curCreditTotalPeriod += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
										curDebitTotalPeriod += (rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
									}
								}
								else
								{
									// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
                                    if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
                                        curDebitTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
                                        curDebitTotalPeriod += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
										// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
									}
                                    else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
                                        curDebitTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
                                        curDebitTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
										// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
									}
                                    else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
                                        curCreditTotal += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
                                        curCreditTotalPeriod += (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1);
										// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
									}
									else
									{
                                        vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
                                        curCreditTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
                                        curCreditTotalPeriod += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance");
									}
								}
							}
							else
							{
                                if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
                                    curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
                                    curNetTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
								}
                                else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
                                    curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
                                    curNetTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
								}
                                else if (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00"));
                                    curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
                                    curNetTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")));
								}
								else
								{
                                    vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00"));
                                    curNetTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
                                    curNetTotalPeriod += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) * -1);
								}
							}
						}
                        else if (rsDetailInfo.Get_Fields("Type") == "E")
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
                                vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, "E");
                                vs1.TextMatrix(vs1.Rows - 1, JournalCol, "E " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
									}
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, JournalCol, "E " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
									{
										rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
										if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
										}
									}
								}
							}
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								//FC:FINAL:MSH - can't implicitly convert decimal to double(same with issue #625)
								revItem.Debit = rsDetailInfo.Get_Fields_Double("Amount");
								revItem.Credit = 0;
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								//FC:FINAL:MSH - can't implicitly convert decimal to double(same with issue #625)
								revItem.Credit = rsDetailInfo.Get_Fields_Double("Amount");
								revItem.Debit = 0;
							}
							if (YTDDCFlag)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0)
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curDebitTotal += rsDetailInfo.Get_Fields("Amount");
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curDebitTotalPeriod += rsDetailInfo.Get_Fields("Amount");
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCreditTotal += rsDetailInfo.Get_Fields("Amount");
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCreditTotalPeriod += rsDetailInfo.Get_Fields("Amount");
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curNetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curNetTotalPeriod += (rsDetailInfo.Get_Fields("Amount") * -1);
							}
						}
						else
						{
							if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("PostedDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, PostedCol, "*");
								}
								vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, PeriodCol, Strings.Format(rsDetailInfo.Get_Fields("Period"), "00"));
								vs1.TextMatrix(vs1.Rows - 1, RCBCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")));
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, JournalCol, rsDetailInfo.Get_Fields("Type") + " " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, WarrantCol, Strings.Format(rsDetailInfo.Get_Fields("Warrant"), "0000"));
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber")));
								if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
								{
									rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
									if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
									{
										vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
									}
								}
							}
							else
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("Status")) == "P")
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, TransCol, "*" + Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy"));
								}
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								vs1.TextMatrix(vs1.Rows - 1, JournalCol, rsDetailInfo.Get_Fields("Type") + " " + Strings.Format(rsDetailInfo.Get_Fields("JournalNumber"), "0000"));
								vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")));
								if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "L")
								{
									if (Conversion.Val(rsDetailInfo.Get_Fields_Int32("VendorNumber")) != 0)
									{
										rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
										if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName"));
										}
										else
										{
											vs1.TextMatrix(vs1.Rows - 1, VendorCol, Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN");
										}
									}
								}
							}
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
								{
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									revItem.Debit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount"));
									revItem.Credit = 0;
								}
								// if the amount is less than 0 and it is a cash receipt journal or a general journal entry with an RCB of C then it is a debit
							}
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								revItem.Debit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount"));
								revItem.Credit = 0;
								// if the amount is more than 0 and it is a cash disbursement or a general journal entry with an RCB of C than it is a Credit
							}
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								revItem.Credit = (FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount")) * -1);
								revItem.Debit = 0;
								// if the amount is less than 0 and it is a general journal entry with an RCB of anything but C then it is a Credit
							}
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
							{
								if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
								{
								}
								else
								{
									// vs1.TextMatrix(vs1.rows - 1, YTDCreditCol) = format(rsDetailInfo.Fields["Amount"] * -1, "#,##0.00")
									// vs1.TextMatrix(vs1.rows - 1, YTDDebitCol) = format(0, "#,##0.00")
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									revItem.Credit = FCConvert.ToDouble(rsDetailInfo.Get_Fields("Amount")) * -1;
									revItem.Debit = 0;
								}
							}
							if (YTDDCFlag)
							{
								// if the amount is more than 0 and it is a general journal entry with an RCB of anything but c it is a debit
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curDebitTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curDebitTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
									// if the amount is less than 0 and it is a cash receipt journal or a general journal entry with an RCB of C then it is a debit
								}
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curDebitTotal += (rsDetailInfo.Get_Fields("Amount"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curDebitTotalPeriod += (rsDetailInfo.Get_Fields("Amount"));
									// if the amount is more than 0 and it is a cash disbursement or a general journal entry with an RCB of C than it is a Credit
								}
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
									vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCreditTotal += (rsDetailInfo.Get_Fields("Amount"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curCreditTotalPeriod += (rsDetailInfo.Get_Fields("Amount"));
									// if the amount is less than 0 and it is a general journal entry with an RCB of anything but C then it is a Credit
								}
											// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
											else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
										vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curCreditTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curCreditTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
								}
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curNetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curNetTotalPeriod += (rsDetailInfo.Get_Fields("Amount") * -1);
									}
								}
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curNetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curNetTotalPeriod += (rsDetailInfo.Get_Fields("Amount") * -1);
								}
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C")
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curNetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									curNetTotalPeriod += (rsDetailInfo.Get_Fields("Amount") * -1);
								}
											// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
											else if (FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C")
								{
									if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "B")
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotal += rsDetailInfo.Get_Fields("Amount");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curBudgetTotalPeriod += rsDetailInfo.Get_Fields("Amount");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format((rsDetailInfo.Get_Fields("Amount")) * -1, "#,##0.00"));
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curNetTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curNetTotalPeriod += (rsDetailInfo.Get_Fields("Amount") * -1);
									}
								}
							}
						}
						if (modAccountTitle.Statics.RevDivFlag)
						{
							vs1.RowOutlineLevel(vs1.Rows - 1, 2);
						}
						else
						{
							vs1.RowOutlineLevel(vs1.Rows - 1, 3);
						}
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
						detReport.Details.AddItem(revItem);
						curUncollectedBalance += FCConvert.ToDecimal(revItem.Debit - revItem.Credit);
						CheckNext:
						;
						rsDetailInfo.MoveNext();
						if (rsDetailInfo.EndOfFile())
						{
							break;
						}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					}
					while (rsDetailInfo.Get_Fields("Account") == strAccount);
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				else if (string.Compare(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), strAccount) < 0)
				{
					rsDetailInfo.MoveNext();
					if (rsDetailInfo.EndOfFile() != true)
					{
						goto CheckAgain;
					}
					else
					{
						goto NoDetailFound;
					}
				}
			}
			else
			{
				if (PendingSummaryFlag)
				{
					vs1.IsSubtotal(vs1.Rows - 1, true);
				}
			}
			NoDetailFound:
			;
			if (!(revItem == null))
			{
				revItem.CurrentBudget = FCConvert.ToDouble(curNetBegin);
				revItem.UncollectedBalance = FCConvert.ToDouble(curUncollectedBalance);
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
			{
				if (intPeriod != -1)
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, MonthCalc(intPeriod));
					vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curBudgetTotalPeriod, "#,##0.00"));
					if (YTDDCFlag)
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotalPeriod, "#,##0.00"));
						vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotalPeriod * -1, "#,##0.00"));
						curPeriodBalance += curDebitTotalPeriod + curCreditTotalPeriod;
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curNetTotalPeriod, "#,##0.00"));
						curPeriodBalance -= curNetTotalPeriod;
					}
					vs1.TextMatrix(vs1.Rows - 1, BalanceCol, Strings.Format(curPeriodBalance, "#,##0.00"));
					curDebitTotalPeriod = 0;
					curCreditTotalPeriod = 0;
					curNetTotalPeriod = 0;
					curBudgetTotalPeriod = 0;
					if (modAccountTitle.Statics.RevDivFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					}
					else
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 3);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				}
			}
			if (PendingSummaryFlag)
			{
				dblPendingTotal = GetPending();
				if (dblPendingTotal != 0)
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Pending Activity");
					if (YTDDCFlag)
					{
						if (dblPendingTotal > 0)
						{
							vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(dblPendingTotal, "#,##0.00"));
							vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(0, "#,##0.00"));
							curDebitTotal += FCConvert.ToDecimal(dblPendingTotal);
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(dblPendingTotal * -1, "#,##0.00"));
							vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(0, "#,##0.00"));
							curCreditTotal += FCConvert.ToDecimal(dblPendingTotal);
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(dblPendingTotal, "#,##0.00"));
						curNetTotal += FCConvert.ToDecimal(dblPendingTotal);
					}
					if (modAccountTitle.Statics.RevDivFlag)
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 2);
					}
					else
					{
						vs1.RowOutlineLevel(vs1.Rows - 1, 3);
					}
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				}
			}
			if (blnRecordsFound || PendingSummaryFlag)
			{
				vs1.Rows += 1;
				vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, "Revenue.....");
				if (YTDDCFlag)
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDDebitCol, Strings.Format(curDebitTotal, "#,##0.00"));
					vs1.TextMatrix(vs1.Rows - 1, YTDCreditCol, Strings.Format(curCreditTotal * -1, "#,##0.00"));
				}
				else
				{
					vs1.TextMatrix(vs1.Rows - 1, YTDNetCol, Strings.Format(curNetTotal, "#,##0.00"));
				}
				vs1.TextMatrix(vs1.Rows - 1, NetBudgetCol, Strings.Format(curBudgetTotal, "#,##0.00"));
				if (modAccountTitle.Statics.RevDivFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				}
				else
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 3);
				}
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
				vs1.Rows += 1;
				if (modAccountTitle.Statics.RevDivFlag)
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				}
				else
				{
					vs1.RowOutlineLevel(vs1.Rows - 1, 3);
				}
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			}
			else
			{
				vs1.IsSubtotal(vs1.Rows - 1, true);
			}
		}

		public bool IsTotalRow(int lngRow)
		{
			bool IsTotalRow = false;
			if (vs1.TextMatrix(lngRow, DescriptionCol) == "Revenue....." || vs1.TextMatrix(lngRow, DescriptionCol) == "Division...." || vs1.TextMatrix(lngRow, DescriptionCol) == "Department..")
			{
				IsTotalRow = true;
			}
			else
			{
				IsTotalRow = false;
			}
			return IsTotalRow;
		}

		public bool IsTotalRowToBold(int lngRow)
		{
			bool IsTotalRowToBold = false;
			if (vs1.TextMatrix(lngRow, DescriptionCol) == "Revenue....." || vs1.TextMatrix(lngRow, DescriptionCol) == "Division...." || vs1.TextMatrix(lngRow, DescriptionCol) == "Department.." || vs1.TextMatrix(lngRow, DescriptionCol) == "January" || vs1.TextMatrix(lngRow, DescriptionCol) == "February" || vs1.TextMatrix(lngRow, DescriptionCol) == "March" || vs1.TextMatrix(lngRow, DescriptionCol) == "April" || vs1.TextMatrix(lngRow, DescriptionCol) == "May" || vs1.TextMatrix(lngRow, DescriptionCol) == "June" || vs1.TextMatrix(lngRow, DescriptionCol) == "July" || vs1.TextMatrix(lngRow, DescriptionCol) == "August" || vs1.TextMatrix(lngRow, DescriptionCol) == "September" || vs1.TextMatrix(lngRow, DescriptionCol) == "October" || vs1.TextMatrix(lngRow, DescriptionCol) == "November" || vs1.TextMatrix(lngRow, DescriptionCol) == "December")
			{
				IsTotalRowToBold = true;
			}
			else
			{
				IsTotalRowToBold = false;
			}
			return IsTotalRowToBold;
		}

		public bool IsDetailRow(int lngRow)
		{
			bool IsDetailRow = false;
			if (modAccountTitle.Statics.RevDivFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 2)
				{
					IsDetailRow = true;
				}
				else
				{
					IsDetailRow = false;
				}
			}
			else
			{
				if (vs1.RowOutlineLevel(lngRow) == 3)
				{
					IsDetailRow = true;
				}
				else
				{
					IsDetailRow = false;
				}
			}
			return IsDetailRow;
		}

		private void GetRowSubtotals()
		{
			// vbPorter upgrade warning: curTotals As Decimal	OnWrite(short, Decimal)
			Decimal[,] curTotals = new Decimal[3 + 1, 3 + 1];
			int counter;
			int counter2;
			int temp = 0;
			for (counter = 0; counter <= 3; counter++)
			{
				for (counter2 = 0; counter2 <= 3; counter2++)
				{
					curTotals[counter, counter2] = 0;
				}
			}
			strBalances = new string[vs1.Rows + 1 + 1];
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (!IsTotalRow(counter) && !IsDetailRow(counter))
				{
					strBalances[counter] = "B" + FCConvert.ToString(vs1.RowOutlineLevel(counter));
				}
				else if (IsTotalRow(counter))
				{
					strBalances[counter] = "E" + FCConvert.ToString(vs1.RowOutlineLevel(counter) - 1);
				}
				else
				{
					strBalances[counter] = "";
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsTotalRow(counter) && IsDetailRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					if (vs1.TextMatrix(counter, NetBudgetCol) != "")
					{
						vs1.TextMatrix(counter, NetBudgetCol, Strings.Format(FCConvert.ToDecimal(vs1.TextMatrix(temp, NetBudgetCol)) + FCConvert.ToDecimal(vs1.TextMatrix(counter, NetBudgetCol)), "#,##0.00"));
					}
					else
					{
						vs1.TextMatrix(counter, NetBudgetCol, vs1.TextMatrix(temp, NetBudgetCol));
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(counter, BalanceCol, FCConvert.ToString(FCConvert.ToDouble(vs1.TextMatrix(counter, NetBudgetCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, YTDNetCol))));
					}
					else
					{
						vs1.TextMatrix(counter, BalanceCol, FCConvert.ToString(FCConvert.ToDouble(vs1.TextMatrix(counter, NetBudgetCol)) + FCConvert.ToDouble(vs1.TextMatrix(counter, YTDDebitCol)) - FCConvert.ToDouble(vs1.TextMatrix(counter, YTDCreditCol))));
					}
				}
			}
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (IsTotalRow(counter) && IsDetailRow(counter))
				{
					for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
					{
						// If counter2 = NetBudgetCol Then
						temp = FindBeginningBalanceRow(ref counter);
						curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += (FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2)) - FCConvert.ToDecimal(vs1.TextMatrix(temp, counter2)));
						// Else
						// curTotals(vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol) = curTotals(vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol) + vs1.TextMatrix(counter, counter2)
						// End If
					}
				}
				else if (IsTotalRow(counter))
				{
					temp = FindBeginningBalanceRow(ref counter);
					// SetNonActivityTotals vs1.RowOutlineLevel(counter), temp + 1, counter - 1
					for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
					{
						if (vs1.TextMatrix(temp, counter2) != "")
						{
							vs1.TextMatrix(counter, counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol] + FCConvert.ToDecimal(vs1.TextMatrix(temp, counter2))));
							curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol];
							curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] = 0;
						}
						else
						{
							vs1.TextMatrix(counter, counter2, FCConvert.ToString(curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol]));
							curTotals[vs1.RowOutlineLevel(counter) - 1, counter2 - NetBudgetCol] += curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] + curNonActivityTotals[counter2 - NetBudgetCol];
							curTotals[vs1.RowOutlineLevel(counter), counter2 - NetBudgetCol] = 0;
						}
					}
				}
			}
		}

		private int FindBeginningBalanceRow(ref int lngRow)
		{
			int FindBeginningBalanceRow = 0;
			int tempLevel;
			int counter;
			tempLevel = vs1.RowOutlineLevel(lngRow) - 1;
			for (counter = lngRow - 1; counter >= 2; counter--)
			{
				if (vs1.RowOutlineLevel(counter) == tempLevel)
				{
					FindBeginningBalanceRow = counter;
					break;
				}
			}
			return FindBeginningBalanceRow;
		}

		private int FindEndingBalanceRow_6(int lngRow, string strBalance)
		{
			return FindEndingBalanceRow(lngRow, ref strBalance);
		}

		private int FindEndingBalanceRow(int lngRow, ref string strBalance)
		{
			int FindEndingBalanceRow = 0;
			string tempLevel;
			int counter;
			tempLevel = strBalance;
			for (counter = lngRow + 1; counter <= vs1.Rows - 1; counter++)
			{
				if (strBalances[counter] == tempLevel)
				{
					FindEndingBalanceRow = counter;
					break;
				}
			}
			return FindEndingBalanceRow;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				GetYTDDebit = 0;
				return GetYTDDebit;
			}
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			GetYTDDebit += GetEncumbrance();
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				GetYTDCredit = 0;
				return GetYTDCredit;
			}
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (CurrentDivision == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (CurrentRevenue == "")
				{
					if (rsDivSummaryInfo.EndOfFile() != true && rsDivSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDivSummaryInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDivSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Division, Revenue", CurrentDepartment + "," + CurrentDivision + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			else
			{
				if (CurrentRevenue == "")
				{
					if (rsDeptSummaryInfo.EndOfFile() != true && rsDeptSummaryInfo.BeginningOfFile() != true)
					{
						if (rsDeptSummaryInfo.FindFirstRecord("Department", CurrentDepartment))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsDeptSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else
				{
					if (rsRevSummaryInfo.EndOfFile() != true && rsRevSummaryInfo.BeginningOfFile() != true)
					{
						if (rsRevSummaryInfo.FindFirstRecord2("Department, Revenue", CurrentDepartment + "," + CurrentRevenue, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsRevSummaryInfo.Get_Fields("PostedCreditsTotal")) * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() - GetYTDCredit();
			return GetYTDNet;
		}

		private void SetNonActivityTotals(ref short intOutlineLevel, ref int intStart, ref int intEnd)
		{
			int counter;
			int counter2;
			for (counter = 0; counter <= 3; counter++)
			{
				curNonActivityTotals[counter] = 0;
			}
			for (counter = intStart; counter <= intEnd; counter++)
			{
				if (strBalances[counter] == "B" + FCConvert.ToString(intOutlineLevel))
				{
					if (strBalances[counter + 1] == "B" + FCConvert.ToString(intOutlineLevel) || counter == intEnd)
					{
						for (counter2 = NetBudgetCol; counter2 <= BalanceCol; counter2++)
						{
							if (vs1.TextMatrix(counter, counter2) != "")
							{
								curNonActivityTotals[counter2 - NetBudgetCol] += FCConvert.ToDecimal(vs1.TextMatrix(counter, counter2));
							}
						}
					}
				}
			}
		}

		public bool IsLowRow(int lngRow)
		{
			bool IsLowRow = false;
			if (modAccountTitle.Statics.RevDivFlag)
			{
				if (vs1.RowOutlineLevel(lngRow) == 1)
				{
					IsLowRow = true;
				}
				else
				{
					IsLowRow = false;
				}
			}
			else
			{
				if (vs1.RowOutlineLevel(lngRow) == 2)
				{
					IsLowRow = true;
				}
				else
				{
					IsLowRow = false;
				}
			}
			return IsLowRow;
		}
	}
}
