﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for MDIParent.
	/// </summary>
	partial class MDIParent : BaseForm
	{
		public fecherFoundation.FCPictureBox picArchive;
		public fecherFoundation.FCFileListBox filADrive;
		public FCCommonDialog CommonDialog1;
		public fecherFoundation.FCPictureBox imgArchive;
		public fecherFoundation.FCGrid GRID;
		public Wisej.Web.ImageList ImageList1;
		public Wisej.Web.StatusBar StatusBar1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel1;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel2;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel3;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel4;
		private Wisej.Web.StatusBarPanel StatusBar1_Panel5;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFExit;
		public fecherFoundation.FCToolStripMenuItem mnuForms;
		public fecherFoundation.FCToolStripMenuItem mnuFileFormsPrintForms;
		public fecherFoundation.FCToolStripMenuItem mnuOMaxForms;
		public fecherFoundation.FCToolStripMenuItem mnuHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRedBookHelp;
		public fecherFoundation.FCToolStripMenuItem mnuBudgetaryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCashReceiptsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuClerkHelp;
		public fecherFoundation.FCToolStripMenuItem mnuCodeEnforcementHelp;
		public fecherFoundation.FCToolStripMenuItem mnuEnhanced911Help;
		public fecherFoundation.FCToolStripMenuItem mnuGeneralEntryHelp;
		public fecherFoundation.FCToolStripMenuItem mnuFixedAssetsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuMotorVehicleRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPayrollHelp;
		public fecherFoundation.FCToolStripMenuItem mnuPersonalPropertyHelp;
		public fecherFoundation.FCToolStripMenuItem mnuRealEstateHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuTaxCollectionsHelp;
		public fecherFoundation.FCToolStripMenuItem mnuUtilityBillingHelp;
		public fecherFoundation.FCToolStripMenuItem mnuVoterRegistrationHelp;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuHAbout;
		public fecherFoundation.FCToolStripMenuItem mnuCustom;
		public fecherFoundation.FCToolStripMenuItem mnuCustomRevenueReport;
		public fecherFoundation.FCToolStripMenuItem mnuCustomCheckRecAP;
		public fecherFoundation.FCToolStripMenuItem mnuCustomCheckRecPY;
		public fecherFoundation.FCToolStripMenuItem mnuCustomPayrollImport;
		public fecherFoundation.FCToolStripMenuItem mnuBoothBayCheckRecExport;
        public fecherFoundation.FCToolStripMenuItem mnuSimpleCheckRecExport;
		public fecherFoundation.FCToolStripMenuItem mnuCustomCheckRecExport;
		public fecherFoundation.FCToolStripMenuItem mnuCustomProgramsCalaisBDSetDBLocation;
		public fecherFoundation.FCToolStripMenuItem mnuCustomProgramsDaytonImport;
		public fecherFoundation.FCToolStripMenuItem mnuCustomProgramsOronoPayrollImport;
		public fecherFoundation.FCToolStripMenuItem mnuCustomProgramsLincolnCountyPayrollImport;
		public fecherFoundation.FCToolStripMenuItem mnuSomersetAPCustomCheckRec;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MDIParent));
			this.picArchive = new fecherFoundation.FCPictureBox();
			this.filADrive = new fecherFoundation.FCFileListBox();
			this.CommonDialog1 = new FCCommonDialog();
			this.imgArchive = new fecherFoundation.FCPictureBox();
			this.GRID = new fecherFoundation.FCGrid();
			this.components = new System.ComponentModel.Container();
			this.ImageList1 = new Wisej.Web.ImageList();
			this.StatusBar1 = new Wisej.Web.StatusBar();
			this.StatusBar1_Panel1 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel2 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel3 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel4 = new Wisej.Web.StatusBarPanel();
			this.StatusBar1_Panel5 = new Wisej.Web.StatusBarPanel();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileFormsPrintForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuOMaxForms = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRedBookHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBudgetaryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCashReceiptsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClerkHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCodeEnforcementHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEnhanced911Help = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGeneralEntryHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFixedAssetsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuMotorVehicleRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayrollHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPersonalPropertyHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuRealEstateHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuTaxCollectionsHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUtilityBillingHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuVoterRegistrationHelp = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuHAbout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustom = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomRevenueReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomCheckRecAP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomCheckRecPY = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomPayrollImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBoothBayCheckRecExport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSimpleCheckRecExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomCheckRecExport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomProgramsCalaisBDSetDBLocation = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomProgramsDaytonImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomProgramsOronoPayrollImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuCustomProgramsLincolnCountyPayrollImport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSomersetAPCustomCheckRec = new fecherFoundation.FCToolStripMenuItem();
			//this.SuspendLayout();
			//
			// picArchive
			//
			this.picArchive.Controls.Add(this.filADrive);
			this.picArchive.Controls.Add(this.imgArchive);
			this.picArchive.Name = "picArchive";
			this.picArchive.TabIndex = 2;
			this.picArchive.Location = new System.Drawing.Point(144, 24);
			this.picArchive.Size = new System.Drawing.Size(599, 423);
			this.picArchive.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.picArchive.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
			this.picArchive.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left));
			//
			// filADrive
			//
			this.filADrive.Name = "filADrive";
			this.filADrive.Visible = false;
			this.filADrive.TabIndex = 3;
			this.filADrive.Location = new System.Drawing.Point(115, 10);
			this.filADrive.Size = new System.Drawing.Size(92, 98);
			//
			// CommonDialog1
			//
			//
			// imgArchive
			//
			this.imgArchive.Name = "imgArchive";
			this.imgArchive.Visible = false;
			this.imgArchive.Location = new System.Drawing.Point(111, 131);
			this.imgArchive.Size = new System.Drawing.Size(256, 82);
			this.imgArchive.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgArchive.BackColor = System.Drawing.SystemColors.Control;
			this.imgArchive.Image = ((System.Drawing.Image)(resources.GetObject("imgArchive.Image")));
			this.imgArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// GRID
			//
			this.GRID.Name = "GRID";
			this.GRID.Enabled = true;
			this.GRID.TabIndex = 1;
			this.GRID.Location = new System.Drawing.Point(0, 24);
			this.GRID.Size = new System.Drawing.Size(144, 423);
			//this.GRID.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("GRID.OcxState")));
			//this.GRID.KeyDown += new KeyEventHandler(this.GRID_KeyDownEvent);
			//this.GRID.CellMouseDown += new DataGridViewCellMouseEventHandler(this.GRID_MouseDownEvent);
			//this.GRID.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GRID_MouseMoveEvent);
			//
			// ImageList1
			//
			this.ImageList1.ImageSize = new System.Drawing.Size(17, 19);
			//this.ImageList1.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
			this.ImageList1.Images.SetKeyName(0, "Clear");
			this.ImageList1.Images.SetKeyName(1, "Left");
			this.ImageList1.Images.SetKeyName(2, "Right");
			//
			// StatusBar1
			//
			this.StatusBar1.Panels.AddRange(new Wisej.Web.StatusBarPanel[] {
				this.StatusBar1_Panel1,
				this.StatusBar1_Panel2,
				this.StatusBar1_Panel3,
				this.StatusBar1_Panel4,
				this.StatusBar1_Panel5
			});
			this.StatusBar1.Name = "StatusBar1";
			this.StatusBar1.TabIndex = 0;
			this.StatusBar1.Location = new System.Drawing.Point(0, 447);
			this.StatusBar1.Size = new System.Drawing.Size(599, 19);
			this.StatusBar1.ShowPanels = true;
			this.StatusBar1.SizingGrip = false;
			//this.StatusBar1.Font = new System.Drawing.Font("MS Sans Serif", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.StatusBar1.PanelClick += new Wisej.Web.StatusBarPanelClickEventHandler(this.StatusBar1_PanelClick);
			//
			// Panel1
			//
			this.StatusBar1_Panel1.Text = "TRIO Software Corporation";
			this.StatusBar1_Panel1.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel1.Width = 166;
			this.StatusBar1_Panel1.MinWidth = 142;
			//
			// Panel2
			//
			this.StatusBar1_Panel2.Text = "";
			this.StatusBar1_Panel2.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Contents;
			this.StatusBar1_Panel2.Width = 114;
			this.StatusBar1_Panel2.MinWidth = 115;
			//
			// Panel3
			//
			this.StatusBar1_Panel3.Text = "";
			this.StatusBar1_Panel3.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel3.BorderStyle = Wisej.Web.StatusBarPanelBorderStyle.None;
			this.StatusBar1_Panel3.AutoSize = Wisej.Web.StatusBarPanelAutoSize.Spring;
			this.StatusBar1_Panel3.Width = 159;
			//
			// Panel4
			//
			this.StatusBar1_Panel4.Text = "";
			this.StatusBar1_Panel4.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel4.Width = 68;
			this.StatusBar1_Panel4.MinWidth = 68;
			//
			// Panel5
			//
			this.StatusBar1_Panel5.Text = "";
			this.StatusBar1_Panel5.Alignment = Wisej.Web.HorizontalAlignment.Right;
			this.StatusBar1_Panel5.Width = 68;
			this.StatusBar1_Panel5.MinWidth = 68;
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuFExit
			//
			this.mnuFExit.Name = "mnuFExit";
			this.mnuFExit.Text = "Exit";
			this.mnuFExit.Click += new System.EventHandler(this.mnuFExit_Click);
			//
			// mnuForms
			//
			this.mnuForms.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileFormsPrintForms,
				this.mnuOMaxForms
			});
			this.mnuForms.Name = "mnuForms";
			this.mnuForms.Text = "Forms";
			//
			// mnuFileFormsPrintForms
			//
			this.mnuFileFormsPrintForms.Name = "mnuFileFormsPrintForms";
			this.mnuFileFormsPrintForms.Text = "Print Form(s)";
			this.mnuFileFormsPrintForms.Click += new System.EventHandler(this.mnuFileFormsPrintForms_Click);
			//
			// mnuOMaxForms
			//
			this.mnuOMaxForms.Name = "mnuOMaxForms";
			this.mnuOMaxForms.Text = "Maximize Forms";
			this.mnuOMaxForms.Click += new System.EventHandler(this.mnuOMaxForms_Click);
			//
			// mnuHelp
			//
			this.mnuHelp.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuRedBookHelp,
				this.mnuBudgetaryHelp,
				this.mnuCashReceiptsHelp,
				this.mnuClerkHelp,
				this.mnuCodeEnforcementHelp,
				this.mnuEnhanced911Help,
				this.mnuGeneralEntryHelp,
				this.mnuFixedAssetsHelp,
				this.mnuMotorVehicleRegistrationHelp,
				this.mnuPayrollHelp,
				this.mnuPersonalPropertyHelp,
				this.mnuRealEstateHelp,
				this.mnuTaxBillingHelp,
				this.mnuTaxCollectionsHelp,
				this.mnuUtilityBillingHelp,
				this.mnuVoterRegistrationHelp,
				this.mnuSeperator,
				this.mnuHAbout
			});
			this.mnuHelp.Name = "mnuHelp";
			this.mnuHelp.Text = "Help";
			//
			// mnuRedBookHelp
			//
			this.mnuRedBookHelp.Name = "mnuRedBookHelp";
			this.mnuRedBookHelp.Text = "Blue Book";
			this.mnuRedBookHelp.Click += new System.EventHandler(this.mnuRedBookHelp_Click);
			//
			// mnuBudgetaryHelp
			//
			this.mnuBudgetaryHelp.Name = "mnuBudgetaryHelp";
			this.mnuBudgetaryHelp.Text = "Budgetary";
			this.mnuBudgetaryHelp.Click += new System.EventHandler(this.mnuBudgetaryHelp_Click);
			//
			// mnuCashReceiptsHelp
			//
			this.mnuCashReceiptsHelp.Name = "mnuCashReceiptsHelp";
			this.mnuCashReceiptsHelp.Text = "Cash Receipts";
			this.mnuCashReceiptsHelp.Click += new System.EventHandler(this.mnuCashReceiptsHelp_Click);
			//
			// mnuClerkHelp
			//
			this.mnuClerkHelp.Name = "mnuClerkHelp";
			this.mnuClerkHelp.Text = "Clerk";
			this.mnuClerkHelp.Click += new System.EventHandler(this.mnuClerkHelp_Click);
			//
			// mnuCodeEnforcementHelp
			//
			this.mnuCodeEnforcementHelp.Name = "mnuCodeEnforcementHelp";
			this.mnuCodeEnforcementHelp.Text = "Code Enforcement";
			this.mnuCodeEnforcementHelp.Click += new System.EventHandler(this.mnuCodeEnforcementHelp_Click);
			//
			// mnuEnhanced911Help
			//
			this.mnuEnhanced911Help.Name = "mnuEnhanced911Help";
			this.mnuEnhanced911Help.Text = "Enhanced 911";
			this.mnuEnhanced911Help.Click += new System.EventHandler(this.mnuEnhanced911Help_Click);
			//
			// mnuGeneralEntryHelp
			//
			this.mnuGeneralEntryHelp.Name = "mnuGeneralEntryHelp";
			this.mnuGeneralEntryHelp.Text = "General Entry";
			this.mnuGeneralEntryHelp.Click += new System.EventHandler(this.mnuGeneralEntryHelp_Click);
			//
			// mnuFixedAssetsHelp
			//
			this.mnuFixedAssetsHelp.Name = "mnuFixedAssetsHelp";
			this.mnuFixedAssetsHelp.Text = "Fixed Assets";
			this.mnuFixedAssetsHelp.Click += new System.EventHandler(this.mnuFixedAssetsHelp_Click);
			//
			// mnuMotorVehicleRegistrationHelp
			//
			this.mnuMotorVehicleRegistrationHelp.Name = "mnuMotorVehicleRegistrationHelp";
			this.mnuMotorVehicleRegistrationHelp.Text = "Motor Vehicle Registration";
			this.mnuMotorVehicleRegistrationHelp.Click += new System.EventHandler(this.mnuMotorVehicleRegistrationHelp_Click);
			//
			// mnuPayrollHelp
			//
			this.mnuPayrollHelp.Name = "mnuPayrollHelp";
			this.mnuPayrollHelp.Text = "Payroll";
			this.mnuPayrollHelp.Click += new System.EventHandler(this.mnuPayrollHelp_Click);
			//
			// mnuPersonalPropertyHelp
			//
			this.mnuPersonalPropertyHelp.Name = "mnuPersonalPropertyHelp";
			this.mnuPersonalPropertyHelp.Text = "Personal Property";
			this.mnuPersonalPropertyHelp.Click += new System.EventHandler(this.mnuPersonalPropertyHelp_Click);
			//
			// mnuRealEstateHelp
			//
			this.mnuRealEstateHelp.Name = "mnuRealEstateHelp";
			this.mnuRealEstateHelp.Text = "Real Estate";
			this.mnuRealEstateHelp.Click += new System.EventHandler(this.mnuRealEstateHelp_Click);
			//
			// mnuTaxBillingHelp
			//
			this.mnuTaxBillingHelp.Name = "mnuTaxBillingHelp";
			this.mnuTaxBillingHelp.Text = "Tax Billing";
			this.mnuTaxBillingHelp.Click += new System.EventHandler(this.mnuTaxBillingHelp_Click);
			//
			// mnuTaxCollectionsHelp
			//
			this.mnuTaxCollectionsHelp.Name = "mnuTaxCollectionsHelp";
			this.mnuTaxCollectionsHelp.Text = "Tax Collections";
			this.mnuTaxCollectionsHelp.Click += new System.EventHandler(this.mnuTaxCollectionsHelp_Click);
			//
			// mnuUtilityBillingHelp
			//
			this.mnuUtilityBillingHelp.Name = "mnuUtilityBillingHelp";
			this.mnuUtilityBillingHelp.Text = "Utility Billing";
			this.mnuUtilityBillingHelp.Click += new System.EventHandler(this.mnuUtilityBillingHelp_Click);
			//
			// mnuVoterRegistrationHelp
			//
			this.mnuVoterRegistrationHelp.Name = "mnuVoterRegistrationHelp";
			this.mnuVoterRegistrationHelp.Text = "Voter Registration";
			this.mnuVoterRegistrationHelp.Click += new System.EventHandler(this.mnuVoterRegistrationHelp_Click);
			//
			// mnuSeperator
			//
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			//
			// mnuHAbout
			//
			this.mnuHAbout.Name = "mnuHAbout";
			this.mnuHAbout.Text = "About";
			this.mnuHAbout.Click += new System.EventHandler(this.mnuHAbout_Click);
			//
			// mnuCustom
			//
			this.mnuCustom.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuCustomRevenueReport,
				this.mnuCustomCheckRecAP,
				this.mnuCustomCheckRecPY,
				this.mnuCustomPayrollImport,
				this.mnuBoothBayCheckRecExport,
                this.mnuSimpleCheckRecExport,
				this.mnuCustomCheckRecExport,
				this.mnuCustomProgramsCalaisBDSetDBLocation,
				this.mnuCustomProgramsDaytonImport,
				this.mnuCustomProgramsOronoPayrollImport,
				this.mnuCustomProgramsLincolnCountyPayrollImport,
				this.mnuSomersetAPCustomCheckRec
			});
			this.mnuCustom.Name = "mnuCustom";
			this.mnuCustom.Text = "Custom Programs";
			//
			// mnuCustomRevenueReport
			//
			this.mnuCustomRevenueReport.Name = "mnuCustomRevenueReport";
			this.mnuCustomRevenueReport.Text = "Town Revenue Report";
			this.mnuCustomRevenueReport.Click += new System.EventHandler(this.mnuCustomRevenueReport_Click);
			//
			// mnuCustomCheckRecAP
			//
			this.mnuCustomCheckRecAP.Name = "mnuCustomCheckRecAP";
			this.mnuCustomCheckRecAP.Text = "Import AP Check Rec Information";
			this.mnuCustomCheckRecAP.Click += new System.EventHandler(this.mnuCustomCheckRecAP_Click);
			//
			// mnuCustomCheckRecPY
			//
			this.mnuCustomCheckRecPY.Name = "mnuCustomCheckRecPY";
			this.mnuCustomCheckRecPY.Text = "Import Payroll Check Rec Information";
			this.mnuCustomCheckRecPY.Click += new System.EventHandler(this.mnuCustomCheckRecPY_Click);
			//
			// mnuCustomPayrollImport
			//
			this.mnuCustomPayrollImport.Name = "mnuCustomPayrollImport";
			this.mnuCustomPayrollImport.Text = "Import Payroll Information";
			this.mnuCustomPayrollImport.Click += new System.EventHandler(this.mnuCustomPayrollImport_Click);
			//
			// mnuBoothBayCheckRecExport
			//
			this.mnuBoothBayCheckRecExport.Name = "mnuBoothBayCheckRecExport";
			this.mnuBoothBayCheckRecExport.Text = "Export Check Rec Information";
			this.mnuBoothBayCheckRecExport.Click += new System.EventHandler(this.mnuBoothBayCheckRecExport_Click);
            //
            // mnuSimpleCheckRecExport
            //
            this.mnuSimpleCheckRecExport.Name = "mnuSimpleCheckRecExport";
            this.mnuSimpleCheckRecExport.Text = "Export Simple Check Rec Information";
            this.mnuSimpleCheckRecExport.Click += new System.EventHandler(this.mnuSimpleCheckRecExport_Click);
			//
			// mnuCustomCheckRecExport
			//
			this.mnuCustomCheckRecExport.Name = "mnuCustomCheckRecExport";
			this.mnuCustomCheckRecExport.Text = "Export Check Rec Information";
			this.mnuCustomCheckRecExport.Click += new System.EventHandler(this.mnuCustomCheckRecExport_Click);
			//
			// mnuCustomProgramsCalaisBDSetDBLocation
			//
			this.mnuCustomProgramsCalaisBDSetDBLocation.Name = "mnuCustomProgramsCalaisBDSetDBLocation";
			this.mnuCustomProgramsCalaisBDSetDBLocation.Text = "Set Water Database Location";
			this.mnuCustomProgramsCalaisBDSetDBLocation.Click += new System.EventHandler(this.mnuCustomProgramsCalaisBDSetDBLocation_Click);
			//
			// mnuCustomProgramsDaytonImport
			//
			this.mnuCustomProgramsDaytonImport.Name = "mnuCustomProgramsDaytonImport";
			this.mnuCustomProgramsDaytonImport.Text = "Import Journal Information";
			this.mnuCustomProgramsDaytonImport.Click += new System.EventHandler(this.mnuCustomProgramsDaytonImport_Click);
			//
			// mnuCustomProgramsOronoPayrollImport
			//
			this.mnuCustomProgramsOronoPayrollImport.Name = "mnuCustomProgramsOronoPayrollImport";
			this.mnuCustomProgramsOronoPayrollImport.Text = "Import Payroll Information";
			this.mnuCustomProgramsOronoPayrollImport.Click += new System.EventHandler(this.mnuCustomProgramsOronoPayrollImport_Click);
			//
			// mnuCustomProgramsLincolnCountyPayrollImport
			//
			this.mnuCustomProgramsLincolnCountyPayrollImport.Name = "mnuCustomProgramsLincolnCountyPayrollImport";
			this.mnuCustomProgramsLincolnCountyPayrollImport.Text = "Import Payroll Information";
			this.mnuCustomProgramsLincolnCountyPayrollImport.Click += new System.EventHandler(this.mnuCustomProgramsLincolnCountyPayrollImport_Click);
			//
			// mnuSomersetAPCustomCheckRec
			//
			this.mnuSomersetAPCustomCheckRec.Name = "mnuSomersetAPCustomCheckRec";
			this.mnuSomersetAPCustomCheckRec.Text = "Import AP Check Rec Information";
			this.mnuSomersetAPCustomCheckRec.Click += new System.EventHandler(this.mnuSomersetAPCustomCheckRec_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile,
				this.mnuForms,
				this.mnuHelp,
				this.mnuCustom
			});
			//
			// MDIParent
			//
			//this.ClientSize = new System.Drawing.Size(599, 442);
			//this.ClientArea.Controls.Add(this.picArchive);
			//this.ClientArea.Controls.Add(this.GRID);
			//this.ClientArea.Controls.Add(this.StatusBar1);
			//this.Menu = this.MainMenu1;
			//this.IsMdiContainer = true;
			//this.Name = "MDIParent";
			//this.BackColor = System.Drawing.SystemColors.AppWorkspace;
			//this.MinimizeBox = true;
			//this.MaximizeBox = true;
			//this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			//this.WindowState = Wisej.Web.FormWindowState.Maximized;
			//this.Activated += new System.EventHandler(this.MDIParent_Activated);
			//this.Load += new System.EventHandler(this.MDIParent_Load);
			//this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.MDIParent_FormClosing);
			//this.Text = " Budgetary (Main)";
			this.picArchive.ResumeLayout(false);
			this.GRID.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			//this.ResumeLayout(false);
		}

     
        #endregion
    }
}
