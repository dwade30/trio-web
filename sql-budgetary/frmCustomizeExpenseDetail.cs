﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseDetail.
	/// </summary>
	public partial class frmCustomizeExpenseDetail : BaseForm
	{
		public frmCustomizeExpenseDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbDescriptions.SelectedIndex = 0;
			this.cmbPaperWidth.SelectedIndex = 0;
			this.cmbReportUse.SelectedIndex = 0;
			this.cmbAccountInformation.SelectedIndex = 1;
			this.cmbPendingActivity.Text = "Do Not Include";
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeExpenseDetail InstancePtr
		{
			get
			{
				return (frmCustomizeExpenseDetail)Sys.GetInstance(typeof(frmCustomizeExpenseDetail));
			}
		}

		protected frmCustomizeExpenseDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		public bool FromExp;
		bool blnSavedFormat;

		private void chkAllInclusive_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAllInclusive.CheckState == CheckState.Checked)
			{
				fraPreferences.Enabled = false;
				cmbDescriptions.SelectedIndex = 1;
				cmbPaperWidth.SelectedIndex = 1;
				cmbReportUse.SelectedIndex = 0;
			}
			else
			{
				fraPreferences.Enabled = true;
			}
		}

		private void chkShowLiquidatedEncumbranceActivity_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked)
			{
				cmbAccountInformation.SelectedIndex = 0;
			}
		}
		
		private void cmbAccountInformation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAccountInformation.SelectedIndex == 1 && chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked)
			{
                MessageBox.Show("You may only view Debits / Credits if you want to show liquidated encumbrance activity.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbAccountInformation.SelectedIndex = 0;
            }
		}

		private void frmCustomizeExpenseDetail_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnExpenseDetailEdit && !modBudgetaryMaster.Statics.blnExpenseDetailReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowAll")))
					{
						chkAllInclusive.CheckState = CheckState.Checked;
						fraPreferences.Enabled = false;
					}
					else
					{
						chkAllInclusive.CheckState = CheckState.Unchecked;
						fraPreferences.Enabled = true;
					}
					cmbReportUse.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Use")) == "P" ? 0 : 1;
					chkShowLiquidatedEncumbranceActivity.CheckState =
                        FCConvert.ToBoolean(
                            modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean(
                                "ShowLiquidatedEncumbranceActivity"))
                            ? CheckState.Checked
                            : CheckState.Unchecked;
					cmbPaperWidth.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("PaperWidth")) == "N" ? 0 : 1;
					cmbDescriptions.SelectedIndex = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("DescriptionLength")) == "L" ? 0 : 1;
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					cmbAccountInformation.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDDebitCredit")) ? 0 : 1;
					//cmbAccountInformation.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDNet")) ? 1 : 1;
					bool optPendingDetail = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingDetail"));
					bool optPendingSummary = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingSummary"));
					if (!optPendingDetail && !optPendingSummary)
					{
						cmbPendingActivity.Text = "Do Not Include";
					}
					else if (optPendingDetail)
					{
						cmbPendingActivity.Text = "Show Detail";
					}
					else 
					{
						cmbPendingActivity.Text = "Show Summary Only";
					}
				}
			}
		}

		private void frmCustomizeExpenseDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeExpenseDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeExpenseDetail.FillStyle	= 0;
			//frmCustomizeExpenseDetail.ScaleWidth	= 9300;
			//frmCustomizeExpenseDetail.ScaleHeight	= 7335;
			//frmCustomizeExpenseDetail.LinkTopic	= "Form2";
			//frmCustomizeExpenseDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromExp)
			{
				FromExp = false;
				frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnExpenseDetailReportEdit)
					{
						modBudgetaryMaster.Statics.blnExpenseDetailReportEdit = false;
						frmExpenseDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmExpenseDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnExpenseDetailReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnExpenseDetailEdit)
			{
				modBudgetaryMaster.Statics.blnExpenseDetailEdit = false;
				frmGetExpenseDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeExpenseDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM ExpenseDetailFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("YTDDebitCredit", cmbAccountInformation.SelectedIndex == 0);
					rs.Set_Fields("YTDNet", cmbAccountInformation.SelectedIndex == 1);
					if (cmbDescriptions.SelectedIndex == 0)
					{
						rs.Set_Fields("DescriptionLength", "L");
					}
					else
					{
						rs.Set_Fields("DescriptionLength", "S");
					}
					rs.Set_Fields("PendingDetail", cmbPendingActivity.Text == "Show Detail");
					rs.Set_Fields("PendingSummary", cmbPendingActivity.Text == "Show Summary Only");
                    rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                        chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                    rs.Set_Fields("ShowAll", chkAllInclusive.CheckState == CheckState.Checked);
                    rs.Set_Fields("Printer", "O");
					rs.Set_Fields("Font", "S");
                    rs.Set_Fields("PaperWidth", cmbPaperWidth.SelectedIndex == 1 ? "N" : "L");
                    rs.Set_Fields("Use", cmbReportUse.SelectedIndex == 0 ? "P" : "D");
                    rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ExpenseDetailFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("YTDDebitCredit", cmbAccountInformation.SelectedIndex == 0);
				rs.Set_Fields("YTDNet", cmbAccountInformation.SelectedIndex == 1);
                rs.Set_Fields("DescriptionLength", cmbDescriptions.SelectedIndex == 0 ? "L" : "S");
                rs.Set_Fields("ShowLiquidatedEncumbranceActivity",
                    chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
                rs.Set_Fields("ShowAll", chkAllInclusive.CheckState == CheckState.Checked);
                rs.Set_Fields("PendingDetail", cmbPendingActivity.Text == "Show Detail");
				rs.Set_Fields("PendingSummary", cmbPendingActivity.Text == "Show Summary Only");
				rs.Set_Fields("Printer", "O");
				rs.Set_Fields("Font", "S");
                rs.Set_Fields("PaperWidth", cmbPaperWidth.SelectedIndex == 1 ? "N" : "L");
                rs.Set_Fields("Use", cmbReportUse.SelectedIndex == 0 ? "P" : "D");
                rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}

	}
}
