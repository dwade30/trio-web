﻿//Fecher vbPorter - Version 1.0.0.27

using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmCheckRegister.
    /// </summary>
    public partial class frmCheckRegister : BaseForm
    {
        public frmCheckRegister()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCheckRegister InstancePtr
        {
            get
            {
                return (frmCheckRegister)Sys.GetInstance(typeof(frmCheckRegister));
            }
        }

        protected frmCheckRegister _InstancePtr;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By   Dave Wade
        // Date         4/26/02
        // This form will be used to print or reprint a check register
        // report of all the AP checks that were issued since the last
        // check register was run
        // ********************************************************
        public string strPrinterName = "";
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //public clsDRWrapper rsMultipleWarrants = new clsDRWrapper();
        public clsDRWrapper rsMultipleWarrants_AutoInitialized;

        public clsDRWrapper rsMultipleWarrants
        {
            get
            {
                if (rsMultipleWarrants_AutoInitialized == null)
                {
                    rsMultipleWarrants_AutoInitialized = new clsDRWrapper();
                }
                return rsMultipleWarrants_AutoInitialized;
            }
            set
            {
                rsMultipleWarrants_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cSettingUtility setUtil = new cSettingUtility();
        private cSettingUtility setUtil_AutoInitialized;

        private cSettingUtility setUtil
        {
            get
            {
                if (setUtil_AutoInitialized == null)
                {
                    setUtil_AutoInitialized = new cSettingUtility();
                }
                return setUtil_AutoInitialized;
            }
            set
            {
                setUtil_AutoInitialized = value;
            }
        }

        private void cmdProcess_Click(object sender, EventArgs e)
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            clsDRWrapper rsMasterJournal = new clsDRWrapper();
            int counter;
            int lngWarrant;
            clsDRWrapper rsAPJournal = new clsDRWrapper();
            clsDRWrapper rsEntries = new clsDRWrapper();
            Decimal curTotalAmount;

            try
            {
                if (cmbInitial.SelectedIndex == 0)
                {
                    rsInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ISNULL(ReportNumber, 0) = 0");
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        bool executePrintReports = false;
                        int journalNumber;

                        if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank")) == 0)
                        {
                            // kk09042015 trobd-1020  GetVariable returning "" if Banks were not set up
                            lngWarrant = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WarrantNumber"));
                            rsInfo.OpenRecordset("SELECT * FROM Banks WHERE Name <> ''");
                            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                            {
                                cboBanks.Clear();
                                do
                                {
                                    cboBanks.AddItem(rsInfo.Get_Fields_Int32("ID") + " - " + rsInfo.Get_Fields_String("Name"));
                                    rsInfo.MoveNext();
                                }
                                while (rsInfo.EndOfFile() != true);
                                cboBanks.SelectedIndex = 0;
                            }
                            else
                            {
                                MessageBox.Show("You must set up your banks before you may continue with this process.", "No Bank Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            rsInfo.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE ISNULL(ReportNumber, 0) = 0");
                            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                            {
                                do
                                {
                                    rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'C' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + rsInfo.Get_Fields_Int32("WarrantNumber") + "')");
                                    if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
                                    {
                                        do
                                        {
                                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                            journalNumber = rsMasterJournal.Get_Fields("JournalNumber");
                                            rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'C' AND JournalNumber = " + journalNumber);
                                            curTotalAmount = 0;
                                            if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                                            {
                                                while (rsAPJournal.EndOfFile() != true)
                                                {
                                                    rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
                                                    if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                                                    {
                                                        //FC:FINAL:MSH - FCConvert.ToDecimal added, because '+=' can't be applied to decimal and double (same with issue #733)
                                                        // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                                        curTotalAmount += FCConvert.ToDecimal(rsEntries.Get_Fields_Decimal("TotalAmount") - rsEntries.Get_Fields("TotalDiscount"));
                                                    }
                                                    rsAPJournal.MoveNext();
                                                }
                                            }
                                            //FC:FINAL:MSH - FCConvert.ToDecimal added, because '!=' can't be applied to double and decimal (same with issue #733)
                                            if (curTotalAmount != FCConvert.ToDecimal(rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo")))
                                            {
                                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                                MessageBox.Show("Journal " + Strings.Format(journalNumber, "0000") + " was run through the Warrant Preview with an amount of " + Strings.Format(rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + ".  This journal now shows an amount of " + Strings.Format(curTotalAmount, "#,##0.00") + ".  You may not continue until these amounts are in balance.", "Different Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                return;
                                            }
                                            rsMasterJournal.MoveNext();
                                        }
                                        while (rsMasterJournal.EndOfFile() != true);
                                    }
                                    rsInfo.MoveNext();
                                }
                                while (rsInfo.EndOfFile() != true);
                            }
                            if (!rsMasterJournal.IsntAnything())
                            {
                                rsMasterJournal.MoveFirst();
                                // TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                                if (Conversion.Val(rsMasterJournal.Get_Fields("BankNumber")) != 0)
                                {
                                    for (counter = 0; counter <= cboBanks.Items.Count - 1; counter++)
                                    {
                                        // TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                                        if (Conversion.Val(Strings.Trim(Strings.Left(cboBanks.Items[counter].ToString(), 2))) != Conversion.Val(rsMasterJournal.Get_Fields("BankNumber"))) continue;

                                        cboBanks.SelectedIndex = counter;
                                        executePrintReports = true;

                                        goto PrintReports;
                                    }
                                }
                            }
                            fraBanks.Visible = true;
                            cmbInitial.Visible = false;
                            lblInitial.Visible = false;
                            //cmdCancel.Visible = false;
                            //cmdProcess.Visible = false;
                            cboBanks.Focus();
                            return;
                        }
                        else
                        {
                            executePrintReports = true;
                        }

                    PrintReports:
                        if (executePrintReports)
                        {
                            rsInfo.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE ISNULL(ReportNumber, 0) = 0");
                            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                            {
                                do
                                {
                                    rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'C' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + rsInfo.Get_Fields_Int32("WarrantNumber") + "')");
                                    if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
                                    {
                                        do
                                        {
                                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                            journalNumber = rsMasterJournal.Get_Fields("JournalNumber");
                                            rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'C' AND JournalNumber = " + journalNumber);
                                            curTotalAmount = 0;
                                            if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
                                            {
                                                while (rsAPJournal.EndOfFile() != true)
                                                {
                                                    rsEntries.OpenRecordset("SELECT SUM(Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"));
                                                    if (rsEntries.EndOfFile() != true && rsEntries.BeginningOfFile() != true)
                                                    {
                                                        // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                                                        curTotalAmount += rsEntries.Get_Fields_Decimal("TotalAmount") - rsEntries.Get_Fields("TotalDiscount");
                                                    }
                                                    rsAPJournal.MoveNext();
                                                }
                                            }
                                            if (FCConvert.ToDecimal(modGlobal.Round_8(FCConvert.ToDouble(curTotalAmount), 2)) != rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo"))
                                            {
                                                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                                MessageBox.Show("Journal " + Strings.Format(journalNumber, "0000") + " was run through the Warrant Preview with an amount of " + Strings.Format(rsMasterJournal.Get_Fields_Decimal("TotalAmount") - rsMasterJournal.Get_Fields_Decimal("TotalCreditMemo"), "#,##0.00") + ".  This journal now shows an amount of " + Strings.Format(curTotalAmount, "#,##0.00") + ".  You may not continue until these amounts are in balance.", "Different Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                                return;
                                            }
                                            rsMasterJournal.MoveNext();
                                        }
                                        while (rsMasterJournal.EndOfFile() != true);
                                    }
                                    rsInfo.MoveNext();
                                }
                                while (rsInfo.EndOfFile() != true);
                            }
                            if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
                            {
                                strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim((modRegistry.GetRegistryKey("PrinterDeviceName") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
                            }

                            //FC:FINA:KV:IIT807+FC-8697
                            Hide();
                            rsMultipleWarrants.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE WarrantNumber <> 0 AND ISNULL(ReportNumber, 0) = 0");
                            if (rsMultipleWarrants.RecordCount() > 1)
                            {
                                using (var selectPrintItem = new SelectPrintItem())
                                {
                                    selectPrintItem.AllowMultiplePrints = false;
                                    selectPrintItem.ClearItems();
                                    do
                                    {
                                        //WarrantNumber
                                        selectPrintItem.AddItem(string.Format("Item {0} of {1} - {2}", rsMultipleWarrants.AbsolutePosition() + 1, rsMultipleWarrants.RecordCount(), "Warrant " + rsMultipleWarrants.Get_Fields("WarrantNumber")));
                                    CheckAgain:
                                        if (modBudgetaryMaster.JobComplete_2("rptCheckRegister"))
                                        {
                                            rsMultipleWarrants.MoveNext();
                                        }
                                        else
                                        {
                                            goto CheckAgain;
                                        }
                                    }
                                    while (rsMultipleWarrants.EndOfFile() != true);

                                    selectPrintItem.CurrentItem = 0;
                                    selectPrintItem.PrintItem += (s, evt) =>
                                    {
                                        if (selectPrintItem.CurrentItem >= 0)
                                        {
                                            InstancePtr.rsMultipleWarrants.SetRow(selectPrintItem.CurrentItem);
                                            modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName);
                                        }
                                    };
                                    selectPrintItem.ShowDialog();
                                }
                                Close();
                            }
                            else
                            {
                                frmReportViewer.InstancePtr.Init(rptCheckRegister.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("There is no information in the Temporary Check File to run a Check Register Report on.", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    if (cboReport.Items.Count > 0)
                    {
                        fraSelectReport.Visible = true;
                        cmbInitial.Visible = false;
                        lblInitial.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show("There are no reports to reprint.", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            finally
            {
                rsAPJournal.DisposeOf();
                rsEntries.DisposeOf();
                rsInfo.DisposeOf();
                rsMasterJournal.DisposeOf();
            }
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            if (cboReport.SelectedIndex == -1)
            {
                MessageBox.Show("You must select which report you wish to reprint before you may continue.", "No Report Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
            {
                strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim((modRegistry.GetRegistryKey("PrinterDeviceName") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
            }

            //FC:FINA:KV:IIT807+FC-8697
            Hide();
            rsMultipleWarrants.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE WarrantNumber <> 0 AND ISNULL(ReportNumber, 0) = 0");
            if (rsMultipleWarrants.RecordCount() > 1)
            {
                using (var selectPrintItem = new SelectPrintItem())
                {
                    selectPrintItem.AllowMultiplePrints = false;
                    selectPrintItem.ClearItems();
                    do
                    {
                        selectPrintItem.AddItem(string.Format("Item {0} of {1} - {2}", rsMultipleWarrants.AbsolutePosition() + 1, rsMultipleWarrants.RecordCount(), "Warrant " + rsMultipleWarrants.Get_Fields("WarrantNumber")));
                    CheckAgain:
                        if (modBudgetaryMaster.JobComplete_2("rptCheckRegister"))
                        {
                            rsMultipleWarrants.MoveNext();
                        }
                        else
                        {
                            goto CheckAgain;
                        }
                    }
                    while (rsMultipleWarrants.EndOfFile() != true);

                    selectPrintItem.CurrentItem = 0;
                    selectPrintItem.PrintItem += (s, evt) =>
                    {
                        if (selectPrintItem.CurrentItem >= 0)
                        {
                            InstancePtr.rsMultipleWarrants.SetRow(selectPrintItem.CurrentItem);
                            modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName);
                        }
                    };
                    selectPrintItem.ShowDialog();
                }
                Close();
            }
            else
            {
                // rptCheckRegister.PrintReport False
                frmReportViewer.InstancePtr.Init(rptCheckRegister.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
            }
        }


        private void Command3_Click(object sender, EventArgs e)
        {
            if (cboBanks.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a bank to use for your check rec before you may continue.", "No Bank Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // If GetRegistryKey("APProcessReportsDefaultPrinter", , False) Then
            if (FCConvert.CBool(setUtil.GetSettingValue("APProcessReportsDefaultPrinter", "Budgetary", "Machine", modGlobalConstants.Statics.clsSecurityClass.GetNameOfComputer(), "", "APProcessReportsDefaultPrinter", "", "False")))
            {
                strPrinterName = modReplaceWorkFiles.StripColon(Strings.Trim((modRegistry.GetRegistryKey("PrinterDeviceName") != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "")));
            }

            //FC:FINA:KV:IIT807+FC-8697
            Hide();
            rsMultipleWarrants.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE WarrantNumber <> 0 AND ISNULL(ReportNumber, 0) = 0");
            if (rsMultipleWarrants.RecordCount() > 1)
            {
                do
                {
                    modDuplexPrinting.DuplexPrintReport(rptCheckRegister.InstancePtr, strPrinterName);
                CheckAgain:

                    if (modBudgetaryMaster.JobComplete_2("rptCheckRegister"))
                    {
                        rsMultipleWarrants.MoveNext();
                    }
                    else
                    {
                        goto CheckAgain;
                    }
                }
                while (rsMultipleWarrants.EndOfFile() != true);
            }
            else
            {
                frmReportViewer.InstancePtr.Init(rptCheckRegister.InstancePtr, strPrinterName, 0, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true);
            }
        }

        private void frmCheckRegister_Activated(object sender, EventArgs e)
        {

            if (modGlobal.FormExist(this))
            {
                return;
            }

            clsDRWrapper rsReportInfo = new clsDRWrapper();
            clsDRWrapper rsWarrrantInfo = new clsDRWrapper();

            try
            {
                rsReportInfo.OpenRecordset("SELECT DISTINCT WarrantNumber FROM TempCheckFile WHERE IsNull(WarrantNumber, 0) <> 0 AND IsNull(ReportNumber, 0) <> 0 ORDER BY WarrantNumber DESC");
                cboReport.Clear();
                if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
                {
                    do
                    {
                        var warrantNumber = rsReportInfo.Get_Fields_Int32("WarrantNumber");
                        rsWarrrantInfo.OpenRecordset("SELECT * FROM WarrantMaster WHERE WarrantNumber = " + warrantNumber);
                        var formattedWarrantNumber = modValidateAccount.GetFormat_6(FCConvert.ToString(warrantNumber), 4);

                        if (rsWarrrantInfo.EndOfFile() != true && rsWarrrantInfo.BeginningOfFile() != true)
                        {
                            cboReport.AddItem("Warrant " + formattedWarrantNumber + "  Date: " + Strings.Format(rsWarrrantInfo.Get_Fields_DateTime("WarrantDate"), "MM/dd/yyyy"));
                        }
                        else
                        {
                            cboReport.AddItem("Warrant " + formattedWarrantNumber + "  Date: UNKNOWN");
                        }
                        rsReportInfo.MoveNext();
                    }
                    while (rsReportInfo.EndOfFile() != true);
                }
                if (cboReport.Items.Count > 0)
                {
                    cboReport.SelectedIndex = 0;
                }
            }
            finally
            {
                rsReportInfo.DisposeOf();
                rsWarrrantInfo.DisposeOf();
            }
            Refresh();
        }

        private void frmCheckRegister_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormUtilities.KeyPressHandler(e, this);
        }

        private void frmCheckRegister_Load(object sender, EventArgs e)
        {

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
            modGlobalFunctions.SetTRIOColors(this);
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            //MDIParent.InstancePtr.Focus();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (fraSelectReport.Visible)
                Command1_Click(btnProcess, EventArgs.Empty);
            else
                if (fraBanks.Visible)
                Command3_Click(btnProcess, EventArgs.Empty);
            else
                cmdProcess_Click(btnProcess, EventArgs.Empty);
        }

        private void frmCheckRegister_Resize(object sender, EventArgs e)
        {
            fraBanks.CenterToContainer(ClientArea);
            fraSelectReport.CenterToContainer(ClientArea);
        }
    }
}
