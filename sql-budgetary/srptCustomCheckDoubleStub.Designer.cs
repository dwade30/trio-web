﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckDoubleStub.
	/// </summary>
	partial class srptCustomCheckDoubleStub
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCustomCheckDoubleStub));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblWarrant = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDiscount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCredit1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDiscount20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmt20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCredit20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.fldDate,
				this.Label3,
				this.fldVendor,
				this.Label4,
				this.fldCheck,
				this.lblWarrant,
				this.lblDescription,
				this.lblReference,
				this.lblDiscount,
				this.lblAmount,
				this.fldMuniName,
				this.lblAmount2,
				this.fldAmount,
				this.fldWarrant1,
				this.fldWarrant2,
				this.fldWarrant3,
				this.fldWarrant4,
				this.fldWarrant5,
				this.fldWarrant6,
				this.fldWarrant7,
				this.fldWarrant8,
				this.fldWarrant9,
				this.fldWarrant10,
				this.fldDescription1,
				this.fldDescription2,
				this.fldDescription3,
				this.fldDescription4,
				this.fldDescription5,
				this.fldDescription6,
				this.fldDescription7,
				this.fldDescription8,
				this.fldDescription9,
				this.fldDescription10,
				this.fldReference1,
				this.fldReference2,
				this.fldReference3,
				this.fldReference4,
				this.fldReference5,
				this.fldReference6,
				this.fldReference7,
				this.fldReference8,
				this.fldReference9,
				this.fldReference10,
				this.fldDiscount1,
				this.fldDiscount2,
				this.fldDiscount3,
				this.fldDiscount4,
				this.fldDiscount5,
				this.fldDiscount6,
				this.fldDiscount7,
				this.fldDiscount8,
				this.fldDiscount9,
				this.fldDiscount10,
				this.fldAmt1,
				this.fldAmt2,
				this.fldAmt3,
				this.fldAmt4,
				this.fldAmt5,
				this.fldAmt6,
				this.fldAmt7,
				this.fldAmt8,
				this.fldAmt9,
				this.fldAmt10,
				this.Label20,
				this.fldCredit1,
				this.fldCredit2,
				this.fldCredit3,
				this.fldCredit4,
				this.fldCredit5,
				this.fldCredit6,
				this.fldCredit7,
				this.fldCredit8,
				this.fldCredit9,
				this.fldCredit10,
				this.fldCheckMessage,
				this.fldWarrant11,
				this.fldWarrant12,
				this.fldWarrant13,
				this.fldWarrant14,
				this.fldWarrant15,
				this.fldWarrant16,
				this.fldWarrant17,
				this.fldWarrant18,
				this.fldWarrant19,
				this.fldWarrant20,
				this.fldDescription11,
				this.fldDescription12,
				this.fldDescription13,
				this.fldDescription14,
				this.fldDescription15,
				this.fldDescription16,
				this.fldDescription17,
				this.fldDescription18,
				this.fldDescription19,
				this.fldDescription20,
				this.fldReference11,
				this.fldReference12,
				this.fldReference13,
				this.fldReference14,
				this.fldReference15,
				this.fldReference16,
				this.fldReference17,
				this.fldReference18,
				this.fldReference19,
				this.fldReference20,
				this.fldDiscount11,
				this.fldDiscount12,
				this.fldDiscount13,
				this.fldDiscount14,
				this.fldDiscount15,
				this.fldDiscount16,
				this.fldDiscount17,
				this.fldDiscount18,
				this.fldDiscount19,
				this.fldDiscount20,
				this.fldAmt11,
				this.fldAmt12,
				this.fldAmt13,
				this.fldAmt14,
				this.fldAmt15,
				this.fldAmt16,
				this.fldAmt17,
				this.fldAmt18,
				this.fldAmt19,
				this.fldAmt20,
				this.fldCredit11,
				this.fldCredit12,
				this.fldCredit13,
				this.fldCredit14,
				this.fldCredit15,
				this.fldCredit16,
				this.fldCredit17,
				this.fldCredit18,
				this.fldCredit19,
				this.fldCredit20
			});
			this.Detail.Height = 7F;
			this.Detail.Name = "Detail";
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 3.53125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label1.Tag = "Large";
			this.Label1.Text = "-- A/P CHECK --";
			this.Label1.Top = 0.28125F;
			this.Label1.Width = 1.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.19F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 4.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label2.Tag = "Large";
			this.Label2.Text = "DATE";
			this.Label2.Top = 0.28125F;
			this.Label2.Width = 0.4375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 5.34375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldDate.Tag = "Large";
			this.fldDate.Text = "Field2";
			this.fldDate.Top = 0.28125F;
			this.fldDate.Width = 0.78125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.19F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label3.Tag = "Large";
			this.Label3.Text = "PAYEE:";
			this.Label3.Top = 0.625F;
			this.Label3.Width = 0.59375F;
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.19F;
			this.fldVendor.Left = 2.28125F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldVendor.Tag = "Large";
			this.fldVendor.Text = "Field2";
			this.fldVendor.Top = 0.625F;
			this.fldVendor.Width = 3.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.19F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.28125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.Label4.Tag = "Large";
			this.Label4.Text = "CHECK";
			this.Label4.Top = 0.625F;
			this.Label4.Width = 0.59375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.19F;
			this.fldCheck.Left = 6.90625F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldCheck.Tag = "Large";
			this.fldCheck.Text = "Field2";
			this.fldCheck.Top = 0.625F;
			this.fldCheck.Width = 0.625F;
			// 
			// lblWarrant
			// 
			this.lblWarrant.Height = 0.1875F;
			this.lblWarrant.HyperLink = null;
			this.lblWarrant.Left = 0.3125F;
			this.lblWarrant.Name = "lblWarrant";
			this.lblWarrant.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblWarrant.Tag = "Large";
			this.lblWarrant.Text = "WRNT";
			this.lblWarrant.Top = 0.875F;
			this.lblWarrant.Width = 0.46875F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 0.875F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblDescription.Tag = "Large";
			this.lblDescription.Text = "DESCRIPTION----------";
			this.lblDescription.Top = 0.875F;
			this.lblDescription.Width = 1.65625F;
			// 
			// lblReference
			// 
			this.lblReference.Height = 0.1875F;
			this.lblReference.HyperLink = null;
			this.lblReference.Left = 3.03125F;
			this.lblReference.Name = "lblReference";
			this.lblReference.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.lblReference.Tag = "Large";
			this.lblReference.Text = "REFERENCE-----";
			this.lblReference.Top = 0.875F;
			this.lblReference.Width = 1.1875F;
			// 
			// lblDiscount
			// 
			this.lblDiscount.Height = 0.1875F;
			this.lblDiscount.HyperLink = null;
			this.lblDiscount.Left = 5.96875F;
			this.lblDiscount.Name = "lblDiscount";
			this.lblDiscount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.lblDiscount.Tag = "Large";
			this.lblDiscount.Text = "-DISC-";
			this.lblDiscount.Top = 0.875F;
			this.lblDiscount.Width = 0.625F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.96875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.lblAmount.Tag = "Large";
			this.lblAmount.Text = "-AMOUNT-";
			this.lblAmount.Top = 0.875F;
			this.lblAmount.Width = 0.78125F;
			// 
			// fldMuniName
			// 
			this.fldMuniName.Height = 0.1875F;
			this.fldMuniName.Left = 1.84375F;
			this.fldMuniName.Name = "fldMuniName";
			this.fldMuniName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldMuniName.Tag = "Large";
			this.fldMuniName.Text = "Field2";
			this.fldMuniName.Top = 5.8125F;
			this.fldMuniName.Width = 1.1875F;
			// 
			// lblAmount2
			// 
			this.lblAmount2.Height = 0.1875F;
			this.lblAmount2.HyperLink = null;
			this.lblAmount2.Left = 3.375F;
			this.lblAmount2.Name = "lblAmount2";
			this.lblAmount2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.lblAmount2.Tag = "Large";
			this.lblAmount2.Text = "AMOUNT-";
			this.lblAmount2.Top = 5.8125F;
			this.lblAmount2.Width = 0.78125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 4.15625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmount.Tag = "Large";
			this.fldAmount.Text = "1234567890123";
			this.fldAmount.Top = 5.8125F;
			this.fldAmount.Width = 1.125F;
			// 
			// fldWarrant1
			// 
			this.fldWarrant1.CanGrow = false;
			this.fldWarrant1.Height = 0.19F;
			this.fldWarrant1.Left = 0.3125F;
			this.fldWarrant1.MultiLine = false;
			this.fldWarrant1.Name = "fldWarrant1";
			this.fldWarrant1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant1.Tag = "Large";
			this.fldWarrant1.Text = "Field1";
			this.fldWarrant1.Top = 1.09375F;
			this.fldWarrant1.Width = 0.46875F;
			// 
			// fldWarrant2
			// 
			this.fldWarrant2.CanGrow = false;
			this.fldWarrant2.Height = 0.19F;
			this.fldWarrant2.Left = 0.3125F;
			this.fldWarrant2.MultiLine = false;
			this.fldWarrant2.Name = "fldWarrant2";
			this.fldWarrant2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant2.Tag = "Large";
			this.fldWarrant2.Text = "Field1";
			this.fldWarrant2.Top = 1.28125F;
			this.fldWarrant2.Width = 0.46875F;
			// 
			// fldWarrant3
			// 
			this.fldWarrant3.CanGrow = false;
			this.fldWarrant3.Height = 0.19F;
			this.fldWarrant3.Left = 0.3125F;
			this.fldWarrant3.MultiLine = false;
			this.fldWarrant3.Name = "fldWarrant3";
			this.fldWarrant3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant3.Tag = "Large";
			this.fldWarrant3.Text = "Field1";
			this.fldWarrant3.Top = 1.46875F;
			this.fldWarrant3.Width = 0.46875F;
			// 
			// fldWarrant4
			// 
			this.fldWarrant4.CanGrow = false;
			this.fldWarrant4.Height = 0.19F;
			this.fldWarrant4.Left = 0.3125F;
			this.fldWarrant4.MultiLine = false;
			this.fldWarrant4.Name = "fldWarrant4";
			this.fldWarrant4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant4.Tag = "Large";
			this.fldWarrant4.Text = "Field1";
			this.fldWarrant4.Top = 1.65625F;
			this.fldWarrant4.Width = 0.46875F;
			// 
			// fldWarrant5
			// 
			this.fldWarrant5.CanGrow = false;
			this.fldWarrant5.Height = 0.19F;
			this.fldWarrant5.Left = 0.3125F;
			this.fldWarrant5.MultiLine = false;
			this.fldWarrant5.Name = "fldWarrant5";
			this.fldWarrant5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant5.Tag = "Large";
			this.fldWarrant5.Text = "Field1";
			this.fldWarrant5.Top = 1.84375F;
			this.fldWarrant5.Width = 0.46875F;
			// 
			// fldWarrant6
			// 
			this.fldWarrant6.CanGrow = false;
			this.fldWarrant6.Height = 0.19F;
			this.fldWarrant6.Left = 0.3125F;
			this.fldWarrant6.MultiLine = false;
			this.fldWarrant6.Name = "fldWarrant6";
			this.fldWarrant6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant6.Tag = "Large";
			this.fldWarrant6.Text = "Field1";
			this.fldWarrant6.Top = 2.03125F;
			this.fldWarrant6.Width = 0.46875F;
			// 
			// fldWarrant7
			// 
			this.fldWarrant7.CanGrow = false;
			this.fldWarrant7.Height = 0.19F;
			this.fldWarrant7.Left = 0.3125F;
			this.fldWarrant7.MultiLine = false;
			this.fldWarrant7.Name = "fldWarrant7";
			this.fldWarrant7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant7.Tag = "Large";
			this.fldWarrant7.Text = "Field1";
			this.fldWarrant7.Top = 2.21875F;
			this.fldWarrant7.Width = 0.46875F;
			// 
			// fldWarrant8
			// 
			this.fldWarrant8.CanGrow = false;
			this.fldWarrant8.Height = 0.19F;
			this.fldWarrant8.Left = 0.3125F;
			this.fldWarrant8.MultiLine = false;
			this.fldWarrant8.Name = "fldWarrant8";
			this.fldWarrant8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant8.Tag = "Large";
			this.fldWarrant8.Text = "Field1";
			this.fldWarrant8.Top = 2.40625F;
			this.fldWarrant8.Width = 0.46875F;
			// 
			// fldWarrant9
			// 
			this.fldWarrant9.CanGrow = false;
			this.fldWarrant9.Height = 0.19F;
			this.fldWarrant9.Left = 0.3125F;
			this.fldWarrant9.MultiLine = false;
			this.fldWarrant9.Name = "fldWarrant9";
			this.fldWarrant9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant9.Tag = "Large";
			this.fldWarrant9.Text = "Field1";
			this.fldWarrant9.Top = 2.59375F;
			this.fldWarrant9.Width = 0.46875F;
			// 
			// fldWarrant10
			// 
			this.fldWarrant10.CanGrow = false;
			this.fldWarrant10.Height = 0.19F;
			this.fldWarrant10.Left = 0.3125F;
			this.fldWarrant10.MultiLine = false;
			this.fldWarrant10.Name = "fldWarrant10";
			this.fldWarrant10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant10.Tag = "Large";
			this.fldWarrant10.Text = "Field1";
			this.fldWarrant10.Top = 2.78125F;
			this.fldWarrant10.Width = 0.46875F;
			// 
			// fldDescription1
			// 
			this.fldDescription1.CanGrow = false;
			this.fldDescription1.Height = 0.19F;
			this.fldDescription1.Left = 0.875F;
			this.fldDescription1.MultiLine = false;
			this.fldDescription1.Name = "fldDescription1";
			this.fldDescription1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription1.Tag = "Large";
			this.fldDescription1.Text = "Field1";
			this.fldDescription1.Top = 1.09375F;
			this.fldDescription1.Width = 2F;
			// 
			// fldDescription2
			// 
			this.fldDescription2.CanGrow = false;
			this.fldDescription2.Height = 0.19F;
			this.fldDescription2.Left = 0.875F;
			this.fldDescription2.MultiLine = false;
			this.fldDescription2.Name = "fldDescription2";
			this.fldDescription2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription2.Tag = "Large";
			this.fldDescription2.Text = "Field1";
			this.fldDescription2.Top = 1.28125F;
			this.fldDescription2.Width = 2F;
			// 
			// fldDescription3
			// 
			this.fldDescription3.CanGrow = false;
			this.fldDescription3.Height = 0.19F;
			this.fldDescription3.Left = 0.875F;
			this.fldDescription3.MultiLine = false;
			this.fldDescription3.Name = "fldDescription3";
			this.fldDescription3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription3.Tag = "Large";
			this.fldDescription3.Text = "Field1";
			this.fldDescription3.Top = 1.46875F;
			this.fldDescription3.Width = 2F;
			// 
			// fldDescription4
			// 
			this.fldDescription4.CanGrow = false;
			this.fldDescription4.Height = 0.19F;
			this.fldDescription4.Left = 0.875F;
			this.fldDescription4.MultiLine = false;
			this.fldDescription4.Name = "fldDescription4";
			this.fldDescription4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription4.Tag = "Large";
			this.fldDescription4.Text = "Field1";
			this.fldDescription4.Top = 1.65625F;
			this.fldDescription4.Width = 2F;
			// 
			// fldDescription5
			// 
			this.fldDescription5.CanGrow = false;
			this.fldDescription5.Height = 0.19F;
			this.fldDescription5.Left = 0.875F;
			this.fldDescription5.MultiLine = false;
			this.fldDescription5.Name = "fldDescription5";
			this.fldDescription5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription5.Tag = "Large";
			this.fldDescription5.Text = "Field1";
			this.fldDescription5.Top = 1.84375F;
			this.fldDescription5.Width = 2F;
			// 
			// fldDescription6
			// 
			this.fldDescription6.CanGrow = false;
			this.fldDescription6.Height = 0.19F;
			this.fldDescription6.Left = 0.875F;
			this.fldDescription6.MultiLine = false;
			this.fldDescription6.Name = "fldDescription6";
			this.fldDescription6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription6.Tag = "Large";
			this.fldDescription6.Text = "Field1";
			this.fldDescription6.Top = 2.03125F;
			this.fldDescription6.Width = 2F;
			// 
			// fldDescription7
			// 
			this.fldDescription7.CanGrow = false;
			this.fldDescription7.Height = 0.19F;
			this.fldDescription7.Left = 0.875F;
			this.fldDescription7.MultiLine = false;
			this.fldDescription7.Name = "fldDescription7";
			this.fldDescription7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription7.Tag = "Large";
			this.fldDescription7.Text = "Field1";
			this.fldDescription7.Top = 2.21875F;
			this.fldDescription7.Width = 2F;
			// 
			// fldDescription8
			// 
			this.fldDescription8.CanGrow = false;
			this.fldDescription8.Height = 0.19F;
			this.fldDescription8.Left = 0.875F;
			this.fldDescription8.MultiLine = false;
			this.fldDescription8.Name = "fldDescription8";
			this.fldDescription8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription8.Tag = "Large";
			this.fldDescription8.Text = "Field1";
			this.fldDescription8.Top = 2.40625F;
			this.fldDescription8.Width = 2F;
			// 
			// fldDescription9
			// 
			this.fldDescription9.CanGrow = false;
			this.fldDescription9.Height = 0.19F;
			this.fldDescription9.Left = 0.875F;
			this.fldDescription9.MultiLine = false;
			this.fldDescription9.Name = "fldDescription9";
			this.fldDescription9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription9.Tag = "Large";
			this.fldDescription9.Text = "Field1";
			this.fldDescription9.Top = 2.59375F;
			this.fldDescription9.Width = 2F;
			// 
			// fldDescription10
			// 
			this.fldDescription10.CanGrow = false;
			this.fldDescription10.Height = 0.19F;
			this.fldDescription10.Left = 0.875F;
			this.fldDescription10.MultiLine = false;
			this.fldDescription10.Name = "fldDescription10";
			this.fldDescription10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription10.Tag = "Large";
			this.fldDescription10.Text = "Field1";
			this.fldDescription10.Top = 2.78125F;
			this.fldDescription10.Width = 2F;
			// 
			// fldReference1
			// 
			this.fldReference1.CanGrow = false;
			this.fldReference1.Height = 0.19F;
			this.fldReference1.Left = 3F;
			this.fldReference1.MultiLine = false;
			this.fldReference1.Name = "fldReference1";
			this.fldReference1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference1.Tag = "Large";
			this.fldReference1.Text = "Field1";
			this.fldReference1.Top = 1.09375F;
			this.fldReference1.Width = 1.34375F;
			// 
			// fldReference2
			// 
			this.fldReference2.CanGrow = false;
			this.fldReference2.Height = 0.19F;
			this.fldReference2.Left = 3F;
			this.fldReference2.MultiLine = false;
			this.fldReference2.Name = "fldReference2";
			this.fldReference2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference2.Tag = "Large";
			this.fldReference2.Text = "Field1";
			this.fldReference2.Top = 1.28125F;
			this.fldReference2.Width = 1.34375F;
			// 
			// fldReference3
			// 
			this.fldReference3.CanGrow = false;
			this.fldReference3.Height = 0.19F;
			this.fldReference3.Left = 3F;
			this.fldReference3.MultiLine = false;
			this.fldReference3.Name = "fldReference3";
			this.fldReference3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference3.Tag = "Large";
			this.fldReference3.Text = "Field1";
			this.fldReference3.Top = 1.46875F;
			this.fldReference3.Width = 1.34375F;
			// 
			// fldReference4
			// 
			this.fldReference4.CanGrow = false;
			this.fldReference4.Height = 0.19F;
			this.fldReference4.Left = 3F;
			this.fldReference4.MultiLine = false;
			this.fldReference4.Name = "fldReference4";
			this.fldReference4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference4.Tag = "Large";
			this.fldReference4.Text = "Field1";
			this.fldReference4.Top = 1.65625F;
			this.fldReference4.Width = 1.34375F;
			// 
			// fldReference5
			// 
			this.fldReference5.CanGrow = false;
			this.fldReference5.Height = 0.19F;
			this.fldReference5.Left = 3F;
			this.fldReference5.MultiLine = false;
			this.fldReference5.Name = "fldReference5";
			this.fldReference5.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference5.Tag = "Large";
			this.fldReference5.Text = "Field1";
			this.fldReference5.Top = 1.84375F;
			this.fldReference5.Width = 1.34375F;
			// 
			// fldReference6
			// 
			this.fldReference6.CanGrow = false;
			this.fldReference6.Height = 0.19F;
			this.fldReference6.Left = 3F;
			this.fldReference6.MultiLine = false;
			this.fldReference6.Name = "fldReference6";
			this.fldReference6.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference6.Tag = "Large";
			this.fldReference6.Text = "Field1";
			this.fldReference6.Top = 2.03125F;
			this.fldReference6.Width = 1.34375F;
			// 
			// fldReference7
			// 
			this.fldReference7.CanGrow = false;
			this.fldReference7.Height = 0.19F;
			this.fldReference7.Left = 3F;
			this.fldReference7.MultiLine = false;
			this.fldReference7.Name = "fldReference7";
			this.fldReference7.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference7.Tag = "Large";
			this.fldReference7.Text = "Field1";
			this.fldReference7.Top = 2.21875F;
			this.fldReference7.Width = 1.34375F;
			// 
			// fldReference8
			// 
			this.fldReference8.CanGrow = false;
			this.fldReference8.Height = 0.19F;
			this.fldReference8.Left = 3F;
			this.fldReference8.MultiLine = false;
			this.fldReference8.Name = "fldReference8";
			this.fldReference8.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference8.Tag = "Large";
			this.fldReference8.Text = "Field1";
			this.fldReference8.Top = 2.40625F;
			this.fldReference8.Width = 1.34375F;
			// 
			// fldReference9
			// 
			this.fldReference9.CanGrow = false;
			this.fldReference9.Height = 0.19F;
			this.fldReference9.Left = 3F;
			this.fldReference9.MultiLine = false;
			this.fldReference9.Name = "fldReference9";
			this.fldReference9.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference9.Tag = "Large";
			this.fldReference9.Text = "Field1";
			this.fldReference9.Top = 2.59375F;
			this.fldReference9.Width = 1.34375F;
			// 
			// fldReference10
			// 
			this.fldReference10.CanGrow = false;
			this.fldReference10.Height = 0.19F;
			this.fldReference10.Left = 3F;
			this.fldReference10.MultiLine = false;
			this.fldReference10.Name = "fldReference10";
			this.fldReference10.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference10.Tag = "Large";
			this.fldReference10.Text = "Field1";
			this.fldReference10.Top = 2.78125F;
			this.fldReference10.Width = 1.34375F;
			// 
			// fldDiscount1
			// 
			this.fldDiscount1.CanGrow = false;
			this.fldDiscount1.Height = 0.19F;
			this.fldDiscount1.Left = 5.5625F;
			this.fldDiscount1.MultiLine = false;
			this.fldDiscount1.Name = "fldDiscount1";
			this.fldDiscount1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount1.Tag = "Large";
			this.fldDiscount1.Text = "Field1";
			this.fldDiscount1.Top = 1.09375F;
			this.fldDiscount1.Width = 1.03125F;
			// 
			// fldDiscount2
			// 
			this.fldDiscount2.CanGrow = false;
			this.fldDiscount2.Height = 0.19F;
			this.fldDiscount2.Left = 5.5625F;
			this.fldDiscount2.MultiLine = false;
			this.fldDiscount2.Name = "fldDiscount2";
			this.fldDiscount2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount2.Tag = "Large";
			this.fldDiscount2.Text = "Field1";
			this.fldDiscount2.Top = 1.28125F;
			this.fldDiscount2.Width = 1.03125F;
			// 
			// fldDiscount3
			// 
			this.fldDiscount3.CanGrow = false;
			this.fldDiscount3.Height = 0.19F;
			this.fldDiscount3.Left = 5.5625F;
			this.fldDiscount3.MultiLine = false;
			this.fldDiscount3.Name = "fldDiscount3";
			this.fldDiscount3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount3.Tag = "Large";
			this.fldDiscount3.Text = "Field1";
			this.fldDiscount3.Top = 1.46875F;
			this.fldDiscount3.Width = 1.03125F;
			// 
			// fldDiscount4
			// 
			this.fldDiscount4.CanGrow = false;
			this.fldDiscount4.Height = 0.19F;
			this.fldDiscount4.Left = 5.5625F;
			this.fldDiscount4.MultiLine = false;
			this.fldDiscount4.Name = "fldDiscount4";
			this.fldDiscount4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount4.Tag = "Large";
			this.fldDiscount4.Text = "Field1";
			this.fldDiscount4.Top = 1.65625F;
			this.fldDiscount4.Width = 1.03125F;
			// 
			// fldDiscount5
			// 
			this.fldDiscount5.CanGrow = false;
			this.fldDiscount5.Height = 0.19F;
			this.fldDiscount5.Left = 5.5625F;
			this.fldDiscount5.MultiLine = false;
			this.fldDiscount5.Name = "fldDiscount5";
			this.fldDiscount5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount5.Tag = "Large";
			this.fldDiscount5.Text = "Field1";
			this.fldDiscount5.Top = 1.84375F;
			this.fldDiscount5.Width = 1.03125F;
			// 
			// fldDiscount6
			// 
			this.fldDiscount6.CanGrow = false;
			this.fldDiscount6.Height = 0.19F;
			this.fldDiscount6.Left = 5.5625F;
			this.fldDiscount6.MultiLine = false;
			this.fldDiscount6.Name = "fldDiscount6";
			this.fldDiscount6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount6.Tag = "Large";
			this.fldDiscount6.Text = "Field1";
			this.fldDiscount6.Top = 2.03125F;
			this.fldDiscount6.Width = 1.03125F;
			// 
			// fldDiscount7
			// 
			this.fldDiscount7.CanGrow = false;
			this.fldDiscount7.Height = 0.19F;
			this.fldDiscount7.Left = 5.5625F;
			this.fldDiscount7.MultiLine = false;
			this.fldDiscount7.Name = "fldDiscount7";
			this.fldDiscount7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount7.Tag = "Large";
			this.fldDiscount7.Text = "Field1";
			this.fldDiscount7.Top = 2.21875F;
			this.fldDiscount7.Width = 1.03125F;
			// 
			// fldDiscount8
			// 
			this.fldDiscount8.CanGrow = false;
			this.fldDiscount8.Height = 0.19F;
			this.fldDiscount8.Left = 5.5625F;
			this.fldDiscount8.MultiLine = false;
			this.fldDiscount8.Name = "fldDiscount8";
			this.fldDiscount8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount8.Tag = "Large";
			this.fldDiscount8.Text = "Field1";
			this.fldDiscount8.Top = 2.40625F;
			this.fldDiscount8.Width = 1.03125F;
			// 
			// fldDiscount9
			// 
			this.fldDiscount9.CanGrow = false;
			this.fldDiscount9.Height = 0.19F;
			this.fldDiscount9.Left = 5.5625F;
			this.fldDiscount9.MultiLine = false;
			this.fldDiscount9.Name = "fldDiscount9";
			this.fldDiscount9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount9.Tag = "Large";
			this.fldDiscount9.Text = "Field1";
			this.fldDiscount9.Top = 2.59375F;
			this.fldDiscount9.Width = 1.03125F;
			// 
			// fldDiscount10
			// 
			this.fldDiscount10.CanGrow = false;
			this.fldDiscount10.Height = 0.19F;
			this.fldDiscount10.Left = 5.5625F;
			this.fldDiscount10.MultiLine = false;
			this.fldDiscount10.Name = "fldDiscount10";
			this.fldDiscount10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount10.Tag = "Large";
			this.fldDiscount10.Text = "Field1";
			this.fldDiscount10.Top = 2.78125F;
			this.fldDiscount10.Width = 1.03125F;
			// 
			// fldAmt1
			// 
			this.fldAmt1.CanGrow = false;
			this.fldAmt1.Height = 0.19F;
			this.fldAmt1.Left = 6.71875F;
			this.fldAmt1.MultiLine = false;
			this.fldAmt1.Name = "fldAmt1";
			this.fldAmt1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt1.Tag = "Large";
			this.fldAmt1.Text = "Field1";
			this.fldAmt1.Top = 1.09375F;
			this.fldAmt1.Width = 1.03125F;
			// 
			// fldAmt2
			// 
			this.fldAmt2.CanGrow = false;
			this.fldAmt2.Height = 0.19F;
			this.fldAmt2.Left = 6.71875F;
			this.fldAmt2.MultiLine = false;
			this.fldAmt2.Name = "fldAmt2";
			this.fldAmt2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt2.Tag = "Large";
			this.fldAmt2.Text = "Field1";
			this.fldAmt2.Top = 1.28125F;
			this.fldAmt2.Width = 1.03125F;
			// 
			// fldAmt3
			// 
			this.fldAmt3.CanGrow = false;
			this.fldAmt3.Height = 0.19F;
			this.fldAmt3.Left = 6.71875F;
			this.fldAmt3.MultiLine = false;
			this.fldAmt3.Name = "fldAmt3";
			this.fldAmt3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt3.Tag = "Large";
			this.fldAmt3.Text = "Field1";
			this.fldAmt3.Top = 1.46875F;
			this.fldAmt3.Width = 1.03125F;
			// 
			// fldAmt4
			// 
			this.fldAmt4.CanGrow = false;
			this.fldAmt4.Height = 0.19F;
			this.fldAmt4.Left = 6.71875F;
			this.fldAmt4.MultiLine = false;
			this.fldAmt4.Name = "fldAmt4";
			this.fldAmt4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt4.Tag = "Large";
			this.fldAmt4.Text = "Field1";
			this.fldAmt4.Top = 1.65625F;
			this.fldAmt4.Width = 1.03125F;
			// 
			// fldAmt5
			// 
			this.fldAmt5.CanGrow = false;
			this.fldAmt5.Height = 0.19F;
			this.fldAmt5.Left = 6.71875F;
			this.fldAmt5.MultiLine = false;
			this.fldAmt5.Name = "fldAmt5";
			this.fldAmt5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt5.Tag = "Large";
			this.fldAmt5.Text = "Field1";
			this.fldAmt5.Top = 1.84375F;
			this.fldAmt5.Width = 1.03125F;
			// 
			// fldAmt6
			// 
			this.fldAmt6.CanGrow = false;
			this.fldAmt6.Height = 0.19F;
			this.fldAmt6.Left = 6.71875F;
			this.fldAmt6.MultiLine = false;
			this.fldAmt6.Name = "fldAmt6";
			this.fldAmt6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt6.Tag = "Large";
			this.fldAmt6.Text = "Field1";
			this.fldAmt6.Top = 2.03125F;
			this.fldAmt6.Width = 1.03125F;
			// 
			// fldAmt7
			// 
			this.fldAmt7.CanGrow = false;
			this.fldAmt7.Height = 0.19F;
			this.fldAmt7.Left = 6.71875F;
			this.fldAmt7.MultiLine = false;
			this.fldAmt7.Name = "fldAmt7";
			this.fldAmt7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt7.Tag = "Large";
			this.fldAmt7.Text = "Field1";
			this.fldAmt7.Top = 2.21875F;
			this.fldAmt7.Width = 1.03125F;
			// 
			// fldAmt8
			// 
			this.fldAmt8.CanGrow = false;
			this.fldAmt8.Height = 0.19F;
			this.fldAmt8.Left = 6.71875F;
			this.fldAmt8.MultiLine = false;
			this.fldAmt8.Name = "fldAmt8";
			this.fldAmt8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt8.Tag = "Large";
			this.fldAmt8.Text = "Field1";
			this.fldAmt8.Top = 2.40625F;
			this.fldAmt8.Width = 1.03125F;
			// 
			// fldAmt9
			// 
			this.fldAmt9.CanGrow = false;
			this.fldAmt9.Height = 0.19F;
			this.fldAmt9.Left = 6.71875F;
			this.fldAmt9.MultiLine = false;
			this.fldAmt9.Name = "fldAmt9";
			this.fldAmt9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt9.Tag = "Large";
			this.fldAmt9.Text = "Field1";
			this.fldAmt9.Top = 2.59375F;
			this.fldAmt9.Width = 1.03125F;
			// 
			// fldAmt10
			// 
			this.fldAmt10.CanGrow = false;
			this.fldAmt10.Height = 0.19F;
			this.fldAmt10.Left = 6.71875F;
			this.fldAmt10.MultiLine = false;
			this.fldAmt10.Name = "fldAmt10";
			this.fldAmt10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt10.Tag = "Large";
			this.fldAmt10.Text = "Field1";
			this.fldAmt10.Top = 2.78125F;
			this.fldAmt10.Width = 1.03125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.8125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.Label20.Tag = "Large";
			this.Label20.Text = "CREDIT";
			this.Label20.Top = 0.875F;
			this.Label20.Width = 0.625F;
			// 
			// fldCredit1
			// 
			this.fldCredit1.CanGrow = false;
			this.fldCredit1.Height = 0.19F;
			this.fldCredit1.Left = 4.40625F;
			this.fldCredit1.MultiLine = false;
			this.fldCredit1.Name = "fldCredit1";
			this.fldCredit1.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit1.Tag = "Large";
			this.fldCredit1.Text = "Field1";
			this.fldCredit1.Top = 1.09375F;
			this.fldCredit1.Width = 1.03125F;
			// 
			// fldCredit2
			// 
			this.fldCredit2.CanGrow = false;
			this.fldCredit2.Height = 0.19F;
			this.fldCredit2.Left = 4.40625F;
			this.fldCredit2.MultiLine = false;
			this.fldCredit2.Name = "fldCredit2";
			this.fldCredit2.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit2.Tag = "Large";
			this.fldCredit2.Text = "Field1";
			this.fldCredit2.Top = 1.28125F;
			this.fldCredit2.Width = 1.03125F;
			// 
			// fldCredit3
			// 
			this.fldCredit3.CanGrow = false;
			this.fldCredit3.Height = 0.19F;
			this.fldCredit3.Left = 4.40625F;
			this.fldCredit3.MultiLine = false;
			this.fldCredit3.Name = "fldCredit3";
			this.fldCredit3.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit3.Tag = "Large";
			this.fldCredit3.Text = "Field1";
			this.fldCredit3.Top = 1.46875F;
			this.fldCredit3.Width = 1.03125F;
			// 
			// fldCredit4
			// 
			this.fldCredit4.CanGrow = false;
			this.fldCredit4.Height = 0.19F;
			this.fldCredit4.Left = 4.40625F;
			this.fldCredit4.MultiLine = false;
			this.fldCredit4.Name = "fldCredit4";
			this.fldCredit4.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit4.Tag = "Large";
			this.fldCredit4.Text = "Field1";
			this.fldCredit4.Top = 1.65625F;
			this.fldCredit4.Width = 1.03125F;
			// 
			// fldCredit5
			// 
			this.fldCredit5.CanGrow = false;
			this.fldCredit5.Height = 0.19F;
			this.fldCredit5.Left = 4.40625F;
			this.fldCredit5.MultiLine = false;
			this.fldCredit5.Name = "fldCredit5";
			this.fldCredit5.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit5.Tag = "Large";
			this.fldCredit5.Text = "Field1";
			this.fldCredit5.Top = 1.84375F;
			this.fldCredit5.Width = 1.03125F;
			// 
			// fldCredit6
			// 
			this.fldCredit6.CanGrow = false;
			this.fldCredit6.Height = 0.19F;
			this.fldCredit6.Left = 4.40625F;
			this.fldCredit6.MultiLine = false;
			this.fldCredit6.Name = "fldCredit6";
			this.fldCredit6.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit6.Tag = "Large";
			this.fldCredit6.Text = "Field1";
			this.fldCredit6.Top = 2.03125F;
			this.fldCredit6.Width = 1.03125F;
			// 
			// fldCredit7
			// 
			this.fldCredit7.CanGrow = false;
			this.fldCredit7.Height = 0.19F;
			this.fldCredit7.Left = 4.40625F;
			this.fldCredit7.MultiLine = false;
			this.fldCredit7.Name = "fldCredit7";
			this.fldCredit7.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit7.Tag = "Large";
			this.fldCredit7.Text = "Field1";
			this.fldCredit7.Top = 2.21875F;
			this.fldCredit7.Width = 1.03125F;
			// 
			// fldCredit8
			// 
			this.fldCredit8.CanGrow = false;
			this.fldCredit8.Height = 0.19F;
			this.fldCredit8.Left = 4.40625F;
			this.fldCredit8.MultiLine = false;
			this.fldCredit8.Name = "fldCredit8";
			this.fldCredit8.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit8.Tag = "Large";
			this.fldCredit8.Text = "Field1";
			this.fldCredit8.Top = 2.40625F;
			this.fldCredit8.Width = 1.03125F;
			// 
			// fldCredit9
			// 
			this.fldCredit9.CanGrow = false;
			this.fldCredit9.Height = 0.19F;
			this.fldCredit9.Left = 4.40625F;
			this.fldCredit9.MultiLine = false;
			this.fldCredit9.Name = "fldCredit9";
			this.fldCredit9.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit9.Tag = "Large";
			this.fldCredit9.Text = "Field1";
			this.fldCredit9.Top = 2.59375F;
			this.fldCredit9.Width = 1.03125F;
			// 
			// fldCredit10
			// 
			this.fldCredit10.CanGrow = false;
			this.fldCredit10.Height = 0.19F;
			this.fldCredit10.Left = 4.40625F;
			this.fldCredit10.MultiLine = false;
			this.fldCredit10.Name = "fldCredit10";
			this.fldCredit10.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit10.Tag = "Large";
			this.fldCredit10.Text = "Field1";
			this.fldCredit10.Top = 2.78125F;
			this.fldCredit10.Width = 1.03125F;
			// 
			// fldCheckMessage
			// 
			this.fldCheckMessage.Height = 0.19F;
			this.fldCheckMessage.Left = 0.28125F;
			this.fldCheckMessage.Name = "fldCheckMessage";
			this.fldCheckMessage.Style = "font-family: \'Courier New\'; font-size: 9pt; ddo-char-set: 0";
			this.fldCheckMessage.Tag = "Large";
			this.fldCheckMessage.Text = "happy";
			this.fldCheckMessage.Top = 0.4375F;
			this.fldCheckMessage.Width = 7.3125F;
			// 
			// fldWarrant11
			// 
			this.fldWarrant11.CanGrow = false;
			this.fldWarrant11.Height = 0.19F;
			this.fldWarrant11.Left = 0.3125F;
			this.fldWarrant11.MultiLine = false;
			this.fldWarrant11.Name = "fldWarrant11";
			this.fldWarrant11.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant11.Tag = "Large";
			this.fldWarrant11.Text = "Field1";
			this.fldWarrant11.Top = 2.9375F;
			this.fldWarrant11.Width = 0.46875F;
			// 
			// fldWarrant12
			// 
			this.fldWarrant12.CanGrow = false;
			this.fldWarrant12.Height = 0.19F;
			this.fldWarrant12.Left = 0.3125F;
			this.fldWarrant12.MultiLine = false;
			this.fldWarrant12.Name = "fldWarrant12";
			this.fldWarrant12.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant12.Tag = "Large";
			this.fldWarrant12.Text = "Field1";
			this.fldWarrant12.Top = 3.125F;
			this.fldWarrant12.Width = 0.46875F;
			// 
			// fldWarrant13
			// 
			this.fldWarrant13.CanGrow = false;
			this.fldWarrant13.Height = 0.19F;
			this.fldWarrant13.Left = 0.3125F;
			this.fldWarrant13.MultiLine = false;
			this.fldWarrant13.Name = "fldWarrant13";
			this.fldWarrant13.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant13.Tag = "Large";
			this.fldWarrant13.Text = "Field1";
			this.fldWarrant13.Top = 3.3125F;
			this.fldWarrant13.Width = 0.46875F;
			// 
			// fldWarrant14
			// 
			this.fldWarrant14.CanGrow = false;
			this.fldWarrant14.Height = 0.19F;
			this.fldWarrant14.Left = 0.3125F;
			this.fldWarrant14.MultiLine = false;
			this.fldWarrant14.Name = "fldWarrant14";
			this.fldWarrant14.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant14.Tag = "Large";
			this.fldWarrant14.Text = "Field1";
			this.fldWarrant14.Top = 3.5F;
			this.fldWarrant14.Width = 0.46875F;
			// 
			// fldWarrant15
			// 
			this.fldWarrant15.CanGrow = false;
			this.fldWarrant15.Height = 0.19F;
			this.fldWarrant15.Left = 0.3125F;
			this.fldWarrant15.MultiLine = false;
			this.fldWarrant15.Name = "fldWarrant15";
			this.fldWarrant15.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant15.Tag = "Large";
			this.fldWarrant15.Text = "Field1";
			this.fldWarrant15.Top = 3.6875F;
			this.fldWarrant15.Width = 0.46875F;
			// 
			// fldWarrant16
			// 
			this.fldWarrant16.CanGrow = false;
			this.fldWarrant16.Height = 0.19F;
			this.fldWarrant16.Left = 0.3125F;
			this.fldWarrant16.MultiLine = false;
			this.fldWarrant16.Name = "fldWarrant16";
			this.fldWarrant16.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant16.Tag = "Large";
			this.fldWarrant16.Text = "Field1";
			this.fldWarrant16.Top = 3.875F;
			this.fldWarrant16.Width = 0.46875F;
			// 
			// fldWarrant17
			// 
			this.fldWarrant17.CanGrow = false;
			this.fldWarrant17.Height = 0.19F;
			this.fldWarrant17.Left = 0.3125F;
			this.fldWarrant17.MultiLine = false;
			this.fldWarrant17.Name = "fldWarrant17";
			this.fldWarrant17.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant17.Tag = "Large";
			this.fldWarrant17.Text = "Field1";
			this.fldWarrant17.Top = 4.0625F;
			this.fldWarrant17.Width = 0.46875F;
			// 
			// fldWarrant18
			// 
			this.fldWarrant18.CanGrow = false;
			this.fldWarrant18.Height = 0.19F;
			this.fldWarrant18.Left = 0.3125F;
			this.fldWarrant18.MultiLine = false;
			this.fldWarrant18.Name = "fldWarrant18";
			this.fldWarrant18.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant18.Tag = "Large";
			this.fldWarrant18.Text = "Field1";
			this.fldWarrant18.Top = 4.25F;
			this.fldWarrant18.Width = 0.46875F;
			// 
			// fldWarrant19
			// 
			this.fldWarrant19.CanGrow = false;
			this.fldWarrant19.Height = 0.19F;
			this.fldWarrant19.Left = 0.3125F;
			this.fldWarrant19.MultiLine = false;
			this.fldWarrant19.Name = "fldWarrant19";
			this.fldWarrant19.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant19.Tag = "Large";
			this.fldWarrant19.Text = "Field1";
			this.fldWarrant19.Top = 4.4375F;
			this.fldWarrant19.Width = 0.46875F;
			// 
			// fldWarrant20
			// 
			this.fldWarrant20.CanGrow = false;
			this.fldWarrant20.Height = 0.19F;
			this.fldWarrant20.Left = 0.3125F;
			this.fldWarrant20.MultiLine = false;
			this.fldWarrant20.Name = "fldWarrant20";
			this.fldWarrant20.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldWarrant20.Tag = "Large";
			this.fldWarrant20.Text = "Field1";
			this.fldWarrant20.Top = 4.625F;
			this.fldWarrant20.Width = 0.46875F;
			// 
			// fldDescription11
			// 
			this.fldDescription11.CanGrow = false;
			this.fldDescription11.Height = 0.19F;
			this.fldDescription11.Left = 0.875F;
			this.fldDescription11.MultiLine = false;
			this.fldDescription11.Name = "fldDescription11";
			this.fldDescription11.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription11.Tag = "Large";
			this.fldDescription11.Text = "Field1";
			this.fldDescription11.Top = 2.9375F;
			this.fldDescription11.Width = 2F;
			// 
			// fldDescription12
			// 
			this.fldDescription12.CanGrow = false;
			this.fldDescription12.Height = 0.19F;
			this.fldDescription12.Left = 0.875F;
			this.fldDescription12.MultiLine = false;
			this.fldDescription12.Name = "fldDescription12";
			this.fldDescription12.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription12.Tag = "Large";
			this.fldDescription12.Text = "Field1";
			this.fldDescription12.Top = 3.125F;
			this.fldDescription12.Width = 2F;
			// 
			// fldDescription13
			// 
			this.fldDescription13.CanGrow = false;
			this.fldDescription13.Height = 0.19F;
			this.fldDescription13.Left = 0.875F;
			this.fldDescription13.MultiLine = false;
			this.fldDescription13.Name = "fldDescription13";
			this.fldDescription13.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription13.Tag = "Large";
			this.fldDescription13.Text = "Field1";
			this.fldDescription13.Top = 3.3125F;
			this.fldDescription13.Width = 2F;
			// 
			// fldDescription14
			// 
			this.fldDescription14.CanGrow = false;
			this.fldDescription14.Height = 0.19F;
			this.fldDescription14.Left = 0.875F;
			this.fldDescription14.MultiLine = false;
			this.fldDescription14.Name = "fldDescription14";
			this.fldDescription14.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription14.Tag = "Large";
			this.fldDescription14.Text = "Field1";
			this.fldDescription14.Top = 3.5F;
			this.fldDescription14.Width = 2F;
			// 
			// fldDescription15
			// 
			this.fldDescription15.CanGrow = false;
			this.fldDescription15.Height = 0.19F;
			this.fldDescription15.Left = 0.875F;
			this.fldDescription15.MultiLine = false;
			this.fldDescription15.Name = "fldDescription15";
			this.fldDescription15.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription15.Tag = "Large";
			this.fldDescription15.Text = "Field1";
			this.fldDescription15.Top = 3.6875F;
			this.fldDescription15.Width = 2F;
			// 
			// fldDescription16
			// 
			this.fldDescription16.CanGrow = false;
			this.fldDescription16.Height = 0.19F;
			this.fldDescription16.Left = 0.875F;
			this.fldDescription16.MultiLine = false;
			this.fldDescription16.Name = "fldDescription16";
			this.fldDescription16.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription16.Tag = "Large";
			this.fldDescription16.Text = "Field1";
			this.fldDescription16.Top = 3.875F;
			this.fldDescription16.Width = 2F;
			// 
			// fldDescription17
			// 
			this.fldDescription17.CanGrow = false;
			this.fldDescription17.Height = 0.19F;
			this.fldDescription17.Left = 0.875F;
			this.fldDescription17.MultiLine = false;
			this.fldDescription17.Name = "fldDescription17";
			this.fldDescription17.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription17.Tag = "Large";
			this.fldDescription17.Text = "Field1";
			this.fldDescription17.Top = 4.0625F;
			this.fldDescription17.Width = 2F;
			// 
			// fldDescription18
			// 
			this.fldDescription18.CanGrow = false;
			this.fldDescription18.Height = 0.19F;
			this.fldDescription18.Left = 0.875F;
			this.fldDescription18.MultiLine = false;
			this.fldDescription18.Name = "fldDescription18";
			this.fldDescription18.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription18.Tag = "Large";
			this.fldDescription18.Text = "Field1";
			this.fldDescription18.Top = 4.25F;
			this.fldDescription18.Width = 2F;
			// 
			// fldDescription19
			// 
			this.fldDescription19.CanGrow = false;
			this.fldDescription19.Height = 0.19F;
			this.fldDescription19.Left = 0.875F;
			this.fldDescription19.MultiLine = false;
			this.fldDescription19.Name = "fldDescription19";
			this.fldDescription19.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription19.Tag = "Large";
			this.fldDescription19.Text = "Field1";
			this.fldDescription19.Top = 4.4375F;
			this.fldDescription19.Width = 2F;
			// 
			// fldDescription20
			// 
			this.fldDescription20.CanGrow = false;
			this.fldDescription20.Height = 0.19F;
			this.fldDescription20.Left = 0.875F;
			this.fldDescription20.MultiLine = false;
			this.fldDescription20.Name = "fldDescription20";
			this.fldDescription20.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDescription20.Tag = "Large";
			this.fldDescription20.Text = "Field1";
			this.fldDescription20.Top = 4.625F;
			this.fldDescription20.Width = 2F;
			// 
			// fldReference11
			// 
			this.fldReference11.CanGrow = false;
			this.fldReference11.Height = 0.19F;
			this.fldReference11.Left = 3F;
			this.fldReference11.MultiLine = false;
			this.fldReference11.Name = "fldReference11";
			this.fldReference11.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference11.Tag = "Large";
			this.fldReference11.Text = "Field1";
			this.fldReference11.Top = 2.9375F;
			this.fldReference11.Width = 1.34375F;
			// 
			// fldReference12
			// 
			this.fldReference12.CanGrow = false;
			this.fldReference12.Height = 0.19F;
			this.fldReference12.Left = 3F;
			this.fldReference12.MultiLine = false;
			this.fldReference12.Name = "fldReference12";
			this.fldReference12.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference12.Tag = "Large";
			this.fldReference12.Text = "Field1";
			this.fldReference12.Top = 3.125F;
			this.fldReference12.Width = 1.34375F;
			// 
			// fldReference13
			// 
			this.fldReference13.CanGrow = false;
			this.fldReference13.Height = 0.19F;
			this.fldReference13.Left = 3F;
			this.fldReference13.MultiLine = false;
			this.fldReference13.Name = "fldReference13";
			this.fldReference13.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference13.Tag = "Large";
			this.fldReference13.Text = "Field1";
			this.fldReference13.Top = 3.3125F;
			this.fldReference13.Width = 1.34375F;
			// 
			// fldReference14
			// 
			this.fldReference14.CanGrow = false;
			this.fldReference14.Height = 0.19F;
			this.fldReference14.Left = 3F;
			this.fldReference14.MultiLine = false;
			this.fldReference14.Name = "fldReference14";
			this.fldReference14.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference14.Tag = "Large";
			this.fldReference14.Text = "Field1";
			this.fldReference14.Top = 3.5F;
			this.fldReference14.Width = 1.34375F;
			// 
			// fldReference15
			// 
			this.fldReference15.CanGrow = false;
			this.fldReference15.Height = 0.19F;
			this.fldReference15.Left = 3F;
			this.fldReference15.MultiLine = false;
			this.fldReference15.Name = "fldReference15";
			this.fldReference15.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference15.Tag = "Large";
			this.fldReference15.Text = "Field1";
			this.fldReference15.Top = 3.6875F;
			this.fldReference15.Width = 1.34375F;
			// 
			// fldReference16
			// 
			this.fldReference16.CanGrow = false;
			this.fldReference16.Height = 0.19F;
			this.fldReference16.Left = 3F;
			this.fldReference16.MultiLine = false;
			this.fldReference16.Name = "fldReference16";
			this.fldReference16.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference16.Tag = "Large";
			this.fldReference16.Text = "Field1";
			this.fldReference16.Top = 3.875F;
			this.fldReference16.Width = 1.34375F;
			// 
			// fldReference17
			// 
			this.fldReference17.CanGrow = false;
			this.fldReference17.Height = 0.19F;
			this.fldReference17.Left = 3F;
			this.fldReference17.MultiLine = false;
			this.fldReference17.Name = "fldReference17";
			this.fldReference17.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference17.Tag = "Large";
			this.fldReference17.Text = "Field1";
			this.fldReference17.Top = 4.0625F;
			this.fldReference17.Width = 1.34375F;
			// 
			// fldReference18
			// 
			this.fldReference18.CanGrow = false;
			this.fldReference18.Height = 0.19F;
			this.fldReference18.Left = 3F;
			this.fldReference18.MultiLine = false;
			this.fldReference18.Name = "fldReference18";
			this.fldReference18.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference18.Tag = "Large";
			this.fldReference18.Text = "Field1";
			this.fldReference18.Top = 4.25F;
			this.fldReference18.Width = 1.34375F;
			// 
			// fldReference19
			// 
			this.fldReference19.CanGrow = false;
			this.fldReference19.Height = 0.19F;
			this.fldReference19.Left = 3F;
			this.fldReference19.MultiLine = false;
			this.fldReference19.Name = "fldReference19";
			this.fldReference19.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference19.Tag = "Large";
			this.fldReference19.Text = "Field1";
			this.fldReference19.Top = 4.4375F;
			this.fldReference19.Width = 1.34375F;
			// 
			// fldReference20
			// 
			this.fldReference20.CanGrow = false;
			this.fldReference20.Height = 0.19F;
			this.fldReference20.Left = 3F;
			this.fldReference20.MultiLine = false;
			this.fldReference20.Name = "fldReference20";
			this.fldReference20.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldReference20.Tag = "Large";
			this.fldReference20.Text = "Field1";
			this.fldReference20.Top = 4.625F;
			this.fldReference20.Width = 1.34375F;
			// 
			// fldDiscount11
			// 
			this.fldDiscount11.CanGrow = false;
			this.fldDiscount11.Height = 0.19F;
			this.fldDiscount11.Left = 5.5625F;
			this.fldDiscount11.MultiLine = false;
			this.fldDiscount11.Name = "fldDiscount11";
			this.fldDiscount11.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount11.Tag = "Large";
			this.fldDiscount11.Text = "Field1";
			this.fldDiscount11.Top = 2.9375F;
			this.fldDiscount11.Width = 1.03125F;
			// 
			// fldDiscount12
			// 
			this.fldDiscount12.CanGrow = false;
			this.fldDiscount12.Height = 0.19F;
			this.fldDiscount12.Left = 5.5625F;
			this.fldDiscount12.MultiLine = false;
			this.fldDiscount12.Name = "fldDiscount12";
			this.fldDiscount12.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount12.Tag = "Large";
			this.fldDiscount12.Text = "Field1";
			this.fldDiscount12.Top = 3.125F;
			this.fldDiscount12.Width = 1.03125F;
			// 
			// fldDiscount13
			// 
			this.fldDiscount13.CanGrow = false;
			this.fldDiscount13.Height = 0.19F;
			this.fldDiscount13.Left = 5.5625F;
			this.fldDiscount13.MultiLine = false;
			this.fldDiscount13.Name = "fldDiscount13";
			this.fldDiscount13.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount13.Tag = "Large";
			this.fldDiscount13.Text = "Field1";
			this.fldDiscount13.Top = 3.3125F;
			this.fldDiscount13.Width = 1.03125F;
			// 
			// fldDiscount14
			// 
			this.fldDiscount14.CanGrow = false;
			this.fldDiscount14.Height = 0.19F;
			this.fldDiscount14.Left = 5.5625F;
			this.fldDiscount14.MultiLine = false;
			this.fldDiscount14.Name = "fldDiscount14";
			this.fldDiscount14.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount14.Tag = "Large";
			this.fldDiscount14.Text = "Field1";
			this.fldDiscount14.Top = 3.5F;
			this.fldDiscount14.Width = 1.03125F;
			// 
			// fldDiscount15
			// 
			this.fldDiscount15.CanGrow = false;
			this.fldDiscount15.Height = 0.19F;
			this.fldDiscount15.Left = 5.5625F;
			this.fldDiscount15.MultiLine = false;
			this.fldDiscount15.Name = "fldDiscount15";
			this.fldDiscount15.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount15.Tag = "Large";
			this.fldDiscount15.Text = "Field1";
			this.fldDiscount15.Top = 3.6875F;
			this.fldDiscount15.Width = 1.03125F;
			// 
			// fldDiscount16
			// 
			this.fldDiscount16.CanGrow = false;
			this.fldDiscount16.Height = 0.19F;
			this.fldDiscount16.Left = 5.5625F;
			this.fldDiscount16.MultiLine = false;
			this.fldDiscount16.Name = "fldDiscount16";
			this.fldDiscount16.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount16.Tag = "Large";
			this.fldDiscount16.Text = "Field1";
			this.fldDiscount16.Top = 3.875F;
			this.fldDiscount16.Width = 1.03125F;
			// 
			// fldDiscount17
			// 
			this.fldDiscount17.CanGrow = false;
			this.fldDiscount17.Height = 0.19F;
			this.fldDiscount17.Left = 5.5625F;
			this.fldDiscount17.MultiLine = false;
			this.fldDiscount17.Name = "fldDiscount17";
			this.fldDiscount17.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount17.Tag = "Large";
			this.fldDiscount17.Text = "Field1";
			this.fldDiscount17.Top = 4.0625F;
			this.fldDiscount17.Width = 1.03125F;
			// 
			// fldDiscount18
			// 
			this.fldDiscount18.CanGrow = false;
			this.fldDiscount18.Height = 0.19F;
			this.fldDiscount18.Left = 5.5625F;
			this.fldDiscount18.MultiLine = false;
			this.fldDiscount18.Name = "fldDiscount18";
			this.fldDiscount18.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount18.Tag = "Large";
			this.fldDiscount18.Text = "Field1";
			this.fldDiscount18.Top = 4.25F;
			this.fldDiscount18.Width = 1.03125F;
			// 
			// fldDiscount19
			// 
			this.fldDiscount19.CanGrow = false;
			this.fldDiscount19.Height = 0.19F;
			this.fldDiscount19.Left = 5.5625F;
			this.fldDiscount19.MultiLine = false;
			this.fldDiscount19.Name = "fldDiscount19";
			this.fldDiscount19.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount19.Tag = "Large";
			this.fldDiscount19.Text = "Field1";
			this.fldDiscount19.Top = 4.4375F;
			this.fldDiscount19.Width = 1.03125F;
			// 
			// fldDiscount20
			// 
			this.fldDiscount20.CanGrow = false;
			this.fldDiscount20.Height = 0.19F;
			this.fldDiscount20.Left = 5.5625F;
			this.fldDiscount20.MultiLine = false;
			this.fldDiscount20.Name = "fldDiscount20";
			this.fldDiscount20.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldDiscount20.Tag = "Large";
			this.fldDiscount20.Text = "Field1";
			this.fldDiscount20.Top = 4.625F;
			this.fldDiscount20.Width = 1.03125F;
			// 
			// fldAmt11
			// 
			this.fldAmt11.CanGrow = false;
			this.fldAmt11.Height = 0.19F;
			this.fldAmt11.Left = 6.71875F;
			this.fldAmt11.MultiLine = false;
			this.fldAmt11.Name = "fldAmt11";
			this.fldAmt11.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt11.Tag = "Large";
			this.fldAmt11.Text = "Field1";
			this.fldAmt11.Top = 2.9375F;
			this.fldAmt11.Width = 1.03125F;
			// 
			// fldAmt12
			// 
			this.fldAmt12.CanGrow = false;
			this.fldAmt12.Height = 0.19F;
			this.fldAmt12.Left = 6.71875F;
			this.fldAmt12.MultiLine = false;
			this.fldAmt12.Name = "fldAmt12";
			this.fldAmt12.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt12.Tag = "Large";
			this.fldAmt12.Text = "Field1";
			this.fldAmt12.Top = 3.125F;
			this.fldAmt12.Width = 1.03125F;
			// 
			// fldAmt13
			// 
			this.fldAmt13.CanGrow = false;
			this.fldAmt13.Height = 0.19F;
			this.fldAmt13.Left = 6.71875F;
			this.fldAmt13.MultiLine = false;
			this.fldAmt13.Name = "fldAmt13";
			this.fldAmt13.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt13.Tag = "Large";
			this.fldAmt13.Text = "Field1";
			this.fldAmt13.Top = 3.3125F;
			this.fldAmt13.Width = 1.03125F;
			// 
			// fldAmt14
			// 
			this.fldAmt14.CanGrow = false;
			this.fldAmt14.Height = 0.19F;
			this.fldAmt14.Left = 6.71875F;
			this.fldAmt14.MultiLine = false;
			this.fldAmt14.Name = "fldAmt14";
			this.fldAmt14.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt14.Tag = "Large";
			this.fldAmt14.Text = "Field1";
			this.fldAmt14.Top = 3.5F;
			this.fldAmt14.Width = 1.03125F;
			// 
			// fldAmt15
			// 
			this.fldAmt15.CanGrow = false;
			this.fldAmt15.Height = 0.19F;
			this.fldAmt15.Left = 6.71875F;
			this.fldAmt15.MultiLine = false;
			this.fldAmt15.Name = "fldAmt15";
			this.fldAmt15.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt15.Tag = "Large";
			this.fldAmt15.Text = "Field1";
			this.fldAmt15.Top = 3.6875F;
			this.fldAmt15.Width = 1.03125F;
			// 
			// fldAmt16
			// 
			this.fldAmt16.CanGrow = false;
			this.fldAmt16.Height = 0.19F;
			this.fldAmt16.Left = 6.71875F;
			this.fldAmt16.MultiLine = false;
			this.fldAmt16.Name = "fldAmt16";
			this.fldAmt16.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt16.Tag = "Large";
			this.fldAmt16.Text = "Field1";
			this.fldAmt16.Top = 3.875F;
			this.fldAmt16.Width = 1.03125F;
			// 
			// fldAmt17
			// 
			this.fldAmt17.CanGrow = false;
			this.fldAmt17.Height = 0.19F;
			this.fldAmt17.Left = 6.71875F;
			this.fldAmt17.MultiLine = false;
			this.fldAmt17.Name = "fldAmt17";
			this.fldAmt17.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt17.Tag = "Large";
			this.fldAmt17.Text = "Field1";
			this.fldAmt17.Top = 4.0625F;
			this.fldAmt17.Width = 1.03125F;
			// 
			// fldAmt18
			// 
			this.fldAmt18.CanGrow = false;
			this.fldAmt18.Height = 0.19F;
			this.fldAmt18.Left = 6.71875F;
			this.fldAmt18.MultiLine = false;
			this.fldAmt18.Name = "fldAmt18";
			this.fldAmt18.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt18.Tag = "Large";
			this.fldAmt18.Text = "Field1";
			this.fldAmt18.Top = 4.25F;
			this.fldAmt18.Width = 1.03125F;
			// 
			// fldAmt19
			// 
			this.fldAmt19.CanGrow = false;
			this.fldAmt19.Height = 0.19F;
			this.fldAmt19.Left = 6.71875F;
			this.fldAmt19.MultiLine = false;
			this.fldAmt19.Name = "fldAmt19";
			this.fldAmt19.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt19.Tag = "Large";
			this.fldAmt19.Text = "Field1";
			this.fldAmt19.Top = 4.4375F;
			this.fldAmt19.Width = 1.03125F;
			// 
			// fldAmt20
			// 
			this.fldAmt20.CanGrow = false;
			this.fldAmt20.Height = 0.19F;
			this.fldAmt20.Left = 6.71875F;
			this.fldAmt20.MultiLine = false;
			this.fldAmt20.Name = "fldAmt20";
			this.fldAmt20.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmt20.Tag = "Large";
			this.fldAmt20.Text = "Field1";
			this.fldAmt20.Top = 4.625F;
			this.fldAmt20.Width = 1.03125F;
			// 
			// fldCredit11
			// 
			this.fldCredit11.CanGrow = false;
			this.fldCredit11.Height = 0.19F;
			this.fldCredit11.Left = 4.40625F;
			this.fldCredit11.MultiLine = false;
			this.fldCredit11.Name = "fldCredit11";
			this.fldCredit11.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit11.Tag = "Large";
			this.fldCredit11.Text = "Field1";
			this.fldCredit11.Top = 2.9375F;
			this.fldCredit11.Width = 1.03125F;
			// 
			// fldCredit12
			// 
			this.fldCredit12.CanGrow = false;
			this.fldCredit12.Height = 0.19F;
			this.fldCredit12.Left = 4.40625F;
			this.fldCredit12.MultiLine = false;
			this.fldCredit12.Name = "fldCredit12";
			this.fldCredit12.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit12.Tag = "Large";
			this.fldCredit12.Text = "Field1";
			this.fldCredit12.Top = 3.125F;
			this.fldCredit12.Width = 1.03125F;
			// 
			// fldCredit13
			// 
			this.fldCredit13.CanGrow = false;
			this.fldCredit13.Height = 0.19F;
			this.fldCredit13.Left = 4.40625F;
			this.fldCredit13.MultiLine = false;
			this.fldCredit13.Name = "fldCredit13";
			this.fldCredit13.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit13.Tag = "Large";
			this.fldCredit13.Text = "Field1";
			this.fldCredit13.Top = 3.3125F;
			this.fldCredit13.Width = 1.03125F;
			// 
			// fldCredit14
			// 
			this.fldCredit14.CanGrow = false;
			this.fldCredit14.Height = 0.19F;
			this.fldCredit14.Left = 4.40625F;
			this.fldCredit14.MultiLine = false;
			this.fldCredit14.Name = "fldCredit14";
			this.fldCredit14.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit14.Tag = "Large";
			this.fldCredit14.Text = "Field1";
			this.fldCredit14.Top = 3.5F;
			this.fldCredit14.Width = 1.03125F;
			// 
			// fldCredit15
			// 
			this.fldCredit15.CanGrow = false;
			this.fldCredit15.Height = 0.19F;
			this.fldCredit15.Left = 4.40625F;
			this.fldCredit15.MultiLine = false;
			this.fldCredit15.Name = "fldCredit15";
			this.fldCredit15.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit15.Tag = "Large";
			this.fldCredit15.Text = "Field1";
			this.fldCredit15.Top = 3.6875F;
			this.fldCredit15.Width = 1.03125F;
			// 
			// fldCredit16
			// 
			this.fldCredit16.CanGrow = false;
			this.fldCredit16.Height = 0.19F;
			this.fldCredit16.Left = 4.40625F;
			this.fldCredit16.MultiLine = false;
			this.fldCredit16.Name = "fldCredit16";
			this.fldCredit16.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit16.Tag = "Large";
			this.fldCredit16.Text = "Field1";
			this.fldCredit16.Top = 3.875F;
			this.fldCredit16.Width = 1.03125F;
			// 
			// fldCredit17
			// 
			this.fldCredit17.CanGrow = false;
			this.fldCredit17.Height = 0.19F;
			this.fldCredit17.Left = 4.40625F;
			this.fldCredit17.MultiLine = false;
			this.fldCredit17.Name = "fldCredit17";
			this.fldCredit17.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit17.Tag = "Large";
			this.fldCredit17.Text = "Field1";
			this.fldCredit17.Top = 4.0625F;
			this.fldCredit17.Width = 1.03125F;
			// 
			// fldCredit18
			// 
			this.fldCredit18.CanGrow = false;
			this.fldCredit18.Height = 0.19F;
			this.fldCredit18.Left = 4.40625F;
			this.fldCredit18.MultiLine = false;
			this.fldCredit18.Name = "fldCredit18";
			this.fldCredit18.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit18.Tag = "Large";
			this.fldCredit18.Text = "Field1";
			this.fldCredit18.Top = 4.25F;
			this.fldCredit18.Width = 1.03125F;
			// 
			// fldCredit19
			// 
			this.fldCredit19.CanGrow = false;
			this.fldCredit19.Height = 0.19F;
			this.fldCredit19.Left = 4.40625F;
			this.fldCredit19.MultiLine = false;
			this.fldCredit19.Name = "fldCredit19";
			this.fldCredit19.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit19.Tag = "Large";
			this.fldCredit19.Text = "Field1";
			this.fldCredit19.Top = 4.4375F;
			this.fldCredit19.Width = 1.03125F;
			// 
			// fldCredit20
			// 
			this.fldCredit20.CanGrow = false;
			this.fldCredit20.Height = 0.19F;
			this.fldCredit20.Left = 4.40625F;
			this.fldCredit20.MultiLine = false;
			this.fldCredit20.Name = "fldCredit20";
			this.fldCredit20.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldCredit20.Tag = "Large";
			this.fldCredit20.Text = "Field1";
			this.fldCredit20.Top = 4.625F;
			this.fldCredit20.Width = 1.03125F;
			// 
			// srptCustomCheckDoubleStub
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDiscount20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmt20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredit20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReference;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDiscount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDiscount20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmt20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredit20;
	}
}
