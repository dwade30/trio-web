﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckRegister.
	/// </summary>
	public partial class rptCheckRegister : BaseSectionReport
	{
		public static rptCheckRegister InstancePtr
		{
			get
			{
				return (rptCheckRegister)Sys.GetInstance(typeof(rptCheckRegister));
			}
		}

		protected rptCheckRegister _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsCheckInfo.Dispose();
				rsOverride.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckRegister	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		int intTotalRegular;
		int intTotalVoid;
		int intWarrant;
		int intBank;
		string strTownOverride = "";
		clsDRWrapper rsOverride = new clsDRWrapper();

		public rptCheckRegister()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Check Register";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string intVendor = "";
			Decimal curAmount;
			string strCheckNumber = "";
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsCheckInfo.MoveNext();
				CheckAgain:
				;
				if (rsCheckInfo.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" || rsCheckInfo.Get_Fields_Boolean("CarryOver") == true)
					{
						intVendor = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("VendorNumber"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curAmount = FCConvert.ToDecimal(rsCheckInfo.Get_Fields("Amount"));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						strCheckNumber = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
						rsCheckInfo.MovePrevious();
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("VendorNumber")) == intVendor && FCConvert.ToDecimal(rsCheckInfo.Get_Fields("Amount")) == curAmount && FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber")) == strCheckNumber)
						{
							rsCheckInfo.MoveNext();
							rsCheckInfo.MoveNext();
							goto CheckAgain;
						}
						else
						{
							rsCheckInfo.MoveNext();
						}
					}
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			if (frmCheckRegister.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Reprint WHERE Type = 'C' ORDER BY ReportOrder");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ReportOrder")) < 9)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReportOrder", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportOrder")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						else
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("Type", "C");
				rsInfo.Set_Fields("ReportOrder", 1);
				rsInfo.Set_Fields("WarrantNumber", intWarrant);
				rsInfo.Update(true);
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'C' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(intWarrant) + "')");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					do
					{
						rsMasterJournal.Edit();
						rsMasterJournal.Set_Fields("Status", "R");
						rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
						rsMasterJournal.Update(true);
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
				}
				// rsInfo.Execute "DELETE FROM TempCheckFile WHERE ReportNumber = 9"
				//FC:FINAL:MSH - Issue #642: Single qoutes added to SQL query, because when WarrantNumber equal "" SQL query has a incorrect format and throw an error
				//rsInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE (ISNULL(ReportNumber, 0) = 0 AND WarrantNumber = " + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("WarrantNumber") + ") or ISNULL(ReportNumber, 0) > 0");
				rsInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE (ISNULL(ReportNumber, 0) = 0 AND WarrantNumber = '" + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_Int32("WarrantNumber") + "') or ISNULL(ReportNumber, 0) > 0");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (rsInfo.Get_Fields_Boolean("CarryOver") == true)
						{
							// Check = '" & rsInfo.Fields["CheckNumber"] + 1 & "',
							if (Information.IsDate(rsInfo.Get_Fields("Checkdate")))
							{
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								rsMasterJournal.Execute("UPDATE APJournal SET Status = 'R' WHERE CheckNumber = '" + rsInfo.Get_Fields("CheckNumber") + "' AND CheckDate = '" + rsInfo.Get_Fields_DateTime("CheckDate") + "' AND Warrant = '" + rsInfo.Get_Fields_Int32("WarrantNumber") + "'", "Budgetary");
							}
						}
						rsInfo.Edit();
						rsInfo.Set_Fields("ReportNumber", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportNumber")) + 1);
						rsInfo.Update(true);
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
			}

            if (frmCheckRegister.InstancePtr.rsMultipleWarrants.RecordCount() <= 1)
            {
                frmCheckRegister.InstancePtr.Close();
            }

        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmCheckRegister.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				//FC:FINAL:MSH - Issue #642: Single qoutes added to SQL query, because when WarrantNumber equal "" SQL query has a incorrect format and throw an error
				//rsCheckInfo.OpenRecordset($"SELECT * FROM TempCheckFile WHERE ISNULL(ReportNumber, 0) = 0 AND WarrantNumber = " + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields("WarrantNumber") + " ORDER BY CheckNumber, VendorName, Status");
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ISNULL(ReportNumber, 0) = 0 AND WarrantNumber = '" + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_Int32("WarrantNumber") + "' ORDER BY CheckNumber, VendorName, Status");
				rsBankInfo.OpenRecordset("SELECT DISTINCT Banks.Name as Name FROM (APJournal INNER JOIN JournalMaster On APJournal.JournalNumber = JournalMaster.JournalNumber AND APJournal.Period = JournalMaster.Period) INNER JOIN Banks On JournalMaster.BankNumber = Banks.ID WHERE APJournal.Warrant = '" + frmCheckRegister.InstancePtr.rsMultipleWarrants.Get_Fields_Int32("WarrantNumber") + "'", "TWBD0000.vb1");
				Label21.Visible = false;
			}
			else
			{
				//FC:FINAL:MSH - Issue #642: Single qoutes added to SQL query, because when WarrantNumber equal "" SQL query has a incorrect format and throw an error
				//rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE WarrantNumber = " + FCConvert.ToString(Conversion.Val(Strings.Mid(frmCheckRegister.InstancePtr.cboReport.Text, 9, 4))) + " ORDER BY CheckNumber, VendorName, Status");
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE WarrantNumber = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmCheckRegister.InstancePtr.cboReport.Text, 9, 4))) + "' ORDER BY CheckNumber, VendorName, Status");
				rsBankInfo.OpenRecordset("SELECT DISTINCT Banks.Name as Name FROM (APJournal INNER JOIN JournalMaster On APJournal.JournalNumber = JournalMaster.JournalNumber AND APJournal.Period = JournalMaster.Period) INNER JOIN Banks On JournalMaster.BankNumber = Banks.ID WHERE APJournal.Warrant = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmCheckRegister.InstancePtr.cboReport.Text, 9, 4))) + "'", "TWBD0000.vb1");
				Label21.Visible = true;
			}
			if (rsBankInfo.RecordCount() > 1)
			{
				lblBankName.Text = "Bank: Multiple Banks";
			}
			else
			{
				lblBankName.Text = "Bank: " + rsBankInfo.Get_Fields_String("Name");
			}
			curTotal = 0;
			intTotalRegular = 0;
			intTotalVoid = 0;
			blnFirstRecord = true;
			if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank")) != 0)
			{
				intBank = FCConvert.ToInt32(modBudgetaryAccounting.GetBankVariable("APBank"));
			}
			else
			{
				intBank = -1;
			}
			if (intBank != -1)
			{
				rsOverride.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(intBank));
				if (rsOverride.EndOfFile() != true && rsOverride.BeginningOfFile() != true)
				{
					strTownOverride = Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("TownCashAccount")));
				}
			}
			// Me.Document.Printer.PrinterName = frmCheckRegister.strPrinterName
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsMaster = new clsDRWrapper();
			int counter = 0;
			int intTempBank;
			clsDRWrapper rsAltCash = new clsDRWrapper();
			int lngJournal = 0;
			int intPeriod = 0;
			bool blnFound = false;
			rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
            if (rsJournalInfo.RecordCount() == 0)
            {
                rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals"))
                    .Replace("Isnull(Seperate, 0) = 0", "Isnull(Seperate, 0) = 1"));
            }

            if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" || rsCheckInfo.Get_Fields_Boolean("CarryOver") == true)
			{
				intTotalVoid += 1;
			}
			else
			{
				intTotalRegular += 1;
			}
			if (rsCheckInfo.Get_Fields_Boolean("CarryOver") == true)
			{
				fldType.Text = "V";
			}
			else if (rsCheckInfo.Get_Fields_Boolean("EFTCheck") == true)
			{
				fldType.Text = "E";
			}
			else
			{
				fldType.Text = rsCheckInfo.Get_Fields_String("Status");
			}
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format(rsCheckInfo.Get_Fields("Amount"), "#,##0.00");
			if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V" && rsCheckInfo.Get_Fields_Boolean("CarryOver") == false)
			{
				//FC:FINAL:MSH - Issue #642: can't implicitly convert to decimal
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotal += FCConvert.ToDecimal(rsCheckInfo.Get_Fields("Amount"));
			}
			fldDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
			if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" || rsCheckInfo.Get_Fields_Boolean("CarryOver") == true)
			{
				fldWarrant.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_Int32("WarrantNumber"));
				intWarrant = FCConvert.ToInt32(rsCheckInfo.Get_Fields_Int32("WarrantNumber"));
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				fldWarrant.Text = FCConvert.ToString(rsJournalInfo.Get_Fields("Warrant"));
				//FC:FINAL:MSH - Issue #642: FCConvert.ToInt32 replaced by Int32.TryParse, because when 'Warrant' equal "" will throw an error
				//intWarrant = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Warrant"));
				int tempWarrant;
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				intWarrant = Int32.TryParse(rsJournalInfo.Get_Fields("Warrant"), out tempWarrant) ? tempWarrant : default(Int32);
			}
			fldPayee.Text = modValidateAccount.GetFormat_6(rsCheckInfo.Get_Fields_Int32("VendorNumber"), 4) + "  " + rsCheckInfo.Get_Fields_String("VendorName");
			if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" && FCConvert.ToInt32(rsCheckInfo.Get_Fields_Boolean("CarryOver")) == -1)
			{
				if (intBank == -1 && frmCheckRegister.InstancePtr.cmbInitial.SelectedIndex == 0)
				{
					counter = 0;
					do
					{
						counter += 1;
						rsCheckInfo.MoveNext();
						if (rsCheckInfo.EndOfFile() != true)
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							if ((FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V" && FCConvert.ToInt32(rsCheckInfo.Get_Fields_Boolean("CarryOver")) == 0) || rsCheckInfo.Get_Fields("CheckNumber") != fldCheck.Text)
							{
								rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
								// corey
								if (!rsJournalInfo.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
									lngJournal = FCConvert.ToInt32(rsJournalInfo.Get_Fields("JournalNumber"));
									// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
									intPeriod = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Period"));
									break;
								}
							}
						}
					}
					while (rsCheckInfo.EndOfFile() != true);
					if (counter == 1)
					{
						for (counter = counter; counter >= 1; counter--)
						{
							rsCheckInfo.MovePrevious();
						}
						rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
					}
					else
					{
						for (counter = counter; counter >= 1; counter--)
						{
							rsCheckInfo.MovePrevious();
						}
					}
					rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod));
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V" || (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" && FCConvert.ToInt32(rsCheckInfo.Get_Fields("Amount")) == 0))
				{
					if (intBank == -1)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + rsJournalInfo.Get_Fields("JournalNumber") + " AND Period = " + rsJournalInfo.Get_Fields("Period"));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						rsOverride.OpenRecordset("SELECT * FROM Banks WHERE ID = " + rsMaster.Get_Fields("BankNumber"));
						if (rsOverride.EndOfFile() != true && rsOverride.BeginningOfFile() != true)
						{
							strTownOverride = Strings.Trim(FCConvert.ToString(rsOverride.Get_Fields_String("TownCashAccount")));
						}
					}
					do
					{
						if (frmCheckRegister.InstancePtr.cmbInitial.SelectedIndex == 0)
						{
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("Status", "R");
							rsJournalInfo.Set_Fields("TownOverride", strTownOverride);
							rsJournalInfo.Set_Fields("SchoolOverride", "");
							rsJournalInfo.Update(true);
							rsJournalInfo.MoveNext();
						}
						else
						{
							break;
						}
					}
					while (rsJournalInfo.EndOfFile() != true);
				}
				else
				{
					counter = 0;
					blnFound = false;
					do
					{
						counter += 1;
						rsCheckInfo.MoveNext();
						if (rsCheckInfo.EndOfFile() != true)
						{
							if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V")
							{
								rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
								blnFound = true;
								break;
							}
						}
					}
					while (rsCheckInfo.EndOfFile() != true);
					for (counter = counter; counter >= 1; counter--)
					{
						rsCheckInfo.MovePrevious();
					}
					if (!blnFound)
					{
						counter = 0;
						blnFound = false;
						do
						{
							counter += 1;
							rsCheckInfo.MovePrevious();
							if (rsCheckInfo.EndOfFile() != true)
							{
								if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V")
								{
									rsJournalInfo.OpenRecordset(FCConvert.ToString(rsCheckInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
									blnFound = true;
									break;
								}
							}
						}
						while (rsCheckInfo.EndOfFile() != true);
						for (counter = counter; counter >= 1; counter--)
						{
							rsCheckInfo.MoveNext();
						}
					}
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + rsJournalInfo.Get_Fields("JournalNumber") + " AND Period = " + rsJournalInfo.Get_Fields("Period"));
				}
			}
			rsCheckRec.OmitNullsOnInsert = true;
			if (frmCheckRegister.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = 0");
				rsCheckRec.AddNew();
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				rsCheckRec.Set_Fields("CheckNumber", rsCheckInfo.Get_Fields("CheckNumber"));
				rsCheckRec.Set_Fields("Type", "1");
				rsCheckRec.Set_Fields("CheckDate", rsCheckInfo.Get_Fields_DateTime("CheckDate"));
				rsCheckRec.Set_Fields("Name", rsCheckInfo.Get_Fields_String("VendorName"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				rsCheckRec.Set_Fields("Amount", rsCheckInfo.Get_Fields("Amount"));
				if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) == "V" || rsCheckInfo.Get_Fields_Boolean("CarryOver") == true)
				{
					rsCheckRec.Set_Fields("Status", "V");
				}
				else
				{
					rsCheckRec.Set_Fields("Status", "1");
				}
				rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
				if (FCConvert.ToString(rsCheckInfo.Get_Fields_String("Status")) != "V")
				{
					rsJournalInfo.MoveFirst();
					if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("PrepaidCheck")) && FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("UseAlternateCash")))
					{
						rsAltCash.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Account = '" + rsJournalInfo.Get_Fields_String("AlternateCashAccount") + "'");
						if (rsAltCash.EndOfFile() != true && rsAltCash.BeginningOfFile() != true)
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							rsCheckRec.Set_Fields("BankNumber", rsAltCash.Get_Fields("BankNumber"));
							goto UpdateRecord;
						}
					}
				}
				if (intBank > 0)
				{
					rsCheckRec.Set_Fields("BankNumber", intBank);
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					rsCheckRec.Set_Fields("BankNumber", rsMaster.Get_Fields("BankNumber"));
				}
				UpdateRecord:
				;
				rsCheckRec.Update(true);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
			fldListedCount.Text = intTotalRegular.ToString();
			fldVoidedCount.Text = intTotalVoid.ToString();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(bool modalDialog, string strPrinter = "")
		{
			frmReportViewer.InstancePtr.Init(this, strPrinter, 1, showModal: modalDialog);
		}

		private void rptCheckRegister_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCheckRegister.Caption	= "Check Register";
			//rptCheckRegister.Icon	= "rptCheckRegister.dsx":0000";
			//rptCheckRegister.Left	= 0;
			//rptCheckRegister.Top	= 0;
			//rptCheckRegister.Width	= 11880;
			//rptCheckRegister.Height	= 8595;
			//rptCheckRegister.StartUpPosition	= 3;
			//rptCheckRegister.SectionData	= "rptCheckRegister.dsx":058A;
			//End Unmaped Properties
		}
	}
}
