﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptEncumbranceLiquidation.
	/// </summary>
	public partial class rptEncumbranceLiquidation : BaseSectionReport
	{
		public static rptEncumbranceLiquidation InstancePtr
		{
			get
			{
				return (rptEncumbranceLiquidation)Sys.GetInstance(typeof(rptEncumbranceLiquidation));
			}
		}

		protected rptEncumbranceLiquidation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsGeneralInfo.Dispose();
				rsGeneralEncInfo.Dispose();
				rsDetailEncInfo.Dispose();
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEncumbranceLiquidation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralEncInfo = new clsDRWrapper();
		clsDRWrapper rsDetailEncInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		bool blnFirstRecord;

		public rptEncumbranceLiquidation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Encumbrance Liquidation Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsGeneralInfo.EndOfFile();
			}
			else
			{
				rsGeneralInfo.MoveNext();
				eArgs.EOF = rsGeneralInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			rsGeneralInfo.OpenRecordset(("SELECT * FROM APJournalDetail WHERE EncumbranceDetailRecord <> 0 AND APJournalID IN (SELECT ID FROM APJournal WHERE EncumbranceRecord <> 0 AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData))) + ")");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsDetailEncInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE ID = " + rsGeneralInfo.Get_Fields_Int32("EncumbranceDetailRecord"));
			rsGeneralEncInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + rsDetailEncInfo.Get_Fields_Int32("EncumbranceID"));
			if (FCConvert.ToInt32(rsGeneralEncInfo.Get_Fields_Int32("VendorNumber")) == 0)
			{
				fldVendor.Text = "0000 " + rsGeneralEncInfo.Get_Fields_String("TempVendorName");
			}
			else
			{
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsGeneralEncInfo.Get_Fields_Int32("VendorNumber"));
				fldVendor.Text = Strings.Format(rsGeneralEncInfo.Get_Fields_Int32("VendorNumber"), "00000") + " " + rsVendorInfo.Get_Fields_String("CheckName");
			}
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			fldReference.Text = FCConvert.ToString(rsGeneralEncInfo.Get_Fields("PO"));
			fldLiquidated.Text = Strings.Format(rsGeneralInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldRemaining.Text = Strings.Format(rsDetailEncInfo.Get_Fields("Amount") + rsDetailEncInfo.Get_Fields_Decimal("Adjustments") - rsDetailEncInfo.Get_Fields_Decimal("Liquidated"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetailEncInfo.Get_Fields_String("Account");
		}

		
	}
}
