﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web.Ext.CustomProperties;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmValidAccounts.
	/// </summary>
	public partial class frmValidAccounts : BaseForm
	{
		public frmValidAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmValidAccounts InstancePtr
		{
			get
			{
				return (frmValidAccounts)Sys.GetInstance(typeof(frmValidAccounts));
			}
		}

		protected frmValidAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int AccountCol;
		int lngColDesc1;
		// column variables
		int lngColDesc2;
		int lngColDesc3;
		int lngColDesc4;
		int lngColDesc5;
		clsDRWrapper rs = new clsDRWrapper();
		int CurrIndex;
		bool dontChange = false;
		// index of currently used account textbox
		string ErrorString = "";
		// Error to show user if account is not valid
		bool EditFlag;
		// makes sure the rowcol Event isn't fired when we don't need it
		bool blnUnload;
		clsDRWrapper rsDeptDiv = new clsDRWrapper();
		clsDRWrapper rsExpObj = new clsDRWrapper();
		clsDRWrapper rsRev = new clsDRWrapper();
		clsDRWrapper rsLedger = new clsDRWrapper();

		private void frmValidAccounts_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				frmWait.InstancePtr.Unload();
				return;
			}
			NumberCol = 0;
			AccountCol = 1;
			lngColDesc1 = 2;
			// initialize column variables
			lngColDesc2 = 3;
			lngColDesc3 = 4;
			lngColDesc4 = 5;
			lngColDesc5 = 6;
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.26));
			vs1.ColWidth(lngColDesc1, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc2, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc3, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc4, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc5, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, lngColDesc1, "Dept/Fund");
			vs1.TextMatrix(0, lngColDesc2, "Div/Acct/Prog");
			// set valid accounts flexgrid column headings
			vs1.TextMatrix(0, lngColDesc3, "Rev/Exp/Fun");
			vs1.TextMatrix(0, lngColDesc4, "Obj");
			vs1.TextMatrix(0, lngColDesc5, "Cost Center");
			//FC:FINAL:DDU:#2965 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, AccountCol, 0, lngColDesc4, 4);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				// show that all these are new accounts
				vs1.TextMatrix(counter, NumberCol, "0");
			}
			EditFlag = true;
			GetData();
			// get the already existing valid accounts and display them
			EditFlag = false;
			vs1.Visible = true;
			// make the flexgrid visible
			// SendKeys "{UP}"
			// SendKeys "{DOWN}"
			frmWait.InstancePtr.Unload();
			this.Refresh();
		}

		private void frmValidAccounts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F12 && KeyCode != Keys.F11 && KeyCode != Keys.F4)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmValidAccounts_Load(object sender, System.EventArgs e)
		{
			rsDeptDiv.OpenRecordset("SELECT * FROM DeptDivTitles");
			rsExpObj.OpenRecordset("SELECT * FROM ExpObjTitles");
			rsLedger.OpenRecordset("SELECT * FROM LedgerTitles");
			rsRev.OpenRecordset("SELECT * FROM RevTitles");
			
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM
			this.vs1.EditingControlShowing += vs1_EditingControlShowing;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int counter;
			bool blnAddedInfo;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			//Application.DoEvents();
			blnAddedInfo = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0 && vs1.TextMatrix(counter, AccountCol) != "" && Strings.InStr(1, vs1.TextMatrix(counter, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					blnAddedInfo = true;
				}
			}
			if (blnAddedInfo)
			{
				ans = MessageBox.Show("If you quit without saving any information added will be lost.  Do you wish to exit?", "Exit without Saving?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					// do nothing
				}
				else
				{
					e.Cancel = true;
					return;
				}
			}
			//MDIParent.InstancePtr.Show();
		}

		private void frmValidAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				// otherwise make it nothing and send a tab instead
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmValidAccounts_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.26));
			vs1.ColWidth(lngColDesc1, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc2, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc3, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc4, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(lngColDesc5, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
		}

		private void btnFileDelete_Click(object sender, System.EventArgs e)
		{
			vs1_KeyDownEvent(vs1, new KeyEventArgs(Keys.Delete));
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			vs1_KeyDownEvent(vs1, new KeyEventArgs(Keys.Insert));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void btnProcessAddLedger_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsValid = new clsDRWrapper();
			rsValid.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' AND Valid <> 1");
			if (rsValid.EndOfFile() != true && rsValid.BeginningOfFile() != true)
			{
				do
				{
					rsValid.Edit();
					rsValid.Set_Fields("DateCreated", DateTime.Today);
					rsValid.Set_Fields("Valid", true);
					rsValid.Update();
					rsValid.MoveNext();
				}
				while (rsValid.EndOfFile() != true);
			}
			EditFlag = true;
			GetData();
			// get the already existing valid accounts and display them
			EditFlag = false;
			//FC:FINAL:DDU:#2965 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, AccountCol, 0, lngColDesc4, 4);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.Visible = true;
			// make the flexgrid visible
			Support.SendKeys("{UP}", false);
			Support.SendKeys("{DOWN}", false);
			this.Refresh();
		}

		private void btnProcessAddRevenue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsValid = new clsDRWrapper();
			rsValid.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND Valid <> 1");
			if (rsValid.EndOfFile() != true && rsValid.BeginningOfFile() != true)
			{
				do
				{
					rsValid.Edit();
					rsValid.Set_Fields("DateCreated", DateTime.Today);
					rsValid.Set_Fields("Valid", true);
					rsValid.Update();
					rsValid.MoveNext();
				}
				while (rsValid.EndOfFile() != true);
			}
			EditFlag = true;
			GetData();
			// get the already existing valid accounts and display them
			EditFlag = false;
			//FC:FINAL:DDU:#2965 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, AccountCol, 0, lngColDesc4, 4);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.Visible = true;
			// make the flexgrid visible
			Support.SendKeys("{UP}", false);
			Support.SendKeys("{DOWN}", false);
			this.Refresh();
		}

		private void btnProcessAddUsed_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsValid = new clsDRWrapper();
			rsValid.OpenRecordset("SELECT * FROM AccountMaster WHERE Valid <> 1");
			if (rsValid.EndOfFile() != true && rsValid.BeginningOfFile() != true)
			{
				do
				{
					rsValid.Edit();
					rsValid.Set_Fields("DateCreated", DateTime.Today);
					rsValid.Set_Fields("Valid", true);
					rsValid.Update();
					rsValid.MoveNext();
				}
				while (rsValid.EndOfFile() != true);
			}
			EditFlag = true;
			GetData();
			// get the already existing valid accounts and display them
			EditFlag = false;
			//FC:FINAL:DDU:#2965 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, AccountCol, 0, lngColDesc4, 4);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(lngColDesc5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.Visible = true;
			// make the flexgrid visible
			Support.SendKeys("{UP}", false);
			Support.SendKeys("{DOWN}", false);
			this.Refresh();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.Col == AccountCol)
			{
				vs1.EditMaxLength = 23;
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, false);
			}
			else
			{
				EditFlag = false;
			}
		}
		// vbPorter upgrade warning: KeyCode As short	OnWrite(Keys, short)
		//private void vs1_KeyDownEvent_6(ref short KeyCode, short Shift)
		//{
		//	vs1_KeyDownEvent(KeyCode, Shift);
		//}
		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			clsDRWrapper rsDel = new clsDRWrapper();
			if (KeyCode == Keys.Insert)
			{
				// if the insert key is pressed then insert a row
				KeyCode = 0;
				if (vs1.Row < vs1.Rows - 1)
				{
					vs1.AddItem("", vs1.Row + 1);
					vs1.TextMatrix(vs1.Row + 1, NumberCol, "0");
				}
				else
				{
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, "0");
				}
				vs1.Row += 1;
			}
			else if (KeyCode == Keys.Delete)
			{
				// if the delete key is pressed then delete the row
				KeyCode = 0;
				if (vs1.Rows > 1)
				{
					if (vs1.TextMatrix(vs1.Row, AccountCol) != "")
					{
						temp = MessageBox.Show("Do you really want to delete this Account?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
						if (temp == DialogResult.Yes)
						{
							if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) == "E")
							{
								rsDel.Execute("DELETE FROM AccountMaster WHERE Account = '" + vs1.TextMatrix(vs1.Row, AccountCol) + "' AND CurrentBudget = 0", "Budgetary");
							}
							vs1.RemoveItem(vs1.Row);
							//FC:FINAL:MSH - Issue #644: Row added for correct work after removing item from list
							vs1.RowSel = vs1.Row;
						}
					}
					else
					{
						vs1.RemoveItem(vs1.Row);
						//FC:FINAL:MSH - Issue #644: Row added for correct work after removing item from list
						vs1.RowSel = vs1.Row;
					}
				}
			}
			else if (KeyCode == Keys.Tab || KeyCode == Keys.Return)
			{
				KeyCode = 0;
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			//Keys KeyCode = e.KeyCode;
			//if (vs1.Col == AccountCol)
			//{
			//	string temp = vs1.EditText;
			//	int selStart = vs1.EditSelStart;
			//	modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp, vs1.EditSelLength);
			//	vs1.EditText = temp;
			//	vs1.EditSelStart = selStart;
			//}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			short keyAscii = FCConvert.ToInt16(Strings.Asc(e.KeyChar));
			if (keyAscii == 13)
			{
				keyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			else if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
				//FC:FINAL:AM:#i735 - in VB6 KeyPressEdit is not fired for the TAB key
				else if (keyAscii == 9)//TAB
			{
			}
			else
			{
				if (vs1.Col == AccountCol)
				{
					//modNewAccountBox.CheckAccountKeyPress(vs1, vs1.Row, vs1.Col, keyAscii, vs1.EditSelStart, vs1.EditText);
				}
			}
		}

		private void vs1_KeyUpEdit(object sender, KeyEventArgs e)
		{
            Keys KeyCode = e.KeyCode;
            if (vs1.Col == AccountCol)
            {
                string temp = vs1.EditText;
                int selStart = vs1.EditSelStart;
                modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, KeyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp, vs1.EditSelLength);
                vs1.EditText = temp;
                vs1.EditSelStart = selStart;
            }
   //         Keys KeyCode = e.KeyCode;
			//if (vs1.Col == AccountCol && KeyCode != Keys.F12)
			//{
			//	if (FCConvert.ToInt32(KeyCode) != 40 && FCConvert.ToInt32(KeyCode) != 38)
			//	{
			//		// up and down arrows
			//		modNewAccountBox.CheckAccountKeyCode(vs1, vs1.Row, vs1.Col, FCConvert.ToInt16(KeyCode), 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
			//	}
			//}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - Issue #646: don't need to do change if account value incorrect to prevent multi handling
			if (dontChange)
			{
				return;
			}
			if (!EditFlag)
			{
				// if we want the event fired
				if (vs1.Rows > 1)
				{
					// check to see if all the rows have been deleted
					if (vs1.Col == AccountCol)
					{
						// if we are in the first column
						modNewAccountBox.SetGridFormat(vs1, vs1.Row, vs1.Col, true);
						vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
					}
					else
					{
                        //FC:FINAl:SBE - #4495 - end edit before switching to new cell
                        if (vs1.IsCurrentCellInEditMode) { vs1.EndEdit(); }
						if (vs1.Row == vs1.Rows - 1)
						{
							vs1.AddItem("", vs1.Row + 1);
							//vs1.Col = AccountCol;
							//vs1.Row += 1;
                            vs1.Select(vs1.Row + 1, AccountCol);
                        }
						else
						{
       //                     vs1.Col = AccountCol;
							//vs1.Row += 1;
                            vs1.Select(vs1.Row + 1, AccountCol);
                        }
					}
				}
			}
		}

		private void SaveData()
		{
			int counter;
			string temp = "";
			string format = "";
			rs.OmitNullsOnInsert = true;
			rs.Execute("UPDATE AccountMaster SET Valid = 0", "Budgetary");
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) == "E" && Strings.InStr(1, vs1.TextMatrix(counter, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					temp = vs1.TextMatrix(counter, AccountCol);
					if (temp != "")
					{
						format = modAccountTitle.Statics.Exp;
						temp = Strings.Right(temp, temp.Length - 2);
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vs1.TextMatrix(counter, AccountCol) + "'");
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.Edit();
						}
						else
						{
							rs.AddNew();
						}
						rs.Set_Fields("AccountType", "E");
						rs.Set_Fields("Account", "E " + temp);
						rs.Set_Fields("FirstAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						if (modAccountTitle.Statics.ExpDivFlag)
						{
							format = Strings.Right(format, format.Length - 4);
							rs.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
							if (!modAccountTitle.Statics.ObjFlag)
							{
								temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
								format = Strings.Right(format, format.Length - 2);
								rs.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
							}
							rs.Set_Fields("FourthAccountField", "");
						}
						else
						{
							format = Strings.Right(format, format.Length - 2);
							rs.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
							format = Strings.Right(format, format.Length - 2);
							rs.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
							if (!modAccountTitle.Statics.ObjFlag)
							{
								temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
								format = Strings.Right(format, format.Length - 2);
								rs.Set_Fields("FourthAccountField", temp);
							}
						}
						rs.Set_Fields("FifthAccountField", "");
						rs.Set_Fields("DateCreated", DateTime.Today);
						rs.Set_Fields("Valid", 1);
						rs.Update();
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					}
				}
				else if (Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) == "R" || Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) == "G")
				{
					if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0)
					{
						rs.OpenRecordset(("SELECT * FROM AccountMaster WHERE Account = '" + vs1.TextMatrix(counter, AccountCol)) + "'");
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
					}
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
						rs.Edit();
						rs.Set_Fields("DateCreated", DateTime.Today);
						rs.Set_Fields("Valid", 1);
						rs.Update();
					}					
				}
			}
		}

		private void GetData()
		{
			// gets the accounts that are already valid and shows them on the flexgrid
			int counter;
			string temp = "";
			vs1.Row = 1;
			rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs1.Rows = rs.RecordCount() + 2;
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					if (FCConvert.ToString(rs.Get_Fields_String("AccountType")) == "E")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						GetExp(rs.Get_Fields("Account"));
					}
					else if (rs.Get_Fields_String("AccountType") == "R")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						GetRev(rs.Get_Fields("Account"));
					}
					else if (rs.Get_Fields_String("AccountType") == "G")
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						GetLedger(rs.Get_Fields("Account"));
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rs.Get_Fields("Account")));
					vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					vs1.Row += 1;
					rs.MoveNext();
				}
				//FC:FINAL:RPU:#i756- Select the first row to avoid scrolling 
				//FC:FINAL:MSH - Issue #644: Saving number of last row in list into vs1.RowSel, because after using Select method vs1.Row will be equals 1.
				// Value in RowSel will be equals value in Row if row wil be selected by user
				vs1.RowSel = vs1.Row;
				vs1.Select(1, 1);
			}
		}

		private void GetExp(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Dept;
			string Div = "";
			string tempDiv;
			string tempObj;
			string Expense = "";
			string Object = "";
			string tempAccount;
			tempDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			tempObj = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 1));
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1));
				Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
				if (!modAccountTitle.Statics.ObjFlag)
				{
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
					Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
				}
			}
			else
			{
				Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
				if (!modAccountTitle.Statics.ObjFlag)
				{
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
					Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
				}
			}
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs1.TextMatrix(vs1.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + Div, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vs1.TextMatrix(vs1.Row, 3, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + Object, ",");
								if (rsExpObj.NoMatch != true)
								{
									vs1.TextMatrix(vs1.Row, 5, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
									return;
								}
								else
								{
									vs1.TextMatrix(vs1.Row, 5, "Undefined");
									vs1.TextMatrix(vs1.Row, 4, "Undefined");
									vs1.TextMatrix(vs1.Row, 3, "Undefined");
									vs1.TextMatrix(vs1.Row, 2, "Undefined");
									return;
								}
							}
							else
							{
								vs1.TextMatrix(vs1.Row, 5, "Undefined");
								vs1.TextMatrix(vs1.Row, 4, "Undefined");
								vs1.TextMatrix(vs1.Row, 3, "Undefined");
								vs1.TextMatrix(vs1.Row, 2, "Undefined");
								return;
							}
						}
						else
						{
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vs1.TextMatrix(vs1.Row, 5, "Undefined");
								vs1.TextMatrix(vs1.Row, 4, "Undefined");
								vs1.TextMatrix(vs1.Row, 3, "Undefined");
								vs1.TextMatrix(vs1.Row, 2, "Undefined");
								return;
							}
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Row, 5, "Undefined");
						vs1.TextMatrix(vs1.Row, 4, "Undefined");
						vs1.TextMatrix(vs1.Row, 3, "Undefined");
						vs1.TextMatrix(vs1.Row, 2, "Undefined");
						return;
					}
				}
				else
				{
					vs1.TextMatrix(vs1.Row, 5, "Undefined");
					vs1.TextMatrix(vs1.Row, 4, "Undefined");
					vs1.TextMatrix(vs1.Row, 3, "Undefined");
					vs1.TextMatrix(vs1.Row, 2, "Undefined");
					return;
				}
			}
			else
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs1.TextMatrix(vs1.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					if (!modAccountTitle.Statics.ObjFlag)
					{
						rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
						if (rsExpObj.NoMatch != true)
						{
							vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + Object, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs1.TextMatrix(vs1.Row, 5, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vs1.TextMatrix(vs1.Row, 5, "Undefined");
								vs1.TextMatrix(vs1.Row, 4, "Undefined");
								vs1.TextMatrix(vs1.Row, 3, "Undefined");
								vs1.TextMatrix(vs1.Row, 2, "Undefined");
								return;
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Row, 5, "Undefined");
							vs1.TextMatrix(vs1.Row, 4, "Undefined");
							vs1.TextMatrix(vs1.Row, 3, "Undefined");
							vs1.TextMatrix(vs1.Row, 2, "Undefined");
							return;
						}
					}
					else
					{
						rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
						if (rsExpObj.NoMatch != true)
						{
							vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vs1.TextMatrix(vs1.Row, 5, "Undefined");
							vs1.TextMatrix(vs1.Row, 4, "Undefined");
							vs1.TextMatrix(vs1.Row, 3, "Undefined");
							vs1.TextMatrix(vs1.Row, 2, "Undefined");
							return;
						}
					}
				}
				else
				{
					vs1.TextMatrix(vs1.Row, 5, "Undefined");
					vs1.TextMatrix(vs1.Row, 4, "Undefined");
					vs1.TextMatrix(vs1.Row, 3, "Undefined");
					vs1.TextMatrix(vs1.Row, 2, "Undefined");
					return;
				}
			}
		}

		private void GetRev(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Dept;
			string Div = "";
			string tempExpDiv;
			string Revenue = "";
			string tempAccount;
			tempExpDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)) + 1));
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 1));
				Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
			}
			else
			{
				Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
			}
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempExpDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs1.TextMatrix(vs1.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + Div, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vs1.TextMatrix(vs1.Row, 3, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						rsRev.FindFirstRecord2("Department, Division, Revenue", Dept + "," + Div + "," + Revenue, ",");
						if (rsRev.NoMatch != true)
						{
							vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vs1.TextMatrix(vs1.Row, 5, "Undefined");
							vs1.TextMatrix(vs1.Row, 4, "Undefined");
							vs1.TextMatrix(vs1.Row, 3, "Undefined");
							vs1.TextMatrix(vs1.Row, 2, "Undefined");
							return;
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Row, 5, "Undefined");
						vs1.TextMatrix(vs1.Row, 4, "Undefined");
						vs1.TextMatrix(vs1.Row, 3, "Undefined");
						vs1.TextMatrix(vs1.Row, 2, "Undefined");
						return;
					}
				}
				else
				{
					vs1.TextMatrix(vs1.Row, 5, "Undefined");
					vs1.TextMatrix(vs1.Row, 4, "Undefined");
					vs1.TextMatrix(vs1.Row, 3, "Undefined");
					vs1.TextMatrix(vs1.Row, 2, "Undefined");
					return;
				}
			}
			else
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempExpDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs1.TextMatrix(vs1.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsRev.FindFirstRecord2("Department, Revenue", Dept + "," + Revenue, ",");
					if (rsRev.NoMatch != true)
					{
						vs1.TextMatrix(vs1.Row, 4, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs1.TextMatrix(vs1.Row, 5, "Undefined");
						vs1.TextMatrix(vs1.Row, 4, "Undefined");
						vs1.TextMatrix(vs1.Row, 3, "Undefined");
						vs1.TextMatrix(vs1.Row, 2, "Undefined");
						return;
					}
				}
				else
				{
					vs1.TextMatrix(vs1.Row, 5, "Undefined");
					vs1.TextMatrix(vs1.Row, 4, "Undefined");
					vs1.TextMatrix(vs1.Row, 3, "Undefined");
					vs1.TextMatrix(vs1.Row, 2, "Undefined");
					return;
				}
			}
		}

		private void GetLedger(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Fund;
			string Acct;
			string tempAcct;
			string Year = "";
			string tempAccount;
			tempAcct = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Fund = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1));
			Acct = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
			if (!modAccountTitle.Statics.YearFlag)
			{
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + 1));
				Year = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
			}
			rsLedger.FindFirstRecord2("Fund, Account", Fund + "," + tempAcct, ",");
			if (rsLedger.NoMatch != true)
			{
				vs1.TextMatrix(vs1.Row, 2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
				if (!modAccountTitle.Statics.YearFlag)
				{
					rsLedger.FindFirstRecord2("Fund, Account, Year", Fund + "," + Acct + "," + Year, ",");
					if (rsLedger.NoMatch != true)
					{
						vs1.TextMatrix(vs1.Row, 3, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs1.TextMatrix(vs1.Row, 5, "Undefined");
						vs1.TextMatrix(vs1.Row, 4, "Undefined");
						vs1.TextMatrix(vs1.Row, 3, "Undefined");
						vs1.TextMatrix(vs1.Row, 2, "Undefined");
						return;
					}
				}
				else
				{
					rsLedger.FindFirstRecord2("Fund, Account", Fund + "," + Acct, ",");
					if (rsLedger.NoMatch != true)
					{
						vs1.TextMatrix(vs1.Row, 3, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs1.TextMatrix(vs1.Row, 5, "Undefined");
						vs1.TextMatrix(vs1.Row, 4, "Undefined");
						vs1.TextMatrix(vs1.Row, 3, "Undefined");
						vs1.TextMatrix(vs1.Row, 2, "Undefined");
						return;
					}
				}
			}
			else
			{
				vs1.TextMatrix(vs1.Row, 5, "Undefined");
				vs1.TextMatrix(vs1.Row, 4, "Undefined");
				vs1.TextMatrix(vs1.Row, 3, "Undefined");
				vs1.TextMatrix(vs1.Row, 2, "Undefined");
				return;
			}
		}

		private void SaveInfo()
		{
			//! Load frmWait; // show the wait form
			FCUtils.StartTask(this, () =>
			{
				this.ShowWait();
				this.UpdateWait("Saving");
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Saving Information";
				//frmWait.InstancePtr.Show();
				//frmWait.InstancePtr.Refresh();
				SaveData();
				// save the information
				//frmWait.InstancePtr.Unload();
				if (blnUnload)
				{
					//Close();
				}
				MessageBox.Show("Save Successful","Saved!");
				this.EndWait();
			});
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int lngTempValidAcctCheck = 0;
			int counter;
			// FC:FINAL:MSH - Issue #646: saving correct indexes of Row and Col, for which validating executed and using them in method
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper rsCheckItOut = new clsDRWrapper();
			if (col == AccountCol)
			{
				lngTempValidAcctCheck = modValidateAccount.Statics.ValidAcctCheck;
				modValidateAccount.Statics.ValidAcctCheck = 3;
				e.Cancel = dontChange = modNewAccountBox.CheckAccountValidate(vs1, row, col, false);
				modValidateAccount.Statics.ValidAcctCheck = lngTempValidAcctCheck;
				if (e.Cancel == false)
				{
					for (counter = 1; counter <= vs1.Rows - 1; counter++)
					{
						if (counter != row)
						{
							if (!string.IsNullOrWhiteSpace(Strings.Trim(vs1.TextMatrix(counter, AccountCol))))
							{
								if (vs1.EditText == Strings.Trim(vs1.TextMatrix(counter, AccountCol)))
								{
									MessageBox.Show("This account has already been entered on this screen.", "Duplicate Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
									e.Cancel = dontChange = true;
									//FC:FINAL:MSH - Issue #646: send edit mode to current cell
									vs1.Select(row, col);
									vs1.EditCell();
									vs1.EditSelStart = 0;
									vs1.EditSelLength = 1;
									break;
								}
							}
						}
					}
				}
				if (e.Cancel == false && !string.IsNullOrWhiteSpace(Strings.Trim(vs1.EditText)))
				{
					if (Strings.Left(vs1.EditText, 1) == "E")
					{
						GetExp(vs1.EditText);
						if (Conversion.Val(vs1.TextMatrix(row, NumberCol)) == 0)
						{
							rsCheckItOut.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vs1.EditText + "'");
							if (rsCheckItOut.EndOfFile() != true && rsCheckItOut.BeginningOfFile() != true)
							{
								vs1.TextMatrix(row, NumberCol, FCConvert.ToString(rsCheckItOut.Get_Fields_Int32("ID")));
							}
							else
							{
								vs1.TextMatrix(row, NumberCol, FCConvert.ToString(0));
							}
						}
					}
					else if (Strings.Left(vs1.EditText, 1) == "R")
					{
						GetRev(vs1.EditText);
						if (Conversion.Val(vs1.TextMatrix(row, NumberCol)) == 0)
						{
							vs1.TextMatrix(row, NumberCol, FCConvert.ToString(0));
						}
					}
					else if (Strings.Left(vs1.EditText, 1) == "G")
					{
						GetLedger(vs1.EditText);
						if (Conversion.Val(vs1.TextMatrix(row, NumberCol)) == 0)
						{
							vs1.TextMatrix(row, NumberCol, FCConvert.ToString(0));
						}
					}
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
		//FC:FINAL:AM:#i735 - attach event handlers
		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				e.Control.KeyDown -= new KeyEventHandler(vs1_KeyDownEdit);
				//e.Control.KeyPress -= new KeyPressEventHandler(vs1_KeyPressEdit);
				e.Control.KeyUp -= new KeyEventHandler(vs1_KeyUpEdit);
				e.Control.Appear -= Control_Appear;
				e.Control.KeyDown += new KeyEventHandler(vs1_KeyDownEdit);
				//e.Control.KeyPress += new KeyPressEventHandler(vs1_KeyPressEdit);
				e.Control.KeyUp += new KeyEventHandler(vs1_KeyUpEdit);
				e.Control.Appear += Control_Appear;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void Control_Appear(object sender, EventArgs e)
		{
			var editor = sender as TextBox;
			if (editor != null)
			{
				editor.SelectionStart = 0;
				editor.SelectionLength = 1;
			}
		}
	}
}
