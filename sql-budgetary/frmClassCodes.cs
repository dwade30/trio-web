﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmClassCodes.
	/// </summary>
	public partial class frmClassCodes : BaseForm
	{
		public frmClassCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmClassCodes InstancePtr
		{
			get
			{
				return (frmClassCodes)Sys.GetInstance(typeof(frmClassCodes));
			}
		}

		protected frmClassCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool blnUnload;

		private void frmClassCodes_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1.ExtendLastCol = true;
			vs1.ColWidth(0, 250);
			// initialize flexgrid properties
			vs1.TextMatrix(0, 1, "Code");
			for (counter = 1; counter <= 9; counter++)
			{
				vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
				// number the first column
			}
			for (counter = 10; counter <= 20; counter++)
			{
				// then go from A to K
				vs1.TextMatrix(counter, 0, FCConvert.ToString(Convert.ToChar(counter + 55)));
			}
			rs.OpenRecordset("SELECT * FROM ClassCodes");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there are some class codes
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= 21; counter++)
				{
					// for each row
					if (rs.EndOfFile() != true)
					{
						// if there is a record
						vs1.TextMatrix(counter, 1, FCConvert.ToString(rs.Get_Fields_String("ClassCode")));
						// get information from database and put it on the form
						rs.MoveNext();
						// go to the next record
					}
					else
					{
						break;
						// if there are no more records then end the loop
					}
				}
			}
			vs1.EditMaxLength = 28;
		}

		private void frmClassCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmClassCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClassCodes.FillStyle	= 0;
			//frmClassCodes.ScaleWidth	= 9045;
			//frmClassCodes.ScaleHeight	= 7500;
			//frmClassCodes.LinkTopic	= "Form2";
			//frmClassCodes.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmClassCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			// give the cell focus
			vs1.EditCell();
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			// give the cell focus
			vs1.EditCell();
		}

		private void SaveInfo()
		{
			int counter;
			vs1.Row = 0;
			// move out of last cell being edited so it changes
			vs1.Col = 0;
			//Application.DoEvents();
			rs.OpenRecordset("SELECT * FROM ClassCodes");
			// get the records
			for (counter = 1; counter <= 20; counter++)
			{
				// go through each row in the table
				if (rs.EndOfFile() != true)
				{
					// if there is a record then edit it
					rs.Edit();
					rs.Set_Fields("ClassKey", vs1.TextMatrix(counter, 0));
					// save the data
					rs.Set_Fields("ClassCode", vs1.TextMatrix(counter, 1));
					rs.Update();
					// update and move to the next record
					rs.MoveNext();
				}
				else
				{
					rs.AddNew();
					// if no records left then add a new one
					rs.Set_Fields("ClassKey", vs1.TextMatrix(counter, 0));
					// get info
					rs.Set_Fields("ClassCode", vs1.TextMatrix(counter, 1));
					rs.Update();
					// update
				}
			}
			if (blnUnload)
			{
				//Close();
			}
			//FC:FINAL:DDU:#2912 - show messagebox when process is finish
			MessageBox.Show("Save Successful", "Saved");
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - Issue #802: replaced by method with Close handling
			//mnuFileSave_Click(sender, e);
			mnuProcessSave_Click(sender, e);
		}
	}
}
