﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	public class cReportExportView
	{
		//=========================================================
		private cReportDataExporter theExporter;
		private string strFilePath = string.Empty;
		private cDetailsReport theReport;
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;

		public delegate void ValuesChangedEventHandler();

		public event ValuesChangedEventHandler ValuesChanged;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theExp As cReportDataExporter	OnWrite(cExpenseDetailController, cLedgerDetailController, cRevenueDetailController, cExpRevSummaryController, cTrialBalanceController)
		public void SetExporter(cReportDataExporter theExp)
		{
			theExporter = theExp;
			intFormatType = theExporter.FormatType;
			if (this.ValuesChanged != null)
				this.ValuesChanged();
		}
		// vbPorter upgrade warning: theRep As cDetailsReport	OnWrite(cExpenseDetailReport, cLedgerDetailReport, cRevenueDetailReport, cExpRevSummaryReport, cTrialBalanceReport)
		public void SetReport(cDetailsReport theRep)
		{
			theReport = theRep;
		}

		public string Filename
		{
			set
			{
				strFilePath = value;
			}
			get
			{
				string Filename = "";
				Filename = strFilePath;
				return Filename;
			}
		}

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public void ExportData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				if (!(theExporter == null))
				{
					if (!(theReport == null))
					{
						theExporter.FormatType = FCConvert.ToInt16(intFormatType);
						theExporter.ExportData(ref theReport, strFilePath);
						if (theExporter.HadError)
						{
							SetError(theExporter.LastErrorNumber, theExporter.LastErrorMessage);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void Browse()
		{
			//FC:FINAL:BBE:#i701 - save file to temp folder, and download to client
			//cSaveFileDialog sfd = new cSaveFileDialog();
			//sfd.MultiSelect = false;
			//sfd.Title = "Save As";
			//sfd.InitialDirectory = Application.StartupPath;
			//sfd.FileName = Filename;
			////sfd.Options = vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNOverwritePrompt;
			Filename = (!string.IsNullOrEmpty(Filename)) ? Filename : Path.GetRandomFileName();
			string tempFileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp", Filename);
			if (!Directory.Exists(Path.GetDirectoryName(tempFileName)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(tempFileName));
			}
			string strExt = "";
			string strFilter = "";
			if (FormatType == 1)
			{
				strExt = "txt";
				strFilter = "Tab Delimited | *.txt";
			}
			else
			{
				strExt = "csv";
				strFilter = "CSV | *.csv";
			}
			//FC:FINAL:BBE:#i701 - save file to temp folder, and download to client
			//sfd.DefaultExtension = strExt;
			//sfd.Filter = strFilter;
			//sfd.FilterIndex = 1;
			tempFileName = Path.ChangeExtension(tempFileName, strExt);
			//if (sfd.ShowDialog() != 0)
			{
				//FC:FINAL:BBE:#i701 - save file to temp folder, and download to client
				//Filename = sfd.FileName;
				Filename = tempFileName;
				if (this.ValuesChanged != null)
					this.ValuesChanged();
			}
		}
	}
}
