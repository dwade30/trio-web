﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmWarrantMessage.
	/// </summary>
	partial class frmWarrantMessage : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> rtfWarrant;
		public fecherFoundation.FCTextBox rtfWarrant_9;
		public fecherFoundation.FCTextBox rtfWarrant_8;
		public fecherFoundation.FCTextBox rtfWarrant_7;
		public fecherFoundation.FCTextBox rtfWarrant_6;
		public fecherFoundation.FCTextBox rtfWarrant_5;
		public fecherFoundation.FCTextBox rtfWarrant_4;
		public fecherFoundation.FCTextBox rtfWarrant_3;
		public fecherFoundation.FCTextBox rtfWarrant_2;
		public fecherFoundation.FCTextBox rtfWarrant_1;
		public fecherFoundation.FCTextBox rtfWarrant_0;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCFrame shpBorder;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent3 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent4 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent5 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent6 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent7 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent8 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent9 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent10 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent11 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent12 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent13 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent14 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent15 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent16 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent17 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent18 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent19 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent20 = new Wisej.Web.JavaScript.ClientEvent();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWarrantMessage));
			this.rtfWarrant_9 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_8 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_7 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_6 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_5 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_4 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_3 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_2 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_1 = new fecherFoundation.FCTextBox();
			this.rtfWarrant_0 = new fecherFoundation.FCTextBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.shpBorder = new fecherFoundation.FCFrame();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.customProperties1 = new Wisej.Web.Ext.CustomProperties.CustomProperties(this.components);
			this.javaScript1 = new Wisej.Web.JavaScript(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.shpBorder)).BeginInit();
			this.shpBorder.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.shpBorder);
			this.ClientArea.Size = new System.Drawing.Size(1088, 532);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(291, 30);
			this.HeaderText.Text = "Warrant Message Screen";
			// 
			// rtfWarrant_9
			// 
			this.rtfWarrant_9.Appearance = 0;
			this.rtfWarrant_9.AutoSize = false;
			this.rtfWarrant_9.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_9.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_9.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent2.Event = "keyup";
			clientEvent2.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_9).Add(clientEvent1);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_9).Add(clientEvent2);
			this.rtfWarrant_9.LinkItem = null;
			this.rtfWarrant_9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_9.LinkTopic = null;
			this.rtfWarrant_9.Location = new System.Drawing.Point(1, 234);
			this.rtfWarrant_9.MaxLength = 80;
			this.rtfWarrant_9.Name = "rtfWarrant_9";
			this.rtfWarrant_9.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_9.TabIndex = 10;
			this.rtfWarrant_9.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_9.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_9.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_9.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_9.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_8
			// 
			this.rtfWarrant_8.Appearance = 0;
			this.rtfWarrant_8.AutoSize = false;
			this.rtfWarrant_8.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_8.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_8.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent3.Event = "keypress";
			clientEvent3.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent4.Event = "keyup";
			clientEvent4.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_8).Add(clientEvent3);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_8).Add(clientEvent4);
			this.rtfWarrant_8.LinkItem = null;
			this.rtfWarrant_8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_8.LinkTopic = null;
			this.rtfWarrant_8.Location = new System.Drawing.Point(1, 208);
			this.rtfWarrant_8.MaxLength = 80;
			this.rtfWarrant_8.Name = "rtfWarrant_8";
			this.rtfWarrant_8.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_8.TabIndex = 9;
			this.rtfWarrant_8.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_8.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_8.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_8.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_8.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_7
			// 
			this.rtfWarrant_7.Appearance = 0;
			this.rtfWarrant_7.AutoSize = false;
			this.rtfWarrant_7.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_7.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_7.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent5.Event = "keypress";
			clientEvent5.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent6.Event = "keyup";
			clientEvent6.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_7).Add(clientEvent5);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_7).Add(clientEvent6);
			this.rtfWarrant_7.LinkItem = null;
			this.rtfWarrant_7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_7.LinkTopic = null;
			this.rtfWarrant_7.Location = new System.Drawing.Point(1, 182);
			this.rtfWarrant_7.MaxLength = 80;
			this.rtfWarrant_7.Name = "rtfWarrant_7";
			this.rtfWarrant_7.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_7.TabIndex = 8;
			this.rtfWarrant_7.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_7.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_7.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_7.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_7.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_6
			// 
			this.rtfWarrant_6.Appearance = 0;
			this.rtfWarrant_6.AutoSize = false;
			this.rtfWarrant_6.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_6.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_6.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent7.Event = "keypress";
			clientEvent7.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent8.Event = "keyup";
			clientEvent8.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_6).Add(clientEvent7);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_6).Add(clientEvent8);
			this.rtfWarrant_6.LinkItem = null;
			this.rtfWarrant_6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_6.LinkTopic = null;
			this.rtfWarrant_6.Location = new System.Drawing.Point(1, 156);
			this.rtfWarrant_6.MaxLength = 80;
			this.rtfWarrant_6.Name = "rtfWarrant_6";
			this.rtfWarrant_6.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_6.TabIndex = 7;
			this.rtfWarrant_6.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_6.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_6.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_6.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_6.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_5
			// 
			this.rtfWarrant_5.Appearance = 0;
			this.rtfWarrant_5.AutoSize = false;
			this.rtfWarrant_5.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_5.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_5.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent9.Event = "keypress";
			clientEvent9.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent10.Event = "keyup";
			clientEvent10.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_5).Add(clientEvent9);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_5).Add(clientEvent10);
			this.rtfWarrant_5.LinkItem = null;
			this.rtfWarrant_5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_5.LinkTopic = null;
			this.rtfWarrant_5.Location = new System.Drawing.Point(1, 130);
			this.rtfWarrant_5.MaxLength = 80;
			this.rtfWarrant_5.Name = "rtfWarrant_5";
			this.rtfWarrant_5.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_5.TabIndex = 6;
			this.rtfWarrant_5.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_5.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_5.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_5.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_5.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_4
			// 
			this.rtfWarrant_4.Appearance = 0;
			this.rtfWarrant_4.AutoSize = false;
			this.rtfWarrant_4.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_4.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_4.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent11.Event = "keypress";
			clientEvent11.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent12.Event = "keyup";
			clientEvent12.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_4).Add(clientEvent11);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_4).Add(clientEvent12);
			this.rtfWarrant_4.LinkItem = null;
			this.rtfWarrant_4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_4.LinkTopic = null;
			this.rtfWarrant_4.Location = new System.Drawing.Point(1, 104);
			this.rtfWarrant_4.MaxLength = 80;
			this.rtfWarrant_4.Name = "rtfWarrant_4";
			this.rtfWarrant_4.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_4.TabIndex = 5;
			this.rtfWarrant_4.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_4.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_4.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_4.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_3
			// 
			this.rtfWarrant_3.Appearance = 0;
			this.rtfWarrant_3.AutoSize = false;
			this.rtfWarrant_3.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_3.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_3.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent13.Event = "keypress";
			clientEvent13.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent14.Event = "keyup";
			clientEvent14.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_3).Add(clientEvent13);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_3).Add(clientEvent14);
			this.rtfWarrant_3.LinkItem = null;
			this.rtfWarrant_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_3.LinkTopic = null;
			this.rtfWarrant_3.Location = new System.Drawing.Point(1, 78);
			this.rtfWarrant_3.MaxLength = 80;
			this.rtfWarrant_3.Name = "rtfWarrant_3";
			this.rtfWarrant_3.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_3.TabIndex = 4;
			this.rtfWarrant_3.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_3.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_3.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_3.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_2
			// 
			this.rtfWarrant_2.Appearance = 0;
			this.rtfWarrant_2.AutoSize = false;
			this.rtfWarrant_2.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_2.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_2.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent15.Event = "keypress";
			clientEvent15.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent16.Event = "keyup";
			clientEvent16.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_2).Add(clientEvent15);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_2).Add(clientEvent16);
			this.rtfWarrant_2.LinkItem = null;
			this.rtfWarrant_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_2.LinkTopic = null;
			this.rtfWarrant_2.Location = new System.Drawing.Point(1, 52);
			this.rtfWarrant_2.MaxLength = 80;
			this.rtfWarrant_2.Name = "rtfWarrant_2";
			this.rtfWarrant_2.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_2.TabIndex = 3;
			this.rtfWarrant_2.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_2.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_2.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_2.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_1
			// 
			this.rtfWarrant_1.Appearance = 0;
			this.rtfWarrant_1.AutoSize = false;
			this.rtfWarrant_1.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent17.Event = "keypress";
			clientEvent17.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent18.Event = "keyup";
			clientEvent18.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_1).Add(clientEvent17);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_1).Add(clientEvent18);
			this.rtfWarrant_1.LinkItem = null;
			this.rtfWarrant_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_1.LinkTopic = null;
			this.rtfWarrant_1.Location = new System.Drawing.Point(1, 26);
			this.rtfWarrant_1.MaxLength = 80;
			this.rtfWarrant_1.Name = "rtfWarrant_1";
			this.rtfWarrant_1.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_1.TabIndex = 2;
			this.rtfWarrant_1.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_1.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_1.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_1.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// rtfWarrant_0
			// 
			this.rtfWarrant_0.Appearance = 0;
			this.rtfWarrant_0.AutoSize = false;
			this.rtfWarrant_0.BackColor = System.Drawing.SystemColors.Window;
			this.rtfWarrant_0.BorderStyle = Wisej.Web.BorderStyle.None;
			this.rtfWarrant_0.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			clientEvent19.Event = "keypress";
			clientEvent19.JavaScript = "WarrantInput_KeyPress(this, e)";
			clientEvent20.Event = "keyup";
			clientEvent20.JavaScript = "WarrantInput_KeyUp(this, e)";
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_0).Add(clientEvent19);
			this.javaScript1.GetJavaScriptEvents(this.rtfWarrant_0).Add(clientEvent20);
			this.rtfWarrant_0.LinkItem = null;
			this.rtfWarrant_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.rtfWarrant_0.LinkTopic = null;
			this.rtfWarrant_0.Location = new System.Drawing.Point(1, 1);
			this.rtfWarrant_0.MaxLength = 80;
			this.rtfWarrant_0.Name = "rtfWarrant_0";
			this.rtfWarrant_0.Size = new System.Drawing.Size(634, 25);
			this.rtfWarrant_0.TabIndex = 0;
			this.rtfWarrant_0.KeyDown += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyDown);
			this.rtfWarrant_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.rtfWarrant_KeyPress);
			this.rtfWarrant_0.KeyUp += new Wisej.Web.KeyEventHandler(this.rtfWarrant_KeyUp);
			this.rtfWarrant_0.Enter += new System.EventHandler(this.rtfWarrant_Enter);
			this.rtfWarrant_0.TextChanged += new System.EventHandler(this.rtfWarrant_TextChanged);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(438, 20);
			this.lblInstructions.TabIndex = 1;
			this.lblInstructions.Text = "ENTER THE WARRANT MESSAGE IN THE BOX BELOW";
			// 
			// shpBorder
			// 
			this.shpBorder.Controls.Add(this.rtfWarrant_9);
			this.shpBorder.Controls.Add(this.rtfWarrant_8);
			this.shpBorder.Controls.Add(this.rtfWarrant_7);
			this.shpBorder.Controls.Add(this.rtfWarrant_6);
			this.shpBorder.Controls.Add(this.rtfWarrant_5);
			this.shpBorder.Controls.Add(this.rtfWarrant_4);
			this.shpBorder.Controls.Add(this.rtfWarrant_3);
			this.shpBorder.Controls.Add(this.rtfWarrant_2);
			this.shpBorder.Controls.Add(this.rtfWarrant_1);
			this.shpBorder.Controls.Add(this.rtfWarrant_0);
			this.shpBorder.Location = new System.Drawing.Point(30, 80);
			this.shpBorder.Name = "shpBorder";
			this.shpBorder.Size = new System.Drawing.Size(640, 279);
			this.shpBorder.TabIndex = 11;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 1;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 384);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmWarrantMessage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1088, 700);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmWarrantMessage";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Warrant Message Screen";
			this.Load += new System.EventHandler(this.frmWarrantMessage_Load);
			this.Activated += new System.EventHandler(this.frmWarrantMessage_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmWarrantMessage_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.shpBorder)).EndInit();
			this.shpBorder.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSave;
		private JavaScript javaScript1;
		private Wisej.Web.Ext.CustomProperties.CustomProperties customProperties1;
		private System.ComponentModel.IContainer components;
	}
}
