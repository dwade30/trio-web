﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using System.IO;
using SharedApplication.Budgetary.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
    public class cMDIView
    {
        //=========================================================
        public bool customMenu = false;
        private cBDCommandHandler bdcHandler;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cGenericCollection gcollCurrentMenu = new cGenericCollection();
        private cGenericCollection gcollCurrentMenu_AutoInitialized;

        private cGenericCollection gcollCurrentMenu
        {
            get
            {
                if (gcollCurrentMenu_AutoInitialized == null) gcollCurrentMenu_AutoInitialized = new cGenericCollection();

                return gcollCurrentMenu_AutoInitialized;
            }
            set
            {
                gcollCurrentMenu_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cMenuController menuController = new cMenuController();
        private cMenuController menuController_AutoInitialized;

        private cMenuController menuController
        {
            get
            {
                if (menuController_AutoInitialized == null) menuController_AutoInitialized = new cMenuController();

                return menuController_AutoInitialized;
            }
            set
            {
                menuController_AutoInitialized = value;
            }
        }

        private string strCurrentMenuName = "";
        private string strCurrentFormCaption = "";
        private string strPanelMessage = string.Empty;
        private string strBaseCaption = string.Empty;
        private string strApplicationName = "";
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBankService bankService = new cBankService();
        private cBankService bankService_AutoInitialized;

        private cBankService bankService
        {
            get
            {
                if (bankService_AutoInitialized == null) bankService_AutoInitialized = new cBankService();

                return bankService_AutoInitialized;
            }
            set
            {
                bankService_AutoInitialized = value;
            }
        }

        public delegate void MenuChangedEventHandler();

        public event MenuChangedEventHandler MenuChanged;

        public delegate void PanelChangedEventHandler();

        public event PanelChangedEventHandler PanelChanged;

        public void Reset()
        {
            SetMenu("Main");
        }

        public string PanelMessage
        {
            set
            {
                strPanelMessage = value;
                if (PanelChanged != null)
                    PanelChanged();
            }
            get
            {
                var PanelMessage = "";
                PanelMessage = strPanelMessage;
                return PanelMessage;
            }
        }

        public string CaptionPrefix
        {
            set
            {
                strBaseCaption = value;
            }
        }

        public string CurrentFormCaption
        {
            get
            {
                var CurrentFormCaption = "";
                CurrentFormCaption = strCurrentFormCaption;
                return CurrentFormCaption;
            }
        }

        public string CurrentMenuName
        {
            get
            {
                var CurrentMenuName = "";
                CurrentMenuName = strCurrentMenuName;
                return CurrentMenuName;
            }
        }

        public cGenericCollection CurrentMenu
        {
            get
            {
                cGenericCollection CurrentMenu = null;
                CurrentMenu = gcollCurrentMenu;
                return CurrentMenu;
            }
        }

        public void ExecuteMenuCommand(string strMenu, int lngCode)
        {
            bdcHandler.ExecuteMenuCommand(strMenu, lngCode);
        }

        public void ExecuteCommand(string strCommand)
        {
            bdcHandler.ExecuteCommand(strCommand);
        }

        public void SetMenu(string strMenuName)
        {
            var menuName = strMenuName.ToLower();

            if (menuName == "custom") gcollCurrentMenu = GetCustomPrograms();

            var currentMenuName = Strings.LCase(strCurrentMenuName);
            if (menuName != currentMenuName && currentMenuName == "checkrec") PanelMessage = "";

            switch (menuName)
            {
                case "main":
                    gcollCurrentMenu = GetMainMenu();

                    break;
                case "payables":
	                gcollCurrentMenu = GetPayablesMenu();

	                break;
                case "projects":
	                gcollCurrentMenu = GetProjectsMenu();

	                break;
                case "accounts":
	                gcollCurrentMenu = GetAccountsMenu();

	                break;
                case "eoy":
	                gcollCurrentMenu = GetEndOfYearMenu();

	                break;
                case "vendor":
                    gcollCurrentMenu = GetVendorMenu();

                    break;
                case "file":
                    gcollCurrentMenu = GetFileMenu();

                    break;
                case "reports":
                    gcollCurrentMenu = GetReportsMenu();

                    break;
                case "eoyreports":
                    gcollCurrentMenu = GetEOYReportsMenu();

                    break;
                case "encumbrancereports":
                    gcollCurrentMenu = GetEncumbranceReportsMenu();

                    break;
                case "account":
                    gcollCurrentMenu = GetAccountMenu();

                    break;
                case "formats":
                    gcollCurrentMenu = GetFormatsMenu();

                    break;
                case "budget":
                    gcollCurrentMenu = GetBudgetMenu();

                    break;
                case "initialrequest":
                    gcollCurrentMenu = GetInitialRequestMenu();

                    break;
                case "managerrequest":
                    gcollCurrentMenu = GetManagerRequestMenu();

                    break;
                case "committeerequest":
                    gcollCurrentMenu = GetCommitteeRequestMenu();

                    break;
                case "electedrequest":
                    gcollCurrentMenu = GetElectedRequestMenu();

                    break;
                case "approvedamount":
                    gcollCurrentMenu = GetApprovedAmountMenu();

                    break;
                case "ap":
                    gcollCurrentMenu = GetAPMenu();

                    break;

                case "checkfile":
                    gcollCurrentMenu = GetCheckFileMenu();

                    break;
                case "1099":
                    gcollCurrentMenu = Get1099Menu();

                    break;
                case "summary":
                    gcollCurrentMenu = GetSummaryMenu();

                    break;
                case "detail":
                    gcollCurrentMenu = GetDetailMenu();

                    break;
                case "purge":
                    gcollCurrentMenu = GetPurgeChartsMenu();

                    break;
                case "checkrec":
                    {

                        var banksColl = GetBanks();
                        var hasBanks = HasBanks(banksColl);

                        if (!hasBanks)
                        {
                            MessageBox.Show("You must set up at least one bank before you may continue with this process", "No Banks Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        var tBank = bankService.GetCurrentBank();
                        if (tBank == null && banksColl.ItemCount() == 1)
                        {
                            banksColl.MoveFirst();
                            tBank = (cBank)banksColl.GetCurrentItem();
                            if (tBank != null) bankService.SetCurrentBank(tBank.ID);
                        }

                        gcollCurrentMenu = GetCheckRecMenu();
                        MenuChanged?.Invoke();
                        //Application.DoEvents();

                        if (tBank != null)
                        {
                            PanelMessage = $"Bank:  {FCConvert.ToString(tBank.ID)} {Strings.Trim(tBank.Name)}";
                            modBudgetaryMaster.Statics.intCurrentBank = tBank.ID;
                            //Application.DoEvents();
                        }
                        else
                        {
                            PanelMessage = "Bank:  NOT ASSIGNED";
                            //Application.DoEvents();
                            bdcHandler.ExecuteCommand("BD_Form_ChangeBank");
                        }

                        var rsSaveInfo = new clsDRWrapper();

                        try
                        {
                            var statementDate = modBudgetaryAccounting.GetBDVariable("StatementDate");

                            if (Information.IsDate(statementDate))
                            {
                                var datStatementDate = (DateTime)statementDate;
                                rsSaveInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '1' OR Status = '2'");
                                while (!rsSaveInfo.EndOfFile())
                                {
                                    var checkDate = rsSaveInfo.Get_Fields_DateTime("CheckDate");
                                    var isDirectDeposit = FCConvert.ToBoolean(rsSaveInfo.Get_Fields_Boolean("DirectDeposit"));
                                    var status = FCConvert.ToString(rsSaveInfo.Get_Fields_String("Status"));

                                    if (status == "1")
                                    {
                                        if (checkDate <= datStatementDate) UpdateSaveInfo(rsSaveInfo, isDirectDeposit ? "3" : "2");
                                    }
                                    else
                                    {
                                        if (checkDate > datStatementDate) UpdateSaveInfo(rsSaveInfo, "1");
                                    }

                                    rsSaveInfo.MoveNext();
                                }
                            }
                        }
                        finally
                        {
                            rsSaveInfo.DisposeOf();
                        }

                        break;
                    }
            }

            MenuChanged?.Invoke();
        }

        private bool HasBanks(cGenericCollection banksColl)
        {
            return banksColl != null && banksColl.ItemCount() > 0;
        }

        private cGenericCollection GetBanks()
        {
            return bankService.GetBanks();
        }

        private static void UpdateSaveInfo(clsDRWrapper rsSaveInfo, string statusCode)
        {
            rsSaveInfo.Edit();
            rsSaveInfo.Set_Fields("Status", statusCode);
            rsSaveInfo.Set_Fields("StatusDate", DateTime.Today);
            rsSaveInfo.Update(true);
        }

        private cGenericCollection GetCustomPrograms()
        {
            int CurrRow;
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Custom";
            GenerateCaption("[Custom Programs]");
            CurrRow = 1;

            gcollMenu.ClearList();
            var muniName = modGlobalConstants.Statics.MuniName.ToUpper();

            switch (muniName)
            {
                case "BOOTHBAY":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BoothbayCheckRecExport", 10, "Export Check Rec Information", 0, "", "", ref CurrRow));

                    break;
                //case "DOVERFOXCROFT":
                //case "DOVER FOXCROFT":
                //case "DOVER-FOXCROFT":
                //    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_DoverFoxcroftSimpleCheckRecExport", 13, "Export Check Rec Information", 0, "", "", ref CurrRow));

                //    break;
                case "ORRINGTON":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_OrringtonCustomCheckRecAP", 1, "Import AP Check Rec Information", 0, "", "", ref CurrRow));
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_OrringtonCustomCheckRecPY", 2, "Import Payroll Check Rec Information", 0, "", "", ref CurrRow));

                    break;
                case "TRIO":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SetupTownRevenueReport", 3, "Town Revenue Report", 0, "", "", ref CurrRow));

                    break;
                case "CLINTON":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ClintonCheckRecExport", 4, "Export Check Rec Information", 0, "", "", ref CurrRow));

                    break;
                //case "CARIBOU":
                //    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CaribouCheckRecExport", 5, "Export Simple Check Rec Information", 0, "", "", ref CurrRow));

                //    break;
                case "HANCOCK COUNTY":
                case "HANCOCK COUNTY JAIL":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_HancockCountyPayrollImport", 6, "Import Payroll Information", 0, "", "", ref CurrRow));

                    break;
                case "CALAIS":
                case "NORWAY":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_CalaisBDSetDBLocation", 7, "Set Water Database Location", 0, "", "", ref CurrRow));

                    break;
                case "LINCOLN COUNTY":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_LincolnCountyPayrollImport", 8, "Import Payroll Information", 0, "", "", ref CurrRow));

                    break;
                case "DAYTON":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_DaytonCustomImport", 9, "Import Journal Information", 0, "", "", ref CurrRow));

                    break;

                case "ORONO":
                case "ORONOPLAY":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_OronoPayrollImport", 11, "Import Payroll Information", 0, "", "", ref CurrRow));

                    break;
                case "SOMERSET COUNTY":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_SomersetCountyCustomCheckRecAP", 12, "Import AP Check Rec Information", 0, "", "", ref CurrRow));

                    break;
                case "MONMOUTH":
                    gcollMenu.AddItem(menuController.CreateMenuItem("BD_OronoPayrollImport", 11, "Import Payroll Information", 0, "", "", ref CurrRow));

                    break;
            }

            return gcollMenu;
        }

        private string GenerateCaption(string strCaption)
        {
            var GenerateCaption = "";
            var strTemp = Strings.Trim(Strings.Trim(strBaseCaption) != ""
                                              ? $"{strBaseCaption} - {strApplicationName}   {strCaption}"
                                              : $"{strApplicationName}   {Strings.Trim(strCaption)}");
            strCurrentFormCaption = strTemp;
            GenerateCaption = strTemp;
            return GenerateCaption;
        }

        private cGenericCollection GetMainMenu()
        {
            cGenericCollection GetMainMenu = null;
            int CurRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Main";
            GenerateCaption("[Main]");
            CurRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_Journals", 1, "Journals", (int)BudgetarySecurityItems.Journals, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_AccountInquiry", 2, "Account Inquiry", (int)BudgetarySecurityItems.Printing, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Payables", 3, "Payables", (int)BudgetarySecurityItems.Payables, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_CheckRec", 4, "Bank Reconciliation", (int)BudgetarySecurityItems.BankReconcilliation, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Accounts", 5, "Accounts", (int)BudgetarySecurityItems.Accounts, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 6, "Budget Process", (int)BudgetarySecurityItems.BudgetProcess, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_EOY", 7, "End of Year", (int)BudgetarySecurityItems.EndofYearMenu, "", "", ref CurRow));
            tItem = menuController.CreateMenuItem("BD_Menu_Projects", 8, "Projects", (int)BudgetarySecurityItems.Projects, "", "", ref CurRow);
            if (!modBudgetaryMaster.Statics.ProjectFlag) tItem.Enabled = false;
            gcollMenu.AddItem(tItem);
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Reports", 9, "Printing", (int)BudgetarySecurityItems.Printing, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_File", 10, "File Maintenance", (int)BudgetarySecurityItems.FileMaintenance, "M", "", ref CurRow));

            
            if (customMenu) gcollMenu.AddItem(menuController.CreateMenuItem("BD_Import_Custom", 11, "Custom Programs", 0, "", "", ref CurRow));
            GetMainMenu = gcollMenu;
            return GetMainMenu;
        }

        private cGenericCollection GetProjectsMenu()
        {
            int CurRow;
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Projects";
            GenerateCaption("[Projects]");
            CurRow = 1;
            gcollMenu.ClearList();

            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SetupProjects", 1, "Setup Projects", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ProjectSummarySelect", 2, "Project Summary", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ProjectDetailSelect", 3, "Project Detail", 0, "", "", ref CurRow));

            return gcollMenu;
        }

        private cGenericCollection GetEndOfYearMenu()
        {
	        int CurRow;
	        var gcollMenu = new cGenericCollection();
	        strCurrentMenuName = "End of Year";
	        GenerateCaption("[End of Year]");
	        CurRow = 1;
	        gcollMenu.ClearList();

	        gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EOYProcessing", 1, "Fiscal End of Year", (int)BudgetarySecurityItems.EndOfYear, "", "", ref CurRow));
	        gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_EOYReports", 2, "End of Year Reports", 0, "", "", ref CurRow));
	        gcollMenu.AddItem(menuController.CreateMenuItem("BD_CreateOpeningAdjustments", 3, "Create Opening Adjustments", (int)BudgetarySecurityItems.CreateOpeningAdjustments, "", "", ref CurRow));
	        gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_1099", 4, "1099 Processing", (int)BudgetarySecurityItems.TaxProcessing, "", "", ref CurRow));
	        gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_TaxTitles", 5, "1099 Titles", 0, "", "", ref CurRow));

            return gcollMenu;
        }
        private cGenericCollection GetAccountsMenu()
        {
            int CurRow;
            var gcollMenu = new cGenericCollection();
            cMenuChoice tItem;

            strCurrentMenuName = "Accounts";
            GenerateCaption("[Accounts]");
            CurRow = 1;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_AccountSetup", 1, "Account Setup", (int)BudgetarySecurityItems.AccountSetup, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ValidAccounts", 2, "Valid Accounts", (int)BudgetarySecurityItems.ValidAccounts, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_LedgerControl", 3, "Default Accounts", (int)BudgetarySecurityItems.DefaultAccounts, "", "", ref CurRow));
            tItem = menuController.CreateMenuItem("BD_Form_LedgerRanges", 4, "Ledger Ranges", (int)BudgetarySecurityItems.LedgerRanges, "", "", ref CurRow);
            if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts) tItem.Enabled = false;
            gcollMenu.AddItem(tItem);
            
            return gcollMenu;
        }

        private cGenericCollection GetPayablesMenu()
        {
            int CurRow;
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Payables";
            GenerateCaption("[Payables]");
            CurRow = 1;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_AP", 1, "AP Processing", (int)BudgetarySecurityItems.AccountsPayableProcessing, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Vendor", 2, "Vendors", (int)BudgetarySecurityItems.Vendors, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetGJCorrDataEntry", 3, "AP Corrections", (int)BudgetarySecurityItems.AccountsPayableCorrections, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetEncDataEntry", 4, "Encumbrances", (int)BudgetarySecurityItems.Encumbrances, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EncumbranceUpdate", 5, "Encumbrance Corrections", (int)BudgetarySecurityItems.EncumbranceCorrections, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetCreditMemoDataEntry", 6, "Credit Memos", (int)BudgetarySecurityItems.CreditMemo, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CreditMemoCorrections", 7, "Credit Memo Corrections", (int)BudgetarySecurityItems.CreditMemoCorrection, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetPurchaseOrders", 8, "Purchase Orders", (int)BudgetarySecurityItems.PurchaseOrders, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_QueryJournal", 9, "View AP Journals", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SetupAPResearch", 10, "AP Research", 0, "", "", ref CurRow));


            return gcollMenu;
        }

        public cGenericCollection GetAPMenu()
        {
            cGenericCollection GetAPMenu = null;
            int CurRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "AP";
            GenerateCaption("[AP Processing]");
            CurRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetAPDataEntry", 10, "Invoice Entry", (int)BudgetarySecurityItems.AccountsPayable, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PrintIndividualChecks", 1, "Print Individual Checks", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_WarrantPreview", 2, "Warrant Preview", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PrintChecks", 3, "Checks", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CreatACHFiles", 4, "Create ACH File", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CheckRegister", 5, "Check Register", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_Warrant", 6, "Warrant", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_WarrantRecap", 7, "Warrant Recap", 0, "", "", ref CurRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_UpdateJournalInformation", 9, "Update Journal Information", 0, "", "", ref CurRow));
            GetAPMenu = gcollMenu;
            return GetAPMenu;
        }

        public cGenericCollection GetApprovedAmountMenu()
        {
            cGenericCollection GetApprovedAmountMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Approved Amounts]"
            strCurrentMenuName = "ApprovedAmount";
            GenerateCaption("[Approved Amounts]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PrintApprovedAmountsWorksheets", 1, "Print Worksheets", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EditApprovedAmounts", 2, "Update Approved Amounts", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 3, "Return to Budget Process", 0, "X", "", ref CurrRow));
            GetApprovedAmountMenu = gcollMenu;
            return GetApprovedAmountMenu;
        }

        public cGenericCollection Get1099Menu()
        {
            cGenericCollection Get1099Menu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [1099 Processing]"
            strCurrentMenuName = "1099";
            GenerateCaption("[1099 Processing]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SelectYear", 1, "Extract Information", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_1099EditReport", 2, "Print Edit Report", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Print1099s", 3, "Print 1099s", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Print1096", 4, "Print 1096", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_CreateElectronic1099File", 5, "Create Electronic 1099 File", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Archive1099ExtractFile", 6, "Archive Extract File", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_1099Restore", 7, "Restore Archive Extract File", (int)BudgetarySecurityItems.RestoreArchive, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Clear1099Adjustments", 8, "Clear 1099 Adjustments", (int)BudgetarySecurityItems.Clear1099Adjustments, "", "", ref CurrRow));
            Get1099Menu = gcollMenu;
            return Get1099Menu;
        }

        public cGenericCollection GetVendorMenu()
        {
            cGenericCollection GetVendorMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Vendor Routines]"
            strCurrentMenuName = "Vendor";
            GenerateCaption("[Vendor Routines]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_GetVendor", 1, "Vendor Maintenance", (int)BudgetarySecurityItems.VendorMaintenance, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_VendorListSetup", 2, "Print Listings", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ClassCodes", 3, "Class Codes", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_VendorLabels", 4, "Print Labels", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_VendorExtract", 6, "Vendor Extract", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_PurgedVendors", 7, "Purge Vendors", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_VendorDefaultAccountInfo", 8, "Vendor Default Account List", 0, "", "", ref CurrRow));
            GetVendorMenu = gcollMenu;
            return GetVendorMenu;
        }

        public cGenericCollection GetCheckFileMenu()
        {
            cGenericCollection GetCheckFileMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [List Check File]"
            strCurrentMenuName = "CheckFile";
            GenerateCaption("[List Check File]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CheckFileBalanceScreen", 1, "Balance Screen", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_CashedItemsOnly", 2, "Cashed Items Only", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_CheckFileList", 3, "Outstanding Items Only", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SetupAutoEntryReport", 4, "Automatic Entries Only", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CheckListSelection", 5, "Miscellaneous (By Selection)", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_rptBalancingReport", 6, "Balancing Report", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_CheckRec", 7, "Return to Check Rec", 0, "X", "", ref CurrRow));
            GetCheckFileMenu = gcollMenu;
            return GetCheckFileMenu;
        }

        public cGenericCollection GetPurgeChartsMenu()
        {
            cGenericCollection GetPurgeChartsMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Purge Charts]"
            strCurrentMenuName = "Purge";
            GenerateCaption("[Purge Charts]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PurgeChart", 1, "Purge Charts", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PurgeGraphItem", 2, "Purge Graph Items", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_File", 3, "Return to File Maintenance", 0, "X", "", ref CurrRow));
            GetPurgeChartsMenu = gcollMenu;
            return GetPurgeChartsMenu;
        }

        public cGenericCollection GetFormatsMenu()
        {
            cGenericCollection GetFormatsMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Report Format]"
            strCurrentMenuName = "Formats";
            GenerateCaption("[Report Format]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetExpenseSummary", 1, "Exp Summ Criteria / Format", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetRevenueSummary", 2, "Rev Summ Criteria / Format", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetLedgerSummary", 3, "G/L Summ Criteria / Format", 0, "", "", ref CurrRow));
            tItem = menuController.CreateMenuItem("BD_Form_GetProjectSummary", 4, "Proj Summ Criteria / Format", 0, "", "", ref CurrRow);
            if (!modBudgetaryMaster.Statics.ProjectFlag) tItem.Enabled = false;
            gcollMenu.AddItem(tItem);
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetExpenseDetail", 5, "Exp Detail Criteria / Format", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetRevenueDetail", 6, "Rev Detail Criteria / Format", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_GetLedgerDetail", 7, "G/L Detail Criteria / Format", 0, "", "", ref CurrRow));
            tItem = menuController.CreateMenuItem("BD_Form_GetProjectDetail", 8, "Proj Detail Criteria / Format", 0, "", "", ref CurrRow);
            if (!modBudgetaryMaster.Statics.ProjectFlag) tItem.Enabled = false;
            gcollMenu.AddItem(tItem);
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ReportSetup", 9, "Report Setup", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_File", 10, "Return to File Maintenance", 0, "X", "", ref CurrRow));
            GetFormatsMenu = gcollMenu;
            return GetFormatsMenu;
        }

        private cGenericCollection GetReportsMenu()
        {
            cGenericCollection GetReportsMenu = null;
            int CurrRow;
            var strTemp = "";
            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Printing]"
            strCurrentMenuName = "Reports";
            GenerateCaption("[Printing]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Summary", 1, "Summary Reports", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Detail", 2, "Detail Reports", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CurrentAccountStatus", 3, "Current Account Status", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ChartOfAccounts", 4, "Chart of Accounts", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BudgetAdjustments", 5, "Budget / Beg Bal Adjustments", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_VendorDetail", 6, "Vendor Detail", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_MultipleJournalSelectionP", 7, "Posted Journal", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_MultipleJournalSelectionU", 8, "Unposted Journal", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_EncumbranceReports", 10, "Encumbrance Reports", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_JournalListing", 11, "Journal Summary List", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CustomReportSelection", 12, "Report Writer", (int)BudgetarySecurityItems.ReportWriter, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Formats", 13, "Custom Report Setup", (int)BudgetarySecurityItems.CustomReportSetup, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_ValidAccounts", 14, "Valid Accounts List", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_AccountResearch", 15, "Account Research", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_OutstandingCreditMemos", 16, "Outstanding CM List", 0, "", "", ref CurrRow));
            GetReportsMenu = gcollMenu;
            return GetReportsMenu;
        }

        private cGenericCollection GetFileMenu()
        {
            cGenericCollection GetFileMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [File Maintenance]"
            strCurrentMenuName = "File";
            GenerateCaption("[File Maintenance]");
            CurrRow = 1;
            
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_Customization", 1, "Customize", (int)BudgetarySecurityItems.Customize, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_CDBS", 2, "Check Database Structure", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BankNames", 3, "Bank Names", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_ForceCalculate", 4, "Force Calculate", 0, "", "", ref CurrRow));
            GetFileMenu = gcollMenu;
            return GetFileMenu;
        }

        private cGenericCollection GetBudgetMenu()
        {
            cGenericCollection GetBudgetMenu = null;
            int CurrRow;
            var gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Budget";
            GenerateCaption("[Budget Process]");
            CurrRow = 1;

            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ClearBudgetAmounts", 1, "Clear Budget Information", (int)BudgetarySecurityItems.ClearBudget, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_InitialRequest", 2, "Initial Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_ManagerRequest", 3, "Manager Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_CommitteeRequest", 4, "Budget Committee Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_ElectedRequest", 5, "Elected Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_ApprovedAmount", 6, "Approved Amounts", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CustomBudgetReport", 7, "Custom Budget Report", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BudgetComments", 8, "Input Budget Comments", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BudgetBreakdown", 9, "Monthly Budget Breakdown", (int)BudgetarySecurityItems.BudgetBreakdown, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_TransferBudget", 10, "Transfer Approved to Budget", (int)BudgetarySecurityItems.TransferApprovedToBudget, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_CreateBudgetExtract", 11, "Create Budget Extract", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_BudgetImport", 12, "Load Budget Info", (int)BudgetarySecurityItems.LoadBudget, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_UpdateBudgetTable", 13, "Update Budget Table", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_UpdatePreviousSelectYear", 14, "Update Previous Year Info", 0, "", "", ref CurrRow));
            GetBudgetMenu = gcollMenu;
            return GetBudgetMenu;
        }

        public cGenericCollection GetAccountMenu()
        {
            cGenericCollection GetAccountMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Account Setup]"
            strCurrentMenuName = "Account";
            GenerateCaption("[Account Setup]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_OutlineDeptQuery", 1, "Department / Division Titles", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_OutlineExpQuery", 2, "Expense / Object Titles", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_OutlineRevQuery", 3, "Revenue Titles", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_OutlineLedgerQuery", 4, "General Ledger Titles", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Main", 12, "Return to Main", 0, "X", "", ref CurrRow));
            GetAccountMenu = gcollMenu;
            return GetAccountMenu;
        }

        public cGenericCollection GetEncumbranceReportsMenu()
        {
            cGenericCollection GetEncumbranceReportsMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Encumbrance Reports]"
            strCurrentMenuName = "EncumbranceReports";
            GenerateCaption("[Encumbrance Reports]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_OutstandingEncumbrances", 1, "Outstanding Enc List", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_LiquidatedEncumbrances", 2, "Liquidated Enc List", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PrintPurchaseOrder", 3, "Print Purchase Order", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PrintPurchaseOrderSummary", 4, "Purchase Order Summary", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Reports", 5, "Return to Printing", 0, "X", "", ref CurrRow));
            GetEncumbranceReportsMenu = gcollMenu;
            return GetEncumbranceReportsMenu;
        }

        public cGenericCollection GetEOYReportsMenu()
        {
            cGenericCollection GetEOYReportsMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [End Of Year (Fiscal) Reports]"
            strCurrentMenuName = "EOYReports";
            GenerateCaption("[End Of Year (Fiscal) Reports]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            tItem = menuController.CreateMenuItem("BD_Report_ClosingJournal", 1, "Print Closing Journal", 0, "", "", ref CurrRow);
            if (Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) == "" || !File.Exists(Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, Strings.Trim(modGlobalConstants.Statics.gstrArchiveYear) + "EOYClosingJournal.rdf"))) tItem.Enabled = false;
            gcollMenu.AddItem(tItem);
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Report_AccountCloseout", 2, "Print Acct Closeout Report", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Reports", 3, "Return to Printing", 0, "X", "", ref CurrRow));
            GetEOYReportsMenu = gcollMenu;
            return GetEOYReportsMenu;
        }

        public cGenericCollection GetElectedRequestMenu()
        {
            cGenericCollection GetElectedRequestMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Elected Request]"
            strCurrentMenuName = "ElectedRequest";
            GenerateCaption("[Elected Request]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PrintElectedRequestWorksheets", 1, "Print Worksheets", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EditElectedRequests", 2, "Update Elec Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_TransferElectedRequestToApproved", 3, "Transfer Elec Requests", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 4, "Return to Budget Process", 0, "X", "", ref CurrRow));
            GetElectedRequestMenu = gcollMenu;
            return GetElectedRequestMenu;
        }

        public cGenericCollection GetCommitteeRequestMenu()
        {
            cGenericCollection GetCommitteeRequestMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Committee Request]"
            strCurrentMenuName = "CommitteeRequest";
            GenerateCaption("[Committee Request]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PrintCommitteeRequestWorksheets", 1, "Print Worksheets", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EditCommitteeRequests", 2, "Update Comm Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_TransferCommittee", 3, "Transfer Comm Requests", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 4, "Return to Budget Process", 0, "X", "", ref CurrRow));
            GetCommitteeRequestMenu = gcollMenu;
            return GetCommitteeRequestMenu;
        }

        public cGenericCollection GetManagerRequestMenu()
        {
            cGenericCollection GetManagerRequestMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Manager Request]"
            strCurrentMenuName = "ManagerRequest";
            GenerateCaption("[Manager Request]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PrintManagerRequestWorksheets", 1, "Print Worksheets", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EditManagerRequests", 2, "Update Manager Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_TransferManager", 3, "Transfer Manager Requests", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 4, "Return to Budget Process", 0, "X", "", ref CurrRow));
            GetManagerRequestMenu = gcollMenu;
            return GetManagerRequestMenu;
        }

        public cGenericCollection GetInitialRequestMenu()
        {
            cGenericCollection GetInitialRequestMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Initial Request]"
            strCurrentMenuName = "InitialRequest";
            GenerateCaption("[Initial Request]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PrintInitialRequestWorksheets", 1, "Print Worksheets", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_EditInitialRequests", 2, "Update Initial Requests", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_TransferInitial", 3, "Transfer Initial Requests", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Budget", 4, "Return to Budget Process", 0, "X", "", ref CurrRow));
            GetInitialRequestMenu = gcollMenu;
            return GetInitialRequestMenu;
        }

        private cGenericCollection GetCheckRecMenu()
        {
            cGenericCollection GetCheckRecMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Check Reconciliation]"
            strCurrentMenuName = "CheckRec";
            GenerateCaption("[Bank Reconciliation]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_CheckRecBalanceScreen", 1, "Balance Screen", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_AddCheckDep", 2, "Add Check or Deposit", (int)BudgetarySecurityItems.AddCheckOrDeposit, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ChangeStatementDate", 3, "Enter Statement Date", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_FlagCashed", 4, "Flag Cashed", (int)BudgetarySecurityItems.FlagCashed, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_CheckFile", 5, "List or Display Check File", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PurgeChecks", 6, "Purge Checks / Reset Bal", (int)BudgetarySecurityItems.PurgeChecks, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ChangeCheckStatus", 7, "Update Status Check / Dep", (int)BudgetarySecurityItems.UpdateStatusCheckOrDeposit, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ChangeBank", 8, "Change Bank Number", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ArchiveViewSelection", 9, "View Archive Records", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_UpdateDeposit", 10, "Update Deposits", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_PurgeSelected", 11, "Purge Selected", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_SetupAuditReport", 12, "Audit Report", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_PositivePayExport",13,"Positive Pay Export",0,"","",ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Main", 13, "Return to Main", 0, "X", "", ref CurrRow));
            GetCheckRecMenu = gcollMenu;
            cBank tBank;
            tBank = bankService.GetCurrentBank();
            if (!(tBank == null))
            {
                PanelMessage = "Bank:  " + FCConvert.ToString(tBank.ID) + " " + Strings.Trim(tBank.Name);
            }
            else
            {
            }
            return GetCheckRecMenu;
        }

        public cGenericCollection GetSummaryMenu()
        {
            cGenericCollection GetSummaryMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary  [Summary Reports]"
            strCurrentMenuName = "Summary";
            GenerateCaption("[Summary Reports]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ExpenseSummarySelect", 1, "Expense Summary", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_RevenueSummarySelect", 2, "Revenue Summary", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_LedgerSummarySelect", 3, "G/L Summary", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ExpRevSummary", 4, "Exp / Rev Summary", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ExpenseDetailVendorSummarySelect", 5, "Exp Vendor Summary", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_JournalAccountSummary", 6, "Journal Account Summary", 0, "", "", ref CurrRow));
            
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_TrialBalance", 14, "Trial Balance", 0, "", "", ref CurrRow));
            //gcollMenu.AddItem(menuController.CreateMenuItem("BD_Menu_Reports", 13, "Return to Printing", 0, "X", "", ref CurrRow));
            GetSummaryMenu = gcollMenu;
            return GetSummaryMenu;
        }

        public cGenericCollection GetDetailMenu()
        {
            cGenericCollection GetDetailMenu = null;
            int CurrRow;
            var strTemp = "";

            var gcollMenu = new cGenericCollection();
            // strCurrentFormCaption = "TRIO Software - Budgetary   [Detail Reports]"
            strCurrentMenuName = "Detail";
            GenerateCaption("[Detail Reports]");
            CurrRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_ExpenseDetailSelect", 1, "Expense Detail", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_RevenueDetailSelect", 2, "Revenue Detail", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_LedgerDetailSelect", 3, "G/L Detail", 0, "", "", ref CurrRow));
            gcollMenu.AddItem(menuController.CreateMenuItem("BD_Form_JournalAccountReportsDetail", 4, "Journal Account Detail", 0, "", "", ref CurrRow));
            
            tItem = menuController.CreateMenuItem("BD_Form_ExpObjDetailSetup", 9, "Exp / Obj Detail", 0, "", "", ref CurrRow);
            gcollMenu.AddItem(tItem);
            GetDetailMenu = gcollMenu;
            return GetDetailMenu;
        }

        public cMDIView() : base()
        {
            strBaseCaption = "TRIO Software";
            strApplicationName = "Budgetary";
            bdcHandler = new cBDCommandHandler();
            //bdcHandler.MenuChanged += bdcHandler_MenuChanged;
        }

        private void bdcHandler_MenuChanged(string strMenuName)
        {
            SetMenu(strMenuName);
        }
    }
}
