﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJDefaultDataEntry.
	/// </summary>
	public partial class frmGJDefaultDataEntry : BaseForm
	{
		public frmGJDefaultDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:DSE #i605 Code moved to constructor (Load event comes when showing the form, it should not delete all row data!)
			int counter;
			int counter2;
			clsDRWrapper rsEncAdj = new clsDRWrapper();
			clsDRWrapper rsProjects = new clsDRWrapper();
			NumberCol = 0;
			DescriptionCol = 1;
			RCBCol = 2;
			AccountCol = 3;
			ProjCol = 4;
			DebitCol = 5;
			CreditCol = 6;
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.27 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.25 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(RCBCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(DebitCol, FCConvert.ToInt32(0.16 * vs1.WidthOriginal));
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, ProjCol, "Proj");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, DebitCol, "Debit");
			vs1.TextMatrix(0, CreditCol, "Credit");
			vs1.TextMatrix(0, RCBCol, "RCB");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DescriptionCol, 0, CreditCol, 4);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			vs1.ColFormat(DebitCol, "#,###.00");
			vs1.ColFormat(CreditCol, "#,###.00");
			strComboList = "# ; " + "\t" + " |";
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vs1.ColComboList(ProjCol, strComboList);
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, DebitCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, CreditCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, RCBCol, "R");
				vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, DescriptionCol, "");
				vs1.TextMatrix(counter, AccountCol, "");
				vs1.TextMatrix(counter, ProjCol, "");
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGJDefaultDataEntry InstancePtr
		{
			get
			{
				return (frmGJDefaultDataEntry)Sys.GetInstance(typeof(frmGJDefaultDataEntry));
			}
		}

		protected frmGJDefaultDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int DescriptionCol;
		int RCBCol;
		int AccountCol;
		int ProjCol;
		int DebitCol;
		int CreditCol;
		// vbPorter upgrade warning: TotalDebit As double	OnWrite(short, Decimal)
		double TotalDebit;
		// vbPorter upgrade warning: TotalCredit As double	OnWrite(short, Decimal)
		double TotalCredit;
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		string ErrorString = "";
		bool BadAccountFlag;
		string strEncOffAccount = "";
		string strRevCtrAccount = "";
		string strExpCtrAccount = "";
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnUnload;
		int lngYear;
		public int lngChosenType;
		string strComboList;
		clsGridAccount vsGrid = new clsGridAccount();

		private void frmGJDefaultDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			CalculateTotals();
			lblNetTotal.Text = Strings.FormatCurrency(TotalDebit - TotalCredit, 2);
			this.Refresh();
		}

		private void frmGJDefaultDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
				}
			}
		}

		private void frmGJDefaultDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGJDefaultDataEntry.FillStyle	= 0;
			//frmGJDefaultDataEntry.ScaleWidth	= 9300;
			//frmGJDefaultDataEntry.ScaleHeight	= 7350;
			//frmGJDefaultDataEntry.LinkTopic	= "Form2";
			//frmGJDefaultDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:DSE #i605 Code moved to constructor (Load event comes when showing the form, it should not delete all row data!)
			//int counter;
			//int counter2;
			//clsDRWrapper rsEncAdj = new clsDRWrapper();
			//clsDRWrapper rsProjects = new clsDRWrapper();
			//NumberCol = 0;
			//DescriptionCol = 1;
			//RCBCol = 2;
			//AccountCol = 3;
			//ProjCol = 4;
			//DebitCol = 5;
			//CreditCol = 6;
			//vsGrid.GRID7Light = vs1;
			//if (modValidateAccount.Statics.gboolTownAccounts)
			//{
			//    vsGrid.DefaultAccountType = "E";
			//}
			//else
			//{
			//    vsGrid.DefaultAccountType = "P";
			//}
			//vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
			//vs1.ColWidth(NumberCol, 0);
			//vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.27 * vs1.WidthOriginal));
			//vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.25 * vs1.WidthOriginal));
			//vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			//vs1.ColWidth(RCBCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			//vs1.ColWidth(DebitCol, FCConvert.ToInt32(0.16 * vs1.WidthOriginal));
			//vs1.TextMatrix(0, DescriptionCol, "Description");
			//vs1.TextMatrix(0, ProjCol, "Proj");
			//vs1.TextMatrix(0, AccountCol, "Account");
			//vs1.TextMatrix(0, DebitCol, "Debit");
			//vs1.TextMatrix(0, CreditCol, "Credit");
			//vs1.TextMatrix(0, RCBCol, "RCB");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DescriptionCol, 0, CreditCol, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			//vs1.ColFormat(DebitCol, "#,###.00");
			//vs1.ColFormat(CreditCol, "#,###.00");
			//strComboList = "# ; " + "\t" + " |";
			//if (modBudgetaryMaster.Statics.ProjectFlag)
			//{
			//    rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
			//    if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
			//    {
			//        do
			//        {
			//            strComboList += "'" + rsProjects.Get_Fields("ProjectCode") + ";" + rsProjects.Get_Fields("ProjectCode") + "\t" + rsProjects.Get_Fields("LongDescription") + "|";
			//            rsProjects.MoveNext();
			//        }
			//        while (rsProjects.EndOfFile() != true);
			//        strComboList = Strings.Left(strComboList, strComboList.Length - 1);
			//    }
			//    vs1.ColComboList(ProjCol, strComboList);
			//}
			//for (counter = 1; counter <= vs1.Rows - 1; counter++)
			//{
			//    vs1.TextMatrix(counter, DebitCol, FCConvert.ToString(0));
			//    vs1.TextMatrix(counter, CreditCol, FCConvert.ToString(0));
			//    vs1.TextMatrix(counter, RCBCol, "R");
			//    vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
			//    vs1.TextMatrix(counter, DescriptionCol, "");
			//    vs1.TextMatrix(counter, AccountCol, "");
			//    vs1.TextMatrix(counter, ProjCol, "");
			//}
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			//vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
			//SetCustomFormColors();
		}

		private void frmGJDefaultDataEntry_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.27 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.25 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(RCBCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(DebitCol, FCConvert.ToInt32(0.16 * vs1.WidthOriginal));
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				vs1.RowHeight(counter, FCConvert.ToInt32(vs1.HeightOriginal * 0.06));
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmGetGJDefault.InstancePtr.Show(App.MainForm);
		}

		private void frmGJDefaultDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vs1")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, ProjCol) == "" && vs1.TextMatrix(vs1.Row, DebitCol) == "0" && vs1.TextMatrix(vs1.Row, CreditCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, CreditCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					//FC:FINAL:VGE - #669 Data from selected field is being carried over to row bellow. Disabling data grid to prevent.
					vs1.Enabled = false;
					DeleteFlag = true;
					temp = vs1.Row;
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					vs1.Col = NumberCol;
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DebitCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, CreditCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, RCBCol, "R");
					vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Rows - 1, RCBCol, 4);
					CalculateTotals();
					lblNetTotal.Text = Strings.FormatCurrency(TotalDebit - TotalCredit, 2);
					DeleteFlag = false;
					//FC:FINAL:VGE - #669 Reenabling data grid.
					vs1.Enabled = true;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DescriptionCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(vs1.Row, DescriptionCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (frmGJDefaultDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			btnProcessDelete.Enabled = true;
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DescriptionCol;
			}
			else if (vs1.Col != AccountCol)
			{
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			vs1.EditMaxLength = 0;
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DescriptionCol;
			}
			else if (vs1.Col == ProjCol)
			{
				if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
				{
					vs1.Col += 1;
				}
				else
				{
					vs1.EditMaxLength = 4;
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			else if (vs1.Col == RCBCol)
			{
				vs1.EditMaxLength = 1;
				vs1.EditCell();
				vs1.EditSelStart = 0;
				vs1.EditSelLength = 1;
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else
			{
				if (vs1.Col == DebitCol || vs1.Col == CreditCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.Col != AccountCol)
			{
				if (vs1.Col == DebitCol || vs1.Col == CreditCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

        private void Vs1_EditingControlShowing1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= vs1_KeyDownEdit;
                e.Control.KeyDown += vs1_KeyDownEdit;
            }
        }


        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
            Keys KeyCode = e.KeyCode;
            if (vs1.Col == AccountCol && e.KeyCode != Keys.F9)
            {
                if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
                {
                    KeyCode = 0;
                    vs1.Col -= 1;
                }
                else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
                {
                    KeyCode = 0;
                    Support.SendKeys("{TAB}", false);
                }
            }
            if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                if (vs1.Col < CreditCol)
                {
                    vs1.Col += 1;
                    if (vs1.Col - 1 == DescriptionCol)
                    {
                        if (vs1.Row > 1)
                        {
                            if (vs1.Col - 1 == DescriptionCol && vs1.TextMatrix(vs1.Row, vs1.Col - 1) == "")
                            {
                                vs1.TextMatrix(vs1.Row, vs1.Col - 1, vs1.TextMatrix(vs1.Row - 1, vs1.Col - 1));
                            }
                        }
                    }
                }
                else
                {
                    if (vs1.Row < vs1.Rows - 1)
                    {
                        vs1.Row += 1;
                        vs1.Col = DescriptionCol;
                    }
                    else
                    {   //FC:FINAL:PB: - issue #2857 :added App.Update, vs1.Select()
                        vs1.AddItem("");
                        FCUtils.ApplicationUpdate(this.vs1);
                        vs1.TextMatrix(vs1.Row + 1, DebitCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, CreditCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, RCBCol, "R");
                        vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Row + 1, RCBCol, 4);
                        vs1.Select(vs1.Row + 1, DescriptionCol);
                        //vs1.Row += 1;
                        //vs1.Col = 0;
                    }
                }
            }
            else if (KeyCode == Keys.Left)
            {
                if (vs1.Col == DebitCol)
                {
                    if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
                    {
                        KeyCode = 0;
                        vs1.Col -= 2;
                    }
                }
            }
            else if (vs1.Col == CreditCol || vs1.Col == DebitCol)
            {
                if (KeyCode == Keys.C)
                {
                    // if the c key is hit then erase the value
                    KeyCode = 0;
                    vs1.EditText = "";
                    Support.SendKeys("{BACKSPACE}", false);
                }
            }
        }
        

        private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string holder = "";
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == DebitCol || col == CreditCol)
			{
				if (Strings.Trim(vs1.EditText) != "")
				{
					if (!Information.IsNumeric(vs1.EditText))
					{
						MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
					else
					{
						temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
						if (temp != 0)
						{
							if (vs1.EditText.Length > temp + 2)
							{
								vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
							}
						}
						CalculateTotals();
						lblNetTotal.Text = Strings.FormatCurrency(TotalDebit - TotalCredit, 2);
					}
				}
				else
				{
					if (vs1.EditText == "")
					{
						vs1.EditText = "0";
					}
				}
			}
			else if (col == RCBCol)
			{
				if (vs1.EditText == "")
				{
					vs1.EditText = "R";
				}
				else if (Strings.UCase(vs1.EditText) == "R" || Strings.UCase(vs1.EditText) == "C" || Strings.UCase(vs1.EditText) == "B" || Strings.UCase(vs1.EditText) == "F")
				{
					vs1.EditText = Strings.UCase(vs1.EditText);
				}
				else
				{
					MessageBox.Show("You may only enter an R, C, or B in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalDebit = 0;
			TotalCredit = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
					if (vs1.Col == DebitCol)
					{
						if (Information.IsNumeric(vs1.EditText))
						{
							TotalDebit += FCConvert.ToDouble(vs1.EditText);
						}
						TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
					}
					else if (vs1.Col == CreditCol)
					{
						if (Information.IsNumeric(vs1.EditText))
						{
							TotalCredit += FCConvert.ToDouble(vs1.EditText);
						}
						TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
					}
					else
					{
						TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
						TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
					}
				}
				else
				{
					TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
					TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnData;
			clsDRWrapper rsDuplicate = new clsDRWrapper();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a description for this default type before you may continue.", "No Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtDescription.Focus();
				return;
			}
			blnData = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol)) != 0 || FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol)) != 0 || vs1.TextMatrix(counter, DescriptionCol) != "" || vs1.TextMatrix(counter, AccountCol) != "" || (modBudgetaryMaster.Statics.ProjectFlag && vs1.TextMatrix(counter, ProjCol) != ""))
				{
					blnData = true;
				}
				if (vs1.TextMatrix(counter, AccountCol) != "")
				{
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
					{
						ans = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "General Journal Default Data Entry");
						}
					}
				}
			}
			if (!blnData)
			{
				MessageBox.Show("You must make at least one entry before you may save this", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (lngChosenType != 0)
			{
				rsDuplicate.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + Strings.Trim(txtDescription.Text) + "'");
				if (rsDuplicate.EndOfFile() != true && rsDuplicate.BeginningOfFile() != true)
				{
					if (FCConvert.ToInt32(rsDuplicate.Get_Fields_Int32("ID")) != lngChosenType)
					{
						MessageBox.Show("The description you entered is used for another default type.  You must enter a different description before you may continue.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtDescription.Focus();
						return;
					}
				}
				rs.Execute("DELETE FROM GJDefaultDetail WHERE GJDefaultMasterID = " + FCConvert.ToString(lngChosenType), "Budgetary");
			}
			else
			{
				rsDuplicate.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + modCustomReport.FixQuotes(Strings.Trim(txtDescription.Text)) + "'");
				if (rsDuplicate.EndOfFile() != true && rsDuplicate.BeginningOfFile() != true)
				{
					MessageBox.Show("The description you entered is used for another default type.  You must enter a different description before you may continue.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtDescription.Focus();
					return;
				}
				rs.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", Strings.Trim(txtDescription.Text));
				rs.Update();
				lngChosenType = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol)) != 0 || FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol)) != 0 || vs1.TextMatrix(counter, DescriptionCol) != "" || vs1.TextMatrix(counter, AccountCol) != "" || (modBudgetaryMaster.Statics.ProjectFlag && vs1.TextMatrix(counter, ProjCol) != ""))
				{
					rs.OpenRecordset("SELECT * FROM GJDefaultDetail WHERE ID = 0");
					rs.AddNew();
					rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
					rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
					rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					rs.Set_Fields("RCB", vs1.TextMatrix(counter, RCBCol));
					rs.Set_Fields("Amount", FCConvert.ToDecimal(vs1.TextMatrix(counter, DebitCol)) - FCConvert.ToDecimal(vs1.TextMatrix(counter, CreditCol)));
					rs.Set_Fields("GJDefaultMasterID", lngChosenType);
					rs.Update();
				}
			}
			if (blnUnload)
			{
				Close();
			}
			//FC:FINAL:PB: - issue: #2856 added "Save Successful" message
            FCMessageBox.Show("Save Successful",MsgBoxStyle.Information,"Save Complete");
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
