﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAPResearch.
	/// </summary>
	public partial class rptAPResearch : BaseSectionReport
	{
		public static rptAPResearch InstancePtr
		{
			get
			{
				return (rptAPResearch)Sys.GetInstance(typeof(rptAPResearch));
			}
		}

		protected rptAPResearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAPResearch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int counter;
		int WarrantCol;
		int JournalCol;
		int VendorNumberCol;
		int VendorNameCol;
		int CheckNumberCol;
		int CheckDateCol;
		int ReferenceCol;
		int JournalDescriptionCol;
		int AmountCol;
		int AccountCol;
		int LineDescriptionCol;

		public rptAPResearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "AP Research";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				counter += 1;
				if (counter > frmAPResearch.InstancePtr.vsInfo.Rows - 1)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 10.5f;
			blnFirstRecord = true;
			counter = 1;
			WarrantCol = 0;
			JournalCol = 1;
			VendorNumberCol = 2;
			VendorNameCol = 3;
			CheckNumberCol = 4;
			CheckDateCol = 5;
			ReferenceCol = 6;
			JournalDescriptionCol = 7;
			LineDescriptionCol = 8;
			AccountCol = 9;
			AmountCol = 10;
			if (frmAPResearch.InstancePtr.vsInfo.Rows <= 1)
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
			else
			{
				if (frmAPResearch.InstancePtr.vsInfo.ColHidden(AccountCol) == true)
				{
					lblLineDescription.Visible = false;
					lblAccount.Visible = false;
					fldLineDescription.Visible = false;
					fldAccount.Visible = false;
					lblAmount.Left = lblLineDescription.Left;
					fldAmount.Left = fldLineDescription.Left;
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldWarrant.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, WarrantCol);
			fldJournal.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, JournalCol);
			fldVendor.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, VendorNumberCol) + "  " + frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, VendorNameCol);
			fldCheck.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, CheckNumberCol);
			fldCheckDate.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, CheckDateCol);
			fldReference.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, ReferenceCol);
			fldEntryDescription.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, JournalDescriptionCol);
			fldAmount.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, AmountCol);
			if (fldAccount.Visible == true)
			{
				fldLineDescription.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, LineDescriptionCol);
				fldAccount.Text = frmAPResearch.InstancePtr.vsInfo.TextMatrix(counter, AccountCol);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			lblTotalAPInvoices.Text = frmAPResearch.InstancePtr.lblTotalAPInvoices.Text;
			lblTotalAPChecks.Text = frmAPResearch.InstancePtr.lblTotalAPChecks.Text;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptAPResearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAPResearch.Caption	= "AP Research";
			//rptAPResearch.Icon	= "rptAPResearch.dsx":0000";
			//rptAPResearch.Left	= 0;
			//rptAPResearch.Top	= 0;
			//rptAPResearch.Width	= 15240;
			//rptAPResearch.Height	= 11115;
			//rptAPResearch.StartUpPosition	= 3;
			//rptAPResearch.SectionData	= "rptAPResearch.dsx":058A;
			//End Unmaped Properties
		}

		private void rptAPResearch_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
