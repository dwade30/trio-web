﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmClearBudgetAmounts.
	/// </summary>
	public partial class frmClearBudgetAmounts : BaseForm
	{
		public frmClearBudgetAmounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmClearBudgetAmounts InstancePtr
		{
			get
			{
				return (frmClearBudgetAmounts)Sys.GetInstance(typeof(frmClearBudgetAmounts));
			}
		}

		protected frmClearBudgetAmounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         3/29/02
		// This form will be used to clear out the budget amounts used
		// for the last fiscal year so the town can start the budget
		// process for the new year
		// ********************************************************
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (cmbAllAmounts.SelectedIndex == 1)
			{
				if (chkInitial.CheckState != CheckState.Checked && chkManager.CheckState != CheckState.Checked && chkComments.CheckState != CheckState.Checked && chkCommittee.CheckState != CheckState.Checked && chkElected.CheckState != CheckState.Checked && chkApproved.CheckState != CheckState.Checked)
				{
					MessageBox.Show("You must select at least one type of amounts to clear before you may proceed", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			ans = MessageBox.Show("Are you sure you wish to delete your budget information?", "Delete Budget Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				ClearAmounts();
			}
			else
			{
				return;
			}
			modGlobalFunctions.AddCYAEntry_8("BD", "Clear Budget Amounts Process Done");
			MessageBox.Show("The data has been cleared successfully.", "Data Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(btnClear, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmClearBudgetAmounts_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmClearBudgetAmounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClearBudgetAmounts.FillStyle	= 0;
			//frmClearBudgetAmounts.ScaleWidth	= 9045;
			//frmClearBudgetAmounts.ScaleHeight	= 6930;
			//frmClearBudgetAmounts.LinkTopic	= "Form2";
			//frmClearBudgetAmounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			cmbAll.SelectedIndex = 0;
			cmbAllAmounts.SelectedIndex = 0;
			cmbPercents.SelectedIndex = 2;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmClearBudgetAmounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void optAllAmounts_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAllAmounts.SelectedIndex == 0)
			{
				chkInitial.CheckState = CheckState.Unchecked;
				chkManager.CheckState = CheckState.Unchecked;
				chkCommittee.CheckState = CheckState.Unchecked;
				chkApproved.CheckState = CheckState.Unchecked;
				chkElected.CheckState = CheckState.Unchecked;
				fraSelected.Enabled = false;
				chkInitial.Enabled = false;
				chkManager.Enabled = false;
				chkCommittee.Enabled = false;
				chkApproved.Enabled = false;
				chkElected.Enabled = false;
				cmbPercents.Enabled = false;
			}
			else if (cmbAllAmounts.SelectedIndex == 1)
			{
				optSelected_CheckedChanged(sender, e);
			}
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelected.Enabled = true;
			fraSelected.Enabled = true;
			chkInitial.Enabled = true;
			chkManager.Enabled = true;
			chkCommittee.Enabled = true;
			chkApproved.Enabled = true;
			chkElected.Enabled = true;
			cmbPercents.Enabled = true;
		}

		private void ClearAmounts()
		{
			clsDRWrapper rsBudgetData = new clsDRWrapper();
			if (cmbAll.SelectedIndex == 0)
			{
				if (cmbAllAmounts.SelectedIndex == 0 && cmbPercents.SelectedIndex == 2 && chkComments.CheckState == CheckState.Checked)
				{
					rsBudgetData.Execute("DELETE FROM Budget WHERE Left(Account, 1) = 'E' or Left(Account, 1) = 'R'", "Budgetary");
				}
				else
				{
					rsBudgetData.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E' or Left(Account, 1) = 'R'");
					if (rsBudgetData.EndOfFile() != true && rsBudgetData.BeginningOfFile() != true)
					{
						do
						{
							rsBudgetData.Edit();
							if (chkInitial.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("InitialRequest", 0);
							}
							if (chkManager.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ManagerRequest", 0);
							}
							if (chkCommittee.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("CommitteeRequest", 0);
							}
							if (chkElected.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ElectedRequest", 0);
							}
							if (chkApproved.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ApprovedAmount", 0);
							}
							if (chkComments.CheckState == CheckState.Checked)
							{
								rsBudgetData.Set_Fields("Comments", "");
							}
							if (cmbPercents.SelectedIndex == 2)
							{
								rsBudgetData.Set_Fields("JanBudget", 0);
								rsBudgetData.Set_Fields("FebBudget", 0);
								rsBudgetData.Set_Fields("MarBudget", 0);
								rsBudgetData.Set_Fields("AprBudget", 0);
								rsBudgetData.Set_Fields("MayBudget", 0);
								rsBudgetData.Set_Fields("JuneBudget", 0);
								rsBudgetData.Set_Fields("JulyBudget", 0);
								rsBudgetData.Set_Fields("AugBudget", 0);
								rsBudgetData.Set_Fields("SeptBudget", 0);
								rsBudgetData.Set_Fields("OctBudget", 0);
								rsBudgetData.Set_Fields("NovBudget", 0);
								rsBudgetData.Set_Fields("DecBudget", 0);
								rsBudgetData.Set_Fields("JanPercent", 0);
								rsBudgetData.Set_Fields("FebPercent", 0);
								rsBudgetData.Set_Fields("MarPercent", 0);
								rsBudgetData.Set_Fields("AprPercent", 0);
								rsBudgetData.Set_Fields("MayPercent", 0);
								rsBudgetData.Set_Fields("JunePercent", 0);
								rsBudgetData.Set_Fields("JulyPercent", 0);
								rsBudgetData.Set_Fields("AugPercent", 0);
								rsBudgetData.Set_Fields("SeptPercent", 0);
								rsBudgetData.Set_Fields("OctPercent", 0);
								rsBudgetData.Set_Fields("NovPercent", 0);
								rsBudgetData.Set_Fields("DecPercent", 0);
								rsBudgetData.Set_Fields("PercentFlag", false);
							}
							else if (cmbPercents.SelectedIndex == 0)
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
									rsBudgetData.Set_Fields("PercentFlag", false);
								}
								else
								{
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							else
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
								}
								else
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							rsBudgetData.Update();
							rsBudgetData.MoveNext();
						}
						while (rsBudgetData.EndOfFile() != true);
					}
				}
			}
			else if (cmbAll.SelectedIndex == 1)
			{
				if (cmbAllAmounts.SelectedIndex == 0 && chkComments.CheckState == CheckState.Checked)
				{
					rsBudgetData.Execute("DELETE FROM Budget WHERE Left(Account, 1) = 'E'", "Budgetary");
				}
				else
				{
					rsBudgetData.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E'");
					if (rsBudgetData.EndOfFile() != true && rsBudgetData.BeginningOfFile() != true)
					{
						do
						{
							rsBudgetData.Edit();
							if (chkInitial.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("InitialRequest", 0);
							}
							if (chkManager.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ManagerRequest", 0);
							}
							if (chkCommittee.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("CommitteeRequest", 0);
							}
							if (chkElected.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ElectedRequest", 0);
							}
							if (chkApproved.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ApprovedAmount", 0);
							}
							if (chkComments.CheckState == CheckState.Checked)
							{
								rsBudgetData.Set_Fields("Comments", "");
							}
							if (cmbPercents.SelectedIndex == 2)
							{
								rsBudgetData.Set_Fields("JanBudget", 0);
								rsBudgetData.Set_Fields("FebBudget", 0);
								rsBudgetData.Set_Fields("MarBudget", 0);
								rsBudgetData.Set_Fields("AprBudget", 0);
								rsBudgetData.Set_Fields("MayBudget", 0);
								rsBudgetData.Set_Fields("JuneBudget", 0);
								rsBudgetData.Set_Fields("JulyBudget", 0);
								rsBudgetData.Set_Fields("AugBudget", 0);
								rsBudgetData.Set_Fields("SeptBudget", 0);
								rsBudgetData.Set_Fields("OctBudget", 0);
								rsBudgetData.Set_Fields("NovBudget", 0);
								rsBudgetData.Set_Fields("DecBudget", 0);
								rsBudgetData.Set_Fields("JanPercent", 0);
								rsBudgetData.Set_Fields("FebPercent", 0);
								rsBudgetData.Set_Fields("MarPercent", 0);
								rsBudgetData.Set_Fields("AprPercent", 0);
								rsBudgetData.Set_Fields("MayPercent", 0);
								rsBudgetData.Set_Fields("JunePercent", 0);
								rsBudgetData.Set_Fields("JulyPercent", 0);
								rsBudgetData.Set_Fields("AugPercent", 0);
								rsBudgetData.Set_Fields("SeptPercent", 0);
								rsBudgetData.Set_Fields("OctPercent", 0);
								rsBudgetData.Set_Fields("NovPercent", 0);
								rsBudgetData.Set_Fields("DecPercent", 0);
								rsBudgetData.Set_Fields("PercentFlag", false);
							}
							else if (cmbPercents.SelectedIndex == 0)
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
									rsBudgetData.Set_Fields("PercentFlag", false);
								}
								else
								{
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							else
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
								}
								else
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							rsBudgetData.Update();
							rsBudgetData.MoveNext();
						}
						while (rsBudgetData.EndOfFile() != true);
					}
				}
			}
			else
			{
				if (cmbAllAmounts.SelectedIndex == 0 && chkComments.CheckState == CheckState.Checked)
				{
					rsBudgetData.Execute("DELETE FROM Budget WHERE Left(Account, 1) = 'R'", "Budgetary");
				}
				else
				{
					rsBudgetData.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'R'");
					if (rsBudgetData.EndOfFile() != true && rsBudgetData.BeginningOfFile() != true)
					{
						do
						{
							rsBudgetData.Edit();
							if (chkInitial.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("InitialRequest", 0);
							}
							if (chkManager.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ManagerRequest", 0);
							}
							if (chkCommittee.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("CommitteeRequest", 0);
							}
							if (chkElected.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ElectedRequest", 0);
							}
							if (chkApproved.CheckState == CheckState.Checked || cmbAllAmounts.SelectedIndex == 0)
							{
								rsBudgetData.Set_Fields("ApprovedAmount", 0);
							}
							if (chkComments.CheckState == CheckState.Checked)
							{
								rsBudgetData.Set_Fields("Comments", "");
							}
							if (cmbPercents.SelectedIndex == 2)
							{
								rsBudgetData.Set_Fields("JanBudget", 0);
								rsBudgetData.Set_Fields("FebBudget", 0);
								rsBudgetData.Set_Fields("MarBudget", 0);
								rsBudgetData.Set_Fields("AprBudget", 0);
								rsBudgetData.Set_Fields("MayBudget", 0);
								rsBudgetData.Set_Fields("JuneBudget", 0);
								rsBudgetData.Set_Fields("JulyBudget", 0);
								rsBudgetData.Set_Fields("AugBudget", 0);
								rsBudgetData.Set_Fields("SeptBudget", 0);
								rsBudgetData.Set_Fields("OctBudget", 0);
								rsBudgetData.Set_Fields("NovBudget", 0);
								rsBudgetData.Set_Fields("DecBudget", 0);
								rsBudgetData.Set_Fields("JanPercent", 0);
								rsBudgetData.Set_Fields("FebPercent", 0);
								rsBudgetData.Set_Fields("MarPercent", 0);
								rsBudgetData.Set_Fields("AprPercent", 0);
								rsBudgetData.Set_Fields("MayPercent", 0);
								rsBudgetData.Set_Fields("JunePercent", 0);
								rsBudgetData.Set_Fields("JulyPercent", 0);
								rsBudgetData.Set_Fields("AugPercent", 0);
								rsBudgetData.Set_Fields("SeptPercent", 0);
								rsBudgetData.Set_Fields("OctPercent", 0);
								rsBudgetData.Set_Fields("NovPercent", 0);
								rsBudgetData.Set_Fields("DecPercent", 0);
								rsBudgetData.Set_Fields("PercentFlag", false);
							}
							else if (cmbPercents.SelectedIndex == 0)
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
									rsBudgetData.Set_Fields("PercentFlag", false);
								}
								else
								{
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							else
							{
								if (FCConvert.ToBoolean(rsBudgetData.Get_Fields_Boolean("PercentFlag")))
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
								}
								else
								{
									rsBudgetData.Set_Fields("JanBudget", 0);
									rsBudgetData.Set_Fields("FebBudget", 0);
									rsBudgetData.Set_Fields("MarBudget", 0);
									rsBudgetData.Set_Fields("AprBudget", 0);
									rsBudgetData.Set_Fields("MayBudget", 0);
									rsBudgetData.Set_Fields("JuneBudget", 0);
									rsBudgetData.Set_Fields("JulyBudget", 0);
									rsBudgetData.Set_Fields("AugBudget", 0);
									rsBudgetData.Set_Fields("SeptBudget", 0);
									rsBudgetData.Set_Fields("OctBudget", 0);
									rsBudgetData.Set_Fields("NovBudget", 0);
									rsBudgetData.Set_Fields("DecBudget", 0);
									rsBudgetData.Set_Fields("JanPercent", 0);
									rsBudgetData.Set_Fields("FebPercent", 0);
									rsBudgetData.Set_Fields("MarPercent", 0);
									rsBudgetData.Set_Fields("AprPercent", 0);
									rsBudgetData.Set_Fields("MayPercent", 0);
									rsBudgetData.Set_Fields("JunePercent", 0);
									rsBudgetData.Set_Fields("JulyPercent", 0);
									rsBudgetData.Set_Fields("AugPercent", 0);
									rsBudgetData.Set_Fields("SeptPercent", 0);
									rsBudgetData.Set_Fields("OctPercent", 0);
									rsBudgetData.Set_Fields("NovPercent", 0);
									rsBudgetData.Set_Fields("DecPercent", 0);
								}
							}
							rsBudgetData.Update();
							rsBudgetData.MoveNext();
						}
						while (rsBudgetData.EndOfFile() != true);
					}
				}
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			cmdClear_Click();
		}
	}
}
