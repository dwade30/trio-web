﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWBD0000
{
	public class cCustomFunctionsService
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		private struct DaytonImportRecord
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] DateCharArray;
			public string Date
			{
				get
				{
					return FCUtils.FixedStringFromArray(DateCharArray);
				}
				set
				{
					DateCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 25)]
			public char[] DescriptionCharArray;
			public string Description
			{
				get
				{
					return FCUtils.FixedStringFromArray(DescriptionCharArray);
				}
				set
				{
					DescriptionCharArray = FCUtils.FixedStringToArray(value, 25);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
			public char[] AccountCharArray;
			public string Account
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountCharArray);
				}
				set
				{
					AccountCharArray = FCUtils.FixedStringToArray(value, 20);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
			public char[] AmountCharArray;
			public string Amount
			{
				get
				{
					return FCUtils.FixedStringFromArray(AmountCharArray);
				}
				set
				{
					AmountCharArray = FCUtils.FixedStringToArray(value, 11);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public DaytonImportRecord(int unusedParam)
			{
				this.DateCharArray = new string(' ', 10).ToArray();
				this.DescriptionCharArray = new string(' ', 25).ToArray();
				this.AccountCharArray = new string(' ', 20).ToArray();
				this.AmountCharArray = new string(' ', 11).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		private struct LincolnCountyPayrollImportStruct
		{
			public string Account;
			public DateTime CheckDate;
			public Decimal Amount;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public LincolnCountyPayrollImportStruct(int unusedParam)
			{
				this.Account = string.Empty;
				this.CheckDate = DateTime.FromOADate(0);
				this.Amount = 0;
			}
		};

		public void CalaisBDSetDBLocation()
		{
			string strCurDir = "";
            string strDbName = "";
            cSettingsController setCont = new cSettingsController();

            strDbName = setCont.GetSettingValue("WATERDBLOCATION", "", "", "", "");
            if (!String.IsNullOrWhiteSpace(strDbName))
            {
                if (MessageBox.Show("Your database is currently set to: " + strDbName + "\r\n" + "Do you wish to change it?","Change Database?",MessageBoxButtons.YesNo ,MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }

            PickAgain:
            ;
            object dbName = strDbName;
            //FC:FINAL:BSE:#4323 form initialization should be done every time to make form reopening possible   
            //var inputForm = new frmInput();
            while ((new frmInput()).Init(ref dbName, "Water Database", "Enter the full Water Budgetary database name", 4320, false, modGlobalConstants.InputDTypes.idtString, strDbName.Trim()))
            {
                strDbName = (string)dbName;
                if (!string.IsNullOrWhiteSpace(strDbName))
                {
                    var rsDb = new clsDRWrapper();
                    rsDb.MegaGroup = strDbName;
                    if (!rsDb.TableExists("JournalEntries","Budgetary"))
                    {
                        MessageBox.Show("Database not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    setCont.SaveSetting(strDbName, "WATERDBLOCATION", "", "", "", "");
                    return;
                }
                else
                {
                    return;
                }
            }
            return;			
		}

		public void DaytonImport()
		{
			string strCurDir;
			string strFilePath = "";
			// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
			int counter;
			DaytonImportRecord DaytonRecord = new DaytonImportRecord(0);
			int intRecords;
			clsDRWrapper rsEntries = new clsDRWrapper();
			int lngJournal;
			clsDRWrapper Master = new clsDRWrapper();
			string strBadAccounts;
			FCCommonDialog ofd = new FCCommonDialog();
			int intPeriod;
			try
			{
				// On Error GoTo ErrorHandler
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				Information.Err().Clear();
				//FC:TODO:PJ File-Handling (Options)
				//ofd.Options = vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames;
				if (ofd.ShowOpen())
				{
					strFilePath = ofd.FileName;
				}
				else
				{
					return;
				}
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Importing Data...");
				intRecords = 0;
				lngJournal = 0;
				strBadAccounts = "";
				intPeriod = DateTime.Today.Month;
				rsEntries.OpenRecordset("SELECT * FROM JournalEntries", "TWBD0000.vb1");
				FCFileSystem.FileOpen(1, strFilePath, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(DaytonRecord));
				for (counter = 1; counter <= FCConvert.ToInt32((FCFileSystem.LOF(1) / Marshal.SizeOf(DaytonRecord))); counter++)
				{
					//Application.DoEvents();
					FCFileSystem.FileGet(1, ref DaytonRecord, counter);
					if (FCConvert.ToDecimal(Strings.Trim(DaytonRecord.Amount)) != 0)
					{
						intRecords += 1;
						if (lngJournal != 0)
						{
							// do nothing
						}
						else
						{
							// get journal number
							if (modBudgetaryAccounting.LockJournal() == false)
							{
								// add entries to incomplete journals table
							}
							else
							{
								Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
								if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
								{
									Master.MoveLast();
									Master.MoveFirst();
									// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
									lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
								}
								else
								{
									lngJournal = 1;
								}
								Master.AddNew();
								Master.Set_Fields("JournalNumber", lngJournal);
								Master.Set_Fields("Status", "E");
								Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
								Master.Set_Fields("StatusChangeDate", DateTime.Today);
								Master.Set_Fields("Description", DaytonRecord.Date + " Journal Import");
								Master.Set_Fields("Type", "GJ");
								Master.Set_Fields("Period", intPeriod);
								Master.Update();
								Master.Reset();
								modBudgetaryAccounting.UnlockJournal();
							}
						}
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields("JournalEntriesDate", DaytonRecord.Date);
						rsEntries.Set_Fields("Description", Strings.Trim(DaytonRecord.Description));
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						rsEntries.Set_Fields("Account", Strings.Trim(DaytonRecord.Account));
						if (modValidateAccount.AccountValidate(Strings.Trim(DaytonRecord.Account)) == false)
						{
							strBadAccounts += Strings.Trim(DaytonRecord.Account) + "\r\n";
						}
						rsEntries.Set_Fields("Amount", Strings.Trim(DaytonRecord.Amount));
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", intPeriod);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
					}
				}
				frmWait.InstancePtr.Unload();
				if (intRecords > 0)
				{
					MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(lngJournal), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("No entries were found to import.", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				if (Strings.Trim(strBadAccounts) != "")
				{
					rptHancockCountyBadAccounts.InstancePtr.strBadAccts = strBadAccounts;
					frmReportViewer.InstancePtr.Init(rptHancockCountyBadAccounts.InstancePtr);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void LincolnCountyPayrollImport()
		{
			int counter;
			int intRecords;
			clsDRWrapper rsEntries = new clsDRWrapper();
			int lngJournal;
			clsDRWrapper Master = new clsDRWrapper();
			string[] strCheckInfo = null;
			StreamReader tsInfo;
			string strFileInfo = "";
			int intSplit;
			LincolnCountyPayrollImportStruct[] hpiInfo = null;
			int intTotalRecords;
			bool blnFound;
			string strCurDir;
			string strFilePath = "";
			string strBadAccounts;
			FCCommonDialog ofd = new FCCommonDialog();
			try
			{
				// On Error GoTo ErrorHandler
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				Information.Err().Clear();
				//FC:TODO:PJ File-Handling (Options)
				//ofd.Options = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames;
				if (ofd.ShowOpen())
				{
					strFilePath = ofd.FileName;
				}
				else
				{
					return;
				}
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Importing Data...");
				tsInfo = File.OpenText(strFilePath);
				intRecords = 0;
				lngJournal = 0;
				strBadAccounts = "";
				intTotalRecords = 0;
				while (tsInfo.EndOfStream == false)
				{
					//Application.DoEvents();
					strFileInfo = Strings.Trim(tsInfo.ReadLine());
					strCheckInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strCheckInfo, 1) >= 4)
					{
						if (!Information.IsNumeric(strCheckInfo[1]))
						{
							// do nothing
						}
						else
						{
							// If Not ValidBranch(Trim(strCheckInfo(3))) Then
							// GoTo NextRecord
							// End If
							intRecords += 1;
							if (lngJournal != 0)
							{
								// do nothing
							}
							else
							{
								// get journal number
								if (modBudgetaryAccounting.LockJournal() == false)
								{
									// add entries to incomplete journals table
								}
								else
								{
									Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
									if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
									{
										Master.MoveLast();
										Master.MoveFirst();
										// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
										lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
									}
									else
									{
										lngJournal = 1;
									}
									Master.AddNew();
									Master.Set_Fields("JournalNumber", lngJournal);
									Master.Set_Fields("Status", "E");
									Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
									Master.Set_Fields("StatusChangeDate", DateTime.Today);
									Master.Set_Fields("Description", strCheckInfo[4] + " Payroll Import");
									Master.Set_Fields("Type", "GJ");
									Master.Set_Fields("Period", DateAndTime.DateValue(strCheckInfo[4]).Month);
									Master.Update();
									Master.Reset();
									modBudgetaryAccounting.UnlockJournal();
								}
							}
							// If intTotalRecords = 0 Then
							Array.Resize(ref hpiInfo, intTotalRecords + 1);
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							hpiInfo[intTotalRecords] = new LincolnCountyPayrollImportStruct(0);
							hpiInfo[intTotalRecords].Account = Strings.Trim(strCheckInfo[0]);
							hpiInfo[intTotalRecords].Amount = FCConvert.ToDecimal(strCheckInfo[1]);
							hpiInfo[intTotalRecords].CheckDate = DateAndTime.DateValue(strCheckInfo[4]);
							intTotalRecords += 1;
							// Else
							// blnFound = False
							// For counter = 0 To intTotalRecords - 1
							// If hpiInfo(counter).Account = Trim(strCheckInfo(0)) And hpiInfo(counter).CheckDate = CDate(Left(strCheckInfo(4), 2) & "/" & Mid(strCheckInfo(4), 4, 2) & "/" & Right(strCheckInfo(4), 4)) Then
							// hpiInfo(counter).Amount = hpiInfo(counter).Amount + CCur(strCheckInfo(1))
							// blnFound = True
							// Exit For
							// End If
							// Next
							// If Not blnFound Then
							// ReDim Preserve hpiInfo(intTotalRecords)
							// hpiInfo(intTotalRecords).Account = strCheckInfo(0)
							// hpiInfo(intTotalRecords).Amount = CCur(strCheckInfo(1))
							// hpiInfo(intTotalRecords).CheckDate = CDate(Left(strCheckInfo(4), 2) & "/" & Mid(strCheckInfo(4), 4, 2) & "/" & Right(strCheckInfo(4), 4))
							// intTotalRecords = intTotalRecords + 1
							// End If
							// End If
						}
					}
					NextRecord:
					;
				}
				rsEntries.OmitNullsOnInsert = true;
				for (counter = 0; counter <= intTotalRecords - 1; counter++)
				{
					//Application.DoEvents();
					rsEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", hpiInfo[counter].CheckDate);
					rsEntries.Set_Fields("Description", "Payroll Import");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", hpiInfo[counter].Account);
					if (modValidateAccount.AccountValidate(Strings.Trim(hpiInfo[counter].Account)) == false)
					{
						strBadAccounts += Strings.Trim(hpiInfo[counter].Account) + "\r\n";
					}
					rsEntries.Set_Fields("Amount", hpiInfo[counter].Amount);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", hpiInfo[counter].CheckDate.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				frmWait.InstancePtr.Unload();
				if (intRecords > 0)
				{
					MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(lngJournal), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("No entries were found to import.", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				if (Strings.Trim(strBadAccounts) != "")
				{
					rptHancockCountyBadAccounts.InstancePtr.strBadAccts = strBadAccounts;
					frmReportViewer.InstancePtr.Init(rptHancockCountyBadAccounts.InstancePtr);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
