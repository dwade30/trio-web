//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCDDataEntry.
	/// </summary>
	partial class frmCDDataEntry : BaseForm
	{
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCComboBox cboBanks;
		public fecherFoundation.FCCheckBox chkCorrection;
		public fecherFoundation.FCTextBox txtPeriod;
		public fecherFoundation.FCComboBox cboJournal;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblNet;
		public fecherFoundation.FCLabel lblNetTotal;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCDDataEntry));
			this.fraJournalSave = new fecherFoundation.FCFrame();
			this.cboSaveJournal = new fecherFoundation.FCComboBox();
			this.cmdOKSave = new fecherFoundation.FCButton();
			this.cmdCancelSave = new fecherFoundation.FCButton();
			this.txtJournalDescription = new fecherFoundation.FCTextBox();
			this.lblSaveInstructions = new fecherFoundation.FCLabel();
			this.lblJournalSave = new fecherFoundation.FCLabel();
			this.lblJournalDescription = new fecherFoundation.FCLabel();
			this.fraBanks = new fecherFoundation.FCFrame();
			this.cboBanks = new fecherFoundation.FCComboBox();
			this.chkCorrection = new fecherFoundation.FCCheckBox();
			this.txtPeriod = new fecherFoundation.FCTextBox();
			this.cboJournal = new fecherFoundation.FCComboBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.lblNet = new fecherFoundation.FCLabel();
			this.lblNetTotal = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnMenuSave = new fecherFoundation.FCButton();
			this.btnProcessDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
			this.fraJournalSave.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
			this.fraBanks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMenuSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnMenuSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 625);
			this.BottomPanel.Size = new System.Drawing.Size(1026, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraJournalSave);
			this.ClientArea.Controls.Add(this.fraBanks);
			this.ClientArea.Controls.Add(this.chkCorrection);
			this.ClientArea.Controls.Add(this.txtPeriod);
			this.ClientArea.Controls.Add(this.cboJournal);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblJournal);
			this.ClientArea.Controls.Add(this.lblNet);
			this.ClientArea.Controls.Add(this.lblNetTotal);
			this.ClientArea.Size = new System.Drawing.Size(1046, 628);
			this.ClientArea.Controls.SetChildIndex(this.lblNetTotal, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblNet, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblJournal, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPeriod, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblExpense, 0);
			this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
			this.ClientArea.Controls.SetChildIndex(this.cboJournal, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtPeriod, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkCorrection, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraBanks, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraJournalSave, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnProcessDelete);
			this.TopPanel.Size = new System.Drawing.Size(1046, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnProcessDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(316, 30);
			this.HeaderText.Text = "Cash Disbursement Journal";
			// 
			// fraJournalSave
			// 
			this.fraJournalSave.BackColor = System.Drawing.Color.White;
			this.fraJournalSave.Controls.Add(this.cboSaveJournal);
			this.fraJournalSave.Controls.Add(this.cmdOKSave);
			this.fraJournalSave.Controls.Add(this.cmdCancelSave);
			this.fraJournalSave.Controls.Add(this.txtJournalDescription);
			this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
			this.fraJournalSave.Controls.Add(this.lblJournalSave);
			this.fraJournalSave.Controls.Add(this.lblJournalDescription);
			this.fraJournalSave.Location = new System.Drawing.Point(203, 100);
			this.fraJournalSave.Name = "fraJournalSave";
			this.fraJournalSave.Size = new System.Drawing.Size(804, 246);
			this.fraJournalSave.TabIndex = 7;
			this.fraJournalSave.Text = "Save Journal";
			this.fraJournalSave.Visible = false;
			// 
			// cboSaveJournal
			// 
			this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboSaveJournal.Location = new System.Drawing.Point(166, 66);
			this.cboSaveJournal.Name = "cboSaveJournal";
			this.cboSaveJournal.Size = new System.Drawing.Size(618, 40);
			this.cboSaveJournal.TabIndex = 11;
			this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
			this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
			this.cboSaveJournal.Leave += new System.EventHandler(this.FormControl_Leave);
			// 
			// cmdOKSave
			// 
			this.cmdOKSave.AppearanceKey = "actionButton";
			this.cmdOKSave.Location = new System.Drawing.Point(20, 186);
			this.cmdOKSave.Name = "cmdOKSave";
			this.cmdOKSave.Size = new System.Drawing.Size(70, 40);
			this.cmdOKSave.TabIndex = 9;
			this.cmdOKSave.Text = "OK";
			this.cmdOKSave.Leave += new System.EventHandler(this.FormControl_Leave);
			this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
			// 
			// cmdCancelSave
			// 
			this.cmdCancelSave.AppearanceKey = "actionButton";
			this.cmdCancelSave.Location = new System.Drawing.Point(110, 186);
			this.cmdCancelSave.Name = "cmdCancelSave";
			this.cmdCancelSave.Size = new System.Drawing.Size(70, 40);
			this.cmdCancelSave.TabIndex = 10;
			this.cmdCancelSave.Text = "Cancel";
			this.cmdCancelSave.Leave += new System.EventHandler(this.FormControl_Leave);
			this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
			// 
			// txtJournalDescription
			// 
			this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournalDescription.Location = new System.Drawing.Point(166, 126);
			this.txtJournalDescription.MaxLength = 100;
			this.txtJournalDescription.Name = "txtJournalDescription";
			this.txtJournalDescription.Size = new System.Drawing.Size(618, 40);
			this.txtJournalDescription.TabIndex = 8;
			this.txtJournalDescription.Leave += new System.EventHandler(this.FormControl_Leave);
			// 
			// lblSaveInstructions
			// 
			this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
			this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblSaveInstructions.Name = "lblSaveInstructions";
			this.lblSaveInstructions.Size = new System.Drawing.Size(782, 16);
			this.lblSaveInstructions.TabIndex = 14;
			this.lblSaveInstructions.Text = "PLEASE SELECT THE JOUNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION FO" +
    "R THE JOURNAL, AND CLICK THE OK BUTTON";
			this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblJournalSave
			// 
			this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
			this.lblJournalSave.Name = "lblJournalSave";
			this.lblJournalSave.Size = new System.Drawing.Size(63, 16);
			this.lblJournalSave.TabIndex = 13;
			this.lblJournalSave.Text = "JOURNAL";
			// 
			// lblJournalDescription
			// 
			this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblJournalDescription.Location = new System.Drawing.Point(20, 140);
			this.lblJournalDescription.Name = "lblJournalDescription";
			this.lblJournalDescription.Size = new System.Drawing.Size(88, 16);
			this.lblJournalDescription.TabIndex = 12;
			this.lblJournalDescription.Text = "DESCRIPTION";
			// 
			// fraBanks
			// 
			this.fraBanks.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.fraBanks.BackColor = System.Drawing.Color.White;
			this.fraBanks.Controls.Add(this.cboBanks);
			this.fraBanks.Location = new System.Drawing.Point(30, 535);
			this.fraBanks.Name = "fraBanks";
			this.fraBanks.Size = new System.Drawing.Size(380, 90);
			this.fraBanks.TabIndex = 17;
			this.fraBanks.Text = "Select A Bank";
			this.fraBanks.Visible = false;
			// 
			// cboBanks
			// 
			this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
			this.cboBanks.Items.AddRange(new object[] {
            "1 - Most Recently Run Report",
            "2 - Next Oldest Report",
            "3 - 3rd Oldest Report",
            "4 - 4th Oldest Report",
            "5 - 5th Oldest Report",
            "6 - 6th Oldest Report",
            "7 - 7th Oldest Report",
            "8 - 8th Oldest Report",
            "9 - 9th Oldest Report"});
			this.cboBanks.Location = new System.Drawing.Point(20, 30);
			this.cboBanks.Name = "cboBanks";
			this.cboBanks.Size = new System.Drawing.Size(340, 40);
			this.cboBanks.TabIndex = 18;
			this.cboBanks.Leave += new System.EventHandler(this.FormControl_Leave);
			// 
			// chkCorrection
			// 
			this.chkCorrection.Location = new System.Drawing.Point(806, 38);
			this.chkCorrection.Name = "chkCorrection";
			this.chkCorrection.Size = new System.Drawing.Size(88, 23);
			this.chkCorrection.TabIndex = 16;
			this.chkCorrection.Text = "Correction";
			this.ToolTip1.SetToolTip(this.chkCorrection, "Checking this option will cause all negative entries to be entered with an RCB op" +
        "tion of C.");
			this.chkCorrection.Leave += new System.EventHandler(this.FormControl_Leave);
			// 
			// txtPeriod
			// 
			this.txtPeriod.BackColor = System.Drawing.SystemColors.Window;
			this.txtPeriod.Location = new System.Drawing.Point(647, 30);
			this.txtPeriod.MaxLength = 2;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Size = new System.Drawing.Size(93, 40);
			this.txtPeriod.TabIndex = 2;
			this.txtPeriod.Enter += new System.EventHandler(this.txtPeriod_Enter);
			this.txtPeriod.Leave += new System.EventHandler(this.FormControl_Leave);
			this.txtPeriod.TextChanged += new System.EventHandler(this.txtPeriod_TextChanged);
			this.txtPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.txtPeriod_Validating);
			// 
			// cboJournal
			// 
			this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournal.Location = new System.Drawing.Point(176, 30);
			this.cboJournal.Name = "cboJournal";
			this.cboJournal.Size = new System.Drawing.Size(234, 40);
			this.cboJournal.TabIndex = 6;
			this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
			this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
			this.cboJournal.Leave += new System.EventHandler(this.FormControl_Leave);
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 10;
			this.vs1.Location = new System.Drawing.Point(30, 115);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 16;
			this.vs1.Size = new System.Drawing.Size(986, 504);
			this.vs1.StandardTab = false;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 1;
			this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Leave += new System.EventHandler(this.FormControl_Leave);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 90);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(638, 16);
			this.lblExpense.TabIndex = 15;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(455, 44);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(130, 16);
			this.lblPeriod.TabIndex = 5;
			this.lblPeriod.Text = "ACCOUNTING PERIOD";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(30, 44);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(79, 16);
			this.lblJournal.TabIndex = 4;
			this.lblJournal.Text = "JOURNAL NO";
			// 
			// lblNet
			// 
			this.lblNet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblNet.Location = new System.Drawing.Point(906, 90);
			this.lblNet.Name = "lblNet";
			this.lblNet.Size = new System.Drawing.Size(106, 16);
			this.lblNet.TabIndex = 3;
			this.lblNet.Text = "= NET ENTRY";
			// 
			// lblNetTotal
			// 
			this.lblNetTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblNetTotal.Location = new System.Drawing.Point(793, 90);
			this.lblNetTotal.Name = "lblNetTotal";
			this.lblNetTotal.Size = new System.Drawing.Size(110, 16);
			this.lblNetTotal.TabIndex = 1001;
			this.lblNetTotal.Text = "$0.00";
			this.lblNetTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnMenuSave
			// 
			this.btnMenuSave.AppearanceKey = "acceptButton";
			this.btnMenuSave.Location = new System.Drawing.Point(398, 30);
			this.btnMenuSave.Name = "btnMenuSave";
			this.btnMenuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnMenuSave.Size = new System.Drawing.Size(80, 48);
			this.btnMenuSave.Text = "Save";
			this.btnMenuSave.Leave += new System.EventHandler(this.FormControl_Leave);
			this.btnMenuSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// btnProcessDelete
			// 
			this.btnProcessDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnProcessDelete.Location = new System.Drawing.Point(923, 29);
			this.btnProcessDelete.Name = "btnProcessDelete";
			this.btnProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
			this.btnProcessDelete.Size = new System.Drawing.Size(93, 24);
			this.btnProcessDelete.TabIndex = 1;
			this.btnProcessDelete.Text = "Delete Entry";
			this.btnProcessDelete.Leave += new System.EventHandler(this.FormControl_Leave);
			this.btnProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// frmCDDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1046, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCDDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Cash Disbursement Journal";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCDDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmCDDataEntry_Activated);
			this.FormClosed += new Wisej.Web.FormClosedEventHandler(this.Form_Closed);
			this.Resize += new System.EventHandler(this.frmCDDataEntry_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCDDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCDDataEntry_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
			this.fraJournalSave.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
			this.fraBanks.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnMenuSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcessDelete)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnMenuSave;
		private FCButton btnProcessDelete;
	}
}