﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevenueSummarySetup.
	/// </summary>
	partial class frmRevenueSummarySetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbDeptDetail;
		public fecherFoundation.FCLabel lblDeptDetail;
		public fecherFoundation.FCComboBox cmbRegular;
		public fecherFoundation.FCLabel lblRegular;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCCheckBox chkShowZeroBalanceAccounts;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboSingleFund;
		public fecherFoundation.FCCheckBox chkCheckAccountRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCFrame fraPageBreaks;
		public fecherFoundation.FCCheckBox chkDepartment;
		public fecherFoundation.FCCheckBox chkDivision;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdCancelPrint;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRevenueSummarySetup));
            this.cmbAllAccounts = new fecherFoundation.FCComboBox();
            this.lblAllAccounts = new fecherFoundation.FCLabel();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbDeptDetail = new fecherFoundation.FCComboBox();
            this.lblDeptDetail = new fecherFoundation.FCLabel();
            this.cmbRegular = new fecherFoundation.FCComboBox();
            this.lblRegular = new fecherFoundation.FCLabel();
            this.chkShowZeroBalanceAccounts = new fecherFoundation.FCCheckBox();
            this.fraDeptRange = new fecherFoundation.FCFrame();
            this.cboSingleFund = new fecherFoundation.FCComboBox();
            this.chkCheckAccountRange = new fecherFoundation.FCCheckBox();
            this.cboBeginningDept = new fecherFoundation.FCComboBox();
            this.cboEndingDept = new fecherFoundation.FCComboBox();
            this.cboSingleDept = new fecherFoundation.FCComboBox();
            this.vsLowAccount = new fecherFoundation.FCGrid();
            this.vsHighAccount = new fecherFoundation.FCGrid();
            this.lblTo_3 = new fecherFoundation.FCLabel();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.lblTo_1 = new fecherFoundation.FCLabel();
            this.fraPageBreaks = new fecherFoundation.FCFrame();
            this.chkDepartment = new fecherFoundation.FCCheckBox();
            this.chkDivision = new fecherFoundation.FCCheckBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelPrint = new fecherFoundation.FCButton();
            this.fraMonths = new fecherFoundation.FCFrame();
            this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
            this.fraDeptRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).BeginInit();
            this.fraPageBreaks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDivision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
            this.fraMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPageBreaks);
            this.ClientArea.Controls.Add(this.chkShowZeroBalanceAccounts);
            this.ClientArea.Controls.Add(this.fraDeptRange);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.fraMonths);
            this.ClientArea.Controls.Add(this.cmbDeptDetail);
            this.ClientArea.Controls.Add(this.lblDeptDetail);
            this.ClientArea.Controls.Add(this.cmbRegular);
            this.ClientArea.Controls.Add(this.lblRegular);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCancelPrint);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdCancelPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(394, 30);
            this.HeaderText.Text = "Revenue Summary Search Criteria";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbAllAccounts
            // 
            this.cmbAllAccounts.Items.AddRange(new object[] {
            "All",
            "Account Range",
            "Single Department",
            "Department Range",
            "Single Fund"});
            this.cmbAllAccounts.Location = new System.Drawing.Point(253, 30);
            this.cmbAllAccounts.Name = "cmbAllAccounts";
            this.cmbAllAccounts.Size = new System.Drawing.Size(220, 40);
            this.cmbAllAccounts.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbAllAccounts, null);
            this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.optAllAccounts_CheckedChanged);
            // 
            // lblAllAccounts
            // 
            this.lblAllAccounts.AutoSize = true;
            this.lblAllAccounts.Location = new System.Drawing.Point(20, 44);
            this.lblAllAccounts.Name = "lblAllAccounts";
            this.lblAllAccounts.Size = new System.Drawing.Size(189, 15);
            this.lblAllAccounts.TabIndex = 4;
            this.lblAllAccounts.Text = "ACCOUNTS TO BE REPORTED";
            this.ToolTip1.SetToolTip(this.lblAllAccounts, null);
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Single Month",
            "Range of Months"});
            this.cmbRange.Location = new System.Drawing.Point(220, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(180, 40);
            this.cmbRange.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbRange, null);
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(145, 15);
            this.lblRange.TabIndex = 3;
            this.lblRange.Text = "MONTH(S) TO REPORT";
            this.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblRange, null);
            // 
            // cmbDeptDetail
            // 
            this.cmbDeptDetail.Items.AddRange(new object[] {
            "Department",
            "Division",
            "Revenue"});
            this.cmbDeptDetail.Location = new System.Drawing.Point(224, 375);
            this.cmbDeptDetail.Name = "cmbDeptDetail";
            this.cmbDeptDetail.Size = new System.Drawing.Size(226, 40);
            this.cmbDeptDetail.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.cmbDeptDetail, null);
            // 
            // lblDeptDetail
            // 
            this.lblDeptDetail.AutoSize = true;
            this.lblDeptDetail.Location = new System.Drawing.Point(30, 389);
            this.lblDeptDetail.Name = "lblDeptDetail";
            this.lblDeptDetail.Size = new System.Drawing.Size(148, 15);
            this.lblDeptDetail.TabIndex = 5;
            this.lblDeptDetail.Text = "LOWEST DETAIL LEVEL";
            this.lblDeptDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDeptDetail, null);
            // 
            // cmbRegular
            // 
            this.cmbRegular.Items.AddRange(new object[] {
            "Regular Totals",
            "Monthly Totals"});
            this.cmbRegular.Location = new System.Drawing.Point(224, 100);
            this.cmbRegular.Name = "cmbRegular";
            this.cmbRegular.Size = new System.Drawing.Size(226, 40);
            this.cmbRegular.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbRegular, null);
            // 
            // lblRegular
            // 
            this.lblRegular.AutoSize = true;
            this.lblRegular.Location = new System.Drawing.Point(30, 114);
            this.lblRegular.Name = "lblRegular";
            this.lblRegular.Size = new System.Drawing.Size(111, 15);
            this.lblRegular.TabIndex = 2;
            this.lblRegular.Text = "TOTALS FORMAT";
            this.lblRegular.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblRegular, null);
            // 
            // chkShowZeroBalanceAccounts
            // 
            this.chkShowZeroBalanceAccounts.Location = new System.Drawing.Point(30, 425);
            this.chkShowZeroBalanceAccounts.Name = "chkShowZeroBalanceAccounts";
            this.chkShowZeroBalanceAccounts.Size = new System.Drawing.Size(242, 27);
            this.chkShowZeroBalanceAccounts.TabIndex = 7;
            this.chkShowZeroBalanceAccounts.Text = "Show Zero Balance Accounts";
            this.ToolTip1.SetToolTip(this.chkShowZeroBalanceAccounts, "This setting shows specific fields - not all formatting options will be available" +
        ".");
            // 
            // fraDeptRange
            // 
            this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
            this.fraDeptRange.Controls.Add(this.cboSingleFund);
            this.fraDeptRange.Controls.Add(this.cmbAllAccounts);
            this.fraDeptRange.Controls.Add(this.lblAllAccounts);
            this.fraDeptRange.Controls.Add(this.chkCheckAccountRange);
            this.fraDeptRange.Controls.Add(this.cboBeginningDept);
            this.fraDeptRange.Controls.Add(this.cboEndingDept);
            this.fraDeptRange.Controls.Add(this.cboSingleDept);
            this.fraDeptRange.Controls.Add(this.vsLowAccount);
            this.fraDeptRange.Controls.Add(this.vsHighAccount);
            this.fraDeptRange.Controls.Add(this.lblTo_3);
            this.fraDeptRange.Controls.Add(this.lblTo_2);
            this.fraDeptRange.Controls.Add(this.lblTo_1);
            this.fraDeptRange.Location = new System.Drawing.Point(480, 171);
            this.fraDeptRange.Name = "fraDeptRange";
            this.fraDeptRange.Size = new System.Drawing.Size(493, 184);
            this.fraDeptRange.TabIndex = 9;
            this.fraDeptRange.Text = "Accounts To Be Reported";
            this.ToolTip1.SetToolTip(this.fraDeptRange, null);
            // 
            // cboSingleFund
            // 
            this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleFund.Location = new System.Drawing.Point(20, 124);
            this.cboSingleFund.Name = "cboSingleFund";
            this.cboSingleFund.Size = new System.Drawing.Size(200, 40);
            this.cboSingleFund.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cboSingleFund, null);
            this.cboSingleFund.Visible = false;
            this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
            // 
            // chkCheckAccountRange
            // 
            this.chkCheckAccountRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckAccountRange.Location = new System.Drawing.Point(20, 83);
            this.chkCheckAccountRange.Name = "chkCheckAccountRange";
            this.chkCheckAccountRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckAccountRange.TabIndex = 2;
            this.chkCheckAccountRange.Text = "Select at report time";
            this.ToolTip1.SetToolTip(this.chkCheckAccountRange, null);
            this.chkCheckAccountRange.Visible = false;
            this.chkCheckAccountRange.CheckedChanged += new System.EventHandler(this.chkCheckAccountRange_CheckedChanged);
            // 
            // cboBeginningDept
            // 
            this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningDept.Location = new System.Drawing.Point(20, 124);
            this.cboBeginningDept.Name = "cboBeginningDept";
            this.cboBeginningDept.Size = new System.Drawing.Size(200, 40);
            this.cboBeginningDept.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.cboBeginningDept, null);
            this.cboBeginningDept.Visible = false;
            this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
            // 
            // cboEndingDept
            // 
            this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingDept.Location = new System.Drawing.Point(273, 124);
            this.cboEndingDept.Name = "cboEndingDept";
            this.cboEndingDept.Size = new System.Drawing.Size(200, 40);
            this.cboEndingDept.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cboEndingDept, null);
            this.cboEndingDept.Visible = false;
            this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
            // 
            // cboSingleDept
            // 
            this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleDept.Location = new System.Drawing.Point(20, 124);
            this.cboSingleDept.Name = "cboSingleDept";
            this.cboSingleDept.Size = new System.Drawing.Size(200, 40);
            this.cboSingleDept.TabIndex = 24;
            this.ToolTip1.SetToolTip(this.cboSingleDept, null);
            this.cboSingleDept.Visible = false;
            this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
            // 
            // vsLowAccount
            // 
            this.vsLowAccount.Cols = 1;
            this.vsLowAccount.ColumnHeadersVisible = false;
            this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLowAccount.FixedCols = 0;
            this.vsLowAccount.FixedRows = 0;
            this.vsLowAccount.Location = new System.Drawing.Point(20, 124);
            this.vsLowAccount.Name = "vsLowAccount";
            this.vsLowAccount.ReadOnly = false;
            this.vsLowAccount.RowHeadersVisible = false;
            this.vsLowAccount.Rows = 1;
            this.vsLowAccount.Size = new System.Drawing.Size(200, 42);
            this.vsLowAccount.TabIndex = 37;
            this.ToolTip1.SetToolTip(this.vsLowAccount, null);
            this.vsLowAccount.Visible = false;
            // 
            // vsHighAccount
            // 
            this.vsHighAccount.Cols = 1;
            this.vsHighAccount.ColumnHeadersVisible = false;
            this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsHighAccount.FixedCols = 0;
            this.vsHighAccount.FixedRows = 0;
            this.vsHighAccount.Location = new System.Drawing.Point(273, 124);
            this.vsHighAccount.Name = "vsHighAccount";
            this.vsHighAccount.ReadOnly = false;
            this.vsHighAccount.RowHeadersVisible = false;
            this.vsHighAccount.Rows = 1;
            this.vsHighAccount.Size = new System.Drawing.Size(200, 42);
            this.vsHighAccount.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.vsHighAccount, null);
            this.vsHighAccount.Visible = false;
            // 
            // lblTo_3
            // 
            this.lblTo_3.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_3.Location = new System.Drawing.Point(238, 138);
            this.lblTo_3.Name = "lblTo_3";
            this.lblTo_3.Size = new System.Drawing.Size(21, 19);
            this.lblTo_3.TabIndex = 4;
            this.lblTo_3.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo_3, null);
            this.lblTo_3.Visible = false;
            // 
            // lblTo_2
            // 
            this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_2.Location = new System.Drawing.Point(238, 138);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(21, 19);
            this.lblTo_2.TabIndex = 35;
            this.lblTo_2.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo_2, null);
            this.lblTo_2.Visible = false;
            // 
            // lblTo_1
            // 
            this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_1.Location = new System.Drawing.Point(238, 138);
            this.lblTo_1.Name = "lblTo_1";
            this.lblTo_1.Size = new System.Drawing.Size(21, 19);
            this.lblTo_1.TabIndex = 34;
            this.lblTo_1.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo_1, null);
            this.lblTo_1.Visible = false;
            // 
            // fraPageBreaks
            // 
            this.fraPageBreaks.Controls.Add(this.chkDepartment);
            this.fraPageBreaks.Controls.Add(this.chkDivision);
            this.fraPageBreaks.Location = new System.Drawing.Point(480, 375);
            this.fraPageBreaks.Name = "fraPageBreaks";
            this.fraPageBreaks.Size = new System.Drawing.Size(161, 99);
            this.fraPageBreaks.TabIndex = 8;
            this.fraPageBreaks.Text = "Page Breaks";
            this.ToolTip1.SetToolTip(this.fraPageBreaks, null);
            // 
            // chkDepartment
            // 
            this.chkDepartment.Location = new System.Drawing.Point(20, 30);
            this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Size = new System.Drawing.Size(113, 27);
            this.chkDepartment.Text = "Department";
            this.ToolTip1.SetToolTip(this.chkDepartment, null);
            this.chkDepartment.CheckedChanged += new System.EventHandler(this.chkDepartment_CheckedChanged);
            // 
            // chkDivision
            // 
            this.chkDivision.Location = new System.Drawing.Point(20, 60);
            this.chkDivision.Name = "chkDivision";
            this.chkDivision.Size = new System.Drawing.Size(85, 27);
            this.chkDivision.TabIndex = 1;
            this.chkDivision.Text = "Division";
            this.ToolTip1.SetToolTip(this.chkDivision, null);
            this.chkDivision.CheckedChanged += new System.EventHandler(this.chkDivision_CheckedChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(224, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(201, 40);
            this.txtDescription.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // cmdCancelPrint
            // 
            this.cmdCancelPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCancelPrint.Location = new System.Drawing.Point(1050, 26);
            this.cmdCancelPrint.Name = "cmdCancelPrint";
            this.cmdCancelPrint.Size = new System.Drawing.Size(55, 24);
            this.cmdCancelPrint.TabIndex = 16;
            this.cmdCancelPrint.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancelPrint, null);
            this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
            // 
            // fraMonths
            // 
            this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
            this.fraMonths.Controls.Add(this.chkCheckDateRange);
            this.fraMonths.Controls.Add(this.cmbRange);
            this.fraMonths.Controls.Add(this.lblRange);
            this.fraMonths.Controls.Add(this.cboEndingMonth);
            this.fraMonths.Controls.Add(this.cboBeginningMonth);
            this.fraMonths.Controls.Add(this.cboSingleMonth);
            this.fraMonths.Controls.Add(this.lblTo_0);
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Location = new System.Drawing.Point(30, 171);
            this.fraMonths.Name = "fraMonths";
            this.fraMonths.Size = new System.Drawing.Size(420, 184);
            this.fraMonths.TabIndex = 4;
            this.fraMonths.Text = "Month(s) To Report";
            this.ToolTip1.SetToolTip(this.fraMonths, null);
            // 
            // chkCheckDateRange
            // 
            this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckDateRange.Location = new System.Drawing.Point(20, 83);
            this.chkCheckDateRange.Name = "chkCheckDateRange";
            this.chkCheckDateRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckDateRange.TabIndex = 2;
            this.chkCheckDateRange.Text = "Select at report time";
            this.ToolTip1.SetToolTip(this.chkCheckDateRange, null);
            this.chkCheckDateRange.Visible = false;
            this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(236, 124);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(164, 40);
            this.cboEndingMonth.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cboEndingMonth, null);
            this.cboEndingMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 124);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(164, 40);
            this.cboBeginningMonth.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cboBeginningMonth, null);
            this.cboBeginningMonth.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(20, 124);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(140, 40);
            this.cboSingleMonth.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.cboSingleMonth, null);
            this.cboSingleMonth.Visible = false;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_0.Location = new System.Drawing.Point(201, 138);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(21, 19);
            this.lblTo_0.TabIndex = 4;
            this.lblTo_0.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo_0, null);
            this.lblTo_0.Visible = false;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(124, 17);
            this.lblDescription.TabIndex = 10;
            this.lblDescription.Text = "SELECTION CRITERIA";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
            this.cmdProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmRevenueSummarySetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmRevenueSummarySetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Revenue Summary Search Criteria";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmRevenueSummarySetup_Load);
            this.Activated += new System.EventHandler(this.frmRevenueSummarySetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRevenueSummarySetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRevenueSummarySetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
            this.fraDeptRange.ResumeLayout(false);
            this.fraDeptRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).EndInit();
            this.fraPageBreaks.ResumeLayout(false);
            this.fraPageBreaks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDivision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
            this.fraMonths.ResumeLayout(false);
            this.fraMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
