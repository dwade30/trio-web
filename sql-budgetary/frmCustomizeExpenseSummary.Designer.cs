﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseSummary.
	/// </summary>
	partial class frmCustomizeExpenseSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbLandscape;
		public fecherFoundation.FCLabel lblLandscape;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbShortDescriptions;
		public fecherFoundation.FCLabel lblShortDescriptions;
		public fecherFoundation.FCComboBox cmbYTD;
		public fecherFoundation.FCLabel lblYTD;
		public fecherFoundation.FCComboBox cmbShowPending;
		public fecherFoundation.FCLabel lblShowPending;
		public fecherFoundation.FCComboBox cmbShowOutstandingEncumbrance;
		public fecherFoundation.FCLabel lblShowOutstanding;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraPreferences;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCFrame fraSelectedMonths;
		public fecherFoundation.FCCheckBox chkSelectedNet;
		public fecherFoundation.FCCheckBox chkSelectedDebitsCredits;
		public fecherFoundation.FCFrame fraBudget;
		public fecherFoundation.FCCheckBox chkNetBudget;
		public fecherFoundation.FCCheckBox chkPercent;
		public fecherFoundation.FCCheckBox chkBalance;
		public fecherFoundation.FCCheckBox chkAdjustments;
		public fecherFoundation.FCCheckBox chkBudget;
		public fecherFoundation.FCFrame fraYTD;
		public fecherFoundation.FCCheckBox chkYTDNet;
		public fecherFoundation.FCCheckBox chkYTDDebitsCredits;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCLabel lblCount;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeExpenseSummary));
            this.cmbLandscape = new fecherFoundation.FCComboBox();
            this.lblLandscape = new fecherFoundation.FCLabel();
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.cmbShortDescriptions = new fecherFoundation.FCComboBox();
            this.lblShortDescriptions = new fecherFoundation.FCLabel();
            this.cmbYTD = new fecherFoundation.FCComboBox();
            this.lblYTD = new fecherFoundation.FCLabel();
            this.cmbShowPending = new fecherFoundation.FCComboBox();
            this.lblShowPending = new fecherFoundation.FCLabel();
            this.cmbShowOutstandingEncumbrance = new fecherFoundation.FCComboBox();
            this.lblShowOutstanding = new fecherFoundation.FCLabel();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.fraPreferences = new fecherFoundation.FCFrame();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.fraSelectedMonths = new fecherFoundation.FCFrame();
            this.chkSelectedNet = new fecherFoundation.FCCheckBox();
            this.chkSelectedDebitsCredits = new fecherFoundation.FCCheckBox();
            this.fraBudget = new fecherFoundation.FCFrame();
            this.chkNetBudget = new fecherFoundation.FCCheckBox();
            this.chkPercent = new fecherFoundation.FCCheckBox();
            this.chkBalance = new fecherFoundation.FCCheckBox();
            this.chkAdjustments = new fecherFoundation.FCCheckBox();
            this.chkBudget = new fecherFoundation.FCCheckBox();
            this.fraYTD = new fecherFoundation.FCFrame();
            this.chkYTDNet = new fecherFoundation.FCCheckBox();
            this.chkYTDDebitsCredits = new fecherFoundation.FCCheckBox();
            this.lblNumber = new fecherFoundation.FCLabel();
            this.lblCount = new fecherFoundation.FCLabel();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).BeginInit();
            this.fraPreferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).BeginInit();
            this.fraSelectedMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBudget)).BeginInit();
            this.fraBudget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNetBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).BeginInit();
            this.fraYTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPreferences);
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(386, 30);
            this.HeaderText.Text = "Expense Summary Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbLandscape
            // 
            this.cmbLandscape.Items.AddRange(new object[] {
            "Landscape",
            "Portrait"});
            this.cmbLandscape.Location = new System.Drawing.Point(533, 30);
            this.cmbLandscape.Name = "cmbLandscape";
            this.cmbLandscape.Size = new System.Drawing.Size(180, 40);
            this.cmbLandscape.TabIndex = 2;
            this.cmbLandscape.Text = "Portrait";
            this.ToolTip1.SetToolTip(this.cmbLandscape, null);
            this.cmbLandscape.SelectedIndexChanged += new System.EventHandler(this.optLandscape_CheckedChanged);
            // 
            // lblLandscape
            // 
            this.lblLandscape.AutoSize = true;
            this.lblLandscape.Location = new System.Drawing.Point(383, 44);
            this.lblLandscape.Name = "lblLandscape";
            this.lblLandscape.Size = new System.Drawing.Size(101, 16);
            this.lblLandscape.TabIndex = 3;
            this.lblLandscape.Text = "PAPER WIDTH";
            this.lblLandscape.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLandscape, null);
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "Print",
            "Display"});
            this.cmbPrint.Location = new System.Drawing.Point(533, 90);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(180, 40);
            this.cmbPrint.TabIndex = 4;
            this.cmbPrint.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmbPrint, null);
            this.cmbPrint.SelectedIndexChanged += new System.EventHandler(this.optPrint_CheckedChanged);
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Location = new System.Drawing.Point(383, 104);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(95, 16);
            this.lblPrint.TabIndex = 5;
            this.lblPrint.Text = "REPORT USE";
            this.ToolTip1.SetToolTip(this.lblPrint, null);
            // 
            // cmbShortDescriptions
            // 
            this.cmbShortDescriptions.Items.AddRange(new object[] {
                "Long Descriptions",
            "Short Descriptions"});
            this.cmbShortDescriptions.Location = new System.Drawing.Point(170, 30);
            this.cmbShortDescriptions.Name = "cmbShortDescriptions";
            this.cmbShortDescriptions.Size = new System.Drawing.Size(180, 40);
            this.cmbShortDescriptions.TabIndex = 6;
            this.cmbShortDescriptions.Text = "Long Descriptions";
            this.ToolTip1.SetToolTip(this.cmbShortDescriptions, null);
            this.cmbShortDescriptions.SelectedIndexChanged += new System.EventHandler(this.optShortDescriptions_CheckedChanged);
            // 
            // lblShortDescriptions
            // 
            this.lblShortDescriptions.AutoSize = true;
            this.lblShortDescriptions.Location = new System.Drawing.Point(20, 44);
            this.lblShortDescriptions.Name = "lblShortDescriptions";
            this.lblShortDescriptions.Size = new System.Drawing.Size(108, 16);
            this.lblShortDescriptions.TabIndex = 7;
            this.lblShortDescriptions.Text = "DESCRIPTIONS";
            this.ToolTip1.SetToolTip(this.lblShortDescriptions, null);
            // 
            // cmbYTD
            // 
            this.cmbYTD.Items.AddRange(new object[] {
            "Total",
            "YTD"});
            this.cmbYTD.Location = new System.Drawing.Point(150, 71);
            this.cmbYTD.Name = "cmbYTD";
            this.cmbYTD.Size = new System.Drawing.Size(108, 40);
            this.cmbYTD.TabIndex = 17;
            this.cmbYTD.Text = "Total";
            this.ToolTip1.SetToolTip(this.cmbYTD, null);
            // 
            // lblYTD
            // 
            this.lblYTD.AutoSize = true;
            this.lblYTD.Location = new System.Drawing.Point(20, 85);
            this.lblYTD.Name = "lblYTD";
            this.lblYTD.Size = new System.Drawing.Size(131, 16);
            this.lblYTD.TabIndex = 18;
            this.lblYTD.Text = "ORIGINAL BUDGET";
            this.lblYTD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblYTD, null);
            // 
            // cmbShowPending
            // 
            this.cmbShowPending.Items.AddRange(new object[] {
            "Show Pending Activity Separately",
            "Include Pending Activity in YTD Account Information",
            "Do Not Include Pending Activity"});
            this.cmbShowPending.Location = new System.Drawing.Point(569, 149);
            this.cmbShowPending.Name = "cmbShowPending";
            this.cmbShowPending.Size = new System.Drawing.Size(430, 40);
            this.cmbShowPending.TabIndex = 31;
            this.cmbShowPending.Text = "Include Pending Activity in YTD Account Information";
            this.ToolTip1.SetToolTip(this.cmbShowPending, null);
            this.cmbShowPending.SelectedIndexChanged += new System.EventHandler(this.optShowPending_CheckedChanged);
            // 
            // lblShowPending
            // 
            this.lblShowPending.AutoSize = true;
            this.lblShowPending.Location = new System.Drawing.Point(320, 163);
            this.lblShowPending.Name = "lblShowPending";
            this.lblShowPending.Size = new System.Drawing.Size(133, 16);
            this.lblShowPending.TabIndex = 32;
            this.lblShowPending.Text = "PENDING ACTIVITY";
            this.lblShowPending.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblShowPending, null);
            // 
            // cmbShowOutstandingEncumbrance
            // 
            this.cmbShowOutstandingEncumbrance.Items.AddRange(new object[] {
            "Show Encumbrances Separately",
            "Include Encumbrances in Regular Account Information"});
            this.cmbShowOutstandingEncumbrance.Location = new System.Drawing.Point(569, 209);
            this.cmbShowOutstandingEncumbrance.Name = "cmbShowOutstandingEncumbrance";
            this.cmbShowOutstandingEncumbrance.Size = new System.Drawing.Size(430, 40);
            this.cmbShowOutstandingEncumbrance.TabIndex = 33;
            this.cmbShowOutstandingEncumbrance.Text = "Include Encumbrances in Regular Account Information";
            this.ToolTip1.SetToolTip(this.cmbShowOutstandingEncumbrance, null);
            this.cmbShowOutstandingEncumbrance.SelectedIndexChanged += new System.EventHandler(this.optShowOutstanding_CheckedChanged);
            // 
            // lblShowOutstanding
            // 
            this.lblShowOutstanding.AutoSize = true;
            this.lblShowOutstanding.Location = new System.Drawing.Point(320, 223);
            this.lblShowOutstanding.Name = "lblShowOutstanding";
            this.lblShowOutstanding.Size = new System.Drawing.Size(223, 16);
            this.lblShowOutstanding.TabIndex = 34;
            this.lblShowOutstanding.Text = "OUTSTANDING ENCUMBRANCES";
            this.ToolTip1.SetToolTip(this.lblShowOutstanding, null);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // fraPreferences
            // 
            this.fraPreferences.AppearanceKey = "groupBoxLeftBorder";
            this.fraPreferences.Controls.Add(this.cmbLandscape);
            this.fraPreferences.Controls.Add(this.lblLandscape);
            this.fraPreferences.Controls.Add(this.cmbPrint);
            this.fraPreferences.Controls.Add(this.lblPrint);
            this.fraPreferences.Controls.Add(this.cmbShortDescriptions);
            this.fraPreferences.Controls.Add(this.lblShortDescriptions);
            this.fraPreferences.Location = new System.Drawing.Point(30, 86);
            this.fraPreferences.Name = "fraPreferences";
            this.fraPreferences.Size = new System.Drawing.Size(730, 141);
            this.fraPreferences.TabIndex = 31;
            this.fraPreferences.Text = "General Preferences";
            this.ToolTip1.SetToolTip(this.fraPreferences, null);
            // 
            // fraInformation
            // 
            this.fraInformation.AppearanceKey = "groupBoxLeftBorder";
            this.fraInformation.Controls.Add(this.fraSelectedMonths);
            this.fraInformation.Controls.Add(this.cmbShowPending);
            this.fraInformation.Controls.Add(this.lblShowPending);
            this.fraInformation.Controls.Add(this.cmbShowOutstandingEncumbrance);
            this.fraInformation.Controls.Add(this.lblShowOutstanding);
            this.fraInformation.Controls.Add(this.fraBudget);
            this.fraInformation.Controls.Add(this.fraYTD);
            this.fraInformation.Controls.Add(this.lblNumber);
            this.fraInformation.Controls.Add(this.lblCount);
            this.fraInformation.Location = new System.Drawing.Point(30, 237);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(1061, 425);
            this.fraInformation.TabIndex = 25;
            this.fraInformation.Text = "Information To Be Reported";
            this.ToolTip1.SetToolTip(this.fraInformation, null);
            // 
            // fraSelectedMonths
            // 
            this.fraSelectedMonths.Controls.Add(this.chkSelectedNet);
            this.fraSelectedMonths.Controls.Add(this.chkSelectedDebitsCredits);
            this.fraSelectedMonths.Location = new System.Drawing.Point(384, 30);
            this.fraSelectedMonths.Name = "fraSelectedMonths";
            this.fraSelectedMonths.Size = new System.Drawing.Size(336, 99);
            this.fraSelectedMonths.TabIndex = 30;
            this.fraSelectedMonths.FormatCaption = false;
            this.fraSelectedMonths.Text = "Selected Month(s) Account Information";
            this.ToolTip1.SetToolTip(this.fraSelectedMonths, null);
            // 
            // chkSelectedNet
            // 
            this.chkSelectedNet.Location = new System.Drawing.Point(20, 62);
            this.chkSelectedNet.Name = "chkSelectedNet";
            this.chkSelectedNet.Size = new System.Drawing.Size(80, 23);
            this.chkSelectedNet.TabIndex = 13;
            this.chkSelectedNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkSelectedNet, null);
            this.chkSelectedNet.CheckedChanged += new System.EventHandler(this.chkSelectedNet_CheckedChanged);
            // 
            // chkSelectedDebitsCredits
            // 
            this.chkSelectedDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkSelectedDebitsCredits.Name = "chkSelectedDebitsCredits";
            this.chkSelectedDebitsCredits.Size = new System.Drawing.Size(117, 23);
            this.chkSelectedDebitsCredits.TabIndex = 12;
            this.chkSelectedDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkSelectedDebitsCredits, "You need to have 2 items available to show this item");
            this.chkSelectedDebitsCredits.CheckedChanged += new System.EventHandler(this.chkSelectedDebitsCredits_CheckedChanged);
            // 
            // fraBudget
            // 
            this.fraBudget.Controls.Add(this.chkNetBudget);
            this.fraBudget.Controls.Add(this.cmbYTD);
            this.fraBudget.Controls.Add(this.lblYTD);
            this.fraBudget.Controls.Add(this.chkPercent);
            this.fraBudget.Controls.Add(this.chkBalance);
            this.fraBudget.Controls.Add(this.chkAdjustments);
            this.fraBudget.Controls.Add(this.chkBudget);
            this.fraBudget.Location = new System.Drawing.Point(20, 149);
            this.fraBudget.Name = "fraBudget";
            this.fraBudget.Size = new System.Drawing.Size(278, 251);
            this.fraBudget.TabIndex = 29;
            this.fraBudget.Text = "Budget Information";
            this.ToolTip1.SetToolTip(this.fraBudget, null);
            // 
            // chkNetBudget
            // 
            this.chkNetBudget.Location = new System.Drawing.Point(20, 155);
            this.chkNetBudget.Name = "chkNetBudget";
            this.chkNetBudget.Size = new System.Drawing.Size(93, 23);
            this.chkNetBudget.TabIndex = 16;
            this.chkNetBudget.Text = "Net Budget";
            this.ToolTip1.SetToolTip(this.chkNetBudget, null);
            this.chkNetBudget.CheckedChanged += new System.EventHandler(this.chkNetBudget_CheckedChanged);
            // 
            // chkPercent
            // 
            this.chkPercent.Location = new System.Drawing.Point(20, 215);
            this.chkPercent.Name = "chkPercent";
            this.chkPercent.Size = new System.Drawing.Size(111, 23);
            this.chkPercent.TabIndex = 18;
            this.chkPercent.Text = "Percent Spent";
            this.ToolTip1.SetToolTip(this.chkPercent, null);
            this.chkPercent.CheckedChanged += new System.EventHandler(this.chkPercent_CheckedChanged);
            // 
            // chkBalance
            // 
            this.chkBalance.Location = new System.Drawing.Point(20, 185);
            this.chkBalance.Name = "chkBalance";
            this.chkBalance.Size = new System.Drawing.Size(154, 23);
            this.chkBalance.TabIndex = 17;
            this.chkBalance.Text = "Unexpended Balance";
            this.ToolTip1.SetToolTip(this.chkBalance, null);
            this.chkBalance.CheckedChanged += new System.EventHandler(this.chkBalance_CheckedChanged);
            // 
            // chkAdjustments
            // 
            this.chkAdjustments.Location = new System.Drawing.Point(20, 125);
            this.chkAdjustments.Name = "chkAdjustments";
            this.chkAdjustments.Size = new System.Drawing.Size(146, 23);
            this.chkAdjustments.TabIndex = 15;
            this.chkAdjustments.Text = "Budget Adjustments";
            this.ToolTip1.SetToolTip(this.chkAdjustments, null);
            this.chkAdjustments.CheckedChanged += new System.EventHandler(this.chkAdjustments_CheckedChanged);
            // 
            // chkBudget
            // 
            this.chkBudget.Location = new System.Drawing.Point(20, 30);
            this.chkBudget.Name = "chkBudget";
            this.chkBudget.Size = new System.Drawing.Size(118, 23);
            this.chkBudget.TabIndex = 14;
            this.chkBudget.Text = "Original Budget";
            this.ToolTip1.SetToolTip(this.chkBudget, null);
            this.chkBudget.CheckedChanged += new System.EventHandler(this.chkBudget_CheckedChanged);
            // 
            // fraYTD
            // 
            this.fraYTD.Controls.Add(this.chkYTDNet);
            this.fraYTD.Controls.Add(this.chkYTDDebitsCredits);
            this.fraYTD.Location = new System.Drawing.Point(144, 30);
            this.fraYTD.Name = "fraYTD";
            this.fraYTD.Size = new System.Drawing.Size(220, 99);
            this.fraYTD.TabIndex = 28;
            this.fraYTD.FormatCaption = false;
            this.fraYTD.Text = "YTD Account Information";
            this.ToolTip1.SetToolTip(this.fraYTD, null);
            // 
            // chkYTDNet
            // 
            this.chkYTDNet.Location = new System.Drawing.Point(20, 62);
            this.chkYTDNet.Name = "chkYTDNet";
            this.chkYTDNet.Size = new System.Drawing.Size(80, 23);
            this.chkYTDNet.TabIndex = 11;
            this.chkYTDNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkYTDNet, null);
            this.chkYTDNet.CheckedChanged += new System.EventHandler(this.chkYTDNet_CheckedChanged);
            // 
            // chkYTDDebitsCredits
            // 
            this.chkYTDDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkYTDDebitsCredits.Name = "chkYTDDebitsCredits";
            this.chkYTDDebitsCredits.Size = new System.Drawing.Size(117, 23);
            this.chkYTDDebitsCredits.TabIndex = 10;
            this.chkYTDDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkYTDDebitsCredits, "You need to have 2 items available to show this item");
            this.chkYTDDebitsCredits.CheckedChanged += new System.EventHandler(this.chkYTDDebitsCredits_CheckedChanged);
            // 
            // lblNumber
            // 
            this.lblNumber.BorderStyle = 1;
            this.lblNumber.Location = new System.Drawing.Point(20, 87);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(46, 22);
            this.lblNumber.TabIndex = 35;
            this.lblNumber.Text = "5";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblNumber, null);
            // 
            // lblCount
            // 
            this.lblCount.Location = new System.Drawing.Point(20, 30);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(116, 52);
            this.lblCount.TabIndex = 34;
            this.lblCount.Text = "NUMBER OF ITEMS YOU MAY ADD TO THIS REPORT";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblCount, null);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(200, 29);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(201, 40);
            this.txtDescription.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 39);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(159, 18);
            this.lblDescription.TabIndex = 24;
            this.lblDescription.Text = "FORMAT DESCRIPTION";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(499, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
            this.cmdProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmCustomizeExpenseSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomizeExpenseSummary";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Expense Summary Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeExpenseSummary_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeExpenseSummary_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeExpenseSummary_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeExpenseSummary_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).EndInit();
            this.fraPreferences.ResumeLayout(false);
            this.fraPreferences.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            this.fraInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).EndInit();
            this.fraSelectedMonths.ResumeLayout(false);
            this.fraSelectedMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBudget)).EndInit();
            this.fraBudget.ResumeLayout(false);
            this.fraBudget.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkNetBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).EndInit();
            this.fraYTD.ResumeLayout(false);
            this.fraYTD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
