﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpObjDetailSetup.
	/// </summary>
	public partial class frmExpObjDetailSetup : BaseForm
	{
		public frmExpObjDetailSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbTrans.SelectedIndex = 0;
			cmbRange.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExpObjDetailSetup InstancePtr
		{
			get
			{
				return (frmExpObjDetailSetup)Sys.GetInstance(typeof(frmExpObjDetailSetup));
			}
		}

		protected frmExpObjDetailSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cboExpense_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetObjectCombos();
		}

		private void frmExpObjDetailSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmExpObjDetailSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExpObjDetailSetup.FillStyle	= 0;
			//frmExpObjDetailSetup.ScaleWidth	= 9045;
			//frmExpObjDetailSetup.ScaleHeight	= 7365;
			//frmExpObjDetailSetup.LinkTopic	= "Form2";
			//frmExpObjDetailSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE IsNull(Object, 0) = 0 ORDER BY Expense");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboExpense.AddItem(rsInfo.Get_Fields_String("Expense") + " - " + rsInfo.Get_Fields_String("ShortDescription"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				cboExpense.SelectedIndex = 0;
			}
			if (modAccountTitle.Statics.ObjFlag)
			{
				//optSingleObject.Enabled = false;
				if (cmbRangeObject.Items.Contains("Range of Objects"))
				{
					cmbRangeObject.Items.Remove("Range of Objects");
				}
				//optRangeObject.Enabled = false;
				if (cmbRangeObject.Items.Contains("Single Object"))
				{
					cmbRangeObject.Items.Remove("Single Object");
				}
			}
			else
			{
				SetObjectCombos();
			}
			cboDeptDivPriority.SelectedIndex = 0;
			cboJournalNumberPriority.SelectedIndex = 1;
			cboPeriodPriority.SelectedIndex = 2;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmExpObjDetailSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (VerifyEntries())
			{
				frmReportViewer.InstancePtr.Init(rptExpObjDetail.InstancePtr);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (VerifyEntries())
			{
				rptExpObjDetail.InstancePtr.PrintReport();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SetObjectCombos()
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboBeginningObject.Clear();
			cboEndingObject.Clear();
			cboSingleObject.Clear();
			rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Strings.Left(cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "' AND IsNull(Object, 0) <> 0 ORDER BY Object");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboBeginningObject.AddItem(rs.Get_Fields_String("Object") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingObject.AddItem(rs.Get_Fields_String("Object") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleObject.AddItem(rs.Get_Fields_String("Object") + " - " + rs.Get_Fields_String("ShortDescription"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
                cmbRangeObject.SelectedIndex = 0;
            }

            //if (cmbRangeObject.Items.Count > 0)
            //{
            //    cmbRangeObject.SelectedIndex = 0;
            //}
        }

		private void optAllMonths_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.SelectedIndex = 0;
			cboEndingMonth.SelectedIndex = 0;
			cboSingleMonth.SelectedIndex = 0;
			cboBeginningMonth.Visible = false;
			cboEndingMonth.Visible = false;
			cboSingleMonth.Visible = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			txtSingleDate.Text = "";
			txtStartDate.Visible = false;
			txtEndDate.Visible = false;
			txtSingleDate.Visible = false;
			lblDateTo.Visible = false;
		}

		private void optAllObjects_CheckedChanged(object sender, System.EventArgs e)
		{
            if (cboBeginningObject.Items.Count > 0)
            {
                cboBeginningObject.SelectedIndex = 0;
            }

            if (cboEndingObject.Items.Count > 0)
            {
                cboEndingObject.SelectedIndex = 0;
            }

            if (cboSingleObject.Items.Count > 0)
            {
                cboSingleObject.SelectedIndex = 0;
            }

            cboBeginningObject.Visible = false;
			cboEndingObject.Visible = false;
			cboSingleObject.Visible = false;
			lblTo.Visible = false;
		}

		private void optPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			//optSingle.Text = "Single Month";
			if (!cmbRange.Items.Contains("Single Month"))
			{
				cmbRange.Items.Insert(1, "Single Month");
			}
			int index = cmbRange.Items.IndexOf("Single Date");
			if (index != -1)
			{
				cmbRange.Items.RemoveAt(index);
			}
			//optRange.Text = "Range of Months";
			if (!cmbRange.Items.Contains("Range of Months"))
			{
				cmbRange.Items.Insert(1, "Range of Months");
			}
			index = cmbRange.Items.IndexOf("Range of Dates");
			if (index != -1)
			{
				cmbRange.Items.RemoveAt(index);
			}
			cmbRange.SelectedIndex = 0;
		}

		private void optPosted_CheckedChanged(object sender, System.EventArgs e)
		{
			//optSingle.Text = "Single Date";
			if (!cmbRange.Items.Contains("Single Date"))
			{
				cmbRange.Items.Insert(1, "Single Date");
			}
			int index = cmbRange.Items.IndexOf("Single Month");
			if (index != -1)
			{
				cmbRange.Items.RemoveAt(index);
			}
			//optRange.Text = "Range of Dates";
			if (!cmbRange.Items.Contains("Range of Dates"))
			{
				cmbRange.Items.Insert(1, "Range of Dates");
			}
			index = cmbRange.Items.IndexOf("Range of Months");
			if (index != -1)
			{
				cmbRange.Items.RemoveAt(index);
			}
			cmbRange.SelectedIndex = 0;
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbTrans.SelectedIndex == 0)
			{
				cboBeginningMonth.SelectedIndex = 0;
				cboEndingMonth.SelectedIndex = 0;
				cboBeginningMonth.Visible = true;
				cboEndingMonth.Visible = true;
				cboSingleMonth.Visible = false;
				lblDateTo.Visible = true;
				txtStartDate.Visible = false;
				txtEndDate.Visible = false;
				txtSingleDate.Visible = false;
			}
			else
			{
				txtStartDate.Text = "";
				txtEndDate.Text = "";
				txtSingleDate.Text = "";
				txtStartDate.Visible = true;
				txtEndDate.Visible = true;
				txtSingleDate.Visible = false;
				lblDateTo.Visible = true;
				cboBeginningMonth.Visible = false;
				cboEndingMonth.Visible = false;
				cboSingleMonth.Visible = false;
			}
		}

		private void optRangeObject_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningObject.SelectedIndex = 0;
			cboEndingObject.SelectedIndex = 0;
			cboBeginningObject.Visible = true;
			cboEndingObject.Visible = true;
			cboSingleObject.Visible = false;
			lblTo.Visible = true;
		}

		private void optSingle_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbTrans.SelectedIndex == 0)
			{
				cboBeginningMonth.SelectedIndex = 0;
				cboEndingMonth.SelectedIndex = 0;
				cboBeginningMonth.Visible = false;
				cboEndingMonth.Visible = false;
				cboSingleMonth.Visible = true;
				lblDateTo.Visible = false;
				//FC:FINAL:AAKV disabling the options
				txtStartDate.Visible = false;
				txtEndDate.Visible = false;
				txtSingleDate.Visible = false;
			}
			else
			{
				txtStartDate.Text = "";
				txtEndDate.Text = "";
				txtSingleDate.Text = "";
				txtStartDate.Visible = false;
				txtEndDate.Visible = false;
				txtSingleDate.Visible = true;
				lblDateTo.Visible = false;
				//FC:FINAL:AAKV disabling the options
				cboBeginningMonth.Visible = false;
				cboEndingMonth.Visible = false;
				cboSingleMonth.Visible = false;
			}
		}

		private void optSingleObject_CheckedChanged(object sender, System.EventArgs e)
		{
			cboSingleObject.SelectedIndex = 0;
			cboBeginningObject.Visible = false;
			cboEndingObject.Visible = false;
			cboSingleObject.Visible = true;
			lblTo.Visible = false;
		}

        private void optTrans_CheckedChanged(object sender, System.EventArgs e)
        {
            //FC:FINAL:AAKV combo items for period
            if (cmbTrans.Text.Equals("Period"))
            {
                //optSingle.Text = "Single Month";
                //optRange.Text = "Range of Months";
                this.cmbRange.Items.Clear();
                this.cmbRange.Items.AddRange(new object[] {
                "All",
                "Single Month",
                "Range of Months"
            });
                cmbRange.SelectedIndex = 0;
            }
            else
            {
                //optSingle.Text = "Single Date";
                //optRange.Text = "Range of Dates";
                this.cmbRange.Items.Clear();
                this.cmbRange.Items.AddRange(new object[] {
                "All",
                "Single Date",
                "Range of Dates"
            });
                cmbRange.SelectedIndex = 0;
            }
        }

		private bool VerifyEntries()
		{
			bool VerifyEntries = false;
			clsDRWrapper rsData = new clsDRWrapper();
			string strSQL = "";
			string strPeriodCheck;
			VerifyEntries = true;
			if (cboExpense.SelectedIndex < 0)
			{
				MessageBox.Show("You must enter an expense before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				VerifyEntries = false;
				return VerifyEntries;
			}
			if (cmbRangeObject.Text == "Single Object")
			{
				if (cboSingleObject.SelectedIndex < 0)
				{
					MessageBox.Show("You must enter an object before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VerifyEntries = false;
					return VerifyEntries;
				}
			}
			else if (cmbRangeObject.Text == "Range of Objects")
			{
				if (cboBeginningObject.SelectedIndex < 0)
				{
					MessageBox.Show("You must enter an object before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VerifyEntries = false;
					return VerifyEntries;
				}
				if (cboEndingObject.SelectedIndex < 0)
				{
					MessageBox.Show("You must enter an object before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VerifyEntries = false;
					return VerifyEntries;
				}
				if (cboBeginningObject.SelectedIndex > cboEndingObject.SelectedIndex)
				{
					MessageBox.Show("Your beginning object must not be greater than your ending object.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					VerifyEntries = false;
					return VerifyEntries;
				}
			}
			if (cmbRange.Text.Contains("Single"))
			{
				if (cmbTrans.SelectedIndex == 0)
				{
					if (cboSingleMonth.SelectedIndex < 0)
					{
						MessageBox.Show("You must enter a period before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
				}
				else
				{
					if (!Information.IsDate(txtSingleDate.Text))
					{
						MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
				}
			}
			else if (cmbRange.Text.Contains("Single"))
			{
				if (cmbTrans.SelectedIndex == 0)
				{
					if (cboBeginningMonth.SelectedIndex < 0)
					{
						MessageBox.Show("You must enter a period before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
					if (cboEndingMonth.SelectedIndex < 0)
					{
						MessageBox.Show("You must enter a period before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
					if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
					{
						MessageBox.Show("Your beginning period must not be greater than your ending period.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
				}
				else
				{
					if (!Information.IsDate(txtStartDate.Text))
					{
						MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
					if (!Information.IsDate(txtEndDate.Text))
					{
						MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
					if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
					{
						MessageBox.Show("Your beginning date must not be greater than your ending date.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
						VerifyEntries = false;
						return VerifyEntries;
					}
				}
			}
			if ((cboJournalNumberPriority.SelectedIndex == cboDeptDivPriority.SelectedIndex) || (cboJournalNumberPriority.SelectedIndex == cboPeriodPriority.SelectedIndex) || (cboPeriodPriority.SelectedIndex == cboDeptDivPriority.SelectedIndex))
			{
				MessageBox.Show("You may not give different items the same order by priority.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				VerifyEntries = false;
				return VerifyEntries;
			}
			if (cmbRangeObject.Text == "All")
			{
				strSQL = "(Expense = '" + Strings.Left(cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "')";
			}
			else if (cmbRangeObject.Text == "Single Object")
			{
				strSQL = "(Expense = '" + Strings.Left(cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "' AND Object = '" + Strings.Left(cboSingleObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "')";
			}
			else
			{
				strSQL = "(Expense = '" + Strings.Left(cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "' AND (Object >= '" + Strings.Left(cboBeginningObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "' AND Object <= '" + Strings.Left(cboEndingObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "'))";
			}
			strPeriodCheck = "AND";
			if (cmbRange.Text == "All")
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			else if (cmbRange.Text.Contains("Single"))
			{
				if (cmbTrans.SelectedIndex == 0)
				{
					strSQL += " AND (Period = " + FCConvert.ToString(cboSingleMonth.SelectedIndex + 1) + ")";
				}
				else
				{
					if (cmbTrans.SelectedIndex == 1)
					{
						strSQL += " AND (PostedDate = '" + txtSingleDate.Text + "')";
					}
					else
					{
						strSQL += " AND (TransDate = '" + txtSingleDate.Text + "')";
					}
				}
			}
			else
			{
				if (cmbTrans.SelectedIndex == 0)
				{
					if (cboBeginningMonth.SelectedIndex > cboEndingMonth.SelectedIndex)
					{
						strPeriodCheck = "OR";
					}
					strSQL += " AND (Period >= " + FCConvert.ToString(cboBeginningMonth.SelectedIndex + 1) + " " + strPeriodCheck + " " + " Period <= " + FCConvert.ToString(cboEndingMonth.SelectedIndex + 1) + ")";
				}
				else
				{
					if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
					{
						strPeriodCheck = "OR";
					}
					if (cmbTrans.SelectedIndex == 1)
					{
						strSQL += " AND (PostedDate >= '" + txtStartDate.Text + "' " + strPeriodCheck + " PostedDate <= '" + txtEndDate.Text + "')";
					}
					else
					{
						strSQL += " AND (TransDate >= '" + txtStartDate.Text + "' " + strPeriodCheck + " TransDate <= '" + txtEndDate.Text + "')";
					}
				}
			}
			rsData.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE " + strSQL);
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("There was no data found.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				VerifyEntries = false;
				return VerifyEntries;
			}
			return VerifyEntries;
		}

		private void cmdPreview_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(sender, e);
		}

		private void cmbRangeObject_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbRangeObject.Text == "Range of Objects")
			{
				optRangeObject_CheckedChanged(sender, e);
			}
			else if (cmbRangeObject.Text == "Single Object")
			{
				optSingleObject_CheckedChanged(sender, e);
			}
			else if (cmbRangeObject.Text == "All")
			{
				optAllObjects_CheckedChanged(sender, e);
			}
		}

		private void cmbRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FC:FINAL:CHN: Incorrect indexes using.
            if (cmbRange.SelectedIndex == 1)
            {
                optSingle_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 2)
            {
                optRange_CheckedChanged(sender, e);
            }
			else if (cmbRange.SelectedIndex == 0)
			{
				optAllMonths_CheckedChanged(sender, e);
			}
		}
	}
}
