﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintIndividualChecks.
	/// </summary>
	partial class frmPrintIndividualChecks : BaseForm
	{
		public fecherFoundation.FCComboBox cmbInitial;
		public fecherFoundation.FCLabel lblInitial;
		public fecherFoundation.FCFrame fraPayDate;
		public fecherFoundation.FCFrame fraReprint;
		public fecherFoundation.FCGrid vsChecks;
		public fecherFoundation.FCFrame fraVendors;
		public fecherFoundation.FCGrid vsVendors;
		public fecherFoundation.FCFrame fraJournals;
		public fecherFoundation.FCGrid vsJournals;
		public fecherFoundation.FCTextBox txtCheckMessage;
		public Global.T2KOverTypeBox txtCheck;
		public fecherFoundation.FCLabel lblCheckNumber;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintIndividualChecks));
			this.cmbInitial = new fecherFoundation.FCComboBox();
			this.lblInitial = new fecherFoundation.FCLabel();
			this.fraPayDate = new fecherFoundation.FCFrame();
			this.fraReprint = new fecherFoundation.FCFrame();
			this.vsChecks = new fecherFoundation.FCGrid();
			this.fraVendors = new fecherFoundation.FCFrame();
			this.vsVendors = new fecherFoundation.FCGrid();
			this.fraJournals = new fecherFoundation.FCFrame();
			this.vsJournals = new fecherFoundation.FCGrid();
			this.txtCheckMessage = new fecherFoundation.FCTextBox();
			this.txtCheck = new Global.T2KOverTypeBox();
			this.lblCheckNumber = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.btnFileProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).BeginInit();
			this.fraPayDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReprint)).BeginInit();
			this.fraReprint.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsChecks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVendors)).BeginInit();
			this.fraVendors.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsVendors)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournals)).BeginInit();
			this.fraJournals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 592);
			this.BottomPanel.Size = new System.Drawing.Size(1088, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPayDate);
			this.ClientArea.Controls.Add(this.fraReprint);
			this.ClientArea.Controls.Add(this.fraVendors);
			this.ClientArea.Controls.Add(this.fraJournals);
			this.ClientArea.Controls.Add(this.txtCheckMessage);
			this.ClientArea.Controls.Add(this.txtCheck);
			this.ClientArea.Controls.Add(this.lblCheckNumber);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1088, 532);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSave);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(263, 30);
			this.HeaderText.Text = "Print Individual Checks";
			// 
			// cmbInitial
			// 
			this.cmbInitial.Items.AddRange(new object[] {
            "Initial Print",
            "Reprint Check"});
			this.cmbInitial.Location = new System.Drawing.Point(158, 30);
			this.cmbInitial.Name = "cmbInitial";
			this.cmbInitial.Size = new System.Drawing.Size(362, 40);
			this.cmbInitial.TabIndex = 2;
			// 
			// lblInitial
			// 
			this.lblInitial.Location = new System.Drawing.Point(20, 44);
			this.lblInitial.Name = "lblInitial";
			this.lblInitial.Size = new System.Drawing.Size(68, 15);
			this.lblInitial.TabIndex = 3;
			this.lblInitial.Text = "PRINT TYPE";
			// 
			// fraPayDate
			// 
			this.fraPayDate.Controls.Add(this.cmbInitial);
			this.fraPayDate.Controls.Add(this.lblInitial);
			this.fraPayDate.Location = new System.Drawing.Point(30, 130);
			this.fraPayDate.Name = "fraPayDate";
			this.fraPayDate.Size = new System.Drawing.Size(540, 90);
			this.fraPayDate.Text = "Check Type";
			// 
			// fraReprint
			// 
			this.fraReprint.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraReprint.AppearanceKey = "groupBoxNoBorders";
			this.fraReprint.Controls.Add(this.vsChecks);
			this.fraReprint.Location = new System.Drawing.Point(30, 340);
			this.fraReprint.Name = "fraReprint";
			this.fraReprint.Size = new System.Drawing.Size(899, 200);
			this.fraReprint.TabIndex = 15;
			this.fraReprint.Text = "Reprint Checks";
			this.fraReprint.Visible = false;
			// 
			// vsChecks
			// 
			this.vsChecks.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsChecks.Cols = 5;
			this.vsChecks.FixedCols = 0;
			this.vsChecks.Location = new System.Drawing.Point(0, 30);
			this.vsChecks.Name = "vsChecks";
			this.vsChecks.RowHeadersVisible = false;
			this.vsChecks.Rows = 50;
			this.vsChecks.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.vsChecks.Size = new System.Drawing.Size(899, 170);
			this.vsChecks.TabIndex = 16;
			// 
			// fraVendors
			// 
			this.fraVendors.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraVendors.AppearanceKey = "groupBoxNoBorders";
			this.fraVendors.Controls.Add(this.vsVendors);
			this.fraVendors.Location = new System.Drawing.Point(30, 340);
			this.fraVendors.Name = "fraVendors";
			this.fraVendors.Size = new System.Drawing.Size(899, 200);
			this.fraVendors.TabIndex = 13;
			this.fraVendors.Text = "Vendors";
			this.fraVendors.Visible = false;
			// 
			// vsVendors
			// 
			this.vsVendors.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsVendors.Cols = 5;
			this.vsVendors.FixedCols = 0;
			this.vsVendors.Location = new System.Drawing.Point(0, 30);
			this.vsVendors.Name = "vsVendors";
			this.vsVendors.RowHeadersVisible = false;
			this.vsVendors.Rows = 50;
			this.vsVendors.Size = new System.Drawing.Size(899, 170);
			this.vsVendors.TabIndex = 14;
			// 
			// fraJournals
			// 
			this.fraJournals.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraJournals.AppearanceKey = "groupBoxNoBorders";
			this.fraJournals.Controls.Add(this.vsJournals);
			this.fraJournals.FormatCaption = false;
			this.fraJournals.Location = new System.Drawing.Point(30, 130);
			this.fraJournals.Name = "fraJournals";
			this.fraJournals.Size = new System.Drawing.Size(899, 200);
			this.fraJournals.TabIndex = 11;
			this.fraJournals.Text = "AP Journals";
			this.fraJournals.Visible = false;
			// 
			// vsJournals
			// 
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.Cols = 7;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.Location = new System.Drawing.Point(0, 30);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.Rows = 50;
			this.vsJournals.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.vsJournals.Size = new System.Drawing.Size(899, 170);
			this.vsJournals.TabIndex = 12;
			this.vsJournals.CurrentCellChanged += new System.EventHandler(this.vsJournals_AfterRowColChange);
			// 
			// txtCheckMessage
			// 
			this.txtCheckMessage.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckMessage.Location = new System.Drawing.Point(200, 80);
			this.txtCheckMessage.Name = "txtCheckMessage";
			this.txtCheckMessage.Size = new System.Drawing.Size(350, 40);
			this.txtCheckMessage.TabIndex = 5;
			// 
			// txtCheck
			// 
			this.txtCheck.Location = new System.Drawing.Point(163, 550);
			this.txtCheck.MaxLength = 6;
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(150, 40);
			this.txtCheck.TabIndex = 17;
			this.txtCheck.Visible = false;
			this.txtCheck.Leave += new System.EventHandler(this.txtCheck_Leave);
			this.txtCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheck_KeyPressEvent);
			// 
			// lblCheckNumber
			// 
			this.lblCheckNumber.Location = new System.Drawing.Point(30, 564);
			this.lblCheckNumber.Name = "lblCheckNumber";
			this.lblCheckNumber.Size = new System.Drawing.Size(100, 18);
			this.lblCheckNumber.TabIndex = 18;
			this.lblCheckNumber.Text = "CHECK NUMBER";
			this.lblCheckNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblCheckNumber.Visible = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(484, 6);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(66, 14);
			this.lblTitle.TabIndex = 10;
			this.lblTitle.Text = "LABEL2";
			this.lblTitle.Visible = false;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 94);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(100, 18);
			this.Label6.TabIndex = 8;
			this.Label6.Text = "CHECK MESSAGE";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.btnProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(840, 27);
			this.Label1.TabIndex = 9;
			this.Label1.Text = "PLEASE SELECT THE JOURNAL AND VENDOR YOU WISH TO PRINT A CHECK FROM AND THEN CLIC" +
    "K THE \"PROCESS\" BUTTON TO CONTINUE";
			this.Label1.Visible = false;
			// 
			// cmdSave
			// 
			this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSave.Location = new System.Drawing.Point(907, 29);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(46, 24);
			this.cmdSave.TabIndex = 8;
			this.cmdSave.Text = "Save";
			this.cmdSave.Visible = false;
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// btnFileProcess
			// 
			this.btnFileProcess.AppearanceKey = "acceptButton";
			this.btnFileProcess.Location = new System.Drawing.Point(494, 30);
			this.btnFileProcess.Name = "btnFileProcess";
			this.btnFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileProcess.Size = new System.Drawing.Size(100, 48);
			this.btnFileProcess.TabIndex = 8;
			this.btnFileProcess.Text = "Process";
			this.btnFileProcess.Click += new System.EventHandler(this.btnFileProcess_Click);
			// 
			// frmPrintIndividualChecks
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1088, 700);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPrintIndividualChecks";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Individual Checks";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmPrintIndividualChecks_Load);
			this.Activated += new System.EventHandler(this.frmPrintIndividualChecks_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintIndividualChecks_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayDate)).EndInit();
			this.fraPayDate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraReprint)).EndInit();
			this.fraReprint.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsChecks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraVendors)).EndInit();
			this.fraVendors.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsVendors)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournals)).EndInit();
			this.fraJournals.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileProcess)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		public FCButton btnFileProcess;
	}
}
