﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;
using Global;
using fecherFoundation;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPassword.
	/// </summary>
	partial class frmPassword : BaseForm
	{
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCLabel lblInstruction;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 213);
			this.BottomPanel.Size = new System.Drawing.Size(456, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtPassword);
			this.ClientArea.Controls.Add(this.lblInstruction);
			this.ClientArea.Size = new System.Drawing.Size(456, 153);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(456, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(353, 30);
			this.HeaderText.Text = "Enter Account Setup Password";
			// 
			// txtPassword
			// 
			this.txtPassword.AutoSize = false;
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword.InputType.Type = Wisej.Web.TextBoxType.Password;
			this.txtPassword.Location = new System.Drawing.Point(30, 65);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(262, 40);
			this.txtPassword.TabIndex = 0;
			this.txtPassword.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPassword_KeyPress);
			// 
			// lblInstruction
			// 
			this.lblInstruction.AutoSize = true;
			this.lblInstruction.Location = new System.Drawing.Point(30, 26);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(197, 15);
			this.lblInstruction.TabIndex = 1;
			this.lblInstruction.Text = "PRESS ENTER WHEN FINISHED";
			this.lblInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frmPassword
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(456, 321);
			this.Name = "frmPassword";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Enter Account Setup Password";
			this.Load += new System.EventHandler(this.frmPassword_Load);
			this.Activated += new System.EventHandler(this.frmPassword_Activated);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
