﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptProjectSummary.
	/// </summary>
	public partial class rptProjectSummary : BaseSectionReport
	{
		public static rptProjectSummary InstancePtr
		{
			get
			{
				return (rptProjectSummary)Sys.GetInstance(typeof(rptProjectSummary));
			}
		}

		protected rptProjectSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo.Dispose();
				rsActivityDetail.Dispose();
				rsCurrentActivity.Dispose();
				rsFormat.Dispose();
				rsProjectInfo.Dispose();
				rsYTDActivity.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptProjectSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rsProjectInfo = new clsDRWrapper();
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		int intTotalCols;
		int lngColumnCounter;
		bool CurrentDCFlag;
		bool CurrentNetFlag;
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool EncumbranceFlag;
		bool PendingFlag;
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsFormat = new clsDRWrapper();
		string strAcctType;
		// vbPorter upgrade warning: curYTDDebits As Decimal	OnWrite(short, Decimal)
		Decimal curYTDDebits;
		// vbPorter upgrade warning: curYTDCredits As Decimal	OnWrite(short, Decimal)
		Decimal curYTDCredits;
		// vbPorter upgrade warning: curYTDNet As Decimal	OnWrite(short, Decimal)
		Decimal curYTDNet;
		// vbPorter upgrade warning: curCurrentDebits As Decimal	OnWrite(short, Decimal)
		Decimal curCurrentDebits;
		// vbPorter upgrade warning: curCurrentCredits As Decimal	OnWrite(short, Decimal)
		Decimal curCurrentCredits;
		// vbPorter upgrade warning: curCurrentNet As Decimal	OnWrite(short, Decimal)
		Decimal curCurrentNet;
		// vbPorter upgrade warning: curEncumbrances As Decimal	OnWrite(short, Decimal)
		Decimal curEncumbrances;
		// vbPorter upgrade warning: curPending As Decimal	OnWrite(short, Decimal)
		Decimal curPending;
		string strPeriodCheck;

		public rptProjectSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Project Summary";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsAccountInfo.MoveNext();
				CheckAgain2:
				;
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementInfoToGet();
					if (rsProjectInfo.EndOfFile() != true)
					{
						RetrieveInfo();
						goto CheckAgain2;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsProjectInfo.Get_Fields_String("ProjectCode");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 15100 / 1440F;
			Line1.X2 = this.PrintWidth;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			strAcctType = "E";
			curYTDDebits = 0;
			curYTDCredits = 0;
			curYTDNet = 0;
			curCurrentDebits = 0;
			curCurrentCredits = 0;
			curCurrentNet = 0;
			curEncumbrances = 0;
			curPending = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "F")
			{
				lblProjects.Text = "Fund ";
				lblProjects.Text = lblProjects.Text + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp");
			}
			else
			{
				lblProjects.Text = "Project(s) ";
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
				{
					lblProjects.Text = lblProjects.Text + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp");
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
				{
					lblProjects.Text = lblProjects.Text + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp");
				}
				else
				{
					lblProjects.Text = lblProjects.Text + "ALL";
				}
			}
			strPeriodCheck = "AND";
			lblMonths.Text = "Month(s) ";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = lblMonths + "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo True, True, True, "E"
			// CalculateAccountInfo True, True, True, "R"
			// CalculateAccountInfo True, True, True, "G"
			rsFormat.OpenRecordset("SELECT * FROM ProjectSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			FormatFields();
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "L" || FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				CenterYTDHeading();
				CenterCurrentHeading();
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster ORDER BY ProjectCode");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY ProjectCode");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "F")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY ProjectCode");
			}
			else
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND ProjectCode <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' ORDER BY ProjectCode");
			}
			if (rsProjectInfo.EndOfFile() != true && rsProjectInfo.BeginningOfFile() != true)
			{
				CheckAgain:
				;
				RetrieveInfo();
				if (rsAccountInfo.EndOfFile() != true)
				{
					// do nothing
				}
				else
				{
					IncrementInfoToGet();
					if (rsProjectInfo.EndOfFile() != true)
					{
						goto CheckAgain;
					}
					else
					{
						MessageBox.Show("No Project Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						this.Cancel();
						return;
					}
				}
			}
			else
			{
				MessageBox.Show("No Project Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsAccountInfo.Get_Fields_String("Account");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			if (rsCurrentActivity.FindFirstRecord("Account", rsAccountInfo.Get_Fields("Account")))
			{
				fldCurrentDebits.Text = Strings.Format(GetCurrentDebit(), "#,##0.00");
				fldCurrentCredits.Text = Strings.Format(GetCurrentCredit(), "#,##0.00");
				fldCurrentNet.Text = Strings.Format(GetCurrentNet(), "#,##0.00");
				curCurrentDebits += GetCurrentDebit();
				curCurrentCredits += GetCurrentCredit();
				curCurrentNet += GetCurrentNet();
			}
			else
			{
				fldCurrentCredits.Text = "";
				fldCurrentDebits.Text = "";
				fldCurrentNet.Text = "";
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			if (rsYTDActivity.FindFirstRecord("Account", rsAccountInfo.Get_Fields("Account")))
			{
				fldYTDDebits.Text = Strings.Format(GetYTDDebit(), "#,##0.00");
				fldYTDCredits.Text = Strings.Format(GetYTDCredit(), "#,##0.00");
				fldYTDNet.Text = Strings.Format(GetYTDNet(), "#,##0.00");
				fldEncumbrances.Text = Strings.Format(GetEncumbrance(), "#,##0.00");
				fldPending.Text = Strings.Format(GetPending(), "#,##0.00");
				curYTDDebits += GetYTDDebit();
				curYTDCredits += GetYTDCredit();
				curYTDNet += GetYTDNet();
				curEncumbrances += GetEncumbrance();
				curPending += GetPending();
			}
			else
			{
				fldYTDDebits.Text = "";
				fldYTDCredits.Text = "";
				fldYTDNet.Text = "";
				fldEncumbrances.Text = "";
				fldPending.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsCost = new clsDRWrapper())
            {
                fldYTDDebitsTotal.Text = Strings.Format(curYTDDebits, "#,##0.00");
                fldYTDCreditsTotal.Text = Strings.Format(curYTDCredits, "#,##0.00");
                fldYTDNetTotal.Text = Strings.Format(curYTDNet, "#,##0.00");
                fldCurrentDebitsTotal.Text = Strings.Format(curCurrentDebits, "#,##0.00");
                fldCurrentCreditsTotal.Text = Strings.Format(curCurrentCredits, "#,##0.00");
                fldCurrentNetTotal.Text = Strings.Format(curCurrentNet, "#,##0.00");
                fldEncumbranceTotal.Text = Strings.Format(curEncumbrances, "#,##0.00");
                fldPendingTotal.Text = Strings.Format(curPending, "#,##0.00");
                rsCost.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode = '" + this.Fields["Binder"].Value +
                                     "'");
                if (rsCost.EndOfFile() != true && rsCost.BeginningOfFile() != true)
                {
                    fldProjectedCost.Text = Strings.Format(rsCost.Get_Fields_Decimal("ProjectedCost"), "#,##0.00");
                }
                else
                {
                    fldProjectedCost.Text = Strings.Format(0, "#,##0.00");
                }

                fldAmountUsed.Text = Strings.Format(curYTDNet, "#,##0.00");
                fldAvailable.Text =
                    Strings.Format(FCConvert.ToDecimal(fldProjectedCost.Text) - FCConvert.ToDecimal(fldAmountUsed.Text),
                        "#,##0.00");
                curYTDDebits = 0;
                curYTDCredits = 0;
                curYTDNet = 0;
                curCurrentDebits = 0;
                curCurrentCredits = 0;
                curCurrentNet = 0;
                curEncumbrances = 0;
                curPending = 0;
            }
        }

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldProjectTitle.Text = rsProjectInfo.Get_Fields_String("ProjectCode") + " - " + rsProjectInfo.Get_Fields_String("LongDescription");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void FormatFields()
		{
			float lngTotalWidth;
			lngTotalWidth = 250 / 1440f;
			fldAccount.Visible = true;
			fldAccount.Width = CalculateFieldWidth_2("fldAccount");
			fldAccount.Left = lngTotalWidth;
			SetFieldFont(fldAccount);
			SetFieldFont(lblAccount);
			lblAccount.Left = fldAccount.Left;
			lblAccount.Width = fldAccount.Width;
			lblAccount.Visible = true;
			lblAccount.Text = "Account";
			SetFieldFont(lblTotalLabel);
			lblTotalLabel.Left = fldAccount.Left;
			lblTotalLabel.Width = fldAccount.Width;
			lblTotalLabel.Visible = true;
			lngTotalWidth += fldAccount.Width + 100 / 1440f;
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("CurrentDebitCredit")))
			{
				SetFieldFont(fldCurrentDebits);
				fldCurrentDebits.Width = CalculateFieldWidth_2("fldCurrentDebits");
				fldCurrentDebits.Left = lngTotalWidth;
				fldCurrentDebits.Visible = true;
				SetFieldFont(lblCurrentDebits1);
				lblCurrentDebits1.Width = fldCurrentDebits.Width;
				lblCurrentDebits1.Left = fldCurrentDebits.Left;
				lblCurrentDebits1.Text = "Curr Mnth";
				lblCurrentDebits1.Visible = true;
				SetFieldFont(lblCurrentDebits2);
				lblCurrentDebits2.Width = fldCurrentDebits.Width;
				lblCurrentDebits2.Left = fldCurrentDebits.Left;
				lblCurrentDebits2.Text = "Debits";
				lblCurrentDebits2.Visible = true;
				SetFieldFont(fldCurrentDebitsTotal);
				fldCurrentDebitsTotal.Width = fldCurrentDebits.Width;
				fldCurrentDebitsTotal.Left = fldCurrentDebits.Left;
				fldCurrentDebitsTotal.Visible = true;
				lngTotalWidth += fldCurrentDebits.Width + 100 / 1440f;
				SetFieldFont(fldCurrentCredits);
				fldCurrentCredits.Width = CalculateFieldWidth_2("fldCurrentDebits");
				fldCurrentCredits.Left = lngTotalWidth;
				fldCurrentCredits.Visible = true;
				SetFieldFont(lblCurrentCredits1);
				lblCurrentCredits1.Width = fldCurrentCredits.Width;
				lblCurrentCredits1.Left = fldCurrentCredits.Left;
				lblCurrentCredits1.Text = "Curr Mnth";
				lblCurrentCredits1.Visible = true;
				SetFieldFont(lblCurrentCredits2);
				lblCurrentCredits2.Width = fldCurrentCredits.Width;
				lblCurrentCredits2.Left = fldCurrentCredits.Left;
				lblCurrentCredits2.Text = "Credits";
				lblCurrentCredits2.Visible = true;
				SetFieldFont(fldCurrentCreditsTotal);
				fldCurrentCreditsTotal.Width = fldCurrentCredits.Width;
				fldCurrentCreditsTotal.Left = fldCurrentCredits.Left;
				fldCurrentCreditsTotal.Visible = true;
				lngTotalWidth += fldCurrentCredits.Width + 100 / 1440f;
				CurrentDCFlag = true;
			}
			else
			{
				lblCurrentDebits1.Visible = false;
				lblCurrentDebits2.Visible = false;
				fldCurrentDebits.Visible = false;
				fldCurrentCreditsTotal.Visible = false;
				lblCurrentCredits1.Visible = false;
				lblCurrentCredits2.Visible = false;
				fldCurrentCredits.Visible = false;
				fldCurrentCreditsTotal.Visible = false;
				CurrentDCFlag = false;
			}
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("CurrentNet")))
			{
				SetFieldFont(fldCurrentNet);
				fldCurrentNet.Width = CalculateFieldWidth_2("fldCurrentNet");
				fldCurrentNet.Left = lngTotalWidth;
				fldCurrentNet.Visible = true;
				SetFieldFont(lblCurrentNet1);
				lblCurrentNet1.Width = fldCurrentNet.Width;
				lblCurrentNet1.Left = fldCurrentNet.Left;
				lblCurrentNet1.Text = "Curr Mnth";
				lblCurrentNet1.Visible = true;
				SetFieldFont(lblCurrentNet2);
				lblCurrentNet2.Width = fldCurrentNet.Width;
				lblCurrentNet2.Left = fldCurrentNet.Left;
				lblCurrentNet2.Text = "Net";
				lblCurrentNet2.Visible = true;
				SetFieldFont(fldCurrentNetTotal);
				fldCurrentNetTotal.Width = fldCurrentNet.Width;
				fldCurrentNetTotal.Left = fldCurrentNet.Left;
				fldCurrentNetTotal.Visible = true;
				lngTotalWidth += fldCurrentNet.Width + 100 / 1440f;
				CurrentNetFlag = true;
			}
			else
			{
				lblCurrentNet1.Visible = false;
				lblCurrentNet2.Visible = false;
				fldCurrentNet.Visible = false;
				fldCurrentNetTotal.Visible = false;
				CurrentNetFlag = false;
			}
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("YTDDebitCredit")))
			{
				SetFieldFont(fldYTDDebits);
				fldYTDDebits.Width = CalculateFieldWidth_2("fldYTDDebits");
				fldYTDDebits.Left = lngTotalWidth;
				fldYTDDebits.Visible = true;
				SetFieldFont(lblYTDDebits1);
				lblYTDDebits1.Width = fldYTDDebits.Width;
				lblYTDDebits1.Left = fldYTDDebits.Left;
				lblYTDDebits1.Text = "YTD";
				lblYTDDebits1.Visible = true;
				SetFieldFont(lblYTDDebits2);
				lblYTDDebits2.Width = fldYTDDebits.Width;
				lblYTDDebits2.Left = fldYTDDebits.Left;
				lblYTDDebits2.Text = "Debits";
				lblYTDDebits2.Visible = true;
				SetFieldFont(fldYTDDebitsTotal);
				fldYTDDebitsTotal.Width = fldYTDDebits.Width;
				fldYTDDebitsTotal.Left = fldYTDDebits.Left;
				fldYTDDebitsTotal.Visible = true;
				lngTotalWidth += fldYTDDebits.Width + 100 / 1440f;
				SetFieldFont(fldYTDCredits);
				fldYTDCredits.Width = CalculateFieldWidth_2("fldYTDDebits");
				fldYTDCredits.Left = lngTotalWidth;
				fldYTDCredits.Visible = true;
				SetFieldFont(lblYTDCredits1);
				lblYTDCredits1.Width = fldYTDCredits.Width;
				lblYTDCredits1.Left = fldYTDCredits.Left;
				lblYTDCredits1.Text = "YTD";
				lblYTDCredits1.Visible = true;
				SetFieldFont(lblYTDCredits2);
				lblYTDCredits2.Width = fldYTDCredits.Width;
				lblYTDCredits2.Left = fldYTDCredits.Left;
				lblYTDCredits2.Text = "Credits";
				lblYTDCredits2.Visible = true;
				SetFieldFont(fldYTDCreditsTotal);
				fldYTDCreditsTotal.Width = fldYTDCredits.Width;
				fldYTDCreditsTotal.Left = fldYTDCredits.Left;
				fldYTDCreditsTotal.Visible = true;
				lngTotalWidth += fldYTDCredits.Width + 100 / 1440f;
				YTDDCFlag = true;
			}
			else
			{
				lblYTDDebits1.Visible = false;
				lblYTDDebits2.Visible = false;
				fldYTDDebits.Visible = false;
				fldYTDDebitsTotal.Visible = false;
				lblYTDCredits1.Visible = false;
				lblYTDCredits2.Visible = false;
				fldYTDCredits.Visible = false;
				fldYTDCreditsTotal.Visible = false;
				YTDDCFlag = false;
			}
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("YTDNet")))
			{
				SetFieldFont(fldYTDNet);
				fldYTDNet.Width = CalculateFieldWidth_2("fldYTDNet");
				fldYTDNet.Left = lngTotalWidth;
				fldYTDNet.Visible = true;
				SetFieldFont(lblYTDNet1);
				lblYTDNet1.Width = fldYTDNet.Width;
				lblYTDNet1.Left = fldYTDNet.Left;
				lblYTDNet1.Text = "YTD";
				lblYTDNet1.Visible = true;
				SetFieldFont(lblYTDNet2);
				lblYTDNet2.Width = fldYTDNet.Width;
				lblYTDNet2.Left = fldYTDNet.Left;
				lblYTDNet2.Text = "Net";
				lblYTDNet2.Visible = true;
				SetFieldFont(fldYTDNetTotal);
				fldYTDNetTotal.Width = fldYTDNet.Width;
				fldYTDNetTotal.Left = fldYTDNet.Left;
				fldYTDNetTotal.Visible = true;
				lngTotalWidth += fldYTDNet.Width + 100 / 1440f;
				YTDNetFlag = true;
			}
			else
			{
				lblYTDNet1.Visible = false;
				lblYTDNet2.Visible = false;
				fldYTDNet.Visible = false;
				fldYTDNetTotal.Visible = false;
				YTDNetFlag = false;
			}
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("OSEncumbrance")))
			{
				SetFieldFont(fldEncumbrances);
				fldEncumbrances.Width = CalculateFieldWidth_2("fldEncumbrances");
				fldEncumbrances.Left = lngTotalWidth;
				fldEncumbrances.Visible = true;
				SetFieldFont(lblEncumbrance1);
				lblEncumbrance1.Width = fldEncumbrances.Width;
				lblEncumbrance1.Left = fldEncumbrances.Left;
				lblEncumbrance1.Text = "Outstanding";
				lblEncumbrance1.Visible = true;
				SetFieldFont(lblEncumbrance2);
				lblEncumbrance2.Width = fldEncumbrances.Width;
				lblEncumbrance2.Left = fldEncumbrances.Left;
				lblEncumbrance2.Text = "Encum";
				lblEncumbrance2.Visible = true;
				SetFieldFont(fldEncumbranceTotal);
				fldEncumbranceTotal.Width = fldEncumbrances.Width;
				fldEncumbranceTotal.Left = fldEncumbrances.Left;
				fldEncumbranceTotal.Visible = true;
				lngTotalWidth += fldEncumbrances.Width + 100 / 1440f;
				EncumbranceFlag = true;
			}
			else
			{
				lblEncumbrance1.Visible = false;
				lblEncumbrance2.Visible = false;
				fldEncumbrances.Visible = false;
				fldEncumbranceTotal.Visible = false;
				EncumbranceFlag = false;
			}
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("SeperatePending")))
			{
				SetFieldFont(fldPending);
				fldPending.Width = CalculateFieldWidth_2("fldPending");
				fldPending.Left = lngTotalWidth;
				fldPending.Visible = true;
				SetFieldFont(lblPending1);
				lblPending1.Width = fldPending.Width;
				lblPending1.Left = fldPending.Left;
				lblPending1.Text = "Pending";
				lblPending1.Visible = true;
				SetFieldFont(lblPending2);
				lblPending2.Width = fldPending.Width;
				lblPending2.Left = fldPending.Left;
				lblPending2.Text = "Activity";
				lblPending2.Visible = true;
				SetFieldFont(fldPendingTotal);
				fldPendingTotal.Width = fldPending.Width;
				fldPendingTotal.Left = fldPending.Left;
				fldPendingTotal.Visible = true;
				lngTotalWidth += fldPending.Width + 100 / 1440f;
				PendingFlag = true;
			}
			else
			{
				lblPending1.Visible = false;
				lblPending2.Visible = false;
				fldPending.Visible = false;
				fldPendingTotal.Visible = false;
				PendingFlag = false;
			}
			linTotal.X2 = lngTotalWidth;
			SetFieldFont(lblProjectSummary);
			SetFieldFont(lblProjectedCost);
			SetFieldFont(lblAmountUsed);
			SetFieldFont(lblAvailable);
			SetFieldFont(fldProjectedCost);
			SetFieldFont(fldAmountUsed);
			SetFieldFont(fldAvailable);
		}

		private void SetFieldFont(GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				x.Font = new Font(x.Font.Name, 10);
			}
			else if (rsFormat.Get_Fields_String("Font") == "L")
			{
				x.Font = new Font(x.Font.Name, 12);
			}
			else
			{
				FontStyle style = FontStyle.Regular;
				if (rsFormat.Get_Fields_Boolean("Bold"))
					style = FontStyle.Bold;
				if (rsFormat.Get_Fields_Boolean("Italic"))
					style |= FontStyle.Italic;
				if (rsFormat.Get_Fields_Boolean("StrikeThru"))
					style |= FontStyle.Strikeout;
				if (rsFormat.Get_Fields_Boolean("Underline"))
					style |= FontStyle.Underline;
				// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
				x.Font = new Font(rsFormat.Get_Fields_String("FontName"), FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
			}
		}

		private void SetFieldFont(GrapeCity.ActiveReports.SectionReportModel.Label x)
		{
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				x.Font = new Font(x.Font.Name, 10);
			}
			else if (rsFormat.Get_Fields_String("Font") == "L")
			{
				x.Font = new Font(x.Font.Name, 12);
			}
			else
			{
				FontStyle style = FontStyle.Regular;
				if (rsFormat.Get_Fields_Boolean("Bold"))
					style = FontStyle.Bold;
				if (rsFormat.Get_Fields_Boolean("Italic"))
					style |= FontStyle.Italic;
				if (rsFormat.Get_Fields_Boolean("StrikeThru"))
					style |= FontStyle.Strikeout;
				if (rsFormat.Get_Fields_Boolean("Underline"))
					style |= FontStyle.Underline;
				// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
				x.Font = new Font(rsFormat.Get_Fields_String("FontName"), FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private float CalculateFieldWidth_2(string x)
		{
			return CalculateFieldWidth(ref x);
		}

		private float CalculateFieldWidth(ref string x)
		{
			float CalculateFieldWidth = 0;
			if (x == "fldAccount")
			{
				if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
				{
					CalculateFieldWidth = 2400 / 1440f;
				}
				else if (rsFormat.Get_Fields_String("Font") == "L")
				{
					CalculateFieldWidth = 2000 / 1440f;
				}
			}
			else
			{
				if (FCConvert.ToString(rsFormat.Get_Fields_String("Printer")) == "O")
				{
					if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
					{
						CalculateFieldWidth = 1300 / 1440f;
					}
					else
					{
						CalculateFieldWidth = 1500 / 1440f;
					}
				}
				else
				{
					CalculateFieldWidth = 1500 / 1440f;
				}
			}
			return CalculateFieldWidth;
		}

		private void CenterYTDHeading()
		{
			if (lblYTDDebits1.Visible == true)
			{
				if (lblYTDNet1.Visible == true)
				{
					lblYTDNet1.Visible = false;
					lblYTDCredits1.Visible = false;
					lblYTDDebits1.Width = lblYTDNet1.Left - lblYTDDebits1.Left + lblYTDNet1.Width;
					lblYTDDebits1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblYTDDebits1.Text = "------------ Y T D ------------";
				}
				else
				{
					lblYTDCredits1.Visible = false;
					lblYTDDebits1.Width = lblYTDCredits1.Left - lblYTDDebits1.Left + lblYTDCredits1.Width;
					lblYTDDebits1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblYTDDebits1.Text = "---- Y T D ----";
				}
			}
		}

		private void CenterCurrentHeading()
		{
			if (lblCurrentDebits1.Visible == true)
			{
				if (lblCurrentNet1.Visible == true)
				{
					lblCurrentNet1.Visible = false;
					lblCurrentCredits1.Visible = false;
					lblCurrentDebits1.Width = lblCurrentNet1.Left - lblCurrentDebits1.Left + lblCurrentNet1.Width;
					lblCurrentDebits1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblCurrentDebits1.Text = "------- C U R R   M O N T H -------";
				}
				else
				{
					lblCurrentCredits1.Visible = false;
					lblCurrentDebits1.Width = lblCurrentCredits1.Left - lblCurrentDebits1.Left + lblCurrentCredits1.Width;
					lblCurrentDebits1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblCurrentDebits1.Text = "- C U R R   M O N T H -";
				}
			}
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (strAcctType == "E")
			{
				rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) ORDER BY Account");
				rsYTDActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account, Project");
				strPeriodCheck = strPeriodCheckHolder;
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project, Period");
				}
				else
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project, Period");
				}
			}
			else if (strAcctType == "R")
			{
				rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) ORDER BY Account");
				rsYTDActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account, Project");
				strPeriodCheck = strPeriodCheckHolder;
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project, Period");
				}
				else
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project, Period");
				}
			}
			else
			{
				rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) ORDER BY Account");
				rsYTDActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account, Project");
				strPeriodCheck = strPeriodCheckHolder;
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + ") GROUP BY Account, Project, Period");
				}
				else
				{
					rsCurrentActivity.OpenRecordset("SELECT Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project");
					rsActivityDetail.OpenRecordset("SELECT Period, Account, Project, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period >= " + FCConvert.ToString(modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + ") GROUP BY Account, Project, Period");
				}
			}
		}

		private void IncrementInfoToGet()
		{
			if (strAcctType == "E")
			{
				strAcctType = "R";
			}
			else if (strAcctType == "R")
			{
				strAcctType = "G";
			}
			else
			{
				rsProjectInfo.MoveNext();
				strAcctType = "E";
			}
		}

		private Decimal GetYTDDebit()
		{
			Decimal GetYTDDebit = 0;
			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
			GetYTDDebit = FCConvert.ToDecimal(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
			if (rsFormat.Get_Fields_Boolean("OSEncumbrance") == false)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (rsFormat.Get_Fields_Boolean("SeperatePending") == false && rsFormat.Get_Fields_Boolean("IncludePending") == true)
			{
				// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
				GetYTDDebit += rsYTDActivity.Get_Fields("PendingDebitsTotal");
			}
			return GetYTDDebit;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(short, Decimal)
		private Decimal GetYTDCredit()
		{
			Decimal GetYTDCredit = 0;
			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
			GetYTDCredit = FCConvert.ToDecimal(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
			if (rsFormat.Get_Fields_Boolean("SeperatePending") == false && rsFormat.Get_Fields_Boolean("IncludePending") == true)
			{
				// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
				GetYTDCredit -= rsYTDActivity.Get_Fields("PendingCreditsTotal");
			}
			return GetYTDCredit;
		}

		private Decimal GetYTDNet()
		{
			Decimal GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() - GetYTDCredit();
			return GetYTDNet;
		}

		private Decimal GetEncumbrance()
		{
			Decimal GetEncumbrance = 0;
			// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
			GetEncumbrance = FCConvert.ToDecimal(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
			return GetEncumbrance;
		}

		private Decimal GetCurrentDebit()
		{
			Decimal GetCurrentDebit = 0;
			// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
			GetCurrentDebit = FCConvert.ToDecimal(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
			return GetCurrentDebit;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal GetCurrentCredit()
		{
			Decimal GetCurrentCredit = 0;
			// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
			GetCurrentCredit = FCConvert.ToDecimal(rsCurrentActivity.Get_Fields("PostedCreditsTotal")) * -1;
			return GetCurrentCredit;
		}

		private Decimal GetCurrentNet()
		{
			Decimal GetCurrentNet = 0;
			GetCurrentNet = GetCurrentDebit() - GetCurrentCredit();
			return GetCurrentNet;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		private Decimal GetPending()
		{
			Decimal GetPending = 0;
			// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
			GetPending = FCConvert.ToDecimal(rsYTDActivity.Get_Fields("PendingDebitsTotal") - rsYTDActivity.Get_Fields("PendingCreditsTotal"));
			return GetPending;
		}

		
		private void rptProjectSummary_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
