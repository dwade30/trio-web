﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Drawing;
using System.Threading;
using SharedApplication;
using TWSharedLibrary;
using Wisej.Web;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmAPDataEntry.
    /// </summary>
    public partial class frmAPDataEntry : BaseForm
    {
        public frmAPDataEntry()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
            txtAddress.AddControlArrayElement(txtAddress_0, 0);
            txtAddress.AddControlArrayElement(txtAddress_1, 1);
            txtAddress.AddControlArrayElement(txtAddress_2, 2);
            txtAddress.AddControlArrayElement(txtAddress_3, 3);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            vsAltCashAccount.ExtendLastCol = true;
            vs3.ExtendLastCol = true;
            vs4.ExtendLastCol = true;
            vsPurchaseOrders.ExtendLastCol = true;
            vs1.ExtendLastCol = true;
            vs1.AutoSizeOnRowChanging = true;
            btnCancelEncumbrance.Click += BtnCancelEncumbrance_Click;
            vs1.CellMouseClick += Vs1_CellMouseClick;
            this.chkUseAltCashAccount.KeyUp += ChkUseAltCashAccount_KeyUp;
        }

        private void ChkUseAltCashAccount_KeyUp(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Enter || KeyCode == Keys.Return)
            {
                e.Handled = true;
                KeyCode = Keys.None;
                if (chkUseAltCashAccount.Checked)
                {
                    vsAltCashAccount.Focus();
                }
                else
                {
                    vs1.Col = DescriptionCol;
                    if (vs1.Rows > 0)
                    {
                        vs1.Row = 1;
                    }
                    vs1.Focus();
                }
                
            }
        }

        private void Vs1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var col = e.ColumnIndex;
        }

        private void BtnCancelEncumbrance_Click(object sender, EventArgs e)
        {
            Frame2.Visible = false;
            txtVendor.Focus();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmAPDataEntry InstancePtr
        {
            get
            {
                return (frmAPDataEntry)Sys.GetInstance(typeof(frmAPDataEntry));
            }
        }

        protected frmAPDataEntry _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        int NumberCol;
        int DescriptionCol;
        int AccountCol;
        int TaxCol;
        int ProjectCol;
        // column variables for flexgrid
        int AmountCol;
        int DiscountCol;
        int EncumbranceCol;
        bool SearchFlag;
        double TotalAmount;
        double TotalDiscount;
        double AmountToPay;
        bool EditFlag;
        public int ChosenEncumbrance;
        public int ChosenPurchaseOrder;
        bool GotEncData;
        // flag to know if we have encummbrance data
        bool GotPOData;
        string ErrorString = "";
        // Error message to show if account is invalid
        bool BadAccountFlag;
        // flag to knwo whetyher the account was good or bad
        int CurrJournal;
        // Which journal we are wokring in
        public int apJournalId;
        public bool OldJournal;
        // variables to keep track of which record we are looking at if we searched for certain ones
        public int FirstRecordShown;
        public bool FromVendorMaster;
        public int NumberOfEncumbrances;
        public int OldJournalNumber;
        public bool EncumbranceFlag;
        public bool PurchaseOrderFlag;
        bool DeleteFlag;
        // vbPorter upgrade warning: TempEncAccount As int	OnWrite(string)
        int TempEncAccount;
        bool JournalFlag;
        bool JournalNameFlag;
        clsDRWrapper Master = new clsDRWrapper();
        public bool blnFromVendor;
        bool blnUnload;
        bool blnClicked;
        bool blnBackedInto;
        // vbPorter upgrade warning: datPayableDate As DateTime	OnWrite(DateTime, string)
        DateTime datPayableDate;
        int intSavedPeriod;
        bool blnSaved;
        bool blnMoveToDescription;
        bool blnJournalLocked;
        bool isFormClosed = false;
        string strComboList = "";
        public bool blnJournalEdit;
        DateTime datOldPayableDate;
        DateTime datNewPayableDate;
        private clsGridAccount vsGrid_AutoInitialized;
        bool openFormWhenThisClosed = false;

        private clsGridAccount vsGrid
        {
            get
            {
                if (vsGrid_AutoInitialized == null)
                {
                    vsGrid_AutoInitialized = new clsGridAccount();
                }
                return vsGrid_AutoInitialized;
            }
            set
            {
                vsGrid_AutoInitialized = value;
            }
        }

        private clsGridAccount vsDetailGrid_AutoInitalized;

        private clsGridAccount vsDetailGrid
        {
            get
            {
                if (vsDetailGrid_AutoInitalized == null)
                {
                    vsDetailGrid_AutoInitalized = new clsGridAccount();
                }
                return vsDetailGrid_AutoInitalized;
            }
            set
            {
                vsDetailGrid_AutoInitalized = value;
            }
        }

        clsDRWrapper rsYTDActivity_AutoInitialized;

        clsDRWrapper rsYTDActivity
        {
            get
            {
                if (rsYTDActivity_AutoInitialized == null)
                {
                    rsYTDActivity_AutoInitialized = new clsDRWrapper();
                }
                return rsYTDActivity_AutoInitialized;
            }
            set
            {
                rsYTDActivity_AutoInitialized = value;
            }
        }

        private bool boolF12;
        private cVendorController vendCont_AutoInitialized;

        private cVendorController vendCont
        {
            get
            {
                if (vendCont_AutoInitialized == null)
                {
                    vendCont_AutoInitialized = new cVendorController();
                }
                return vendCont_AutoInitialized;
            }
            set
            {
                vendCont_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cEncumbranceController encCont = new cEncumbranceController();
        private cEncumbranceController encCont_AutoInitialized;

        private cEncumbranceController encCont
        {
            get
            {
                if (encCont_AutoInitialized == null)
                {
                    encCont_AutoInitialized = new cEncumbranceController();
                }
                return encCont_AutoInitialized;
            }
            set
            {
                encCont_AutoInitialized = value;
            }
        }

        private void cboEncDept_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cboEncDept.Visible)
            {
                GetEncData();
                if (vs3.Rows == 1)
                {
                    MessageBox.Show("No Encumbrance Information Found", "No Encumbrances", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                vs3.Row = 1;
                vs3.Focus();
            }
        }

        private void cboPurchaseOrderDepartment_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cboPurchaseOrderDepartment.Visible)
            {
                GetPOData();
                if (vsPurchaseOrders.Rows == 1)
                {
                    MessageBox.Show("No Purchase Order Information Found", "No Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                vsPurchaseOrders.Row = 1;
                vsPurchaseOrders.Focus();
            }
        }

        private void cboFrequency_Enter(object sender, System.EventArgs e)
        {
            // make sure Vendor data has been input
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                txtAmount.Text = "";
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
            // erase the account title
        }

        private void AdvanceToVendor()
        {
            if (txtVendor.Enabled)
            {
                txtVendor.Focus();
            }
            else
            {
                txtAddress[0].Focus();
            }
        }

        private void cboJournal_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void chkSeperate_CheckedChanged(object sender, System.EventArgs e)
        {
            lblExpense.Text = "";
            // erase the account title
        }

        private void chkUseAltCashAccount_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkUseAltCashAccount.CheckState == CheckState.Checked)
            {
                vsAltCashAccount.Enabled = true;
                vsAltCashAccount.TabStop = true;
            }
            else
            {
                vsAltCashAccount.Enabled = false;
                vsAltCashAccount.TextMatrix(0, 0, "");
                vsAltCashAccount.TabStop = false;
            }
        }

        private void cmdBalanceOK_Click(object sender, System.EventArgs e)
        {
            fraAccountBalance.Visible = false;
            vs1.Focus();
        }

        private void cmdCancel_Click(object sender, System.EventArgs e)
        {
            int counter;
            txtSearch.Text = "";
            frmSearch.Visible = false;
            // txtPeriod.Enabled = True            'enable all controls on the form
            // cboJournal.Enabled = True
            txtVendor.Enabled = true;
            for (counter = 0; counter <= 3; counter++)
            {
                txtAddress[counter].Enabled = true;
            }
            txtCity.Enabled = true;
            txtState.Enabled = true;
            txtZip.Enabled = true;
            txtZip4.Enabled = true;

            cboFrequency.Enabled = true;
            txtUntil.Enabled = true;
            txtDescription.Enabled = true;
            txtReference.Enabled = true;
            txtAmount.Enabled = true;
            txtCheck.Enabled = true;
            vs1.Enabled = true;
            txtVendor.Text = "";
            txtVendor.Focus();
            btnProcessSave.Text = "Save & Continue";
        }

        private void cmdCancelSave_Click(object sender, System.EventArgs e)
        {
            // set the save flag back to false if the user cancels the save
            modBudgetaryAccounting.UnlockJournal();
            blnJournalLocked = false;
            if (Master.IsntAnything())
            {
                // do nothing
            }
            else
            {
                Master.Reset();
            }
            fraJournalSave.Visible = false;
        }

        private void cmdNo_Click(object sender, System.EventArgs e)
        {
            // if te user does not want the encumbrance they selected
            vs4.Rows = 1;
            fraEncDetail.Visible = false;
            Frame2.Visible = true;
            vs3.Row = 1;
            vs3.Focus();
        }

        public void cmdNo_Click()
        {
            cmdNo_Click(cmdNo, new System.EventArgs());
        }

        private void cmdOk_Click(object sender, System.EventArgs e)
        {
            // once the user has selected an encumbrance show the detail accounts
            GetEncumbranceData();
        }

        private void cmdOKSave_Click(object sender, System.EventArgs e)
        {
            // save the information
            blnUnload = false;
            SaveJournal();
        }

        public void cmdOKSave_Click()
        {
            cmdOKSave_Click(cmdOKSave, new System.EventArgs());
        }

        private void cmdRetrieve_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: tempAccount As int	OnWrite(string)
            int tempAccount = 0;
            int counter;
            string temp = "";
            clsDRWrapper rs2 = new clsDRWrapper();
            int counter2;
            string strLabel = "";
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsVendorInfo = new clsDRWrapper();

            try
            {
                if (lstRecords.Items[lstRecords.SelectedIndex].Text.Length > 0)
                {
                    // if a record is highlighted
                    tempAccount = FCConvert.ToInt32(Strings.Mid(lstRecords.Items[lstRecords.SelectedIndex].Text, 1, 5));
                    // get the account number of that record
                }
                if (tempAccount != 0)
                {
                    // if there is a valid account number
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(tempAccount));
                    if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
                    {
                        // if there is a record
                        rsVendorInfo.MoveLast();
                        rsVendorInfo.MoveFirst();
                        if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "S")
                        {
                            MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        EncumbranceFlag = false;
                        PurchaseOrderFlag = false;
                        // txtPeriod.Enabled = True
                        // cboJournal.Enabled = True
                        txtVendor.Enabled = true;
                        for (counter = 0; counter <= 3; counter++)
                        {
                            txtAddress[counter].Enabled = true;
                        }
                        txtCity.Enabled = true;
                        txtState.Enabled = true;
                        txtZip.Enabled = true;
                        txtZip4.Enabled = true;
                        DateTime parsedDate;
                        if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
                        {
                            txtPayable.Enabled = true;
                        }
                        else
                        {
                            txtPayable.Enabled = false;
                        }

                        cboFrequency.Enabled = true;
                        // enable all controls on form
                        txtUntil.Enabled = true;
                        txtDescription.Enabled = true;
                        txtReference.Enabled = true;
                        txtAmount.Enabled = true;
                        txtCheck.Enabled = true;
                        vs1.Enabled = true;
                        temp = Conversion.Str(rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                        txtVendor.Text = Strings.Format(temp, "00000");
                        txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                        // bring in vendor data
                        txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                        txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                        txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                        txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                        txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                        txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                        txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
                        if (!modBudgetaryMaster.Statics.blnAPEdit)
                        {
                            txtDescription.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultDescription"));
                            txtCheck.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultCheckNumber"));
                            counter = 1;
                            // get vendor details if there are any
                            rs2.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY DescriptionNumber");
                            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                            {
                                // if there is any default information
                                rs2.MoveLast();
                                rs2.MoveFirst();
                                while (rs2.EndOfFile() != true)
                                {
                                    // get all the default information there is
                                    if (counter >= vs1.Rows)
                                    {
                                        vs1.AddItem("");
                                        vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
                                        vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
                                        vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                                        //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, counter, TaxCol, 4);
                                    }
                                    // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                    vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rs2.Get_Fields("AccountNumber")));
                                    vs1.TextMatrix(counter, DescriptionCol, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                    vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(rs2.Get_Fields("Amount")));
                                    // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                    vs1.TextMatrix(counter, TaxCol, FCConvert.ToString(rs2.Get_Fields("Tax")));
                                    vs1.TextMatrix(counter, ProjectCol, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                                    rs2.MoveNext();
                                    counter += 1;
                                }
                                for (counter = counter; counter <= vs1.Rows - 1; counter++)
                                {
                                    vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, 1, "");
                                    vs1.TextMatrix(counter, 2, "");
                                    vs1.TextMatrix(counter, 3, "");
                                    vs1.TextMatrix(counter, 4, "");
                                }
                                CalculateTotals();
                                // figure out new totals with vendor data included
                                txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                                AmountToPay = TotalAmount;
                                lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                                lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                                // display how much is left to pay
                            }
                        }
                        lstRecords.Clear();
                        // clear the list box
                        frmInfo.Visible = false;
                        // make the list of records invisible
                        if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")) != "")
                        {
                            // show data entry message if there is one
                            ans = MessageBox.Show(rsVendorInfo.Get_Fields_String("DataEntryMessage") + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this vendor message at this time?", "Data Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (ans == DialogResult.Yes)
                            {
                                rsVendorInfo.Edit();
                                rsVendorInfo.Set_Fields("DataEntryMessage", "");
                                rsVendorInfo.Update(true);
                            }
                        }
                        txtDescription.Focus();
                        SearchFlag = false;
                        btnProcessSave.Text = "Save & Continue";
                    }
                }
            }
            finally
            {
                rs2.DisposeOf();
                rsVendorInfo.DisposeOf();
            }
        }

        public void cmdRetrieve_Click()
        {
            cmdRetrieve_Click(cmdRetrieve, new System.EventArgs());
        }

        private void cmdReturn_Click(object sender, System.EventArgs e)
        {
            int counter;
            frmInfo.Visible = false;
            lstRecords.Clear();
            txtVendor.Enabled = true;
            for (counter = 0; counter <= 3; counter++)
            {
                txtAddress[counter].Enabled = true;
            }
            txtCity.Enabled = true;
            txtState.Enabled = true;
            txtZip.Enabled = true;
            txtZip4.Enabled = true;
            DateTime parsedDate;
            if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
            {
                txtPayable.Enabled = true;
            }
            else
            {
                txtPayable.Enabled = false;
            }

            cboFrequency.Enabled = true;
            // enable all the controls on the form
            txtUntil.Enabled = true;
            txtDescription.Enabled = true;
            txtReference.Enabled = true;
            txtAmount.Enabled = true;
            txtCheck.Enabled = true;
            vs1.Enabled = true;
            txtVendor.Focus();
            btnProcessSave.Text = "Save & Continue";
        }

        private void cmdSearch_Click(object sender, System.EventArgs e)
        {
            string temp = "";
            int counter;
            clsDRWrapper rs2 = new clsDRWrapper();
            int counter2;
            string strLabel = "";
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            //! Load frmWait; // show the wait form
            frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Searching";
            frmWait.InstancePtr.Refresh();
            frmWait.InstancePtr.Show();
            if (txtSearch.Text == "")
            {
                // make sure there is some criteria to search for
                frmWait.InstancePtr.Unload();
                //Application.DoEvents();
                MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            frmSearch.Visible = false;
            rsVendorInfo.OpenRecordset("SELECT * FROM (SELECT CheckName, CheckAddress1, VendorNumber, Status, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DefaultDescription, DefaultCheckNumber, DataEntryMessage, CheckName as VendorSearchName FROM VendorMaster WHERE CheckName like '" + modCustomReport.FixQuotes(txtSearch.Text) + "%' AND Status <> 'D' UNION ALL SELECT CheckName, CheckAddress1, VendorNumber,Status, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DefaultDescription, DefaultCheckNumber, DataEntryMessage, CheckAddress1 as VendorSearchName FROM VendorMaster WHERE upper(left(CheckAddress1, " + FCConvert.ToString(1 + txtSearch.Text.Length) + ")) = '%" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND upper(left(CheckName, " + FCConvert.ToString(txtSearch.Text.Length) + ")) <> '" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND Status <> 'D') as temp ORDER BY VendorSearchName");
            if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
            {
                // if there is a record
                if (rsVendorInfo.RecordCount() == 1)
                {
                    // if there is only 1 record
                    if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "S")
                    {
                        frmWait.InstancePtr.Unload();
                        //Application.DoEvents();
                        MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        frmSearch.Visible = true;
                        return;
                    }
                    txtSearch.Text = "";
                    frmWait.InstancePtr.Unload();
                    EncumbranceFlag = false;
                    PurchaseOrderFlag = false;
                    txtVendor.Enabled = true;
                    for (counter = 0; counter <= 3; counter++)
                    {
                        txtAddress[counter].Enabled = true;
                    }
                    txtCity.Enabled = true;
                    txtState.Enabled = true;
                    txtZip.Enabled = true;
                    txtZip4.Enabled = true;
                    // enable all the controls on the form
                    cboFrequency.Enabled = true;
                    txtUntil.Enabled = true;
                    txtDescription.Enabled = true;
                    txtReference.Enabled = true;
                    txtAmount.Enabled = true;
                    txtCheck.Enabled = true;
                    vs1.Enabled = true;
                    temp = Strings.Trim(Conversion.Str(rsVendorInfo.Get_Fields_Int32("VendorNumber")));
                    txtVendor.Text = Strings.Format(temp, "00000");
                    txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                    txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                    txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                    // bring in vendor data
                    txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                    txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                    txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                    txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                    txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
                    if (!modBudgetaryMaster.Statics.blnAPEdit)
                    {
                        txtDescription.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultDescription"));
                        txtCheck.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultCheckNumber"));
                        // If Trim(txtCheck.Text) <> "" Then
                        // chkUseAltCashAccount.Visible = True
                        // fraAltCashAccount.Visible = True
                        // chkUseAltCashAccount.SetFocus
                        // End If
                        counter = 1;
                        // get vendor default information if there is any
                        rs2.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY DescriptionNumber");
                        if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                        {
                            // if there is any default information
                            rs2.MoveLast();
                            rs2.MoveFirst();
                            while (rs2.EndOfFile() != true)
                            {
                                // get all the default information there is
                                if (counter >= vs1.Rows)
                                {
                                    vs1.AddItem("");
                                    vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
                                    vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, counter, TaxCol, 4);
                                }
                                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rs2.Get_Fields("AccountNumber")));
                                vs1.TextMatrix(counter, DescriptionCol, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(rs2.Get_Fields("Amount")));
                                // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                vs1.TextMatrix(counter, TaxCol, FCConvert.ToString(rs2.Get_Fields("Tax")));
                                vs1.TextMatrix(counter, ProjectCol, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                                rs2.MoveNext();
                                counter += 1;
                            }
                            for (counter = counter; counter <= vs1.Rows - 1; counter++)
                            {
                                vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, 1, "");
                                vs1.TextMatrix(counter, 2, "");
                                vs1.TextMatrix(counter, 3, "");
                                vs1.TextMatrix(counter, 4, "");
                            }
                            CalculateTotals();
                            // calculate new totals and display information
                            txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                            AmountToPay = TotalAmount;
                            lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                            lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                        }
                    }
                    if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")) != "")
                    {
                        // shwo the data entry message if there is one
                        ans = MessageBox.Show(rsVendorInfo.Get_Fields_String("DataEntryMessage") + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this vendor message at this time?", "Data Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (ans == DialogResult.Yes)
                        {
                            rsVendorInfo.Edit();
                            rsVendorInfo.Set_Fields("DataEntryMessage", "");
                            rsVendorInfo.Update(true);
                        }
                    }
                    txtDescription.Focus();
                    btnProcessSave.Text = "Save & Continue";
                }
                else
                {
                    txtSearch.Text = "";
                    // erase data in the search box
                    Fill_List(ref rsVendorInfo);
                    // put multiple records into a list box so the user can choose which he/she would like
                    frmWait.InstancePtr.Unload();
                    frmInfo.Visible = true;
                    // make the list of records visible
                    vs1.Editable = FCGrid.EditableSettings.flexEDNone;
                    if (lstRecords.Items.Count > 0)
                    {
                        lstRecords.SelectedIndex = 0;
                        lstRecords.Focus();
                    }
                    btnProcessSave.Text = "Retrieve Record";
                    // show listbox with all records
                }
            }
            else
            {
                // else tell them that no records were found that matched the criteria
                frmWait.InstancePtr.Unload();
                //Application.DoEvents();
                MessageBox.Show("No records were found that match that name.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtVendor.Enabled = true;
                for (counter = 0; counter <= 3; counter++)
                {
                    txtAddress[counter].Enabled = true;
                }
                txtCity.Enabled = true;
                txtState.Enabled = true;
                txtZip.Enabled = true;
                txtZip4.Enabled = true;
                // enable all the controls on the form
                cboFrequency.Enabled = true;
                txtUntil.Enabled = true;
                txtDescription.Enabled = true;
                txtReference.Enabled = true;
                txtAmount.Enabled = true;
                txtCheck.Enabled = true;
                vs1.Enabled = true;
                txtVendor.Focus();
                btnProcessSave.Text = "Save & Continue";
            }
        }

        public void cmdSearch_Click()
        {
            cmdSearch_Click(cmdSearch, new System.EventArgs());
        }

        private void cmdSelect_Click(object sender, System.EventArgs e)
        {
            int counter;
            for (counter = 1; counter <= vs4.Rows - 1; counter++)
            {
                vs4.TextMatrix(counter, 0, FCConvert.ToString(-1));
            }
        }

        private void frmAPDataEntry_Activated(object sender, System.EventArgs e)
        {
            if (modGlobal.FormExist(this))
            {
                return;
            }

            txtVendor.Focus();
        }

        private void frmAPDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.F10)
            {
                KeyCode = (Keys)0;
            }
            if (ActiveControl.GetType().Name.ToUpper() == "T2KOVERTYPEBOX")
            {
                if (KeyCode == Keys.Enter)
                    e.Handled = true;
            }
            if (Strings.UCase(ActiveControl.GetName()) == "VS1")
            {
                if (vs1.Col == AccountCol && vs1.Row > 0)
                {
                    modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
                }
            }
            else if (Strings.UCase(ActiveControl.GetName()) == "VSALTCASHACCOUNT")
            {
                modNewAccountBox.CheckFormKeyDown(vsAltCashAccount, vsAltCashAccount.Row, vsAltCashAccount.Col, KeyCode, Shift, vsAltCashAccount.EditSelStart, vsAltCashAccount.EditText, vsAltCashAccount.EditSelLength);
            }
        }

        private void frmAPDataEntry_Load(object sender, System.EventArgs e)
        {

            blnJournalLocked = false;
            blnSaved = false;
            modGlobalFunctions.SetTRIOColors(this, false);
            SetCustomFormColors();
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            vsGrid.GRID7Light = vsAltCashAccount;
            vsDetailGrid.GRID7Light = vs1;
            if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
            {
                LoadDeptCombo();
            }
            vsGrid.DefaultAccountType = "G";
            vsDetailGrid.DefaultAccountType = "E";
            vsGrid.AccountCol = -1;
            vsDetailGrid.AccountCol = 2;
            vsDetailGrid.AllowSplits = modRegionalTown.IsRegionalTown();
            RetrieveInfo();
            // If UCase(MuniName) = "LISBON" Or UCase(MuniName) = "LINCOLN" Then
            mnuFileShowPurchaseOrders.Visible = true;
            // Else
            // mnuFileShowPurchaseOrders.Visible = False
            // End If
            Refresh();
            vsAltCashAccount.ColWidth(0, FCConvert.ToInt32((vsAltCashAccount.WidthOriginal * 0.97)));
            //FC:FINAL:MSH - Issue #791: enable btn for next entry(if it exists)
            if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == false)
            {
                btnProcessNextEntry.Enabled = true;
            }
            vs1.EditingControlShowing -= Vs1_EditingControlShowing;
            vs1.EditingControlShowing += Vs1_EditingControlShowing;
            frmWait.InstancePtr.Refresh();
            int counter;
            clsDRWrapper rsProjects = new clsDRWrapper();
            vsPurchaseOrders.ColHidden(0, true);
            if (!FromVendorMaster)
            {
                if (!SearchFlag)
                {
                    if (!EncumbranceFlag)
                    {
                        if (modBudgetaryMaster.Statics.EncumbranceDetail != null)
                        {
                            FCUtils.EraseSafe(modBudgetaryMaster.Statics.EncumbranceDetail);
                        }
                        NumberOfEncumbrances = 0;
                    }
                    GotEncData = false;
                    GotPOData = false;
                    NumberCol = 0;
                    DescriptionCol = 1;
                    AccountCol = 2;
                    TaxCol = 3;
                    // initialize columns
                    ProjectCol = 4;
                    AmountCol = 5;
                    DiscountCol = 6;
                    EncumbranceCol = 7;
                    vs1.ColHidden(NumberCol, true);
                    vs1.ColWidth(DescriptionCol, 3200);
                    vs1.ColWidth(AccountCol, 2100);
                    vs1.ColWidth(TaxCol, 700);
                    // set column widths
                    vs1.ColWidth(ProjectCol, 700);
                    //FC:FINAL:DDU:#1191 - aligned grids columns
                    vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, EncumbranceCol, 4);
                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, TaxCol, vs1.Rows - 1, TaxCol, 4);
                    vs1.TextMatrix(0, DescriptionCol, "Description");
                    vs1.TextMatrix(0, AccountCol, "Account");
                    vs1.TextMatrix(0, TaxCol, "1099");
                    // set column headings
                    vs1.TextMatrix(0, ProjectCol, "Proj");
                    vs1.TextMatrix(0, AmountCol, "Amount");
                    vs1.TextMatrix(0, DiscountCol, "Disc");
                    vs1.TextMatrix(0, EncumbranceCol, "Enc");
                    vs1.ColFormat(AmountCol, "#,###.00");
                    vs1.ColFormat(DiscountCol, "#,###.00");
                    // set column formats
                    vs1.ColFormat(EncumbranceCol, "#,###.00");
                    vs1.ColComboList(TaxCol, "D|N|1|2|3|4|5|6|7|8|9");
                    vs1.Row = 1;
                    vs1.Col = 0;
                    strComboList = "# ; " + "\t" + " |";
                    if (modBudgetaryMaster.Statics.ProjectFlag)
                    {
                        rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
                        if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
                        {
                            do
                            {
                                strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
                                rsProjects.MoveNext();
                            }
                            while (rsProjects.EndOfFile() != true);
                            strComboList = Strings.Left(strComboList, strComboList.Length - 1);
                        }
                        vs1.ColComboList(ProjectCol, strComboList);
                    }
                    vs3.ColWidth(1, FCConvert.ToInt32(vs3.WidthOriginal * 0.17));
                    vs3.ColWidth(2, FCConvert.ToInt32(vs3.WidthOriginal * 0.08));
                    vs3.ColWidth(3, FCConvert.ToInt32(vs3.WidthOriginal * 0.35));
                    // set encumbrance flexgrid column widths
                    vs3.ColWidth(4, FCConvert.ToInt32(vs3.WidthOriginal * 0.27));
                    vs3.ColWidth(5, FCConvert.ToInt32(vs3.WidthOriginal * 0.13));
                    vs3.ColWidth(6, 0);
                    //FC:FINAL:BSE #3438 column should be hidden
                    vs3.ColHidden(0, true);
                    vs3.TextMatrix(0, 1, "Journal#");
                    vs3.TextMatrix(0, 2, "Vndr#");
                    vs3.TextMatrix(0, 3, "Vendor Name");
                    // set encumbrance flexgrid column headings
                    vs3.TextMatrix(0, 4, "Description");
                    vs3.TextMatrix(0, 5, "PO#");
                    //FC:FINAL:BSE #3438 grid should be aligned left
                    vs3.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs3.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs3.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs3.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs3.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    //vs3.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, 4);
                    vsPurchaseOrders.ColWidth(1, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.2366304));
                    vsPurchaseOrders.ColWidth(2, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.0866304));
                    vsPurchaseOrders.ColWidth(3, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.3288466));
                    // set encumbrance flexgrid column widths
                    vsPurchaseOrders.ColWidth(4, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.2254072));
                    vsPurchaseOrders.ColWidth(5, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.1042099));
                    vsPurchaseOrders.ColWidth(6, 0);
                    vsPurchaseOrders.TextMatrix(0, 1, "Department");
                    vsPurchaseOrders.TextMatrix(0, 2, "Vndr#");
                    vsPurchaseOrders.TextMatrix(0, 3, "Vendor Name");
                    // set encumbrance flexgrid column headings
                    vsPurchaseOrders.TextMatrix(0, 4, "Description");
                    vsPurchaseOrders.TextMatrix(0, 5, "PO#");
                    //vsPurchaseOrders.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, 4);
                    vs4.TextMatrix(0, 1, "Description");
                    vs4.TextMatrix(0, 2, "Account");
                    vs4.TextMatrix(0, 3, "Proj");
                    vs4.TextMatrix(0, 4, "Amount");
                    vs4.TextMatrix(0, 5, "Pending");
                    vs4.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
                    vs4.ColWidth(0, FCConvert.ToInt32(vs4.WidthOriginal * 0.0716215));
                    vs4.ColWidth(1, FCConvert.ToInt32(vs4.WidthOriginal * 0.3282148));
                    vs4.ColWidth(2, FCConvert.ToInt32(vs4.WidthOriginal * 0.2020191));
                    vs4.ColWidth(3, FCConvert.ToInt32(vs4.WidthOriginal * 0.0716215));
                    vs4.ColWidth(4, FCConvert.ToInt32(vs4.WidthOriginal * 0.1628708));
                    //FC:FINAL:BSE #3438 grid should be aligned
                    vs4.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs4.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs4.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    vs4.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vs4.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    //vs4.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, 4);
                    if (!modBudgetaryMaster.Statics.blnAPEdit)
                    {
                        FillJournalCombo();
                        for (counter = 1; counter <= vs1.Rows - 1; counter++)
                        {
                            vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
                            // if we are inputting then initialize data and account formats
                            vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
                        }
                        txtPayable.Text = Strings.Format(datPayableDate, "MM/dd/yyyy");
                        // set date and period
                        //txtPeriod.Text = CalculatePeriod();
                        if (!OldJournal)
                        {
                            cboJournal.SelectedIndex = 0;
                            cboSaveJournal.SelectedIndex = 0;
                        }
                        else
                        {
                            SetCombo(OldJournalNumber);
                        }
                        AmountToPay = 0;
                        TotalAmount = 0;
                        // initialize money amounts
                        TotalDiscount = 0;
                    }
                    else
                    {
                        FillJournalCombo();
                        SetCombo(OldJournalNumber);
                        // calculate amounts and display results
                        AmountToPay = FCConvert.ToDouble(txtAmount.Text);
                        CalculateTotals();
                        lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                        lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                    }
                    if (lblReadOnly.Visible == false)
                    {
                        txtAmount.Focus();
                        txtUntil.Focus();
                        txtVendor.Focus();
                    }
                    Form_Resize();
                    Refresh();
                    CurrJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                    // initialize current journal
                }
                else
                {
                    SearchFlag = false;
                    // if we were searching reset the search flag
                }
            }
            else
            {
                FromVendorMaster = false;
            }
        }
        //FC:FINAL:MSH - Issue #769: add handler if edit control of vs1 table is activated
        private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                //FC:FINAL:MSH - Issue #385: in VB vs1_KeyDownEdit event is executed when key pressed in edit area
                e.Control.KeyDown -= vs1_KeyDownEdit;
                e.Control.KeyDown += vs1_KeyDownEdit;
            }
        }

        private void frmAPDataEntry_Resize(object sender, System.EventArgs e)
        {
            vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.25));
            vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.20));
            vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
            vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
            // resize flexgrid columns
            vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
            vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
            vs3.ColWidth(1, FCConvert.ToInt32(vs3.WidthOriginal * 0.1174496));
            vs3.ColWidth(2, FCConvert.ToInt32(vs3.WidthOriginal * 0.0866304));
            vs3.ColWidth(3, FCConvert.ToInt32(vs3.WidthOriginal * 0.3580273));
            // set encumbrance flexgrid column widths
            vs3.ColWidth(4, FCConvert.ToInt32(vs3.WidthOriginal * 0.2754072));
            vs3.ColWidth(5, FCConvert.ToInt32(vs3.WidthOriginal * 0.1442099));
            vs3.ColWidth(6, 0);
            vsPurchaseOrders.ColWidth(1, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.2366304));
            vsPurchaseOrders.ColWidth(2, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.0866304));
            vsPurchaseOrders.ColWidth(3, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.3288466));
            // set encumbrance flexgrid column widths
            vsPurchaseOrders.ColWidth(4, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.2254072));
            vsPurchaseOrders.ColWidth(5, FCConvert.ToInt32(vsPurchaseOrders.WidthOriginal * 0.1042099));
            vsPurchaseOrders.ColWidth(6, 0);
            vs4.ColWidth(0, FCConvert.ToInt32(vs4.WidthOriginal * 0.0586751));
            vs4.ColWidth(1, FCConvert.ToInt32(vs4.WidthOriginal * 0.2882148));
            vs4.ColWidth(2, FCConvert.ToInt32(vs4.WidthOriginal * 0.2620191));
            vs4.ColWidth(3, FCConvert.ToInt32(vs4.WidthOriginal * 0.0716215));
            vs4.ColWidth(4, FCConvert.ToInt32(vs4.WidthOriginal * 0.1628708));
            fraAccountBalance.CenterToContainer(ClientArea);
            fraPurchaseOrders.CenterToContainer(ClientArea, maximize: true);
            fraJournalSave.CenterToContainer(ClientArea);
            Frame2.CenterToContainer(ClientArea);
            fraEncDetail.CenterToContainer(ClientArea, maximize: true);
            frmSearch.CenterToContainer(ClientArea);
            frmInfo.CenterToContainer(ClientArea);
        }

        public void Form_Resize()
        {
            frmAPDataEntry_Resize(this, new System.EventArgs());
        }

        private void FrmAPDataEntry_FormClosed(object sender, FormClosedEventArgs e)
        {
            //FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
            if (openFormWhenThisClosed)
            {
                frmGetAPDataEntry.InstancePtr.Show(App.MainForm);
            }
        }


        // vbPorter upgrade warning: Cancel As short	OnWrite(bool)
        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            if (!modBudgetaryMaster.Statics.gboolClosingProgram)
            {
                if (!blnSaved)
                {
                    ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                if (blnJournalLocked)
                {
                    modBudgetaryAccounting.UnlockJournal();
                }
                //FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
                //FC:FINAL:MSH - Issue #708: showing new form before closing current form not always set focus to the new form.
                // Hiding current form before closing gives opportunity to set focus correctly
                //this.Hide();
                openFormWhenThisClosed = true;
            }
            else
            {
                if (blnJournalLocked)
                {
                    modBudgetaryAccounting.UnlockJournal();
                }
            }
        }

        private void frmAPDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                if (Frame2.Visible == true)
                {
                    Frame2.Visible = false;
                    txtVendor.Text = "";
                    txtVendor.Focus();
                }
                else if (fraPurchaseOrders.Visible == true)
                {
                    fraPurchaseOrders.Visible = false;
                    txtVendor.Text = "";
                    txtVendor.Focus();
                }
                else if (fraEncDetail.Visible == true)
                {
                    cmdNo_Click();
                }
                else if (fraJournalSave.Visible == true)
                {
                    modBudgetaryAccounting.UnlockJournal();
                    blnJournalLocked = false;
                    if (Master.IsntAnything())
                    {
                        // do nothing
                    }
                    else
                    {
                        Master.Reset();
                    }
                    fraJournalSave.Visible = false;
                }
                else
                {
                    KeyAscii = (Keys)0;
                    Close();
                }
            }
            else if (KeyAscii == Keys.Return)
            {
                if (ActiveControl.GetName() == "lstRecords")
                {
                    KeyAscii = (Keys)0;
                    cmdRetrieve_Click();
                }
                else if (ActiveControl.GetName() != "vs1" && ActiveControl.GetName() != "txtSearch" && ActiveControl.GetName() != "vs3" && ActiveControl.GetName() != "vsPurchaseOrders")
                {
                    //KeyAscii = (Keys)0;
                    e.KeyChar = '0';
                    Support.SendKeys("{TAB}", false);
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void lblExpense_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        {
            MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
            int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
            float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
            float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
            string strAcct = "";
            if (Button == MouseButtonConstants.RightButton)
            {
                strAcct = vs1.TextMatrix(vs1.Row, AccountCol);
                if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
                {
                    if (Strings.Left(strAcct, 1) != "E" && Strings.Left(strAcct, 1) != "P")
                    {
                        // do nothing
                    }
                    else
                    {
                        lblBalAccount.Text = "Account: " + strAcct;
                        lblBalNetBudget.Text = Strings.Format(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct), "#,##0.00");
                        lblBalPostedYTDNet.Text = Strings.Format(GetYTDNet(strAcct) + GetEncumbrance(strAcct), "#,##0.00");
                        lblBalPendingYTDNet.Text = Strings.Format(GetPendingYTDNet(strAcct), "#,##0.00");
                        lblBalBalance.Text = Strings.Format(FCConvert.ToDouble(lblBalNetBudget.Text) - FCConvert.ToDouble(lblBalPostedYTDNet.Text) - FCConvert.ToDouble(lblBalPendingYTDNet.Text), "#,##0.00");
                        fraAccountBalance.Visible = true;
                    }
                }
            }
        }

        private void lstRecords_DoubleClick(object sender, System.EventArgs e)
        {
            cmdRetrieve_Click();
        }

        private void mnuFileAddVendor_Click(object sender, System.EventArgs e)
        {
            //FC:FINA:KV:IIT807+FC-8697
            Hide();
            modBudgetaryMaster.Statics.blnEdit = false;
            // else show we are creating a new vendor account
            modBudgetaryMaster.Statics.blnFromAP = true;
            frmVendorMaster.InstancePtr.Show(App.MainForm);
            // show the blankform
        }

        private void mnuFileChangePayableDate_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: datTempDate As string	OnRead(DateTime)
            object datTempDate;
            bool blnNoCancel;
            DateTime datLowDate;
            DateTime datHighDate;
            datTempDate = Strings.Format(datPayableDate, "MM/dd/yyyy");
            //FC:FINAL:RPU-#388: Make the field larger 
            //blnNoCancel = frmInput.InstancePtr.Init(ref datTempDate, "Enter Payable Date", "Please enter the new payable date.", 1440);
            blnNoCancel = frmInput.InstancePtr.Init(ref datTempDate, "Enter Payable Date", "Please enter the new payable date.", 1700);
            if (blnNoCancel && Information.IsDate(datTempDate))
            {
                //FC:FINAL:AM:#389 - convert to DateTime
                DateTime temp = Convert.ToDateTime(datTempDate);
                datLowDate = DateAndTime.DateAdd("m", -1, DateTime.Today);
                datHighDate = DateAndTime.DateAdd("m", 1, DateTime.Today);
                if (temp >= datLowDate && temp <= datHighDate)
                {
                    datPayableDate = temp;
                    txtPayable.Text = Strings.Format(datPayableDate, "MM/dd/yyyy");
                }
                else
                {
                    MessageBox.Show("The payable date must be less then a month from todays date.", "Invalid Payable Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }

        private void mnuFileDisplayAmount_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsAmount = new clsDRWrapper();
            int lngJournal = 0;
            // vbPorter upgrade warning: curAmount As Decimal	OnWrite(double, short)
            Decimal curAmount;
            if (cboJournal.SelectedIndex > 0)
            {
                lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Text, 4))));
                rsAmount.OpenRecordset("SELECT SUM(APJournalDetail.Amount) as TotalAmount, SUM(APJournalDetail.Discount) as TotalDiscount FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE (APJournal.Status = 'E' or APJournal.Status = 'V') AND APJournal.JournalNumber = " + FCConvert.ToString(lngJournal));
                if (rsAmount.EndOfFile() != true && rsAmount.BeginningOfFile() != true)
                {
                    // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                    curAmount = FCConvert.ToDecimal(rsAmount.Get_Fields_Decimal("TotalAmount") - rsAmount.Get_Fields("TotalDiscount"));
                }
                else
                {
                    curAmount = 0;
                }
                MessageBox.Show("The total pending amount for journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngJournal), 4) + " is " + Strings.Format(curAmount, "#,##0.00"), "Pending Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("You must select a journal before you may proceed.", "Select Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void mnuFileSave_Click(object sender, System.EventArgs e)
        {
            blnUnload = false;
            boolF12 = false;
            SaveJournalInfo();
        }

        private void mnuFileVendorDetail_Click(object sender, System.EventArgs e)
        {
            if (Conversion.Val(txtVendor.Text) > 0)
            {
                rptVendorDetail.InstancePtr.blnFromAP = true;
                rptVendorDetail.InstancePtr.lngVendor = FCConvert.ToInt32(Math.Round(Conversion.Val(txtVendor.Text)));
                rptVendorDetail.InstancePtr.Init("1", "A", "A", false, Modal);
            }
            else
            {
                if (txtVendor.Text == "" && Strings.Trim(txtAddress[0].Text) != "")
                {
                    rptVendorDetail.InstancePtr.blnFromAP = true;
                    rptVendorDetail.InstancePtr.lngVendor = FCConvert.ToInt32(Math.Round(Conversion.Val(txtVendor.Text)));
                    rptVendorDetail.InstancePtr.strVendor = Strings.Trim(txtAddress[0].Text);
                    rptVendorDetail.InstancePtr.Init("1", "A", "A", false, Modal);
                }
                else
                {
                    MessageBox.Show("You must select a vendor before you may use this option.", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void mnuProcessDelete_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: counter As short, int --> As DialogResult
            int counter;
            int temp = 0;
            // if the row has no data in it then do nothing
            if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, TaxCol) == "" && vs1.TextMatrix(vs1.Row, ProjectCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0" && vs1.TextMatrix(vs1.Row, DiscountCol) == "0" && vs1.TextMatrix(vs1.Row, EncumbranceCol) == "0")
            {
                // do nothing
            }
            else
            {
                // if the row is not empty then highlight the row
                vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
                vs1.Select(vs1.Row, EncumbranceCol, vs1.Row, NumberCol);
                // make sure they want to delete this item
                counter = FCConvert.ToInt32(MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                // if they do
                if (counter == FCConvert.ToInt32(DialogResult.Yes))
                {
                    // set the delete flag to true and remember the row to delete
                    DeleteFlag = true;
                    temp = vs1.Row;
                    // move the cursor so everythig gets deleted
                    if (vs1.Row < vs1.Rows - 1)
                    {
                        vs1.Row += 1;
                    }
                    else
                    {
                        vs1.Row -= 1;
                    }
                    // if we are deleting an encumbrance
                    if (EncumbranceFlag)
                    {
                        if (temp <= NumberOfEncumbrances)
                        {
                            // delete the encumbrance from our encumbrance array
                            for (counter = temp - 1; counter <= NumberOfEncumbrances - 2; counter++)
                            {
                                modBudgetaryMaster.Statics.EncumbranceDetail[counter] = modBudgetaryMaster.Statics.EncumbranceDetail[counter + 1];
                                modBudgetaryMaster.Statics.EncumbranceDetailRecords[counter] = modBudgetaryMaster.Statics.EncumbranceDetailRecords[counter + 1];
                            }
                            // change the size of the array and decrement how many encumbrances we have
                            Array.Resize(ref modBudgetaryMaster.Statics.EncumbranceDetail, NumberOfEncumbrances - 1 + 1);
                            Array.Resize(ref modBudgetaryMaster.Statics.EncumbranceDetailRecords, NumberOfEncumbrances - 1 + 1);
                            NumberOfEncumbrances -= 1;
                        }
                    }
                    // delete the row and add a new opne to the bottom of the grid
                    vs1.RemoveItem(temp);
                    vs1.Rows += 1;
                    vs1.TextMatrix(vs1.Rows - 1, EncumbranceCol, FCConvert.ToString(0));
                    vs1.TextMatrix(vs1.Rows - 1, DiscountCol, FCConvert.ToString(0));
                    vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
                    vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Rows - 1, TaxCol, 4);
                    // recalculate the totals then show amount left to pay
                    CalculateTotals();
                    lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                    lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                    // reset the delete flag and turn off highlighitng
                    DeleteFlag = false;
                    vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                    // set control back to cell we were at
                    vs1.Select(temp, DescriptionCol);
                    // if they dont want to then set ocntrol back to cell we were at
                }
                else
                {
                    vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                    vs1.Select(vs1.Row, DescriptionCol);
                }
            }
        }

        private void mnuProcessDeleteEntry_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: counter As short, int --> As DialogResult
            DialogResult counter;
            clsDRWrapper rsDetail = new clsDRWrapper();
            int lngRecordToDelete = 0;
            clsDRWrapper rsJournalEntriesLeft = new clsDRWrapper();
            clsDRWrapper rsPurchaseOrders = new clsDRWrapper();
            // make sure they want to delete this item
            counter = MessageBox.Show("Are you sure you want to delete this journal entry?", "Delete This Journal Entry?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (counter == DialogResult.Yes)
            {
                modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                if (PurchaseOrderFlag)
                {
                    rsPurchaseOrders.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + FCConvert.ToString(ChosenPurchaseOrder), "TWBD0000.vb1");
                    if (rsPurchaseOrders.EndOfFile() != true && rsPurchaseOrders.BeginningOfFile() != true)
                    {
                        rsPurchaseOrders.Edit();
                        rsPurchaseOrders.Set_Fields("Closed", false);
                        rsPurchaseOrders.Update();
                    }
                }
                rsDetail.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"), "Budgetary");
                if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
                {
                    if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                    {
                        lngRecordToDelete = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                        modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
                        if (lngRecordToDelete == FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")))
                        {
                            modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                            FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                            apJournalId = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                            modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                        }
                        else
                        {
                            FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                            apJournalId = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                            do
                            {
                                modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                            }
                            while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != lngRecordToDelete);
                        }
                        modBudgetaryAccounting.Statics.SearchResults.Delete();
                        modBudgetaryAccounting.Statics.SearchResults.Update();
                        modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
                        GetJournalData();
                        btnProcessPreviousEntry.Enabled = false;
                        if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
                        {
                            btnProcessNextEntry.Enabled = true;
                        }
                        else
                        {
                            btnProcessNextEntry.Enabled = false;
                        }
                    }
                    else
                    {
                        modBudgetaryAccounting.Statics.SearchResults.Delete();
                        modBudgetaryAccounting.Statics.SearchResults.Update();
                        if (btnProcessNextEntry.Enabled)
                        {
                            if (btnProcessPreviousEntry.Enabled == false)
                            {
                                mnuProcessNextEntry_Click();
                                btnProcessPreviousEntry.Enabled = false;
                            }
                            else
                            {
                                mnuProcessNextEntry_Click();
                            }
                        }
                        else
                        {
                            mnuProcessPreviousEntry_Click();
                            btnProcessNextEntry.Enabled = false;
                        }
                    }
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    rsJournalEntriesLeft.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber") + " AND Status = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status") + "' AND ID <> " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
                    if (rsJournalEntriesLeft.EndOfFile() != true && rsJournalEntriesLeft.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        rsDetail.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status") + "' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
                        if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
                        {
                            rsDetail.Edit();
                            rsDetail.Set_Fields("Status", "D");
                            rsDetail.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                            rsDetail.Set_Fields("StatusChangeDate", DateTime.Today);
                            rsDetail.Update();
                        }
                    }
                    modBudgetaryAccounting.Statics.SearchResults.Delete();
                    modBudgetaryAccounting.Statics.SearchResults.Update();
                    //Application.DoEvents();
                    frmGetAPDataEntry.InstancePtr.FillJournals();
                    blnSaved = true;
                    ProcessQuit();
                }
            }
        }

        private void mnuProcessEncumbrance_Click(object sender, System.EventArgs e)
        {
            string strDept = "";
            mnuProcessEncumbrance.Enabled = false;
            // show encumbrance list
            if (!FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
            {
                lblDepartment.Visible = false;
                cboEncDept.Visible = false;
                GetEncData();
                //FC:FINAL:SBE - #4483 - workaround to check if grid was populated
                //if (vs3.TextMatrix(1, 1) == "")
                if (vs3.Rows < 2)
                {
                    //Application.DoEvents();
                    txtVendor.Text = "";
                    MessageBox.Show("No Encumbrance Information Found", "No Encumbrances", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    mnuProcessEncumbrance.Enabled = true;
                    return;
                }
                Frame2.Visible = true;
                vs3.Row = 1;
                vs3.Focus();
            }
            else
            {
                strDept = modRegistry.GetRegistryKey("CURENCDEPT");
                if (Strings.Trim(strDept) != "")
                {
                    SetEncDeptCombo(ref strDept);
                }
                else
                {
                    cboEncDept.SelectedIndex = -1;
                }
                lblDepartment.Visible = true;
                cboEncDept.Visible = true;
                vs3.Rows = 1;
                Frame2.Visible = true;
                cboEncDept.Focus();
            }
        }

        public void mnuProcessEncumbrance_Click()
        {
            mnuProcessEncumbrance_Click(mnuProcessEncumbrance, new System.EventArgs());
        }

        private void mnuFileShowPurchaseOrders_Click(object sender, System.EventArgs e)
        {
            string strDept = "";
            mnuFileShowPurchaseOrders.Enabled = false;
            // show encumbrance list
            if (!FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
            {
                lblPurchaseOrderDepartment.Visible = false;
                cboPurchaseOrderDepartment.Visible = false;
                GetPOData();
                if (vsPurchaseOrders.TextMatrix(1, 1) == "")
                {
                    //Application.DoEvents();
                    txtVendor.Text = "";
                    MessageBox.Show("No Purchase Order Information Found", "No Purchase Orders", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    mnuFileShowPurchaseOrders.Enabled = true;
                    return;
                }
                fraPurchaseOrders.Visible = true;
                vsPurchaseOrders.Row = 1;
                vsPurchaseOrders.Focus();
            }
            else
            {
                strDept = modRegistry.GetRegistryKey("CURENCDEPT");
                if (Strings.Trim(strDept) != "")
                {
                    SetPurchaseOrderDeptCombo(ref strDept);
                }
                else
                {
                    cboEncDept.SelectedIndex = -1;
                }
                lblPurchaseOrderDepartment.Visible = true;
                cboPurchaseOrderDepartment.Visible = true;
                vsPurchaseOrders.Rows = 1;
                fraPurchaseOrders.Visible = true;
                cboPurchaseOrderDepartment.Focus();
            }
        }

        public void mnuFileShowPurchaseOrders_Click()
        {
            mnuFileShowPurchaseOrders_Click(mnuFileShowPurchaseOrders, new System.EventArgs());
        }

        private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
        {
            vs1.Row = 1;
            //Application.DoEvents();
            txtVendor.Focus();
            if (apJournalId == FirstRecordShown)
            {
                // move to next entry
                modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
                if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                {
                    modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                    modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                    if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
                    {
                        btnProcessNextEntry.Enabled = false;
                    }
                    modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                }
                else
                {
                    modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                    if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                    {
                        modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                        if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
                        {
                            btnProcessNextEntry.Enabled = false;
                        }
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                    }
                    else
                    {
                        if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
                        {
                            btnProcessNextEntry.Enabled = false;
                        }
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                    }
                }
            }
            else
            {
                modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                {
                    modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                }
                modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
                {
                    if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                    {
                        modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                        if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
                        {
                            btnProcessNextEntry.Enabled = false;
                        }
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                    }
                    else
                    {
                        if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
                        {
                            btnProcessNextEntry.Enabled = false;
                        }
                        modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                    }
                }
                else
                {
                    btnProcessNextEntry.Enabled = false;
                    modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                }
            }
            if (btnProcessPreviousEntry.Enabled == false)
            {
                btnProcessPreviousEntry.Enabled = true;
            }
            GetJournalData();
        }

        public void mnuProcessNextEntry_Click()
        {
            mnuProcessNextEntry_Click(btnProcessNextEntry, new System.EventArgs());
        }

        private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
        {
            vs1.Row = 1;
            //Application.DoEvents();
            txtVendor.Focus();
            modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
            // move to previous record
            if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
            {
                modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != FirstRecordShown)
                {
                    modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                }
                btnProcessPreviousEntry.Enabled = false;
            }
            else
            {
                if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
                {
                    modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
                    if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
                    {
                        modBudgetaryAccounting.Statics.SearchResults.MoveNext();
                        btnProcessPreviousEntry.Enabled = false;
                    }
                }
            }
            if (btnProcessNextEntry.Enabled == false)
            {
                btnProcessNextEntry.Enabled = true;
            }
            GetJournalData();
        }

        public void mnuProcessPreviousEntry_Click()
        {
            mnuProcessPreviousEntry_Click(btnProcessPreviousEntry, new System.EventArgs());
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            if (Frame2.Visible == true)
            {
                Frame2.Visible = false;
                txtVendor.Text = "";
                txtVendor.Focus();
            }
            else if (fraEncDetail.Visible == true)
            {
                cmdNo_Click();
            }
            else if (fraJournalSave.Visible == true)
            {
                modBudgetaryAccounting.UnlockJournal();
                blnJournalLocked = false;
                if (Master.IsntAnything())
                {
                    // do nothing
                }
                else
                {
                    Master.Reset();
                }
                fraJournalSave.Visible = false;
            }
            else
            {
                Close();
            }
        }

        private void ProcessQuit()
        {
            if (Frame2.Visible == true)
            {
                Frame2.Visible = false;
                txtVendor.Text = "";
                txtVendor.Focus();
            }
            else if (fraEncDetail.Visible == true)
            {
                cmdNo_Click();
            }
            else if (fraJournalSave.Visible == true)
            {
                modBudgetaryAccounting.UnlockJournal();
                blnJournalLocked = false;
                if (Master.IsntAnything())
                {
                    // do nothing
                }
                else
                {
                    Master.Reset();
                }
                fraJournalSave.Visible = false;
            }
            else
            {
                // Unload Me
            }
        }

        private string CalculatePeriod()
        {
            string CalculatePeriod = "";
            int temp = 0;

            temp = intSavedPeriod;
            if (temp < 10)
            {
                CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
            }
            else
            {
                CalculatePeriod = Strings.Trim(Conversion.Str(temp));
            }
            return CalculatePeriod;
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            boolF12 = true;
            if (frmSearch.Visible == true)
            {
                cmdSearch_Click();
            }
            else if (frmInfo.Visible == true)
            {
                cmdRetrieve_Click();
            }
            else if (fraJournalSave.Visible == true)
            {
                cmdOKSave_Click();
            }
            else
            {
                blnUnload = true;
                vs1.Col = 0;
                SaveJournalInfo();
                // If blnSaved Then
                // Unload Me
                // End If
            }
        }

        private void ProcessSave()
        {
            if (frmSearch.Visible == true)
            {
                cmdSearch_Click();
            }
            else if (frmInfo.Visible == true)
            {
                cmdRetrieve_Click();
            }
            else if (fraJournalSave.Visible == true)
            {
                cmdOKSave_Click();
            }
            else
            {
                blnUnload = false;
                SaveJournalInfo();
            }
        }

        private void mnuProcessSearch_Click(object sender, System.EventArgs e)
        {
            int counter;
            // show search screen and disable all other controls
            lblExpense.Text = "";
            txtSearch.Text = "";
            frmSearch.Visible = true;
            txtPeriod.Enabled = false;
            cboJournal.Enabled = false;
            txtVendor.Enabled = false;
            for (counter = 0; counter <= 3; counter++)
            {
                txtAddress[counter].Enabled = false;
            }
            txtCity.Enabled = false;
            txtState.Enabled = false;
            txtZip.Enabled = false;
            txtZip4.Enabled = false;
            txtPayable.Enabled = false;
            cboFrequency.Enabled = false;
            txtUntil.Enabled = false;
            txtDescription.Enabled = false;
            txtReference.Enabled = false;
            txtAmount.Enabled = false;
            txtCheck.Enabled = false;
            vs1.Enabled = false;
            txtSearch.Focus();
            btnProcessSave.Text = "Search";
        }

        public void mnuProcessSearch_Click()
        {
            mnuProcessSearch_Click(btnProcessSearch, new System.EventArgs());
        }

        private void mnuViewAttachedDocuments_Click(object sender, System.EventArgs e)
        {
            int lngInvoiceNumber;
            lngInvoiceNumber = apJournalId;
            if (lngInvoiceNumber > 0)
            {                
                StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer(
                    "Invoice " + FCConvert.ToString(lngInvoiceNumber), "Budgetary", "APJournal", lngInvoiceNumber, "", true,
                    false));
            }
            else
            {
                MessageBox.Show("You must save this entry before you can attach a document to it.", "Entry Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtAddress_Enter(int Index, object sender, System.EventArgs e)
        {
            int counter;
            int tempIndex = 0;
            lblExpense.Text = "";
            // dont show a title
            // if we are not doing a vendor search
            if (!SearchFlag)
            {
                // if there is no vendor number entered change it to 0000
                if (txtVendor.Text == "")
                {
                    //                    txtVendor.Text = "0000";
                    //                    // if we are not dealing with a temporary vendor then do to the description field
                    //                    // ElseIf val(txtVendor.Text) <> 0 Then
                }
                else if (blnMoveToDescription)
                {
                    blnMoveToDescription = false;
                    txtDescription.Focus();
                }
                else
                {
                    // if there is a temp vendor make sure the user puts an address in
                    if (Index > 2)
                    {
                        if (txtAddress[Index - 1].Text == "")
                        {
                            if (txtAddress[Index].Text == "")
                            {
                                txtCity.Focus();
                            }
                            else
                            {
                                for (counter = Index - 1; counter >= 0; counter--)
                                {
                                    if (txtAddress[counter].Text == "")
                                    {
                                        tempIndex = counter;
                                    }
                                }
                                txtAddress[FCConvert.ToInt16(tempIndex)].Focus();
                            }
                        }
                    }
                }
            }
        }

        private void txtAddress_Enter(object sender, System.EventArgs e)
        {
            //FC:FINAL:MSH - if the text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type (same with issue #795)
            if (sender.GetType() == typeof(T2KOverTypeBox))
            {
                int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
                txtAddress_Enter(index, sender, e);
            }
        }
        // DJW@10272009 Commented out because people who did not put addresses on first row were getting warnigns about vendor addresses not matching
        // Private Sub txtAddress_Validate(Index As Integer, Cancel As Boolean)
        // Dim counter As Integer
        //
        // if a row is erased then move the other rows back
        // If Index < 3 Then
        // If Trim(txtAddress(Index + 1).Text) <> "" Then
        // If txtAddress(Index).Text = "" Then
        // For counter = Index + 1 To 3
        // txtAddress(counter - 1).Text = txtAddress(counter).Text
        // Next
        // txtAddress(3).Text = ""
        // End If
        // End If
        // End If
        // End Sub
        private void txtAmount_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                txtAmount.Text = "";
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
            txtAmount.SelectionStart = 0;
            txtAmount.SelectionLength = txtAmount.Text.Length;
        }

        private void txtAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //FC:FINAL:DDU:#1725 - check if string is not empty first
            // show how much we have to pay
            if (String.Empty != txtAmount.Text && !Information.IsNumeric(txtAmount.Text))
            {
                MessageBox.Show("You may only put numeric values in this field.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAmount.Text = "0.00";
                txtAmount.SelectionStart = 0;
                txtAmount.SelectionLength = txtAmount.Text.Length;
                e.Cancel = true;
                return;
            }
            else if (String.Empty != txtAmount.Text && FCConvert.ToDecimal(txtAmount.Text) < 0)
            {
                MessageBox.Show("You may only put positive values in this field.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAmount.Text = "0.00";
                txtAmount.SelectionStart = 0;
                txtAmount.SelectionLength = txtAmount.Text.Length;
                e.Cancel = true;
                return;
            }
            else
            {
                txtAmount.Text = Strings.Format(txtAmount.Text, "#,##0.00");
            }
            if (txtAmount.Text != "")
            {
                AmountToPay = FCConvert.ToDouble(txtAmount.Text);
                lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
            }
            else
            {
                AmountToPay = 0;
                lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
            }
        }

        private void txtCheck_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void txtCheck_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
            {
                keyAscii = 0;
            }
        }

        private void txtCheck_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsDupCheckInfo = new clsDRWrapper();
            // make sure they enter a number in the check field
            if (txtCheck.SelectionStart > 0)
            {
                if (!Information.IsNumeric(txtCheck.Text) || Conversion.Val(txtCheck.Text) == 0)
                {
                    MessageBox.Show("You must enter a number other than 0 in this field", "Invalid Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    txtCheck.SelectionStart = 0;
                    txtCheck.SelectionLength = txtCheck.Text.Length;
                }
                else
                {
                    rsDupCheckInfo.OpenRecordset("SELECT * FROM APJournal WHERE CheckNumber = '" + Strings.Trim(txtCheck.Text) + "'");
                    if (rsDupCheckInfo.EndOfFile() != true && rsDupCheckInfo.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                        ans = MessageBox.Show("Check# " + rsDupCheckInfo.Get_Fields("CheckNumber") + " was paid to Vendor " + rsDupCheckInfo.Get_Fields_Int32("VendorNumber") + " on " + Strings.Format(rsDupCheckInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy") + ".  Do you wish to use this check #?", "Check # Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (ans == DialogResult.No)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    // chkUseAltCashAccount.Visible = True
                    // fraAltCashAccount.Visible = True
                    // chkUseAltCashAccount.SetFocus
                }
                // Else
                // chkUseAltCashAccount = vbUnchecked
                // chkUseAltCashAccount.Visible = False
                // fraAltCashAccount.Visible = False
            }
        }

        private void txtCity_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
        }

        private void txtDescription_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void txtPayable_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void txtPayable_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtPayable.IsEmpty)
            {
                txtPayable.Text = Strings.Format(datPayableDate, "MM/dd/yyyy");
            }
            else
            {
                if (Information.IsDate(txtPayable.Text))
                {
                    datPayableDate = DateAndTime.DateValue(txtPayable.Text);
                }
            }
        }

        private void txtPeriod_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            else
            {
                txtPeriod.SelectionStart = 0;
                txtPeriod.SelectionLength = txtPeriod.Text.Length;
            }
            lblExpense.Text = "";
        }

        private void txtPeriod_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if (keyAscii < 48 || keyAscii > 57)
            {
                keyAscii = 0;
            }
        }

        private void txtPeriod_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // make sure they enter a number for the period
            if (!Information.IsNumeric(txtPeriod.Text) || Conversion.Val(txtPeriod.Text) > 12)
            {
                MessageBox.Show("You must enter a number less than or equal to 12 in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
                txtPeriod.SelectionStart = 0;
                txtPeriod.SelectionLength = txtPeriod.Text.Length;
            }
            else
            {
                intSavedPeriod = FCConvert.ToInt16(FCConvert.ToDouble(txtPeriod.Text));
            }
        }

        private void txtReference_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void txtReference_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
            clsDRWrapper rsAPJournals = new clsDRWrapper();
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsInfo = new clsDRWrapper();
            int intDataID = 0;
            string strArchivePath = "";
            cArchiveUtility autPastYear = new cArchiveUtility();
            if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckRef")) == "Y" && Strings.Trim(txtReference.Text) != "")
            {
                if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("SameVendorOnly")))
                {
                    rsAPJournals.OpenRecordset("SELECT * FROM APJournal WHERE Amount >= 0 AND Reference = '" + modCustomReport.FixQuotes(Strings.Trim(txtReference.Text)) + "' AND Returned <> 'R' AND ID <> " + FCConvert.ToString(apJournalId) + " AND VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)));
                }
                else
                {
                    rsAPJournals.OpenRecordset("SELECT * FROM APJournal WHERE Amount >= 0 AND Reference = '" + modCustomReport.FixQuotes(Strings.Trim(txtReference.Text)) + "' AND Returned <> 'R' AND ID <> " + FCConvert.ToString(apJournalId));
                }
                if (rsAPJournals.EndOfFile() != true && rsAPJournals.BeginningOfFile() != true)
                {
                    if (FCConvert.ToString(rsAPJournals.Get_Fields_String("Status")) == "E" || FCConvert.ToString(rsAPJournals.Get_Fields_String("Status")) == "V")
                    {
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        ans = MessageBox.Show("Reference " + Strings.Trim(txtReference.Text) + " is currently being processed for Vendor " + rsAPJournals.Get_Fields_Int32("VendorNumber") + " in Journal# " + rsAPJournals.Get_Fields("JournalNumber") + ".  Do you wish to continue?", "Reference # Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                        ans = MessageBox.Show("Reference " + Strings.Trim(txtReference.Text) + " was paid to Vendor " + rsAPJournals.Get_Fields_Int32("VendorNumber") + " with Check# " + rsAPJournals.Get_Fields("CheckNumber") + " on " + Strings.Format(rsAPJournals.Get_Fields_DateTime("CheckDate"), "MM/dd/yy") + ".  Do you wish to continue?", "Reference # Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    if (ans == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
                rsInfo.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    clsDRWrapper rsAPJournals2 = new clsDRWrapper();
                    intDataID = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ArchiveID"));
                    strArchivePath = autPastYear.DeriveGroupName("Archive", intDataID);
                    rsAPJournals2.GroupName = strArchivePath;
                    if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("SameVendorOnly")))
                    {
                        rsAPJournals2.OpenRecordset("SELECT * FROM APJournal WHERE Amount >= 0 AND Reference = '" + modCustomReport.FixQuotes(Strings.Trim(txtReference.Text)) + "' AND Returned <> 'R' AND ID <> " + FCConvert.ToString(apJournalId) + " AND VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)), "Budgetary");
                    }
                    else
                    {
                        rsAPJournals2.OpenRecordset("SELECT * FROM APJournal WHERE Amount >= 0 AND Reference = '" + modCustomReport.FixQuotes(Strings.Trim(txtReference.Text)) + "' AND Returned <> 'R' AND ID <> " + FCConvert.ToString(apJournalId), "Budgetary");
                    }
                    if (rsAPJournals2.EndOfFile() != true && rsAPJournals2.BeginningOfFile() != true)
                    {
                        if (FCConvert.ToString(rsAPJournals2.Get_Fields_String("Status")) == "E" || FCConvert.ToString(rsAPJournals2.Get_Fields_String("Status")) == "V")
                        {
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            ans = MessageBox.Show("Reference " + Strings.Trim(txtReference.Text) + " is currently being processed for Vendor " + rsAPJournals2.Get_Fields_Int32("VendorNumber") + " in Journal# " + rsAPJournals2.Get_Fields("JournalNumber") + ".  Do you wish to continue?", "Reference # Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            ans = MessageBox.Show("Reference " + Strings.Trim(txtReference.Text) + " was paid to Vendor " + rsAPJournals2.Get_Fields_Int32("VendorNumber") + " with Check# " + rsAPJournals2.Get_Fields("CheckNumber") + " on " + Strings.Format(rsAPJournals2.Get_Fields_DateTime("CheckDate"), "MM/dd/yy") + ".  Do you wish to continue?", "Reference # Paid", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        }
                        if (ans == DialogResult.No)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // if they hit enter after typing in search string act a if they hit the search button
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                cmdSearch_Click();
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtState_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
        }

        private void txtUntil_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                txtUntil.Text = "";
                //txtAddress[0].Focus();
                AdvanceToVendor();
            }
            lblExpense.Text = "";
        }

        private void txtVendor_Enter(object sender, System.EventArgs e)
        {
            txtVendor.Select(0, 1);
            if (lblReadOnly.Visible == true)
            {
                btnProcessSearch.Enabled = false;
                mnuProcessEncumbrance.Enabled = false;
                mnuFileShowPurchaseOrders.Enabled = false;
                mnuFileAddVendor.Enabled = false;
            }
            else
            {
                btnProcessSearch.Enabled = true;
                mnuProcessEncumbrance.Enabled = true;
                mnuFileShowPurchaseOrders.Enabled = true;
                mnuFileAddVendor.Enabled = true;
            }
            lblExpense.Text = "";
        }

        private void txtVendor_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.A)
            {
                //FC:FINA:KV:IIT807+FC-8697
                Hide();
                modBudgetaryMaster.Statics.blnEdit = false;
                // else show we are creating a new vendor account
                modBudgetaryMaster.Statics.blnFromAP = true;
                frmVendorMaster.InstancePtr.Show(App.MainForm);
                // show the blankform
            }
            else if (KeyCode == Keys.E)
            {
                mnuProcessEncumbrance_Click();
            }
            else if (KeyCode == Keys.P)
            {
                // If UCase(MuniName) = "LISBON" Or UCase(MuniName) = "LINCOLN" Then
                mnuFileShowPurchaseOrders_Click();
                // End If
            }
            else if (KeyCode == Keys.S)
            {
                mnuProcessSearch_Click();
            }
        }

        public void txtVendor_Validate(bool validate)
        {
            txtVendor_Validate(txtVendor, new System.ComponentModel.CancelEventArgs(validate));
        }

        public void txtVendor_Validate(object sender, System.ComponentModel.CancelEventArgs e)
        {
           
            string temp = "";
            // make sure there
            if ((txtVendor.SelectionStart > 0 && Conversion.Val(txtVendor.Text) != 0) || blnFromVendor)
            {
                blnFromVendor = false;
                if (!Information.IsNumeric(txtVendor.Text))
                {
                    MessageBox.Show("You may only enter a number in this field", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtVendor.Text = "";
                    e.Cancel = true;
                }
                else
                {
                    temp = txtVendor.Text;
                    if (!string.IsNullOrWhiteSpace(temp))
                    {
                        txtVendor.Text = Strings.Format(temp, "00000");
                    }

                    if (!GetVendorInfo())
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        blnMoveToDescription = true;
                    }
                }
            }
            else
            {
                // if we are not searching and the value.IsntAnything then set vendor number to 0000
                if (!SearchFlag )
                {
                    if (txtVendor.Text == "" || Conversion.Val(txtVendor.Text) == 0)
                    {
                        txtVendor.Text = "";
                       // txtAddress[0].Focus();
                    }
                }
            }
        }

        private bool GetVendorInfo()
        {
            bool GetVendorInfo = false;
            clsDRWrapper rs2 = new clsDRWrapper();
            int counter;
            int counter2;
            string strLabel = "";
            // vbPorter upgrade warning: curTotal As Decimal	OnWriteFCConvert.ToInt16(
            Decimal curTotal;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            counter = 1;
            curTotal = 0;
            rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + txtVendor.Text.ToIntegerValue() + "AND Status <> 'D'");
            if (rsVendorInfo.BeginningOfFile() != true && rsVendorInfo.EndOfFile() != true)
            {
                if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "S")
                {
                    MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtVendor.Text = "";
                    GetVendorInfo = false;
                    return GetVendorInfo;
                }
                EncumbranceFlag = false;
                PurchaseOrderFlag = false;
                ChosenEncumbrance = 0;
                ChosenPurchaseOrder = 0;
                NumberOfEncumbrances = 0;
                txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
                if (!modBudgetaryMaster.Statics.blnAPEdit)
                {
                    txtDescription.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultDescription"));
                    txtCheck.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("DefaultCheckNumber"));
                    txtReference.Text = "";
                    rs2.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY DescriptionNumber");
                    if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                    {
                        // if there is any default information
                        rs2.MoveLast();
                        rs2.MoveFirst();
                        while (rs2.EndOfFile() != true)
                        {
                            // get all the default information there is
                            if (counter >= vs1.Rows)
                            {
                                vs1.AddItem("");
                                vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
                                vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                                //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, counter, TaxCol, 4);
                            }
                            // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                            if (modValidateAccount.AccountValidate(rs2.Get_Fields("AccountNumber")))
                            {
                                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rs2.Get_Fields("AccountNumber")));
                            }
                            else
                            {
                                MessageBox.Show("A bad account was found in the default vendor information.  You will have to manually enter an account for this line.", "Bad Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                vs1.TextMatrix(counter, AccountCol, "");
                            }
                            vs1.TextMatrix(counter, DescriptionCol, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(rs2.Get_Fields("Amount")));
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            vs1.TextMatrix(counter, TaxCol, FCConvert.ToString(rs2.Get_Fields("Tax")));
                            vs1.TextMatrix(counter, ProjectCol, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                            rs2.MoveNext();
                            counter += 1;
                        }
                        for (counter = counter; counter <= vs1.Rows - 1; counter++)
                        {
                            vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 1, "");
                            vs1.TextMatrix(counter, 2, "");
                            vs1.TextMatrix(counter, 3, "");
                            vs1.TextMatrix(counter, 4, "");
                        }
                        CalculateTotals();
                        txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                        AmountToPay = TotalAmount;
                        lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                        lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                    }
                    else
                    {
                        for (counter = counter; counter <= vs1.Rows - 1; counter++)
                        {
                            vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 1, "");
                            vs1.TextMatrix(counter, 2, "");
                            vs1.TextMatrix(counter, 3, "");
                            vs1.TextMatrix(counter, 4, "");
                        }
                        CalculateTotals();
                        txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                        AmountToPay = TotalAmount;
                        lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                        lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                    }
                }
                if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")) != "")
                {
                    ans = MessageBox.Show(rsVendorInfo.Get_Fields_String("DataEntryMessage") + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this vendor message at this time?", "Data Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (ans == DialogResult.Yes)
                    {
                        rsVendorInfo.Edit();
                        rsVendorInfo.Set_Fields("DataEntryMessage", "");
                        rsVendorInfo.Update(true);
                    }
                }
                GetVendorInfo = true;
            }
            else
            {
                MessageBox.Show("There is no Vendor that matches that number", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtVendor.Text = "";
                GetVendorInfo = false;
            }
            return GetVendorInfo;
        }

        private void Fill_List(ref clsDRWrapper rsVendor)
        {
            string temp;
            int i;
            //FC:FINAL:BBE:#584 - wisej do not supported spaces and tabs in ListViewItems. Use colums instead.
            lstRecords.Columns.Clear();
            lstRecords.Columns.Add("VendorNumber", 120);
            lstRecords.Columns.Add("VendorName", lstRecords.Width - lstRecords.Columns[0].Width - 20);
            temp = "";
            // this fills the listbox with the account information so that the user can dblclick
            // an account to choose it
            for (i = 1; i <= rsVendor.RecordCount(); i++)
            {
                // for each record found put info into a listbox
                if (rsVendor.Get_Fields_String("Status") != "S")
                {
                    if (!rsVendor.IsFieldNull("VendorNumber"))
                    {
                        temp = rsVendor.Get_Fields_Int32("VendorNumber").ToString();
                        temp = Strings.Format(temp, "00000");
                        temp += "\t";
                    }
                    if (rsVendor.Get_Fields_String("CheckName") != "")
                        temp += rsVendor.Get_Fields_String("CheckName") + " // " + rsVendor.Get_Fields_String("CheckAddress1");
                    if (i < rsVendor.RecordCount())
                        rsVendor.MoveNext();
                    lstRecords.AddItem(temp);
                    // go to the next record
                }
            }
        }

        private void txtZip_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                // txtAddress[0].Focus();
                AdvanceToVendor();
            }
        }

        private void txtZip4_Enter(object sender, System.EventArgs e)
        {
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
                return;
            }
        }

        private void vs1_ChangeEdit(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
            if (vs1.IsCurrentCellInEditMode)
            {
                Decimal curBalance;
                if (vs1.Col == AccountCol)
                {
                    lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
                    if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
                    {
                        if (Strings.InStr(1, vs1.EditText, "_", CompareConstants.vbBinaryCompare) == 0)
                        {
                            if (Strings.Left(vs1.EditText, 1) != "E" && Strings.Left(vs1.EditText, 1) != "P")
                            {
                                // do nothing
                            }
                            else
                            {
                                curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.EditText) - GetBudgetAdjustments(vs1.EditText));
                                curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.EditText) + GetEncumbrance(vs1.EditText));
                                curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.EditText));
                                lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
                            }
                        }
                    }
                }
            }
        }

        private void vs1_Enter(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
            Decimal curBalance;
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                // txtAddress[0].Focus();
                AdvanceToVendor();
                return;
            }
            mnuProcessDelete.Enabled = true;
            lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
            if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
            {
                if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
                {
                    if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
                    {
                        // do nothing
                    }
                    else
                    {
                        curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
                        curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
                        curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
                        lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
                    }
                }
            }

            vs1_RowColChange(sender, e);
        }

        private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Enter)
            {
                // if enter is hit
                KeyCode = 0;
                if (vs1.Col < EncumbranceCol)
                {
                    // if we are not in the last column
                    if (vs1.Col == DescriptionCol)
                    {
                        // if we are in the description column
                        if (vs1.EditText == "")
                        {
                            // if it is blank
                            vs1.EditText = txtDescription.Text;
                            // bring down information form the previous line
                        }
                    }
                    vs1.Col += 1;
                    // move to the next column
                }
                else
                {
                    if (vs1.Row < vs1.Rows - 1)
                    {
                        // if we are not on the last row
                        //vs1.Row += 1;
                        //// move to the first column of the next row
                        //vs1.Col = DescriptionCol;
                        //e.Handled = true;
                        vs1.Select(vs1.Row + 1,DescriptionCol);
                        e.Handled = true;
                    }
                    else
                    {
                        vs1.AddItem("");
                        // if we are at the end then create a new line
                        vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, DiscountCol, FCConvert.ToString(0));
                        // initialize values for the new row
                        vs1.TextMatrix(vs1.Row + 1, EncumbranceCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
                        //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Row + 1, TaxCol, 4);
                        vs1.Row += 1;
                        // move to the next line in the first column
                        vs1.Col = 0;
                    }
                }
                //Application.DoEvents();
            }
        }

        private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
        {
            //FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
            if (e.KeyChar == 9)
            {
                return;
            }
            int keyAscii = Strings.Asc(e.KeyChar);
            if (keyAscii == 29)
            {
                keyAscii = 0;
            }
        }

        private void vs1_RowColChange(object sender, System.EventArgs e)
        {
            int counter;
            // vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
            Decimal curBalance;
            vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
                return;
            }
            vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            // make the flex grid editable
            lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
            if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
            {
                if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
                {
                    if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
                    {
                        // do nothing
                    }
                    else
                    {
                        curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
                        curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
                        curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
                        lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
                    }
                }
            }
            if (vs1.Col != TaxCol)
            {
                blnClicked = false;
                blnBackedInto = false;
            }
            if (!DeleteFlag)
            {
                // Dave 7/17/07
                // CalculateTotals
                // DoEvents
                lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                if (vs1.Col == DescriptionCol)
                {
                    vs1.EditMaxLength = 25;
                    vs1.EditCell();
                    vs1.EditSelStart = 0;
                }
                else if (vs1.Col == TaxCol)
                {
                    if (Conversion.Val(txtVendor.Text) == 0)
                    {
                        vs1.Editable = FCGrid.EditableSettings.flexEDNone;
                    }
                    //if (blnBackedInto || blnClicked)
                    //{
                        blnBackedInto = false;
                        blnClicked = false;
                        vs1.EditMaxLength = 1;
                    //}
                    //else
                    //{
                    //    vs1.Col += 1;
                    //}
                    if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
                    {
                        if (Conversion.Val(txtVendor.Text) != 0)
                        {
                            vs1.TextMatrix(vs1.Row, TaxCol, "D");
                        }
                        else
                        {
                            vs1.TextMatrix(vs1.Row, TaxCol, "N");
                        }
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                    }
                    else
                    {
                        if (vs1.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
                        {
                            vs1.EditCell();
                            vs1.EditSelStart = 0;
                        }
                    }
                }
                else if (vs1.Col == ProjectCol)
                {
                    if (modBudgetaryMaster.Statics.ProjectFlag && strComboList != "")
                    {
                        vs1.EditCell();
                    }
                    else
                    {
                        vs1.Col += 1;
                    }
                }
                else if (vs1.Col == AmountCol)
                {
                    vs1.EditMaxLength = 10;
                    if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                        vs1.EditSelLength = 1;
                    }
                    else
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                    }
                }
                else if (vs1.Col == DiscountCol)
                {
                    vs1.EditMaxLength = 9;
                    if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                        vs1.EditSelLength = 1;
                    }
                    else
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                    }
                }
                else if (vs1.Col == EncumbranceCol)
                {
                    if (vs1.Row > NumberOfEncumbrances)
                    {
                        vs1.Editable = FCGrid.EditableSettings.flexEDNone;
                    }
                    else
                    {
                        vs1.EditMaxLength = 10;
                        if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
                        {
                            vs1.EditCell();
                            vs1.EditSelStart = 0;
                            vs1.EditSelLength = 1;
                        }
                        else
                        {
                            vs1.EditCell();
                            vs1.EditSelStart = 0;
                        }
                    }
                }
            }
            if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
            {
                vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
            }
            else
            {
                vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            }
        }

        private void vs1_ClickEvent(object sender, System.EventArgs e)
        {
            blnClicked = true;
            if (txtVendor.Text == "" && txtAddress[0].Text == "")
            {
                //txtAddress[0].Focus();
                AdvanceToVendor();
                return;
            }
            if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
            {
                // if the grid is editable then
                // do nothing
            }
            else
            {
                if (vs1.Col == AmountCol || vs1.Col == DiscountCol || vs1.Col == EncumbranceCol)
                {
                    if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                        vs1.EditSelLength = 1;
                    }
                    else
                    {
                        vs1.EditCell();
                        vs1.EditSelStart = 0;
                    }
                }
                else
                {
                    vs1.EditCell();
                    // edit the cell and put the cursor at the beginning
                    vs1.EditSelStart = 0;
                }
            }
        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
        {
            Keys keyCode = e.KeyCode;
            if (vs1.Col == AccountCol && keyCode != Keys.F9 && keyCode != Keys.F12)
            {
                if (keyCode == Keys.Left && vs1.EditSelStart == 0)
                {
                    keyCode = 0;
                    vs1.Col -= 1;
                }
                else if (keyCode == Keys.Right && vs1.EditSelStart == vs1.EditMaxLength)
                {
                    keyCode = 0;
                    Support.SendKeys("{TAB}", false);
                }
            }
            //FC:FINAL:MSH - Incorrect conversion from VB. In VB vbKeyReturn equal 'Enter' key.
            //if (keyCode == Keys.Return)
            if (keyCode == Keys.Enter)
            {
                keyCode = 0;
                if (vs1.Col < EncumbranceCol)
                {
                    if (vs1.Col == DescriptionCol)
                    {
                        if (string.IsNullOrEmpty(vs1.EditText))
                        {
                            vs1.EditText = txtDescription.Text;
                        }
                    }
                    vs1.Col += 1;
                }
                else
                {
                    if (vs1.Row < vs1.Rows - 1)
                    {
                        vs1.Row += 1;
                        vs1.Col = 0;
                    }
                    else
                    {
                        vs1.AddItem("");
                        vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, DiscountCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, EncumbranceCol, FCConvert.ToString(0));
                        vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
                        //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Row + 1, TaxCol, 4);
                        vs1.Row += 1;
                        vs1.Col = 0;
                    }
                }
                //Application.DoEvents();
            }
            else if (keyCode == Keys.Left)
            {
                // if the left key is pressed
                blnBackedInto = true;
                if (modBudgetaryMaster.Statics.ProjectFlag == false || strComboList == "")
                {
                    // if the user doesnt use project numbers
                    if (vs1.Col == AmountCol)
                    {
                        // if we are in the amount column
                        if (vs1.EditSelStart == 0)
                        {
                            // if we are at the beginning of the column
                            keyCode = 0;
                            vs1.Col = TaxCol;
                            // move to the tax column
                        }
                    }
                }
                // if we are on any of the currency columns
            }
            else if (vs1.Col == AmountCol || vs1.Col == DiscountCol || vs1.Col == EncumbranceCol)
            {
                if (keyCode == Keys.C)
                {
                    // if the c key is hit then erase the value
                    keyCode = 0;
                    vs1.EditText = "";
                    Support.SendKeys("{BACKSPACE}", false);
                }
                if (keyCode == Keys.Insert)
                {
                    keyCode = 0;
                    if (vs1.Col == AmountCol)
                    {
                        if (Information.IsNumeric(vs1.TextMatrix(vs1.Row, vs1.Col)))
                        {
                            vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) + FCConvert.ToDecimal(lblRemAmount.Text));
                        }
                        else
                        {
                            vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(lblRemAmount.Text));
                        }
                    }
                }
            }
            else if (vs1.Col == DescriptionCol)
            {
                if (keyCode == Keys.Insert)
                {
                    keyCode = 0;
                    vs1.EditText = txtDescription.Text;
                    vs1.Col += 1;
                }
            }
        }

        private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int temp = 0;
            string temp2 = "";
            // vbPorter upgrade warning: percent As float	OnWrite(string, float)
            float percent = 0;
            // vbPorter upgrade warning: curAmtLeft As Decimal	OnWrite(string, Decimal)
            Decimal curAmtLeft;
            clsDRWrapper rsExpObjInfo = new clsDRWrapper();
            clsDRWrapper rsRegionalInfo = new clsDRWrapper();
            int intRowsToJump = 0;
            int intRowToUse = 0;
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = vs1.GetFlexRowIndex(e.RowIndex);
            int col = vs1.GetFlexColIndex(e.ColumnIndex);
            if (col == AmountCol)
            {
                // if coming from the amount column
                if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
                {
                    // if there is not a number entered
                    MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#4481 - don't cancel the validation
                    //e.Cancel = true;
                    vs1.EditText = "0";
                    vs1.TextMatrix(row, col, "0");
                    // give an error message
                }
                else if (vs1.EditText == "")
                {
                    // if nothing is there then make the value 0
                    vs1.EditText = "0";
                }
                else
                {
                    temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
                    // if there is a decimal point chop the number after 2 places
                    if (temp > 0)
                    {
                        if (vs1.EditText.Length > temp + 2)
                        {
                            vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
                        }
                    }
                    if (Conversion.Val(vs1.EditText) < 0)
                    {
                        // if the number is negative give an error message
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, col, Color.Red);
                    }
                    else
                    {
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, col, Color.Black);
                    }
                    if (Conversion.Val(modBudgetaryAccounting.GetDepartment(vs1.TextMatrix(row, AccountCol))) > 0 && Conversion.Val(modBudgetaryAccounting.GetDepartment(vs1.TextMatrix(row, AccountCol))) < 100 && modRegionalTown.IsRegionalTown() && Conversion.Val(vs1.EditText) != 0)
                    {
                        if (!modAccountTitle.Statics.ObjFlag)
                        {
                            rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Expense = '" + modBudgetaryAccounting.GetExpense(vs1.TextMatrix(row, AccountCol)) + "' AND Object = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2))), "0") + "'");
                        }
                        else
                        {
                            rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Expense = '" + modBudgetaryAccounting.GetExpense(vs1.TextMatrix(row, AccountCol)) + "'");
                        }
                        if (rsExpObjInfo.EndOfFile() != true && rsExpObjInfo.BeginningOfFile() != true)
                        {
                            rsRegionalInfo.OpenRecordset("SELECT * FROM RegionalTownBreakDown WHERE Code = " + rsExpObjInfo.Get_Fields_Int32("BreakdownCode") + " ORDER BY Percentage", "CentralData");
                            if (rsRegionalInfo.EndOfFile() != true && rsRegionalInfo.BeginningOfFile() != true)
                            {
                                curAmtLeft = FCConvert.ToDecimal(vs1.EditText);
                                intRowsToJump = 0;
                                do
                                {
                                    vs1.AddItem("", row + 1);
                                    vs1.TextMatrix(row + 1, AmountCol, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble(vs1.EditText) * rsRegionalInfo.Get_Fields_Double("Percentage"), 2), "#,##0.00"));
                                    curAmtLeft -= FCConvert.ToDecimal(vs1.TextMatrix(row + 1, AmountCol));
                                    // TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
                                    vs1.TextMatrix(row + 1, AccountCol, Strings.Left(vs1.TextMatrix(row, AccountCol), 2) + rsRegionalInfo.Get_Fields("TownNumber") + Strings.Right(vs1.TextMatrix(row, AccountCol), vs1.TextMatrix(row, AccountCol).Length - 3));
                                    vs1.TextMatrix(row + 1, DescriptionCol, vs1.TextMatrix(row, DescriptionCol));
                                    vs1.TextMatrix(row + 1, TaxCol, vs1.TextMatrix(row, TaxCol));
                                    if (modBudgetaryMaster.Statics.ProjectFlag)
                                    {
                                        vs1.TextMatrix(row + 1, ProjectCol, vs1.TextMatrix(row, ProjectCol));
                                    }
                                    vs1.TextMatrix(row + 1, DiscountCol, FCConvert.ToString(0));
                                    vs1.TextMatrix(row + 1, EncumbranceCol, FCConvert.ToString(0));
                                    vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
                                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, row + 1, TaxCol, 4);
                                    intRowsToJump += 1;
                                    rsRegionalInfo.MoveNext();
                                }
                                while (rsRegionalInfo.EndOfFile() != true);
                                if (curAmtLeft != 0)
                                {
                                    vs1.TextMatrix(row + 1, AmountCol, Strings.Format(vs1.TextMatrix(row + 1, AmountCol) + curAmtLeft, "#,##0.00"));
                                }
                                intRowToUse = row;
                                vs1.RemoveItem(row);
                                vs1.Select(row + intRowsToJump - 1, col);
                                vs1.EditText = vs1.TextMatrix(intRowToUse, AmountCol);
                            }
                        }
                        else
                        {
                            if (!modAccountTitle.Statics.ExpDivFlag)
                            {
                                rsExpObjInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Department = '1" + Strings.Right(modBudgetaryAccounting.GetDepartment(vs1.TextMatrix(row, AccountCol)), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) - 1)) + "' AND Division = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "0") + "'");
                            }
                            else
                            {
                                rsExpObjInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Department = '1" + Strings.Right(modBudgetaryAccounting.GetDepartment(vs1.TextMatrix(row, AccountCol)), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) - 1)) + "'");
                            }
                            if (rsExpObjInfo.EndOfFile() != true && rsExpObjInfo.BeginningOfFile() != true)
                            {
                                rsRegionalInfo.OpenRecordset("SELECT * FROM RegionalTownBreakDown WHERE Code = " + rsExpObjInfo.Get_Fields_Int32("BreakdownCode") + " ORDER BY Percentage", "CentralData");
                                if (rsRegionalInfo.EndOfFile() != true && rsRegionalInfo.BeginningOfFile() != true)
                                {
                                    curAmtLeft = FCConvert.ToDecimal(vs1.EditText);
                                    intRowsToJump = 0;
                                    do
                                    {
                                        vs1.AddItem("", row + 1);
                                        vs1.TextMatrix(row + 1, AmountCol, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble(vs1.EditText) * rsRegionalInfo.Get_Fields_Double("Percentage"), 2), "#,##0.00"));
                                        curAmtLeft -= FCConvert.ToDecimal(vs1.TextMatrix(row + 1, AmountCol));
                                        // TODO Get_Fields: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
                                        vs1.TextMatrix(row + 1, AccountCol, Strings.Left(vs1.TextMatrix(row, AccountCol), 2) + rsRegionalInfo.Get_Fields("TownNumber") + Strings.Right(vs1.TextMatrix(row, AccountCol), vs1.TextMatrix(row, AccountCol).Length - 3));
                                        vs1.TextMatrix(row + 1, DescriptionCol, vs1.TextMatrix(row, DescriptionCol));
                                        vs1.TextMatrix(row + 1, TaxCol, vs1.TextMatrix(row, TaxCol));
                                        if (modBudgetaryMaster.Statics.ProjectFlag)
                                        {
                                            vs1.TextMatrix(row + 1, ProjectCol, vs1.TextMatrix(row, ProjectCol));
                                        }
                                        vs1.TextMatrix(row + 1, DiscountCol, FCConvert.ToString(0));
                                        vs1.TextMatrix(row + 1, EncumbranceCol, FCConvert.ToString(0));
                                        vs1.TextMatrix(row + 1, NumberCol, FCConvert.ToString(0));
                                        //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, row + 1, TaxCol, 4);
                                        intRowsToJump += 1;
                                        rsRegionalInfo.MoveNext();
                                    }
                                    while (rsRegionalInfo.EndOfFile() != true);
                                    if (curAmtLeft != 0)
                                    {
                                        vs1.TextMatrix(row + 1, AmountCol, Strings.Format(vs1.TextMatrix(row + 1, AmountCol) + curAmtLeft, "#,##0.00"));
                                    }
                                    intRowToUse = row;
                                    vs1.RemoveItem(row);
                                    vs1.Select(row + intRowsToJump - 1, col);
                                    vs1.EditText = vs1.TextMatrix(intRowToUse, AmountCol);
                                }
                            }
                        }
                    }
                    if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
                    {
                        if (!ValidateBalance_6(vs1.TextMatrix(row, AccountCol), FCConvert.ToDecimal(vs1.EditText), row))
                        {
                            //FC:FINAL:AM:#4481 - don't cancel the validation
                            //e.Cancel = true;
                            vs1.EditText = "0";
                            vs1.TextMatrix(row, col, "0");
                            return;
                        }
                    }
                    // Dave 7/17/07
                    CalculateTotals();
                    // calculate new totals and display information
                    //Application.DoEvents();
                    if (AmountToPay < TotalDiscount)
                    {
                        MessageBox.Show("Your discount cannot be greater than the amount you have to pay this vendor", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "0";
                        vs1.TextMatrix(row, col, "0");
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, row, col, Color.Black);
                    }
                    // lblActualPayment = format(CCur(vs1.EditText) - CCur(vs1.TextMatrix(vs1.Row, DiscountCol)), "#,##0.00")
                    lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                    lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                }
            }
            else if (col == DiscountCol)
            {
                if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
                {
                    if (Strings.Right(vs1.EditText, 1) == "%")
                    {
                        if (!Information.IsNumeric(Strings.Left(vs1.EditText, vs1.EditText.Length - 1)))
                        {
                            MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //FC:FINAL:AM:#4481 - don't cancel the validation
                            //e.Cancel = true;
                            vs1.EditText = "";
                            vs1.TextMatrix(row, col, "0");
                            return;
                        }
                        else
                        {
                            percent = FCConvert.ToSingle(Strings.Left(vs1.EditText, vs1.EditText.Length - 1));
                            percent /= 100;
                            temp2 = Strings.Format(percent * FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(vs1.TextMatrix(row, AmountCol)))), "Fixed");
                            vs1.EditText = Strings.Trim(temp2);
                            if (Conversion.Val(vs1.EditText) < 0)
                            {
                                MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                //FC:FINAL:AM:#4481 - don't cancel the validation
                                //e.Cancel = true;
                                vs1.EditText = "";
                                vs1.TextMatrix(row, col, "0");
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                        return;
                    }
                }
                else if (vs1.EditText == "")
                {
                    vs1.EditText = FCConvert.ToString(0);
                }
                temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
                if (temp > 0)
                {
                    if (vs1.EditText.Length > temp + 2)
                    {
                        vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
                    }
                }
                if (FCConvert.ToDecimal(vs1.EditText) < 0)
                {
                    MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#4481 - don't cancel the validation
                    //e.Cancel = true;
                    vs1.EditText = "0";
                    vs1.TextMatrix(row, col, "0");
                    return;
                }
                if (FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol)) < FCConvert.ToDecimal(vs1.EditText) && FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol)) >= 0)
                {
                    MessageBox.Show("The discount cannot be greater then the actual payment", "Invalid Discount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#4481 - don't cancel the validation
                    //e.Cancel = true;
                    vs1.EditText = "0";
                    vs1.TextMatrix(row, col, "0");
                    return;
                }
                CalculateTotals();
                //Application.DoEvents();
                lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
            }
            else if (col == EncumbranceCol)
            {
                if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
                {
                    MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#4481 - don't cancel the validation
                    //e.Cancel = true;
                    vs1.EditText = "";
                    vs1.TextMatrix(row, col, "0");
                }
                else if (vs1.EditText == "")
                {
                    vs1.EditText = FCConvert.ToString(0);
                }
                else if (vs1.EditText != "0")
                {
                    if (FCConvert.ToDouble(vs1.EditText) > modBudgetaryMaster.Statics.EncumbranceDetail[row - 1])
                    {
                        MessageBox.Show("You cannot enter a number that exceeds $" + Strings.Format(modBudgetaryMaster.Statics.EncumbranceDetail[row - 1], "#,##0.00") + " in this field", "Invalid Encumbrance", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                        return;
                    }
                    temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
                    if (temp > 0)
                    {
                        if (vs1.EditText.Length > temp + 2)
                        {
                            vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
                        }
                    }
                    if (Conversion.Val(vs1.EditText) < 0)
                    {
                        MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                        return;
                    }
                }
            }
            else if (col == TaxCol)
            {
                if (vs1.EditText == "n" || vs1.EditText == "d")
                {
                    temp2 = vs1.EditText;
                    vs1.EditText = Strings.UCase(temp2);
                }
                if (!Information.IsNumeric(vs1.EditText))
                {
                    if (vs1.EditText == "")
                    {
                        vs1.EditText = "D";
                    }
                    else if (vs1.EditText != "N" && vs1.EditText != "D")
                    {
                        MessageBox.Show("You may only enter a D, N, or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                    }
                }
                else
                {
                    if (FCConvert.ToDouble(vs1.EditText) < 1)
                    {
                        MessageBox.Show("You may only enter a D, N, or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                    }
                }
            }
            else if (col == ProjectCol)
            {
                if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
                {
                    MessageBox.Show("You must enter a number in this field", "Invalid Project Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#4481 - don't cancel the validation
                    //e.Cancel = true;
                    vs1.EditText = "";
                    vs1.TextMatrix(row, col, "0");
                }
            }
            else if (col == AccountCol)
            {
                if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
                {
                    if (!ValidateBalance_6(vs1.EditText, FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol)), row))
                    {
                        //FC:FINAL:AM:#4481 - don't cancel the validation
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(row, col, "0");
                        return;
                    }
                }
            }
            //Application.DoEvents();
        }

        private void CalculateTotals(bool blnBeforeEdit = false)
        {
            int counter;
            // vbPorter upgrade warning: TempMoney As Decimal	OnWrite(short, string)
            Decimal TempMoney = 0;
            int Col = 0;
            if (blnBeforeEdit)
            {
                Col = vs1.Col - 1;
            }
            else
            {
                Col = vs1.Col;
                if (vs1.EditText == "" || Col < AmountCol)
                {
                    TempMoney = 0;
                }
                else
                {
                    TempMoney = FCConvert.ToDecimal(vs1.EditText);
                }
            }
            TotalAmount = 0;
            TotalDiscount = 0;
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                if (!blnBeforeEdit)
                {
                    if (counter == vs1.Row)
                    {
                        if (Col == AmountCol)
                        {
                            if (TempMoney >= 0)
                            {
                                TotalAmount += Conversion.Val(TempMoney);
                                TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol));
                            }
                            else
                            {
                                TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol)) + (Conversion.Val(TempMoney) * -1);
                            }
                        }
                        else if (Col == DiscountCol)
                        {
                            if (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) >= 0)
                            {
                                TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
                                TotalDiscount += Conversion.Val(TempMoney);
                            }
                            else
                            {
                                TotalDiscount += Conversion.Val(TempMoney) + (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) * -1);
                            }
                        }
                        else
                        {
                            if (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) >= 0)
                            {
                                TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
                                TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol));
                            }
                            else
                            {
                                TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol)) + (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) * -1);
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) >= 0)
                        {
                            TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
                            TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol));
                        }
                        else
                        {
                            TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol)) + (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) * -1);
                        }
                    }
                }
                else
                {
                    if (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) >= 0)
                    {
                        TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
                        TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol));
                    }
                    else
                    {
                        TotalDiscount += Conversion.Val(vs1.TextMatrix(counter, DiscountCol)) + (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) * -1);
                    }
                }
            }
        }

        private void SaveDetails()
        {
            int counter = 0;
            double TotalEnc;
            clsDRWrapper rsEncDetail = new clsDRWrapper();
            clsDRWrapper rsDetailInfo = new clsDRWrapper();
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rs = new clsDRWrapper();
            if (modBudgetaryMaster.Statics.blnAPEdit)
            {
                // if we are editing then erase the last recordset so we can save the new
                rs.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(apJournalId), "Budgetary");
            }
            TotalEnc = 0;
            rsDetailInfo.OpenRecordset("SELECT * FROM DefaultInfo WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + " ORDER BY DescriptionNumber");
            if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
            {
                counter = 1;
                do
                {
                    // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    if (Strings.Trim(vs1.TextMatrix(counter, DescriptionCol)) == FCConvert.ToString(rsDetailInfo.Get_Fields_String("Description")) && Strings.Trim(vs1.TextMatrix(counter, AccountCol)) == FCConvert.ToString(rsDetailInfo.Get_Fields("AccountNumber")) && Strings.Trim(vs1.TextMatrix(counter, TaxCol)) == FCConvert.ToString(rsDetailInfo.Get_Fields("Tax")) && Strings.Trim(vs1.TextMatrix(counter, ProjectCol)) == FCConvert.ToString(rsDetailInfo.Get_Fields_String("Project")) && FCConvert.ToDecimal(Strings.Trim(vs1.TextMatrix(counter, AmountCol))) != rsDetailInfo.Get_Fields("Amount") && FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) != 0)
                    {
                        ans = MessageBox.Show("The default amount for this vendor in line item " + FCConvert.ToString(counter) + " has changed.  Do you wish to change the default amount for this line item to reflect the change you made in this entry?", "Change Default Amount?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                        if (ans == DialogResult.Yes)
                        {
                            rsDetailInfo.Edit();
                            rsDetailInfo.Set_Fields("Amount", FCConvert.ToDecimal(Strings.Trim(vs1.TextMatrix(counter, AmountCol))));
                            rsDetailInfo.Update(false);
                        }
                    }
                    counter += 1;
                    rsDetailInfo.MoveNext();
                    if (counter == vs1.Rows)
                    {
                        break;
                    }
                }
                while (rsDetailInfo.EndOfFile() != true);
            }
            rs.OmitNullsOnInsert = true;
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                // save the grid information
                // if (vs1.TextMatrix(counter, AmountCol) != "0")  
                // FC:FINAL:VGE - #707 Comparing to "0" isn't accurate. Replaced with decimal
                if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) != 0)
                {
                    rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
                    if (!rs.EndOfFile())
                    {
                        rs.Edit();
                    }
                    else
                    {
                        rs.AddNew();
                        // if so add it
                    }
                    rs.Set_Fields("APJournalID", apJournalId);
                    rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
                    rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
                    rs.Set_Fields("Amount", vs1.TextMatrix(counter, AmountCol));
                    rs.Set_Fields("Discount", vs1.TextMatrix(counter, DiscountCol));
                    rs.Set_Fields("Encumbrance", vs1.TextMatrix(counter, EncumbranceCol));
                    if (ChosenEncumbrance != 0 && FCConvert.ToDecimal(vs1.TextMatrix(counter, EncumbranceCol)) != 0)
                    {
                        rs.Set_Fields("EncumbranceDetailRecord", modBudgetaryMaster.Statics.EncumbranceDetailRecords[counter - 1]);
                        TotalEnc += FCConvert.ToDouble(vs1.TextMatrix(counter, EncumbranceCol));
                    }
                    else if (ChosenEncumbrance != 0)
                    {
                        rs.Set_Fields("PurchaseOrderDetailsID", vs1.RowData(counter - 1));
                    }
                    rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
                    if (Strings.Trim(vs1.TextMatrix(counter, TaxCol)) == "")
                    {
                        rs.Set_Fields("1099", "D");
                    }
                    else
                    {
                        rs.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
                        // TextMatrix(counter, TaxCol)
                    }
                    rs.Update();
                    // update the database
                    vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("id")));
                }
                else
                {
                    //FC:FINAL:DSE #i625 Exception on conversion if string is empty
                    if (!String.IsNullOrEmpty(vs1.TextMatrix(counter, NumberCol)) && FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
                    {
                        rs.Execute("DELETE FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))), "Budgetary");
                    }
                }
            }
            rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(apJournalId));
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Edit();
                rs.Set_Fields("TotalEncumbrance", TotalEnc);
                rs.Set_Fields("EncumbranceRecord", ChosenEncumbrance);
                rs.Set_Fields("PurchaseOrderID", ChosenPurchaseOrder);
                rs.Update();
            }
        }

        private void GetJournalData()
        {
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            // get information to show on the accounts payable screen
            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
            SetCombo(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")));
            txtVendor.Text = modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"), 5);
            if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")) == 0)
            {
                txtAddress[0].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorName"));
                txtAddress[1].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress1"));
                txtAddress[2].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress2"));
                txtAddress[3].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress3"));
                txtCity.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorCity"));
                txtState.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorState"));
                txtZip.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip"));
                txtZip4.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip4"));
            }
            else
            {
                rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
                txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
            }
            if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Payable"))
            {
                //FC:FINAL:MSH - Issue #698: operator '==' can't be applied to types DateTime and double.
                //if (blnJournalEdit && modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Payable") == datOldPayableDate.ToOADate())
                if (blnJournalEdit && datOldPayableDate.CompareTo(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable")) == 0)
                {
                    txtPayable.Text = Strings.Format(datNewPayableDate, "MM/dd/yyyy");
                }
                else
                {
                    txtPayable.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable"));
                }
            }
            else
            {
                //FC:FINAL:MSH - Issue #698: operator '==' can't be applied to types DateTime and double.
                //if (blnJournalEdit && modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Payable") == datOldPayableDate.ToOADate())
                if (blnJournalEdit && datOldPayableDate.CompareTo(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable")) == 0)
                {
                    txtPayable.Text = FCConvert.ToString(datNewPayableDate);
                }
            }
            if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Until"))
            {
                txtUntil.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Until"), "MM/dd/yyyy");
            }
            else
            {
                txtUntil.Text = "";
            }
            // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
            if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Frequency")) != "")
            {
                // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                cboFrequency.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Frequency"));
            }
            else
            {
                cboFrequency.SelectedIndex = -1;
            }
            txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
            txtAmount.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount"), "#,##0.00");
            //FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
            if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period") != null)
                txtPeriod.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"));
            //FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
            if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Reference") != null)
            {
                txtReference.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Reference"));
            }
            else
            {
                txtReference.Text = "";
            }
            //FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
            // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
            if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("CheckNumber") != null)
            {
                // TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                txtCheck.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("CheckNumber"));
                if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("UseAlternateCash")))
                {
                    chkUseAltCashAccount.CheckState = CheckState.Checked;
                    vsAltCashAccount.Enabled = true;
                    vsAltCashAccount.TabStop = true;
                    vsAltCashAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("AlternateCashAccount")));
                }
                else
                {
                    chkUseAltCashAccount.CheckState = CheckState.Unchecked;
                    vsAltCashAccount.TextMatrix(0, 0, "");
                    vsAltCashAccount.Enabled = false;
                    vsAltCashAccount.TabStop = false;
                }
            }
            else
            {
                txtCheck.Text = "";
                chkUseAltCashAccount.CheckState = CheckState.Unchecked;
                vsAltCashAccount.TextMatrix(0, 0, "");
                vsAltCashAccount.Enabled = false;
                vsAltCashAccount.TabStop = false;
            }
            chkSeperate.CheckState = (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("Seperate") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
            apJournalId = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
            if (Conversion.Val(FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord"))) != 0)
            {
                ChosenEncumbrance = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord"));
            }
            else
            {
                ChosenEncumbrance = 0;
            }
            if (Conversion.Val(FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID"))) != 0)
            {
                ChosenPurchaseOrder = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID"));
            }
            else
            {
                ChosenPurchaseOrder = 0;
            }
            GetJournalDetails();
            AmountToPay = FCConvert.ToDouble(FCConvert.ToDecimal(txtAmount.Text));
            CalculateTotals();
            lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
            lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
            SetReadOnly();
        }

        public void SetReadOnly(cAPJournal aJourn = null)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();
            int counter;
            bool boolReadOnly;
            boolReadOnly = false;
            if (modBudgetaryMaster.Statics.blnAPEdit)
            {
                if (!(aJourn == null))
                {
                    rsTemp.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + aJourn.JournalNumber);
                    if (aJourn.PrintedIndividual)
                    {
                        boolReadOnly = true;
                    }
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    rsTemp.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                if (FCConvert.ToString(rsTemp.Get_Fields("Type")) == "AC" || boolReadOnly)
                {
                    lblReadOnly.Visible = true;
                    txtPeriod.Enabled = false;
                    cboJournal.Enabled = false;
                    txtVendor.Enabled = false;
                    for (counter = 0; counter <= 3; counter++)
                    {
                        txtAddress[counter].Enabled = false;
                    }
                    txtCity.Enabled = false;
                    txtState.Enabled = false;
                    txtZip.Enabled = false;
                    txtZip4.Enabled = false;
                    txtPayable.Enabled = false;
                    cboFrequency.Enabled = false;
                    txtUntil.Enabled = false;
                    txtDescription.Enabled = false;
                    txtReference.Enabled = false;
                    txtAmount.Enabled = false;
                    txtCheck.Enabled = false;
                    vs1.Enabled = false;
                    mnuProcessDelete.Enabled = false;
                    mnuProcessDeleteEntry.Enabled = false;
                    mnuFileChangePayableDate.Enabled = false;
                    mnuProcessEncumbrance.Enabled = false;
                    mnuFileShowPurchaseOrders.Enabled = false;
                    btnProcessSearch.Enabled = false;
                    btnProcessSave.Enabled = false;
                    mnuFileAddVendor.Enabled = false;
                }
                else
                {
                    lblReadOnly.Visible = false;
                    txtVendor.Enabled = true;
                    for (counter = 0; counter <= 3; counter++)
                    {
                        txtAddress[counter].Enabled = true;
                    }
                    txtCity.Enabled = true;
                    txtState.Enabled = true;
                    txtZip.Enabled = true;
                    txtZip4.Enabled = true;
                    DateTime parsedDate;
                    if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
                    {
                        txtPayable.Enabled = true;
                    }
                    else
                    {
                        txtPayable.Enabled = false;
                    }

                    cboFrequency.Enabled = true;
                    txtUntil.Enabled = true;
                    txtDescription.Enabled = true;
                    txtReference.Enabled = true;
                    txtAmount.Enabled = true;
                    txtCheck.Enabled = true;
                    vs1.Enabled = true;
                    mnuProcessDelete.Enabled = true;
                    mnuProcessDeleteEntry.Enabled = true;
                    mnuFileChangePayableDate.Enabled = true;
                    mnuProcessEncumbrance.Enabled = true;
                    mnuFileShowPurchaseOrders.Enabled = true;
                    btnProcessSearch.Enabled = true;
                    btnProcessSave.Enabled = true;
                    mnuFileAddVendor.Enabled = true;
                }
            }
            else
            {
                lblReadOnly.Visible = false;
                txtVendor.Enabled = true;
                for (counter = 0; counter <= 3; counter++)
                {
                    txtAddress[counter].Enabled = true;
                }
                txtCity.Enabled = true;
                txtState.Enabled = true;
                txtZip.Enabled = true;
                txtZip4.Enabled = true;
                DateTime parsedDate;
                if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
                {
                    txtPayable.Enabled = true;
                }
                else
                {
                    txtPayable.Enabled = false;
                }
                cboFrequency.Enabled = true;
                txtUntil.Enabled = true;
                txtDescription.Enabled = true;
                txtReference.Enabled = true;
                txtAmount.Enabled = true;
                txtCheck.Enabled = true;
                vs1.Enabled = true;
                mnuProcessDelete.Enabled = true;
                mnuProcessDeleteEntry.Enabled = true;
                mnuFileChangePayableDate.Enabled = true;
                mnuProcessEncumbrance.Enabled = true;
                mnuFileShowPurchaseOrders.Enabled = true;
                btnProcessSearch.Enabled = true;
                btnProcessSave.Enabled = true;
                mnuFileAddVendor.Enabled = true;
            }
        }

        private void ClearData()
        {
            int counter;
            txtVendor.Text = "";
            // blank the screen
            for (counter = 0; counter <= 3; counter++)
            {
                txtAddress[counter].Text = "";
            }
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtZip4.Text = "";
            txtAmount.Text = "";
            txtDescription.Text = "";
            txtReference.Text = "";
            txtCheck.Text = "";
            chkUseAltCashAccount.CheckState = CheckState.Unchecked;
            vsAltCashAccount.TextMatrix(0, 0, "");
            vsAltCashAccount.Enabled = false;
            vsAltCashAccount.TabStop = false;
            apJournalId = 0;
            AmountToPay = 0;
            NumberOfEncumbrances = 0;
            FCUtils.EraseSafe(modBudgetaryMaster.Statics.EncumbranceDetail);
            ChosenEncumbrance = 0;
            ChosenPurchaseOrder = 0;
            txtPayable.Text = Strings.Format(datPayableDate, "MM/dd/yyyy");
            txtUntil.Text = "";
            cboFrequency.SelectedIndex = -1;
            vs1.Clear();
            //txtPeriod.Text = CalculatePeriod();
            chkSeperate.CheckState = CheckState.Unchecked;
            vs1.Rows = 16;
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
                vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
                vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
                vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
            }
            // reformat the grid so it is ready for new information
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, EncumbranceCol, 4);
            //vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, TaxCol, vs1.Rows - 1, TaxCol, 4);
            vs1.TextMatrix(0, DescriptionCol, "Description");
            vs1.TextMatrix(0, AccountCol, "Account");
            vs1.TextMatrix(0, TaxCol, "1099");
            vs1.TextMatrix(0, ProjectCol, "Proj");
            vs1.TextMatrix(0, AmountCol, "Amount");
            vs1.TextMatrix(0, DiscountCol, "Disc");
            vs1.TextMatrix(0, EncumbranceCol, "Enc");
            vs1.ColFormat(AmountCol, "#,###.00");
            vs1.ColFormat(DiscountCol, "#,###.00");
            vs1.ColFormat(EncumbranceCol, "#,###.00");
            vs1.Select(1, 1);
            // FC:FINAL:VGE - #783 Ending edit of the grid to let Vendor be selected.
            vs1.EndEdit();
            lblExpense.Text = "";
            lblRemAmount.Text = "0.00";
            lblActualPayment.Text = "0.00";
            AmountToPay = 0;
            TotalAmount = 0;
            TotalDiscount = 0;
        }

        private void GetJournalDetails()
        {
            int counter = 0;
            clsDRWrapper rs2 = new clsDRWrapper();
            int counter2;
            string strLabel = "";
            int ACounter = 0;
            clsDRWrapper rs3 = new clsDRWrapper();
            // get the detail infgormation to show on the screen
            rs3.OpenRecordset("SELECT EncumbranceDetail.Account AS Account, EncumbranceDetail.Amount AS Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated AS Liquidated, EncumbranceDetail.ID AS VendorNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.ID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord") + " ORDER BY EncumbranceDetail.ID");
            if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
            {
                rs3.MoveLast();
                rs3.MoveFirst();
                EncumbranceFlag = true;
                modBudgetaryMaster.Statics.EncumbranceDetail = new double[rs3.RecordCount() + 1];
                modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[rs3.RecordCount() + 1];
            }
            else
            {
                EncumbranceFlag = false;
            }
            if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID")) != 0)
            {
                PurchaseOrderFlag = true;
            }
            else
            {
                PurchaseOrderFlag = false;
            }
            rs2.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " ORDER BY ID");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
            {
                // if there is any default information
                if (rs2.RecordCount() > 15)
                {
                    vs1.Rows = rs2.RecordCount() + 1;
                }
                else
                {
                    vs1.Rows = 16;
                }
                counter = 1;
                ACounter = 0;
                NumberOfEncumbrances = 0;
                while (rs2.EndOfFile() != true)
                {
                    // get all the default information there is
                    vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
                    vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                    // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                    vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("account")));
                    // TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
                    vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields("1099")));
                    vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields("Amount")));
                    // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                    vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Discount")));
                    vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
                    vs1.RowData(counter, rs2.Get_Fields_Int32("PurchaseOrderDetailsID"));
                    if (rs2.Get_Fields_Decimal("Encumbrance") > 0)
                    {
                        for (counter2 = 1; counter2 <= rs3.RecordCount(); counter2++)
                        {
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            if (rs3.Get_Fields("account") == rs2.Get_Fields("account"))
                            {
                                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                                modBudgetaryMaster.Statics.EncumbranceDetail[ACounter] = rs3.Get_Fields("Amount") + rs3.Get_Fields_Decimal("Adjustments") - (rs3.Get_Fields_Decimal("Liquidated") - rs2.Get_Fields_Decimal("Encumbrance"));
                                modBudgetaryMaster.Statics.EncumbranceDetailRecords[ACounter] = FCConvert.ToInt32(rs3.Get_Fields_Int32("EncumbranceID"));
                                ACounter += 1;
                                NumberOfEncumbrances += 1;
                                rs3.MoveFirst();
                                break;
                            }
                            else
                            {
                                rs3.MoveNext();
                            }
                        }
                    }
                    // get labels
                    rs2.MoveNext();
                    counter += 1;
                }
                // blank the rest of the grid
                if (counter < vs1.Rows - 1)
                {
                    for (counter = counter; counter <= vs1.Rows - 1; counter++)
                    {
                        vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 1, "");
                        vs1.TextMatrix(counter, 2, "");
                        vs1.TextMatrix(counter, 3, "");
                        vs1.TextMatrix(counter, 4, "");
                    }
                }
            }
            else
            {
                for (counter = 1; counter <= vs1.Rows - 1; counter++)
                {
                    vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                    vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                    vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                    vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                    vs1.TextMatrix(counter, 1, "");
                    vs1.TextMatrix(counter, 2, "");
                    vs1.TextMatrix(counter, 3, "");
                    vs1.TextMatrix(counter, 4, "");
                }
            }
        }

        private void GetEncData()
        {
            int counter;
            string temp = "";
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            clsDRWrapper rs = new clsDRWrapper();
            // get encumbrance information to show if the user does the get encumbrance option
            if (GotEncData == false || FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
            {
                if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
                {
                    rs.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND (Amount + Adjustments - Liquidated) > 0 AND ID IN (SELECT DISTINCT EncumbranceID FROM EncumbranceDetail WHERE left(Account, 2 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = 'E " + Strings.Left(cboEncDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND Amount + Adjustments - Liquidated <> 0) ORDER BY JournalNumber");
                }
                else
                {
                    rs.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND (Amount + Adjustments - Liquidated) > 0 ORDER BY JournalNumber");
                }
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    //vs3.Rows = rs.RecordCount() + 1;
                    if (cboEncDept.Visible)
                    {
                        vs3.Top = 90;
                        vs3.Height = btnCancelEncumbrance.Location.Y  - 110;
                    }
                    else
                    {
                        vs3.Top = 30;
                        vs3.Height = btnCancelEncumbrance.Location.Y - 50;
                    }

                    vs3.Rows = 1;
                    for (counter = 1; counter <= rs.RecordCount(); counter++)
                    {
                        if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
                        {
                            vs3.Rows = vs3.Rows + 1;
                            vs3.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            vs3.TextMatrix(counter, 1, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
                            vs3.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
                            vs3.TextMatrix(counter, 3, FCConvert.ToString(rs.Get_Fields_String("TempVendorName")));
                            vs3.TextMatrix(counter, 4, FCConvert.ToString(rs.Get_Fields_String("Description")));
                            // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                            vs3.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields("PO")));
                        }
                        else
                        {
                            rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
                            vs3.Rows = vs3.Rows + 1;
                            vs3.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            vs3.TextMatrix(counter, 1, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
                            vs3.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
                            vs3.TextMatrix(counter, 3, FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
                            vs3.TextMatrix(counter, 4, FCConvert.ToString(rs.Get_Fields_String("Description")));
                            // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                            vs3.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields("PO")));
                        }
                        //if (vs3.Row < vs3.Rows - 1)
                        //{
                        //    vs3.Row += 1;
                        //}
                        rs.MoveNext();
                    }
                }
                GotEncData = true;
            }
        }

        private void GetPOData()
        {
            int counter;
            string temp = "";
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsDept = new clsDRWrapper();
            // get encumbrance information to show if the user does the get encumbrance option
            // DJW Took out this chekc so it always gets updated
            // If GotPOData = False Or GetBDVariable("EncByDept") Then
            if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
            {
                rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND Department = '" + Strings.Left(cboEncDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY PO");
            }
            else
            {
                rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 ORDER BY PO");
            }
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                vsPurchaseOrders.Rows = rs.RecordCount() + 1;
                //FC:FINAL:DDU:#i759 - Height is already set right in frame
                //if (vsPurchaseOrders.Rows < 18)
                //{
                //    vsPurchaseOrders.Height = vsPurchaseOrders.RowHeight(0) * vsPurchaseOrders.Rows + 75;
                //}
                for (counter = 1; counter <= rs.RecordCount(); counter++)
                {
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
                    vsPurchaseOrders.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                    rsDept.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rs.Get_Fields_String("Department") + "' AND convert(int, IsNull(Division, 0)) = 0", "TWBD0000.vb1");
                    if (rsDept.EndOfFile() != true && rsDept.BeginningOfFile() != true)
                    {
                        vsPurchaseOrders.TextMatrix(counter, 1, rs.Get_Fields_String("Department") + " - " + rsDept.Get_Fields_String("LongDescription"));
                    }
                    else
                    {
                        vsPurchaseOrders.TextMatrix(counter, 1, rs.Get_Fields_String("Department") + " - UNKNOWN");
                    }
                    vsPurchaseOrders.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
                    vsPurchaseOrders.TextMatrix(counter, 3, FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
                    vsPurchaseOrders.TextMatrix(counter, 4, FCConvert.ToString(rs.Get_Fields_String("Description")));
                    // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                    vsPurchaseOrders.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields("PO")));
                    if (vsPurchaseOrders.Row < vsPurchaseOrders.Rows - 1)
                    {
                        vsPurchaseOrders.Row += 1;
                    }
                    rs.MoveNext();
                }
            }
            GotPOData = true;
            // End If
        }

        private void vs3_ClickEvent(object sender, System.EventArgs e)
        {
            EditFlag = true;
            vs3.Select(vs3.Row, 5, vs3.Row, 0);
            EditFlag = false;
        }

        private void vs3_DblClick(object sender, System.EventArgs e)
        {
            clsDRWrapper rs4 = new clsDRWrapper();
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rs = new clsDRWrapper();
            // makre sure the encumbrance isn't already used in another unposted journal
            rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + vs3.TextMatrix(vs3.Row, 0));
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // if there is a record
                rs.MoveLast();
                rs.MoveFirst();
                rs4.OpenRecordset("SELECT JournalNumber as Journal, TotalEncumbrance FROM APJournal WHERE EncumbranceRecord = " + vs3.TextMatrix(vs3.Row, 0) + " AND Status = 'E'");
                if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
                {
                    // TODO Get_Fields: Field [Journal] not found!! (maybe it is an alias?)
                    ans = MessageBox.Show(Strings.Format(rs4.Get_Fields_Decimal("TotalEncumbrance"), "#,##0.00") + " of this encumbrance is already used in Journal# " + rs4.Get_Fields("Journal") + " which is unposted. Do you wish to continue?", "Retrieve Encumbrance?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        return;
                    }
                }
                if (cboEncDept.Visible == true)
                {
                    modRegistry.SaveRegistryKey("CURENCDEPT", Strings.Left(cboEncDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
                }
                FillEncumbranceDetail();
                // if not show the detail items
            }
        }

        private void vs3_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Up)
            {
                if (vs3.Row == 1)
                {
                    // grid movement
                    KeyCode = 0;
                }
            }
            else if (KeyCode == Keys.Down)
            {
                if (vs3.Row == vs3.Rows - 1)
                {
                    KeyCode = 0;
                }
            }
        }

        private void vs3_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            clsDRWrapper rs4 = new clsDRWrapper();
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rs = new clsDRWrapper();
            int keyAscii = Strings.Asc(e.KeyChar);
            if (keyAscii == 13)
            {
                if (vs3.Row > 0)
                {
                    rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + vs3.TextMatrix(vs3.Row, 0));
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        // if there is a record
                        rs.MoveLast();
                        rs.MoveFirst();
                        rs4.OpenRecordset("SELECT JournalNumber as Journal, TotalEncumbrance FROM APJournal WHERE EncumbranceRecord = " + vs3.TextMatrix(vs3.Row, 0) + " AND Status = 'E'");
                        if (rs4.EndOfFile() != true && rs4.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Field [Journal] not found!! (maybe it is an alias?)
                            ans = MessageBox.Show(Strings.Format(rs4.Get_Fields_Decimal("TotalEncumbrance"), "#,##0.00") + " of this encumbrance is already used in Journal# " + rs4.Get_Fields("Journal") + " which is unposted. Do you wish to continue?", "Retrieve Encumbrance?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.No)
                            {
                                return;
                            }
                        }
                        FillEncumbranceDetail();
                        
                    }
                }
            }
        }

        private void vs3_RowColChange(object sender, System.EventArgs e)
        {
            if (!EditFlag)
            {
                EditFlag = true;
                vs3.Select(vs3.Row, 5, vs3.Row, 0);
                EditFlag = false;
            }
        }

        private void vsPurchaseOrders_ClickEvent(object sender, System.EventArgs e)
        {
            EditFlag = true;
            vsPurchaseOrders.Select(vsPurchaseOrders.Row, 5, vsPurchaseOrders.Row, 0);
            EditFlag = false;
        }

        private void vsPurchaseOrders_DblClick(object sender, System.EventArgs e)
        {
            if (vsPurchaseOrders.MouseRow > 0)
            {
                GetPurchaseOrderData(FCConvert.ToInt32(vsPurchaseOrders.TextMatrix(vsPurchaseOrders.MouseRow, 0)));
            }
        }

        private void vsPurchaseOrders_KeyDownEvent(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if (KeyCode == Keys.Up)
            {
                if (vsPurchaseOrders.Row == 1)
                {
                    // grid movement
                    KeyCode = 0;
                }
            }
            else if (KeyCode == Keys.Down)
            {
                if (vsPurchaseOrders.Row == vs3.Rows - 1)
                {
                    KeyCode = 0;
                }
            }
        }

        private void vsPurchaseOrders_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            if (keyAscii == 13)
            {
                if (vsPurchaseOrders.Row > 0)
                {
                    GetPurchaseOrderData(FCConvert.ToInt32(vsPurchaseOrders.TextMatrix(vsPurchaseOrders.Row, 0)));
                }
            }
        }

        private void vsPurchaseOrders_RowColChange(object sender, System.EventArgs e)
        {
            if (!EditFlag)
            {
                EditFlag = true;
                vsPurchaseOrders.Select(vsPurchaseOrders.Row, 5, vsPurchaseOrders.Row, 0);
                EditFlag = false;
            }
        }
        // vbPorter upgrade warning: lngId As int	OnWrite(string)
        private void GetPurchaseOrderData(int lngId)
        {
            int counter;
            // show encumbrance data on the screen
            string temp = "";
            int counter2;
            string strLabel = "";
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            int total = 0;
            int ArrayCounter = 0;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rs2 = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + FCConvert.ToString(lngId));
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // if there is a record
                rs.MoveLast();
                rs.MoveFirst();
                ChosenPurchaseOrder = lngId;
                txtVendor.Enabled = true;
                for (counter = 0; counter <= 3; counter++)
                {
                    txtAddress[counter].Enabled = true;
                }
                txtCity.Enabled = true;
                txtState.Enabled = true;
                txtZip.Enabled = true;
                txtZip4.Enabled = true;
                DateTime parsedDate;
                if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
                {
                    txtPayable.Enabled = true;
                }
                else
                {
                    txtPayable.Enabled = false;
                }
                cboFrequency.Enabled = true;
                txtUntil.Enabled = true;
                txtDescription.Enabled = true;
                txtReference.Enabled = true;
                txtAmount.Enabled = true;
                txtCheck.Enabled = true;
                vs1.Enabled = true;
                PurchaseOrderFlag = true;
                temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
                txtVendor.Text = Strings.Format(temp, "00000");
                rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
                txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
                txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
                // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                txtReference.Text = FCConvert.ToString(rs.Get_Fields("PO"));
                counter = 1;
                rs2.OpenRecordset("SELECT * FROM PurchaseOrderDetails WHERE PurchaseOrderID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
                if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                {
                    // if there is any default information
                    rs2.MoveLast();
                    rs2.MoveFirst();
                    total = 0;
                    for (counter2 = 1; counter2 <= vs4.Rows - 1; counter2++)
                    {
                        if (Conversion.Val(vs4.TextMatrix(counter2, 0)) == -1)
                        {
                            total += 1;
                        }
                    }
                    if (rs2.RecordCount() > 15)
                    {
                        vs1.Rows = rs2.RecordCount() + 1;
                    }
                    else
                    {
                        vs1.Rows = 16;
                    }
                    ArrayCounter = 0;
                    while (rs2.EndOfFile() != true)
                    {
                        // get all the default information there is
                        // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                        vs1.TextMatrix(ArrayCounter + 1, AccountCol, FCConvert.ToString(rs2.Get_Fields("account")));
                        vs1.TextMatrix(ArrayCounter + 1, DescriptionCol, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                        vs1.TextMatrix(ArrayCounter + 1, AmountCol, FCConvert.ToString(rs2.Get_Fields_Decimal("Total")));
                        vs1.TextMatrix(ArrayCounter + 1, DiscountCol, FCConvert.ToString(0));
                        vs1.TextMatrix(ArrayCounter + 1, EncumbranceCol, FCConvert.ToString(0));
                        vs1.TextMatrix(ArrayCounter + 1, TaxCol, "D");
                        vs1.RowData(ArrayCounter, rs2.Get_Fields_Int32("ID"));
                        ArrayCounter += 1;
                        counter += 1;
                        rs2.MoveNext();
                    }
                    for (counter = counter; counter <= vs1.Rows - 1; counter++)
                    {
                        vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 1, "");
                        vs1.TextMatrix(counter, 2, "");
                        vs1.TextMatrix(counter, 3, "");
                        vs1.TextMatrix(counter, 4, "");
                    }
                    CalculateTotals();
                    txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                    AmountToPay = TotalAmount;
                    lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                    lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                }
                fraPurchaseOrders.Visible = false;
                // make the list of records invisible
                if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")) != "")
                {
                    ans = MessageBox.Show(rsVendorInfo.Get_Fields_String("DataEntryMessage") + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this vendor message at this time?", "Data Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (ans == DialogResult.Yes)
                    {
                        rsVendorInfo.Edit();
                        rsVendorInfo.Set_Fields("DataEntryMessage", "");
                        rsVendorInfo.Update(true);
                    }
                }
                txtDescription.Focus();
            }
        }

        private void GetEncumbranceData()
        {
            int counter;
            // show encumbrance data on the screen
            string temp = "";
            int counter2;
            string strLabel = "";
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            int total = 0;
            int ArrayCounter = 0;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rs2 = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM Encumbrances WHERE ID = " + FCConvert.ToString(TempEncAccount));
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                // if there is a record
                rs.MoveLast();
                rs.MoveFirst();
                ChosenEncumbrance = TempEncAccount;
                txtVendor.Enabled = true;
                for (counter = 0; counter <= 3; counter++)
                {
                    txtAddress[counter].Enabled = true;
                }
                txtCity.Enabled = true;
                txtState.Enabled = true;
                txtZip.Enabled = true;
                txtZip4.Enabled = true;
                DateTime parsedDate;
                if (!DateTime.TryParse(txtPayable.Text, out parsedDate))
                {
                    txtPayable.Enabled = true;
                }
                else
                {
                    txtPayable.Enabled = false;
                }
                cboFrequency.Enabled = true;
                txtUntil.Enabled = true;
                txtDescription.Enabled = true;
                txtReference.Enabled = true;
                txtAmount.Enabled = true;
                txtCheck.Enabled = true;
                vs1.Enabled = true;
                EncumbranceFlag = true;
                temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
                txtVendor.Text = Strings.Format(temp, "00000");
                if (FCConvert.ToDouble(temp) != 0)
                {
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
                    txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
                    txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
                    txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
                    txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
                    txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
                    txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
                    txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
                    txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
                }
                else
                {
                    txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
                    txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
                    txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
                    txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
                    txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
                    txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
                    txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
                    txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
                }
                txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
                // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                txtReference.Text = FCConvert.ToString(rs.Get_Fields("PO"));
                counter = 1;
                rs2.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE (Amount + Adjustments - Liquidated) > 0 AND EncumbranceID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
                if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                {
                    // if there is any default information
                    rs2.MoveLast();
                    rs2.MoveFirst();
                    total = 0;
                    for (counter2 = 1; counter2 <= vs4.Rows - 1; counter2++)
                    {
                        if (Conversion.Val(vs4.TextMatrix(counter2, 0)) == -1)
                        {
                            total += 1;
                        }
                    }
                    NumberOfEncumbrances = total;
                    modBudgetaryMaster.Statics.EncumbranceDetail = new double[total + 1];
                    modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[total + 1];
                    ArrayCounter = 0;
                    while (rs2.EndOfFile() != true)
                    {
                        // get all the default information there is
                        if (Conversion.Val(vs4.TextMatrix(counter, 0)) == -1)
                        {
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            modBudgetaryMaster.Statics.EncumbranceDetail[ArrayCounter] = Conversion.Val(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("Adjustments") - rs2.Get_Fields_Decimal("Liquidated")) - FCConvert.ToDouble(vs4.TextMatrix(counter, 5));
                            modBudgetaryMaster.Statics.EncumbranceDetailRecords[ArrayCounter] = FCConvert.ToInt32(rs2.Get_Fields_Int32("ID"));
                            // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                            vs1.TextMatrix(ArrayCounter + 1, AccountCol, FCConvert.ToString(rs2.Get_Fields("account")));
                            vs1.TextMatrix(ArrayCounter + 1, DescriptionCol, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            vs1.TextMatrix(ArrayCounter + 1, AmountCol, FCConvert.ToString(Conversion.Val(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("Adjustments") - rs2.Get_Fields_Decimal("Liquidated")) - FCConvert.ToDouble(vs4.TextMatrix(counter, 5))));
                            if (Conversion.Val(txtVendor.Text) != 0)
                            {
                                vs1.TextMatrix(ArrayCounter + 1, TaxCol, "D");
                            }
                            else
                            {
                                vs1.TextMatrix(ArrayCounter + 1, TaxCol, "N");
                            }
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            vs1.TextMatrix(ArrayCounter + 1, EncumbranceCol, FCConvert.ToString(Conversion.Val(rs2.Get_Fields("Amount") + rs2.Get_Fields_Decimal("Adjustments") - rs2.Get_Fields_Decimal("Liquidated")) - FCConvert.ToDouble(vs4.TextMatrix(counter, 5))));
                            vs1.TextMatrix(ArrayCounter + 1, ProjectCol, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                            ArrayCounter += 1;
                        }
                        counter += 1;
                        rs2.MoveNext();
                    }
                    for (counter = counter; counter <= vs1.Rows - 1; counter++)
                    {
                        vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 1, "");
                        vs1.TextMatrix(counter, 2, "");
                        vs1.TextMatrix(counter, 3, "");
                        vs1.TextMatrix(counter, 4, "");
                    }
                    CalculateTotals();
                    txtAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
                    AmountToPay = TotalAmount;
                    lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
                    lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
                }
                vs4.Rows = 1;
                fraEncDetail.Visible = false;
                // make the list of records invisible
                if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")) != "")
                {
                    ans = MessageBox.Show(rsVendorInfo.Get_Fields_String("DataEntryMessage") + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this vendor message at this time?", "Data Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (ans == DialogResult.Yes)
                    {
                        rsVendorInfo.Edit();
                        rsVendorInfo.Set_Fields("DataEntryMessage", "");
                        rsVendorInfo.Update(true);
                    }
                }
                txtDescription.Focus();
            }
        }

        private void vs4_ClickEvent(object sender, System.EventArgs e)
        {
            if (vs4.Col == 0)
            {
                if (Conversion.Val(vs4.TextMatrix(vs4.Row, 0)) == 0)
                {
                    vs4.TextMatrix(vs4.Row, 0, FCConvert.ToString(-1));
                }
                else
                {
                    vs4.TextMatrix(vs4.Row, 0, FCConvert.ToString(0));
                }
            }
        }

        private void FillEncumbranceDetail()
        {
            int counter;
            clsDRWrapper rsPendingInfo = new clsDRWrapper();
            clsDRWrapper rs = new clsDRWrapper();
            // fills the grid with the detail information of the encumbrance
            TempEncAccount = FCConvert.ToInt32(vs3.TextMatrix(vs3.Row, 0));
            // get the account number of that record
            if (TempEncAccount != 0)
            {
                // if there is a valid account number
                rs.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE (Amount + Adjustments - Liquidated) > 0 AND EncumbranceID = " + FCConvert.ToString(TempEncAccount));
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    // if there is a record
                    rs.MoveLast();
                    rs.MoveFirst();
                    for (counter = 1; counter <= rs.RecordCount(); counter++)
                    {
                        vs4.Rows += 1;
                        vs4.TextMatrix(counter, 1, FCConvert.ToString(rs.Get_Fields_String("Description")));
                        // TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
                        vs4.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields("account")));
                        vs4.TextMatrix(counter, 3, FCConvert.ToString(rs.Get_Fields_String("Project")));
                        // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        vs4.TextMatrix(counter, 4, Strings.Format(rs.Get_Fields("Amount") + rs.Get_Fields_Decimal("Adjustments") - rs.Get_Fields_Decimal("Liquidated"), "#,##0.00"));
                        rsPendingInfo.OpenRecordset("SELECT SUM(APJournalDetail.Encumbrance) as PendingTotal FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.Status <> 'P' AND APJournalDetail.EncumbranceDetailRecord = " + rs.Get_Fields_Int32("ID"));
                        if (rsPendingInfo.EndOfFile() != true && rsPendingInfo.BeginningOfFile() != true)
                        {
                            // TODO Get_Fields: Field [PendingTotal] not found!! (maybe it is an alias?)
                            vs4.TextMatrix(counter, 5, Strings.Format(FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsPendingInfo.Get_Fields("PendingTotal")))), "#,##0.00"));
                        }
                        else
                        {
                            vs4.TextMatrix(counter, 5, Strings.Format(0, "#,##0.00"));
                        }
                        rs.MoveNext();
                    }
                }
                fraEncDetail.Visible = true;
                vs4.Focus();
                Frame2.Visible = false;
            }
        }

        private void SetCombo(int x)
        {
            int counter;
            for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
            {
                if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4))
                {
                    cboJournal.SelectedIndex = counter;
                    cboSaveJournal.SelectedIndex = counter;
                    return;
                }
            }
        }

        private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
        }

        private void cboJournal_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
        }

        private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (fraJournalSave.Visible == true)
            {
                if (cboSaveJournal.SelectedIndex == 0)
                {
                    txtJournalDescription.Visible = true;
                    lblJournalDescription.Visible = true;
                    lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
                    txtJournalDescription.Focus();
                }
                else
                {
                    txtJournalDescription.Visible = false;
                    lblJournalDescription.Visible = false;
                    lblSaveInstructions.Text = "Please click the OK button to save";
                    cmdOKSave.Focus();
                }
            }
        }

        private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
        }

        private void SaveJournal()
        {
            int TempJournal = 0;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            int intPeriod;
            clsDRWrapper rsJournalCheck = new clsDRWrapper();
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsVendorInfo = new clsDRWrapper();
            clsDRWrapper rsPurchaseOrders = new clsDRWrapper();
            txtVendor.Focus();
            intPeriod = -1;
            if (cboSaveJournal.SelectedIndex == 0)
            {
                Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
                if (Master.EndOfFile() != true)
                {
                    // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
                }
                else
                {
                    TempJournal = 1;
                }
                Master.AddNew();
                Master.Set_Fields("JournalNumber", TempJournal);
                Master.Set_Fields("Status", "E");
                Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                Master.Set_Fields("StatusChangeDate", DateTime.Today);
                Master.Set_Fields("Description", txtJournalDescription.Text);
                Master.Set_Fields("Type", "AP");
                Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
                Master.Update();
                Master.Reset();
                modBudgetaryAccounting.UnlockJournal();
                blnJournalLocked = false;
            }
            else
            {
                Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status <> 'E' AND Status <> 'V'");
                if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
                {
                    MessageBox.Show("Changes to this journal cannot be saved because it has gone through too much of the AP process.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    blnSaved = true;
                    Close();
                    return;
                }
                Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
                // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text) && FCConvert.ToString(Master.Get_Fields_String("Status")) != "E")
                {
                    // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                    ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + Master.Get_Fields("Period") + ".  Do you wish to add your entry to this Journal?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.Yes)
                    {
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        intPeriod = FCConvert.ToInt32(Master.Get_Fields("Period"));
                    }
                    else
                    {
                        cboJournal.SelectedIndex = 0;
                        // mnuProcessSave_Click
                        ProcessSave();
                        return;
                    }
                }
                modBudgetaryAccounting.UnlockJournal();
                blnJournalLocked = false;
                if (Master.IsntAnything())
                {
                    // do nothing
                }
                else
                {
                    Master.Reset();
                }
            }
            // continue with add because you now know the journal number
            if (apJournalId != 0)
            {
                if (cboSaveJournal.SelectedIndex != 0)
                {
                    modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                }
                else
                {
                    modBudgetaryMaster.RemoveExistingCreditMemos_2(TempJournal);
                }
                rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(apJournalId));
                if (rs.EndOfFile())
                {
                    rs.AddNew();
                }
                else
                {
                    rs.Edit();
                }
                if (FCConvert.ToInt32(rs.Get_Fields_Int32("PurchaseOrderID")) != 0)
                {
                    if (ChosenPurchaseOrder != FCConvert.ToInt32(rs.Get_Fields_Int32("PurchaseOrderID")))
                    {
                        rsPurchaseOrders.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + rs.Get_Fields_Int32("PurchaseOrderID"), "TWBD0000.vb1");
                        if (rsPurchaseOrders.EndOfFile() != true && rsPurchaseOrders.BeginningOfFile() != true)
                        {
                            rsPurchaseOrders.Edit();
                            rsPurchaseOrders.Set_Fields("Closed", false);
                            rsPurchaseOrders.Update();
                        }
                    }
                }
                if (cboSaveJournal.SelectedIndex != 0)
                {
                    rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                }
                else
                {
                    rs.Set_Fields("JournalNumber", TempJournal);
                }
                rs.Set_Fields("VendorNumber", txtVendor.Text);
                if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
                {
                    rs.Set_Fields("TempVendorName", txtAddress[0].Text);
                    rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
                    rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
                    rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
                    rs.Set_Fields("TempVendorCity", Strings.Trim(txtCity.Text));
                    rs.Set_Fields("TempVendorState", Strings.Trim(txtState.Text));
                    rs.Set_Fields("TempVendorZip", Strings.Trim(txtZip.Text));
                    rs.Set_Fields("TempVendorZip4", Strings.Trim(txtZip4.Text));
                }
                else
                {
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)));
                    if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
                    {
                        if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))) != Strings.Trim(txtAddress[1].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"))) != Strings.Trim(txtAddress[2].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"))) != Strings.Trim(txtAddress[3].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"))) != Strings.Trim(txtCity.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != Strings.Trim(txtState.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) != Strings.Trim(txtZip.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != Strings.Trim(txtZip4.Text))
                        {
                            ans = MessageBox.Show("The vendor address you have entered into this journal is different than the one saved for that vendor.  Do you wish to save this new address for the vendor?", "Save New Address", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                rsVendorInfo.Edit();
                                rsVendorInfo.Set_Fields("CheckAddress1", Strings.Trim(txtAddress[1].Text));
                                rsVendorInfo.Set_Fields("CheckAddress2", Strings.Trim(txtAddress[2].Text));
                                rsVendorInfo.Set_Fields("CheckAddress3", Strings.Trim(txtAddress[3].Text));
                                rsVendorInfo.Set_Fields("CheckCity", Strings.Trim(txtCity.Text));
                                rsVendorInfo.Set_Fields("CheckState", Strings.Trim(txtState.Text));
                                rsVendorInfo.Set_Fields("CheckZip", Strings.Trim(txtZip.Text));
                                rsVendorInfo.Set_Fields("CheckZip4", Strings.Trim(txtZip4.Text));
                                rsVendorInfo.Update(false);
                            }
                        }
                    }
                }
                if (apJournalId != 0 && blnJournalEdit)
                {
                    if (Information.IsDate(rs.Get_FieldsDataType("Payable")))
                    {
                        datOldPayableDate = (DateTime)rs.Get_Fields_DateTime("Payable");
                    }
                    else
                    {
                        datNewPayableDate = DateTime.Parse("12:00:00 AM");
                    }
                    datNewPayableDate = DateAndTime.DateValue(txtPayable.Text);
                }
                rs.Set_Fields("Payable", txtPayable.Text);
                if (cboFrequency.SelectedIndex != -1)
                {
                    if (Information.IsDate(txtUntil.Text))
                    {
                        rs.Set_Fields("Until", txtUntil.Text);
                    }
                    else
                    {
                        rs.Set_Fields("Until", null);
                    }
                    rs.Set_Fields("Frequency", cboFrequency.Text);
                }
                if (intPeriod == -1)
                {
                    rs.Set_Fields("Period", txtPeriod.Text);
                }
                else
                {
                    rs.Set_Fields("Period", intPeriod);
                }
                rs.Set_Fields("Description", txtDescription.Text);
                rs.Set_Fields("Reference", txtReference.Text);
                rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
                if (Conversion.Val(txtCheck.Text) != 0)
                {
                    rs.Set_Fields("CheckNumber", Strings.Trim(FCConvert.ToString(Conversion.Val(txtCheck.Text))));
                }
                else
                {
                    rs.Set_Fields("CheckNumber", "");
                }
                if (chkUseAltCashAccount.CheckState == CheckState.Checked)
                {
                    if (modValidateAccount.AccountValidate(vsAltCashAccount.TextMatrix(0, 0)))
                    {
                        rs.Set_Fields("UseAlternateCash", true);
                        rs.Set_Fields("AlternateCashAccount", vsAltCashAccount.TextMatrix(0, 0));
                    }
                    else
                    {
                        rs.Set_Fields("UseAlternateCash", false);
                        rs.Set_Fields("AlternateCashAccount", "");
                    }
                }
                else
                {
                    rs.Set_Fields("UseAlternateCash", false);
                    rs.Set_Fields("AlternateCashAccount", "");
                }
                if (Strings.Trim(txtCheck.Text) != "")
                {
                    rs.Set_Fields("PrepaidCheck", true);
                }
                else
                {
                    rs.Set_Fields("PrepaidCheck", false);
                }
                if (chkSeperate.CheckState == CheckState.Checked)
                {
                    rs.Set_Fields("seperate", 1);
                }
                else
                {
                    rs.Set_Fields("seperate", 0);
                }
                apJournalId = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
                rs.Set_Fields("Status", "E");
                rs.Update();
            }
            else
            {
                rs.OmitNullsOnInsert = true;
                rs.OpenRecordset("SELECT * FROM APJournal where id = -1");
                if (cboSaveJournal.SelectedIndex != 0)
                {
                    modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                }
                else
                {
                    modBudgetaryMaster.RemoveExistingCreditMemos_2(TempJournal);
                }
                rs.AddNew();
                if (cboSaveJournal.SelectedIndex != 0)
                {
                    rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
                }
                else
                {
                    rs.Set_Fields("JournalNumber", TempJournal);
                }
                rs.Set_Fields("VendorNumber", txtVendor.Text);
                if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
                {
                    rs.Set_Fields("TempVendorName", txtAddress[0].Text);
                    rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
                    rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
                    rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
                    rs.Set_Fields("TempVendorCity", txtCity.Text);
                    rs.Set_Fields("TempVendorState", txtState.Text);
                    rs.Set_Fields("TempVendorZip", txtZip.Text);
                    rs.Set_Fields("TempVendorZip4", txtZip4.Text);
                }
                else
                {
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)));
                    if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
                    {
                        if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))) != Strings.Trim(txtAddress[1].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"))) != Strings.Trim(txtAddress[2].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"))) != Strings.Trim(txtAddress[3].Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"))) != Strings.Trim(txtCity.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != Strings.Trim(txtState.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) != Strings.Trim(txtZip.Text) || Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != Strings.Trim(txtZip4.Text))
                        {
                            ans = MessageBox.Show("The vendor address you have entered into this journal is different than the one saved for that vendor.  Do you wish to save this new address for the vendor?", "Save New Address", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                rsVendorInfo.Edit();
                                rsVendorInfo.Set_Fields("CheckAddress1", Strings.Trim(txtAddress[1].Text));
                                rsVendorInfo.Set_Fields("CheckAddress2", Strings.Trim(txtAddress[2].Text));
                                rsVendorInfo.Set_Fields("CheckAddress3", Strings.Trim(txtAddress[3].Text));
                                rsVendorInfo.Set_Fields("CheckCity", Strings.Trim(txtCity.Text));
                                rsVendorInfo.Set_Fields("CheckState", Strings.Trim(txtState.Text));
                                rsVendorInfo.Set_Fields("CheckZip", Strings.Trim(txtZip.Text));
                                rsVendorInfo.Set_Fields("CheckZip4", Strings.Trim(txtZip4.Text));
                                rsVendorInfo.Update(false);
                            }
                        }
                    }
                }
                rs.Set_Fields("Payable", txtPayable.Text);
                if (cboFrequency.SelectedIndex != -1)
                {
                    if (Information.IsDate(txtUntil.Text))
                    {
                        rs.Set_Fields("Until", txtUntil.Text);
                    }
                    else
                    {
                        rs.Set_Fields("Until", null);
                    }
                    rs.Set_Fields("Frequency", cboFrequency.Text);
                }
                if (intPeriod == -1)
                {
                    rs.Set_Fields("Period", txtPeriod.Text);
                }
                else
                {
                    rs.Set_Fields("Period", intPeriod);
                }
                rs.Set_Fields("Description", txtDescription.Text);
                rs.Set_Fields("Reference", txtReference.Text);
                rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
                if (Conversion.Val(txtCheck.Text) != 0)
                {
                    rs.Set_Fields("CheckNumber", Strings.Trim(FCConvert.ToString(Conversion.Val(txtCheck.Text))));
                }
                else
                {
                    rs.Set_Fields("CheckNumber", "");
                }
                if (chkUseAltCashAccount.CheckState == CheckState.Checked)
                {
                    if (modValidateAccount.AccountValidate(vsAltCashAccount.TextMatrix(0, 0)))
                    {
                        rs.Set_Fields("UseAlternateCash", true);
                        rs.Set_Fields("AlternateCashAccount", vsAltCashAccount.TextMatrix(0, 0));
                    }
                    else
                    {
                        rs.Set_Fields("UseAlternateCash", false);
                        rs.Set_Fields("AlternateCashAccount", "");
                    }
                }
                else
                {
                    rs.Set_Fields("UseAlternateCash", false);
                    rs.Set_Fields("AlternateCashAccount", "");
                }
                if (Strings.Trim(txtCheck.Text) != "")
                {
                    rs.Set_Fields("PrepaidCheck", true);
                }
                else
                {
                    rs.Set_Fields("PrepaidCheck", false);
                }
                if (chkSeperate.CheckState == CheckState.Checked)
                {
                    rs.Set_Fields("seperate", 1);
                }
                else
                {
                    rs.Set_Fields("seperate", 0);
                }
                rs.Set_Fields("Returned", "N");
                rs.Set_Fields("Status", "E");
                rs.Update();
                apJournalId = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
            }
            if (PurchaseOrderFlag)
            {
                rsPurchaseOrders.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + FCConvert.ToString(ChosenPurchaseOrder), "TWBD0000.vb1");
                if (rsPurchaseOrders.EndOfFile() != true && rsPurchaseOrders.BeginningOfFile() != true)
                {
                    rsPurchaseOrders.Edit();
                    rsPurchaseOrders.Set_Fields("Closed", true);
                    rsPurchaseOrders.Update();
                }
            }
            SaveDetails();
            if (cboSaveJournal.SelectedIndex != 0)
            {
                modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
            }
            else
            {
                modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
            }
            if (cboSaveJournal.SelectedIndex != 0)
            {
                modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
                modBudgetaryMaster.Statics.CurrentAPEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
            }
            else
            {
                modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(TempJournal));
                modBudgetaryMaster.Statics.CurrentAPEntry = TempJournal;
                FillJournalCombo();
            }
            if (!modBudgetaryMaster.Statics.blnAPEdit)
            {
                fraJournalSave.Visible = false;
                if (cboSaveJournal.SelectedIndex == 0)
                {
                    //FC:FINAL:DSE #i600 Use FCComboBox.AddItem instead of FCComboBox.Items[...] to add new items
                    //cboJournal.Items[cboJournal.ListCount] = modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4) + " - " + txtJournalDescription.Text;
                    //cboSaveJournal.Items[cboJournal.ListCount - 1] = modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4) + " - " + txtJournalDescription.Text;
                    cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4) + " - " + txtJournalDescription.Text);
                    cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4) + " - " + txtJournalDescription.Text);
                    cboJournal.SelectedIndex = cboJournal.Items.Count - 1;
                    cboSaveJournal.SelectedIndex = cboJournal.Items.Count - 1;
                    txtJournalDescription.Text = "";
                }
                if (boolF12)
                {
                    //FC:FINAL:SBE - #4611 - force validate event on txtVendor input by changing focus to another control
                    vs1.Focus();
                    ClearData();
                }
                txtUntil.Focus();
                //txtAmount.Focus();

                App.DoEvents();
                txtVendor.Focus();
                blnSaved = true;
                if (blnUnload)
                {
                    ProcessQuit();
                    // processsave
                }
            }
            else
            {
                // If mnuProcessNextEntry.Enabled = False Or blnUnload Then
                if (blnUnload)
                {
                    blnSaved = true;
                    Close();
                }
                else
                {
                    // blnSaved = False
                    // mnuProcessNextEntry_Click
                    blnSaved = true;
                }
            }
        }

        private void FillJournalCombo()
        {
            clsDRWrapper rsJournalPeriod = new clsDRWrapper();
            int counter;
            clsDRWrapper rsMaster = new clsDRWrapper();
            cboJournal.AddItem("Auto");
            cboSaveJournal.AddItem("Auto");
            if (modBudgetaryMaster.Statics.blnAPEdit)
            {
                // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                if (FCConvert.ToString(rsMaster.Get_Fields("Type")) == "AC")
                {
                    rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' ORDER BY JournalNumber DESC");
                    lblReadOnly.Visible = true;
                    txtPeriod.Enabled = false;
                    cboJournal.Enabled = false;
                    txtVendor.Enabled = false;
                    for (counter = 0; counter <= 3; counter++)
                    {
                        txtAddress[counter].Enabled = false;
                    }
                    txtCity.Enabled = false;
                    txtState.Enabled = false;
                    txtZip.Enabled = false;
                    txtZip4.Enabled = false;
                    txtPayable.Enabled = false;
                    cboFrequency.Enabled = false;
                    txtUntil.Enabled = false;
                    txtDescription.Enabled = false;
                    txtReference.Enabled = false;
                    txtAmount.Enabled = false;
                    txtCheck.Enabled = false;
                    vs1.Enabled = false;
                    mnuProcessDelete.Enabled = false;
                    mnuProcessDeleteEntry.Enabled = false;
                    mnuFileChangePayableDate.Enabled = false;
                    mnuProcessEncumbrance.Enabled = false;
                    mnuFileShowPurchaseOrders.Enabled = false;
                    btnProcessSearch.Enabled = false;
                    btnProcessSave.Enabled = false;
                    mnuFileAddVendor.Enabled = false;
                }
                else
                {
                    rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE (Status = 'E' or Status = 'V') AND Type = 'AP' ORDER BY JournalNumber DESC");
                }
            }
            else
            {
                rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE (Status = 'E' or Status = 'V') AND Type = 'AP' ORDER BY JournalNumber DESC");
            }
            if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
            {
                rsJournalPeriod.MoveLast();
                rsJournalPeriod.MoveFirst();
                for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
                {
                    //FC:FINAL:DSE #i540 Use .AddItem method call to add items to the FCComboBox
                    while (cboJournal.ListCount < counter)
                    {
                        cboJournal.AddItem("");
                    }
                    while (cboSaveJournal.ListCount < counter)
                    {
                        cboSaveJournal.AddItem("");
                    }
                    // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                    if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
                    {
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                        cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
                    }
                    rsJournalPeriod.MoveNext();
                }
            }
        }

        private void SetCustomFormColors()
        {
            lblExpense.ForeColor = Color.Blue;
            lblReadOnly.ForeColor = Color.Red;
            lblReadOnly.Font = FCUtils.SetFontStyle(lblReadOnly.Font, FontStyle.Bold, true);
            //frmSearch.BackColor = SystemColors.AppWorkspace;
        }

        private void SaveJournalInfo()
        {
            int counter;
            bool NoSaveFlag = false;
            int TempJournal = 0;
            // vbPorter upgrade warning: answer As short, int --> As DialogResult
            DialogResult answer;
            int intBadRow = 0;
            int intBadCol = 0;
            bool blnWrongFund;

            Support.SendKeys("{TAB}", false);
            //Application.DoEvents();
            // make sure all data has been filled in
            if (txtVendor.Text == "")
            {
                MessageBox.Show("You must fill in the Vendor data before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtVendor.Focus();
                return;
            }
            else if (Conversion.Val(txtVendor.Text) == 0 && txtAddress[0].Text == "")
            {
                MessageBox.Show("You must fill in the Vendor data before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //txtAddress[0].Focus();
                AdvanceToVendor();
                return;
            }
            if (txtDescription.Text == "")
            {
                if (txtReference.Text == "")
                {
                    MessageBox.Show("You must fill in the Description or the Reference Number before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtReference.Focus();
                    return;
                }
            }
            if (Conversion.Val(txtPeriod.Text) > 12 || Conversion.Val(txtPeriod.Text) < 1)
            {
                MessageBox.Show("The Accounting Period must be 1 - 12", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPeriod.Focus();
                return;
            }
            if (FCConvert.ToDouble(Strings.Format((AmountToPay - TotalAmount), "####.00")) != 0)
            {
                MessageBox.Show("The Remaining Amount must be 0.00 before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (txtAmount.Text == "" || txtAmount.Text == ".00" || txtAmount.Text == "0.00" || !Information.IsNumeric(txtAmount.Text))
            {
                MessageBox.Show("There must be an Amount to Pay in a Journal Entry", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAmount.Focus();
                return;
            }
            if (cboFrequency.SelectedIndex != -1)
            {
                if (!txtUntil.IsEmpty)
                {
                    if (DateAndTime.DateValue(txtUntil.Text).ToOADate() < DateTime.Today.ToOADate())
                    {
                        MessageBox.Show("The Until Date must be a future date", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtUntil.Focus();
                        return;
                    }
                }
            }
            if (chkUseAltCashAccount.CheckState == CheckState.Checked)
            {
                if (!modValidateAccount.AccountValidate(vsAltCashAccount.TextMatrix(0, 0)) || Strings.Left(vsAltCashAccount.TextMatrix(0, 0), 1) != "G")
                {
                    MessageBox.Show("You must enter a valid alternate cash account before you may proceed.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    vsAltCashAccount.Focus();
                    return;
                }
            }
            blnWrongFund = false;
            for (counter = 1; counter <= vs1.Rows - 1; counter++)
            {
                if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) != 0 || FCConvert.ToDecimal(vs1.TextMatrix(counter, DiscountCol)) != 0)
                {
                    if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) == 0)
                    {
                        intBadRow = counter;
                        intBadCol = AmountCol;
                        NoSaveFlag = true;
                        break;
                    }
                    else if (vs1.TextMatrix(counter, AccountCol) == "" || Strings.InStr(1, vs1.TextMatrix(counter, AccountCol), "_", CompareConstants.vbBinaryCompare) != 0)
                    {
                        intBadRow = counter;
                        intBadCol = AccountCol;
                        NoSaveFlag = true;
                        break;
                    }
                    else if (vs1.TextMatrix(counter, DescriptionCol) == "")
                    {
                        intBadRow = counter;
                        intBadCol = DescriptionCol;
                        NoSaveFlag = true;
                        break;
                    }
                    if (chkUseAltCashAccount.CheckState == CheckState.Checked)
                    {
                        if (modBudgetaryMaster.GetAccountFund(vs1.TextMatrix(counter, AccountCol)) != modBudgetaryMaster.GetAccountFund(vsAltCashAccount.TextMatrix(0, 0)))
                        {
                            intBadRow = counter;
                            intBadCol = AccountCol;
                            NoSaveFlag = true;
                            blnWrongFund = true;
                            break;
                        }
                    }
                    if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
                    {
                        answer = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (answer == DialogResult.No)
                        {
                            vs1.Select(counter, AccountCol);
                            vs1.Focus();
                            return;
                        }
                        else
                        {
                            modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Accounts Payable Data Entry");
                        }
                    }
                }
            }
            if (NoSaveFlag == true)
            {
                NoSaveFlag = false;
                if (blnWrongFund)
                {
                    MessageBox.Show("All the accounts you use in this AP entry must have the same fund as the alternate cash account you have chosen to use.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Each entry must have a Description, Account, 1099 Entry, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                vs1.Select(intBadRow, intBadCol);
                vs1_RowColChange(vs1, EventArgs.Empty);
                return;
            }
            else
            {
                // if it is a new journal then set the save flag so no one else can save at the same time
                if (cboJournal.SelectedIndex == 0)
                {
                    if (modBudgetaryAccounting.LockJournal() == false)
                    {
                        MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    blnJournalLocked = true;
                    // get the next available journal
                    Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
                    if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                    {
                        Master.MoveLast();
                        Master.MoveFirst();
                        // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
                    }
                    else
                    {
                        TempJournal = 1;
                    }
                    // ask the user if this is the journal they want the information saved under
                    answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    // if they choose to cancel then end save routine
                    if (answer == DialogResult.Cancel)
                    {
                        Master.Reset();
                        modBudgetaryAccounting.UnlockJournal();
                        blnJournalLocked = false;
                        if (Master.IsntAnything())
                        {
                            // do nothing
                        }
                        else
                        {
                            Master.Reset();
                        }
                        return;
                        // if they dont want to save under this journal number then let them pick  another existing journal
                    }
                    else if (answer == DialogResult.No)
                    {
                        fraJournalSave.Visible = true;
                        lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
                        lblJournalSave.Visible = true;
                        cboSaveJournal.Visible = true;
                        lblJournalDescription.Visible = false;
                        txtJournalDescription.Visible = false;
                        if (Master.IsntAnything())
                        {
                            // do nothing
                        }
                        else
                        {
                            Master.Reset();
                        }
                        cboSaveJournal.Focus();
                        return;
                        // if they do want this journal have them put in a description for it
                    }
                    else
                    {
                        fraJournalSave.Visible = true;
                        lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
                        lblJournalSave.Visible = false;
                        cboSaveJournal.Visible = false;
                        txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " AP";
                        lblJournalDescription.Visible = true;
                        txtJournalDescription.Visible = true;
                        txtJournalDescription.Focus();
                        txtJournalDescription.SelectionStart = 0;
                        txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
                        return;
                    }
                    // if not a new journal then just save it to the existing journal
                }
                else
                {
                    SaveJournal();
                }
            }
            blnSaved = true;
        }

        private double GetOriginalBudget(string strAcct)
        {
            double GetOriginalBudget = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                    GetOriginalBudget = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("OriginalBudgetTotal")));
                }
                else
                {
                    GetOriginalBudget = 0;
                }
            }
            else
            {
                GetOriginalBudget = 0;
            }
            return GetOriginalBudget;
        }

        private double GetBudgetAdjustments(string strAcct)
        {
            double GetBudgetAdjustments = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                    GetBudgetAdjustments = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal")));
                }
                else
                {
                    GetBudgetAdjustments = 0;
                }
            }
            else
            {
                GetBudgetAdjustments = 0;
            }
            return GetBudgetAdjustments;
        }

        private double GetPendingYTDDebit(ref string strAcct)
        {
            double GetPendingYTDDebit = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                    GetPendingYTDDebit = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("PendingDebitsTotal")));
                }
                else
                {
                    GetPendingYTDDebit = 0;
                }
            }
            else
            {
                GetPendingYTDDebit = 0;
            }
            return GetPendingYTDDebit;
        }

        private double GetPendingYTDCredit(ref string strAcct)
        {
            double GetPendingYTDCredit = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                    GetPendingYTDCredit = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("PendingCreditsTotal"))) * -1;
                }
                else
                {
                    GetPendingYTDCredit = 0;
                }
            }
            else
            {
                GetPendingYTDCredit = 0;
            }
            return GetPendingYTDCredit;
        }

        private double GetPendingYTDNet(string strAcct)
        {
            double GetPendingYTDNet = 0;
            GetPendingYTDNet = GetPendingYTDDebit(ref strAcct) - GetPendingYTDCredit(ref strAcct);
            return GetPendingYTDNet;
        }

        private double GetYTDDebit(ref string strAcct)
        {
            double GetYTDDebit = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                    GetYTDDebit = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("PostedDebitsTotal")));
                }
                else
                {
                    GetYTDDebit = 0;
                }
            }
            else
            {
                GetYTDDebit = 0;
            }
            return GetYTDDebit;
        }

        private double GetYTDCredit(ref string strAcct)
        {
            double GetYTDCredit = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                    GetYTDCredit = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("PostedCreditsTotal"))) * -1;
                }
                else
                {
                    GetYTDCredit = 0;
                }
            }
            else
            {
                GetYTDCredit = 0;
            }
            return GetYTDCredit;
        }

        private double GetYTDNet(string strAcct)
        {
            double GetYTDNet = 0;
            GetYTDNet = GetYTDDebit(ref strAcct) - GetYTDCredit(ref strAcct);
            return GetYTDNet;
        }

        private double GetEncumbrance(string strAcct)
        {
            double GetEncumbrance = 0;
            if (Strings.Left(strAcct, 1) == "E")
            {
                if (rsYTDActivity.FindFirstRecord("Account", strAcct))
                {
                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                    GetEncumbrance = Conversion.Val(FCConvert.ToString(rsYTDActivity.Get_Fields("EncumbActivityTotal")));
                }
                else
                {
                    GetEncumbrance = 0;
                }
            }
            else
            {
                GetEncumbrance = 0;
            }
            return GetEncumbrance;
        }

        private void RetrieveInfo()
        {
            int HighDate = 0;
            int LowDate;
            string strPeriodCheck = "";
            string strTable = "";
            LowDate = modBudgetaryMaster.Statics.FirstMonth;
            if (modBudgetaryMaster.Statics.FirstMonth == 1)
            {
                HighDate = 12;
            }
            else
            {
                HighDate = LowDate - 1;
            }
            if (LowDate > HighDate)
            {
                strPeriodCheck = "OR";
            }
            else
            {
                strPeriodCheck = "AND";
            }
            rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
        }
        // vbPorter upgrade warning: curAmt As Decimal	OnWrite(double, Decimal)
        // vbPorter upgrade warning: 'Return' As bool	OnWriteFCConvert.ToInt16(
        private bool ValidateBalance_6(string strAcct, Decimal curAmt, int lngRow)
        {
            return ValidateBalance(strAcct, curAmt, lngRow);
        }

        private bool ValidateBalance(string strAcct, Decimal curAmt, int lngRow)
        {
            bool ValidateBalance = false;
            int intLevel = 0;
            // vbPorter upgrade warning: curNetBudget As Decimal	OnWriteFCConvert.ToDouble(
            Decimal curNetBudget;
            // vbPorter upgrade warning: curYTDPostedNet As Decimal	OnWriteFCConvert.ToDouble(
            Decimal curYTDPostedNet;
            // vbPorter upgrade warning: curYTDPendingNet As Decimal	OnWriteFCConvert.ToDouble(
            Decimal curYTDPendingNet;
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsAcctInfo = new clsDRWrapper();
            if (modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
            {
                intLevel = 1;
            }
            else
            {
                intLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("BalanceValidation")))));
            }
            if (Strings.Left(strAcct, 1) != "E" && Strings.Left(strAcct, 1) != "P")
            {
                ValidateBalance = FCConvert.ToBoolean(1);
            }
            else
            {
                rsAcctInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + strAcct + "'");
                if (rsAcctInfo.EndOfFile() != true && rsAcctInfo.BeginningOfFile() != true)
                {
                    if (rsAcctInfo.Get_Fields_Boolean("ValidateBalance") == false)
                    {
                        ValidateBalance = FCConvert.ToBoolean(1);
                        return ValidateBalance;
                    }
                }
                curNetBudget = FCConvert.ToDecimal(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct));
                curYTDPostedNet = FCConvert.ToDecimal(GetYTDNet(strAcct) + GetEncumbrance(strAcct));
                curYTDPendingNet = FCConvert.ToDecimal(GetPendingYTDNet(strAcct));
                if (curNetBudget - curYTDPostedNet - curYTDPendingNet - curAmt < 0)
                {
                    if (intLevel == 1)
                    {
                        MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "You may not enter this amount because it would put this account over budget.", "Account Over Budget", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ValidateBalance = FCConvert.ToBoolean(0);
                    }
                    else if (intLevel == 2)
                    {
                        if (FCConvert.ToString(vs1.RowData(lngRow)) == strAcct)
                        {
                            ValidateBalance = FCConvert.ToBoolean(1);
                        }
                        else
                        {
                            ans = MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "Entering this amount would put this account over budget.  Do you wish to continue?", "Account Over Budget", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (ans == DialogResult.Yes)
                            {
                                ValidateBalance = FCConvert.ToBoolean(1);
                                vs1.RowData(lngRow, strAcct);
                            }
                            else
                            {
                                ValidateBalance = FCConvert.ToBoolean(0);
                            }
                        }
                    }
                    else
                    {
                        ValidateBalance = FCConvert.ToBoolean(1);
                    }
                }
                else
                {
                    ValidateBalance = FCConvert.ToBoolean(1);
                }
            }
            return ValidateBalance;
        }

        private void LoadDeptCombo()
        {
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsEncData = new clsDRWrapper();
            cboEncDept.Clear();
            rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE convert(int, IsNull(Division, 0)) = 0 AND Department IN (SELECT substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") FROM EncumbranceDetail WHERE Amount + Adjustments - Liquidated <> 0) ORDER BY Department");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                do
                {
                    cboEncDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
                    rs.MoveNext();
                }
                while (rs.EndOfFile() != true);
            }
        }

        private void LoadPurchaseOrderDeptCombo()
        {
            clsDRWrapper rs = new clsDRWrapper();
            clsDRWrapper rsEncData = new clsDRWrapper();
            cboPurchaseOrderDepartment.Clear();
            rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE convert(int, IsNull(Division, 0)) = 0 AND Department IN (SELECT Department FROM PurchaseOrders WHERE Closed = 0) ORDER BY Department");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                do
                {
                    cboPurchaseOrderDepartment.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
                    rs.MoveNext();
                }
                while (rs.EndOfFile() != true);
            }
        }

        private void SetEncDeptCombo(ref string strDepartment)
        {
            int counter;
            cboEncDept.SelectedIndex = -1;
            for (counter = 0; counter <= cboEncDept.Items.Count - 1; counter++)
            {
                if (Strings.Left(cboEncDept.Items[counter].ToString(), Strings.Trim(strDepartment).Length) == Strings.Trim(strDepartment))
                {
                    cboEncDept.SelectedIndex = counter;
                    break;
                }
            }
        }

        private void SetPurchaseOrderDeptCombo(ref string strDepartment)
        {
            int counter;
            cboPurchaseOrderDepartment.SelectedIndex = -1;
            for (counter = 0; counter <= cboPurchaseOrderDepartment.Items.Count - 1; counter++)
            {
                if (Strings.Left(cboPurchaseOrderDepartment.Items[counter].ToString(), Strings.Trim(strDepartment).Length) == Strings.Trim(strDepartment))
                {
                    cboPurchaseOrderDepartment.SelectedIndex = counter;
                    break;
                }
            }
        }

        public void Init(ref cAPJournal aJourn)
        {
            datPayableDate = DateTime.Today;
            intSavedPeriod = DateTime.Today.Month;

            if (!(aJourn.ID == 0))
            {
                txtVendor.Text = modValidateAccount.GetFormat_6(aJourn.VendorNumber.ToString(), 5);
                if (aJourn.VendorNumber > 0)
                {
                    cVendor vend;
                    vend = aJourn.Vendor;
                    if (!(vend == null))
                    {
                        txtAddress[0].Text = vend.CheckName;
                        txtAddress[1].Text = vend.CheckAddress1;
                        txtAddress[2].Text = vend.CheckAddress2;
                        txtAddress[3].Text = vend.CheckAddress3;
                        txtCity.Text = vend.CheckCity;
                        txtState.Text = vend.CheckState;
                        txtZip.Text = vend.CheckZip;
                        txtZip4.Text = vend.CheckZip4;
                    }
                    else
                    {
                        txtAddress[0].Text = "";
                        txtAddress[1].Text = "";
                        txtAddress[2].Text = "";
                        txtAddress[3].Text = "";
                        txtCity.Text = "";
                        txtState.Text = "";
                        txtZip.Text = "";
                        txtZip4.Text = "";
                    }
                }
                else
                {
                    txtAddress[0].Text = aJourn.TempVendorName;
                    txtAddress[1].Text = aJourn.TempVendorAddress1;
                    txtAddress[2].Text = aJourn.TempVendorAddress2;
                    txtAddress[3].Text = aJourn.TempVendorAddress3;
                    txtCity.Text = aJourn.TempVendorCity;
                    txtState.Text = aJourn.TempVendorState;
                    txtZip.Text = aJourn.TempVendorZip;
                    txtZip4.Text = aJourn.TempVendorZip4;
                }
                if (Information.IsDate(aJourn.PayableDate))
                {
                    txtPayable.Text = Strings.Format(aJourn.PayableDate, "MM/dd/yyyy");
                }
                else
                {
                    txtPayable.Text = "";
                }
                if (Information.IsDate(aJourn.UntilDate))
                {
                    txtUntil.Text = Strings.Format(aJourn.UntilDate, "MM/dd/yyyy");
                }
                else
                {
                    txtUntil.Text = "";
                }
                if (aJourn.Frequency != "")
                {
                    cboFrequency.Text = aJourn.Frequency;
                }
                txtDescription.Text = aJourn.Description;
                txtAmount.Text = Strings.Format(aJourn.Amount, "#,##0.00");
                blnJournalEdit = true;
                if (aJourn.Period > 0)
                {
                    txtPeriod.Text = FCConvert.ToString(aJourn.Period);
                }
                txtReference.Text = aJourn.Reference;
                txtCheck.Text = aJourn.CheckNumber;
                if (aJourn.UseAlternateCash)
                {
                    chkUseAltCashAccount.CheckState = CheckState.Checked;
                    vsAltCashAccount.Enabled = true;
                    vsAltCashAccount.TabStop = true;
                    vsAltCashAccount.TextMatrix(0, 0, aJourn.AlternateCashAccount);
                }
                else
                {
                    chkUseAltCashAccount.CheckState = CheckState.Unchecked;
                    vsAltCashAccount.TextMatrix(0, 0, "");
                    vsAltCashAccount.Enabled = false;
                    vsAltCashAccount.TabStop = false;
                }
                if (aJourn.Separate)
                {
                    chkSeperate.CheckState = CheckState.Checked;
                }
                else
                {
                    chkSeperate.CheckState = CheckState.Unchecked;
                }
               // VendorNumber = aJourn.VendorNumber;
                FirstRecordShown = aJourn.ID;
                ChosenEncumbrance = aJourn.EncumbranceRecord;
                ChosenPurchaseOrder = aJourn.PurchaseOrderID;
                if (aJourn.PurchaseOrderID > 0)
                {
                    PurchaseOrderFlag = true;
                }
                modBudgetaryMaster.Statics.blnAPEdit = true;
                modBudgetaryMaster.Statics.CurrentAPEntry = aJourn.JournalNumber;
                modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
                OldJournal = true;
                OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
                apJournalId = aJourn.ID;
                modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("select * from apjournal where id = " + aJourn.ID, "Budgetary");
                SetReadOnly(aJourn);
                FillDetails(aJourn.JournalEntries, aJourn.EncumbranceRecord);
            }
            else
            {
                modBudgetaryMaster.Statics.CurrentAPEntry = aJourn.JournalNumber;
                modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
                OldJournal = true;
                OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
                EncumbranceFlag = false;
                modBudgetaryMaster.Statics.blnAPEdit = false;
                PurchaseOrderFlag = false;
                FromVendorMaster = false;
                btnProcessNextEntry.Enabled = false;
                btnProcessPreviousEntry.Enabled = false;
                mnuProcessDeleteEntry.Enabled = false;
                apJournalId = 0;
                blnJournalEdit = false;
                if (aJourn.Period > 0)
                {
                    txtPeriod.Text = aJourn.Period.ToString();
                    intSavedPeriod = aJourn.Period;
                }
                DateTime parsedDate;
                if (DateTime.TryParse(aJourn.PayableDate, out parsedDate))
                {
                    txtPayable.Text = aJourn.PayableDate;
                    datPayableDate = parsedDate;
                }
                else
                {
                    txtPayable.Text = "";
                    txtPayable.Enabled = true;
                }
            }
            Show(App.MainForm);
        }

        private void FillDetails(cGenericCollection collDetails, int lngEncumbranceID)
        {
            try
            {
                // On Error GoTo ErrorHandler
                int counter = 0;
                if (!(collDetails == null))
                {
                    collDetails.MoveFirst();
                    if (collDetails.ItemCount() > 15)
                    {
                        vs1.Rows = collDetails.ItemCount() + 1;
                    }
                    int counter2;
                    int ACounter = 0;
                    ACounter = 0;
                    counter = 1;
                    NumberOfEncumbrances = 0;
                    cAPJournalDetail aDetail;
                    cEncumbrance enc;
                    cEncumbranceDetail encDetail;
                    cGenericCollection collEncDetails = new cGenericCollection();
                    enc = encCont.GetEncumbrance(lngEncumbranceID);
                    if (!(enc == null))
                    {
                        if (enc.ID > 0)
                        {
                            EncumbranceFlag = true;
                            collEncDetails = encCont.GetEncumbranceDetails(enc.ID);
                            modBudgetaryMaster.Statics.EncumbranceDetail = new double[collEncDetails.ItemCount() + 1];
                            modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[collEncDetails.ItemCount() + 1];
                        }
                    }
                    while (collDetails.IsCurrent())
                    {
                        aDetail = (cAPJournalDetail)collDetails.GetCurrentItem();
                        vs1.TextMatrix(counter, 0, FCConvert.ToString(aDetail.ID));
                        vs1.TextMatrix(counter, 1, aDetail.Description);
                        vs1.TextMatrix(counter, 2, aDetail.Account);
                        vs1.TextMatrix(counter, 3, aDetail.Ten99);
                        vs1.TextMatrix(counter, 4, aDetail.Project);
                        vs1.TextMatrix(counter, 5, FCConvert.ToString(aDetail.Amount));
                        vs1.TextMatrix(counter, 6, FCConvert.ToString(aDetail.Discount));
                        vs1.TextMatrix(counter, 7, FCConvert.ToString(aDetail.Encumbrance));
                        if (aDetail.Encumbrance > 0)
                        {
                            collEncDetails.MoveFirst();
                            while (collEncDetails.IsCurrent())
                            {
                                encDetail = (cEncumbranceDetail)collEncDetails.GetCurrentItem();
                                if (encDetail.Account == aDetail.Account)
                                {
                                    modBudgetaryMaster.Statics.EncumbranceDetail[ACounter] = encDetail.Amount + encDetail.Adjustments - encDetail.LiquidatedAmount;
                                    modBudgetaryMaster.Statics.EncumbranceDetailRecords[ACounter] = encDetail.ID;
                                    NumberOfEncumbrances += 1;
                                    ACounter += 1;
                                    break;
                                }
                                collEncDetails.MoveNext();
                            }
                        }
                        counter += 1;
                        collDetails.MoveNext();
                    }
                    if (collDetails.ItemCount() + 1 <= vs1.Rows - 1)
                    {
                        for (counter = collDetails.ItemCount() + 1; counter <= vs1.Rows - 1; counter++)
                        {
                            vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                            vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                        }
                    }
                }
                else
                {
                    for (counter = 1; counter <= vs1.Rows - 1; counter++)
                    {
                        vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
                        vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void txtVendor_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
