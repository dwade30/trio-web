﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCheckRegister.
	/// </summary>
	partial class frmCheckRegister : BaseForm
	{
		public fecherFoundation.FCComboBox cmbInitial;
		public fecherFoundation.FCLabel lblInitial;
		public fecherFoundation.FCFrame fraBanks;
		public fecherFoundation.FCComboBox cboBanks;
		public FCCommonDialog dlg1_Save;
		public FCCommonDialog dlg1;
		public fecherFoundation.FCFrame fraSelectReport;
		public fecherFoundation.FCComboBox cboReport;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckRegister));
			this.cmbInitial = new fecherFoundation.FCComboBox();
			this.lblInitial = new fecherFoundation.FCLabel();
			this.fraBanks = new fecherFoundation.FCFrame();
			this.cboBanks = new fecherFoundation.FCComboBox();
			this.dlg1_Save = new fecherFoundation.FCCommonDialog();
			this.dlg1 = new fecherFoundation.FCCommonDialog();
			this.fraSelectReport = new fecherFoundation.FCFrame();
			this.cboReport = new fecherFoundation.FCComboBox();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).BeginInit();
			this.fraBanks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).BeginInit();
			this.fraSelectReport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 266);
			this.BottomPanel.Size = new System.Drawing.Size(598, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbInitial);
			this.ClientArea.Controls.Add(this.lblInitial);
			this.ClientArea.Controls.Add(this.fraSelectReport);
			this.ClientArea.Controls.Add(this.fraBanks);
			this.ClientArea.Size = new System.Drawing.Size(598, 206);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(598, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(180, 30);
			this.HeaderText.Text = "Check Register";
			// 
			// cmbInitial
			// 
			this.cmbInitial.AutoSize = false;
			this.cmbInitial.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbInitial.FormattingEnabled = true;
			this.cmbInitial.Items.AddRange(new object[] {
				"Initial Run",
				"Reprint Register"
			});
			this.cmbInitial.Location = new System.Drawing.Point(180, 30);
			this.cmbInitial.Name = "cmbInitial";
			this.cmbInitial.Size = new System.Drawing.Size(370, 40);
			this.cmbInitial.TabIndex = 13;
			this.cmbInitial.Text = "Initial Run";
			// 
			// lblInitial
			// 
			this.lblInitial.AutoSize = true;
			this.lblInitial.Location = new System.Drawing.Point(30, 44);
			this.lblInitial.Name = "lblInitial";
			this.lblInitial.Size = new System.Drawing.Size(95, 15);
			this.lblInitial.TabIndex = 14;
			this.lblInitial.Text = "REPORT TYPE";
			// 
			// fraBanks
			// 
			this.fraBanks.Controls.Add(this.cboBanks);
			this.fraBanks.Location = new System.Drawing.Point(30, 30);
			this.fraBanks.Name = "fraBanks";
			this.fraBanks.Size = new System.Drawing.Size(540, 90);
			this.fraBanks.TabIndex = 0;
			this.fraBanks.Text = "Select A Bank";
			this.fraBanks.Visible = false;
			// 
			// cboBanks
			// 
			this.cboBanks.AutoSize = false;
			this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
			this.cboBanks.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBanks.FormattingEnabled = true;
			this.cboBanks.Items.AddRange(new object[] {
				"1 - Most Recently Run Report",
				"2 - Next Oldest Report",
				"3 - 3rd Oldest Report",
				"4 - 4th Oldest Report",
				"5 - 5th Oldest Report",
				"6 - 6th Oldest Report",
				"7 - 7th Oldest Report",
				"8 - 8th Oldest Report",
				"9 - 9th Oldest Report"
			});
			this.cboBanks.Location = new System.Drawing.Point(20, 30);
			this.cboBanks.Name = "cboBanks";
			this.cboBanks.Size = new System.Drawing.Size(500, 40);
			this.cboBanks.TabIndex = 1;
			// 
			// dlg1_Save
			// 
			this.dlg1_Save.Color = System.Drawing.Color.Black;
			this.dlg1_Save.DefaultExt = null;
			this.dlg1_Save.FilterIndex = ((short)(0));
			this.dlg1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1_Save.FontName = "Microsoft Sans Serif";
			this.dlg1_Save.FontSize = 8.25F;
			this.dlg1_Save.ForeColor = System.Drawing.Color.Black;
			this.dlg1_Save.FromPage = 0;
			this.dlg1_Save.Location = new System.Drawing.Point(0, 0);
			this.dlg1_Save.Name = "dlg1_Save";
			this.dlg1_Save.PrinterSettings = null;
			this.dlg1_Save.Size = new System.Drawing.Size(0, 0);
			this.dlg1_Save.TabIndex = 0;
			this.dlg1_Save.ToPage = 0;
			// 
			// dlg1
			// 
			this.dlg1.Color = System.Drawing.Color.Black;
			this.dlg1.DefaultExt = null;
			this.dlg1.FilterIndex = ((short)(0));
			this.dlg1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.dlg1.FontName = "Microsoft Sans Serif";
			this.dlg1.FontSize = 8.25F;
			this.dlg1.ForeColor = System.Drawing.Color.Black;
			this.dlg1.FromPage = 0;
			this.dlg1.Location = new System.Drawing.Point(0, 0);
			this.dlg1.Name = "dlg1";
			this.dlg1.PrinterSettings = null;
			this.dlg1.Size = new System.Drawing.Size(0, 0);
			this.dlg1.TabIndex = 0;
			this.dlg1.ToPage = 0;
			// 
			// fraSelectReport
			// 
			this.fraSelectReport.Controls.Add(this.cboReport);
			this.fraSelectReport.Location = new System.Drawing.Point(30, 30);
			this.fraSelectReport.Name = "fraSelectReport";
			this.fraSelectReport.Size = new System.Drawing.Size(540, 90);
			this.fraSelectReport.TabIndex = 4;
			this.fraSelectReport.Text = "Select Report";
			this.fraSelectReport.Visible = false;
			// 
			// cboReport
			// 
			this.cboReport.AutoSize = false;
			this.cboReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReport.FormattingEnabled = true;
			this.cboReport.Items.AddRange(new object[] {
				"1 - Most Recently Run Report",
				"2 - Next Oldest Report",
				"3 - 3rd Oldest Report",
				"4 - 4th Oldest Report",
				"5 - 5th Oldest Report",
				"6 - 6th Oldest Report",
				"7 - 7th Oldest Report",
				"8 - 8th Oldest Report",
				"9 - 9th Oldest Report"
			});
			this.cboReport.Location = new System.Drawing.Point(20, 30);
			this.cboReport.Name = "cboReport";
			this.cboReport.Size = new System.Drawing.Size(500, 40);
			this.cboReport.TabIndex = 7;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(30, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(104, 48);
			this.btnProcess.TabIndex = 9;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmCheckRegister
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(598, 374);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCheckRegister";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Check Register";
			this.Load += new System.EventHandler(this.frmCheckRegister_Load);
			this.Activated += new System.EventHandler(this.frmCheckRegister_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckRegister_KeyPress);
			this.Resize += new System.EventHandler(this.frmCheckRegister_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBanks)).EndInit();
			this.fraBanks.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelectReport)).EndInit();
			this.fraSelectReport.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton btnProcess;
	}
}
