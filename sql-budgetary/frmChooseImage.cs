﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChooseImage.
	/// </summary>
	public partial class frmChooseImage : BaseForm
	{
		public frmChooseImage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbShow.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseImage InstancePtr
		{
			get
			{
				return (frmChooseImage)Sys.GetInstance(typeof(frmChooseImage));
			}
		}

		protected frmChooseImage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strPath = "";
		string strFileName = "";

		private void chkUseTownSeal_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkUseTownSeal.CheckState == CheckState.Checked)
			{
				//Label2.Visible = false;
				//Label3.Visible = false;
				//Label4.Visible = false;
				//Drive1.Visible = false;
				//Dir1.Visible = false;
				//File1.Visible = false;
				strFileName = "TownSeal.pic";
				strPath = FCFileSystem.Statics.UserDataFolder;
				if (strPath.Length > 0)
				{
					if (Strings.Right(strPath, 1) != "\\" && Strings.Right(strPath, 1) != "/")
						strPath += "\\";
				}
				Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
			}
			else
			{
				//Label2.Visible = true;
				//Label3.Visible = true;
				//Label4.Visible = true;
				//Drive1.Visible = true;
				//Dir1.Visible = true;
				//File1.Visible = true;
				if (upload1.Value != string.Empty)
				{
					strFileName = upload1.Value;
					strPath = FCFileSystem.Statics.UserDataFolder;
					if (strPath.Length > 0)
					{
						if (Strings.Right(strPath, 1) != "\\" && Strings.Right(strPath, 1) != "/")
							strPath += "\\";
					}
					Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
				}
			}
		}
		//private void Dir1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    File1.Path = Dir1.Path;
		//}
		//private void Drive1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    DriveInfo drv = new DriveInfo(Strings.Left(Drive1.Drive, 2));
		//    if (drv.IsReady)
		//    {
		//        Dir1.Path = Drive1.Drive;
		//    }
		//}
		//private void File1_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//    if (File1.FileName != string.Empty)
		//    {
		//        strFileName = File1.FileName;
		//        strPath = File1.Path;
		//        if (strPath.Length > 0)
		//        {
		//            if (Strings.Right(strPath, 1) != "\\" && Strings.Right(strPath, 1) != "/") strPath += "\\";
		//        }
		//        Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
		//    }
		//}
		private void frmChooseImage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				mnuExit_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmChooseImage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseImage.ScaleWidth	= 9045;
			//frmChooseImage.ScaleHeight	= 7440;
			//frmChooseImage.LinkTopic	= "Form1";
			//End Unmaped Properties
			clsDRWrapper clsTemp = new clsDRWrapper();
			clsTemp.OpenRecordset("SELECT * FROM Budgetary");
			strPath = FCFileSystem.Statics.UserDataFolder;
			if (Strings.Right(strPath, 1) != "\\")
			{
				strPath += "\\";
			}
			strFileName = "TownSeal.pic";
			if (File.Exists(strPath + strFileName))
			{
				chkUseTownSeal.Enabled = true;
				if (FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("UseTownSeal")))
				{
					chkUseTownSeal.CheckState = CheckState.Checked;
				}
				else
				{
					chkUseTownSeal.CheckState = CheckState.Unchecked;
				}
			}
			else
			{
				chkUseTownSeal.Enabled = false;
				chkUseTownSeal.CheckState = CheckState.Unchecked;
			}
			if (!clsTemp.EndOfFile())
			{
				if (!FCConvert.ToBoolean(clsTemp.Get_Fields_Boolean("ShowIcon")))
				{
					cmbShow.SelectedIndex = 1;
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("CheckIcon"))) != string.Empty)
					{
						strPath = FCFileSystem.Statics.UserDataFolder;
						if (Strings.Right(strPath, 1) != "\\")
						{
							strPath += "\\CheckIcons\\";
						}
						else
						{
							strPath += "CheckIcons\\";
						}
						strFileName = Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("CheckIcon")));
						if (File.Exists(strPath + strFileName))
						{
							Image1.Image = FCUtils.LoadPicture(strPath + strFileName);
						}
						else
						{
							strFileName = "";
						}
					}
					else
					{
						strPath = "";
						strFileName = "";
					}
				}
			}
			else
			{
				strPath = "";
				strFileName = "";
			}
			modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			//Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label3.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//Label4.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//FC:FINAL:BB - removed when redesign
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strDestinationPath = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsTemp.OpenRecordset("SELECT * FROM Budgetary");
				if (clsTemp.EndOfFile())
				{
					clsTemp.AddNew();
				}
				else
				{
					clsTemp.Edit();
				}
				if (cmbShow.SelectedIndex == 0)
				{
					clsTemp.Set_Fields("ShowIcon", true);
					if (chkUseTownSeal.CheckState == CheckState.Unchecked)
					{
						if (strFileName != string.Empty)
						{
							strDestinationPath = FCFileSystem.Statics.UserDataFolder;
							if (Strings.Right(strDestinationPath, 1) != "\\")
							{
								strDestinationPath += "\\CheckIcons";
							}
							else
							{
								strDestinationPath += "CheckIcons";
							}
							if (!Directory.Exists(strDestinationPath))
							{
								Directory.CreateDirectory(strDestinationPath);
							}
							File.Copy(strPath + strFileName, strDestinationPath + "\\" + strFileName, true);
							clsTemp.Set_Fields("CheckIcon", strFileName);
						}
						clsTemp.Set_Fields("UseTownSeal", false);
					}
					else
					{
						clsTemp.Set_Fields("CheckIcon", "TownSeal.pic");
						clsTemp.Set_Fields("UseTownSeal", true);
					}
				}
				else
				{
					clsTemp.Set_Fields("ShowIcon", false);
					clsTemp.Set_Fields("UseTownSeal", false);
				}
				clsTemp.Update();
				//FC:FINAL:DDU:#3036 - added save message box
				FCMessageBox.Show("Save Successful", MsgBoxStyle.OkOnly, "Saved");
				//Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSaveExit_Click(sender, EventArgs.Empty);
		}

		private void upload1_Uploaded(object sender, UploadedEventArgs e)
		{
			if (e.Files != null && e.Files.Count > 0)
			{
				System.Web.HttpPostedFile file = e.Files[0];
				strPath = FCFileSystem.Statics.UserDataFolder + "\\";
				if (!Directory.Exists(strPath))
				{
					Directory.CreateDirectory(strPath);
				}
				strFileName = file.FileName;
				using (FileStream fileStream = new FileStream(Path.Combine(strPath, strFileName), FileMode.Create, FileAccess.Write))
				{
					file.InputStream.CopyTo(fileStream);
				}
				Image1.Image = FCUtils.LoadPicture(Path.Combine(strPath, strFileName));
			}
		}
	}
}
