﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;
using Wisej.Web;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1099EditReport.
	/// </summary>
	public partial class rpt1099EditReport : BaseSectionReport
	{
		int PageCounter;
		bool blnFirstRecord;
		bool blnCurrentDone;
		Decimal VendorTotal;
		bool blnShowVendorInfo;
		bool blnShowVendorTotal;

		Decimal[] curGroupTotals = new Decimal[9 + 1];
		Decimal[] curCompleteTotals = new Decimal[9 + 1];
		clsDRWrapper rsDefaultInfo = new clsDRWrapper();
		bool blnSummary;
        private EditReportSetupInfo setupInfo;
        private ITaxFormService taxFormService;
        private List<VendorInfo1099Process> vendors = new List<VendorInfo1099Process>();
        private int vendorCounter = 0;
        private List<Adjustment> vendorAdjustments = new List<Adjustment>();
        private int adjustmentCounter = 0;
        private List<EditReportDetailInfo> vendorDetails = new List<EditReportDetailInfo>();
        private int detailCounter = 0;
        private TaxFormType currentFormType = TaxFormType.All;

		public rpt1099EditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public rpt1099EditReport(EditReportSetupInfo setupInfo) : this()
        {
            this.setupInfo = setupInfo;
        }

        private bool IncrementFormType()
        {
            if (setupInfo.FormType == TaxFormType.All)
            {
                if ((TaxFormType)currentFormType == TaxFormType.All)
                {
                    currentFormType = TaxFormType.MISC1099;
                    return true;
                }
				else if ((TaxFormType)currentFormType == TaxFormType.MISC1099)
                {
                    currentFormType = TaxFormType.NEC1099;
                    return true;
                }
                else
                {
                    return false;
                }
			}
            else
            {
				if ((TaxFormType)currentFormType == TaxFormType.All)
                {
                    currentFormType = setupInfo.FormType;
                    return true;
                }
                else
                {
                    return false;
                }
			}
        }
		private void InitializeComponentEx()
		{
			this.Name = "1099 Edit Report";
            this.ReportEnd += Rpt1099EditReport_ReportEnd;
		}

        private void Rpt1099EditReport_ReportEnd(object sender, EventArgs e)
        {
            rsDefaultInfo.DisposeOf();
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckCurrent = false;
			if (blnFirstRecord)
            {
                blnFirstRecord = false;
                GetVendorDetailInfo();
				executeCheckCurrent = true;
            }
			else
			{
				if (!blnCurrentDone)
                {
                    detailCounter++;
					executeCheckCurrent = true;
				}
				else
                {
                    adjustmentCounter++;
					if (adjustmentCounter >= vendorAdjustments.Count)
                    {
                        vendorCounter++;
						if (vendorCounter < vendors.Count)
						{
                            GetVendorDetailInfo();
							executeCheckCurrent = true;
		                }
						else
						{
                            if (GetVendorsForFormType())
                            {
                                GetVendorDetailInfo();
                                executeCheckCurrent = true;
                            }
                            else
                            {
                                eArgs.EOF = true;
                            }
						}
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			CheckCurrent:
			;
			if (executeCheckCurrent)
			{
				if (detailCounter >= vendorDetails.Count)
				{
					blnCurrentDone = true;
					if (adjustmentCounter >= vendorAdjustments.Count)
					{
                        vendorCounter++;
                        if (vendorCounter < vendors.Count)
						{
                            GetVendorDetailInfo();
							executeCheckCurrent = true;
							goto CheckCurrent;
						}
						else
						{
                            if (GetVendorsForFormType())
                            {
                                GetVendorDetailInfo();
								executeCheckCurrent = true;
                                goto CheckCurrent;
                            }
                            else
                            {
								eArgs.EOF = true;
							}
                        }
					}
					else
					{
						eArgs.EOF = false;
					}
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			if (eArgs.EOF == false)
			{
				this.Fields["VendorBinder"].Value = vendors[vendorCounter].VendorNumber;
				this.Fields["SpaceBinder"].Value = vendors[vendorCounter].VendorNumber;
                this.Fields["FormTypeBinder"].Value = currentFormType;
			}
		}

        private void GetVendorDetailInfo()
        {
            vendorAdjustments = taxFormService.GetVendorAdjustments(currentFormType, vendors[vendorCounter].VendorNumber);
            vendorDetails = taxFormService.GetVendorEditReportDetails(currentFormType, vendors[vendorCounter].VendorNumber,
                setupInfo.Year, vendors[vendorCounter].IncludeAllPayments, vendors[vendorCounter].TaxCode);
            adjustmentCounter = 0;
            detailCounter = 0;
            blnCurrentDone = false;
            blnShowVendorInfo = true;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblTitle1.Text = "FOR THE YEAR -" + setupInfo.Year + "-";
			lblTitle2.Text = "MINIMUM LISTED = " + setupInfo.MinimumAmount.ToString("#,##0.00") + " DOLLARS";
			blnSummary = false;

            taxFormService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxFormService()).Result;

            if (GetVendorsForFormType())
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
				return;
			}
			blnFirstRecord = true;
			ClearGroupTotals();
			for (counter = 0; counter <= 9; counter++)
			{
				curCompleteTotals[counter] = 0;
			}
		}

        private bool GetVendorsForFormType()
        {
            do
            {
                var incrementResult = IncrementFormType();

                if (incrementResult)
                {
                    vendors = taxFormService.GetEligibleVendors(currentFormType, setupInfo.MinimumAmount);

                    if (setupInfo.SortOption == VendorSortOption.VendorNumber)
                    {
                        vendors = vendors.OrderBy(x => x.VendorNumber).ToList();
                    }
                    else
                    {
                        vendors = vendors.OrderBy(x => x.VendorName).ToList();
                    }
                }
                else
                {
                    return false;
                }
			} while (vendors.Count == 0);

            vendorCounter = 0;
			return true;
        }

        private void Detail_Format(object sender, EventArgs e)
		{
            if (vendorCounter < vendors.Count)
            {
				if (blnShowVendorInfo)
				{
					blnShowVendorInfo = false;
					fldVendor.Visible = true;
					fldTaxID.Visible = true;
					fldVendor.Text = vendors[vendorCounter].IsAttorney ? "* " : "";
					fldVendor.Text += vendors[vendorCounter].VendorNumber.ToString("00000") + "  " + vendors[vendorCounter].VendorName.Trim();
					if (vendors[vendorCounter].Address.Left(1) == "*")
					{
						fldDBA.Visible = true;
						fldDBA.Text = "      DBA " + vendors[vendorCounter].Address.Right(vendors[vendorCounter].Address.Length - 1);
					}
					else
					{
						fldDBA.Text = "";
						fldDBA.Visible = false;
					}
					// End If
					fldTaxID.Text = vendors[vendorCounter].TaxIdNumber;
				}
				else
				{
					blnShowVendorTotal = true;
					fldVendor.Visible = false;
					fldDBA.Visible = false;
					fldTaxID.Visible = false;
				}

				string tempCode = "";

				if (!blnCurrentDone)
				{
					tempCode = vendorDetails[detailCounter].TaxCode;

					fldJournal.Text = vendorDetails[detailCounter].JournalNumber.ToString("0000");
					fldDate.Text = vendorDetails[detailCounter].CheckDate.ToString("MM/dd/yy");
					fldDescription.Text = vendorDetails[detailCounter].Description.Trim();
					fldCode.Text = vendorDetails[detailCounter].TaxCode;
					fldAmount.Text = vendorDetails[detailCounter].Amount.ToString("#,##0.00");
				}
				else
				{
					tempCode = (vendorAdjustments[adjustmentCounter].Code ?? 0).ToString();

					fldJournal.Text = "";
					fldDate.Text = "";
					fldDescription.Text = "MANUAL ADJUSTMENT " + vendorAdjustments[adjustmentCounter].AdjustmentNumber;
					fldCode.Text = tempCode;
					fldAmount.Text = (vendorAdjustments[adjustmentCounter].Positive ?? false)
						? (vendorAdjustments[adjustmentCounter].Amount ?? 0).ToString("#,##0.00")
						: ((vendorAdjustments[adjustmentCounter].Amount ?? 0) * -1).ToString("#,##0.00");
				}

				if (tempCode == "1")
				{
					curGroupTotals[1] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "2")
				{
					curGroupTotals[2] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "3")
				{
					curGroupTotals[3] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "4")
				{
					curGroupTotals[4] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "5")
				{
					curGroupTotals[5] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "6")
				{
					curGroupTotals[6] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "7")
				{
					curGroupTotals[7] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "8")
				{
					curGroupTotals[8] += FCConvert.ToDecimal(fldAmount.Text);
				}
				else if (tempCode == "9")
				{
					curGroupTotals[9] += FCConvert.ToDecimal(fldAmount.Text);
				}
			}
        }

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			int counter;
			int ClassCount;
			VendorTotal = 0;
			ClassCount = 0;
			for (counter = 1; counter <= 9; counter++)
			{
				curCompleteTotals[counter] += curGroupTotals[counter];
				VendorTotal += curGroupTotals[counter];
				if (curGroupTotals[counter] != 0)
				{
					ClassCount += 1;
				}
			}
			if (ClassCount > 1)
			{
				if (curGroupTotals[1] != 0)
				{
					fldCode1Title.Visible = true;
					fldCode1GroupTotal.Visible = true;
					fldCode1Title.Text = GetCodeTitle(1);
					fldCode1GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[1]);
				}
				else
				{
					fldCode1Title.Visible = false;
					fldCode1GroupTotal.Visible = false;
					fldCode1Title.Text = "";
					fldCode1GroupTotal.Text = "";
				}
				if (curGroupTotals[2] != 0)
				{
					fldCode2Title.Visible = true;
					fldCode2GroupTotal.Visible = true;
					fldCode2Title.Text = GetCodeTitle(2);
					fldCode2GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[2]);
				}
				else
				{
					fldCode2Title.Visible = false;
					fldCode2GroupTotal.Visible = false;
					fldCode2Title.Text = "";
					fldCode2GroupTotal.Text = "";
				}
				if (curGroupTotals[3] != 0)
				{
					fldCode3Title.Visible = true;
					fldCode3GroupTotal.Visible = true;
					fldCode3Title.Text = GetCodeTitle(3);
					fldCode3GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[3]);
				}
				else
				{
					fldCode3Title.Visible = false;
					fldCode3GroupTotal.Visible = false;
					fldCode3Title.Text = "";
					fldCode3GroupTotal.Text = "";
				}
				if (curGroupTotals[4] != 0)
				{
					fldCode4Title.Visible = true;
					fldCode4GroupTotal.Visible = true;
					fldCode4Title.Text = GetCodeTitle(4);
					fldCode4GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[4]);
				}
				else
				{
					fldCode4Title.Visible = false;
					fldCode4GroupTotal.Visible = false;
					fldCode4Title.Text = "";
					fldCode4GroupTotal.Text = "";
				}
				if (curGroupTotals[5] != 0)
				{
					fldCode5Title.Visible = true;
					fldCode5GroupTotal.Visible = true;
					fldCode5Title.Text = GetCodeTitle(5);
					fldCode5GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[5]);
				}
				else
				{
					fldCode5Title.Visible = false;
					fldCode5GroupTotal.Visible = false;
					fldCode5Title.Text = "";
					fldCode5GroupTotal.Text = "";
				}
				if (curGroupTotals[6] != 0)
				{
					fldCode6Title.Visible = true;
					fldCode6GroupTotal.Visible = true;
					fldCode6Title.Text = GetCodeTitle(6);
                    fldCode6GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[6]);
                }
				else
				{
					fldCode6Title.Visible = false;
					fldCode6GroupTotal.Visible = false;
					fldCode6Title.Text = "";
					fldCode6GroupTotal.Text = "";
				}
				if (curGroupTotals[7] != 0)
				{
					fldCode7Title.Visible = true;
					fldCode7GroupTotal.Visible = true;
					fldCode7Title.Text = GetCodeTitle(7);
					fldCode7GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[7]);
				}
				else
				{
					fldCode7Title.Visible = false;
					fldCode7GroupTotal.Visible = false;
					fldCode7Title.Text = "";
					fldCode7GroupTotal.Text = "";
				}
				if (curGroupTotals[8] != 0)
				{
					fldCode8Title.Visible = true;
					fldCode8GroupTotal.Visible = true;
					fldCode8Title.Text = GetCodeTitle(8);
					fldCode8GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[8]);
				}
				else
				{
					fldCode8Title.Visible = false;
					fldCode8GroupTotal.Visible = false;
					fldCode8Title.Text = "";
					fldCode8GroupTotal.Text = "";
				}
				if (curGroupTotals[9] != 0)
				{
					fldCode9Title.Visible = true;
					fldCode9GroupTotal.Visible = true;
					fldCode9Title.Text = GetCodeTitle(9);
					fldCode9GroupTotal.Text = modUtilities.FormatTotal(curGroupTotals[9]);
				}
				else
				{
					fldCode9Title.Visible = false;
					fldCode9GroupTotal.Visible = false;
					fldCode9Title.Text = "";
					fldCode9GroupTotal.Text = "";
				}
				FixGroupTotalFields();
			}
			else
			{
				fldCode1Title.Visible = false;
				fldCode1GroupTotal.Visible = false;
				fldCode2Title.Visible = false;
				fldCode2GroupTotal.Visible = false;
				fldCode3Title.Visible = false;
				fldCode3GroupTotal.Visible = false;
				fldCode4Title.Visible = false;
				fldCode4GroupTotal.Visible = false;
				fldCode5Title.Visible = false;
				fldCode5GroupTotal.Visible = false;
				fldCode6Title.Visible = false;
				fldCode6GroupTotal.Visible = false;
				fldCode7Title.Visible = false;
				fldCode7GroupTotal.Visible = false;
				fldCode8Title.Visible = false;
				fldCode8GroupTotal.Visible = false;
				fldCode9Title.Visible = false;
				fldCode9GroupTotal.Visible = false;
			}
			if (blnShowVendorTotal)
			{
				blnShowVendorTotal = false;
				fldVendorTotal.Visible = true;
				fldVendorTitle.Visible = true;
				fldVendorTotal.Text = modUtilities.FormatTotal(VendorTotal);
			}
			else
			{
				fldVendorTotal.Visible = false;
				fldVendorTitle.Visible = false;
			}
			ClearGroupTotals();
		}

		private void GroupFooter4_Format(object sender, EventArgs e)
		{
			rsDefaultInfo.OpenRecordset("SELECT * FROM [1099Information]");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				fldFederalID.Text = rsDefaultInfo.Get_Fields_String("FederalCode");
				fldStateID.Text = rsDefaultInfo.Get_Fields_String("StateIDCode");
				fldMunicipality.Text = rsDefaultInfo.Get_Fields_String("Municipality");
				fldAddress1Info.Text = rsDefaultInfo.Get_Fields_String("Address1");
				fldAddress2Info.Text = rsDefaultInfo.Get_Fields_String("Address2");
				fldCity.Text = rsDefaultInfo.Get_Fields_String("City");
				fldState.Text = rsDefaultInfo.Get_Fields_String("State");
				if (Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Zip4"))) != "")
				{
					fldZip.Text = rsDefaultInfo.Get_Fields_String("Zip") + "-" + rsDefaultInfo.Get_Fields_String("Zip4");
				}
				else
				{
					fldZip.Text = rsDefaultInfo.Get_Fields_String("Zip");
				}
				fldContact.Text = rsDefaultInfo.Get_Fields_String("Contact");
				if (Strings.Trim(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("Extension"))) != "")
				{
					fldPhone.Text = Strings.Format(rsDefaultInfo.Get_Fields_String("Phone"), "(000)###-####") + "  Ext: " + rsDefaultInfo.Get_Fields_String("Extension");
				}
				else
				{
					fldPhone.Text = Strings.Format(rsDefaultInfo.Get_Fields_String("Phone"), "(000)###-####");
				}
				if (FCConvert.ToBoolean(rsDefaultInfo.Get_Fields_Boolean("ElectronicFiling")))
				{
					fldFilingElectronically.Text = "Yes";
				}
				else
				{
					fldFilingElectronically.Text = "No";
				}
			}
		}

		private void GroupFooter5_Format(object sender, EventArgs e)
		{
			int counter;
			VendorTotal = 0;
			if (curCompleteTotals[1] != 0)
			{
				lblCode1.Visible = true;
				fldCode1.Visible = true;
				lblCode1.Text = GetCodeTitle(1);
				fldCode1.Text = modUtilities.FormatTotal(curCompleteTotals[1]);
			}
			else
			{
				lblCode1.Visible = false;
				fldCode1.Visible = false;
				lblCode1.Text = "";
				fldCode1.Text = "";
			}
			if (curCompleteTotals[2] != 0)
			{
				lblCode2.Visible = true;
				fldCode2.Visible = true;
				lblCode2.Text = GetCodeTitle(2);
				fldCode2.Text = modUtilities.FormatTotal(curCompleteTotals[2]);
			}
			else
			{
				lblCode2.Visible = false;
				fldCode2.Visible = false;
				lblCode2.Text = "";
				fldCode2.Text = "";
			}
			if (curCompleteTotals[3] != 0)
			{
				lblCode3.Visible = true;
				fldCode3.Visible = true;
				lblCode3.Text = GetCodeTitle(3);
				fldCode3.Text = modUtilities.FormatTotal(curCompleteTotals[3]);
			}
			else
			{
				lblCode3.Visible = false;
				fldCode3.Visible = false;
				lblCode3.Text = "";
				fldCode3.Text = "";
			}
			if (curCompleteTotals[4] != 0)
			{
				lblCode4.Visible = true;
				fldCode4.Visible = true;
				lblCode4.Text = GetCodeTitle(4);
				fldCode4.Text = modUtilities.FormatTotal(curCompleteTotals[4]);
			}
			else
			{
				lblCode4.Visible = false;
				fldCode4.Visible = false;
				lblCode4.Text = "";
				fldCode4.Text = "";
			}
			if (curCompleteTotals[5] != 0)
			{
				lblCode5.Visible = true;
				fldCode5.Visible = true;
				lblCode5.Text = GetCodeTitle(5);
				fldCode5.Text = modUtilities.FormatTotal(curCompleteTotals[5]);
			}
			else
			{
				lblCode5.Visible = false;
				fldCode5.Visible = false;
				lblCode5.Text = "";
				fldCode5.Text = "";
			}
			if (curCompleteTotals[6] != 0)
			{
				lblCode6.Visible = true;
				fldCode6.Visible = true;
				lblCode6.Text = GetCodeTitle(6);
				fldCode6.Text = modUtilities.FormatTotal(curCompleteTotals[6]);
			}
			else
			{
				lblCode6.Visible = false;
				fldCode6.Visible = false;
				lblCode6.Text = "";
				fldCode6.Text = "";
			}
			if (curCompleteTotals[7] != 0)
			{
				lblCode7.Visible = true;
				fldCode7.Visible = true;
				lblCode7.Text = GetCodeTitle(7);
				fldCode7.Text = modUtilities.FormatTotal(curCompleteTotals[7]);
			}
			else
			{
				lblCode7.Visible = false;
				fldCode7.Visible = false;
				lblCode7.Text = "";
				fldCode7.Text = "";
			}
			if (curCompleteTotals[8] != 0)
			{
				lblCode8.Visible = true;
				fldCode8.Visible = true;
				lblCode8.Text = GetCodeTitle(8);
				fldCode8.Text = modUtilities.FormatTotal(curCompleteTotals[8]);
			}
			else
			{
				lblCode8.Visible = false;
				fldCode8.Visible = false;
				lblCode8.Text = "";
				fldCode8.Text = "";
			}
			if (curCompleteTotals[9] != 0)
			{
				lblCode9.Visible = true;
				fldCode9.Visible = true;
				lblCode9.Text = GetCodeTitle(9);
				fldCode9.Text = modUtilities.FormatTotal(curCompleteTotals[9]);
			}
			else
			{
				lblCode9.Visible = false;
				fldCode9.Visible = false;
				lblCode9.Text = "";
				fldCode9.Text = "";
			}
			FixFinalTotalFields();
			for (counter = 1; counter <= 9; counter++)
			{
				VendorTotal += curCompleteTotals[counter];
			}
			fldTotal.Text = modUtilities.FormatTotal(VendorTotal);

            ClearFormTypeTotals();
        }

		private void GroupFooter6_Format(object sender, EventArgs e)
		{
			blnSummary = true;
			
			var reportData = new List<VendorErrorReportData>();

            foreach (var vendor in vendors.OrderBy(x => x.VendorNumber))
            {
                if (vendor.TaxIdNumber.Trim() == "" ||
                    vendor.Zip.Trim() == "" ||
                    vendor.City.Trim() == "" ||
                    vendor.State.Trim() == "")
                {
                    reportData.Add(new VendorErrorReportData
                    {
                        VendorNumber = vendor.VendorNumber,
                        VendorName = vendor.VendorName.Trim(),
                        CheckCity = vendor.City.Trim(),
                        CheckState = vendor.State.Trim(),
                        CheckZip = vendor.Zip.Trim(),
                        TaxNumber = vendor.TaxIdNumber.Trim(),
                    });
				}
            }
			
			if (reportData.Count > 0)
			{
				subVendorErrors.Report = new srptVendorErrors(reportData);
			}
			else
			{
				subVendorErrors.Report = null;
			}
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			blnShowVendorInfo = true;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (blnSummary)
			{
				Label15.Visible = false;
				Label16.Visible = false;
				Label17.Visible = false;
				Label18.Visible = false;
				Label19.Visible = false;
				Label20.Visible = false;
				Label21.Visible = false;
				Line1.Visible = false;
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("VendorBinder");
			this.Fields.Add("SpaceBinder");
            this.Fields.Add("FormTypeBinder");
		}

		private void ClearGroupTotals()
		{
			int counter;
			for (counter = 0; counter <= 9; counter++)
			{
				curGroupTotals[counter] = 0;
			}
		}

        private void ClearFormTypeTotals()
        {
            int counter;
            for (counter = 0; counter <= 9; counter++)
            {
                curCompleteTotals[counter] = 0;
            }
        }

		private string GetCodeTitle(int x)
		{
            return x + " - " + taxFormService.GetTaxCodeDescription(x);
        }

        private void FixGroupTotalFields()
        {
            int counter;
            int counter2;
            int intWhileCounter;

            for (counter = 1; counter <= 8; counter++)
            {
                if (((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter) + "Title"]).Text == "")
                {
                    intWhileCounter = 0;
                    do
                    {
                        for (counter2 = counter + 1; counter2 <= 9; counter2++)
                        {
                            ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter2 - 1) + "Title"]).Text = ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter2) + "Title"]).Text;
                            ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter2 - 1) + "GroupTotal"]).Text = ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter2) + "GroupTotal"]).Text;
                        }
                        ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode9Title"]).Text = "";
                        ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode9GroupTotal"]).Text = "";
                        intWhileCounter += 1;
                    }
                    while (intWhileCounter < 9 - counter && ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + counter + "Title"]).Text == "");
                }
            }
            for (counter = 1; counter <= 9; counter++)
            {
                if (((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter2.Controls["fldCode" + FCConvert.ToString(counter) + "Title"]).Text == "")
                {
                    this.GroupFooter2.Controls["fldCode" + counter + "Title"].Visible = false;
                    this.GroupFooter2.Controls["fldCode" + counter + "GroupTotal"].Visible = false;
                }
                else
                {
                    this.GroupFooter2.Controls["fldCode" + counter + "Title"].Visible = true;
                    this.GroupFooter2.Controls["fldCode" + counter + "GroupTotal"].Visible = true;
                }
            }
        }

		private void FixFinalTotalFields()
		{
			int counter;
			int counter2;
			int intWhileCounter;
			for (counter = 1; counter <= 8; counter++)
			{
				if (((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode" + FCConvert.ToString(counter)]).Text == "")
				{
					intWhileCounter = 0;
					do
					{
						for (counter2 = counter + 1; counter2 <= 9; counter2++)
						{
							((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode" + FCConvert.ToString(counter2 - 1)]).Text = ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode" + FCConvert.ToString(counter2)]).Text;
							((GrapeCity.ActiveReports.SectionReportModel.Label)this.GroupFooter5.Controls["lblCode" + FCConvert.ToString(counter2 - 1)]).Text = ((GrapeCity.ActiveReports.SectionReportModel.Label)this.GroupFooter5.Controls["lblCode" + FCConvert.ToString(counter2)]).Text;
						}
						((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode9"]).Text = "";
						((GrapeCity.ActiveReports.SectionReportModel.Label)this.GroupFooter5.Controls["lblCode9"]).Text = "";
						intWhileCounter += 1;
					}
					while (intWhileCounter < 9 - counter && ((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode" + counter]).Text == "");
				}
			}
			for (counter = 1; counter <= 9; counter++)
			{
				if (((GrapeCity.ActiveReports.SectionReportModel.TextBox)this.GroupFooter5.Controls["fldCode" + FCConvert.ToString(counter)]).Text == "")
				{
					this.GroupFooter5.Controls["fldCode" + counter].Visible = false;
					this.GroupFooter5.Controls["lblCode" + counter].Visible = false;
				}
				else
				{
					this.GroupFooter5.Controls["fldCode" + counter].Visible = true;
					this.GroupFooter5.Controls["lblCode" + counter].Visible = true;
				}
			}
		}

		private void GroupHeader5_Format(object sender, EventArgs e)
		{
            switch (this.Fields["FormTypeBinder"].Value)
            {
				case TaxFormType.MISC1099:
                    lblFormTypeLabel.Text = "1099 MISC";
                    break;
				case TaxFormType.NEC1099:
                    lblFormTypeLabel.Text = "1099 NEC";
                    break;
				default:
                    lblFormTypeLabel.Text = "UNKNOWN FORM TYPE " + this.Fields["FormTypeBinder"].Value;
                    break;
			}
		}
	}
}
