﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckRecList.
	/// </summary>
	partial class rptCheckRecList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckRecList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheckDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtCheckNumber,
				this.txtType,
				this.txtCheckDate,
				this.txtAmount,
				this.txtStatus,
				this.txtPayee
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.txtTotal,
				this.txtCount,
				this.Label23,
				this.Label24
			});
			this.ReportFooter.Height = 0.375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.lblPage,
				this.Label3,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label21,
				this.Label22,
				this.lblSubTitle
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.40625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Bank Reconciliation";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.677083F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.1875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Page 1";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label15.Text = "Check";
			this.Label15.Top = 0.6875F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label16.Text = "Type";
			this.Label16.Top = 0.6875F;
			this.Label16.Width = 0.375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.46875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 0";
			this.Label17.Text = "Date";
			this.Label17.Top = 0.6875F;
			this.Label17.Width = 0.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.34375F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label18.Text = "Amount";
			this.Label18.Top = 0.6875F;
			this.Label18.Width = 1.0625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.65625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label21.Text = "Payee";
			this.Label21.Top = 0.6875F;
			this.Label21.Width = 2.59375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.5F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 0";
			this.Label22.Text = "Status";
			this.Label22.Top = 0.6875F;
			this.Label22.Width = 1.0625F;
			// 
			// lblSubTitle
			// 
			this.lblSubTitle.Height = 0.1666667F;
			this.lblSubTitle.HyperLink = null;
			this.lblSubTitle.Left = 1.40625F;
			this.lblSubTitle.Name = "lblSubTitle";
			this.lblSubTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.lblSubTitle.Text = null;
			this.lblSubTitle.Top = 0.2291667F;
			this.lblSubTitle.Width = 4.677083F;
			// 
			// txtCheckNumber
			// 
			this.txtCheckNumber.Height = 0.1875F;
			this.txtCheckNumber.Left = 0.01041667F;
			this.txtCheckNumber.Name = "txtCheckNumber";
			this.txtCheckNumber.Style = "font-family: \'Tahoma\'";
			this.txtCheckNumber.Text = null;
			this.txtCheckNumber.Top = 0F;
			this.txtCheckNumber.Width = 0.9375F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 1F;
			this.txtType.Name = "txtType";
			this.txtType.Style = "font-family: \'Tahoma\'";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 0.375F;
			// 
			// txtCheckDate
			// 
			this.txtCheckDate.Height = 0.1875F;
			this.txtCheckDate.Left = 1.46875F;
			this.txtCheckDate.Name = "txtCheckDate";
			this.txtCheckDate.Style = "font-family: \'Tahoma\'";
			this.txtCheckDate.Text = null;
			this.txtCheckDate.Top = 0F;
			this.txtCheckDate.Width = 0.8125F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.1875F;
			this.txtAmount.Left = 2.34375F;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 1.0625F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 3.5F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Style = "font-family: \'Tahoma\'";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0F;
			this.txtStatus.Width = 1.0625F;
			// 
			// txtPayee
			// 
			this.txtPayee.Height = 0.1875F;
			this.txtPayee.Left = 4.65625F;
			this.txtPayee.Name = "txtPayee";
			this.txtPayee.Style = "font-family: \'Tahoma\'";
			this.txtPayee.Text = null;
			this.txtPayee.Top = 0F;
			this.txtPayee.Width = 2.78125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.04166667F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.05208333F;
			this.Line1.Width = 7.385417F;
			this.Line1.X1 = 0.04166667F;
			this.Line1.X2 = 7.427083F;
			this.Line1.Y1 = 0.05208333F;
			this.Line1.Y2 = 0.05208333F;
			// 
			// txtTotal
			// 
			this.txtTotal.Height = 0.1875F;
			this.txtTotal.Left = 2.34375F;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotal.Text = null;
			this.txtTotal.Top = 0.125F;
			this.txtTotal.Width = 1.0625F;
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.1875F;
			this.txtCount.Left = 4.65625F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "font-family: \'Tahoma\'; text-align: left";
			this.txtCount.Text = null;
			this.txtCount.Top = 0.125F;
			this.txtCount.Width = 1.0625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 1.46875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label23.Text = "Total";
			this.Label23.Top = 0.125F;
			this.Label23.Width = 0.8125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.75F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label24.Text = "Count";
			this.Label24.Top = 0.125F;
			this.Label24.Width = 0.8125F;
			// 
			// rptCheckRecList
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheckDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheckDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPayee;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
