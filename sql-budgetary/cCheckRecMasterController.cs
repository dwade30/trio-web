﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class cCheckRecMasterController
	{
		//=========================================================
		private string strLastError = string.Empty;
		private int lngLastError;

		private void SetError(int lngError, string strErrorMessage)
		{
			lngLastError = lngError;
			strLastError = strErrorMessage;
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}
		// vbPorter upgrade warning: strType As string	OnWriteFCConvert.ToInt16
		public void SetCheckToCashed(int lngCheckNumber, string strType, int lngBankID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (lngCheckNumber > 0)
				{
					clsDRWrapper rs = new clsDRWrapper();
					string strSQL = "";
					strSQL = "update checkrecmaster set status = '3', statusdate = '" + FCConvert.ToString(DateTime.Now) + "' where status = '2' and checknumber = " + FCConvert.ToString(lngCheckNumber) + " and [type] = '" + strType + "' and banknumber = " + FCConvert.ToString(lngBankID);
					rs.Execute(strSQL, "Budgetary");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public cCheckRecMaster GetCheckRecMaster(int lngID)
		{
			cCheckRecMaster GetCheckRecMaster = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCheckRecMaster crmaster = new cCheckRecMaster();
				rsLoad.OpenRecordset("select * from checkrecmaster where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					crmaster = new cCheckRecMaster();
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					crmaster.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					crmaster.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					// TODO Get_Fields: Field [CDEEntry] not found!! (maybe it is an alias?)
					crmaster.CDEntry = FCConvert.ToBoolean(rsLoad.Get_Fields("CDEEntry"));
					if (Information.IsDate(rsLoad.Get_Fields("checkdate")))
					{
						crmaster.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
					}
					else
					{
						crmaster.CheckDate = "";
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					crmaster.CheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("CheckNumber"))));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					crmaster.CheckType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					crmaster.CRPeriodCloseoutKey = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("crperiodcloseoutkey"));
					crmaster.DirectDeposit = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("DirectDeposit"));
					crmaster.EnteredBy = FCConvert.ToString(rsLoad.Get_Fields_String("EnteredBy"));
					if (Information.IsDate(rsLoad.Get_Fields("EnteredDate")))
					{
						crmaster.EnteredDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("EnteredDate"));
					}
					else
					{
						crmaster.EnteredDate = "";
					}
					crmaster.IsAutomatic = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("automatic"));
					crmaster.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					crmaster.PayrunID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("PYPayrunID"));
					if (Information.IsDate(rsLoad.Get_Fields("PYPayDate")))
					{
						crmaster.PYPayDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PYPayDate"));
					}
					else
					{
						crmaster.PYPayDate = "";
					}
					crmaster.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					if (Information.IsDate(rsLoad.Get_Fields("statusdate")))
					{
						crmaster.StatusDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("statusdate"));
					}
					else
					{
						crmaster.StatusDate = "";
					}
					crmaster.Transferred = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Transferred"));
					crmaster.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					crmaster.IsUpdated = false;
				}
				GetCheckRecMaster = crmaster;
				return GetCheckRecMaster;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCheckRecMaster;
		}

		public cCheckRecMaster GetCheckRecMasterByCheckAndBank(int lngCheckNumber, int lngBankID)
		{
			cCheckRecMaster GetCheckRecMasterByCheckAndBank = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCheckRecMaster crmaster = new cCheckRecMaster();
				rsLoad.OpenRecordset("select * from checkrecmaster where checknumber = " + FCConvert.ToString(lngCheckNumber) + " and BankNumber = " + FCConvert.ToString(lngBankID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					crmaster = new cCheckRecMaster();
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					crmaster.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					crmaster.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					crmaster.CDEntry = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CDEntry"));
					if (Information.IsDate(rsLoad.Get_Fields("checkdate")))
					{
						crmaster.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
					}
					else
					{
						crmaster.CheckDate = "";
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					crmaster.CheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("CheckNumber"))));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					crmaster.CheckType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					crmaster.CRPeriodCloseoutKey = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("crperiodcloseoutkey"));
					crmaster.DirectDeposit = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("DirectDeposit"));
					crmaster.EnteredBy = FCConvert.ToString(rsLoad.Get_Fields_String("EnteredBy"));
					if (Information.IsDate(rsLoad.Get_Fields("EnteredDate")))
					{
						crmaster.EnteredDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("EnteredDate"));
					}
					else
					{
						crmaster.EnteredDate = "";
					}
					crmaster.IsAutomatic = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("automatic"));
					crmaster.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					crmaster.PayrunID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("PYPayrunID"));
					if (Information.IsDate(rsLoad.Get_Fields("PYPayDate")))
					{
						crmaster.PYPayDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PYPayDate"));
					}
					else
					{
						crmaster.PYPayDate = "";
					}
					crmaster.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					if (Information.IsDate(rsLoad.Get_Fields("statusdate")))
					{
						crmaster.StatusDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("statusdate"));
					}
					else
					{
						crmaster.StatusDate = "";
					}
					crmaster.Transferred = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Transferred"));
					crmaster.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					crmaster.IsUpdated = false;
				}
				GetCheckRecMasterByCheckAndBank = crmaster;
				return GetCheckRecMasterByCheckAndBank;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCheckRecMasterByCheckAndBank;
		}
	}
}
