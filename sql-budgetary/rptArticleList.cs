﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptArticleList.
	/// </summary>
	public partial class rptArticleList : BaseSectionReport
	{
		public static rptArticleList InstancePtr
		{
			get
			{
				return (rptArticleList)Sys.GetInstance(typeof(rptArticleList));
			}
		}

		protected rptArticleList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsExpYTDActivity.Dispose();
				rsRevYTDActivity.Dispose();
				rsLedgerYTDActivity.Dispose();
				rsDetailInfo.Dispose();
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptArticleList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curArticleTotal As Decimal	OnWrite(short, Decimal)
		Decimal curArticleTotal;
		// vbPorter upgrade warning: curArticleYTDTotal As Decimal	OnWrite(short, Decimal)
		Decimal curArticleYTDTotal;
		// vbPorter upgrade warning: curArticleBalanceTotal As Decimal	OnWrite(short, Decimal)
		Decimal curArticleBalanceTotal;
		clsDRWrapper rsExpYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();

		public rptArticleList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Article List";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("ArticleBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNextArticle = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				rsDetailInfo.OpenRecordset("SELECT * FROM ArticleAccounts WHERE ArticleNumber = " + rsInfo.Get_Fields_Int32("ArticleNumber") + " ORDER BY Account");
				if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					executeCheckNextArticle = true;
					goto CheckNextArticle;
				}
			}
			else
			{
				rsDetailInfo.MoveNext();
				executeCheckNextArticle = true;
				goto CheckNextArticle;
			}
			CheckNextArticle:
			;
			if (executeCheckNextArticle)
			{
				if (rsDetailInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile() != true)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM ArticleAccounts WHERE ArticleNumber = " + rsInfo.Get_Fields_Int32("ArticleNumber") + " ORDER BY Account");
						if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
						{
							eArgs.EOF = false;
						}
						else
						{
							goto CheckNextArticle;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["ArticleBinder"].Value = rsInfo.Get_Fields_Int32("ArticleNumber");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			curArticleTotal = 0;
			curArticleYTDTotal = 0;
			curArticleBalanceTotal = 0;
			rsInfo.OpenRecordset("SELECT * FROM ArticleMaster WHERE ArticleBudget <> 0 ORDER BY ArticleNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo();
				// CalculateAccountInfo True, True, False, "E"
				// CalculateAccountInfo True, True, False, "R"
				// CalculateAccountInfo True, True, False, "G"
				RetrieveInfo();
			}
			else
			{
				MessageBox.Show("There are no articles set up at this time.", "No Articles", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			double dblPercent = 0;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetailInfo.Get_Fields_String("Account");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldTitle.Text = modAccountTitle.ReturnAccountDescription(rsDetailInfo.Get_Fields("Account"));
			fldBudget.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00");
			curArticleTotal += rsDetailInfo.Get_Fields_Decimal("ArticleBudget");
			if (Strings.Left(FCConvert.ToString(fldAccount), 1) == "E")
			{
				if (rsExpYTDActivity.FindFirstRecord("Account", fldAccount))
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					dblPercent = rsDetailInfo.Get_Fields_Decimal("ArticleBudget") / (rsExpYTDActivity.Get_Fields("OriginalBudgetTotal") + rsExpYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					fldYTD.Text = Strings.Format((rsExpYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpYTDActivity.Get_Fields("PostedCreditsTotal")) * dblPercent, "#,##0.00");
					fldBalance.Text = Strings.Format(FCConvert.ToDouble(fldBudget.Text) - FCConvert.ToDouble(fldYTD.Text), "#,##0.00");
					if (FCConvert.ToDouble(fldYTD.Text) != 0)
					{
						fldSpent.Text = Strings.Format(FCConvert.ToDouble(fldYTD.Text) / FCConvert.ToDouble(fldBudget.Text), "#.00%");
					}
					else
					{
						fldSpent.Text = "0.00%";
					}
				}
				else
				{
					fldYTD.Text = "0.00";
					fldBalance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00");
					fldSpent.Text = "0.00%";
				}
			}
			else if (Strings.Left(FCConvert.ToString(fldAccount.Text), 1) == "R")
			{
				if (rsRevYTDActivity.FindFirstRecord("Account", fldAccount.Text))
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					dblPercent = rsDetailInfo.Get_Fields_Decimal("ArticleBudget") / (rsRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					fldYTD.Text = Strings.Format((rsRevYTDActivity.Get_Fields("PostedDebitsTotal") - rsRevYTDActivity.Get_Fields("PostedCreditsTotal")) * dblPercent, "#,##0.00");
					fldBalance.Text = Strings.Format(FCConvert.ToDouble(fldBudget.Text) - FCConvert.ToDouble(fldYTD.Text), "#,##0.00");
					if (FCConvert.ToDouble(fldYTD.Text) != 0)
					{
						fldSpent.Text = Strings.Format(FCConvert.ToDouble(fldYTD.Text) / FCConvert.ToDouble(fldBudget.Text), "#.00%");
					}
					else
					{
						fldSpent.Text = "0.00%";
					}
				}
				else
				{
					fldYTD.Text = "0.00";
					fldBalance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00");
					fldSpent.Text = "0.00%";
				}
			}
			else
			{
				if (rsLedgerYTDActivity.FindFirstRecord("Account", fldAccount.Text))
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					dblPercent = rsDetailInfo.Get_Fields_Decimal("ArticleBudget") / (rsLedgerYTDActivity.Get_Fields("OriginalBudgetTotal") + rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					fldYTD.Text = Strings.Format((rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal") - rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal")) * dblPercent, "#,##0.00");
					fldBalance.Text = Strings.Format(FCConvert.ToDouble(fldBudget.Text) - FCConvert.ToDouble(fldYTD.Text), "#,##0.00");
					if (FCConvert.ToDecimal(fldYTD) != 0)
					{
						fldSpent.Text = Strings.Format(FCConvert.ToDouble(fldYTD.Text) / FCConvert.ToDouble(fldBudget.Text), "#.00%");
					}
					else
					{
						fldSpent.Text = "0.00%";
					}
				}
				else
				{
					fldYTD.Text = "0.00";
					fldBalance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00");
					fldSpent.Text = "0.00%";
				}
			}
			curArticleYTDTotal += FCConvert.ToDecimal(fldYTD);
			curArticleBalanceTotal += FCConvert.ToDecimal(fldBalance);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldArticleTotal.Text = Strings.Format(curArticleTotal, "#,##0.00");
			fldYTDTotal.Text = Strings.Format(curArticleYTDTotal, "#,##0.00");
			fldBalanceTotal.Text = Strings.Format(curArticleBalanceTotal, "#,##0.00");
			if (FCConvert.ToDecimal(fldYTDTotal) != 0)
			{
				fldSpentTotal.Text = Strings.Format(FCConvert.ToDouble(fldYTDTotal.Text) / FCConvert.ToDouble(fldArticleTotal.Text), "#.00%");
			}
			else
			{
				fldSpentTotal.Text = "0.00%";
			}
			curArticleTotal = 0;
			curArticleBalanceTotal = 0;
			curArticleYTDTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldArticleTitle.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields_Int32("ArticleNumber"), 3) + " " + rsInfo.Get_Fields_String("ArticleTitle");
			fldArticleBudget.Text = "Article Budget: " + Strings.Format(rsInfo.Get_Fields_Decimal("ArticleBudget"), "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent;
			int LowDateCurrent;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			string strTable = "";
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rsExpYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}

		private void rptArticleList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptArticleList.Caption	= "Article List";
			//rptArticleList.Icon	= "rptArticleList.dsx":0000";
			//rptArticleList.Left	= 0;
			//rptArticleList.Top	= 0;
			//rptArticleList.Width	= 11880;
			//rptArticleList.Height	= 8595;
			//rptArticleList.StartUpPosition	= 3;
			//rptArticleList.SectionData	= "rptArticleList.dsx":058A;
			//End Unmaped Properties
		}

		private void rptArticleList_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
