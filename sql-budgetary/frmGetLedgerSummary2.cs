﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetLedgerSummary.
	/// </summary>
	public partial class frmGetLedgerSummary : BaseForm
	{
		public frmGetLedgerSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbFormat.SelectedIndex = 0;
			this.cmbEditLayout.SelectedIndex = 0;
			this.cmbEditFormat.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetLedgerSummary InstancePtr
		{
			get
			{
				return (frmGetLedgerSummary)Sys.GetInstance(typeof(frmGetLedgerSummary));
			}
		}

		protected frmGetLedgerSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			int lngRecord = 0;
			if (cmbFormat.SelectedIndex == 1)
			{
				if (cmbEditLayout.SelectedIndex == 0)
				{
					if (cboLayout.SelectedIndex != -1)
					{
						if (cboLayout.SelectedIndex != 0)
						{
							// If optTown Then
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayout.Text + "' AND Type = 'LS'");
							modBudgetaryMaster.Statics.blnLedgerSummaryEdit = true;
							frmLedgerSummarySetup.InstancePtr.Show(App.MainForm);
							// End If
						}
						else
						{
							// If optTown Then
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.Reset();
							modBudgetaryMaster.Statics.blnLedgerSummaryEdit = true;
							frmLedgerSummarySetup.InstancePtr.Show(App.MainForm);
							// End If
						}
					}
				}
				else
				{
					if (cboLayoutDelete.SelectedIndex != -1)
					{
						// If optTown Then
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayoutDelete.Text + "' AND Type = 'LS'");
						lngRecord = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM Reports WHERE CriteriaID = " + FCConvert.ToString(lngRecord) + " AND Type = 'LS'");
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
						{
							MessageBox.Show("You can't delete this Selection Criteria because it is used in one or more reports.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						answer = MessageBox.Show("Do you really want to delete this selection criteria?", "Delete Layout?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
						if (answer == DialogResult.No)
						{
							// do nothing
						}
						else
						{
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + cboLayoutDelete.Text + "' AND Type = 'LS'");
							modBudgetaryAccounting.Statics.SearchResults.Delete();
							modBudgetaryAccounting.Statics.SearchResults.Update();
							FillLayoutCombo();
							cboLayoutDelete.SelectedIndex = -1;
						}
						// End If
					}
				}
			}
			else
			{
				if (cmbEditFormat.SelectedIndex == 0)
				{
					if (cboFormat.SelectedIndex != -1)
					{
						if (cboFormat.SelectedIndex != 0)
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
							modBudgetaryMaster.Statics.blnLedgerSummaryEdit = true;
							frmCustomizeLedgerSummary.InstancePtr.Show(App.MainForm);
						}
						else
						{
							//FC:FINA:KV:IIT807+FC-8697
							this.Hide();
							modBudgetaryAccounting.Statics.SearchResults.Reset();
							modBudgetaryMaster.Statics.blnLedgerSummaryEdit = true;
							frmCustomizeLedgerSummary.InstancePtr.Show(App.MainForm);
						}
					}
				}
				else
				{
					if (cboFormatDelete.SelectedIndex != -1)
					{
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + cboFormatDelete.Text + "'");
						lngRecord = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM Reports WHERE FormatID = " + FCConvert.ToString(lngRecord) + " AND Type = 'LS'");
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
						{
							MessageBox.Show("You can't delete this Ledger Report Format because it is used in one or more reports.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						answer = MessageBox.Show("Do you really want to delete this format?", "Delete Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
						if (answer == DialogResult.No)
						{
							// do nothing
						}
						else
						{
							modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + cboFormatDelete.Text + "'");
							modBudgetaryAccounting.Statics.SearchResults.Delete();
							modBudgetaryAccounting.Statics.SearchResults.Update();
							FillFormatCombo();
							cboFormatDelete.SelectedIndex = -1;
						}
					}
				}
			}
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdOK, new System.EventArgs());
		}

		private void frmGetLedgerSummary_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				FillFormatCombo();
				FillLayoutCombo();
				return;
			}
			FillFormatCombo();
			FillLayoutCombo();
			this.Refresh();
		}

		private void frmGetLedgerSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetLedgerSummary.FillStyle	= 0;
			//frmGetLedgerSummary.ScaleWidth	= 9045;
			//frmGetLedgerSummary.ScaleHeight	= 7320;
			//frmGetLedgerSummary.LinkTopic	= "Form2";
			//frmGetLedgerSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetLedgerSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdOK_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmbEditFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbEditFormat.SelectedIndex == 0)
			{
				cboFormat.SelectedIndex = -1;
				cboFormat.Visible = true;
				cboFormatDelete.Visible = false;
			}
			if (cmbEditFormat.SelectedIndex == 1)
			{
				cboFormatDelete.SelectedIndex = -1;
				cboFormatDelete.Visible = true;
				cboFormat.Visible = false;
			}
		}

		private void cmbEditLayout_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbEditLayout.SelectedIndex == 0)
			{
				cboLayout.SelectedIndex = -1;
				cboLayout.Visible = true;
				cboLayoutDelete.Visible = false;
			}
			else if (cmbEditLayout.SelectedIndex == 1)
			{
				cboLayoutDelete.SelectedIndex = -1;
				cboLayoutDelete.Visible = true;
				cboLayout.Visible = false;
			}
		}

		private void cmbFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbFormat.SelectedIndex == 0)
			{
				cboFormat.Enabled = true;
				cmbEditFormat.Enabled = true;
				cboLayout.Enabled = false;
				cmbEditLayout.Enabled = false;
			}
			else if (cmbFormat.SelectedIndex == 1)
			{
				cboLayout.Enabled = true;
				cmbEditLayout.Enabled = true;
				cboFormat.Enabled = false;
				cmbEditFormat.Enabled = false;
			}
		}

		private void FillLayoutCombo()
		{
			int counter = 0;
			cboLayout.Clear();
			cboLayoutDelete.Clear();
			cboLayout.AddItem("Create New Layout");
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboLayout.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					cboLayoutDelete.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
		}

		private void FillFormatCombo()
		{
			int counter = 0;
			cboFormat.Clear();
			cboFormatDelete.Clear();
			cboFormat.AddItem("Create New Format");
			rs.OpenRecordset("SELECT * FROM LedgerSummaryFormats ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboFormat.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					cboFormatDelete.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}
	}
}
