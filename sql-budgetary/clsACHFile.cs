﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class clsACHFile
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHFileInfo tFileInfo = new clsACHFileInfo();
		private clsACHFileInfo tFileInfo_AutoInitialized;

		private clsACHFileInfo tFileInfo
		{
			get
			{
				if (tFileInfo_AutoInitialized == null)
				{
					tFileInfo_AutoInitialized = new clsACHFileInfo();
				}
				return tFileInfo_AutoInitialized;
			}
			set
			{
				tFileInfo_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHFileHeaderRecord tFileHeader = new clsACHFileHeaderRecord();
		private clsACHFileHeaderRecord tFileHeader_AutoInitialized;

		private clsACHFileHeaderRecord tFileHeader
		{
			get
			{
				if (tFileHeader_AutoInitialized == null)
				{
					tFileHeader_AutoInitialized = new clsACHFileHeaderRecord();
				}
				return tFileHeader_AutoInitialized;
			}
			set
			{
				tFileHeader_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHFileControlRecord tfilecontrol = new clsACHFileControlRecord();
		private clsACHFileControlRecord tfilecontrol_AutoInitialized;

		private clsACHFileControlRecord tfilecontrol
		{
			get
			{
				if (tfilecontrol_AutoInitialized == null)
				{
					tfilecontrol_AutoInitialized = new clsACHFileControlRecord();
				}
				return tfilecontrol_AutoInitialized;
			}
			set
			{
				tfilecontrol_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection tBatches = new FCCollection();
		private FCCollection tBatches_AutoInitialized;

		private FCCollection tBatches
		{
			get
			{
				if (tBatches_AutoInitialized == null)
				{
					tBatches_AutoInitialized = new FCCollection();
				}
				return tBatches_AutoInitialized;
			}
			set
			{
				tBatches_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection tFillers = new FCCollection();
		private FCCollection tFillers_AutoInitialized;

		private FCCollection tFillers
		{
			get
			{
				if (tFillers_AutoInitialized == null)
				{
					tFillers_AutoInitialized = new FCCollection();
				}
				return tFillers_AutoInitialized;
			}
			set
			{
				tFillers_AutoInitialized = value;
			}
		}

		public clsACHFileHeaderRecord FileHeader()
		{
			clsACHFileHeaderRecord FileHeader = null;
			FileHeader = tFileHeader;
			return FileHeader;
		}

		public clsACHFileControlRecord FileControl()
		{
			clsACHFileControlRecord FileControl = null;
			FileControl = tfilecontrol;
			return FileControl;
		}

		public clsACHFileInfo FileInfo()
		{
			clsACHFileInfo FileInfo = null;
			FileInfo = tFileInfo;
			return FileInfo;
		}

		public FCCollection Batches
		{
			get
			{
				return tBatches;
			}
		}

		public FCCollection Fillers()
		{
			FCCollection Fillers = null;
			Fillers = tFillers;
			return Fillers;
		}

		public void AddBatch(ref clsACHBatch tBatch)
		{
			tBatches.Add(tBatch);
			tBatch.BatchNumber = FCConvert.ToInt16(tBatches.Count);
		}

		public void CreateFillers_2(int intNumFillers)
		{
			CreateFillers(ref intNumFillers);
		}

		public void CreateFillers(ref int intNumFillers)
		{
			int X;
			for (X = tFillers.Count - 1; X >= 0; X--)
			{
				tFillers.Remove(X);
			}
			// X
			clsACHBlockFiller tBlockFill;
			for (X = 1; X <= intNumFillers; X++)
			{
				tBlockFill = new clsACHBlockFiller();
				tFillers.Add(tBlockFill);
			}
			// X
		}

		public void BuildFileControl()
		{
			double dblDebits = 0;
			double dblCredits = 0;
			double dblHash = 0;
			int intEntries = 0;
			int intBlocks;
			int intBlockRecs = 0;
			foreach (clsACHBatch tBatch in tBatches)
			{
				dblDebits += tBatch.Debits;
				dblCredits += tBatch.Credits;
				dblHash += tBatch.Hash;
				intEntries += tBatch.BatchControl().EntryAddendaCount;
				intBlockRecs += tBatch.BlockRecords;
			}
			// tBatch
			tfilecontrol.TotalCredit = dblCredits;
			tfilecontrol.TotalDebit = dblDebits;
			tfilecontrol.Hash = dblHash;
			tfilecontrol.BatchCount = FCConvert.ToInt16(tBatches.Count);
			tfilecontrol.EntryAddendaCount = FCConvert.ToInt16(intEntries);
			intBlockRecs += 2;
			// file header and control
			intBlocks = FCConvert.ToInt16(intBlockRecs / 10);
			if (intBlockRecs % 10 > 0)
			{
				intBlocks += 1;
			}
			tfilecontrol.BlockCount = FCConvert.ToInt16(intBlocks);
			CreateFillers_2((10 - (intBlockRecs % 10)) % 10);
		}
	}
}
