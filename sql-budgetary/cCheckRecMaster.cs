﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class cCheckRecMaster
	{
		//=========================================================
		private int lngRecordID;
		private int lngCheckNumber;
		// vbPorter upgrade warning: strCheckType As short --> As int	OnWrite(string)
		private int strCheckType;
		private string strCheckDate = "";
		private string strPayer = string.Empty;
		private double dblAmount;
		private string strStatus = string.Empty;
		private string strStatusDate = "";
		private int lngBankNumber;
		private bool boolDirectDeposit;
		private int lngCRPeriodCloseoutKey;
		private string strPYPayDate = "";
		private int lngPayrunID;
		private bool boolCDEntry;
		private bool boolTransferred;
		private bool boolAutomatic;
		private string strEnteredBy = string.Empty;
		private string strEnteredDate = "";
		private bool boolUpdated;
		private bool boolDeleted;

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
				IsUpdated = true;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}
		// vbPorter upgrade warning: strType As string	OnReadFCConvert.ToInt32(
		public string CheckType
		{
			set
			{
				strCheckType = FCConvert.ToInt32(value);
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As string	OnWriteFCConvert.ToInt32(
			get
			{
				string CheckType = "";
				CheckType = FCConvert.ToString(strCheckType);
				return CheckType;
			}
		}

		public string CheckDate
		{
			set
			{
				IsUpdated = true;
				if (Information.IsDate(value))
				{
					strCheckDate = value;
				}
				else
				{
					strCheckDate = "";
				}
			}
			get
			{
				string CheckDate = "";
				CheckDate = strCheckDate;
				return CheckDate;
			}
		}

		public string Name
		{
			set
			{
				strPayer = value;
				IsUpdated = true;
			}
			get
			{
				string Name = "";
				Name = strPayer;
				return Name;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public string Status
		{
			set
			{
				IsUpdated = true;
				strStatus = value;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}

		public string StatusDate
		{
			set
			{
				IsUpdated = true;
				if (Information.IsDate(value))
				{
					strStatusDate = value;
				}
				else
				{
					strStatusDate = "";
				}
			}
			get
			{
				string StatusDate = "";
				StatusDate = strStatusDate;
				return StatusDate;
			}
		}

		public int BankNumber
		{
			set
			{
				lngBankNumber = value;
				IsUpdated = true;
			}
			get
			{
				int BankNumber = 0;
				BankNumber = lngBankNumber;
				return BankNumber;
			}
		}

		public bool DirectDeposit
		{
			set
			{
				IsUpdated = true;
				boolDirectDeposit = value;
			}
			get
			{
				bool DirectDeposit = false;
				DirectDeposit = boolDirectDeposit;
				return DirectDeposit;
			}
		}

		public int CRPeriodCloseoutKey
		{
			set
			{
				IsUpdated = true;
				lngCRPeriodCloseoutKey = value;
			}
			get
			{
				int CRPeriodCloseoutKey = 0;
				CRPeriodCloseoutKey = lngCRPeriodCloseoutKey;
				return CRPeriodCloseoutKey;
			}
		}

		public string PYPayDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPYPayDate = value;
				}
				else
				{
					strPYPayDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PYPayDate = "";
				PYPayDate = strPYPayDate;
				return PYPayDate;
			}
		}

		public int PayrunID
		{
			set
			{
				lngPayrunID = value;
				IsUpdated = true;
			}
			get
			{
				int PayrunID = 0;
				PayrunID = lngPayrunID;
				return PayrunID;
			}
		}

		public bool CDEntry
		{
			set
			{
				IsUpdated = true;
				boolCDEntry = value;
			}
			get
			{
				bool CDEntry = false;
				CDEntry = boolCDEntry;
				return CDEntry;
			}
		}

		public bool Transferred
		{
			set
			{
				boolTransferred = value;
				IsUpdated = true;
			}
			get
			{
				bool Transferred = false;
				Transferred = boolTransferred;
				return Transferred;
			}
		}

		public bool IsAutomatic
		{
			set
			{
				boolAutomatic = value;
				IsUpdated = true;
			}
			get
			{
				bool IsAutomatic = false;
				IsAutomatic = boolAutomatic;
				return IsAutomatic;
			}
		}

		public string EnteredBy
		{
			set
			{
				strEnteredBy = value;
				IsUpdated = true;
			}
			get
			{
				string EnteredBy = "";
				EnteredBy = strEnteredBy;
				return EnteredBy;
			}
		}

		public string EnteredDate
		{
			set
			{
				IsUpdated = true;
				if (Information.IsDate(value))
				{
					strEnteredDate = value;
				}
				else
				{
					strEnteredDate = "";
				}
			}
			get
			{
				string EnteredDate = "";
				EnteredDate = strEnteredDate;
				return EnteredDate;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
