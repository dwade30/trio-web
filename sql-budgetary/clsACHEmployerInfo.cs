﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class clsACHEmployerInfo
	{
		//=========================================================
		private string strEmployerAccount = "";
		private string strEmployerID = "";
		private string strEmployerName = "";
		private string strEmployerRT = "";
		private string strAccountType = string.Empty;
		private string strEmployerIDPrefix = "";
		private string strEmployerAccountType = "";

		public string AccountType
		{
			set
			{
				strAccountType = value;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}

		public string EmployerAccount
		{
			get
			{
				string EmployerAccount = "";
				EmployerAccount = strEmployerAccount;
				return EmployerAccount;
			}
			set
			{
				strEmployerAccount = value;
			}
		}

		public string EmployerID
		{
			get
			{
				string EmployerID = "";
				EmployerID = strEmployerID;
				return EmployerID;
			}
			set
			{
				strEmployerID = value;
			}
		}

		public string EmployerName
		{
			get
			{
				string EmployerName = "";
				EmployerName = strEmployerName;
				return EmployerName;
			}
			set
			{
				strEmployerName = value;
			}
		}

		public string EmployerRT
		{
			get
			{
				string EmployerRT = "";
				EmployerRT = strEmployerRT;
				return EmployerRT;
			}
			set
			{
				strEmployerRT = value;
			}
		}

		public string IDPrefix
		{
			get
			{
				string IDPrefix = "";
				IDPrefix = strEmployerIDPrefix;
				return IDPrefix;
			}
			set
			{
				strEmployerIDPrefix = value;
			}
		}
	}
}
