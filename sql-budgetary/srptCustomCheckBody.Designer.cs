﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckBody.
	/// </summary>
	partial class srptCustomCheckBody
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCustomCheckBody));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldTextAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTextAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVendorName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgIcon = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldCheckName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgElectronicSignature = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.fldCarryOver = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblVoid = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMICRLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPay = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblToTheOrderOf = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblVoidAfter90Days = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBankName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMemoLine = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSignature2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReturnName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLargeCheckNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEFT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgElectronicSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCarryOver)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMICRLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblToTheOrderOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoidAfter90Days)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMemoLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLargeCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEFT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTextAmount2,
				this.fldTextAmount1,
				this.fldDate,
				this.fldAmount,
				this.fldVendorName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.imgIcon,
				this.fldCheckName,
				this.fldCheckNumber,
				this.imgElectronicSignature,
				this.fldCarryOver,
				this.lblVoid,
				this.fldMICRLine,
				this.lblPay,
				this.lblToTheOrderOf,
				this.lblVoidAfter90Days,
				this.fldBankName,
				this.lblMemoLine,
				this.lblAmount,
				this.lblDate,
				this.lblSignature1,
				this.lblSignature2,
				this.lblReturnName,
				this.lblReturnAddress1,
				this.lblReturnAddress2,
				this.lblReturnAddress3,
				this.fldLargeCheckNumber,
				this.fldEFT
			});
			this.Detail.Height = 3.489583F;
			this.Detail.Name = "Detail";
			// 
			// fldTextAmount2
			// 
			this.fldTextAmount2.Height = 0.1875F;
			this.fldTextAmount2.Left = 0.65625F;
			this.fldTextAmount2.Name = "fldTextAmount2";
			this.fldTextAmount2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldTextAmount2.Tag = "Large";
			this.fldTextAmount2.Text = "Field2";
			this.fldTextAmount2.Top = 1.15625F;
			this.fldTextAmount2.Width = 5.875F;
			// 
			// fldTextAmount1
			// 
			this.fldTextAmount1.Height = 0.1875F;
			this.fldTextAmount1.Left = 0.65625F;
			this.fldTextAmount1.Name = "fldTextAmount1";
			this.fldTextAmount1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldTextAmount1.Tag = "Large";
			this.fldTextAmount1.Text = "1234567890123456789012345678901234567890123456789012345678901234567890";
			this.fldTextAmount1.Top = 0.96875F;
			this.fldTextAmount1.Width = 5.875F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 5.03125F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldDate.Tag = "Large";
			this.fldDate.Text = "Field2";
			this.fldDate.Top = 1.65625F;
			this.fldDate.Width = 0.78125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 6.3125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: right";
			this.fldAmount.Tag = "Large";
			this.fldAmount.Text = "12345678901234";
			this.fldAmount.Top = 1.65625F;
			this.fldAmount.Width = 1.21875F;
			// 
			// fldVendorName
			// 
			this.fldVendorName.Height = 0.1666667F;
			this.fldVendorName.Left = 0.875F;
			this.fldVendorName.Name = "fldVendorName";
			this.fldVendorName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldVendorName.Tag = "Large";
			this.fldVendorName.Text = "Field2";
			this.fldVendorName.Top = 1.9375F;
			this.fldVendorName.Width = 3.3125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1666667F;
			this.fldAddress1.Left = 0.875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress1.Tag = "Large";
			this.fldAddress1.Text = "Field2";
			this.fldAddress1.Top = 2.125F;
			this.fldAddress1.Width = 3.3125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1666667F;
			this.fldAddress2.Left = 0.875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress2.Tag = "Large";
			this.fldAddress2.Text = "Field2";
			this.fldAddress2.Top = 2.3125F;
			this.fldAddress2.Width = 3.3125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1666667F;
			this.fldAddress3.Left = 0.875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress3.Tag = "Large";
			this.fldAddress3.Text = "Field2";
			this.fldAddress3.Top = 2.5F;
			this.fldAddress3.Width = 3.3125F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1666667F;
			this.fldAddress4.Left = 0.875F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldAddress4.Tag = "Large";
			this.fldAddress4.Text = "Field2";
			this.fldAddress4.Top = 2.6875F;
			this.fldAddress4.Width = 3.3125F;
			// 
			// imgIcon
			// 
			this.imgIcon.Height = 0.96875F;
			this.imgIcon.HyperLink = null;
			this.imgIcon.ImageData = null;
			this.imgIcon.Left = 0.03125F;
			this.imgIcon.LineWeight = 1F;
			this.imgIcon.Name = "imgIcon";
			this.imgIcon.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.imgIcon.Top = 0F;
			this.imgIcon.Width = 0.96875F;
			// 
			// fldCheckName
			// 
			this.fldCheckName.Height = 0.1666667F;
			this.fldCheckName.Left = 0.875F;
			this.fldCheckName.Name = "fldCheckName";
			this.fldCheckName.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldCheckName.Tag = "Large";
			this.fldCheckName.Text = "Field2";
			this.fldCheckName.Top = 1.6875F;
			this.fldCheckName.Visible = false;
			this.fldCheckName.Width = 3.3125F;
			// 
			// fldCheckNumber
			// 
			this.fldCheckNumber.Height = 0.1875F;
			this.fldCheckNumber.Left = 6.46875F;
			this.fldCheckNumber.Name = "fldCheckNumber";
			this.fldCheckNumber.Style = "font-family: \'Courier New\'; font-size: 9pt";
			this.fldCheckNumber.Tag = "Large";
			this.fldCheckNumber.Text = "Field2";
			this.fldCheckNumber.Top = 0.125F;
			this.fldCheckNumber.Visible = false;
			this.fldCheckNumber.Width = 0.78125F;
			// 
			// imgElectronicSignature
			// 
			this.imgElectronicSignature.Height = 0.5625F;
			this.imgElectronicSignature.HyperLink = null;
			this.imgElectronicSignature.ImageData = null;
			this.imgElectronicSignature.Left = 5.0625F;
			this.imgElectronicSignature.LineWeight = 1F;
			this.imgElectronicSignature.Name = "imgElectronicSignature";
			this.imgElectronicSignature.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Stretch;
			this.imgElectronicSignature.Top = 2F;
			this.imgElectronicSignature.Width = 2.4375F;
			// 
			// fldCarryOver
			// 
			this.fldCarryOver.Height = 0.1875F;
			this.fldCarryOver.Left = 5.03125F;
			this.fldCarryOver.Name = "fldCarryOver";
			this.fldCarryOver.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center";
			this.fldCarryOver.Tag = "Large";
			this.fldCarryOver.Text = "Continued on next check";
			this.fldCarryOver.Top = 2.5625F;
			this.fldCarryOver.Visible = false;
			this.fldCarryOver.Width = 1.84375F;
			// 
			// lblVoid
			// 
			this.lblVoid.Height = 0.65625F;
			this.lblVoid.HyperLink = null;
			this.lblVoid.Left = 5.03125F;
			this.lblVoid.Name = "lblVoid";
			this.lblVoid.Style = "font-family: \'Courier New\'; font-size: 48pt; text-align: center; ddo-char-set: 1";
			this.lblVoid.Tag = "Large";
			this.lblVoid.Text = "VOID";
			this.lblVoid.Top = 1.90625F;
			this.lblVoid.Visible = false;
			this.lblVoid.Width = 1.84375F;
			// 
			// fldMICRLine
			// 
			this.fldMICRLine.Height = 0.1875F;
			this.fldMICRLine.Left = 2.09375F;
			this.fldMICRLine.Name = "fldMICRLine";
			this.fldMICRLine.Style = "font-family: \'IDAutomationMICR\'; font-size: 12pt";
			this.fldMICRLine.Tag = "Large";
			this.fldMICRLine.Text = "C123456C A345678909A  12345678901C";
			this.fldMICRLine.Top = 3.15625F;
			this.fldMICRLine.Visible = false;
			this.fldMICRLine.Width = 4.444445F;
			// 
			// lblPay
			// 
			this.lblPay.Height = 0.1875F;
			this.lblPay.HyperLink = null;
			this.lblPay.Left = 0.15625F;
			this.lblPay.Name = "lblPay";
			this.lblPay.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; ddo-char-set: 1";
			this.lblPay.Text = "PAY";
			this.lblPay.Top = 1.15625F;
			this.lblPay.Visible = false;
			this.lblPay.Width = 0.375F;
			// 
			// lblToTheOrderOf
			// 
			this.lblToTheOrderOf.Height = 0.46875F;
			this.lblToTheOrderOf.HyperLink = null;
			this.lblToTheOrderOf.Left = 0.125F;
			this.lblToTheOrderOf.Name = "lblToTheOrderOf";
			this.lblToTheOrderOf.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblToTheOrderOf.Text = "TO THE ORDER OF";
			this.lblToTheOrderOf.Top = 1.5625F;
			this.lblToTheOrderOf.Visible = false;
			this.lblToTheOrderOf.Width = 0.5F;
			// 
			// lblVoidAfter90Days
			// 
			this.lblVoidAfter90Days.Height = 0.19F;
			this.lblVoidAfter90Days.HyperLink = null;
			this.lblVoidAfter90Days.Left = 5.25F;
			this.lblVoidAfter90Days.Name = "lblVoidAfter90Days";
			this.lblVoidAfter90Days.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblVoidAfter90Days.Text = "VOID AFTER 60 DAYS";
			this.lblVoidAfter90Days.Top = 1.84375F;
			this.lblVoidAfter90Days.Visible = false;
			this.lblVoidAfter90Days.Width = 1.59375F;
			// 
			// fldBankName
			// 
			this.fldBankName.Height = 0.1875F;
			this.fldBankName.Left = 4.21875F;
			this.fldBankName.Name = "fldBankName";
			this.fldBankName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldBankName.Tag = "Large";
			this.fldBankName.Text = "Field2";
			this.fldBankName.Top = 0.3125F;
			this.fldBankName.Visible = false;
			this.fldBankName.Width = 2.0625F;
			// 
			// lblMemoLine
			// 
			this.lblMemoLine.Height = 0.19F;
			this.lblMemoLine.HyperLink = null;
			this.lblMemoLine.Left = 0.0625F;
			this.lblMemoLine.MultiLine = false;
			this.lblMemoLine.Name = "lblMemoLine";
			this.lblMemoLine.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblMemoLine.Text = "FOR________________________________________";
			this.lblMemoLine.Top = 2.90625F;
			this.lblMemoLine.Visible = false;
			this.lblMemoLine.Width = 3.1875F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.19F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.71875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblAmount.Text = "AMOUNT";
			this.lblAmount.Top = 1.5F;
			this.lblAmount.Visible = false;
			this.lblAmount.Width = 0.59375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.19F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.1875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblDate.Text = "DATE";
			this.lblDate.Top = 1.46875F;
			this.lblDate.Visible = false;
			this.lblDate.Width = 0.4375F;
			// 
			// lblSignature1
			// 
			this.lblSignature1.Height = 0.19F;
			this.lblSignature1.HyperLink = null;
			this.lblSignature1.Left = 3.8125F;
			this.lblSignature1.MultiLine = false;
			this.lblSignature1.Name = "lblSignature1";
			this.lblSignature1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblSignature1.Text = "_________________________________________________________________________________" + "__________";
			this.lblSignature1.Top = 2.65625F;
			this.lblSignature1.Visible = false;
			this.lblSignature1.Width = 3.125F;
			// 
			// lblSignature2
			// 
			this.lblSignature2.Height = 0.19F;
			this.lblSignature2.HyperLink = null;
			this.lblSignature2.Left = 3.8125F;
			this.lblSignature2.MultiLine = false;
			this.lblSignature2.Name = "lblSignature2";
			this.lblSignature2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; white-space: nowrap; dd" + "o-char-set: 1";
			this.lblSignature2.Text = "_________________________________________________________________________";
			this.lblSignature2.Top = 2.84375F;
			this.lblSignature2.Visible = false;
			this.lblSignature2.Width = 3.125F;
			// 
			// lblReturnName
			// 
			this.lblReturnName.Height = 0.19F;
			this.lblReturnName.Left = 1.0625F;
			this.lblReturnName.Name = "lblReturnName";
			this.lblReturnName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.lblReturnName.Tag = "Large";
			this.lblReturnName.Text = "COUNTY OF HANCOCK";
			this.lblReturnName.Top = 0.03125F;
			this.lblReturnName.Visible = false;
			this.lblReturnName.Width = 3.125F;
			// 
			// lblReturnAddress1
			// 
			this.lblReturnAddress1.Height = 0.19F;
			this.lblReturnAddress1.Left = 1.0625F;
			this.lblReturnAddress1.Name = "lblReturnAddress1";
			this.lblReturnAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.lblReturnAddress1.Tag = "Large";
			this.lblReturnAddress1.Text = "50 STATE STREET, SUITE #8";
			this.lblReturnAddress1.Top = 0.21875F;
			this.lblReturnAddress1.Visible = false;
			this.lblReturnAddress1.Width = 3.125F;
			// 
			// lblReturnAddress2
			// 
			this.lblReturnAddress2.Height = 0.19F;
			this.lblReturnAddress2.Left = 1.0625F;
			this.lblReturnAddress2.Name = "lblReturnAddress2";
			this.lblReturnAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.lblReturnAddress2.Tag = "Large";
			this.lblReturnAddress2.Text = "ELLSWORTH, ME 04605";
			this.lblReturnAddress2.Top = 0.40625F;
			this.lblReturnAddress2.Visible = false;
			this.lblReturnAddress2.Width = 3.125F;
			// 
			// lblReturnAddress3
			// 
			this.lblReturnAddress3.Height = 0.19F;
			this.lblReturnAddress3.Left = 1.0625F;
			this.lblReturnAddress3.Name = "lblReturnAddress3";
			this.lblReturnAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.lblReturnAddress3.Tag = "Large";
			this.lblReturnAddress3.Text = "(207) 667-8272";
			this.lblReturnAddress3.Top = 0.59375F;
			this.lblReturnAddress3.Visible = false;
			this.lblReturnAddress3.Width = 3.125F;
			// 
			// fldLargeCheckNumber
			// 
			this.fldLargeCheckNumber.Height = 0.28125F;
			this.fldLargeCheckNumber.Left = 6.25F;
			this.fldLargeCheckNumber.Name = "fldLargeCheckNumber";
			this.fldLargeCheckNumber.Style = "font-family: \'Tahoma\'; font-size: 18pt";
			this.fldLargeCheckNumber.Tag = "Large";
			this.fldLargeCheckNumber.Text = null;
			this.fldLargeCheckNumber.Top = 0.03125F;
			this.fldLargeCheckNumber.Visible = false;
			this.fldLargeCheckNumber.Width = 1.041667F;
			// 
			// fldEFT
			// 
			this.fldEFT.Height = 0.1875F;
			this.fldEFT.Left = 5.03125F;
			this.fldEFT.Name = "fldEFT";
			this.fldEFT.Style = "font-family: \'Courier New\'; font-size: 9pt; text-align: center";
			this.fldEFT.Tag = "Large";
			this.fldEFT.Text = "** EFT ** EFT ** EFT **";
			this.fldEFT.Top = 2.5625F;
			this.fldEFT.Visible = false;
			this.fldEFT.Width = 1.84375F;
			// 
			// srptCustomCheckBody
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.499306F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTextAmount1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgIcon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgElectronicSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCarryOver)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMICRLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblToTheOrderOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVoidAfter90Days)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBankName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMemoLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSignature2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLargeCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEFT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTextAmount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTextAmount1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgIcon;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgElectronicSignature;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCarryOver;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVoid;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMICRLine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPay;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblToTheOrderOf;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVoidAfter90Days;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBankName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMemoLine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSignature2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLargeCheckNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEFT;
	}
}
