﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptACH.
	/// </summary>
	public partial class rptACH : BaseSectionReport
	{
		public static rptACH InstancePtr
		{
			get
			{
				return (rptACH)Sys.GetInstance(typeof(rptACH));
			}
		}

		protected rptACH _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsDDBanks.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptACH	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intpage;
		//clsDRWrapper clsLoad = new clsDRWrapper();
		double lngTotal;
		int lngCount;
		clsDRWrapper clsDDBanks = new clsDRWrapper();
		DateTime dtDate;
		int intJournal;
		clsACHFile ACHInfo;
		int intCounter;

		public rptACH()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ACH";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            IList batchDetails = ACHInfo.Batches[1].Details();
			eArgs.EOF = intCounter > batchDetails.Count;
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intpage = 1;
			intCounter = 1;
			lblTime.Text = Strings.Format(DateTime.Now, "hh:mm AMPM");
			txtPage.Text = "Page  1";
			lblMuniname.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}
		// vbPorter upgrade warning: intJournalNumber As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intBankNumber As short	OnWriteFCConvert.ToInt32(
		public void Init(DateTime dtPDate, short intJournalNumber, short intBankNumber, clsACHFile tFile, bool modalDialog)
		{
			string strSQL = "";
			string strOrderBy = "";
			dtDate = dtPDate;
			intJournal = intJournalNumber;
			clsDDBanks.OpenRecordset("select * from Banks WHERE ID = " + FCConvert.ToString(intBankNumber), "twbd0000.vb1");
			lngTotal = 0;
			lngCount = 0;
			ACHInfo = tFile;
			if (intJournal > 0)
			{
				lblPayDate.Text = "Check Date: " + Strings.Format(dtDate, "MM/dd/yyyy") + "   Journal #: " + FCConvert.ToString(intJournal);
			}
			else
			{
				lblPayDate.Text = "Check Date: " + Strings.Format(dtDate, "MM/dd/yyyy");
			}
			txtBankName.Text = clsDDBanks.Get_Fields_String("Name");
			frmReportViewer.InstancePtr.Init(this, string.Empty, FCConvert.ToInt16(FCForm.FormShowEnum.Modal), false,
                false, "Pages", false, string.Empty, "TRIO Software", false, false, showModal: modalDialog);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
            IList<clsACHDetailRecord> details = ACHInfo.Batches[1].Details();
            if (!(intCounter > details.Count))
			{
				lngCount += 1;
				var dblTemp = FCConvert.ToDouble(Strings.Format(Conversion.Val(details[intCounter - 1].TotalAmount), "0.00"));
				lngTotal += dblTemp;
				fldEmployee.Text = details[intCounter - 1].Name;
				fldAccount.Text = details[intCounter - 1].AccountNumber;
				this.Field1.Text = details[intCounter - 1].AccountType;
				txtDFI.Text = details[intCounter - 1].BankID;
				fldAmount.Text = Strings.Format(details[intCounter - 1].TotalAmount, "#,###,###,##0.00");
				intCounter += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page  " + FCConvert.ToString(intpage);
			intpage += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldFinalTotal.Text = Strings.Format(lngTotal, "#,###,###,##0.00");
			fldFinalCount.Text = lngCount.ToString();
		}

		private void rptACH_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptACH.Caption	= "ACH";
			//rptACH.Icon	= "rptACH.dsx":0000";
			//rptACH.Left	= 0;
			//rptACH.Top	= 0;
			//rptACH.Width	= 11880;
			//rptACH.Height	= 8595;
			//rptACH.StartUpPosition	= 3;
			//rptACH.SectionData	= "rptACH.dsx":058A;
			//End Unmaped Properties
		}
	}
}
