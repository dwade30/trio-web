﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptPurchaseOrderSummary.
	/// </summary>
	public partial class rptPurchaseOrderSummary : BaseSectionReport
	{
		public static rptPurchaseOrderSummary InstancePtr
		{
			get
			{
				return (rptPurchaseOrderSummary)Sys.GetInstance(typeof(rptPurchaseOrderSummary));
			}
		}

		protected rptPurchaseOrderSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPurchaseOrderSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;

		public rptPurchaseOrderSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Purchase Order Summary";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmPrintPurchaseOrderSummary.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrderBy = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			PageCounter = 0;
			curTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmPrintPurchaseOrderSummary.InstancePtr.cmbVendor.SelectedIndex == 0)
			{
				strOrderBy = "VendorNumber, PODate";
			}
			else
			{
				strOrderBy = "PODate, VendorNumber";
			}
			if (frmPrintPurchaseOrderSummary.InstancePtr.cmbBoth.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM PurchaseOrderDetails INNER JOIN PurchaseOrders ON PurchaseOrderDetails.PurchaseOrderID = PurchaseOrders.ID ORDER BY " + strOrderBy);
			}
			else if (frmPrintPurchaseOrderSummary.InstancePtr.cmbBoth.SelectedIndex == 2)
			{
				rsInfo.OpenRecordset("SELECT * FROM PurchaseOrderDetails INNER JOIN PurchaseOrders ON PurchaseOrderDetails.PurchaseOrderID = PurchaseOrders.ID WHERE Closed <> 1 ORDER BY " + strOrderBy);
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM PurchaseOrderDetails INNER JOIN PurchaseOrders ON PurchaseOrderDetails.PurchaseOrderID = PurchaseOrders.ID WHERE Closed = 1 ORDER BY " + strOrderBy);
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No encumbrance information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"));
			fldDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("PODate"), "MM/dd/yy");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				fldVendor.Text = Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000") + " " + rsVendorInfo.Get_Fields_String("CheckName");
			}
			else
			{
				fldVendor.Text = Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000") + " UNKNOWN";
			}
			// TODO Get_Fields: Field [PurchaseOrderDetails.Description] not found!! (maybe it is an alias?)
			fldDescription.Text = FCConvert.ToString(rsInfo.Get_Fields("PurchaseOrderDetails.Description"));
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			fldReference.Text = FCConvert.ToString(rsInfo.Get_Fields("PO"));
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("Total"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsInfo.Get_Fields_String("Account");
			chkClosed.Checked = rsInfo.Get_Fields_Boolean("Closed");
			curTotal += rsInfo.Get_Fields_Decimal("Total");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
