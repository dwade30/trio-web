﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckFileListByNumber.
	/// </summary>
	public partial class rptCheckFileListByNumber : BaseSectionReport
	{
		public static rptCheckFileListByNumber InstancePtr
		{
			get
			{
				return (rptCheckFileListByNumber)Sys.GetInstance(typeof(rptCheckFileListByNumber));
			}
		}

		protected rptCheckFileListByNumber _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

			if (disposing)
            {
				rsInfo.Dispose();
				rsCheckInfo.Dispose();
            }
            base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckFileListByNumber	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		public string strTransactions = "";
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		bool blnFirstAccount;
		bool blnChecksDone;
		int intTotalCount;
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
		Decimal curTotalAmount;

		public rptCheckFileListByNumber()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Check Listing";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (strTransactions == "M")
			{
				if (blnFirstAccount)
				{
					blnFirstAccount = false;
					bool executeCheckFirstNonCheck = false;
					if (blnChecksDone)
					{
						executeCheckFirstNonCheck = true;
						goto CheckFirstNonCheck;
					}
					else
					{
						if (rsCheckInfo.EndOfFile() == true)
						{
							blnChecksDone = true;
							executeCheckFirstNonCheck = true;
							goto CheckFirstNonCheck;
						}
						else
						{
							eArgs.EOF = false;
						}
					}
					CheckFirstNonCheck:
					;
					if (executeCheckFirstNonCheck)
					{
						if (rsInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
						}
						else
						{
							eArgs.EOF = false;
						}
					}
				}
				else
				{
					bool executeCheckNextNonCheck = false;
					if (blnChecksDone)
					{
						rsInfo.MoveNext();
						executeCheckNextNonCheck = true;
						goto CheckNextNonCheck;
					}
					else
					{
						rsCheckInfo.MoveNext();
						if (rsCheckInfo.EndOfFile() == true)
						{
							blnChecksDone = true;
							executeCheckNextNonCheck = true;
							goto CheckNextNonCheck;
						}
						else
						{
							eArgs.EOF = false;
						}
					}
					CheckNextNonCheck:
					;
					if (executeCheckNextNonCheck)
					{
						if (rsInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
						}
						else
						{
							eArgs.EOF = false;
						}
					}
				}
			}
			else
			{
				if (blnFirstAccount)
				{
					blnFirstAccount = false;
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (strTransactions == "M")
			{
				frmCheckListSelection.InstancePtr.Unload();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (strTransactions == "C")
			{
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '3' AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY CheckNumber");
			}
			else if (strTransactions == "O")
			{
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '2' AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY CheckNumber");
			}
			else if (strTransactions == "M")
			{
				if (frmCheckListSelection.InstancePtr.blnNoOther == false)
				{
					if (frmCheckListSelection.InstancePtr.blnNoChecks == false)
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + frmCheckListSelection.InstancePtr.strOtherSQL + " UNION ALL " + frmCheckListSelection.InstancePtr.strCheckSQL + ") as temp ORDER BY CheckNumber, CheckDate");
						rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '2' AND Status <> '2' ORDER BY CheckNumber, CheckDate");
					}
					else
					{
						rsInfo.OpenRecordset(frmCheckListSelection.InstancePtr.strOtherSQL + " ORDER BY CheckNumber, CheckDate");
						rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '2' AND Status <> '2' ORDER BY CheckNumber, CheckDate");
					}
				}
				else
				{
					if (frmCheckListSelection.InstancePtr.blnNoChecks == false)
					{
						rsInfo.OpenRecordset(frmCheckListSelection.InstancePtr.strCheckSQL + " ORDER BY CheckNumber, CheckDate");
						rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status = '2' AND Status <> '2' ORDER BY CheckNumber, CheckDate");
					}
					else
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + frmCheckListSelection.InstancePtr.strOtherSQL + " UNION ALL " + frmCheckListSelection.InstancePtr.strCheckSQL + ") as temp ORDER BY CheckNumber, CheckDate");
						rsCheckInfo.OpenRecordset("SELECT * FROM (" + frmCheckListSelection.InstancePtr.strOtherSQL + " UNION ALL " + frmCheckListSelection.InstancePtr.strCheckSQL + ") as temp ORDER BY CheckNumber, CheckDate");
					}
				}
				// If frmCheckListSelection.blnNoChecks = False Then
				// rsCheckInfo.OpenRecordset frmCheckListSelection.strCheckSQL & " ORDER BY CheckNumber"
				// Else
				// rsCheckInfo.OpenRecordset "SELECT * FROM CheckRecMaster WHERE Status = '2' AND Status <> '2' ORDER BY CheckNumber"
				// End If
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Cancel();
					//MDIParent.InstancePtr.GRID.Focus();
					return;
				}
			}
			blnFirstAccount = true;
			blnChecksDone = false;
			intTotalCount = 0;
			curTotalAmount = 0;
			lblBank.Text = App.MainForm.StatusBarText3;
			lblStatement.Text = "Statement:  " + Strings.Format(modBudgetaryAccounting.GetBDVariable("StatementDate"), "MM/dd/yy");
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strTransactions == "M" && !blnChecksDone)
			{
				//FC:FINAL:MSH - Issue #711 - can't implicitly convert int to string.
				//fldCheck.Text = rsCheckInfo.Get_Fields("CheckNumber");
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar = rsCheckInfo.Get_Fields("Type");
				if (vbPorterVar == "1")
				{
					fldType.Text = "AP";
				}
				else if (vbPorterVar == "2")
				{
					fldType.Text = "PY";
				}
				else if (vbPorterVar == "3")
				{
					fldType.Text = "DP";
				}
				else if (vbPorterVar == "4")
				{
					fldType.Text = "RT";
				}
				else if (vbPorterVar == "5")
				{
					fldType.Text = "IN";
				}
				else if (vbPorterVar == "6")
				{
					fldType.Text = "OC";
				}
				else if (vbPorterVar == "7")
				{
					fldType.Text = "OD";
				}
				fldCheckDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsCheckInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar1 = rsCheckInfo.Get_Fields_String("Status");
				if (vbPorterVar1 == "1")
				{
					fldCode.Text = "ISSD";
				}
				else if (vbPorterVar1 == "2")
				{
					fldCode.Text = "O/S";
				}
				else if (vbPorterVar1 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar1 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar1 == "D")
				{
					fldCode.Text = "DLTD";
				}
				//FC:FINAL:MSH - Issue #711: can't implicitly convert DateTime to string.
				//fldStatusDate.Text = rsCheckInfo.Get_Fields("StatusDate");
				fldStatusDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy");
				fldPayee.Text = rsCheckInfo.Get_Fields_String("Name");
				intTotalCount += 1;
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += rsCheckInfo.Get_Fields("Amount");
			}
			else
			{
				//FC:FINAL:MSH - Issue #711 - can't implicitly convert int to string.
				//fldCheck.Text = rsInfo.Get_Fields("CheckNumber");
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar2 = rsInfo.Get_Fields("Type");
				if (vbPorterVar2 == "1")
				{
					fldType.Text = "AP";
				}
				else if (vbPorterVar2 == "2")
				{
					fldType.Text = "PY";
				}
				else if (vbPorterVar2 == "3")
				{
					fldType.Text = "DP";
				}
				else if (vbPorterVar2 == "4")
				{
					fldType.Text = "RT";
				}
				else if (vbPorterVar2 == "5")
				{
					fldType.Text = "IN";
				}
				else if (vbPorterVar2 == "6")
				{
					fldType.Text = "OC";
				}
				else if (vbPorterVar2 == "7")
				{
					fldType.Text = "OD";
				}
				fldCheckDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar3 = rsInfo.Get_Fields_String("Status");
				if (vbPorterVar3 == "1")
				{
					fldCode.Text = "ISSD";
				}
				else if (vbPorterVar3 == "2")
				{
					fldCode.Text = "O/S";
				}
				else if (vbPorterVar3 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar3 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar3 == "D")
				{
					fldCode.Text = "DLTD";
				}
				//FC:FINAL:MSH - Issue #711: can't implicitly convert DateTime to string.
				//fldStatusDate.Text = rsInfo.Get_Fields("StatusDate");
				fldStatusDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy");
				fldPayee.Text = rsInfo.Get_Fields_String("Name");
				intTotalCount += 1;
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += rsInfo.Get_Fields("Amount");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotalAmount, "#,##0.00");
			fldCount.Text = intTotalCount.ToString();
			curTotalAmount = 0;
			intTotalCount = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			int intTemp = 0;
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (strTransactions == "C")
			{
				this.Name = "Cashed Item Check Listing";
				lblTitle1.Text = "Cashed Only";
				lblTitle2.Text = "";
			}
			else if (strTransactions == "O")
			{
				this.Name = "Outstanding Item Check Listing";
				lblTitle1.Text = "Outstanding Only";
				lblTitle2.Text = "";
			}
			else if (strTransactions == "M")
			{
				if (frmCheckListSelection.InstancePtr.strTitle.Length > 60)
				{
					intTemp = Strings.InStr(55, frmCheckListSelection.InstancePtr.strTitle, ",", CompareConstants.vbBinaryCompare);
					if (intTemp > 0)
					{
						lblTitle1.Text = Strings.Left(frmCheckListSelection.InstancePtr.strTitle, intTemp);
						lblTitle2.Text = Strings.Right(frmCheckListSelection.InstancePtr.strTitle, frmCheckListSelection.InstancePtr.strTitle.Length - (intTemp + 1));
					}
					else
					{
						lblTitle1.Text = frmCheckListSelection.InstancePtr.strTitle;
						lblTitle2.Text = "";
					}
				}
				else
				{
					lblTitle1.Text = frmCheckListSelection.InstancePtr.strTitle;
					lblTitle2.Text = "";
				}
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void rptCheckFileListByNumber_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCheckFileListByNumber.Caption	= "Check Listing";
			//rptCheckFileListByNumber.Icon	= "rptCheckFileListByNumber.dsx":0000";
			//rptCheckFileListByNumber.Left	= 0;
			//rptCheckFileListByNumber.Top	= 0;
			//rptCheckFileListByNumber.Width	= 11880;
			//rptCheckFileListByNumber.Height	= 8595;
			//rptCheckFileListByNumber.StartUpPosition	= 3;
			//rptCheckFileListByNumber.SectionData	= "rptCheckFileListByNumber.dsx":058A;
			//End Unmaped Properties
		}

		private void rptCheckFileListByNumber_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
