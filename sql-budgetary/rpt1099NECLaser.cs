﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1099Laser.
	/// </summary>
	public partial class rpt1099NECLaser : BaseSectionReport
	{
		public static rpt1099NECLaser InstancePtr
		{
			get
			{
				return (rpt1099NECLaser)Sys.GetInstance(typeof(rpt1099NECLaser));
			}
		}

		protected rpt1099NECLaser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		public Decimal curTotalAmount;
		public int intTotalForms;
        private ITaxFormService taxFormService;
		List<int> vendorNumbers = new List<int>();
        private int vendorCounter = 0;

		public rpt1099NECLaser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "1099 Forms";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
            {
                vendorCounter++;
				eArgs.EOF = vendorCounter >= vendorNumbers.Count;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTemp;
			int lngAdjust;
			lngAdjust = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENT"))));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += (200 * lngAdjust) / 1440F;
			}
			lngAdjust = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENTHORIZONTAL"))));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Left += (140 * lngAdjust) / 1440F;
			}

            taxFormService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxFormService()).Result;

            vendorNumbers = taxFormService.GetEligibleVendorNumbers(TaxFormType.NEC1099, 600).OrderBy(x => x).ToList();

			if (vendorNumbers.Count > 0)
			{
				// do nothing
			}
			else
			{
                rpt1099NECLaser.InstancePtr.Cancel();
			}
			blnFirstRecord = true;
			curTotalAmount = 0;
			intTotalForms = 0;
            vendorCounter = 0;
        }

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			rsDefaults.OpenRecordset("SELECT * FROM [1099FormTotals] WHERE FormName = 'NEC'");
            if (!rsDefaults.EndOfFile() && !rsDefaults.BeginningOfFile())
            {
                rsDefaults.Edit();
			}
            else
            {
                rsDefaults.AddNew();
				rsDefaults.Set_Fields("FormName", "NEC");
            }
			rsDefaults.Set_Fields("TotalAmountReported", curTotalAmount);
			rsDefaults.Set_Fields("TotalNumberOfFormsPrinted", intTotalForms);
			rsDefaults.Update();
			rsVendorInfo.DisposeOf();
			rsDefaults.DisposeOf();
			frm1099Setup.InstancePtr.Unload();
			MessageBox.Show("Total 1099 Amount  -  " + modUtilities.FormatTotal(curTotalAmount), "Total Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Decimal curTotal;

			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + vendorNumbers[vendorCounter]);
			intTotalForms += 1;
			fldPayer.Text = Strings.Trim(frm1099Setup.InstancePtr.txtMunicipality.Text);
			fldAddress1.Text = Strings.Trim(frm1099Setup.InstancePtr.txtAddress1.Text);
			fldAddress2.Text = Strings.Trim(frm1099Setup.InstancePtr.txtAddress2.Text);
			if (Strings.Trim(frm1099Setup.InstancePtr.txtZip4.Text) != "")
			{
				fldAddress3.Text = Strings.Trim(frm1099Setup.InstancePtr.txtCity.Text) + ", " + 
                                   Strings.Trim(frm1099Setup.InstancePtr.txtState.Text) + " " + 
                                   Strings.Trim(frm1099Setup.InstancePtr.txtZip.Text) + "-" + 
                                   Strings.Trim(frm1099Setup.InstancePtr.txtZip4.Text);
			}
			else
			{
				fldAddress3.Text = Strings.Trim(frm1099Setup.InstancePtr.txtCity.Text) + ", " + 
                                   Strings.Trim(frm1099Setup.InstancePtr.txtState.Text) + " " + 
                                   Strings.Trim(frm1099Setup.InstancePtr.txtZip.Text);
			}
			fldPayerFederalID.Text = Strings.Trim(frm1099Setup.InstancePtr.txtFederalID.Text);
			fldTaxID.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("TaxNumber")));
			fldPhone.Text = Strings.Trim(frm1099Setup.InstancePtr.txtPhone.Text);
			if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("1099UseCorrName")))
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))).Length > 30)
				{
					fldVendorName1.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))), 30);
				}
				else
				{
					fldVendorName1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName")));
				}
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 1) == "*")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))).Length + 3 > 30)
					{
						fldVendorName2.Text = "DBA " + Strings.Mid(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 2, 30);
					}
					else
					{
						fldVendorName2.Text = "DBA " + Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))).Length - 1);
					}
				}
				else
				{
					fldVendorName2.Text = "";
				}
				
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 1) == "*")
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress2")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress3")));
				}
				else
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress2")));
				}
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + ", " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))) + "-" + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + ", " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))) + "-" + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))));
					}
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))).Length > 30)
				{
					fldVendorName1.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))), 30);
				}
				else
				{
					fldVendorName1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
				}
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 1) == "*")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))).Length + 3 > 30)
					{
						fldVendorName2.Text = "DBA " + Strings.Mid(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 2, 30);
					}
					else
					{
						fldVendorName2.Text = "DBA " + Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))),
                                                  Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))).Length - 1);
					}
				}
				else
				{
					fldVendorName2.Text = "";
				}
				
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 1) == "*")
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3")));
				}
				else
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")));
				}
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + 
                                                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
			}
			fldVendorNumber.Text = Strings.Format(rsVendorInfo.Get_Fields_Int32("VendorNumber"), "00000");
			fldStateID.Text = Strings.Trim(frm1099Setup.InstancePtr.txtStateId.Text);
			curTotal = CalcNonEmployeeCompensationTotal(vendorNumbers[vendorCounter]);
			if (curTotal != 0)
			{
				fldNonEmployeeCompensation.Text = curTotal.ToString("#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
                fldNonEmployeeCompensation.Text = "";
			}

            curTotal = CalcFederalTaxWithheldTotal(vendorNumbers[vendorCounter]);
            if (curTotal != 0)
            {
                fldFederalTaxWithheld.Text = curTotal.ToString("#,##0.00");
                curTotalAmount += curTotal;
            }
            else
            {
                fldFederalTaxWithheld.Text = "";
            }
		}

		private Decimal CalcNonEmployeeCompensationTotal(int x)
        {
            return taxFormService.GetVendorTaxCategoryTotal(vendorNumbers[vendorCounter], TaxFormType.NEC1099, 1);
        }

        private Decimal CalcFederalTaxWithheldTotal(int x)
        {
            return taxFormService.GetVendorTaxCategoryTotal(vendorNumbers[vendorCounter], TaxFormType.NEC1099, 4);
        }
	}
}
