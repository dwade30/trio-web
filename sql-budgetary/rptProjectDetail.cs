﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptProjectDetail.
	/// </summary>
	public partial class rptProjectDetail : BaseSectionReport
	{
		public static rptProjectDetail InstancePtr
		{
			get
			{
				return (rptProjectDetail)Sys.GetInstance(typeof(rptProjectDetail));
			}
		}

		protected rptProjectDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo.Dispose();
				rsFormat.Dispose();
				rsProjectInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptProjectDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rsProjectInfo = new clsDRWrapper();
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		clsDRWrapper rsFormat = new clsDRWrapper();
		string strAcctType = "";
		string strPeriodCheck;
		// vbPorter upgrade warning: curProjectYTDDebits As Decimal	OnWrite(Decimal, short)
		Decimal curProjectYTDDebits;
		// vbPorter upgrade warning: curProjectYTDCredits As Decimal	OnWrite(Decimal, short)
		Decimal curProjectYTDCredits;
		bool blnShowPending;
		bool blnPending;
		bool blnPendingSummary;

		public rptProjectDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Project Detail";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("ProjectBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsAccountInfo.MoveNext();
				CheckAgain2:
				;
				if (rsAccountInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					IncrementInfoToGet();
					if (rsProjectInfo.EndOfFile() != true)
					{
						RetrieveInfo();
						goto CheckAgain2;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["ProjectBinder"].Value = rsProjectInfo.Get_Fields_String("ProjectCode");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 15100 / 1440F;
			Line1.X2 = this.PrintWidth;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "F")
			{
				lblProjects.Text = "Fund ";
				lblProjects.Text = lblProjects.Text + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp");
			}
			else
			{
				lblProjects.Text = "Project(s) ";
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
				{
					lblProjects.Text = lblProjects + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp");
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
				{
					lblProjects.Text = lblProjects + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp");
				}
				else
				{
					lblProjects.Text = lblProjects + "ALL";
				}
			}
			strPeriodCheck = "AND";
			lblMonths.Text = "Month(s) ";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = lblMonths + "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo False, True, False, "E"
			// CalculateAccountInfo False, True, False, "R"
			// CalculateAccountInfo False, True, False, "G"
			rsFormat.OpenRecordset("SELECT * FROM ProjectDetailFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			if (FCConvert.ToBoolean(rsFormat.Get_Fields_Boolean("PendingDetail")))
			{
				blnShowPending = true;
				blnPendingSummary = false;
			}
			else if (rsFormat.Get_Fields_Boolean("PendingSummary"))
			{
				blnShowPending = true;
				blnPendingSummary = true;
			}
			else
			{
				blnShowPending = false;
				blnPendingSummary = false;
			}
			// If blnPendingSummary Then
			// CalculateAccountInfo True, True, True, "E"
			// CalculateAccountInfo True, True, True, "R"
			// CalculateAccountInfo True, True, True, "G"
			// End If
			blnPending = false;
			FormatFields();
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster ORDER BY ProjectCode");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY ProjectCode");
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "F")
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE Fund = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY ProjectCode");
			}
			else
			{
				rsProjectInfo.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND ProjectCode <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "' ORDER BY ProjectCode");
			}
			if (rsProjectInfo.EndOfFile() != true && rsProjectInfo.BeginningOfFile() != true)
			{
				CheckAgain:
				;
				RetrieveInfo();
				if (rsAccountInfo.EndOfFile() != true)
				{
					// do nothing
				}
				else
				{
					IncrementInfoToGet();
					if (rsProjectInfo.EndOfFile() != true)
					{
						goto CheckAgain;
					}
					else
					{
						MessageBox.Show("No Project Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						this.Cancel();
						return;
					}
				}
			}
			else
			{
				MessageBox.Show("No Project Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void FormatFields()
		{
			SetFieldFont(ref lblPeriod);
			SetFieldFont(ref lblJournal);
			SetFieldFont(ref lblDate);
			SetFieldFont(ref lblDescription);
			SetFieldFont(ref lblRCB);
			SetFieldFont(ref lblType);
			SetFieldFont(ref lblDebits);
			SetFieldFont(ref lblCredits);
			SetFieldFont(ref lblAccount);
			SetFieldFont(ref fldPeriod);
			SetFieldFont(ref fldJournal);
			SetFieldFont(ref fldDate);
			SetFieldFont(ref fldDescription);
			SetFieldFont(ref fldRCB);
			SetFieldFont(ref fldType);
			SetFieldFont(ref fldDebits);
			SetFieldFont(ref fldCredits);
			SetFieldFont(ref fldAccount);
			SetFieldFont(ref lblProjectTotals);
			SetFieldFont(ref fldProjectDebits);
			SetFieldFont(ref fldProjectCredits);
			SetFieldFont(ref lblProjectSummary);
			SetFieldFont(ref lblProjectedCost);
			SetFieldFont(ref lblAmountUsed);
			SetFieldFont(ref lblAvailable);
			SetFieldFont(ref fldProjectedCost);
			SetFieldFont(ref fldAmountUsed);
			SetFieldFont(ref fldAvailable);
		}

		private void SetFieldFont(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				x.Font = new Font(x.Font.Name, 10);
			}
			else if (rsFormat.Get_Fields_String("Font") == "L")
			{
				x.Font = new Font(x.Font.Name, 12);
			}
			else
			{
				FontStyle style = FontStyle.Regular;
				if (rsFormat.Get_Fields_Boolean("Bold"))
					style = FontStyle.Bold;
				if (rsFormat.Get_Fields_Boolean("Italic"))
					style |= FontStyle.Italic;
				if (rsFormat.Get_Fields_Boolean("StrikeThru"))
					style |= FontStyle.Strikeout;
				if (rsFormat.Get_Fields_Boolean("Underline"))
					style |= FontStyle.Underline;
				// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
				x.Font = new Font(rsFormat.Get_Fields_String("FontName"), FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
			}
		}

		private void SetFieldFont(ref GrapeCity.ActiveReports.SectionReportModel.Label x)
		{
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				x.Font = new Font(x.Font.Name, 10);
			}
			else if (rsFormat.Get_Fields_String("Font") == "L")
			{
				x.Font = new Font(x.Font.Name, 12);
			}
			else
			{
				FontStyle style = FontStyle.Regular;
				if (rsFormat.Get_Fields_Boolean("Bold"))
					style = FontStyle.Bold;
				if (rsFormat.Get_Fields_Boolean("Italic"))
					style |= FontStyle.Italic;
				if (rsFormat.Get_Fields_Boolean("StrikeThru"))
					style |= FontStyle.Strikeout;
				if (rsFormat.Get_Fields_Boolean("Underline"))
					style |= FontStyle.Underline;
				// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
				x.Font = new Font(rsFormat.Get_Fields_String("FontName"), FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
			}
		}

		private void IncrementInfoToGet()
		{
			if (blnPendingSummary && !blnPending)
			{
				blnPending = true;
			}
			else
			{
				blnPending = false;
				rsProjectInfo.MoveNext();
			}
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strSelection;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			strSelection = "Account, Period, Status, TransDate, RCB, JournalNumber, Description, Amount, Discount, Encumbrance, Adjustments, Liquidated, Type, OrderMonth, Project";
			if (!blnPending)
			{
				if (blnShowPending && !blnPendingSummary)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM (SELECT " + strSelection +
                                                " FROM ExpenseDetailInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                                                ")) UNION ALL SELECT " + strSelection +
                                                " FROM RevenueDetailInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + " " + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                                                ")) UNION ALL SELECT " + strSelection +
                                                " FROM LedgerDetailInfo WHERE Project = '" +
                                                rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                                                "))) ORDER BY OrderMonth, TransDate, Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT " + strSelection +
                                                " FROM ExpenseDetailInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND Status = 'P' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) " +
                                                "UNION ALL SELECT " + strSelection + 
                                                " FROM RevenueDetailInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND Status = 'P' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + " " + strPeriodCheck + " Period <= " +
                                                FCConvert.ToString(HighDate) + ")) UNION ALL SELECT " + strSelection +
                                                " FROM LedgerDetailInfo WHERE Project = '" + rsProjectInfo.Get_Fields_String("ProjectCode") +
                                                "' AND Status = 'P' AND (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                                                ")) ORDER BY OrderMonth, TransDate, Account");
				}
			}
			else
			{
				rsAccountInfo.OpenRecordset(
                    "SELECT SUM(PendingDebitsTotal) as ProjectDebitsTotal, SUM(PendingCreditsTotal) as ProjectCreditsTotal FROM (SELECT SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Project = '" +
                    rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " +
                    FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                    ")) UNION ALL SELECT SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Project = '" +
                    rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " +
                    FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                    ")) UNION ALL SELECT SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Project = '" +
                    rsProjectInfo.Get_Fields_String("ProjectCode") + "' AND (Period = 0 OR (Period >= " +
                    FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) +
                    ")))");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnPending)
			{
				fldPeriod.Text = "";
				fldJournal.Text = "";
				fldDate.Text = "";
				fldAccount.Text = "";
				fldDescription.Text = "Pending Acticity";
				fldRCB.Text = "";
				fldType.Text = "";
				// TODO Get_Fields: Field [ProjectDebitsTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [ProjectCreditsTotal] not found!! (maybe it is an alias?)
				if (rsAccountInfo.Get_Fields("ProjectDebitsTotal") - rsAccountInfo.Get_Fields("ProjectCreditsTotal") > 0)
				{
					fldDebits.Text = "0.00";
					// TODO Get_Fields: Field [ProjectDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ProjectCreditsTotal] not found!! (maybe it is an alias?)
					fldCredits.Text = Strings.Format((rsAccountInfo.Get_Fields("ProjectDebitsTotal") - rsAccountInfo.Get_Fields("ProjectCreditsTotal")) * -1, "#,##0.00");
					// TODO Get_Fields: Field [ProjectDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ProjectCreditsTotal] not found!! (maybe it is an alias?)
					curProjectYTDCredits += ((rsAccountInfo.Get_Fields("ProjectDebitsTotal") - rsAccountInfo.Get_Fields("ProjectCreditsTotal")) * -1);
				}
				else
				{
					fldCredits.Text = "0.00";
					// TODO Get_Fields: Field [ProjectDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ProjectCreditsTotal] not found!! (maybe it is an alias?)
					fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("ProjectDebitsTotal") - rsAccountInfo.Get_Fields("ProjectCreditsTotal"), "#,##0.00");
					// TODO Get_Fields: Field [ProjectDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [ProjectCreditsTotal] not found!! (maybe it is an alias?)
					curProjectYTDDebits += (rsAccountInfo.Get_Fields("ProjectDebitsTotal") - rsAccountInfo.Get_Fields("ProjectCreditsTotal"));
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldPeriod.Text = Strings.Format(rsAccountInfo.Get_Fields("Period"), "00");
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				fldJournal.Text = Strings.Format(rsAccountInfo.Get_Fields("JournalNumber"), "0000");
				fldDate.Text = Strings.Format(rsAccountInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = rsAccountInfo.Get_Fields_String("Account");
				fldDescription.Text = Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Description")));
				fldRCB.Text = rsAccountInfo.Get_Fields_String("RCB");
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsAccountInfo.Get_Fields("Type")) == "A")
				{
					fldType.Text = "AP";
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsAccountInfo.Get_Fields("Type") == "E")
				{
					fldType.Text = "EN";
				}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsAccountInfo.Get_Fields("Type")) == "C" || FCConvert.ToString(rsAccountInfo.Get_Fields("Type")) == "W")
				{
					fldType.Text = "CR";
				}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsAccountInfo.Get_Fields("Type") == "D")
				{
					fldType.Text = "CD";
				}
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							else if (rsAccountInfo.Get_Fields("Type") == "G")
				{
					fldType.Text = "GJ";
				}
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								else if (rsAccountInfo.Get_Fields("Type") == "P")
				{
					fldType.Text = "PY";
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsAccountInfo.Get_Fields("Type")) == "A")
				{
					// if the amount is more than 0 and the journal entry is an AP Journal Entry it is a Debit
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					if (rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						fldDebits.Text = Strings.Format((rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance");
						// if the amount is less than 0 and the journal entry is an AP Correction Entry it is a Debit
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					else if (rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance") < 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) == "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						fldDebits.Text = Strings.Format((rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						curProjectYTDDebits += ((rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance")));
						// if the amount is more than 0 and the journal entry is an AP Correction Entry it is a Credit
					}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						else if (rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance") > 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) == "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						fldCredits.Text = Strings.Format((rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00");
						fldDebits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						curProjectYTDCredits += (rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance"));
						// if the amount is less than 0 and the journal entry is an AP Journal Entry it is a Credit
					}
					else
					{
						// if the encumbrance is greater than the amount it is a negative debit
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (rsAccountInfo.Get_Fields_Decimal("Encumbrance") != 0 && rsAccountInfo.Get_Fields_Decimal("Encumbrance") > rsAccountInfo.Get_Fields("Amount"))
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
							fldCredits.Text = Strings.Format(0, "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance");
						}
						else
						{
							// else the entry is a credit
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldCredits.Text = Strings.Format((rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00");
							fldDebits.Text = Strings.Format(0, "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							curProjectYTDCredits += rsAccountInfo.Get_Fields("Amount") - rsAccountInfo.Get_Fields("Discount") - rsAccountInfo.Get_Fields_Decimal("Encumbrance");
						}
					}
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsAccountInfo.Get_Fields("Type") == "E")
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Amount")) > 0)
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount"), "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldCredits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount") * -1, "#,##0.00");
						fldDebits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curProjectYTDCredits += rsAccountInfo.Get_Fields("Amount");
					}
				}
				else
				{
					// if the amount is more than 0 and an RCB of anything but c it is a debit
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount"), "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount");
						// if the amount is less than 0 and an RCB of C then it is a debit
					}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						else if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) == "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount"), "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount");
						// if the amount is more than 0 and an RCB of C than it is a Credit
					}
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							else if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Amount")) > 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) == "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldCredits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount") * -1, "#,##0.00");
						fldDebits.Text = Strings.Format(0, "#,##0.00");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curProjectYTDCredits += rsAccountInfo.Get_Fields("Amount");
						// if the amount is less than 0 and it is a general journal entry with an RCB of anything but C then it is a Credit
					}
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								else if (FCConvert.ToInt32(rsAccountInfo.Get_Fields("Amount")) < 0 && FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) != "C")
					{
						if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("RCB")) == "E")
						{
							fldCredits.Text = Strings.Format(0, "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							fldDebits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount"), "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curProjectYTDDebits += rsAccountInfo.Get_Fields("Amount");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							fldCredits.Text = Strings.Format(rsAccountInfo.Get_Fields("Amount") * -1, "#,##0.00");
							fldDebits.Text = Strings.Format(0, "#,##0.00");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curProjectYTDCredits += rsAccountInfo.Get_Fields("Amount");
						}
					}
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsCost = new clsDRWrapper())
            {
                fldProjectDebits.Text = Strings.Format(curProjectYTDDebits, "#,##0.00");
                fldProjectCredits.Text = Strings.Format(curProjectYTDCredits * -1, "#,##0.00");
                rsCost.OpenRecordset("SELECT * FROM ProjectMaster WHERE ProjectCode = '" +
                                     Strings.Left(fldProjectTitle.Text, 4) + "'");
                if (rsCost.EndOfFile() != true && rsCost.BeginningOfFile() != true)
                {
                    fldProjectedCost.Text = Strings.Format(rsCost.Get_Fields_Decimal("ProjectedCost"), "#,##0.00");
                }
                else
                {
                    fldProjectedCost.Text = Strings.Format(0, "#,##0.00");
                }

                fldAmountUsed.Text = Strings.Format(curProjectYTDDebits + curProjectYTDCredits, "#,##0.00");
                fldAvailable.Text =
                    Strings.Format(FCConvert.ToDecimal(fldProjectedCost.Text) - FCConvert.ToDecimal(fldAmountUsed.Text),
                        "#,##0.00");
                curProjectYTDCredits = 0;
                curProjectYTDDebits = 0;
            }
        }

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldProjectTitle.Text = rsProjectInfo.Get_Fields_String("ProjectCode") + " - " + rsProjectInfo.Get_Fields_String("LongDescription");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		

		private void rptProjectDetail_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
