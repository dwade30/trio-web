﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCustomBudgetRevenueReport.
	/// </summary>
	public partial class rptCustomBudgetRevenueReport : BaseSectionReport
	{
		public static rptCustomBudgetRevenueReport InstancePtr
		{
			get
			{
				return (rptCustomBudgetRevenueReport)Sys.GetInstance(typeof(rptCustomBudgetRevenueReport));
			}
		}

		protected rptCustomBudgetRevenueReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                rsDepartmentInfo.Dispose();
				rsDivisionInfo.Dispose();
				rsAccountInfo.Dispose();
				rsRevenueInfo.Dispose();
				rsBudgetInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomBudgetRevenueReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool DeptBreakFlag;
		bool DivBreakFlag;
		string strComments = "";
		bool blnDepartmentTotals;
		bool blnDivisionTotals;
		bool blnExpenseTotals;
		string strAccountType = "";
		string strSingleDepartment = "";
		string strSingleDivision = "";
		string strLowDepartment = "";
		string strHighDepartment = "";
		clsDRWrapper rsDepartmentInfo = new clsDRWrapper();
		clsDRWrapper rsDivisionInfo = new clsDRWrapper();
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		clsDRWrapper rsRevenueInfo = new clsDRWrapper();
		clsDRWrapper rsBudgetInfo = new clsDRWrapper();
		//clsDRWrapper rsBudgetAccounts = new clsDRWrapper();
		bool blnDeptChange;
		bool blnFirstRecord;
		// vbPorter upgrade warning: curDivisionTotal As Decimal	OnWrite(short, Decimal)
		Decimal[] curDivisionTotal = new Decimal[8 + 1];
		// vbPorter upgrade warning: curDepartmentTotal As Decimal	OnWrite(short, Decimal)
		Decimal[] curDepartmentTotal = new Decimal[8 + 1];
		Decimal[] curExpenseTotal = new Decimal[8 + 1];
		Decimal[] curTotal = new Decimal[8 + 1];
		int[] intType = new int[8 + 1];
		// vbPorter upgrade warning: curOldDivisionTotal As Decimal	OnWrite(short, Decimal)
		Decimal[] curOldDivisionTotal = new Decimal[8 + 1];
		// vbPorter upgrade warning: curOldDepartmentTotal As Decimal	OnWrite(short, Decimal)
		Decimal[] curOldDepartmentTotal = new Decimal[8 + 1];
		Decimal[] curOldExpenseTotal = new Decimal[8 + 1];
		Decimal[] curOldTotal = new Decimal[8 + 1];
		string strExpFooterTitle = "";
		string strDivFooterTitle = "";
		string strDeptFooterTitle = "";
		bool blnCurrentYearBudget;
		int lngCurrent;
		int lngBudget;
		string strReport = "";
		int intNumberOfFields;
		string strHeaderTitle = "";
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		int PageCounter;
		string strAccounts;
		string strInfoType = "";
		string strReportTitle = "";
		bool blnIncBudAdj;
		bool blnIncCarryForwards;
		int intCurrentDetailField;
		string strExpLowLevel;
		string strRevLowLevel = "";

		public rptCustomBudgetRevenueReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Budget Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("GroupTitle");
			this.Fields.Add("DeptGroupFooter");
			this.Fields.Add("DivGroupFooter");
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// VB6 Bad Scope Dim:
			bool blnChangeDeptDiv = false;
			string strTempAccount = "";
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (rsDepartmentInfo.EndOfFile() && rsDivisionInfo.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
			}
			else
			{
				if (rsDepartmentInfo.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
			}
			if (blnFirstRecord)
			{
				CheckDivInfo:
				;
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2))) + "' ORDER BY Division");
					if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
					{
						// do nothing
					}
					else
					{
						rsDepartmentInfo.MoveNext();
						if (rsDepartmentInfo.EndOfFile() != true)
						{
							goto CheckDivInfo;
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				CheckAccountInfo:
				;
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND SecondAccountField = '" + rsDivisionInfo.Get_Fields_String("Division") + "' ORDER BY ThirdAccountField");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' ORDER BY SecondAccountField, ThirdAccountField");
				}
				blnFirstRecord = false;
				CheckAccountAgain2:
				;
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
					strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
					if (BudgetAccountExists(strTempAccount))
					{
						// do nothing
					}
					else
					{
						rsAccountInfo.MoveNext();
						goto CheckAccountAgain2;
					}
				}
				else
				{
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rsDivisionInfo.MoveNext();
						if (rsDivisionInfo.EndOfFile() != true)
						{
							goto CheckAccountInfo;
						}
						else
						{
							rsDepartmentInfo.MoveNext();
							if (rsDepartmentInfo.EndOfFile() != true)
							{
								goto CheckDivInfo;
							}
							else
							{
								eArgs.EOF = true;
								return;
							}
						}
					}
					else
					{
						rsDepartmentInfo.MoveNext();
						if (rsDepartmentInfo.EndOfFile() != true)
						{
							goto CheckDivInfo;
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				blnChangeDeptDiv = false;
				//FC:FINAL:BBE:#i597 - initialize array with empty string when null
				if (this.Fields["GroupTitle"].Value == null)
				{
					this.Fields["GroupTitle"].Value = string.Empty;
				}
				if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
				{
					if (Strings.Left(this.Fields["GroupTitle"].Value.ToString(), ("Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription")).Length) == "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription"))
					{
						blnChangeDeptDiv = false;
					}
					else
					{
						blnChangeDeptDiv = true;
					}
				}
				else
				{
					if (Strings.Left(this.Fields["GroupTitle"].Value.ToString(), ("Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription")).Length) == "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription"))
					{
						blnChangeDeptDiv = false;
					}
					else
					{
						blnChangeDeptDiv = true;
					}
				}
				if (blnChangeDeptDiv)
				{
					if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
					{
						this.Fields["GroupTitle"].Value = "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription");
					}
					else
					{
						this.Fields["GroupTitle"].Value = "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription");
					}
				}
				else
				{
					if (Strings.Right(this.Fields["GroupTitle"].Value.ToString(), 6) != "CONT'D")
					{
						this.Fields["GroupTitle"].Value = this.Fields["GroupTitle"].Value + " CONT'D";
					}
					if (Strings.Right(fldDeptDivTitle.Text, 6) != "CONT'D")
					{
						fldDeptDivTitle.Text = fldDeptDivTitle.Text + " CONT'D";
					}
				}
				// set title for group footer
				this.Fields["DeptGroupFooter"].Value = rsDepartmentInfo.Get_Fields_String("LongDescription");
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					if (strRevLowLevel != "Di")
					{
						this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("LongDescription");
					}
					else
					{
						this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("Division") + " - " + rsDivisionInfo.Get_Fields_String("LongDescription");
					}
				}
				else
				{
					this.Fields["DivGroupFooter"].Value = "";
				}
				if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
				{
					this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department") + rsDivisionInfo.Get_Fields_String("Division");
				}
				else
				{
					this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
				}
				eArgs.EOF = false;
				return;
			}
			rsAccountInfo.MoveNext();
			CheckAccountAgain3:
			;
			if (rsAccountInfo.EndOfFile() != true)
			{
				strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
				strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
				if (BudgetAccountExists(strTempAccount))
				{
					// do nothing
				}
				else
				{
					rsAccountInfo.MoveNext();
					goto CheckAccountAgain3;
				}
			}
			else
			{
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					CheckNewDiv:
					;
					rsDivisionInfo.MoveNext();
					CheckNewDept:
					;
					if (rsDivisionInfo.EndOfFile() != true)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND SecondAccountField = '" + rsDivisionInfo.Get_Fields_String("Division") + "' ORDER BY ThirdAccountField");
						CheckAccountAgain4:
						;
						if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								// do nothing
							}
							else
							{
								rsAccountInfo.MoveNext();
								goto CheckAccountAgain4;
							}
						}
						else
						{
							goto CheckNewDiv;
						}
					}
					else
					{
						rsDepartmentInfo.MoveNext();
						blnDeptChange = true;
						if (rsDepartmentInfo.EndOfFile() != true)
						{
							rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2))) + "' ORDER BY Division");
							goto CheckNewDept;
						}
						else
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				else
				{
					CheckNewDept2:
					;
					rsDepartmentInfo.MoveNext();
					blnDeptChange = true;
					if (rsDepartmentInfo.EndOfFile() != true)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' ORDER BY SecondAccountField");
						CheckAccountAgain5:
						;
						if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
						{
							strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
							strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
							if (BudgetAccountExists(strTempAccount))
							{
								// do nothing
							}
							else
							{
								rsAccountInfo.MoveNext();
								goto CheckAccountAgain5;
							}
						}
						else
						{
							goto CheckNewDept2;
						}
					}
					else
					{
						eArgs.EOF = true;
						return;
					}
				}
			}
			blnChangeDeptDiv = false;
			if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
			{
				if (Strings.Left(this.Fields["GroupTitle"].Value.ToString(), ("Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription")).Length) == "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription"))
				{
					blnChangeDeptDiv = false;
				}
				else
				{
					blnChangeDeptDiv = true;
				}
			}
			else
			{
				if (Strings.Left(this.Fields["GroupTitle"].Value.ToString(), ("Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription")).Length) == "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription"))
				{
					blnChangeDeptDiv = false;
				}
				else
				{
					blnChangeDeptDiv = true;
				}
			}
			if (blnChangeDeptDiv)
			{
				if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
				{
					this.Fields["GroupTitle"].Value = "Dept/Div:  " + rsDepartmentInfo.Get_Fields_String("Department") + "-" + rsDivisionInfo.Get_Fields_String("Division") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription") + " / " + rsDivisionInfo.Get_Fields_String("LongDescription");
				}
				else
				{
					this.Fields["GroupTitle"].Value = "Dept:  " + rsDepartmentInfo.Get_Fields_String("Department") + "  " + rsDepartmentInfo.Get_Fields_String("LongDescription");
				}
			}
			else
			{
				if (Strings.Right(this.Fields["GroupTitle"].Value.ToString(), 6) != "CONT'D")
				{
					this.Fields["GroupTitle"].Value = this.Fields["GroupTitle"].Value + " CONT'D";
				}
				if (Strings.Right(fldDeptDivTitle.Text, 6) != "CONT'D")
				{
					fldDeptDivTitle.Text = fldDeptDivTitle.Text + " CONT'D";
				}
			}
			// set title for group footer
			this.Fields["DeptGroupFooter"].Value = rsDepartmentInfo.Get_Fields_String("LongDescription");
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				if (strRevLowLevel != "Di")
				{
					this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("LongDescription");
				}
				else
				{
					this.Fields["DivGroupFooter"].Value = rsDivisionInfo.Get_Fields_String("Division") + " - " + rsDivisionInfo.Get_Fields_String("LongDescription");
				}
			}
			else
			{
				this.Fields["DivGroupFooter"].Value = "";
			}
			if (!modAccountTitle.Statics.RevDivFlag && strRevLowLevel == "Re")
			{
				this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department") + rsDivisionInfo.Get_Fields_String("Division");
			}
			else
			{
				this.Fields["Binder"].Value = rsDepartmentInfo.Get_Fields_String("Department");
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//DDActiveReports2.Canvas pg = new DDActiveReports2.Canvas();
			int intPageIndex;
			// If strAccounts = "B" Then
			// intPageIndex = 0
			// For Each pg In rptCustomBudgetExpenseReport.Pages
			// Me.Pages.Insert intPageIndex, pg
			// intPageIndex = intPageIndex + 1
			// Next pg
			// Me.Pages.Commit
			// Me.Refresh
			// DoEvents
			// Unload rptCustomBudgetExpenseReport
			// End If
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			int counter2;
			int fldCounter;
			// set size of report to show on screen
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (Strings.Trim(strReportTitle) != "")
			{
				Label1.Text = strReportTitle;
			}
			// initialize the flex grid that holds the column titles and show titles based on what the user selected in the report setup screen
			// vsHeadings.Cols = intNumberOfFields
			// vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 2, vsHeadings.Cols - 1) = flexAlignRightCenter
			fldCounter = 0;
			Line2.X1 = 0;
			for (counter = 0; counter <= 44; counter++)
			{
				if (modBudgetaryMaster.Statics.blnFields[counter] == true)
				{
					CreateTitle(ref fldCounter, ref counter);
					fldCounter += 1;
				}
			}
			// if there are more than 5 fields selected make the report print landscape
			if (intNumberOfFields > 5 || (intNumberOfFields > 2 && strComments == "S") || frmCustomBudgetReport.InstancePtr.cmbPortrait.SelectedIndex == 1)
			{
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 14608 / 1440f;
				// set the page header labels position if the report is changed to print landscape
				Label1.Left = 0;
				Label1.Width = this.PrintWidth;
				Label14.Left = 0;
				Label14.Width = this.PrintWidth;
				Label3.Left = this.PrintWidth - Label3.Width;
				Label4.Left = this.PrintWidth - Label4.Width;
			}
			else
			{
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
			}
			frmCustomBudgetReport.InstancePtr.Unload();
			// make sure the report is unloaded before running
			Line2.X2 = this.PrintWidth;
			// set the fields up in the detail section where they should be
			FormatFields();
			// set the width of the grid based on how many items were selected to be printed
			lblTitle1Line1.Left = fldDetail1.Left;
			lblTitle1Line2.Left = fldDetail1.Left;
			lblTitle1Line3.Left = fldDetail1.Left;
			lblTitle1Line1.Width = fldDetail1.Width;
			lblTitle1Line2.Width = fldDetail1.Width;
			lblTitle1Line3.Width = fldDetail1.Width;
			for (counter = 1; counter <= intNumberOfFields; counter++)
			{
				switch (counter)
				{
					case 1:
						{
							//vsHeadings.ColWidth(counter, (fldDetail2.Left + fldDetail2.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle2Line1.Left = fldDetail2.Left;
							lblTitle2Line2.Left = fldDetail2.Left;
							lblTitle2Line3.Left = fldDetail2.Left;
							lblTitle2Line1.Width = fldDetail2.Width;
							lblTitle2Line2.Width = fldDetail2.Width;
							lblTitle2Line3.Width = fldDetail2.Width;
							break;
						}
					case 2:
						{
							//vsHeadings.ColWidth(counter, (fldDetail3.Left + fldDetail3.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle3Line1.Left = fldDetail3.Left;
							lblTitle3Line2.Left = fldDetail3.Left;
							lblTitle3Line3.Left = fldDetail3.Left;
							lblTitle3Line1.Width = fldDetail3.Width;
							lblTitle3Line2.Width = fldDetail3.Width;
							lblTitle3Line3.Width = fldDetail3.Width;
							break;
						}
					case 3:
						{
							//vsHeadings.ColWidth(counter, (fldDetail4.Left + fldDetail4.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle4Line1.Left = fldDetail4.Left;
							lblTitle4Line2.Left = fldDetail4.Left;
							lblTitle4Line3.Left = fldDetail4.Left;
							lblTitle4Line1.Width = fldDetail4.Width;
							lblTitle4Line2.Width = fldDetail4.Width;
							lblTitle4Line3.Width = fldDetail4.Width;
							break;
						}
					case 4:
						{
							//vsHeadings.ColWidth(counter, (fldDetail5.Left + fldDetail5.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle5Line1.Left = fldDetail5.Left;
							lblTitle5Line2.Left = fldDetail5.Left;
							lblTitle5Line3.Left = fldDetail5.Left;
							lblTitle5Line1.Width = fldDetail5.Width;
							lblTitle5Line2.Width = fldDetail5.Width;
							lblTitle5Line3.Width = fldDetail5.Width;
							break;
						}
					case 5:
						{
							//vsHeadings.ColWidth(counter, (fldDetail6.Left + fldDetail6.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle6Line1.Left = fldDetail6.Left;
							lblTitle6Line2.Left = fldDetail6.Left;
							lblTitle6Line3.Left = fldDetail6.Left;
							lblTitle6Line1.Width = fldDetail6.Width;
							lblTitle6Line2.Width = fldDetail6.Width;
							lblTitle6Line3.Width = fldDetail6.Width;
							break;
						}
					case 6:
						{
							//vsHeadings.ColWidth(counter, (fldDetail7.Left + fldDetail7.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle7Line1.Left = fldDetail7.Left;
							lblTitle7Line2.Left = fldDetail7.Left;
							lblTitle7Line3.Left = fldDetail7.Left;
							lblTitle7Line1.Width = fldDetail7.Width;
							lblTitle7Line2.Width = fldDetail7.Width;
							lblTitle7Line3.Width = fldDetail7.Width;
							break;
						}
					case 7:
						{
							//vsHeadings.ColWidth(counter, (fldDetail8.Left + fldDetail8.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle8Line1.Left = fldDetail8.Left;
							lblTitle8Line2.Left = fldDetail8.Left;
							lblTitle8Line3.Left = fldDetail8.Left;
							lblTitle8Line1.Width = fldDetail8.Width;
							lblTitle8Line2.Width = fldDetail8.Width;
							lblTitle8Line3.Width = fldDetail8.Width;
							break;
						}
					case 8:
						{
							//vsHeadings.ColWidth(counter, (fldDetail9.Left + fldDetail9.Width) - (vsHeadings.Left + vsHeadings.Cell(FCGrid.CellPropertySettings.flexcpLeft, 0, counter - 1) + vsHeadings.ColWidth(counter - 1)));
							lblTitle9Line1.Left = fldDetail9.Left;
							lblTitle9Line2.Left = fldDetail9.Left;
							lblTitle9Line3.Left = fldDetail9.Left;
							lblTitle9Line1.Width = fldDetail9.Width;
							lblTitle9Line2.Width = fldDetail9.Width;
							lblTitle9Line3.Width = fldDetail9.Width;
							break;
						}
				}
				//end switch
			}
			if (strRevLowLevel != "Re")
			{
				this.Detail.Height = 0;
				this.Detail.Visible = false;
			}
			// retrieve summary informaiton so we can get at it easily
			// troges126
			// CalculateAccountInfo True, True, False, "R"
			modBudgetaryAccounting.CalculateAccountInfo();
			RetrieveInfo();
			//Document.Printer.RenderMode = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTempAccount;
			strTempAccount = modBudgetaryMaster.CreateAccount_242("R", rsAccountInfo.Get_Fields_String("FirstAccountField"), rsAccountInfo.Get_Fields_String("SecondAccountField"), rsAccountInfo.Get_Fields_String("ThirdAccountField"), rsAccountInfo.Get_Fields_String("FourthAccountField"));
			strTempAccount = Strings.Left(strTempAccount, strTempAccount.Length - 1);
			rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strTempAccount + "'");
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Revenue = '" + rsAccountInfo.Get_Fields_String("SecondAccountField") + "'");
				if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("SecondAccountField") + " " + rsRevenueInfo.Get_Fields_String("LongDescription");
					FillDetailFields();
				}
				else
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("SecondAccountField") + " UNKNOWN";
					FillDetailFields();
				}
			}
			else
			{
				rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND Revenue = '" + rsAccountInfo.Get_Fields_String("ThirdAccountField") + "'");
				if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("ThirdAccountField") + " " + rsRevenueInfo.Get_Fields_String("LongDescription");
					FillDetailFields();
				}
				else
				{
					fldAccount.Text = rsAccountInfo.Get_Fields_String("ThirdAccountField") + " UNKNOWN";
					FillDetailFields();
				}
			}
			if (strComments == "S" || strComments == "B" || strComments == "P")
			{
				if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
				{
					fldComments.Text = rsBudgetInfo.Get_Fields_String("Comments");
				}
				else
				{
					fldComments.Text = "";
				}
			}
			else
			{
				fldComments.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			int intCounter;
			if (modAccountTitle.Statics.RevDivFlag)
			{
				if (blnDepartmentTotals)
				{
					fldDepartmentName.Visible = true;
					intCounter = 0;
					FillDeptTotals();
				}
				else
				{
					fldDepartmentName.Visible = false;
					ClearDeptTotals();
				}
				fldDivisionName.Text = "";
				ClearDivTotals();
				for (intCounter = 0; intCounter <= 8; intCounter++)
				{
					curDepartmentTotal[intCounter] = 0;
					curDivisionTotal[intCounter] = 0;
					curOldDepartmentTotal[intCounter] = 0;
					curOldDivisionTotal[intCounter] = 0;
				}
				if (DeptBreakFlag)
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				else
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
			}
			else
			{
				if (blnDeptChange)
				{
					blnDeptChange = false;
					if (blnDepartmentTotals)
					{
						fldDepartmentName.Visible = true;
						FillDeptTotals();
					}
					else
					{
						fldDepartmentName.Visible = false;
						ClearDeptTotals();
					}
					if (blnDivisionTotals)
					{
						fldDivisionName.Visible = true;
						FillDivTotals();
					}
					else
					{
						fldDivisionName.Visible = false;
						ClearDivTotals();
					}
					for (intCounter = 0; intCounter <= 8; intCounter++)
					{
						curDepartmentTotal[intCounter] = 0;
						curDivisionTotal[intCounter] = 0;
						curOldDepartmentTotal[intCounter] = 0;
						curOldDivisionTotal[intCounter] = 0;
					}
					if (DeptBreakFlag || DivBreakFlag)
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
					}
					else
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
					}
				}
				else
				{
					fldDepartmentName.Visible = false;
					ClearDeptTotals();
					if (blnDivisionTotals)
					{
						fldDivisionName.Visible = true;
						FillDivTotals();
					}
					else
					{
						fldDivisionName.Visible = false;
						ClearDivTotals();
					}
					for (intCounter = 0; intCounter <= 8; intCounter++)
					{
						curDivisionTotal[intCounter] = 0;
						curOldDivisionTotal[intCounter] = 0;
					}
					if (DivBreakFlag)
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
					}
					else
					{
						GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
					}
				}
			}
			if (rsDepartmentInfo.EndOfFile() != true)
			{
				if (rsDepartmentInfo.Get_Fields_String("LongDescription") == fldDepartmentName.Text)
				{
					if (modAccountTitle.Statics.RevDivFlag)
					{
						rsDepartmentInfo.MoveNext();
						if (rsDepartmentInfo.EndOfFile() != true)
						{
							Line1.Visible = false;
							fldTotalName.Text = "";
							ClearTotals();
						}
						else
						{
							Line1.Visible = true;
							fldTotalName.Text = "Revenue Totals:";
							FillTotals();
						}
						rsDepartmentInfo.MovePrevious();
					}
					else
					{
						rsDepartmentInfo.MoveNext();
						if (rsDepartmentInfo.EndOfFile() != true || rsDivisionInfo.EndOfFile() != true)
						{
							Line1.Visible = false;
							fldTotalName.Text = "";
							ClearTotals();
						}
						else
						{
							Line1.Visible = true;
							fldTotalName.Text = "Revenue Totals:";
							FillTotals();
						}
						rsDepartmentInfo.MovePrevious();
					}
				}
				else
				{
					Line1.Visible = false;
					fldTotalName.Text = "";
					ClearTotals();
				}
			}
			else
			{
				Line1.Visible = true;
				fldTotalName.Text = "Revenue Totals:";
				FillTotals();
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			FillTotals();
		}
		// vbPorter upgrade warning: lngBudgetYear As int	OnWrite(string, int)
		// vbPorter upgrade warning: lngCurrentYear As int	OnWrite(string, int)
		// vbPorter upgrade warning: intPageToStartOn As short	OnWrite(string, int)
		public void Init(bool blnIncludeBudAdj, string strTitle, int lngBudgetYear, int lngCurrentYear, string strReportType, string strCom, bool blnDeptTotal, bool blnDivTotal, bool blnExpTotal, bool blnDeptBreak, bool blnDivBreak, string strInfo, int intPageToStartOn = 1, bool blnIncludeCarryForwards = true, string strExpLowestLevel = "Ob", string strRevLowestLevel = "Re", string strFirstDept = "", string strSecondDept = "", string strDiv = "")
		{
			int counter;
			// Set Initial Variables
			strReportTitle = strTitle;
			lngCurrent = lngCurrentYear;
			// Fiscal year you are currently in
			lngBudget = lngBudgetYear;
			// Year you are budgeting for
			strAccounts = strReportType;
			// Are we showing only expense accounts, reveneue accounts, or both
			strComments = strCom;
			// do we want to show comments
			blnDepartmentTotals = blnDeptTotal;
			// display department totals
			blnDivisionTotals = blnDivTotal;
			// display division totals
			blnExpenseTotals = blnExpTotal;
			// display expense totals
			DeptBreakFlag = blnDeptBreak;
			// page break on new department
			DivBreakFlag = blnDivBreak;
			// page break on new division
			PageCounter = intPageToStartOn - 1;
			blnIncBudAdj = blnIncludeBudAdj;
			blnIncCarryForwards = blnIncludeCarryForwards;
			strExpLowLevel = strExpLowestLevel;
			strRevLowLevel = strRevLowestLevel;
			if (strRevLowLevel == "De")
			{
				blnDivisionTotals = false;
				blnExpenseTotals = false;
				DivBreakFlag = false;
			}
			else if (strRevLowLevel == "Di")
			{
				blnExpenseTotals = false;
			}
			if (strInfo == "A")
			{
				// if printing all departments
				strInfoType = "A";
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else if (strInfo == "R")
			{
				// if printing for a range of departments
				strInfoType = "R";
				strLowDepartment = strFirstDept;
				strHighDepartment = strSecondDept;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department >= '" + strLowDepartment + "' AND Department <= '" + strHighDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else if (strInfo == "D")
			{
				// if printing for a single department
				strInfoType = "D";
				strSingleDepartment = strFirstDept;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strSingleDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				strInfoType = "F";
				strSingleDepartment = strFirstDept;
				rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Fund = '" + strSingleDepartment + "' AND Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			rsDepartmentInfo.MoveLast();
			rsDepartmentInfo.MoveFirst();
			blnFirstRecord = true;
			intNumberOfFields = 0;
			// initialize number of fields to be shown
			for (counter = 0; counter <= 44; counter++)
			{
				// save the exact information they want reported
				modBudgetaryMaster.Statics.blnFields[counter] = false;
			}
			GetFieldsForReport();
			// set up fields depending on what data they want reported
			//Application.DoEvents();
			if (strAccounts == "B")
			{
				this.Run(false);
			}
			else
			{
				if (frmCustomBudgetReport.InstancePtr.blnPrint)
				{
					// either print or show the report based on the user choice
					this.Document.Print(false);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(this);
				}
			}
		}

		private void FillDeptTotals()
		{
			int intCounter;
			// fill in department total fields with information
			intCounter = 0;
			if (intNumberOfFields > 0)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal1.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal1.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal1.Text = "100.00%";
						}
						else
						{
							fldDeptTotal1.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal1.Text = "";
				}
				else
				{
					fldDeptTotal1.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 1)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal2.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal2.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal2.Text = "100.00%";
						}
						else
						{
							fldDeptTotal2.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal2.Text = "";
				}
				else
				{
					fldDeptTotal2.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 2)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal3.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal3.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal3.Text = "100.00%";
						}
						else
						{
							fldDeptTotal3.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal3.Text = "";
				}
				else
				{
					fldDeptTotal3.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 3)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal4.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal4.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal4.Text = "100.00%";
						}
						else
						{
							fldDeptTotal4.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal4.Text = "";
				}
				else
				{
					fldDeptTotal4.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 4)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal5.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal5.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal5.Text = "100.00%";
						}
						else
						{
							fldDeptTotal5.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal5.Text = "";
				}
				else
				{
					fldDeptTotal5.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 5)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal6.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal6.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal6.Text = "100.00%";
						}
						else
						{
							fldDeptTotal6.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal6.Text = "";
				}
				else
				{
					fldDeptTotal6.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 6)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal7.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal7.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal7.Text = "100.00%";
						}
						else
						{
							fldDeptTotal7.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal7.Text = "";
				}
				else
				{
					fldDeptTotal7.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 7)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal8.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal8.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal8.Text = "100.00%";
						}
						else
						{
							fldDeptTotal8.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal8.Text = "";
				}
				else
				{
					fldDeptTotal8.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 8)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDepartmentTotal[intCounter] != 0)
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal9.Text = Strings.Format((curDepartmentTotal[intCounter] / curOldDepartmentTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDeptTotal9.Text = "-100.00%";
						}
					}
					else
					{
						if (curDepartmentTotal[intCounter] != 0)
						{
							fldDeptTotal9.Text = "100.00%";
						}
						else
						{
							fldDeptTotal9.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDeptTotal9.Text = "";
				}
				else
				{
					fldDeptTotal9.Text = Strings.Format(curDepartmentTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
		}

		private void FillDivTotals()
		{
			int intCounter;
			// fill in division total fields with informaiton
			intCounter = 0;
			if (intNumberOfFields > 0)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal1.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal1.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal1.Text = "100.00%";
						}
						else
						{
							fldDivTotal1.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal1.Text = "";
				}
				else
				{
					fldDivTotal1.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 1)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal2.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal2.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal2.Text = "100.00%";
						}
						else
						{
							fldDivTotal2.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal2.Text = "";
				}
				else
				{
					fldDivTotal2.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 2)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal3.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal3.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal3.Text = "100.00%";
						}
						else
						{
							fldDivTotal3.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal3.Text = "";
				}
				else
				{
					fldDivTotal3.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 3)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal4.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal4.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal4.Text = "100.00%";
						}
						else
						{
							fldDivTotal4.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal4.Text = "";
				}
				else
				{
					fldDivTotal4.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 4)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal5.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal5.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal5.Text = "100.00%";
						}
						else
						{
							fldDivTotal5.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal5.Text = "";
				}
				else
				{
					fldDivTotal5.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 5)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal6.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal6.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal6.Text = "100.00%";
						}
						else
						{
							fldDivTotal6.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal6.Text = "";
				}
				else
				{
					fldDivTotal6.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 6)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal7.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal7.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal7.Text = "100.00%";
						}
						else
						{
							fldDivTotal7.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal7.Text = "";
				}
				else
				{
					fldDivTotal7.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 7)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal8.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal8.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal8.Text = "100.00%";
						}
						else
						{
							fldDivTotal8.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal8.Text = "";
				}
				else
				{
					fldDivTotal8.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 8)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldDivisionTotal[intCounter] != 0)
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal9.Text = Strings.Format((curDivisionTotal[intCounter] / curOldDivisionTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldDivTotal9.Text = "-100.00%";
						}
					}
					else
					{
						if (curDivisionTotal[intCounter] != 0)
						{
							fldDivTotal9.Text = "100.00%";
						}
						else
						{
							fldDivTotal9.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldDivTotal9.Text = "";
				}
				else
				{
					fldDivTotal9.Text = Strings.Format(curDivisionTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
		}

		private void FillTotals()
		{
			int intCounter;
			// fill the grand total fields with informaiton
			intCounter = 0;
			if (intNumberOfFields > 0)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal1.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal1.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal1.Text = "100.00%";
						}
						else
						{
							fldTotal1.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal1.Text = "";
				}
				else
				{
					fldTotal1.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 1)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal2.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal2.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal2.Text = "100.00%";
						}
						else
						{
							fldTotal2.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal2.Text = "";
				}
				else
				{
					fldTotal2.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 2)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal3.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal3.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal3.Text = "100.00%";
						}
						else
						{
							fldTotal3.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal3.Text = "";
				}
				else
				{
					fldTotal3.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 3)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal4.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal4.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal4.Text = "100.00%";
						}
						else
						{
							fldTotal4.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal4.Text = "";
				}
				else
				{
					fldTotal4.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 4)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal5.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal5.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal5.Text = "100.00%";
						}
						else
						{
							fldTotal5.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal5.Text = "";
				}
				else
				{
					fldTotal5.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 5)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal6.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal6.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal6.Text = "100.00%";
						}
						else
						{
							fldTotal6.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal6.Text = "";
				}
				else
				{
					fldTotal6.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 6)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal7.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal7.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal7.Text = "100.00%";
						}
						else
						{
							fldTotal7.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal7.Text = "";
				}
				else
				{
					fldTotal7.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 7)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal8.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal8.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal8.Text = "100.00%";
						}
						else
						{
							fldTotal8.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal8.Text = "";
				}
				else
				{
					fldTotal8.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
			if (intNumberOfFields > 8)
			{
				if ((intType[intCounter] > 18 && intType[intCounter] < 24) || (intType[intCounter] > 28 && intType[intCounter] < 34) || intType[intCounter] == 40 || intType[intCounter] == 42 || intType[intCounter] == 44)
				{
					if (curOldTotal[intCounter] != 0)
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal9.Text = Strings.Format((curTotal[intCounter] / curOldTotal[intCounter]) - 1, "#.00%");
						}
						else
						{
							fldTotal9.Text = "-100.00%";
						}
					}
					else
					{
						if (curTotal[intCounter] != 0)
						{
							fldTotal9.Text = "100.00%";
						}
						else
						{
							fldTotal9.Text = ".00%";
						}
					}
				}
				else if (intType[intCounter] > 33 && intType[intCounter] < 39)
				{
					fldTotal9.Text = "";
				}
				else
				{
					fldTotal9.Text = Strings.Format(curTotal[intCounter], "#,##0.00");
				}
			}
			intCounter += 1;
		}

		private void ClearDeptTotals()
		{
			fldDeptTotal1.Text = "";
			fldDeptTotal2.Text = "";
			fldDeptTotal3.Text = "";
			fldDeptTotal4.Text = "";
			fldDeptTotal5.Text = "";
			fldDeptTotal6.Text = "";
			fldDeptTotal7.Text = "";
			fldDeptTotal8.Text = "";
			fldDeptTotal9.Text = "";
		}

		private void ClearTotals()
		{
			fldTotal1.Text = "";
			fldTotal2.Text = "";
			fldTotal3.Text = "";
			fldTotal4.Text = "";
			fldTotal5.Text = "";
			fldTotal6.Text = "";
			fldTotal7.Text = "";
			fldTotal8.Text = "";
			fldTotal9.Text = "";
		}

		private void ClearDivTotals()
		{
			fldDivTotal1.Text = "";
			fldDivTotal2.Text = "";
			fldDivTotal3.Text = "";
			fldDivTotal4.Text = "";
			fldDivTotal5.Text = "";
			fldDivTotal6.Text = "";
			fldDivTotal7.Text = "";
			fldDivTotal8.Text = "";
			fldDivTotal9.Text = "";
		}

		private void FormatFields()
		{
			float lngCurrentStart = 0;
			if (intNumberOfFields > 0)
			{
				fldDeptTotal1.Visible = true;
				fldDivTotal1.Visible = true;
				fldDetail1.Visible = true;
				fldTotal1.Visible = true;
				fldDeptTotal1.Left = 3600 / 1440f;
				fldDivTotal1.Left = 3600 / 1440f;
				fldDetail1.Left = 3600 / 1440f;
				fldTotal1.Left = 3600 / 1440f;
				Line1.X2 = fldTotal1.Left + fldTotal1.Width;
				lngCurrentStart = fldDetail1.Left + fldDetail1.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 1)
			{
				fldDeptTotal2.Visible = true;
				fldDivTotal2.Visible = true;
				fldDetail2.Visible = true;
				fldTotal2.Visible = true;
				fldDeptTotal2.Left = fldDeptTotal1.Left + fldDeptTotal1.Width + 50 / 1440f;
				fldDivTotal2.Left = fldDivTotal1.Left + fldDivTotal1.Width + 50 / 1440f;
				fldDetail2.Left = fldDetail1.Left + fldDetail1.Width + 50 / 1440f;
				fldTotal2.Left = fldTotal1.Left + fldTotal1.Width + 50 / 1440f;
				Line1.X2 = fldTotal2.Left + fldTotal2.Width;
				lngCurrentStart = fldDetail2.Left + fldDetail2.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 2)
			{
				fldDeptTotal3.Visible = true;
				fldDivTotal3.Visible = true;
				fldDetail3.Visible = true;
				fldTotal3.Visible = true;
				fldDeptTotal3.Left = fldDeptTotal2.Left + fldDeptTotal2.Width + 50 / 1440f;
				fldDivTotal3.Left = fldDivTotal2.Left + fldDivTotal2.Width + 50 / 1440f;
				fldDetail3.Left = fldDetail2.Left + fldDetail2.Width + 50 / 1440f;
				fldTotal3.Left = fldTotal2.Left + fldTotal2.Width + 50 / 1440f;
				Line1.X2 = fldTotal3.Left + fldTotal3.Width;
				lngCurrentStart = fldDetail3.Left + fldDetail3.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 3)
			{
				fldDeptTotal4.Visible = true;
				fldDivTotal4.Visible = true;
				fldDetail4.Visible = true;
				fldTotal4.Visible = true;
				fldDeptTotal4.Left = fldDeptTotal3.Left + fldDeptTotal3.Width + 50 / 1440f;
				fldDivTotal4.Left = fldDivTotal3.Left + fldDivTotal3.Width + 50 / 1440f;
				fldDetail4.Left = fldDetail3.Left + fldDetail3.Width + 50 / 1440f;
				fldTotal4.Left = fldTotal3.Left + fldTotal3.Width + 50 / 1440f;
				Line1.X2 = fldTotal4.Left + fldTotal4.Width;
				lngCurrentStart = fldDetail4.Left + fldDetail4.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 4)
			{
				fldDeptTotal5.Visible = true;
				fldDivTotal5.Visible = true;
				fldDetail5.Visible = true;
				fldTotal5.Visible = true;
				fldDeptTotal5.Left = fldDeptTotal4.Left + fldDeptTotal4.Width + 50 / 1440f;
				fldDivTotal5.Left = fldDivTotal4.Left + fldDivTotal4.Width + 50 / 1440f;
				fldDetail5.Left = fldDetail4.Left + fldDetail4.Width + 50 / 1440f;
				fldTotal5.Left = fldTotal4.Left + fldTotal4.Width + 50 / 1440f;
				Line1.X2 = fldTotal5.Left + fldTotal5.Width;
				lngCurrentStart = fldDetail5.Left + fldDetail5.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 5)
			{
				fldDeptTotal6.Visible = true;
				fldDivTotal6.Visible = true;
				fldDetail6.Visible = true;
				fldTotal6.Visible = true;
				fldDeptTotal6.Left = fldDeptTotal5.Left + fldDeptTotal5.Width + 50 / 1440f;
				fldDivTotal6.Left = fldDivTotal5.Left + fldDivTotal5.Width + 50 / 1440f;
				fldDetail6.Left = fldDetail5.Left + fldDetail5.Width + 50 / 1440f;
				fldTotal6.Left = fldTotal5.Left + fldTotal5.Width + 50 / 1440f;
				Line1.X2 = fldTotal6.Left + fldTotal6.Width;
				lngCurrentStart = fldDetail6.Left + fldDetail6.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 6)
			{
				fldDeptTotal7.Visible = true;
				fldDivTotal7.Visible = true;
				fldDetail7.Visible = true;
				fldTotal7.Visible = true;
				fldDeptTotal7.Left = fldDeptTotal6.Left + fldDeptTotal6.Width + 50 / 1440f;
				fldDivTotal7.Left = fldDivTotal6.Left + fldDivTotal6.Width + 50 / 1440f;
				fldDetail7.Left = fldDetail6.Left + fldDetail6.Width + 50 / 1440f;
				fldTotal7.Left = fldTotal6.Left + fldTotal6.Width + 50 / 1440f;
				Line1.X2 = fldTotal7.Left + fldTotal7.Width;
				lngCurrentStart = fldDetail7.Left + fldDetail7.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 7)
			{
				fldDeptTotal8.Visible = true;
				fldDivTotal8.Visible = true;
				fldDetail8.Visible = true;
				fldTotal8.Visible = true;
				fldDeptTotal8.Left = fldDeptTotal7.Left + fldDeptTotal7.Width + 50 / 1440f;
				fldDivTotal8.Left = fldDivTotal7.Left + fldDivTotal7.Width + 50 / 1440f;
				fldDetail8.Left = fldDetail7.Left + fldDetail7.Width + 50 / 1440f;
				fldTotal8.Left = fldTotal7.Left + fldTotal7.Width + 50 / 1440f;
				Line1.X2 = fldTotal8.Left + fldTotal8.Width;
				lngCurrentStart = fldDetail8.Left + fldDetail8.Width + 50 / 1440f;
			}
			if (intNumberOfFields > 8)
			{
				fldDeptTotal9.Visible = true;
				fldDivTotal9.Visible = true;
				fldDetail9.Visible = true;
				fldTotal9.Visible = true;
				fldDeptTotal9.Left = fldDeptTotal8.Left + fldDeptTotal8.Width + 50 / 1440f;
				fldDivTotal9.Left = fldDivTotal8.Left + fldDivTotal8.Width + 50 / 1440f;
				fldDetail9.Left = fldDetail8.Left + fldDetail8.Width + 50 / 1440f;
				fldTotal9.Left = fldTotal8.Left + fldTotal8.Width + 50 / 1440f;
				Line1.X2 = fldTotal9.Left + fldTotal9.Width;
				lngCurrentStart = fldDetail9.Left + fldDetail9.Width + 50 / 1440f;
			}
			if (strComments == "S")
			{
				fldComments.Top = 0;
				fldComments.Left = lngCurrentStart + 50 / 1440f;
				fldComments.Width = fldDetail1.Width * 3;
				Line1.X2 = fldComments.Left + fldComments.Width;
			}
			else if (strComments == "P")
			{
				fldComments.Width = this.PrintWidth - fldComments.Left;
			}
		}

		private bool BudgetAccountExists(string strAccount)
		{
			bool BudgetAccountExists = false;
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                rsTemp.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + strAccount + "'");
                if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
                {
                    BudgetAccountExists = true;
                }
                else
                {
                    BudgetAccountExists = false;
                }
            }

            return BudgetAccountExists;
		}

		private void FillDetailFields()
		{
			int counter;
			int intFieldTracker;
			Decimal curValue;
			bool blnData;
			// this function gets the actual values that the user wanted on the report
			counter = 0;
			intFieldTracker = 0;
			blnData = false;
			if (intNumberOfFields > 0)
			{
				for (intFieldTracker = intFieldTracker; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail1.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail1.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail1.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 1)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail2.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail2.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail2.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 2)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail3.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail3.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail3.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 3)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail4.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail4.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail4.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 4)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail5.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail5.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail5.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 5)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail6.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail6.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail6.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 6)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail7.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail7.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail7.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 7)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail8.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail8.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail8.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (intNumberOfFields > 8)
			{
				for (intFieldTracker = intFieldTracker + 1; intFieldTracker <= 44; intFieldTracker++)
				{
					if (modBudgetaryMaster.Statics.blnFields[intFieldTracker])
					{
						break;
					}
				}
				intCurrentDetailField = counter;
				if (intFieldTracker > 33 && intFieldTracker < 39)
				{
					fldDetail9.Text = "";
				}
				else
				{
					curValue = GetDetailValue(ref intFieldTracker);
					if (curValue != 0)
					{
						blnData = true;
					}
					if ((intFieldTracker > 18 && intFieldTracker < 24) || (intFieldTracker > 28 && intFieldTracker < 34) || intFieldTracker == 40 || intFieldTracker == 42 || intFieldTracker == 44)
					{
						fldDetail9.Text = Strings.Format(curValue, "#.00%");
					}
					else
					{
						fldDetail9.Text = Strings.Format(curValue, "#,##0.00");
						curDivisionTotal[counter] += curValue;
						curDepartmentTotal[counter] += curValue;
						curTotal[counter] += curValue;
					}
				}
				intType[counter] = intFieldTracker;
				counter += 1;
			}
			if (!blnData)
			{
				fldAccount.Visible = false;
				fldComments.Visible = false;
				if (intNumberOfFields > 0)
				{
					fldDetail1.Visible = false;
				}
				if (intNumberOfFields > 1)
				{
					fldDetail2.Visible = false;
				}
				if (intNumberOfFields > 2)
				{
					fldDetail3.Visible = false;
				}
				if (intNumberOfFields > 3)
				{
					fldDetail4.Visible = false;
				}
				if (intNumberOfFields > 4)
				{
					fldDetail5.Visible = false;
				}
				if (intNumberOfFields > 5)
				{
					fldDetail6.Visible = false;
				}
				if (intNumberOfFields > 6)
				{
					fldDetail7.Visible = false;
				}
				if (intNumberOfFields > 7)
				{
					fldDetail8.Visible = false;
				}
				if (intNumberOfFields > 8)
				{
					fldDetail9.Visible = false;
				}
			}
			else
			{
				fldAccount.Visible = true;
				fldComments.Visible = true;
				if (intNumberOfFields > 0)
				{
					fldDetail1.Visible = true;
				}
				if (intNumberOfFields > 1)
				{
					fldDetail2.Visible = true;
				}
				if (intNumberOfFields > 2)
				{
					fldDetail3.Visible = true;
				}
				if (intNumberOfFields > 3)
				{
					fldDetail4.Visible = true;
				}
				if (intNumberOfFields > 4)
				{
					fldDetail5.Visible = true;
				}
				if (intNumberOfFields > 5)
				{
					fldDetail6.Visible = true;
				}
				if (intNumberOfFields > 6)
				{
					fldDetail7.Visible = true;
				}
				if (intNumberOfFields > 7)
				{
					fldDetail8.Visible = true;
				}
				if (intNumberOfFields > 8)
				{
					fldDetail9.Visible = true;
				}
			}
		}
		// vbPorter upgrade warning: intInfo As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(double, short, Decimal)
		private Decimal GetDetailValue(ref int intInfo)
		{
			Decimal GetDetailValue = 0;
            using (clsDRWrapper rsPreviousInfo = new clsDRWrapper())
            {
                // vbPorter upgrade warning: curBud As Decimal	OnWrite(double, short)
                Decimal curBud;
                // vbPorter upgrade warning: curCurrentYTDNet As Decimal	OnWriteFCConvert.ToDouble(
                Decimal curCurrentYTDNet = 0.0M;
                switch (intInfo)
                {
                    case 0:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 3));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                    rsPreviousInfo.Get_Fields("CarryForward") -
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                    rsPreviousInfo.Get_Fields("CarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                GetDetailValue = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 1:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 3));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            GetDetailValue = FCConvert.ToDecimal(
                                (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                 rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 2:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                    rsPreviousInfo.Get_Fields("CarryForward") -
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                    rsPreviousInfo.Get_Fields("CarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                GetDetailValue = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 3:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            GetDetailValue = FCConvert.ToDecimal(
                                (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                 rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 4:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                    rsPreviousInfo.Get_Fields("CarryForward") -
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                GetDetailValue = FCConvert.ToDecimal(
                                    rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                    rsPreviousInfo.Get_Fields("CarryForward") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                GetDetailValue = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 5:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            GetDetailValue = FCConvert.ToDecimal(
                                (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                 rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            GetDetailValue = 0;
                        }

                        break;
                    }
                    case 6:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue =
                                FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue =
                                FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 7:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        GetDetailValue = FCConvert.ToDecimal(GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")) * -1);
                        break;
                    }
                    case 8:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")) +
                                GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false) +
                                GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false) +
                                GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsAccountInfo.Get_Fields("CurrentBudget") +
                                GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")));
                        }

                        break;
                    }
                    case 9:
                    {
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        GetDetailValue = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("InitialRequest"));
                        break;
                    }
                    case 10:
                    {
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        GetDetailValue = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("ManagerRequest"));
                        break;
                    }
                    case 11:
                    {
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        GetDetailValue = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("CommitteeRequest"));
                        break;
                    }
                    case 12:
                    {
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        GetDetailValue = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields("ElectedRequest"));
                        break;
                    }
                    case 13:
                    {
                        GetDetailValue = FCConvert.ToDecimal(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
                        break;
                    }
                    case 14:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("InitialRequest") -
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("InitialRequest") -
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("InitialRequest") -
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("InitialRequest") - rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 15:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ManagerRequest") -
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ManagerRequest") -
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ManagerRequest") -
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ManagerRequest") - rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 16:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("CommitteeRequest") -
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("CommitteeRequest") -
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("CommitteeRequest") -
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("CommitteeRequest") -
                                rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 17:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ElectedRequest") -
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ElectedRequest") -
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ElectedRequest") -
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields("ElectedRequest") - rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 18:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") -
                                GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") -
                                GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") -
                                GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            GetDetailValue = FCConvert.ToDecimal(
                                rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") -
                                rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        break;
                    }
                    case 19:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("InitialRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        break;
                    }
                    case 20:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ManagerRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("ManagerRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ManagerRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        break;
                    }
                    case 21:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("CommitteeRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("CommitteeRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("CommitteeRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        break;
                    }
                    case 22:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ElectedRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("ElectedRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ElectedRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        break;
                    }
                    case 23:
                    {
                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_2(rsBudgetInfo.Get_Fields("Account")));
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_23(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(GetNetBudget_8(rsBudgetInfo.Get_Fields("Account"), false));
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                            curBud = FCConvert.ToDecimal(rsAccountInfo.Get_Fields("CurrentBudget"));
                        }

                        if (curBud != 0)
                        {
                            if (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") != 0)
                            {
                                GetDetailValue = (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            if (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        break;
                    }
                    case 24:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("InitialRequest") - curBud;
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("InitialRequest") - curBud;
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("InitialRequest") - curBud;
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("InitialRequest") - curBud;
                        }

                        break;
                    }
                    case 25:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ManagerRequest") - curBud;
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ManagerRequest") - curBud;
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ManagerRequest") - curBud;
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ManagerRequest") - curBud;
                        }

                        break;
                    }
                    case 26:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("CommitteeRequest") - curBud;
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("CommitteeRequest") - curBud;
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("CommitteeRequest") - curBud;
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("CommitteeRequest") - curBud;
                        }

                        break;
                    }
                    case 27:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ElectedRequest") - curBud;
                        }
                        else if (blnIncBudAdj)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ElectedRequest") - curBud;
                        }
                        else if (blnIncCarryForwards)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ElectedRequest") - curBud;
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            GetDetailValue = rsBudgetInfo.Get_Fields("ElectedRequest") - curBud;
                        }

                        break;
                    }
                    case 28:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (blnIncBudAdj && blnIncCarryForwards)
                        {
                            GetDetailValue = rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") - curBud;
                        }
                        else if (blnIncBudAdj)
                        {
                            GetDetailValue = rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") - curBud;
                        }
                        else if (blnIncCarryForwards)
                        {
                            GetDetailValue = rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") - curBud;
                        }
                        else
                        {
                            GetDetailValue = rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") - curBud;
                        }

                        break;
                    }
                    case 29:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("InitialRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("InitialRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        // TODO Get_Fields: Check the table for the column [InitialRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("InitialRequest");
                        break;
                    }
                    case 30:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ManagerRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("ManagerRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ManagerRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        // TODO Get_Fields: Check the table for the column [ManagerRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ManagerRequest");
                        break;
                    }
                    case 31:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("CommitteeRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("CommitteeRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("CommitteeRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        // TODO Get_Fields: Check the table for the column [CommitteeRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("CommitteeRequest");
                        break;
                    }
                    case 32:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ElectedRequest")) != 0)
                            {
                                // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                                GetDetailValue = (rsBudgetInfo.Get_Fields("ElectedRequest") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsBudgetInfo.Get_Fields("ElectedRequest")) != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        // TODO Get_Fields: Check the table for the column [ElectedRequest] and replace with corresponding Get_Field method
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields("ElectedRequest");
                        break;
                    }
                    case 33:
                    {
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            if (blnIncBudAdj && blnIncCarryForwards)
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncBudAdj)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("EndBudget") -
                                                             rsPreviousInfo.Get_Fields("CarryForward") -
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDBudgetAdjustments"));
                            }
                            else if (blnIncCarryForwards)
                            {
                                // TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget") +
                                                             rsPreviousInfo.Get_Fields("CarryForward") +
                                                             rsPreviousInfo.Get_Fields_Decimal("YTDCarryForward"));
                            }
                            else
                            {
                                curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("StartBudget"));
                            }
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            if (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") != 0)
                            {
                                GetDetailValue = (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            if (rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount") != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        curExpenseTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curDivisionTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curDepartmentTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        curTotal[intCurrentDetailField] += rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount");
                        break;
                    }
                    case 39:
                    {
                        // 3 years ago vs 2 years ago actual $
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 3));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                          rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            curBud = 0;
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (curBud != 0)
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                GetDetailValue =
                                    ((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                      rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1) - curBud;
                            }
                            else
                            {
                                GetDetailValue = curBud * -1;
                            }
                        }
                        else
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                GetDetailValue = FCConvert.ToDecimal(
                                    (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                     rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        break;
                    }
                    case 40:
                    {
                        // 3 years ago vs 2 years ago actual %
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 3));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                         rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments"));
                        }
                        else
                        {
                            curBud = 0;
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (curBud != 0)
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                if (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments") != 0)
                                {
                                    GetDetailValue =
                                        ((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                          rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) / curBud) - 1;
                                }
                                else
                                {
                                    GetDetailValue = -1;
                                }
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                if (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments") != 0)
                                {
                                    GetDetailValue = 1;
                                }
                                else
                                {
                                    GetDetailValue = 0;
                                }
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curExpenseTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curDivisionTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curDepartmentTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                        }

                        break;
                    }
                    case 41:
                    {
                        // 2 years ago vs 1 year ago actual $
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                          rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            curBud = 0;
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (curBud != 0)
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                GetDetailValue =
                                    ((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                      rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1) - curBud;
                            }
                            else
                            {
                                GetDetailValue = curBud * -1;
                            }
                        }
                        else
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                GetDetailValue = FCConvert.ToDecimal(
                                    (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                     rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        break;
                    }
                    case 42:
                    {
                        // 2 years ago vs 1 year ago actual %
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 2));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                         rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments"));
                        }
                        else
                        {
                            curBud = 0;
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (curBud != 0)
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                if (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments") != 0)
                                {
                                    GetDetailValue =
                                        ((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                          rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) / curBud) - 1;
                                }
                                else
                                {
                                    GetDetailValue = -1;
                                }
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                            {
                                if (rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                    rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments") != 0)
                                {
                                    GetDetailValue = 1;
                                }
                                else
                                {
                                    GetDetailValue = 0;
                                }
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curExpenseTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curDivisionTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curDepartmentTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                            curTotal[intCurrentDetailField] +=
                                rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments");
                        }

                        break;
                    }
                    case 43:
                    {
                        // 1 year ago vs current actual $
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal((rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                          rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments")) * -1);
                        }
                        else
                        {
                            curBud = 0;
                        }

                        if (curBud != 0)
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            curCurrentYTDNet =
                                FCConvert.ToDecimal(GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")) * -1);
                            if (curCurrentYTDNet != 0)
                            {
                                GetDetailValue = curCurrentYTDNet - curBud;
                            }
                            else
                            {
                                GetDetailValue = curBud * -1;
                            }
                        }
                        else
                        {
                            GetDetailValue = curCurrentYTDNet;
                        }

                        break;
                    }
                    case 44:
                    {
                        // 1 year ago vs current actual %
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        rsPreviousInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Account = '" +
                                                     rsBudgetInfo.Get_Fields("Account") + "' AND Year = " +
                                                     FCConvert.ToString(lngCurrent - 1));
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curBud = FCConvert.ToDecimal(rsPreviousInfo.Get_Fields_Decimal("ActualSpent") +
                                                         rsPreviousInfo.Get_Fields_Decimal("YTDAdjustments"));
                        }
                        else
                        {
                            curBud = 0;
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        curCurrentYTDNet = FCConvert.ToDecimal(GetYTDNet_2(rsBudgetInfo.Get_Fields("Account")));
                        if (curBud != 0)
                        {
                            if (curCurrentYTDNet != 0)
                            {
                                GetDetailValue = (curCurrentYTDNet / curBud) - 1;
                            }
                            else
                            {
                                GetDetailValue = -1;
                            }
                        }
                        else
                        {
                            if (curCurrentYTDNet != 0)
                            {
                                GetDetailValue = 1;
                            }
                            else
                            {
                                GetDetailValue = 0;
                            }
                        }

                        curOldExpenseTotal[intCurrentDetailField] += curBud;
                        curOldDivisionTotal[intCurrentDetailField] += curBud;
                        curOldDepartmentTotal[intCurrentDetailField] += curBud;
                        curOldTotal[intCurrentDetailField] += curBud;
                        if (rsPreviousInfo.EndOfFile() != true && rsPreviousInfo.BeginningOfFile() != true)
                        {
                            curExpenseTotal[intCurrentDetailField] += curCurrentYTDNet;
                            curDivisionTotal[intCurrentDetailField] += curCurrentYTDNet;
                            curDepartmentTotal[intCurrentDetailField] += curCurrentYTDNet;
                            curTotal[intCurrentDetailField] += curCurrentYTDNet;
                        }

                        break;
                    }
                }

                //end switch
            }

            return GetDetailValue;
		}

		private string Revenue(ref string x)
		{
			string Revenue = "";
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				Revenue = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			else
			{
				Revenue = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)))));
			}
			return Revenue;
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private string Division(ref string x)
		{
			string Division = "";
			Division = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))));
			return Division;
		}

		private double GetYTDDebit(ref string strAcct)
		{
			double GetYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
				GetYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PostedDebitsTotal")) + Conversion.Val(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
			}
			else
			{
				GetYTDDebit = 0;
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit(ref string strAcct)
		{
			double GetYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				GetYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
			}
			else
			{
				GetYTDCredit = 0;
			}
			return GetYTDCredit;
		}

		private double GetYTDNet_2(string strAcct)
		{
			return GetYTDNet(ref strAcct);
		}

		private double GetYTDNet(ref string strAcct)
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit(ref strAcct) - GetYTDCredit(ref strAcct);
			return GetYTDNet;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent;
			int LowDateCurrent;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			string strTable;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			strTable = "RevenueReportInfo";
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(CarryForward) as CarryForwardTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// increment page counter and show page and report type
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void GetFieldsForReport()
		{
			// Set what data will be shown on report base don what the user checked on the report setup screen
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(0))
			{
				modBudgetaryMaster.Statics.blnFields[0] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(1))
			{
				modBudgetaryMaster.Statics.blnFields[1] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(2))
			{
				modBudgetaryMaster.Statics.blnFields[2] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(3))
			{
				modBudgetaryMaster.Statics.blnFields[3] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(4))
			{
				modBudgetaryMaster.Statics.blnFields[4] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(5))
			{
				modBudgetaryMaster.Statics.blnFields[5] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(6))
			{
				modBudgetaryMaster.Statics.blnFields[6] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(7))
			{
				modBudgetaryMaster.Statics.blnFields[7] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(8))
			{
				modBudgetaryMaster.Statics.blnFields[8] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(9))
			{
				modBudgetaryMaster.Statics.blnFields[9] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(10))
			{
				modBudgetaryMaster.Statics.blnFields[10] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(11))
			{
				modBudgetaryMaster.Statics.blnFields[11] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(12))
			{
				modBudgetaryMaster.Statics.blnFields[12] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(13))
			{
				modBudgetaryMaster.Statics.blnFields[13] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(14))
			{
				modBudgetaryMaster.Statics.blnFields[14] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(15))
			{
				modBudgetaryMaster.Statics.blnFields[15] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(16))
			{
				modBudgetaryMaster.Statics.blnFields[16] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(17))
			{
				modBudgetaryMaster.Statics.blnFields[17] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(18))
			{
				modBudgetaryMaster.Statics.blnFields[18] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(19))
			{
				modBudgetaryMaster.Statics.blnFields[19] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(20))
			{
				modBudgetaryMaster.Statics.blnFields[20] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(21))
			{
				modBudgetaryMaster.Statics.blnFields[21] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(22))
			{
				modBudgetaryMaster.Statics.blnFields[22] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(23))
			{
				modBudgetaryMaster.Statics.blnFields[23] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(24))
			{
				modBudgetaryMaster.Statics.blnFields[24] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(25))
			{
				modBudgetaryMaster.Statics.blnFields[25] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(26))
			{
				modBudgetaryMaster.Statics.blnFields[26] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(27))
			{
				modBudgetaryMaster.Statics.blnFields[27] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(28))
			{
				modBudgetaryMaster.Statics.blnFields[28] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(29))
			{
				modBudgetaryMaster.Statics.blnFields[29] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(30))
			{
				modBudgetaryMaster.Statics.blnFields[30] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(31))
			{
				modBudgetaryMaster.Statics.blnFields[31] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(32))
			{
				modBudgetaryMaster.Statics.blnFields[32] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(33))
			{
				modBudgetaryMaster.Statics.blnFields[33] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(34))
			{
				modBudgetaryMaster.Statics.blnFields[34] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(35))
			{
				modBudgetaryMaster.Statics.blnFields[35] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(36))
			{
				modBudgetaryMaster.Statics.blnFields[36] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(37))
			{
				modBudgetaryMaster.Statics.blnFields[37] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(38))
			{
				modBudgetaryMaster.Statics.blnFields[38] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(39))
			{
				modBudgetaryMaster.Statics.blnFields[39] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(40))
			{
				modBudgetaryMaster.Statics.blnFields[40] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(41))
			{
				modBudgetaryMaster.Statics.blnFields[41] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(42))
			{
				modBudgetaryMaster.Statics.blnFields[42] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(43))
			{
				modBudgetaryMaster.Statics.blnFields[43] = true;
				intNumberOfFields += 1;
			}
			if (frmCustomBudgetReport.InstancePtr.lstFields.Selected(44))
			{
				modBudgetaryMaster.Statics.blnFields[44] = true;
				intNumberOfFields += 1;
			}
		}
		
		private string GetTitle_2(short intLine, int intData)
		{
			return GetTitle(ref intLine, ref intData);
		}

		private string GetTitle(ref short intLine, ref int intData)
		{
			string GetTitle = "";
			switch (intData)
			{
				case 0:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 3);
									break;
								}
							case 3:
								{
									GetTitle = "Budget";
									break;
								}
						}
						//end switch
						break;
					}
				case 1:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 3);
									break;
								}
							case 3:
								{
									GetTitle = "Actual";
									break;
								}
						}
						//end switch
						break;
					}
				case 2:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 2);
									break;
								}
							case 3:
								{
									GetTitle = "Budget";
									break;
								}
						}
						//end switch
						break;
					}
				case 3:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 2);
									break;
								}
							case 3:
								{
									GetTitle = "Actual";
									break;
								}
						}
						//end switch
						break;
					}
				case 4:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 1);
									break;
								}
							case 3:
								{
									GetTitle = "Budget";
									break;
								}
						}
						//end switch
						break;
					}
				case 5:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent - 1);
									break;
								}
							case 3:
								{
									GetTitle = "Actual";
									break;
								}
						}
						//end switch
						break;
					}
				case 6:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent);
									break;
								}
							case 3:
								{
									GetTitle = "Budget";
									break;
								}
						}
						//end switch
						break;
					}
				case 7:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent);
									break;
								}
							case 3:
								{
									GetTitle = "YTD";
									break;
								}
						}
						//end switch
						break;
					}
				case 8:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngCurrent);
									break;
								}
							case 3:
								{
									GetTitle = "Balance";
									break;
								}
						}
						//end switch
						break;
					}
				case 9:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Initial";
									break;
								}
						}
						//end switch
						break;
					}
				case 10:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Manager";
									break;
								}
						}
						//end switch
						break;
					}
				case 11:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Committee";
									break;
								}
						}
						//end switch
						break;
					}
				case 12:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Elected";
									break;
								}
						}
						//end switch
						break;
					}
				case 13:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Approved";
									break;
								}
						}
						//end switch
						break;
					}
				case 14:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Init Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 15:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Man Req vs\"";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 16:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Comm Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 17:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Elec Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 18:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "App Amt vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 19:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Init Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 20:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Man Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 21:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Comm Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 22:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Elec Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 23:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "App Amt vs";
									break;
								}
							case 2:
								{
									GetTitle = "Curr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 24:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Init Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 25:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Man Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 26:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Comm Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 27:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Elec Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 28:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "App Amt vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 29:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Init Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 30:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Man Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 31:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Comm Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 32:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "Elec Req vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 33:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "App Amt vs";
									break;
								}
							case 2:
								{
									GetTitle = "Last Yr Bud";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 34:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Initial";
									break;
								}
						}
						//end switch
						break;
					}
				case 35:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Manager";
									break;
								}
						}
						//end switch
						break;
					}
				case 36:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Committee";
									break;
								}
						}
						//end switch
						break;
					}
				case 37:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Elected";
									break;
								}
						}
						//end switch
						break;
					}
				case 38:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "";
									break;
								}
							case 2:
								{
									GetTitle = FCConvert.ToString(lngBudget);
									break;
								}
							case 3:
								{
									GetTitle = "Approved";
									break;
								}
						}
						//end switch
						break;
					}
				case 39:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "3 Yrs Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "2 Yrs Ago Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 40:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "3 Yrs Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "2 Yrs Ago Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 41:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "2 Yrs Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "1 Yr Ago Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 42:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "2 Yrs Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "1 Yr Ago Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
				case 43:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "1 Yr Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "Current Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change $";
									break;
								}
						}
						//end switch
						break;
					}
				case 44:
					{
						switch (intLine)
						{
							case 1:
								{
									GetTitle = "1 Yr Ago vs";
									break;
								}
							case 2:
								{
									GetTitle = "Current Act";
									break;
								}
							case 3:
								{
									GetTitle = "Change %";
									break;
								}
						}
						//end switch
						break;
					}
			}
			//end switch
			return GetTitle;
		}
		// vbPorter upgrade warning: intCol As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intData As short	OnWriteFCConvert.ToInt32(
		private void CreateTitle(ref int intCol, ref int intData)
		{
			switch (intCol)
			{
				case 0:
					{
						lblTitle1Line1.Visible = true;
						lblTitle1Line2.Visible = true;
						lblTitle1Line3.Visible = true;
						lblTitle1Line1.Text = GetTitle_2(1, intData);
						lblTitle1Line2.Text = GetTitle_2(2, intData);
						lblTitle1Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 1:
					{
						lblTitle2Line1.Visible = true;
						lblTitle2Line2.Visible = true;
						lblTitle2Line3.Visible = true;
						lblTitle2Line1.Text = GetTitle_2(1, intData);
						lblTitle2Line2.Text = GetTitle_2(2, intData);
						lblTitle2Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 2:
					{
						lblTitle3Line1.Visible = true;
						lblTitle3Line2.Visible = true;
						lblTitle3Line3.Visible = true;
						lblTitle3Line1.Text = GetTitle_2(1, intData);
						lblTitle3Line2.Text = GetTitle_2(2, intData);
						lblTitle3Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 3:
					{
						lblTitle4Line1.Visible = true;
						lblTitle4Line2.Visible = true;
						lblTitle4Line3.Visible = true;
						lblTitle4Line1.Text = GetTitle_2(1, intData);
						lblTitle4Line2.Text = GetTitle_2(2, intData);
						lblTitle4Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 4:
					{
						lblTitle5Line1.Visible = true;
						lblTitle5Line2.Visible = true;
						lblTitle5Line3.Visible = true;
						lblTitle5Line1.Text = GetTitle_2(1, intData);
						lblTitle5Line2.Text = GetTitle_2(2, intData);
						lblTitle5Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 5:
					{
						lblTitle6Line1.Visible = true;
						lblTitle6Line2.Visible = true;
						lblTitle6Line3.Visible = true;
						lblTitle6Line1.Text = GetTitle_2(1, intData);
						lblTitle6Line2.Text = GetTitle_2(2, intData);
						lblTitle6Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 6:
					{
						lblTitle7Line1.Visible = true;
						lblTitle7Line2.Visible = true;
						lblTitle7Line3.Visible = true;
						lblTitle7Line1.Text = GetTitle_2(1, intData);
						lblTitle7Line2.Text = GetTitle_2(2, intData);
						lblTitle7Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 7:
					{
						lblTitle8Line1.Visible = true;
						lblTitle8Line2.Visible = true;
						lblTitle8Line3.Visible = true;
						lblTitle8Line1.Text = GetTitle_2(1, intData);
						lblTitle8Line2.Text = GetTitle_2(2, intData);
						lblTitle8Line3.Text = GetTitle_2(3, intData);
						break;
					}
				case 8:
					{
						lblTitle9Line1.Visible = true;
						lblTitle9Line2.Visible = true;
						lblTitle9Line3.Visible = true;
						lblTitle9Line1.Text = GetTitle_2(1, intData);
						lblTitle9Line2.Text = GetTitle_2(2, intData);
						lblTitle9Line3.Text = GetTitle_2(3, intData);
						break;
					}
			}
			//end switch
		}

		private double GetNetBudget_2(string strAcct, bool blnAdj = true, bool blnCarry = true)
		{
			return GetNetBudget(ref strAcct, blnAdj, blnCarry);
		}

		private double GetNetBudget_8(string strAcct, bool blnAdj = true, bool blnCarry = true)
		{
			return GetNetBudget(ref strAcct, blnAdj, blnCarry);
		}

		private double GetNetBudget_23(string strAcct, bool blnCarry = true)
		{
			return GetNetBudget(ref strAcct, true, blnCarry);
		}

		private double GetNetBudget(ref string strAcct, bool blnAdj = true, bool blnCarry = true)
		{
			double GetNetBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				if (blnAdj && blnCarry)
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					GetNetBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
				}
				else if (blnAdj)
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [CarryForwardTotal] not found!! (maybe it is an alias?)
					GetNetBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal")) - Conversion.Val(rsYTDActivity.Get_Fields("CarryForwardTotal"));
				}
				else if (blnCarry)
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [CarryForwardTotal] not found!! (maybe it is an alias?)
					GetNetBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal")) + Conversion.Val(rsYTDActivity.Get_Fields("CarryForwardTotal"));
				}
				else
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					GetNetBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
				}
			}
			else
			{
				GetNetBudget = 0;
			}
			return GetNetBudget;
		}

		
	}
}
