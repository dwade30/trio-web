﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOFSummaryInfo.
	/// </summary>
	partial class srptOFSummaryInfo
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptOFSummaryInfo));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFund = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAmount,
				this.fldFund
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Field29,
				this.Field36,
				this.Line1
			});
			this.GroupHeader1.Height = 0.6354167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalAmount,
				this.Field38,
				this.Line2
			});
			this.GroupFooter1.Height = 0.1979167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label22.Text = "OF Summary";
			this.Label22.Top = 0.15625F;
			this.Label22.Width = 7.53125F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 3F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Fund";
			this.Field29.Top = 0.4375F;
			this.Field29.Width = 0.4375F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.1875F;
			this.Field36.Left = 3.6875F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field36.Tag = " ";
			this.Field36.Text = "Amount";
			this.Field36.Top = 0.4375F;
			this.Field36.Width = 0.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 1.65625F;
			this.Line1.X1 = 2.9375F;
			this.Line1.X2 = 4.59375F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.19F;
			this.fldAmount.Left = 3.5F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldAmount.Text = "Field36";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.0625F;
			// 
			// fldFund
			// 
			this.fldFund.Height = 0.19F;
			this.fldFund.Left = 3F;
			this.fldFund.MultiLine = false;
			this.fldFund.Name = "fldFund";
			this.fldFund.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; white-space: nowrap; ddo" + "-char-set: 1";
			this.fldFund.Text = "Field36";
			this.fldFund.Top = 0F;
			this.fldFund.Width = 0.4375F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.19F;
			this.fldTotalAmount.Left = 3.5F;
			this.fldTotalAmount.MultiLine = false;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; whit" + "e-space: nowrap; ddo-char-set: 1";
			this.fldTotalAmount.Text = "Field36";
			this.fldTotalAmount.Top = 0.03125F;
			this.fldTotalAmount.Width = 1.0625F;
			// 
			// Field38
			// 
			this.Field38.Height = 0.19F;
			this.Field38.Left = 3F;
			this.Field38.MultiLine = false;
			this.Field38.Name = "Field38";
			this.Field38.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; white" + "-space: nowrap; ddo-char-set: 1";
			this.Field38.Text = "Total";
			this.Field38.Top = 0.03125F;
			this.Field38.Width = 0.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 1.65625F;
			this.Line2.X1 = 2.9375F;
			this.Line2.X2 = 4.59375F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// srptOFSummaryInfo
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFund)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFund;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field38;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
