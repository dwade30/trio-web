﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWBD0000
{
	public class cBDAccountInquiryReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collAccounts = new cGenericCollection();
		private cGenericCollection collAccounts_AutoInitialized;

		private cGenericCollection collAccounts
		{
			get
			{
				if (collAccounts_AutoInitialized == null)
				{
					collAccounts_AutoInitialized = new cGenericCollection();
				}
				return collAccounts_AutoInitialized;
			}
			set
			{
				collAccounts_AutoInitialized = value;
			}
		}

		private string[] strSegmentFilter = new string[5 + 1];
		private int[] SegmentLength = new int[5 + 1];
		private int intAccountTypeToShow;

		public string GetSegmentFilter(int intSegment)
		{
			string GetSegmentFilter = "";
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegmentFilter = strSegmentFilter[intSegment - 1];
			}
			return GetSegmentFilter;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short GetSegmentLength(int intSegment)
		{
			short GetSegmentLength = 0;
			if (intSegment > 0 && intSegment < 5)
			{
				GetSegmentLength = FCConvert.ToInt16(SegmentLength[intSegment - 1]);
			}
			return GetSegmentLength;
		}

		public void SetSegmentFilter(int intSegment, string strFilter)
		{
			if (intSegment > 0 && intSegment < 5)
			{
				strSegmentFilter[intSegment - 1] = strFilter;
			}
		}

		public void SetSegmentLength(int intSegment, int intLength)
		{
			if (intSegment > 0 && intSegment < 5)
			{
				SegmentLength[intSegment - 1] = intLength;
			}
		}

		public short AccountTypeToShow
		{
			set
			{
				intAccountTypeToShow = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short AccountTypeToShow = 0;
				AccountTypeToShow = FCConvert.ToInt16(intAccountTypeToShow);
				return AccountTypeToShow;
			}
		}

		public cGenericCollection Accounts
		{
			get
			{
				cGenericCollection Accounts = null;
				Accounts = collAccounts;
				return Accounts;
			}
		}
	}
}
