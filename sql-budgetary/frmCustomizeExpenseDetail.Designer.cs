﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeExpenseDetail.
	/// </summary>
	partial class frmCustomizeExpenseDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReportUse;
		public fecherFoundation.FCLabel lblReportUse;
		public fecherFoundation.FCComboBox cmbPaperWidth;
		public fecherFoundation.FCLabel lblPaperWidth;
		public fecherFoundation.FCComboBox cmbDescriptions;
		public fecherFoundation.FCLabel lblDescriptions;
		public fecherFoundation.FCComboBox cmbAccountInformation;
		public fecherFoundation.FCLabel lblAccountInformation;
		public fecherFoundation.FCComboBox cmbPendingActivity;
		public fecherFoundation.FCLabel lblPendingActivity;
		public fecherFoundation.FCCheckBox chkAllInclusive;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraPreferences;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeExpenseDetail));
			this.cmbReportUse = new fecherFoundation.FCComboBox();
			this.lblReportUse = new fecherFoundation.FCLabel();
			this.cmbPaperWidth = new fecherFoundation.FCComboBox();
			this.lblPaperWidth = new fecherFoundation.FCLabel();
			this.cmbDescriptions = new fecherFoundation.FCComboBox();
			this.lblDescriptions = new fecherFoundation.FCLabel();
			this.cmbAccountInformation = new fecherFoundation.FCComboBox();
			this.lblAccountInformation = new fecherFoundation.FCLabel();
			this.cmbPendingActivity = new fecherFoundation.FCComboBox();
			this.lblPendingActivity = new fecherFoundation.FCLabel();
			this.chkAllInclusive = new fecherFoundation.FCCheckBox();
			this.cdgFont = new fecherFoundation.FCCommonDialog();
			this.fraPreferences = new fecherFoundation.FCFrame();
			this.fraInformation = new fecherFoundation.FCFrame();
			this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAllInclusive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).BeginInit();
			this.fraPreferences.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
			this.fraInformation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkAllInclusive);
			this.ClientArea.Controls.Add(this.fraPreferences);
			this.ClientArea.Controls.Add(this.fraInformation);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(343, 30);
			this.HeaderText.Text = "Expense Detail Report Format";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbReportUse
			// 
			this.cmbReportUse.Items.AddRange(new object[] {
            "Print",
            "Display"});
			this.cmbReportUse.Location = new System.Drawing.Point(172, 130);
			this.cmbReportUse.Name = "cmbReportUse";
			this.cmbReportUse.Size = new System.Drawing.Size(200, 40);
			this.ToolTip1.SetToolTip(this.cmbReportUse, null);
			// 
			// lblReportUse
			// 
			this.lblReportUse.AutoSize = true;
			this.lblReportUse.Location = new System.Drawing.Point(20, 144);
			this.lblReportUse.Name = "lblReportUse";
			this.lblReportUse.Size = new System.Drawing.Size(88, 15);
			this.lblReportUse.TabIndex = 1;
			this.lblReportUse.Text = "REPORT USE";
			this.ToolTip1.SetToolTip(this.lblReportUse, null);
			// 
			// cmbPaperWidth
			// 
			this.cmbPaperWidth.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbPaperWidth.Location = new System.Drawing.Point(172, 80);
			this.cmbPaperWidth.Name = "cmbPaperWidth";
			this.cmbPaperWidth.Size = new System.Drawing.Size(200, 40);
			this.cmbPaperWidth.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.cmbPaperWidth, null);
			// 
			// lblPaperWidth
			// 
			this.lblPaperWidth.AutoSize = true;
			this.lblPaperWidth.Location = new System.Drawing.Point(20, 94);
			this.lblPaperWidth.Name = "lblPaperWidth";
			this.lblPaperWidth.Size = new System.Drawing.Size(93, 15);
			this.lblPaperWidth.TabIndex = 5;
			this.lblPaperWidth.Text = "PAPER WIDTH";
			this.ToolTip1.SetToolTip(this.lblPaperWidth, null);
			// 
			// cmbDescriptions
			// 
			this.cmbDescriptions.Items.AddRange(new object[] {
            "Long Descriptions",
            "Short Descriptions" });
			this.cmbDescriptions.Location = new System.Drawing.Point(172, 30);
			this.cmbDescriptions.Name = "cmbDescriptions";
			this.cmbDescriptions.Size = new System.Drawing.Size(200, 40);
			this.cmbDescriptions.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.cmbDescriptions, null);
			// 
			// lblDescriptions
			// 
			this.lblDescriptions.AutoSize = true;
			this.lblDescriptions.Location = new System.Drawing.Point(20, 44);
			this.lblDescriptions.Name = "lblDescriptions";
			this.lblDescriptions.Size = new System.Drawing.Size(100, 15);
			this.lblDescriptions.TabIndex = 7;
			this.lblDescriptions.Text = "DESCRIPTIONS";
			this.ToolTip1.SetToolTip(this.lblDescriptions, null);
			// 
			// cmbAccountInformation
			// 
			this.cmbAccountInformation.Items.AddRange(new object[] {
            "Debits / Credits",
            "Net Total"});
			this.cmbAccountInformation.Location = new System.Drawing.Point(230, 30);
			this.cmbAccountInformation.Name = "cmbAccountInformation";
			this.cmbAccountInformation.Size = new System.Drawing.Size(200, 40);
			this.cmbAccountInformation.TabIndex = 29;
			this.ToolTip1.SetToolTip(this.cmbAccountInformation, null);
			this.cmbAccountInformation.SelectedIndexChanged += new System.EventHandler(this.cmbAccountInformation_CheckedChanged);
			// 
			// lblAccountInformation
			// 
			this.lblAccountInformation.AutoSize = true;
			this.lblAccountInformation.Location = new System.Drawing.Point(20, 44);
			this.lblAccountInformation.Name = "lblAccountInformation";
			this.lblAccountInformation.Size = new System.Drawing.Size(158, 15);
			this.lblAccountInformation.TabIndex = 30;
			this.lblAccountInformation.Text = "ACCOUNT INFORMATION";
			this.ToolTip1.SetToolTip(this.lblAccountInformation, null);
			// 
			// cmbPendingActivity
			// 
			this.cmbPendingActivity.Items.AddRange(new object[] {
            "Show Detail",
            "Show Summary Only",
            "Do Not Include"});
			this.cmbPendingActivity.Location = new System.Drawing.Point(230, 80);
			this.cmbPendingActivity.Name = "cmbPendingActivity";
			this.cmbPendingActivity.Size = new System.Drawing.Size(200, 40);
			this.cmbPendingActivity.TabIndex = 31;
			this.ToolTip1.SetToolTip(this.cmbPendingActivity, null);
			// 
			// lblPendingActivity
			// 
			this.lblPendingActivity.AutoSize = true;
			this.lblPendingActivity.Location = new System.Drawing.Point(20, 94);
			this.lblPendingActivity.Name = "lblPendingActivity";
			this.lblPendingActivity.Size = new System.Drawing.Size(123, 15);
			this.lblPendingActivity.TabIndex = 32;
			this.lblPendingActivity.Text = "PENDING ACTIVITY";
			this.ToolTip1.SetToolTip(this.lblPendingActivity, null);
			// 
			// chkAllInclusive
			// 
			this.chkAllInclusive.Location = new System.Drawing.Point(30, 490);
			this.chkAllInclusive.Name = "chkAllInclusive";
			this.chkAllInclusive.Size = new System.Drawing.Size(236, 27);
			this.chkAllInclusive.TabIndex = 29;
			this.chkAllInclusive.Text = "Show All Journal Information";
			this.ToolTip1.SetToolTip(this.chkAllInclusive, "This setting shows specific fields - not all formatting options will be available" +
        ".");
			this.chkAllInclusive.CheckedChanged += new System.EventHandler(this.chkAllInclusive_CheckedChanged);
			// 
			// cdgFont
			// 
			this.cdgFont.Name = "cdgFont";
			this.cdgFont.Size = new System.Drawing.Size(0, 0);
			this.ToolTip1.SetToolTip(this.cdgFont, null);
			// 
			// fraPreferences
			// 
			this.fraPreferences.AppearanceKey = "groupBoxLeftBorder";
			this.fraPreferences.Controls.Add(this.cmbReportUse);
			this.fraPreferences.Controls.Add(this.lblReportUse);
			this.fraPreferences.Controls.Add(this.cmbPaperWidth);
			this.fraPreferences.Controls.Add(this.lblPaperWidth);
			this.fraPreferences.Controls.Add(this.cmbDescriptions);
			this.fraPreferences.Controls.Add(this.lblDescriptions);
			this.fraPreferences.Location = new System.Drawing.Point(30, 100);
			this.fraPreferences.Name = "fraPreferences";
			this.fraPreferences.Size = new System.Drawing.Size(600, 186);
			this.fraPreferences.TabIndex = 20;
			this.fraPreferences.Text = "General Preferences";
			this.ToolTip1.SetToolTip(this.fraPreferences, null);
			// 
			// fraInformation
			// 
			this.fraInformation.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
			this.fraInformation.Controls.Add(this.cmbAccountInformation);
			this.fraInformation.Controls.Add(this.lblAccountInformation);
			this.fraInformation.Controls.Add(this.cmbPendingActivity);
			this.fraInformation.Controls.Add(this.lblPendingActivity);
			this.fraInformation.Location = new System.Drawing.Point(30, 292);
			this.fraInformation.Name = "fraInformation";
			this.fraInformation.Size = new System.Drawing.Size(450, 186);
			this.fraInformation.TabIndex = 18;
			this.fraInformation.Text = "Information To Be Reported";
			this.ToolTip1.SetToolTip(this.fraInformation, null);
			// 
			// chkShowLiquidatedEncumbranceActivity
			// 
			this.chkShowLiquidatedEncumbranceActivity.Location = new System.Drawing.Point(20, 140);
			this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
			this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(311, 27);
			this.chkShowLiquidatedEncumbranceActivity.TabIndex = 28;
			this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
			this.ToolTip1.SetToolTip(this.chkShowLiquidatedEncumbranceActivity, null);
			this.chkShowLiquidatedEncumbranceActivity.CheckedChanged += new System.EventHandler(this.chkShowLiquidatedEncumbranceActivity_CheckedChanged);
			// 
			// txtDescription
			// 
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(193, 30);
			this.txtDescription.MaxLength = 25;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(201, 40);
			this.txtDescription.TabIndex = 30;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 44);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(124, 18);
			this.lblDescription.TabIndex = 17;
			this.lblDescription.Text = "REPORT FORMAT";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(472, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(130, 48);
			this.cmdSave.TabIndex = 19;
			this.cmdSave.Text = "Save & Exit";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmCustomizeExpenseDetail
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomizeExpenseDetail";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Expense Detail Report Format";
			this.ToolTip1.SetToolTip(this, null);
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCustomizeExpenseDetail_Load);
			this.Activated += new System.EventHandler(this.frmCustomizeExpenseDetail_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeExpenseDetail_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeExpenseDetail_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAllInclusive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).EndInit();
			this.fraPreferences.ResumeLayout(false);
			this.fraPreferences.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
			this.fraInformation.ResumeLayout(false);
			this.fraInformation.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdSave;
	}
}
