﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptStandardChecks.
	/// </summary>
	public partial class rptStandardChecks : FCSectionReport
	{
		public static rptStandardChecks InstancePtr
		{
			get
			{
				return (rptStandardChecks)Sys.GetInstance(typeof(rptStandardChecks));
			}
		}

		protected rptStandardChecks _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptStandardChecks	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		// vbPorter upgrade warning: curCarryOver As Decimal	OnWrite(Decimal, short)
		Decimal curCarryOver;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		bool blnFirstRecord;
		string strSQL = "";
		int lngcheck;
		int WarrantCol;
		int DescriptionCol;
		int ReferenceCol;
		int CreditCol;
		int DiscountCol;
		int AmountCol;
		int lngCheckToReprint;
		int lngLastCheckToReprint;
		string strFooterVendorName = "";
		string strFooterAddress1 = "";
		string strFooterAddress2 = "";
		string strFooterAddress3 = "";
		string strFooterAddress4 = "";
		string strFooterCity = "";
		string strFooterState = "";
		string strFooterZip = "";
		string strFooterZip4 = "";
		string strFooterJournals = "";
		Decimal curFooterAmount;
		int lngFooterVendorNumber;
		string strFooterReprintedCheckNumber = "";
		int intFooterReportNumber;
		clsDRWrapper rsPrePaidJournalInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		int lngLowCheck;
		int lngHighCheck;
		bool blnInfoToPrint;
		clsReportPrinterFunctions instReportFunctions = new clsReportPrinterFunctions();
		bool blnDonePreview;
		bool blnPrinting;
		bool blnCarryOver;
		public bool blnIndividualCheck;
		bool blnInitial;
		int lngWarrant;
		// vbPorter upgrade warning: intPeriod As short --> As int	OnWrite(int, string)
		int intPeriod;
		// vbPorter upgrade warning: datPayDate As DateTime	OnWrite(DateTime, string)
		DateTime datPayDate;
		bool blnUnprintedChecks;
		int lngFirstCheck;
		int lngFirstReprintedCheck;
		int lngLastReprintedCheck;
		string strCheckMessage = "";
		bool blnEFTCheck;
		int lngCheckBank;

		public rptStandardChecks()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Checks";
		}

		public int CheckBankID
		{
			set
			{
				lngCheckBank = value;
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void ActiveReport_QueryClose(ref short Cancel, ref short CloseMode)
		{
			if (!blnDonePreview && blnPrinting)
			{
				MessageBox.Show("Checks have not finished processing.", "Checks Not Done", MessageBoxButtons.OK, MessageBoxIcon.Information, modal: false);
				Cancel = FCConvert.ToInt16(true);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			clsDRWrapper rsAPJournals = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (blnInitial)
			{
				if (!blnIndividualCheck)
				{
					rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'V' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(lngWarrant) + "')");
					if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
					{
						do
						{
							rsMasterJournal.Edit();
							rsMasterJournal.Set_Fields("Status", "C");
							rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
							rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
							rsMasterJournal.Set_Fields("Period", intPeriod);
							rsMasterJournal.Set_Fields("CheckDate", datPayDate);
							rsMasterJournal.Update(true);
							rsMasterJournal.MoveNext();
						}
						while (rsMasterJournal.EndOfFile() != true);
						rsMasterJournal.MoveFirst();
						do
						{
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							rsAPJournals.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + rsMasterJournal.Get_Fields("JournalNumber"));
							if (rsAPJournals.EndOfFile() != true && rsAPJournals.BeginningOfFile() != true)
							{
								rsAPJournals.OpenRecordset("SELECT * FROM JournalMaster WHERE ID = 0");
								rsAPJournals.AddNew();
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("JournalNumber", rsMasterJournal.Get_Fields("JournalNumber"));
								rsAPJournals.Set_Fields("Description", rsMasterJournal.Get_Fields_String("Description"));
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
								rsAPJournals.Set_Fields("Status", "E");
								rsAPJournals.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
								rsAPJournals.Set_Fields("StatusChangeDate", DateTime.Today);
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Period", rsMasterJournal.Get_Fields("Period"));
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								rsAPJournals.Set_Fields("Type", rsMasterJournal.Get_Fields("Type"));
								rsAPJournals.Update(true);
							}
							rsMasterJournal.MoveNext();
						}
						while (rsMasterJournal.EndOfFile() != true);
					}
				}
			}
			if (blnUnprintedChecks == false)
			{
				if (blnIndividualCheck)
				{
					frmPrintIndividualChecks.InstancePtr.Unload();
				}
				else
				{
					frmPrintChecks.InstancePtr.Unload();
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CarryOver = 1 ORDER BY CheckNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsMasterJournal.Execute("UPDATE APJournal SET CheckNumber = '" + FCConvert.ToString(FCConvert.ToInt32(rsInfo.Get_Fields("CheckNumber")) + 1) + "' WHERE CheckNumber = '" + rsInfo.Get_Fields("CheckNumber") + "' AND CheckDate = '" + rsInfo.Get_Fields_DateTime("CheckDate") + "' AND Warrant = '" + rsInfo.Get_Fields_Int32("WarrantNumber") + "'", "Budgetary");
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			if (lngCheckBank > 0)
			{
				cBankService bServ = new cBankService();
				bServ.UpdateLastCheckNumberForBankForCorrectCheckType(lngCheckBank, lngHighCheck, "AP");
			}
			if (blnInfoToPrint)
			{
				rsCheckInfo.OpenRecordset("SELECT * FROM LastChecksRun");
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					rsCheckInfo.Edit();
				}
				else
				{
					rsCheckInfo.OmitNullsOnInsert = true;
					rsCheckInfo.AddNew();
				}
				rsCheckInfo.Set_Fields("StartCheckNumber", lngLowCheck);
				rsCheckInfo.Set_Fields("EndCheckNumber", lngHighCheck);
				rsCheckInfo.Update(true);
			}
			else
			{
				this.Cancel();
			}
			blnDonePreview = true;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			string strLargeFonttoUse;
			string strSmallFonttoUse = "";
			int cnt;
			int const_PrintToolID;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_PrintToolID = 9950;
			//for (cnt = 0; cnt <= this.Toolbar.Tools.Count - 1; cnt++)
			//{
			//	if ("&Print..." == this.Toolbar.Tools(cnt).Caption)
			//	{
			//		this.Toolbar.Tools(cnt).ID = const_PrintToolID;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			if (Convert.ToBoolean(modBudgetaryAccounting.GetBDVariable("PreviewAPChecks")) == true)
			{
				blnDonePreview = false;
			}
			else
			{
				blnDonePreview = true;
			}
			strLargeFonttoUse = instReportFunctions.GetFont(this.Document.Printer.PrinterName, 12, "Roman 12cpi");
			if (strLargeFonttoUse != string.Empty)
			{
				instReportFunctions.SetReportFontsByTag(this, "Large", strLargeFonttoUse);
			}
			if (blnIndividualCheck)
			{
				blnInitial = frmPrintIndividualChecks.InstancePtr.cmbInitial.SelectedIndex == 0;
				lngWarrant = 0;
				intPeriod = DateTime.Today.Month;
				datPayDate = DateTime.Today;
				blnUnprintedChecks = false;
				lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.txtCheck.Text));
				if (!blnInitial)
				{
					lngFirstReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 1)));
					lngLastReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 1)));
				}
				else
				{
					lngFirstReprintedCheck = 0;
					lngLastReprintedCheck = 0;
				}
				strCheckMessage = frmPrintIndividualChecks.InstancePtr.txtCheckMessage.Text;
			}
			else
			{
				blnInitial = frmPrintChecks.InstancePtr.cmbInitial.SelectedIndex == 0;
				lngWarrant = frmPrintChecks.InstancePtr.TempWarrant;
				if (blnInitial)
				{
					intPeriod = FCConvert.ToInt32(frmPrintChecks.InstancePtr.cboPeriod.Text);
				}
				else
				{
					intPeriod = -1;
				}
				datPayDate = FCConvert.ToDateTime(frmPrintChecks.InstancePtr.txtPayDate.Text);
				blnUnprintedChecks = frmPrintChecks.InstancePtr.blnPrintingUnprintedChecks;
				if (blnInitial)
				{
					lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtCheck.Text));
				}
				else
				{
					lngFirstCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtFirstCheck.Text));
				}
				if (!blnInitial)
				{
					lngFirstReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtFirstReprintCheck.Text));
					lngLastReprintedCheck = FCConvert.ToInt32(FCConvert.ToDouble(frmPrintChecks.InstancePtr.txtLastReprintCheck.Text));
				}
				else
				{
					lngFirstReprintedCheck = 0;
					lngLastReprintedCheck = 0;
				}
				strCheckMessage = frmPrintChecks.InstancePtr.txtCheckMessage.Text;
			}
			blnFirstRecord = true;
			curTotal = 0;
			if (blnInitial)
			{
				lngcheck = lngFirstCheck;
				if (!blnIndividualCheck)
				{
					strSQL = "APJournal.JournalNumber IN (";
					for (counter = 1; counter <= frmPrintChecks.InstancePtr.vsJournals.Rows - 1; counter++)
					{
						if (FCUtils.CBool(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 0)) == true)
						{
							strSQL += FCConvert.ToString(Conversion.Val(frmPrintChecks.InstancePtr.vsJournals.TextMatrix(counter, 1))) + ", ";
						}
					}
					strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ") AND APJournal.Payable <= '" + FCConvert.ToString(datPayDate) + "' ";
				}
				else
				{
					if (FCUtils.CBool(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 3)) == false)
					{
						strSQL = "APJournal.JournalNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsJournals.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsJournals.Row, 0)))) + " AND APJournal.VendorNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 1)))) + " AND Isnull(Seperate, 0, 0 ";
					}
					else
					{
						strSQL = "APJournal.ID = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsVendors.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsVendors.Row, 0)))) + " ";
					}
				}
				FillTemp();
			}
			else
			{
				lngcheck = lngFirstCheck;
				lngCheckToReprint = lngFirstReprintedCheck;
				lngLastCheckToReprint = lngLastReprintedCheck;
				if (blnIndividualCheck)
				{
					rsVendorInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + FCConvert.ToString(lngCheckToReprint) + " AND VendorNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(frmPrintIndividualChecks.InstancePtr.vsChecks.TextMatrix(frmPrintIndividualChecks.InstancePtr.vsChecks.Row, 2)))) + " AND WarrantNumber = 0 ORDER BY CheckNumber");
				}
				else
				{
					rsVendorInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE Status = 'R' AND CheckNumber >= " + FCConvert.ToString(lngCheckToReprint) + " ORDER BY CheckNumber");
				}
			}
			lngLowCheck = lngcheck;
			if (blnInitial)
			{
				blnInfoToPrint = false;
			}
			else
			{
				blnInfoToPrint = true;
			}
		}

		private void FillTemp()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
			// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
			strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, false as EFT, true as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 ORDER BY TempVendorName";
			// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorNumber";
				}
				else
				{
					strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorName";
				}
			}
			else
			{
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName, VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, EFT, Prenote FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM" + " (SELECT DISTINCT VendorNumber, TempVendorName as VendorName, TempVendorAddress1 as CheckAddress1, TempVendorAddress2 as CheckAddress2, TempVendorAddress3 as CheckAddress3, TempVendorCity as CheckCity, TempVendorState as CheckState, TempVendorZip as CheckZip, TempVendorZip4 as CheckZip4, 0 as EFT, 1 as Prenote FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0) as TempVendorInfo ORDER BY VendorNumber";
			}
			rsVendorInfo.OpenRecordset(strTemp);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int intReturn;
			blnPrinting = true;
			this.Fields.Add("Binder");
			// If MDIParent.SysInfo1.OSPlatform > 1 Then
			// CheckDefaultPrinter Me.Document.Printer.PrinterName
			// MsgBox Me.Document.Printer.PrinterName
			//FC:TODO:AM
			//intReturn = modCustomPageSize.SelectForm("StandardCheckSize", this.Handle.ToInt32(), 215900, 177800); // 177800
			//																										 // MsgBox intReturn
			//if (intReturn > 0)
			//{
			//	this.Document.Printer.PaperSize = intReturn;
			//}
			// MsgBox "Page Width: " & Me.PageSettings.PaperWidth & vbNewLine & "Page Height: " & Me.PageSettings.PaperHeight
			// Else
			// set size of paper to the size of a standard pin fed check form
			// rptStandardChecks.Printer.PaperSize = 255
			// rptStandardChecks.Printer.PaperHeight = 10080
			// rptStandardChecks.Printer.PaperWidth = 12240
			// End If
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// ResetDefaultPrinter
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	string vbPorterVar = Tool.Caption;
		//	if (vbPorterVar == "&Print...")
		//	{
		//		if (blnDonePreview)
		//		{
		//			this.Document.Print(false);
		//		}
		//		else
		//		{
		//			MessageBox.Show("You may not print checks until they have all finished processing.", "Checks Not Finished", MessageBoxButtons.OK, MessageBoxIcon.Information, modal:false);
		//		}
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			int counter;
			int counter2 = 0;
			clsDRWrapper rsDiscount = new clsDRWrapper();
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			clsDRWrapper rsSepCred = new clsDRWrapper();
			if (rsVendorInfo.EndOfFile() != true)
			{
				curTotal = curCarryOver;
				for (counter = 0; counter <= 9; counter++)
				{
					if (blnInitial)
					{
						if (rsJournalInfo.EndOfFile() != true)
						{
							rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (Conversion.Val(rsJournalInfo.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								FillRows_2184(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curTotal += rsJournalInfo.Get_Fields("Amount");
							}
							else
							{
								rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								FillRows_2184(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								curTotal += rsJournalInfo.Get_Fields("Amount") - rsDiscount.Get_Fields("TotalDiscount") + rsCreditMemo.Get_Fields("TotalCredit");
							}
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("CheckNumber", lngcheck - 1);
							if (!blnIndividualCheck)
							{
								rsJournalInfo.Set_Fields("Status", "C");
								rsJournalInfo.Set_Fields("Period", intPeriod);
							}
							else
							{
								rsJournalInfo.Set_Fields("PrepaidCheck", true);
								rsJournalInfo.Set_Fields("PrintedIndividual", true);
							}
							rsJournalInfo.Set_Fields("Warrant", lngWarrant);
							rsJournalInfo.Set_Fields("CheckDate", datPayDate);
							rsJournalInfo.Set_Fields("EFTCheck", blnEFTCheck);
							rsJournalInfo.Update(true);
							if (Convert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
							{
								rsSepCred.OpenRecordset("SELECT * FROM APJournal WHERE SeperateCheckRecord = " + rsJournalInfo.Get_Fields_Int32("ID"));
								if (rsSepCred.EndOfFile() != true && rsSepCred.BeginningOfFile() != true)
								{
									counter2 = 1;
									do
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										FillRows_2184(counter2, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsSepCred.Get_Fields_String("Description"), rsSepCred.Get_Fields_String("Reference"), Strings.Format(FCConvert.ToInt16(rsSepCred.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curTotal += rsSepCred.Get_Fields("Amount");
										counter2 += 1;
										rsSepCred.Edit();
										rsSepCred.Set_Fields("CheckNumber", lngcheck - 1);
										if (!blnIndividualCheck)
										{
											rsSepCred.Set_Fields("Status", "C");
											rsSepCred.Set_Fields("Period", intPeriod);
										}
										else
										{
											rsSepCred.Set_Fields("PrepaidCheck", true);
											rsSepCred.Set_Fields("PrintedIndividual", true);
										}
										rsSepCred.Set_Fields("Warrant", lngWarrant);
										rsSepCred.Set_Fields("CheckDate", datPayDate);
										rsSepCred.Set_Fields("EFTCheck", blnEFTCheck);
										rsSepCred.Update(true);
										rsSepCred.MoveNext();
									}
									while (rsSepCred.EndOfFile() != true);
								}
								else
								{
									counter2 = 1;
								}
								for (counter2 = counter2; counter2 <= 9; counter2++)
								{
									FillRows_2184(counter2, "", "", "", "", "", "");
								}
								rsJournalInfo.MoveNext();
								goto GetVendorInfo;
							}
						}
						else
						{
							FillRows_2184(counter, "", "", "", "", "", "");
						}
					}
					else
					{
						if (rsJournalInfo.EndOfFile() != true)
						{
							rsDiscount.OpenRecordset("SELECT SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (Conversion.Val(rsJournalInfo.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
							{
								// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								FillRows_2184(counter, modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("Warrant"), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curTotal += rsJournalInfo.Get_Fields("Amount");
							}
							else
							{
								rsCreditMemo.OpenRecordset("SELECT SUM(Amount) as TotalCredit FROM APJournalDetail WHERE Amount < 0 AND APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
								// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								FillRows_2184(counter, modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("Warrant"), 4), rsJournalInfo.Get_Fields_String("Description"), rsJournalInfo.Get_Fields_String("Reference"), Strings.Format(Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")) * -1, "#,##0.00"), Strings.Format(Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")), "#,##0.00"), Strings.Format(Conversion.Val(rsJournalInfo.Get_Fields("Amount")) - Conversion.Val(rsDiscount.Get_Fields("TotalDiscount")) + Conversion.Val(rsCreditMemo.Get_Fields("TotalCredit")), "#,##0.00"));
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCredit] not found!! (maybe it is an alias?)
								curTotal += rsJournalInfo.Get_Fields("Amount") - rsDiscount.Get_Fields("TotalDiscount") + rsCreditMemo.Get_Fields("TotalCredit");
							}
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("CheckNumber", lngcheck - 1);
							rsJournalInfo.Set_Fields("CheckDate", datPayDate);
							rsJournalInfo.Set_Fields("EFTCheck", blnEFTCheck);
							rsJournalInfo.Update(true);
							if (Convert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("Seperate")))
							{
								rsSepCred.OpenRecordset("SELECT * FROM APJournal WHERE SeperateCheckRecord = " + rsJournalInfo.Get_Fields_Int32("ID"));
								if (rsSepCred.EndOfFile() != true && rsSepCred.BeginningOfFile() != true)
								{
									counter2 = 1;
									do
									{
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										FillRows_2184(counter2, modValidateAccount.GetFormat_6(FCConvert.ToString(lngWarrant), 4), rsSepCred.Get_Fields_String("Description"), rsSepCred.Get_Fields_String("Reference"), Strings.Format(FCConvert.ToInt16(rsSepCred.Get_Fields("Amount")) * -1, "#,##0.00"), "0.00", "0.00");
										// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
										curTotal += rsSepCred.Get_Fields("Amount");
										counter2 += 1;
										rsSepCred.Edit();
										rsSepCred.Set_Fields("CheckNumber", lngcheck - 1);
										if (!blnIndividualCheck)
										{
											rsSepCred.Set_Fields("Status", "C");
											rsSepCred.Set_Fields("Period", intPeriod);
										}
										else
										{
											rsSepCred.Set_Fields("PrepaidCheck", true);
											rsSepCred.Set_Fields("PrintedIndividual", true);
										}
										rsSepCred.Set_Fields("Warrant", lngWarrant);
										rsSepCred.Set_Fields("CheckDate", datPayDate);
										rsSepCred.Set_Fields("EFTCheck", blnEFTCheck);
										rsSepCred.Update(true);
										rsSepCred.MoveNext();
									}
									while (rsSepCred.EndOfFile() != true);
								}
								else
								{
									counter2 = 1;
								}
								for (counter2 = counter2; counter2 <= 9; counter2++)
								{
									FillRows_2184(counter2, "", "", "", "", "", "");
								}
								rsJournalInfo.MoveNext();
								goto GetVendorInfo;
							}
						}
						else
						{
							FillRows_2184(counter, "", "", "", "", "", "");
						}
					}
					rsJournalInfo.MoveNext();
				}
				if (rsJournalInfo.EndOfFile() != true)
				{
					curCarryOver = curTotal;
					curTotal = 0;
				}
				else
				{
					curCarryOver = 0;
				}
				GetVendorInfo:
				;
				curFooterAmount = curTotal;
				strFooterVendorName = FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"));
				lngFooterVendorNumber = FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber"));
				strFooterAddress1 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
				strFooterAddress2 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
				strFooterAddress3 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						strFooterAddress4 = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				strFooterCity = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
				strFooterState = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
				strFooterZip = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
				strFooterZip4 = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
				if (!blnInitial)
				{
					strFooterJournals = rsJournalInfo.Name();
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					strFooterReprintedCheckNumber = FCConvert.ToString(rsVendorInfo.Get_Fields("CheckNumber"));
					intFooterReportNumber = FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("ReportNumber"));
				}
				else
				{
					strFooterJournals = rsPrePaidJournalInfo.Name();
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			string strAmount = "";
			int intPlaceHolder = 0;
			string strCheckSQL = "";
			clsDRWrapper rsDupChecks = new clsDRWrapper();
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			clsDRWrapper rsCheckRecJournalInfo = new clsDRWrapper();
			int intBank = 0;
			clsDRWrapper rsVendorData = new clsDRWrapper();
			if (blnInfoToPrint)
			{
				if (curCarryOver != 0)
				{
					fldAmount.Text = "";
					fldAmount2.Text = "";
					fldDate2.Text = "";
					fldMuniName.Text = "";
					lblAmount2.Visible = false;
				}
				else
				{
					fldAmount.Text = Strings.Format(curTotal, "#,##0.00");
					fldAmount.Text = Strings.StrDup(13 - fldAmount.Text.Length, "*") + fldAmount.Text;
					fldAmount2.Text = "$" + fldAmount.Text;
					fldDate2.Text = Strings.Format(datPayDate, "MM/dd/yy");
					fldMuniName.Text = modGlobalConstants.Statics.MuniName;
					lblAmount2.Visible = true;
				}
				fldVendorName.Text = Strings.Trim(strFooterVendorName);
				fldAddress1.Text = Strings.Trim(strFooterAddress1);
				fldAddress2.Text = Strings.Trim(strFooterAddress2);
				fldAddress3.Text = Strings.Trim(strFooterAddress3);
				fldAddress4.Text = Strings.Trim(strFooterAddress4);
				if (Strings.Trim(fldAddress3.Text) == "")
				{
					fldAddress3.Text = fldAddress4.Text;
					fldAddress4.Text = "";
				}
				if (Strings.Trim(fldAddress2.Text) == "")
				{
					fldAddress2.Text = fldAddress3.Text;
					fldAddress3.Text = "";
				}
				if (Strings.Trim(fldAddress1.Text) == "")
				{
					fldAddress1.Text = fldAddress2.Text;
					fldAddress2.Text = "";
				}
				if (curTotal != 0)
				{
					strAmount = modConvertAmountToString.ConvertAmountToString(curTotal);
					rsVendorData.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					if (rsVendorData.EndOfFile() != true && rsVendorData.BeginningOfFile() != true)
					{
						blnEFTCheck = Convert.ToBoolean(rsVendorData.Get_Fields_Boolean("EFT")) && !Convert.ToBoolean(rsVendorData.Get_Fields_Boolean("Prenote"));
					}
					if (!blnEFTCheck)
					{
						lblVoid.Visible = false;
						fldCarryOver.Visible = false;
						fldEFT.Visible = false;
					}
					else
					{
						lblVoid.Visible = true;
						fldCarryOver.Visible = false;
						fldEFT.Visible = true;
					}
					if (strAmount.Length > 70)
					{
						intPlaceHolder = Strings.InStr(55, strAmount, " ", CompareConstants.vbBinaryCompare);
						fldTextAmount1.Text = Strings.Left(strAmount, intPlaceHolder) + Strings.StrDup(70 - Strings.Left(strAmount, intPlaceHolder).Length, "*");
						fldTextAmount2.Text = Strings.Right(strAmount, strAmount.Length - intPlaceHolder) + Strings.StrDup(70 - Strings.Right(strAmount, strAmount.Length - intPlaceHolder).Length, "*");
					}
					else
					{
						fldTextAmount1.Text = "";
						fldTextAmount2.Text = strAmount + Strings.StrDup(70 - strAmount.Length, "*");
					}
				}
				else
				{
					if (curCarryOver != 0)
					{
						strAmount = "";
						lblVoid.Visible = true;
						fldCarryOver.Visible = true;
						fldTextAmount1.Text = "";
						fldTextAmount2.Text = "";
					}
					else
					{
						strAmount = "ZERO 00/100";
						lblVoid.Visible = true;
						if (strAmount.Length > 70)
						{
							intPlaceHolder = Strings.InStr(55, strAmount, " ", CompareConstants.vbBinaryCompare);
							fldTextAmount1.Text = Strings.Left(strAmount, intPlaceHolder) + Strings.StrDup(70 - Strings.Left(strAmount, intPlaceHolder).Length, "*");
							fldTextAmount2.Text = Strings.Right(strAmount, strAmount.Length - intPlaceHolder) + Strings.StrDup(70 - Strings.Right(strAmount, strAmount.Length - intPlaceHolder).Length, "*");
						}
						else
						{
							fldTextAmount1.Text = "";
							fldTextAmount2.Text = strAmount + Strings.StrDup(70 - strAmount.Length, "*");
						}
					}
				}
				rsCheckInfo.OmitNullsOnInsert = true;
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
				rsCheckInfo.AddNew();
				rsCheckInfo.Set_Fields("CheckNumber", lngcheck - 1);
				rsCheckInfo.Set_Fields("VendorNumber", lngFooterVendorNumber);
				rsCheckInfo.Set_Fields("VendorName", Strings.Trim(strFooterVendorName));
				rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(strFooterAddress1));
				rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(strFooterAddress2));
				rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(strFooterAddress3));
				rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(strFooterCity));
				rsCheckInfo.Set_Fields("CheckState", Strings.Trim(strFooterState));
				rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(strFooterZip));
				rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(strFooterZip4));
				rsCheckInfo.Set_Fields("WarrantNumber", lngWarrant);
				rsCheckInfo.Set_Fields("Amount", curTotal);
				rsCheckInfo.Set_Fields("CheckDate", datPayDate);
				rsCheckInfo.Set_Fields("EFTCheck", blnEFTCheck);
				if (curCarryOver != 0)
				{
					rsCheckInfo.Set_Fields("CarryOver", true);
				}
				else
				{
					rsCheckInfo.Set_Fields("CarryOver", false);
				}
				rsCheckRec.OmitNullsOnInsert = true;
				strCheckSQL = strFooterJournals;
				if (!blnInitial)
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck - 1) + "' ORDER BY IsNull(Seperate, 0) DESC";
					rsCheckInfo.Set_Fields("ReprintedCheck", true);
					rsCheckInfo.Set_Fields("OldCheckNumber", strFooterReprintedCheckNumber);
					rsCheckInfo.Set_Fields("ReportNumber", intFooterReportNumber);
					if (!blnIndividualCheck)
					{
						rsCheckRecJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE convert(int, IsNull(CheckNumber, 0)) = " + FCConvert.ToString(lngcheck - 1) + " AND Warrant = '" + FCConvert.ToString(lngWarrant) + "'");
						if (rsCheckRecJournalInfo.EndOfFile() != true && rsCheckRecJournalInfo.BeginningOfFile() != true)
						{
							if (FCConvert.ToString(rsCheckRecJournalInfo.Get_Fields_String("Status")) != "C")
							{
								rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + FCConvert.ToString(Conversion.Val(strFooterReprintedCheckNumber)));
								rsCheckRec.Edit();
								// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
								intBank = FCConvert.ToInt32(rsCheckRec.Get_Fields("BankNumber"));
								rsCheckRec.Set_Fields("Status", "V");
								rsCheckRec.Update();
								rsCheckRec.AddNew();
								rsCheckRec.Set_Fields("CheckNumber", lngcheck - 1);
								rsCheckRec.Set_Fields("Type", "1");
								rsCheckRec.Set_Fields("CheckDate", datPayDate);
								rsCheckRec.Set_Fields("Name", Strings.Trim(strFooterVendorName));
								rsCheckRec.Set_Fields("Amount", curTotal);
								if (curCarryOver != 0)
								{
									rsCheckRec.Set_Fields("Status", "V");
								}
								else
								{
									rsCheckRec.Set_Fields("Status", "1");
								}
								rsCheckRec.Set_Fields("StatusDate", DateTime.Today);
								rsCheckRec.Set_Fields("BankNumber", intBank);
								rsCheckRec.Update(true);
							}
						}
					}
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + FCConvert.ToString(lngcheck - 1) + "' ORDER BY IsNull(Seperate, 0) DESC";
					rsCheckInfo.Set_Fields("ReprintedCheck", false);
					rsCheckInfo.Set_Fields("OldCheckNumber", "");
					if (blnIndividualCheck)
					{
						rsCheckInfo.Set_Fields("ReportNumber", -1);
					}
					else
					{
						rsCheckInfo.Set_Fields("ReportNumber", 0);
					}
				}
				rsCheckInfo.Set_Fields("Journals", strCheckSQL);
				if (curTotal != 0)
				{
					if (blnIndividualCheck)
					{
						rsCheckInfo.Set_Fields("Status", "P");
					}
					else
					{
						rsCheckInfo.Set_Fields("Status", "R");
					}
				}
				else
				{
					rsCheckInfo.Set_Fields("Status", "V");
				}
				if (blnIndividualCheck)
				{
					rsCheckInfo.Set_Fields("PrintedIndividual", true);
				}
				rsCheckInfo.Update(true);
				curTotal = 0;
				if (lngFooterVendorNumber != 0)
				{
					rsCheckInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(lngFooterVendorNumber));
					if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
					{
						rsCheckInfo.Edit();
						rsCheckInfo.Set_Fields("LastPayment", DateTime.Today);
						rsCheckInfo.Set_Fields("CheckNumber", lngcheck - 1);
						rsCheckInfo.Update();
					}
				}
				if (!blnInitial)
				{
					if (rsVendorInfo.EndOfFile())
					{
						rsVendorInfo.MovePrevious();
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsVendorInfo.Get_Fields("CheckNumber")) != strFooterReprintedCheckNumber)
					{
						rsVendorInfo.MovePrevious();
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsDupChecks.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsVendorInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = " + FCConvert.ToString(lngWarrant) + " AND ID <> " + rsVendorInfo.Get_Fields_Int32("ID"));
						if (rsDupChecks.EndOfFile() != true && rsDupChecks.BeginningOfFile() != true)
						{
							rsVendorInfo.Delete();
							rsVendorInfo.Update();
						}
						else
						{
							rsVendorInfo.Edit();
							rsVendorInfo.Set_Fields("Status", "V");
							rsVendorInfo.Update();
							rsVendorInfo.MoveNext();
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsDupChecks.OpenRecordset("SELECT * FROM TempCheckFile WHERE CheckNumber = " + rsVendorInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = " + FCConvert.ToString(lngWarrant) + " AND ID <> " + rsVendorInfo.Get_Fields_Int32("ID"));
						if (rsDupChecks.EndOfFile() != true && rsDupChecks.BeginningOfFile() != true)
						{
							rsVendorInfo.Delete();
							rsVendorInfo.Update();
						}
						else
						{
							rsVendorInfo.Edit();
							rsVendorInfo.Set_Fields("Status", "V");
							rsVendorInfo.Update();
						}
					}
				}
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsCheckMessage = new clsDRWrapper();
			if (rsVendorInfo.EndOfFile() != true)
			{
				fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + "  " + rsVendorInfo.Get_Fields_String("VendorName");
				fldDate.Text = Strings.Format(datPayDate, "MM/dd/yy");
				fldCheck.Text = lngcheck.ToString();
				lngHighCheck = lngcheck;
				if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) != 0)
				{
					rsCheckMessage.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					if (rsCheckMessage.EndOfFile() != true && rsCheckMessage.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage"))) != "")
						{
							fldCheckMessage.Text = Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage")));
							if (Convert.ToBoolean(rsCheckMessage.Get_Fields_Boolean("OneTimeCheckMessage")))
							{
								rsCheckMessage.Edit();
								rsCheckMessage.Set_Fields("CheckMessage", "");
								rsCheckMessage.Set_Fields("OneTimeCheckMessage", false);
								rsCheckMessage.Update();
							}
						}
						else
						{
							fldCheckMessage.Text = strCheckMessage;
						}
					}
					else
					{
						fldCheckMessage.Text = strCheckMessage;
					}
				}
				else
				{
					fldCheckMessage.Text = strCheckMessage;
				}
				lngcheck += 1;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool blnDontContinue = false;
			if (blnInitial)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					blnDontContinue = true;
					if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
						rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND IsNull(SeperateCheckRecord, 0) = 0 AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
						rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC");
					}
					//goto CheckJournal;
				}
				//else
				{
					CheckJournal:
					;
					SavePrePaidJournalInfo();
					if (rsJournalInfo.EndOfFile() == true)
					{
						rsVendorInfo.MoveNext();
						CheckNextVendor:
						;
						if (rsVendorInfo.EndOfFile() == true)
						{
							if (blnDontContinue)
							{
								ActiveReport_ReportEnd(sender, EventArgs.Empty);
								rptStandardChecks.InstancePtr.Cancel();
							}
							else
							{
								eArgs.EOF = true;
								return;
							}
						}
						else
						{
							blnDontContinue = false;
							if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
								rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC");
							}
							else
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND IsNull(SeperateCheckRecord, 0) = 0 AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) = '' ORDER BY IsNull(Seperate, 0) DESC, CreditMemoRecord DESC, ID");
								rsPrePaidJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND rtrim(Isnull(CheckNumber, '')) <> '' ORDER BY IsNull(Seperate, 0) DESC");
							}
							SavePrePaidJournalInfo();
							if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
							{
								rptStandardChecks.InstancePtr.Fields["Binder"].Value = rptStandardChecks.InstancePtr.Fields["Binder"].Value + "1";
								eArgs.EOF = false;
								blnInfoToPrint = true;
								return;
							}
							else
							{
								rsVendorInfo.MoveNext();
								goto CheckNextVendor;
							}
						}
					}
					else
					{
						blnDontContinue = false;
						rptStandardChecks.InstancePtr.Fields["Binder"].Value = rptStandardChecks.InstancePtr.Fields["Binder"].Value + "1";
						eArgs.EOF = false;
						blnInfoToPrint = true;
						return;
					}
				}
			}
			else
			{
				bool executeGetNextReprintVendor = false;
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) != "P" || rsVendorInfo.Get_Fields_Boolean("PrintedIndividual") == true)
					{
						rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
						if (intPeriod < 1)
						{
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							intPeriod = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Period"));
						}
					}
					else
					{
						executeGetNextReprintVendor = true;
						goto GetNextReprintVendor;
					}
				}
				else
				{
					if (rsJournalInfo.EndOfFile() == true)
					{
						executeGetNextReprintVendor = true;
						goto GetNextReprintVendor;
					}
					else
					{
						rptStandardChecks.InstancePtr.Fields["Binder"].Value = rptStandardChecks.InstancePtr.Fields["Binder"].Value + "1";
						eArgs.EOF = false;
						blnInfoToPrint = true;
						return;
					}
				}
				GetNextReprintVendor:
				;
				if (executeGetNextReprintVendor)
				{
					rsVendorInfo.MoveNext();
					if (rsVendorInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
						return;
					}
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					else if (rsVendorInfo.Get_Fields("CheckNumber") > lngLastCheckToReprint)
					{
						eArgs.EOF = true;
						return;
					}
					else if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) == "P" && rsVendorInfo.Get_Fields_Boolean("PrintedIndividual") != true)
					{
						goto GetNextReprintVendor;
					}
					else
					{
						rsJournalInfo.OpenRecordset(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Journals")).Replace("Separate", "Seperate"));
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							if (intPeriod < 1)
							{
								// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
								intPeriod = FCConvert.ToInt32(rsJournalInfo.Get_Fields("Period"));
							}
							rptStandardChecks.InstancePtr.Fields["Binder"].Value = rptStandardChecks.InstancePtr.Fields["Binder"].Value + "1";
							eArgs.EOF = false;
							blnInfoToPrint = true;
							return;
						}
						else
						{
							goto GetNextReprintVendor;
						}
					}
				}
			}
		}

		private void SavePrePaidJournalInfo()
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			clsDRWrapper rsTotal = new clsDRWrapper();
			string strCheckSQL = "";
			rsCheckInfo.OmitNullsOnInsert = true;
			while (rsPrePaidJournalInfo.EndOfFile() != true)
			{
				if (FCConvert.ToString(rsPrePaidJournalInfo.Get_Fields_String("Status")) == "V")
				{
					if (!Convert.ToBoolean(rsPrePaidJournalInfo.Get_Fields_Boolean("PrintedIndividual")))
					{
						bool executeAddNew = false;
						if (Information.IsDate(rsPrePaidJournalInfo.Get_Fields("CheckDate")))
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = " + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + " AND CheckDate = '" + rsPrePaidJournalInfo.Get_Fields_DateTime("CheckDate") + "'");
						}
						else
						{
							rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE ID = 0");
							executeAddNew = true;
							goto AddNew;
						}
						if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
						{
							rsCheckInfo.Edit();
						}
						else
						{
							executeAddNew = true;
							goto AddNew;
						}
						AddNew:
						;
						if (executeAddNew)
						{
							rsCheckInfo.AddNew();
						}
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCheckInfo.Set_Fields("CheckNumber", rsPrePaidJournalInfo.Get_Fields("CheckNumber"));
						rsCheckInfo.Set_Fields("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber"));
						rsCheckInfo.Set_Fields("VendorName", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName"))));
						rsCheckInfo.Set_Fields("CheckAddress1", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))));
						rsCheckInfo.Set_Fields("CheckAddress2", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"))));
						rsCheckInfo.Set_Fields("CheckAddress3", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"))));
						rsCheckInfo.Set_Fields("CheckCity", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"))));
						rsCheckInfo.Set_Fields("CheckState", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))));
						rsCheckInfo.Set_Fields("CheckZip", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
						rsCheckInfo.Set_Fields("CheckZip4", Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
						rsCheckInfo.Set_Fields("WarrantNumber", lngWarrant);
						rsTotal.OpenRecordset("SELECT SUM(Amount) as TotalAmt, SUM(Discount) as TotalDiscount FROM APJournalDetail WHERE APJournalID = " + rsPrePaidJournalInfo.Get_Fields_Int32("ID"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Field [TotalAmt] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
						rsCheckInfo.Set_Fields("Amount", Conversion.Val(rsCheckInfo.Get_Fields("Amount")) + Conversion.Val(rsTotal.Get_Fields("TotalAmt")) - Conversion.Val(rsTotal.Get_Fields("TotalDiscount")));
						rsCheckInfo.Set_Fields("CheckDate", datPayDate);
						strCheckSQL = rsPrePaidJournalInfo.Name();
						if (!blnInitial)
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "=", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
							strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - ((strCheckSQL.Length + 2) - Strings.InStrRev(strCheckSQL, "<", -1, CompareConstants.vbTextCompare/*?*/))) + " = '" + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + "' ORDER BY IsNull(Seperate, 0) DESC";
						}
						rsCheckInfo.Set_Fields("Journals", strCheckSQL);
						rsCheckInfo.Set_Fields("Status", "P");
						rsCheckInfo.Update(true);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber") + " AND CheckNumber = " + rsPrePaidJournalInfo.Get_Fields("CheckNumber") + " AND WarrantNumber = 0");
						if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
						{
							do
							{
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								UpdateIndividualCheck_8(FCConvert.ToInt32(rsCheckInfo.Get_Fields("CheckNumber")), FCConvert.ToInt32(rsPrePaidJournalInfo.Get_Fields_Int32("VendorNumber")));
								rsCheckInfo.MoveNext();
							}
							while (rsCheckInfo.EndOfFile() != true);
						}
					}
					if (!blnIndividualCheck)
					{
						rsPrePaidJournalInfo.Edit();
						rsPrePaidJournalInfo.Set_Fields("Status", "C");
						rsPrePaidJournalInfo.Set_Fields("Warrant", lngWarrant);
						rsPrePaidJournalInfo.Set_Fields("CheckDate", datPayDate);
						rsPrePaidJournalInfo.Set_Fields("Period", intPeriod);
						rsPrePaidJournalInfo.Update(true);
					}
				}
				rsPrePaidJournalInfo.MoveNext();
			}
		}

		private void UpdateIndividualCheck_2(int lngTempCheck, int lngVendorNumber)
		{
			UpdateIndividualCheck(ref lngTempCheck, ref lngVendorNumber);
		}

		private void UpdateIndividualCheck_8(int lngTempCheck, int lngVendorNumber)
		{
			UpdateIndividualCheck(ref lngTempCheck, ref lngVendorNumber);
		}

		private void UpdateIndividualCheck(ref int lngTempCheck, ref int lngVendorNumber)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM TempCheckFile WHERE VendorNumber = " + FCConvert.ToString(lngVendorNumber) + " AND CheckNumber = " + FCConvert.ToString(lngTempCheck) + " AND WarrantNumber = 0");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(rsTemp.Get_Fields_String("OldCheckNumber")) != "")
				{
					UpdateIndividualCheck_2(FCConvert.ToInt32(rsTemp.Get_Fields_String("OldCheckNumber")), lngVendorNumber);
				}
				rsTemp.Edit();
				rsTemp.Set_Fields("WarrantNumber", lngWarrant);
				rsTemp.Set_Fields("ReportNumber", 0);
				rsTemp.Update();
			}
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void FillRows_2184(int intRow, string strWarr, string strDesc, string strRef, string curCredit, string curDisc, string curAmt)
		{
			FillRows(ref intRow, ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
		}

		private void FillRows(ref int intRow, ref string strWarr, ref string strDesc, ref string strRef, ref string curCredit, ref string curDisc, ref string curAmt)
		{
			switch (intRow)
			{
				case 0:
					{
						FillRow1(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 1:
					{
						FillRow2(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 2:
					{
						FillRow3(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 3:
					{
						FillRow4(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 4:
					{
						FillRow5(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 5:
					{
						FillRow6(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 6:
					{
						FillRow7(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 7:
					{
						FillRow8(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 8:
					{
						FillRow9(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 9:
					{
						FillRow10(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
			}
			//end switch
		}

		private void FillRow1(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant1.Text = strWarr;
			fldDescription1.Text = strDesc;
			fldReference1.Text = strRef;
			fldDiscount1.Text = strDisc;
			fldAmt1.Text = strAmt;
			fldCredit1.Text = strCredit;
		}

		private void FillRow2(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant2.Text = strWarr;
			fldDescription2.Text = strDesc;
			fldReference2.Text = strRef;
			fldDiscount2.Text = strDisc;
			fldAmt2.Text = strAmt;
			fldCredit2.Text = strCredit;
		}

		private void FillRow3(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant3.Text = strWarr;
			fldDescription3.Text = strDesc;
			fldReference3.Text = strRef;
			fldDiscount3.Text = strDisc;
			fldAmt3.Text = strAmt;
			fldCredit3.Text = strCredit;
		}

		private void FillRow4(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant4.Text = strWarr;
			fldDescription4.Text = strDesc;
			fldReference4.Text = strRef;
			fldDiscount4.Text = strDisc;
			fldAmt4.Text = strAmt;
			fldCredit4.Text = strCredit;
		}

		private void FillRow5(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant5.Text = strWarr;
			fldDescription5.Text = strDesc;
			fldReference5.Text = strRef;
			fldDiscount5.Text = strDisc;
			fldAmt5.Text = strAmt;
			fldCredit5.Text = strCredit;
		}

		private void FillRow6(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant6.Text = strWarr;
			fldDescription6.Text = strDesc;
			fldReference6.Text = strRef;
			fldDiscount6.Text = strDisc;
			fldAmt6.Text = strAmt;
			fldCredit6.Text = strCredit;
		}

		private void FillRow7(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant7.Text = strWarr;
			fldDescription7.Text = strDesc;
			fldReference7.Text = strRef;
			fldDiscount7.Text = strDisc;
			fldAmt7.Text = strAmt;
			fldCredit7.Text = strCredit;
		}

		private void FillRow8(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant8.Text = strWarr;
			fldDescription8.Text = strDesc;
			fldReference8.Text = strRef;
			fldDiscount8.Text = strDisc;
			fldAmt8.Text = strAmt;
			fldCredit8.Text = strCredit;
		}

		private void FillRow9(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant9.Text = strWarr;
			fldDescription9.Text = strDesc;
			fldReference9.Text = strRef;
			fldDiscount9.Text = strDisc;
			fldAmt9.Text = strAmt;
			fldCredit9.Text = strCredit;
		}

		private void FillRow10(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant10.Text = strWarr;
			fldDescription10.Text = strDesc;
			fldReference10.Text = strRef;
			fldDiscount10.Text = strDisc;
			fldAmt10.Text = strAmt;
			fldCredit10.Text = strCredit;
		}

		public void Init(string strPrinter, bool blnModal, int lngBankID)
		{
			lngCheckBank = lngBankID;
			frmReportViewer.InstancePtr.Init(this, strPrinter, 1, this.PageSettings.Duplex == System.Drawing.Printing.Duplex.Default, false, "Pages", false, string.Empty, "TRIO Software", false, false, showModal: true);
		}

		private void rptStandardChecks_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptStandardChecks.Caption	= "Checks";
			//rptStandardChecks.Icon	= "rptStandardChecks.dsx":0000";
			//rptStandardChecks.Left	= 0;
			//rptStandardChecks.Top	= 0;
			//rptStandardChecks.Width	= 11880;
			//rptStandardChecks.Height	= 8595;
			//rptStandardChecks.StartUpPosition	= 3;
			//rptStandardChecks.SectionData	= "rptStandardChecks.dsx":058A;
			//End Unmaped Properties
		}
	}
}
