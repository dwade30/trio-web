﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBalancingReport.
	/// </summary>
	public partial class rptBalancingReport : BaseSectionReport
	{
		public static rptBalancingReport InstancePtr
		{
			get
			{
				return (rptBalancingReport)Sys.GetInstance(typeof(rptBalancingReport));
			}
		}

		protected rptBalancingReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsCheckInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBalancingReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int TitleCol;
		int AmountCol;
		int CountCol;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		int intTotalCount;
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
		Decimal curTotalAmount;
		int PageCounter;
		bool blnFirstAccount;
		bool blnChecksDone;

		public rptBalancingReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Balancing Report";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status > '1' AND Type > '2' AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY Type, Status, CheckNumber, CheckDate");
			rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Status > '1' AND Type <= '2' AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY Status, CheckNumber, CheckDate");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Cancel();
					//MDIParent.InstancePtr.GRID.Focus();
					return;
				}
			}
			blnFirstAccount = true;
			blnChecksDone = false;
			intTotalCount = 0;
			curTotalAmount = 0;
			lblBank.Text = App.MainForm.StatusBarText3;
			lblStatement.Text = "Statement Date:  " + Strings.Format(modBudgetaryAccounting.GetBDVariable("StatementDate"), "MM/dd/yy");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnChecksDone)
			{
				//FC:FINAL:MSH - can't implicitly convert int to string (same with issue #711)
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar = rsInfo.Get_Fields("Type");
				if (vbPorterVar == "3")
				{
					fldType.Text = "DP";
				}
				else if (vbPorterVar == "4")
				{
					fldType.Text = "RT";
				}
				else if (vbPorterVar == "5")
				{
					fldType.Text = "IN";
				}
				else if (vbPorterVar == "6")
				{
					fldType.Text = "OC";
				}
				else if (vbPorterVar == "7")
				{
					fldType.Text = "OD";
				}
				fldCheckDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar1 = rsInfo.Get_Fields_String("Status");
				if (vbPorterVar1 == "1")
				{
					fldCode.Text = "ISSD";
				}
				else if (vbPorterVar1 == "2")
				{
					fldCode.Text = "O/S";
				}
				else if (vbPorterVar1 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar1 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar1 == "D")
				{
					fldCode.Text = "DLTD";
				}
				fldStatusDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy");
				fldPayee.Text = rsInfo.Get_Fields_String("Name");
				intTotalCount += 1;
				//FC:FINAL:MSH - Issue #742: can't implicitly convert to decimal
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
			}
			else
			{
				//FC:FINAL:MSH - can't implicitly convert int to string (same with issue #711)
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar2 = rsCheckInfo.Get_Fields("Type");
				if (vbPorterVar2 == "1")
				{
					fldType.Text = "AP";
				}
				else if (vbPorterVar2 == "2")
				{
					fldType.Text = "PY";
				}
				fldCheckDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsCheckInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar3 = rsCheckInfo.Get_Fields_String("Status");
				if (vbPorterVar3 == "1")
				{
					fldCode.Text = "ISSD";
				}
				else if (vbPorterVar3 == "2")
				{
					fldCode.Text = "O/S";
				}
				else if (vbPorterVar3 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar3 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar3 == "D")
				{
					fldCode.Text = "DLTD";
				}
				fldStatusDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy");
				fldPayee.Text = rsCheckInfo.Get_Fields_String("Name");
				intTotalCount += 1;
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += FCConvert.ToDecimal(rsCheckInfo.Get_Fields("Amount"));
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotalAmount, "#,##0.00");
			fldCount.Text = intTotalCount.ToString();
			curTotalAmount = 0;
			intTotalCount = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (blnChecksDone)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar = rsInfo.Get_Fields("Type");
				if (vbPorterVar == "3")
				{
					fldTitle.Text = "Deposits";
				}
				else if (vbPorterVar == "4")
				{
					fldTitle.Text = "Returned Checks";
				}
				else if (vbPorterVar == "5")
				{
					fldTitle.Text = "Interest";
				}
				else if (vbPorterVar == "6")
				{
					fldTitle.Text = "Other Credits";
				}
				else if (vbPorterVar == "7")
				{
					fldTitle.Text = "Other Debits";
				}
			}
			else
			{
				fldTitle.Text = "Checks";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			lblTitle1.Text = "Balancing Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("TypeBinder");
			this.Fields.Add("StatusBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstAccount)
			{
				blnFirstAccount = false;
				bool executeCheckFirstNonCheck = false;
				if (blnChecksDone)
				{
					executeCheckFirstNonCheck = true;
					goto CheckFirstNonCheck;
				}
				else
				{
					if (rsCheckInfo.EndOfFile() == true)
					{
						blnChecksDone = true;
						executeCheckFirstNonCheck = true;
						goto CheckFirstNonCheck;
					}
					else
					{
						eArgs.EOF = false;
						this.Fields["TypeBinder"].Value = 1;
						this.Fields["StatusBinder"].Value = rsCheckInfo.Get_Fields_String("Status");
					}
				}
				CheckFirstNonCheck:
				;
				if (executeCheckFirstNonCheck)
				{
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						this.Fields["TypeBinder"].Value = rsInfo.Get_Fields("Type");
						this.Fields["StatusBinder"].Value = rsInfo.Get_Fields_String("Status");
					}
				}
			}
			else
			{
				bool executeCheckNextNonCheck = false;
				if (blnChecksDone)
				{
					rsInfo.MoveNext();
					executeCheckNextNonCheck = true;
					goto CheckNextNonCheck;
				}
				else
				{
					rsCheckInfo.MoveNext();
					if (rsCheckInfo.EndOfFile() == true)
					{
						blnChecksDone = true;
						executeCheckNextNonCheck = true;
						goto CheckNextNonCheck;
					}
					else
					{
						eArgs.EOF = false;
						this.Fields["TypeBinder"].Value = 1;
						this.Fields["StatusBinder"].Value = rsCheckInfo.Get_Fields_String("Status");
					}
				}
				CheckNextNonCheck:
				;
				if (executeCheckNextNonCheck)
				{
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						this.Fields["TypeBinder"].Value = rsInfo.Get_Fields("Type");
						this.Fields["StatusBinder"].Value = rsInfo.Get_Fields_String("Status");
					}
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			object temp;
			int counter = 0;
			Decimal curTotal;
			clsDRWrapper rsSummaryInfo = new clsDRWrapper();
			string strCombinedSQL;
			temp = modBudgetaryAccounting.GetBDVariable("StatementDate");
			rsInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
			fldBegBalance.Text = "BEGINNING BALANCE......";
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			fldBegBalanceAmount.Text = Strings.Format(rsInfo.Get_Fields("Balance"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			curTotal = FCConvert.ToDecimal(rsInfo.Get_Fields("Balance"));
			strCombinedSQL = "SELECT Status, Type, SUM(Amount) as TotalAmount, COUNT(Amount) as TotalCount FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " GROUP BY Status, Type";
			rsInfo.OpenRecordset(strCombinedSQL + " ORDER BY Status, Type");
			// Cashed Deposits
			if (rsInfo.FindFirstRecord2("Status, Type", "3,3", ","))
			{
				fldCashedDeposits.Text = " + DEPOSITS ON STMT....";
				fldCashedDepositsAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldCashedDepositsCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
				counter += 1;
			}
			else
			{
				fldCashedDeposits.Text = "";
				fldCashedDepositsAmount.Text = "";
				fldCashedDepositsCount.Text = "";
			}
			// Returned Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "3,4", ","))
			{
				fldReturnedChecks.Text = " - RETURNED CHECKS.....";
				fldReturnedChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldReturnedChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldReturnedChecks.Text = "";
				fldReturnedChecksAmount.Text = "";
				fldReturnedChecksCount.Text = "";
			}
			// Interest
			if (rsInfo.FindFirstRecord2("Status, Type", "3,5", ","))
			{
				fldInterest.Text = " + INTEREST............";
				fldInterestAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldInterestCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldInterest.Text = "";
				fldInterestAmount.Text = "";
				fldInterestCount.Text = "";
			}
			// Other Credits
			if (rsInfo.FindFirstRecord2("Status, Type", "3,6", ","))
			{
				fldOtherCredits.Text = " + OTHER CREDITS.......";
				fldOtherCreditsAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldOtherCreditsCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOtherCredits.Text = "";
				fldOtherCreditsAmount.Text = "";
				fldOtherCreditsCount.Text = "";
			}
			// Cashed Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "3,1", ","))
			{
				fldCashedChecks.Text = " - CASHED CHECKS.......";
				fldCashedChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldCashedChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldCashedChecks.Text = "";
				fldCashedChecksAmount.Text = "";
				fldCashedChecksCount.Text = "";
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "3,2", ","))
			{
				fldCashedChecks.Text = " - CASHED CHECKS.......";
				if (fldCashedChecksAmount.Text != "")
				{
					fldCashedChecksAmount.Text = Strings.Format(FCConvert.ToDecimal(fldCashedChecksAmount.Text) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				else
				{
					fldCashedChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				if (FCConvert.ToString(fldCashedChecksCount.Text) != "")
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldCashedChecksCount.Text = FCConvert.ToString(FCConvert.ToInt32(fldCashedChecksCount) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount")));
				}
				else
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldCashedChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				}
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(fldCashedChecksCount.Text))
				{
					fldCashedChecks.Text = "";
					fldCashedChecksAmount.Text = "";
					fldCashedChecksCount.Text = "";
				}
			}
			// Other Debits
			if (rsInfo.FindFirstRecord2("Status, Type", "3,7", ","))
			{
				fldOtherDebits.Text = " - OTHER DEBITS........";
				fldOtherDebitsAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldOtherDebitsCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOtherDebits.Text = "";
				fldOtherDebitsAmount.Text = "";
				fldOtherDebitsCount.Text = "";
			}
			// Statement Total
			fldStatementTotal.Text = "STATEMENT BALANCE......";
			fldStatementTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
			// Outstanding Deposits
			if (rsInfo.FindFirstRecord2("Status, Type", "2,3", ","))
			{
				fldOutstandingDeposits.Text = " + OUTSTANDING DEPOSITS";
				fldOutstandingDepositsAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldOutstandingDepositsCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOutstandingDeposits.Text = "";
				fldOutstandingDepositsAmount.Text = "";
				fldOutstandingDepositsCount.Text = "";
			}
			// Outstanding Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "2,1", ","))
			{
				fldOutstandingChecks.Text = " - OUTSTANDING CHECKS..";
				fldOutstandingChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldOutstandingChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOutstandingChecks.Text = "";
				fldOutstandingChecksAmount.Text = "";
				fldOutstandingChecksCount.Text = "";
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "2,2", ","))
			{
				fldOutstandingChecks.Text = " - OUTSTANDING CHECKS..";
				if (fldOutstandingChecksAmount.Text != "")
				{
					fldOutstandingChecksAmount.Text = Strings.Format(FCConvert.ToDecimal(fldOutstandingChecksAmount.Text) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				else
				{
					fldOutstandingChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				if (FCConvert.ToString(fldOutstandingChecksCount.Text) != "")
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldOutstandingChecksCount.Text = FCConvert.ToString(FCConvert.ToInt32(fldOutstandingChecksCount.Text) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount")));
				}
				else
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldOutstandingChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				}
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(fldOutstandingChecksCount.Text))
				{
					fldOutstandingChecks.Text = "";
					fldOutstandingChecksAmount.Text = "";
					fldOutstandingChecksCount.Text = "";
				}
			}
			// Outstanding Other
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '2' AND (Type = '5' OR Type = '6')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				fldOutstandingOther.Text = " + OUTSTANDING OTHER";
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				fldOutstandingOtherAmount.Text = Strings.Format(rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00");
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				fldOutstandingOtherCount.Text = FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount"));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal += FCConvert.ToDecimal(rsSummaryInfo.Get_Fields("OtherTotal"));
			}
			else
			{
				fldOutstandingOther.Text = "";
				fldOutstandingOtherAmount.Text = "";
				fldOutstandingOtherCount.Text = "";
			}
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '2' AND (Type = '4' OR Type = '7')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				fldOutstandingOther.Text = " + OUTSTANDING OTHER";
				if (fldOutstandingOtherAmount.Text != "")
				{
					// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
					fldOutstandingOtherAmount.Text = Strings.Format(FCConvert.ToDecimal(fldOutstandingOtherAmount.Text) - rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00");
				}
				else
				{
					// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
					fldOutstandingOtherAmount.Text = Strings.Format(FCConvert.ToInt16(rsSummaryInfo.Get_Fields("OtherTotal")) * -1, "#,##0.00");
				}
				if (FCConvert.ToString(fldOutstandingOtherCount.Text) != "")
				{
					// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
					fldOutstandingOtherCount.Text = FCConvert.ToString(FCConvert.ToInt32(fldOutstandingOtherCount.Text) + FCConvert.ToInt32(rsSummaryInfo.Get_Fields("OtherCount")));
				}
				else
				{
					// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
					fldOutstandingOtherCount.Text = FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount"));
				}
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal -= FCConvert.ToDecimal(rsSummaryInfo.Get_Fields("OtherTotal"));
			}
			else
			{
				if (!Information.IsNumeric(fldOutstandingOtherCount.Text))
				{
					fldOutstandingOther.Text = "";
					fldOutstandingOtherAmount.Text = "";
					fldOutstandingOtherCount.Text = "";
				}
			}
			// Checkbook at Statement Time
			fldCheckBookStatement.Text = "CHECKBOOK AT STMT DATE.";
			fldCheckBookStatementAmount.Text = Strings.Format(curTotal, "#,##0.00");
			// Other Deposits
			if (rsInfo.FindFirstRecord2("Status, Type", "1,3", ","))
			{
				fldOtherDeposits.Text = " + OTHER DEPOSITS......";
				fldOtherDepositsAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldOtherDepositsCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldOtherDeposits.Text = "";
				fldOtherDepositsAmount.Text = "";
				fldOtherDepositsCount.Text = "";
			}
			// Issued Checks
			if (rsInfo.FindFirstRecord2("Status, Type", "1,1", ","))
			{
				fldIssuedChecks.Text = " - ISSUED CHECKS.......";
				fldIssuedChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
				fldIssuedChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				fldIssuedChecks.Text = "";
				fldIssuedChecksAmount.Text = "";
				fldIssuedChecksCount.Text = "";
			}
			if (rsInfo.FindFirstRecord2("Status, Type", "1,2", ","))
			{
				fldIssuedChecks.Text = " - ISSUED CHECKS.......";
				if (fldIssuedChecksAmount.Text != "")
				{
					fldIssuedChecksAmount.Text = Strings.Format(FCConvert.ToDecimal(fldIssuedChecksAmount.Text) + rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				else
				{
					fldIssuedChecksAmount.Text = Strings.Format(rsInfo.Get_Fields_Decimal("TotalAmount"), "#,##0.00");
				}
				if (FCConvert.ToString(fldIssuedChecksCount.Text) != "")
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldIssuedChecksCount.Text = FCConvert.ToString(FCConvert.ToInt32(fldIssuedChecksCount.Text) + FCConvert.ToInt32(rsInfo.Get_Fields("TotalCount")));
				}
				else
				{
					// TODO Get_Fields: Field [TotalCount] not found!! (maybe it is an alias?)
					fldIssuedChecksCount.Text = FCConvert.ToString(rsInfo.Get_Fields("TotalCount"));
				}
				curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
			}
			else
			{
				if (!Information.IsNumeric(fldIssuedChecksCount.Text))
				{
					fldIssuedChecks.Text = "";
					fldIssuedChecksAmount.Text = "";
					fldIssuedChecksCount.Text = "";
				}
			}
			// Issued Other
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '1' AND (Type = '5' OR Type = '6')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				fldIssuedOther.Text = " + ISSUED OTHER";
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				fldIssuedOtherAmount.Text = Strings.Format(rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00");
				// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
				fldIssuedOtherCount.Text = FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount"));
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal += FCConvert.ToDecimal(rsSummaryInfo.Get_Fields("OtherTotal"));
			}
			else
			{
				fldIssuedOther.Text = "";
				fldIssuedOtherAmount.Text = "";
				fldIssuedOtherCount.Text = "";
			}
			rsSummaryInfo.OpenRecordset("SELECT SUM(TotalAmount) as OtherTotal, SUM(TotalCount) as OtherCount FROM (" + strCombinedSQL + ") as Temp WHERE Status = '1' AND (Type = '4' OR Type = '7')");
			// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
			if (Information.IsNumeric(rsSummaryInfo.Get_Fields("OtherTotal")))
			{
				fldIssuedOther.Text = " + ISSUED OTHER";
				if (fldIssuedOtherAmount.Text != "")
				{
					// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
					fldIssuedOtherAmount.Text = Strings.Format(FCConvert.ToDecimal(fldIssuedOtherAmount.Text) - rsSummaryInfo.Get_Fields("OtherTotal"), "#,##0.00");
				}
				else
				{
					// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
					fldIssuedOtherAmount.Text = Strings.Format(FCConvert.ToDouble(rsSummaryInfo.Get_Fields("OtherTotal")) * -1, "#,##0.00");
				}
				if (FCConvert.ToString(fldIssuedOtherCount.Text) != "")
				{
					// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
					fldIssuedOtherCount.Text = FCConvert.ToString(FCConvert.ToInt32(fldIssuedOtherCount.Text) + FCConvert.ToInt32(rsSummaryInfo.Get_Fields("OtherCount")));
				}
				else
				{
					// TODO Get_Fields: Field [OtherCount] not found!! (maybe it is an alias?)
					fldIssuedOtherCount.Text = FCConvert.ToString(rsSummaryInfo.Get_Fields("OtherCount"));
				}
				// TODO Get_Fields: Field [OtherTotal] not found!! (maybe it is an alias?)
				curTotal -= FCConvert.ToDecimal(rsSummaryInfo.Get_Fields("OtherTotal"));
			}
			else
			{
				if (!Information.IsNumeric(fldIssuedOtherCount.Text))
				{
					fldIssuedOther.Text = "";
					fldIssuedOtherAmount.Text = "";
					fldIssuedOtherCount.Text = "";
				}
			}
			// Current Checkbook
			fldCurrentCheckbook.Text = "CURRENT CHECKBOOK......";
			fldCurrentCheckbookAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void rptBalancingReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBalancingReport.Caption	= "Balancing Report";
			//rptBalancingReport.Icon	= "rptBalancingReport.dsx":0000";
			//rptBalancingReport.Left	= 0;
			//rptBalancingReport.Top	= 0;
			//rptBalancingReport.Width	= 11880;
			//rptBalancingReport.Height	= 8595;
			//rptBalancingReport.StartUpPosition	= 3;
			//rptBalancingReport.SectionData	= "rptBalancingReport.dsx":058A;
			//End Unmaped Properties
		}

		private void rptBalancingReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
