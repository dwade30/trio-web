﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmClintonCheckRecExport.
	/// </summary>
	public partial class frmClintonCheckRecExport : BaseForm
	{
		public frmClintonCheckRecExport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            //FC:FINAL:BSE:#4340 allow only numeric input in textbox
            txtAcctNumber.AllowOnlyNumericInput(true);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmClintonCheckRecExport InstancePtr
		{
			get
			{
				return (frmClintonCheckRecExport)Sys.GetInstance(typeof(frmClintonCheckRecExport));
			}
		}

		protected frmClintonCheckRecExport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();

		private void frmClintonCheckRecExport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
            //FC:FINAL:BSE:#4340 focus should default to textbox
            txtAcctNumber.Focus();
		}

		private void frmClintonCheckRecExport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClintonCheckRecExport.FillStyle	= 0;
			//frmClintonCheckRecExport.ScaleWidth	= 3885;
			//frmClintonCheckRecExport.ScaleHeight	= 1995;
			//frmClintonCheckRecExport.LinkTopic	= "Form2";
			//frmClintonCheckRecExport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strAcctNumber;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			strAcctNumber = modRegistry.GetRegistryKey("ClinCustCheckRecNum");
			txtAcctNumber.Text = strAcctNumber;
		}

		private void frmClintonCheckRecExport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (Strings.Trim(txtAcctNumber.Text) == "")
			{
				MessageBox.Show("You must enter an account number before you may proceed.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!Information.IsDate(txtLowDate.Text) || !Information.IsDate(txtHighDate.Text))
			{
				MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
			{
				MessageBox.Show("Your low date must be earlier than your high date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// If Not CheckReportDateRange(Month(txtLowDate.Text), Month(txtHighDate.Text)) Then
			// MsgBox "You have entered an invalid date range for this report", vbExclamation, "Unable to Create Report"
			// Exit Sub
			// End If
			modRegistry.SaveRegistryKey("ClinCustCheckRecNum", Strings.Trim(txtAcctNumber.Text));
			CreateExport();
		}

		private void CreateExport()
		{
            try
            {
                int intRecordCounter;
                int counter;
                int ans;
                string strTemp = "";
                string strDirectory;
                string temp = "";
                FCFileSystem.FileClose(1);
                intRecordCounter = 1;
                strDirectory = FCFileSystem.Statics.UserDataFolder;
                Information.Err().Clear();
                App.MainForm.CommonDialog1.Flags = 0;
                // - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                App.MainForm.CommonDialog1.CancelError = true;
                /*? On Error Resume Next  */
                App.MainForm.CommonDialog1.FileName = "CheckRecExport.txt";
                App.MainForm.CommonDialog1.Filter = "*.txt";
                App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
                //FC:FINAL:DSE:#667 Upon cancelling the print dialog, there should be no errors thrown.
                try
                {
                    App.MainForm.CommonDialog1.ShowSave();
                }
                catch
                {
                }
                if (Information.Err().Number == 0)
                {
                    temp = App.MainForm.CommonDialog1.FileName;
                    FCFileSystem.ChDrive(strDirectory);
                    //Application.StartupPath = strDirectory;
                }
                else
                {
                    return;
                }
                rs.OpenRecordset("SELECT * FROM CheckRecMaster WHERE (CheckDate >= '" + FCConvert.ToString(DateAndTime.DateValue(txtLowDate.Text)) + "' AND CheckDate <= '" + FCConvert.ToString(DateAndTime.DateValue(txtHighDate.Text)) + "') AND (Type = '1' or Type = '2') AND CheckNumber <> 0");
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    // do nothing
                }
                else
                {
                    MessageBox.Show("There is no check rec information to export", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                FCFileSystem.FileClose(1);
                FCFileSystem.Kill(temp);
                FCFileSystem.FileOpen(1, temp, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(modClintonCheckRecExport.Statics.CCHR));
                CreateHeaderRecord();
                FCFileSystem.FilePut(1, modClintonCheckRecExport.Statics.CCHR, -1/*? intRecordCounter */);
                intRecordCounter += 1;
                do
                {
                    if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "V")
                    {
                        if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "CLINTON")
                        {
                            CreateVoidRecord();
                            FCFileSystem.FilePut(1, modClintonCheckRecExport.Statics.CCVR, -1/*? intRecordCounter */);
                            intRecordCounter += 1;
                        }
                        else
                        {
                            CreateWebExpressVoidRecord();
                            FCFileSystem.FilePut(1, modClintonCheckRecExport.Statics.CCVRWE, -1/*? intRecordCounter */);
                            intRecordCounter += 1;
                        }
                    }
                    else
                    {
                        CreateIssueRecord();
                        FCFileSystem.FilePut(1, modClintonCheckRecExport.Statics.CCIR, -1/*? intRecordCounter */);
                        intRecordCounter += 1;
                    }
                    rs.MoveNext();
                }
                while (rs.EndOfFile() != true);
                FCFileSystem.FileClose(1);
                FCUtils.Download(temp, "CheckRecExport.txt");
                MessageBox.Show("Process Completed Successfully!", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //FC:FINAL:SBE - #4353 - add try/catch/finally block in order to close the file when there are exceptions. Otherwise File will be in use on next run of the process
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                FCFileSystem.FileClose(1);
            }
		}

		private object CreateHeaderRecord()
		{
			object CreateHeaderRecord = null;
			modClintonCheckRecExport.Statics.CCHR.AccountNumber = (Strings.StrDup(10 - Strings.Trim(txtAcctNumber.Text).Length, "0") + txtAcctNumber.Text);
			modClintonCheckRecExport.Statics.CCHR.BankNumber = "910010000000";
			modClintonCheckRecExport.Statics.CCHR.FillerNumeric = Strings.StrDup(7, "0");
			modClintonCheckRecExport.Statics.CCHR.FillerString = Strings.StrDup(46, " ");
			modClintonCheckRecExport.Statics.CCHR.ID = "001";
			modClintonCheckRecExport.Statics.CCHR.TranCode = "BH";
			modClintonCheckRecExport.Statics.CCHR.BlankOrCRLF = "\r\n";
			return CreateHeaderRecord;
		}

		private object CreateVoidRecord()
		{
			object CreateVoidRecord = null;
			modClintonCheckRecExport.Statics.CCVR.AccountNumber = (Strings.StrDup(10 - Strings.Trim(txtAcctNumber.Text).Length, "0") + txtAcctNumber.Text);
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCVR.Amount = (Strings.StrDup(11 - Strings.Trim(FCConvert.ToString((FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100))).Length, "0") + FCConvert.ToString(FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100));
			modClintonCheckRecExport.Statics.CCVR.BankNumber = "910010000000";
			//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
			modClintonCheckRecExport.Statics.CCVR.Date = Strings.Format(rs.Get_Fields_DateTime("StatusDate"), "MMddyy");
			modClintonCheckRecExport.Statics.CCVR.Filler1 = Strings.StrDup(3, " ");
			modClintonCheckRecExport.Statics.CCVR.Filler2 = Strings.StrDup(3, " ");
			modClintonCheckRecExport.Statics.CCVR.Filler3 = Strings.StrDup(11, " ");
			modClintonCheckRecExport.Statics.CCVR.HighSerialNumber = Strings.StrDup(10, " ");
			modClintonCheckRecExport.Statics.CCVR.NewStatus = "V";
			modClintonCheckRecExport.Statics.CCVR.OldStatus = "A";
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCVR.SerialNumber = (Strings.StrDup(10 - Strings.Trim(rs.Get_Fields("CheckNumber").ToString()).Length, "0") + rs.Get_Fields("CheckNumber"));
			modClintonCheckRecExport.Statics.CCVR.TranCode = "50";
			modClintonCheckRecExport.Statics.CCVR.BlankOrCRLF = "\r\n";
			return CreateVoidRecord;
		}

		private object CreateWebExpressVoidRecord()
		{
			object CreateWebExpressVoidRecord = null;
			modClintonCheckRecExport.Statics.CCVRWE.AccountNumber = (Strings.StrDup(10 - Strings.Trim(txtAcctNumber.Text).Length, "0") + txtAcctNumber.Text);
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCVRWE.Amount = (Strings.StrDup(11 - Strings.Trim(FCConvert.ToString((FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100))).Length, "0") + FCConvert.ToString(FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100));
			modClintonCheckRecExport.Statics.CCVRWE.BankNumber = "910010000000";
			//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
			modClintonCheckRecExport.Statics.CCVRWE.Date = Strings.Format(rs.Get_Fields_DateTime("StatusDate"), "MMddyy");
			modClintonCheckRecExport.Statics.CCVRWE.Filler = Strings.StrDup(12, " ");
			modClintonCheckRecExport.Statics.CCVRWE.SequenceNumber = "999";
			if (FCConvert.ToString(rs.Get_Fields_String("Name")).Length >= 13)
			{
				modClintonCheckRecExport.Statics.CCVRWE.Name = Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Name")), 13);
			}
			else
			{
				modClintonCheckRecExport.Statics.CCVRWE.Name = (Strings.StrDup(13 - Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name"))).Length, " ") + rs.Get_Fields_String("Name"));
			}
			modClintonCheckRecExport.Statics.CCVRWE.flag = "0";
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCVRWE.SerialNumber = (Strings.StrDup(10 - Strings.Trim(rs.Get_Fields("CheckNumber").ToString()).Length, "0") + rs.Get_Fields("CheckNumber"));
			modClintonCheckRecExport.Statics.CCVRWE.TranCode = "50";
			modClintonCheckRecExport.Statics.CCVRWE.BlankOrCRLF = "\r\n";
			return CreateWebExpressVoidRecord;
		}

		private object CreateIssueRecord()
		{
			object CreateIssueRecord = null;
			modClintonCheckRecExport.Statics.CCIR.AccountNumber = (Strings.StrDup(10 - Strings.Trim(txtAcctNumber.Text).Length, "0") + txtAcctNumber.Text);
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCIR.Amount = (Strings.StrDup(11 - Strings.Trim(FCConvert.ToString((FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100))).Length, "0") + FCConvert.ToString(FCConvert.ToDecimal(rs.Get_Fields("Amount")) * 100));
			modClintonCheckRecExport.Statics.CCIR.BankNumber = "910010000000";
			//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
			modClintonCheckRecExport.Statics.CCIR.Date = Strings.Format(rs.Get_Fields_DateTime("CheckDate"), "MMddyy");
			modClintonCheckRecExport.Statics.CCIR.Filler = Strings.StrDup(12, " ");
			modClintonCheckRecExport.Statics.CCIR.SequenceNumber = "999";
			if (FCConvert.ToString(rs.Get_Fields_String("Name")).Length >= 13)
			{
				modClintonCheckRecExport.Statics.CCIR.Name = Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Name")), 13);
			}
			else
			{
				modClintonCheckRecExport.Statics.CCIR.Name = (Strings.StrDup(13 - Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name"))).Length, " ") + rs.Get_Fields_String("Name"));
			}
			modClintonCheckRecExport.Statics.CCIR.flag = "0";
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			modClintonCheckRecExport.Statics.CCIR.SerialNumber = (Strings.StrDup(10 - Strings.Trim(rs.Get_Fields("CheckNumber").ToString()).Length, "0") + rs.Get_Fields("CheckNumber"));
			modClintonCheckRecExport.Statics.CCIR.TranCode = "40";
			modClintonCheckRecExport.Statics.CCIR.BlankOrCRLF = "\r\n";
			return CreateIssueRecord;
		}

		private void txtAcctNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
