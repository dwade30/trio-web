﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantPreviewDeptSummary.
	/// </summary>
	partial class rptWarrantPreviewDeptSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptWarrantPreviewDeptSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPayDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldDepartment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DepartmentBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldDepartmentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDepartmentTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldDivision = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DivisionBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldDivisionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDivisionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DepartmentBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepartmentTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivision)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DivisionBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDivisionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVendor,
				this.fldAmount,
				this.fldAccount
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.19F;
			this.fldVendor.Left = 0.53125F;
			this.fldVendor.MultiLine = false;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldVendor.Text = "Field27";
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 2.28125F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.19F;
			this.fldAmount.Left = 2.875F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field27";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.03125F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 4F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; white-space: nowrap; ddo" + "-char-set: 1";
			this.fldAccount.Text = "Field27";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 3.46875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFinalTotal,
				this.Field28,
				this.Line5
			});
			this.ReportFooter.Height = 0.2291667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.CanShrink = true;
			this.fldFinalTotal.Height = 0.19F;
			this.fldFinalTotal.Left = 2.9375F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalTotal.Text = "Field3";
			this.fldFinalTotal.Top = 0.03125F;
			this.fldFinalTotal.Width = 1F;
			// 
			// Field28
			// 
			this.Field28.CanShrink = true;
			this.Field28.Height = 0.19F;
			this.Field28.Left = 1.71875F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Field28.Text = "Final Total-";
			this.Field28.Top = 0.03125F;
			this.Field28.Width = 1.1875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 2.9375F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0F;
			this.Line5.Width = 1.03125F;
			this.Line5.X1 = 2.9375F;
			this.Line5.X2 = 3.96875F;
			this.Line5.Y1 = 0F;
			this.Line5.Y2 = 0F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblPayDate,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Line1,
				this.Field26,
				this.lblAccount,
				this.lblAmount
			});
			this.PageHeader.Height = 0.71875F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Department Summary";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// lblPayDate
			// 
			this.lblPayDate.Height = 0.1875F;
			this.lblPayDate.HyperLink = null;
			this.lblPayDate.Left = 1.5F;
			this.lblPayDate.Name = "lblPayDate";
			this.lblPayDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblPayDate.Text = "Label6";
			this.lblPayDate.Top = 0.21875F;
			this.lblPayDate.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 0.53125F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field26.Tag = " ";
			this.Field26.Text = "Vendor";
			this.Field26.Top = 0.5F;
			this.Field26.Width = 2.28125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.Left = 4F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Tag = " ";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 3F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.Left = 2.875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblAmount.Tag = " ";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.5F;
			this.lblAmount.Width = 1.03125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDepartment,
				this.DepartmentBinder
			});
			this.GroupHeader1.DataField = "DepartmentBinder";
			this.GroupHeader1.Height = 0.1875F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldDepartment
			// 
			this.fldDepartment.Height = 0.19F;
			this.fldDepartment.Left = 0F;
			this.fldDepartment.Name = "fldDepartment";
			this.fldDepartment.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldDepartment.Text = "Field3";
			this.fldDepartment.Top = 0F;
			this.fldDepartment.Width = 3.5F;
			// 
			// DepartmentBinder
			// 
			this.DepartmentBinder.DataField = "DepartmentBinder";
			this.DepartmentBinder.Height = 0.125F;
			this.DepartmentBinder.Left = 3.96875F;
			this.DepartmentBinder.Name = "DepartmentBinder";
			this.DepartmentBinder.Text = "Field27";
			this.DepartmentBinder.Top = 0F;
			this.DepartmentBinder.Visible = false;
			this.DepartmentBinder.Width = 0.625F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDepartmentTotal,
				this.lblDepartmentTotal,
				this.Line4
			});
			this.GroupFooter1.Height = 0.21875F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// fldDepartmentTotal
			// 
			this.fldDepartmentTotal.CanShrink = true;
			this.fldDepartmentTotal.Height = 0.19F;
			this.fldDepartmentTotal.Left = 2.9375F;
			this.fldDepartmentTotal.Name = "fldDepartmentTotal";
			this.fldDepartmentTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDepartmentTotal.Text = "Field3";
			this.fldDepartmentTotal.Top = 0.03125F;
			this.fldDepartmentTotal.Width = 1F;
			// 
			// lblDepartmentTotal
			// 
			this.lblDepartmentTotal.CanShrink = true;
			this.lblDepartmentTotal.Height = 0.19F;
			this.lblDepartmentTotal.Left = 1.71875F;
			this.lblDepartmentTotal.Name = "lblDepartmentTotal";
			this.lblDepartmentTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblDepartmentTotal.Text = "Department Total-";
			this.lblDepartmentTotal.Top = 0.03125F;
			this.lblDepartmentTotal.Width = 1.1875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 2.9375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0F;
			this.Line4.Width = 1.03125F;
			this.Line4.X1 = 2.9375F;
			this.Line4.X2 = 3.96875F;
			this.Line4.Y1 = 0F;
			this.Line4.Y2 = 0F;
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.CanShrink = true;
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDivision,
				this.DivisionBinder
			});
			this.GroupHeader2.DataField = "DivisionBinder";
			this.GroupHeader2.Height = 0.1875F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			// 
			// fldDivision
			// 
			this.fldDivision.Height = 0.19F;
			this.fldDivision.Left = 0.3125F;
			this.fldDivision.Name = "fldDivision";
			this.fldDivision.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldDivision.Text = "Field3";
			this.fldDivision.Top = 0F;
			this.fldDivision.Width = 3.5F;
			// 
			// DivisionBinder
			// 
			this.DivisionBinder.DataField = "DivisionBinder";
			this.DivisionBinder.Height = 0.125F;
			this.DivisionBinder.Left = 4.125F;
			this.DivisionBinder.Name = "DivisionBinder";
			this.DivisionBinder.Text = "Field27";
			this.DivisionBinder.Top = 0F;
			this.DivisionBinder.Visible = false;
			this.DivisionBinder.Width = 0.625F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.CanShrink = true;
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDivisionTotal,
				this.lblDivisionTotal,
				this.Line3
			});
			this.GroupFooter2.Height = 0.21875F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// fldDivisionTotal
			// 
			this.fldDivisionTotal.CanShrink = true;
			this.fldDivisionTotal.Height = 0.19F;
			this.fldDivisionTotal.Left = 2.9375F;
			this.fldDivisionTotal.Name = "fldDivisionTotal";
			this.fldDivisionTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldDivisionTotal.Text = "Field3";
			this.fldDivisionTotal.Top = 0.03125F;
			this.fldDivisionTotal.Width = 1F;
			// 
			// lblDivisionTotal
			// 
			this.lblDivisionTotal.CanShrink = true;
			this.lblDivisionTotal.Height = 0.19F;
			this.lblDivisionTotal.Left = 1.71875F;
			this.lblDivisionTotal.Name = "lblDivisionTotal";
			this.lblDivisionTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.lblDivisionTotal.Text = "Division Total-";
			this.lblDivisionTotal.Top = 0.03125F;
			this.lblDivisionTotal.Width = 1.1875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.9375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 1.03125F;
			this.Line3.X1 = 2.9375F;
			this.Line3.X2 = 3.96875F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// rptWarrantPreviewDeptSummary
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptWarrantPreviewDeptSummary_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPayDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DepartmentBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDepartmentTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivision)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DivisionBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDivisionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPayDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox DepartmentBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDepartmentTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivision;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox DivisionBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDivisionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
