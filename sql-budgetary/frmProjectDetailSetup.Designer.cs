﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmProjectDetailSetup.
	/// </summary>
	partial class frmProjectDetailSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboSingleFund;
		public fecherFoundation.FCCheckBox chkCheckAccountRange;
		public fecherFoundation.FCComboBox cboBeginningProject;
		public fecherFoundation.FCComboBox cboEndingProject;
		public fecherFoundation.FCComboBox cboSingleProject;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProjectDetailSetup));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbAllAccounts = new fecherFoundation.FCComboBox();
            this.lblAllAccounts = new fecherFoundation.FCLabel();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fraMonths = new fecherFoundation.FCFrame();
            this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
            this.cboEndingMonth = new fecherFoundation.FCComboBox();
            this.cboBeginningMonth = new fecherFoundation.FCComboBox();
            this.cboSingleMonth = new fecherFoundation.FCComboBox();
            this.lblTo_0 = new fecherFoundation.FCLabel();
            this.fraDeptRange = new fecherFoundation.FCFrame();
            this.cboSingleFund = new fecherFoundation.FCComboBox();
            this.chkCheckAccountRange = new fecherFoundation.FCCheckBox();
            this.cboBeginningProject = new fecherFoundation.FCComboBox();
            this.cboEndingProject = new fecherFoundation.FCComboBox();
            this.cboSingleProject = new fecherFoundation.FCComboBox();
            this.lblTo_2 = new fecherFoundation.FCLabel();
            this.lblTo_1 = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
            this.fraMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
            this.fraDeptRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 359);
            this.BottomPanel.Size = new System.Drawing.Size(610, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.fraMonths);
            this.ClientArea.Controls.Add(this.fraDeptRange);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(610, 299);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(610, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(354, 30);
            this.HeaderText.Text = "Project Detail Selection Criteria";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "Range of Months",
            "Single Month",
            "All"});
            this.cmbRange.Location = new System.Drawing.Point(115, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(225, 40);
            this.cmbRange.TabIndex = 5;
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
            // 
            // lblRange
            // 
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(50, 16);
            this.lblRange.TabIndex = 6;
            this.lblRange.Text = "RANGE";
            // 
            // cmbAllAccounts
            // 
            this.cmbAllAccounts.Items.AddRange(new object[] {
            "All",
            "Single Project",
            "Project Range",
            "Single Fund"});
            this.cmbAllAccounts.Location = new System.Drawing.Point(115, 30);
            this.cmbAllAccounts.Name = "cmbAllAccounts";
            this.cmbAllAccounts.Size = new System.Drawing.Size(225, 40);
            this.cmbAllAccounts.TabIndex = 25;
            this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAllAccounts_SelectedIndexChanged);
            // 
            // lblAllAccounts
            // 
            this.lblAllAccounts.AutoSize = true;
            this.lblAllAccounts.Location = new System.Drawing.Point(20, 44);
            this.lblAllAccounts.Name = "lblAllAccounts";
            this.lblAllAccounts.Size = new System.Drawing.Size(51, 15);
            this.lblAllAccounts.TabIndex = 26;
            this.lblAllAccounts.Text = "RANGE";
            // 
            // txtDescription
            // 
            this.txtDescription.Appearance = 0;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtDescription.Location = new System.Drawing.Point(30, 66);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(200, 40);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(224, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(120, 48);
            this.cmdSave.TabIndex = 15;
            this.cmdSave.Text = "Save Criteria";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // fraMonths
            // 
            this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
            this.fraMonths.Controls.Add(this.chkCheckDateRange);
            this.fraMonths.Controls.Add(this.cmbRange);
            this.fraMonths.Controls.Add(this.lblRange);
            this.fraMonths.Controls.Add(this.cboEndingMonth);
            this.fraMonths.Controls.Add(this.cboBeginningMonth);
            this.fraMonths.Controls.Add(this.cboSingleMonth);
            this.fraMonths.Controls.Add(this.lblTo_0);
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Location = new System.Drawing.Point(30, 126);
            this.fraMonths.Name = "fraMonths";
            this.fraMonths.Size = new System.Drawing.Size(360, 197);
            this.fraMonths.TabIndex = 19;
            this.fraMonths.Text = "Month(s) To Report";
            // 
            // chkCheckDateRange
            // 
            this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckDateRange.Location = new System.Drawing.Point(20, 90);
            this.chkCheckDateRange.Name = "chkCheckDateRange";
            this.chkCheckDateRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckDateRange.TabIndex = 4;
            this.chkCheckDateRange.Text = "Select at report time";
            this.chkCheckDateRange.Visible = false;
            this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
            // 
            // cboEndingMonth
            // 
            this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboEndingMonth.Location = new System.Drawing.Point(210, 137);
            this.cboEndingMonth.Name = "cboEndingMonth";
            this.cboEndingMonth.Size = new System.Drawing.Size(130, 40);
            this.cboEndingMonth.TabIndex = 6;
            this.cboEndingMonth.Visible = false;
            // 
            // cboBeginningMonth
            // 
            this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboBeginningMonth.Location = new System.Drawing.Point(20, 137);
            this.cboBeginningMonth.Name = "cboBeginningMonth";
            this.cboBeginningMonth.Size = new System.Drawing.Size(130, 40);
            this.cboBeginningMonth.TabIndex = 5;
            this.cboBeginningMonth.Visible = false;
            // 
            // cboSingleMonth
            // 
            this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cboSingleMonth.Location = new System.Drawing.Point(20, 137);
            this.cboSingleMonth.Name = "cboSingleMonth";
            this.cboSingleMonth.Size = new System.Drawing.Size(320, 40);
            this.cboSingleMonth.TabIndex = 7;
            this.cboSingleMonth.Visible = false;
            // 
            // lblTo_0
            // 
            this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_0.Location = new System.Drawing.Point(170, 151);
            this.lblTo_0.Name = "lblTo_0";
            this.lblTo_0.Size = new System.Drawing.Size(20, 16);
            this.lblTo_0.TabIndex = 20;
            this.lblTo_0.Text = "TO";
            this.lblTo_0.Visible = false;
            // 
            // fraDeptRange
            // 
            this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
            this.fraDeptRange.Controls.Add(this.cboSingleFund);
            this.fraDeptRange.Controls.Add(this.cmbAllAccounts);
            this.fraDeptRange.Controls.Add(this.lblAllAccounts);
            this.fraDeptRange.Controls.Add(this.chkCheckAccountRange);
            this.fraDeptRange.Controls.Add(this.cboBeginningProject);
            this.fraDeptRange.Controls.Add(this.cboEndingProject);
            this.fraDeptRange.Controls.Add(this.cboSingleProject);
            this.fraDeptRange.Controls.Add(this.lblTo_2);
            this.fraDeptRange.Controls.Add(this.lblTo_1);
            this.fraDeptRange.Location = new System.Drawing.Point(410, 126);
            this.fraDeptRange.Name = "fraDeptRange";
            this.fraDeptRange.Size = new System.Drawing.Size(360, 197);
            this.fraDeptRange.TabIndex = 17;
            this.fraDeptRange.Text = "Accounts To Be Reported";
            // 
            // cboSingleFund
            // 
            this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleFund.Location = new System.Drawing.Point(20, 137);
            this.cboSingleFund.Name = "cboSingleFund";
            this.cboSingleFund.Size = new System.Drawing.Size(320, 40);
            this.cboSingleFund.TabIndex = 24;
            this.cboSingleFund.Visible = false;
            this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
            // 
            // chkCheckAccountRange
            // 
            this.chkCheckAccountRange.BackColor = System.Drawing.SystemColors.Menu;
            this.chkCheckAccountRange.Location = new System.Drawing.Point(20, 90);
            this.chkCheckAccountRange.Name = "chkCheckAccountRange";
            this.chkCheckAccountRange.Size = new System.Drawing.Size(174, 27);
            this.chkCheckAccountRange.TabIndex = 11;
            this.chkCheckAccountRange.Text = "Select at report time";
            this.chkCheckAccountRange.Visible = false;
            this.chkCheckAccountRange.CheckedChanged += new System.EventHandler(this.chkCheckAccountRange_CheckedChanged);
            // 
            // cboBeginningProject
            // 
            this.cboBeginningProject.BackColor = System.Drawing.SystemColors.Window;
            this.cboBeginningProject.Location = new System.Drawing.Point(20, 137);
            this.cboBeginningProject.Name = "cboBeginningProject";
            this.cboBeginningProject.Size = new System.Drawing.Size(130, 40);
            this.cboBeginningProject.TabIndex = 12;
            this.cboBeginningProject.Visible = false;
            this.cboBeginningProject.DropDown += new System.EventHandler(this.cboBeginningProject_DropDown);
            // 
            // cboEndingProject
            // 
            this.cboEndingProject.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndingProject.Location = new System.Drawing.Point(210, 137);
            this.cboEndingProject.Name = "cboEndingProject";
            this.cboEndingProject.Size = new System.Drawing.Size(130, 40);
            this.cboEndingProject.TabIndex = 13;
            this.cboEndingProject.Visible = false;
            this.cboEndingProject.DropDown += new System.EventHandler(this.cboEndingProject_DropDown);
            // 
            // cboSingleProject
            // 
            this.cboSingleProject.BackColor = System.Drawing.SystemColors.Window;
            this.cboSingleProject.Location = new System.Drawing.Point(20, 137);
            this.cboSingleProject.Name = "cboSingleProject";
            this.cboSingleProject.Size = new System.Drawing.Size(320, 40);
            this.cboSingleProject.TabIndex = 14;
            this.cboSingleProject.Visible = false;
            this.cboSingleProject.DropDown += new System.EventHandler(this.cboSingleProject_DropDown);
            // 
            // lblTo_2
            // 
            this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo_2.Location = new System.Drawing.Point(170, 151);
            this.lblTo_2.Name = "lblTo_2";
            this.lblTo_2.Size = new System.Drawing.Size(20, 16);
            this.lblTo_2.TabIndex = 21;
            this.lblTo_2.Text = "TO";
            this.lblTo_2.Visible = false;
            // 
            // lblTo_1
            // 
            this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
            this.lblTo_1.Location = new System.Drawing.Point(170, 151);
            this.lblTo_1.Name = "lblTo_1";
            this.lblTo_1.Size = new System.Drawing.Size(20, 16);
            this.lblTo_1.TabIndex = 18;
            this.lblTo_1.Text = "TO";
            this.lblTo_1.Visible = false;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 30);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(150, 16);
            this.lblDescription.TabIndex = 22;
            this.lblDescription.Text = "SELECTION CRITERIA";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmProjectDetailSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(610, 467);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmProjectDetailSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Project Detail Selection Criteria";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmProjectDetailSetup_Load);
            this.Activated += new System.EventHandler(this.frmProjectDetailSetup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmProjectDetailSetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmProjectDetailSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
            this.fraMonths.ResumeLayout(false);
            this.fraMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
            this.fraDeptRange.ResumeLayout(false);
            this.fraDeptRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).EndInit();
            this.ResumeLayout(false);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmProjectDetailSetup_Closing);
        }
		#endregion
	}
}
