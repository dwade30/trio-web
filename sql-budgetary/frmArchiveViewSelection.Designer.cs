//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmArchiveViewSelection.
	/// </summary>
	partial class frmArchiveViewSelection : BaseForm
	{
		public fecherFoundation.FCComboBox cmbStatusAll;
		public fecherFoundation.FCComboBox cmbTypeAll;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCButton Command1;
		public fecherFoundation.FCButton cmdRequestDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCTextBox txtLowCheck;
		public fecherFoundation.FCFrame fraStatusType;
		public fecherFoundation.FCFrame fraStatusOptions;
		public fecherFoundation.FCCheckBox chkDeleted;
		public fecherFoundation.FCCheckBox chkVoided;
		public fecherFoundation.FCCheckBox chkCashed;
		public fecherFoundation.FCFrame fraAccountType;
		public fecherFoundation.FCFrame fraTypeOptions;
		public fecherFoundation.FCCheckBox chkAP;
		public fecherFoundation.FCCheckBox chkPY;
		public fecherFoundation.FCCheckBox chkDP;
		public fecherFoundation.FCCheckBox chkRT;
		public fecherFoundation.FCCheckBox chkIN;
		public fecherFoundation.FCCheckBox chkOC;
		public fecherFoundation.FCCheckBox chkOD;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblBank;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArchiveViewSelection));
            this.cmbStatusAll = new fecherFoundation.FCComboBox();
            this.cmbTypeAll = new fecherFoundation.FCComboBox();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.Command1 = new fecherFoundation.FCButton();
            this.cmdRequestDate = new fecherFoundation.FCButton();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.txtLowCheck = new fecherFoundation.FCTextBox();
            this.fraStatusType = new fecherFoundation.FCFrame();
            this.fraStatusOptions = new fecherFoundation.FCFrame();
            this.chkDeleted = new fecherFoundation.FCCheckBox();
            this.chkVoided = new fecherFoundation.FCCheckBox();
            this.chkCashed = new fecherFoundation.FCCheckBox();
            this.fraAccountType = new fecherFoundation.FCFrame();
            this.fraTypeOptions = new fecherFoundation.FCFrame();
            this.chkAP = new fecherFoundation.FCCheckBox();
            this.chkPY = new fecherFoundation.FCCheckBox();
            this.chkDP = new fecherFoundation.FCCheckBox();
            this.chkRT = new fecherFoundation.FCCheckBox();
            this.chkIN = new fecherFoundation.FCCheckBox();
            this.chkOC = new fecherFoundation.FCCheckBox();
            this.chkOD = new fecherFoundation.FCCheckBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblBank = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusType)).BeginInit();
            this.fraStatusType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusOptions)).BeginInit();
            this.fraStatusOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoided)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCashed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).BeginInit();
            this.fraAccountType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTypeOptions)).BeginInit();
            this.fraTypeOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtLowCheck);
            this.ClientArea.Controls.Add(this.fraDateRange);
            this.ClientArea.Controls.Add(this.fraStatusType);
            this.ClientArea.Controls.Add(this.fraAccountType);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblBank);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdCancel);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCancel, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 30);
            this.HeaderText.Text = "View Archive Records";
            // 
            // cmbStatusAll
            // 
            this.cmbStatusAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbStatusAll.Location = new System.Drawing.Point(20, 30);
            this.cmbStatusAll.Name = "cmbStatusAll";
            this.cmbStatusAll.Size = new System.Drawing.Size(260, 40);
            this.cmbStatusAll.TabIndex = 14;
            this.cmbStatusAll.SelectedIndexChanged += new System.EventHandler(this.optStatusSelected_CheckedChanged);
            // 
            // cmbTypeAll
            // 
            this.cmbTypeAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbTypeAll.Location = new System.Drawing.Point(20, 30);
            this.cmbTypeAll.Name = "cmbTypeAll";
            this.cmbTypeAll.Size = new System.Drawing.Size(260, 40);
            this.cmbTypeAll.TabIndex = 3;
            this.cmbTypeAll.SelectedIndexChanged += new System.EventHandler(this.optTypeSelected_CheckedChanged);
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.Command1);
            this.fraDateRange.Controls.Add(this.cmdRequestDate);
            this.fraDateRange.Controls.Add(this.txtStartDate);
            this.fraDateRange.Controls.Add(this.txtEndDate);
            this.fraDateRange.Controls.Add(this.lblTo);
            this.fraDateRange.Location = new System.Drawing.Point(30, 590);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(450, 90);
            this.fraDateRange.TabIndex = 25;
            this.fraDateRange.Text = "Date Range";
            // 
            // Command1
            // 
            this.Command1.AppearanceKey = "imageButton";
            this.Command1.ImageSource = "icon - calendar?color=#707884";
            this.Command1.Location = new System.Drawing.Point(402, 30);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(40, 40);
            this.Command1.TabIndex = 27;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdRequestDate
            // 
            this.cmdRequestDate.AppearanceKey = "imageButton";
            this.cmdRequestDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdRequestDate.Location = new System.Drawing.Point(153, 30);
            this.cmdRequestDate.Name = "cmdRequestDate";
            this.cmdRequestDate.Size = new System.Drawing.Size(40, 40);
            this.cmdRequestDate.TabIndex = 26;
            this.cmdRequestDate.Click += new System.EventHandler(this.cmdRequestDate_Click);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 28;
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(269, 30);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 29;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(214, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 16);
            this.lblTo.TabIndex = 30;
            this.lblTo.Text = "TO";
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(474, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(100, 48);
            this.cmdPrint.TabIndex = 21;
            this.cmdPrint.Text = "Display";
            this.cmdPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtLowCheck
            // 
            this.txtLowCheck.BackColor = System.Drawing.SystemColors.Window;
            clientEvent1.Event = "keypress";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtLowCheck).Add(clientEvent1);
            this.txtLowCheck.Location = new System.Drawing.Point(30, 64);
            this.txtLowCheck.Name = "txtLowCheck";
            this.txtLowCheck.Size = new System.Drawing.Size(150, 40);
            this.txtLowCheck.TabIndex = 19;
            this.txtLowCheck.Text = "0";
            this.txtLowCheck.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtLowCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLowCheck_KeyPress);
            // 
            // fraStatusType
            // 
            this.fraStatusType.BackColor = System.Drawing.Color.FromName("@window");
            this.fraStatusType.Controls.Add(this.fraStatusOptions);
            this.fraStatusType.Controls.Add(this.cmbStatusAll);
            this.fraStatusType.Location = new System.Drawing.Point(360, 134);
            this.fraStatusType.Name = "fraStatusType";
            this.fraStatusType.Size = new System.Drawing.Size(300, 236);
            this.fraStatusType.TabIndex = 11;
            this.fraStatusType.Text = "Transaction Status";
            // 
            // fraStatusOptions
            // 
            this.fraStatusOptions.AppearanceKey = "groupBoxNoBorders";
            this.fraStatusOptions.Controls.Add(this.chkDeleted);
            this.fraStatusOptions.Controls.Add(this.chkVoided);
            this.fraStatusOptions.Controls.Add(this.chkCashed);
            this.fraStatusOptions.Enabled = false;
            this.fraStatusOptions.Location = new System.Drawing.Point(1, 70);
            this.fraStatusOptions.Name = "fraStatusOptions";
            this.fraStatusOptions.Size = new System.Drawing.Size(123, 165);
            this.fraStatusOptions.TabIndex = 13;
            // 
            // chkDeleted
            // 
            this.chkDeleted.Location = new System.Drawing.Point(19, 120);
            this.chkDeleted.Name = "chkDeleted";
            this.chkDeleted.Size = new System.Drawing.Size(84, 27);
            this.chkDeleted.TabIndex = 16;
            this.chkDeleted.Text = "Deleted";
            // 
            // chkVoided
            // 
            this.chkVoided.Location = new System.Drawing.Point(19, 70);
            this.chkVoided.Name = "chkVoided";
            this.chkVoided.Size = new System.Drawing.Size(78, 27);
            this.chkVoided.TabIndex = 15;
            this.chkVoided.Text = "Voided";
            // 
            // chkCashed
            // 
            this.chkCashed.Location = new System.Drawing.Point(19, 20);
            this.chkCashed.Name = "chkCashed";
            this.chkCashed.Size = new System.Drawing.Size(84, 27);
            this.chkCashed.TabIndex = 14;
            this.chkCashed.Text = "Cashed";
            // 
            // fraAccountType
            // 
            this.fraAccountType.BackColor = System.Drawing.Color.FromName("@window");
            this.fraAccountType.Controls.Add(this.fraTypeOptions);
            this.fraAccountType.Controls.Add(this.cmbTypeAll);
            this.fraAccountType.Location = new System.Drawing.Point(30, 134);
            this.fraAccountType.Name = "fraAccountType";
            this.fraAccountType.Size = new System.Drawing.Size(300, 436);
            this.fraAccountType.Text = "Transaction Type";
            // 
            // fraTypeOptions
            // 
            this.fraTypeOptions.AppearanceKey = "groupBoxNoBorders";
            this.fraTypeOptions.Controls.Add(this.chkAP);
            this.fraTypeOptions.Controls.Add(this.chkPY);
            this.fraTypeOptions.Controls.Add(this.chkDP);
            this.fraTypeOptions.Controls.Add(this.chkRT);
            this.fraTypeOptions.Controls.Add(this.chkIN);
            this.fraTypeOptions.Controls.Add(this.chkOC);
            this.fraTypeOptions.Controls.Add(this.chkOD);
            this.fraTypeOptions.Enabled = false;
            this.fraTypeOptions.Location = new System.Drawing.Point(1, 70);
            this.fraTypeOptions.Name = "fraTypeOptions";
            this.fraTypeOptions.Size = new System.Drawing.Size(198, 365);
            this.fraTypeOptions.TabIndex = 2;
            // 
            // chkAP
            // 
            this.chkAP.Location = new System.Drawing.Point(19, 20);
            this.chkAP.Name = "chkAP";
            this.chkAP.Size = new System.Drawing.Size(159, 27);
            this.chkAP.TabIndex = 9;
            this.chkAP.Text = "Accounts Payable";
            // 
            // chkPY
            // 
            this.chkPY.Location = new System.Drawing.Point(19, 70);
            this.chkPY.Name = "chkPY";
            this.chkPY.Size = new System.Drawing.Size(77, 27);
            this.chkPY.TabIndex = 8;
            this.chkPY.Text = "Payroll";
            // 
            // chkDP
            // 
            this.chkDP.Location = new System.Drawing.Point(19, 120);
            this.chkDP.Name = "chkDP";
            this.chkDP.Size = new System.Drawing.Size(91, 27);
            this.chkDP.TabIndex = 7;
            this.chkDP.Text = "Deposits";
            // 
            // chkRT
            // 
            this.chkRT.Location = new System.Drawing.Point(19, 170);
            this.chkRT.Name = "chkRT";
            this.chkRT.Size = new System.Drawing.Size(154, 27);
            this.chkRT.TabIndex = 6;
            this.chkRT.Text = "Returned Checks";
            // 
            // chkIN
            // 
            this.chkIN.Location = new System.Drawing.Point(19, 220);
            this.chkIN.Name = "chkIN";
            this.chkIN.Size = new System.Drawing.Size(82, 27);
            this.chkIN.TabIndex = 5;
            this.chkIN.Text = "Interest";
            // 
            // chkOC
            // 
            this.chkOC.Location = new System.Drawing.Point(19, 270);
            this.chkOC.Name = "chkOC";
            this.chkOC.Size = new System.Drawing.Size(125, 27);
            this.chkOC.TabIndex = 4;
            this.chkOC.Text = "Other Credits";
            // 
            // chkOD
            // 
            this.chkOD.Location = new System.Drawing.Point(19, 320);
            this.chkOD.Name = "chkOD";
            this.chkOD.Size = new System.Drawing.Size(119, 27);
            this.chkOD.TabIndex = 3;
            this.chkOD.Text = "Other Debits";
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 9;
            this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(30, 64);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 50;
            this.vs1.Size = new System.Drawing.Size(916, 316);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 22;
            this.vs1.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(538, 14);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "PLEASE INPUT THE LOWEST CHECK NUMBER YOU WOULD LIKE DISPLAYED ON THIS REPORT";
            // 
            // lblBank
            // 
            this.lblBank.Location = new System.Drawing.Point(30, 30);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(598, 14);
            this.lblBank.TabIndex = 23;
            this.lblBank.Text = "LABEL1";
            this.lblBank.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuPreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuPreview
            // 
            this.mnuPreview.Index = 1;
            this.mnuPreview.Name = "mnuPreview";
            this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPreview.Text = "Print / Preview";
            this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCancel.Location = new System.Drawing.Point(1001, 29);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(47, 24);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Back";
            this.cmdCancel.Visible = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // frmArchiveViewSelection
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmArchiveViewSelection";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "View Archive Records";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmArchiveViewSelection_Load);
            this.Activated += new System.EventHandler(this.frmArchiveViewSelection_Activated);
            this.Resize += new System.EventHandler(this.frmArchiveViewSelection_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmArchiveViewSelection_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Command1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRequestDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusType)).EndInit();
            this.fraStatusType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusOptions)).EndInit();
            this.fraStatusOptions.ResumeLayout(false);
            this.fraStatusOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVoided)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCashed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).EndInit();
            this.fraAccountType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraTypeOptions)).EndInit();
            this.fraTypeOptions.ResumeLayout(false);
            this.fraTypeOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdCancel;
		private JavaScript javaScript1;
		private System.ComponentModel.IContainer components;
	}
}