﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptExpenseDetail.
	/// </summary>
	partial class rptExpenseDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptExpenseDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDeptTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fld1,
				this.fld2,
				this.fld3,
				this.fld4,
				this.fld5,
				this.fld6,
				this.fld7,
				this.fld8,
				this.fld9,
				this.fld10,
				this.fld11,
				this.fld12,
				this.fldDescription,
				this.Line2
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Line1,
				this.Field14,
				this.Field15,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Field22,
				this.Field23,
				this.Field24,
				this.Field25
			});
			this.GroupHeader2.Height = 1.145833F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader3
			// 
			this.GroupHeader3.Format += new System.EventHandler(this.GroupHeader3_Format);
			this.GroupHeader3.CanShrink = true;
			this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDeptTitle
			});
			this.GroupHeader3.Height = 0.1875F;
			this.GroupHeader3.Name = "GroupHeader3";
			this.GroupHeader3.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter3
			// 
			this.GroupFooter3.Height = 0F;
			this.GroupFooter3.Name = "GroupFooter3";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field26
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.46875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Expense Detail Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.71875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 1.46875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label5.Text = "Label5";
			this.Label5.Top = 0.40625F;
			this.Label5.Width = 4.71875F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 1.46875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label6.Text = "Label6";
			this.Label6.Top = 0.21875F;
			this.Label6.Width = 4.71875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field1.Text = null;
			this.Field1.Top = 0.75F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field2.Text = null;
			this.Field2.Top = 0.75F;
			this.Field2.Visible = false;
			this.Field2.Width = 0.625F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.25F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field3.Text = null;
			this.Field3.Top = 0.75F;
			this.Field3.Visible = false;
			this.Field3.Width = 0.5625F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 1.8125F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field4.Text = null;
			this.Field4.Top = 0.75F;
			this.Field4.Visible = false;
			this.Field4.Width = 0.5625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 2.375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field5.Text = null;
			this.Field5.Top = 0.75F;
			this.Field5.Visible = false;
			this.Field5.Width = 0.5F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 2.875F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field6.Text = null;
			this.Field6.Top = 0.75F;
			this.Field6.Visible = false;
			this.Field6.Width = 0.5F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 3.375F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field7.Text = null;
			this.Field7.Top = 0.75F;
			this.Field7.Visible = false;
			this.Field7.Width = 0.5F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 3.875F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field8.Text = null;
			this.Field8.Top = 0.75F;
			this.Field8.Visible = false;
			this.Field8.Width = 0.5F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 4.375F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field9.Text = null;
			this.Field9.Top = 0.75F;
			this.Field9.Visible = false;
			this.Field9.Width = 0.5F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 4.875F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field10.Text = null;
			this.Field10.Top = 0.75F;
			this.Field10.Visible = false;
			this.Field10.Width = 0.5F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 5.375F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field11.Text = null;
			this.Field11.Top = 0.75F;
			this.Field11.Visible = false;
			this.Field11.Width = 0.5F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 5.875F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field12.Text = null;
			this.Field12.Top = 0.75F;
			this.Field12.Visible = false;
			this.Field12.Width = 0.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.125F;
			this.Line1.Width = 7.5625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5625F;
			this.Line1.Y1 = 1.125F;
			this.Line1.Y2 = 1.125F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 0F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field14.Text = null;
			this.Field14.Top = 0.9375F;
			this.Field14.Visible = false;
			this.Field14.Width = 0.625F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 0.625F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field15.Text = null;
			this.Field15.Top = 0.9375F;
			this.Field15.Visible = false;
			this.Field15.Width = 0.625F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 1.25F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field16.Text = null;
			this.Field16.Top = 0.9375F;
			this.Field16.Visible = false;
			this.Field16.Width = 0.5625F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 1.8125F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field17.Text = null;
			this.Field17.Top = 0.9375F;
			this.Field17.Visible = false;
			this.Field17.Width = 0.5625F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 2.375F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field18.Text = null;
			this.Field18.Top = 0.9375F;
			this.Field18.Visible = false;
			this.Field18.Width = 0.5F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 2.875F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field19.Text = null;
			this.Field19.Top = 0.9375F;
			this.Field19.Visible = false;
			this.Field19.Width = 0.5F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 3.375F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field20.Text = null;
			this.Field20.Top = 0.9375F;
			this.Field20.Visible = false;
			this.Field20.Width = 0.5F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 3.875F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field21.Text = null;
			this.Field21.Top = 0.9375F;
			this.Field21.Visible = false;
			this.Field21.Width = 0.5F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 4.375F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field22.Text = null;
			this.Field22.Top = 0.9375F;
			this.Field22.Visible = false;
			this.Field22.Width = 0.5F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 4.875F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field23.Text = null;
			this.Field23.Top = 0.9375F;
			this.Field23.Visible = false;
			this.Field23.Width = 0.5F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 5.375F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field24.Text = null;
			this.Field24.Top = 0.9375F;
			this.Field24.Visible = false;
			this.Field24.Width = 0.5F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 5.875F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field25.Text = null;
			this.Field25.Top = 0.9375F;
			this.Field25.Visible = false;
			this.Field25.Width = 0.5F;
			// 
			// fldDeptTitle
			// 
			this.fldDeptTitle.Height = 0.1875F;
			this.fldDeptTitle.Left = 0F;
			this.fldDeptTitle.Name = "fldDeptTitle";
			this.fldDeptTitle.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 9pt";
			this.fldDeptTitle.Text = "Field28";
			this.fldDeptTitle.Top = 0F;
			this.fldDeptTitle.Width = 4.59375F;
			// 
			// Field26
			// 
			this.Field26.DataField = "Binder";
			this.Field26.Height = 0.19F;
			this.Field26.Left = 1.8125F;
			this.Field26.Name = "Field26";
			this.Field26.Text = "Binder";
			this.Field26.Top = 0.03125F;
			this.Field26.Visible = false;
			this.Field26.Width = 0.8125F;
			// 
			// fld1
			// 
			this.fld1.Height = 0.1875F;
			this.fld1.Left = 0F;
			this.fld1.MultiLine = false;
			this.fld1.Name = "fld1";
			this.fld1.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; ddo-ch" + "ar-set: 1";
			this.fld1.Text = null;
			this.fld1.Top = 0F;
			this.fld1.Visible = false;
			this.fld1.Width = 0.625F;
			// 
			// fld2
			// 
			this.fld2.Height = 0.1875F;
			this.fld2.Left = 0.625F;
			this.fld2.MultiLine = false;
			this.fld2.Name = "fld2";
			this.fld2.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld2.Text = null;
			this.fld2.Top = 0F;
			this.fld2.Visible = false;
			this.fld2.Width = 0.625F;
			// 
			// fld3
			// 
			this.fld3.Height = 0.1875F;
			this.fld3.Left = 1.25F;
			this.fld3.MultiLine = false;
			this.fld3.Name = "fld3";
			this.fld3.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld3.Text = null;
			this.fld3.Top = 0F;
			this.fld3.Visible = false;
			this.fld3.Width = 0.5625F;
			// 
			// fld4
			// 
			this.fld4.Height = 0.1875F;
			this.fld4.Left = 1.8125F;
			this.fld4.MultiLine = false;
			this.fld4.Name = "fld4";
			this.fld4.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld4.Text = null;
			this.fld4.Top = 0F;
			this.fld4.Visible = false;
			this.fld4.Width = 0.5625F;
			// 
			// fld5
			// 
			this.fld5.Height = 0.1875F;
			this.fld5.Left = 2.375F;
			this.fld5.MultiLine = false;
			this.fld5.Name = "fld5";
			this.fld5.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld5.Text = null;
			this.fld5.Top = 0F;
			this.fld5.Visible = false;
			this.fld5.Width = 0.5F;
			// 
			// fld6
			// 
			this.fld6.Height = 0.1875F;
			this.fld6.Left = 2.875F;
			this.fld6.MultiLine = false;
			this.fld6.Name = "fld6";
			this.fld6.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld6.Text = null;
			this.fld6.Top = 0F;
			this.fld6.Visible = false;
			this.fld6.Width = 0.5F;
			// 
			// fld7
			// 
			this.fld7.Height = 0.1875F;
			this.fld7.Left = 3.375F;
			this.fld7.MultiLine = false;
			this.fld7.Name = "fld7";
			this.fld7.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld7.Text = null;
			this.fld7.Top = 0F;
			this.fld7.Visible = false;
			this.fld7.Width = 0.5F;
			// 
			// fld8
			// 
			this.fld8.Height = 0.1875F;
			this.fld8.Left = 3.875F;
			this.fld8.MultiLine = false;
			this.fld8.Name = "fld8";
			this.fld8.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld8.Text = null;
			this.fld8.Top = 0F;
			this.fld8.Visible = false;
			this.fld8.Width = 0.5F;
			// 
			// fld9
			// 
			this.fld9.Height = 0.1875F;
			this.fld9.Left = 4.375F;
			this.fld9.MultiLine = false;
			this.fld9.Name = "fld9";
			this.fld9.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld9.Text = null;
			this.fld9.Top = 0F;
			this.fld9.Visible = false;
			this.fld9.Width = 0.5F;
			// 
			// fld10
			// 
			this.fld10.Height = 0.1875F;
			this.fld10.Left = 4.875F;
			this.fld10.MultiLine = false;
			this.fld10.Name = "fld10";
			this.fld10.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld10.Text = null;
			this.fld10.Top = 0F;
			this.fld10.Visible = false;
			this.fld10.Width = 0.5F;
			// 
			// fld11
			// 
			this.fld11.Height = 0.1875F;
			this.fld11.Left = 5.375F;
			this.fld11.MultiLine = false;
			this.fld11.Name = "fld11";
			this.fld11.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld11.Text = null;
			this.fld11.Top = 0F;
			this.fld11.Visible = false;
			this.fld11.Width = 0.5F;
			// 
			// fld12
			// 
			this.fld12.Height = 0.1875F;
			this.fld12.Left = 5.875F;
			this.fld12.MultiLine = false;
			this.fld12.Name = "fld12";
			this.fld12.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 8pt; text-a" + "lign: right; ddo-char-set: 1";
			this.fld12.Text = null;
			this.fld12.Top = 0F;
			this.fld12.Visible = false;
			this.fld12.Width = 0.5F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 0F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "background-color: rgb(192,192,192); font-family: \'Tahoma\'; font-size: 9pt; ddo-ch" + "ar-set: 1";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 0F;
			this.fldDescription.Visible = false;
			this.fldDescription.Width = 3.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.1875F;
			this.Line2.Visible = false;
			this.Line2.Width = 7.5625F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5625F;
			this.Line2.Y1 = 0.1875F;
			this.Line2.Y2 = 0.1875F;
			// 
			// rptExpenseDetail
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader3);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter3);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptTitle;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
