//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditCashAccounts.
	/// </summary>
	partial class frmEditCashAccounts : BaseForm
	{
		public fecherFoundation.FCTabControl tabAccounts;
		public fecherFoundation.FCTabPage tabAccounts_Page1;
		public fecherFoundation.FCLabel lblExpense;
		public FCGrid vsAccounts;
		public fecherFoundation.FCTabPage tabAccounts_Page2;
		public FCGrid vsInterest;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileInsert;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditCashAccounts));
			this.tabAccounts = new fecherFoundation.FCTabControl();
			this.tabAccounts_Page1 = new fecherFoundation.FCTabPage();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.tabAccounts_Page2 = new fecherFoundation.FCTabPage();
			this.vsInterest = new fecherFoundation.FCGrid();
			this.lblInterest = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileInsert = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.tabAccounts.SuspendLayout();
			this.tabAccounts_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			this.tabAccounts_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 413);
			this.BottomPanel.Size = new System.Drawing.Size(728, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.tabAccounts);
			this.ClientArea.Size = new System.Drawing.Size(728, 353);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Size = new System.Drawing.Size(728, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(290, 30);
			this.HeaderText.Text = "Edit Bank Cash Accounts";
			// 
			// tabAccounts
			// 
			this.tabAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.tabAccounts.Controls.Add(this.tabAccounts_Page1);
			this.tabAccounts.Controls.Add(this.tabAccounts_Page2);
			this.tabAccounts.Location = new System.Drawing.Point(30, 30);
			this.tabAccounts.Name = "tabAccounts";
			this.tabAccounts.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
			this.tabAccounts.ShowFocusRect = false;
			this.tabAccounts.Size = new System.Drawing.Size(644, 535);
			this.tabAccounts.TabIndex = 0;
			this.tabAccounts.TabsPerRow = 0;
			this.tabAccounts.Text = "Interest Accounts";
			this.tabAccounts.WordWrap = false;
			// 
			// tabAccounts_Page1
			// 
			this.tabAccounts_Page1.Controls.Add(this.lblExpense);
			this.tabAccounts_Page1.Controls.Add(this.vsAccounts);
			this.tabAccounts_Page1.Location = new System.Drawing.Point(1, 50);
			this.tabAccounts_Page1.Name = "tabAccounts_Page1";
			this.tabAccounts_Page1.Text = "Cash Accounts";
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(20, 30);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(400, 16);
			this.lblExpense.TabIndex = 2;
			// 
			// vsAccounts
			// 
			this.vsAccounts.AllowSelection = false;
			this.vsAccounts.AllowUserToResizeColumns = false;
			this.vsAccounts.AllowUserToResizeRows = false;
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccounts.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsAccounts.ColumnHeadersHeight = 30;
			this.vsAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccounts.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsAccounts.DragIcon = null;
			this.vsAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.FrozenCols = 0;
			this.vsAccounts.GridColor = System.Drawing.Color.Empty;
			this.vsAccounts.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.Location = new System.Drawing.Point(20, 66);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = true;
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccounts.RowHeightMin = 0;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.ScrollTipText = null;
			this.vsAccounts.ShowColumnVisibilityMenu = false;
			this.vsAccounts.Size = new System.Drawing.Size(604, 399);
			this.vsAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsAccounts.TabIndex = 1;
			this.vsAccounts.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsAccounts_ChangeEdit);
			this.vsAccounts.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsAccounts_BeforeEdit);
			this.vsAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAccounts_ValidateEdit);
			this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
			this.vsAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAccounts_KeyDownEvent);
			this.vsAccounts.Enter += new System.EventHandler(this.vsAccounts_Enter);
			this.vsAccounts.Click += new System.EventHandler(this.vsAccounts_ClickEvent);
			// 
			// tabAccounts_Page2
			// 
			this.tabAccounts_Page2.Controls.Add(this.vsInterest);
			this.tabAccounts_Page2.Controls.Add(this.lblInterest);
			this.tabAccounts_Page2.Location = new System.Drawing.Point(1, 50);
			this.tabAccounts_Page2.Name = "tabAccounts_Page2";
			this.tabAccounts_Page2.Text = "Interest Accounts";
			// 
			// vsInterest
			// 
			this.vsInterest.AllowSelection = false;
			this.vsInterest.AllowUserToResizeColumns = false;
			this.vsInterest.AllowUserToResizeRows = false;
			this.vsInterest.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsInterest.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsInterest.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsInterest.BackColorBkg = System.Drawing.Color.Empty;
			this.vsInterest.BackColorFixed = System.Drawing.Color.Empty;
			this.vsInterest.BackColorSel = System.Drawing.Color.Empty;
			this.vsInterest.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsInterest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsInterest.ColumnHeadersHeight = 30;
			this.vsInterest.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsInterest.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsInterest.DragIcon = null;
			this.vsInterest.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsInterest.FixedCols = 0;
			this.vsInterest.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsInterest.FrozenCols = 0;
			this.vsInterest.GridColor = System.Drawing.Color.Empty;
			this.vsInterest.GridColorFixed = System.Drawing.Color.Empty;
			this.vsInterest.Location = new System.Drawing.Point(20, 66);
			this.vsInterest.Name = "vsInterest";
			this.vsInterest.ReadOnly = true;
			this.vsInterest.RowHeadersVisible = false;
			this.vsInterest.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsInterest.RowHeightMin = 0;
			this.vsInterest.Rows = 1;
			this.vsInterest.ScrollTipText = null;
			this.vsInterest.ShowColumnVisibilityMenu = false;
			this.vsInterest.Size = new System.Drawing.Size(604, 399);
			this.vsInterest.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsInterest.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsInterest.TabIndex = 3;
			this.vsInterest.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsInterest_ChangeEdit);
			this.vsInterest.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsInterest_BeforeEdit);
			this.vsInterest.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsInterest_ValidateEdit);
			this.vsInterest.CurrentCellChanged += new System.EventHandler(this.vsInterest_RowColChange);
			this.vsInterest.Enter += new System.EventHandler(this.vsInterest_Enter);
			this.vsInterest.Click += new System.EventHandler(this.vsInterest_ClickEvent);
			// 
			// lblInterest
			// 
			this.lblInterest.Location = new System.Drawing.Point(20, 30);
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Size = new System.Drawing.Size(400, 16);
			this.lblInterest.TabIndex = 4;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileInsert,
				this.mnuFileDelete,
				this.mnuSeperator,
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileInsert
			// 
			this.mnuFileInsert.Index = 0;
			this.mnuFileInsert.Name = "mnuFileInsert";
			this.mnuFileInsert.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuFileInsert.Text = "New";
			this.mnuFileInsert.Click += new System.EventHandler(this.mnuFileInsert_Click);
			// 
			// mnuFileDelete
			// 
			this.mnuFileDelete.Index = 1;
			this.mnuFileDelete.Name = "mnuFileDelete";
			this.mnuFileDelete.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuFileDelete.Text = "Delete";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 2;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 3;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(589, 27);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdNew.Size = new System.Drawing.Size(43, 24);
			this.cmdNew.TabIndex = 1;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuFileInsert_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(635, 27);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdDelete.Size = new System.Drawing.Size(54, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(310, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(90, 48);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmEditCashAccounts
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(728, 521);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditCashAccounts";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Bank Cash Accounts";
			this.Load += new System.EventHandler(this.frmEditCashAccounts_Load);
			this.Activated += new System.EventHandler(this.frmEditCashAccounts_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditCashAccounts_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditCashAccounts_KeyPress);
			this.Resize += new System.EventHandler(this.frmEditCashAccounts_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.tabAccounts.ResumeLayout(false);
			this.tabAccounts_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			this.tabAccounts_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton cmdSave;
	}
}