﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetWorksheetSetup.
	/// </summary>
	partial class frmBudgetWorksheetSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRevenue;
		public fecherFoundation.FCLabel lblRevenue;
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public fecherFoundation.FCComboBox cmbNo;
		public fecherFoundation.FCLabel lblNo;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCCheckBox chkOnlyShowChangedAmounts;
		public fecherFoundation.FCCheckBox chkIncludeBudgetAdjustments;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtReportTitle;
		public fecherFoundation.FCFrame fraBudgetYear;
		public fecherFoundation.FCComboBox cboBudgetYear;
		public fecherFoundation.FCFrame fraCurrentYear;
		public fecherFoundation.FCComboBox cboCurrentYear;
		public fecherFoundation.FCFrame fraTownPageBreaks;
		public fecherFoundation.FCCheckBox chkDepartment;
		public fecherFoundation.FCCheckBox chkDivision;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public fecherFoundation.FCComboBox cboSingleDivision;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblDepartment;
		public fecherFoundation.FCLabel lblDivision;
		public fecherFoundation.FCFrame fraTownTotals;
		public fecherFoundation.FCCheckBox chkDeptTotal;
		public fecherFoundation.FCCheckBox chkDivTotal;
		public fecherFoundation.FCCheckBox chkExpTotal;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBudgetWorksheetSetup));
			this.cmbRevenue = new fecherFoundation.FCComboBox();
			this.lblRevenue = new fecherFoundation.FCLabel();
			this.cmbAllAccounts = new fecherFoundation.FCComboBox();
			this.lblAllAccounts = new fecherFoundation.FCLabel();
			this.cmbNo = new fecherFoundation.FCComboBox();
			this.lblNo = new fecherFoundation.FCLabel();
			this.chkOnlyShowChangedAmounts = new fecherFoundation.FCCheckBox();
			this.chkIncludeBudgetAdjustments = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtReportTitle = new fecherFoundation.FCTextBox();
			this.fraBudgetYear = new fecherFoundation.FCFrame();
			this.cboBudgetYear = new fecherFoundation.FCComboBox();
			this.fraCurrentYear = new fecherFoundation.FCFrame();
			this.cboCurrentYear = new fecherFoundation.FCComboBox();
			this.fraTownPageBreaks = new fecherFoundation.FCFrame();
			this.chkDepartment = new fecherFoundation.FCCheckBox();
			this.chkDivision = new fecherFoundation.FCCheckBox();
			this.fraDeptRange = new fecherFoundation.FCFrame();
			this.cboBeginningDept = new fecherFoundation.FCComboBox();
			this.cboEndingDept = new fecherFoundation.FCComboBox();
			this.cboSingleDept = new fecherFoundation.FCComboBox();
			this.cboSingleDivision = new fecherFoundation.FCComboBox();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.lblDepartment = new fecherFoundation.FCLabel();
			this.lblDivision = new fecherFoundation.FCLabel();
			this.fraTownTotals = new fecherFoundation.FCFrame();
			this.chkDeptTotal = new fecherFoundation.FCCheckBox();
			this.chkDivTotal = new fecherFoundation.FCCheckBox();
			this.chkExpTotal = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOnlyShowChangedAmounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeBudgetAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBudgetYear)).BeginInit();
			this.fraBudgetYear.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCurrentYear)).BeginInit();
			this.fraCurrentYear.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTownPageBreaks)).BeginInit();
			this.fraTownPageBreaks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDivision)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
			this.fraDeptRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTownTotals)).BeginInit();
			this.fraTownTotals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeptTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDivTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExpTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 431);
			this.BottomPanel.Size = new System.Drawing.Size(658, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkOnlyShowChangedAmounts);
			this.ClientArea.Controls.Add(this.chkIncludeBudgetAdjustments);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraBudgetYear);
			this.ClientArea.Controls.Add(this.fraCurrentYear);
			this.ClientArea.Controls.Add(this.cmbRevenue);
			this.ClientArea.Controls.Add(this.lblRevenue);
			this.ClientArea.Controls.Add(this.fraTownPageBreaks);
			this.ClientArea.Controls.Add(this.fraDeptRange);
			this.ClientArea.Controls.Add(this.cmbNo);
			this.ClientArea.Controls.Add(this.lblNo);
			this.ClientArea.Controls.Add(this.fraTownTotals);
			this.ClientArea.Size = new System.Drawing.Size(658, 371);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(658, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(312, 30);
			this.HeaderText.Text = "Request Worksheets Setup";
			// 
			// cmbRevenue
			// 
			this.cmbRevenue.AutoSize = false;
			this.cmbRevenue.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRevenue.FormattingEnabled = true;
			this.cmbRevenue.Items.AddRange(new object[] {
				"All Accounts",
				"Expense Accounts",
				"Revenue Accounts"
			});
			this.cmbRevenue.Location = new System.Drawing.Point(197, 140);
			this.cmbRevenue.Name = "cmbRevenue";
			this.cmbRevenue.Size = new System.Drawing.Size(203, 40);
			this.cmbRevenue.TabIndex = 36;
			this.cmbRevenue.SelectedIndexChanged += new System.EventHandler(this.cmbRevenue_SelectedIndexChanged);
			// 
			// lblRevenue
			// 
			this.lblRevenue.Location = new System.Drawing.Point(30, 154);
			this.lblRevenue.Name = "lblRevenue";
			this.lblRevenue.Size = new System.Drawing.Size(125, 16);
			this.lblRevenue.TabIndex = 37;
			this.lblRevenue.Text = "ACCOUNT CATEGORY";
			// 
			// cmbAllAccounts
			// 
			this.cmbAllAccounts.AutoSize = false;
			this.cmbAllAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllAccounts.FormattingEnabled = true;
			this.cmbAllAccounts.Items.AddRange(new object[] {
				"All",
				"Single Division",
				"Single Department",
				"Department Range"
			});
			this.cmbAllAccounts.Location = new System.Drawing.Point(167, 30);
			this.cmbAllAccounts.Name = "cmbAllAccounts";
			this.cmbAllAccounts.Size = new System.Drawing.Size(203, 40);
			this.cmbAllAccounts.TabIndex = 14;
			this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAllAccounts_SelectedIndexChanged);
			// 
			// lblAllAccounts
			// 
			this.lblAllAccounts.Location = new System.Drawing.Point(20, 44);
			this.lblAllAccounts.Name = "lblAllAccounts";
			this.lblAllAccounts.Size = new System.Drawing.Size(112, 16);
			this.lblAllAccounts.TabIndex = 15;
			this.lblAllAccounts.Text = "SELECT ACCOUNTS";
			// 
			// cmbNo
			// 
			this.cmbNo.AutoSize = false;
			this.cmbNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNo.FormattingEnabled = true;
			this.cmbNo.Items.AddRange(new object[] {
				"No",
				"Yes"
			});
			this.cmbNo.Location = new System.Drawing.Point(477, 430);
			this.cmbNo.Name = "cmbNo";
			this.cmbNo.Size = new System.Drawing.Size(73, 40);
			this.cmbNo.TabIndex = 38;
			// 
			// lblNo
			// 
			this.lblNo.Location = new System.Drawing.Point(372, 444);
			this.lblNo.Name = "lblNo";
			this.lblNo.Size = new System.Drawing.Size(70, 16);
			this.lblNo.TabIndex = 39;
			this.lblNo.Text = "COMMENTS";
			// 
			// chkOnlyShowChangedAmounts
			// 
			this.chkOnlyShowChangedAmounts.AutoSize = false;
			this.chkOnlyShowChangedAmounts.Location = new System.Drawing.Point(30, 626);
			this.chkOnlyShowChangedAmounts.Name = "chkOnlyShowChangedAmounts";
			this.chkOnlyShowChangedAmounts.Size = new System.Drawing.Size(240, 26);
			this.chkOnlyShowChangedAmounts.TabIndex = 35;
			this.chkOnlyShowChangedAmounts.Text = "Only Show Changed Amounts";
			// 
			// chkIncludeBudgetAdjustments
			// 
			this.chkIncludeBudgetAdjustments.AutoSize = false;
			this.chkIncludeBudgetAdjustments.Checked = true;
			this.chkIncludeBudgetAdjustments.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkIncludeBudgetAdjustments.Location = new System.Drawing.Point(30, 592);
			this.chkIncludeBudgetAdjustments.Name = "chkIncludeBudgetAdjustments";
			this.chkIncludeBudgetAdjustments.Size = new System.Drawing.Size(225, 24);
			this.chkIncludeBudgetAdjustments.TabIndex = 34;
			this.chkIncludeBudgetAdjustments.Text = "Include Budget Adjustments";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtReportTitle);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(390, 90);
			this.Frame1.TabIndex = 32;
			this.Frame1.Text = "Report Title";
			// 
			// txtReportTitle
			// 
			this.txtReportTitle.AutoSize = false;
			this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportTitle.Location = new System.Drawing.Point(20, 30);
			this.txtReportTitle.Name = "txtReportTitle";
			this.txtReportTitle.Size = new System.Drawing.Size(350, 40);
			this.txtReportTitle.TabIndex = 33;
			this.txtReportTitle.Text = "Text1";
			// 
			// fraBudgetYear
			// 
			this.fraBudgetYear.Controls.Add(this.cboBudgetYear);
			this.fraBudgetYear.Location = new System.Drawing.Point(440, 200);
			this.fraBudgetYear.Name = "fraBudgetYear";
			this.fraBudgetYear.Size = new System.Drawing.Size(130, 90);
			this.fraBudgetYear.TabIndex = 30;
			this.fraBudgetYear.Text = "Budget Year";
			// 
			// cboBudgetYear
			// 
			this.cboBudgetYear.AutoSize = false;
			this.cboBudgetYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboBudgetYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBudgetYear.FormattingEnabled = true;
			this.cboBudgetYear.Location = new System.Drawing.Point(20, 30);
			this.cboBudgetYear.Name = "cboBudgetYear";
			this.cboBudgetYear.Size = new System.Drawing.Size(90, 40);
			this.cboBudgetYear.TabIndex = 31;
			// 
			// fraCurrentYear
			// 
			this.fraCurrentYear.Controls.Add(this.cboCurrentYear);
			this.fraCurrentYear.Location = new System.Drawing.Point(440, 320);
			this.fraCurrentYear.Name = "fraCurrentYear";
			this.fraCurrentYear.Size = new System.Drawing.Size(130, 90);
			this.fraCurrentYear.TabIndex = 28;
			this.fraCurrentYear.Text = "Current Year";
			// 
			// cboCurrentYear
			// 
			this.cboCurrentYear.AutoSize = false;
			this.cboCurrentYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboCurrentYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCurrentYear.FormattingEnabled = true;
			this.cboCurrentYear.Location = new System.Drawing.Point(20, 30);
			this.cboCurrentYear.Name = "cboCurrentYear";
			this.cboCurrentYear.Size = new System.Drawing.Size(90, 40);
			this.cboCurrentYear.TabIndex = 29;
			// 
			// fraTownPageBreaks
			// 
			this.fraTownPageBreaks.Controls.Add(this.chkDepartment);
			this.fraTownPageBreaks.Controls.Add(this.chkDivision);
			this.fraTownPageBreaks.Location = new System.Drawing.Point(30, 430);
			this.fraTownPageBreaks.Name = "fraTownPageBreaks";
			this.fraTownPageBreaks.Size = new System.Drawing.Size(152, 108);
			this.fraTownPageBreaks.TabIndex = 21;
			this.fraTownPageBreaks.Text = "Page Breaks";
			// 
			// chkDepartment
			// 
			this.chkDepartment.AutoSize = false;
			this.chkDepartment.Checked = true;
			this.chkDepartment.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkDepartment.Location = new System.Drawing.Point(20, 30);
			this.chkDepartment.Name = "chkDepartment";
			this.chkDepartment.Size = new System.Drawing.Size(112, 24);
			this.chkDepartment.TabIndex = 23;
			this.chkDepartment.Text = "Department";
			// 
			// chkDivision
			// 
			this.chkDivision.AutoSize = false;
			this.chkDivision.Location = new System.Drawing.Point(20, 64);
			this.chkDivision.Name = "chkDivision";
			this.chkDivision.Size = new System.Drawing.Size(82, 24);
			this.chkDivision.TabIndex = 22;
			this.chkDivision.Text = "Division";
			// 
			// fraDeptRange
			// 
			this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
			this.fraDeptRange.Controls.Add(this.cboBeginningDept);
			this.fraDeptRange.Controls.Add(this.cmbAllAccounts);
			this.fraDeptRange.Controls.Add(this.lblAllAccounts);
			this.fraDeptRange.Controls.Add(this.cboEndingDept);
			this.fraDeptRange.Controls.Add(this.cboSingleDept);
			this.fraDeptRange.Controls.Add(this.cboSingleDivision);
			this.fraDeptRange.Controls.Add(this.lblTo_2);
			this.fraDeptRange.Controls.Add(this.lblDepartment);
			this.fraDeptRange.Controls.Add(this.lblDivision);
			this.fraDeptRange.Location = new System.Drawing.Point(30, 200);
			this.fraDeptRange.Name = "fraDeptRange";
			this.fraDeptRange.Size = new System.Drawing.Size(390, 210);
			this.fraDeptRange.TabIndex = 9;
			this.fraDeptRange.Text = "Accounts To Be Reported";
			// 
			// cboBeginningDept
			// 
			this.cboBeginningDept.AutoSize = false;
			this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningDept.FormattingEnabled = true;
			this.cboBeginningDept.Location = new System.Drawing.Point(167, 90);
			this.cboBeginningDept.Name = "cboBeginningDept";
			this.cboBeginningDept.Size = new System.Drawing.Size(75, 40);
			this.cboBeginningDept.TabIndex = 13;
			this.cboBeginningDept.Visible = false;
			this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
			// 
			// cboEndingDept
			// 
			this.cboEndingDept.AutoSize = false;
			this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingDept.FormattingEnabled = true;
			this.cboEndingDept.Location = new System.Drawing.Point(295, 90);
			this.cboEndingDept.Name = "cboEndingDept";
			this.cboEndingDept.Size = new System.Drawing.Size(75, 40);
			this.cboEndingDept.TabIndex = 12;
			this.cboEndingDept.Visible = false;
			this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
			// 
			// cboSingleDept
			// 
			this.cboSingleDept.AutoSize = false;
			this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleDept.FormattingEnabled = true;
			this.cboSingleDept.Location = new System.Drawing.Point(167, 90);
			this.cboSingleDept.Name = "cboSingleDept";
			this.cboSingleDept.Size = new System.Drawing.Size(75, 40);
			this.cboSingleDept.TabIndex = 11;
			this.cboSingleDept.Visible = false;
			this.cboSingleDept.SelectedIndexChanged += new System.EventHandler(this.cboSingleDept_SelectedIndexChanged);
			this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
			// 
			// cboSingleDivision
			// 
			this.cboSingleDivision.AutoSize = false;
			this.cboSingleDivision.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDivision.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleDivision.FormattingEnabled = true;
			this.cboSingleDivision.Location = new System.Drawing.Point(167, 150);
			this.cboSingleDivision.Name = "cboSingleDivision";
			this.cboSingleDivision.Size = new System.Drawing.Size(75, 40);
			this.cboSingleDivision.TabIndex = 10;
			this.cboSingleDivision.Visible = false;
			this.cboSingleDivision.DropDown += new System.EventHandler(this.cboSingleDivision_DropDown);
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_2.Location = new System.Drawing.Point(258, 104);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(20, 16);
			this.lblTo_2.TabIndex = 20;
			this.lblTo_2.Text = "TO";
			this.lblTo_2.Visible = false;
			// 
			// lblDepartment
			// 
			this.lblDepartment.Location = new System.Drawing.Point(20, 104);
			this.lblDepartment.Name = "lblDepartment";
			this.lblDepartment.Size = new System.Drawing.Size(78, 16);
			this.lblDepartment.TabIndex = 19;
			this.lblDepartment.Text = "DEPARTMENT";
			this.lblDepartment.Visible = false;
			// 
			// lblDivision
			// 
			this.lblDivision.Location = new System.Drawing.Point(20, 164);
			this.lblDivision.Name = "lblDivision";
			this.lblDivision.Size = new System.Drawing.Size(52, 16);
			this.lblDivision.TabIndex = 18;
			this.lblDivision.Text = "DIVISION";
			this.lblDivision.Visible = false;
			// 
			// fraTownTotals
			// 
			this.fraTownTotals.Controls.Add(this.chkDeptTotal);
			this.fraTownTotals.Controls.Add(this.chkDivTotal);
			this.fraTownTotals.Controls.Add(this.chkExpTotal);
			this.fraTownTotals.Location = new System.Drawing.Point(202, 430);
			this.fraTownTotals.Name = "fraTownTotals";
			this.fraTownTotals.Size = new System.Drawing.Size(150, 142);
			this.fraTownTotals.TabIndex = 0;
			this.fraTownTotals.Text = "Totals";
			// 
			// chkDeptTotal
			// 
			this.chkDeptTotal.AutoSize = false;
			this.chkDeptTotal.Checked = true;
			this.chkDeptTotal.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkDeptTotal.Location = new System.Drawing.Point(20, 30);
			this.chkDeptTotal.Name = "chkDeptTotal";
			this.chkDeptTotal.Size = new System.Drawing.Size(113, 24);
			this.chkDeptTotal.TabIndex = 3;
			this.chkDeptTotal.Text = "Department";
			// 
			// chkDivTotal
			// 
			this.chkDivTotal.AutoSize = false;
			this.chkDivTotal.Location = new System.Drawing.Point(20, 64);
			this.chkDivTotal.Name = "chkDivTotal";
			this.chkDivTotal.Size = new System.Drawing.Size(85, 24);
			this.chkDivTotal.TabIndex = 2;
			this.chkDivTotal.Text = "Division";
			// 
			// chkExpTotal
			// 
			this.chkExpTotal.AutoSize = false;
			this.chkExpTotal.Location = new System.Drawing.Point(20, 98);
			this.chkExpTotal.Name = "chkExpTotal";
			this.chkExpTotal.Size = new System.Drawing.Size(91, 24);
			this.chkExpTotal.TabIndex = 1;
			this.chkExpTotal.Text = "Expense";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print / Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(280, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(99, 48);
			this.cmdProcess.TabIndex = 2;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmBudgetWorksheetSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(658, 539);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBudgetWorksheetSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Request Worksheets Setup";
			this.Load += new System.EventHandler(this.frmBudgetWorksheetSetup_Load);
			this.Activated += new System.EventHandler(this.frmBudgetWorksheetSetup_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBudgetWorksheetSetup_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOnlyShowChangedAmounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIncludeBudgetAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraBudgetYear)).EndInit();
			this.fraBudgetYear.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraCurrentYear)).EndInit();
			this.fraCurrentYear.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTownPageBreaks)).EndInit();
			this.fraTownPageBreaks.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDivision)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
			this.fraDeptRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTownTotals)).EndInit();
			this.fraTownTotals.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkDeptTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDivTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExpTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdProcess;
	}
}
