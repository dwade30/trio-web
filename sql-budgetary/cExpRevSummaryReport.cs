﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cExpRevSummaryReport : cDetailsReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection listDetails = new cGenericCollection();
		private cGenericCollection listDetails_AutoInitialized;

		private cGenericCollection listDetails
		{
			get
			{
				if (listDetails_AutoInitialized == null)
				{
					listDetails_AutoInitialized = new cGenericCollection();
				}
				return listDetails_AutoInitialized;
			}
			set
			{
				listDetails_AutoInitialized = value;
			}
		}

		private string strRangeType = string.Empty;
		private string strLowestLevel = string.Empty;
		private string strBeginRange = string.Empty;
		private string strEndRange = string.Empty;
		private string strDateRangeType = string.Empty;
		private bool boolIncludeEncumbrances;
		private bool boolIncludePending;
		private bool boolDeptPageBreak;
		private string strShowPLBy = string.Empty;
		private string strDateRangeStart = string.Empty;
		private string strDateRangeEnd = string.Empty;

		public string DateRangeStart
		{
			set
			{
				strDateRangeStart = value;
			}
			get
			{
				string DateRangeStart = "";
				DateRangeStart = strDateRangeStart;
				return DateRangeStart;
			}
		}

		public string DateRangeEnd
		{
			set
			{
				strDateRangeEnd = value;
			}
			get
			{
				string DateRangeEnd = "";
				DateRangeEnd = strDateRangeEnd;
				return DateRangeEnd;
			}
		}

		public string RangeType
		{
			set
			{
				strRangeType = value;
			}
			get
			{
				string RangeType = "";
				RangeType = strRangeType;
				return RangeType;
			}
		}

		public string LowestLevel
		{
			set
			{
				strLowestLevel = value;
			}
			get
			{
				string LowestLevel = "";
				LowestLevel = strLowestLevel;
				return LowestLevel;
			}
		}

		public string RangeStart
		{
			set
			{
				strBeginRange = value;
			}
			get
			{
				string RangeStart = "";
				RangeStart = strBeginRange;
				return RangeStart;
			}
		}

		public string RangeEnd
		{
			set
			{
				strEndRange = value;
			}
			get
			{
				string RangeEnd = "";
				RangeEnd = strEndRange;
				return RangeEnd;
			}
		}

		public string DateRangeType
		{
			set
			{
				strDateRangeType = value;
			}
			get
			{
				string DateRangeType = "";
				DateRangeType = strDateRangeType;
				return DateRangeType;
			}
		}

		public bool IncludeEncumbrances
		{
			set
			{
				boolIncludeEncumbrances = value;
			}
			get
			{
				bool IncludeEncumbrances = false;
				IncludeEncumbrances = boolIncludeEncumbrances;
				return IncludeEncumbrances;
			}
		}

		public bool IncludePending
		{
			set
			{
				boolIncludePending = value;
			}
			get
			{
				bool IncludePending = false;
				IncludePending = boolIncludePending;
				return IncludePending;
			}
		}

		public bool DepartmentPageBreak
		{
			set
			{
				boolDeptPageBreak = value;
			}
			get
			{
				bool DepartmentPageBreak = false;
				DepartmentPageBreak = boolDeptPageBreak;
				return DepartmentPageBreak;
			}
		}

		public string ShowPLBy
		{
			set
			{
				strShowPLBy = value;
			}
			get
			{
				string ShowPLBy = "";
				ShowPLBy = strShowPLBy;
				return ShowPLBy;
			}
		}

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				Details = listDetails;
				return Details;
			}
		}
	}
}
