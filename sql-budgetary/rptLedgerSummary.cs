﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptLedgerSummary.
	/// </summary>
	public partial class rptLedgerSummary : BaseSectionReport
	{
		public static rptLedgerSummary InstancePtr
		{
			get
			{
				return (rptLedgerSummary)Sys.GetInstance(typeof(rptLedgerSummary));
			}
		}

		protected rptLedgerSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLedgerSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool BreakFlag;
		clsDRWrapper rs = new clsDRWrapper();
		int counter;
		int counter2;
		int TotRows;
		int counter3;
		float lngTotalWidth;
		int temp;
		int lngRowCounter;
		bool blnFirstRow;
		int lngColumnCounter;
		bool blnShade = true;
		bool blnReportShading;
		string strTitleToShow = "";
		bool IsColorChanged = false;

		public rptLedgerSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Ledger Summary";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngTempCounter;
			lngTempCounter = 0;
			if (lngRowCounter > frmLedgerSummary.InstancePtr.vs1.Rows - 1)
			{
				eArgs.EOF = true;
				return;
			}
			if (blnFirstRow)
			{
				blnFirstRow = false;
				return;
			}
			if (frmLedgerSummary.InstancePtr.vs1.IsCollapsed(lngRowCounter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				do
				{
					lngTempCounter += 1;
					if (lngRowCounter + lngTempCounter > frmLedgerSummary.InstancePtr.vs1.Rows)
					{
						eArgs.EOF = true;
						return;
					}
				}
				while (!(frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) >= frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter + lngTempCounter)));
				lngRowCounter += lngTempCounter;
			}
			else
			{
				lngRowCounter += 1;
				if (lngRowCounter > frmLedgerSummary.InstancePtr.vs1.Rows - 1)
				{
					eArgs.EOF = true;
					return;
				}
			}
			eArgs.EOF = false;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intTotalCols;
			double dblAdjust;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			lngRowCounter = 0 + frmLedgerSummary.InstancePtr.vs1.FixedRows;
			blnFirstRow = true;
			dblAdjust = Conversion.Val(modRegistry.GetRegistryKey("REPORTADJUSTMENT"));
			this.PageSettings.Margins.Top += FCConvert.ToSingle(200 * dblAdjust / 1440f);
			if (frmLedgerSummary.InstancePtr.strTitle != "")
			{
				Label1.Text = frmLedgerSummary.InstancePtr.strTitle;
			}
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ShadeReports")) == "Y")
			{
				blnReportShading = true;
			}
			else
			{
				blnReportShading = false;
			}
			// get the report format for the report we want printed
			rs.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				// do nothing
			}
			else
			{
				Label1.Font = new Font("Roman 10cpi", Label1.Font.Size);
				Label1.Font = new Font(Label1.Font.Name, 10, FontStyle.Bold);
				Label2.Font = new Font("Roman 12cpi", Label2.Font.Size);
				Label2.Font = new Font(Label2.Font.Name, 10);
				Label3.Font = new Font("Roman 12cpi", Label3.Font.Size);
				Label3.Font = new Font(Label3.Font.Name, 10);
				Label4.Font = new Font("Roman 12cpi", Label4.Font.Size);
				Label4.Font = new Font(Label4.Font.Name, 10);
				Label5.Font = new Font("Roman 12cpi", Label5.Font.Size);
				Label5.Font = new Font(Label5.Font.Name, 10);
				Label6.Font = new Font("Roman 12cpi", Label6.Font.Size);
				Label6.Font = new Font(Label6.Font.Name, 10);
				Label7.Font = new Font("Roman 12cpi", Label7.Font.Size);
				Label7.Font = new Font(Label7.Font.Name, 10);
			}
			// if we are using wide paper let the print object know what type we are using
			if (FCConvert.ToString(rs.Get_Fields_String("PaperWidth")) == "W")
			{
				//rptLedgerSummary.InstancePtr.Document.Printer.PaperSize = 39;
				this.PrintWidth = 20420 / 1440f;
				Line1.X2 = this.PrintWidth - 400 / 1440f;
				Label1.Width = this.PrintWidth - (Label3.Width * 2);
				Label5.Width = this.PrintWidth - (Label3.Width * 2);
				Label6.Width = this.PrintWidth - (Label3.Width * 2);
				Label3.Left = this.PrintWidth - (Label3.Width + 400 / 1440f);
				Label4.Left = this.PrintWidth - (Label4.Width + 400 / 1440f);
			}
			else if (rs.Get_Fields_String("PaperWidth") == "L")
			{
				rptLedgerSummary.InstancePtr.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 15100 / 1440f;
				Line1.X2 = this.PrintWidth - 400 / 1440f;
				Label1.Width = this.PrintWidth - (Label3.Width * 2);
				Label5.Width = this.PrintWidth - (Label3.Width * 2);
				Label6.Width = this.PrintWidth - (Label3.Width * 2);
				Label3.Left = this.PrintWidth - (Label3.Width + 400 / 1440f);
				Label4.Left = this.PrintWidth - (Label4.Width + 400 / 1440f);
			}
			fldDeptTitle.Width = this.PrintWidth - 400 / 1440f;
			// show information about what departments are being reported for which months
			// If frmLedgerSummary.lblRangeDept <> "ALL" Then
			Label6.Text = frmLedgerSummary.InstancePtr.lblTitle.Text + ": " + frmLedgerSummary.InstancePtr.lblRangeDept.Text;
			Label5.Text = frmLedgerSummary.InstancePtr.lblMonths;
			// Else
			// Label6 = ""
			// Label5 = frmLedgerSummary.lblMonths
			// End If
			lngTotalWidth = 0;
			lngColumnCounter = 1;
			intTotalCols = 0;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 2)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field1, 0);
					FormatFixedFields_6(Field14, 1);
				}
				else
				{
					FormatFixedFields_6(Field14, 1);
				}
				FormatFields(ref fld1);
				intTotalCols += 1;
			}
			lngColumnCounter = 2;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 3)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field2, 0);
					FormatFixedFields_6(Field15, 1);
				}
				else
				{
					FormatFixedFields_6(Field15, 1);
				}
				FormatFields(ref fld2);
				intTotalCols += 1;
			}
			lngColumnCounter = 3;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 4)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field3, 0);
					FormatFixedFields_6(Field16, 1);
				}
				else
				{
					FormatFixedFields_6(Field16, 1);
				}
				FormatFields(ref fld3);
				intTotalCols += 1;
			}
			lngColumnCounter = 4;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 5)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field4, 0);
					FormatFixedFields_6(Field17, 1);
				}
				else
				{
					FormatFixedFields_6(Field17, 1);
				}
				FormatFields(ref fld4);
				intTotalCols += 1;
			}
			lngColumnCounter = 5;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 6)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field5, 0);
					FormatFixedFields_6(Field18, 1);
				}
				else
				{
					FormatFixedFields_6(Field18, 1);
				}
				FormatFields(ref fld5);
				intTotalCols += 1;
			}
			lngColumnCounter = 6;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 7)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field6, 0);
					FormatFixedFields_6(Field19, 1);
				}
				else
				{
					FormatFixedFields_6(Field19, 1);
				}
				FormatFields(ref fld6);
				intTotalCols += 1;
			}
			lngColumnCounter = 7;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 8)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field7, 0);
					FormatFixedFields_6(Field20, 1);
				}
				else
				{
					FormatFixedFields_6(Field20, 1);
				}
				FormatFields(ref fld7);
				intTotalCols += 1;
			}
			lngColumnCounter = 8;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 9)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field8, 0);
					FormatFixedFields_6(Field21, 1);
				}
				else
				{
					FormatFixedFields_6(Field21, 1);
				}
				FormatFields(ref fld8);
				intTotalCols += 1;
			}
			lngColumnCounter = 9;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 10)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field9, 0);
					FormatFixedFields_6(Field22, 1);
				}
				else
				{
					FormatFixedFields_6(Field22, 1);
				}
				FormatFields(ref fld9);
				intTotalCols += 1;
			}
			lngColumnCounter = 10;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 11)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field10, 0);
					FormatFixedFields_6(Field23, 1);
				}
				else
				{
					FormatFixedFields_6(Field23, 1);
				}
				FormatFields(ref fld10);
				intTotalCols += 1;
			}
			lngColumnCounter = 11;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 12)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field11, 0);
					FormatFixedFields_6(Field24, 1);
				}
				else
				{
					FormatFixedFields_6(Field24, 1);
				}
				FormatFields(ref fld11);
				intTotalCols += 1;
			}
			lngColumnCounter = 12;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 13)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field12, 0);
					FormatFixedFields_6(Field25, 1);
				}
				else
				{
					FormatFixedFields_6(Field25, 1);
				}
				FormatFields(ref fld12);
				intTotalCols += 1;
			}
			lngColumnCounter = 13;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 14)
			{
				if (frmLedgerSummary.InstancePtr.vs1.FixedRows == 2)
				{
					FormatFixedFields_6(Field13, 0);
					FormatFixedFields_6(Field26, 1);
				}
				else
				{
					FormatFixedFields_6(Field26, 1);
				}
				FormatFields(ref fld13);
				intTotalCols += 1;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "L" || FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
			{
				CenterYTDHeading(intTotalCols);
				CenterCurrentHeading(intTotalCols);
				CenterBalanceHeading(intTotalCols);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngColumnCounter = 1;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 2)
			{
				PrintFields(ref fld1);
			}
			lngColumnCounter = 2;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 3)
			{
				PrintFields(ref fld2);
			}
			lngColumnCounter = 3;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 4)
			{
				PrintFields(ref fld3);
			}
			lngColumnCounter = 4;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 5)
			{
				PrintFields(ref fld4);
			}
			lngColumnCounter = 5;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 6)
			{
				PrintFields(ref fld5);
			}
			lngColumnCounter = 6;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 7)
			{
				PrintFields(ref fld6);
			}
			lngColumnCounter = 7;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 8)
			{
				PrintFields(ref fld7);
			}
			lngColumnCounter = 8;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 9)
			{
				PrintFields(ref fld8);
			}
			lngColumnCounter = 9;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 10)
			{
				PrintFields(ref fld9);
			}
			lngColumnCounter = 10;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 11)
			{
				PrintFields(ref fld10);
			}
			lngColumnCounter = 11;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 12)
			{
				PrintFields(ref fld11);
			}
			lngColumnCounter = 12;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 13)
			{
				PrintFields(ref fld12);
			}
			lngColumnCounter = 13;
			if (frmLedgerSummary.InstancePtr.vs1.Cols >= 14)
			{
				PrintFields(ref fld13);
			}
			//FC:FINAL:MSH - Issue #686 - moved for correct colors changing
			if (blnReportShading && !IsColorChanged)
			{
				//this.Detail.BackStyle = ddBKNormal;
				if (blnShade)
				{
					blnShade = false;
					this.Detail.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld1.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld2.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld3.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld4.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld5.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld6.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld7.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld8.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld9.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld10.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld11.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld12.BackColor = ColorTranslator.FromOle(0xE0E0E0);
					fld13.BackColor = ColorTranslator.FromOle(0xE0E0E0);
				}
				else
				{
					blnShade = true;
					this.Detail.BackColor = Color.White;
					fld1.BackColor = Color.White;
					fld2.BackColor = Color.White;
					fld3.BackColor = Color.White;
					fld4.BackColor = Color.White;
					fld5.BackColor = Color.White;
					fld6.BackColor = Color.White;
					fld7.BackColor = Color.White;
					fld8.BackColor = Color.White;
					fld9.BackColor = Color.White;
					fld10.BackColor = Color.White;
					fld11.BackColor = Color.White;
					fld12.BackColor = Color.White;
					fld13.BackColor = Color.White;
				}
			}
		}

		private void FormatFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			clsDRWrapper rsFormat = new clsDRWrapper();
			rsFormat.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			x.Visible = true;
			x.Left = lngTotalWidth;
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				x.Width = (frmLedgerSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f) / 1440f;
			}
			else
			{
				x.Width = (frmLedgerSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f - 100) / 1440f;
			}
			x.OutputFormat = frmLedgerSummary.InstancePtr.vs1.ColFormat(lngColumnCounter);
			lngTotalWidth += x.Width;
			if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 2)
			{
				fld1.Height = 290 / 1440f;
				rptLedgerSummary.InstancePtr.Detail.Height = fld1.Height;
				fld1.Font = new Font(fld1.Font, FontStyle.Bold);
			}
			else
			{
				fld1.Height = 240 / 1440f;
				rptLedgerSummary.InstancePtr.Detail.Height = fld1.Height;
				fld1.Font = new Font(fld1.Font, FontStyle.Regular);
			}
			if (FCConvert.ToString(rsFormat.Get_Fields_String("Font")) == "S")
			{
				x.Font = new Font(x.Font.Name, 10);
			}
			else if (rsFormat.Get_Fields_String("Font") == "L")
			{
				x.Font = new Font(x.Font.Name, 12);
			}
			else
			{
				FontStyle style = FontStyle.Regular;
				if (rsFormat.Get_Fields_Boolean("Bold"))
					style = FontStyle.Bold;
				if (rsFormat.Get_Fields_Boolean("Italic"))
					style |= FontStyle.Italic;
				if (rsFormat.Get_Fields_Boolean("StrikeThru"))
					style |= FontStyle.Strikeout;
				if (rsFormat.Get_Fields_Boolean("Underline"))
					style |= FontStyle.Underline;
				// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
				x.Font = new Font(rsFormat.Get_Fields_String("FontName"), FCConvert.ToInt32(rsFormat.Get_Fields("FontSize")) + 2, style);
			}
			rsFormat.Dispose();
		}

		private void PrintFields(ref GrapeCity.ActiveReports.SectionReportModel.TextBox x)
		{
			if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
			{
				strTitleToShow = frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, 1);
				//fldDeptTitle.BackStyle = 1;
				fldDeptTitle.Height = 290 / 1440f;
				fldDeptTitle.Font = new Font(fldDeptTitle.Font, FontStyle.Regular);
				fldDeptTitle.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
				if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
				{
					if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
					{
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 8);
					}
					else if (rs.Get_Fields_String("Font") == "L")
					{
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 10);
					}
					else
					{
						FontStyle style = FontStyle.Regular;
						if (rs.Get_Fields_Boolean("Bold"))
							style = FontStyle.Bold;
						if (rs.Get_Fields_Boolean("Italic"))
							style |= FontStyle.Italic;
						if (rs.Get_Fields_Boolean("StrikeThru"))
							style |= FontStyle.Strikeout;
						if (rs.Get_Fields_Boolean("Underline"))
							style |= FontStyle.Underline;
						// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
						fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, rs.Get_Fields("FontSize"));
						fldDeptTitle.Font = new Font(rs.Get_Fields_String("FontName"), fldDeptTitle.Font.Size, style);
					}
					fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, fldDeptTitle.Font.Size + 1);
				}
				else
				{
					fldDeptTitle.Font = new Font("Roman 17cpi", fldDeptTitle.Font.Size);
					fldDeptTitle.Font = new Font(fldDeptTitle.Font.Name, 10);
				}
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				//if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
				//{
				//	x.BackStyle = 1;
				//}
				//else
				//{
				//	x.BackStyle = 0;
				//}
				//FC:FINAL:MSH - Issue #686: in VB6 we have BackStyle property, which can change opacity of textbox background.
				// In web we can't change opacity of backround, so we must change color of TextBox background.
				if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
				{
					//x.BackStyle = 1;
					this.Detail.BackColor = Color.FromArgb(192, 192, 192);
					x.BackColor = Color.FromArgb(192, 192, 192);
					IsColorChanged = true;
				}
				else
				{
					//x.BackStyle = 0;
					this.Detail.BackColor = Color.White;
					x.BackColor = Color.White;
					IsColorChanged = false;
				}
			}
			if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
			{
				x.VerticalAlignment = GrapeCity.ActiveReports.Document.Section.VerticalTextAlignment.Top;
			}
			else
			{
				x.VerticalAlignment = 0;
			}
			if (lngRowCounter < frmLedgerSummary.InstancePtr.vs1.Rows - 1)
			{
				if (frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter + 1, 1) == "Assets" || frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter + 1, 1) == "Liabilities" || frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter + 1, 1) == "Fund Balance" || frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter + 1, 1) == "Final Totals")
				{
					x.Height *= FCConvert.ToSingle(2.5);
				}
			}
			if (ColorTranslator.FromOle(frmLedgerSummary.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.Black || ColorTranslator.FromOle(frmLedgerSummary.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter)) == Color.White || frmLedgerSummary.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter) == 0x05CC47)
			{
				x.ForeColor = Color.Black;
			}
			else
			{
				x.ForeColor = ColorTranslator.FromOle(frmLedgerSummary.InstancePtr.vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRowCounter, lngColumnCounter));
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					x.Font = new Font(x.Font.Name, 8);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					x.Font = new Font(x.Font.Name, 10);
				}
				else
				{
					FontStyle style = FontStyle.Regular;
					if (rs.Get_Fields_Boolean("Bold"))
						style = FontStyle.Bold;
					if (rs.Get_Fields_Boolean("Italic"))
						style |= FontStyle.Italic;
					if (rs.Get_Fields_Boolean("StrikeThru"))
						style |= FontStyle.Strikeout;
					if (rs.Get_Fields_Boolean("Underline"))
						style |= FontStyle.Underline;
					// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
					x.Font = new Font(rs.Get_Fields_String("FontName"), FCConvert.ToInt32(rs.Get_Fields("FontSize")) + 2, style);
				}
				if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 1)
				{
					if (lngColumnCounter == 1)
					{
						x.Font = new Font(x.Font.Name, x.Font.Size + 1);
					}
				}
			}
			else
			{
				x.Font = new Font("Roman 17cpi", x.Font.Size);
				x.Font = new Font(x.Font.Name, 10);
			}
			//FC:FINAL:MSH - Issue #686: moved to the end, because previously the FontStyle was redefined in the end
			if (frmLedgerSummary.InstancePtr.vs1.RowOutlineLevel(lngRowCounter) == 2)
			{
				x.Height = 290 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Bold);
			}
			else
			{
				x.Height = 240 / 1440f;
				x.Font = new Font(x.Font, FontStyle.Regular);
			}
			if (frmLedgerSummary.InstancePtr.vs1.ColFormat(lngColumnCounter) == "")
			{
				x.Text = frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter);
			}
			else
			{
				x.Text = Strings.Format(frmLedgerSummary.InstancePtr.vs1.TextMatrix(lngRowCounter, lngColumnCounter), frmLedgerSummary.InstancePtr.vs1.ColFormat(lngColumnCounter));
			}
		}

		private void FormatFixedFields_6(GrapeCity.ActiveReports.SectionReportModel.TextBox x, short TempRow)
		{
			FormatFixedFields(x, ref TempRow);
		}

		private void FormatFixedFields(GrapeCity.ActiveReports.SectionReportModel.TextBox x, ref short TempRow)
		{
			x.Visible = true;
			x.Left = lngTotalWidth;
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				x.Width = (frmLedgerSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f) / 1440f;
			}
			else
			{
				x.Width = (frmLedgerSummary.InstancePtr.vs1.ColWidth(lngColumnCounter) * 0.75f - 100) / 1440f;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Printer")) == "O")
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					x.Font = new Font(x.Font.Name, 10);
				}
				else if (rs.Get_Fields_String("Font") == "L")
				{
					x.Font = new Font(x.Font.Name, 12);
				}
				else
				{
					FontStyle style = FontStyle.Regular;
					if (rs.Get_Fields_Boolean("Bold"))
						style = FontStyle.Bold;
					if (rs.Get_Fields_Boolean("Italic"))
						style |= FontStyle.Italic;
					if (rs.Get_Fields_Boolean("StrikeThru"))
						style |= FontStyle.Strikeout;
					if (rs.Get_Fields_Boolean("Underline"))
						style |= FontStyle.Underline;
					// TODO Get_Fields: Check the table for the column [FontSize] and replace with corresponding Get_Field method
					x.Font = new Font(rs.Get_Fields_String("FontName"), FCConvert.ToInt32(rs.Get_Fields("FontSize")) + 2, style);
				}
			}
			else
			{
				x.Font = new Font("Roman 12cpi", x.Font.Size);
				x.Font = new Font(x.Font.Name, 10);
			}
			x.Text = Strings.Trim(frmLedgerSummary.InstancePtr.vs1.TextMatrix(TempRow, lngColumnCounter));
            if (x.Text == "CURRENT MONTH")
            {
                x.Text = "CURR MNTH";
            }
            else if (x.Text == "YEAR TO DATE")
            {
                x.Text = "YTD";
            }
            else if (x.Text == "ENCUMBRANCE")
            {
                x.Text = "ENCUM";
            }
            else if (x.Text == "ADJUSTMENTS")
            {
                x.Text = "ADJUSTMENT";
            }
            else if (x.Text == "OUTSTANDING")
            {
                x.Text = "OUTSTAND";
            }
		}
		// vbPorter upgrade warning: intFields As short	OnWriteFCConvert.ToInt32(
		private void CenterYTDHeading(int intFields)
		{
			int intYTDFields;
			if (Field1.Text == "YTD")
			{
				if (intFields >= 3)
				{
					if (Field2.Text == "" && Field3.Text == "")
					{
						Field2.Visible = false;
						Field3.Visible = false;
						Field1.Width = Field3.Left - Field1.Left + Field3.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "------------ Y T D ------------";
					}
					else if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 2)
				{
					if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field2.Text == "YTD")
			{
				if (intFields >= 4)
				{
					if (Field3.Text == "" && Field4.Text == "")
					{
						Field3.Visible = false;
						Field4.Visible = false;
						Field2.Width = Field4.Left - Field2.Left + Field4.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "------------ Y T D ------------";
					}
					else if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 3)
				{
					if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field3.Text == "YTD")
			{
				if (intFields >= 5)
				{
					if (Field4.Text == "" && Field5.Text == "")
					{
						Field4.Visible = false;
						Field5.Visible = false;
						Field3.Width = Field5.Left - Field3.Left + Field5.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "------------ Y T D ------------";
					}
					else if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 4)
				{
					if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field4.Text == "YTD")
			{
				if (intFields >= 6)
				{
					if (Field5.Text == "" && Field6.Text == "")
					{
						Field5.Visible = false;
						Field6.Visible = false;
						Field4.Width = Field6.Left - Field4.Left + Field6.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "------------ Y T D ------------";
					}
					else if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 5)
				{
					if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field5.Text == "YTD")
			{
				if (intFields >= 7)
				{
					if (Field6.Text == "" && Field7.Text == "")
					{
						Field6.Visible = false;
						Field7.Visible = false;
						Field5.Width = Field7.Left - Field5.Left + Field7.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "------------ Y T D ------------";
					}
					else if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 6)
				{
					if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field6.Text == "YTD")
			{
				if (intFields >= 8)
				{
					if (Field7.Text == "" && Field8.Text == "")
					{
						Field7.Visible = false;
						Field8.Visible = false;
						Field6.Width = Field8.Left - Field6.Left + Field8.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "------------ Y T D ------------";
					}
					else if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 7)
				{
					if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field7.Text == "YTD")
			{
				if (intFields >= 9)
				{
					if (Field8.Text == "" && Field9.Text == "")
					{
						Field8.Visible = false;
						Field9.Visible = false;
						Field7.Width = Field9.Left - Field7.Left + Field9.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "------------ Y T D ------------";
					}
					else if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 8)
				{
					if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field8.Text == "YTD")
			{
				if (intFields >= 10)
				{
					if (Field9.Text == "" && Field10.Text == "")
					{
						Field9.Visible = false;
						Field10.Visible = false;
						Field8.Width = Field10.Left - Field8.Left + Field10.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "------------ Y T D ------------";
					}
					else if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 9)
				{
					if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field9.Text == "YTD")
			{
				if (intFields >= 11)
				{
					if (Field10.Text == "" && Field11.Text == "")
					{
						Field10.Visible = false;
						Field11.Visible = false;
						Field9.Width = Field11.Left - Field9.Left + Field11.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "------------ Y T D ------------";
					}
					else if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 10)
				{
					if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field10.Text == "YTD")
			{
				if (intFields >= 12)
				{
					if (Field11.Text == "" && Field12.Text == "")
					{
						Field11.Visible = false;
						Field12.Visible = false;
						Field10.Width = Field12.Left - Field10.Left + Field12.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "------------ Y T D ------------";
					}
					else if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 11)
				{
					if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field11.Text == "YTD")
			{
				if (intFields >= 13)
				{
					if (Field12.Text == "" && Field13.Text == "")
					{
						Field12.Visible = false;
						Field13.Visible = false;
						Field11.Width = Field13.Left - Field11.Left + Field13.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "------------ Y T D ------------";
					}
					else if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "---- Y T D ----";
					}
				}
				else if (intFields >= 12)
				{
					if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "---- Y T D ----";
					}
				}
			}
			else if (Field12.Text == "YTD")
			{
				if (intFields >= 13)
				{
					if (Field13.Text == "")
					{
						Field13.Visible = false;
						Field12.Width = Field13.Left - Field12.Left + Field13.Width;
						Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field12.Text = "---- Y T D ----";
					}
				}
			}
		}
		//FC:FINAL:AM:#i653 - because the cells are merged the text in some header cells is empty
		private void CenterCurrentHeading(int intFields)
		{
			int intYTDFields;
			if (Field1.Text == "Curr Mnth")
			{
				if (intFields >= 3)
				{
					//if (Field2.Text == "Curr Mnth" && Field3.Text == "Curr Mnth")
					if (Field2.Text == "" && Field3.Text == "")
					{
						Field2.Visible = false;
						Field3.Visible = false;
						Field1.Width = Field3.Left - Field1.Left + Field3.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "------- C U R R   M O N T H -------";
					}
					//else if (Field2.Text == "Curr Mnth")
					else if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 2)
				{
					//if (Field2.Text == "Curr Mnth")
					if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field2.Text == "Curr Mnth")
			{
				if (intFields >= 4)
				{
					//if (Field3.Text == "Curr Mnth" && Field4.Text == "Curr Mnth")
					if (Field3.Text == "" && Field4.Text == "")
					{
						Field3.Visible = false;
						Field4.Visible = false;
						Field2.Width = Field4.Left - Field2.Left + Field4.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "------- C U R R   M O N T H -------";
					}
						//else if (Field3.Text == "Curr Mnth")
						else if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 3)
				{
					//if (Field3.Text == "Curr Mnth")
					if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field3.Text == "Curr Mnth")
			{
				if (intFields >= 5)
				{
					//if (Field4.Text == "Curr Mnth" && Field5.Text == "Curr Mnth")
					if (Field4.Text == "" && Field5.Text == "")
					{
						Field4.Visible = false;
						Field5.Visible = false;
						Field3.Width = Field5.Left - Field3.Left + Field5.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "------- C U R R   M O N T H -------";
					}
							//else if (Field4.Text == "Curr Mnth")
							else if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 4)
				{
					//if (Field4.Text == "Curr Mnth")
					if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field4.Text == "Curr Mnth")
			{
				if (intFields >= 6)
				{
					//if (Field5.Text == "Curr Mnth" && Field6.Text == "Curr Mnth")
					if (Field5.Text == "" && Field6.Text == "")
					{
						Field5.Visible = false;
						Field6.Visible = false;
						Field4.Width = Field6.Left - Field4.Left + Field6.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "------- C U R R   M O N T H -------";
					}
								//else if (Field5.Text == "Curr Mnth")
								else if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 5)
				{
					//if (Field5.Text == "Curr Mnth")
					if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field5.Text == "Curr Mnth")
			{
				if (intFields >= 7)
				{
					//if (Field6.Text == "Curr Mnth" && Field7.Text == "Curr Mnth")
					if (Field6.Text == "" && Field7.Text == "")
					{
						Field6.Visible = false;
						Field7.Visible = false;
						Field5.Width = Field7.Left - Field5.Left + Field7.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "------- C U R R   M O N T H -------";
					}
									//else if (Field6.Text == "Curr Mnth")
									else if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 6)
				{
					//if (Field6.Text == "Curr Mnth")
					if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field6.Text == "Curr Mnth")
			{
				if (intFields >= 8)
				{
					//if (Field7.Text == "Curr Mnth" && Field8.Text == "Curr Mnth")
					if (Field7.Text == "" && Field8.Text == "")
					{
						Field7.Visible = false;
						Field8.Visible = false;
						Field6.Width = Field8.Left - Field6.Left + Field8.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "------- C U R R   M O N T H -------";
					}
										//else if (Field7.Text == "Curr Mnth")
										else if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 7)
				{
					//if (Field7.Text == "Curr Mnth")
					if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field7.Text == "Curr Mnth")
			{
				if (intFields >= 9)
				{
					//if (Field8.Text == "Curr Mnth" && Field9.Text == "Curr Mnth")
					if (Field8.Text == "" && Field9.Text == "")
					{
						Field8.Visible = false;
						Field9.Visible = false;
						Field7.Width = Field9.Left - Field7.Left + Field9.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "------- C U R R   M O N T H -------";
					}
											//else if (Field8.Text == "Curr Mnth")
											else if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 8)
				{
					//if (Field8.Text == "Curr Mnth")
					if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field8.Text == "Curr Mnth")
			{
				if (intFields >= 10)
				{
					//if (Field9.Text == "Curr Mnth" && Field10.Text == "Curr Mnth")
					if (Field9.Text == "" && Field10.Text == "")
					{
						Field9.Visible = false;
						Field10.Visible = false;
						Field8.Width = Field10.Left - Field8.Left + Field10.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "------- C U R R   M O N T H -------";
					}
												//else if (Field9.Text == "Curr Mnth")
												else if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 9)
				{
					//if (Field9.Text == "Curr Mnth")
					if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field9.Text == "Curr Mnth")
			{
				if (intFields >= 11)
				{
					//if (Field10.Text == "Curr Mnth" && Field11.Text == "Curr Mnth")
					if (Field10.Text == "" && Field11.Text == "")
					{
						Field10.Visible = false;
						Field11.Visible = false;
						Field9.Width = Field11.Left - Field9.Left + Field11.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "------- C U R R   M O N T H -------";
					}
													//else if (Field10.Text == "Curr Mnth")
													else if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 10)
				{
					//if (Field10.Text == "Curr Mnth")
					if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field10.Text == "Curr Mnth")
			{
				if (intFields >= 12)
				{
					//if (Field11.Text == "Curr Mnth" && Field12.Text == "Curr Mnth")
					if (Field11.Text == "" && Field12.Text == "")
					{
						Field11.Visible = false;
						Field12.Visible = false;
						Field10.Width = Field12.Left - Field10.Left + Field12.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "------- C U R R   M O N T H -------";
					}
														//else if (Field11.Text == "Curr Mnth")
														else if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 11)
				{
					//if (Field11.Text == "Curr Mnth")
					if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field11.Text == "Curr Mnth")
			{
				if (intFields >= 13)
				{
					//if (Field12.Text == "Curr Mnth" && Field13.Text == "Curr Mnth")
					if (Field12.Text == "" && Field13.Text == "")
					{
						Field12.Visible = false;
						Field13.Visible = false;
						Field11.Width = Field13.Left - Field11.Left + Field13.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "------- C U R R   M O N T H -------";
					}
															//else if (Field12.Text == "Curr Mnth")
															else if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "- C U R R   M O N T H -";
					}
				}
				else if (intFields >= 12)
				{
					//if (Field12.Text == "Curr Mnth")
					if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "- C U R R   M O N T H -";
					}
				}
			}
			else if (Field12.Text == "Curr Mnth")
			{
				if (intFields >= 13)
				{
					//if (Field13.Text == "Curr Mnth")
					if (Field13.Text == "")
					{
						Field13.Visible = false;
						Field12.Width = Field13.Left - Field12.Left + Field13.Width;
						Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field12.Text = "- C U R R   M O N T H -";
					}
				}
			}
		}
		// vbPorter upgrade warning: intFields As short	OnWriteFCConvert.ToInt32(
		private void CenterBalanceHeading(int intFields)
		{
			int intYTDFields;
			if (Field1.Text == "Balance")
			{
				if (intFields >= 3)
				{
					if (Field2.Text == "" && Field3.Text == "")
					{
						Field2.Visible = false;
						Field3.Visible = false;
						Field1.Width = Field3.Left - Field1.Left + Field3.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "--------- B A L A N C E ---------";
					}
					else if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 2)
				{
					if (Field2.Text == "")
					{
						Field2.Visible = false;
						Field1.Width = Field2.Left - Field1.Left + Field2.Width;
						Field1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field1.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field2.Text == "Balance")
			{
				if (intFields >= 4)
				{
					if (Field3.Text == "" && Field4.Text == "")
					{
						Field3.Visible = false;
						Field4.Visible = false;
						Field2.Width = Field4.Left - Field2.Left + Field4.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "--------- B A L A N C E ---------";
					}
					else if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 3)
				{
					if (Field3.Text == "")
					{
						Field3.Visible = false;
						Field2.Width = Field3.Left - Field2.Left + Field3.Width;
						Field2.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field2.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field3.Text == "Balance")
			{
				if (intFields >= 5)
				{
					if (Field4.Text == "" && Field5.Text == "")
					{
						Field4.Visible = false;
						Field5.Visible = false;
						Field3.Width = Field5.Left - Field3.Left + Field5.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "--------- B A L A N C E ---------";
					}
					else if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 4)
				{
					if (Field4.Text == "")
					{
						Field4.Visible = false;
						Field3.Width = Field4.Left - Field3.Left + Field4.Width;
						Field3.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field3.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field4.Text == "Balance")
			{
				if (intFields >= 6)
				{
					if (Field5.Text == "" && Field6.Text == "")
					{
						Field5.Visible = false;
						Field6.Visible = false;
						Field4.Width = Field6.Left - Field4.Left + Field6.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "--------- B A L A N C E ---------";
					}
					else if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 5)
				{
					if (Field5.Text == "")
					{
						Field5.Visible = false;
						Field4.Width = Field5.Left - Field4.Left + Field5.Width;
						Field4.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field4.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field5.Text == "Balance")
			{
				if (intFields >= 7)
				{
					if (Field6.Text == "" && Field7.Text == "")
					{
						Field6.Visible = false;
						Field7.Visible = false;
						Field5.Width = Field7.Left - Field5.Left + Field7.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "--------- B A L A N C E ---------";
					}
					else if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 6)
				{
					if (Field6.Text == "")
					{
						Field6.Visible = false;
						Field5.Width = Field6.Left - Field5.Left + Field6.Width;
						Field5.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field5.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field6.Text == "Balance")
			{
				if (intFields >= 8)
				{
					if (Field7.Text == "" && Field8.Text == "")
					{
						Field7.Visible = false;
						Field8.Visible = false;
						Field6.Width = Field8.Left - Field6.Left + Field8.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "--------- B A L A N C E ---------";
					}
					else if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 7)
				{
					if (Field7.Text == "")
					{
						Field7.Visible = false;
						Field6.Width = Field7.Left - Field6.Left + Field7.Width;
						Field6.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field6.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field7.Text == "Balance")
			{
				if (intFields >= 9)
				{
					if (Field8.Text == "" && Field9.Text == "")
					{
						Field8.Visible = false;
						Field9.Visible = false;
						Field7.Width = Field9.Left - Field7.Left + Field9.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "--------- B A L A N C E ---------";
					}
					else if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 8)
				{
					if (Field8.Text == "")
					{
						Field8.Visible = false;
						Field7.Width = Field8.Left - Field7.Left + Field8.Width;
						Field7.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field7.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field8.Text == "Balance")
			{
				if (intFields >= 10)
				{
					if (Field9.Text == "" && Field10.Text == "")
					{
						Field9.Visible = false;
						Field10.Visible = false;
						Field8.Width = Field10.Left - Field8.Left + Field10.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "--------- B A L A N C E ---------";
					}
					else if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 9)
				{
					if (Field9.Text == "")
					{
						Field9.Visible = false;
						Field8.Width = Field9.Left - Field8.Left + Field9.Width;
						Field8.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field8.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field9.Text == "Balance")
			{
				if (intFields >= 11)
				{
					if (Field10.Text == "" && Field11.Text == "")
					{
						Field10.Visible = false;
						Field11.Visible = false;
						Field9.Width = Field11.Left - Field9.Left + Field11.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "--------- B A L A N C E ---------";
					}
					else if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 10)
				{
					if (Field10.Text == "")
					{
						Field10.Visible = false;
						Field9.Width = Field10.Left - Field9.Left + Field10.Width;
						Field9.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field9.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field10.Text == "Balance")
			{
				if (intFields >= 12)
				{
					if (Field11.Text == "" && Field12.Text == "")
					{
						Field11.Visible = false;
						Field12.Visible = false;
						Field10.Width = Field12.Left - Field10.Left + Field12.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "--------- B A L A N C E ---------";
					}
					else if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 11)
				{
					if (Field11.Text == "")
					{
						Field11.Visible = false;
						Field10.Width = Field11.Left - Field10.Left + Field11.Width;
						Field10.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field10.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field11.Text == "Balance")
			{
				if (intFields >= 13)
				{
					if (Field12.Text == "" && Field13.Text == "")
					{
						Field12.Visible = false;
						Field13.Visible = false;
						Field11.Width = Field13.Left - Field11.Left + Field13.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "--------- B A L A N C E ---------";
					}
					else if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "--- B A L A N C E ---";
					}
				}
				else if (intFields >= 12)
				{
					if (Field12.Text == "")
					{
						Field12.Visible = false;
						Field11.Width = Field12.Left - Field11.Left + Field12.Width;
						Field11.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field11.Text = "--- B A L A N C E ---";
					}
				}
			}
			else if (Field12.Text == "Balance")
			{
				if (intFields >= 13)
				{
					if (Field13.Text == "")
					{
						Field13.Visible = false;
						Field12.Width = Field13.Left - Field12.Left + Field13.Width;
						Field12.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
						Field12.Text = "--- B A L A N C E ---";
					}
				}
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (this.PageNumber == 1)
			{
				fldDeptTitle.Text = "";
				fldDeptTitle.Visible = false;
			}
			else
			{
				fldDeptTitle.Text = strTitleToShow + " CONT'D";
				fldDeptTitle.Visible = true;
			}
		}

		private void rptLedgerSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLedgerSummary.Caption	= "Ledger Summary";
			//rptLedgerSummary.Icon	= "rptLedgerSummary.dsx":0000";
			//rptLedgerSummary.Left	= 0;
			//rptLedgerSummary.Top	= 0;
			//rptLedgerSummary.Width	= 11880;
			//rptLedgerSummary.Height	= 8595;
			//rptLedgerSummary.StartUpPosition	= 3;
			//rptLedgerSummary.SectionData	= "rptLedgerSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
