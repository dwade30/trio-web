﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmViewJournals.
	/// </summary>
	public partial class frmViewJournals : BaseForm
	{
		private bool expandCollapseProgramatically = false;
		private bool expandColapseOnClick = true;

		public frmViewJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:SBE - initialize constants before triggering events
			DetailCol = 0;
			AccountCol = 1;
			DetailAmountCol = 2;
			DiscountCol = 3;
			EncumbranceCol = 4;
			WarrantCol = 3;
			JournalCol = 2;
			VendorCol = 1;
			NameCol = 4;
			DescriptionCol = 5;
			CheckCol = 6;
			ReferenceCol = 7;
			AmountCol = 8;
			StatusCol = 9;
			//ColPercents[0] = 0.01912826f;
			ColPercents[0] = 0.05f;
			//ColPercents[WarrantCol] = 0.06f;
			ColPercents[WarrantCol] = 0.059f;
			//ColPercents[JournalCol] = 0.05940844f;
			ColPercents[JournalCol] = 0.059f;
			ColPercents[VendorCol] = 0.09550844f;
			ColPercents[NameCol] = 0.2344142f;
			ColPercents[DescriptionCol] = 0.1510896f;
			ColPercents[CheckCol] = 0.08f;
			ColPercents[ReferenceCol] = 0.1328331f;
			ColPercents[AmountCol] = 0.1264009f;
			//ColPercents[StatusCol] = 0.059f;
			ColPercents[StatusCol] = 0.045f;
			// 0.05940844
			//vs2ColPercents[0] = 0.29f;
			//vs2ColPercents[1] = 0.27f;
			vs2ColPercents[0] = 0.25f;
			vs2ColPercents[1] = 0.24f;
			vs2ColPercents[2] = 0.15f;
			vs2ColPercents[3] = 0.14f;
			//vs2ColPercents[4] = 0.15f;
			vs2ColPercents[4] = 0.22f;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmViewJournals InstancePtr
		{
			get
			{
				return (frmViewJournals)Sys.GetInstance(typeof(frmViewJournals));
			}
		}

		protected frmViewJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int JournalCol;
		int VendorCol;
		int NameCol;
		int DescriptionCol;
		int AmountCol;
		int WarrantCol;
		int CheckCol;
		int StatusCol;
		int ReferenceCol;
		float[] ColPercents = new float[9 + 1];
		float[] vs2ColPercents = new float[4 + 1];
		int AccountCol;
		int DetailCol;
		int DetailAmountCol;
		int DiscountCol;
		int EncumbranceCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool blnSecondResize;
		bool blnDontRunCollapsedEvent;

		private void frmViewJournals_Activated(object sender, System.EventArgs e)
		{
			int CurrJournal;
			int counter;
			clsDRWrapper rsCashControl = new clsDRWrapper();
			int RecordCounter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Loading Journals", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			//FC:FINAL:AM:#i794 - add the expand button and hide the row header; the data in it shouldn't be visible
			vs1.AddExpandButton();
			vs1.RowHeadersVisible = false;
			RecordCounter = 0;
			vs1.TextMatrix(0, WarrantCol, "Wrnt");
			vs1.TextMatrix(0, JournalCol, "Jrnl");
			vs1.TextMatrix(0, VendorCol, "Vnd #");
			vs1.TextMatrix(0, NameCol, "Vendor Name");
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, CheckCol, "Check");
			vs1.TextMatrix(0, ReferenceCol, "Reference");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, StatusCol, "Status");
			vs2.TextMatrix(0, DetailCol, "Description");
			vs2.TextMatrix(0, AccountCol, "Account");
			vs2.TextMatrix(0, DetailAmountCol, "Amount");
			vs2.TextMatrix(0, DiscountCol, "Discount");
			vs2.TextMatrix(0, EncumbranceCol, "Enc");
			vs1.ColFormat(AmountCol, "#,##0.00");
			vs2.ColFormat(DetailAmountCol, "#,##0.00");
			vs2.ColFormat(DiscountCol, "#,##0.00");
			vs2.ColFormat(EncumbranceCol, "#,##0.00");
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ReferenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs2.ColAlignment(DetailCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			CurrJournal = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
			vs1.Rows += 1;
			rsCashControl.OpenRecordset("SELECT SUM(APJournalDetail.Amount) as Amount FROM APJournal INNER JOIN APJournalDetail ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.Description = 'Control Entries' AND APJournal.JournalNumber = " + FCConvert.ToString(CurrJournal) + " AND (APJournalDetail.Description = 'Cash A/P' OR APjournalDetail.Description = 'OFFSET-CASH A/P' or APjournalDetail.Description = 'Void A/P')");
			if (rsCashControl.EndOfFile() != true && rsCashControl.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				vs1.TextMatrix(vs1.Rows - 1, AmountCol, Strings.Format(Conversion.Val(rsCashControl.Get_Fields("Amount")) * -1, "#,##0.00"));
			}
			else
			{
				vs1.TextMatrix(vs1.Rows - 1, AmountCol, Strings.Format(0, "#,##0.00"));
			}
			vs1.TextMatrix(vs1.Rows - 1, NameCol, "Date: " + Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("CheckDate"), "MM/dd/yy"));
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			vs1.TextMatrix(vs1.Rows - 1, JournalCol, modValidateAccount.GetFormat_6(Conversion.Str(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")), 4));
			// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
			vs1.TextMatrix(vs1.Rows - 1, WarrantCol, modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Warrant"), 4));
			vs1.TextMatrix(vs1.Rows - 1, StatusCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status")));
			//FC:FINAL:BBE:#391 - remove colors
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, StatusCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, StatusCol, 0x80000018);
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
			while (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
			{
				//Application.DoEvents();
				RecordCounter += 1;
				frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(RecordCounter) / modBudgetaryAccounting.Statics.SearchResults.RecordCount()) * 100);
				frmWait.InstancePtr.Refresh();
				vs1.Rows += 1;
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				if (CurrJournal != FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")))
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					CurrJournal = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
					rsCashControl.OpenRecordset("SELECT SUM(APJournalDetail.Amount) as Amount FROM APJournal INNER JOIN APJournalDetail ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.Description = 'Control Entries' AND APJournal.JournalNumber = " + FCConvert.ToString(CurrJournal) + " AND (APJournalDetail.Description = 'Cash A/P' OR APjournalDetail.Description = 'OFFSET-CASH A/P' or APjournalDetail.Description = 'Void A/P')");
					if (rsCashControl.EndOfFile() != true && rsCashControl.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs1.TextMatrix(vs1.Rows - 1, AmountCol, Strings.Format(Conversion.Val(rsCashControl.Get_Fields("Amount")) * -1, "#,##0.00"));
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, AmountCol, Strings.Format(0, "#,##0.00"));
					}
					vs1.TextMatrix(vs1.Rows - 1, NameCol, "Date: " + Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("CheckDate"), "MM/dd/yy"));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(vs1.Rows - 1, JournalCol, modValidateAccount.GetFormat_6(Conversion.Str(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")), 4));
					// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
					vs1.TextMatrix(vs1.Rows - 1, WarrantCol, modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Warrant"), 4));
					vs1.TextMatrix(vs1.Rows - 1, StatusCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status")));
					//FC:FINAL:BBE:#391 - remove colors
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, StatusCol, 0x80000017);
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, StatusCol, 0x80000018);
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.IsSubtotal(vs1.Rows - 1, true);
					vs1.Rows += 1;
				}
				vs1.TextMatrix(vs1.Rows - 1, VendorCol, modValidateAccount.GetFormat_6(Conversion.Str(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")), 5));
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")) == 0)
				{
					vs1.TextMatrix(vs1.Rows - 1, NameCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorName")));
				}
				else
				{
					rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						vs1.TextMatrix(vs1.Rows - 1, NameCol, FCConvert.ToString(rs.Get_Fields_String("CheckName")));
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, NameCol, "UNKNOWN");
					}
				}
				vs1.TextMatrix(vs1.Rows - 1, DescriptionCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description")));
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				vs1.TextMatrix(vs1.Rows - 1, CheckCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("CheckNumber")));
				vs1.TextMatrix(vs1.Rows - 1, ReferenceCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Reference")));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount")));
				vs1.TextMatrix(vs1.Rows - 1, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")));
				vs1.RowOutlineLevel(vs1.Rows - 1, 2);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, 0x8000000E);
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
			}
			//FC:FINAL:SBE - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.Rows - 1, vs1.Cols - 1, 8);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, WarrantCol, 0, StatusCol, 4);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, StatusCol, vs1.Rows - 1, StatusCol, 4);
			//FC:FINAL:SBE - font size used in original for samll row height
			//vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.Rows - 1, EncumbranceCol, 8);
			vs2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DetailCol, 0, EncumbranceCol, 4);
			blnDontRunCollapsedEvent = true;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (vs1.RowOutlineLevel(counter) == 2)
				{
					expandCollapseProgramatically = true;
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					expandCollapseProgramatically = false;
				}
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					expandCollapseProgramatically = true;
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					expandCollapseProgramatically = false;
				}
			}
			blnDontRunCollapsedEvent = false;
			//FC:FINAL:SBE - fixed row height in web version
			//for (counter = 1; counter <= vs1.Rows - 1; counter++)
			//{
			//    //Application.DoEvents();
			//    vs1.RowHeight(counter, vs1.RowHeight(counter) - 50);
			//}
			vs1_Collapsed();
            modColorScheme.ColorGrid(vs1);
			Form_Resize();
			frmWait.InstancePtr.Unload();
			this.Refresh();
		}

		private void frmViewJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmViewJournals.FillStyle	= 0;
			//frmViewJournals.ScaleWidth	= 9495;
			//frmViewJournals.ScaleHeight	= 7140;
			//frmViewJournals.LinkTopic	= "Form2";
			//frmViewJournals.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmViewJournals_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= 9; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * ColPercents[counter]));
			}
			for (counter = 0; counter <= 3; counter++)
			{
				vs2.ColWidth(counter, FCConvert.ToInt32(vs2.WidthOriginal * vs2ColPercents[counter]));
			}
			//vs2.ColWidth(4, 100);
			vs2.ColWidth(4, 1000);
		}

		public void Form_Resize()
		{
			frmViewJournals_Resize(this, new System.EventArgs());
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//FC:FINAL:BBE:#397 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
			//frmQueryJournal.InstancePtr.Show(App.MainForm);
		}

		//FC:FINAL:BBE:#397 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			base.OnFormClosed(e);
			frmQueryJournal.InstancePtr.Show(App.MainForm);
		}

		private void frmViewJournals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), false);
			if (!expandCollapseProgramatically)
				expandColapseOnClick = false;
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_AfterCollapse(this.vs1.GetFlexRowIndex(e.RowIndex), true);
			if (!expandCollapseProgramatically)
				expandColapseOnClick = false;
		}

		private void vs1_AfterCollapse(int row, bool isCollapsed)
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool flag = false;
			if (!blnDontRunCollapsedEvent)
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							flag = true;
						}
						else
						{
							rows += 1;
							flag = false;
						}
					}
					else
					{
						if (flag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				//FC:FINA::SBE - fixed size on web version
				//if (vs1.RightCol < vs1.Cols - 1)
				//{
				//    height = (rows + 2) * vs1.RowHeight(0);
				//}
				//else
				//{
				//    height = (rows + 1) * vs1.RowHeight(0);
				//}
				//if (height < frmViewJournals.InstancePtr.Height * 0.4)
				//{
				//    if (vs1.Height != height)
				//    {
				//        vs1.Height = height + 75;
				//    }
				//}
				//else
				//{
				//    if (vs1.Height < frmViewJournals.InstancePtr.Height * 0.4 + 75)
				//    {
				//        vs1.Height = FCConvert.ToInt32(frmViewJournals.InstancePtr.Height * 0.4 + 75);
				//    }
				//}
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.MouseRow > 0)
			{
				if (vs1.RowOutlineLevel(vs1.MouseRow) == 1)
				{
					if (expandColapseOnClick)
					{
						//FC:FINAL:SBE - add flag to avoid expand/collapse during click event
						expandCollapseProgramatically = true;
						if (vs1.IsCollapsed(vs1.MouseRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							vs1.IsCollapsed(vs1.MouseRow, FCGrid.CollapsedSettings.flexOutlineExpanded);
						}
						else
						{
							vs1.IsCollapsed(vs1.MouseRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						}
						expandCollapseProgramatically = false;
					}
				}
				else
				{
					// do nothing
				}
			}
			expandColapseOnClick = true;
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool flag = false;
			if (!blnDontRunCollapsedEvent)
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							flag = true;
						}
						else
						{
							rows += 1;
							flag = false;
						}
					}
					else
					{
						if (flag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				//FC:FINAL:SBE - fixed size on web version
				//if (vs1.RightCol < vs1.Cols - 1)
				//{
				//    height = (rows + 2) * vs1.RowHeight(0);
				//}
				//else
				//{
				//    height = (rows + 1) * vs1.RowHeight(0);
				//}
				//if (height < frmViewJournals.InstancePtr.Height * 0.4)
				//{
				//    if (vs1.Height != height)
				//    {
				//        vs1.Height = height + 75;
				//    }
				//}
				//else
				//{
				//    if (vs1.Height < frmViewJournals.InstancePtr.Height * 0.4 + 75)
				//    {
				//        vs1.Height = FCConvert.ToInt32(frmViewJournals.InstancePtr.Height * 0.4 + 75);
				//    }
				//}
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int counter = 0;
			if (KeyCode == Keys.Down)
			{
				if (vs1.Row < vs1.Rows - 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row + 1) == 1)
					{
						counter = vs1.Row + 1;
						while (vs1.RowOutlineLevel(counter) != 2)
						{
							counter += 1;
							if (counter > vs1.Rows - 1)
							{
								break;
							}
						}
						if (counter > vs1.Rows)
						{
							KeyCode = 0;
						}
						else
						{
							vs1.Row = counter - 1;
						}
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row > 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row - 1) == 1)
					{
						counter = vs1.Row - 1;
						while (vs1.RowOutlineLevel(counter) != 2)
						{
							counter -= 1;
							if (counter < 1)
							{
								break;
							}
						}
						if (counter < 1)
						{
							KeyCode = 0;
						}
						else
						{
							vs1.Row = counter + 1;
						}
					}
				}
			}
			else if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
			}
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
            if (vs1.GetFlexColIndex(e.ColumnIndex) == StatusCol)
			{
				//ToolTip1.SetToolTip(vs1, "E - Entered,  V - Warrant Preview Printed,  C - Checks Printed,  R - Check Register Printed,  W - Warrant Printed,  X - Warrant Recap Printed,  P - Posted");
				cell.ToolTipText = "E - Entered,  V - Warrant Preview Printed,  C - Checks Printed,  R - Check Register Printed,  W - Warrant Printed,  X - Warrant Recap Printed,  P - Posted";
			}
			else
			{
                //ToolTip1.SetToolTip(vs1, "");
                cell.ToolTipText = "";
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs2.Rows = 1;
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
				vs2.Rows = 1;
				rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (rs.EndOfFile() != true)
					{
						vs2.Rows += 1;
						vs2.TextMatrix(vs2.Rows - 1, DetailCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
						// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, AccountCol, FCConvert.ToString(rs.Get_Fields("account")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, DetailAmountCol, FCConvert.ToString(rs.Get_Fields("Amount")));
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						vs2.TextMatrix(vs2.Rows - 1, DiscountCol, FCConvert.ToString(rs.Get_Fields("Discount")));
						vs2.TextMatrix(vs2.Rows - 1, EncumbranceCol, FCConvert.ToString(rs.Get_Fields_Decimal("Encumbrance")));
						rs.MoveNext();
					}
				}
				//FC:FINAL:SBE - font size used in original for samll row height
				//vs2.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, DetailCol, vs2.Rows - 1, EncumbranceCol, 8);
				vs2_Collapsed();
			}
		}

		private void vs2_Collapsed()
		{
			//if (vs2.Rows >= 8)
			if (vs2.Rows >= 5)
			{
				vs2.Height = 5 * 40 + 32;
			}
			else
			{
				//if (vs2.RightCol < vs2.Cols - 1)
				//{
				//    vs2.Height = (vs2.Rows + 1) * vs2.RowHeight(0) + 75;
				//}
				//else
				//{
				//    vs2.Height = vs2.Rows * vs2.RowHeight(0) + 75;
				//}
				vs2.Height = (vs2.Rows - 1) * 40 + 32;
			}
		}

		private void vs2_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs2_AfterCollapse(this.vs2.GetFlexRowIndex(e.RowIndex), false);
		}

		private void vs2_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs2_AfterCollapse(this.vs2.GetFlexRowIndex(e.RowIndex), true);
		}

		private void vs2_AfterCollapse(int row, bool isCollapsed)
		{
			//FC:FINAL:SBE - fixed size on web version
			//if (vs2.Rows >= 8)
			//{
			//    vs2.Height = 8 * vs2.RowHeight(0) + 75;
			//}
			//else
			//{
			//    if (vs2.RightCol < vs2.Cols - 1)
			//    {
			//        vs2.Height = (vs2.Rows + 1) * vs2.RowHeight(0) + 75;
			//    }
			//    else
			//    {
			//        vs2.Height = vs2.Rows * vs2.RowHeight(0) + 75;
			//    }
			//}
		}
	}
}
