﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System.Linq;
using System.Runtime.InteropServices;

namespace TWBD0000
{
	public class modElectronic1099
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct TRecord
		{
            // vbPorter upgrade warning: RecType As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecTypeCharArray;
            public string RecType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecTypeCharArray);
                }
                set
                {
                    RecTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
			// vbPorter upgrade warning: PaymentYear As FixedString	OnWriteFCConvert.ToInt32(
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] PaymentYearCharArray;
            public string PaymentYear
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentYearCharArray);
                }
                set
                {
                    PaymentYearCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: PriorYearData As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] PriorYearDataCharArray;
            public string PriorYearData
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PriorYearDataCharArray);
                }
                set
                {
                    PriorYearDataCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: TransmittersTIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] TransmittersTINCharArray;
            public string TransmittersTIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransmittersTINCharArray);
                }
                set
                {
                    TransmittersTINCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: TransmittersControlCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] TransmittersControlCodeCharArray;
            public string TransmittersControlCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransmittersControlCodeCharArray);
                }
                set
                {
                    TransmittersControlCodeCharArray = FCUtils.FixedStringToArray(value, 5);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 7);
                }

            }
            // vbPorter upgrade warning: TestFileIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TestFileIndicatorCharArray;
            public string TestFileIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TestFileIndicatorCharArray);
                }
                set
                {
                    TestFileIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: ForeignEntityIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] ForeignEntityIndicatorCharArray;
            public string ForeignEntityIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ForeignEntityIndicatorCharArray);
                }
                set
                {
                    ForeignEntityIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: TransmitterName As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] TransmitterNameCharArray;
            public string TransmitterName
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransmitterNameCharArray);
                }
                set
                {
                    TransmitterNameCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: TransmitterNameContinued As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] TransmitterNameContinuedCharArray;
            public string TransmitterNameContinued
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransmitterNameContinuedCharArray);
                }
                set
                {
                    TransmitterNameContinuedCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: CompanyName As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] CompanyNameCharArray;
            public string CompanyName
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyNameCharArray);
                }
                set
                {
                    CompanyNameCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: CompanyNameContinued As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] CompanyNameContinuedCharArray;
            public string CompanyNameContinued
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyNameContinuedCharArray);
                }
                set
                {
                    CompanyNameContinuedCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: CompanyMailingAddress As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] CompanyMailingAddressCharArray;
            public string CompanyMailingAddress
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyMailingAddressCharArray);
                }
                set
                {
                    CompanyMailingAddressCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: CompanyCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] CompanyCityCharArray;
            public string CompanyCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyCityCharArray);
                }
                set
                {
                    CompanyCityCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: CompanyState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CompanyStateCharArray;
            public string CompanyState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyStateCharArray);
                }
                set
                {
                    CompanyStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: CompanyZip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] CompanyZipCharArray;
            public string CompanyZip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CompanyZipCharArray);
                }
                set
                {
                    CompanyZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 15);
                }

            }
            // vbPorter upgrade warning: TotalNumberOfPayees As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] TotalNumberOfPayeesCharArray;
            public string TotalNumberOfPayees
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalNumberOfPayeesCharArray);
                }
                set
                {
                    TotalNumberOfPayeesCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: ContactName As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] ContactNameCharArray;
            public string ContactName
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ContactNameCharArray);
                }
                set
                {
                    ContactNameCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: ContactPhoneAndExtension As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
            public char[] ContactPhoneAndExtensionCharArray;
            public string ContactPhoneAndExtension
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ContactPhoneAndExtensionCharArray);
                }
                set
                {
                    ContactPhoneAndExtensionCharArray = FCUtils.FixedStringToArray(value, 15);
                }

            }
            // vbPorter upgrade warning: ContactEmail As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 50)]
            public char[] ContactEmailCharArray;
            public string ContactEmail
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ContactEmailCharArray);
                }
                set
                {
                    ContactEmailCharArray = FCUtils.FixedStringToArray(value, 50);
                }

            }
            // vbPorter upgrade warning: Blank3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 91)]
            public char[] Blank3CharArray;
            public string Blank3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank3CharArray);
                }
                set
                {
                    Blank3CharArray = FCUtils.FixedStringToArray(value, 91);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] Blank4CharArray;
            public string Blank4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank4CharArray);
                }
                set
                {
                    Blank4CharArray = FCUtils.FixedStringToArray(value, 10);
                }

            }
            // vbPorter upgrade warning: VendorIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] VendorIndicatorCharArray;
            public string VendorIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorIndicatorCharArray);
                }
                set
                {
                    VendorIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: VendorName As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] VendorNameCharArray;
            public string VendorName
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorNameCharArray);
                }
                set
                {
                    VendorNameCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: VendorMailingAddress As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] VendorMailingAddressCharArray;
            public string VendorMailingAddress
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorMailingAddressCharArray);
                }
                set
                {
                    VendorMailingAddressCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: VendorCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] VendorCityCharArray;
            public string VendorCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorCityCharArray);
                }
                set
                {
                    VendorCityCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: VendorState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] VendorStateCharArray;
            public string VendorState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorStateCharArray);
                }
                set
                {
                    VendorStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: VendorZip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] VendorZipCharArray;
            public string VendorZip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorZipCharArray);
                }
                set
                {
                    VendorZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: VendorContact As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] VendorContactCharArray;
            public string VendorContact
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorContactCharArray);
                }
                set
                {
                    VendorContactCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: VendorContactPhoneAndExtension As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
            public char[] VendorContactPhoneAndExtensionCharArray;
            public string VendorContactPhoneAndExtension
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorContactPhoneAndExtensionCharArray);
                }
                set
                {
                    VendorContactPhoneAndExtensionCharArray = FCUtils.FixedStringToArray(value, 15);
                }

            }
            // vbPorter upgrade warning: Blank6 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 35)]
            public char[] Blank6CharArray;
            public string Blank6
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank6CharArray);
                }
                set
                {
                    Blank6CharArray = FCUtils.FixedStringToArray(value, 35);
                }

            }
            // vbPorter upgrade warning: VendorForeignEntityIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] VendorForeignEntityIndicatorCharArray;
            public string VendorForeignEntityIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(VendorForeignEntityIndicatorCharArray);
                }
                set
                {
                    VendorForeignEntityIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: Blank5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] Blank5CharArray;
            public string Blank5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank5CharArray);
                }
                set
                {
                    Blank5CharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public TRecord(int unusedParam)
			{
				this.RecTypeCharArray = new string(' ', 1).ToArray();
				this.PaymentYearCharArray = new string(' ', 4).ToArray();
				this.PriorYearDataCharArray = new string(' ', 1).ToArray();
				this.TransmittersTINCharArray = new string(' ', 9).ToArray();
				this.TransmittersControlCodeCharArray = new string(' ', 5).ToArray();
				this.Blank1CharArray = new string(' ', 1).ToArray();
				this.TestFileIndicatorCharArray = new string(' ', 1).ToArray();
				this.ForeignEntityIndicatorCharArray = new string(' ', 1).ToArray();
				this.TransmitterNameCharArray = new string(' ', 40).ToArray();
				this.TransmitterNameContinuedCharArray = new string(' ', 40).ToArray();
				this.CompanyNameCharArray = new string(' ', 40).ToArray();
				this.CompanyNameContinuedCharArray = new string(' ', 40).ToArray();
				this.CompanyMailingAddressCharArray = new string(' ', 40).ToArray();
				this.CompanyCityCharArray = new string(' ', 40).ToArray();
				this.CompanyStateCharArray = new string(' ', 2).ToArray();
				this.CompanyZipCharArray = new string(' ', 9).ToArray();
				this.Blank2CharArray = new string(' ', 15).ToArray();
				this.TotalNumberOfPayeesCharArray = new string(' ', 8).ToArray();
				this.ContactNameCharArray = new string(' ', 40).ToArray();
				this.ContactPhoneAndExtensionCharArray = new string(' ', 15).ToArray();
				this.ContactEmailCharArray = new string(' ', 50).ToArray();
				this.Blank3CharArray = new string(' ', 91).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank4CharArray = new string(' ', 10).ToArray();
				this.VendorIndicatorCharArray = new string(' ', 1).ToArray();
				this.VendorNameCharArray = new string(' ', 40).ToArray();
				this.VendorMailingAddressCharArray = new string(' ', 40).ToArray();
				this.VendorCityCharArray = new string(' ', 40).ToArray();
				this.VendorStateCharArray = new string(' ', 2).ToArray();
				this.VendorZipCharArray = new string(' ', 9).ToArray();
				this.VendorContactCharArray = new string(' ', 40).ToArray();
				this.VendorContactPhoneAndExtensionCharArray = new string(' ', 15).ToArray();
				this.Blank6CharArray = new string(' ', 35).ToArray();
				this.VendorForeignEntityIndicatorCharArray = new string(' ', 1).ToArray();
				this.Blank5CharArray = new string(' ', 8).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct ARecord
		{
			// vbPorter upgrade warning: RecordType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecordTypeCharArray;
            public string RecordType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordTypeCharArray);
                }
                set
                {
                    RecordTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: PaymentYear As FixedString	OnWriteFCConvert.ToInt32(
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] PaymentYearCharArray;
            public string PaymentYear
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentYearCharArray);
                }
                set
                {
                    PaymentYearCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: CombinedFederalStateFiler As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] CombinedFederalStateFilerCharArray;
            public string CombinedFederalStateFiler
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CombinedFederalStateFilerCharArray);
                }
                set
                {
                    CombinedFederalStateFilerCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 5);
                }

            }
            // vbPorter upgrade warning: PayerTIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] PayerTINCharArray;
            public string PayerTIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerTINCharArray);
                }
                set
                {
                    PayerTINCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: PayerNameControl As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] PayerNameControlCharArray;
            public string PayerNameControl
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerNameControlCharArray);
                }
                set
                {
                    PayerNameControlCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: LastFilingIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] LastFilingIndicatorCharArray;
            public string LastFilingIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LastFilingIndicatorCharArray);
                }
                set
                {
                    LastFilingIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: TypeOfReturn As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] TypeOfReturnCharArray;
            public string TypeOfReturn
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TypeOfReturnCharArray);
                }
                set
                {
                    TypeOfReturnCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: AmountCodes As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public char[] AmountCodesCharArray;
            public string AmountCodes
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AmountCodesCharArray);
                }
                set
                {
                    AmountCodesCharArray = FCUtils.FixedStringToArray(value, 16);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: ForeignEntityIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] ForeignEntityIndicatorCharArray;
            public string ForeignEntityIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ForeignEntityIndicatorCharArray);
                }
                set
                {
                    ForeignEntityIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: FirstPayerNameLine As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] FirstPayerNameLineCharArray;
            public string FirstPayerNameLine
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FirstPayerNameLineCharArray);
                }
                set
                {
                    FirstPayerNameLineCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: SecondPayerNameLine As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] SecondPayerNameLineCharArray;
            public string SecondPayerNameLine
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SecondPayerNameLineCharArray);
                }
                set
                {
                    SecondPayerNameLineCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: TransferAgentIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TransferAgentIndicatorCharArray;
            public string TransferAgentIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TransferAgentIndicatorCharArray);
                }
                set
                {
                    TransferAgentIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: PayerShippingAddress As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] PayerShippingAddressCharArray;
            public string PayerShippingAddress
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerShippingAddressCharArray);
                }
                set
                {
                    PayerShippingAddressCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: PayerCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] PayerCityCharArray;
            public string PayerCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerCityCharArray);
                }
                set
                {
                    PayerCityCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: PayerState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] PayerStateCharArray;
            public string PayerState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerStateCharArray);
                }
                set
                {
                    PayerStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: PayerZip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] PayerZipCharArray;
            public string PayerZip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerZipCharArray);
                }
                set
                {
                    PayerZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: PayerPhoneAndExtension As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
            public char[] PayerPhoneAndExtensionCharArray;
            public string PayerPhoneAndExtension
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayerPhoneAndExtensionCharArray);
                }
                set
                {
                    PayerPhoneAndExtensionCharArray = FCUtils.FixedStringToArray(value, 15);
                }

            }
            // vbPorter upgrade warning: Blank4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
            public char[] Blank4CharArray;
            public string Blank4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank4CharArray);
                }
                set
                {
                    Blank4CharArray = FCUtils.FixedStringToArray(value, 260);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 241)]
            public char[] Blank5CharArray;
            public string Blank5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank5CharArray);
                }
                set
                {
                    Blank5CharArray = FCUtils.FixedStringToArray(value, 241);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public ARecord(int unusedVariable)
			{
				this.RecordTypeCharArray = new string(' ', 1).ToArray();
				this.PaymentYearCharArray = new string(' ', 4).ToArray();
				this.CombinedFederalStateFilerCharArray = new string(' ', 1).ToArray();
				this.Blank1CharArray = new string(' ', 5).ToArray();
				this.PayerTINCharArray = new string(' ', 9).ToArray();
				this.PayerNameControlCharArray = new string(' ', 4).ToArray();
				this.LastFilingIndicatorCharArray = new string(' ', 1).ToArray();
				this.TypeOfReturnCharArray = new string(' ', 2).ToArray();
				this.AmountCodesCharArray = new string(' ', 16).ToArray();
				this.Blank2CharArray = new string(' ', 8).ToArray();
				this.ForeignEntityIndicatorCharArray = new string(' ', 1).ToArray();
				this.FirstPayerNameLineCharArray = new string(' ', 40).ToArray();
				this.SecondPayerNameLineCharArray = new string(' ', 40).ToArray();
				this.TransferAgentIndicatorCharArray = new string(' ', 1).ToArray();
				this.PayerShippingAddressCharArray = new string(' ', 40).ToArray();
				this.PayerCityCharArray = new string(' ', 40).ToArray();
				this.PayerStateCharArray = new string(' ', 2).ToArray();
				this.PayerZipCharArray = new string(' ', 9).ToArray();
				this.PayerPhoneAndExtensionCharArray = new string(' ', 15).ToArray();
				this.Blank4CharArray = new string(' ', 260).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank5CharArray = new string(' ', 241).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct BRecord
		{
			// vbPorter upgrade warning: RecordType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecordTypeCharArray;
            public string RecordType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordTypeCharArray);
                }
                set
                {
                    RecordTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: PaymentYear As FixedString	OnWriteFCConvert.ToInt32(
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] PaymentYearCharArray;
            public string PaymentYear
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentYearCharArray);
                }
                set
                {
                    PaymentYearCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: CorrectedReturnIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] CorrectedReturnIndicatorCharArray;
            public string CorrectedReturnIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CorrectedReturnIndicatorCharArray);
                }
                set
                {
                    CorrectedReturnIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: NameControl As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] NameControlCharArray;
            public string NameControl
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NameControlCharArray);
                }
                set
                {
                    NameControlCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: TypeOfTIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] TypeOfTINCharArray;
            public string TypeOfTIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TypeOfTINCharArray);
                }
                set
                {
                    TypeOfTINCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: PayeesTIN As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] PayeesTINCharArray;
            public string PayeesTIN
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayeesTINCharArray);
                }
                set
                {
                    PayeesTINCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: PayersAccountNumberForPayee As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public char[] PayersAccountNumberForPayeeCharArray;
            public string PayersAccountNumberForPayee
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayersAccountNumberForPayeeCharArray);
                }
                set
                {
                    PayersAccountNumberForPayeeCharArray = FCUtils.FixedStringToArray(value, 20);
                }

            }
            // vbPorter upgrade warning: PayersOfficeCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] PayersOfficeCodeCharArray;
            public string PayersOfficeCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayersOfficeCodeCharArray);
                }
                set
                {
                    PayersOfficeCodeCharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 10);
                }

            }
            // vbPorter upgrade warning: Payment1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment1CharArray;
            public string Payment1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment1CharArray);
                }
                set
                {
                    Payment1CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment2CharArray;
            public string Payment2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment2CharArray);
                }
                set
                {
                    Payment2CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment3CharArray;
            public string Payment3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment3CharArray);
                }
                set
                {
                    Payment3CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment4CharArray;
            public string Payment4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment4CharArray);
                }
                set
                {
                    Payment4CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment5CharArray;
            public string Payment5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment5CharArray);
                }
                set
                {
                    Payment5CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment6 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment6CharArray;
            public string Payment6
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment6CharArray);
                }
                set
                {
                    Payment6CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment7 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment7CharArray;
            public string Payment7
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment7CharArray);
                }
                set
                {
                    Payment7CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment8 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment8CharArray;
            public string Payment8
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment8CharArray);
                }
                set
                {
                    Payment8CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: Payment9 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] Payment9CharArray;
            public string Payment9
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Payment9CharArray);
                }
                set
                {
                    Payment9CharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentA As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentACharArray;
            public string PaymentA
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentACharArray);
                }
                set
                {
                    PaymentACharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentB As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentBCharArray;
            public string PaymentB
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentBCharArray);
                }
                set
                {
                    PaymentBCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentC As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentCCharArray;
            public string PaymentC
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentCCharArray);
                }
                set
                {
                    PaymentCCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentD As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentDCharArray;
            public string PaymentD
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentDCharArray);
                }
                set
                {
                    PaymentDCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentE As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentECharArray;
            public string PaymentE
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentECharArray);
                }
                set
                {
                    PaymentECharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentFCharArray;
            public string PaymentF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentFCharArray);
                }
                set
                {
                    PaymentFCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: PaymentG As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] PaymentGCharArray;
            public string PaymentG
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentGCharArray);
                }
                set
                {
                    PaymentGCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: ForeignCountryIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] ForeignCountryIndicatorCharArray;
            public string ForeignCountryIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ForeignCountryIndicatorCharArray);
                }
                set
                {
                    ForeignCountryIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: FirstPayeeNameLine As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] FirstPayeeNameLineCharArray;
            public string FirstPayeeNameLine
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FirstPayeeNameLineCharArray);
                }
                set
                {
                    FirstPayeeNameLineCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: SecondPayeeNameLine As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] SecondPayeeNameLineCharArray;
            public string SecondPayeeNameLine
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SecondPayeeNameLineCharArray);
                }
                set
                {
                    SecondPayeeNameLineCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
			// vbPorter upgrade warning: PayeeMailingAddress As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] PayeeMailingAddressCharArray;
            public string PayeeMailingAddress
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayeeMailingAddressCharArray);
                }
                set
                {
                    PayeeMailingAddressCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: Blank3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] Blank3CharArray;
            public string Blank3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank3CharArray);
                }
                set
                {
                    Blank3CharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: PayeeCity As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
            public char[] PayeeCityCharArray;
            public string PayeeCity
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayeeCityCharArray);
                }
                set
                {
                    PayeeCityCharArray = FCUtils.FixedStringToArray(value, 40);
                }

            }
            // vbPorter upgrade warning: PayeeState As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] PayeeStateCharArray;
            public string PayeeState
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayeeStateCharArray);
                }
                set
                {
                    PayeeStateCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: PayeeZip As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
            public char[] PayeeZipCharArray;
            public string PayeeZip
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PayeeZipCharArray);
                }
                set
                {
                    PayeeZipCharArray = FCUtils.FixedStringToArray(value, 9);
                }

            }
            // vbPorter upgrade warning: Blank4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] Blank4CharArray;
            public string Blank4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank4CharArray);
                }
                set
                {
                    Blank4CharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 36)]
            public char[] Blank5CharArray;
            public string Blank5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank5CharArray);
                }
                set
                {
                    Blank5CharArray = FCUtils.FixedStringToArray(value, 36);
                }

            }
            // vbPorter upgrade warning: SecondTINNotice As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] SecondTINNoticeCharArray;
            public string SecondTINNotice
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SecondTINNoticeCharArray);
                }
                set
                {
                    SecondTINNoticeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: Blank6 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] Blank6CharArray;
            public string Blank6
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank6CharArray);
                }
                set
                {
                    Blank6CharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: DirectSalesIndicator As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] DirectSalesIndicatorCharArray;
            public string DirectSalesIndicator
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DirectSalesIndicatorCharArray);
                }
                set
                {
                    DirectSalesIndicatorCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: Blank7 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 115)]
            public char[] Blank7CharArray;
            public string Blank7
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank7CharArray);
                }
                set
                {
                    Blank7CharArray = FCUtils.FixedStringToArray(value, 115);
                }

            }
            // vbPorter upgrade warning: SpecialDataEntries As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 60)]
            public char[] SpecialDataEntriesCharArray;
            public string SpecialDataEntries
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SpecialDataEntriesCharArray);
                }
                set
                {
                    SpecialDataEntriesCharArray = FCUtils.FixedStringToArray(value, 60);
                }

            }
            // vbPorter upgrade warning: StateIncomeTaxWithheld As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] StateIncomeTaxWithheldCharArray;
            public string StateIncomeTaxWithheld
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StateIncomeTaxWithheldCharArray);
                }
                set
                {
                    StateIncomeTaxWithheldCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: LocalIncomeTaxWithheld As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] LocalIncomeTaxWithheldCharArray;
            public string LocalIncomeTaxWithheld
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LocalIncomeTaxWithheldCharArray);
                }
                set
                {
                    LocalIncomeTaxWithheldCharArray = FCUtils.FixedStringToArray(value, 12);
                }

            }
            // vbPorter upgrade warning: CombinedFederalStateCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CombinedFederalStateCodeCharArray;
            public string CombinedFederalStateCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CombinedFederalStateCodeCharArray);
                }
                set
                {
                    CombinedFederalStateCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public BRecord(int unusedParam)
			{
				this.RecordTypeCharArray = new string(' ', 1).ToArray();
				this.PaymentYearCharArray = new string(' ', 4).ToArray();
				this.CorrectedReturnIndicatorCharArray = new string(' ', 1).ToArray();
				this.NameControlCharArray = new string(' ', 4).ToArray();
				this.TypeOfTINCharArray = new string(' ', 1).ToArray();
				this.PayeesTINCharArray = new string(' ', 9).ToArray();
				this.PayersAccountNumberForPayeeCharArray = new string(' ', 20).ToArray();
				this.PayersOfficeCodeCharArray = new string(' ', 4).ToArray();
				this.Blank1CharArray = new string(' ', 10).ToArray();
				this.Payment1CharArray = new string(' ', 12).ToArray();
				this.Payment2CharArray = new string(' ', 12).ToArray();
				this.Payment3CharArray = new string(' ', 12).ToArray();
				this.Payment4CharArray = new string(' ', 12).ToArray();
				this.Payment5CharArray = new string(' ', 12).ToArray();
				this.Payment6CharArray = new string(' ', 12).ToArray();
				this.Payment7CharArray = new string(' ', 12).ToArray();
				this.Payment8CharArray = new string(' ', 12).ToArray();
				this.Payment9CharArray = new string(' ', 12).ToArray();
				this.PaymentACharArray = new string(' ', 12).ToArray();
				this.PaymentBCharArray = new string(' ', 12).ToArray();
				this.PaymentCCharArray = new string(' ', 12).ToArray();
				this.PaymentDCharArray = new string(' ', 12).ToArray();
				this.PaymentECharArray = new string(' ', 12).ToArray();
				this.PaymentFCharArray = new string(' ', 12).ToArray();
				this.PaymentGCharArray = new string(' ', 12).ToArray();
				this.ForeignCountryIndicatorCharArray = new string(' ', 1).ToArray();
				this.FirstPayeeNameLineCharArray = new string(' ', 40).ToArray();
				this.SecondPayeeNameLineCharArray = new string(' ', 40).ToArray();
				this.Blank2CharArray = new string(' ', 40).ToArray();
				this.PayeeMailingAddressCharArray = new string(' ', 40).ToArray();
				this.Blank3CharArray = new string(' ', 40).ToArray();
				this.PayeeCityCharArray = new string(' ', 40).ToArray();
				this.PayeeStateCharArray = new string(' ', 2).ToArray();
				this.PayeeZipCharArray = new string(' ', 9).ToArray();
				this.Blank4CharArray = new string(' ', 1).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank5CharArray = new string(' ', 36).ToArray();
				this.SecondTINNoticeCharArray = new string(' ', 1).ToArray();
				this.Blank6CharArray = new string(' ', 2).ToArray();
				this.DirectSalesIndicatorCharArray = new string(' ', 1).ToArray();
				this.Blank7CharArray = new string(' ', 115).ToArray();
				this.SpecialDataEntriesCharArray = new string(' ', 60).ToArray();
				this.StateIncomeTaxWithheldCharArray = new string(' ', 12).ToArray();
				this.LocalIncomeTaxWithheldCharArray = new string(' ', 12).ToArray();
				this.CombinedFederalStateCodeCharArray = new string(' ', 2).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct CRecord
		{
			// vbPorter upgrade warning: RecordType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecordTypeCharArray;
            public string RecordType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordTypeCharArray);
                }
                set
                {
                    RecordTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: NumberOfPayees As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] NumberOfPayeesCharArray;
            public string NumberOfPayees
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NumberOfPayeesCharArray);
                }
                set
                {
                    NumberOfPayeesCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 6);
                }

            }
            // vbPorter upgrade warning: ControlTotal1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal1CharArray;
            public string ControlTotal1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal1CharArray);
                }
                set
                {
                    ControlTotal1CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal2CharArray;
            public string ControlTotal2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal2CharArray);
                }
                set
                {
                    ControlTotal2CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal3CharArray;
            public string ControlTotal3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal3CharArray);
                }
                set
                {
                    ControlTotal3CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal4CharArray;
            public string ControlTotal4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal4CharArray);
                }
                set
                {
                    ControlTotal4CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal5CharArray;
            public string ControlTotal5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal5CharArray);
                }
                set
                {
                    ControlTotal5CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal6 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal6CharArray;
            public string ControlTotal6
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal6CharArray);
                }
                set
                {
                    ControlTotal6CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal7 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal7CharArray;
            public string ControlTotal7
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal7CharArray);
                }
                set
                {
                    ControlTotal7CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal8 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal8CharArray;
            public string ControlTotal8
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal8CharArray);
                }
                set
                {
                    ControlTotal8CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal9 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal9CharArray;
            public string ControlTotal9
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal9CharArray);
                }
                set
                {
                    ControlTotal9CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalA As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalACharArray;
            public string ControlTotalA
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalACharArray);
                }
                set
                {
                    ControlTotalACharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalB As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalBCharArray;
            public string ControlTotalB
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalBCharArray);
                }
                set
                {
                    ControlTotalBCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalC As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalCCharArray;
            public string ControlTotalC
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalCCharArray);
                }
                set
                {
                    ControlTotalCCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalD As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalDCharArray;
            public string ControlTotalD
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalDCharArray);
                }
                set
                {
                    ControlTotalDCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalE As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalECharArray;
            public string ControlTotalE
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalECharArray);
                }
                set
                {
                    ControlTotalECharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalFCharArray;
            public string ControlTotalF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalFCharArray);
                }
                set
                {
                    ControlTotalFCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalG As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalGCharArray;
            public string ControlTotalG
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalGCharArray);
                }
                set
                {
                    ControlTotalGCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 196)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 196);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 241)]
            public char[] Blank3CharArray;
            public string Blank3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank3CharArray);
                }
                set
                {
                    Blank3CharArray = FCUtils.FixedStringToArray(value, 241);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public CRecord(int unusedParam)
			{
				this.RecordTypeCharArray = new string(' ', 1).ToArray();
				this.NumberOfPayeesCharArray = new string(' ', 8).ToArray();
				this.Blank1CharArray = new string(' ', 6).ToArray();
				this.ControlTotal1CharArray = new string(' ', 18).ToArray();
				this.ControlTotal2CharArray = new string(' ', 18).ToArray();
				this.ControlTotal3CharArray = new string(' ', 18).ToArray();
				this.ControlTotal4CharArray = new string(' ', 18).ToArray();
				this.ControlTotal5CharArray = new string(' ', 18).ToArray();
				this.ControlTotal6CharArray = new string(' ', 18).ToArray();
				this.ControlTotal7CharArray = new string(' ', 18).ToArray();
				this.ControlTotal8CharArray = new string(' ', 18).ToArray();
				this.ControlTotal9CharArray = new string(' ', 18).ToArray();
				this.ControlTotalACharArray = new string(' ', 18).ToArray();
				this.ControlTotalBCharArray = new string(' ', 18).ToArray();
				this.ControlTotalCCharArray = new string(' ', 18).ToArray();
				this.ControlTotalDCharArray = new string(' ', 18).ToArray();
				this.ControlTotalECharArray = new string(' ', 18).ToArray();
				this.ControlTotalFCharArray = new string(' ', 18).ToArray();
				this.ControlTotalGCharArray = new string(' ', 18).ToArray();
				this.Blank2CharArray = new string(' ', 196).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank3CharArray = new string(' ', 241).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct KRecord
		{
			// vbPorter upgrade warning: RecordType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecordTypeCharArray;
            public string RecordType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordTypeCharArray);
                }
                set
                {
                    RecordTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: NumberOfPayees As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] NumberOfPayeesCharArray;
            public string NumberOfPayees
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NumberOfPayeesCharArray);
                }
                set
                {
                    NumberOfPayeesCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 6);
                }

            }
            // vbPorter upgrade warning: ControlTotal1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal1CharArray;
            public string ControlTotal1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal1CharArray);
                }
                set
                {
                    ControlTotal1CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal2CharArray;
            public string ControlTotal2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal2CharArray);
                }
                set
                {
                    ControlTotal2CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal3CharArray;
            public string ControlTotal3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal3CharArray);
                }
                set
                {
                    ControlTotal3CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal4CharArray;
            public string ControlTotal4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal4CharArray);
                }
                set
                {
                    ControlTotal4CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal5 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal5CharArray;
            public string ControlTotal5
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal5CharArray);
                }
                set
                {
                    ControlTotal5CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal6 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal6CharArray;
            public string ControlTotal6
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal6CharArray);
                }
                set
                {
                    ControlTotal6CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal7 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal7CharArray;
            public string ControlTotal7
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal7CharArray);
                }
                set
                {
                    ControlTotal7CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal8 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal8CharArray;
            public string ControlTotal8
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal8CharArray);
                }
                set
                {
                    ControlTotal8CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotal9 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotal9CharArray;
            public string ControlTotal9
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotal9CharArray);
                }
                set
                {
                    ControlTotal9CharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalA As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalACharArray;
            public string ControlTotalA
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalACharArray);
                }
                set
                {
                    ControlTotalACharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalB As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalBCharArray;
            public string ControlTotalB
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalBCharArray);
                }
                set
                {
                    ControlTotalBCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalC As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalCCharArray;
            public string ControlTotalC
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalCCharArray);
                }
                set
                {
                    ControlTotalCCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalD As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalDCharArray;
            public string ControlTotalD
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalDCharArray);
                }
                set
                {
                    ControlTotalDCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalE As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalECharArray;
            public string ControlTotalE
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalECharArray);
                }
                set
                {
                    ControlTotalECharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalFCharArray;
            public string ControlTotalF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalFCharArray);
                }
                set
                {
                    ControlTotalFCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: ControlTotalG As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] ControlTotalGCharArray;
            public string ControlTotalG
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ControlTotalGCharArray);
                }
                set
                {
                    ControlTotalGCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 196)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 196);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 199)]
            public char[] Blank3CharArray;
            public string Blank3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank3CharArray);
                }
                set
                {
                    Blank3CharArray = FCUtils.FixedStringToArray(value, 199);
                }

            }
            // vbPorter upgrade warning: StateIncomeTaxWithheld As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] StateIncomeTaxWithheldCharArray;
            public string StateIncomeTaxWithheld
            {
                get
                {
                    return FCUtils.FixedStringFromArray(StateIncomeTaxWithheldCharArray);
                }
                set
                {
                    StateIncomeTaxWithheldCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: LocalIncomeTaxWithheld As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
            public char[] LocalIncomeTaxWithheldCharArray;
            public string LocalIncomeTaxWithheld
            {
                get
                {
                    return FCUtils.FixedStringFromArray(LocalIncomeTaxWithheldCharArray);
                }
                set
                {
                    LocalIncomeTaxWithheldCharArray = FCUtils.FixedStringToArray(value, 18);
                }

            }
            // vbPorter upgrade warning: Blank4 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] Blank4CharArray;
            public string Blank4
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank4CharArray);
                }
                set
                {
                    Blank4CharArray = FCUtils.FixedStringToArray(value, 4);
                }

            }
            // vbPorter upgrade warning: CombinedFederalStateCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CombinedFederalStateCodeCharArray;
            public string CombinedFederalStateCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CombinedFederalStateCodeCharArray);
                }
                set
                {
                    CombinedFederalStateCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public KRecord(int unusedParam)
			{
				this.RecordTypeCharArray = new string(' ', 1).ToArray();
				this.NumberOfPayeesCharArray = new string(' ', 8).ToArray();
				this.Blank1CharArray = new string(' ', 6).ToArray();
				this.ControlTotal1CharArray = new string(' ', 18).ToArray();
				this.ControlTotal2CharArray = new string(' ', 18).ToArray();
				this.ControlTotal3CharArray = new string(' ', 18).ToArray();
				this.ControlTotal4CharArray = new string(' ', 18).ToArray();
				this.ControlTotal5CharArray = new string(' ', 18).ToArray();
				this.ControlTotal6CharArray = new string(' ', 18).ToArray();
				this.ControlTotal7CharArray = new string(' ', 18).ToArray();
				this.ControlTotal8CharArray = new string(' ', 18).ToArray();
				this.ControlTotal9CharArray = new string(' ', 18).ToArray();
				this.ControlTotalACharArray = new string(' ', 18).ToArray();
				this.ControlTotalBCharArray = new string(' ', 18).ToArray();
				this.ControlTotalCCharArray = new string(' ', 18).ToArray();
				this.ControlTotalDCharArray = new string(' ', 18).ToArray();
				this.ControlTotalECharArray = new string(' ', 18).ToArray();
				this.ControlTotalFCharArray = new string(' ', 18).ToArray();
				this.ControlTotalGCharArray = new string(' ', 18).ToArray();
				this.Blank2CharArray = new string(' ', 196).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank3CharArray = new string(' ', 199).ToArray();
				this.StateIncomeTaxWithheldCharArray = new string(' ', 18).ToArray();
				this.LocalIncomeTaxWithheldCharArray = new string(' ', 18).ToArray();
				this.Blank4CharArray = new string(' ', 4).ToArray();
				this.CombinedFederalStateCodeCharArray = new string(' ', 2).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct FRecord
		{
			// vbPorter upgrade warning: RecordType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] RecordTypeCharArray;
            public string RecordType
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordTypeCharArray);
                }
                set
                {
                    RecordTypeCharArray = FCUtils.FixedStringToArray(value, 1);
                }

            }
            // vbPorter upgrade warning: NumberOfARecords As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] NumberOfARecordsCharArray;
            public string NumberOfARecords
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NumberOfARecordsCharArray);
                }
                set
                {
                    NumberOfARecordsCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Zeros As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 21)]
            public char[] ZerosCharArray;
            public string Zeros
            {
                get
                {
                    return FCUtils.FixedStringFromArray(ZerosCharArray);
                }
                set
                {
                    ZerosCharArray = FCUtils.FixedStringToArray(value, 21);
                }

            }
            // vbPorter upgrade warning: Blank1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 19)]
            public char[] Blank1CharArray;
            public string Blank1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank1CharArray);
                }
                set
                {
                    Blank1CharArray = FCUtils.FixedStringToArray(value, 19);
                }

            }
            // vbPorter upgrade warning: TotalNumberOfPayees As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] TotalNumberOfPayeesCharArray;
            public string TotalNumberOfPayees
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TotalNumberOfPayeesCharArray);
                }
                set
                {
                    TotalNumberOfPayeesCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 442)]
            public char[] Blank2CharArray;
            public string Blank2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank2CharArray);
                }
                set
                {
                    Blank2CharArray = FCUtils.FixedStringToArray(value, 442);
                }

            }
            // vbPorter upgrade warning: RecordSequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] RecordSequenceNumberCharArray;
            public string RecordSequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(RecordSequenceNumberCharArray);
                }
                set
                {
                    RecordSequenceNumberCharArray = FCUtils.FixedStringToArray(value, 8);
                }

            }
            // vbPorter upgrade warning: Blank3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 241)]
            public char[] Blank3CharArray;
            public string Blank3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Blank3CharArray);
                }
                set
                {
                    Blank3CharArray = FCUtils.FixedStringToArray(value, 241);
                }

            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }

            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public FRecord(int unusedParam)
			{
				this.RecordTypeCharArray = new string(' ', 1).ToArray();
				this.NumberOfARecordsCharArray = new string(' ', 8).ToArray();
				this.ZerosCharArray = new string(' ', 21).ToArray();
				this.Blank1CharArray = new string(' ', 19).ToArray();
				this.TotalNumberOfPayeesCharArray = new string(' ', 8).ToArray();
				this.Blank2CharArray = new string(' ', 442).ToArray();
				this.RecordSequenceNumberCharArray = new string(' ', 8).ToArray();
				this.Blank3CharArray = new string(' ', 241).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		public class StaticVariables
		{
			public TRecord TR = new TRecord(0);
			public ARecord AR = new ARecord(0);
			public BRecord BR = new BRecord(0);
			public CRecord CR = new CRecord(0);
			public KRecord KR = new KRecord(0);
			public FRecord FR = new FRecord(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
