﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmMultipleJournalSelection.
	/// </summary>
	public partial class frmMultipleJournalSelection : BaseForm
	{
		public frmMultipleJournalSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public string strReportType = "";
		int SelectCol;
		int JournalCol;
		int TypeCol;
		int DescriptionCol;

		private void CancelButton_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmMultipleJournalSelection_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vsJournals.Focus();
		}

		private void frmMultipleJournalSelection_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMultipleJournalSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMultipleJournalSelection.FillStyle	= 0;
			//frmMultipleJournalSelection.ScaleWidth	= 5880;
			//frmMultipleJournalSelection.ScaleHeight	= 4260;
			//frmMultipleJournalSelection.LinkTopic	= "Form2";
			//frmMultipleJournalSelection.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsJournals = new clsDRWrapper();
			SelectCol = 0;
			JournalCol = 1;
			TypeCol = 2;
			DescriptionCol = 3;
			if (strReportType == "P")
			{
				rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Type, Description FROM JournalMaster WHERE Status = 'P' ORDER BY JournalNumber");
				this.Text = "Posted Journal";
			}
			else
			{
				rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Type, Description FROM JournalMaster WHERE Status <> 'P' AND Status <> 'D' ORDER BY JournalNumber");
				this.Text = "Unposted Journal";
			}
			vsJournals.TextMatrix(0, SelectCol, "Select");
			vsJournals.TextMatrix(0, JournalCol, "Jrnl");
			vsJournals.TextMatrix(0, TypeCol, "Type");
			vsJournals.TextMatrix(0, DescriptionCol, "Description");
			//vsJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsJournals.Cols - 1, 4);
			vsJournals.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsJournals.ColWidth(SelectCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
			vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
			vsJournals.ColWidth(TypeCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				do
				{
					vsJournals.AddItem("");
					vsJournals.TextMatrix(vsJournals.Rows - 1, SelectCol, FCConvert.ToString(false));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vsJournals.TextMatrix(vsJournals.Rows - 1, JournalCol, modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournals.Get_Fields("JournalNumber")), 4));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					vsJournals.TextMatrix(vsJournals.Rows - 1, TypeCol, FCConvert.ToString(rsJournals.Get_Fields("Type")));
					vsJournals.TextMatrix(vsJournals.Rows - 1, DescriptionCol, FCConvert.ToString(rsJournals.Get_Fields_String("Description")));
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
				vsJournals.Row = 1;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmMultipleJournalSelection_Resize(object sender, System.EventArgs e)
		{
			vsJournals.ColWidth(SelectCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
			vsJournals.ColWidth(JournalCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
			vsJournals.ColWidth(TypeCol, FCConvert.ToInt32(vsJournals.WidthOriginal * 0.1264679));
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			OKButton_Click();
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsJournalCheck = new clsDRWrapper();
			int counter;
			bool blnDataSelected;
			string strSQL;
			blnDataSelected = false;
			strSQL = "";
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, SelectCol)) == true)
				{
					blnDataSelected = true;
					strSQL += "JournalNumber = " + FCConvert.ToString(Conversion.Val(vsJournals.TextMatrix(counter, JournalCol))) + " OR ";
				}
			}
			if (blnDataSelected)
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
				if (strReportType == "P")
				{
					rsJournalCheck.OpenRecordset("SELECT DISTINCT JournalNumber FROM JournalMaster WHERE Status = 'P' AND (" + strSQL + ") ORDER BY JournalNumber");
				}
				else
				{
					rsJournalCheck.OpenRecordset("SELECT DISTINCT JournalNumber FROM JournalMaster WHERE Status <> 'P' AND (" + strSQL + ") ORDER BY JournalNumber");
				}
				if (rsJournalCheck.EndOfFile() != true && rsJournalCheck.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					if (strReportType == "P")
					{
						MessageBox.Show("There are no posted journals that match the number you entered.  Please enter a valid journal number to continue.", "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("There are no unposted journals that match the number you entered.  Please enter a valid journal number to continue.", "No Journal Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					return;
				}
			}
			else
			{
				MessageBox.Show("You must select at least 1 journal number before you may proceed.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			this.Close();
			rptMultipleJournals.InstancePtr.strSQL = rsJournalCheck.Name();
			rptMultipleJournals.InstancePtr.strReportType = strReportType;
			rptMultipleJournals.InstancePtr.blnShowLiquidatedEncActivity = chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked;
			frmReportViewer.InstancePtr.Init(rptMultipleJournals.InstancePtr);
		}

		public void OKButton_Click()
		{
			OKButton_Click(cmdProcessSave, new System.EventArgs());
		}

		private void vsJournals_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsJournals.Row >= 1)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, SelectCol)) == false)
				{
					vsJournals.TextMatrix(vsJournals.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsJournals.TextMatrix(vsJournals.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsJournals.Row >= 1)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, SelectCol)) == false)
					{
						vsJournals.TextMatrix(vsJournals.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsJournals.TextMatrix(vsJournals.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsJournals_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsJournals[e.ColumnIndex, e.RowIndex];
            if (vsJournals.GetFlexRowIndex(e.RowIndex) > 0)
			{
				//ToolTip1.SetToolTip(vsJournals, vsJournals.TextMatrix(vsJournals.MouseRow, DescriptionCol));
				cell.ToolTipText =  vsJournals.TextMatrix(vsJournals.GetFlexRowIndex(e.RowIndex), DescriptionCol);
			}
			else
			{
                //ToolTip1.SetToolTip(vsJournals, "");
                cell.ToolTipText = "";
			}
		}
	}
}
