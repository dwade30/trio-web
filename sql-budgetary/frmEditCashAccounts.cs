﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditCashAccounts.
	/// </summary>
	public partial class frmEditCashAccounts : BaseForm
	{
		public frmEditCashAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditCashAccounts InstancePtr
		{
			get
			{
				return (frmEditCashAccounts)Sys.GetInstance(typeof(frmEditCashAccounts));
			}
		}

		protected frmEditCashAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int NumberCol;
		int AccountCol;
		int AllSuffixCol;
		// vbPorter upgrade warning: lngBankNumber As int	OnWrite(string)
		public int lngBankNumber;
		bool EditFlag;

		private void frmEditCashAccounts_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			vsAccounts.Focus();
			Support.SendKeys("{UP}", false);
			Support.SendKeys("{DOWN}", false);
		}

		private void frmEditCashAccounts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSACCOUNTS")
			{
				if (vsAccounts.Col == AccountCol && vsAccounts.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(vsAccounts, vsAccounts.Row, vsAccounts.Col, KeyCode, Shift, vsAccounts.EditSelStart, vsAccounts.EditText, vsAccounts.EditSelLength);
					vsAccounts.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSINTEREST")
			{
				if (vsInterest.Col == AccountCol && vsInterest.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(vsInterest, vsInterest.Row, vsInterest.Col, KeyCode, Shift, vsInterest.EditSelStart, vsInterest.EditText, vsInterest.EditSelLength);
					vsInterest.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmEditCashAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditCashAccounts.FillStyle	= 0;
			//frmEditCashAccounts.ScaleWidth	= 5880;
			//frmEditCashAccounts.ScaleHeight	= 4290;
			//frmEditCashAccounts.LinkTopic	= "Form2";
			//frmEditCashAccounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			NumberCol = 0;
			AccountCol = 1;
			AllSuffixCol = 2;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			vsAccounts.TextMatrix(0, AccountCol, "Account");
			vsAccounts.TextMatrix(0, AllSuffixCol, "All Suffixes");
			vsAccounts.ColHidden(NumberCol, true);
			vsAccounts.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.90));
			vsAccounts.ColDataType(AllSuffixCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsInterest.ColHidden(NumberCol, true);
			vsInterest.TextMatrix(0, AccountCol, "Account");
			vsInterest.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.99));
			EditFlag = true;
			rsInfo.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 0 AND BankNumber = " + FCConvert.ToString(lngBankNumber) + " ORDER BY Account");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsAccounts.Rows += 1;
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, NumberCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, FCConvert.ToString(rsInfo.Get_Fields("Account")));
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, AllSuffixCol, FCConvert.ToString(rsInfo.Get_Fields_Boolean("AllSuffix")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				vsAccounts.Rows += 1;
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, NumberCol, FCConvert.ToString(0));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, AllSuffixCol, FCConvert.ToString(false));
			}
			else
			{
				vsAccounts.Rows += 1;
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, NumberCol, FCConvert.ToString(0));
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, AccountCol, "");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, AllSuffixCol, FCConvert.ToString(false));
			}
			rsInfo.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 1 AND BankNumber = " + FCConvert.ToString(lngBankNumber) + " ORDER BY Account");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsInterest.Rows += 1;
					vsInterest.TextMatrix(vsInterest.Rows - 1, NumberCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsInterest.TextMatrix(vsInterest.Rows - 1, AccountCol, FCConvert.ToString(rsInfo.Get_Fields("Account")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				vsInterest.Rows += 1;
				vsInterest.TextMatrix(vsInterest.Rows - 1, NumberCol, FCConvert.ToString(0));
				vsInterest.TextMatrix(vsInterest.Rows - 1, AccountCol, "");
			}
			else
			{
				vsInterest.Rows += 1;
				vsInterest.TextMatrix(vsInterest.Rows - 1, NumberCol, FCConvert.ToString(0));
				vsInterest.TextMatrix(vsInterest.Rows - 1, AccountCol, "");
			}
			EditFlag = false;
			if (!modBudgetaryAccounting.Statics.gboolAutoInterest)
			{
				//FC:FINAL:AM:can't disable a tabpage; hide it instead
				//tabAccounts.TabPages[1].Enabled = false;
				tabAccounts.TabPages[1].Visible = false;
			}
			else
			{
				tabAccounts.TabPages[0].Enabled = true;
			}
			//FC:FINAL:AM: attach event handlers
			this.vsAccounts.EditingControlShowing += vsAccounts_EditingControlShowing;
			this.vsInterest.EditingControlShowing += vsInterest_EditingControlShowing;
		}

		private void frmEditCashAccounts_Resize(object sender, EventArgs e)
		{
			vsAccounts.ColWidth(AccountCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.90));
		}

		private void frmEditCashAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmBankNames.InstancePtr.Show(App.MainForm);
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (tabAccounts.SelectedIndex == 0)
			{
				if (vsAccounts.Row > 0)
				{
					if (vsAccounts.TextMatrix(vsAccounts.Row, AccountCol) != "" && Strings.InStr(1, vsAccounts.TextMatrix(vsAccounts.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						ans = MessageBox.Show("Are you sure you wish to delete this account?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (FCConvert.ToDouble(vsAccounts.TextMatrix(vsAccounts.Row, NumberCol)) != 0)
							{
								rsDelete.Execute("DELETE FROM BankCashAccounts WHERE ID = " + vsAccounts.TextMatrix(vsAccounts.Row, NumberCol), "Budgetary");
							}
							vsAccounts.RemoveItem(vsAccounts.Row);
							if (vsAccounts.Row > 1)
							{
								vsAccounts.Row -= 1;
							}
						}
					}
					else
					{
						vsAccounts.RemoveItem(vsAccounts.Row);
						if (vsAccounts.Row > 1)
						{
							vsAccounts.Row -= 1;
						}
					}
				}
			}
			else
			{
				if (vsInterest.Row > 0)
				{
					if (vsInterest.TextMatrix(vsInterest.Row, AccountCol) != "" && Strings.InStr(1, vsInterest.TextMatrix(vsInterest.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
					{
						ans = MessageBox.Show("Are you sure you wish to delete this account?", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (FCConvert.ToDouble(vsInterest.TextMatrix(vsInterest.Row, NumberCol)) != 0)
							{
								rsDelete.Execute("DELETE FROM BankCashAccounts WHERE ID = " + vsInterest.TextMatrix(vsInterest.Row, NumberCol), "Budgetary");
							}
							vsInterest.RemoveItem(vsInterest.Row);
							if (vsInterest.Row > 1)
							{
								vsInterest.Row -= 1;
							}
						}
					}
					else
					{
						vsInterest.RemoveItem(vsInterest.Row);
						if (vsInterest.Row > 1)
						{
							vsInterest.Row -= 1;
						}
					}
				}
			}
		}

		private void mnuFileInsert_Click(object sender, System.EventArgs e)
		{
			if (tabAccounts.SelectedIndex == 0)
			{
				vsAccounts.AddItem("", vsAccounts.Row + 1);
				vsAccounts.TextMatrix(vsAccounts.Row + 1, NumberCol, FCConvert.ToString(0));
				vsAccounts.TextMatrix(vsAccounts.Row + 1, AccountCol, "");
				vsAccounts.TextMatrix(vsAccounts.Row + 1, AllSuffixCol, FCConvert.ToString(false));
				vsAccounts.Row += 1;
			}
			else
			{
				vsInterest.AddItem("", vsInterest.Row + 1);
				vsInterest.TextMatrix(vsInterest.Row + 1, NumberCol, FCConvert.ToString(0));
				vsInterest.TextMatrix(vsInterest.Row + 1, AccountCol, "");
				vsInterest.Row += 1;
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			int counter;
			if (tabAccounts.SelectedIndex == 0)
			{
				if (vsAccounts.Row > 0)
				{
					vsAccounts.Select(vsAccounts.Row, AllSuffixCol);
				}
			}
			else
			{
				if (vsInterest.Row > 0)
				{
					if (vsInterest.Row < vsInterest.Rows - 1)
					{
						vsInterest.Select(vsInterest.Row + 1, AccountCol);
					}
					else
					{
						vsInterest.Select(vsInterest.Row - 1, AccountCol);
					}
				}
			}
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				if (vsAccounts.TextMatrix(counter, AccountCol) != "")
				{
					if (FCUtils.CBool(vsAccounts.TextMatrix(counter, AllSuffixCol)) == false)
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 0 AND BankNumber <> " + FCConvert.ToString(lngBankNumber) + " AND (Account = '" + vsAccounts.TextMatrix(counter, AccountCol) + "' OR (left(Account, " + FCConvert.ToString(vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + ") = '" + Strings.Left(vsAccounts.TextMatrix(counter, AccountCol), vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + "' AND AllSuffix = 1))");
					}
					else
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 0 AND BankNumber <> " + FCConvert.ToString(lngBankNumber) + " AND (left(Account, " + FCConvert.ToString(vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + ") = '" + Strings.Left(vsAccounts.TextMatrix(counter, AccountCol), vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + "')");
					}
					if (rsSave.EndOfFile() != true && rsSave.BeginningOfFile() != true)
					{
						if (FCUtils.CBool(vsAccounts.TextMatrix(counter, AllSuffixCol)) == true)
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							MessageBox.Show("One of the accounts included in " + Strings.Left(vsAccounts.TextMatrix(counter, AccountCol), vsAccounts.TextMatrix(counter, AccountCol).Length - 2) + "XX has already been set up as a cash account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the other bank before you may add it to this bank.", "Cash Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							MessageBox.Show("Account " + vsAccounts.TextMatrix(counter, AccountCol) + " has already been set up as a cash account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the other bank before you may add it to this bank.", "Cash Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						tabAccounts.SelectedIndex = 0;
						vsAccounts.Select(counter, AccountCol);
						return;
					}
					if (FCUtils.CBool(vsAccounts.TextMatrix(counter, AllSuffixCol)) == false)
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 1 AND Account = '" + vsAccounts.TextMatrix(counter, AccountCol) + "'");
					}
					else
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 1 AND (left(Account, " + FCConvert.ToString(vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + ") = '" + Strings.Left(vsAccounts.TextMatrix(counter, AccountCol), vsAccounts.TextMatrix(counter, AccountCol).Length - 3) + "')");
					}
					if (rsSave.EndOfFile() != true && rsSave.BeginningOfFile() != true)
					{
						if (FCUtils.CBool(vsAccounts.TextMatrix(counter, AllSuffixCol)) == true)
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							MessageBox.Show("One of the accounts included in " + Strings.Left(vsAccounts.TextMatrix(counter, AccountCol), vsAccounts.TextMatrix(counter, AccountCol).Length - 2) + "XX has already been set up as an interest account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the banks interest accounts before you may add it to this bank.", "Cash Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							MessageBox.Show("Account " + vsAccounts.TextMatrix(counter, AccountCol) + " has already been set up as an interest account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the banks interest accounts before you may add it to this bank.", "Cash Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						tabAccounts.SelectedIndex = 0;
						vsAccounts.Select(counter, AccountCol);
						return;
					}
				}
			}
			for (counter = 1; counter <= vsInterest.Rows - 1; counter++)
			{
				if (vsInterest.TextMatrix(counter, AccountCol) != "")
				{
					rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 1 AND BankNumber <> " + FCConvert.ToString(lngBankNumber) + " AND (Account = '" + vsInterest.TextMatrix(counter, AccountCol) + "' OR (left(Account, " + FCConvert.ToString(vsInterest.TextMatrix(counter, AccountCol).Length - 3) + ") = '" + Strings.Left(vsInterest.TextMatrix(counter, AccountCol), vsInterest.TextMatrix(counter, AccountCol).Length - 3) + "' AND AllSuffix = 1))");
					if (rsSave.EndOfFile() != true && rsSave.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						MessageBox.Show("Account " + vsInterest.TextMatrix(counter, AccountCol) + " has already been set up as an interest account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the other bank before you may add it to this bank.", "Interest Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						tabAccounts.SelectedIndex = 1;
						vsInterest.Select(counter, AccountCol);
						return;
					}
				}
				if (vsInterest.TextMatrix(counter, AccountCol) != "")
				{
					rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE Interest = 0 AND (Account = '" + vsInterest.TextMatrix(counter, AccountCol) + "' OR (left(Account, " + FCConvert.ToString(vsInterest.TextMatrix(counter, AccountCol).Length - 3) + ") = '" + Strings.Left(vsInterest.TextMatrix(counter, AccountCol), vsInterest.TextMatrix(counter, AccountCol).Length - 3) + "' AND AllSuffix = 1))");
					if (rsSave.EndOfFile() != true && rsSave.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						MessageBox.Show("Account " + vsInterest.TextMatrix(counter, AccountCol) + " has already been set up as a cash account for bank " + rsSave.Get_Fields("BankNumber") + ".  You must remove the account from the other bank before you may add it to this bank.", "Interest Account Already Set Up", MessageBoxButtons.OK, MessageBoxIcon.Information);
						tabAccounts.SelectedIndex = 1;
						vsInterest.Select(counter, AccountCol);
						return;
					}
				}
			}
			for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
			{
				if (vsAccounts.TextMatrix(counter, AccountCol) != "")
				{
					if (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, NumberCol)) != 0)
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE ID = " + vsAccounts.TextMatrix(counter, NumberCol));
						rsSave.Edit();
					}
					else
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE ID = 0");
						rsSave.AddNew();
					}
					rsSave.Set_Fields("BankNumber", lngBankNumber);
					rsSave.Set_Fields("Account", vsAccounts.TextMatrix(counter, AccountCol));
					rsSave.Set_Fields("AllSuffix", vsAccounts.TextMatrix(counter, AllSuffixCol));
					rsSave.Set_Fields("Interest", false);
					rsSave.Update();
				}
			}
			for (counter = 1; counter <= vsInterest.Rows - 1; counter++)
			{
				if (vsInterest.TextMatrix(counter, AccountCol) != "")
				{
					if (FCConvert.ToDouble(vsInterest.TextMatrix(counter, NumberCol)) != 0)
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE ID = " + vsInterest.TextMatrix(counter, NumberCol));
						rsSave.Edit();
					}
					else
					{
						rsSave.OpenRecordset("SELECT * FROM BankCashAccounts WHERE ID = 0");
						rsSave.AddNew();
					}
					rsSave.Set_Fields("BankNumber", lngBankNumber);
					rsSave.Set_Fields("Account", vsInterest.TextMatrix(counter, AccountCol));
					rsSave.Set_Fields("Interest", true);
					rsSave.Update();
				}
			}
			MessageBox.Show("Accounts Saved", "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
		}

		private void vsAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsAccounts.Col == AccountCol)
			{
				vsAccounts.EditMaxLength = 23;
			}
		}

		private void vsAccounts_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsAccounts.IsCurrentCellInEditMode)
			{
				if (vsAccounts.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsAccounts.EditText);
				}
			}
		}

		private void vsAccounts_Enter(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
                modNewAccountBox.SetGridFormat(vsAccounts, vsAccounts.Row, vsAccounts.Col, false, "", "G");
			}
			else
			{
				EditFlag = false;
			}
		}

		private void vsAccounts_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			//if (vsAccounts.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F12)
			//{
			//	string temp = vsAccounts.EditText;
			//	int selStart = vsAccounts.EditSelStart;
			//	modNewAccountBox.CheckKeyDownEditF2(vsAccounts, vsAccounts.Row, vsAccounts.Col, KeyCode, FCConvert.ToInt32(e.Shift), vsAccounts.EditSelStart, ref temp, vsAccounts.EditSelLength);
			//	vsAccounts.EditText = temp;
			//	vsAccounts.EditSelStart = selStart;
			//}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vsAccounts.Col < AllSuffixCol)
				{
					vsAccounts.Col += 1;
				}
				else
				{
					if (vsAccounts.Row < vsAccounts.Rows - 1)
					{
						vsAccounts.Row += 1;
						vsAccounts.Col = AccountCol;
					}
					else
					{
						vsAccounts.AddItem("");
						vsAccounts.TextMatrix(vsAccounts.Row + 1, NumberCol, FCConvert.ToString(0));
						vsAccounts.TextMatrix(vsAccounts.Row + 1, AccountCol, "");
						vsAccounts.TextMatrix(vsAccounts.Row + 1, AllSuffixCol, FCConvert.ToString(false));
						vsAccounts.Row += 1;
					}
				}
			}
		}

		private void vsAccounts_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			else if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
				//FC:FINAL:AM:#i735 - in VB6 KeyPressEdit is not fired for the TAB key
				else if (keyAscii == 9)//TAB
			{
			}
			else
			{
				if (vsAccounts.Col == AccountCol)
				{
					//modNewAccountBox.CheckAccountKeyPress(vsAccounts, vsAccounts.Row, vsAccounts.Col, FCConvert.ToInt16keyAscii, vsAccounts.EditSelStart, vsAccounts.EditText);
				}
			}
		}

		private void vsAccounts_KeyUpEdit(object sender, KeyEventArgs e)
		{
            Keys KeyCode = e.KeyCode;
            if (vsAccounts.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F12)
            {
                string temp = vsAccounts.EditText;
                int selStart = vsAccounts.EditSelStart;
                modNewAccountBox.CheckKeyDownEditF2(vsAccounts, vsAccounts.Row, vsAccounts.Col, KeyCode, FCConvert.ToInt32(e.Shift), vsAccounts.EditSelStart, ref temp, vsAccounts.EditSelLength);
                vsAccounts.EditText = temp;
                vsAccounts.EditSelStart = selStart;
            }
   //         Keys KeyCode = e.KeyCode;
			//if (vsAccounts.Col == AccountCol && KeyCode != Keys.F12)
			//{
			//	if (FCConvert.ToInt32(KeyCode) != 40 && FCConvert.ToInt32(KeyCode) != 38)
			//	{
			//		// up and down arrows
			//		//modNewAccountBox.CheckAccountKeyCode(vsAccounts, vsAccounts.Row, vsAccounts.Col, FCConvert.ToInt16(KeyCode), 0, vsAccounts.EditSelStart, vsAccounts.EditText, vsAccounts.EditSelLength);
			//	}
			//}
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				// if we want the event fired
				lblExpense.Text = modAccountTitle.ReturnAccountDescription(vsAccounts.TextMatrix(vsAccounts.Row, AccountCol));
				if (vsAccounts.Row > 0)
				{
					// check to see if all the rows have been deleted
					if (vsAccounts.Col == AccountCol)
					{
						// if we are in the first column
                        modNewAccountBox.SetGridFormat(vsAccounts, vsAccounts.Row, vsAccounts.Col, true, "", "G");
						vsAccounts.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
					}
				}
			}
		}

		private void vsAccounts_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsAccounts.Col == AllSuffixCol)
			{
				if (vsAccounts.Row > 0)
				{
					if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol)) == false)
					{
						vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol, FCConvert.ToString(true));
					}
					else
					{
						vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol, FCConvert.ToString(false));
					}
				}
			}
			else
			{
				if (vsAccounts.MouseRow > 0)
				{
					vsAccounts_RowColChange(sender, e);
				}
			}
		}

		private void vsAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsAccounts.Col == AllSuffixCol)
			{
				if (KeyCode == Keys.Space)
				{
					if (vsAccounts.Row > 0)
					{
						KeyCode = 0;
						if (FCUtils.CBool(vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol)) == false)
						{
							vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol, FCConvert.ToString(true));
						}
						else
						{
							vsAccounts.TextMatrix(vsAccounts.Row, AllSuffixCol, FCConvert.ToString(false));
						}
					}
				}
			}
		}

		private void vsAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsAccounts.GetFlexRowIndex(e.RowIndex);
			int col = vsAccounts.GetFlexColIndex(e.ColumnIndex);
			if (col == AccountCol)
			{
				e.Cancel = modNewAccountBox.CheckAccountValidate(vsAccounts, row, col, e.Cancel);
			}
		}

		private void vsInterest_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsInterest.Col == AccountCol)
			{
				vsInterest.EditMaxLength = 95;
			}
		}

		private void vsInterest_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsInterest.IsCurrentCellInEditMode)
			{
				if (vsInterest.Col == AccountCol)
				{
					lblInterest.Text = modAccountTitle.ReturnAccountDescription(vsInterest.EditText);
				}
			}
		}

		private void vsInterest_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsInterest.MouseRow > 0)
			{
				vsInterest_RowColChange(sender, e);
			}
		}

		private void vsInterest_Enter(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
                modNewAccountBox.SetGridFormat(vsInterest, vsInterest.Row, vsInterest.Col, false, "", "R");
			}
			else
			{
				EditFlag = false;
			}
		}

		private void vsInterest_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			//if (vsInterest.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F12)
			//{
			//	string temp = vsInterest.EditText;
			//	modNewAccountBox.CheckKeyDownEditF2(vsInterest, vsInterest.Row, vsInterest.Col, KeyCode, FCConvert.ToInt32(e.Shift), vsInterest.EditSelStart, ref temp, vsInterest.EditSelLength);
			//	vsInterest.EditText = temp;
			//}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vsInterest.Col < AccountCol)
				{
					vsInterest.Col += 1;
				}
				else
				{
					if (vsInterest.Row < vsInterest.Rows - 1)
					{
						vsInterest.Row += 1;
						vsInterest.Col = AccountCol;
					}
					else
					{
						vsInterest.AddItem("");
						vsInterest.TextMatrix(vsInterest.Row + 1, NumberCol, FCConvert.ToString(0));
						vsInterest.TextMatrix(vsInterest.Row + 1, AccountCol, "");
						vsInterest.Row += 1;
					}
				}
			}
		}

		private void vsInterest_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			else if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			else
			{
				if (vsInterest.Col == AccountCol)
				{
					//modNewAccountBox.CheckAccountKeyPress(vsInterest, vsInterest.Row, vsInterest.Col, FCConvert.ToInt16keyAscii, vsInterest.EditSelStart, vsInterest.EditText);
				}
			}
		}

		private void vsInterest_KeyUpEdit(object sender, KeyEventArgs e)
		{
            Keys KeyCode = e.KeyCode;
            if (vsInterest.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F12)
            {
                string temp = vsInterest.EditText;
                modNewAccountBox.CheckKeyDownEditF2(vsInterest, vsInterest.Row, vsInterest.Col, KeyCode, FCConvert.ToInt32(e.Shift), vsInterest.EditSelStart, ref temp, vsInterest.EditSelLength);
                vsInterest.EditText = temp;
            }
   //         Keys KeyCode = e.KeyCode;
			//if (vsInterest.Col == AccountCol && KeyCode != Keys.F12)
			//{
			//	if (FCConvert.ToInt32(KeyCode) != 40 && FCConvert.ToInt32(KeyCode) != 38)
			//	{
			//		// up and down arrows
			//		//modNewAccountBox.CheckAccountKeyCode(vsInterest, vsInterest.Row, vsInterest.Col, FCConvert.ToInt16(KeyCode), 0, vsInterest.EditSelStart, vsInterest.EditText, vsInterest.EditSelLength);
			//	}
			//}
		}

		private void vsInterest_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				// if we want the event fired
				lblInterest.Text = modAccountTitle.ReturnAccountDescription(vsInterest.TextMatrix(vsInterest.Row, AccountCol));
				if (vsInterest.Row > 0)
				{
					// check to see if all the rows have been deleted
					if (vsInterest.Col == AccountCol)
					{
						// if we are in the first column
                        modNewAccountBox.SetGridFormat(vsInterest, vsInterest.Row, vsInterest.Col, true, "", "R");
						vsInterest.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
					}
				}
			}
		}

		private void vsInterest_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsInterest.GetFlexRowIndex(e.RowIndex);
			int col = vsInterest.GetFlexColIndex(e.ColumnIndex);
			if (col == AccountCol)
			{
				e.Cancel = modNewAccountBox.CheckAccountValidate(vsInterest, row, col, e.Cancel);
			}
		}
		//FC:FINAL:AM:#i735 - attach event handlers
		private void vsAccounts_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyDown -= new KeyEventHandler(vsAccounts_KeyDownEdit);
				e.Control.KeyPress -= new KeyPressEventHandler(vsAccounts_KeyPressEdit);
				e.Control.KeyUp -= new KeyEventHandler(vsAccounts_KeyUpEdit);
				e.Control.Appear -= Control_Appear;
				e.Control.KeyDown += new KeyEventHandler(vsAccounts_KeyDownEdit);
				e.Control.KeyPress += new KeyPressEventHandler(vsAccounts_KeyPressEdit);
				e.Control.KeyUp += new KeyEventHandler(vsAccounts_KeyUpEdit);
				e.Control.Appear += Control_Appear;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void vsInterest_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.KeyDown -= new KeyEventHandler(vsInterest_KeyDownEdit);
				e.Control.KeyPress -= new KeyPressEventHandler(vsInterest_KeyPressEdit);
				e.Control.KeyUp -= new KeyEventHandler(vsInterest_KeyUpEdit);
				e.Control.Appear -= Control_Appear;
				e.Control.KeyDown += new KeyEventHandler(vsInterest_KeyDownEdit);
				e.Control.KeyPress += new KeyPressEventHandler(vsInterest_KeyPressEdit);
				e.Control.KeyUp += new KeyEventHandler(vsInterest_KeyUpEdit);
				e.Control.Appear += Control_Appear;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void Control_Appear(object sender, EventArgs e)
		{
			var editor = sender as TextBox;
			if (editor != null)
			{
				editor.SelectionStart = 0;
				editor.SelectionLength = 1;
			}
		}
	}
}
