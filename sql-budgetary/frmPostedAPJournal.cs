﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPostedAPJournal.
	/// </summary>
	public partial class frmPostedAPJournal : BaseForm
	{
		public frmPostedAPJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			NumberCol = 0;
			DescriptionCol = 1;
			AccountCol = 2;
			TaxCol = 3;
			// initialize columns
			ProjectCol = 4;
			AmountCol = 5;
			DiscountCol = 6;
			EncumbranceCol = 7;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPostedAPJournal InstancePtr
		{
			get
			{
				return (frmPostedAPJournal)Sys.GetInstance(typeof(frmPostedAPJournal));
			}
		}

		protected frmPostedAPJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int DescriptionCol;
		int AccountCol;
		int TaxCol;
		int ProjectCol;
		// column variables for flexgrid
		int AmountCol;
		int DiscountCol;
		int EncumbranceCol;
		bool SearchFlag;
		// flag that lets the form know we are searching for a vendor
		// vbPorter upgrade warning: TotalAmount As double	OnWrite(short, Decimal)
		double TotalAmount;
		// vbPorter upgrade warning: TotalDiscount As double	OnWrite(short, Decimal)
		double TotalDiscount;
		// keeps track of money
		// vbPorter upgrade warning: AmountToPay As double	OnWrite(Decimal, short)
		double AmountToPay;
		bool EditFlag;
		public int ChosenEncumbrance;
		public int ChosenPurchaseOrder;
		bool GotEncData;
		// flag to know if we have encummbrance data
		bool GotPOData;
		string ErrorString = "";
		// Error message to show if account is invalid
		bool BadAccountFlag;
		// flag to knwo whetyher the account was good or bad
		int CurrJournal;
		// Which journal we are wokring in
		public int VendorNumber;
		public bool OldJournal;
		// variables to keep track of which record we are looking at if we searched for certain ones
		public int FirstRecordShown;
		public bool FromVendorMaster;
		public int NumberOfEncumbrances;
		public int OldJournalNumber;
		public bool EncumbranceFlag;
		public bool PurchaseOrderFlag;
		bool DeleteFlag;
		int TempEncAccount;
		bool JournalFlag;
		bool JournalNameFlag;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper Master = new clsDRWrapper();
		clsDRWrapper Master_AutoInitialized;

		clsDRWrapper Master
		{
			get
			{
				if (Master_AutoInitialized == null)
				{
					Master_AutoInitialized = new clsDRWrapper();
				}
				return Master_AutoInitialized;
			}
			set
			{
				Master_AutoInitialized = value;
			}
		}

		public bool blnFromVendor;
		bool blnUnload;
		bool blnClicked;
		bool blnBackedInto;
		DateTime datPayableDate;
		int intSavedPeriod;
		bool blnSaved;
		bool blnMoveToDescription;
		bool blnJournalLocked;
		string strComboList;
		public bool blnJournalEdit;
		DateTime datOldPayableDate;
		DateTime datNewPayableDate;

		clsDRWrapper rsYTDActivity_AutoInitialized;

		clsDRWrapper rsYTDActivity
		{
			get
			{
				if (rsYTDActivity_AutoInitialized == null)
				{
					rsYTDActivity_AutoInitialized = new clsDRWrapper();
				}
				return rsYTDActivity_AutoInitialized;
			}
			set
			{
				rsYTDActivity_AutoInitialized = value;
			}
		}
		// Dim rsSchoolYTDActivity         As New clsDRWrapper
		private bool boolF12;

		private cVendorController vendCont_AutoInitialized;

		private cVendorController vendCont
		{
			get
			{
				if (vendCont_AutoInitialized == null)
				{
					vendCont_AutoInitialized = new cVendorController();
				}
				return vendCont_AutoInitialized;
			}
			set
			{
				vendCont_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEncumbranceController encCont = new cEncumbranceController();
		private cEncumbranceController encCont_AutoInitialized;

		private cEncumbranceController encCont
		{
			get
			{
				if (encCont_AutoInitialized == null)
				{
					encCont_AutoInitialized = new cEncumbranceController();
				}
				return encCont_AutoInitialized;
			}
			set
			{
				encCont_AutoInitialized = value;
			}
		}

		private int lngCurrentInvoiceID;

		private void cboFrequency_Enter(object sender, System.EventArgs e)
		{
			// make sure Vendor data has been input
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAmount.Text = "";
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
			// erase the account title
		}

		private void cboJournal_GotFocus()
		{
			if (txtVendor.Text == "0000" && txtAddress[0].Text == "")
			{
				txtAddress[0].Focus();
			}
			lblExpense.Text = "";
		}

		private void chkSeperate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
			// erase the account title
		}

		private void chkUseAltCashAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkUseAltCashAccount.CheckState == CheckState.Checked)
			{
				fraAltCashAccount.Enabled = true;
			}
			else
			{
				fraAltCashAccount.Enabled = false;
				vsAltCashAccount.TextMatrix(0, 0, "");
			}
		}

		private void cmdBalanceOK_Click(object sender, System.EventArgs e)
		{
			fraAccountBalance.Visible = false;
			vs1.Focus();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			int counter;
			txtSearch.Text = "";
			frmSearch.Visible = false;
			txtVendor.Enabled = true;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = true;
			}
			txtCity.Enabled = true;
			txtState.Enabled = true;
			txtZip.Enabled = true;
			txtZip4.Enabled = true;
			txtPayable.Enabled = true;
			cboFrequency.Enabled = true;
			txtUntil.Enabled = true;
			txtDescription.Enabled = true;
			txtReference.Enabled = true;
			txtAmount.Enabled = true;
			txtCheck.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Text = "";
			// txtVendor.SetFocus
		}

		private void frmPostedAPJournal_Activated(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			//Application.DoEvents();
			if (modGlobal.FormExist(this))
			{
				return;
			}

			vs1.Cols = 8;
			vs1.ColHidden(NumberCol, true);
			//FC:FINAL:DDU:#2904 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, EncumbranceCol, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, TaxCol, vs1.Rows - 1, TaxCol, 4);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, TaxCol, "1099");
			// set column headings
			vs1.TextMatrix(0, ProjectCol, "Proj");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, DiscountCol, "Disc");
			vs1.TextMatrix(0, EncumbranceCol, "Enc");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(DiscountCol, "#,###.00");
			// set column formats
			vs1.ColFormat(EncumbranceCol, "#,###.00");
			vs1.ColComboList(TaxCol, "D|N|1|2|3|4|5|6|7|8|9");
			// "#0;D|#1;N|#2;1|#3;2|#4;3|#5;4|#6;5|#7;6|#8;7|#9;8|#10;9"
			strComboList = "# ; " + "\t" + " |";
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vs1.ColComboList(ProjectCol, strComboList);
			}

			Form_Resize();
			this.Refresh();
		}

		private void frmPostedAPJournal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSALTCASHACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsAltCashAccount, vsAltCashAccount.Row, vsAltCashAccount.Col, KeyCode, Shift, vsAltCashAccount.EditSelStart, vsAltCashAccount.EditText, vsAltCashAccount.EditSelLength);
			}
		}

		private void frmPostedAPJournal_Load(object sender, System.EventArgs e)
		{
			blnJournalLocked = false;
			datPayableDate = DateTime.Today;
			intSavedPeriod = DateTime.Today.Month;
			blnSaved = false;
			modGlobalFunctions.SetTRIOColors(this, false);
			SetCustomFormColors();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			RetrieveInfo();
			this.Refresh();
			frmWait.InstancePtr.Refresh();
		}

		private void frmPostedAPJournal_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2609));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2246));
			vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0619));
			vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0719));
			// resize flexgrid columns
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1287));
			vs1.ColWidth(DiscountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.0967));
			fraAccountBalance.CenterToContainer(this.ClientArea);
			frmSearch.CenterToContainer(this.ClientArea);
			frmInfo.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmPostedAPJournal_Resize(this, new System.EventArgs());
		}

		private void frmPostedAPJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void lblExpense_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			string strAcct = "";
			if (Button == MouseButtonConstants.RightButton)
			{
				strAcct = vs1.TextMatrix(vs1.Row, AccountCol);
				if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(strAcct, 1) != "E" && Strings.Left(strAcct, 1) != "P")
					{
						// do nothing
					}
					else
					{
						lblBalAccount.Text = "Account: " + strAcct;
						lblBalNetBudget.Text = Strings.Format(GetOriginalBudget(ref strAcct) - GetBudgetAdjustments(ref strAcct), "#,##0.00");
						lblBalPostedYTDNet.Text = Strings.Format(GetYTDNet(ref strAcct) + GetEncumbrance(ref strAcct), "#,##0.00");
						lblBalPendingYTDNet.Text = Strings.Format(GetPendingYTDNet(ref strAcct), "#,##0.00");
						lblBalBalance.Text = Strings.Format(FCConvert.ToDouble(lblBalNetBudget.Text) - FCConvert.ToDouble(lblBalPostedYTDNet.Text) - FCConvert.ToDouble(lblBalPendingYTDNet.Text), "#,##0.00");
						fraAccountBalance.Visible = true;
					}
				}
			}
		}

		private void mnuFileDisplayAmount_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsAmount = new clsDRWrapper();
			int lngJournal;
			// vbPorter upgrade warning: curAmount As Decimal	OnWrite(double, short)
			Decimal curAmount;
			lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(lblJournalNumber.Text)));
			rsAmount.OpenRecordset("SELECT SUM(APJournalDetail.Amount) as TotalAmount, SUM(APJournalDetail.Discount) as TotalDiscount FROM APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID WHERE APJournal.JournalNumber = " + FCConvert.ToString(lngJournal), "Budgetary");
			if (!rsAmount.EndOfFile())
			{
				// TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
				curAmount = FCConvert.ToDecimal(rsAmount.Get_Fields_Decimal("TotalAmount") - rsAmount.Get_Fields("TotalDiscount"));
			}
			else
			{
				curAmount = 0;
			}
			MessageBox.Show("The total amount for journal " + modValidateAccount.GetFormat_6(FCConvert.ToString(lngJournal), 4) + " is " + Strings.Format(curAmount, "#,##0.00"), "Pending Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuFileVendorDetail_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtVendor.Text) > 0)
			{
				rptVendorDetail.InstancePtr.blnFromAP = true;
				rptVendorDetail.InstancePtr.lngVendor = FCConvert.ToInt32(Math.Round(Conversion.Val(txtVendor.Text)));
				rptVendorDetail.InstancePtr.Init("1", "A", "A", false, this.Modal);
			}
			else
			{
				if (txtVendor.Text == "0000" && Strings.Trim(txtAddress[0].Text) != "")
				{
					rptVendorDetail.InstancePtr.blnFromAP = true;
					rptVendorDetail.InstancePtr.lngVendor = FCConvert.ToInt32(Math.Round(Conversion.Val(txtVendor.Text)));
					rptVendorDetail.InstancePtr.strVendor = Strings.Trim(txtAddress[0].Text);
					rptVendorDetail.InstancePtr.Init("1", "A", "A", false, this.Modal);
				}
				else
				{
					MessageBox.Show("You must select a vendor before you may use this option.", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuProcessDelete_Click()
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			int counter;
			int temp = 0;
			// if the row has no data in it then do nothing
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, TaxCol) == "" && vs1.TextMatrix(vs1.Row, ProjectCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0" && vs1.TextMatrix(vs1.Row, DiscountCol) == "0" && vs1.TextMatrix(vs1.Row, EncumbranceCol) == "0")
			{
				// do nothing
			}
			else
			{
				// if the row is not empty then highlight the row
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, EncumbranceCol, vs1.Row, NumberCol);
				// make sure they want to delete this item
				counter = FCConvert.ToInt32(MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
				// if they do
				if (counter == FCConvert.ToInt32(DialogResult.Yes))
				{
					// set the delete flag to true and remember the row to delete
					DeleteFlag = true;
					temp = vs1.Row;
					// move the cursor so everythig gets deleted
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					// if we are deleting an encumbrance
					if (EncumbranceFlag)
					{
						if (temp <= NumberOfEncumbrances)
						{
							// delete the encumbrance from our encumbrance array
							for (counter = temp - 1; counter <= NumberOfEncumbrances - 2; counter++)
							{
								modBudgetaryMaster.Statics.EncumbranceDetail[counter] = modBudgetaryMaster.Statics.EncumbranceDetail[counter + 1];
								modBudgetaryMaster.Statics.EncumbranceDetailRecords[counter] = modBudgetaryMaster.Statics.EncumbranceDetailRecords[counter + 1];
							}
							// change the size of the array and decrement how many encumbrances we have
							Array.Resize(ref modBudgetaryMaster.Statics.EncumbranceDetail, NumberOfEncumbrances - 1 + 1);
							Array.Resize(ref modBudgetaryMaster.Statics.EncumbranceDetailRecords, NumberOfEncumbrances - 1 + 1);
							NumberOfEncumbrances -= 1;
						}
					}
					// delete the row and add a new opne to the bottom of the grid
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, EncumbranceCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, DiscountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Rows - 1, TaxCol, 4);
					//FC:FINAL:DDU:#2904 - aligned columns
					vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					// recalculate the totals then show amount left to pay
					CalculateTotals();
					lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
					lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
					// reset the delete flag and turn off highlighitng
					DeleteFlag = false;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					// set control back to cell we were at
					vs1.Select(temp, DescriptionCol);
					// if they dont want to then set ocntrol back to cell we were at
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(vs1.Row, DescriptionCol);
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuViewAttachedDocuments_Click(object sender, System.EventArgs e)
        {
            var lngInvoiceNumber = lngCurrentInvoiceID;

            if (lngInvoiceNumber > 0)
			{				
                StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer(
                    "Invoice " + FCConvert.ToString(lngInvoiceNumber), "Budgetary", "APJournal",
                    lngInvoiceNumber, "", false, true));
            }
			else
			{
				MessageBox.Show("You must save this entry before you can attach a document to it.", "Entry Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
        }
		private void CalculateTotals(bool blnBeforeEdit = false)
		{
			int counter;
			// vbPorter upgrade warning: TempMoney As Decimal	OnWrite(short, string)
			Decimal TempMoney = 0;
			int Col = 0;
			if (blnBeforeEdit)
			{
				Col = vs1.Col - 1;
			}
			else
			{
				Col = vs1.Col;
				if (vs1.EditText == "" || Col < AmountCol)
				{
					TempMoney = 0;
				}
				else
				{
					TempMoney = FCConvert.ToDecimal(vs1.EditText);
				}
			}
			TotalAmount = 0;
			TotalDiscount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (!blnBeforeEdit)
				{
					if (counter == vs1.Row)
					{
						if (Col == AmountCol)
						{
							if (FCConvert.ToDouble(TempMoney) >= 0)
							{
								TotalAmount += FCConvert.ToDouble(TempMoney);
								TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol));
							}
							else
							{
								TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol)) + (FCConvert.ToDouble(TempMoney) * -1);
							}
						}
						else if (Col == DiscountCol)
						{
							if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) >= 0)
							{
								TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
								TotalDiscount += FCConvert.ToDouble(TempMoney);
							}
							else
							{
								TotalDiscount += FCConvert.ToDouble(TempMoney) + (FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
							}
						}
						else
						{
							if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) >= 0)
							{
								TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
								TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol));
							}
							else
							{
								TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol)) + (FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
							}
						}
					}
					else
					{
						if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) >= 0)
						{
							TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
							TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol));
						}
						else
						{
							TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol)) + (FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
						}
					}
				}
				else
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) >= 0)
					{
						TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
						TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol));
					}
					else
					{
						TotalDiscount += FCConvert.ToDouble(vs1.TextMatrix(counter, DiscountCol)) + (FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol)) * -1);
					}
				}
			}
		}

		private void GetJournalData()
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			// get information to show on the accounts payable screen
			// txtVendor.Text = GetFormat(SearchResults.Fields["VendorNumber"], 5)
			if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber")) == 0)
			{
				txtAddress[0].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorName"));
				txtAddress[1].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress1"));
				txtAddress[2].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress2"));
				txtAddress[3].Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorAddress3"));
				txtCity.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorCity"));
				txtState.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorState"));
				txtZip.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip"));
				txtZip4.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("TempVendorZip4"));
			}
			else
			{
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
				txtAddress[0].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"));
				txtAddress[1].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"));
				txtAddress[2].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2"));
				txtAddress[3].Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3"));
				txtCity.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckCity"));
				txtState.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"));
				txtZip.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"));
				txtZip4.Text = FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"));
			}
			if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Payable"))
			{
				//FC:FINAL:MSH - Operator '==' can't be applied to types DateTime and double (same with issue #698)
				//if (blnJournalEdit && modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Payable") == datOldPayableDate.ToOADate())
				if (blnJournalEdit && datOldPayableDate.CompareTo(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable")) == 0)
				{
					txtPayable.Text = Strings.Format(datNewPayableDate, "MM/dd/yyyy");
				}
				else
				{
					txtPayable.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable"));
				}
			}
			else
			{
				//FC:FINAL:MSH - Operator '==' can't be applied to types DateTime and double (same with issue #698)
				//if (blnJournalEdit && modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Payable") == datOldPayableDate.ToOADate())
				if (blnJournalEdit && datOldPayableDate.CompareTo(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Payable")) == 0)
				{
					txtPayable.Text = FCConvert.ToString(datNewPayableDate);
				}
			}
			if (!modBudgetaryAccounting.Statics.SearchResults.IsFieldNull("Until"))
			{
				txtUntil.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("Until"), "MM/dd/yyyy");
			}
			else
			{
				txtUntil.Text = "";
			}
			// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Frequency")) != "")
			{
				// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
				cboFrequency.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Frequency"));
			}
			else
			{
				cboFrequency.SelectedIndex = -1;
			}
			txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			txtAmount.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount"), "#,##0.00");
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period") != null)
				txtPeriod.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"));
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Reference") != null)
			{
				txtReference.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Reference"));
			}
			else
			{
				txtReference.Text = "";
			}
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("CheckNumber") != null)
			{
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				txtCheck.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("CheckNumber"));
				if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("UseAlternateCash")))
				{
					chkUseAltCashAccount.CheckState = CheckState.Checked;
					fraAltCashAccount.Enabled = true;
					vsAltCashAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("AlternateCashAccount")));
				}
				else
				{
					chkUseAltCashAccount.CheckState = CheckState.Unchecked;
					vsAltCashAccount.TextMatrix(0, 0, "");
					fraAltCashAccount.Enabled = false;
				}
			}
			else
			{
				txtCheck.Text = "";
				chkUseAltCashAccount.CheckState = CheckState.Unchecked;
				vsAltCashAccount.TextMatrix(0, 0, "");
				fraAltCashAccount.Enabled = false;
			}
			chkSeperate.CheckState = (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("Seperate") == true ? CheckState.Checked : CheckState.Unchecked);
			VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
			if (Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord")) != 0)
			{
				ChosenEncumbrance = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord"));
			}
			else
			{
				ChosenEncumbrance = 0;
			}
			if (Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID")) != 0)
			{
				ChosenPurchaseOrder = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID"));
			}
			else
			{
				ChosenPurchaseOrder = 0;
			}
			GetJournalDetails();
			AmountToPay = FCConvert.ToDouble(FCConvert.ToDecimal(txtAmount.Text));
			CalculateTotals();
			lblActualPayment.Text = Strings.Format(TotalAmount - TotalDiscount, "#,##0.00");
			lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
			SetReadOnly();
		}

		public void SetReadOnly(cAPJournal aJourn = null)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			int counter;
			bool boolReadOnly;
			boolReadOnly = false;
			lblReadOnly.Visible = true;
			txtPeriod.Enabled = false;
			txtVendor.Enabled = false;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = false;
			}
			txtCity.Enabled = false;
			txtState.Enabled = false;
			txtZip.Enabled = false;
			txtZip4.Enabled = false;
			txtPayable.Enabled = false;
			cboFrequency.Enabled = false;
			txtUntil.Enabled = false;
			txtDescription.Enabled = false;
			txtReference.Enabled = false;
			txtAmount.Enabled = false;
			txtCheck.Enabled = false;
		}

		private void ClearData()
		{
			int counter;
			txtVendor.Text = "";
			// blank the screen
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Text = "";
			}
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			txtAmount.Text = "";
			txtDescription.Text = "";
			txtReference.Text = "";
			txtCheck.Text = "";
			chkUseAltCashAccount.CheckState = CheckState.Unchecked;
			vsAltCashAccount.TextMatrix(0, 0, "");
			fraAltCashAccount.Enabled = false;			
			VendorNumber = 0;
			AmountToPay = 0;
			NumberOfEncumbrances = 0;
			FCUtils.EraseSafe(modBudgetaryMaster.Statics.EncumbranceDetail);
			ChosenEncumbrance = 0;
			ChosenPurchaseOrder = 0;
			txtPayable.Text = Strings.Format(datPayableDate, "MM/dd/yyyy");
			txtUntil.Text = "";
			cboFrequency.SelectedIndex = -1;
			vs1.Clear();
			txtPeriod.Text = FCConvert.ToString(0);
			chkSeperate.CheckState = CheckState.Unchecked;
			vs1.Rows = 16;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
			}
			// reformat the grid so it is ready for new information
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, EncumbranceCol, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, TaxCol, vs1.Rows - 1, TaxCol, 4);
			//FC:FINAL:DDU:#2904 - aligned columns
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(DiscountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(EncumbranceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, TaxCol, "1099");
			vs1.TextMatrix(0, ProjectCol, "Proj");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, DiscountCol, "Disc");
			vs1.TextMatrix(0, EncumbranceCol, "Enc");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(DiscountCol, "#,###.00");
			vs1.ColFormat(EncumbranceCol, "#,###.00");
			vs1.Select(1, 1);
			lblExpense.Text = "";
			lblRemAmount.Text = "0.00";
			lblActualPayment.Text = "0.00";
			AmountToPay = 0;
			TotalAmount = 0;
			TotalDiscount = 0;
		}

		private void GetJournalDetails()
		{
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			int ACounter = 0;
			clsDRWrapper rs3 = new clsDRWrapper();
			// get the detail infgormation to show on the screen
			rs3.OpenRecordset("SELECT EncumbranceDetail.Account AS Account, EncumbranceDetail.Amount AS Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated AS Liquidated, EncumbranceDetail.ID AS VendorNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.ID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EncumbranceRecord") + " ORDER BY EncumbranceDetail.ID");
			if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
			{
				rs3.MoveLast();
				rs3.MoveFirst();
				EncumbranceFlag = true;
				modBudgetaryMaster.Statics.EncumbranceDetail = new double[rs3.RecordCount() + 1];
				modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[rs3.RecordCount() + 1];
			}
			else
			{
				EncumbranceFlag = false;
			}
			if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("PurchaseOrderID")) != 0)
			{
				PurchaseOrderFlag = true;
			}
			else
			{
				PurchaseOrderFlag = false;
			}
			rs2.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				if (rs2.RecordCount() > 15)
				{
					vs1.Rows = rs2.RecordCount() + 1;
				}
				else
				{
					vs1.Rows = 16;
				}
				counter = 1;
				ACounter = 0;
				NumberOfEncumbrances = 0;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("account")));
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields("1099")));
					vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields("Amount")));
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Discount")));
					vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
					vs1.RowData(counter, rs2.Get_Fields_Int32("PurchaseOrderDetailsID"));
					if (rs2.Get_Fields_Decimal("Encumbrance") > 0)
					{
						for (counter2 = 1; counter2 <= rs3.RecordCount(); counter2++)
						{
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
							if (rs3.Get_Fields("account") == rs2.Get_Fields("account"))
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								modBudgetaryMaster.Statics.EncumbranceDetail[ACounter] = rs3.Get_Fields("Amount") + rs3.Get_Fields_Decimal("Adjustments") - (rs3.Get_Fields_Decimal("Liquidated") - rs2.Get_Fields_Decimal("Encumbrance"));
								modBudgetaryMaster.Statics.EncumbranceDetailRecords[ACounter] = FCConvert.ToInt32(rs3.Get_Fields_Int32("EncumbranceID"));
								ACounter += 1;
								NumberOfEncumbrances += 1;
								rs3.MoveFirst();
								break;
							}
							else
							{
								rs3.MoveNext();
							}
						}
					}
					// get labels
					rs2.MoveNext();
					counter += 1;
				}
				// blank the rest of the grid
				if (counter < vs1.Rows - 1)
				{
					for (counter = counter; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 1, "");
						vs1.TextMatrix(counter, 2, "");
						vs1.TextMatrix(counter, 3, "");
						vs1.TextMatrix(counter, 4, "");
					}
				}
			}
			else
			{
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 1, "");
					vs1.TextMatrix(counter, 2, "");
					vs1.TextMatrix(counter, 3, "");
					vs1.TextMatrix(counter, 4, "");
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
			lblReadOnly.ForeColor = Color.Red;
			lblReadOnly.Font = FCUtils.SetFontStyle(lblReadOnly.Font, FontStyle.Bold, true);
			frmSearch.BackColor = SystemColors.AppWorkspace;
		}

		private double GetOriginalBudget(ref string strAcct)
		{
			double GetOriginalBudget = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					GetOriginalBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
				}
				else
				{
					GetOriginalBudget = 0;
				}
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments(ref string strAcct)
		{
			double GetBudgetAdjustments = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					GetBudgetAdjustments = Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
			}
			return GetBudgetAdjustments;
		}

		private double GetPendingYTDDebit(ref string strAcct)
		{
			double GetPendingYTDDebit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
					GetPendingYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PendingDebitsTotal"));
				}
				else
				{
					GetPendingYTDDebit = 0;
				}
			}
			return GetPendingYTDDebit;
		}

		private double GetPendingYTDCredit(ref string strAcct)
		{
			double GetPendingYTDCredit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
					GetPendingYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PendingCreditsTotal")) * -1;
				}
				else
				{
					GetPendingYTDCredit = 0;
				}
			}
			return GetPendingYTDCredit;
		}

		private double GetPendingYTDNet(ref string strAcct)
		{
			double GetPendingYTDNet = 0;
			GetPendingYTDNet = GetPendingYTDDebit(ref strAcct) - GetPendingYTDCredit(ref strAcct);
			return GetPendingYTDNet;
		}

		private double GetYTDDebit(ref string strAcct)
		{
			double GetYTDDebit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					GetYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit(ref string strAcct)
		{
			double GetYTDCredit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					GetYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet(ref string strAcct)
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit(ref strAcct) - GetYTDCredit(ref strAcct);
			return GetYTDNet;
		}

		private double GetEncumbrance(ref string strAcct)
		{
			double GetEncumbrance = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
					GetEncumbrance = Conversion.Val(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			return GetEncumbrance;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheck = "";
			string strTable = "";
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}
		
		public void Init(ref cAPJournal aJourn)
		{
			if (!(aJourn.ID == 0))
			{
				txtVendor.Text = modValidateAccount.GetFormat_6(aJourn.VendorNumber.ToString(), 5);
				if (aJourn.VendorNumber > 0)
				{
					cVendor vend;
					vend = aJourn.Vendor;
					if (!(vend == null))
					{
						txtAddress[0].Text = vend.CheckName;
						txtAddress[1].Text = vend.CheckAddress1;
						txtAddress[2].Text = vend.CheckAddress2;
						txtAddress[3].Text = vend.CheckAddress3;
						txtCity.Text = vend.CheckCity;
						txtState.Text = vend.CheckState;
						txtZip.Text = vend.CheckZip;
						txtZip4.Text = vend.CheckZip4;
					}
					else
					{
						txtAddress[0].Text = "";
						txtAddress[1].Text = "";
						txtAddress[2].Text = "";
						txtAddress[3].Text = "";
						txtCity.Text = "";
						txtState.Text = "";
						txtZip.Text = "";
						txtZip4.Text = "";
					}
				}
				else
				{
					txtAddress[0].Text = aJourn.TempVendorName;
					txtAddress[1].Text = aJourn.TempVendorAddress1;
					txtAddress[2].Text = aJourn.TempVendorAddress2;
					txtAddress[3].Text = aJourn.TempVendorAddress3;
					txtCity.Text = aJourn.TempVendorCity;
					txtState.Text = aJourn.TempVendorState;
					txtZip.Text = aJourn.TempVendorZip;
					txtZip4.Text = aJourn.TempVendorZip4;
				}
				if (Information.IsDate(aJourn.PayableDate))
				{
					txtPayable.Text = Strings.Format(aJourn.PayableDate, "MM/dd/yyyy");
				}
				else
				{
					txtPayable.Text = "";
				}
				if (Information.IsDate(aJourn.UntilDate))
				{
					txtUntil.Text = Strings.Format(aJourn.UntilDate, "MM/dd/yyyy");
				}
				else
				{
					txtUntil.Text = "";
				}
				if (aJourn.Frequency != "")
				{
					cboFrequency.Text = aJourn.Frequency;
				}
				txtDescription.Text = aJourn.Description;
				txtAmount.Text = Strings.Format(aJourn.Amount, "#,##0.00");
				blnJournalEdit = true;
				if (aJourn.Period > 0)
				{
					txtPeriod.Text = FCConvert.ToString(aJourn.Period);
				}
				txtReference.Text = aJourn.Reference;
				txtCheck.Text = aJourn.CheckNumber;
				if (aJourn.UseAlternateCash)
				{
					chkUseAltCashAccount.CheckState = CheckState.Checked;
					fraAltCashAccount.Enabled = true;
					vsAltCashAccount.TextMatrix(0, 0, aJourn.AlternateCashAccount);
				}
				else
				{
					chkUseAltCashAccount.CheckState = CheckState.Unchecked;
					vsAltCashAccount.TextMatrix(0, 0, "");
					fraAltCashAccount.Enabled = false;
				}
				if (aJourn.Separate)
				{
					chkSeperate.CheckState = CheckState.Checked;
				}
				else
				{
					chkSeperate.CheckState = CheckState.Unchecked;
				}
				VendorNumber = aJourn.VendorNumber;
				FirstRecordShown = aJourn.ID;
				ChosenEncumbrance = aJourn.EncumbranceRecord;
				ChosenPurchaseOrder = aJourn.PurchaseOrderID;
				if (aJourn.PurchaseOrderID > 0)
				{
					PurchaseOrderFlag = true;
				}
				modBudgetaryMaster.Statics.blnAPEdit = true;
				modBudgetaryMaster.Statics.CurrentAPEntry = aJourn.JournalNumber;
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				OldJournal = true;
				OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
				lblJournalNumber.Text = FCConvert.ToString(aJourn.JournalNumber);
				lngCurrentInvoiceID = aJourn.ID;
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("select * from apjournal where id = " + aJourn.ID, "Budgetary");
				SetReadOnly(aJourn);
				FillDetails(aJourn.JournalEntries, aJourn.EncumbranceRecord);
			}
			else
			{
				modBudgetaryMaster.Statics.CurrentAPEntry = aJourn.JournalNumber;
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				OldJournal = true;
				OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
				EncumbranceFlag = false;
				modBudgetaryMaster.Statics.blnAPEdit = false;
				PurchaseOrderFlag = false;
				FromVendorMaster = false;
				VendorNumber = 0;
				blnJournalEdit = false;
			}
			this.Show(App.MainForm);
		}

		private void FillDetails(cGenericCollection collDetails, int lngEncumbranceID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				int counter = 0;
				if (!(collDetails == null))
				{
					collDetails.MoveFirst();
					// If collDetails.ItemCount > 15 Then
					vs1.Rows = collDetails.ItemCount() + 1;
					// End If
					int counter2;
					int ACounter = 0;
					ACounter = 0;
					counter = 1;
					NumberOfEncumbrances = 0;
					cAPJournalDetail aDetail;
					cEncumbrance enc;
					cEncumbranceDetail encDetail;
					cGenericCollection collEncDetails = new cGenericCollection();
					enc = encCont.GetEncumbrance(lngEncumbranceID);
					if (!(enc == null))
					{
						if (enc.ID > 0)
						{
							EncumbranceFlag = true;
							collEncDetails = encCont.GetEncumbranceDetails(enc.ID);
							modBudgetaryMaster.Statics.EncumbranceDetail = new double[collEncDetails.ItemCount() + 1];
							modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[collEncDetails.ItemCount() + 1];
						}
					}
					while (collDetails.IsCurrent())
					{
						aDetail = (cAPJournalDetail)collDetails.GetCurrentItem();
						//FC:FINAL:BBE:#i765 - Set the correct number of columns before the data is added to invoid IndexOutOfRange exception
						vs1.Cols = 8;
						vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(aDetail.ID));
						vs1.TextMatrix(counter, DescriptionCol, aDetail.Description);
						vs1.TextMatrix(counter, AccountCol, aDetail.Account);
						vs1.TextMatrix(counter, TaxCol, aDetail.Ten99);
						vs1.TextMatrix(counter, ProjectCol, aDetail.Project);
						vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(aDetail.Amount));
						vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(aDetail.Discount));
						vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(aDetail.Encumbrance));
						if (aDetail.Encumbrance > 0)
						{
							collEncDetails.MoveFirst();
							while (collEncDetails.IsCurrent())
							{
								encDetail = (cEncumbranceDetail)collEncDetails.GetCurrentItem();
								if (encDetail.Account == aDetail.Account)
								{
									modBudgetaryMaster.Statics.EncumbranceDetail[ACounter] = encDetail.Amount + encDetail.Adjustments - encDetail.LiquidatedAmount;
									modBudgetaryMaster.Statics.EncumbranceDetailRecords[ACounter] = encDetail.ID;
									NumberOfEncumbrances += 1;
									ACounter += 1;
									break;
								}
								collEncDetails.MoveNext();
							}
						}
						counter += 1;
						collDetails.MoveNext();
					}
					if (collDetails.ItemCount() + 1 <= vs1.Rows - 1)
					{
						for (counter = collDetails.ItemCount() + 1; counter <= vs1.Rows - 1; counter++)
						{
							vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, DiscountCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
						}
					}
				}
				else
				{
					vs1.Rows = 1;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void fraAltCashAccount_EnabledChanged(object sender, EventArgs e)
		{
			if (!fraAltCashAccount.Enabled)
			{
				vsAltCashAccount.TextMatrix(0, 0, "");
			}
		}
	}
}
