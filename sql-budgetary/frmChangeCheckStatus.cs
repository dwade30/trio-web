﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeCheckStatus.
	/// </summary>
	public partial class frmChangeCheckStatus : BaseForm
	{
		public frmChangeCheckStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.txtCheck.AllowOnlyNumericInput();

		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangeCheckStatus InstancePtr
		{
			get
			{
				return (frmChangeCheckStatus)Sys.GetInstance(typeof(frmChangeCheckStatus));
			}
		}

		protected frmChangeCheckStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/1/02
		// This form will be used by towns to change the status of
		// checks and deposits in the check rec file so it can be
		// calculated correctly in the check rec process
		// ********************************************************
		int RecordCol;
		int BankCol;
		int TypeCol;
		int CheckCol;
		int AmountCol;
		int CodeCol;
		int StatusDateCol;
		int PayeeCol;
		int CheckDateCol;
		double[] dblColPercents = new double[9 + 1];
		bool blnFormatFlag;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdGetInfo_Click(object sender, System.EventArgs e)
		{
			int counter = 0;
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (Strings.Trim(txtCheck.Text) == "")
			{
				MessageBox.Show("You must enter a check number before you may proceed.", "No Check Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				blnFormatFlag = true;
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber >= " + Strings.Trim(txtCheck.Text) + " AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY CheckNumber");
				counter = 1;
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					// If rsInfo.RecordCount >= 50 Then
					vs1.Rows = rsInfo.RecordCount() + 1;
					// End If
					do
					{
						vs1.TextMatrix(counter, RecordCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter, BankCol, FCConvert.ToString(rsInfo.Get_Fields("BankNumber")));
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter, TypeCol, FCConvert.ToString(rsInfo.Get_Fields("Type")));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter, CheckCol, FCConvert.ToString(rsInfo.Get_Fields("CheckNumber")));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(rsInfo.Get_Fields("Amount")));
						vs1.TextMatrix(counter, CodeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Status")));
						vs1.TextMatrix(counter, StatusDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy"));
						vs1.TextMatrix(counter, PayeeCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
						vs1.TextMatrix(counter, CheckDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy"));
						counter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				blnFormatFlag = false;
				fraCheck.Visible = false;
				//lblBank.Visible = true;
				vs1Header.Visible = true;
				vs1.Visible = true;
				vs1.Rows = counter;
				Form_Resize();
				// If vs1.rows > 2 Then
				// vs1.Select 2, CodeCol
				// vs1.Editable = True
				// vs1.EditCell
				// End If
				// Dave 12/14/2006---------------------------------------------------
				intTotalNumberOfControls = 0;
				// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
				FillControlInformationClassFromGrid();
				// This will initialize the old data so we have somethign to compare the new data against when you save
				for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
				{
					clsControlInfo[counter].FillOldValue(this);
				}
				// ---------------------------------------------------------------------
			}
		}

		public void cmdGetInfo_Click()
		{
			cmdGetInfo_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			int counter2;
			string strStatus = "";
			int x;
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Check Rec Update Status Check / Deposit", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClassFromGrid();
			for (x = 0; x <= intTotalNumberOfControls - 1; x++)
			{
				clsControlInfo[x].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = " + vs1.TextMatrix(counter, RecordCol));
				//FC:FINAL:MSH - The 'Status' can be equal { "1", "2", "3", "V", "D" }, so we can simplify this part of code (same with issue #740)
				/*
                if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) < 4)
                {
                    strStatus = Strings.Trim(vs1.TextMatrix(counter, CodeCol));
                }
                else if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) == 4 || Strings.Trim(vs1.TextMatrix(counter, CodeCol)) == "V")
                {
                    strStatus = "V";
                }
                else
                {
                    strStatus = "D";
                }
                */
				strStatus = Strings.Trim(vs1.TextMatrix(counter, CodeCol));
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) != strStatus)
				{
					if (strStatus == "V" || strStatus == "D")
					{
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						modBudgetaryMaster.WriteAuditRecord_8("Check# " + rsInfo.Get_Fields("CheckNumber") + " status set to " + strStatus, "Update Check Status");
					}
					rsInfo.Edit();
					rsInfo.Set_Fields("Status", strStatus);
					rsInfo.Set_Fields("StatusDate", DateTime.Today);
					rsInfo.Update(true);
				}
			}
			Close();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmChangeCheckStatus_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtCheck.Focus();
			this.Refresh();
		}
		// Private Sub Form_Click()
		// cmdProcess.SetFocus
		// End Sub
		private void frmChangeCheckStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmChangeCheckStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangeCheckStatus.FillStyle	= 0;
			//frmChangeCheckStatus.ScaleWidth	= 9480;
			//frmChangeCheckStatus.ScaleHeight	= 7410;
			//frmChangeCheckStatus.LinkTopic	= "Form2";
			//frmChangeCheckStatus.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// define column variables
			RecordCol = 0;
			BankCol = 1;
			TypeCol = 2;
			CheckCol = 3;
			AmountCol = 4;
			CodeCol = 5;
			StatusDateCol = 6;
			PayeeCol = 7;
			CheckDateCol = 8;
			// set alignment of grid
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            // set mergerows for title
            //FC:FINAL:AM:#i683 - use instead a label for the first row
            //vs1.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
            //vs1.MergeRow(0, true);
            // show titles for columns
            //vs1.TextMatrix(0, CodeCol, "--Status--");
            //vs1.TextMatrix(0, StatusDateCol, "--Status--");
            vs1.TextMatrix(0, BankCol, "Bank");
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, CheckCol, "Check");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, CodeCol, "Code");
			vs1.TextMatrix(0, StatusDateCol, "Date");
			vs1.TextMatrix(0, PayeeCol, "Payee");
			vs1.TextMatrix(0, CheckDateCol, "Date");
			// set format type for columns
			vs1.ColFormat(AmountCol, "#,###.00");
			// set up editmask for date columns
			vs1.ColEditMask(StatusDateCol, "00/00/00");
			// set up column data types to make sure sort works correctly
			vs1.ColDataType(StatusDateCol, FCGrid.DataTypeSettings.flexDTDate);
			vs1.ColDataType(CheckDateCol, FCGrid.DataTypeSettings.flexDTDate);
			// set up column sizes
			vs1.ColWidth(RecordCol, 0);
			vs1.ColWidth(BankCol, 500);
			vs1.ColWidth(TypeCol, 500);
			vs1.ColWidth(CheckCol, 800);
			vs1.ColWidth(AmountCol, 1200);
			vs1.ColWidth(CodeCol, 600);
			vs1.ColWidth(StatusDateCol, 800);
			vs1.ColWidth(PayeeCol, 3400);
			vs1.ColWidth(CheckDateCol, 800);
			dblColPercents[0] = 0;
			dblColPercents[1] = 0.0556179;
			dblColPercents[2] = 0.0556179;
			dblColPercents[3] = 0.0893258;
			dblColPercents[4] = 0.1348314;
			dblColPercents[5] = 0.0674157;
			dblColPercents[6] = 0.0893258;
			dblColPercents[7] = 0.3825842;
			dblColPercents[8] = 0.0893258;
			// set up combo lists for type and code columns
			vs1.ColComboList(CodeCol, "#1;1" + "\t" + "Issued|#2;2" + "\t" + "Outstanding|#3;3" + "\t" + "Cashed / Cleared|#4;V" + "\t" + "Voided|#5;D" + "\t" + "Deleted");
			blnFormatFlag = true;
			// show a border between the titles and the data input section of the grid
			vs1.Select(1, 0, 1, vs1.Cols - 1);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			blnFormatFlag = false;
			//lblBank.Text = App.MainForm.StatusBarText2;
			HeaderText.Text = App.MainForm.StatusBarText3;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmChangeCheckStatus_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 8; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPercents[counter]));
			}
			//FC:FINAL:AM: set the location of the status label
			vs1HeaderText.Left = vs1.Columns[0].Width + vs1.Columns[1].Width + vs1.Columns[2].Width + vs1.Columns[3].Width + vs1.Columns[4].Width;
			//FC:FINAL:ASZ - use anchors: 
			//vs1.Height = vs1.RowHeight(0) * 17 + 75;
		}

		public void Form_Resize()
		{
			frmChangeCheckStatus_Resize(this, new System.EventArgs());
		}

		private void frmChangeCheckStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (vs1.Row < vs1.Rows - 1)
				{
					vs1.Row += 1;
				}
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (fraCheck.Visible)
			{
				cmdProcessSave.Text = "Save";
				cmdProcessSave.Focus();
				cmdGetInfo_Click();
				//FC:FINAL:DDU:#3010 - changed location of button where needed
				if (vs1.Visible == true)
				{
					this.BottomPanel.Controls.Add(cmdProcessSave);
					cmdProcessSave.Location = new Point(0, 30);
					base.CenterBottomControls();
				}
			}
			else
			{
				cmdProcessSave.Focus();
				cmdProcess_Click();
			}
		}

		private void txtCheck_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			vs1.Col = CodeCol;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				Support.SendKeys("{TAB}", false);
				return;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (!blnFormatFlag)
			{
				vs1.Col = CodeCol;
				vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void SetCustomFormColors()
		{
			//lblBank.ForeColor = Color.Blue;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			int lngRows;
			int intCols;
			for (lngRows = 1; lngRows <= vs1.Rows - 1; lngRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				//FC:FINAL:ASZ: vb6 autocreate instance
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "vs1";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = lngRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = CodeCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Check: " + vs1.TextMatrix(lngRows, CheckCol) + "  Date: " + vs1.TextMatrix(lngRows, CheckDateCol) + "  Payee: " + vs1.TextMatrix(lngRows, PayeeCol) + "  Amount: " + vs1.TextMatrix(lngRows, AmountCol) + " Status";
				intTotalNumberOfControls += 1;
			}
		}
	}
}
