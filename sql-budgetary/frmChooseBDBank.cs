﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChooseBDBank.
	/// </summary>
	public partial class frmChooseBDBank : BaseForm
	{
		public frmChooseBDBank()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngBankID;
		private int lngDefaultBank;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBankService bankServ = new cBankService();
		private cBankService bankServ_AutoInitialized;

		private cBankService bankServ
		{
			get
			{
				if (bankServ_AutoInitialized == null)
				{
					bankServ_AutoInitialized = new cBankService();
				}
				return bankServ_AutoInitialized;
			}
			set
			{
				bankServ_AutoInitialized = value;
			}
		}

		private cGenericCollection bankList;

		public int DefaultBank
		{
			set
			{
				lngDefaultBank = value;
			}
			get
			{
				int DefaultBank = 0;
				DefaultBank = lngDefaultBank;
				return DefaultBank;
			}
		}

		public int Init(int lngDefaultID)
		{
			int Init = 0;
			lngDefaultBank = lngDefaultID;
			lngBankID = 0;
			bankList = bankServ.GetBanks();
			if (bankList.ItemCount() <= 1)
			{
				if (bankList.ItemCount() > 0)
				{
					cBank bk;
					bankList.MoveFirst();
					bk = (cBank)bankList.GetCurrentItem();
					lngBankID = bk.ID;
				}
			}
			else
			{
				this.Show(FormShowEnum.Modal);
			}
			Init = lngBankID;
			return Init;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			SetBankAndExit();
		}

		private void SetBankAndExit()
		{
			SetBank();
			if (lngBankID > 0)
			{
				Close();
			}
			else
			{
				MessageBox.Show("You must choose a bank first", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void frmChooseBDBank_Activated(object sender, System.EventArgs e)
		{
			if (lngDefaultBank > 0 && cmbBank.SelectedIndex < 0)
			{
				SelectBank(lngDefaultBank);
			}
		}

		private void SelectBank(int lngId)
		{
			int x;
			for (x = 0; x <= cmbBank.Items.Count - 1; x++)
			{
				if (cmbBank.ItemData(x) == lngId)
				{
					cmbBank.SelectedIndex = x;
					break;
				}
			}
		}

		private void frmChooseBDBank_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseBDBank_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseBDBank.FillStyle	= 0;
			//frmChooseBDBank.ScaleWidth	= 3885;
			//frmChooseBDBank.ScaleHeight	= 2445;
			//frmChooseBDBank.LinkTopic	= "Form2";
			//frmChooseBDBank.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillBankCombo(ref bankList);
		}

		private void FillBankCombo(ref cGenericCollection bankList)
		{
			cmbBank.Clear();
			bankList.MoveFirst();
			cBank bk;
			while (bankList.IsCurrent())
			{
				bk = (cBank)bankList.GetCurrentItem();
				cmbBank.AddItem(bk.Name);
				cmbBank.ItemData(cmbBank.NewIndex, bk.ID);
				bankList.MoveNext();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			SetBankAndExit();
		}

		private void SetBank()
		{
			if (cmbBank.SelectedIndex >= 0)
			{
				lngBankID = cmbBank.ItemData(cmbBank.SelectedIndex);
			}
		}
	}
}
