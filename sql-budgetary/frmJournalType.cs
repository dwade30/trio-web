﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournalType.
	/// </summary>
	public partial class frmJournalType : BaseForm
	{
		public frmJournalType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmJournalType InstancePtr
		{
			get
			{
				return (frmJournalType)Sys.GetInstance(typeof(frmJournalType));
			}
		}

		protected frmJournalType _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolCancel;
		private string strReturn = string.Empty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			boolCancel = true;
			strReturn = "";
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int intChoice;
			intChoice = cmbJournals.ItemData(cmbJournals.SelectedIndex);
			switch (intChoice)
			{
				case 1:
					{
						strReturn = "AP";
						break;
					}
				case 2:
					{
						strReturn = "CR";
						break;
					}
				case 3:
					{
						strReturn = "AC";
						break;
					}
				case 4:
					{
						strReturn = "GJ";
						break;
					}
				case 5:
					{
						strReturn = "EN";
						break;
					}
				case 6:
				{
					strReturn = "CD";
					break;
				}
			}
			//end switch
			Close();
		}

		private void frmJournalType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmJournalType_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCombo()
		{
			cmbJournals.Clear();
			cmbJournals.AddItem("Accounts Payable");
			cmbJournals.ItemData(cmbJournals.NewIndex, 1);
			cmbJournals.AddItem("Cash Receipts");
			cmbJournals.ItemData(cmbJournals.NewIndex, 2);
			cmbJournals.AddItem("Cash Disbursements");
			cmbJournals.ItemData(cmbJournals.NewIndex, 6);
			cmbJournals.AddItem("Credit Memo");
			cmbJournals.ItemData(cmbJournals.NewIndex, 3);
			cmbJournals.AddItem("Encumbrance");
			cmbJournals.ItemData(cmbJournals.NewIndex, 5);
			cmbJournals.AddItem("General Journal");
			cmbJournals.ItemData(cmbJournals.NewIndex, 4);
			cmbJournals.SelectedIndex = 0;
		}

		public string Init()
		{
			string Init = "";
			this.Show(FormShowEnum.Modal, fecherFoundation.FCMainForm.InstancePtr);
			//this.ShowDialog();
			if (!boolCancel)
			{
				Init = strReturn;
			}
			else
			{
				Init = "";
			}
			return Init;
		}
	}
}
