﻿using fecherFoundation;
using Global;
using System;
using SharedApplication.Extensions;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptPostedJournal.
	/// </summary>
	public partial class rptPostedJournal : BaseSectionReport
	{
		public static rptPostedJournal InstancePtr
		{
			get
			{
				return (rptPostedJournal)Sys.GetInstance(typeof(rptPostedJournal));
			}
		}

		protected rptPostedJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsDetail.Dispose();
				rsEntry.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPostedJournal	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsDetail = new clsDRWrapper();
		clsDRWrapper rsEntry = new clsDRWrapper();
		bool blnFirstRecord;
        Decimal curTotalDebits;
        Decimal curTotalCredits;
		bool blnLineShown;

		public rptPostedJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Posted Journal Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("PayDateBinder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsDetail.MoveNext();
                if (FCConvert.ToString(rsInfo.Get_Fields("Type")) != "AP" && FCConvert.ToString(rsInfo.Get_Fields("Type")) != "AC" && FCConvert.ToString(rsInfo.Get_Fields("Type")) != "EN")
				{
					rsEntry.MoveNext();
					eArgs.EOF = rsDetail.EndOfFile();
				}
				else
				{
					CheckAgain:
					;
					if (rsDetail.EndOfFile())
					{
						rsEntry.MoveNext();
						CheckNewEntry:
						;
						if (rsEntry.EndOfFile() != true)
						{
                            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
							{
								rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsEntry.Get_Fields_Int32("ID"));
							}
                            else if (rsInfo.Get_Fields("Type") == "EN")
							{
								rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsEntry.Get_Fields_Int32("ID"));
							}
							goto CheckAgain;
						}
						else
						{
                            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP")
							{
								rsInfo.MoveNext();
								if (rsInfo.EndOfFile() != true)
								{
									rsEntry.OpenRecordset("SELECT * FROM APJournal WHERE CheckDate = '" + rsInfo.Get_Fields_DateTime("CheckDate") + "' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
									goto CheckNewEntry;
								}
								else
								{
									eArgs.EOF = true;
								}
							}
							else
							{
								eArgs.EOF = true;
							}
						}
					}
					else
					{
						eArgs.EOF = false;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["PayDateBinder"].Value = rsInfo.Get_Fields_DateTime("StatusChangeDate");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnLineShown = false;
			rsInfo.OpenRecordset("SELECT StatusChangeDate, CheckDate, Type, Description FROM JournalMaster WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " GROUP BY Type, StatusChangeDate, CheckDate, Description ORDER BY Type, StatusChangeDate, CheckDate");
			fldJournal.Text = "Journal - " + Strings.Format(rptMultipleJournals.InstancePtr.UserData, "0000") + " - " + rsInfo.Get_Fields_String("Description");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
				if (!rsInfo.IsFieldNull("CheckDate"))
				{
					rsEntry.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND CheckDate = '" + rsInfo.Get_Fields_DateTime("CheckDate") + "' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
				}
				else
				{
					rsEntry.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
				}
				rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsEntry.Get_Fields_Int32("ID"));
			}
            else if (rsInfo.Get_Fields("Type") == "EN")
			{
				rsEntry.OpenRecordset("SELECT * FROM Encumbrances WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
				rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsEntry.Get_Fields_Int32("ID"));
			}
			else
			{
				rsEntry.OpenRecordset("SELECT * FROM JournalEntries WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
				rsDetail.OpenRecordset("SELECT * FROM JournalEntries WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(rptMultipleJournals.InstancePtr.UserData)) + " ORDER BY ID");
			}
			blnFirstRecord = true;
			if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rptPostedJournal.InstancePtr.Cancel();
				return;
			}
			curTotalDebits = 0;
			curTotalCredits = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            fldPeriod.Text = Strings.Format(rsEntry.Get_Fields("Period"), "00");
            fldType.Text = FCConvert.ToString(rsInfo.Get_Fields("Type"));
			if (FCConvert.ToInt32(rsEntry.Get_Fields_Int32("VendorNumber")) != 0)
			{
				fldVendor.Text = Strings.Format(rsEntry.Get_Fields_Int32("VendorNumber"), "00000");
			}
			else
			{
				fldVendor.Text = "";
			}
			fldDescription.Text = rsDetail.Get_Fields_String("Description");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
                fldCheck.Text = FCConvert.ToString(rsEntry.Get_Fields("CheckNumber"));
				fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				if (FCConvert.ToString(rsDetail.Get_Fields_String("Project")) == "CTRL")
				{
					fldRCB.Text = "L";
				}
				else
				{
					fldRCB.Text = rsDetail.Get_Fields_String("RCB");
				}
			}
            else if (rsInfo.Get_Fields("Type") == "EN")
			{
				fldCheck.Text = "";
				fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("EncumbrancesDate"), "MM/dd/yy");
				if (FCConvert.ToString(rsDetail.Get_Fields_String("Project")) == "CTRL")
				{
					fldRCB.Text = "L";
				}
				else
				{
					fldRCB.Text = "R";
				}
			}
			else
			{
                fldCheck.Text = FCConvert.ToString(rsEntry.Get_Fields("CheckNumber"));
				fldDate.Text = Strings.Format(rsEntry.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yy");
				fldRCB.Text = rsDetail.Get_Fields_String("RCB");
			}
			if (!blnLineShown)
			{
				if (FCConvert.ToString(fldRCB) == "L")
				{
					Line3.Visible = true;
					blnLineShown = true;
				}
				else
				{
					Line3.Visible = false;
				}
			}
			else
			{
				Line3.Visible = false;
			}
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP" || FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AC")
			{
				if (rsDetail.Get_Fields_Decimal("Encumbrance") == 0)
				{
                    if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && rsDetail.Get_Fields_Decimal("Amount") < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && rsDetail.Get_Fields_Decimal("Amount") > 0))
					{
                        fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")).FormatAsCurrencyNoSymbol();
						if (FCConvert.ToString(fldRCB) == "L" && (rsDetail.Get_Fields_String("Description").ToUpper().Trim() == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
						{
							// do nothing
						}
						else
						{
                            curTotalDebits += rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount");
						}
						fldCredits.Text = "";
					}
					else
					{
                        fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1).FormatAsCurrencyNoSymbol();
						if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || rsDetail.Get_Fields_String("Description").ToUpper().Trim() == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
						{
							// do nothing
						}
						else
						{
                            curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1);
						}
						fldDebits.Text = "";
					}
				}
				else
				{
					if (rptMultipleJournals.InstancePtr.blnShowLiquidatedEncActivity == true)
					{
                        if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && rsDetail.Get_Fields_Decimal("Amount") < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && rsDetail.Get_Fields_Decimal("Amount") > 0))
						{
                            fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")).FormatAsCurrencyNoSymbol();
							if (FCConvert.ToString(fldRCB.Text) == "L" && (rsDetail.Get_Fields_String("Description").ToUpper().Trim() == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
							{
								// do nothing
							}
							else
							{
                                curTotalDebits += rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount");
							}
							fldCredits.Text = rsDetail.Get_Fields_Decimal("Encumbrance").FormatAsCurrencyNoSymbol();
							curTotalCredits += rsDetail.Get_Fields_Decimal("Encumbrance");
						}
						else
						{
                            fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1).FormatAsCurrencyNoSymbol();
							if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
							{
								// do nothing
							}
							else
							{
                                curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount")) * -1);
							}
							fldDebits.Text = rsDetail.Get_Fields_Decimal("Encumbrance").FormatAsCurrencyNoSymbol();
							curTotalDebits += rsDetail.Get_Fields_Decimal("Encumbrance");
						}
					}
					else
					{
                        if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) > 0))
						{
                            fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")).FormatAsCurrencyNoSymbol();
							if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
							{
								// do nothing
							}
							else
							{
                                curTotalDebits += (rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance"));
							}
							fldCredits.Text = "";
						}
						else
						{
                            fldCredits.Text = ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1).FormatAsCurrencyNoSymbol();
							if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
							{
								// do nothing
							}
							else
							{
                                curTotalCredits += ((rsDetail.Get_Fields_Decimal("Amount") - rsDetail.Get_Fields_Decimal("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1);
							}
							fldDebits.Text = "";
						}
					}
				}
			}
            else if (rsInfo.Get_Fields("Type") == "EN")
			{
                if (rsDetail.Get_Fields_Decimal("Amount") > 0)
				{
                    fldDebits.Text = rsDetail.Get_Fields_Decimal("Amount").FormatAsCurrencyNoSymbol();
					if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
					{
						// do nothing
					}
					else
					{
                        curTotalDebits += rsDetail.Get_Fields_Decimal("Amount");
					}
					fldCredits.Text = "";
				}
				else
				{
                    fldCredits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
					if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
					{
						// do nothing
					}
					else
					{
                        curTotalCredits += rsDetail.Get_Fields_Decimal("Amount") * -1;
					}
					fldDebits.Text = "";
				}
			}
			else
			{
                if ((FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) < 0) || (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) > 0))
				{
                    fldDebits.Text = rsDetail.Get_Fields_Decimal("Amount").FormatAsCurrencyNoSymbol();
					if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
					{
						// do nothing
					}
					else
					{
                        curTotalDebits += rsDetail.Get_Fields_Decimal("Amount");
					}
					fldCredits.Text = "";
				}
				else
				{
                    fldCredits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
					if (FCConvert.ToString(fldRCB.Text) == "L" && (Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "REVENUE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "EXPENSE CTL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-APPROP CNTRL" || Strings.Trim(Strings.UCase(FCConvert.ToString(rsDetail.Get_Fields_String("Description")))) == "OFFSET-REVENUE CNTRL"))
					{
						// do nothing
					}
					else
					{
                        curTotalCredits += rsDetail.Get_Fields_Decimal("Amount") * -1;
					}
                    if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "E" && FCConvert.ToString(rsDetail.Get_Fields("Type")) == "P")
					{
						if (rptMultipleJournals.InstancePtr.blnShowLiquidatedEncActivity == true)
						{
                            fldDebits.Text = (rsDetail.Get_Fields_Decimal("Amount") * -1).FormatAsCurrencyNoSymbol();
                            curTotalDebits += rsDetail.Get_Fields_Decimal("Amount") * -1;
						}
						else
						{
                            curTotalCredits -= rsDetail.Get_Fields_Decimal("Amount") * -1;
							fldCredits.Text = "";
							fldDebits.Text = "";
						}
					}
					else
					{
						fldDebits.Text = "";
					}
				}
			}
            fldAccount.Text = rsDetail.Get_Fields_String("Account");
            if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "AP")
			{
                fldWarrant.Text = Strings.Format(rsEntry.Get_Fields("Warrant"), "0000");
			}
			else
			{
				fldWarrant.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalDebits.Text = curTotalDebits.FormatAsCurrencyNoSymbol();
			fldTotalCredits.Text = curTotalCredits.FormatAsCurrencyNoSymbol();
			curTotalDebits = 0;
			curTotalCredits = 0;
			if (rsInfo.EndOfFile() != true)
			{
				this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			else
			{
				this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (rsInfo.EndOfFile())
			{
				rsInfo.MovePrevious();
			}
			fldPosted.Text = Strings.Format(rsInfo.Get_Fields_DateTime("StatusChangeDate"), "MM/dd/yy");
		}

	}
}
