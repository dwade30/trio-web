﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmOutlineRevQuery.
    /// </summary>
    public partial class frmOutlineRevQuery : BaseForm
    {
        private int _lastSelectedRow = 0;

        public frmOutlineRevQuery()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmOutlineRevQuery InstancePtr
        {
            get
            {
                return (frmOutlineRevQuery)Sys.GetInstance(typeof(frmOutlineRevQuery));
            }
        }

        protected frmOutlineRevQuery _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        [DllImport("user32")]
        private static extern int GetCursorPos(ref POINTAPI lpPoint);

        private struct POINTAPI
        {
            public int x;
            public int y;
        };

        string length = "";
        // holds format account lengths
        string length2 = "";
        string ExpDivision;
        int ExpDivLength;
        int NumberOfDepartments;
        bool UpArrowFlag;
        bool DownArrowFlag;
        bool LeftArrowFlag;
        bool RightArrow;
        int TempCol;
        bool CollapsedFlag;
        bool RowChangeFlag;
        string OldDepartment = "";
        int DivLength;
        // holds division length
        int DeptLength;
        // holds department length
        int RevLength;
        int NumberCol;
        // column in the flex grid that has the database key
        int DeptCol;
        int RevCol;
        int DeptNode;
        int DivNode;
        int RevNode;
        int DivCol;
        // column in the grid that holds the division number
        int ShortCol;
        // column in the grid that holds the short description
        int LongCol;
        // column in the grid that holds the long description
        int AccountCol;
        clsDRWrapper rs = new clsDRWrapper();
        clsDRWrapper rs2 = new clsDRWrapper();
        clsDRWrapper rs3 = new clsDRWrapper();
        float DeptPercent;
        float DivPercent;
        float RevPercent;
        bool blnUnload;
        bool blnDirty;

        private void frmOutlineRevQuery_Activated(object sender, System.EventArgs e)
        {
            int deptdivRowNumber;
            string tempDivision;
            if (modGlobal.FormExist(this))
            {
                return;
            }
            NumberCol = 1;
            DeptCol = 2;
            DivCol = 3;
            // initialize the columns
            RevCol = 4;
            ShortCol = 5;
            LongCol = 6;
            AccountCol = 7;
            DeptNode = 0;
            DeptLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 1, 2)))));
            // initialize department length
            RevLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 5, 2)))));
            vs1.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
            // allow the user to resize the columns
            DivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length, 3, 2)))));
            // initialize division lngth
            ExpDivLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Mid(length2, 3, 2)))));
            vs1.ColWidth(0, 300);
            vs1.ColWidth(NumberCol, 0);
            SetDeptWidth();
            if (modAccountTitle.Statics.RevDivFlag)
            {
                RevNode = 1;
                DivNode = -1;
                vs1.ColWidth(DivCol, 0);
            }
            else
            {
                SetDivWidth();
            }
            SetRevWidth();
            vs1.ColWidth(ShortCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
            vs1.ColWidth(LongCol, FCConvert.ToInt32(0.35 * vs1.WidthOriginal));
            vs1.ColWidth(AccountCol, 100);
            SetColumnHeadings();
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            ExpDivision = modValidateAccount.GetFormat("0", ref ExpDivLength);
            tempDivision = modValidateAccount.GetFormat("0", ref DivLength);
            //FC:FINAL:DDU:#2939 - aligned columns
            vs1.ColAlignment(DeptCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(DivCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vs1.ColAlignment(RevCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);

            if (modAccountTitle.Statics.RevDivFlag)
            {
                rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + ExpDivision + "'");
            }
            else
            {
                rs.OpenRecordset("SELECT * FROM DeptDivTitles");
            }

            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.MoveLast();
                rs.MoveFirst();
                vs1.Rows = 1;
            }
            else
            {
                vs1.Rows = 1;
                cmdProcessSave.Enabled = false;
            }
            vs1.Visible = true;
            rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + ExpDivision + "' ORDER BY Department");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.MoveLast();
                rs.MoveFirst();
                NumberOfDepartments = rs.RecordCount();
                while (!rs.EndOfFile())
                {
                    deptdivRowNumber = vs1.Rows;

                    vs1.Rows += 1;
                    //FC:FINAL:AAKV grid color change
                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, temp2, DeptCol, temp2, vs1.Cols - 1, 0x80000017);
                    //vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, temp2, DeptCol, temp2, vs1.Cols - 1, 0x80000018);
                    var department = rs.Get_Fields_String("Department");
                    vs1.TextMatrix(deptdivRowNumber, DeptCol, department + " -");
                    vs1.TextMatrix(deptdivRowNumber, ShortCol, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
                    vs1.TextMatrix(deptdivRowNumber, LongCol, (rs.Get_Fields_String("LongDescription") == null ? "" : rs.Get_Fields_String("LongDescription")));
                    vs1.TextMatrix(deptdivRowNumber, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                    vs1.RowOutlineLevel(deptdivRowNumber, FCConvert.ToInt16(DeptNode));
                    vs1.IsSubtotal(deptdivRowNumber, true);
                    if (!modAccountTitle.Statics.RevDivFlag)
                    {
                        rs2.OpenRecordset($"SELECT * FROM DeptDivTitles WHERE Department = '{department}' AND NOT Division = '{ExpDivision}' ORDER BY Division");
                        if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
                        {
                            rs2.MoveLast();
                            rs2.MoveFirst();
                            while (!rs2.EndOfFile())
                            {
                                vs1.Rows += 1;
                                deptdivRowNumber = vs1.Rows - 1;
                                //FC:FINAL:AAKV: font color change 
                                //vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, temp2, DeptCol, temp2, vs1.Cols - 1, 0x80000017);
                                //vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, temp2, DeptCol, temp2, vs1.Cols - 1, 0x80000016);
                                vs1.IsSubtotal(deptdivRowNumber, true);
                                var division = rs2.Get_Fields_String("Division");
                                vs1.TextMatrix(deptdivRowNumber, DivCol, division + " -");
                                vs1.TextMatrix(deptdivRowNumber, ShortCol, FCConvert.ToString(rs2.Get_Fields_String("ShortDescription")));
                                var longDescription = rs2.Get_Fields_String("LongDescription");
                                vs1.TextMatrix(deptdivRowNumber, LongCol, longDescription ?? "");
                                vs1.TextMatrix(deptdivRowNumber, NumberCol, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
                                vs1.RowOutlineLevel(deptdivRowNumber, FCConvert.ToInt16(DivNode));
                                if (!modAccountTitle.Statics.RevDivFlag)
                                {
                                    FillGridWithRevenueTitles(division, department);
                                }
                                else
                                {
                                    FillGridWithRevenueTitles("", department);
                                }
                                rs2.MoveNext();
                            }
                        }
                    }
                    else
                    {
                        FillGridWithRevenueTitles("", department);
                    }
                    rs.MoveNext();

                }
            }

            CollapseAllDivisions();
            CollapseAllDepts();

            if (vs1.Rows > 1)
            {
                vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vs1.Rows - 1, 0, Color.White);
            }
            if (modAccountTitle.Statics.RevDivFlag)
            {
                vs1.ColWidth(0, 300);
            }
            modColorScheme.ColorGrid(vs1);
            //SBE - use readonly instead of enabled. If grid is disabled, expand/collapse is not working
            vs1.Enabled = true;
            vs1.ReadOnly = false;
            vs1.Focus();
            Refresh();
            // refresh the form
        }

        private void FillGridWithRevenueTitles(string division, string department)
        {
            var _rs = new clsDRWrapper();

            try
            {
                if (string.IsNullOrEmpty(division))
                    _rs.OpenRecordset($"SELECT * FROM RevTitles WHERE Department = '{department}' ORDER BY Revenue");
                else
                    _rs.OpenRecordset($"SELECT * FROM RevTitles WHERE Division = '{division}' AND Department = '{department}' ORDER BY Revenue");

                if (_rs.EndOfFile() == true || _rs.BeginningOfFile() == true) return;

                _rs.MoveLast();
                _rs.MoveFirst();

                while (!_rs.EndOfFile())
                {
                    vs1.Rows += 1;
                    var targetRowNumber = vs1.Rows - 1;
                    vs1.TextMatrix(targetRowNumber, RevCol, FCConvert.ToString(_rs.Get_Fields_String("Revenue")));
                    vs1.TextMatrix(targetRowNumber, ShortCol, FCConvert.ToString(_rs.Get_Fields_String("ShortDescription")));
                    vs1.TextMatrix(targetRowNumber, LongCol, _rs.Get_Fields_String("LongDescription") ?? "");
                    vs1.TextMatrix(targetRowNumber, NumberCol, FCConvert.ToString(_rs.Get_Fields_Int32("ID")));
                    vs1.TextMatrix(targetRowNumber, AccountCol, FCConvert.ToString(_rs.Get_Fields_String("CloseoutAccount")));
                    vs1.RowOutlineLevel(targetRowNumber, FCConvert.ToInt16(RevNode));

                    _rs.MoveNext();

                }
            }
            finally
            {
                _rs.DisposeOf();
            }
        }

        private void CollapseAllDepts()
        {
            for (var i = 1; i <= vs1.Rows - 1; i++)
            {
                if (IsOnDepartmentEntry(i))
                {
                    vs1.IsCollapsed(i, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                }
            }
        }

        private void CollapseAllDivisions()
        {
            for (var i = 1; i <= vs1.Rows - 1; i++)
            {
                if (IsOnDivisionEntry(i))
                {
                    vs1.IsCollapsed(i, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                }
            }
        }

        private void SetColumnHeadings()
        {
            vs1.TextMatrix(0, DeptCol, "Dept");
            vs1.TextMatrix(0, DivCol, "Div");
            vs1.TextMatrix(0, ShortCol, "Short Desc");
            vs1.TextMatrix(0, LongCol, "Long Desc");
            vs1.TextMatrix(0, RevCol, "Rev");
            vs1.TextMatrix(0, AccountCol, "Closeout Acct");
        }

        private void SetRevWidth()
        {
            if (RevLength < 4)
            {
                vs1.ColWidth(RevCol, 600);
            }
            else
            {
                vs1.ColWidth(RevCol, RevLength * 140 + 50);
            }

            RevPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(RevCol)) / vs1.WidthOriginal);

        }

        private void SetDivWidth()
        {
            DivNode = 1;
            RevNode = 2;

            if (DivLength < 4)
            {
                vs1.ColWidth(DivCol, 500);
            }
            else
            {
                vs1.ColWidth(DivCol, DivLength * 140 + 50);
            }

            DivPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DivCol)) / vs1.WidthOriginal);
        }

        private void SetDeptWidth()
        {
            if (DeptLength < 4)
            {
                vs1.ColWidth(DeptCol, 600);
                DeptPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DeptCol)) / vs1.WidthOriginal);
            }
            else
            {
                vs1.ColWidth(DeptCol, DeptLength * 140 + 50);
                DeptPercent = FCConvert.ToSingle(FCConvert.ToDouble(vs1.ColWidth(DeptCol)) / vs1.WidthOriginal);
            }
        }

        private void frmOutlineRevQuery_Click(object sender, System.EventArgs e)
        {
            POINTAPI MousePosition = new POINTAPI();
            bool PassFail;
            if (vs1.MouseCol == 0)
            {
                //SBE - use readonly instead of enabled. If grid is disabled, expand/collapse is not working
                vs1.Enabled = true;
                vs1.ReadOnly = false;
                vs1.HighLight = FCGrid.HighLightSettings.flexHighlightWithFocus;
                return;
            }
        }

        private void frmOutlineRevQuery_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Tab)
            {
                KeyCode = (Keys)0;
                Support.SendKeys("{ENTER}", false);
                return;
            }
            if (vs1.Col == AccountCol && IsOnRevenueEntry(vs1.Row))
            {
                modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
            }
            if (KeyCode == Keys.F10)
            {
                // if the user saves
                KeyCode = (Keys)0;
            }
        }

        private void frmOutlineRevQuery_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmOutlineRevQuery.FillStyle	= 0;
            //frmOutlineRevQuery.ScaleWidth	= 9045;
            //frmOutlineRevQuery.ScaleHeight	= 7230;
            //frmOutlineRevQuery.LinkTopic	= "Form2";
            //frmOutlineRevQuery.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            length = modAccountTitle.Statics.Rev;
            length2 = modAccountTitle.Statics.Exp;
            blnDirty = false;
            modValidateAccount.SetBDFormats();
            vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            modNewAccountBox.GetFormats();
            modGlobalFunctions.SetTRIOColors(this);
            vs1.AddExpandButton(1);
            //FC:FINAL:SBE - #i789 - attach key event handlers to grid editor
            vs1.EditingControlShowing += vs1_EditingControlShowing;
        }

        private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control == null) return;

            JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
            CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
            //
            e.Control.KeyDown -= new KeyEventHandler(vs1_KeyDownEdit);
            e.Control.KeyUp -= new KeyEventHandler(vs1_KeyUpEdit);
            //
            e.Control.KeyDown += new KeyEventHandler(vs1_KeyDownEdit);
            e.Control.KeyUp += new KeyEventHandler(vs1_KeyUpEdit);
            //
            int col = (sender as FCGrid).Col;
            if (AccountCol == -1 || AccountCol == col)
            {
                if (e.Control is TextBox)
                {
                    (e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
                }
                DynamicObject customClientData = new DynamicObject();
                customClientData["GRID7Light_Col"] = col;
                customClientData["intAcctCol"] = AccountCol;
                customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
                customClientData["gboolTownAccounts"] = true;
                customClientData["gboolSchoolAccounts"] = false;
                customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
                customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
                customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
                customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
                customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
                customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
                customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
                customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
                customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
                customProperties.SetCustomPropertiesValue(e.Control, customClientData);
                if (e.Control.UserData.GRID7LightClientSideKeys == null)
                {
                    e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
                    JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
                    JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
                    keyDownEvent.Event = "keydown";
                    keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
                    JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
                    keyUpEvent.Event = "keyup";
                    keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
                    clientEvents.Add(keyDownEvent);
                    clientEvents.Add(keyUpEvent);
                }
            }
        }


        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            if (e.CloseReason != FCCloseReason.FormCode)
            {
                if (blnDirty)
                {
                    ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ans == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            //MDIParent.InstancePtr.GRID.Focus();
        }

        //private void frmOutlineRevQuery_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        //{
        //    Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

        //    switch (KeyAscii)
        //    {
        //        case Keys.Escape:
        //            KeyAscii = (Keys)0;
        //            Close();

        //            break;

        //        case Keys.Return:
        //        {
        //            if (frmOutlineRevQuery.InstancePtr.ActiveControl.GetName() != "vs1")
        //            {
        //                KeyAscii = (Keys) 0;
        //                Support.SendKeys("{TAB}", false);
        //            }

        //            break;
        //        }
        //    }

        //    e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        //}

        private void frmOutlineRevQuery_Resize(object sender, System.EventArgs e)
        {
            vs1.ColWidth(DeptCol, FCConvert.ToInt32(vs1.WidthOriginal * DeptPercent + 250));
            vs1.ColWidth(DivCol, FCConvert.ToInt32(vs1.WidthOriginal * DivPercent + 250));
            vs1.ColWidth(RevCol, FCConvert.ToInt32(vs1.WidthOriginal * RevPercent));
            vs1.ColWidth(ShortCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2));
            vs1.ColWidth(LongCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
            vs1.ColWidth(AccountCol, 100);
        }

        private void mnuFileSave_Click(object sender, System.EventArgs e)
        {
            blnUnload = false;
            SaveInfo();
        }

        private void mnuRowsAdd_Click(object sender, System.EventArgs e)
        {
            int counter = 0;
            if (vs1.RowOutlineLevel(vs1.Row) == DeptNode)
            {
                if (modAccountTitle.Statics.RevDivFlag)
                {
                    if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                    {
                        MessageBox.Show("You must open up a Department to add a Revenue to it", "Unable to Create a Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                        return;
                    }

                    if (vs1.Row == vs1.Rows - 1)
                    {
                        vs1.Rows += 1;
                        RowChangeFlag = true;
                        vs1.Select(vs1.Row + 1, RevCol);
                        RowChangeFlag = false;
                        vs1.EditText = "";
                        vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
                        vs1.RowOutlineLevel(vs1.Row, FCConvert.ToInt16(RevNode));
                        vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));

                        counter = vs1.Row;
                        while (vs1.RowOutlineLevel(counter) == RevNode)
                        {
                            counter -= 1;
                        }
                        vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("You may only add a Revenue to a Division", "Unable to Create Revenue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                    return;
                }
            }
            else
            {
                if (vs1.RowOutlineLevel(vs1.Row) == DivNode)
                {
                    if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                    {
                        MessageBox.Show("You must open up a Division to add a Revenue to it", "Unable to Create a Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                        return;
                    }
                    else
                    {
                        if (vs1.Row == vs1.Rows - 1)
                        {
                            vs1.Rows += 1;
                            RowChangeFlag = true;
                            vs1.Select(vs1.Row + 1, RevCol);
                            RowChangeFlag = false;
                            vs1.EditText = "";
                            vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
                            vs1.RowOutlineLevel(vs1.Row, FCConvert.ToInt16(RevNode));
                            vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));

                            counter = vs1.Row;
                            while (vs1.RowOutlineLevel(counter) == RevNode)
                            {
                                counter -= 1;
                            }
                            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
                            vs1.Select(vs1.Row, RevCol);
                            vs1.EditCell();
                            return;
                        }
                    }
                }
            }

            RowChangeFlag = true;
            if (vs1.RowOutlineLevel(vs1.Row) != RevNode)
            {
                vs1.Select(vs1.Row + 1, RevCol);
            }
            RowChangeFlag = false;
            vs1.AddItem("", vs1.Row);
            // add a row in
            vs1.EditText = "";
            vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
            vs1.RowOutlineLevel(vs1.Row, FCConvert.ToInt16(RevNode));
            vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));

            counter = vs1.Row - 1;
            while (vs1.RowOutlineLevel(counter) == RevNode)
            {
                counter -= 1;
            }
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            vs1.Select(vs1.Row, RevCol);
            vs1.EditCell();
        }

        private void mnuRowsDelete_Click(object sender, System.EventArgs e)
        {
            int counter = 0;
            // vbPorter upgrade warning: Check As short, int --> As DialogResult
            DialogResult Check;
            string tempDepartment = "";
            string tempDivision = "";
            if (vs1.TextMatrix(vs1.Row, RevCol) != "" && vs1.TextMatrix(vs1.Row, ShortCol) != "")
            {
                if (IsOnRevenueEntry(vs1.Row))
                {
                    Check = MessageBox.Show("Are You Sure You Want To Delete This Record?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    // make sure they want to delete
                    if (Check == DialogResult.Yes)
                    {
                        // if they do
                        if (IsRevenueAlreadyUsed(Strings.Trim(vs1.TextMatrix(vs1.Row, RevCol))) == false)
                        {
                            if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, NumberCol)) != 0)
                            {
                                rs.OpenRecordset("SELECT * FROM RevTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, NumberCol))));
                                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                                {
                                    tempDepartment = FCConvert.ToString(rs.Get_Fields_String("Department"));
                                    tempDivision = FCConvert.ToString(rs.Get_Fields_String("Division"));
                                    rs.Delete();
                                    rs.Update();
                                }
                                if (modAccountTitle.Statics.RevDivFlag)
                                {
                                    rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' and FirstAccountField = '" + tempDepartment + "' and SecondAccountField = '" + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, RevCol))) + "'");
                                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                                    {
                                        rs.Delete();
                                        rs.Update();
                                    }
                                }
                                else
                                {
                                    rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' and FirstAccountField = '" + tempDepartment + "' and SecondAccountField = '" + tempDivision + "' and ThirdAccountField = '" + vs1.TextMatrix(vs1.Row, RevCol) + "'");
                                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                                    {
                                        rs.Delete();
                                        rs.Update();
                                    }
                                }
                            }
                            vs1.RemoveItem(vs1.Row);

                            counter = vs1.Row;
                            while (vs1.RowOutlineLevel(counter) == RevNode)
                            {
                                counter -= 1;
                            }
                        }
                        else
                        {
                            MessageBox.Show("You may not delete this Revenue Account because it it has a budget associated with it or is used in one or more journal entries.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            else
            {
                if (IsOnRevenueEntry(vs1.Row))
                {
                    vs1.RemoveItem(vs1.Row);

                    counter = vs1.Row - 1;
                    while (vs1.RowOutlineLevel(counter) == RevNode)
                    {
                        counter -= 1;
                    }
                }
            }
            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        }


        private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (vs1.Col == AccountCol && IsOnRevenueEntry(vs1.Row))
            {
                vs1.EditMaxLength = 23;
            }
        }

        private void vs1_ChangeEdit(object sender, System.EventArgs e)
        {
            if (vs1.IsCurrentCellInEditMode)
            {
                blnDirty = true;
            }
        }

        #region Node Levels

        private bool IsOnRevenueEntry(int row = -1)
        {
            var nodeLevel = GetNodeLevel(row);

            return nodeLevel == RevNode;
        }

        private bool IsOnDivisionEntry(int row = -1)
        {
            var nodeLevel = GetNodeLevel(row);

            return nodeLevel == DivNode;
        }

        private bool IsOnDepartmentEntry(int row = -1)
        {
            var nodeLevel = GetNodeLevel(row);

            return nodeLevel == DeptNode;
        }

        private int GetNodeLevel(int row)
        {
            // by default we just use the current row, but user can look at any row
            if (row == -1) row = vs1.Row;
            var nodeLevel = vs1.RowOutlineLevel(row);

            return nodeLevel;
        }

        #endregion

        #region Delete

        private void HandleDeleteKey()
        {
            int verifyDelete;
            int counter;
            string tempDepartment;
            string tempDivision;

            // if they hit the delete key
            if (vs1.TextMatrix(vs1.Row, RevCol) != "" && vs1.TextMatrix(vs1.Row, ShortCol) != "")
            {
                if (IsOnRevenueEntry(vs1.Row))
                {
                    verifyDelete = FCConvert.ToInt32(MessageBox.Show("Are You Sure You Want To Delete This Record?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question));

                    // make sure they want to delete
                    if (verifyDelete == FCConvert.ToInt32(DialogResult.Yes))
                    {
                        // if they do
                        if (IsRevenueAlreadyUsed(Strings.Trim(vs1.TextMatrix(vs1.Row, RevCol))))
                        {
                            MessageBox.Show("You may not delete this Revenue Account because it it has a budget associated with it or is used in one or more journal entries.", "Unable to Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            return;
                        }

                        if (FCConvert.ToDouble(vs1.TextMatrix(vs1.Row, NumberCol)) != 0)
                        {
                            rs.OpenRecordset("SELECT * FROM RevTitles WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row, NumberCol))));

                            if (rs.EndOfFile() == true || rs.BeginningOfFile() == true)
                            {
                                return;
                            }

                            tempDepartment = FCConvert.ToString(rs.Get_Fields_String("Department"));
                            tempDivision = FCConvert.ToString(rs.Get_Fields_String("Division"));

                            rs.Delete();
                            rs.Update();

                            DeleteAccountMaster(tempDepartment, tempDivision);
                        }

                        vs1.RemoveItem(vs1.Row);

                        counter = vs1.Row;

                        while (vs1.RowOutlineLevel(counter) == RevNode)
                        {
                            counter -= 1;
                        }
                    }
                }
            }
            else
            {
                if (IsOnRevenueEntry(vs1.Row))
                {
                    vs1.RemoveItem(vs1.Row);

                    counter = vs1.Row - 1;

                    while (vs1.RowOutlineLevel(counter) == RevNode)
                    {
                        counter -= 1;
                    }
                }
            }

            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
        }

        private void DeleteAccountMaster(string tempDepartment, string tempDivision)
        {
            if (modAccountTitle.Statics.RevDivFlag)
            {
                rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' and FirstAccountField = '" + tempDepartment + "' and SecondAccountField = '" + vs1.TextMatrix(vs1.Row, RevCol) + "'");
            }
            else
            {
                rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' and FirstAccountField = '" + tempDepartment + "' and SecondAccountField = '" + tempDivision + "' and ThirdAccountField = '" + vs1.TextMatrix(vs1.Row, RevCol) + "'");
            }

            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                rs.Delete();
                rs.Update();
            }
        }

        #endregion

        #region Insert

        private void HandleInsertKey()
        {
            //if (IsOnDepartmentEntry())
            //{
            //    InsertDeptEntry();
            //}
            //else
            //{
            vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
            InsertRevenueEntry();
            //}
        }

        private bool InsertDivEntry()
        {
            //if (vs1.IsCollapsed(counter) != FCGrid.CollapsedSettings.flexOutlineExpanded)
            //{
            //    MessageBox.Show("You must open up a Division to add a Revenue to it", "Unable to Create a Department", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;

            //    return true;
            //}

            //if (vs1.Row != vs1.Rows - 1) return false;

            vs1.Rows += 1;
            RowChangeFlag = true;
            vs1.Select(vs1.Row + 1, RevCol);
            RowChangeFlag = false;
            vs1.EditText = "";
            vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, 0, Color.White);
            vs1.RowOutlineLevel(vs1.Row, FCConvert.ToInt16(RevNode));
            vs1.TextMatrix(vs1.Row, NumberCol, FCConvert.ToString(0));

            var counter = vs1.Row;

            while (vs1.RowOutlineLevel(counter) == RevNode)
            {
                counter -= 1;
            }

            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            vs1.Select(vs1.Row, RevCol);
            vs1.EditCell();

            return true;
        }

        private bool InsertDeptEntry()
        {
            if (modAccountTitle.Statics.RevDivFlag)
            {
                // be sure the dept is expanded
                vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);

                InsertRevenueEntry();

                return true;
            }
            else
            {
                MessageBox.Show("You may only add a Revenue to a Division", "Unable to Create Revenue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;

                return false;
            }

        }

        private void InsertRevenueEntry()
        {
            int newRow = vs1.Row + 1;

            // add a row in
            vs1.AddItem("", newRow);
            vs1.EditText = "";
            vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, newRow, 0, Color.White);
            vs1.RowOutlineLevel(newRow, FCConvert.ToInt16(RevNode));
            vs1.TextMatrix(newRow, NumberCol, FCConvert.ToString(0));
            //

            var upperRow = GetPreviousUpperRow();

            vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
            RowChangeFlag = true;
            vs1.ClearSelection();
            vs1.Select(newRow, RevCol);
            vs1.ShowCell(newRow, RevCol);

            RowChangeFlag = false;
            vs1.EditCell();
        }

        #endregion

        #region grid keypress and mousemovements

        private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
        {
            var keyCode = e.KeyCode;

            switch (keyCode)
            {
                case Keys.Up:
                    {
                        //keyCode = 0;
                        //int previousRow = vs1.Row - 1;

                        //if (IsOnRevenueEntry(previousRow))
                        //{
                        //    TempCol = vs1.TextMatrix(previousRow, RevCol) == "" ? RevCol : ShortCol;
                        //    vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                        //    vs1.ClearSelection();
                        //    vs1.Select(previousRow, TempCol);
                        //    vs1.EditCell();
                        //}

                        break;
                    }
                case Keys.Insert:
                    HandleInsertKey();

                    break;
                case Keys.Delete:
                    HandleDeleteKey();

                    break;

                case Keys.Escape:
                
                    keyCode = 0;

                    break;
                
            }

        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
        {
            int rowNumber = 0;
            Keys KeyCode = e.KeyCode;
            switch (KeyCode)
            {
                case Keys.Return:
                    {
                        KeyCode = 0;

                        if (vs1.Col == AccountCol)
                        {
                            // if on the last column, then go to the next row, if there is one
                            if (vs1.Row < vs1.Rows - 1)
                            {
                                rowNumber = GetNextRow();
                                if (rowNumber == 0) return; 

                                SetRow(rowNumber);

                                // if the row doesn't have the revenue code set already, go there, otherwise go to the short desc column
                                vs1.Col = vs1.TextMatrix(vs1.Row, RevCol) == "" ? RevCol : ShortCol;
                                vs1.EditCell();
                            }
                        }
                        else
                        {
                            if (vs1.Col == RevCol)
                            {
                                if (vs1.EditText != "")
                                {
                                    vs1.Select(vs1.Row, vs1.Col + 1);
                                    vs1.EditCell();
                                }
                                else
                                {
                                    rowNumber = GetNextRow();

                                    if (rowNumber == 0)
                                    {
                                        return;
                                    }

                                    RowChangeFlag = true;
                                    vs1.Row = rowNumber;
                                    RowChangeFlag = false;
                                    vs1.EditCell();
                                }
                            }
                            else
                                if (vs1.Col == LongCol)
                                {
                                    vs1.Col = AccountCol;
                                }
                                else
                                {
                                    vs1.Select(vs1.Row, vs1.Col + 1);
                                    vs1.EditCell();
                                }
                        }

                        break;
                    }
                case Keys.Down:
                    {
                        vs1.Editable = FCGrid.EditableSettings.flexEDNone;
                        DownArrowFlag = true;
                        UpArrowFlag = false;
                        LeftArrowFlag = false;
                        RightArrow = false;
                        int counter = GetNextRow();
                        if (counter > 0)
                        {
                            KeyCode = 0;
                            if (vs1.TextMatrix(counter, RevCol) == "")
                            {
                                vs1.Select(counter, RevCol);
                                vs1.EditCell();
                            }
                            else
                            {
                                RowChangeFlag = true;
                                vs1.Select(counter, vs1.Col);
                                RowChangeFlag = false;
                                vs1.EditCell();

                                if (vs1.TextMatrix(vs1.Row, vs1.Col) != "")
                                {
                                    Support.SendKeys("{LEFT}", false);
                                }
                            }
                        }

                        break;
                    }
                case Keys.Left:
                    {
                        DownArrowFlag = false;
                        UpArrowFlag = false;
                        LeftArrowFlag = true;
                        RightArrow = false;
                        
                        break;
                    }
                case Keys.Up:
                    {
                        DownArrowFlag = false;
                        UpArrowFlag = true;
                        LeftArrowFlag = false;
                        RightArrow = false;
                        TempCol = vs1.Col;
                        rowNumber = GetPreviousRow();
                        if (rowNumber == 0)
                        {
                            KeyCode = 0;
                        }
                        else
                        {
                            KeyCode = 0;
                            TempCol = vs1.TextMatrix(rowNumber, RevCol) == ""
                                ? RevCol
                                : ShortCol;

                            RowChangeFlag = true;
                            vs1.Select(rowNumber, TempCol);
                            RowChangeFlag = false;
                            vs1.EditCell();
                        }

                        break;
                    }
                case Keys.Right:
                    {
                        DownArrowFlag = false;
                        UpArrowFlag = false;
                        LeftArrowFlag = false;
                        RightArrow = true;
                        
                        break;
                    }
                case Keys.Escape:
                    KeyCode = 0;
                    //Close();

                    break;
                case Keys.Insert when vs1.Row == vs1.Rows - 1:
                    {
                        vs1.Rows += 1;
                        HandleInsertKey();

                        break;
                    }
                case Keys.Insert:
                    {

                        HandleInsertKey();

                        break;
                    }
            }

            if (vs1.Col != RevCol) return;

            // if they are in the division field
           // if (vs1.EditText.Length != RevLength) return;

            // if the cell is the length of the division number
            //if (vs1.EditSelStart != RevLength) return;

            // don't move the cursor
            if (KeyCode == Keys.Back || KeyCode == Keys.Left || KeyCode == Keys.Up || KeyCode == Keys.Down) return;

        }

        private void vs1_KeyUpEdit(object sender, KeyEventArgs e)
        {
            Keys keyCode = e.KeyCode;

            if (vs1.Col == AccountCol && IsOnRevenueEntry(vs1.Row))
            {
                // this is for the left and right arrow key
                if (keyCode != Keys.Return)
                {
                    string temp1 = vs1.EditText;

                    //FC:FINAL:SBE - #i789 - keep and restore editor SelectionStart, because it will be lost after text is changed
                    int selectionStart = vs1.EditSelStart;
                    modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, keyCode, FCConvert.ToInt32(e.Shift), vs1.EditSelStart, ref temp1, vs1.EditSelLength);
                    vs1.EditText = temp1;

                    return;
                }

                if (FCConvert.ToInt32(keyCode) != 40 && FCConvert.ToInt32(keyCode) != 38 && FCConvert.ToInt32(keyCode) != 13)
                {
                    // up and down arrows
                    //modNewAccountBox.CheckAccountKeyCode(vs1, vs1.Row, vs1.Col, FCConvert.ToInt16(KeyCode), 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
                    return;
                }
            }

            if (keyCode != Keys.Right || vs1.Col == TempCol) return;

            //vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            //vs1.EditCell();
        }

        private void vs1_CellFormattingEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var rowNumber = e.RowIndex;
            var columnNumber = e.ColumnIndex;
            var flexRowIndex = vs1.GetFlexRowIndex(rowNumber);

            if (columnNumber == -1 || rowNumber == -1) return;
            if (flexRowIndex < 0) return;

            DataGridViewCell cell = vs1[columnNumber, rowNumber];

            if (columnNumber == AccountCol && IsOnRevenueEntry(rowNumber))
            {
                modNewAccountBox.SetGridFormat(vs1, rowNumber, columnNumber, true, "", "G");
                
            }
            cell.ToolTipText = modAccountTitle.Statics.ExpDivFlag || vs1.RowOutlineLevel(flexRowIndex) != 0
                ? "Click on a row to highlight it then click the Insert key to add a row beneath/beside it."
                : "";
        }

        #endregion


        private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            bool flag = false;

            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = vs1.GetFlexRowIndex(e.RowIndex);
            int col = vs1.GetFlexColIndex(e.ColumnIndex);
            if (col == RevCol)
            {
                // if we are in the division column
                vs1.DataRefresh();
                if (vs1.EditText.Length > 0)
                {
                    // if there is something in that field
                    if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) == 0)
                    {
                        MessageBox.Show("You may only enter a number greater than 0 in this field", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        vs1.EditSelStart = 0;
                        vs1.EditSelLength = vs1.TextMatrix(row, col).Length;
                        return;
                    }
                    else
                    {
                        vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref RevLength);
                    }
                }
                if (vs1.EditText.Length == RevLength && RevLength != 0)
                {
                    // if there is something in the field
                    var counter = row - 1;
                    var search = modAccountTitle.Statics.RevDivFlag ? 0 : 1;
                    while (vs1.RowOutlineLevel(counter) != search)
                    {
                        counter -= 1;
                    }

                    var counter2 = row == vs1.Rows - 1 ? row : row + 1;

                    while (vs1.RowOutlineLevel(counter2) != search)
                    {
                        counter2 += 1;

                        if (counter2 != vs1.Rows) continue;

                        counter2 -= 1;
                        break;
                    }

                    for (int i = counter; i <= counter2 - 1; i++)
                    {
                        // check to see if it is a duplicate division number
                        if (i == row || vs1.EditText != vs1.TextMatrix(i, col)) continue;

                        // if there is a match give an error
                        MessageBox.Show("This Revenue Number is Not Allowable Because it Matches a Previously Existing Revenue Number", "Invalid Revenue Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;

                        // cancel their action
                        vs1.EditSelStart = 0;

                        // highlight the field
                        vs1.EditSelLength = vs1.EditText.Length;

                        return;
                    }
                }
            }

            if (col != AccountCol || vs1.RowOutlineLevel(row) != RevNode) return;

            flag = e.Cancel = modNewAccountBox.CheckAccountValidate(vs1, row, col, e.Cancel);
            if (flag)
            {
                vs1.EditText = "";
                return;
            }

            var isRevOrExpCtr = modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strRevCtrAccount
                                || modBudgetaryAccounting.GetAccount(vs1.EditText) == modBudgetaryAccounting.Statics.strExpCtrAccount;

            if (modAccountTitle.Statics.YearFlag)
            {
                if (!isRevOrExpCtr) return;

                MessageBox.Show("You may not enter the Revenue or Expense Control Account as a closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                vs1.EditText = "";
            }
            else
            {
                if (!isRevOrExpCtr || Strings.Right(vs1.EditText, 2) != "00")
                    return;

                MessageBox.Show("You may not enter the Revenue or Expense Control Account as a closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                vs1.EditText = "";
            }
        }

        #region Utility Functions

        private bool IsRevenueAlreadyUsed(string strRev)
        {
            string strAcct = "";
            var strDiv = "";


            for (int i = vs1.Row; i >= 1; i--)
            {
                if (!modAccountTitle.Statics.RevDivFlag && vs1.TextMatrix(i, DivCol) != "" && strDiv == "")
                {
                    strDiv = Strings.Left(vs1.TextMatrix(i, DivCol), DivLength);
                }

                if (vs1.TextMatrix(i, DeptCol) == "") continue;

                strAcct = "R " + Strings.Left(vs1.TextMatrix(i, DeptCol), DeptLength);
                if (!modAccountTitle.Statics.RevDivFlag)
                {
                    strAcct += $"-{strDiv}-{strRev}";
                }
                else
                {
                    strAcct += $"-{strRev}";
                }
                break;
            }

            return IsAccountUsed(strAcct);

        }

        private static bool IsAccountUsed(string strAcct)
        {
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset($"SELECT * FROM APJournalDetail WHERE Account = '{strAcct}'");

                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    return true;
                }
                else
                {
                    rs.OpenRecordset($"SELECT * FROM AccountMaster WHERE Account = '{strAcct}' AND CurrentBudget <> 0");

                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        return true;
                    }
                    else
                    {
                        rs.OpenRecordset($"SELECT * FROM EncumbranceDetail WHERE Account = '{strAcct}'");

                        if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                        {
                            return true;
                        }

                        else
                        {
                            rs.OpenRecordset($"SELECT * FROM JournalEntries WHERE Account = '{strAcct}'");

                            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                            {
                                return true;
                            }

                            else
                            {
                                rs.OpenRecordset($"SELECT * FROM StandardAccounts WHERE Account = '{strAcct}'");

                                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                rs.DisposeOf();
            }

            return false;
        }
 
        private void SetRow(int targetRowNumber)
        {
            RowChangeFlag = true;

            //FC:FINAL:AM:#2940 - can't set the row to a collapsed one
            if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineExpanded)
            {
                vs1.Row = targetRowNumber;
            }
            else
            {
                vs1.Editable = FCGrid.EditableSettings.flexEDNone;
            }

            RowChangeFlag = false;
        }

        private void SetEditable()
        {
            // first set the cell to not be editable
            vs1.Editable = FCGrid.EditableSettings.flexEDNone;

            if (!vs1.IsCurrentCellInEditMode && IsOnRevenueEntry(vs1.Row))
            {
                vs1.EditCell();
            }
            
        }
        #endregion

        #region Grid Row Movement Methods

        private short GetNextRow()
        {
            short rowNumber = 0;
            bool divisionFlag = false;
            bool deptFlag = false;
            var i = vs1.Row + 1;

            while (i < vs1.Rows)
            {
                if (vs1.RowOutlineLevel(i) == RevNode)
                {
                    if (!divisionFlag && !deptFlag && vs1.IsCollapsed(GetPreviousUpperRow()) == FCGrid.CollapsedSettings.flexOutlineExpanded)
                    {
                        break;
                    }
                }
                else
                {
                    if (vs1.RowOutlineLevel(i) == DivNode)
                    {
                        if (!deptFlag)
                        {
                            divisionFlag = vs1.IsCollapsed(i) != FCGrid.CollapsedSettings.flexOutlineExpanded;
                        }
                    }
                    else
                    {
                        deptFlag = vs1.IsCollapsed(i) != FCGrid.CollapsedSettings.flexOutlineExpanded;
                    }
                }

                i += 1;

                if (i != vs1.Rows) continue;

                rowNumber = 0;
                return rowNumber;
            }

            rowNumber = i == vs1.Rows
                ? (short)0
                : FCConvert.ToInt16(i);

            return rowNumber;
        }
        private short GetPreviousRow()
        {
            short rowNumber = 0;
            var tempRow = 0;
            var i = vs1.Row - 1;

            while (i > 1)
            {
                if (vs1.RowOutlineLevel(i) == RevNode)
                {
                    if (tempRow == 0)
                    {
                        tempRow = i;
                        break;
                    }
                }
                else
                {
                    if (vs1.RowOutlineLevel(i) == DivNode)
                    {
                        if (vs1.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineExpanded)
                        {
                            if (tempRow != 0)
                            {
                                break;
                            }
                        }
                        else
                        {
                            tempRow = 0;
                        }
                    }
                    else
                    {
                        if (modAccountTitle.Statics.RevDivFlag)
                        {
                            if (vs1.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineExpanded)
                            {
                                break;
                            }
                            else
                            {
                                tempRow = 0;
                            }
                        }
                    }
                }

                i -= 1;

                if (i != 1) continue;

                rowNumber = 0;
                return rowNumber;
            }
            rowNumber = FCConvert.ToInt16(tempRow);
            return rowNumber;
        }
        private short GetPreviousUpperRow()
        {
            var TempRow = 0;
            var rowNumberToCheck = vs1.Row;
            while (rowNumberToCheck > 1)
            {
                if (vs1.RowOutlineLevel(rowNumberToCheck) == DivNode || vs1.RowOutlineLevel(rowNumberToCheck) == DeptNode)
                {
                    TempRow = rowNumberToCheck;

                    break;
                }

                rowNumberToCheck--;

            }

            if (TempRow == 0) TempRow = 1;

            return FCConvert.ToInt16(TempRow);
        }

        #endregion

        #region Save Methods

        private void SaveAccountMasterInfo()
        {
            var rs3 = new clsDRWrapper();

            try
            {
                var dept = rs.Get_Fields_String("Department");
                var division = rs.Get_Fields_String("Division");
                var revenue = rs.Get_Fields_String("Revenue");

                rs3.OmitNullsOnInsert = true;
                rs3.OpenRecordset("SELECT * FROM AccountMaster WHERE ID = 0");
                rs3.AddNew();
                rs3.Set_Fields("AccountType", "R");
                rs3.Set_Fields("FirstAccountField", dept);
                rs3.Set_Fields("DateCreated", DateTime.Today);

                if (modAccountTitle.Statics.RevDivFlag)
                {
                    rs3.Set_Fields("SecondAccountField", revenue);
                    rs3.Set_Fields("Account", modBudgetaryMaster.CreateAccount_2186("R", dept, revenue, "", "", "", false));
                }
                else
                {
                    rs3.Set_Fields("SecondAccountField", division);
                    rs3.Set_Fields("ThirdAccountField", revenue);
                    rs3.Set_Fields("Account", modBudgetaryMaster.CreateAccount_2186("R", dept, division, revenue, "", "", false));
                }

                rs3.Set_Fields("Valid", 0);
                rs3.Update();
            }

            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
            finally
            {
                rs3.DisposeOf();
            }
        }

        private void SaveInfo()
        {
            int i;
            bool flag = false;
            string tempDepartment = "";
            string tempDivision = "";
            var rsTesting = new clsDRWrapper();
            // vbPorter upgrade warning: strFund As string	OnWriteFCConvert.ToInt32(
            string strFund = "";
            vs1.Select(0, 1);
            for (i = 1; i <= vs1.Rows - 1; i++)
            {
                if (vs1.RowOutlineLevel(i) == DeptNode)
                {
                    strFund = FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount("E " + Strings.Left(vs1.TextMatrix(i, DeptCol), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))) + "-00-00"));
                }
                else if (vs1.RowOutlineLevel(i) == RevNode)
                {
                    if (Strings.Trim(vs1.TextMatrix(i, ShortCol)) == "" || Strings.Trim(vs1.TextMatrix(i, RevCol)) == "")
                    {
                        flag = true;
                        break;
                    }

                    var acct = vs1.TextMatrix(i, AccountCol);

                    if (acct != "")
                    {
                        if (Strings.Left(acct, 1) != "G")
                        {
                            MessageBox.Show("You may only use general ledger accounts for an alternate closeout account.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            vs1.Row = i;
                            vs1.Col = AccountCol;
                            vs1.EditCell();
                            return;
                        }
                        else
                        {
                            if (FCConvert.ToDouble(strFund) != modBudgetaryAccounting.GetFundFromAccount(acct))
                            {
                                MessageBox.Show("The fund your department uses must be the fund of the closeout account you are using for the department or any of it's divisions.", "Invalid Closeout Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                vs1.Row = i;
                                vs1.Col = AccountCol;
                                vs1.EditCell();
                                return;
                            }
                        }
                    }
                }
            }
            if (flag)
            {
                MessageBox.Show("There must be a short description and an account for every revenue before you can save", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //! Load frmWait; // show the wait form
            frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Saving Data";
            frmWait.InstancePtr.Show();
            frmWait.InstancePtr.Refresh();

            for (i = 1; i <= vs1.Rows - 1; i++)
            {
                var dept = vs1.TextMatrix(i, DeptCol);
                var division = vs1.TextMatrix(i, DivCol);
                var revenue = vs1.TextMatrix(i, RevCol);
                var number = vs1.TextMatrix(i, NumberCol);

                var rowOutlineLevel = vs1.RowOutlineLevel(i);

                if (rowOutlineLevel == DeptNode)
                {
                    tempDepartment = Strings.Mid(dept, 1, DeptLength);
                }
                else
                {
                    if (rowOutlineLevel == DivNode)
                    {
                        tempDivision = Strings.Mid(division, 1, DivLength);
                    }
                    else
                    {
                        if (rowOutlineLevel == RevNode)
                        {
                            var shortDesc = vs1.TextMatrix(i, ShortCol);

                            var longDesc = vs1.TextMatrix(i, LongCol);

                            var acct = vs1.TextMatrix(i, AccountCol);

                            if (FCConvert.ToDouble(number) == 0)
                            {
                                // is this a new record
                                if (modAccountTitle.Statics.RevDivFlag)
                                {
                                    rsTesting.OpenRecordset($"SELECT * FROM RevTitles WHERE Department = '{tempDepartment}' AND Revenue = '{revenue}'");
                                }
                                else
                                {
                                    rsTesting.OpenRecordset($"SELECT * FROM RevTitles WHERE Department = '{tempDepartment}' AND Division = '{tempDivision}' AND Revenue = '{revenue}'");
                                }

                                if (rsTesting.EndOfFile() != true && rsTesting.BeginningOfFile() != true)
                                {
                                    if (modAccountTitle.Statics.RevDivFlag)
                                    {
                                        MessageBox.Show($"Saving this information would create duplicate entries for revenue {tempDepartment}-{revenue}.", "Duplicate Revenue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                    else
                                    {
                                        MessageBox.Show($"Saving this information would create duplicate entries for revenue {tempDepartment}-{tempDivision}-{revenue}.", "Duplicate Revenue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }

                                    frmWait.InstancePtr.Unload();

                                    return;
                                }

                                rs.OpenRecordset("SELECT * FROM RevTitles WHERE ID = 0");
                                rs.AddNew();

                                // if so add it
                                rs.Set_Fields("Department", tempDepartment);

                                if (!modAccountTitle.Statics.RevDivFlag)
                                {
                                    rs.Set_Fields("Division", tempDivision);
                                }

                                rs.Set_Fields("Revenue", revenue);
                                rs.Set_Fields("ShortDescription", shortDesc);

                                if (Strings.Trim(longDesc) == "")
                                {
                                    rs.Set_Fields("LongDescription", shortDesc);
                                }
                                else
                                {
                                    rs.Set_Fields("LongDescription", longDesc);
                                }

                                rs.Set_Fields("CloseoutAccount", acct);
                                SaveAccountMasterInfo();
                                rs.Update();

                                // update the database
                                vs1.TextMatrix(i, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                            }
                            else
                            {
                                rs.OpenRecordset($"SELECT * FROM RevTitles WHERE ID = {FCConvert.ToString(Conversion.Val(number))}");
                                rs.Edit();

                                // if not a new record edit the existing record
                                rs.Set_Fields("Department", tempDepartment);

                                if (!modAccountTitle.Statics.RevDivFlag)
                                {
                                    rs.Set_Fields("Division", tempDivision);
                                }

                                rs.Set_Fields("ShortDescription", shortDesc);
                                rs.Set_Fields("LongDescription", Strings.Trim(longDesc) == "" ? shortDesc : longDesc);
                                rs.Set_Fields("Revenue", revenue);
                                rs.Set_Fields("CloseoutAccount", acct);

                                // SaveAccountMasterInfo
                                rs.Update();

                                // update the database
                            }
                        }
                    }
                }
            }

            rsTesting.DisposeOf();
            rs.DisposeOf();
            frmWait.InstancePtr.Unload();
            blnDirty = false;
            //Application.DoEvents();
            MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (blnUnload)
            {
                Close();
            }
        }


        #endregion

        private void vs1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            blnDirty = true;
        }


        private void vs1_Click(object sender, EventArgs e)
        {
            SetEditable();
        }


        private void vs1_SelectionChanged(object sender, EventArgs e)
        {
            
        }


        private void vs1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //SetEditable();
        }

        private void vs1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void vs1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            SetEditable();
        }
    }
}
