﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptReportWriter.
	/// </summary>
	public partial class rptCustomReport : BaseSectionReport
	{
		public rptCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Report Writer";
		}

		public static rptCustomReport InstancePtr
		{
			get
			{
				return (rptCustomReport)Sys.GetInstance(typeof(rptCustomReport));
			}
		}

		protected rptCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRevYTDActivity.Dispose();
				rsRevActivityDetail.Dispose();
				rsExpBudgetInfo.Dispose();
				rsDetailInfo.Dispose();
				rsRevBudgetInfo.Dispose();
				rsExpActivityDetail.Dispose();
				rsExpYTDActivity.Dispose();
				rsLedgerActivityDetail.Dispose();
				rsLedgerBudgetInfo.Dispose();
				rsLedgerYTDActivity.Dispose();
				rsReportInfo.Dispose();
				rsSummaryAccounts.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsReportInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		clsDRWrapper rsSummaryAccounts = new clsDRWrapper();
		// vbPorter upgrade warning: curFinalBudget As Decimal	OnWrite(Decimal, short)
		Decimal curFinalBudget;
		// vbPorter upgrade warning: curFinalCurrent As Decimal	OnWrite(Decimal, short)
		Decimal curFinalCurrent;
		// vbPorter upgrade warning: curFinalCurrentDebits As Decimal	OnWrite(Decimal, short)
		Decimal curFinalCurrentDebits;
		// vbPorter upgrade warning: curFinalCurrentCredits As Decimal	OnWrite(Decimal, short)
		Decimal curFinalCurrentCredits;
		// vbPorter upgrade warning: curFinalYTD As Decimal	OnWrite(Decimal, short)
		Decimal curFinalYTD;
		// vbPorter upgrade warning: curFinalYTDDebits As Decimal	OnWrite(Decimal, short)
		Decimal curFinalYTDDebits;
		// vbPorter upgrade warning: curFinalYTDCredits As Decimal	OnWrite(Decimal, short)
		Decimal curFinalYTDCredits;
		// vbPorter upgrade warning: curFinalBalance As Decimal	OnWrite(Decimal, short)
		Decimal curFinalBalance;
		// vbPorter upgrade warning: curSubBudget As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubBudget = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubCurrent As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubCurrent = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubCurrentDebits As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubCurrentDebits = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubCurrentCredits As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubCurrentCredits = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubYTD As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubYTD = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubYTDDebits As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubYTDDebits = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubYTDCredits As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubYTDCredits = new Decimal[4 + 1];
		// vbPorter upgrade warning: curSubBalance As Decimal	OnWrite(Decimal, short)
		Decimal[] curSubBalance = new Decimal[4 + 1];
		// vbPorter upgrade warning: curBudgetAdjustments As Decimal	OnWriteFCConvert.ToInt16
		Decimal curBudgetAdjustments;
		bool blnFirstRecord;
		clsDRWrapper rsExpYTDActivity = new clsDRWrapper();
		clsDRWrapper rsExpActivityDetail = new clsDRWrapper();
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
		clsDRWrapper rsLedgerActivityDetail = new clsDRWrapper();
		clsDRWrapper rsExpBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsRevBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsLedgerBudgetInfo = new clsDRWrapper();
		bool blnIncludePending;
		bool blnIncludeEnc;
		string strAcctInfo;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strCriteria = "";
			if (blnFirstRecord)
			{
				Check1:
				;
				blnFirstRecord = false;
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A" && Strings.InStr(1, FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), "X", CompareConstants.vbBinaryCompare) != 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("DetailSummary")) == "D")
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strCriteria = CreateLikeString_2(rsDetailInfo.Get_Fields("Account"));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
					{
						rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM ExpenseReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
					{
						rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM RevenueReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
					}
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM LedgerReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
					}
					if (rsSummaryAccounts.EndOfFile() != true && rsSummaryAccounts.BeginningOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						rsDetailInfo.MoveNext();
						if (rsDetailInfo.EndOfFile() != true)
						{
							goto Check1;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				bool executeCheck2 = false;
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A" && FCConvert.ToString(rsDetailInfo.Get_Fields_String("DetailSummary")) == "D" && Strings.InStr(1, FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), "X", CompareConstants.vbBinaryCompare) != 0)
				{
					rsSummaryAccounts.MoveNext();
					if (rsSummaryAccounts.EndOfFile() != true)
					{
						eArgs.EOF = false;
					}
					else
					{
						rsDetailInfo.MoveNext();
						if (rsDetailInfo.EndOfFile() != true)
						{
							executeCheck2 = true;
							goto Check2;
						}
						else
						{
							eArgs.EOF = true;
						}
					}
				}
				else
				{
					rsDetailInfo.MoveNext();
					executeCheck2 = true;
					goto Check2;
				}
				Check2:
				;
				if (executeCheck2)
				{
					if (rsDetailInfo.EndOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A" && Strings.InStr(1, FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), "X", CompareConstants.vbBinaryCompare) != 0 && FCConvert.ToString(rsDetailInfo.Get_Fields_String("DetailSummary")) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strCriteria = CreateLikeString_2(rsDetailInfo.Get_Fields("Account"));
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
							{
								rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM ExpenseReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
							}
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
							{
								rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM RevenueReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
							}
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
							{
								rsSummaryAccounts.OpenRecordset("SELECT DISTINCT Account FROM LedgerReportInfo WHERE Account LIKE '" + strCriteria + "' ORDER BY Account");
							}
							if (rsSummaryAccounts.EndOfFile() != true && rsSummaryAccounts.BeginningOfFile() != true)
							{
								eArgs.EOF = false;
							}
							else
							{
								rsDetailInfo.MoveNext();
								if (rsDetailInfo.EndOfFile() != true)
								{
									goto Check2;
								}
								else
								{
									eArgs.EOF = true;
								}
							}
						}
						else
						{
							eArgs.EOF = false;
						}
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsReportInfo.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(frmCustomReportSelection.InstancePtr.cboReports.Text) + "'");
			lblTitle.Text = rsReportInfo.Get_Fields_String("Title");
			blnIncludePending = FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("IncludePending"));
			blnIncludeEnc = FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("IncludeEnc"));
			strAcctInfo = FCConvert.ToString(rsReportInfo.Get_Fields_String("AcctInfo"));
			if (blnIncludeEnc && blnIncludePending)
			{
				lblIncludes.Text = "Pending Activity and Encumbrance Activity Included";
			}
			else if (blnIncludeEnc)
			{
				lblIncludes.Text = "Encumbrance Activity Included";
			}
			else if (blnIncludePending)
			{
				lblIncludes.Text = "Pending Activity Included";
			}
			else
			{
				lblIncludes.Text = "";
			}
			if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate")));
			}
			else if (rsReportInfo.Get_Fields_String("DateRange") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"))) + " to " + MonthCalc(FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("HighDate")));
			}
			else
			{
				lblMonths.Text = "ALL";
			}
			SetHeadings();
			rsDetailInfo.OpenRecordset("SELECT * FROM CustomReportsDetail WHERE ReportKey = " + rsReportInfo.Get_Fields_Int32("ID") + " ORDER BY Row");
			if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				Cancel();
				this.Close();
				return;
			}
			// 
			// frmWait.Init "Please Wait...." & vbNewLine & "Building Report Data", True
			// frmWait.Show
			// Me.Refresh
			// 
			// frmWait.prgProgress.Value = 0
			// frmWait.Refresh
			// troges126
			frmBusy bWait = new frmBusy();
			FCUtils.StartTask(frmReportViewer.InstancePtr, () =>
			{
				bWait.Message = "Please Wait...." + "\r\n" + "Building Report Data";
				bWait.StartBusy();
				modBudgetaryAccounting.CalculateAccountInfo();
				// CalculateAccountInfo True, True, True, "E"
				// frmWait.prgProgress.Value = 33
				// frmWait.Refresh
				// CalculateAccountInfo True, True, True, "R"
				// frmWait.prgProgress.Value = 66
				// frmWait.Refresh
				// CalculateAccountInfo True, True, True, "G"
				RetrieveInfo();
				// frmWait.prgProgress.Value = 100
				// frmWait.Refresh
				bWait.StopBusy();
				bWait.Unload();
				FCUtils.UnlockUserInterface();
			});
			bWait.Show(FCForm.FormShowEnum.Modal);
			/*- bWait = null; */
			blnFirstRecord = true;
			// Unload frmWait
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsTotalsInfo = new clsDRWrapper())
            {
                string strCriteria = "";
                int counter;
                // vbPorter upgrade warning: curBudget As Decimal	OnWrite(short, Decimal, double)
                Decimal curBudget = 0;
                // vbPorter upgrade warning: curYTDAmount As Decimal	OnWrite(short, Decimal)
                Decimal curYTDAmount = 0;
                // vbPorter upgrade warning: curYTDDebitAmount As Decimal	OnWrite(short, Decimal)
                Decimal curYTDDebitAmount = 0;
                // vbPorter upgrade warning: curYTDCreditAmount As Decimal	OnWrite(short, Decimal)
                Decimal curYTDCreditAmount = 0;
                // vbPorter upgrade warning: curPendingAmount As Decimal	OnWrite(short, Decimal)
                Decimal curPendingAmount = 0;
                // vbPorter upgrade warning: curEncAmount As Decimal	OnWrite(short, Decimal)
                Decimal curEncAmount = 0;
                // vbPorter upgrade warning: curTotal As Decimal	OnWrite(Decimal, double)
                Decimal curTotal = 0;
                // vbPorter upgrade warning: curBalance As Decimal	OnWriteFCConvert.ToInt16
                Decimal curBalance = 0;
                // vbPorter upgrade warning: curCurrent As Decimal	OnWrite(short, Decimal)
                Decimal curCurrent = 0;
                // vbPorter upgrade warning: curCurrentDebit As Decimal	OnWrite(short, Decimal)
                Decimal curCurrentDebit = 0;
                // vbPorter upgrade warning: curCurrentCredit As Decimal	OnWrite(short, Decimal)
                Decimal curCurrentCredit = 0;
                int HighDateCurrent = 0;
                int LowDateCurrent = 0;
                string strPeriodCheckCurrent = "";
                int intSubCounter = 0;
                int HighDate;
                int LowDate;
                string strPeriodCheck = "";
                LowDate = modBudgetaryMaster.Statics.FirstMonth;
                if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "A")
                {
                    LowDateCurrent = modBudgetaryMaster.Statics.FirstMonth;
                    if (modBudgetaryMaster.Statics.FirstMonth == 1)
                    {
                        HighDateCurrent = 12;
                    }
                    else
                    {
                        HighDateCurrent = LowDateCurrent - 1;
                    }

                    if (LowDateCurrent > HighDateCurrent)
                    {
                        strPeriodCheckCurrent = "OR";
                    }
                    else
                    {
                        strPeriodCheckCurrent = "AND";
                    }
                }
                else if (rsReportInfo.Get_Fields_String("DateRange") == "S")
                {
                    HighDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
                    LowDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
                    strPeriodCheckCurrent = "AND";
                }
                else
                {
                    HighDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("HighDate"));
                    LowDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
                    if (LowDateCurrent > HighDateCurrent)
                    {
                        strPeriodCheckCurrent = "OR";
                    }
                    else
                    {
                        strPeriodCheckCurrent = "AND";
                    }
                }

                HighDate = HighDateCurrent;
                if (LowDate > HighDate)
                {
                    strPeriodCheck = "OR";
                }
                else
                {
                    strPeriodCheck = "AND";
                }

                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "H")
                {
                    ShowHeading();
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "S")
                {
                    ShowSubHeading_2(1);
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "S2")
                {
                    ShowSubHeading_2(2);
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "S3")
                {
                    ShowSubHeading_2(3);
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "S4")
                {
                    ShowSubHeading_2(4);
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "F")
                {
                    ShowFinalTotals();
                }
                // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                else if (rsDetailInfo.Get_Fields("Type") == "A")
                {
                    fldTitle.Text = "";
                    fldTotalTitle.Text = "";
                    fldAccountTitle.Font = new Font(fldAccountTitle.Font.Name, 7, FontStyle.Regular);
                    linSubTotal.Visible = false;
                    fldBudget.Font = new Font(fldBudget.Font.Name, 8, FontStyle.Regular);
                    fldCurrent.Font = new Font(fldCurrent.Font.Name, 8, FontStyle.Regular);
                    fldYTD.Font = new Font(fldYTD.Font.Name, 8, FontStyle.Regular);
                    fldCurrentDebits.Font = new Font(fldCurrentDebits.Font.Name, 8, FontStyle.Regular);
                    fldYTDDebits.Font = new Font(fldYTDDebits.Font.Name, 8, FontStyle.Regular);
                    fldCurrentCredits.Font = new Font(fldCurrentCredits.Font.Name, 8, FontStyle.Regular);
                    fldYTDCredits.Font = new Font(fldYTDCredits.Font.Name, 8, FontStyle.Regular);
                    fldBalance.Font = new Font(fldBalance.Font.Name, 8, FontStyle.Regular);
                    fldSpent.Font = new Font(fldSpent.Font.Name, 8, FontStyle.Regular);
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    if (Strings.InStr(1, FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), "X",
                        CompareConstants.vbBinaryCompare) != 0)
                    {
                        if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("DetailSummary")) == "S")
                        {
                            if (FCConvert.ToString(rsReportInfo.Get_Fields_String("AcctInfo")) == "B")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) + "  " +
                                                       rsDetailInfo.Get_Fields_String("Title");
                            }
                            else if (rsReportInfo.Get_Fields_String("AcctInfo") == "N")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text = rsDetailInfo.Get_Fields_String("Account");
                            }
                            else
                            {
                                fldAccountTitle.Text = rsDetailInfo.Get_Fields_String("Title");
                            }

                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            strCriteria = CreateLikeString_2(rsDetailInfo.Get_Fields("Account"));
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
                            {
                                rsTotalsInfo.OpenRecordset(
                                    "SELECT SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal, Period FROM ExpenseReportInfo WHERE Account LIKE '" +
                                    strCriteria + "' GROUP BY Period");
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
                            {
                                rsTotalsInfo.OpenRecordset(
                                    "SELECT SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal, Period FROM RevenueReportInfo WHERE Account LIKE '" +
                                    strCriteria + "' GROUP BY Period");
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
                            {
                                rsTotalsInfo.OpenRecordset(
                                    "SELECT SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal, Period FROM LedgerReportInfo WHERE Account LIKE '" +
                                    strCriteria + "' GROUP BY Period");
                            }

                            curYTDAmount = 0;
                            curYTDDebitAmount = 0;
                            curYTDCreditAmount = 0;
                            curCurrentDebit = 0;
                            curCurrentCredit = 0;
                            curPendingAmount = 0;
                            curEncAmount = 0;
                            curBudget = 0;
                            curBalance = 0;
                            curCurrent = 0;
                            curBudgetAdjustments = 0;
                            if (rsTotalsInfo.EndOfFile() != true && rsTotalsInfo.BeginningOfFile() != true)
                            {
                                while (rsTotalsInfo.EndOfFile() != true)
                                {
                                    if (LowDate <= HighDate)
                                    {
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        if ((FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) >= LowDate &&
                                             FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) <= HighDate) ||
                                            FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) == 0)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curYTDDebitAmount += rsTotalsInfo.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curYTDCreditAmount += rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curYTDAmount +=
                                                rsTotalsInfo.Get_Fields("PostedDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                            curPendingAmount +=
                                                rsTotalsInfo.Get_Fields("PendingDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PendingCreditsTotal");
                                            if (blnIncludePending)
                                            {
                                                // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                                curYTDDebitAmount += rsTotalsInfo.Get_Fields("PendingDebitsTotal");
                                                // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                                curYTDCreditAmount += rsTotalsInfo.Get_Fields("PendingCreditsTotal");
                                            }

                                            // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                            curEncAmount += rsTotalsInfo.Get_Fields("EncumbActivityTotal");
                                            if (blnIncludeEnc)
                                            {
                                                // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                                curYTDDebitAmount += rsTotalsInfo.Get_Fields("EncumbActivityTotal");
                                            }

                                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                            if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")),
                                                    1) == "E" ||
                                                Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")),
                                                    1) == "P")
                                            {
                                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                curBudget +=
                                                    rsTotalsInfo.Get_Fields("OriginalBudgetTotal") -
                                                    rsTotalsInfo.Get_Fields("BudgetAdjustmentsTotal");
                                            }
                                            else
                                            {
                                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                curBudget +=
                                                    rsTotalsInfo.Get_Fields("OriginalBudgetTotal") +
                                                    rsTotalsInfo.Get_Fields("BudgetAdjustmentsTotal");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        if ((FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) >= LowDate ||
                                             FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) <= HighDate) ||
                                            FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) == 0)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curYTDDebitAmount += rsTotalsInfo.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curYTDCreditAmount += rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curYTDAmount +=
                                                rsTotalsInfo.Get_Fields("PostedDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                            curPendingAmount +=
                                                rsTotalsInfo.Get_Fields("PendingDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PendingCreditsTotal");
                                            if (blnIncludePending)
                                            {
                                                // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                                curYTDDebitAmount += rsTotalsInfo.Get_Fields("PendingDebitsTotal");
                                                // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                                curYTDCreditAmount += rsTotalsInfo.Get_Fields("PendingCreditsTotal");
                                            }

                                            // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                            curEncAmount += rsTotalsInfo.Get_Fields("EncumbActivityTotal");
                                            if (blnIncludeEnc)
                                            {
                                                // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                                curYTDDebitAmount += rsTotalsInfo.Get_Fields("EncumbActivityTotal");
                                            }

                                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                            if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")),
                                                    1) == "E" ||
                                                Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")),
                                                    1) == "P")
                                            {
                                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                curBudget +=
                                                    rsTotalsInfo.Get_Fields("OriginalBudgetTotal") -
                                                    rsTotalsInfo.Get_Fields("BudgetAdjustmentsTotal");
                                            }
                                            else
                                            {
                                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                curBudget +=
                                                    rsTotalsInfo.Get_Fields("OriginalBudgetTotal") +
                                                    rsTotalsInfo.Get_Fields("BudgetAdjustmentsTotal");
                                            }
                                        }
                                    }

                                    if (LowDateCurrent <= HighDateCurrent)
                                    {
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        if (FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) >= LowDateCurrent &&
                                            FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) <= HighDateCurrent)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curCurrent +=
                                                rsTotalsInfo.Get_Fields("PostedDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curCurrentDebit += rsTotalsInfo.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curCurrentCredit += rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                        }
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                                        if (FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) >= LowDateCurrent ||
                                            FCConvert.ToInt32(rsTotalsInfo.Get_Fields("Period")) <= HighDateCurrent)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curCurrent +=
                                                rsTotalsInfo.Get_Fields("PostedDebitsTotal") +
                                                rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curCurrentDebit += rsTotalsInfo.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curCurrentCredit += rsTotalsInfo.Get_Fields("PostedCreditsTotal");
                                        }
                                    }

                                    rsTotalsInfo.MoveNext();
                                }

                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E" ||
                                    Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "P")
                                {
                                    curBudget *= -1;
                                }

                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldCurrent.Text = Strings.Format(curCurrent, "#,##0.00");
                                curFinalCurrent += curCurrent;
                                fldCurrentDebits.Text = Strings.Format(curCurrentDebit, "#,##0.00");
                                curFinalCurrentDebits += curCurrentDebit;
                                fldCurrentCredits.Text = Strings.Format(curCurrentCredit, "#,##0.00");
                                curFinalCurrentCredits += curCurrentCredit;
                                curTotal = curYTDAmount;
                                if (blnIncludePending)
                                {
                                    curTotal += curPendingAmount;
                                }

                                if (blnIncludeEnc)
                                {
                                    curTotal += curEncAmount;
                                }

                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                curFinalYTD += curTotal;
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                curFinalYTDDebits += curYTDDebitAmount;
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubCurrent[counter] += curCurrent;
                                    curSubCurrentDebits[counter] += curCurrentDebit;
                                    curSubCurrentCredits[counter] += curCurrentCredit;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            else
                            {
                                fldBudget.Text = "0.00";
                                fldCurrent.Text = "0.00";
                                fldCurrentDebits.Text = "0.00";
                                fldCurrentCredits.Text = "0.00";
                                fldYTD.Text = "0.00";
                                fldYTDDebits.Text = "0.00";
                                fldYTDCredits.Text = "0.00";
                                fldBalance.Text = "0.00";
                                fldSpent.Text = "0.00";
                            }
                        }
                        else
                        {
                            if (FCConvert.ToString(rsReportInfo.Get_Fields_String("AcctInfo")) == "B")
                            {
                                if (Strings.Trim(FCConvert.ToString(rsDetailInfo.Get_Fields_String("Title"))) != "")
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    fldAccountTitle.Text =
                                        FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")) + "  " +
                                        rsDetailInfo.Get_Fields_String("Title");
                                }
                                else
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    fldAccountTitle.Text =
                                        FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")) + "  " +
                                        modAccountTitle.ReturnAccountDescription(
                                            FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")));
                                }
                            }
                            else if (rsReportInfo.Get_Fields_String("AcctInfo") == "N")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text = rsSummaryAccounts.Get_Fields_String("Account");
                            }
                            else
                            {
                                if (Strings.Trim(FCConvert.ToString(rsDetailInfo.Get_Fields_String("Title"))) != "")
                                {
                                    fldAccountTitle.Text = rsDetailInfo.Get_Fields_String("Title");
                                }
                                else
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    fldAccountTitle.Text =
                                        modAccountTitle.ReturnAccountDescription(
                                            rsSummaryAccounts.Get_Fields("Account"));
                                }
                            }

                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (Strings.Left(FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")), 1) == "E")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsExpYTDActivity.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsExpActivityDetail.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrent.Text =
                                            Strings.Format(
                                                rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsExpActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentDebits.Text =
                                            Strings.Format(rsExpActivityDetail.Get_Fields("PostedDebitsTotal"),
                                                "#,##0.00");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentCredits.Text =
                                            Strings.Format(rsExpActivityDetail.Get_Fields("PostedCreditsTotal"),
                                                "#,##0.00");
                                        for (counter = 1; counter <= 4; counter++)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrent[counter] +=
                                                rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentDebits[counter] +=
                                                rsExpActivityDetail.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentCredits[counter] +=
                                                rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                        }

                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrent +=
                                            rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentDebits += rsExpActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentCredits += rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }
                                    else
                                    {
                                        fldCurrent.Text = "0.00";
                                        fldCurrentDebits.Text = "0.00";
                                        fldCurrentCredits.Text = "0.00";
                                    }

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsExpBudgetInfo.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = FCConvert.ToDecimal(
                                            rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal") -
                                            rsExpYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = 0 - FCConvert.ToInt32(
                                            rsExpYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal = FCConvert.ToDecimal(
                                        rsExpYTDActivity.Get_Fields("PostedDebitsTotal") +
                                        rsExpYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount =
                                        FCConvert.ToDecimal(rsExpYTDActivity.Get_Fields("PostedDebitsTotal"));
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount =
                                        FCConvert.ToDecimal(rsExpYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    if (blnIncludePending)
                                    {
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsExpYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                    rsExpYTDActivity.Get_Fields("PendingCreditsTotal");
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsExpYTDActivity.Get_Fields("PendingDebitsTotal");
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curYTDCreditAmount += rsExpYTDActivity.Get_Fields("PendingCreditsTotal");
                                    }

                                    if (blnIncludeEnc)
                                    {
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsExpYTDActivity.Get_Fields("EncumbActivityTotal");
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsExpYTDActivity.Get_Fields("EncumbActivityTotal");
                                    }

                                    curBudget *= -1;
                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                else if (rsExpBudgetInfo.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                    curBudget *= -1;
                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                else
                                {
                                    fldBudget.Text = "0.00";
                                    fldCurrent.Text = "0.00";
                                    fldYTD.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldYTDDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                    fldYTDCredits.Text = "0.00";
                                    fldBalance.Text = "0.00";
                                    fldSpent.Text = "0.00";
                                }
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (Strings.Left(FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")), 1) ==
                                     "R")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsRevYTDActivity.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsRevActivityDetail.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrent.Text =
                                            Strings.Format(
                                                rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsRevActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentDebits.Text =
                                            Strings.Format(rsRevActivityDetail.Get_Fields("PostedDebitsTotal"),
                                                "#,##0.00");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentCredits.Text =
                                            Strings.Format(rsRevActivityDetail.Get_Fields("PostedCreditsTotal"),
                                                "#,##0.00");
                                        for (counter = 1; counter <= 4; counter++)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrent[counter] +=
                                                rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentDebits[counter] +=
                                                rsRevActivityDetail.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentCredits[counter] +=
                                                rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                        }

                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrent +=
                                            rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentDebits += rsRevActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentCredits += rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }
                                    else
                                    {
                                        fldCurrent.Text = "0.00";
                                        fldCurrentDebits.Text = "0.00";
                                        fldCurrentCredits.Text = "0.00";
                                    }

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsRevBudgetInfo.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = FCConvert.ToDecimal(
                                            rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal") +
                                            rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = 0 + FCConvert.ToInt32(
                                            rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal = FCConvert.ToDecimal(
                                        rsRevYTDActivity.Get_Fields("PostedDebitsTotal") +
                                        rsRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount =
                                        FCConvert.ToDecimal(rsRevYTDActivity.Get_Fields("PostedDebitsTotal"));
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount =
                                        FCConvert.ToDecimal(rsRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    if (blnIncludePending)
                                    {
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsRevYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                    rsRevYTDActivity.Get_Fields("PendingCreditsTotal");
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsRevYTDActivity.Get_Fields("PendingDebitsTotal");
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curYTDCreditAmount += rsRevYTDActivity.Get_Fields("PendingCreditsTotal");
                                    }

                                    if (blnIncludeEnc)
                                    {
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
                                    }

                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                else if (rsRevBudgetInfo.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                else
                                {
                                    fldBudget.Text = "0.00";
                                    fldCurrent.Text = "0.00";
                                    fldYTD.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldYTDDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                    fldYTDCredits.Text = "0.00";
                                    fldBalance.Text = "0.00";
                                    fldSpent.Text = "0.00";
                                }
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (Strings.Left(FCConvert.ToString(rsSummaryAccounts.Get_Fields("Account")), 1) ==
                                     "G")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsLedgerYTDActivity.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsLedgerActivityDetail.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrent.Text =
                                            Strings.Format(
                                                rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentDebits.Text =
                                            Strings.Format(rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal"),
                                                "#,##0.00");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        fldCurrentCredits.Text =
                                            Strings.Format(rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal"),
                                                "#,##0.00");
                                        for (counter = 1; counter <= 4; counter++)
                                        {
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrent[counter] +=
                                                rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                            // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentDebits[counter] +=
                                                rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal");
                                            // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                            curSubCurrentCredits[counter] +=
                                                rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                        }

                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrent +=
                                            rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentDebits += rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curFinalCurrentCredits +=
                                            rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }
                                    else
                                    {
                                        fldCurrent.Text = "0.00";
                                        fldCurrentDebits.Text = "0.00";
                                        fldCurrentCredits.Text = "0.00";
                                    }

                                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (rsLedgerBudgetInfo.FindFirstRecord("Account",
                                        rsSummaryAccounts.Get_Fields("Account")))
                                    {
                                        // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = FCConvert.ToDecimal(
                                            rsLedgerBudgetInfo.Get_Fields("OriginalBudgetTotal") +
                                            rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }
                                    else
                                    {
                                        // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        curBudget = 0 + FCConvert.ToInt32(
                                            rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal = FCConvert.ToDecimal(
                                        rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal") +
                                        rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount =
                                        FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal"));
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount =
                                        FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    if (blnIncludePending)
                                    {
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsLedgerYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                    rsLedgerYTDActivity.Get_Fields("PendingCreditsTotal");
                                        // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsLedgerYTDActivity.Get_Fields("PendingDebitsTotal");
                                        // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                        curYTDCreditAmount += rsLedgerYTDActivity.Get_Fields("PendingCreditsTotal");
                                    }

                                    if (blnIncludeEnc)
                                    {
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curTotal += rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal");
                                        // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                        curYTDDebitAmount += rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal");
                                    }

                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                else if (rsLedgerBudgetInfo.FindFirstRecord("Account",
                                    rsSummaryAccounts.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(
                                        rsLedgerBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                    fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                    curFinalBudget += curBudget;
                                    fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                    fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                    fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                    curFinalYTD += curTotal;
                                    curFinalYTDDebits += curYTDDebitAmount;
                                    curFinalYTDCredits += curYTDCreditAmount;
                                    fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                    curFinalBalance += curBudget + curTotal;
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        curSubBudget[counter] += curBudget;
                                        curSubYTD[counter] += curTotal;
                                        curSubYTDDebits[counter] += curYTDDebitAmount;
                                        curSubYTDCredits[counter] += curYTDCreditAmount;
                                        curSubBalance[counter] += curBudget + curTotal;
                                    }

                                    if (curBudget != 0)
                                    {
                                        fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                        if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                        {
                                            fldSpent.Text = "999.99";
                                        }
                                        else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                        {
                                            fldSpent.Text = "-999.99";
                                        }
                                    }
                                    else
                                    {
                                        fldSpent.Text = "0.00";
                                    }
                                }
                                else
                                {
                                    fldBudget.Text = "0.00";
                                    fldCurrent.Text = "0.00";
                                    fldYTD.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldYTDDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                    fldYTDCredits.Text = "0.00";
                                    fldBalance.Text = "0.00";
                                    fldSpent.Text = "0.00";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (FCConvert.ToString(rsReportInfo.Get_Fields_String("AcctInfo")) == "B")
                        {
                            if (Strings.Trim(FCConvert.ToString(rsDetailInfo.Get_Fields_String("Title"))) != "")
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) + "  " +
                                                       rsDetailInfo.Get_Fields_String("Title");
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("Account")) + "  " +
                                                       modAccountTitle.ReturnAccountDescription(
                                                           FCConvert.ToString(rsDetailInfo.Get_Fields("Account")));
                            }
                        }
                        else if (rsReportInfo.Get_Fields_String("AcctInfo") == "N")
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            fldAccountTitle.Text = rsDetailInfo.Get_Fields_String("Account");
                        }
                        else
                        {
                            if (Strings.Trim(FCConvert.ToString(rsDetailInfo.Get_Fields_String("Title"))) != "")
                            {
                                fldAccountTitle.Text = rsDetailInfo.Get_Fields_String("Title");
                            }
                            else
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                fldAccountTitle.Text =
                                    modAccountTitle.ReturnAccountDescription(rsDetailInfo.Get_Fields("Account"));
                            }
                        }

                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E")
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (rsExpYTDActivity.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsExpActivityDetail.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrent.Text =
                                        Strings.Format(
                                            rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsExpActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentDebits.Text =
                                        Strings.Format(rsExpActivityDetail.Get_Fields("PostedDebitsTotal"), "#,##0.00");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentCredits.Text =
                                        Strings.Format(rsExpActivityDetail.Get_Fields("PostedCreditsTotal"),
                                            "#,##0.00");
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrent[counter] +=
                                            rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentDebits[counter] +=
                                            rsExpActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentCredits[counter] +=
                                            rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrent += rsExpActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                       rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentDebits += rsExpActivityDetail.Get_Fields("PostedDebitsTotal");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentCredits += rsExpActivityDetail.Get_Fields("PostedCreditsTotal");
                                }
                                else
                                {
                                    fldCurrent.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                }

                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsExpBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(
                                        rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal") -
                                        rsExpYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }
                                else
                                {
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = 0 - FCConvert.ToInt32(
                                        rsExpYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }

                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curTotal = FCConvert.ToDecimal(
                                    rsExpYTDActivity.Get_Fields("PostedDebitsTotal") +
                                    rsExpYTDActivity.Get_Fields("PostedCreditsTotal"));
                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                curYTDDebitAmount =
                                    FCConvert.ToDecimal(rsExpYTDActivity.Get_Fields("PostedDebitsTotal"));
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curYTDCreditAmount =
                                    FCConvert.ToDecimal(rsExpYTDActivity.Get_Fields("PostedCreditsTotal"));
                                if (blnIncludePending)
                                {
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsExpYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                rsExpYTDActivity.Get_Fields("PendingCreditsTotal");
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsExpYTDActivity.Get_Fields("PendingDebitsTotal");
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount += rsExpYTDActivity.Get_Fields("PendingCreditsTotal");
                                }

                                if (blnIncludeEnc)
                                {
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsExpYTDActivity.Get_Fields("EncumbActivityTotal");
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsExpYTDActivity.Get_Fields("EncumbActivityTotal");
                                }

                                curBudget *= -1;
                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (rsExpBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                curBudget = FCConvert.ToDecimal(rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                curBudget *= -1;
                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            else
                            {
                                fldBudget.Text = "0.00";
                                fldCurrent.Text = "0.00";
                                fldYTD.Text = "0.00";
                                fldCurrentDebits.Text = "0.00";
                                fldYTDDebits.Text = "0.00";
                                fldCurrentCredits.Text = "0.00";
                                fldYTDCredits.Text = "0.00";
                                fldBalance.Text = "0.00";
                                fldSpent.Text = "0.00";
                            }
                        }
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R")
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (rsRevYTDActivity.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsRevActivityDetail.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrent.Text =
                                        Strings.Format(
                                            rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsRevActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentDebits.Text =
                                        Strings.Format(rsRevActivityDetail.Get_Fields("PostedDebitsTotal"), "#,##0.00");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentCredits.Text =
                                        Strings.Format(rsRevActivityDetail.Get_Fields("PostedCreditsTotal"),
                                            "#,##0.00");
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrent[counter] +=
                                            rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentDebits[counter] +=
                                            rsRevActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentCredits[counter] +=
                                            rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrent += rsRevActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                       rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentDebits += rsRevActivityDetail.Get_Fields("PostedDebitsTotal");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentCredits += rsRevActivityDetail.Get_Fields("PostedCreditsTotal");
                                }
                                else
                                {
                                    fldCurrent.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                }

                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsRevBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(
                                        rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal") +
                                        rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }
                                else
                                {
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = 0 + FCConvert.ToInt32(
                                        rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }

                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curTotal = FCConvert.ToDecimal(
                                    rsRevYTDActivity.Get_Fields("PostedDebitsTotal") +
                                    rsRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                curYTDDebitAmount =
                                    FCConvert.ToDecimal(rsRevYTDActivity.Get_Fields("PostedDebitsTotal"));
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curYTDCreditAmount =
                                    FCConvert.ToDecimal(rsRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                if (blnIncludePending)
                                {
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsRevYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                rsRevYTDActivity.Get_Fields("PendingCreditsTotal");
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsRevYTDActivity.Get_Fields("PendingDebitsTotal");
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount += rsRevYTDActivity.Get_Fields("PendingCreditsTotal");
                                }

                                if (blnIncludeEnc)
                                {
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
                                }

                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (rsRevBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                curBudget = FCConvert.ToDecimal(rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            else
                            {
                                fldBudget.Text = "0.00";
                                fldCurrent.Text = "0.00";
                                fldYTD.Text = "0.00";
                                fldCurrentDebits.Text = "0.00";
                                fldYTDDebits.Text = "0.00";
                                fldCurrentCredits.Text = "0.00";
                                fldYTDCredits.Text = "0.00";
                                fldBalance.Text = "0.00";
                                fldSpent.Text = "0.00";
                            }
                        }
                        // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                        else if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            if (rsLedgerYTDActivity.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsLedgerActivityDetail.FindFirstRecord("Account",
                                    rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrent.Text =
                                        Strings.Format(
                                            rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal"), "#,##0.00");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentDebits.Text =
                                        Strings.Format(rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal"),
                                            "#,##0.00");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    fldCurrentCredits.Text =
                                        Strings.Format(rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal"),
                                            "#,##0.00");
                                    for (counter = 1; counter <= 4; counter++)
                                    {
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrent[counter] +=
                                            rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                            rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                        // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentDebits[counter] +=
                                            rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal");
                                        // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curSubCurrentCredits[counter] +=
                                            rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                    }

                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrent += rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal") +
                                                       rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                    // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentDebits += rsLedgerActivityDetail.Get_Fields("PostedDebitsTotal");
                                    // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                    curFinalCurrentCredits += rsLedgerActivityDetail.Get_Fields("PostedCreditsTotal");
                                }
                                else
                                {
                                    fldCurrent.Text = "0.00";
                                    fldCurrentDebits.Text = "0.00";
                                    fldCurrentCredits.Text = "0.00";
                                }

                                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                                if (rsLedgerBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                                {
                                    // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = FCConvert.ToDecimal(
                                        rsLedgerBudgetInfo.Get_Fields("OriginalBudgetTotal") +
                                        rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }
                                else
                                {
                                    // TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                    curBudget = 0 + FCConvert.ToInt32(
                                        rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
                                }

                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curTotal = FCConvert.ToDecimal(
                                    rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal") +
                                    rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal"));
                                // TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                curYTDDebitAmount =
                                    FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal"));
                                // TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                curYTDCreditAmount =
                                    FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal"));
                                if (blnIncludePending)
                                {
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsLedgerYTDActivity.Get_Fields("PendingDebitsTotal") +
                                                rsLedgerYTDActivity.Get_Fields("PendingCreditsTotal");
                                    // TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsLedgerYTDActivity.Get_Fields("PendingDebitsTotal");
                                    // TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
                                    curYTDCreditAmount += rsLedgerYTDActivity.Get_Fields("PendingCreditsTotal");
                                }

                                if (blnIncludeEnc)
                                {
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curTotal += rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal");
                                    // TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                                    curYTDDebitAmount += rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal");
                                }

                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            else if (rsLedgerBudgetInfo.FindFirstRecord("Account", rsDetailInfo.Get_Fields("Account")))
                            {
                                // TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                curBudget = FCConvert.ToDecimal(rsLedgerBudgetInfo.Get_Fields("OriginalBudgetTotal"));
                                fldBudget.Text = Strings.Format(curBudget, "#,##0.00");
                                curFinalBudget += curBudget;
                                fldYTD.Text = Strings.Format(curTotal, "#,##0.00");
                                fldYTDDebits.Text = Strings.Format(curYTDDebitAmount, "#,##0.00");
                                fldYTDCredits.Text = Strings.Format(curYTDCreditAmount, "#,##0.00");
                                curFinalYTD += curTotal;
                                curFinalYTDDebits += curYTDDebitAmount;
                                curFinalYTDCredits += curYTDCreditAmount;
                                fldBalance.Text = Strings.Format(curBudget + curTotal, "#,##0.00");
                                curFinalBalance += curBudget + curTotal;
                                for (counter = 1; counter <= 4; counter++)
                                {
                                    curSubBudget[counter] += curBudget;
                                    curSubYTD[counter] += curTotal;
                                    curSubYTDDebits[counter] += curYTDDebitAmount;
                                    curSubYTDCredits[counter] += curYTDCreditAmount;
                                    curSubBalance[counter] += curBudget + curTotal;
                                }

                                if (curBudget != 0)
                                {
                                    fldSpent.Text = Strings.Format((curTotal / curBudget) * -100, "0.00");
                                    if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
                                    {
                                        fldSpent.Text = "999.99";
                                    }
                                    else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
                                    {
                                        fldSpent.Text = "-999.99";
                                    }
                                }
                                else
                                {
                                    fldSpent.Text = "0.00";
                                }
                            }
                            else
                            {
                                fldBudget.Text = "0.00";
                                fldCurrent.Text = "0.00";
                                fldYTD.Text = "0.00";
                                fldCurrentDebits.Text = "0.00";
                                fldYTDDebits.Text = "0.00";
                                fldCurrentCredits.Text = "0.00";
                                fldYTDCredits.Text = "0.00";
                                fldBalance.Text = "0.00";
                                fldSpent.Text = "0.00";
                            }
                        }
                    }
                }
            }
        }

		private void ShowFinalTotals()
		{
			fldTitle.Text = "";
			fldAccountTitle.Text = "";
			fldTotalTitle.Font = new Font(fldTotalTitle.Font.Name, 9, FontStyle.Bold);
			linSubTotal.Visible = true;
			fldBudget.Font = new Font(fldBudget.Font.Name, 9, FontStyle.Bold);
			fldCurrent.Font = new Font(fldCurrent.Font.Name, 9, FontStyle.Bold);
			fldYTD.Font = new Font(fldYTD.Font.Name, 9, FontStyle.Bold);
			fldCurrentDebits.Font = new Font(fldCurrentDebits.Font.Name, 9, FontStyle.Bold);
			fldYTDDebits.Font = new Font(fldYTDDebits.Font.Name, 9, FontStyle.Bold);
			fldCurrentCredits.Font = new Font(fldCurrentCredits.Font.Name, 9, FontStyle.Bold);
			fldYTDCredits.Font = new Font(fldYTDCredits.Font.Name, 9, FontStyle.Bold);
			fldBalance.Font = new Font(fldBalance.Font.Name, 9, FontStyle.Bold);
			fldSpent.Font = new Font(fldSpent.Font.Name, 9, FontStyle.Bold);
			fldTotalTitle.Text = rsDetailInfo.Get_Fields_String("Title");
			fldBudget.Text = Strings.Format(curFinalBudget, "#,##0.00");
			fldCurrent.Text = Strings.Format(curFinalCurrent, "#,##0.00");
			fldYTD.Text = Strings.Format(curFinalYTD, "#,##0.00");
			fldCurrentDebits.Text = Strings.Format(curFinalCurrentDebits, "#,##0.00");
			fldCurrentCredits.Text = Strings.Format(curFinalCurrentCredits, "#,##0.00");
			fldYTDDebits.Text = Strings.Format(curFinalYTDDebits, "#,##0.00");
			fldYTDCredits.Text = Strings.Format(curFinalYTDCredits, "#,##0.00");
			fldBalance.Text = Strings.Format(curFinalBalance, "#,##0.00");
			if (curFinalBudget != 0)
			{
				fldSpent.Text = Strings.Format((curFinalYTD / curFinalBudget) * -100, "0.00");
				if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
				{
					fldSpent.Text = "999.99";
				}
				else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
				{
					fldSpent.Text = "-999.99";
				}
			}
			else
			{
				fldSpent.Text = "0.00";
			}
			curFinalBudget = 0;
			curFinalCurrent = 0;
			curFinalYTD = 0;
			curFinalCurrentDebits = 0;
			curFinalYTDCredits = 0;
			curFinalCurrentCredits = 0;
			curFinalYTDDebits = 0;
			curFinalBalance = 0;
		}

		private void ShowSubHeading_2(short intIndex)
		{
			ShowSubHeading(ref intIndex);
		}

		private void ShowSubHeading(ref short intIndex)
		{
			fldTotalTitle.Font = new Font(fldTotalTitle.Font.Name, 8, FontStyle.Bold);
			linSubTotal.Visible = true;
			fldBudget.Font = new Font(fldBudget.Font.Name, 8, FontStyle.Bold);
			fldCurrent.Font = new Font(fldCurrent.Font.Name, 8, FontStyle.Bold);
			fldYTD.Font = new Font(fldYTD.Font.Name, 8, FontStyle.Bold);
			fldCurrentDebits.Font = new Font(fldCurrentDebits.Font.Name, 8, FontStyle.Bold);
			fldYTDDebits.Font = new Font(fldYTDDebits.Font.Name, 8, FontStyle.Bold);
			fldCurrentCredits.Font = new Font(fldCurrentCredits.Font.Name, 8, FontStyle.Bold);
			fldYTDCredits.Font = new Font(fldYTDCredits.Font.Name, 8, FontStyle.Bold);
			fldBalance.Font = new Font(fldBalance.Font.Name, 8, FontStyle.Bold);
			fldSpent.Font = new Font(fldSpent.Font.Name, 8, FontStyle.Bold);
			fldTotalTitle.Text = rsDetailInfo.Get_Fields_String("Title");
			fldTitle.Text = "";
			fldAccountTitle.Text = "";
			fldBudget.Text = Strings.Format(curSubBudget[intIndex], "#,##0.00");
			fldCurrent.Text = Strings.Format(curSubCurrent[intIndex], "#,##0.00");
			fldYTD.Text = Strings.Format(curSubYTD[intIndex], "#,##0.00");
			fldCurrentDebits.Text = Strings.Format(curSubCurrentDebits[intIndex], "#,##0.00");
			fldCurrentCredits.Text = Strings.Format(curSubCurrentCredits[intIndex], "#,##0.00");
			fldYTDDebits.Text = Strings.Format(curSubYTDDebits[intIndex], "#,##0.00");
			fldYTDCredits.Text = Strings.Format(curSubYTDCredits[intIndex], "#,##0.00");
			fldBalance.Text = Strings.Format(curSubBalance[intIndex], "#,##0.00");
			if (curSubBudget[intIndex] != 0)
			{
				fldSpent.Text = Strings.Format((curSubYTD[intIndex] / curSubBudget[intIndex]) * -100, "0.00");
				if (FCConvert.ToDouble(fldSpent.Text) > 999.99)
				{
					fldSpent.Text = "999.99";
				}
				else if (FCConvert.ToDouble(fldSpent.Text) < -999.99)
				{
					fldSpent.Text = "-999.99";
				}
			}
			else
			{
				fldSpent.Text = "0.00";
			}
			curSubBudget[intIndex] = 0;
			curSubCurrent[intIndex] = 0;
			curSubYTD[intIndex] = 0;
			curSubCurrentDebits[intIndex] = 0;
			curSubCurrentCredits[intIndex] = 0;
			curSubYTDDebits[intIndex] = 0;
			curSubYTDCredits[intIndex] = 0;
			curSubBalance[intIndex] = 0;
		}

		private void ShowHeading()
		{
			fldTitle.Font = new Font(fldTitle.Font.Name, 9, FontStyle.Bold);
			linSubTotal.Visible = false;
			fldTitle.Text = rsDetailInfo.Get_Fields_String("Title");
			fldBudget.Text = "";
			fldCurrent.Text = "";
			fldCurrentDebits.Text = "";
			fldCurrentCredits.Text = "";
			fldYTD.Text = "";
			fldYTDDebits.Text = "";
			fldYTDCredits.Text = "";
			fldSpent.Text = "";
			fldTotalTitle.Text = "";
			fldAccountTitle.Text = "";
			fldBalance.Text = "";
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private string CreateLikeString_2(string strAccount)
		{
			return CreateLikeString(ref strAccount);
		}

		private string CreateLikeString(ref string strAccount)
		{
			string CreateLikeString = "";
			int counter;
			CreateLikeString = strAccount.Replace("X", "%");
			while (Strings.InStr(1, CreateLikeString, "%%", CompareConstants.vbBinaryCompare) != 0)
			{
				CreateLikeString = CreateLikeString.Replace("%%", "%");
			}
			return CreateLikeString;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent = 0;
			int LowDateCurrent = 0;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "A")
			{
				HighDateCurrent = HighDate;
				LowDateCurrent = LowDate;
				strPeriodCheckCurrent = strPeriodCheck;
			}
			else if (rsReportInfo.Get_Fields_String("DateRange") == "S")
			{
				HighDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
				LowDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
				strPeriodCheckCurrent = "AND";
			}
			else
			{
				HighDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("HighDate"));
				LowDateCurrent = FCConvert.ToInt32(rsReportInfo.Get_Fields_Int32("LowDate"));
				if (LowDateCurrent > HighDateCurrent)
				{
					strPeriodCheckCurrent = "OR";
				}
				else
				{
					strPeriodCheckCurrent = "AND";
				}
			}
			rsExpBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsRevBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsLedgerBudgetInfo.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			HighDate = HighDateCurrent;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rsExpYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsExpActivityDetail.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period >= " + FCConvert.ToString(LowDateCurrent) + strPeriodCheckCurrent + " Period <= " + FCConvert.ToString(HighDateCurrent) + ") GROUP BY Account");
			rsRevActivityDetail.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period >= " + FCConvert.ToString(LowDateCurrent) + strPeriodCheckCurrent + " Period <= " + FCConvert.ToString(HighDateCurrent) + ") GROUP BY Account");
			rsLedgerActivityDetail.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period >= " + FCConvert.ToString(LowDateCurrent) + strPeriodCheckCurrent + " Period <= " + FCConvert.ToString(HighDateCurrent) + ") GROUP BY Account");
		}

		private void SetHeadings()
		{
			int intFieldCount;
			intFieldCount = 0;
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("Budget")))
			{
				lblHeading1.Visible = true;
				fldBudget.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading1.Visible = false;
				fldBudget.Visible = false;
			}
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CurrentDebCred")))
			{
				lblHeading8.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldCurrentDebits.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading8.Visible = true;
				fldCurrentDebits.Visible = true;
				intFieldCount += 1;
				lblHeading9.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldCurrentCredits.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading9.Visible = true;
				fldCurrentCredits.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading8.Visible = false;
				fldCurrentDebits.Visible = false;
				lblHeading9.Visible = false;
				fldCurrentCredits.Visible = false;
			}
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CurrentActivity")))
			{
				lblHeading2.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldCurrent.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading2.Visible = true;
				fldCurrent.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading2.Visible = false;
				fldCurrent.Visible = false;
			}
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("YTDDebCred")))
			{
				lblHeading6.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldYTDDebits.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading6.Visible = true;
				fldYTDDebits.Visible = true;
				intFieldCount += 1;
				lblHeading7.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldYTDCredits.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading7.Visible = true;
				fldYTDCredits.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading6.Visible = false;
				fldYTDDebits.Visible = false;
				lblHeading7.Visible = false;
				fldYTDCredits.Visible = false;
			}
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("YTDActivity")))
			{
				lblHeading3.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldYTD.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading3.Visible = true;
				fldYTD.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading3.Visible = false;
				fldYTD.Visible = false;
			}
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields("Balance")))
			{
				lblHeading4.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldBalance.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading4.Visible = true;
				fldBalance.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading4.Visible = false;
				fldBalance.Visible = false;
			}
			if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("PercentSpent")))
			{
				lblHeading5.Left = lblHeading1.Left + (1395 * intFieldCount) / 1440f;
				fldSpent.Left = fldBudget.Left + (1395 * intFieldCount) / 1440f;
				lblHeading5.Visible = true;
				fldSpent.Visible = true;
				intFieldCount += 1;
			}
			else
			{
				lblHeading5.Visible = false;
				fldSpent.Visible = false;
			}
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		
	}
}
