﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBDAccountDetail.
	/// </summary>
	public partial class rptBDAccountDetail : BaseSectionReport
    {
		public static rptBDAccountDetail InstancePtr
		{
			get
			{
				return (rptBDAccountDetail)Sys.GetInstance(typeof(rptBDAccountDetail));
			}
		}

		protected rptBDAccountDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBDAccountDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cBDAccountDetailReport theReport;
		private int lngPage;
		private cBDAccountSummaryGroup currAccount;
		private cAccountDetailSummaryInfo currDetail;
		private string strLastAccount = string.Empty;

		public rptBDAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public void Init(ref cBDAccountDetailReport repObj)
		{
			theReport = repObj;
			theReport.Accounts.MoveFirst();
			theReport.AccountSummaries.MoveFirst();
			frmReportViewer.InstancePtr.Init(this);
			lngPage = 1;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// set up binder values for groups in report
			this.Fields.Add("GroupHeader1Binder");
			theReport.AccountSummaries.MoveFirst();
			currAccount = (cBDAccountSummaryGroup)theReport.AccountSummaries.GetCurrentItem();
			currAccount.Details.MoveFirst();
			this.Fields["GroupHeader1Binder"].Value = currAccount.Account.Account;
			strLastAccount = currAccount.Account.Account;
			txtAccount.Text = currAccount.Account.Account;
			txtAccountDescription.Text = currAccount.Account.Description;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			currAccount = null;
			currDetail = null;
			if (theReport.AccountSummaries.IsCurrent())
			{
				currAccount = (cBDAccountSummaryGroup)theReport.AccountSummaries.GetCurrentItem();
			}
			while (!(currAccount == null))
			{
				currDetail = (cAccountDetailSummaryInfo)currAccount.Details.GetCurrentItem();
				if (!(currDetail == null))
				{
					while (!currDetail.Include)
					{
						currAccount.Details.MoveNext();
						if (currAccount.Details.IsCurrent())
						{
							currDetail = (cAccountDetailSummaryInfo)currAccount.Details.GetCurrentItem();
						}
						else
						{
							break;
						}
					}
				}
				if (currAccount.Details.IsCurrent())
				{
					break;
				}
				theReport.AccountSummaries.MoveNext();
				if (theReport.AccountSummaries.IsCurrent())
				{
					currAccount = (cBDAccountSummaryGroup)theReport.AccountSummaries.GetCurrentItem();
					currAccount.Details.MoveFirst();
					this.Fields["GroupHeader1Binder"].Value = currAccount.Account.Account;
					txtAccount.Text = currAccount.Account.Account;
					txtAccountDescription.Text = currAccount.Account.Description;
				}
				else
				{
					currAccount = null;
				}
			}
			if (currDetail == null)
			{
				eArgs.EOF = true;
				currAccount = null;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (!(currAccount == null))
			{
				if (currAccount.Details.IsCurrent())
				{
					currAccount.Details.MoveNext();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Label8.Text = modGlobalConstants.Statics.MuniName;
			Label10.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblUser.Text = modGlobalConstants.Statics.clsSecurityClass.Get_UserName();
			lblSubtitle1.Text = "";
			lblSubTitle2.Text = "";
			if (theReport.StartPeriod > 0 && theReport.EndPeriod > 0)
			{
				lblSubtitle1.Text = "Periods " + FCConvert.ToString(theReport.StartPeriod) + " to " + FCConvert.ToString(theReport.EndPeriod);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(currDetail == null))
			{
				txtPeriod.Text = FCConvert.ToString(currDetail.Period);
				txtJournal.Text = FCConvert.ToString(currDetail.JournalNumber);
				txtJournalType.Text = currDetail.JournalTypeDescription;
				txtCheck.Text = currDetail.Check;
				txtCredits.Text = Strings.Format(currDetail.Credits, "#,###,###,##0.00");
				txtDebits.Text = Strings.Format(currDetail.Debits, "#,###,###,##0.00");
				txtDescription.Text = currDetail.Description;
				txtDate.Text = currDetail.EntryDate;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		private void rptBDAccountDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBDAccountDetail.Caption	= "ActiveReport1";
			//rptBDAccountDetail.Left	= 0;
			//rptBDAccountDetail.Top	= 0;
			//rptBDAccountDetail.Width	= 19590;
			//rptBDAccountDetail.Height	= 8490;
			//rptBDAccountDetail.StartUpPosition	= 3;
			//rptBDAccountDetail.SectionData	= "rptBDAccountDetail.dsx":0000;
			//End Unmaped Properties
		}
	}
}
