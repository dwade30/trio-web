﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1096Laser.
	/// </summary>
	public partial class rpt1096Laser : BaseSectionReport
	{
		public static rpt1096Laser InstancePtr
		{
			get
			{
				return (rpt1096Laser)Sys.GetInstance(typeof(rpt1096Laser));
			}
		}

		protected rpt1096Laser _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1096Laser	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsTotals = new clsDRWrapper();

        private TaxFormType formType;
        private ITaxFormService taxFormService;

		public rpt1096Laser(TaxFormType type) : this()
        {
            formType = type;
        }

        public rpt1096Laser()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "1096 Form";
            this.ReportEnd += Rpt1096Laser_ReportEnd;
		}

        private void Rpt1096Laser_ReportEnd(object sender, EventArgs e)
        {
            rsInfo.DisposeOf();
			rsTotals.DisposeOf();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int lngAdjust;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngAdjust = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENT"))));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Top += (200 * lngAdjust) / 1440F;
			}
			lngAdjust = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENTHORIZONTAL"))));
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ControlName in this.Detail.Controls)
			{
				ControlName.Left += (140 * lngAdjust) / 1440F;
			}

            rsInfo.OpenRecordset("SELECT * FROM [1099Information]");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
			}

            string formName = "";

            if (formType == TaxFormType.MISC1099)
            {
                formName = "MISC";
                fldMISCX.Visible = true;
                fldNECX.Visible = false;
            }
            else
            {
                formName = "NEC";
                fldMISCX.Visible = false;
                fldNECX.Visible = true;
			}

            taxFormService = StaticSettings.GlobalCommandDispatcher.Send(new GetTaxFormService()).Result;

			rsTotals.OpenRecordset("SELECT * FROM [1099FormTotals] WHERE FormName = '" + formName + "'", "Budgetary");
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			string strPhone;
			fldMunicipality.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Municipality")));
			fldAddress1.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address1")));
			fldAddress2.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address2")));
			if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip4"))) != "")
			{
				fldCityStateZip.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip"))) + "-" + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip4")));
			}
			else
			{
				fldCityStateZip.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Zip")));
			}
			fldContact.Text = rsInfo.Get_Fields_String("Contact");
			fldFederalID.Text = rsInfo.Get_Fields_String("FederalCode");
			fldEmail.Text = rsInfo.Get_Fields_String("Email");
			strPhone = FCConvert.ToString(rsInfo.Get_Fields_String("Phone")).Replace("(", "").Replace(")", "");
			if (Strings.InStr(1, strPhone, "000-0000", CompareConstants.vbBinaryCompare) == 0 && strPhone != "")
			{
				fldPhone.Text = Strings.Left(strPhone, 3) + "   " + Strings.Right(strPhone, strPhone.Length - 3);
			}
			else
			{
				fldPhone.Text = "";
			}
			strPhone = FCConvert.ToString(rsInfo.Get_Fields_String("Fax")).Replace("(", "").Replace(")", "");
			if (Strings.InStr(1, strPhone, "000-0000", CompareConstants.vbBinaryCompare) == 0 && strPhone != "")
			{
				fldFax.Text = Strings.Left(strPhone, 3) + "   " + Strings.Right(strPhone, strPhone.Length - 3);
			}
			else
			{
				fldFax.Text = "";
			}

            if (!rsTotals.EndOfFile() && !rsTotals.BeginningOfFile())
            {
				var vendors = taxFormService.GetEligibleVendors(formType, 600);
                decimal federalTax = 0;

                foreach (var vendor in vendors)
                {
                    federalTax += taxFormService.GetVendorTaxCategoryTotal(vendor.VendorNumber, formType, 4);
				}

                fldTotalFederalTax.Text = federalTax.ToString("#,##0.00");
                fldTotalAmount.Text = Strings.Format(rsTotals.Get_Fields_Decimal("TotalAmountReported") - federalTax, "#,##0.00");
                fldNumberOfForms.Text = Strings.Format(rsTotals.Get_Fields_Int32("TotalNumberOfFormsPrinted"), "#,##0");
			}
            else
            {
                fldTotalFederalTax.Text = Strings.Format(0, "#,##0.00");
                fldTotalAmount.Text = Strings.Format(0, "#,##0.00");
				fldNumberOfForms.Text = Strings.Format(0, "#,##0");
			}
        }
	}
}
