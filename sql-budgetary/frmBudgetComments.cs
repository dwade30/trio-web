﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetComments.
	/// </summary>
	public partial class frmBudgetComments : BaseForm
	{
		public frmBudgetComments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbRevenue.SelectedIndex = 0;
            txtComment.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Space','Tab',48..57,65..90,97..122", false);
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBudgetComments InstancePtr
		{
			get
			{
				return (frmBudgetComments)Sys.GetInstance(typeof(frmBudgetComments));
			}
		}

		protected frmBudgetComments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/5/2002
		// This form will be used to add comments to accounts during
		// the budget process.  These comments can then be printed out
		// on the budget worksheets
		// ********************************************************
		bool blnUnload;

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
            //FC:FINAL:CHN: Change using index on text (fix incorrect result).
            // if (cmbRevenue.SelectedIndex == 1)
            if (cmbRevenue.Text == "Expense Accounts")
            {
                rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E' ORDER BY Account");
			}
			else
			{
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'R' ORDER BY Account");
			}
			if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsAccounts.AddItem(rsBudgetInfo.Get_Fields_Int32("ID") + "\t" + rsBudgetInfo.Get_Fields("Account"));
					if (Strings.Trim(FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Comments"))) == "")
					{
						vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Rows - 1, 1, vsAccounts.Rows - 1, 1, Color.Red);
					}
					else
					{
						vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Rows - 1, 1, vsAccounts.Rows - 1, 1, Color.Blue);
					}
					rsBudgetInfo.MoveNext();
				}
				while (rsBudgetInfo.EndOfFile() != true);
			}
			Label1.Visible = true;
			Label2.Visible = true;
			Label3.Visible = true;
			Label4.Visible = true;
			vsAccounts.Visible = true;
			lblTitle.Visible = true;
			txtComment.Visible = true;
			lblRevenue.Visible = false;
			cmbRevenue.Visible = false;
			vsAccounts.Focus();
			vsAccounts.Select(1, 1);
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(btnProcess, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void frmBudgetComments_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			btnProcess.Focus();
			this.Refresh();
		}

		private void frmBudgetComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmBudgetComments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBudgetComments.FillStyle	= 0;
			//frmBudgetComments.ScaleWidth	= 9045;
			//frmBudgetComments.ScaleHeight	= 7380;
			//frmBudgetComments.LinkTopic	= "Form2";
			//frmBudgetComments.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			vsAccounts.ColWidth(0, 0);
			vsAccounts.TextMatrix(0, 1, "Account");
			//FC:FINAL:DDU:#2964 - aligned column and fixed width
			//vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsAccounts.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:RPU:#i658- Enlarge account column
			vsAccounts.ColWidth(1, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.9));
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmBudgetComments_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (cmbRevenue.Visible)
			{
				cmdProcess_Click();
				//FC:FINAL:RPU: Change the width of button according to its text
				btnProcess.Width = 162;
				btnProcess.Text = "Save Comment";
			}
			else
			{
				blnUnload = true;
				SaveInfo();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cmbRevenue.Visible)
			{
				cmdProcess_Click();
				//FC:FINAL:RPU: Change the width of button according to its text
				btnProcess.Width = 162;
				btnProcess.Text = "Save Comment";
			}
			else
			{
				blnUnload = false;
				SaveInfo();
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			if (ColorTranslator.FromOle(vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1)) == Color.Blue)
			{
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = " + vsAccounts.TextMatrix(vsAccounts.Row, 0));
				txtComment.Text = FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Comments"));
			}
			else
			{
				txtComment.Text = "";
			}
			modAccountTitle.DetermineAccountTitle(vsAccounts.TextMatrix(vsAccounts.Row, 1), ref lblTitle);
		}

		private void SetCustomFormColors()
		{
			lblTitle.ForeColor = Color.Blue;
			Label2.BackColor = Color.Blue;
			Label1.BackColor = Color.Red;
		}

		private void SaveInfo()
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = " + vsAccounts.TextMatrix(vsAccounts.Row, 0));
			rsBudgetInfo.Edit();
			rsBudgetInfo.Set_Fields("Comments", Strings.Trim(txtComment.Text));
			rsBudgetInfo.Update();
			if (Strings.Trim(txtComment.Text) != "")
			{
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1, vsAccounts.Row, 1, Color.Blue);
			}
			else
			{
				vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1, vsAccounts.Row, 1, Color.Red);
			}
			vsAccounts.Focus();
			if (blnUnload)
			{
				mnuProcessQuit_Click();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
