﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAPResearch.
	/// </summary>
	partial class frmAPResearch : BaseForm
	{
		public fecherFoundation.FCGrid vsInfo;
		public fecherFoundation.FCLabel lblTotalAPChecks;
		public fecherFoundation.FCLabel lblTotalAPInvoices;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
        public fecherFoundation.FCFrame Frame1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAPResearch));
			this.vsInfo = new fecherFoundation.FCGrid();
			this.lblTotalAPChecks = new fecherFoundation.FCLabel();
			this.lblTotalAPInvoices = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.Frame1);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(1070, 100);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsInfo);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(164, 30);
			this.HeaderText.Text = "AP Research";
			// 
			// vsInfo
			// 
			this.vsInfo.AllowSelection = false;
			this.vsInfo.AllowUserToResizeColumns = true;
			this.vsInfo.AllowUserToResizeRows = false;
			this.vsInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsInfo.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsInfo.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsInfo.BackColorBkg = System.Drawing.Color.Empty;
			this.vsInfo.BackColorFixed = System.Drawing.Color.Empty;
			this.vsInfo.BackColorSel = System.Drawing.Color.Empty;
			this.vsInfo.Cols = 11;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsInfo.ColumnHeadersHeight = 30;
			this.vsInfo.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsInfo.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsInfo.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsInfo.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShowAndMove;
			this.vsInfo.FixedCols = 0;
			this.vsInfo.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsInfo.FrozenCols = 0;
			this.vsInfo.GridColor = System.Drawing.Color.Empty;
			this.vsInfo.Location = new System.Drawing.Point(30, 20);
			this.vsInfo.Name = "vsInfo";
			this.vsInfo.ReadOnly = true;
			this.vsInfo.RowHeadersVisible = false;
			this.vsInfo.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsInfo.RowHeightMin = 0;
			this.vsInfo.Rows = 1;
			this.vsInfo.ShowColumnVisibilityMenu = false;
			this.vsInfo.Size = new System.Drawing.Size(1018, 480);
			this.vsInfo.StandardTab = true;
			this.vsInfo.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsInfo.TabIndex = 0;
            //
            //Frame1
            //
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(lblTotalAPChecks);
            this.Frame1.Controls.Add(lblTotalAPInvoices);
            this.Frame1.Controls.Add(cmdPrintPreview);
            this.Frame1.Location = new System.Drawing.Point(10, 10);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(350, 250);
            this.Frame1.TabIndex = 1;
			// 
			// lblTotalAPChecks
			// 
			this.lblTotalAPChecks.Location = new System.Drawing.Point(190, 10);
			this.lblTotalAPChecks.Name = "lblTotalAPChecks";
			this.lblTotalAPChecks.Size = new System.Drawing.Size(175, 48);
			this.lblTotalAPChecks.TabIndex = 2;
			this.lblTotalAPChecks.Text = "TOTAL AP CHECKS";
			// 
			// lblTotalAPInvoices
			// 
			this.lblTotalAPInvoices.Location = new System.Drawing.Point(30, 10);
			this.lblTotalAPInvoices.Name = "lblTotalAPInvoices";
			this.lblTotalAPInvoices.Size = new System.Drawing.Size(175, 48);
			this.lblTotalAPInvoices.TabIndex = 1;
			this.lblTotalAPInvoices.Text = "TOTAL AP INVOICES";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print / Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdPrintPreview
            // 
			this.cmdPrintPreview.AppearanceKey = "acceptButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(120, 30);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Size = new System.Drawing.Size(147, 48);
			this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrintPreview.TabIndex = 3;
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(1004, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(44, 24);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// frmAPResearch
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmAPResearch";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "AP Research";
			this.Load += new System.EventHandler(this.frmAPResearch_Load);
			this.Activated += new System.EventHandler(this.frmAPResearch_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAPResearch_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPrintPreview;
		private FCButton cmdPrint;
	}
}
