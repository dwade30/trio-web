﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineExpQuery.
	/// </summary>
	partial class frmOutlineExpQuery : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtDelete;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCLabel lblDelete;
		public fecherFoundation.FCGrid vs1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutlineExpQuery));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtDelete = new fecherFoundation.FCTextBox();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.lblDelete = new fecherFoundation.FCLabel();
			this.vs1 = new fecherFoundation.FCGrid();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.cmdExpenseAdd = new fecherFoundation.FCButton();
			this.cmdExpenseDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdExpenseDelete);
			this.TopPanel.Controls.Add(this.cmdExpenseAdd);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdExpenseAdd, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdExpenseDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Expense / Object Titles";
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.FromName("@window");
			this.Frame1.Controls.Add(this.txtDelete);
			this.Frame1.Controls.Add(this.cmdCancel);
			this.Frame1.Controls.Add(this.cmdDelete);
			this.Frame1.Controls.Add(this.lblDelete);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(285, 184);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Delete Expense";
			this.Frame1.Visible = false;
			// 
			// txtDelete
			// 
			this.txtDelete.AutoSize = false;
			this.txtDelete.BackColor = System.Drawing.SystemColors.Window;
			this.txtDelete.LinkItem = null;
			this.txtDelete.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDelete.LinkTopic = null;
			this.txtDelete.Location = new System.Drawing.Point(20, 66);
			this.txtDelete.Name = "txtDelete";
			this.txtDelete.Size = new System.Drawing.Size(88, 40);
			this.txtDelete.TabIndex = 3;
			this.txtDelete.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtDelete.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDelete_KeyPress);
			this.txtDelete.Validating += new System.ComponentModel.CancelEventHandler(this.txtDelete_Validating);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(110, 126);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(70, 40);
			this.cmdCancel.TabIndex = 2;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.AppearanceKey = "actionButton";
			this.cmdDelete.Location = new System.Drawing.Point(20, 126);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(70, 40);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// lblDelete
			// 
			this.lblDelete.Location = new System.Drawing.Point(20, 30);
			this.lblDelete.Name = "lblDelete";
			this.lblDelete.Size = new System.Drawing.Size(245, 16);
			this.lblDelete.TabIndex = 4;
			this.lblDelete.Text = "WHICH EXPENSE DO YOU WISH TO DELETE";
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vs1.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
			this.vs1.ExtendLastCol = true;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 30);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 2;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1028, 438);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 5;
			this.vs1.Visible = false;
            this.vs1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Vs1_EditingControlShowing);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vs1_MouseMoveEvent);
			this.vs1.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
			this.vs1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(254, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
			this.cmdProcessSave.TabIndex = 2;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdExpenseAdd
			// 
			this.cmdExpenseAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdExpenseAdd.AppearanceKey = "toolbarButton";
			this.cmdExpenseAdd.Location = new System.Drawing.Point(822, 29);
			this.cmdExpenseAdd.Name = "cmdExpenseAdd";
			this.cmdExpenseAdd.Size = new System.Drawing.Size(102, 24);
			this.cmdExpenseAdd.TabIndex = 8;
			this.cmdExpenseAdd.Text = "Add Expense";
			this.cmdExpenseAdd.Click += new System.EventHandler(this.mnuExpenseAdd_Click);
			// 
			// cmdExpenseDelete
			// 
			this.cmdExpenseDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdExpenseDelete.AppearanceKey = "toolbarButton";
			this.cmdExpenseDelete.Location = new System.Drawing.Point(930, 29);
			this.cmdExpenseDelete.Name = "cmdExpenseDelete";
			this.cmdExpenseDelete.Size = new System.Drawing.Size(118, 24);
			this.cmdExpenseDelete.TabIndex = 9;
			this.cmdExpenseDelete.Text = "Delete Expense";
			this.cmdExpenseDelete.Click += new System.EventHandler(this.mnuExpenseDelete_Click);
			// 
			// frmOutlineExpQuery
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmOutlineExpQuery";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Expense / Object Titles";
			this.Load += new System.EventHandler(this.frmOutlineExpQuery_Load);
			this.Activated += new System.EventHandler(this.frmOutlineExpQuery_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOutlineExpQuery_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmOutlineExpQuery_KeyPress);
			this.Resize += new System.EventHandler(this.frmOutlineExpQuery_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdExpenseDelete)).EndInit();
			this.ResumeLayout(false);
		}
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
		public FCButton cmdExpenseAdd;
		public FCButton cmdExpenseDelete;
	}
}
