﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cBDAccountDetailView
	{
		//=========================================================
		public delegate void DetailsChangedEventHandler();

		public event DetailsChangedEventHandler DetailsChanged;

		public delegate void AccountChangedEventHandler();

		public event AccountChangedEventHandler AccountChanged;

		private cBDAccount currAccount;
		private string strAccount = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collDetails = new cGenericCollection();
		private cGenericCollection collDetails_AutoInitialized;

		private cGenericCollection collDetails
		{
			get
			{
				if (collDetails_AutoInitialized == null)
				{
					collDetails_AutoInitialized = new cGenericCollection();
				}
				return collDetails_AutoInitialized;
			}
			set
			{
				collDetails_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController bactController = new cBDAccountController();
		private cBDAccountController bactController_AutoInitialize;

		private cBDAccountController bactController
		{
			get
			{
				if (bactController_AutoInitialize == null)
				{
					bactController_AutoInitialize = new cBDAccountController();
				}
				return bactController_AutoInitialize;
			}
			set
			{
				bactController_AutoInitialize = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDReportService repServ = new cBDReportService();
		private cBDReportService repServ_AutoInitialized;

		private cBDReportService repServ
		{
			get
			{
				if (repServ_AutoInitialized == null)
				{
					repServ_AutoInitialized = new cBDReportService();
				}
				return repServ_AutoInitialized;
			}
			set
			{
				repServ_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountDetailReport detRep = new cBDAccountDetailReport();
		private cBDAccountDetailReport detRep_AutoInitialized;

		private cBDAccountDetailReport detRep
		{
			get
			{
				if (detRep_AutoInitialized == null)
				{
					detRep_AutoInitialized = new cBDAccountDetailReport();
				}
				return detRep_AutoInitialized;
			}
			set
			{
				detRep_AutoInitialized = value;
			}
		}

		private int intStartPeriod;
		private int intEndPeriod;
		private int intFirstPeriod;
		private int intLastPeriod;
		private cBDAccountSummaryGroup currSummary;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cJournalService journServ = new cJournalService();
		private cJournalService journServ_AutoInitialized;

		private cJournalService journServ
		{
			get
			{
				if (journServ_AutoInitialized == null)
				{
					journServ_AutoInitialized = new cJournalService();
				}
				return journServ_AutoInitialized;
			}
			set
			{
				journServ_AutoInitialized = value;
			}
		}

		public short EndPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					if (value != intEndPeriod)
					{
						intEndPeriod = value;
					}
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EndPeriod = 0;
				EndPeriod = FCConvert.ToInt16(intEndPeriod);
				return EndPeriod;
			}
		}

		public short StartPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					if (value != intStartPeriod)
					{
						intStartPeriod = value;
					}
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short StartPeriod = 0;
				StartPeriod = FCConvert.ToInt16(intStartPeriod);
				return StartPeriod;
			}
		}

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				if (!(currSummary == null))
				{
					Details = currSummary.Details;
				}
				else
				{
					Details = collDetails;
				}
				return Details;
			}
		}

		public string AccountNumber
		{
			set
			{
				if (value != strAccount)
				{
					strAccount = value;
					RefreshAccount();
				}
			}
			get
			{
				string AccountNumber = "";
				AccountNumber = strAccount;
				return AccountNumber;
			}
		}

		public cBDAccount currentAccount
		{
			get
			{
				cBDAccount currentAccount = null;
				currentAccount = currAccount;
				return currentAccount;
			}
		}
		// Public Sub Refresh()
		// RefreshAccount
		// End Sub
		private void RefreshAccount()
		{
			try
			{
				// On Error GoTo ErrorHandler
				currAccount = bactController.GetFullAccountByAccount(strAccount);
				detRep.Accounts.ClearList();
				detRep.AccountSummaries.ClearList();
				detRep.Accounts.AddItem(currAccount.Account);
				detRep.StartPeriod = FCConvert.ToInt16(intStartPeriod);
				detRep.EndPeriod = FCConvert.ToInt16(intEndPeriod);
				//FC:FINAL:DSE A property may not be passed as a ref parameter
				//repServ.FillBDAccountDetailReport(ref detRep);
				cBDAccountDetailReport temp = detRep;
				repServ.FillBDAccountDetailReport(ref temp);
				detRep = temp;
				if (detRep.AccountSummaries.ItemCount() > 0)
				{
					detRep.AccountSummaries.MoveFirst();
					currSummary = (cBDAccountSummaryGroup)detRep.AccountSummaries.GetCurrentItem();
				}
				else
				{
					currSummary = null;
				}
				if (this.AccountChanged != null)
					this.AccountChanged();
				FilterDetails();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public cBDAccountDetailView() : base()
		{
			detRep = new cBDAccountDetailReport();
			intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (intFirstPeriod == 1)
			{
				intLastPeriod = 12;
			}
			else
			{
				intLastPeriod = intFirstPeriod - 1;
			}
			intStartPeriod = intFirstPeriod;
			intEndPeriod = intLastPeriod;
			detRep.StartPeriod = FCConvert.ToInt16(intStartPeriod);
			detRep.EndPeriod = FCConvert.ToInt16(intEndPeriod);
		}

		public void FilterDetails()
		{
			if (!(currSummary == null))
			{
				currSummary.Details.MoveFirst();
				cAccountDetailSummaryInfo summItem;
				while (currSummary.Details.IsCurrent())
				{
					//Application.DoEvents();
					summItem = (cAccountDetailSummaryInfo)currSummary.Details.GetCurrentItem();
					if (!(summItem == null))
					{
						if (InPeriodRange(summItem.Period))
						{
							summItem.Include = true;
						}
						else
						{
							summItem.Include = false;
						}
					}
					currSummary.Details.MoveNext();
				}
			}
			if (this.DetailsChanged != null)
				this.DetailsChanged();
		}

		private bool InPeriodRange(int intPeriod)
		{
			bool InPeriodRange = false;
			if (intEndPeriod >= intStartPeriod)
			{
				if (intPeriod >= intStartPeriod && intPeriod <= intEndPeriod)
				{
					InPeriodRange = true;
					return InPeriodRange;
				}
			}
			else
			{
				if (intPeriod >= intStartPeriod || intPeriod <= intEndPeriod)
				{
					InPeriodRange = true;
					return InPeriodRange;
				}
			}
			InPeriodRange = false;
			return InPeriodRange;
		}

		public void PrintAccountDetail()
		{
			detRep.StartPeriod = FCConvert.ToInt16(intStartPeriod);
			detRep.EndPeriod = FCConvert.ToInt16(intEndPeriod);
			//FC:FINAL:DSE A Property cannot be passed as a ref parameter
			//repServ.ShowAccountDetailReport(ref detRep);
			cBDAccountDetailReport temp = detRep;
			repServ.ShowAccountDetailReport(ref temp);
			detRep = temp;
			if (repServ.HadError)
			{
				MessageBox.Show("Error " + FCConvert.ToString(repServ.LastErrorNumber) + "  " + repServ.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngParentID As int	OnWrite(string)
		public void ShowJournal(int lngParentID, string strJournalType)
		{
			if (lngParentID > 0 && strJournalType != "")
			{
				if ((Strings.LCase(strJournalType) == "ap") || (Strings.LCase(strJournalType) == "ac"))
				{
					cAPJournal aJourn;
					aJourn = journServ.GetFullAPJournal(lngParentID);
					if (!(aJourn == null))
					{
						frmPostedAPJournal.InstancePtr.Init(ref aJourn);
					}
				}
				else if (Strings.LCase(strJournalType) == "en")
				{
				}
				else if ((Strings.LCase(strJournalType) == "gj") || (Strings.LCase(strJournalType) == "py") || (Strings.LCase(strJournalType) == "cw") || (Strings.LCase(strJournalType) == "cr"))
				{
				}
			}
		}
	}
}
