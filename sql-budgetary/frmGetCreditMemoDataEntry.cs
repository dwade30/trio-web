﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetCreditMemoDataEntry.
	/// </summary>
	public partial class frmGetCreditMemoDataEntry : BaseForm
	{
		public frmGetCreditMemoDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetCreditMemoDataEntry InstancePtr
		{
			get
			{
				return (frmGetCreditMemoDataEntry)Sys.GetInstance(typeof(frmGetCreditMemoDataEntry));
			}
		}

		protected frmGetCreditMemoDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		bool EditFlag;
		bool SearchFlag;
		int[] recordNum = null;
		bool OKFlag;

		public void StartProgram(int Record)
		{
			clsDRWrapper rsPeriod = new clsDRWrapper();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			rs.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + FCConvert.ToString(Record));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				frmCreditMemoDataEntry.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
				rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
				frmCreditMemoDataEntry.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
				frmCreditMemoDataEntry.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
				frmCreditMemoDataEntry.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
				frmCreditMemoDataEntry.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
				frmCreditMemoDataEntry.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
				frmCreditMemoDataEntry.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
				frmCreditMemoDataEntry.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
				frmCreditMemoDataEntry.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
				//FC:FINAL:MSH - Issue #593: IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
				if (rs.Get_Fields_DateTime("CreditMemoDate") != null)
					frmCreditMemoDataEntry.InstancePtr.txtDate.Text = FCConvert.ToString(rs.Get_Fields_DateTime("CreditMemoDate"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCreditMemoDataEntry.InstancePtr.txtPeriod.Text = FCConvert.ToString(rs.Get_Fields("Period"));
				frmCreditMemoDataEntry.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				frmCreditMemoDataEntry.InstancePtr.txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,##0.00");
				//FC:FINAL:MSH - Issue #593: IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
				if (rs.Get_Fields_String("MemoNumber") != null)
					frmCreditMemoDataEntry.InstancePtr.txtMemoNumber.Text = FCConvert.ToString(rs.Get_Fields_String("MemoNumber"));
				frmCreditMemoDataEntry.InstancePtr.VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				frmCreditMemoDataEntry.InstancePtr.FirstRecordShown = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				GetDetails();
				// put detail information into the AP Data Entry form
				modBudgetaryMaster.Statics.blnCredMemoEdit = true;
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != Record)
				{
					do
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					}
					while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != Record);
				}
				frmCreditMemoDataEntry.InstancePtr.OldJournalNumber = modBudgetaryMaster.Statics.CurrentCredMemoEntry;
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCreditMemoDataEntry.InstancePtr.OldPeriod = FCConvert.ToInt32(rs.Get_Fields("Period"));
				frmCreditMemoDataEntry.InstancePtr.Show(App.MainForm);
				// show the form
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				modRegistry.SaveRegistryKey("CURRCREDMEMOJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCredMemoEntry));
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Account Found", "Non-Existent Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// Me.Hide
			Close();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			cmbSearchType.SelectedIndex = 0;
			//optHidden.Checked = true; // clear the search info
			txtSearch.Text = "";
			vs1.Clear();
			vs1.TextMatrix(0, 0, "Jrnl#");
			vs1.TextMatrix(0, 1, "Vendor#");
			vs1.TextMatrix(0, 2, "Vendor Name");
			vs1.TextMatrix(0, 3, "Description");
			vs1.TextMatrix(0, 4, "Memo#");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, 4);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the account number of that record
			if (intAccount != 0)
			{
				// if there is a valid account number
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = intAccount;
				// gets the Account Number for the item that is double clicked
				SetCombo(intAccount);
				// put the record number into the text box
				cmdGetAccountNumber_Click();
				// call the retrieve record button event
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the list of records invisible
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			if (cboEntry.SelectedIndex != 0)
			{
				// if there is a valid account number
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
				// save the account number
				modRegistry.SaveRegistryKey("CURRCREDMEMOJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCredMemoEntry));
				frmCreditMemoDataEntry.InstancePtr.OldJournal = true;
				frmCreditMemoDataEntry.InstancePtr.OldJournalNumber = modBudgetaryMaster.Statics.CurrentCredMemoEntry;
                frmCreditMemoDataEntry.InstancePtr.OldPeriod = FCConvert.ToInt32(Strings.Mid(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 12, 2));
			}
			else
			{
				frmCreditMemoDataEntry.InstancePtr.OldJournal = false;
			}
			modBudgetaryMaster.Statics.blnCredMemoEdit = false;
			frmCreditMemoDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
			frmCreditMemoDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
			frmCreditMemoDataEntry.InstancePtr.VendorNumber = 0;
			if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
			{
				// do nothing
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.Reset();
			}
			frmCreditMemoDataEntry.InstancePtr.Show(App.MainForm);
			// show the blankform
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			frmWait.InstancePtr.Unload();
			Close();
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click()
		{
			Close();
			// unload this form
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			vs1.Clear();
			Frame3.Visible = false;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string SearchType = "";
			string SearchCriteria = "";
			bool BadSearchFlag = false;
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (cmbSearchType.SelectedIndex != 0)
			{
				// make sure a search type is clicked
				if (cmbSearchType.SelectedIndex != 1)
				{
					if (cmbSearchType.SelectedIndex != 2)
					{
						if (cmbSearchType.SelectedIndex != 3)
						{
							if (cmbSearchType.SelectedIndex != 4)
							{
								frmWait.InstancePtr.Unload();
								//Application.DoEvents();
								MessageBox.Show("You must Choose a Search Type (Vendor Name, Vendor Number, Journal Number, Description,  or Memo Number)", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							else
							{
								SearchType = "MemoNumber";
							}
						}
						else
						{
							SearchType = "Description";
							// remember which type of search we are doing
						}
					}
					else
					{
						SearchType = "VendorName";
					}
				}
				else
				{
					SearchType = "VendorNum";
				}
			}
			else
			{
				SearchType = "JournalNum";
			}
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				SearchCriteria = txtSearch.Text;
				// remember the search criteria
			}
			if (SearchType == "JournalNum")
			{
				// do the right kind of search
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE JournalNumber = " + SearchCriteria + " AND Status = 'E'");
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE JournalNumber = " + SearchCriteria + " AND Status = 'E'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "VendorNum")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE VendorNumber = " + SearchCriteria + " AND Status = 'E'");
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE VendorNumber = " + SearchCriteria + " AND Status = 'E'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "VendorName")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'E' AND VendorNumber IN (SELECT VendorNumber FROM VendorMaster WHERE CheckName Like '" + SearchCriteria + "%')");
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'E' AND VendorNumber IN (SELECT VendorNumber FROM VendorMaster WHERE CheckName Like '" + SearchCriteria + "%')");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "Description")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE Description like '" + SearchCriteria + "%' AND Status = 'E'");
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE Description like '" + SearchCriteria + "%' AND Status = 'E'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CreditMemo WHERE MemoNumber like '" + SearchCriteria + "%' AND Status = 'E'");
				rs.OpenRecordset("SELECT * FROM CreditMemo WHERE MemoNumber like '" + SearchCriteria + "%' AND Status = 'E'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			if (!BadSearchFlag)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				modBudgetaryAccounting.Statics.SearchResults.MoveLast();
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				SearchFlag = true;
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					frmCreditMemoDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
					frmCreditMemoDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					recordNum = new int[1 + 1];
					recordNum[0] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					cmdClear_Click();
					// clear the search
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					SetCombo(FCConvert.ToInt32(rs.Get_Fields("JournalNumber")));
					// retrieve the info
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					modBudgetaryMaster.Statics.CurrentCredMemoEntry = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
					// save the account number
					StartProgram(recordNum[0]);
					// call the procedure to retrieve the info
				}
				else
				{
					frmCreditMemoDataEntry.InstancePtr.btnProcessNextEntry.Enabled = true;
					frmCreditMemoDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					Fill_List(ref SearchType);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					// make the list of records visible
					vs1.Select(1, 5, 1, 0);
					vs1.Focus();
				}
				frmCreditMemoDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = true;
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				BadSearchFlag = false;
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetCreditMemoDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmGetCreditMemoDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetCreditMemoDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				if (this.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					if (this.ActiveControl.GetName() == "cmbSearchType")
					{
						OKFlag = true;
						optSearchType_Click(FCConvert.ToInt16(cmbSearchType.SelectedIndex));
					}
					if (this.ActiveControl.GetName() == "cboEntry")
					{
						cmdGetAccountNumber_Click();
						return;
					}
					if (this.ActiveControl.GetName() == "txtSearch")
						cmdSearch_Click();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void GetDetails()
		{
			int counter = 0;
			rs2.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE CreditMemoID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 15)
				{
					frmCreditMemoDataEntry.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
				}
				counter = 1;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields("account")));
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("1099")));
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("Amount")));
					rs2.MoveNext();
					counter += 1;
				}
				if (counter < frmCreditMemoDataEntry.InstancePtr.vs1.Rows - 1)
				{
					for (counter = counter; counter <= frmCreditMemoDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				counter = 1;
				for (counter = counter; counter <= frmCreditMemoDataEntry.InstancePtr.vs1.Rows - 1; counter++)
				{
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
					frmCreditMemoDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
				}
			}
		}

		private void Fill_List(ref string x)
		{
			int I;
			recordNum = new int[rs.RecordCount() + 1];
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				recordNum[I - 1] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("VendorNumber"))));
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
				vs1.TextMatrix(I, 1, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
				vs1.TextMatrix(I, 2, FCConvert.ToString(rs2.Get_Fields_String("CheckName")));
				vs1.TextMatrix(I, 3, FCConvert.ToString(rs.Get_Fields_String("Description")));
				vs1.TextMatrix(I, 4, FCConvert.ToString(rs.Get_Fields_String("MemoNumber")));
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
		}

		private void frmGetCreditMemoDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetCreditMemoDataEntry.ScaleWidth	= 9045;
			//frmGetCreditMemoDataEntry.ScaleHeight	= 7350;
			//frmGetCreditMemoDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			lblLastAccount.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCredMemoEntry), 4);
			FillJournals();
			modBudgetaryMaster.Statics.CurrentCredMemoEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCREDMEMOJRNL"))));
			vs1.TextMatrix(0, 0, "Jrnl#");
			vs1.TextMatrix(0, 1, "Vendor#");
			vs1.TextMatrix(0, 2, "Vendor Name");
			vs1.TextMatrix(0, 3, "Description");
			vs1.TextMatrix(0, 4, "Memo#");
			vs1.ColWidth(0, 800);
			vs1.ColWidth(1, 1000);
			vs1.ColWidth(2, 4400);
			vs1.ColWidth(3, 3000);
			vs1.ColWidth(4, 1630);
			vs1.ColWidth(5, 0);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, 4);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbSearchType.SelectedIndex == 1 || cmbSearchType.SelectedIndex == 2)
			{
				if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			EditFlag = true;
			vs1.Select(vs1.Row, 5, vs1.Row, 0);
			EditFlag = false;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount;
			// if there is a record selected
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = intAccount;
				// gets the Account Number for the item that is double clicked
				SetCombo(intAccount);
				StartProgram(recordNum[vs1.Row - 1]);
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the listbox invisible
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modBudgetaryMaster.Statics.CurrentCredMemoEntry = intAccount;
					// gets the Account Number for the item that is double clicked
					SetCombo(intAccount);
					StartProgram(recordNum[vs1.Row - 1]);
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				EditFlag = true;
				vs1.Select(vs1.Row, 5, vs1.Row, 0);
				EditFlag = false;
			}
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboEntry.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboEntry.Items[counter].ToString(), 4))
				{
					cboEntry.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboEntry_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEntry.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			//if (optSearchType[0].Checked != true && optSearchType[1].Checked != true && optSearchType[2].Checked != true && optSearchType[3].Checked != true && optSearchType[4].Checked != true)
			//if (cmbSearchType.SelectedIndex < 1)
			//{
			//	cmbSearchType.Focus();
			//}
		}

		private void optSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (OKFlag)
			{
				if (this.Visible == true)
				{
					txtSearch.Focus();
				}
				OKFlag = false;
			}
		}

		public void optSearchType_Click(short Index)
		{
			optSearchType_CheckedChanged(Index, cmbSearchType.SelectedIndex, new System.EventArgs());
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_CheckedChanged(index, sender, e);
		}

		public void FillJournals()
		{
			int counter;
			cboEntry.Clear();
			cboEntry.AddItem("0000 - New Journal Entry");
			rs.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' AND CreditMemo = 1 ORDER BY JournalNumber DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
                    if (FCConvert.ToInt32(rs.Get_Fields("Period")) < 10)
					{
                        cboEntry.AddItem(modValidateAccount.GetFormat_6(rs.Get_Fields("JournalNumber"), 4) + " - " + "Per 0" + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
					}
					else
					{
                        cboEntry.AddItem(modValidateAccount.GetFormat_6(rs.Get_Fields("JournalNumber"), 4) + " - " + "Per " + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
					}
					rs.MoveNext();
				}
			}
			cboEntry.SelectedIndex = 0;
		}
	}
}
