﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckRegister.
	/// </summary>
	partial class rptCheckRegister
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckRegister));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBankName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldListedCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldVoidedCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldListedCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVoidedCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldType,
				this.fldCheck,
				this.fldAmount,
				this.fldDate,
				this.fldWarrant,
				this.fldPayee
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.CanShrink = true;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Line1,
				this.Label21,
				this.lblBankName
			});
			this.PageHeader.Height = 0.9583333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.Label15,
				this.fldTotalAmount,
				this.Label17,
				this.Label18,
				this.Label19,
				this.fldListedCount,
				this.fldVoidedCount,
				this.Line5
			});
			this.GroupFooter1.Height = 0.9583333F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "A / P Check Register";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.3125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Type";
			this.Label8.Top = 0.75F;
			this.Label8.Width = 0.4375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label9.Text = "Check";
			this.Label9.Top = 0.75F;
			this.Label9.Width = 0.5625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 1.6875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Amount";
			this.Label10.Top = 0.75F;
			this.Label10.Width = 0.875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 2.75F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label11.Text = "Date";
			this.Label11.Top = 0.75F;
			this.Label11.Width = 0.625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.5625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "Wrnt";
			this.Label12.Top = 0.75F;
			this.Label12.Width = 0.4375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 4.1875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label13.Text = "Payee";
			this.Label13.Top = 0.75F;
			this.Label13.Width = 1.8125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 1.5F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label21.Text = "**** REPRINT ****";
			this.Label21.Top = 0.40625F;
			this.Label21.Visible = false;
			this.Label21.Width = 4.6875F;
			// 
			// lblBankName
			// 
			this.lblBankName.Height = 0.1875F;
			this.lblBankName.HyperLink = null;
			this.lblBankName.Left = 1.5F;
			this.lblBankName.Name = "lblBankName";
			this.lblBankName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblBankName.Text = "**** REPRINT ****";
			this.lblBankName.Top = 0.21875F;
			this.lblBankName.Width = 4.6875F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.19F;
			this.fldType.Left = 0.28125F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "Field1";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.40625F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 0.875F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.59375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.19F;
			this.fldAmount.Left = 1.65625F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.90625F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 2.75F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.59375F;
			// 
			// fldWarrant
			// 
			this.fldWarrant.Height = 0.19F;
			this.fldWarrant.Left = 3.53125F;
			this.fldWarrant.Name = "fldWarrant";
			this.fldWarrant.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldWarrant.Text = "Field1";
			this.fldWarrant.Top = 0F;
			this.fldWarrant.Width = 0.4375F;
			// 
			// fldPayee
			// 
			this.fldPayee.Height = 0.19F;
			this.fldPayee.Left = 4.15625F;
			this.fldPayee.Name = "fldPayee";
			this.fldPayee.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPayee.Text = "yyyy";
			this.fldPayee.Top = 0F;
			this.fldPayee.Width = 3.3125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 3.03125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5F;
			this.Line3.Width = 1.40625F;
			this.Line3.X1 = 3.03125F;
			this.Line3.X2 = 4.4375F;
			this.Line3.Y1 = 0.5F;
			this.Line3.Y2 = 0.5F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label15.Text = "Total";
			this.Label15.Top = 0.03125F;
			this.Label15.Width = 0.5625F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 1.5F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalAmount.Text = "Field1";
			this.fldTotalAmount.Top = 0.03125F;
			this.fldTotalAmount.Width = 1.0625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.40625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label17.Text = "Count";
			this.Label17.Top = 0.28125F;
			this.Label17.Width = 0.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 3.03125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "Checks";
			this.Label18.Top = 0.53125F;
			this.Label18.Width = 0.59375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.03125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "Voids";
			this.Label19.Top = 0.75F;
			this.Label19.Width = 0.59375F;
			// 
			// fldListedCount
			// 
			this.fldListedCount.Height = 0.1875F;
			this.fldListedCount.Left = 3.71875F;
			this.fldListedCount.Name = "fldListedCount";
			this.fldListedCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldListedCount.Text = "Field1";
			this.fldListedCount.Top = 0.53125F;
			this.fldListedCount.Width = 0.71875F;
			// 
			// fldVoidedCount
			// 
			this.fldVoidedCount.Height = 0.1875F;
			this.fldVoidedCount.Left = 3.71875F;
			this.fldVoidedCount.Name = "fldVoidedCount";
			this.fldVoidedCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldVoidedCount.Text = "Field1";
			this.fldVoidedCount.Top = 0.75F;
			this.fldVoidedCount.Width = 0.71875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.78125F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0F;
			this.Line5.Width = 1.78125F;
			this.Line5.X1 = 2.5625F;
			this.Line5.X2 = 0.78125F;
			this.Line5.Y1 = 0F;
			this.Line5.Y2 = 0F;
			// 
			// rptCheckRegister
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBankName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldListedCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVoidedCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBankName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldListedCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVoidedCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
	}
}
