﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAccountCloseout.
	/// </summary>
	public partial class AccountCloseoutReport : BaseSectionReport
	{
		public static AccountCloseoutReport InstancePtr
		{
			get
			{
				return (AccountCloseoutReport)Sys.GetInstance(typeof(AccountCloseoutReport));
			}
		}

		protected AccountCloseoutReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAccountCloseout	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
		clsDRWrapper rsInfo = new clsDRWrapper();

		public AccountCloseoutReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Closeout Report";
            this.Disposed += AccountCloseoutReport_Disposed;
		}

        private void AccountCloseoutReport_Disposed(object sender, EventArgs e)
        {
			rsRevYTDActivity.Dispose();
            rsYTDActivity.Dispose();
            rsLedgerYTDActivity.Dispose();
            rsInfo.Dispose();

		}

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter = -1;
			modBudgetaryMaster.Statics.sumCloseoutTotals = new modBudgetaryMaster.CloseoutInfo[0 + 1];
			// If gboolTownAccounts Then
			if (modBudgetaryAccounting.Statics.strFundBalCtrAccount == "")
			{
				MessageBox.Show("You must set up a Fund Balance account before you may continue.", "No Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				this.Cancel();
				return;
			}
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo();

			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account");
			rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
			rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account");

			rsInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' OR AccountType = 'R' ORDER BY Account");
			// End If
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsInfo.Get_Fields_String("Account");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldDescription.Text = modAccountTitle.ReturnAccountDescription(rsInfo.Get_Fields("Account"));
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldBalance.Text = Strings.Format(GetBalance_2(rsInfo.Get_Fields("Account")), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldCloseoutAccount.Text = GetCloseoutAccount_2(rsInfo.Get_Fields("Account"));
			UpdateCloseoutTotals_6(fldCloseoutAccount.Text, FCConvert.ToDecimal(fldBalance.Text));
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - Issue #699 - incorrect converting from VB project to .NET
			// vbPorter upgrade warning: srptSummary As Variant --> As srptCloseoutAccountsTotals
			//srptCloseoutAccountsTotals srptSummary = null;  // - "AutoDim"
			srptSummary.Report = new srptCloseoutAccountsTotals();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(double, short)
		private Decimal GetBalance_2(string strAcct)
		{
			return GetBalance(ref strAcct);
		}

		private Decimal GetBalance(ref string strAcct)
		{
			Decimal GetBalance = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.FindFirstRecord("Account", strAcct))
				{
					// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
					GetBalance = FCConvert.ToDecimal(rsYTDActivity.Get_Fields("OriginalBudgetTotal") - rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsYTDActivity.Get_Fields("PostedDebitsTotal") - rsYTDActivity.Get_Fields("PostedCreditsTotal"));
				}
				else
				{
					GetBalance = 0;
				}
			}
			else if (Strings.Left(strAcct, 1) == "R")
			{
				if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
				{
					GetBalance = FCConvert.ToDecimal((rsRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevYTDActivity.Get_Fields("PostedCreditsTotal")) * -1);
				}
				else
				{
					GetBalance = 0;
				}
			}


			else
			{
				GetBalance = 0;
			}
			return GetBalance;
		}

		private string GetCloseoutAccount_2(string strAcct)
		{
			return GetCloseoutAccount(ref strAcct);
		}

		private string GetCloseoutAccount(ref string strAcct)
		{
			string GetCloseoutAccount = "";
			clsDRWrapper rsCloseout = new clsDRWrapper();
			string strFund = "";
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" + modBudgetaryAccounting.GetExpDivision(strAcct) + "'");
					if (rsCloseout.EndOfFile() != true && rsCloseout.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount"))) != "")
						{
							GetCloseoutAccount = Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount")));
							return GetCloseoutAccount;
						}
					}
				}
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "0") + "'");
				}
				else
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "'");
				}
				if (rsCloseout.EndOfFile() != true && rsCloseout.BeginningOfFile() != true)
				{
					if (Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount"))) != "")
					{
						GetCloseoutAccount = Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount")));
					}
					else
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							GetCloseoutAccount = "G " + rsCloseout.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							GetCloseoutAccount = "G " + rsCloseout.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount;
						}
					}
				}
				else
				{
					GetCloseoutAccount = "UNKNOWN";
				}
			}
			else if (Strings.Left(strAcct, 1) == "R")
			{
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsCloseout.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" + modBudgetaryAccounting.GetRevDivision(strAcct) + "' AND Revenue = '" + modBudgetaryAccounting.GetRevenue(strAcct) + "'");
				}
				else
				{
					rsCloseout.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Revenue = '" + modBudgetaryAccounting.GetRevenue(strAcct) + "'");
				}
				if (rsCloseout.EndOfFile() != true && rsCloseout.BeginningOfFile() != true)
				{
					if (Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount"))) != "")
					{
						GetCloseoutAccount = Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount")));
						return GetCloseoutAccount;
					}
				}
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" + modBudgetaryAccounting.GetRevDivision(strAcct) + "'");
					if (rsCloseout.EndOfFile() != true && rsCloseout.BeginningOfFile() != true)
					{
						if (Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount"))) != "")
						{
							GetCloseoutAccount = Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount")));
							return GetCloseoutAccount;
						}
					}
				}
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "' AND Division = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2))), "0") + "'");
				}
				else
				{
					rsCloseout.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strAcct) + "'");
				}
				if (rsCloseout.EndOfFile() != true && rsCloseout.BeginningOfFile() != true)
				{
					if (Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount"))) != "")
					{
						GetCloseoutAccount = Strings.Trim(FCConvert.ToString(rsCloseout.Get_Fields_String("CloseoutAccount")));
					}
					else
					{
						if (!modAccountTitle.Statics.YearFlag)
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							GetCloseoutAccount = "G " + rsCloseout.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00";
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
							GetCloseoutAccount = "G " + rsCloseout.Get_Fields("Fund") + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount;
						}
					}
				}
				else
				{
					GetCloseoutAccount = "UNKNOWN";
				}

			}
			else
			{
				GetCloseoutAccount = "UNKNOWN";
			}
			return GetCloseoutAccount;
		}

		private void UpdateCloseoutTotals_6(string strAcct, Decimal curAmount)
		{
			UpdateCloseoutTotals(ref strAcct, ref curAmount);
		}

		private void UpdateCloseoutTotals(ref string strAcct, ref Decimal curAmount)
		{
			int counter;
			int counter2;
			if (modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter == -1)
			{
				modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter = 0;
				Array.Resize(ref modBudgetaryMaster.Statics.sumCloseoutTotals, modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter + 1);
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				modBudgetaryMaster.Statics.sumCloseoutTotals[modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter] = new modBudgetaryMaster.CloseoutInfo(0);
				modBudgetaryMaster.Statics.sumCloseoutTotals[modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter].strAccount = strAcct;
				modBudgetaryMaster.Statics.sumCloseoutTotals[modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter].curAmount = curAmount;
				modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter += 1;
			}
			else
			{
				for (counter = 0; counter <= modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter - 1; counter++)
				{
					if (modBudgetaryMaster.Statics.sumCloseoutTotals[counter].strAccount == strAcct)
					{
						modBudgetaryMaster.Statics.sumCloseoutTotals[counter].curAmount += curAmount;
						return;
					}
					else if (modBudgetaryMaster.Statics.sumCloseoutTotals[counter].strAccount.CompareTo(strAcct) > 0)
					{
						Array.Resize(ref modBudgetaryMaster.Statics.sumCloseoutTotals, modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter + 1);
						for (counter2 = modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter - 1; counter2 >= counter; counter2--)
						{
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							modBudgetaryMaster.Statics.sumCloseoutTotals[counter2 + 1] = new modBudgetaryMaster.CloseoutInfo(0);
							modBudgetaryMaster.Statics.sumCloseoutTotals[counter2 + 1].strAccount = modBudgetaryMaster.Statics.sumCloseoutTotals[counter2].strAccount;
							modBudgetaryMaster.Statics.sumCloseoutTotals[counter2 + 1].curAmount = modBudgetaryMaster.Statics.sumCloseoutTotals[counter2].curAmount;
						}
						modBudgetaryMaster.Statics.sumCloseoutTotals[counter].strAccount = strAcct;
						modBudgetaryMaster.Statics.sumCloseoutTotals[counter].curAmount = curAmount;
						modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter += 1;
						return;
					}
				}
				Array.Resize(ref modBudgetaryMaster.Statics.sumCloseoutTotals, modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter + 1);
				modBudgetaryMaster.Statics.sumCloseoutTotals[modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter].strAccount = strAcct;
				modBudgetaryMaster.Statics.sumCloseoutTotals[modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter].curAmount = curAmount;
				modBudgetaryMaster.Statics.lngCloseoutTotalsArrayCounter += 1;
				//Application.DoEvents();
			}
		}

		private void rptAccountCloseout_Load(object sender, System.EventArgs e)
		{

		}

		private void AccountCloseoutReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
