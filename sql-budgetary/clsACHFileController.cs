﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBD0000
{
	public class clsACHFileController
	{
		//=========================================================
		private string strLastError = "";

		public string LastError
		{
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public clsACHFileInfo NewFileInfo()
		{
			clsACHFileInfo NewFileInfo = null;
			clsACHFileInfo tInfo = new clsACHFileInfo();
			NewFileInfo = tInfo;
			return NewFileInfo;
		}

		public bool FillFileInfo(ref clsACHFile OutFile, ref short intBankNumber)
		{
			bool FillFileInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				OutFile.FileInfo().EffectiveEntryDate = FCConvert.ToString(DateTime.Now);
				clsACHSetup tSetup = new clsACHSetup();
				clsACHSetupController tCon = new clsACHSetupController();
				strLastError = "";
				if (!tCon.Load(tSetup, FCConvert.ToInt16(intBankNumber)))
				{
					strLastError = "Unable to load ACH setup information.";
					return FillFileInfo;
				}
				OutFile.FileInfo().OriginInfo().ImmediateOriginName = tSetup.ImmediateOriginName;
				OutFile.FileInfo().OriginInfo().ImmediateOriginRT = tSetup.ImmediateOriginRT;
				OutFile.FileInfo().OriginInfo().OriginatingDFI = tSetup.ImmediateOriginODFI;
				OutFile.FileInfo().EmployerInfo().EmployerName = tSetup.EmployerName;
				OutFile.FileInfo().EmployerInfo().EmployerAccount = tSetup.EmployerAccount;
				OutFile.FileInfo().EmployerInfo().EmployerRT = tSetup.EmployerRT;
				OutFile.FileInfo().EmployerInfo().EmployerID = tSetup.EmployerID;
				OutFile.FileInfo().EmployerInfo().AccountType = FCConvert.ToString(tSetup.EmployerAccountType);
				OutFile.FileInfo().DestinationInfo().ACHBankName = tSetup.ImmediateDestinationName;
				OutFile.FileInfo().DestinationInfo().ACHBankRT = tSetup.ImmediateDestinationRT;
				FillFileInfo = true;
				return FillFileInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
			}
			return FillFileInfo;
		}

		public bool FillHeaderRec(ref clsACHFile OutFile)
		{
			bool FillHeaderRec = false;
			try
			{
				// On Error GoTo ErrorHandler
				strLastError = "";
				OutFile.FileHeader().FileIDModifier = "A";
				OutFile.FileHeader().ImmediateDestination = OutFile.FileInfo().DestinationInfo().ACHBankRT;
				OutFile.FileHeader().ImmediateDestinationName = OutFile.FileInfo().DestinationInfo().ACHBankName;
				OutFile.FileHeader().ImmediateOriginRT = OutFile.FileInfo().OriginInfo().ImmediateOriginRT;
				OutFile.FileHeader().ImmediateOriginName = OutFile.FileInfo().OriginInfo().ImmediateOriginName;
				FillHeaderRec = true;
				return FillHeaderRec;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
			}
			return FillHeaderRec;
		}

		public bool FillBatchRecs(ref clsACHFile OutFile, ref clsACHBatch tBatch)
		{
			bool FillBatchRecs = false;
			try
			{
				// On Error GoTo ErrorHandler
				strLastError = "";
				tBatch.BatchHeader().BatchNumber = tBatch.BatchNumber;
				tBatch.BatchHeader().CompanyID = OutFile.FileInfo().EmployerInfo().EmployerID;
				tBatch.BatchHeader().CompanyName = OutFile.FileInfo().EmployerInfo().EmployerName;
				tBatch.BatchHeader().EffectiveEntryDate = OutFile.FileInfo().EffectiveEntryDate;
				tBatch.BatchHeader().DescriptiveDate = Strings.Format(DateTime.Now, "YYMMDD");
				tBatch.BatchHeader().OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
				tBatch.BatchControl().CompanyID = OutFile.FileInfo().EmployerInfo().EmployerID;
				tBatch.BatchControl().OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
				tBatch.BatchControl().BatchNumber = tBatch.BatchNumber;
				FillBatchRecs = true;
				return FillBatchRecs;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
			}
			return FillBatchRecs;
		}

		public bool WriteACHFile(ref clsACHFile OutFile)
		{
			StreamWriter ts = null;
			bool WriteACHFile = false;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				strLastError = "";
				int intRecNum;
				boolOpen = false;
				intRecNum = 0;
				ts = new StreamWriter(OutFile.FileInfo().FileName);
				boolOpen = true;
				string strReturn;
				strReturn = "";
				strReturn = Strings.UCase(OutFile.FileHeader().OutputLine());
				ts.WriteLine(strReturn);
				FCCollection tBatches = new FCCollection();
				// create batch header
				tBatches = OutFile.Batches;
				if (!(tBatches == null))
				{
					foreach (clsACHBatch tBatch in tBatches)
					{
						//Application.DoEvents();
						strReturn = Strings.UCase(tBatch.BatchHeader().OutputLine());
						ts.WriteLine(strReturn);
						foreach (clsACHDetailRecord tDet in tBatch.Details())
						{
							//Application.DoEvents();
							strReturn = Strings.UCase(tDet.OutputLine());
							if (strReturn != "")
							{
								ts.WriteLine(strReturn);
							}
							else
							{
								strLastError = tDet.LastError;
								ts.Close();
								return WriteACHFile;
							}
						}
						// tDet
						foreach (clsACHOffsetRecord tOff in tBatch.Offsets())
						{
							strReturn = Strings.UCase(tOff.OutputLine());
							ts.WriteLine(strReturn);
						}
						// tOff
						strReturn = tBatch.BatchControl().OutputLine();
						ts.WriteLine(strReturn);
					}
					// tBatch
				}
				else
				{
					strLastError = "No batches found.";
					ts.Close();
					return WriteACHFile;
				}
				strReturn = Strings.UCase(OutFile.FileControl().OutputLine());
				ts.WriteLine(strReturn);
				// fillers
				foreach (clsACHBlockFiller tFill in OutFile.Fillers())
				{
					strReturn = Strings.UCase(tFill.OutputLine());
					ts.WriteLine(strReturn);
				}
				// tFill
				boolOpen = false;
				ts.Close();
				WriteACHFile = true;
				//FC:FINAL:DDU:#2816 - download file to client
				FCUtils.Download(OutFile.FileInfo().FileName);
				return WriteACHFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolOpen)
				{
					ts.Close();
				}
				strLastError = Information.Err(ex).Description;
			}
			return WriteACHFile;
		}
		// vbPorter upgrade warning: dtEntryDate As DateTime	OnWrite(string)
		// vbPorter upgrade warning: intBankNumber As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intJournalNumber As short	OnWriteFCConvert.ToInt32(
		public clsACHFile CreateACHFile(bool boolRegular, bool boolPrenotes, bool boolBalanced, DateTime dtEntryDate, short intBankNumber, int lngRecipientID, bool boolForcePreNote, short intJournalNumber = 0)
		{
			clsACHFile CreateACHFile = null;
			strLastError = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsACHFile OutFile = new clsACHFile();
				OutFile.FileInfo().Balanced = boolBalanced;
				OutFile.FileInfo().Regular = boolRegular;
				OutFile.FileInfo().PreNote = boolPrenotes;
				OutFile.FileInfo().ForcePreNote = boolForcePreNote;
				if (boolForcePreNote)
				{
					OutFile.FileInfo().Regular = false;
					OutFile.FileInfo().PreNote = true;
				}
				if (!FillFileInfo(ref OutFile, ref intBankNumber))
				{
					return CreateACHFile;
				}
				OutFile.FileInfo().EffectiveEntryDate = FCConvert.ToString(dtEntryDate);
				if (!FillHeaderRec(ref OutFile))
				{
					return CreateACHFile;
				}
				clsACHBatch tBat = new clsACHBatch();
				tBat.BatchNumber = 1;
				FillBatchRecs(ref OutFile, ref tBat);
				OutFile.AddBatch(ref tBat);
				CreateDetail(ref OutFile, lngRecipientID, ref intBankNumber, intJournalNumber);
				clsACHOffsetRecord tOff;
				foreach (clsACHBatch tBatch in OutFile.Batches)
				{
					if (OutFile.FileInfo().Balanced)
					{
						tOff = new clsACHOffsetRecord();
						tOff.PreNote = !boolRegular;
						tOff.AccountType = OutFile.FileInfo().EmployerInfo().AccountType;
						tOff.TotalAmount = tBatch.CalcTotalCredits();
						tOff.EmployerRT = OutFile.FileInfo().EmployerInfo().EmployerRT;
						tOff.EmployerID = OutFile.FileInfo().EmployerInfo().EmployerID;
						tOff.EmployerAccount = OutFile.FileInfo().EmployerInfo().EmployerAccount;
						tOff.Name = OutFile.FileInfo().EmployerInfo().EmployerName;
						tOff.OriginatingDFI = OutFile.FileInfo().OriginInfo().OriginatingDFI;
						tBatch.AddOffset(ref tOff);
					}
					tBatch.BuildBatchControlRecord();
				}
				// tBatch
				OutFile.BuildFileControl();
				CreateACHFile = OutFile;
				return CreateACHFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
			}
			return CreateACHFile;
		}

		public bool CreateDetail(ref clsACHFile OutFile, int lngRecipientID, ref short intBankNumber, short intJournalNumber = 0)
		{
			bool CreateDetail = false;
			strLastError = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL = "";
				string strWhere = "";
				string strPrenoteWhere;
				clsDRWrapper clsRecips = new clsDRWrapper();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsBanks = new clsDRWrapper();
				string strRecipWhere;
				clsDRWrapper rsCheckTotal = new clsDRWrapper();
				strRecipWhere = "";
				strPrenoteWhere = "";
				if (OutFile.FileInfo().PreNote && !OutFile.FileInfo().Regular && !OutFile.FileInfo().ForcePreNote)
				{
					strRecipWhere = " and Prenote = 1 ";
				}
				else if (OutFile.FileInfo().Regular && !OutFile.FileInfo().PreNote)
				{
					strRecipWhere = " and Prenote <> 1 ";
				}
				rsBanks.OpenRecordset("select * from Banks WHERE ID = " + FCConvert.ToString(intBankNumber), "twbd0000.vb1");
				if (lngRecipientID > 0)
				{
					strWhere = " and trustrecipientid = " + FCConvert.ToString(lngRecipientID);
					strPrenoteWhere = " and ID = " + FCConvert.ToString(lngRecipientID);
					clsRecips.OpenRecordset("SELECT * from VendorMaster where EFT = 1 " + strRecipWhere + " and VendorNumber = " + FCConvert.ToString(lngRecipientID), "twbd0000.vb1");
				}
				else
				{
					clsRecips.OpenRecordset("SELECT * from VendorMaster where EFT = 1 " + strRecipWhere, "twbd0000.vb1");
				}
				if (OutFile.FileInfo().Regular)
				{					
					strSQL = "select distinct VendorNumber, CheckNumber, Warrant FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(intJournalNumber) + " " + strWhere;
				}
				else if (OutFile.FileInfo().ForcePreNote)
				{
					strSQL = "select VendorNumber as VendorNumber,0 as TotAmount from VendorMaster where EFT = 1 " + strPrenoteWhere;
				}
				else
				{
					// just prenotes
					strSQL = "select VendorNumber as VendorNumber,0 as TotAmount from VendorMaster where EFT = 1 and Prenote = 1 " + strPrenoteWhere;
				}
				rsLoad.OpenRecordset(strSQL, "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					if (!clsRecips.EndOfFile())
					{
						clsACHBatch tBatch;
						clsACHDetailRecord TRec;
						tBatch = OutFile.Batches[OutFile.Batches.Count];
						while (!rsLoad.EndOfFile())
						{
							//Application.DoEvents();
							if (clsRecips.FindFirstRecord("VendorNumber", Conversion.Val(rsLoad.Get_Fields_Int32("VendorNumber"))))
							{
								TRec = new clsACHDetailRecord();
								TRec.BankID = FCConvert.ToString(clsRecips.Get_Fields_String("RoutingNumber"));
								TRec.PreNote = FCConvert.ToBoolean(clsRecips.Get_Fields_Boolean("Prenote")) || OutFile.FileInfo().ForcePreNote;
								TRec.AccountNumber = FCConvert.ToString(clsRecips.Get_Fields_String("BankAccountNumber"));
								if (FCConvert.ToString(clsRecips.Get_Fields_String("AccountType")) == "S")
								{
									TRec.AccountType = "3";
								}
								else
								{
									TRec.AccountType = "2";
								}
								TRec.Name = FCConvert.ToString(clsRecips.Get_Fields_String("CheckName"));
								TRec.TotalAmount = 0;
								if (!TRec.PreNote)
								{
									// TODO Get_Fields: Field [Check] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
									rsCheckTotal.OpenRecordset("select sum(Amount) as TotAmount from TempCheckFile WHERE CheckNumber = " + rsLoad.Get_Fields("CheckNumber") + " AND WarrantNumber = " + FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsLoad.Get_Fields("Warrant")))) + " AND VendorNumber = " + rsLoad.Get_Fields_Int32("VendorNumber"),"Budgetary");
									if (rsCheckTotal.EndOfFile() != true && rsCheckTotal.BeginningOfFile() != true)
									{
										// TODO Get_Fields: Field [TotAmount] not found!! (maybe it is an alias?)
										if (Conversion.Val(rsCheckTotal.Get_Fields("TotAmount")) != 0)
										{
											// TODO Get_Fields: Field [TotAmount] not found!! (maybe it is an alias?)
											TRec.TotalAmount = Convert.ToDouble(rsCheckTotal.Get_Fields("TotAmount")) ;
										}
									}
								}
								TRec.ImmediateOriginRT = OutFile.FileHeader().ImmediateOriginRT;
								tBatch.AddDetail(ref TRec);
							}
							else
							{
								// strLastError = "Could not find recipient " & rsLoad.Fields["trustrecipientid"]
								// Exit Function
							}
							rsLoad.MoveNext();
						}
					}
					else
					{
						// If lngRecipientID <> 0 Then strLastError = "No matching recipients found."
						return CreateDetail;
					}
				}
				CreateDetail = true;
				return CreateDetail;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
			}
			return CreateDetail;
		}
	}
}
