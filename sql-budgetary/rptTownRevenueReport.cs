﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptTownRevenueReport.
	/// </summary>
	public partial class rptTownRevenueReport : BaseSectionReport
	{
		public static rptTownRevenueReport InstancePtr
		{
			get
			{
				return (rptTownRevenueReport)Sys.GetInstance(typeof(rptTownRevenueReport));
			}
		}

		protected rptTownRevenueReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDetailInfo.Dispose();
				rsSummaryInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTownRevenueReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTownTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTownTotal;
		// vbPorter upgrade warning: curFinalTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalTotal;
		clsDRWrapper rsSummaryInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();

		public rptTownRevenueReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Town Revenue Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsSummaryInfo.MoveNext();
					if (rsSummaryInfo.EndOfFile() != true)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE Department = '2' AND Description = '" + rsSummaryInfo.Get_Fields_String("Description") + "' AND (Period >= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboStartMonth.SelectedIndex + 1) + " AND Period <= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboEndMonth.SelectedIndex + 1) + ") ORDER BY Revenue, Period");
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsSummaryInfo.Get_Fields_String("Description");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			curTownTotal = 0;
			curFinalTotal = 0;
			rsSummaryInfo.OpenRecordset("SELECT DISTINCT Description FROM RevenueDetailInfo WHERE Department = '2' AND (Period >= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboStartMonth.SelectedIndex + 1) + " AND Period <= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboEndMonth.SelectedIndex + 1) + ") ORDER BY Description");
			if (rsSummaryInfo.EndOfFile() != true && rsSummaryInfo.BeginningOfFile() != true)
			{
				rsDetailInfo.OpenRecordset("SELECT * FROM RevenueDetailInfo WHERE Department = '2' AND Description = '" + rsSummaryInfo.Get_Fields_String("Description") + "' AND (Period >= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboStartMonth.SelectedIndex + 1) + " AND Period <= " + FCConvert.ToString(frmSetupTownRevenueReport.InstancePtr.cboEndMonth.SelectedIndex + 1) + ") ORDER BY Revenue, Period");
			}
			else
			{
				MessageBox.Show("No Info found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldRevenue.Text = FCConvert.ToString(rsDetailInfo.Get_Fields_String("Revenue"));
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = Strings.Format(rsDetailInfo.Get_Fields("Period"), "00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount") * -1, "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			curTownTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldFinalTotal.Text = Strings.Format(curFinalTotal, "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldTownTotal.Text = Strings.Format(curTownTotal, "#,##0.00");
			curFinalTotal += curTownTotal;
			curTownTotal = 0;
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			lblTown.Text = FCConvert.ToString(rsSummaryInfo.Get_Fields_String("Description"));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		

		private void rptTownRevenueReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
