﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using GrapeCity.ActiveReports.Expressions.Remote.GlobalDataTypes;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAccountResearch.
	/// </summary>
	public partial class rptAccountResearch : BaseSectionReport
	{
		public static rptAccountResearch InstancePtr
		{
			get
			{
				return (rptAccountResearch)Sys.GetInstance(typeof(rptAccountResearch));
			}
		}

		protected rptAccountResearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAccountResearch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditTotal;
		// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitTotal;
		// vbPorter upgrade warning: curEncumbranceTotal As Decimal	OnWrite(Decimal, short)
		Decimal curEncumbranceTotal;
		// vbPorter upgrade warning: curFinalCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalCreditTotal;
		// vbPorter upgrade warning: curFinalDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalDebitTotal;
		Decimal curFinalEncumbranceTotal;
		string strTable = "";

		public rptAccountResearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Research";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsAccountInfo.MoveNext();
					if (rsAccountInfo.EndOfFile() != true)
					{
						if (frmAccountResearch.InstancePtr.strSql != "")
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsDetailInfo.OpenRecordset("SELECT * FROM " + strTable + " WHERE Status = 'P' AND Account = '" + rsAccountInfo.Get_Fields("Account") + "' AND " + frmAccountResearch.InstancePtr.strSql + " ORDER BY OrderMonth, JournalNumber");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsDetailInfo.OpenRecordset("SELECT * FROM " + strTable + " WHERE Status = 'P' AND Account = '" + rsAccountInfo.Get_Fields("Account") + "' ORDER BY OrderMonth, JournalNumber");
						}
						eArgs.EOF = false;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (eArgs.EOF != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				this.Fields["Binder"].Value = rsAccountInfo.Get_Fields("Account");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strJournal;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			curCreditTotal = 0;
			curDebitTotal = 0;
			curFinalCreditTotal = 0;
			curFinalDebitTotal = 0;
			blnFirstRecord = true;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblDateRange.Text = "";
			if (frmAccountResearch.InstancePtr.chkDateRange.CheckState == CheckState.Checked)
			{
				lblDateRange.Text = "Dates: " + frmAccountResearch.InstancePtr.txtLowDate.Text + "  -  " + frmAccountResearch.InstancePtr.txtHighDate.Text;
			}
			if (frmAccountResearch.InstancePtr.chkMonthRange.CheckState == CheckState.Checked)
			{
				lblDateRange.Text = lblDateRange.Text + "   Acct Per: " + frmAccountResearch.InstancePtr.cboLowMonth.Text + "  -  " + frmAccountResearch.InstancePtr.cboHighMonth.Text;
			}
			if (lblDateRange.Text == "")
			{
				lblDateRange.Text = "Dates: All";
			}
			strJournal = "";
			if (frmAccountResearch.InstancePtr.chkAP.CheckState == CheckState.Checked)
			{
				strJournal += "AP, ";
			}
			if (frmAccountResearch.InstancePtr.chkCR.CheckState == CheckState.Checked)
			{
				strJournal += "CR, ";
			}
			if (frmAccountResearch.InstancePtr.chkCD.CheckState == CheckState.Checked)
			{
				strJournal += "CD, ";
			}
			if (frmAccountResearch.InstancePtr.chkEN.CheckState == CheckState.Checked)
			{
				strJournal += "EN, ";
			}
			if (frmAccountResearch.InstancePtr.chkGJ.CheckState == CheckState.Checked)
			{
				strJournal += "GJ, ";
			}
			if (frmAccountResearch.InstancePtr.chkPY.CheckState == CheckState.Checked)
			{
				strJournal += "PY, ";
			}
			strJournal = Strings.Left(strJournal, strJournal.Length - 2);
			if (frmAccountResearch.InstancePtr.cmbAllJournals.SelectedIndex == 0)
			{
				lblJournalOptions.Text = "Journal(s): All   " + strJournal;
			}
			else
			{
				lblJournalOptions.Text = "Journal(s): " + Strings.Left(frmAccountResearch.InstancePtr.cboStartJournal.Text, 4) + " - " + Strings.Left(frmAccountResearch.InstancePtr.cboEndJournal.Text, 4) + "   " + strJournal;
			}
			if (frmAccountResearch.InstancePtr.cmbSingleAccount.SelectedIndex == 0)
			{
				lblAccount.Text = "Account(s): All";
			}
			else if (frmAccountResearch.InstancePtr.cmbSingleAccount.SelectedIndex == 2)
			{
				lblAccount.Text = "Account(s): " + frmAccountResearch.InstancePtr.vsLowAccount.TextMatrix(0, 0) + " to " + frmAccountResearch.InstancePtr.vsHighAccount.TextMatrix(0, 0);
			}
			else if (frmAccountResearch.InstancePtr.cmbSingleAccount.SelectedIndex == 1)
			{
				lblAccount.Text = "Account(s): " + frmAccountResearch.InstancePtr.vsSingleAccount.TextMatrix(0, 0);
			}
			else if (frmAccountResearch.InstancePtr.cmbSingleAccount.SelectedIndex == 3)
			{
				if (frmAccountResearch.InstancePtr.cmbLedger.SelectedIndex == 2)
				{
					lblAccount.Text = "Fund(s): " + Strings.Left(frmAccountResearch.InstancePtr.cboSingleFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
				}
				else
				{
					lblAccount.Text = "Department(s): " + Strings.Left(frmAccountResearch.InstancePtr.cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
			}
			else
			{
				if (frmAccountResearch.InstancePtr.cmbLedger.SelectedIndex == 2)
				{
					lblAccount.Text = "Fund(s): " + Strings.Left(frmAccountResearch.InstancePtr.cboBeginningFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + " to " + Strings.Left(frmAccountResearch.InstancePtr.cboEndingFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))));
				}
				else
				{
					lblAccount.Text = "Department(s): " + Strings.Left(frmAccountResearch.InstancePtr.cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + " to " + Strings.Left(frmAccountResearch.InstancePtr.cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
			}
			if (frmAccountResearch.InstancePtr.cmbLedger.SelectedIndex == 0)
			{
				strTable = "ExpenseDetailInfo";
			}
			else if (frmAccountResearch.InstancePtr.cmbLedger.SelectedIndex == 1)
			{
				strTable = "RevenueDetailInfo";
			}
			else
			{
				strTable = "LedgerDetailInfo";
			}
			if (frmAccountResearch.InstancePtr.strSql != "")
			{
				rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM " + strTable + " WHERE Status = 'P' AND " + frmAccountResearch.InstancePtr.strSql + " ORDER BY Account");
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsDetailInfo.OpenRecordset("SELECT * FROM " + strTable + " WHERE Status = 'P' AND Account = '" + rsAccountInfo.Get_Fields("Account") + "' AND " + frmAccountResearch.InstancePtr.strSql + " ORDER BY OrderMonth, JournalNumber");
			}
			else
			{
				rsAccountInfo.OpenRecordset("SELECT DISTINCT Account FROM " + strTable + " WHERE Status = 'P' ORDER BY Account");
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsDetailInfo.OpenRecordset("SELECT * FROM " + strTable + " WHERE Status = 'P' AND Account = '" + rsAccountInfo.Get_Fields("Account") + "' ORDER BY OrderMonth, JournalNumber");
			}
			if (frmAccountResearch.InstancePtr.cmbDetail.SelectedIndex == 1)
			{
				fldPeriod.Visible = false;
				fldJournal.Visible = false;
				fldCheck.Visible = false;
				fldDate.Visible = false;
				fldVendor.Visible = false;
				fldDescription.Visible = false;
				fldRCB.Visible = false;
				fldType.Visible = false;
				chkLiquidated.Visible = false;
				fldLiquidatedCheck.Visible = false;
				fldDebits.Visible = false;
				fldCredits.Visible = false;
				fldEncumbrance.Visible = false;
				Line3.Visible = false;
				Label21.Visible = false;
				fldDebitTotal.Visible = false;
				fldCreditTotal.Visible = false;
				fldEncumbranceTotal.Visible = false;
				lblPeriod.Visible = false;
				lblJournal.Visible = false;
				lblDate.Visible = false;
				lblCheck.Visible = false;
				lblVendor.Visible = false;
				lblDescription.Visible = false;
				lblRCBType.Visible = false;
				lblAccountTitle.Font = new System.Drawing.Font(lblAccountTitle.Font, FontStyle.Regular);
				lblLiquidated.Visible = false;
				lblLiquidatedCheck.Visible = false;
			}
			else
			{
				fldHeaderCreditsTotal.Visible = false;
				fldHeaderDebitsTotal.Visible = false;
				fldHeaderEncumbranceTotal.Visible = false;
				lblAcct.Visible = false;
			}
			if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
			{
				lblDebits.Visible = false;
				fldHeaderDebitsTotal.Visible = false;
				fldDebits.Visible = false;
				fldDebitTotal.Visible = false;
				fldDebitsFinalTotal.Visible = false;
				lblCredits.Text = "Net";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//FC:FINAL:AM:#3128 - form not closed anymore
            //frmAccountResearch.InstancePtr.Show(App.MainForm);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			clsDRWrapper rsCheck = new clsDRWrapper();
			if (frmAccountResearch.InstancePtr.cmbDetail.SelectedIndex == 0)
			{
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldPeriod.Text = modValidateAccount.GetFormat_6(rsDetailInfo.Get_Fields("Period"), 2);
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				fldJournal.Text = modValidateAccount.GetFormat_6(rsDetailInfo.Get_Fields("JournalNumber"), 4);
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsDetailInfo.Get_Fields("CheckNumber"));
				fldDate.Text = Strings.Format(rsDetailInfo.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
				if (FCConvert.ToInt32(rsDetailInfo.Get_Fields_Int32("VendorNumber")) == 0)
				{
					fldVendor.Text = "";
				}
				else
				{
					rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetailInfo.Get_Fields_Int32("VendorNumber"));
					if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
					{
						fldVendor.Text = Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + "  " + Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")), 12);
					}
					else
					{
						fldVendor.Text = Strings.Format(rsDetailInfo.Get_Fields_Int32("VendorNumber"), "00000") + "  UNKNOWN";
					}
				}
				fldDescription.Text = rsDetailInfo.Get_Fields_String("Description");
				fldRCB.Text = rsDetailInfo.Get_Fields_String("RCB");
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "C" || FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "W")
				{
					fldType.Text = "CR";
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsDetailInfo.Get_Fields("Type") == "D")
				{
					fldType.Text = "CD";
				}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsDetailInfo.Get_Fields("Type") == "G")
				{
					fldType.Text = "GJ";
				}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsDetailInfo.Get_Fields("Type") == "P")
				{
					fldType.Text = "PY";
				}
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							else if (rsDetailInfo.Get_Fields("Type") == "E")
				{
					fldType.Text = "EN";
				}
								// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
								else if (rsDetailInfo.Get_Fields("Type") == "A")
				{
					fldType.Text = "AP";
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
				{
					if (FigureDebitOrCredit() == "D")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						curDebitTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
						curEncumbranceTotal -= rsDetailInfo.Get_Fields_Decimal("Encumbrance");
						if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldCredits.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
							fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1, "#,##0.00");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldDebits.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
							fldCredits.Text = Strings.Format(0, "#,##0.00");
							fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance") * -1, "#,##0.00");
						}
					}
					else
					{
						curEncumbranceTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
						curCreditTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1);
						if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldCredits.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
							fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							fldCredits.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1, "#,##0.00");
							fldDebits.Text = Strings.Format(0, "#,##0.00");
							fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
						}
					}
					chkLiquidated.Visible = false;
					fldLiquidatedCheck.Visible = false;
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsDetailInfo.Get_Fields("Type") == "E")
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curEncumbranceTotal += rsDetailInfo.Get_Fields("Amount");
					if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
					{
						fldCredits.Text = Strings.Format(0, "#,##0.00");
					}
					else
					{
						fldDebits.Text = Strings.Format(0, "#,##0.00");
						fldCredits.Text = Strings.Format(0, "#,##0.00");
					}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
					chkLiquidated.Visible = true;
					fldLiquidatedCheck.Visible = true;
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (rsDetailInfo.Get_Fields("Amount") + rsDetailInfo.Get_Fields_Decimal("Adjustments") - rsDetailInfo.Get_Fields_Decimal("Liquidated") == 0)
					{
						chkLiquidated.Checked = true;
						rsCheck.OpenRecordset("SELECT DISTINCT CheckNumber FROM " + strTable + " WHERE EncumbranceKey = " + rsDetailInfo.Get_Fields_Int32("EncumbranceKey") + " AND Type = 'A'", "twbd0000.vb1");
						if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
						{
							if (rsCheck.RecordCount() > 1)
							{
								fldLiquidatedCheck.Text = "MULTI";
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
								fldLiquidatedCheck.Text = FCConvert.ToString(rsCheck.Get_Fields("CheckNumber"));
							}
						}
						else
						{
							fldLiquidatedCheck.Text = "N/A";
						}
					}
					else
					{
						chkLiquidated.Checked = false;
						fldLiquidatedCheck.Text = "";
					}
				}
				else
				{
					if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "E")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curEncumbranceTotal += rsDetailInfo.Get_Fields("Amount");
						if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
						{
							fldCredits.Text = Strings.Format(0, "#,##0.00");
						}
						else
						{
							fldDebits.Text = Strings.Format(0, "#,##0.00");
							fldCredits.Text = Strings.Format(0, "#,##0.00");
						}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
						chkLiquidated.Visible = false;
						fldLiquidatedCheck.Visible = false;
					}
					else
					{
						if (FigureDebitOrCredit() == "D")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curDebitTotal += rsDetailInfo.Get_Fields("Amount");
							if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								fldCredits.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
								fldEncumbrance.Text = Strings.Format(0, "#,##0.00");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								fldDebits.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
								fldCredits.Text = Strings.Format(0, "#,##0.00");
								fldEncumbrance.Text = Strings.Format(0, "#,##0.00");
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curCreditTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
							if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								fldCredits.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount"), "#,##0.00");
								fldEncumbrance.Text = Strings.Format(0, "#,##0.00");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								fldCredits.Text = Strings.Format(rsDetailInfo.Get_Fields("Amount") * -1, "#,##0.00");
								fldDebits.Text = Strings.Format(0, "#,##0.00");
								fldEncumbrance.Text = Strings.Format(0, "#,##0.00");
							}
						}
						chkLiquidated.Visible = false;
						fldLiquidatedCheck.Visible = false;
					}
				}
			}
			rsVendorInfo.Dispose();
			rsCheck.Dispose();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (frmAccountResearch.InstancePtr.cmbDetail.SelectedIndex == 0)
			{
				if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
				{
					fldCreditTotal.Text = Strings.Format(curDebitTotal - curCreditTotal, "#,##0.00");
					fldEncumbranceTotal.Text = Strings.Format(curEncumbranceTotal, "#,##0.00");
				}
				else
				{
					fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
					fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
					fldEncumbranceTotal.Text = Strings.Format(curEncumbranceTotal, "#,##0.00");
				}
				curFinalDebitTotal += curDebitTotal;
				curFinalCreditTotal += curCreditTotal;
				curFinalEncumbranceTotal += curEncumbranceTotal;
				curCreditTotal = 0;
				curDebitTotal = 0;
				curEncumbranceTotal = 0;
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
			{
				fldCreditsFinalTotal.Text = Strings.Format(curFinalDebitTotal - curFinalCreditTotal, "#,##0.00");
				fldEncumbranceFinalTotal.Text = Strings.Format(curFinalEncumbranceTotal, "#,##0.00");
			}
			else
			{
				fldCreditsFinalTotal.Text = Strings.Format(curFinalCreditTotal, "#,##0.00");
				fldDebitsFinalTotal.Text = Strings.Format(curFinalDebitTotal, "#,##0.00");
				fldEncumbranceFinalTotal.Text = Strings.Format(curFinalEncumbranceTotal, "#,##0.00");
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			lblAccountTitle.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("Account")) + " - " + modAccountTitle.ReturnAccountDescription(FCConvert.ToString(rsAccountInfo.Get_Fields("Account")));
			if (frmAccountResearch.InstancePtr.cmbDetail.SelectedIndex == 1)
			{
				curCreditTotal = 0;
				curDebitTotal = 0;
				curEncumbranceTotal = 0;
				rsDetailInfo.MoveFirst();
				do
				{
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
					{
						if (FigureDebitOrCredit() == "D")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							curDebitTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
							curEncumbranceTotal -= rsDetailInfo.Get_Fields_Decimal("Encumbrance");
						}
						else
						{
							curEncumbranceTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
							curCreditTotal += ((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")) * -1);
						}
					}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsDetailInfo.Get_Fields("Type") == "E")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curEncumbranceTotal += rsDetailInfo.Get_Fields("Amount");
					}
					else
					{
						if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "E")
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							curEncumbranceTotal += rsDetailInfo.Get_Fields("Amount");
						}
						else
						{
							if (FigureDebitOrCredit() == "D")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += rsDetailInfo.Get_Fields("Amount");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (rsDetailInfo.Get_Fields("Amount") * -1);
							}
						}
					}
					rsDetailInfo.MoveNext();
				}
				while (rsDetailInfo.EndOfFile() != true);
				rsDetailInfo.MovePrevious();
				if (frmAccountResearch.InstancePtr.cmbDebitsCredits.SelectedIndex == 1)
				{
					fldHeaderCreditsTotal.Text = Strings.Format(curDebitTotal - curCreditTotal, "#,##0.00");
					fldHeaderEncumbranceTotal.Text = Strings.Format(curEncumbranceTotal, "#,##0.00");
				}
				else
				{
					fldHeaderCreditsTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
					fldHeaderDebitsTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
					fldHeaderEncumbranceTotal.Text = Strings.Format(curEncumbranceTotal, "#,##0.00");
				}
				curFinalDebitTotal += curDebitTotal;
				curFinalCreditTotal += curCreditTotal;
				curFinalEncumbranceTotal += curEncumbranceTotal;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private string FigureDebitOrCredit()
		{
			string FigureDebitOrCredit = "";
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsDetailInfo.Get_Fields("Type")) == "A")
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C" && (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance") > 0))
				{
					FigureDebitOrCredit = "D";
				}
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C" && (rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount") - rsDetailInfo.Get_Fields_Decimal("Encumbrance")) < 0)
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (rsDetailInfo.Get_Fields_Decimal("Encumbrance") != 0 && rsDetailInfo.Get_Fields_Decimal("Encumbrance") > rsDetailInfo.Get_Fields("Amount"))
					{
						FigureDebitOrCredit = "D";
					}
					else
					{
						FigureDebitOrCredit = "C";
					}
				}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C" && FCConvert.ToInt32(rsDetailInfo.Get_Fields("Amount")) > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsDetailInfo.Get_Fields("Type") == "E")
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (rsDetailInfo.Get_Fields("Amount") > 0)
				{
					FigureDebitOrCredit = "D";
				}
				else
				{
					FigureDebitOrCredit = "C";
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C" && rsDetailInfo.Get_Fields("Amount") > 0)
				{
					FigureDebitOrCredit = "D";
				}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "C" && FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) != "E" && rsDetailInfo.Get_Fields("Amount") < 0)
				{
					FigureDebitOrCredit = "C";
				}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						else if (FCConvert.ToString(rsDetailInfo.Get_Fields_String("RCB")) == "C" && rsDetailInfo.Get_Fields("Amount") > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			return FigureDebitOrCredit;
		}

		private void rptAccountResearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAccountResearch.Caption	= "Account Research";
			//rptAccountResearch.Icon	= "rptAccountResearch.dsx":0000";
			//rptAccountResearch.Left	= 0;
			//rptAccountResearch.Top	= 0;
			//rptAccountResearch.Width	= 11880;
			//rptAccountResearch.Height	= 8595;
			//rptAccountResearch.StartUpPosition	= 3;
			//rptAccountResearch.SectionData	= "rptAccountResearch.dsx":058A;
			//End Unmaped Properties
		}
	}
}
