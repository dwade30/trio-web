﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckFileListByNumber.
	/// </summary>
	partial class rptCheckFileListByNumber
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCheckFileListByNumber));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatusDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBank = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStatement = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatusDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBank)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCheck,
				this.fldType,
				this.fldCheckDate,
				this.fldAmount,
				this.fldCode,
				this.fldStatusDate,
				this.fldPayee
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 0.09375F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0.03125F;
			this.fldCheck.Width = 0.8125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldType.Text = "Field2";
			this.fldType.Top = 0.03125F;
			this.fldType.Width = 0.4375F;
			// 
			// fldCheckDate
			// 
			this.fldCheckDate.Height = 0.1875F;
			this.fldCheckDate.Left = 1.46875F;
			this.fldCheckDate.Name = "fldCheckDate";
			this.fldCheckDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldCheckDate.Text = "Field1";
			this.fldCheckDate.Top = 0.03125F;
			this.fldCheckDate.Width = 0.71875F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 2.28125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0.03125F;
			this.fldAmount.Width = 1.0625F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 3.4375F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCode.Text = "Field1";
			this.fldCode.Top = 0.03125F;
			this.fldCode.Width = 0.46875F;
			// 
			// fldStatusDate
			// 
			this.fldStatusDate.Height = 0.1875F;
			this.fldStatusDate.Left = 4F;
			this.fldStatusDate.Name = "fldStatusDate";
			this.fldStatusDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldStatusDate.Text = "Field1";
			this.fldStatusDate.Top = 0.03125F;
			this.fldStatusDate.Width = 0.71875F;
			// 
			// fldPayee
			// 
			this.fldPayee.Height = 0.1875F;
			this.fldPayee.Left = 4.84375F;
			this.fldPayee.Name = "fldPayee";
			this.fldPayee.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPayee.Text = "Field1";
			this.fldPayee.Top = 0.03125F;
			this.fldPayee.Width = 2.59375F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanShrink = true;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblTitle1,
				this.lblBank,
				this.lblStatement,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Line1,
				this.Label22,
				this.lblTitle2
			});
			this.PageHeader.Height = 1.583333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Bank Reconciliation";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.21875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 0F;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblTitle1.Text = "Expense";
			this.lblTitle1.Top = 0.40625F;
			this.lblTitle1.Width = 7.5F;
			// 
			// lblBank
			// 
			this.lblBank.Height = 0.1875F;
			this.lblBank.HyperLink = null;
			this.lblBank.Left = 0.96875F;
			this.lblBank.Name = "lblBank";
			this.lblBank.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.lblBank.Text = "Label15";
			this.lblBank.Top = 0.9375F;
			this.lblBank.Width = 2.6875F;
			// 
			// lblStatement
			// 
			this.lblStatement.Height = 0.1875F;
			this.lblStatement.HyperLink = null;
			this.lblStatement.Left = 4.59375F;
			this.lblStatement.Name = "lblStatement";
			this.lblStatement.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.lblStatement.Text = "Label16";
			this.lblStatement.Top = 0.9375F;
			this.lblStatement.Width = 2.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.09375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label15.Text = "Check";
			this.Label15.Top = 1.375F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label16.Text = "Type";
			this.Label16.Top = 1.375F;
			this.Label16.Width = 0.375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.46875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label17.Text = "Date";
			this.Label17.Top = 1.375F;
			this.Label17.Width = 0.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.28125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label18.Text = "Amount";
			this.Label18.Top = 1.375F;
			this.Label18.Width = 1.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.4375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label19.Text = "Code";
			this.Label19.Top = 1.375F;
			this.Label19.Width = 0.46875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label20.Text = "Date";
			this.Label20.Top = 1.375F;
			this.Label20.Width = 0.71875F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.84375F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
			this.Label21.Text = "Payee";
			this.Label21.Top = 1.375F;
			this.Label21.Width = 2.59375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.5625F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 7.46875F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 1.5625F;
			this.Line1.Y2 = 1.5625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.4375F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label22.Text = "--Status--";
			this.Label22.Top = 1.1875F;
			this.Label22.Width = 1.0625F;
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.21875F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 0F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblTitle2.Text = "Expense";
			this.lblTitle2.Top = 0.625F;
			this.lblTitle2.Width = 7.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label23,
				this.Label24,
				this.fldCount,
				this.fldTotal
			});
			this.GroupFooter1.Height = 0.6041667F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 4.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label23.Text = "Count";
			this.Label23.Top = 0.09375F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.6875F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label24.Text = "Total";
			this.Label24.Top = 0.09375F;
			this.Label24.Width = 0.5F;
			// 
			// fldCount
			// 
			this.fldCount.Height = 0.1875F;
			this.fldCount.Left = 4.78125F;
			this.fldCount.Name = "fldCount";
			this.fldCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCount.Text = "Field1";
			this.fldCount.Top = 0.09375F;
			this.fldCount.Width = 0.53125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 2.25F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.09375F;
			this.fldTotal.Width = 1.09375F;
			// 
			// rptCheckFileListByNumber
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.75F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptCheckFileListByNumber_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatusDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBank)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatusDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayee;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBank;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStatement;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
	}
}
