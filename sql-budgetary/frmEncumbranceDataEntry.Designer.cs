//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEncumbranceDataEntry.
	/// </summary>
	partial class frmEncumbranceDataEntry : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtTownAddress;
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraTownInfo;
		public fecherFoundation.FCButton cmdCancelPrint;
		public fecherFoundation.FCButton cmdOK;
		public Global.T2KOverTypeBox txtTownName;
		public Global.T2KOverTypeBox txtTownAddress_0;
		public Global.T2KOverTypeBox txtTownAddress_1;
		public Global.T2KOverTypeBox txtTownAddress_2;
		public Global.T2KOverTypeBox txtTownAddress_3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraAccountBalance;
		public fecherFoundation.FCButton cmdBalanceOK;
		public fecherFoundation.FCLabel lblBalAccount;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblBalNetBudget;
		public fecherFoundation.FCLabel lblBalPostedYTDNet;
		public fecherFoundation.FCLabel lblBalBalance;
		public fecherFoundation.FCLabel lblBalPendingYTDNet;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCFrame frmInfo;
		public fecherFoundation.FCButton cmdRetrieve;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCListBox lstRecords;
		public fecherFoundation.FCLabel lblRecordNumber;
		public fecherFoundation.FCLabel lblVendorName;
		public fecherFoundation.FCFrame frmSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdCancel;
		public Global.T2KOverTypeBox txtPeriod;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KOverTypeBox txtPO;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCComboBox cboJournal;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public FCGrid vs1;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblPO;
		public fecherFoundation.FCLabel lblAmount;
		public fecherFoundation.FCLabel lblDate1;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblRemAmount;
		public fecherFoundation.FCLabel lblRemaining;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAccounts;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteEntry;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEncumbranceDataEntry));
            this.fraTownInfo = new fecherFoundation.FCFrame();
            this.cmdCancelPrint = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.txtTownName = new Global.T2KOverTypeBox();
            this.txtTownAddress_0 = new Global.T2KOverTypeBox();
            this.txtTownAddress_1 = new Global.T2KOverTypeBox();
            this.txtTownAddress_2 = new Global.T2KOverTypeBox();
            this.txtTownAddress_3 = new Global.T2KOverTypeBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraAccountBalance = new fecherFoundation.FCFrame();
            this.cmdBalanceOK = new fecherFoundation.FCButton();
            this.lblBalAccount = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblBalNetBudget = new fecherFoundation.FCLabel();
            this.lblBalPostedYTDNet = new fecherFoundation.FCLabel();
            this.lblBalBalance = new fecherFoundation.FCLabel();
            this.lblBalPendingYTDNet = new fecherFoundation.FCLabel();
            this.fraJournalSave = new fecherFoundation.FCFrame();
            this.cboSaveJournal = new fecherFoundation.FCComboBox();
            this.cmdOKSave = new fecherFoundation.FCButton();
            this.cmdCancelSave = new fecherFoundation.FCButton();
            this.txtJournalDescription = new fecherFoundation.FCTextBox();
            this.lblSaveInstructions = new fecherFoundation.FCLabel();
            this.lblJournalSave = new fecherFoundation.FCLabel();
            this.lblJournalDescription = new fecherFoundation.FCLabel();
            this.frmInfo = new fecherFoundation.FCFrame();
            this.cmdRetrieve = new fecherFoundation.FCButton();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.lstRecords = new fecherFoundation.FCListBox();
            this.lblRecordNumber = new fecherFoundation.FCLabel();
            this.lblVendorName = new fecherFoundation.FCLabel();
            this.frmSearch = new fecherFoundation.FCFrame();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.txtPeriod = new Global.T2KOverTypeBox();
            this.txtAddress_0 = new Global.T2KOverTypeBox();
            this.txtVendor = new Global.T2KOverTypeBox();
            this.txtDescription = new Global.T2KOverTypeBox();
            this.txtPO = new Global.T2KOverTypeBox();
            this.txtAmount = new Global.T2KBackFillDecimal();
            this.txtAddress_1 = new Global.T2KOverTypeBox();
            this.txtAddress_2 = new Global.T2KOverTypeBox();
            this.txtAddress_3 = new Global.T2KOverTypeBox();
            this.txtDate = new Global.T2KDateBox();
            this.cboJournal = new fecherFoundation.FCComboBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.lblPO = new fecherFoundation.FCLabel();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.lblDate1 = new fecherFoundation.FCLabel();
            this.lblJournal = new fecherFoundation.FCLabel();
            this.lblExpense = new fecherFoundation.FCLabel();
            this.lblRemAmount = new fecherFoundation.FCLabel();
            this.lblRemaining = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDeleteEntry = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessAccounts = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnFileSave = new fecherFoundation.FCButton();
            this.btnFilePrint = new fecherFoundation.FCButton();
            this.btnProcessNextEntry = new fecherFoundation.FCButton();
            this.btnProcessPreviousEntry = new fecherFoundation.FCButton();
            this.btnFileAddVendor = new fecherFoundation.FCButton();
            this.btnProcessSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).BeginInit();
            this.fraTownInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).BeginInit();
            this.fraAccountBalance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
            this.fraJournalSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmInfo)).BeginInit();
            this.frmInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSearch)).BeginInit();
            this.frmSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileAddVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnFileSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 562);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraTownInfo);
            this.ClientArea.Controls.Add(this.fraAccountBalance);
            this.ClientArea.Controls.Add(this.fraJournalSave);
            this.ClientArea.Controls.Add(this.frmInfo);
            this.ClientArea.Controls.Add(this.frmSearch);
            this.ClientArea.Controls.Add(this.txtPeriod);
            this.ClientArea.Controls.Add(this.txtAddress_0);
            this.ClientArea.Controls.Add(this.txtVendor);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtPO);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtAddress_1);
            this.ClientArea.Controls.Add(this.txtAddress_2);
            this.ClientArea.Controls.Add(this.txtAddress_3);
            this.ClientArea.Controls.Add(this.txtDate);
            this.ClientArea.Controls.Add(this.cboJournal);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.lblPeriod);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.lblPO);
            this.ClientArea.Controls.Add(this.lblAmount);
            this.ClientArea.Controls.Add(this.lblDate1);
            this.ClientArea.Controls.Add(this.lblJournal);
            this.ClientArea.Controls.Add(this.lblExpense);
            this.ClientArea.Controls.Add(this.lblRemAmount);
            this.ClientArea.Controls.Add(this.lblRemaining);
            this.ClientArea.Size = new System.Drawing.Size(1078, 502);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnProcessSearch);
            this.TopPanel.Controls.Add(this.btnFileAddVendor);
            this.TopPanel.Controls.Add(this.btnProcessPreviousEntry);
            this.TopPanel.Controls.Add(this.btnProcessNextEntry);
            this.TopPanel.Controls.Add(this.btnFilePrint);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFilePrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessNextEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessPreviousEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileAddVendor, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(287, 30);
            this.HeaderText.Text = "Encumbrance Data Entry";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // fraTownInfo
            // 
            this.fraTownInfo.BackColor = System.Drawing.Color.White;
            this.fraTownInfo.Controls.Add(this.cmdCancelPrint);
            this.fraTownInfo.Controls.Add(this.cmdOK);
            this.fraTownInfo.Controls.Add(this.txtTownName);
            this.fraTownInfo.Controls.Add(this.txtTownAddress_0);
            this.fraTownInfo.Controls.Add(this.txtTownAddress_1);
            this.fraTownInfo.Controls.Add(this.txtTownAddress_2);
            this.fraTownInfo.Controls.Add(this.txtTownAddress_3);
            this.fraTownInfo.Controls.Add(this.Label2);
            this.fraTownInfo.Controls.Add(this.Label1);
            this.fraTownInfo.Location = new System.Drawing.Point(1113, 30);
            this.fraTownInfo.Name = "fraTownInfo";
            this.fraTownInfo.Size = new System.Drawing.Size(346, 348);
            this.fraTownInfo.TabIndex = 38;
            this.fraTownInfo.Text = "Enter Town Information";
            this.ToolTip1.SetToolTip(this.fraTownInfo, null);
            this.fraTownInfo.Visible = false;
            // 
            // cmdCancelPrint
            // 
            this.cmdCancelPrint.AppearanceKey = "actionButton";
            this.cmdCancelPrint.Location = new System.Drawing.Point(106, 290);
            this.cmdCancelPrint.Name = "cmdCancelPrint";
            this.cmdCancelPrint.Size = new System.Drawing.Size(70, 40);
            this.cmdCancelPrint.TabIndex = 47;
            this.cmdCancelPrint.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancelPrint, null);
            this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 290);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(77, 40);
            this.cmdOK.TabIndex = 46;
            this.cmdOK.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOK, null);
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // txtTownName
            // 
            this.txtTownName.Location = new System.Drawing.Point(106, 30);
            this.txtTownName.MaxLength = 35;
            this.txtTownName.Name = "txtTownName";
            this.txtTownName.Size = new System.Drawing.Size(209, 40);
            this.txtTownName.TabIndex = 39;
            this.ToolTip1.SetToolTip(this.txtTownName, null);
            // 
            // txtTownAddress_0
            // 
            this.txtTownAddress_0.Location = new System.Drawing.Point(106, 80);
            this.txtTownAddress_0.MaxLength = 35;
            this.txtTownAddress_0.Name = "txtTownAddress_0";
            this.txtTownAddress_0.Size = new System.Drawing.Size(209, 40);
            this.txtTownAddress_0.TabIndex = 40;
            this.ToolTip1.SetToolTip(this.txtTownAddress_0, null);
            // 
            // txtTownAddress_1
            // 
            this.txtTownAddress_1.Location = new System.Drawing.Point(106, 130);
            this.txtTownAddress_1.MaxLength = 35;
            this.txtTownAddress_1.Name = "txtTownAddress_1";
            this.txtTownAddress_1.Size = new System.Drawing.Size(209, 40);
            this.txtTownAddress_1.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.txtTownAddress_1, null);
            // 
            // txtTownAddress_2
            // 
            this.txtTownAddress_2.Location = new System.Drawing.Point(106, 180);
            this.txtTownAddress_2.MaxLength = 35;
            this.txtTownAddress_2.Name = "txtTownAddress_2";
            this.txtTownAddress_2.Size = new System.Drawing.Size(209, 40);
            this.txtTownAddress_2.TabIndex = 42;
            this.ToolTip1.SetToolTip(this.txtTownAddress_2, null);
            // 
            // txtTownAddress_3
            // 
            this.txtTownAddress_3.Location = new System.Drawing.Point(106, 230);
            this.txtTownAddress_3.MaxLength = 35;
            this.txtTownAddress_3.Name = "txtTownAddress_3";
            this.txtTownAddress_3.Size = new System.Drawing.Size(209, 40);
            this.txtTownAddress_3.TabIndex = 43;
            this.ToolTip1.SetToolTip(this.txtTownAddress_3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(55, 16);
            this.Label2.TabIndex = 45;
            this.Label2.Text = "ADDRESS";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(41, 16);
            this.Label1.TabIndex = 44;
            this.Label1.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // fraAccountBalance
            // 
            this.fraAccountBalance.BackColor = System.Drawing.Color.White;
            this.fraAccountBalance.Controls.Add(this.cmdBalanceOK);
            this.fraAccountBalance.Controls.Add(this.lblBalAccount);
            this.fraAccountBalance.Controls.Add(this.Label3);
            this.fraAccountBalance.Controls.Add(this.Label4);
            this.fraAccountBalance.Controls.Add(this.Label5);
            this.fraAccountBalance.Controls.Add(this.Label6);
            this.fraAccountBalance.Controls.Add(this.lblBalNetBudget);
            this.fraAccountBalance.Controls.Add(this.lblBalPostedYTDNet);
            this.fraAccountBalance.Controls.Add(this.lblBalBalance);
            this.fraAccountBalance.Controls.Add(this.lblBalPendingYTDNet);
            this.fraAccountBalance.Location = new System.Drawing.Point(1107, 30);
            this.fraAccountBalance.Name = "fraAccountBalance";
            this.fraAccountBalance.Size = new System.Drawing.Size(527, 268);
            this.fraAccountBalance.TabIndex = 54;
            this.fraAccountBalance.Text = "Account Balance";
            this.ToolTip1.SetToolTip(this.fraAccountBalance, null);
            this.fraAccountBalance.Visible = false;
            // 
            // cmdBalanceOK
            // 
            this.cmdBalanceOK.AppearanceKey = "actionButton";
            this.cmdBalanceOK.ForeColor = System.Drawing.Color.White;
            this.cmdBalanceOK.Location = new System.Drawing.Point(20, 210);
            this.cmdBalanceOK.Name = "cmdBalanceOK";
            this.cmdBalanceOK.Size = new System.Drawing.Size(96, 40);
            this.cmdBalanceOK.TabIndex = 55;
            this.cmdBalanceOK.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdBalanceOK, null);
            this.cmdBalanceOK.Click += new System.EventHandler(this.cmdBalanceOK_Click);
            // 
            // lblBalAccount
            // 
            this.lblBalAccount.Location = new System.Drawing.Point(20, 30);
            this.lblBalAccount.Name = "lblBalAccount";
            this.lblBalAccount.Size = new System.Drawing.Size(78, 16);
            this.lblBalAccount.TabIndex = 64;
            this.lblBalAccount.Text = "ACCOUNT";
            this.lblBalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblBalAccount, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 66);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(96, 16);
            this.Label3.TabIndex = 63;
            this.Label3.Text = "NET BUDGET";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 102);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(105, 16);
            this.Label4.TabIndex = 62;
            this.Label4.Text = "POSTED YTD NET";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 174);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(55, 16);
            this.Label5.TabIndex = 61;
            this.Label5.Text = "BALANCE";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 138);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(104, 18);
            this.Label6.TabIndex = 60;
            this.Label6.Text = "PENDING YTD NET";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblBalNetBudget
            // 
            this.lblBalNetBudget.Location = new System.Drawing.Point(165, 66);
            this.lblBalNetBudget.Name = "lblBalNetBudget";
            this.lblBalNetBudget.Size = new System.Drawing.Size(105, 16);
            this.lblBalNetBudget.TabIndex = 59;
            this.lblBalNetBudget.Text = "99,999,999.00";
            this.lblBalNetBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblBalNetBudget, null);
            // 
            // lblBalPostedYTDNet
            // 
            this.lblBalPostedYTDNet.Location = new System.Drawing.Point(165, 102);
            this.lblBalPostedYTDNet.Name = "lblBalPostedYTDNet";
            this.lblBalPostedYTDNet.Size = new System.Drawing.Size(105, 16);
            this.lblBalPostedYTDNet.TabIndex = 58;
            this.lblBalPostedYTDNet.Text = "POSTED YTD NET";
            this.lblBalPostedYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblBalPostedYTDNet, null);
            // 
            // lblBalBalance
            // 
            this.lblBalBalance.Location = new System.Drawing.Point(165, 174);
            this.lblBalBalance.Name = "lblBalBalance";
            this.lblBalBalance.Size = new System.Drawing.Size(105, 16);
            this.lblBalBalance.TabIndex = 57;
            this.lblBalBalance.Text = "BALANCE";
            this.lblBalBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblBalBalance, null);
            // 
            // lblBalPendingYTDNet
            // 
            this.lblBalPendingYTDNet.Location = new System.Drawing.Point(165, 138);
            this.lblBalPendingYTDNet.Name = "lblBalPendingYTDNet";
            this.lblBalPendingYTDNet.Size = new System.Drawing.Size(105, 16);
            this.lblBalPendingYTDNet.TabIndex = 56;
            this.lblBalPendingYTDNet.Text = "PENDING YTD NET";
            this.lblBalPendingYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblBalPendingYTDNet, null);
            // 
            // fraJournalSave
            // 
            this.fraJournalSave.BackColor = System.Drawing.Color.White;
            this.fraJournalSave.Controls.Add(this.cboSaveJournal);
            this.fraJournalSave.Controls.Add(this.cmdOKSave);
            this.fraJournalSave.Controls.Add(this.cmdCancelSave);
            this.fraJournalSave.Controls.Add(this.txtJournalDescription);
            this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
            this.fraJournalSave.Controls.Add(this.lblJournalSave);
            this.fraJournalSave.Controls.Add(this.lblJournalDescription);
            this.fraJournalSave.Location = new System.Drawing.Point(1113, 30);
            this.fraJournalSave.Name = "fraJournalSave";
            this.fraJournalSave.Size = new System.Drawing.Size(792, 249);
            this.fraJournalSave.TabIndex = 29;
            this.fraJournalSave.Text = "Save Journal";
            this.ToolTip1.SetToolTip(this.fraJournalSave, null);
            this.fraJournalSave.Visible = false;
            // 
            // cboSaveJournal
            // 
            this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboSaveJournal.Location = new System.Drawing.Point(166, 66);
            this.cboSaveJournal.Name = "cboSaveJournal";
            this.cboSaveJournal.Size = new System.Drawing.Size(331, 40);
            this.cboSaveJournal.TabIndex = 33;
            this.ToolTip1.SetToolTip(this.cboSaveJournal, null);
            this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
            this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
            // 
            // cmdOKSave
            // 
            this.cmdOKSave.AppearanceKey = "actionButton";
            this.cmdOKSave.ForeColor = System.Drawing.Color.White;
            this.cmdOKSave.Location = new System.Drawing.Point(20, 186);
            this.cmdOKSave.Name = "cmdOKSave";
            this.cmdOKSave.Size = new System.Drawing.Size(65, 40);
            this.cmdOKSave.TabIndex = 31;
            this.cmdOKSave.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOKSave, null);
            this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
            // 
            // cmdCancelSave
            // 
            this.cmdCancelSave.AppearanceKey = "actionButton";
            this.cmdCancelSave.ForeColor = System.Drawing.Color.White;
            this.cmdCancelSave.Location = new System.Drawing.Point(94, 186);
            this.cmdCancelSave.Name = "cmdCancelSave";
            this.cmdCancelSave.Size = new System.Drawing.Size(82, 40);
            this.cmdCancelSave.TabIndex = 32;
            this.cmdCancelSave.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancelSave, null);
            this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
            // 
            // txtJournalDescription
            // 
            this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtJournalDescription.Location = new System.Drawing.Point(166, 126);
            this.txtJournalDescription.MaxLength = 100;
            this.txtJournalDescription.Name = "txtJournalDescription";
            this.txtJournalDescription.Size = new System.Drawing.Size(331, 40);
            this.txtJournalDescription.TabIndex = 30;
            this.ToolTip1.SetToolTip(this.txtJournalDescription, null);
            // 
            // lblSaveInstructions
            // 
            this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
            this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblSaveInstructions.Name = "lblSaveInstructions";
            this.lblSaveInstructions.Size = new System.Drawing.Size(771, 16);
            this.lblSaveInstructions.TabIndex = 36;
            this.lblSaveInstructions.Text = "PLEASE SELECT THE JOUNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION FO" +
    "R THE JOURNAL, AND CLICK THE OK BUTTON";
            this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblSaveInstructions, null);
            // 
            // lblJournalSave
            // 
            this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalSave.Location = new System.Drawing.Point(20, 80);
            this.lblJournalSave.Name = "lblJournalSave";
            this.lblJournalSave.Size = new System.Drawing.Size(85, 16);
            this.lblJournalSave.TabIndex = 35;
            this.lblJournalSave.Text = "JOURNAL";
            this.ToolTip1.SetToolTip(this.lblJournalSave, null);
            // 
            // lblJournalDescription
            // 
            this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalDescription.Location = new System.Drawing.Point(20, 140);
            this.lblJournalDescription.Name = "lblJournalDescription";
            this.lblJournalDescription.Size = new System.Drawing.Size(85, 16);
            this.lblJournalDescription.TabIndex = 34;
            this.lblJournalDescription.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.lblJournalDescription, null);
            // 
            // frmInfo
            // 
            this.frmInfo.BackColor = System.Drawing.Color.White;
            this.frmInfo.Controls.Add(this.cmdRetrieve);
            this.frmInfo.Controls.Add(this.cmdReturn);
            this.frmInfo.Controls.Add(this.lstRecords);
            this.frmInfo.Controls.Add(this.lblRecordNumber);
            this.frmInfo.Controls.Add(this.lblVendorName);
            this.frmInfo.Location = new System.Drawing.Point(1113, 30);
            this.frmInfo.Name = "frmInfo";
            this.frmInfo.Size = new System.Drawing.Size(618, 347);
            this.frmInfo.TabIndex = 48;
            this.frmInfo.Text = "Multiple Records";
            this.ToolTip1.SetToolTip(this.frmInfo, null);
            this.frmInfo.Visible = false;
            // 
            // cmdRetrieve
            // 
            this.cmdRetrieve.AppearanceKey = "actionButton";
            this.cmdRetrieve.ForeColor = System.Drawing.Color.White;
            this.cmdRetrieve.Location = new System.Drawing.Point(20, 288);
            this.cmdRetrieve.Name = "cmdRetrieve";
            this.cmdRetrieve.Size = new System.Drawing.Size(156, 40);
            this.cmdRetrieve.TabIndex = 51;
            this.cmdRetrieve.Text = "Retrieve Record";
            this.ToolTip1.SetToolTip(this.cmdRetrieve, null);
            this.cmdRetrieve.Click += new System.EventHandler(this.cmdRetrieve_Click);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.ForeColor = System.Drawing.Color.White;
            this.cmdReturn.Location = new System.Drawing.Point(189, 288);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(89, 40);
            this.cmdReturn.TabIndex = 50;
            this.cmdReturn.Text = "Cancel ";
            this.ToolTip1.SetToolTip(this.cmdReturn, null);
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // lstRecords
            // 
            this.lstRecords.BackColor = System.Drawing.SystemColors.Window;
            this.lstRecords.Location = new System.Drawing.Point(20, 66);
            this.lstRecords.Name = "lstRecords";
            this.lstRecords.Size = new System.Drawing.Size(578, 202);
            this.lstRecords.TabIndex = 49;
            this.ToolTip1.SetToolTip(this.lstRecords, null);
            this.lstRecords.DoubleClick += new System.EventHandler(this.lstRecords_DoubleClick);
            // 
            // lblRecordNumber
            // 
            this.lblRecordNumber.Location = new System.Drawing.Point(20, 30);
            this.lblRecordNumber.Name = "lblRecordNumber";
            this.lblRecordNumber.Size = new System.Drawing.Size(65, 16);
            this.lblRecordNumber.TabIndex = 53;
            this.lblRecordNumber.Text = "VENDOR #";
            this.ToolTip1.SetToolTip(this.lblRecordNumber, null);
            // 
            // lblVendorName
            // 
            this.lblVendorName.Location = new System.Drawing.Point(144, 30);
            this.lblVendorName.Name = "lblVendorName";
            this.lblVendorName.Size = new System.Drawing.Size(41, 16);
            this.lblVendorName.TabIndex = 52;
            this.lblVendorName.Text = "NAME";
            this.ToolTip1.SetToolTip(this.lblVendorName, null);
            // 
            // frmSearch
            // 
            this.frmSearch.BackColor = System.Drawing.Color.White;
            this.frmSearch.Controls.Add(this.txtSearch);
            this.frmSearch.Controls.Add(this.cmdSearch);
            this.frmSearch.Controls.Add(this.cmdCancel);
            this.frmSearch.Location = new System.Drawing.Point(1113, 30);
            this.frmSearch.Name = "frmSearch";
            this.frmSearch.Size = new System.Drawing.Size(307, 150);
            this.frmSearch.TabIndex = 25;
            this.frmSearch.Text = "Vendor Search";
            this.ToolTip1.SetToolTip(this.frmSearch, null);
            this.frmSearch.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(20, 30);
            this.txtSearch.MaxLength = 35;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(267, 40);
            this.txtSearch.TabIndex = 28;
            this.ToolTip1.SetToolTip(this.txtSearch, null);
            this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.ForeColor = System.Drawing.Color.White;
            this.cmdSearch.Location = new System.Drawing.Point(20, 90);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(71, 40);
            this.cmdSearch.TabIndex = 27;
            this.cmdSearch.Text = "Search";
            this.ToolTip1.SetToolTip(this.cmdSearch, null);
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.ForeColor = System.Drawing.Color.White;
            this.cmdCancel.Location = new System.Drawing.Point(97, 90);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(65, 40);
            this.cmdCancel.TabIndex = 26;
            this.cmdCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancel, null);
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // txtPeriod
            // 
            this.txtPeriod.Location = new System.Drawing.Point(141, 280);
            this.txtPeriod.MaxLength = 2;
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.Size = new System.Drawing.Size(113, 40);
            this.txtPeriod.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtPeriod, null);
            this.txtPeriod.GotFocus += new System.EventHandler(this.txtPeriod_Enter);
            // 
            // txtAddress_0
            // 
            this.txtAddress_0.Location = new System.Drawing.Point(607, 30);
            this.txtAddress_0.MaxLength = 50;
            this.txtAddress_0.Name = "txtAddress_0";
            this.txtAddress_0.Size = new System.Drawing.Size(413, 40);
            this.txtAddress_0.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtAddress_0, null);
            this.txtAddress_0.GotFocus += new System.EventHandler(this.txtAddress_Enter);
            this.txtAddress_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
            // 
            // txtVendor
            // 
            this.txtVendor.Location = new System.Drawing.Point(502, 31);
            this.txtVendor.MaxLength = 5;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new System.Drawing.Size(85, 40);
            this.txtVendor.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.txtVendor, "Enter S for Vendor Search or A for Add Vendor");
            this.txtVendor.GotFocus += new System.EventHandler(this.txtVendor_Enter);
            this.txtVendor.Leave += new System.EventHandler(this.txtVendor_Leave);
            this.txtVendor.Validating += new System.ComponentModel.CancelEventHandler(this.txtVendor_Validate);
            this.txtVendor.KeyDown += new Wisej.Web.KeyEventHandler(this.txtVendor_KeyDownEvent);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(141, 130);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(238, 40);
            this.txtDescription.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            this.txtDescription.GotFocus += new System.EventHandler(this.txtDescription_Enter);
            // 
            // txtPO
            // 
            this.txtPO.Location = new System.Drawing.Point(141, 180);
            this.txtPO.MaxLength = 15;
            this.txtPO.Name = "txtPO";
            this.txtPO.Size = new System.Drawing.Size(181, 40);
            this.txtPO.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtPO, null);
            this.txtPO.GotFocus += new System.EventHandler(this.txtPO_Enter);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(141, 230);
            this.txtAmount.MaxLength = 14;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(181, 40);
            this.txtAmount.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.txtAmount, null);
            this.txtAmount.GotFocus += new System.EventHandler(this.txtAmount_Enter);
            this.txtAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAmount_Validate);
            // 
            // txtAddress_1
            // 
            this.txtAddress_1.Location = new System.Drawing.Point(607, 80);
            this.txtAddress_1.MaxLength = 35;
            this.txtAddress_1.Name = "txtAddress_1";
            this.txtAddress_1.Size = new System.Drawing.Size(413, 40);
            this.txtAddress_1.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtAddress_1, null);
            this.txtAddress_1.GotFocus += new System.EventHandler(this.txtAddress_Enter);
            this.txtAddress_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
            // 
            // txtAddress_2
            // 
            this.txtAddress_2.Location = new System.Drawing.Point(607, 130);
            this.txtAddress_2.MaxLength = 35;
            this.txtAddress_2.Name = "txtAddress_2";
            this.txtAddress_2.Size = new System.Drawing.Size(413, 40);
            this.txtAddress_2.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtAddress_2, null);
            this.txtAddress_2.GotFocus += new System.EventHandler(this.txtAddress_Enter);
            this.txtAddress_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
            // 
            // txtAddress_3
            // 
            this.txtAddress_3.Location = new System.Drawing.Point(607, 180);
            this.txtAddress_3.MaxLength = 35;
            this.txtAddress_3.Name = "txtAddress_3";
            this.txtAddress_3.Size = new System.Drawing.Size(413, 40);
            this.txtAddress_3.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtAddress_3, null);
            this.txtAddress_3.GotFocus += new System.EventHandler(this.txtAddress_Enter);
            this.txtAddress_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validate);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(141, 80);
            this.txtDate.Mask = "##/##/####";
            this.txtDate.MaxLength = 10;
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(115, 40);
            this.txtDate.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtDate, null);
            this.txtDate.GotFocus += new System.EventHandler(this.txtDate_Enter);
            this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
            // 
            // cboJournal
            // 
            this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboJournal.Location = new System.Drawing.Point(141, 30);
            this.cboJournal.Name = "cboJournal";
            this.cboJournal.Size = new System.Drawing.Size(181, 40);
            this.cboJournal.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cboJournal, null);
            this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
            this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
            this.cboJournal.GotFocus += new System.EventHandler(this.cboJournal_Enter);
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(951, 230);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(69, 40);
            this.txtZip4.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtZip4, null);
            this.txtZip4.GotFocus += new System.EventHandler(this.txtZip4_Enter);
            this.txtZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip4_KeyPress);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(864, 230);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(80, 40);
            this.txtZip.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.txtZip, null);
            this.txtZip.GotFocus += new System.EventHandler(this.txtZip_Enter);
            this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(798, 230);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(60, 40);
            this.txtState.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtState, null);
            this.txtState.GotFocus += new System.EventHandler(this.txtState_Enter);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(607, 230);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(185, 40);
            this.txtCity.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtCity, null);
            this.txtCity.GotFocus += new System.EventHandler(this.txtCity_Enter);
            // 
            // vs1
            // 
            this.vs1.Cols = 5;
            this.vs1.ExtendLastCol = true;
            this.vs1.FixedCols = 0;
            this.vs1.Location = new System.Drawing.Point(30, 410);
            this.vs1.Name = "vs1";
            this.vs1.RowHeadersVisible = false;
            this.vs1.Rows = 16;
            this.vs1.ShowFocusCell = false;
            this.vs1.Size = new System.Drawing.Size(990, 219);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.vs1, null);
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
            this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_CellBeginEdit);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.GotFocus += new System.EventHandler(this.vs1_Enter);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            // 
            // lblPeriod
            // 
            this.lblPeriod.Location = new System.Drawing.Point(30, 294);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(51, 16);
            this.lblPeriod.TabIndex = 37;
            this.lblPeriod.Text = "PERIOD";
            this.ToolTip1.SetToolTip(this.lblPeriod, null);
            // 
            // lblVendor
            // 
            this.lblVendor.Location = new System.Drawing.Point(428, 44);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(65, 16);
            this.lblVendor.TabIndex = 24;
            this.lblVendor.Text = "VENDOR";
            this.ToolTip1.SetToolTip(this.lblVendor, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 144);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(82, 16);
            this.lblDescription.TabIndex = 23;
            this.lblDescription.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // lblPO
            // 
            this.lblPO.Location = new System.Drawing.Point(30, 194);
            this.lblPO.Name = "lblPO";
            this.lblPO.Size = new System.Drawing.Size(31, 16);
            this.lblPO.TabIndex = 22;
            this.lblPO.Text = "PO";
            this.ToolTip1.SetToolTip(this.lblPO, null);
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(30, 244);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(59, 16);
            this.lblAmount.TabIndex = 21;
            this.lblAmount.Text = "AMOUNT";
            this.ToolTip1.SetToolTip(this.lblAmount, null);
            // 
            // lblDate1
            // 
            this.lblDate1.Location = new System.Drawing.Point(30, 94);
            this.lblDate1.Name = "lblDate1";
            this.lblDate1.Size = new System.Drawing.Size(31, 16);
            this.lblDate1.TabIndex = 20;
            this.lblDate1.Text = "DATE";
            this.ToolTip1.SetToolTip(this.lblDate1, null);
            // 
            // lblJournal
            // 
            this.lblJournal.Location = new System.Drawing.Point(30, 44);
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Size = new System.Drawing.Size(56, 16);
            this.lblJournal.TabIndex = 19;
            this.lblJournal.Text = "JOURNAL";
            this.ToolTip1.SetToolTip(this.lblJournal, null);
            // 
            // lblExpense
            // 
            this.lblExpense.Location = new System.Drawing.Point(30, 380);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(564, 16);
            this.lblExpense.TabIndex = 18;
            this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblExpense, "Right click to see account balance");
            this.lblExpense.MouseUp += new Wisej.Web.MouseEventHandler(this.lblExpense_MouseUp);
            // 
            // lblRemAmount
            // 
            this.lblRemAmount.Location = new System.Drawing.Point(188, 340);
            this.lblRemAmount.Name = "lblRemAmount";
            this.lblRemAmount.Size = new System.Drawing.Size(130, 16);
            this.lblRemAmount.TabIndex = 17;
            this.lblRemAmount.Text = "0.00";
            this.lblRemAmount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblRemAmount, null);
            // 
            // lblRemaining
            // 
            this.lblRemaining.Location = new System.Drawing.Point(30, 340);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(140, 16);
            this.lblRemaining.TabIndex = 1;
            this.lblRemaining.Text = "REMAINING AMOUNT";
            this.ToolTip1.SetToolTip(this.lblRemaining, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessDelete,
            this.mnuProcessDeleteEntry});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuProcessDelete
            // 
            this.mnuProcessDelete.Enabled = false;
            this.mnuProcessDelete.Index = 0;
            this.mnuProcessDelete.Name = "mnuProcessDelete";
            this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuProcessDelete.Text = "Delete Detail Item";
            this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
            // 
            // mnuProcessDeleteEntry
            // 
            this.mnuProcessDeleteEntry.Enabled = false;
            this.mnuProcessDeleteEntry.Index = 1;
            this.mnuProcessDeleteEntry.Name = "mnuProcessDeleteEntry";
            this.mnuProcessDeleteEntry.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuProcessDeleteEntry.Text = "Delete Journal Entry";
            this.mnuProcessDeleteEntry.Click += new System.EventHandler(this.mnuProcessDeleteEntry_Click);
            // 
            // mnuProcessAccounts
            // 
            this.mnuProcessAccounts.Enabled = false;
            this.mnuProcessAccounts.Index = -1;
            this.mnuProcessAccounts.Name = "mnuProcessAccounts";
            this.mnuProcessAccounts.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuProcessAccounts.Text = "Show Valid Accounts";
            // 
            // btnFileSave
            // 
            this.btnFileSave.AppearanceKey = "acceptButton";
            this.btnFileSave.Location = new System.Drawing.Point(485, 30);
            this.btnFileSave.Name = "btnFileSave";
            this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnFileSave.Size = new System.Drawing.Size(71, 48);
            this.btnFileSave.Text = "Save";
            this.btnFileSave.Click += new System.EventHandler(this.cmdProcessSave_Click);
            // 
            // btnFilePrint
            // 
            this.btnFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFilePrint.Location = new System.Drawing.Point(960, 27);
            this.btnFilePrint.Name = "btnFilePrint";
            this.btnFilePrint.Size = new System.Drawing.Size(143, 24);
            this.btnFilePrint.TabIndex = 1;
            this.btnFilePrint.Text = "Print Purchase Order";
            this.btnFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // btnProcessNextEntry
            // 
            this.btnProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessNextEntry.Location = new System.Drawing.Point(829, 27);
            this.btnProcessNextEntry.Name = "btnProcessNextEntry";
            this.btnProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnProcessNextEntry.Size = new System.Drawing.Size(128, 24);
            this.btnProcessNextEntry.TabIndex = 2;
            this.btnProcessNextEntry.Text = "Next Journal Entry";
            this.btnProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
            // 
            // btnProcessPreviousEntry
            // 
            this.btnProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessPreviousEntry.Location = new System.Drawing.Point(674, 27);
            this.btnProcessPreviousEntry.Name = "btnProcessPreviousEntry";
            this.btnProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
            this.btnProcessPreviousEntry.Size = new System.Drawing.Size(151, 24);
            this.btnProcessPreviousEntry.TabIndex = 3;
            this.btnProcessPreviousEntry.Text = "Previous Journal Entry";
            this.btnProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
            // 
            // btnFileAddVendor
            // 
            this.btnFileAddVendor.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileAddVendor.Location = new System.Drawing.Point(579, 27);
            this.btnFileAddVendor.Name = "btnFileAddVendor";
            this.btnFileAddVendor.Size = new System.Drawing.Size(91, 24);
            this.btnFileAddVendor.TabIndex = 4;
            this.btnFileAddVendor.Text = "Add Vendor";
            this.btnFileAddVendor.Click += new System.EventHandler(this.mnuFileAddVendor_Click);
            // 
            // btnProcessSearch
            // 
            this.btnProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessSearch.Location = new System.Drawing.Point(467, 27);
            this.btnProcessSearch.Name = "btnProcessSearch";
            this.btnProcessSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnProcessSearch.Size = new System.Drawing.Size(108, 24);
            this.btnProcessSearch.TabIndex = 5;
            this.btnProcessSearch.Text = "Vendor Search";
            this.btnProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // frmEncumbranceDataEntry
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 670);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmEncumbranceDataEntry";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Encumbrance Data Entry";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmEncumbranceDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmEncumbranceDataEntry_Activated);
            this.Resize += new System.EventHandler(this.frmEncumbranceDataEntry_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEncumbranceDataEntry_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEncumbranceDataEntry_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).EndInit();
            this.fraTownInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).EndInit();
            this.fraAccountBalance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
            this.fraJournalSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmInfo)).EndInit();
            this.frmInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSearch)).EndInit();
            this.frmSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileAddVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnFileSave;
		private FCButton btnFilePrint;
		internal FCButton btnProcessNextEntry;
		internal FCButton btnProcessPreviousEntry;
		private FCButton btnFileAddVendor;
		private FCButton btnProcessSearch;
	}
}