﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorInvoices.
	/// </summary>
	public partial class frmVendorInvoices : BaseForm
	{
		public frmVendorInvoices()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			SetupGridInvoices();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendorInvoices InstancePtr
		{
			get
			{
				return (frmVendorInvoices)Sys.GetInstance(typeof(frmVendorInvoices));
			}
		}

		protected frmVendorInvoices _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int cnstInvoiceColID = 0;
		const int cnstInvoiceColJournalID = 1;
		const int cnstInvoiceColDescription = 5;
		const int cnstInvoiceColStatus = 4;
		const int cnstInvoiceColJournal = 3;
		const int cnstInvoiceColDate = 2;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cVendorController vendCont = new cVendorController();
		private cVendorController vendCont_AutoInitialized;

		private cVendorController vendCont
		{
			get
			{
				if (vendCont_AutoInitialized == null)
				{
					vendCont_AutoInitialized = new cVendorController();
				}
				return vendCont_AutoInitialized;
			}
			set
			{
				vendCont_AutoInitialized = value;
			}
		}

		private cVendorInvoicesView theView;
		private int lngVID;

		public void Init(int lngVendorID)
		{
			theView = new cVendorInvoicesView();
			lngVID = lngVendorID;
			// #789 - Event subscription to display Vendor's Invoices (Note: should be done before Show/Load)
			theView.ViewChanged += this.theView_ViewChanged;
			this.Show(App.MainForm);
		}

		public void InitByVendorNumber(int lngVendorNumber)
		{
			theView = new cVendorInvoicesView();
			cVendor vend;
			vend = vendCont.GetVendorByVendorNumber(lngVendorNumber);
			if (!(vend == null))
			{
				lngVID = vend.ID;
				// #789 - Event subscription to display Vendor's Invoices (Note: should be done before Show/Load)
				theView.ViewChanged += this.theView_ViewChanged;
				this.Show(App.MainForm);
			}
		}

		private void frmVendorInvoices_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmVendorInvoices_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendorInvoices.FillStyle	= 0;
			//frmVendorInvoices.ScaleWidth	= 9300;
			//frmVendorInvoices.ScaleHeight	= 7590;
			//frmVendorInvoices.LinkTopic	= "Form2";
			//frmVendorInvoices.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//SetupGridInvoices();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:DDU:#2903 - aligned columns
			gridInvoices.ColAlignment(cnstInvoiceColJournal, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridInvoices.ColAlignment(cnstInvoiceColStatus, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			gridInvoices.ColAlignment(cnstInvoiceColDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			theView.LoadVendor(lngVID);
		}

		private void SetupGridInvoices()
		{
			gridInvoices.Rows = 1;
			gridInvoices.Cols = 6;
			gridInvoices.ColHidden(cnstInvoiceColID, true);
			gridInvoices.ColHidden(cnstInvoiceColStatus, true);
			gridInvoices.ColHidden(cnstInvoiceColJournalID, true);
			gridInvoices.TextMatrix(0, cnstInvoiceColDescription, "Description");
			gridInvoices.TextMatrix(0, cnstInvoiceColJournal, "Journal");
			gridInvoices.TextMatrix(0, cnstInvoiceColDate, "Posted");
		}

		private void frmVendorInvoices_Resize(object sender, System.EventArgs e)
		{
			ResizeGridInvoices();
		}

		private void ResizeGridInvoices()
		{
			int lngWidth;
			lngWidth = gridInvoices.WidthOriginal;
			gridInvoices.ColWidth(cnstInvoiceColJournal, FCConvert.ToInt32(0.1 * lngWidth));
			gridInvoices.ColWidth(cnstInvoiceColDate, FCConvert.ToInt32(0.1 * lngWidth));
		}

		private void gridInvoices_DblClick(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = gridInvoices.MouseRow;
			if (lngRow > 0)
			{
				// vbPorter upgrade warning: lngId As int	OnWrite(string)
				int lngId = 0;
				lngId = FCConvert.ToInt32(gridInvoices.TextMatrix(lngRow, cnstInvoiceColID));
				if (lngId > 0)
				{
					theView.ViewInvoice(lngId);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void theView_ViewChanged()
		{
			ShowInvoices();
		}

		private void ShowInvoices()
		{
			gridInvoices.Rows = 1;
			theView.Invoices.MoveFirst();
			cAPJournal apJourn;
			int lngRow;
			while (theView.Invoices.IsCurrent())
			{
				//Application.DoEvents();
				apJourn = (cAPJournal)theView.Invoices.GetCurrentItem();
				gridInvoices.Rows += 1;
				lngRow = gridInvoices.Rows - 1;
				gridInvoices.TextMatrix(lngRow, cnstInvoiceColID, FCConvert.ToString(apJourn.ID));
				gridInvoices.TextMatrix(lngRow, cnstInvoiceColJournal, FCConvert.ToString(apJourn.JournalNumber));
				gridInvoices.TextMatrix(lngRow, cnstInvoiceColDescription, apJourn.Description);
				gridInvoices.TextMatrix(lngRow, cnstInvoiceColStatus, apJourn.Status);
				if (apJourn.Status == "P")
				{
					if (Information.IsDate(apJourn.PostedDate))
					{
						gridInvoices.TextMatrix(lngRow, cnstInvoiceColDate, Strings.Format(apJourn.PostedDate, "MM/dd/yyyy"));
					}
				}
				else
				{
					gridInvoices.TextMatrix(lngRow, cnstInvoiceColDate, "");
				}
				theView.Invoices.MoveNext();
			}
		}
	}
}
