﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAuditReport.
	/// </summary>
	public partial class rptAuditReport : BaseSectionReport
	{
		public static rptAuditReport InstancePtr
		{
			get
			{
				return (rptAuditReport)Sys.GetInstance(typeof(rptAuditReport));
			}
		}

		protected rptAuditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData_AutoInitialized.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAuditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private int intCounter;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsData = new clsDRWrapper();
		private clsDRWrapper rsData_AutoInitialized;

		private clsDRWrapper rsData
		{
			get
			{
				if (rsData_AutoInitialized == null)
				{
					rsData_AutoInitialized = new clsDRWrapper();
				}
				return rsData_AutoInitialized;
			}
			set
			{
				rsData_AutoInitialized = value;
			}
		}

		bool blnFirstRecord;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsBankInfo = new clsDRWrapper();
		clsDRWrapper rsBankInfo_AutoInitialized;

		clsDRWrapper rsBankInfo
		{
			get
			{
				if (rsBankInfo_AutoInitialized == null)
				{
					rsBankInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsBankInfo_AutoInitialized;
			}
			set
			{
				rsBankInfo_AutoInitialized = value;
			}
		}

		public rptAuditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Audit Archive Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "ActiveReport_FetchData";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				eArgs.EOF = rsData.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			object txtMuniname;
			string strSQL;
			/*? On Error Resume Next  */// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			//txtMuniname = gstrMuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			blnFirstRecord = true;
			strSQL = "";
			if (frmSetupAuditReport.InstancePtr.cmbBanksAll.SelectedIndex == 0)
			{
				// do nothing
			}
			else
			{
				strSQL += "UserField1 = '" + Strings.Trim(Strings.Left(frmSetupAuditReport.InstancePtr.cboBanks.Text, Strings.InStr(1, frmSetupAuditReport.InstancePtr.cboBanks.Text, " - ", CompareConstants.vbBinaryCompare))) + "' AND ";
			}
			if (frmSetupAuditReport.InstancePtr.cmbScreenAll.SelectedIndex == 0)
			{
				// do nothing
			}
			else
			{
				strSQL += "Location = '" + frmSetupAuditReport.InstancePtr.cboScreen.Text + "' AND ";
			}
			if (frmSetupAuditReport.InstancePtr.cmbDateAll.SelectedIndex == 0)
			{
				// do nothing
			}
			else
			{
				strSQL += "(DateUpdated >= '" + frmSetupAuditReport.InstancePtr.txtLowDate.Text + "' AND DateUpdated <= '" + frmSetupAuditReport.InstancePtr.txtHighDate.Text + "') AND ";
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 5);
				rsData.OpenRecordset("Select * from AuditChanges WHERE " + strSQL + " ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1", modGlobal.DEFAULTDATABASE);
			}
			else
			{
				rsData.OpenRecordset("Select * from AuditChanges ORDER BY DateUpdated DESC, TimeUpdated DESC, UserField1", modGlobal.DEFAULTDATABASE);
			}
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				rsBankInfo.OpenRecordset("SELECT * FROM Banks");
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsBankInfo.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields_String("UserField1"))))
			{
				txtBank.Text = rsData.Get_Fields_String("UserField1") + " " + Strings.Trim(FCConvert.ToString(rsBankInfo.Get_Fields_String("Name")));
			}
			else
			{
				txtBank.Text = rsData.Get_Fields_String("UserField1") + " UNKNOWN";
			}
			// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
			txtUser.Text = FCConvert.ToString(rsData.Get_Fields("UserID"));
			//i688 - Use the date from first value and the time from the other
			//txtDateTimeField.Text = rsData.Get_Fields("DateUpdated") + " " + rsData.Get_Fields("TimeUpdated");
			//FC:FINAL:DDU:#i748 - verify first if TimeUpdated has value in it
			if (rsData.Get_Fields_DateTime("TimeUpdated") is DateTime)
			{
				txtDateTimeField.Text = ((DateTime)rsData.Get_Fields_DateTime("DateUpdated")).ToShortDateString() + " " + ((DateTime)rsData.Get_Fields_DateTime("TimeUpdated")).ToLongTimeString();
			}
			else
			{
				txtDateTimeField.Text = ((DateTime)rsData.Get_Fields_DateTime("DateUpdated")).ToShortDateString();
			}
			fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
			fldDescription.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ChangeDescription")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptAuditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAuditReport.Caption	= "Audit Archive Report";
			//rptAuditReport.Icon	= "rptAuditReport.dsx":0000";
			//rptAuditReport.Left	= 0;
			//rptAuditReport.Top	= 0;
			//rptAuditReport.Width	= 19080;
			//rptAuditReport.Height	= 11115;
			//rptAuditReport.StartUpPosition	= 3;
			//rptAuditReport.WindowState	= 2;
			//rptAuditReport.SectionData	= "rptAuditReport.dsx":058A;
			//End Unmaped Properties
		}

		private void rptArticleList_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
