﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOFDetailInfo.
	/// </summary>
	public partial class srptOFDetailInfo : FCSectionReport
	{
		public static srptOFDetailInfo InstancePtr
		{
			get
			{
				return (srptOFDetailInfo)Sys.GetInstance(typeof(srptOFDetailInfo));
			}
		}

		protected srptOFDetailInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMasterInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOFDetailInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsMasterInfo = new clsDRWrapper();
		bool blnFirstRecord;
		modBudgetaryAccounting.FundType[] RegularFundInfo = null;
		int intFundCounter;

		public srptOFDetailInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int counter;
			bool executeCheckFunds = false;
			bool executeCheckNextJournal = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				for (counter = intFundCounter; counter <= 99; counter++)
				{
					if (RegularFundInfo[counter].Amount != 0)
					{
						intFundCounter = counter;
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						this.Fields["Binder"].Value = rsMasterInfo.Get_Fields("JournalNumber");
						return;
					}
				}
				intFundCounter = 1;
				executeCheckNextJournal = true;
				goto CheckNextJournal;
			}
			else
			{
				intFundCounter += 1;
				if (intFundCounter <= 99)
				{
					executeCheckFunds = true;
					goto CheckFunds;
				}
				else
				{
					intFundCounter = 1;
					executeCheckNextJournal = true;
					goto CheckNextJournal;
				}
				executeCheckNextJournal = true;
				goto CheckNextJournal;
			}
			CheckFunds:
			;
			if (executeCheckFunds)
			{
				for (counter = intFundCounter; counter <= 99; counter++)
				{
					if (RegularFundInfo[counter].Amount != 0)
					{
						intFundCounter = counter;
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						this.Fields["Binder"].Value = rsMasterInfo.Get_Fields("JournalNumber");
						return;
					}
				}
				intFundCounter = 1;
				executeCheckNextJournal = true;
			}
			CheckNextJournal:
			;
			if (executeCheckNextJournal)
			{
				rsMasterInfo.MoveNext();
				if (rsMasterInfo.EndOfFile() != true)
				{
					do
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (!CheckOBF_8(FCConvert.ToInt32(rsMasterInfo.Get_Fields("JournalNumber")), rsMasterInfo.Get_Fields("Type")))
						{
							executeCheckFunds = true;
							goto CheckFunds;
						}
						rsMasterInfo.MoveNext();
					}
					while (rsMasterInfo.EndOfFile() != true);
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			rsMasterInfo.OpenRecordset(rptJournalList.InstancePtr.UserData.ToString());
			do
			{
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (!CheckOBF_8(FCConvert.ToInt32(rsMasterInfo.Get_Fields("JournalNumber")), rsMasterInfo.Get_Fields("Type")))
				{
					break;
				}
				rsMasterInfo.MoveNext();
			}
			while (rsMasterInfo.EndOfFile() != true);
			blnFirstRecord = true;
			for (counter = 1; counter <= 99; counter++)
			{
				modBudgetaryMaster.Statics.curOFFundTotals[counter] = 0;
			}
			intFundCounter = 1;
		}

		private bool CheckOBF_8(int lngJournal, string strJournalType)
		{
			return CheckOBF(ref lngJournal, ref strJournalType);
		}

		private bool CheckOBF(ref int lngJournal, ref string strJournalType)
		{
			bool CheckOBF = false;
            using (clsDRWrapper rsJournalInfo = new clsDRWrapper())
            {
                //clsDRWrapper rsDetailInfo = new clsDRWrapper();
                modBudgetaryAccounting.FundType[] RegularAccountInfo = null;
                modBudgetaryAccounting.FundType[] ControlAccountInfo = null;
                modBudgetaryAccounting.FundType[] ControlFundInfo = null;
                int counter = 0;
                CheckOBF = true;
                if (strJournalType == "AP" || strJournalType == "AC")
                {
                    rsJournalInfo.OpenRecordset(
                        "SELECT * FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (SELECT ID FROM APJournal WHERE JournalNumber = " +
                        FCConvert.ToString(lngJournal) + ")");
                    if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                    {
                        RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                        counter = 0;
                        do
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Account =
                                FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Amount =
                                FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount") -
                                                    rsJournalInfo.Get_Fields("Discount"));
                            RegularAccountInfo[counter].Description =
                                FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                            rsJournalInfo.MoveNext();
                            counter += 1;
                        } while (rsJournalInfo.EndOfFile() != true);

                        RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                    }
                    else
                    {
                        return CheckOBF;
                    }

                    for (counter = 1; counter <= 99; counter++)
                    {
                        if (RegularFundInfo[counter].Amount != 0)
                        {
                            CheckOBF = false;
                            return CheckOBF;
                        }
                    }
                }
                else if (strJournalType == "EN")
                {
                    rsJournalInfo.OpenRecordset(
                        "SELECT * FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (SELECT ID FROM Encumbrances WHERE JournalNumber = " +
                        FCConvert.ToString(lngJournal) + ")");
                    if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                    {
                        RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                        counter = 0;
                        do
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Account =
                                FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Amount =
                                FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
                            RegularAccountInfo[counter].Description =
                                FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                            rsJournalInfo.MoveNext();
                            counter += 1;
                        } while (rsJournalInfo.EndOfFile() != true);

                        RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                    }
                    else
                    {
                        return CheckOBF;
                    }

                    for (counter = 1; counter <= 99; counter++)
                    {
                        if (RegularFundInfo[counter].Amount != 0)
                        {
                            CheckOBF = false;
                            return CheckOBF;
                        }
                    }
                }
                else
                {
                    rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " +
                                                FCConvert.ToString(lngJournal) + " AND Left(Account, 1) = 'G'");
                    if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
                    {
                        RegularAccountInfo = new modBudgetaryAccounting.FundType[rsJournalInfo.RecordCount() - 1 + 1];
                        counter = 0;
                        do
                        {
                            // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Account =
                                FCConvert.ToString(rsJournalInfo.Get_Fields("Account"));
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].AcctType = FCConvert.ToString(rsJournalInfo.Get_Fields("Type"));
                            // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            RegularAccountInfo[counter].Amount =
                                FCConvert.ToDecimal(rsJournalInfo.Get_Fields("Amount"));
                            RegularAccountInfo[counter].Description =
                                FCConvert.ToString(rsJournalInfo.Get_Fields_String("Description"));
                            rsJournalInfo.MoveNext();
                            counter += 1;
                        } while (rsJournalInfo.EndOfFile() != true);

                        RegularFundInfo = modBudgetaryAccounting.CalcFundCash(ref RegularAccountInfo);
                    }
                    else
                    {
                        return CheckOBF;
                    }

                    for (counter = 1; counter <= 99; counter++)
                    {
                        if (RegularFundInfo[counter].Amount != 0)
                        {
                            CheckOBF = false;
                            return CheckOBF;
                        }
                    }
                }
            }

            return CheckOBF;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldFund.Text = intFundCounter.ToString();
			fldAmount.Text = Strings.Format(RegularFundInfo[intFundCounter].Amount, "#,##0.00");
			modBudgetaryMaster.Statics.curOFFundTotals[intFundCounter] += RegularFundInfo[intFundCounter].Amount;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			subOFSummary.Report = new srptOFSummaryInfo();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Period"));
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMasterInfo.Get_Fields("Type")) == "AC")
			{
				fldType.Text = "AP";
			}
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			else if (rsMasterInfo.Get_Fields("Type") == "CW")
			{
				fldType.Text = "CR";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				fldType.Text = FCConvert.ToString(rsMasterInfo.Get_Fields("Type"));
			}
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsMasterInfo.Get_Fields("JournalNumber"), 4);
			fldDescription.Text = Strings.Trim(rsMasterInfo.Get_Fields_String("Description"));
		}

		
	}
}
