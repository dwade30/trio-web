﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorDetail.
	/// </summary>
	public partial class frmVendorDetail : BaseForm
	{
		public frmVendorDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendorDetail InstancePtr
		{
			get
			{
				return (frmVendorDetail)Sys.GetInstance(typeof(frmVendorDetail));
			}
		}

		protected frmVendorDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomReport.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomReport, "Births")
		//
		//
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp;
		bool boolSaveReport;
		int intChoices;
		public bool blnPrint;
		bool blnEditFlag;
		public bool blnVendorSelected;
		public bool blnAccountSelected;
		string strSearch = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private AccountInputClass vsLowGrid_AutoInitialized;

		private AccountInputClass vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new AccountInputClass();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private AccountInputClass vsHighGrid_AutoInitialized;

		private AccountInputClass vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new AccountInputClass();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsSingleGrid = new clsGridAccount();
		private AccountInputClass vsSingleGrid_AutoInitialized;

		private AccountInputClass vsSingleGrid
		{
			get
			{
				if (vsSingleGrid_AutoInitialized == null)
				{
					vsSingleGrid_AutoInitialized = new AccountInputClass();
				}
				return vsSingleGrid_AutoInitialized;
			}
			set
			{
				vsSingleGrid_AutoInitialized = value;
			}
		}

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			//FC:FINAL:BBE - Correct indexes on the converted FCRadioButton in FCComboBox
			//if (optReport[2].Checked)
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DEFAULTDATABASE);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			//FC:FINAL:BBE - Correct indexes on the converted FCRadioButton in FCComboBox
			//else if (optReport[1].Checked)
			else if (cmbReport.SelectedIndex == 1)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				vsLayout.Clear();
				vsLayout.Rows = 2;
				vsLayout.Cols = 1;
				rs.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)) + " Order by RowID, ColumnID", modGlobal.DEFAULTDATABASE);
				while (!rs.EndOfFile())
				{
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) == 1)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")) > 0)
						{
							vsLayout.Cols += 1;
						}
					}
					else
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) >= vsLayout.Rows)
						{
							vsLayout.Rows += 1;
							vsLayout.MergeRow(vsLayout.Rows - 1, true);
						}
					}
					vsLayout.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")), FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")), FCConvert.ToString(rs.Get_Fields_String("DisplayText")));
					// TODO Get_Fields: Check the table for the column [FieldID] and replace with corresponding Get_Field method
					vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), rs.Get_Fields("FieldID"));
					// TODO Get_Fields: Check the table for the column [Width] and replace with corresponding Get_Field method
					vsLayout.ColWidth(FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")), FCConvert.ToInt32(rs.Get_Fields("Width")));
					rs.MoveNext();
				}
			}
		}

		private void cboVendors_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (!Information.IsNumeric(Strings.Left(cboVendors.Text, 1)))
				{
					strSearch = cboVendors.Text;
					//Application.DoEvents();
					cmdSearch_Click();
				}
				else
				{
					cboVendors_Validating_2(false);
					//Application.DoEvents();
				}
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void cboVendors_Validating_2(bool Cancel)
		{
			cboVendors_Validating(cboVendors, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cboVendors_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int intVendorNumber = 0;
			bool blnVendorFound = false;
			int counter;
			if (!Information.IsNumeric(Strings.Left(cboVendors.Text, 1)))
			{
				strSearch = cboVendors.Text;
				cboVendors.SelectedIndex = 0;
			}
			else
			{
				strSearch = "";
			}
			if (cboVendors.SelectedIndex < 0)
			{
				blnVendorFound = false;
				if (strSearch == "")
				{
					intVendorNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Left(cboVendors.Text, 5)))));
					for (counter = 0; counter <= cboVendors.Items.Count - 1; counter++)
					{
						if (Conversion.Val(Strings.Trim(Strings.Left(cboVendors.Items[counter].ToString(), 5))) == intVendorNumber)
						{
							cboVendors.SelectedIndex = counter;
							blnVendorFound = true;
							break;
						}
					}
				}
				if (!blnVendorFound && strSearch == "")
				{
					MessageBox.Show("No vendor found matching that number", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboVendors.Text = "";
					e.Cancel = true;
				}
				else if (strSearch == "")
				{
					cmdPreview.Focus();
				}
			}
		}

		public void cboVendors_Validate(ref bool Cancel)
		{
			cboVendors_Validating(cboVendors, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void cmdAccountCancel_Click(object sender, System.EventArgs e)
		{
			blnAccountSelected = false;
			fraAccountRange.Visible = false;
		}

		private void cmdAccountProcess_Click(object sender, System.EventArgs e)
		{
			if (vsLowAccount.Visible == true)
			{
				if (Strings.InStr(vsLowAccount.Text, "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.Text == "" || Strings.InStr(vsHighAccount.Text, "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.Text == "")
				{
					MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.CompareString(vsLowAccount.Text, vsHighAccount.Text, true) > 0)
				{
					MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (Strings.InStr(vsSingleAccount.Text, "_", CompareConstants.vbBinaryCompare) != 0 || vsSingleAccount.Text == "")
				{
					MessageBox.Show("You must specify the Account you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			blnAccountSelected = true;
			fraAccountRange.Visible = false;
			cmdPrint_Click();
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn;
			clsDRWrapper rsSave = new clsDRWrapper();
			int intRow;
			int intCol;
			clsDRWrapper RSLayout = new clsDRWrapper();
			strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report", null);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + strReturn + "' and type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					MessageBox.Show("A report by that name already exists. A different name must be selected.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type) VALUES ('" + strReturn + "','" + modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL) + "','" + Strings.UCase(modCustomReport.Statics.strReportType) + "')", modGlobal.DEFAULTDATABASE);
					rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGlobal.DEFAULTDATABASE);
					if (!rsSave.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobal.DEFAULTDATABASE);
						for (intRow = 1; intRow <= this.vsLayout.Rows - 1; intRow++)
						{
							for (intCol = 0; intCol <= this.vsLayout.Cols - 1; intCol++)
							{
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobal.DEFAULTDATABASE);
								RSLayout.AddNew();
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
								RSLayout.Set_Fields("RowID", intRow);
								RSLayout.Set_Fields("ColumnID", intCol);
								if (FCConvert.ToString(this.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									RSLayout.Set_Fields("FieldID", -1);
								}
								else
								{
									RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(this.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
								}
								RSLayout.Set_Fields("Width", this.vsLayout.ColWidth(intCol));
								RSLayout.Set_Fields("DisplayText", this.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
								RSLayout.Update();
							}
						}
					}
					LoadCombo();
					MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdBackToSelect_Click(object sender, System.EventArgs e)
		{
			fraSearch.Visible = false;
			txtCriteria.Text = "";
			fraVendorSelect.Visible = true;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			strSearch = "";
			fraVendorSelect.Visible = false;
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= cboVendors.Items.Count - 1; counter++)
			{
				if (Strings.Trim(cboVendors.Items[counter].ToString()) == Strings.Trim(vsVendors.TextMatrix(vsVendors.Row, 0) + "  " + vsVendors.TextMatrix(vsVendors.Row, 1)))
				{
					cboVendors.SelectedIndex = counter;
					break;
				}
			}
			fraVendorSelect.Visible = true;
			fraMultiple.Visible = false;
			cmdOK_Click();
		}

		public void cmdGet_Click()
		{
			cmdGet_Click(cmdGet, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			if (strSearch != "")
			{
				strSearch = "";
				cboVendors.Text = "";
				MessageBox.Show("No vendor found matching that number", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Information);
				cboVendors.Focus();
				return;
			}
			fraVendorSelect.Visible = false;
			blnVendorSelected = true;
			cmdPrint_Click();
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdPreview, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			int counter;
			string temp = "";
			for (counter = 0; counter <= vsWhere.Rows - 1; counter++)
			{
				if (vsWhere.TextMatrix(counter, 1) == "" && vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 1) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					MessageBox.Show("You must fill out all the selection criteria before you may print this report", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 2) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					// do nothing
				}
				else
				{
					if (vsWhere.TextMatrix(counter, 2) == "")
					{
						MessageBox.Show("You must fill out all the selection criteria before you may print this report", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (vsWhere.TextMatrix(2, 1) == "M")
			{
				if (!modBudgetaryMaster.CheckReportDateRange(FCConvert.ToInt16(vsWhere.TextMatrix(3, 1)), FCConvert.ToInt16(vsWhere.TextMatrix(3, 2))))
				{
					MessageBox.Show("Your Ending Month must be greater than your Beginning Month", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (vsWhere.TextMatrix(2, 1) == "D")
			{
				if (Information.IsDate(vsWhere.TextMatrix(3, 1)) && Information.IsDate(vsWhere.TextMatrix(3, 2)))
				{
					if (DateAndTime.DateValue(vsWhere.TextMatrix(3, 1)).ToOADate() > DateAndTime.DateValue(vsWhere.TextMatrix(3, 2)).ToOADate())
					{
						MessageBox.Show("Your Ending Month must be greater than your Beginning Month", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (!modBudgetaryMaster.CheckReportDateRange_80(FCConvert.ToInt16(FCConvert.ToDateTime(vsWhere.TextMatrix(3, 1)).Month), FCConvert.ToInt16(FCConvert.ToDateTime(vsWhere.TextMatrix(3, 2)).Month), FCConvert.ToInt16(FCConvert.ToDateTime(vsWhere.TextMatrix(3, 1)).Year), FCConvert.ToInt16(FCConvert.ToDateTime(vsWhere.TextMatrix(3, 2)).Year)))
					{
						MessageBox.Show("Your date range extends into multiple fiscal years", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					MessageBox.Show("You must enter valid dates before you may proceed.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (vsWhere.TextMatrix(0, 1) == "1")
			{
				if (blnVendorSelected)
				{
					blnVendorSelected = false;
				}
				else
				{
					cboVendors.SelectedIndex = 0;
					fraVendorSelect.Visible = true;
					cboVendors.Focus();
					return;
				}
			}
			else if (vsWhere.TextMatrix(0, 1) == "4" || vsWhere.TextMatrix(0, 1) == "5")
			{
				if (blnAccountSelected)
				{
					// do nothing
				}
				else
				{
					if (vsWhere.TextMatrix(1, 1) == "A")
					{
						// do nothing
					}
					else if (vsWhere.TextMatrix(1, 1) == "R")
					{
						vsLowAccount.Visible = true;
						vsHighAccount.Visible = true;
						vsSingleAccount.Visible = false;
						vsLowAccount.Text = "";
						vsHighAccount.Text = "";
						lblTo.Visible = true;
						fraAccountRange.Visible = true;
						vsLowAccount.Focus();
						return;
					}
					else
					{
						vsLowAccount.Visible = false;
						vsHighAccount.Visible = false;
						vsSingleAccount.Visible = true;
						vsSingleAccount.Text = "";
						fraAccountRange.Visible = true;
						vsSingleAccount.Focus();
						return;
					}
				}
			}
			rptVendorDetail.InstancePtr.blnFromAP = false;
			if (vsWhere.TextMatrix(2, 1) == "A")
			{
				rptVendorDetail.InstancePtr.Init(vsWhere.TextMatrix(0, 1), vsWhere.TextMatrix(1, 1), vsWhere.TextMatrix(2, 1), vsWhere.TextMatrix(4, 1) == "Y", this.Modal);
			}
			else if (vsWhere.TextMatrix(2, 1) == "M")
			{
				rptVendorDetail.InstancePtr.Init(vsWhere.TextMatrix(0, 1), vsWhere.TextMatrix(1, 1), vsWhere.TextMatrix(2, 1), vsWhere.TextMatrix(4, 1) == "Y", this.Modal, FCConvert.ToInt16(vsWhere.TextMatrix(3, 1)), FCConvert.ToInt16(vsWhere.TextMatrix(3, 2)));
			}
			else if (vsWhere.TextMatrix(2, 1) == "D")
			{
				rptVendorDetail.InstancePtr.Init(vsWhere.TextMatrix(0, 1), vsWhere.TextMatrix(1, 1), vsWhere.TextMatrix(2, 1), vsWhere.TextMatrix(4, 1) == "Y", this.Modal, 0, 0, DateAndTime.DateValue(vsWhere.TextMatrix(3, 1)), DateAndTime.DateValue(vsWhere.TextMatrix(3, 2)));
			}
			else
			{
				rptVendorDetail.InstancePtr.Init(vsWhere.TextMatrix(0, 1), vsWhere.TextMatrix(1, 1), vsWhere.TextMatrix(2, 1), vsWhere.TextMatrix(4, 1) == "Y", this.Modal, FCConvert.ToInt16(vsWhere.TextMatrix(3, 1)), FCConvert.ToInt16(vsWhere.TextMatrix(3, 1)));
			}
			//Application.DoEvents();
			// If Not blnPrint Then
			// Me.Hide
			// End If
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPreview, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			int intRow;
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			vsWhere.Select(0, 0);
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			for (intRow = 1; intRow <= vsLayout.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= vsLayout.Cols - 1; intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							break;
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.Items[intRow].Text == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
			{
				modCustomReport.Statics.intNumberOfSQLFields = -1;
				return;
			}
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					strSort += FCConvert.ToString(modCustomReport.CheckForAS_6(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (Strings.Trim(strSort) != string.Empty)
			{
				modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOL WRAPPED AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and " + Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "'";
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 3) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = " + vsWhere.TextMatrix(intCounter, 3);
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strFields[intCounter]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "%'";
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + modCustomReport.Statics.strFields[intCounter] + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + modCustomReport.Statics.strFields[intCounter] + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += modCustomReport.Statics.strFields[intCounter] + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "%'";
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (strWhere != " ")
			{
				modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
			}
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			fraMultiple.Visible = false;
			txtCriteria.Text = "";
			fraSearch.Visible = true;
			if (txtCriteria.Visible)
			{
				txtCriteria.Focus();
			}
		}

		private void cmdRunSearch_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			int counter;
			//Application.DoEvents();
			rsVendorInfo.OpenRecordset("SELECT DISTINCT VendorNumber, VendorName FROM (SELECT * FROM (SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM APJournal WHERE VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo WHERE VendorName Like '" + Strings.Trim(txtCriteria.Text) + "%' UNION ALL SELECT VendorNumber as VendorNumber, CheckName as VendorName FROM VendorMaster WHERE Status = 'A' AND CheckName Like '" + Strings.Trim(txtCriteria.Text) + "%' UNION ALL SELECT * FROM (SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM Encumbrances WHERE VendorNumber = 0 AND Description <> 'Control Entries') as EncTempVendorInfo WHERE VendorName Like '" + Strings.Trim(txtCriteria.Text) + "%') as temp ORDER BY VendorNumber");
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				if (rsVendorInfo.RecordCount() > 1)
				{
					vsVendors.Rows = 1;
					do
					{
						vsVendors.Rows += 1;
						vsVendors.TextMatrix(vsVendors.Rows - 1, 0, modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5));
						vsVendors.TextMatrix(vsVendors.Rows - 1, 1, FCConvert.ToString(rsVendorInfo.Get_Fields_String("VendorName")));
						rsVendorInfo.MoveNext();
					}
					while (rsVendorInfo.EndOfFile() != true);
					fraSearch.Visible = false;
					fraMultiple.Visible = true;
					vsVendors.Focus();
				}
				else
				{
					for (counter = 0; counter <= cboVendors.Items.Count - 1; counter++)
					{
						if (Strings.Left(cboVendors.Items[counter].ToString(), 5) == modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5))
						{
							cboVendors.SelectedIndex = counter;
							break;
						}
					}
					fraVendorSelect.Visible = true;
					fraSearch.Visible = false;
					strSearch = "";
					cmdOK_Click();
				}
			}
			else
			{
				MessageBox.Show("No vendors found that match this criteria.", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void cmdRunSearch_Click()
		{
			cmdRunSearch_Click(cmdRunSearch, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			if (strSearch != "")
			{
				txtCriteria.Text = strSearch;
				strSearch = "";
				//Application.DoEvents();
				fraVendorSelect.Visible = false;
				fraSearch.Visible = true;
				cmdRunSearch_Click();
				return;
			}
			fraVendorSelect.Visible = false;
			fraSearch.Visible = true;
			txtCriteria.Focus();
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmVendorDetail_Activated(object sender, System.EventArgs e)
		{
			Form_Resize();
		}

		private void frmVendorDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			//FC:FINAL:MSH - Issue #548: ActiveControl can be equal NULL and in this case is impossible to get element name. Will be throw a null reference exception as result.
			if (this.ActiveControl != null)
			{
				if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
				{
					modNewAccountBox.CheckFormKeyDown(vsLowAccount, KeyCode, Shift, vsLowAccount.SelectionStart, vsLowAccount.Text, vsLowAccount.SelectionLength);
				}
				else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
				{
					if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
					{
						modNewAccountBox.CheckFormKeyDown(vsHighAccount, KeyCode, Shift, vsHighAccount.SelectionStart, vsHighAccount.Text, vsHighAccount.SelectionLength);
					}
				}
				else if (Strings.UCase(this.ActiveControl.GetName()) == "VSSINGLEACCOUNT")
				{
					if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
					{
						modNewAccountBox.CheckFormKeyDown(vsSingleAccount, KeyCode, Shift, vsSingleAccount.SelectionStart, vsSingleAccount.Text, vsSingleAccount.SelectionLength);
					}
				}
			}
			//FC:FINAL:MSH - Added to prevent double event processing
			e.Handled = true;
		}

		private void frmVendorDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVendorDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendorDetail.ScaleWidth	= 8880;
			//frmVendorDetail.ScaleHeight	= 7395;
			//frmVendorDetail.LinkTopic	= "Form1";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			vsLowGrid.AccountInputEditor = vsLowAccount;
			vsHighGrid.AccountInputEditor = vsHighAccount;
			vsSingleGrid.AccountInputEditor = vsSingleAccount;
			vsLowGrid.DefaultAccountType = "E";
			//vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.DefaultAccountType = "E";
			//vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsSingleGrid.DefaultAccountType = "E";
			//vsSingleGrid.AccountCol = -1;
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			//Image1.Width = 1440 * 14;
			Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.Width > Frame1.Width)
			//{
			//    HScroll1.Maximum = Image1.Width - Frame1.Width;
			//}
			//else
			//{
			//    HScroll1.Enabled = false;
			//}
			strTemp = "SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM APJournal WHERE VendorNumber = 0 AND Description <> 'Control Entries'";
			// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
			strTemp = "SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM Encumbrances WHERE VendorNumber = 0 AND Description <> 'Control Entries'";
			// rsTemp.CreateStoredProcedure "EncTempVendorInfo", strTemp
			rsTemp.OpenRecordset($"SELECT DISTINCT VendorNumber, VendorName FROM (SELECT * FROM (SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM APJournal WHERE VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo WHERE VendorName Like '{txtCriteria.Text}%' UNION ALL SELECT VendorNumber as VendorNumber, CheckName as VendorName FROM VendorMaster WHERE Status = 'A' UNION ALL SELECT * FROM (SELECT VendorNumber, rtrim(TempVendorName) as VendorName FROM Encumbrances WHERE VendorNumber = 0 AND Description <> 'Control Entries') as EncTempVendorInfo) as Temp ORDER BY VendorName");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				do
				{
					cboVendors.AddItem(modValidateAccount.GetFormat_6(rsTemp.Get_Fields_Int32("VendorNumber"), 5) + "  " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("VendorName"))));
					rsTemp.MoveNext();
				}
				while (rsTemp.EndOfFile() != true);
			}
			vsVendors.TextMatrix(0, 0, "Vndr #");
			vsVendors.ColWidth(0, 800);
			vsVendors.TextMatrix(0, 1, "Vendor");
			// vsWhere.ColWidth(1) = (vsWhere.Width - vsWhere.ColWidth(0)) * 0.45
			// vsWhere.ColWidth(2) = (vsWhere.Width - vsWhere.ColWidth(0)) * 0.45
			//Line1.X1 = Image1.Left + (1440 * 8) - 900;
			//Line1.X2 = Image1.Left + (1440 * 8) - 900;
			//Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
			intChoices = 0;
			blnVendorSelected = false;
			blnAccountSelected = false;
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmVendorDetail_Resize(object sender, System.EventArgs e)
		{
			vsWhere.Columns[1].Width = 200;
			vsWhere.Columns[2].Width = 200;
			//vsWhere.ColWidth(1, FCConvert.ToInt32((vsWhere.WidthOriginal - vsWhere.ColWidth(0)) * 0.493));
			//vsWhere.ColWidth(2, FCConvert.ToInt32((vsWhere.WidthOriginal - vsWhere.ColWidth(0)) * 0.493));
			fraVendorSelect.CenterToContainer(this.ClientArea);
			fraSearch.CenterToContainer(this.ClientArea);
			fraMultiple.CenterToContainer(this.ClientArea);
			fraAccountRange.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmVendorDetail_Resize(this, new System.EventArgs());
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = 720 + (-HScroll1.Value + 30);
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = -HScroll1.Value + 30;
			//vsLayout.Left = 720 + (-HScroll1.Value + 30);
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from tblCustomReports where Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DEFAULTDATABASE);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	if (intStart != lstSort.SelectedIndex)
		//	{
		//		// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//		ListViewItem temp = lstSort.Items[lstSort.SelectedIndex];
		//		intID = lstSort.ItemData(lstSort.SelectedIndex);
		//		// CHANGE THE NEW ITEM
		//		lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//		lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//		// SAVE THE OLD ITEM
		//		lstSort.Items[intStart] = temp;
		//		lstSort.ItemData(intStart, intID);
		//		// SET BOTH ITEMS TO BE SELECTED
		//		lstSort.SetSelected(lstSort.ListIndex, true);
		//		lstSort.SetSelected(intStart, true);
		//	}
		//}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Rows += 1;
			vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			blnPrint = true;
			vsWhere.Select(0, 0);
			cmdPrint_Click();
		}

		private void mnuPreview_Click(object sender, System.EventArgs e)
		{
			blnPrint = false;
			vsWhere.Select(0, 0);
			cmdPrint_Click();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			fraSort.Enabled = Index == 0;
			fraFields.Enabled = Index == 0;
			fraWhere.Enabled = Index == 0;
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:BBE - Correct indexes on the converted FCRadioButton in FCComboBox
			//int index = optReport.IndexOf((FCRadioButton)sender);
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void txtCriteria_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdRunSearch_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsVendors_DblClick(object sender, System.EventArgs e)
		{
			strSearch = "";
			cmdGet_Click();
		}

		private void vsVendors_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				strSearch = "";
				keyAscii = 0;
				cmdGet_Click();
			}
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOTEXT:
					{
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsWhere.IsCurrentCellInEditMode)
			{
				if (vsWhere.Row == 2)
				{
					if (vsWhere.EditText == "A")
					{
						modCustomReport.Statics.strWhereType[3] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
						modCustomReport.LoadGridCellAsCombo(this, 3, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
					}
					else if (vsWhere.EditText == "M")
					{
						modCustomReport.Statics.strWhereType[3] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
						modCustomReport.LoadGridCellAsCombo(this, 3, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, Color.White);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
					}
					else if (vsWhere.EditText == "S")
					{
						modCustomReport.Statics.strWhereType[3] = FCConvert.ToString(modCustomReport.GRIDCOMBOIDTEXT);
						modCustomReport.LoadGridCellAsCombo(this, 3, "#0;01" + "\t" + "January|#1;02" + "\t" + "February|#2;03" + "\t" + "March|#3;04" + "\t" + "April|#4;05" + "\t" + "May|#5;06" + "\t" + "June|#6;07" + "\t" + "July|#7;08" + "\t" + "August|#8;09" + "\t" + "September|#9;10" + "\t" + "October|#10;11" + "\t" + "November|#11;12" + "\t" + "December");
						vsWhere.TextMatrix(3, 1, "");
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
						vsWhere.TextMatrix(3, 2, "");
					}
					else
					{
						modCustomReport.Statics.strWhereType[3] = FCConvert.ToString(modCustomReport.GRIDDATE);
						vsWhere.TextMatrix(3, 2, "");
						vsWhere.TextMatrix(3, 1, "");
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, Color.White);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, Color.White);
					}
				}
				if (vsWhere.Row == 0)
				{
					if (vsWhere.EditText != "4" && vsWhere.EditText != "5")
					{
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.TextMatrix(1, 2, "");
						vsWhere.TextMatrix(1, 1, "");
					}
					else
					{
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, Color.White);
						vsWhere.TextMatrix(1, 2, "");
						vsWhere.TextMatrix(1, 1, "A");
					}
					if (vsWhere.EditText == "1")
					{
						ShowVendorSearchMenu();
					}
					else
					{
						ShowPrintMenu();
					}
				}
			}
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vsWhere.Row > 0)
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row - 1, 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (vsWhere.Row > 1)
						{
							KeyCode = 0;
							blnEditFlag = true;
							vsWhere.Select(vsWhere.Row - 2, 1);
							blnEditFlag = false;
						}
						else
						{
							KeyCode = 0;
						}
					}
				}
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (!blnEditFlag)
			{
				if (vsWhere.Col == 2)
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							if (vsWhere.Row == vsWhere.Rows - 1)
							{
								vsWhere.Select(vsWhere.Row - 1, 1);
							}
							else
							{
								vsWhere.Select(vsWhere.Row + 1, 1);
							}
						}
						else
						{
							vsWhere.Col = 1;
						}
					}
				}
				else if (vsWhere.Col == 1)
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (vsWhere.Row == vsWhere.Rows - 1)
						{
							vsWhere.Select(vsWhere.Row - 1, 1);
						}
						else
						{
							vsWhere.Select(vsWhere.Row + 1, 1);
						}
					}
					else
					{
						vsWhere.Col = 1;
					}
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!modCustomReport.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
		}

		private void SetCustomFormColors()
		{
			Label3.ForeColor = ColorTranslator.FromOle(0xC00000);
			Label2.ForeColor = ColorTranslator.FromOle(0xC00000);
			Label1.ForeColor = ColorTranslator.FromOle(0xC00000);
		}

		private void ShowPrintMenu()
		{
			mnuFilePrint.Visible = true;
			cmdPreview.Text = "Print Preview";
		}

		private void ShowVendorSearchMenu()
		{
			mnuFilePrint.Visible = false;
			cmdPreview.Text = "Select Vendor";
		}

		private void cmdPreview_Click(object sender, EventArgs e)
		{
			mnuPreview_Click(sender, e);
		}
	}
}
