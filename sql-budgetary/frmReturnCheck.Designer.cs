﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReturnCheck.
	/// </summary>
	partial class frmReturnCheck : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
		public fecherFoundation.FCFrame fraJournalSave;
		public fecherFoundation.FCComboBox cboSavePeriod;
		public fecherFoundation.FCTextBox txtJournalDescription;
		public fecherFoundation.FCButton cmdCancelSave;
		public fecherFoundation.FCButton cmdOKSave;
		public fecherFoundation.FCComboBox cboSaveJournal;
		public fecherFoundation.FCLabel lblSavePeriod;
		public fecherFoundation.FCLabel lblJournalDescription;
		public fecherFoundation.FCLabel lblJournalSave;
		public fecherFoundation.FCLabel lblSaveInstructions;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCFrame fraBorder;
		public fecherFoundation.FCComboBox cboJournal;
		public Global.T2KOverTypeBox txtPeriod;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCLabel lblPeriod;
		public fecherFoundation.FCGrid vsInfo;
		public Global.T2KOverTypeBox txtAddress_0;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtCheck;
		public Global.T2KOverTypeBox txtAddress_1;
		public Global.T2KOverTypeBox txtAddress_2;
		public Global.T2KOverTypeBox txtAddress_3;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblCheck;
		public fecherFoundation.FCButton cmdProcessSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReturnCheck));
            this.fraJournalSave = new fecherFoundation.FCFrame();
            this.cboSavePeriod = new fecherFoundation.FCComboBox();
            this.txtJournalDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelSave = new fecherFoundation.FCButton();
            this.cmdOKSave = new fecherFoundation.FCButton();
            this.cboSaveJournal = new fecherFoundation.FCComboBox();
            this.lblSavePeriod = new fecherFoundation.FCLabel();
            this.lblJournalDescription = new fecherFoundation.FCLabel();
            this.lblJournalSave = new fecherFoundation.FCLabel();
            this.lblSaveInstructions = new fecherFoundation.FCLabel();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.fraBorder = new fecherFoundation.FCFrame();
            this.cboJournal = new fecherFoundation.FCComboBox();
            this.txtPeriod = new Global.T2KOverTypeBox();
            this.lblJournal = new fecherFoundation.FCLabel();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.vsInfo = new fecherFoundation.FCGrid();
            this.txtAddress_0 = new Global.T2KOverTypeBox();
            this.txtVendor = new Global.T2KOverTypeBox();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.txtAddress_1 = new Global.T2KOverTypeBox();
            this.txtAddress_2 = new Global.T2KOverTypeBox();
            this.txtAddress_3 = new Global.T2KOverTypeBox();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblCheck = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
            this.fraJournalSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).BeginInit();
            this.fraBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraJournalSave);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.fraBorder);
            this.ClientArea.Controls.Add(this.vsInfo);
            this.ClientArea.Controls.Add(this.txtAddress_0);
            this.ClientArea.Controls.Add(this.txtVendor);
            this.ClientArea.Controls.Add(this.txtCheck);
            this.ClientArea.Controls.Add(this.txtAddress_1);
            this.ClientArea.Controls.Add(this.txtAddress_2);
            this.ClientArea.Controls.Add(this.txtAddress_3);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblCheck);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(162, 30);
            this.HeaderText.Text = "Return Check";
            // 
            // fraJournalSave
            // 
            this.fraJournalSave.BackColor = System.Drawing.Color.White;
            this.fraJournalSave.Controls.Add(this.cboSavePeriod);
            this.fraJournalSave.Controls.Add(this.txtJournalDescription);
            this.fraJournalSave.Controls.Add(this.cmdCancelSave);
            this.fraJournalSave.Controls.Add(this.cmdOKSave);
            this.fraJournalSave.Controls.Add(this.cboSaveJournal);
            this.fraJournalSave.Controls.Add(this.lblSavePeriod);
            this.fraJournalSave.Controls.Add(this.lblJournalDescription);
            this.fraJournalSave.Controls.Add(this.lblJournalSave);
            this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
            this.fraJournalSave.Location = new System.Drawing.Point(30, 30);
            this.fraJournalSave.Name = "fraJournalSave";
            this.fraJournalSave.Size = new System.Drawing.Size(953, 323);
            this.fraJournalSave.TabIndex = 14;
            this.fraJournalSave.Text = "Save Journal";
            this.fraJournalSave.Visible = false;
            // 
            // cboSavePeriod
            // 
            this.cboSavePeriod.Location = new System.Drawing.Point(165, 182);
            this.cboSavePeriod.Name = "cboSavePeriod";
            this.cboSavePeriod.Size = new System.Drawing.Size(170, 40);
            this.cboSavePeriod.TabIndex = 6;
            // 
            // txtJournalDescription
            // 
            this.txtJournalDescription.Location = new System.Drawing.Point(165, 122);
            this.txtJournalDescription.MaxLength = 100;
            this.txtJournalDescription.Name = "txtJournalDescription";
            this.txtJournalDescription.Size = new System.Drawing.Size(303, 40);
            this.txtJournalDescription.TabIndex = 4;
            // 
            // cmdCancelSave
            // 
            this.cmdCancelSave.AppearanceKey = "actionButton";
            this.cmdCancelSave.Location = new System.Drawing.Point(395, 242);
            this.cmdCancelSave.Name = "cmdCancelSave";
            this.cmdCancelSave.Size = new System.Drawing.Size(96, 40);
            this.cmdCancelSave.TabIndex = 8;
            this.cmdCancelSave.Text = "Cancel";
            this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
            // 
            // cmdOKSave
            // 
            this.cmdOKSave.AppearanceKey = "actionButton";
            this.cmdOKSave.Location = new System.Drawing.Point(303, 242);
            this.cmdOKSave.Name = "cmdOKSave";
            this.cmdOKSave.Size = new System.Drawing.Size(86, 40);
            this.cmdOKSave.TabIndex = 7;
            this.cmdOKSave.Text = "OK";
            this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
            // 
            // cboSaveJournal
            // 
            this.cboSaveJournal.Location = new System.Drawing.Point(165, 62);
            this.cboSaveJournal.Name = "cboSaveJournal";
            this.cboSaveJournal.Size = new System.Drawing.Size(170, 40);
            this.cboSaveJournal.TabIndex = 2;
            this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
            this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
            // 
            // lblSavePeriod
            // 
            this.lblSavePeriod.Location = new System.Drawing.Point(20, 196);
            this.lblSavePeriod.Name = "lblSavePeriod";
            this.lblSavePeriod.Size = new System.Drawing.Size(58, 16);
            this.lblSavePeriod.TabIndex = 5;
            this.lblSavePeriod.Text = "PERIOD";
            this.lblSavePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJournalDescription
            // 
            this.lblJournalDescription.Location = new System.Drawing.Point(20, 136);
            this.lblJournalDescription.Name = "lblJournalDescription";
            this.lblJournalDescription.Size = new System.Drawing.Size(88, 16);
            this.lblJournalDescription.TabIndex = 3;
            this.lblJournalDescription.Text = "DESCRIPTION";
            this.lblJournalDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJournalSave
            // 
            this.lblJournalSave.Location = new System.Drawing.Point(20, 76);
            this.lblJournalSave.Name = "lblJournalSave";
            this.lblJournalSave.Size = new System.Drawing.Size(58, 16);
            this.lblJournalSave.TabIndex = 1;
            this.lblJournalSave.Text = "JOURNAL";
            this.lblJournalSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaveInstructions
            // 
            this.lblSaveInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblSaveInstructions.Name = "lblSaveInstructions";
            this.lblSaveInstructions.Size = new System.Drawing.Size(798, 16);
            this.lblSaveInstructions.Text = "PLEASE SELECT THE JOURNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION F" +
    "OR THE JOURNAL, AND CLICK THE OK BUTTON";
            this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZip4
            // 
            this.txtZip4.Appearance = 0;
            this.txtZip4.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtZip4.Location = new System.Drawing.Point(840, 330);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(106, 40);
            this.txtZip4.TabIndex = 12;
            // 
            // txtZip
            // 
            this.txtZip.Appearance = 0;
            this.txtZip.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtZip.Location = new System.Drawing.Point(716, 330);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.TabIndex = 11;
            // 
            // txtState
            // 
            this.txtState.Appearance = 0;
            this.txtState.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtState.Location = new System.Drawing.Point(586, 330);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.TabIndex = 10;
            // 
            // txtCity
            // 
            this.txtCity.Appearance = 0;
            this.txtCity.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtCity.Location = new System.Drawing.Point(423, 330);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(132, 40);
            this.txtCity.TabIndex = 9;
            // 
            // fraBorder
            // 
            this.fraBorder.AppearanceKey = "groupBoxNoBorders";
            this.fraBorder.Controls.Add(this.cboJournal);
            this.fraBorder.Controls.Add(this.txtPeriod);
            this.fraBorder.Controls.Add(this.lblJournal);
            this.fraBorder.Controls.Add(this.lblPeriod);
            this.fraBorder.Location = new System.Drawing.Point(30, 90);
            this.fraBorder.Name = "fraBorder";
            this.fraBorder.Size = new System.Drawing.Size(315, 116);
            this.fraBorder.TabIndex = 2;
            // 
            // cboJournal
            // 
            this.cboJournal.Location = new System.Drawing.Point(155, 0);
            this.cboJournal.Name = "cboJournal";
            this.cboJournal.Size = new System.Drawing.Size(150, 40);
            this.cboJournal.TabIndex = 1;
            this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
            this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
            // 
            // txtPeriod
            // 
            this.txtPeriod.Enabled = false;
            this.txtPeriod.Location = new System.Drawing.Point(155, 60);
            this.txtPeriod.MaxLength = 2;
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.Size = new System.Drawing.Size(150, 40);
            this.txtPeriod.TabIndex = 3;
            this.txtPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.txtPeriod_Validate);
            // 
            // lblJournal
            // 
            this.lblJournal.Location = new System.Drawing.Point(0, 14);
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Size = new System.Drawing.Size(55, 16);
            this.lblJournal.Text = "JOURNAL";
            this.lblJournal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPeriod
            // 
            this.lblPeriod.Location = new System.Drawing.Point(0, 74);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(94, 16);
            this.lblPeriod.TabIndex = 2;
            this.lblPeriod.Text = "ACCTG PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vsInfo
            // 
            this.vsInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsInfo.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.Fill;
            this.vsInfo.Cols = 4;
            this.vsInfo.ExtendLastCol = true;
            this.vsInfo.FixedCols = 0;
            this.vsInfo.Location = new System.Drawing.Point(30, 390);
            this.vsInfo.Name = "vsInfo";
            this.vsInfo.RowHeadersVisible = false;
            this.vsInfo.Rows = 1;
            this.vsInfo.Size = new System.Drawing.Size(933, 253);
            this.vsInfo.TabIndex = 13;
            // 
            // txtAddress_0
            // 
            this.txtAddress_0.Enabled = false;
            this.txtAddress_0.Location = new System.Drawing.Point(423, 90);
            this.txtAddress_0.MaxLength = 35;
            this.txtAddress_0.Name = "txtAddress_0";
            this.txtAddress_0.Size = new System.Drawing.Size(523, 40);
            this.txtAddress_0.TabIndex = 5;
            // 
            // txtVendor
            // 
            this.txtVendor.Enabled = false;
            this.txtVendor.Location = new System.Drawing.Point(544, 30);
            this.txtVendor.MaxLength = 5;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new System.Drawing.Size(150, 40);
            this.txtVendor.TabIndex = 4;
            // 
            // txtCheck
            // 
            this.txtCheck.Enabled = false;
            this.txtCheck.Location = new System.Drawing.Point(185, 30);
            this.txtCheck.MaxLength = 6;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(150, 40);
            this.txtCheck.TabIndex = 1;
            // 
            // txtAddress_1
            // 
            this.txtAddress_1.Enabled = false;
            this.txtAddress_1.Location = new System.Drawing.Point(423, 150);
            this.txtAddress_1.MaxLength = 35;
            this.txtAddress_1.Name = "txtAddress_1";
            this.txtAddress_1.Size = new System.Drawing.Size(523, 40);
            this.txtAddress_1.TabIndex = 6;
            // 
            // txtAddress_2
            // 
            this.txtAddress_2.Enabled = false;
            this.txtAddress_2.Location = new System.Drawing.Point(423, 210);
            this.txtAddress_2.MaxLength = 35;
            this.txtAddress_2.Name = "txtAddress_2";
            this.txtAddress_2.Size = new System.Drawing.Size(523, 40);
            this.txtAddress_2.TabIndex = 7;
            // 
            // txtAddress_3
            // 
            this.txtAddress_3.Enabled = false;
            this.txtAddress_3.Location = new System.Drawing.Point(423, 270);
            this.txtAddress_3.MaxLength = 35;
            this.txtAddress_3.Name = "txtAddress_3";
            this.txtAddress_3.Size = new System.Drawing.Size(523, 40);
            this.txtAddress_3.TabIndex = 8;
            // 
            // lblVendor
            // 
            this.lblVendor.Location = new System.Drawing.Point(423, 44);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(63, 16);
            this.lblVendor.TabIndex = 3;
            this.lblVendor.Text = "VENDOR";
            this.lblVendor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCheck
            // 
            this.lblCheck.Location = new System.Drawing.Point(30, 44);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(63, 16);
            this.lblCheck.Text = "CHECK#";
            this.lblCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
            this.cmdProcessSave.Text = "Process";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmReturnCheck
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmReturnCheck";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Return Check";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmReturnCheck_Load);
            this.Activated += new System.EventHandler(this.frmReturnCheck_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReturnCheck_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
            this.fraJournalSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).EndInit();
            this.fraBorder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
