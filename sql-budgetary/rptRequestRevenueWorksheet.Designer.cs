﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptRequestRevenueWorksheet.
	/// </summary>
	partial class rptRequestRevenueWorksheet
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRequestRevenueWorksheet));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrentBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrentBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInitialBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInitialBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblManagerBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblManagerBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCommitteeBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCommitteeBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblApprovedBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblApprovedBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblElectedBudget = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblElectedBudget2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDeptDivTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldComments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInitialBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldManagerBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCommitteeBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldApprovedAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldElectedBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalCurrentBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalCurrentBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalInitialRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalInitialRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalManagerRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalManagerRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalCommitteeRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalCommitteeRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalApprovedAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalApprovedAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDivisionTotalElectedRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepartmentTotalElectedRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCurrentBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInitialRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalManagerRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCommitteeRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalApprovedAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalElectedRequest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInitialBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInitialBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblManagerBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblManagerBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCommitteeBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCommitteeBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApprovedBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApprovedBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblElectedBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblElectedBudget2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptDivTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInitialBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldManagerBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitteeBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldApprovedAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldElectedBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalCurrentBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalCurrentBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalInitialRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalInitialRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalManagerRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalManagerRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalCommitteeRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalCommitteeRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalApprovedAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalApprovedAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalElectedRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalElectedRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalManagerRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCommitteeRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalApprovedAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalElectedRequest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldCurrentBudget,
				this.fldComments,
				this.fldInitialBudget,
				this.fldManagerBudget,
				this.fldCommitteeBudget,
				this.fldApprovedAmount,
				this.Line3,
				this.fldElectedBudget
			});
			this.Detail.Height = 0.4270833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Line2,
				this.Label14,
				this.lblAccount,
				this.lblCurrentBudget,
				this.lblCurrentBudget2,
				this.lblInitialBudget,
				this.lblInitialBudget2,
				this.lblManagerBudget,
				this.lblManagerBudget2,
				this.lblCommitteeBudget,
				this.lblCommitteeBudget2,
				this.lblApprovedBudget,
				this.lblApprovedBudget2,
				this.lblElectedBudget,
				this.lblElectedBudget2
			});
			this.PageHeader.Height = 0.9583333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalName,
				this.fldTotalCurrentBudget,
				this.fldTotalInitialRequest,
				this.fldTotalManagerRequest,
				this.fldTotalCommitteeRequest,
				this.fldTotalApprovedAmount,
				this.fldTotalElectedRequest,
				this.Line1
			});
			this.GroupFooter2.Height = 0.28125F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanShrink = true;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDeptDivTitle,
				this.Field1
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.3333333F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDivisionName,
				this.fldDepartmentName,
				this.fldDivisionTotalCurrentBudget,
				this.fldDepartmentTotalCurrentBudget,
				this.fldDivisionTotalInitialRequest,
				this.fldDepartmentTotalInitialRequest,
				this.fldDivisionTotalManagerRequest,
				this.fldDepartmentTotalManagerRequest,
				this.fldDivisionTotalCommitteeRequest,
				this.fldDepartmentTotalCommitteeRequest,
				this.fldDivisionTotalApprovedAmount,
				this.fldDepartmentTotalApprovedAmount,
				this.fldDivisionTotalElectedRequest,
				this.fldDepartmentTotalElectedRequest
			});
			this.GroupFooter1.Height = 0.4270833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Initial Request Worksheet";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.9375F;
			this.Line2.Width = 7.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0.9375F;
			this.Line2.Y2 = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.21875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.5F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label14.Text = "Revenue";
			this.Label14.Top = 0.21875F;
			this.Label14.Width = 4.6875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.03125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.75F;
			this.lblAccount.Width = 2.46875F;
			// 
			// lblCurrentBudget
			// 
			this.lblCurrentBudget.Height = 0.1875F;
			this.lblCurrentBudget.HyperLink = null;
			this.lblCurrentBudget.Left = 2.59375F;
			this.lblCurrentBudget.Name = "lblCurrentBudget";
			this.lblCurrentBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCurrentBudget.Text = "Current";
			this.lblCurrentBudget.Top = 0.5625F;
			this.lblCurrentBudget.Width = 0.90625F;
			// 
			// lblCurrentBudget2
			// 
			this.lblCurrentBudget2.Height = 0.1875F;
			this.lblCurrentBudget2.HyperLink = null;
			this.lblCurrentBudget2.Left = 2.59375F;
			this.lblCurrentBudget2.Name = "lblCurrentBudget2";
			this.lblCurrentBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCurrentBudget2.Text = "Budget";
			this.lblCurrentBudget2.Top = 0.75F;
			this.lblCurrentBudget2.Width = 0.90625F;
			// 
			// lblInitialBudget
			// 
			this.lblInitialBudget.Height = 0.1875F;
			this.lblInitialBudget.HyperLink = null;
			this.lblInitialBudget.Left = 3.59375F;
			this.lblInitialBudget.Name = "lblInitialBudget";
			this.lblInitialBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblInitialBudget.Text = "Budget";
			this.lblInitialBudget.Top = 0.5625F;
			this.lblInitialBudget.Width = 0.90625F;
			// 
			// lblInitialBudget2
			// 
			this.lblInitialBudget2.Height = 0.1875F;
			this.lblInitialBudget2.HyperLink = null;
			this.lblInitialBudget2.Left = 3.59375F;
			this.lblInitialBudget2.Name = "lblInitialBudget2";
			this.lblInitialBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblInitialBudget2.Text = "Initial";
			this.lblInitialBudget2.Top = 0.75F;
			this.lblInitialBudget2.Width = 0.90625F;
			// 
			// lblManagerBudget
			// 
			this.lblManagerBudget.Height = 0.1875F;
			this.lblManagerBudget.HyperLink = null;
			this.lblManagerBudget.Left = 4.59375F;
			this.lblManagerBudget.Name = "lblManagerBudget";
			this.lblManagerBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblManagerBudget.Text = "Current";
			this.lblManagerBudget.Top = 0.5625F;
			this.lblManagerBudget.Width = 0.90625F;
			// 
			// lblManagerBudget2
			// 
			this.lblManagerBudget2.Height = 0.1875F;
			this.lblManagerBudget2.HyperLink = null;
			this.lblManagerBudget2.Left = 4.59375F;
			this.lblManagerBudget2.Name = "lblManagerBudget2";
			this.lblManagerBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblManagerBudget2.Text = "Manager";
			this.lblManagerBudget2.Top = 0.75F;
			this.lblManagerBudget2.Width = 0.90625F;
			// 
			// lblCommitteeBudget
			// 
			this.lblCommitteeBudget.Height = 0.1875F;
			this.lblCommitteeBudget.HyperLink = null;
			this.lblCommitteeBudget.Left = 5.59375F;
			this.lblCommitteeBudget.Name = "lblCommitteeBudget";
			this.lblCommitteeBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCommitteeBudget.Text = "Budget";
			this.lblCommitteeBudget.Top = 0.5625F;
			this.lblCommitteeBudget.Width = 0.90625F;
			// 
			// lblCommitteeBudget2
			// 
			this.lblCommitteeBudget2.Height = 0.1875F;
			this.lblCommitteeBudget2.HyperLink = null;
			this.lblCommitteeBudget2.Left = 5.59375F;
			this.lblCommitteeBudget2.Name = "lblCommitteeBudget2";
			this.lblCommitteeBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblCommitteeBudget2.Text = "Committee";
			this.lblCommitteeBudget2.Top = 0.75F;
			this.lblCommitteeBudget2.Width = 0.90625F;
			// 
			// lblApprovedBudget
			// 
			this.lblApprovedBudget.Height = 0.1875F;
			this.lblApprovedBudget.HyperLink = null;
			this.lblApprovedBudget.Left = 6.59375F;
			this.lblApprovedBudget.Name = "lblApprovedBudget";
			this.lblApprovedBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblApprovedBudget.Text = "Budget";
			this.lblApprovedBudget.Top = 0.5625F;
			this.lblApprovedBudget.Width = 0.90625F;
			// 
			// lblApprovedBudget2
			// 
			this.lblApprovedBudget2.Height = 0.1875F;
			this.lblApprovedBudget2.HyperLink = null;
			this.lblApprovedBudget2.Left = 6.59375F;
			this.lblApprovedBudget2.Name = "lblApprovedBudget2";
			this.lblApprovedBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblApprovedBudget2.Text = "Approved";
			this.lblApprovedBudget2.Top = 0.75F;
			this.lblApprovedBudget2.Width = 0.90625F;
			// 
			// lblElectedBudget
			// 
			this.lblElectedBudget.Height = 0.1875F;
			this.lblElectedBudget.HyperLink = null;
			this.lblElectedBudget.Left = 6.59375F;
			this.lblElectedBudget.Name = "lblElectedBudget";
			this.lblElectedBudget.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblElectedBudget.Text = "Budget";
			this.lblElectedBudget.Top = 0.5625F;
			this.lblElectedBudget.Width = 0.90625F;
			// 
			// lblElectedBudget2
			// 
			this.lblElectedBudget2.Height = 0.1875F;
			this.lblElectedBudget2.HyperLink = null;
			this.lblElectedBudget2.Left = 6.59375F;
			this.lblElectedBudget2.Name = "lblElectedBudget2";
			this.lblElectedBudget2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblElectedBudget2.Text = "Elected";
			this.lblElectedBudget2.Top = 0.75F;
			this.lblElectedBudget2.Width = 0.90625F;
			// 
			// fldDeptDivTitle
			// 
			this.fldDeptDivTitle.DataField = "GroupTitle";
			this.fldDeptDivTitle.Height = 0.1875F;
			this.fldDeptDivTitle.Left = 0.03125F;
			this.fldDeptDivTitle.Name = "fldDeptDivTitle";
			this.fldDeptDivTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldDeptDivTitle.Text = "Field1";
			this.fldDeptDivTitle.Top = 0.125F;
			this.fldDeptDivTitle.Width = 3.90625F;
			// 
			// Field1
			// 
			this.Field1.DataField = "Binder";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 4.9375F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "Binder";
			this.Field1.Top = 0.03125F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.4375F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.21875F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0.03125F;
			this.fldAccount.Width = 2.3125F;
			// 
			// fldCurrentBudget
			// 
			this.fldCurrentBudget.Height = 0.1875F;
			this.fldCurrentBudget.Left = 2.5625F;
			this.fldCurrentBudget.Name = "fldCurrentBudget";
			this.fldCurrentBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCurrentBudget.Text = "Field1";
			this.fldCurrentBudget.Top = 0.03125F;
			this.fldCurrentBudget.Width = 0.96875F;
			// 
			// fldComments
			// 
			this.fldComments.CanShrink = true;
			this.fldComments.Height = 0.1875F;
			this.fldComments.Left = 0.34375F;
			this.fldComments.Name = "fldComments";
			this.fldComments.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldComments.Text = "Field1";
			this.fldComments.Top = 0.21875F;
			this.fldComments.Width = 2.1875F;
			// 
			// fldInitialBudget
			// 
			this.fldInitialBudget.Height = 0.1875F;
			this.fldInitialBudget.Left = 3.5625F;
			this.fldInitialBudget.Name = "fldInitialBudget";
			this.fldInitialBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldInitialBudget.Text = "Field1";
			this.fldInitialBudget.Top = 0.03125F;
			this.fldInitialBudget.Width = 0.96875F;
			// 
			// fldManagerBudget
			// 
			this.fldManagerBudget.Height = 0.1875F;
			this.fldManagerBudget.Left = 4.5625F;
			this.fldManagerBudget.Name = "fldManagerBudget";
			this.fldManagerBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldManagerBudget.Text = "Field1";
			this.fldManagerBudget.Top = 0.03125F;
			this.fldManagerBudget.Width = 0.96875F;
			// 
			// fldCommitteeBudget
			// 
			this.fldCommitteeBudget.Height = 0.1875F;
			this.fldCommitteeBudget.Left = 5.5625F;
			this.fldCommitteeBudget.Name = "fldCommitteeBudget";
			this.fldCommitteeBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCommitteeBudget.Text = "Field1";
			this.fldCommitteeBudget.Top = 0.03125F;
			this.fldCommitteeBudget.Width = 0.96875F;
			// 
			// fldApprovedAmount
			// 
			this.fldApprovedAmount.Height = 0.1875F;
			this.fldApprovedAmount.Left = 6.5625F;
			this.fldApprovedAmount.Name = "fldApprovedAmount";
			this.fldApprovedAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldApprovedAmount.Text = "Field1";
			this.fldApprovedAmount.Top = 0.03125F;
			this.fldApprovedAmount.Width = 0.96875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.5625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.21875F;
			this.Line3.Visible = false;
			this.Line3.Width = 1.96875F;
			this.Line3.X1 = 2.5625F;
			this.Line3.X2 = 4.53125F;
			this.Line3.Y1 = 0.21875F;
			this.Line3.Y2 = 0.21875F;
			// 
			// fldElectedBudget
			// 
			this.fldElectedBudget.Height = 0.1875F;
			this.fldElectedBudget.Left = 6.5625F;
			this.fldElectedBudget.Name = "fldElectedBudget";
			this.fldElectedBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldElectedBudget.Text = "Field1";
			this.fldElectedBudget.Top = 0.03125F;
			this.fldElectedBudget.Width = 0.96875F;
			// 
			// fldDivisionName
			// 
			this.fldDivisionName.CanShrink = true;
			this.fldDivisionName.DataField = "DivGroupFooter";
			this.fldDivisionName.Height = 0.1875F;
			this.fldDivisionName.Left = 1.375F;
			this.fldDivisionName.Name = "fldDivisionName";
			this.fldDivisionName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDivisionName.Text = "Field1";
			this.fldDivisionName.Top = 0.03125F;
			this.fldDivisionName.Width = 1.15625F;
			// 
			// fldDepartmentName
			// 
			this.fldDepartmentName.CanShrink = true;
			this.fldDepartmentName.DataField = "DeptGroupFooter";
			this.fldDepartmentName.Height = 0.1875F;
			this.fldDepartmentName.Left = 1.375F;
			this.fldDepartmentName.Name = "fldDepartmentName";
			this.fldDepartmentName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldDepartmentName.Text = "Field2";
			this.fldDepartmentName.Top = 0.21875F;
			this.fldDepartmentName.Width = 1.15625F;
			// 
			// fldDivisionTotalCurrentBudget
			// 
			this.fldDivisionTotalCurrentBudget.CanShrink = true;
			this.fldDivisionTotalCurrentBudget.Height = 0.1875F;
			this.fldDivisionTotalCurrentBudget.Left = 2.5625F;
			this.fldDivisionTotalCurrentBudget.Name = "fldDivisionTotalCurrentBudget";
			this.fldDivisionTotalCurrentBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalCurrentBudget.Text = "Field1";
			this.fldDivisionTotalCurrentBudget.Top = 0.03125F;
			this.fldDivisionTotalCurrentBudget.Width = 0.96875F;
			// 
			// fldDepartmentTotalCurrentBudget
			// 
			this.fldDepartmentTotalCurrentBudget.CanShrink = true;
			this.fldDepartmentTotalCurrentBudget.Height = 0.1875F;
			this.fldDepartmentTotalCurrentBudget.Left = 2.5625F;
			this.fldDepartmentTotalCurrentBudget.Name = "fldDepartmentTotalCurrentBudget";
			this.fldDepartmentTotalCurrentBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalCurrentBudget.Text = "Field2";
			this.fldDepartmentTotalCurrentBudget.Top = 0.21875F;
			this.fldDepartmentTotalCurrentBudget.Width = 0.96875F;
			// 
			// fldDivisionTotalInitialRequest
			// 
			this.fldDivisionTotalInitialRequest.CanShrink = true;
			this.fldDivisionTotalInitialRequest.Height = 0.1875F;
			this.fldDivisionTotalInitialRequest.Left = 3.5625F;
			this.fldDivisionTotalInitialRequest.Name = "fldDivisionTotalInitialRequest";
			this.fldDivisionTotalInitialRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalInitialRequest.Text = "Field1";
			this.fldDivisionTotalInitialRequest.Top = 0.03125F;
			this.fldDivisionTotalInitialRequest.Width = 0.96875F;
			// 
			// fldDepartmentTotalInitialRequest
			// 
			this.fldDepartmentTotalInitialRequest.CanShrink = true;
			this.fldDepartmentTotalInitialRequest.Height = 0.1875F;
			this.fldDepartmentTotalInitialRequest.Left = 3.5625F;
			this.fldDepartmentTotalInitialRequest.Name = "fldDepartmentTotalInitialRequest";
			this.fldDepartmentTotalInitialRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalInitialRequest.Text = "Field2";
			this.fldDepartmentTotalInitialRequest.Top = 0.21875F;
			this.fldDepartmentTotalInitialRequest.Width = 0.96875F;
			// 
			// fldDivisionTotalManagerRequest
			// 
			this.fldDivisionTotalManagerRequest.CanShrink = true;
			this.fldDivisionTotalManagerRequest.Height = 0.1875F;
			this.fldDivisionTotalManagerRequest.Left = 4.5625F;
			this.fldDivisionTotalManagerRequest.Name = "fldDivisionTotalManagerRequest";
			this.fldDivisionTotalManagerRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalManagerRequest.Text = "Field1";
			this.fldDivisionTotalManagerRequest.Top = 0.03125F;
			this.fldDivisionTotalManagerRequest.Width = 0.96875F;
			// 
			// fldDepartmentTotalManagerRequest
			// 
			this.fldDepartmentTotalManagerRequest.CanShrink = true;
			this.fldDepartmentTotalManagerRequest.Height = 0.1875F;
			this.fldDepartmentTotalManagerRequest.Left = 4.5625F;
			this.fldDepartmentTotalManagerRequest.Name = "fldDepartmentTotalManagerRequest";
			this.fldDepartmentTotalManagerRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalManagerRequest.Text = "Field2";
			this.fldDepartmentTotalManagerRequest.Top = 0.21875F;
			this.fldDepartmentTotalManagerRequest.Width = 0.96875F;
			// 
			// fldDivisionTotalCommitteeRequest
			// 
			this.fldDivisionTotalCommitteeRequest.CanShrink = true;
			this.fldDivisionTotalCommitteeRequest.Height = 0.1875F;
			this.fldDivisionTotalCommitteeRequest.Left = 5.5625F;
			this.fldDivisionTotalCommitteeRequest.Name = "fldDivisionTotalCommitteeRequest";
			this.fldDivisionTotalCommitteeRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalCommitteeRequest.Text = "Field1";
			this.fldDivisionTotalCommitteeRequest.Top = 0.03125F;
			this.fldDivisionTotalCommitteeRequest.Width = 0.96875F;
			// 
			// fldDepartmentTotalCommitteeRequest
			// 
			this.fldDepartmentTotalCommitteeRequest.CanShrink = true;
			this.fldDepartmentTotalCommitteeRequest.Height = 0.1875F;
			this.fldDepartmentTotalCommitteeRequest.Left = 5.5625F;
			this.fldDepartmentTotalCommitteeRequest.Name = "fldDepartmentTotalCommitteeRequest";
			this.fldDepartmentTotalCommitteeRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalCommitteeRequest.Text = "Field2";
			this.fldDepartmentTotalCommitteeRequest.Top = 0.21875F;
			this.fldDepartmentTotalCommitteeRequest.Width = 0.96875F;
			// 
			// fldDivisionTotalApprovedAmount
			// 
			this.fldDivisionTotalApprovedAmount.CanShrink = true;
			this.fldDivisionTotalApprovedAmount.Height = 0.1875F;
			this.fldDivisionTotalApprovedAmount.Left = 6.5625F;
			this.fldDivisionTotalApprovedAmount.Name = "fldDivisionTotalApprovedAmount";
			this.fldDivisionTotalApprovedAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalApprovedAmount.Text = "Field1";
			this.fldDivisionTotalApprovedAmount.Top = 0.03125F;
			this.fldDivisionTotalApprovedAmount.Width = 0.96875F;
			// 
			// fldDepartmentTotalApprovedAmount
			// 
			this.fldDepartmentTotalApprovedAmount.CanShrink = true;
			this.fldDepartmentTotalApprovedAmount.Height = 0.1875F;
			this.fldDepartmentTotalApprovedAmount.Left = 6.5625F;
			this.fldDepartmentTotalApprovedAmount.Name = "fldDepartmentTotalApprovedAmount";
			this.fldDepartmentTotalApprovedAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalApprovedAmount.Text = "Field2";
			this.fldDepartmentTotalApprovedAmount.Top = 0.21875F;
			this.fldDepartmentTotalApprovedAmount.Width = 0.96875F;
			// 
			// fldDivisionTotalElectedRequest
			// 
			this.fldDivisionTotalElectedRequest.CanShrink = true;
			this.fldDivisionTotalElectedRequest.Height = 0.1875F;
			this.fldDivisionTotalElectedRequest.Left = 6.5625F;
			this.fldDivisionTotalElectedRequest.Name = "fldDivisionTotalElectedRequest";
			this.fldDivisionTotalElectedRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDivisionTotalElectedRequest.Text = "Field1";
			this.fldDivisionTotalElectedRequest.Top = 0.03125F;
			this.fldDivisionTotalElectedRequest.Width = 0.96875F;
			// 
			// fldDepartmentTotalElectedRequest
			// 
			this.fldDepartmentTotalElectedRequest.CanShrink = true;
			this.fldDepartmentTotalElectedRequest.Height = 0.1875F;
			this.fldDepartmentTotalElectedRequest.Left = 6.5625F;
			this.fldDepartmentTotalElectedRequest.Name = "fldDepartmentTotalElectedRequest";
			this.fldDepartmentTotalElectedRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDepartmentTotalElectedRequest.Text = "Field2";
			this.fldDepartmentTotalElectedRequest.Top = 0.21875F;
			this.fldDepartmentTotalElectedRequest.Width = 0.96875F;
			// 
			// fldTotalName
			// 
			this.fldTotalName.CanShrink = true;
			this.fldTotalName.Height = 0.1875F;
			this.fldTotalName.Left = 1.34375F;
			this.fldTotalName.Name = "fldTotalName";
			this.fldTotalName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldTotalName.Text = "Field2";
			this.fldTotalName.Top = 0.0625F;
			this.fldTotalName.Width = 1.15625F;
			// 
			// fldTotalCurrentBudget
			// 
			this.fldTotalCurrentBudget.CanShrink = true;
			this.fldTotalCurrentBudget.Height = 0.1875F;
			this.fldTotalCurrentBudget.Left = 2.53125F;
			this.fldTotalCurrentBudget.Name = "fldTotalCurrentBudget";
			this.fldTotalCurrentBudget.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalCurrentBudget.Text = "Field2";
			this.fldTotalCurrentBudget.Top = 0.0625F;
			this.fldTotalCurrentBudget.Width = 0.96875F;
			// 
			// fldTotalInitialRequest
			// 
			this.fldTotalInitialRequest.CanShrink = true;
			this.fldTotalInitialRequest.Height = 0.1875F;
			this.fldTotalInitialRequest.Left = 3.53125F;
			this.fldTotalInitialRequest.Name = "fldTotalInitialRequest";
			this.fldTotalInitialRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalInitialRequest.Text = "Field2";
			this.fldTotalInitialRequest.Top = 0.0625F;
			this.fldTotalInitialRequest.Width = 0.96875F;
			// 
			// fldTotalManagerRequest
			// 
			this.fldTotalManagerRequest.CanShrink = true;
			this.fldTotalManagerRequest.Height = 0.1875F;
			this.fldTotalManagerRequest.Left = 4.53125F;
			this.fldTotalManagerRequest.Name = "fldTotalManagerRequest";
			this.fldTotalManagerRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalManagerRequest.Text = "Field2";
			this.fldTotalManagerRequest.Top = 0.0625F;
			this.fldTotalManagerRequest.Width = 0.96875F;
			// 
			// fldTotalCommitteeRequest
			// 
			this.fldTotalCommitteeRequest.CanShrink = true;
			this.fldTotalCommitteeRequest.Height = 0.1875F;
			this.fldTotalCommitteeRequest.Left = 5.53125F;
			this.fldTotalCommitteeRequest.Name = "fldTotalCommitteeRequest";
			this.fldTotalCommitteeRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalCommitteeRequest.Text = "Field2";
			this.fldTotalCommitteeRequest.Top = 0.0625F;
			this.fldTotalCommitteeRequest.Width = 0.96875F;
			// 
			// fldTotalApprovedAmount
			// 
			this.fldTotalApprovedAmount.CanShrink = true;
			this.fldTotalApprovedAmount.Height = 0.1875F;
			this.fldTotalApprovedAmount.Left = 6.53125F;
			this.fldTotalApprovedAmount.Name = "fldTotalApprovedAmount";
			this.fldTotalApprovedAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalApprovedAmount.Text = "Field2";
			this.fldTotalApprovedAmount.Top = 0.0625F;
			this.fldTotalApprovedAmount.Width = 0.96875F;
			// 
			// fldTotalElectedRequest
			// 
			this.fldTotalElectedRequest.CanShrink = true;
			this.fldTotalElectedRequest.Height = 0.1875F;
			this.fldTotalElectedRequest.Left = 6.53125F;
			this.fldTotalElectedRequest.Name = "fldTotalElectedRequest";
			this.fldTotalElectedRequest.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalElectedRequest.Text = "Field2";
			this.fldTotalElectedRequest.Top = 0.0625F;
			this.fldTotalElectedRequest.Width = 0.96875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.34375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.03125F;
			this.Line1.Width = 6.125F;
			this.Line1.X1 = 1.34375F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.03125F;
			this.Line1.Y2 = 0.03125F;
			// 
			// rptRequestRevenueWorksheet
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.53125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInitialBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInitialBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblManagerBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblManagerBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCommitteeBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCommitteeBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApprovedBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblApprovedBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblElectedBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblElectedBudget2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDeptDivTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInitialBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldManagerBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitteeBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldApprovedAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldElectedBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalCurrentBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalCurrentBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalInitialRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalInitialRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalManagerRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalManagerRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalCommitteeRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalCommitteeRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalApprovedAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalApprovedAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDivisionTotalElectedRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepartmentTotalElectedRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInitialRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalManagerRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCommitteeRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalApprovedAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalElectedRequest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldComments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInitialBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldManagerBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCommitteeBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldApprovedAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldElectedBudget;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInitialBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInitialBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblManagerBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblManagerBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCommitteeBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCommitteeBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblApprovedBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblApprovedBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblElectedBudget;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblElectedBudget2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrentBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInitialRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalManagerRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCommitteeRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalApprovedAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalElectedRequest;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDeptDivTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalCurrentBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalCurrentBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalInitialRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalInitialRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalManagerRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalManagerRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalCommitteeRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalCommitteeRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalApprovedAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalApprovedAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDivisionTotalElectedRequest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepartmentTotalElectedRequest;
	}
}
