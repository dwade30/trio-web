﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
using Wisej.Core;

namespace TWBD0000
{
	public class MDIParent
	//: BaseForm
	{
		public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// Last Updated:  6/27/01
		const int WM_USER = 0x400;
		const int SB_GETRECT = (WM_USER + 10);
		private string strTrio = string.Empty;
		const int CNSTMenuGridColCommand = 3;
		const int CNSTMenuGridColSecurity = 2;
		const int CNSTMenuGridColCode = 4;
		const int CNSTMenuGridColTooltip = 5;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingsController setCont = new cSettingsController();
		private cSettingsController setCont_AutoInitialized;

		private cSettingsController setCont
		{
			get
			{
				if (setCont_AutoInitialized == null)
				{
					setCont_AutoInitialized = new cSettingsController();
				}
				return setCont_AutoInitialized;
			}
			set
			{
				setCont_AutoInitialized = value;
			}
		}

		private cMDIView mainView;
		bool blnSetup;
		int lngFCode;
		int[] LabelNumber = new int[200 + 1];
		int CurRow;

		private struct RECT
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;
		};

		public void Init()
		{
			App.MainForm.NavigationMenu.Owner = this;
            App.MainForm.NavigationMenu.OriginName = "Budgetary";
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			App.MainForm.StatusBarText2 = "GL Year: " + (String.IsNullOrEmpty(modGlobalConstants.Statics.gstrArchiveYear) ? "Current" : modGlobalConstants.Statics.gstrArchiveYear);
			App.MainForm.Caption = "BUDGETARY";
			App.MainForm.ApplicationIcon = "icon-budgetary-system";
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-budgetary-system");
			}
			App.MainForm.menuTree.ImageList = CreateImageList();
			mainView = new cMDIView();
			mainView.PanelChanged += new cMDIView.PanelChangedEventHandler(this.mainView_PanelChanged);           
			SetupCustomMenuOptions(ref mainView);
			mainView.Reset();
			this.RefreshMenu();
		}

        private void SetupCustomMenuOptions(ref cMDIView mainView)
        {
            var muniName = modGlobalConstants.Statics.MuniName.ToUpper();

            switch (muniName)
            {
                case "ORRINGTON":
                case "TRIO":
                case "CLINTON":
                case "CARIBOU":
                case "DOVERFOXCROFT":
                case "DOVER FOXCROFT":
                case "DOVER-FOXCROFT":
                case "HANCOCK COUNTY":
                case "HANCOCK COUNTY JAIL":
                case "CALAIS":
                case "NORWAY":
                case "LINCOLN COUNTY":
                case "DAYTON":
                case "BOOTHBAY":
                case "ORONO":
                case "ORONOPLAY":
                case "SOMERSET COUNTY":
                case "MONMOUTH":
                    mainView.customMenu = true;

                    break;
                default:
                    mainView.customMenu = false;

                    break;
            }
        }

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-data-entry", "data-entry"),
				new ImageListEntry("menutree-posting", "posting"),
				new ImageListEntry("menutree-end-of-year", "end-of-year"),
				new ImageListEntry("menutree-1099-processing", "1099-processing"),
				new ImageListEntry("menutree-check-reconciliation", "check-reconciliation"),
				new ImageListEntry("menutree-view-ap-journals", "view-ap-journals"),
				new ImageListEntry("menutree-ap-research", "view-ap-research"),
				new ImageListEntry("menutree-setup-projects", "setup-projects"),
				new ImageListEntry("menutree-account-inquiry", "account-inquiry"),
				new ImageListEntry("menutree-account-payable-processing", "account-payable-processing"),
				new ImageListEntry("menutree-account-setup", "account-setup"),
				new ImageListEntry("menutree-budget-process", "budget-process"),
				new ImageListEntry("menutree-collections", "collections"),
				new ImageListEntry("menutree-delete-unposted-journal", "delete-unposted-journal"),
				new ImageListEntry("menutree-receipt-input", "receipt-input"),
				new ImageListEntry("menutree-vendor-routines", "vendor-routines"),
				new ImageListEntry("menutree-account-inquiry-active", "account-inquiry-active"),
				new ImageListEntry("menutree-account-payable-processing-active", "account-payable-processing-active"),
				new ImageListEntry("menutree-account-setup-active", "account-setup-active"),
				new ImageListEntry("menutree-budget-process-active", "budget-process-active"),
				new ImageListEntry("menutree-collections-active", "collections-active"),
				new ImageListEntry("menutree-delete-unposted-journal-active", "delete-unposted-journal-active"),
				new ImageListEntry("menutree-receipt-input-active", "receipt-input-active"),
				new ImageListEntry("menutree-vendor-routines-active", "vendor-routines-active"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-street-maintenance", "street-maintenance")
			});
			return imageList;
		}



		private void mainView_PanelChanged()
		{
			App.MainForm.StatusBarText3 = mainView.PanelMessage;
		}

		private void mnuBoothBayCheckRecExport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_Form_BoothbayCheckRecExport");
		}

		private void mnuCustomCheckRecAP_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_OrringtonCustomCheckRecAP");
		}

		private void mnuCustomCheckRecExport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_Form_ClintonCheckRecExport");
		}

		private void mnuCustomCheckRecPY_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_OrringtonCustomCheckRecPY");
		}

		private void mnuCustomPayrollImport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_Form_HancockCountyPayrollImport");
		}

		private void mnuCustomProgramsDaytonImport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_DaytonCustomImport");
		}

		private void mnuCustomProgramsLincolnCountyPayrollImport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_LincolnCountyPayrollImport");
		}

		private void mnuCustomProgramsCalaisBDSetDBLocation_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_CalaisBDSetDBLocation");
		}

		private void mnuCustomProgramsOronoPayrollImport_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_OronoPayrollImport");
		}

		private void mnuCustomRevenueReport_Click(object sender, System.EventArgs e)
		{
			// frmSetupTownRevenueReport.Show , MDIParent
			mainView.ExecuteCommand("BD_Form_SetupTownRevenueReport");
		}

		private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", string strCommand = "", bool boolEnabled = true)
		{
			// vbPorter upgrade warning: strOption As string	OnWrite(string, short)
			string strOption;
			string imageKey = "";
			string itemName = "";
			          
			bool hasSubItems = false;
			switch (intRow)
			{
				case 1:
				{
					imageKey = "data-entry";
					break;
				}
				case 2:
				{
					imageKey = "account-inquiry";
					break;
				}
				case 3:
				{
					mainView.SetMenu("payables");
					imageKey = "account-payable-processing";
					hasSubItems = true;
					break;
				}
				case 4:
				{
					mainView.SetMenu("checkrec");
					imageKey = "check-reconciliation";
					hasSubItems = true;
					break;
				}
				case 5:
				{
					mainView.SetMenu("accounts");
					imageKey = "account-setup";
					hasSubItems = true;
					break;
				}
				case 6:
				{
					mainView.SetMenu("budget");
					imageKey = "budget-process";
					hasSubItems = true;
					break;
				}
				case 7:
				{
					mainView.SetMenu("eoy");
					imageKey = "end-of-year";
					hasSubItems = true;
					break;
				}
				case 8:
				{
					mainView.SetMenu("projects");
					imageKey = "setup-projects";
					hasSubItems = true;
					break;
				}
				case 9:
				{
					mainView.SetMenu("reports");
					imageKey = "printing";
					hasSubItems = true;
					break;
				}
				case 10:
				{
					mainView.SetMenu("file");
					imageKey = "file-maintenance";
					hasSubItems = true;
					break;
				}
				case 11:
				{
					mainView.SetMenu("custom");
					imageKey = "street-maintenance";
					hasSubItems = true;
					break;
				}
					//case 5:
					//	{
					//		mainView.SetMenu("vendor");
					//		imageKey = "vendor-routines";
					//		hasSubItems = true;
					//		break;
					//	}




					//case 10:
					//	{
					//		mainView.SetMenu("1099");
					//		imageKey = "1099-processing";
					//		hasSubItems = true;
					//		break;
					//	}
					//case 11:
					//	{
					//		imageKey = "delete-unposted-journal";
					//		break;
					//	}

					//case 13:
					//	{
					//		imageKey = "view-ap-journals";
					//		break;
					//	}
					//case 14:
					//	{
					//		imageKey = "view-ap-research";
					//		break;
					//	}



			}
			FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strCaption, "Execute:" + intCode, "main", boolEnabled, 1, imageKey);
			if (hasSubItems)
			{
				cGenericCollection menuColl;
				menuColl = mainView.CurrentMenu;
				if (!(menuColl == null))
				{
					cMenuChoice tItem;
					menuColl.MoveFirst();
					int row = 1;
					itemName = mainView.CurrentMenuName;
					while (menuColl.IsCurrent())
					{
						tItem = (cMenuChoice)menuColl.GetCurrentItem();
						FCMenuItem newItem2 = newItem.SubItems.Add(tItem.Description, "Execute:" + tItem.CommandCode, itemName, tItem.Enabled, 2);
						bool hasSubItems2 = false;
						cGenericCollection menuColl2 = null;
						switch (row)
						{
							case 1:
								{
									if (itemName == "Reports")
									{
										menuColl2 = mainView.GetSummaryMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "Payables")
									{
										menuColl2 = mainView.GetAPMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "Accounts")
									{
										menuColl2 = mainView.GetAccountMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 2:
								{
									if (itemName == "Reports")
									{
										menuColl2 = mainView.GetDetailMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "Budget")
									{
										menuColl2 = mainView.GetInitialRequestMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "Payables")
									{
										menuColl2 = mainView.GetVendorMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "End of Year")
									{
										menuColl2 = mainView.GetEOYReportsMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 3:
								{
									if (itemName == "Budget")
									{
										menuColl2 = mainView.GetManagerRequestMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 4:
								{
									if (itemName == "Budget")
									{
										menuColl2 = mainView.GetCommitteeRequestMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "End of Year")
									{
										menuColl2 = mainView.Get1099Menu();
										hasSubItems2 = true;
									}
									break;
								}
							case 5:
								{
									if (itemName == "CheckRec")
									{
										menuColl2 = mainView.GetCheckFileMenu();
										hasSubItems2 = true;
									}
									else if (itemName == "Budget")
									{
										menuColl2 = mainView.GetElectedRequestMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 6:
								{
									if (itemName == "Budget")
									{
										menuColl2 = mainView.GetApprovedAmountMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 9:
								{
									if (itemName == "Reports")
									{
										menuColl2 = mainView.GetEncumbranceReportsMenu();
										hasSubItems2 = true;
									}
									break;
								}
							case 12:
							{
								if (itemName == "Reports")
								{
									menuColl2 = mainView.GetFormatsMenu();
									hasSubItems2 = true;
								}

								break;
							}
							case 13:
							{
								if (itemName == "File")
								{
									menuColl2 = mainView.GetPurgeChartsMenu();
									hasSubItems2 = true;
								}
								
								break;
							}
						}
						if (hasSubItems2)
						{
							if (!(menuColl2 == null))
							{
								cMenuChoice tItem2;
								menuColl2.MoveFirst();
								while (menuColl2.IsCurrent())
								{
									tItem2 = (cMenuChoice)menuColl2.GetCurrentItem();
									newItem2.SubItems.Add(tItem2.Description, "Execute:" + tItem2.CommandCode, mainView.CurrentMenuName, tItem2.Enabled, 3);
									menuColl2.MoveNext();
								}
							}
						}
						row++;
						menuColl.MoveNext();
					}
				}
			}
			intRow += 1;
		}

		public void Execute(int code)
		{
			mainView.ExecuteMenuCommand(App.MainForm.NavigationMenu.CurrentItem.Menu, code);
		}

		private void mnuSomersetAPCustomCheckRec_Click(object sender, System.EventArgs e)
		{
			mainView.ExecuteCommand("BD_SomersetCountyCustomCheckRecAP");
		}

		private void RefreshMenu()
		{
			int CurRow = 0;
			cGenericCollection menuColl;
			menuColl = mainView.CurrentMenu;
			if (!(menuColl == null))
			{
				//ClearMenuItems();
				cMenuChoice tItem;
				menuColl.MoveFirst();
				CurRow = 1;
				while (menuColl.IsCurrent())
				{
					tItem = (cMenuChoice)menuColl.GetCurrentItem();
					AddMenuItem(ref CurRow, FCConvert.ToInt16(tItem.CommandCode), tItem.Description, tItem.SecurityCode, tItem.MenuCaption, tItem.ToolTip, tItem.CommandName, tItem.Enabled);
					menuColl.MoveNext();
				}
			}
		}

		public class StaticVariables
		{
			/// </summary>
			/// Summary description for MDIParent.
			/// <summary>
			public int R = 0, c;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}

        private void mnuSimpleCheckRecExport_Click(object sender, EventArgs e)
        {
            mainView.ExecuteCommand("BD_Form_CaribouCheckRecExport");
        }
    }
}
