﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptShowCustomReportSetup.
	/// </summary>
	public partial class rptShowCustomReportSetup : BaseSectionReport
	{
		public static rptShowCustomReportSetup InstancePtr
		{
			get
			{
				return (rptShowCustomReportSetup)Sys.GetInstance(typeof(rptShowCustomReportSetup));
			}
		}

		protected rptShowCustomReportSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptShowCustomReportSetup	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int RowCounter;

		public rptShowCustomReportSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Report Setup";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckNext = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 2) == "A" && frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 4) == "")
				{
					executeCheckNext = true;
					goto CheckNext;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
			else
			{
				executeCheckNext = true;
				goto CheckNext;
			}
			CheckNext:
			;
			if (executeCheckNext)
			{
				RowCounter += 1;
				if (RowCounter <= frmCreateCustomReport.InstancePtr.vsCustomFormat.Rows - 1)
				{
					if (frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 2) == "A" && frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 4) == "")
					{
						goto CheckNext;
					}
					else
					{
						eArgs.EOF = false;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			RowCounter = 1;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			fldTitle.Text = frmCreateCustomReport.InstancePtr.txtDescription;
			if (frmCreateCustomReport.InstancePtr.cmbRange.SelectedIndex == 2)
			{
				chkAll.Checked = true;
				fldMonthSelection.Text = "";
			}
			else if (frmCreateCustomReport.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				chkSingle.Checked = true;
				if (frmCreateCustomReport.InstancePtr.chkCheckDateRange.CheckState == CheckState.Checked)
				{
					fldMonthSelection.Text = "Select at Report Time";
				}
				else
				{
					fldMonthSelection.Text = frmCreateCustomReport.InstancePtr.cboSingleMonth.Text;
				}
			}
			else
			{
				chkRange.Checked = true;
				if (frmCreateCustomReport.InstancePtr.chkCheckDateRange.CheckState == CheckState.Checked)
				{
					fldMonthSelection.Text = "Select at Report Time";
				}
				else
				{
					fldMonthSelection.Text = frmCreateCustomReport.InstancePtr.cboBeginningMonth.Text + " To " + frmCreateCustomReport.InstancePtr.cboEndingMonth.Text;
				}
			}
			if (frmCreateCustomReport.InstancePtr.chkAccountDescription.CheckState == CheckState.Checked)
			{
				chkAccountDescription.Checked = true;
			}
			else
			{
				chkAccountDescription.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkAccountNumber.CheckState == CheckState.Checked)
			{
				chkAccountNumber.Checked = true;
			}
			else
			{
				chkAccountNumber.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkBalance.CheckState == CheckState.Checked)
			{
				chkBalance.Checked = true;
			}
			else
			{
				chkBalance.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkBudget.CheckState == CheckState.Checked)
			{
				chkBudget.Checked = true;
			}
			else
			{
				chkBudget.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkCurrent.CheckState == CheckState.Checked)
			{
				chkCurrentActivity.Checked = true;
			}
			else
			{
				chkCurrentActivity.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkEncumbrances.CheckState == CheckState.Checked)
			{
				chkEncumbrances.Checked = true;
			}
			else
			{
				chkEncumbrances.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkPending.CheckState == CheckState.Checked)
			{
				chkPending.Checked = true;
			}
			else
			{
				chkPending.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkPercent.CheckState == CheckState.Checked)
			{
				chkPercent.Checked = true;
			}
			else
			{
				chkPercent.Checked = false;
			}
			if (frmCreateCustomReport.InstancePtr.chkYTD.CheckState == CheckState.Checked)
			{
				chkYTDActivity.Checked = true;
			}
			else
			{
				chkYTDActivity.Checked = false;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldRow.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 1);
			fldType.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 2);
			fldAcctType.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 3);
			fldAccount.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 4);
			fldDS.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 5);
			fldTitle.Text = frmCreateCustomReport.InstancePtr.vsCustomFormat.TextMatrix(RowCounter, 6);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		
	}
}
