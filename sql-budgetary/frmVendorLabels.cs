﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorLabels.
	/// </summary>
	public partial class frmVendorLabels : BaseForm
	{
		public frmVendorLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkClassCode = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkClassCode.AddControlArrayElement(chkClassCode_19, 19);
			this.chkClassCode.AddControlArrayElement(chkClassCode_18, 18);
			this.chkClassCode.AddControlArrayElement(chkClassCode_17, 17);
			this.chkClassCode.AddControlArrayElement(chkClassCode_16, 16);
			this.chkClassCode.AddControlArrayElement(chkClassCode_15, 15);
			this.chkClassCode.AddControlArrayElement(chkClassCode_14, 14);
			this.chkClassCode.AddControlArrayElement(chkClassCode_13, 13);
			this.chkClassCode.AddControlArrayElement(chkClassCode_12, 12);
			this.chkClassCode.AddControlArrayElement(chkClassCode_11, 11);
			this.chkClassCode.AddControlArrayElement(chkClassCode_10, 10);
			this.chkClassCode.AddControlArrayElement(chkClassCode_9, 9);
			this.chkClassCode.AddControlArrayElement(chkClassCode_8, 8);
			this.chkClassCode.AddControlArrayElement(chkClassCode_7, 7);
			this.chkClassCode.AddControlArrayElement(chkClassCode_6, 6);
			this.chkClassCode.AddControlArrayElement(chkClassCode_5, 5);
			this.chkClassCode.AddControlArrayElement(chkClassCode_4, 4);
			this.chkClassCode.AddControlArrayElement(chkClassCode_3, 3);
			this.chkClassCode.AddControlArrayElement(chkClassCode_2, 2);
			this.chkClassCode.AddControlArrayElement(chkClassCode_1, 1);
			this.chkClassCode.AddControlArrayElement(chkClassCode_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - 
			cmbName.SelectedIndex = 0;
			cmbSpecific.SelectedIndex = 0;
			cmbStatusSelect.SelectedIndex = 0;
			cmbVendorJournal.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendorLabels InstancePtr
		{
			get
			{
				return (frmVendorLabels)Sys.GetInstance(typeof(frmVendorLabels));
			}
		}

		protected frmVendorLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/29/03
		// This form will be used to pick the vendors you wish to
		// print labels for and in which order they will be printed
		// ********************************************************
		string strClassCodeSQL;
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		public bool blnFromVendorMaster;
		public int lngVendorID;

		public void Init(bool blnFromVendor = false, int lngVend = -1)
		{
			blnFromVendorMaster = blnFromVendor;
			lngVendorID = lngVend;
			this.Show(App.MainForm);
		}

		private void cboLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intindex;
			intindex = labLabelTypes.Get_IndexFromID(cboLabelType.ItemData(cboLabelType.SelectedIndex));
			lblLabelDescription.Text = labLabelTypes.Get_Description(intindex);
		}

		private void frmVendorLabels_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVendorLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendorLabels.FillStyle	= 0;
			//frmVendorLabels.ScaleWidth	= 9045;
			//frmVendorLabels.ScaleHeight	= 6930;
			//frmVendorLabels.LinkTopic	= "Form2";
			//frmVendorLabels.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsClassCodeInfo = new clsDRWrapper();
			int counter;
			rsClassCodeInfo.OpenRecordset("SELECT * FROM ClassCodes");
			for (counter = 0; counter <= 19; counter++)
			{
				chkClassCode[FCConvert.ToInt16(counter)].Text = rsClassCodeInfo.Get_Fields_String("ClassKey") + "  " + rsClassCodeInfo.Get_Fields_String("ClassCode");
				rsClassCodeInfo.MoveNext();
			}
			FillJournal();
			FillWarrant();
			FillLabelTypeCombo();
			cboLabelType.SelectedIndex = 0;
			if (blnFromVendorMaster)
			{
				cmbVendorJournal.SelectedIndex = 1;
				SetRangeForVendor();
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void SetRangeForVendor()
		{
			int counter;
			for (counter = 0; counter <= cboStart.Items.Count - 1; counter++)
			{
				if (Conversion.Val(Strings.Left(cboStart.Items[counter].ToString(), 5)) == lngVendorID)
				{
					cboStart.SelectedIndex = counter;
					cboEnd.SelectedIndex = counter;
					break;
				}
			}
		}

		private void frmVendorLabels_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool blnClassCodeSelected = false;
			string strSQL = "";
			clsDRWrapper rsInfo = new clsDRWrapper();
			string strPrinter = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			int intCPI = 0;
			int X;
			string strFont = "";
			string strStatusSQL = "";
			string strOldPrinter = "";
			if (cmbVendorJournal.SelectedIndex == 1)
			{
				if (cboStart.SelectedIndex > cboEnd.SelectedIndex)
				{
					MessageBox.Show("This is not a valid range of vendors.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbVendorJournal.SelectedIndex == 3)
			{
				if (cboWarrant.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a warrant before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else if (cmbVendorJournal.SelectedIndex == 3)
			{
				if (cboJournal.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a journal before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (cmbStatusSelect.SelectedIndex == 1)
			{
				if (chkActive.CheckState != CheckState.Checked && chkSuspended.CheckState != CheckState.Checked && chkDeleted.CheckState != CheckState.Checked)
				{
					MessageBox.Show("You must select at least 1 vendor status before you may continue.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (cmbSpecific.SelectedIndex == 1)
			{
				blnClassCodeSelected = false;
				for (counter = 0; counter <= 19; counter++)
				{
					if (chkClassCode[FCConvert.ToInt16(counter)].CheckState == CheckState.Checked)
					{
						blnClassCodeSelected = true;
						break;
					}
				}
				if (!blnClassCodeSelected)
				{
					MessageBox.Show("You must select at least one class code or choose the all option for class codes before you may continue", "Invalid Class Code Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			strClassCodeSQL = "";
			if (cmbSpecific.SelectedIndex == 1)
			{
				CreateSQL();
			}
			if (cmbStatusSelect.SelectedIndex == 0)
			{
				strStatusSQL = "";
			}
			else
			{
				strStatusSQL = "(";
				if (chkActive.CheckState == CheckState.Checked)
				{
					strStatusSQL += "Status = 'A' OR ";
				}
				if (chkSuspended.CheckState == CheckState.Checked)
				{
					strStatusSQL += "Status = 'S' OR ";
				}
				if (chkDeleted.CheckState == CheckState.Checked)
				{
					strStatusSQL += "Status = 'D' OR ";
				}
				strStatusSQL = Strings.Left(strStatusSQL, strStatusSQL.Length - 4) + ") ";
			}
			if (strStatusSQL != "")
			{
				if (strClassCodeSQL != "")
				{
					if (cmbVendorJournal.SelectedIndex == 0)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND " + strStatusSQL;
					}
					else if (cmbVendorJournal.SelectedIndex == 2)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND " + strStatusSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(Conversion.Val(cboWarrant.Text)) + "')";
					}
					else if (cmbVendorJournal.SelectedIndex == 1)
					{
						if (cmbName.SelectedIndex == 1)
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND " + strStatusSQL + " AND VendorNumber >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboStart.Text, 5))) + " AND VendorNumber <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboEnd.Text, 5)));
						}
						else
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND " + strStatusSQL + " AND CheckName >= '" + frmVendorLabels.InstancePtr.cboStart.Text + "' AND CheckName <= '" + frmVendorLabels.InstancePtr.cboEnd.Text + "'";
						}
					}
					else
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND " + strStatusSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(cboJournal.Text)) + ")";
					}
				}
				else
				{
					if (cmbVendorJournal.SelectedIndex == 0)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strStatusSQL;
					}
					else if (cmbVendorJournal.SelectedIndex == 2)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(Conversion.Val(cboWarrant.Text)) + "')";
					}
					else if (cmbVendorJournal.SelectedIndex == 1)
					{
						if (cmbName.SelectedIndex == 1)
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + " AND VendorNumber >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboStart.Text, 5))) + " AND VendorNumber <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboEnd.Text, 5)));
						}
						else
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + " AND CheckName >= '" + Strings.Trim(Strings.Right(frmVendorLabels.InstancePtr.cboStart.Text, frmVendorLabels.InstancePtr.cboStart.Text.Length - 4)) + "' AND CheckName <= '" + Strings.Trim(Strings.Right(frmVendorLabels.InstancePtr.cboEnd.Text, frmVendorLabels.InstancePtr.cboEnd.Text.Length - 4)) + "'";
						}
					}
					else
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(cboJournal.Text)) + ")";
					}
				}
			}
			else
			{
				if (strClassCodeSQL != "")
				{
					if (cmbVendorJournal.SelectedIndex == 0)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL;
					}
					else if (cmbVendorJournal.SelectedIndex == 2)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(Conversion.Val(cboWarrant.Text)) + "')";
					}
					else if (cmbVendorJournal.SelectedIndex == 1)
					{
						if (cmbName.SelectedIndex == 1)
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND VendorNumber >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboStart.Text, 5))) + " AND VendorNumber <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboEnd.Text, 5)));
						}
						else
						{
							strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND CheckName >= '" + frmVendorLabels.InstancePtr.cboStart.Text + "' AND CheckName <= '" + frmVendorLabels.InstancePtr.cboEnd.Text + "'";
						}
					}
					else
					{
						strSQL = "SELECT * FROM VendorMaster WHERE " + strClassCodeSQL + " AND VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(cboJournal.Text)) + ")";
					}
				}
				else
				{
					if (cmbVendorJournal.SelectedIndex == 0)
					{
						strSQL = "SELECT * FROM VendorMaster";
					}
					else if (cmbVendorJournal.SelectedIndex == 2)
					{
						strSQL = "SELECT * FROM VendorMaster WHERE VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE Warrant = '" + FCConvert.ToString(Conversion.Val(cboWarrant.Text)) + "')";
					}
					else if (cmbVendorJournal.SelectedIndex == 1)
					{
						if (cmbName.SelectedIndex == 1)
						{
							strSQL = "SELECT * FROM VendorMaster WHERE VendorNumber >= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboStart.Text, 5))) + " AND VendorNumber <= " + FCConvert.ToString(Conversion.Val(Strings.Left(frmVendorLabels.InstancePtr.cboEnd.Text, 5)));
						}
						else
						{
							strSQL = "SELECT * FROM VendorMaster WHERE CheckName >= '" + Strings.Trim(Strings.Right(frmVendorLabels.InstancePtr.cboStart.Text, frmVendorLabels.InstancePtr.cboStart.Text.Length - 4)) + "' AND CheckName <= '" + Strings.Trim(Strings.Right(frmVendorLabels.InstancePtr.cboEnd.Text, frmVendorLabels.InstancePtr.cboEnd.Text.Length - 4)) + "'";
						}
					}
					else
					{
						strSQL = "SELECT * FROM VendorMaster WHERE VendorNumber IN (SELECT DISTINCT VendorNumber FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(cboJournal.Text)) + ")";
					}
				}
			}
			if (cmbName.SelectedIndex == 0)
			{
				strSQL += " ORDER BY CheckName";
			}
			else
			{
				strSQL += " ORDER BY VendorNumber";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// Err.Clear
				// dlg1.FLAGS = 0
				// strDefaultPrinter = Document.Printer.PrinterName
				// dlg1.FLAGS = cdlPDDisablePrintToFile
				// dlg1.CancelError = True
				// On Error Resume Next
				// dlg1.ShowPrinter
				// 
				// If Err.Number = 0 Then
				// strPrinter = Document.Printer.PrinterName
				// Else
				// Exit Sub
				// End If
				//FC:FINAL:DDU:#2913 - show preview and don't print using printer
				//modCustomPageSize.Statics.strDefaultPrinter = FCGlobal.Printer.DeviceName;
				//if (modPrinterDialogBox.ShowPrinterDialog(this.Handle.ToInt32()))
				//{
				//	strPrinter = FCGlobal.Printer.DeviceName;
				//}
				//else
				//{
				//	return;
				//}
				//NumFonts = FCGlobal.Printer.FontCount;
				//boolUseFont = false;
				//intCPI = 10;
				//for (X = 0; X <= NumFonts - 1; X++)
				//{
				//	strFont = FCGlobal.Printer.Fonts[X];
				//	if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
				//	{
				//		strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
				//		if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
				//		{
				//			boolUseFont = true;
				//			strFont = FCGlobal.Printer.Fonts[X];
				//			break;
				//		}
				//	}
				//}
				// X
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				this.Hide();
				rptCustomLabels.InstancePtr.Init(strSQL, "VENDORS", FCConvert.ToInt16(cboLabelType.ItemData(cboLabelType.SelectedIndex)), ref strPrinter, strFont);
				Close();
			}
			else
			{
				MessageBox.Show("No vendors were found that matched the criteria you selected.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 19; counter++)
			{
				chkClassCode[FCConvert.ToInt16(counter)].CheckState = CheckState.Unchecked;
			}
			fraSpecificClassCodes.Enabled = false;
		}

		private void optComplete_CheckedChanged(object sender, System.EventArgs e)
		{
			cboStart.Clear();
			cboEnd.Clear();
			fraRange.Visible = false;
			fraWarrant.Visible = false;
			fraJournal.Visible = false;
			if (cboWarrant.Items.Count > 0)
			{
				cboWarrant.SelectedIndex = 0;
			}
			if (cboJournal.Items.Count > 0)
			{
				cboJournal.SelectedIndex = 0;
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			fraWarrant.Visible = false;
			fraJournal.Visible = false;
			if (cboWarrant.Items.Count > 0)
			{
				cboWarrant.SelectedIndex = 0;
			}
			if (cboJournal.Items.Count > 0)
			{
				cboJournal.SelectedIndex = 0;
			}
			cboStart.Clear();
			cboEnd.Clear();
            rsVendorInfo.OpenRecordset(cmbName.SelectedIndex == 1 
                                           ? "SELECT * FROM VendorMaster WHERE Status <> 'D' ORDER BY VendorNumber" 
                                           : "SELECT * FROM VendorMaster WHERE Status <> 'D' ORDER BY CheckName");

            if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				do
				{
                    var vendorNumber = modValidateAccount.GetFormat_6(FCConvert.ToString(rsVendorInfo.Get_Fields_Int32("VendorNumber")), 5);
                    var checkName = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
                    var itemKey = vendorNumber + "  " + checkName;

                    cboStart.AddItem(itemKey);
					cboEnd.AddItem(itemKey);
					rsVendorInfo.MoveNext();
				}
				while (rsVendorInfo.EndOfFile() != true);

				cboStart.SelectedIndex = 0;
				cboEnd.SelectedIndex = 0;
			}
			fraRange.Visible = true;
			if (cboStart.Visible == true)
			{
				cboStart.Focus();
			}
		}

		private void optSpecific_CheckedChanged(object sender, System.EventArgs e)
        {
            switch (cmbSpecific.SelectedIndex)
            {
                case 1:
                    fraSpecificClassCodes.Enabled = true;

                    break;
                case 0:
                    optAll_CheckedChanged(sender, e);

                    break;
            }
        }

		private void FillLabelTypeCombo()
		{
			int counter;
			
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE4065 || labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30252)
					{
						cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}

		private void CreateSQL()
		{
			int counter;
			strClassCodeSQL = "(";
			for (counter = 0; counter <= 19; counter++)
			{
				if (frmVendorLabels.InstancePtr.chkClassCode[FCConvert.ToInt16(counter)].CheckState == CheckState.Checked)
				{
					strClassCodeSQL += "Class = '" + Strings.Left(frmVendorLabels.InstancePtr.chkClassCode[FCConvert.ToInt16(counter)].Text, 1) + "' OR ";
				}
			}
			strClassCodeSQL = Strings.Left(strClassCodeSQL, strClassCodeSQL.Length - 4) + ")";
		}

		private void optStatusAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkActive.CheckState = CheckState.Unchecked;
			chkSuspended.CheckState = CheckState.Unchecked;
			chkDeleted.CheckState = CheckState.Unchecked;
			chkActive.Enabled = false;
			chkSuspended.Enabled = false;
			chkDeleted.Enabled = false;
			fraStatus.Enabled = false;
		}

		private void optStatusSelect_CheckedChanged(object sender, System.EventArgs e)
        {
            switch (cmbStatusSelect.SelectedIndex)
            {
                case 1:
                    fraStatus.Enabled = true;
                    chkActive.Enabled = true;
                    chkSuspended.Enabled = true;
                    chkDeleted.Enabled = true;

                    break;
                case 0:
                    optStatusAll_CheckedChanged(sender, e);

                    break;
            }
        }

		private void optVendorJournal_CheckedChanged(object sender, System.EventArgs e)
        {
            switch (cmbVendorJournal.SelectedIndex)
            {
                case 3:
                {
                    cboStart.Clear();
                    cboEnd.Clear();
                    fraRange.Visible = false;
                    fraWarrant.Visible = false;
                    fraJournal.Visible = true;
                    if (cboWarrant.Items.Count > 0)
                    {
                        cboWarrant.SelectedIndex = 0;
                    }

                    break;
                }
                case 1:
                    optRange_CheckedChanged(sender, e);

                    break;
                case 0:
                    optComplete_CheckedChanged(sender, e);

                    break;
                case 2:
                    optVendorWarrant_CheckedChanged(sender, e);

                    break;
            }
        }

		private void optVendorWarrant_CheckedChanged(object sender, System.EventArgs e)
		{
			cboStart.Clear();
			cboEnd.Clear();
			fraRange.Visible = false;
			fraWarrant.Visible = true;
			fraJournal.Visible = false;
			if (cboJournal.Items.Count > 0)
			{
				cboJournal.SelectedIndex = 0;
			}
		}

		private void FillJournal()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboJournal.Clear();
			rsInfo.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status <> 'D' AND Type = 'AP' ORDER BY JournalNumber, Description");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboJournal.AddItem(Strings.Format(rsInfo.Get_Fields("JournalNumber"), "0000") + " - " + rsInfo.Get_Fields_String("Description"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void FillWarrant()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboWarrant.Clear();
			// FC:FINAL:VGE - #821 Fixed ORDER value to match SELECT one
			rsInfo.OpenRecordset("SELECT DISTINCT Convert(int, IsNull(Warrant, 0)) as WarrantNumber FROM APJournal WHERE Convert(int, IsNull(Warrant, 0)) <> 0 ORDER BY Convert(int, IsNull(Warrant, 0))");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboWarrant.AddItem(Strings.Format(rsInfo.Get_Fields_Int32("WarrantNumber"), "0000"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
