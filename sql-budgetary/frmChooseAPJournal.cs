﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSelectAccounts.
	/// </summary>
	public partial class frmChooseAPJournal : BaseForm
	{
		public frmChooseAPJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseAPJournal InstancePtr
		{
			get
			{
				return (frmChooseAPJournal)Sys.GetInstance(typeof(frmChooseAPJournal));
			}
		}

		protected frmChooseAPJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
        private cGenericCollection journalList;
        private const short IDCOLUMN = 0;
        private const short JOURNALCOLUMN = 1;
        private const short DESCRIPTIONCOLUMN = 2;
        private const short PERIODCOLUMN = 3;
        private const short PAYABLECOLUMN = 4;
        private const short BANKCOLUMN = 5;
        private cBankController bankCont = new cBankController();
        private int lngChosenJournal;

        private void frmChooseAPJournal_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

        public Int32 Init(ref cGenericCollection journals)
        {
            lngChosenJournal = 0;
            journalList = journals;
            this.Show(FormShowEnum.Modal);   //  MAKE THIS MODAL 
            return lngChosenJournal;
        }

        private void cmdOK_Click(object sender, System.EventArgs e)
        {
            Int32 lngRow;
            lngRow = gridJournals.Row;
            if (lngRow > 0)
            {
                lngChosenJournal = Convert.ToInt32(gridJournals.TextMatrix(lngRow, JOURNALCOLUMN));
            }
            Close();
        }

        private void frmChooseAPJournal_Load(object sender, System.EventArgs e)
		{
            //Begin Unmaped Properties
            //frmChooseAPJournal.FillStyle	= 0;
            //frmChooseAPJournal.ScaleWidth	= 5880;
            //frmChooseAPJournal.ScaleHeight	= 3810;
            //frmChooseAPJournal.LinkTopic	= "Form2";
            //frmChooseAPJournal.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
            SetupGrid();
            FillGrid();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

        private void frmChooseAPJournal_Resize(object sender, System.EventArgs e)
        {
            ResizeGrid();
        }

        private void SetupGrid()
        {
            gridJournals.ColHidden(IDCOLUMN, true);
            gridJournals.TextMatrix(0, JOURNALCOLUMN, "Journal");
            gridJournals.TextMatrix(0, DESCRIPTIONCOLUMN, "Description");
            gridJournals.TextMatrix(0, PERIODCOLUMN, "Period");
            gridJournals.TextMatrix(0, BANKCOLUMN, "Bank");
            gridJournals.TextMatrix(0, PAYABLECOLUMN, "Payable Date");
            //gridJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
        }

        private void ResizeGrid()
        {
            int lngWidth = gridJournals.WidthOriginal;
            gridJournals.ColWidth(JOURNALCOLUMN, FCConvert.ToInt32(0.08 * lngWidth));
            gridJournals.ColWidth(DESCRIPTIONCOLUMN, FCConvert.ToInt32(0.3 * lngWidth));
            gridJournals.ColWidth(PERIODCOLUMN, FCConvert.ToInt32(0.08 * lngWidth));
            gridJournals.ColWidth(PAYABLECOLUMN, FCConvert.ToInt32(0.2 * lngWidth));
         }

        private void FillGrid()
        {
            if (journalList != null)
            {
                journalList.MoveFirst();
                Int32 lngRow = 0;
                cAPJournalBrief brief;
                cBank bank;
                while (journalList.IsCurrent())
                {
                    //Application.DoEvents
                    gridJournals.Rows += 1;
                    lngRow = gridJournals.Rows - 1;
                    brief = (cAPJournalBrief) journalList.GetCurrentItem();
                    gridJournals.TextMatrix(lngRow, JOURNALCOLUMN, brief.JournalNumber);
                    gridJournals.TextMatrix(lngRow, DESCRIPTIONCOLUMN, brief.Description);
                    gridJournals.TextMatrix(lngRow, IDCOLUMN, brief.ID);
                    gridJournals.TextMatrix(lngRow, PERIODCOLUMN, brief.Period);
                    gridJournals.TextMatrix(lngRow, PAYABLECOLUMN, brief.PayableDate);
                    bank = bankCont.GetBank(brief.BankID);
                    if (bank != null)
                    {
                        gridJournals.TextMatrix(lngRow, BANKCOLUMN, bank.Name);
                    }
                    journalList.MoveNext();
                }
                gridJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 5, lngRow - 1, 5,
                    FCGrid.AlignmentSettings.flexAlignCenterCenter);
            }
        }


        private void frmChooseAPJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
            Int32 lngRow;
            lngRow = gridJournals.Row;
            if (lngRow > 0)
            {
                lngChosenJournal = Convert.ToInt32(gridJournals.TextMatrix(lngRow, JOURNALCOLUMN));
            }
			Close();
		}

		private void gridJournals_ClickEvent(object sender, System.EventArgs e)
		{
			if (gridJournals.Row > 0)
			{
			}
		}

		private void gridJournals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (gridJournals.Row > 0)
				{
				}
			}
		}

		private void gridJournals_DoubleClick(object sender, EventArgs e)
		{
			if (gridJournals.Row > 0)
			{
				cmdOK_Click(sender, e);
			}
		}
	}
}
