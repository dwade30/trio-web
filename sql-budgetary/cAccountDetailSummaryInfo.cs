﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cAccountDetailSummaryInfo
	{
		//=========================================================
		private string strAccount = string.Empty;
		private int lngJournalNumber;
		private int intPeriod;
		private string strDate = "";
		private string strCheck = string.Empty;
		private double dblDebits;
		private double dblCredits;
		private string strDescription = string.Empty;
		private string strJournalType = string.Empty;
		private bool boolCreditMemo;
		private bool boolInclude;
		private int lngParentID;

		public int ParentID
		{
			set
			{
				lngParentID = value;
			}
			get
			{
				int ParentID = 0;
				ParentID = lngParentID;
				return ParentID;
			}
		}

		public bool Include
		{
			set
			{
				boolInclude = value;
			}
			get
			{
				bool Include = false;
				Include = boolInclude;
				return Include;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}

		public string EntryDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strDate = value;
				}
				else
				{
					strDate = "";
				}
			}
			get
			{
				string EntryDate = "";
				EntryDate = strDate;
				return EntryDate;
			}
		}

		public string Check
		{
			set
			{
				strCheck = value;
			}
			get
			{
				string Check = "";
				Check = strCheck;
				return Check;
			}
		}

		public double Debits
		{
			set
			{
				dblDebits = value;
			}
			get
			{
				double Debits = 0;
				Debits = dblDebits;
				return Debits;
			}
		}

		public double Credits
		{
			set
			{
				dblCredits = value;
			}
			get
			{
				double Credits = 0;
				Credits = dblCredits;
				return Credits;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public bool IsCreditMemo
		{
			set
			{
				boolCreditMemo = value;
			}
			get
			{
				bool IsCreditMemo = false;
				IsCreditMemo = boolCreditMemo;
				return IsCreditMemo;
			}
		}

		public string JournalTypeDescription
		{
			get
			{
				string JournalTypeDescription = "";
				string strReturn = "";
				if (Strings.LCase(strJournalType) == "ap")
				{
					strReturn = "Accounts Payable";
				}
				else if (Strings.LCase(strJournalType) == "py")
				{
					strReturn = "Payroll";
				}
				else if (Strings.LCase(strJournalType) == "gj")
				{
					strReturn = "General Journal";
				}
				else if (Strings.LCase(strJournalType) == "ac")
				{
					if (!IsCreditMemo)
					{
						strReturn = "AP Correction";
					}
					else
					{
						strReturn = "Credit Memo";
					}
				}
				else if (Strings.LCase(strJournalType) == "cm")
				{
					strReturn = "Credit Memo";
				}
				else if (Strings.LCase(strJournalType) == "en")
				{
					strReturn = "Encumbrance";
				}
				else if (Strings.LCase(strJournalType) == "cw")
				{
					strReturn = "Cash Receipts";
				}
				else if (Strings.LCase(strJournalType) == "cr")
				{
					strReturn = "Cash Receipts - Manual";
				}
				else if (Strings.LCase(strJournalType) == "cd")
				{
					strReturn = "Cash Disbursement";
				}
				JournalTypeDescription = strReturn;
				return JournalTypeDescription;
			}
		}
	}
}
