﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBD0000
{
	public class cTrialBalanceController : cReportDataExporter
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;
		// 0 = CSV, 1 = Tab
		private bool boolUseExpDivision;
		private bool boolUseExpObject;
		private bool boolUseRevDivision;

		public cTrialBalanceController() : base()
		{
			boolUseExpDivision = !modAccountTitle.Statics.ExpDivFlag;
			boolUseExpObject = !modAccountTitle.Statics.ObjFlag;
			boolUseRevDivision = !modAccountTitle.Statics.RevDivFlag;
		}

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theReport As cDetailsReport	OnRead(cTrialBalanceReport)
		public void ExportData(ref cDetailsReport theReport, string strFileName)
		{
			ClearErrors();
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				string strSeparator = "";
				string strQ = "";
				// vbPorter upgrade warning: tbReport As cTrialBalanceReport	OnWrite(cDetailsReport)
				cTrialBalanceReport tbReport;
				tbReport = (cTrialBalanceReport)theReport;
				strPath = Path.GetDirectoryName(strFileName);
				if (!Directory.Exists(strPath))
				{
					Information.Err().Raise(9999, null, "Path not found", null, null);
				}
				if (intFormatType == 1)
				{
					strSeparator = "\t";
					strQ = "";
				}
				else
				{
					strSeparator = ",";
					strQ = FCConvert.ToString(Convert.ToChar(34));
				}
				cAccountReportGroup fundItem;
				cAccountSummaryItem currItem;
				cAccountReportGroup acctGroup;
				string strLine;
				ts = new StreamWriter(strFileName);
				strLine = strQ + "Fund" + strQ;
				strLine += strSeparator + strQ + "Account" + strQ;
				strLine += strSeparator + strQ + "Account Type" + strQ;
				strLine += strSeparator + strQ + "Account Group Type" + strQ;
				strLine += strSeparator + strQ + "Beginning Balance" + strQ;
				strLine += strSeparator + strQ + "Debits" + strQ;
				strLine += strSeparator + strQ + "Credits" + strQ;
				strLine += strSeparator + strQ + "Net Change" + strQ;
				strLine += strSeparator + strQ + "Balance" + strQ;
				ts.WriteLine(strLine);
				theReport.Details.MoveFirst();
				string strFund = "";
				while (theReport.Details.IsCurrent())
				{
					strLine = "";
					fundItem = (cAccountReportGroup)theReport.Details.GetCurrentItem();
					strFund = fundItem.GroupValue;
					fundItem.Accounts.MoveFirst();
					while (fundItem.Accounts.IsCurrent())
					{
						//Application.DoEvents();
						acctGroup = (cAccountReportGroup)fundItem.Accounts.GetCurrentItem();
						acctGroup.Accounts.MoveFirst();
						while (acctGroup.Accounts.IsCurrent())
						{
							//Application.DoEvents();
							currItem = (cAccountSummaryItem)acctGroup.Accounts.GetCurrentItem();
							strLine = strQ + strFund + strQ;
                            strLine = strLine + strSeparator + strQ + currItem.Account + strQ;
							strLine += strSeparator + strQ + currItem.AccountType + strQ;
							strLine += strSeparator + strQ + acctGroup.Description + strQ;
							strLine += strSeparator + Strings.Format(currItem.BeginningNet, "0.00");
							strLine += strSeparator + Strings.Format(currItem.Debits, "0.00");
							strLine += strSeparator + Strings.Format(currItem.Credits, "0.00");
							strLine += strSeparator + Strings.Format(currItem.Net, "0.00");
							strLine += strSeparator + Strings.Format(currItem.EndBalance, "0.00");
							ts.WriteLine(strLine);
							acctGroup.Accounts.MoveNext();
						}
						fundItem.Accounts.MoveNext();
					}
					theReport.Details.MoveNext();
				}
				ts.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (!(ts == null))
				{
					ts.Close();
				}
			}
		}
	}
}
