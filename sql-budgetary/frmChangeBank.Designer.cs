//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeBank.
	/// </summary>
	partial class frmChangeBank : BaseForm
	{
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCComboBox cboBanks;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeBank));
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cboBanks = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 244);
			this.BottomPanel.Size = new System.Drawing.Size(498, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.cboBanks);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(498, 184);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(498, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(161, 30);
			this.HeaderText.Text = "Change Bank";
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(30, 126);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(104, 48);
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.TabIndex = 2;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// cboBanks
			// 
			this.cboBanks.AutoSize = false;
			this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
			this.cboBanks.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBanks.FormattingEnabled = true;
			this.cboBanks.Location = new System.Drawing.Point(30, 66);
			this.cboBanks.Name = "cboBanks";
			this.cboBanks.Size = new System.Drawing.Size(438, 40);
			this.cboBanks.TabIndex = 0;
			this.cboBanks.Text = "Combo1";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(40, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "BANK";
			// 
			// frmChangeBank
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(498, 352);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmChangeBank";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Change Bank";
			this.Load += new System.EventHandler(this.frmChangeBank_Load);
			this.Activated += new System.EventHandler(this.frmChangeBank_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChangeBank_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}