﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class clsACHBatchHeader
	{
		//=========================================================
		private bool boolBalanced;
		private string strCompanyName = "";
		private string strDiscretionaryData = "";
		private string strCompanyID = "";
		private string strEntryClassCode = "";
		private string strEntryDescription = "";
		private string strDescriptiveDate = "";
		private string strEffectiveEntryDate = "";
		private string strOriginatorStatusCode = "";
		private string strOriginatingDFI = "";
		private int intBatchNumber;
		private string strLastError = "";
		private string strACHCompanyIDPrefix = string.Empty;

		public string RecordCode
		{
			get
			{
				string RecordCode = "";
				RecordCode = "5";
				return RecordCode;
			}
		}

		public bool Balanced
		{
			get
			{
				bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}
			set
			{
				boolBalanced = value;
			}
		}

		public string CompanyName
		{
			get
			{
				string CompanyName = "";
				CompanyName = strCompanyName;
				return CompanyName;
			}
			set
			{
				strCompanyName = value;
			}
		}

		public string DiscretionaryData
		{
			get
			{
				string DiscretionaryData = "";
				DiscretionaryData = strDiscretionaryData;
				return DiscretionaryData;
			}
			set
			{
				strDiscretionaryData = value;
			}
		}

		public string CompanyID
		{
			get
			{
				string CompanyID = "";
				CompanyID = strCompanyID;
				return CompanyID;
			}
			set
			{
				strCompanyID = value;
			}
		}

		public string EntryClassCode
		{
			get
			{
				string EntryClassCode = "";
				EntryClassCode = strEntryClassCode;
				return EntryClassCode;
			}
			set
			{
				strEntryClassCode = value;
			}
		}

		public string EntryDescription
		{
			get
			{
				string EntryDescription = "";
				EntryDescription = strEntryDescription;
				return EntryDescription;
			}
			set
			{
				strEntryDescription = value;
			}
		}

		public string DescriptiveDate
		{
			get
			{
				string DescriptiveDate = "";
				DescriptiveDate = strDescriptiveDate;
				return DescriptiveDate;
			}
			set
			{
				strDescriptiveDate = value;
			}
		}

		public string EffectiveEntryDate
		{
			get
			{
				string EffectiveEntryDate = "";
				EffectiveEntryDate = strEffectiveEntryDate;
				return EffectiveEntryDate;
			}
			set
			{
				strEffectiveEntryDate = value;
			}
		}

		public string OriginatorStatusCode
		{
			get
			{
				string OriginatorStatusCode = "";
				OriginatorStatusCode = strOriginatorStatusCode;
				return OriginatorStatusCode;
			}
			set
			{
				strOriginatorStatusCode = value;
			}
		}

		public string OriginatingDFI
		{
			get
			{
				string OriginatingDFI = "";
				OriginatingDFI = strOriginatingDFI;
				return OriginatingDFI;
			}
			set
			{
				strOriginatingDFI = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short BatchNumber
		{
			get
			{
				short BatchNumber = 0;
				BatchNumber = FCConvert.ToInt16(intBatchNumber);
				return BatchNumber;
			}
			set
			{
				intBatchNumber = value;
			}
		}

		public string LastError
		{
			get
			{
				string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public string ServiceClassCode
		{
			get
			{
				string ServiceClassCode = "";
				if (boolBalanced)
				{
					ServiceClassCode = "200";
				}
				else
				{
					ServiceClassCode = "220";
				}
				return ServiceClassCode;
			}
		}

		public clsACHBatchHeader() : base()
		{
			strEntryClassCode = "PPD";
			strEntryDescription = "ACCTS PAY";
			strOriginatorStatusCode = "1";
			intBatchNumber = 0;
			strACHCompanyIDPrefix = "1";
			strLastError = "";
		}

		public string OutputLine()
		{
			string OutputLine = "";
			try
			{
				// On Error GoTo ErrorHandler
				string strLine;
				strLine = RecordCode;
				strLine += ServiceClassCode;
				strLine += Strings.Left(strCompanyName + Strings.StrDup(16, " "), 16);
				strLine += Strings.Left(strDiscretionaryData + Strings.StrDup(20, " "), 20);
				strLine += strACHCompanyIDPrefix + strCompanyID;
				strLine += strEntryClassCode;
				strLine += Strings.Left(strEntryDescription + Strings.StrDup(10, " "), 10);
				if (Information.IsDate(strDescriptiveDate))
				{
					strLine += Strings.Right(FCConvert.ToString(FCConvert.ToDateTime(strDescriptiveDate).Year), 2) + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(strDescriptiveDate).Month), 2) + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(strDescriptiveDate).Day), 2);
				}
				else
				{
					strLine += Strings.StrDup(6, "0");
				}
				// effective entry date
				if (Information.IsDate(strEffectiveEntryDate))
				{
					strLine += Strings.Right(FCConvert.ToString(FCConvert.ToDateTime(strEffectiveEntryDate).Year), 2) + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(strEffectiveEntryDate).Month), 2) + Strings.Right("00" + FCConvert.ToString(FCConvert.ToDateTime(strEffectiveEntryDate).Day), 2);
				}
				else
				{
					strLine += Strings.StrDup(6, "0");
				}
				strLine += Strings.StrDup(3, " ");
				strLine += strOriginatorStatusCode;
				strLine += Strings.Left(strOriginatingDFI + Strings.StrDup(FCConvert.ToInt32("0"), Strings.Chr(8).ToString()), 8);
				strLine += Strings.Right(Strings.StrDup(7, "0") + FCConvert.ToString(intBatchNumber), 7);
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = "Could not build batch header record." + "\r\n" + Information.Err(ex).Description;
			}
			return OutputLine;
		}
	}
}
