﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorListSetup.
	/// </summary>
	partial class frmVendorListSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbMessageNo;
		public fecherFoundation.FCComboBox cmbNo;
		public fecherFoundation.FCComboBox cmbAdjustmentsYes;
		public fecherFoundation.FCComboBox cmbInsuranceDateYes;
		public fecherFoundation.FCComboBox cmbSpecificClass;
		public fecherFoundation.FCLabel lblSpecificClass;
		public fecherFoundation.FCComboBox cmbStatusAll;
		public fecherFoundation.FCLabel lblStatusAll;
		public fecherFoundation.FCFrame fraListSelection;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCCheckBox chkInsuranceVerifieddate;
		public fecherFoundation.FCCheckBox chkPhone;
		public fecherFoundation.FCCheckBox chkFax;
		public fecherFoundation.FCCheckBox chkVendorMessage;
		public fecherFoundation.FCCheckBox chkDataEntryMessage;
		public fecherFoundation.FCFrame fraDataEntryMessage;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCCheckBox chkTaxID;
		public fecherFoundation.FCCheckBox chkAddress;
		public fecherFoundation.FCCheckBox chkYTDAmount;
		public fecherFoundation.FCCheckBox chkAdjustments;
		public fecherFoundation.FCFrame fraZeroYTDAmount;
		public fecherFoundation.FCCheckBox chkOnlyZeroAmounts;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraZeroAdjustments;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraInsuranceRequired;
		public Global.T2KDateBox txtInsuranceVerifiedDate;
		public fecherFoundation.FCLabel lblInsuranceVerifiedDate;
		public fecherFoundation.FCFrame fraSpecificClass;
		public fecherFoundation.FCFrame fraClass;
		public fecherFoundation.FCComboBox cboClass;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraStatus;
		public fecherFoundation.FCCheckBox chkDeleted;
		public fecherFoundation.FCCheckBox chkSuspended;
		public fecherFoundation.FCCheckBox chkActive;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendorListSetup));
			this.cmbNumber = new fecherFoundation.FCComboBox();
			this.lblNumber = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbMessageNo = new fecherFoundation.FCComboBox();
			this.cmbNo = new fecherFoundation.FCComboBox();
			this.cmbAdjustmentsYes = new fecherFoundation.FCComboBox();
			this.cmbInsuranceDateYes = new fecherFoundation.FCComboBox();
			this.cmbSpecificClass = new fecherFoundation.FCComboBox();
			this.lblSpecificClass = new fecherFoundation.FCLabel();
			this.cmbStatusAll = new fecherFoundation.FCComboBox();
			this.lblStatusAll = new fecherFoundation.FCLabel();
			this.fraListSelection = new fecherFoundation.FCFrame();
			this.fraRange = new fecherFoundation.FCFrame();
			this.txtLow = new fecherFoundation.FCTextBox();
			this.txtHigh = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraInformation = new fecherFoundation.FCFrame();
			this.chkInsuranceVerifieddate = new fecherFoundation.FCCheckBox();
			this.chkPhone = new fecherFoundation.FCCheckBox();
			this.chkFax = new fecherFoundation.FCCheckBox();
			this.chkVendorMessage = new fecherFoundation.FCCheckBox();
			this.chkDataEntryMessage = new fecherFoundation.FCCheckBox();
			this.fraDataEntryMessage = new fecherFoundation.FCFrame();
			this.Label5 = new fecherFoundation.FCLabel();
			this.chkTaxID = new fecherFoundation.FCCheckBox();
			this.chkAddress = new fecherFoundation.FCCheckBox();
			this.chkYTDAmount = new fecherFoundation.FCCheckBox();
			this.chkAdjustments = new fecherFoundation.FCCheckBox();
			this.fraZeroYTDAmount = new fecherFoundation.FCFrame();
			this.chkOnlyZeroAmounts = new fecherFoundation.FCCheckBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraZeroAdjustments = new fecherFoundation.FCFrame();
			this.Label4 = new fecherFoundation.FCLabel();
			this.fraInsuranceRequired = new fecherFoundation.FCFrame();
			this.txtInsuranceVerifiedDate = new Global.T2KDateBox();
			this.lblInsuranceVerifiedDate = new fecherFoundation.FCLabel();
			this.fraSpecificClass = new fecherFoundation.FCFrame();
			this.fraClass = new fecherFoundation.FCFrame();
			this.cboClass = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraStatus = new fecherFoundation.FCFrame();
			this.chkDeleted = new fecherFoundation.FCCheckBox();
			this.chkSuspended = new fecherFoundation.FCCheckBox();
			this.chkActive = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraListSelection)).BeginInit();
			this.fraListSelection.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
			this.fraInformation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInsuranceVerifieddate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVendorMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntryMessage)).BeginInit();
			this.fraDataEntryMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkTaxID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYTDAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraZeroYTDAmount)).BeginInit();
			this.fraZeroYTDAmount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOnlyZeroAmounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraZeroAdjustments)).BeginInit();
			this.fraZeroAdjustments.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraInsuranceRequired)).BeginInit();
			this.fraInsuranceRequired.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtInsuranceVerifiedDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClass)).BeginInit();
			this.fraSpecificClass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraClass)).BeginInit();
			this.fraClass.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).BeginInit();
			this.fraStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuspended)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 485);
			this.BottomPanel.Size = new System.Drawing.Size(791, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbNumber);
			this.ClientArea.Controls.Add(this.lblNumber);
			this.ClientArea.Controls.Add(this.fraListSelection);
			this.ClientArea.Controls.Add(this.fraInformation);
			this.ClientArea.Controls.Add(this.fraSpecificClass);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(791, 425);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(791, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(153, 30);
			this.HeaderText.Text = "Print Listings";
			// 
			// cmbNumber
			// 
			this.cmbNumber.AutoSize = false;
			this.cmbNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNumber.FormattingEnabled = true;
			this.cmbNumber.Items.AddRange(new object[] {
				"Number",
				"Name"
			});
			this.cmbNumber.Location = new System.Drawing.Point(111, 30);
			this.cmbNumber.Name = "cmbNumber";
			this.cmbNumber.Size = new System.Drawing.Size(106, 40);
			this.cmbNumber.TabIndex = 0;
            this.cmbNumber.SelectedIndexChanged += new System.EventHandler(CmbNumber_SelectedIndexChanged);
			// 
			// lblNumber
			// 
			this.lblNumber.Location = new System.Drawing.Point(30, 44);
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Size = new System.Drawing.Size(45, 16);
			this.lblNumber.TabIndex = 1;
			this.lblNumber.Text = "LIST BY";
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Selected Range"
			});
			this.cmbRange.Location = new System.Drawing.Point(142, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(165, 40);
			this.cmbRange.TabIndex = 26;
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.Location = new System.Drawing.Point(20, 40);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(88, 16);
			this.lblRange.TabIndex = 27;
			this.lblRange.Text = "SELECT RANGE";
			// 
			// cmbMessageNo
			// 
			this.cmbMessageNo.AutoSize = false;
			this.cmbMessageNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbMessageNo.FormattingEnabled = true;
			this.cmbMessageNo.Items.AddRange(new object[] {
				"No",
				"Yes"
			});
			this.cmbMessageNo.Location = new System.Drawing.Point(20, 56);
			this.cmbMessageNo.Name = "cmbMessageNo";
			this.cmbMessageNo.Size = new System.Drawing.Size(115, 40);
			this.cmbMessageNo.TabIndex = 41;
			// 
			// cmbNo
			// 
			this.cmbNo.AutoSize = false;
			this.cmbNo.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNo.FormattingEnabled = true;
			this.cmbNo.Items.AddRange(new object[] {
				"No",
				"Yes"
			});
			this.cmbNo.Location = new System.Drawing.Point(20, 56);
			this.cmbNo.Name = "cmbNo";
			this.cmbNo.Size = new System.Drawing.Size(115, 40);
			this.cmbNo.TabIndex = 43;
			this.cmbNo.SelectedIndexChanged += new System.EventHandler(this.optNo_CheckedChanged);
			// 
			// cmbAdjustmentsYes
			// 
			this.cmbAdjustmentsYes.AutoSize = false;
			this.cmbAdjustmentsYes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAdjustmentsYes.FormattingEnabled = true;
			this.cmbAdjustmentsYes.Items.AddRange(new object[] {
				"Yes",
				"No"
			});
			this.cmbAdjustmentsYes.Location = new System.Drawing.Point(20, 56);
			this.cmbAdjustmentsYes.Name = "cmbAdjustmentsYes";
			this.cmbAdjustmentsYes.Size = new System.Drawing.Size(115, 40);
			this.cmbAdjustmentsYes.TabIndex = 22;
			// 
			// cmbInsuranceDateYes
			// 
			this.cmbInsuranceDateYes.AutoSize = false;
			this.cmbInsuranceDateYes.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbInsuranceDateYes.FormattingEnabled = true;
			this.cmbInsuranceDateYes.Items.AddRange(new object[] {
				"Yes",
				"No"
			});
			this.cmbInsuranceDateYes.Location = new System.Drawing.Point(20, 56);
			this.cmbInsuranceDateYes.Name = "cmbInsuranceDateYes";
			this.cmbInsuranceDateYes.Size = new System.Drawing.Size(115, 40);
			this.cmbInsuranceDateYes.TabIndex = 49;
			this.cmbInsuranceDateYes.SelectedIndexChanged += new System.EventHandler(this.optInsuranceDateYes_CheckedChanged);
			// 
			// cmbSpecificClass
			// 
			this.cmbSpecificClass.AutoSize = false;
			this.cmbSpecificClass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSpecificClass.FormattingEnabled = true;
			this.cmbSpecificClass.Items.AddRange(new object[] {
				"All",
				"Selected Class"
			});
			this.cmbSpecificClass.Location = new System.Drawing.Point(143, 30);
			this.cmbSpecificClass.Name = "cmbSpecificClass";
			this.cmbSpecificClass.Size = new System.Drawing.Size(151, 40);
			this.cmbSpecificClass.TabIndex = 8;
			this.cmbSpecificClass.SelectedIndexChanged += new System.EventHandler(this.optSpecificClass_CheckedChanged);
			// 
			// lblSpecificClass
			// 
			this.lblSpecificClass.Location = new System.Drawing.Point(20, 44);
			this.lblSpecificClass.Name = "lblSpecificClass";
			this.lblSpecificClass.Size = new System.Drawing.Size(87, 16);
			this.lblSpecificClass.TabIndex = 9;
			this.lblSpecificClass.Text = "SELECT CLASS";
			// 
			// cmbStatusAll
			// 
			this.cmbStatusAll.AutoSize = false;
			this.cmbStatusAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStatusAll.FormattingEnabled = true;
			this.cmbStatusAll.Items.AddRange(new object[] {
				"All",
				"Selected Status"
			});
			this.cmbStatusAll.Location = new System.Drawing.Point(147, 30);
			this.cmbStatusAll.Name = "cmbStatusAll";
			this.cmbStatusAll.Size = new System.Drawing.Size(165, 40);
			this.cmbStatusAll.TabIndex = 4;
			this.cmbStatusAll.SelectedIndexChanged += new System.EventHandler(this.optStatusAll_CheckedChanged);
			// 
			// lblStatusAll
			// 
			this.lblStatusAll.Location = new System.Drawing.Point(20, 44);
			this.lblStatusAll.Name = "lblStatusAll";
			this.lblStatusAll.Size = new System.Drawing.Size(92, 16);
			this.lblStatusAll.TabIndex = 5;
			this.lblStatusAll.Text = "SELECT STATUS";
			// 
			// fraListSelection
			// 
			this.fraListSelection.Controls.Add(this.fraRange);
			this.fraListSelection.Controls.Add(this.cmbRange);
			this.fraListSelection.Controls.Add(this.lblRange);
			this.fraListSelection.Location = new System.Drawing.Point(382, 90);
			this.fraListSelection.Name = "fraListSelection";
			this.fraListSelection.Size = new System.Drawing.Size(327, 208);
			this.fraListSelection.TabIndex = 22;
			this.fraListSelection.Text = "List Selection";
			// 
			// fraRange
			// 
			this.fraRange.AppearanceKey = "groupBoxNoBorders";
			this.fraRange.Controls.Add(this.txtLow);
			this.fraRange.Controls.Add(this.txtHigh);
			this.fraRange.Controls.Add(this.Label1);
			this.fraRange.Controls.Add(this.Label2);
			this.fraRange.Enabled = false;
			this.fraRange.Location = new System.Drawing.Point(0, 70);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(327, 138);
			this.fraRange.TabIndex = 25;
			// 
			// txtLow
			// 
			this.txtLow.AutoSize = false;
			this.txtLow.BackColor = System.Drawing.SystemColors.Window;
			this.txtLow.Location = new System.Drawing.Point(144, 20);
			this.txtLow.Name = "txtLow";
			this.txtLow.Size = new System.Drawing.Size(163, 40);
			this.txtLow.TabIndex = 27;
			this.txtLow.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLow_KeyPress);
			this.txtLow.Enter += new System.EventHandler(this.txtLow_Enter);
			// 
			// txtHigh
			// 
			this.txtHigh.AutoSize = false;
			this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
			this.txtHigh.Location = new System.Drawing.Point(144, 80);
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Size = new System.Drawing.Size(163, 40);
			this.txtHigh.TabIndex = 26;
			this.txtHigh.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHigh_KeyPress);
			this.txtHigh.Enter += new System.EventHandler(this.txtHigh_Enter);
			// 
			// Label1
			// 
			this.Label1.Enabled = false;
			this.Label1.Location = new System.Drawing.Point(20, 34);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(40, 16);
			this.Label1.TabIndex = 29;
			this.Label1.Text = "START";
			// 
			// Label2
			// 
			this.Label2.Enabled = false;
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(27, 16);
			this.Label2.TabIndex = 28;
			this.Label2.Text = "END";
			// 
			// fraInformation
			// 
			this.fraInformation.Controls.Add(this.chkInsuranceVerifieddate);
			this.fraInformation.Controls.Add(this.chkPhone);
			this.fraInformation.Controls.Add(this.chkFax);
			this.fraInformation.Controls.Add(this.chkVendorMessage);
			this.fraInformation.Controls.Add(this.chkDataEntryMessage);
			this.fraInformation.Controls.Add(this.fraDataEntryMessage);
			this.fraInformation.Controls.Add(this.chkTaxID);
			this.fraInformation.Controls.Add(this.chkAddress);
			this.fraInformation.Controls.Add(this.chkYTDAmount);
			this.fraInformation.Controls.Add(this.chkAdjustments);
			this.fraInformation.Controls.Add(this.fraZeroYTDAmount);
			this.fraInformation.Controls.Add(this.fraZeroAdjustments);
			this.fraInformation.Controls.Add(this.fraInsuranceRequired);
			this.fraInformation.Location = new System.Drawing.Point(30, 318);
			this.fraInformation.Name = "fraInformation";
			this.fraInformation.Size = new System.Drawing.Size(614, 511);
			this.fraInformation.TabIndex = 9;
			this.fraInformation.Text = "Information To Report";
			// 
			// chkInsuranceVerifieddate
			// 
			this.chkInsuranceVerifieddate.AutoSize = false;
			this.chkInsuranceVerifieddate.Location = new System.Drawing.Point(272, 311);
			this.chkInsuranceVerifieddate.Name = "chkInsuranceVerifieddate";
			this.chkInsuranceVerifieddate.Size = new System.Drawing.Size(193, 24);
			this.chkInsuranceVerifieddate.TabIndex = 45;
			this.chkInsuranceVerifieddate.Text = "Insurance Verified Date";
			this.chkInsuranceVerifieddate.CheckedChanged += new System.EventHandler(this.chkInsuranceVerifieddate_CheckedChanged);
			// 
			// chkPhone
			// 
			this.chkPhone.AutoSize = false;
			this.chkPhone.Location = new System.Drawing.Point(20, 132);
			this.chkPhone.Name = "chkPhone";
			this.chkPhone.Size = new System.Drawing.Size(165, 24);
			this.chkPhone.TabIndex = 44;
			this.chkPhone.Text = "Telephone Number";
			// 
			// chkFax
			// 
			this.chkFax.AutoSize = false;
			this.chkFax.Location = new System.Drawing.Point(20, 166);
			this.chkFax.Name = "chkFax";
			this.chkFax.Size = new System.Drawing.Size(114, 24);
			this.chkFax.TabIndex = 43;
			this.chkFax.Text = "Fax Number";
			// 
			// chkVendorMessage
			// 
			this.chkVendorMessage.AutoSize = false;
			this.chkVendorMessage.Location = new System.Drawing.Point(20, 98);
			this.chkVendorMessage.Name = "chkVendorMessage";
			this.chkVendorMessage.Size = new System.Drawing.Size(148, 24);
			this.chkVendorMessage.TabIndex = 41;
			this.chkVendorMessage.Text = "Vendor Message";
			// 
			// chkDataEntryMessage
			// 
			this.chkDataEntryMessage.AutoSize = false;
			this.chkDataEntryMessage.Location = new System.Drawing.Point(272, 170);
			this.chkDataEntryMessage.Name = "chkDataEntryMessage";
			this.chkDataEntryMessage.Size = new System.Drawing.Size(170, 24);
			this.chkDataEntryMessage.TabIndex = 36;
			this.chkDataEntryMessage.Text = "Data Entry Message";
			this.chkDataEntryMessage.CheckedChanged += new System.EventHandler(this.chkDataEntryMessage_CheckedChanged);
			// 
			// fraDataEntryMessage
			// 
			this.fraDataEntryMessage.AppearanceKey = "groupBoxNoBorders";
			this.fraDataEntryMessage.Controls.Add(this.Label5);
			this.fraDataEntryMessage.Controls.Add(this.cmbMessageNo);
			this.fraDataEntryMessage.Enabled = false;
			this.fraDataEntryMessage.Location = new System.Drawing.Point(272, 194);
			this.fraDataEntryMessage.Name = "fraDataEntryMessage";
			this.fraDataEntryMessage.Size = new System.Drawing.Size(290, 117);
			this.fraDataEntryMessage.TabIndex = 37;
			// 
			// Label5
			// 
			this.Label5.Enabled = false;
			this.Label5.Location = new System.Drawing.Point(20, 20);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(250, 16);
			this.Label5.TabIndex = 40;
			this.Label5.Text = "ONLY VENDORS HAVING A DATA MESSAGE";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// chkTaxID
			// 
			this.chkTaxID.AutoSize = false;
			this.chkTaxID.Location = new System.Drawing.Point(20, 30);
			this.chkTaxID.Name = "chkTaxID";
			this.chkTaxID.Size = new System.Drawing.Size(73, 24);
			this.chkTaxID.TabIndex = 13;
			this.chkTaxID.Text = "Tax ID";
			// 
			// chkAddress
			// 
			this.chkAddress.AutoSize = false;
			this.chkAddress.Location = new System.Drawing.Point(20, 64);
			this.chkAddress.Name = "chkAddress";
			this.chkAddress.Size = new System.Drawing.Size(87, 24);
			this.chkAddress.TabIndex = 12;
			this.chkAddress.Text = "Address";
			// 
			// chkYTDAmount
			// 
			this.chkYTDAmount.AutoSize = false;
			this.chkYTDAmount.Location = new System.Drawing.Point(20, 200);
			this.chkYTDAmount.Name = "chkYTDAmount";
			this.chkYTDAmount.Size = new System.Drawing.Size(117, 24);
			this.chkYTDAmount.TabIndex = 11;
			this.chkYTDAmount.Text = "YTD Amount";
			this.chkYTDAmount.CheckedChanged += new System.EventHandler(this.chkYTDAmount_CheckedChanged);
			// 
			// chkAdjustments
			// 
			this.chkAdjustments.AutoSize = false;
			this.chkAdjustments.Location = new System.Drawing.Point(272, 30);
			this.chkAdjustments.Name = "chkAdjustments";
			this.chkAdjustments.Size = new System.Drawing.Size(115, 24);
			this.chkAdjustments.TabIndex = 10;
			this.chkAdjustments.Text = "Adjustments";
			this.chkAdjustments.CheckedChanged += new System.EventHandler(this.chkAdjustments_CheckedChanged);
			// 
			// fraZeroYTDAmount
			// 
			this.fraZeroYTDAmount.AppearanceKey = "groupBoxNoBorders";
			this.fraZeroYTDAmount.Controls.Add(this.chkOnlyZeroAmounts);
			this.fraZeroYTDAmount.Controls.Add(this.cmbNo);
			this.fraZeroYTDAmount.Controls.Add(this.Label3);
			this.fraZeroYTDAmount.Enabled = false;
			this.fraZeroYTDAmount.Location = new System.Drawing.Point(0, 224);
			this.fraZeroYTDAmount.Name = "fraZeroYTDAmount";
			this.fraZeroYTDAmount.Size = new System.Drawing.Size(252, 150);
			this.fraZeroYTDAmount.TabIndex = 14;
			// 
			// chkOnlyZeroAmounts
			// 
			this.chkOnlyZeroAmounts.AutoSize = false;
			this.chkOnlyZeroAmounts.Location = new System.Drawing.Point(20, 106);
			this.chkOnlyZeroAmounts.Name = "chkOnlyZeroAmounts";
			this.chkOnlyZeroAmounts.Size = new System.Drawing.Size(176, 24);
			this.chkOnlyZeroAmounts.TabIndex = 42;
			this.chkOnlyZeroAmounts.Text = "Only Show Zero YTD";
			// 
			// Label3
			// 
			this.Label3.Enabled = false;
			this.Label3.Location = new System.Drawing.Point(20, 20);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(215, 16);
			this.Label3.TabIndex = 17;
			this.Label3.Text = "REPORT ZERO YTD AMOUNT LISTINGS";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraZeroAdjustments
			// 
			this.fraZeroAdjustments.AppearanceKey = "groupBoxNoBorders";
			this.fraZeroAdjustments.Controls.Add(this.Label4);
			this.fraZeroAdjustments.Controls.Add(this.cmbAdjustmentsYes);
			this.fraZeroAdjustments.Enabled = false;
			this.fraZeroAdjustments.Location = new System.Drawing.Point(272, 54);
			this.fraZeroAdjustments.Name = "fraZeroAdjustments";
			this.fraZeroAdjustments.Size = new System.Drawing.Size(260, 116);
			this.fraZeroAdjustments.TabIndex = 18;
			// 
			// Label4
			// 
			this.Label4.Enabled = false;
			this.Label4.Location = new System.Drawing.Point(20, 20);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(224, 16);
			this.Label4.TabIndex = 21;
			this.Label4.Text = "REPORT ZERO ADJUSTMENTS LISTINGS";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraInsuranceRequired
			// 
			this.fraInsuranceRequired.AppearanceKey = "groupBoxNoBorders";
			this.fraInsuranceRequired.Controls.Add(this.txtInsuranceVerifiedDate);
			this.fraInsuranceRequired.Controls.Add(this.cmbInsuranceDateYes);
			this.fraInsuranceRequired.Controls.Add(this.lblInsuranceVerifiedDate);
			this.fraInsuranceRequired.Enabled = false;
			this.fraInsuranceRequired.Location = new System.Drawing.Point(272, 335);
			this.fraInsuranceRequired.Name = "fraInsuranceRequired";
			this.fraInsuranceRequired.Size = new System.Drawing.Size(342, 176);
			this.fraInsuranceRequired.TabIndex = 46;
			// 
			// txtInsuranceVerifiedDate
			// 
			this.txtInsuranceVerifiedDate.Location = new System.Drawing.Point(20, 116);
			this.txtInsuranceVerifiedDate.Mask = "00/00/0000";
			this.txtInsuranceVerifiedDate.Name = "txtInsuranceVerifiedDate";
			this.txtInsuranceVerifiedDate.Size = new System.Drawing.Size(115, 40);
			this.txtInsuranceVerifiedDate.TabIndex = 48;
			this.txtInsuranceVerifiedDate.Text = "  /  /";
			// 
			// lblInsuranceVerifiedDate
			// 
			this.lblInsuranceVerifiedDate.Enabled = false;
			this.lblInsuranceVerifiedDate.Location = new System.Drawing.Point(20, 20);
			this.lblInsuranceVerifiedDate.Name = "lblInsuranceVerifiedDate";
			this.lblInsuranceVerifiedDate.Size = new System.Drawing.Size(305, 16);
			this.lblInsuranceVerifiedDate.TabIndex = 47;
			this.lblInsuranceVerifiedDate.Text = "ONLY VENDORS WITH DATE EARLIER THAN SPECIFIED";
			this.lblInsuranceVerifiedDate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraSpecificClass
			// 
			this.fraSpecificClass.Controls.Add(this.fraClass);
			this.fraSpecificClass.Controls.Add(this.cmbSpecificClass);
			this.fraSpecificClass.Controls.Add(this.lblSpecificClass);
			this.fraSpecificClass.Location = new System.Drawing.Point(729, 90);
			this.fraSpecificClass.Name = "fraSpecificClass";
			this.fraSpecificClass.Size = new System.Drawing.Size(314, 150);
			this.fraSpecificClass.TabIndex = 4;
			this.fraSpecificClass.Text = "Class Selection";
			// 
			// fraClass
			// 
			this.fraClass.AppearanceKey = "groupBoxNoBorders";
			this.fraClass.Controls.Add(this.cboClass);
			this.fraClass.Enabled = false;
			this.fraClass.Location = new System.Drawing.Point(123, 70);
			this.fraClass.Name = "fraClass";
			this.fraClass.Size = new System.Drawing.Size(111, 80);
			this.fraClass.TabIndex = 7;
			// 
			// cboClass
			// 
			this.cboClass.AutoSize = false;
			this.cboClass.BackColor = System.Drawing.SystemColors.Window;
			this.cboClass.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboClass.Enabled = false;
			this.cboClass.FormattingEnabled = true;
			this.cboClass.Items.AddRange(new object[] {
				"1",
				"2",
				"3",
				"4",
				"5",
				"6",
				"7",
				"8",
				"9",
				"A",
				"B",
				"C",
				"D",
				"E",
				"F",
				"G",
				"H",
				"I",
				"J",
				"K"
			});
			this.cboClass.Location = new System.Drawing.Point(20, 20);
			this.cboClass.Name = "cboClass";
			this.cboClass.Size = new System.Drawing.Size(71, 40);
			this.cboClass.TabIndex = 8;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.fraStatus);
			this.Frame1.Controls.Add(this.cmbStatusAll);
			this.Frame1.Controls.Add(this.lblStatusAll);
			this.Frame1.Location = new System.Drawing.Point(30, 90);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(332, 208);
			this.Frame1.TabIndex = 23;
			this.Frame1.Text = "Status Selection";
			// 
			// fraStatus
			// 
			this.fraStatus.AppearanceKey = "groupBoxNoBorders";
			this.fraStatus.Controls.Add(this.chkDeleted);
			this.fraStatus.Controls.Add(this.chkSuspended);
			this.fraStatus.Controls.Add(this.chkActive);
			this.fraStatus.Enabled = false;
			this.fraStatus.Location = new System.Drawing.Point(0, 70);
			this.fraStatus.Name = "fraStatus";
			this.fraStatus.Size = new System.Drawing.Size(150, 138);
			this.fraStatus.TabIndex = 3;
			// 
			// chkDeleted
			// 
			this.chkDeleted.AutoSize = false;
			this.chkDeleted.Enabled = false;
			this.chkDeleted.Location = new System.Drawing.Point(20, 92);
			this.chkDeleted.Name = "chkDeleted";
			this.chkDeleted.Size = new System.Drawing.Size(84, 24);
			this.chkDeleted.TabIndex = 35;
			this.chkDeleted.Text = "Deleted";
			// 
			// chkSuspended
			// 
			this.chkSuspended.AutoSize = false;
			this.chkSuspended.Enabled = false;
			this.chkSuspended.Location = new System.Drawing.Point(20, 56);
			this.chkSuspended.Name = "chkSuspended";
			this.chkSuspended.Size = new System.Drawing.Size(110, 24);
			this.chkSuspended.TabIndex = 34;
			this.chkSuspended.Text = "Suspended";
			// 
			// chkActive
			// 
			this.chkActive.AutoSize = false;
			this.chkActive.Enabled = false;
			this.chkActive.Location = new System.Drawing.Point(20, 20);
			this.chkActive.Name = "chkActive";
			this.chkActive.Size = new System.Drawing.Size(84, 24);
			this.chkActive.TabIndex = 33;
			this.chkActive.Text = "Active";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print / Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnPrint
			// 
			this.btnPrint.AppearanceKey = "acceptButton";
			this.btnPrint.Location = new System.Drawing.Point(344, 30);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnPrint.Size = new System.Drawing.Size(85, 48);
			this.btnPrint.TabIndex = 3;
			this.btnPrint.Text = "Print";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// frmVendorListSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(791, 593);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVendorListSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Listings";
			this.Load += new System.EventHandler(this.frmVendorListSetup_Load);
			this.Activated += new System.EventHandler(this.frmVendorListSetup_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendorListSetup_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraListSelection)).EndInit();
			this.fraListSelection.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
			this.fraInformation.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkInsuranceVerifieddate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVendorMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDataEntryMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDataEntryMessage)).EndInit();
			this.fraDataEntryMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkTaxID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYTDAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraZeroYTDAmount)).EndInit();
			this.fraZeroYTDAmount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkOnlyZeroAmounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraZeroAdjustments)).EndInit();
			this.fraZeroAdjustments.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraInsuranceRequired)).EndInit();
			this.fraInsuranceRequired.ResumeLayout(false);
			this.fraInsuranceRequired.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtInsuranceVerifiedDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSpecificClass)).EndInit();
			this.fraSpecificClass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraClass)).EndInit();
			this.fraClass.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).EndInit();
			this.fraStatus.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSuspended)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnPrint)).EndInit();
			this.ResumeLayout(false);
		}
        #endregion

        private FCButton btnPrint;
	}
}
