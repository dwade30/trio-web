﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class clsACHOriginatorInfo
	{
		//=========================================================
		private string strImmediateOriginName = "";
		private string strImmediateOriginRT = "";
		private string strOriginatingDFI = "";

		public string ImmediateOriginName
		{
			get
			{
				string ImmediateOriginName = "";
				ImmediateOriginName = strImmediateOriginName;
				return ImmediateOriginName;
			}
			set
			{
				strImmediateOriginName = value;
			}
		}

		public string ImmediateOriginRT
		{
			get
			{
				string ImmediateOriginRT = "";
				ImmediateOriginRT = strImmediateOriginRT;
				return ImmediateOriginRT;
			}
			set
			{
				strImmediateOriginRT = value;
			}
		}

		public string OriginatingDFI
		{
			get
			{
				string OriginatingDFI = "";
				OriginatingDFI = strOriginatingDFI;
				return OriginatingDFI;
			}
			set
			{
				strOriginatingDFI = value;
			}
		}
	}
}
