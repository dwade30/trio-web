﻿using System;
using System.Globalization;
using Wisej.Web;
using Global;
using TWSharedLibrary;
using fecherFoundation;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;

namespace TWBD0000
{
    public partial class frmEditAPJournal : BaseForm
    {
        public frmEditAPJournal()
        {
            InitializeComponent();
        }

        private cEditAPViewModel theView;

        public void InitializeScreen(ref cEditAPViewModel viewModel)
        {
            LoadPeriodCombo();
            theView = viewModel;
            theView.BanksChanged += theView_BanksChanged;
            theView.JournalChanged += theView_JournalChanged;
            RefreshScreen();
            theView.Cancelled = true;

            this.Show(FormShowEnum.Modal);
        }

        private void RefreshScreen()
        {
            LoadBankCombo();
            var theViewApJournalInfo = theView.APJournalInfo;
            FillJournalDetails(ref theViewApJournalInfo);
        }

        private void theView_BanksChanged()
        {
            LoadBankCombo();
        }

        private void theView_JournalChanged()
        {
            var theViewApJournalInfo = theView.APJournalInfo;
            FillJournalDetails(ref theViewApJournalInfo);
        }

        private void cmdCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void cmdOK_Click(object sender, System.EventArgs e)
        {
            Submit();
        }

        private void LoadBankCombo()
        {
            //cBank bank;
            cmbBanks.Clear();
            foreach (cBank bank in theView.Banks)
            {               
                cmbBanks.AddItem((bank.BankNumber.ToString() + "   ").Left(3) + " " + bank.Name);
                cmbBanks.ItemData(cmbBanks.ListCount - 1, bank.ID);
            }
        }

        private void LoadPeriodCombo()
        {
            short X;
            cmbPeriod.Clear();
            for (X = 1; X <= 12; X++)
            {
                cmbPeriod.AddItem(X.ToString());
                cmbPeriod.ItemData(cmbPeriod.ListCount - 1, X);
            }
        }

        private void Submit()
        {
            if (SaveJournalInfo())
            {
                theView.Cancelled = false;
                Close();
            }
        }

        private bool SaveJournalInfo()
        {
            bool SaveJournalInfo = false;
            if (ValidateData())
            {
                UpdateViewModel();
                theView.Save();
                SaveJournalInfo = true;
            }
            return SaveJournalInfo;
        }

        private void UpdateViewModel()
        {
            theView.APJournalInfo.BankID = GetSelectedBank();
            theView.APJournalInfo.Description = txtDescription.Text;
            theView.APJournalInfo.Period = GetSelectedPeriod();
            theView.APJournalInfo.PayableDate = GetPayableDate();
        }

        private int GetSelectedBank()
        {
            int GetSelectedBank = 0;
            if (cmbBanks.SelectedIndex < 0)
            {
                GetSelectedBank = 0;
                return GetSelectedBank;
            }
            GetSelectedBank = cmbBanks.ItemData(cmbBanks.SelectedIndex);
            return GetSelectedBank;
        }

        private short GetSelectedPeriod()
        {
            short GetSelectedPeriod = 0;
            if (cmbPeriod.SelectedIndex < 0)
            {
                GetSelectedPeriod = 0;
                return GetSelectedPeriod;
            }
            GetSelectedPeriod = Convert.ToInt16(cmbPeriod.ItemData(cmbPeriod.SelectedIndex));
            return GetSelectedPeriod;
        }

        private string GetPayableDate()
        {
            if (t2kPayableDate.Text.IsDate())
            {
                return t2kPayableDate.Text;
            }
            return "";
        }

        private bool ValidateData()
        {            
            if (String.IsNullOrWhiteSpace(txtDescription.Text.Trim()))
            {
                MessageBox.Show("You must enter a valid description", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            if (GetSelectedBank() <= 0)
            {
                MessageBox.Show("You must select a bank", "Invalid Bank", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            if (GetSelectedPeriod() <= 0)
            {
                MessageBox.Show("You must select a period", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            if (!t2kPayableDate.Text.IsDate())
            {
                MessageBox.Show("You must enter a payable date", "Invalid Payable Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
            return true;
        }

        private void FillJournalDetails(ref cAPJournalMasterInfo apJournMasterInfo)
        {
            if (apJournMasterInfo != null)
            {
                txtDescription.Text = apJournMasterInfo.Description;
                SetPeriodCombo(apJournMasterInfo.Period);
                SetBankCombo(apJournMasterInfo.BankID);
                if (apJournMasterInfo.PayableDate.IsDate())
                {
                    t2kPayableDate.Text = DateTime.Parse(apJournMasterInfo.PayableDate).ToString("MM/dd/yyyy");
                }
                else
                {
                    t2kPayableDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                }

                ShowJournalNumber(apJournMasterInfo.JournalNumber);
            }

            SetControlsEditableStatuses();
            SetControlsVisibilities();
        }

        private void SetPeriodCombo(int intPeriod)
        {
            short X;
            for (X = 0; X <= cmbPeriod.Items.Count - 1; X++)
            {
                if (cmbPeriod.ItemData(X) == intPeriod)
                {
                    cmbPeriod.SelectedIndex = X;
                    return;
                }
            } // X
            cmbPeriod.SelectedIndex = -1;
        }
        private void SetBankCombo(int lngBankID)
        {
            int X;
            if (lngBankID == 0)
            {
                lngBankID = Convert.ToInt32(modBudgetaryAccounting.GetBankVariable("APBank"));
            }
            for (X = 0; X <= cmbBanks.Items.Count - 1; X++)
            {
                if (cmbBanks.ItemData(X) == lngBankID)
                {
                    cmbBanks.SelectedIndex = X;
                    return;
                }
            } // X
            cmbBanks.SelectedIndex = -1;
        }

        private void SetControlsVisibilities()
        {
            lblBank.Visible = theView.BankIsVisible;
            cmbBanks.Visible = lblBank.Visible;
        }

        private void ShowJournalNumber(int lngJournalNumber)
        {
            if (lngJournalNumber > 0)
            {
                this.Text = "Update Invoice Journal";
                HeaderText.Text = "Invoice Journal " + lngJournalNumber.ToString();
            }
            else
            {
                this.Text = "Create New Invoice Journal";
                HeaderText.Text = "New Invoice Journal";
            }
        }

        private void SetControlsEditableStatuses()
        {
            cmbBanks.Enabled = theView.BankIsEditable;
            cmbPeriod.Enabled = theView.PeriodIsEditable;
            t2kPayableDate.Enabled = theView.PayableDateIsEditable;
        }
    }
}
