﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for rptVendorDetail.
    /// </summary>
    public partial class rptVendorDetail : BaseSectionReport
    {
        private const string ContD = " CONT'D";
        private const string TO = " to ";
        private const string DoubleSpace = "  ";
        private const string SingleSpace = " ";
        private const string UNKNOWN = " UNKNOWN";
        private const string STANDARD_CURRENCY_FORMAT = "#,##0.00";

        public static rptVendorDetail InstancePtr
        {
            get
            {
                return (rptVendorDetail)Sys.GetInstance(typeof(rptVendorDetail));
            }
        }

        protected rptVendorDetail _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsVendorInfo.Dispose();
                rsAPJournalInfo.Dispose();
                rsCDJournalInfo.Dispose();
                rsVendorNames.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	rptVendorDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        //=========================================================
        int PageCounter;
        bool blnFirstRecord;
        string strDateRange = "";
        // reporting range A - All, M - Month Range, S - Single Month
        string strAcctRange = "";
        int intLowDate;
        int intHighDate;
        DateTime datStart;
        DateTime datEnd;
        public string strReportType = "";
        clsDRWrapper rsVendorInfo = new clsDRWrapper();
        clsDRWrapper rsAPJournalInfo = new clsDRWrapper();
        clsDRWrapper rsCDJournalInfo = new clsDRWrapper();
        string strLowAccount = "";
        string strHighAccount = "";
        // vbPorter upgrade warning: curDebitsTotal As Decimal	OnWrite(short, Decimal)
        Decimal curDebitsTotal;
        // vbPorter upgrade warning: curCreditsTotal As Decimal	OnWrite(short, Decimal)
        Decimal curCreditsTotal;
        string strJournalType = "";
        string strDateSQL = "";
        string strAPDateSQL = "";
        string strEndDateSQL = "";
        string strOtherDateSQL = "";
        string strAccountSQL = "";
        clsDRWrapper rsVendorNames = new clsDRWrapper();
        // vbPorter upgrade warning: curGrandTotal As Decimal	OnWrite(Decimal, short)
        public Decimal curGrandTotal;
        int intVendorCount;
        bool blnShowOSEncumbrances;
        string strEncSearchString = "";
        bool blnEncTempVendor;
        bool blnChangeVendor;
        bool blnNoData;
        public bool blnFromAP;
        public int lngVendor;
        public string strVendor = "";
        private string vendorName;
        private string DateFormat;
        private string vendorAccountNumber;

        public rptVendorDetail()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            Name = "Vendor Detail";
        }

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
        {
            // set up binder values for groups in report
            Fields.Add("VendorBinder");
            Fields.Add("GroupTitle");
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            var groupTitle = Fields["GroupTitle"].Value?.ToString();

            var vendorNumber = rsVendorInfo.Get_Fields_Int32("VendorNumber");

            vendorAccountNumber = rsVendorInfo.Get_Fields("AcctNumber").ToString();
            var acctNumber = vendorAccountNumber;

            var vendorName = rsVendorInfo.Get_Fields_String("VendorName");

            var formattedVendorNumber = modValidateAccount.GetFormat_6(vendorNumber, 5);

            if (blnFirstRecord)
            {
                blnFirstRecord = false;
                intVendorCount += 1;
            CheckJournals:
                GetSql();
                if (IsAcctReport())
                {
                    if (HasVendorNumber(rsVendorInfo))
                    {
                        blnEncTempVendor = false;
                        strEncSearchString = FCConvert.ToString(vendorNumber);
                    }
                    else
                    {
                        blnEncTempVendor = true;
                        strEncSearchString = FCConvert.ToString(vendorName);
                    }
                }
                else
                {
                    strEncSearchString = vendorAccountNumber;
                }

                if ((rsAPJournalInfo.EndOfFile() != true && rsAPJournalInfo.BeginningOfFile() != true) || (rsCDJournalInfo.EndOfFile() != true && rsCDJournalInfo.BeginningOfFile() != true))
                {
                    strJournalType = SelectJournalType();
                    if (IsAcctReport())
                    {
                        Fields["VendorBinder"].Value = vendorNumber + vendorName;
                    }
                    else
                    {
                        Fields["VendorBinder"].Value = acctNumber;
                    }
                    // check to see if we are reporting on a new vendor
                    blnChangeVendor = false;
                    if (Fields["GroupTitle"].Value == null)
                    {
                        Fields["GroupTitle"].Value = string.Empty;
                    }
                    if (IsVendorReport())
                    {

                        blnChangeVendor = Strings.Left(groupTitle, (acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber)).Length)
                                          != acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber);
                    }
                    else
                    {
                        blnChangeVendor = Strings.Left(groupTitle, (formattedVendorNumber + DoubleSpace + vendorName).Length)
                                          != formattedVendorNumber + DoubleSpace + vendorName;
                    }
                    // if this is a new department/division we are reporting on not just a repeating group header at the top of the page then get the new department title
                    if (blnChangeVendor)
                    {
                        if (IsVendorReport())
                        {

                            Fields["GroupTitle"].Value = acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber);
                        }
                        else
                        {
                            Fields["GroupTitle"].Value = formattedVendorNumber + DoubleSpace + vendorName;
                        }
                    }
                    else
                    {
                        if (NeedsContinuationTitle(groupTitle))
                        {
                            Fields["GroupTitle"].Value = groupTitle + ContD;
                        }
                        if (lblVendor.Text != groupTitle)
                        {
                            lblVendor.Text = groupTitle;
                        }
                        if (NeedsContinuationTitle(lblVendor.Text))
                        {
                            lblVendor.Text += ContD;
                        }
                    }
                    eArgs.EOF = false;
                }
                else
                {
                    rsVendorInfo.MoveNext();
                    if (rsVendorInfo.EndOfFile())
                    {
                        eArgs.EOF = true;
                        blnNoData = true;
                        MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Cancel();
                    }
                    else
                    {
                        goto CheckJournals;
                    }
                }
            }
            else
            {
                CheckJournals2:
                
                if (IsAcctReport())
                {
                    
                    if (String.Compare(FCConvert.ToString(Fields["VendorBinder"].Value), FCConvert.ToString(vendorNumber + vendorName)) != 0)
                    {
                        intVendorCount += 1;
                    }
                }
                if (NotAtBeginningOrEnd(rsAPJournalInfo) || NotAtBeginningOrEnd(rsCDJournalInfo))
                {
                    strJournalType = SelectJournalType();
                    if (IsAcctReport())
                    {
                        Fields["VendorBinder"].Value = vendorNumber + vendorName;
                    }
                    else
                    {
                        Fields["VendorBinder"].Value = acctNumber;
                    }
                    // check to see if we are reporting on a new vendor
                    blnChangeVendor = false;
                    if (IsVendorReport())
                    {
                        blnChangeVendor = Strings.Left(groupTitle, (acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber)).Length)
                                          != acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber);
                    }
                    else
                    {
                        blnChangeVendor = Strings.Left(groupTitle, (formattedVendorNumber + DoubleSpace + vendorName).Length)
                                          != formattedVendorNumber + DoubleSpace + vendorName;
                    }
                    // if this is a new department/division we are reporting on not just a repeating group header at the top of the page then get the new department title
                    if (blnChangeVendor)
                    {
                        if (IsVendorReport())
                        {
                            Fields["GroupTitle"].Value = acctNumber + DoubleSpace + modAccountTitle.ReturnAccountDescription(acctNumber);
                        }
                        else
                        {
                            Fields["GroupTitle"].Value = formattedVendorNumber + DoubleSpace + vendorName;
                        }
                    }
                    else
                    {
                        if (Strings.Right(groupTitle, 6) != ContD)
                        {
                            Fields["GroupTitle"].Value = Fields["GroupTitle"].Value + SingleSpace + ContD;
                        }
                        if (lblVendor.Text != groupTitle)
                        {
                            lblVendor.Text = groupTitle;
                        }
                        if (Strings.Right(lblVendor.Text, 6) != ContD)
                        {
                            lblVendor.Text += SingleSpace + ContD;
                        }
                    }
                    eArgs.EOF = false;
                }
                else
                {
                    if (IsAcctReport())
                    {
                        if (HasVendorNumber(rsVendorInfo))
                        {
                            blnEncTempVendor = false;
                            strEncSearchString = FCConvert.ToString(vendorNumber);
                        }
                        else
                        {
                            blnEncTempVendor = true;
                            strEncSearchString = FCConvert.ToString(vendorName);
                        }
                    }
                    else
                    {
                        strEncSearchString = FCConvert.ToString(acctNumber);
                    }
                    rsVendorInfo.MoveNext();
                    if (rsVendorInfo.EndOfFile())
                    {
                        eArgs.EOF = true;
                    }
                    else
                    {
	                    vendorNumber = rsVendorInfo.Get_Fields_Int32("VendorNumber");
						vendorAccountNumber = rsVendorInfo.Get_Fields("AcctNumber").ToString();
	                    acctNumber = vendorAccountNumber;
						vendorName = rsVendorInfo.Get_Fields_String("VendorName");
						formattedVendorNumber = modValidateAccount.GetFormat_6(vendorNumber, 5);

						GetSql();
                        goto CheckJournals2;
                    }
                }
            }
        }

        private bool HasVendorNumber(clsDRWrapper rs)
        {
            return rs.Get_Fields_Int32("VendorNumber") != 0;
        }

        private bool NeedsContinuationTitle(string title)
        {
            return !title.EndsWith(ContD);
        }

        private bool NotAtBeginningOrEnd(clsDRWrapper recordset)
        {
            return recordset.EndOfFile() != true && recordset.BeginningOfFile() != true;
        }

        private bool IsVendorReport()
        {
            return FCConvert.ToInt16(strReportType) == 4 || FCConvert.ToInt16(strReportType) == 5;
        }

        private bool IsAcctReport()
        {
            return !IsVendorReport();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            PageCounter = 0;
            blnNoData = false;
            Label2.Text = modGlobalConstants.Statics.MuniName;
            DateFormat = "MM/dd/yyyy";
            Label3.Text = Strings.Format(DateTime.Today, DateFormat);
            Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
            blnFirstRecord = true;

            switch (strDateRange)
            {
                case "S":
                    lblDateRange.Text = MonthCalc(intLowDate);

                    break;
                case "M":
                    lblDateRange.Text = MonthCalc(intLowDate) + TO + MonthCalc(intHighDate);
                    break;
                case "D":
                    lblDateRange.Text = Strings.Format(datStart, DateFormat) + TO + Strings.Format(datEnd, DateFormat);
                    break;
                default:
                    lblDateRange.Text = "ALL Months";
                    break;
            }


            if (IsVendorReport())
            {
                switch (strAcctRange)
                {
                    case "S":
                        lblAccountRange.Text = strLowAccount;

                        break;
                    case "R":
                        lblAccountRange.Text = strLowAccount + TO + strHighAccount;

                        break;
                    default:
                        lblAccountRange.Text = "ALL Accounts";

                        break;
                }
            }
            else
            {
                lblAccountRange.Text = FCConvert.ToDouble(strReportType) == 1 ? "Single Vendor" : "All Vendors";
            }

            if (IsVendorReport())
            {
                rsVendorNames.OpenRecordset("SELECT * FROM VendorMaster");
                lblAccount.Text = "Vendor";
            }

            intVendorCount = 0;
            curGrandTotal = 0;
            curDebitsTotal = 0;
            curCreditsTotal = 0;
            if (rsVendorInfo.EndOfFile() || rsVendorInfo.BeginningOfFile())
            {
                MessageBox.Show("No info found for this vendor", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel();
            }
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {

            if (blnFromAP) return;

            frmVendorDetail.InstancePtr.blnVendorSelected = false;
            frmVendorDetail.InstancePtr.blnAccountSelected = false;
            frmVendorDetail.InstancePtr.Show(App.MainForm);

            frmVendorDetail.InstancePtr.Focus();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (strJournalType == "A")
            {
                GetAPData();
                rsAPJournalInfo.MoveNext();
            }
            else
            {
                GetOtherData();
                rsCDJournalInfo.MoveNext();
            }
        }

        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            using (clsDRWrapper rsEncJournalInfo = new clsDRWrapper())
            {
                fldTotal.Text = Strings.Format(curDebitsTotal - curCreditsTotal, STANDARD_CURRENCY_FORMAT);
                if (curDebitsTotal == 0 || curCreditsTotal == 0)
                {
                    fldDebits.Text = "";
                    fldCredits.Text = "";
                    lblDebits.Text = "";
                    lblCredits.Text = "";
                    fldDebits.Visible = false;
                    fldCredits.Visible = false;
                    lblCredits.Visible = false;
                    lblDebits.Visible = false;
                    srptOutstandingEncumbrances.Top = fldTotal.Top + fldTotal.Height + 50 / 1440f;
                }
                else
                {
                    lblDebits.Text = "Debits";
                    lblCredits.Text = "Credits";
                    fldDebits.Text = Strings.Format(curDebitsTotal, STANDARD_CURRENCY_FORMAT);
                    fldCredits.Text = Strings.Format(curCreditsTotal, STANDARD_CURRENCY_FORMAT);
                    fldDebits.Visible = true;
                    fldCredits.Visible = true;
                    lblCredits.Visible = true;
                    lblDebits.Visible = true;
                    srptOutstandingEncumbrances.Top = fldCredits.Top + fldCredits.Height + 50 / 1440f;
                }

                curGrandTotal += curDebitsTotal - curCreditsTotal;
                curDebitsTotal = 0;
                curCreditsTotal = 0;
                if (blnShowOSEncumbrances)
                {
                    rsEncJournalInfo.OpenRecordset(GetEncumbranceSql(strEncSearchString, blnEncTempVendor));

                    if (rsEncJournalInfo.EndOfFile() != true && rsEncJournalInfo.BeginningOfFile() != true)
                    {
                        srptOutstandingEncumbrances.Report = srptOutstandingVendorEncumbrances.InstancePtr;
                        srptOutstandingEncumbrances.Visible = true;
                    }
                    else
                    {
                        srptOutstandingEncumbrances.Visible = false;
                    }
                }
                else
                {
                    srptOutstandingEncumbrances.Visible = false;
                }
            }
        }

        private void GroupFooter2_Format(object sender, EventArgs e)
        {
            fldGrandTotal.Text = Strings.Format(curGrandTotal, STANDARD_CURRENCY_FORMAT);
            if (IsVendorReport())
            {
                lblVendorCount.Visible = false;
                fldVendorCount.Visible = false;
            }
            else
            {
                lblVendorCount.Visible = true;
                fldVendorCount.Visible = true;
                fldVendorCount.Text = intVendorCount.ToString();
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            PageCounter += 1;
            Label4.Text = "Page " + FCConvert.ToString(PageCounter);
        }

        public void Init(string strType, string strAccountRange, string strMonthRange, bool ShowEnc, bool modalDialog, short intLowMonth = 0, short intHighMonth = 0, DateTime? datStartDateTemp = null, DateTime? datEndDateTemp = null)
        {
            //FC:FINAL:PJ initialize Date-Values correctly
            DateTime datStartDate = datStartDateTemp ?? FCConvert.ToDateTime("1/1/1899");
            DateTime datEndDate = datEndDateTemp ?? FCConvert.ToDateTime("1/1/1899");

            string strPeriodCheck = "";
            string strEndDateSQL = "";

           // clsDRWrapper rsTemp = new clsDRWrapper();
            string strVendorInfo = "";
            string strTempVendorInfo = "";
            string strEncVendorInfo = "";
            string strEncTempVendorInfo = "";
            string strOtherVendorInfo = "";
            string strAccountInfo = "";
            string strEncAccountInfo = "";
            string strOtherAccountInfo = "";
            strReportType = strType;
            strDateRange = strMonthRange;
            strAcctRange = strAccountRange;
            intLowDate = intLowMonth;
            intHighDate = intHighMonth;
            datStart = datStartDate;
            datEnd = datEndDate;
            blnShowOSEncumbrances = ShowEnc;
            strPeriodCheck = intHighDate < intLowDate ? "OR" : "AND";
            var lowDate = FCConvert.ToString(intLowDate);
            var highDate = FCConvert.ToString(intHighDate);
            var startDate = FCConvert.ToString(datStart);
            var endDate = FCConvert.ToString(datEnd);

            switch (strDateRange)
            {
                case "A":
                    strAPDateSQL = SingleSpace;
                    strDateSQL = SingleSpace;
                    break;
                case "M":
                    strAPDateSQL = " AND (APJournal.Period >= " + lowDate + SingleSpace + strPeriodCheck + " APJournal.Period <= " + highDate + ") ";
                    strDateSQL = " AND (Period >= " + lowDate + SingleSpace + strPeriodCheck + " Period <= " + highDate + ") ";
                    break;
                case "D":
                    strAPDateSQL = " AND (APJournal.CheckDate >= '" + startDate + "' AND APJournal.CheckDate <= '" + endDate + "') ";
                    strEndDateSQL = " AND (EncumbrancesDate >= '" + startDate + "' AND EncumbrancesDate <= '" + endDate + "') ";
                    strOtherDateSQL = " AND (JournalEntriesDate >= '" + startDate + "' AND JournalEntriesDate <= '" + endDate + "') ";
                    break;
                default:
                    strAPDateSQL = " AND APJournal.Period = " + lowDate + SingleSpace;
                    strDateSQL = " AND Period = " + lowDate + SingleSpace;
                    break;
            }

            if (IsVendorReport())
            {
                switch (strAcctRange)
                {
                    case "A":
                        strAccountSQL = SingleSpace;

                        break;
                    case "R":

                        strLowAccount = frmVendorDetail.InstancePtr.vsLowAccount.Text;
                        strHighAccount = frmVendorDetail.InstancePtr.vsHighAccount.Text;
                        strAccountSQL = " AND (Account >= '" + strLowAccount + "' AND Account <= '" + strHighAccount + "') ";

                        break;
                    default:

                        strLowAccount = frmVendorDetail.InstancePtr.vsSingleAccount.Text;
                        strHighAccount = frmVendorDetail.InstancePtr.vsSingleAccount.Text;
                        strAccountSQL = " AND Account = '" + strLowAccount + "' ";

                        break;
                }
            }
            else
            {
                strAccountSQL = SingleSpace;
            }

            if (IsAcctReport())
            {
                switch (strReportType)
                {
                    case "2":
                    case "3":
                        if (IsDateRange())
                        {
                            strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE  APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' AND APJournal.VendorNumber > 0" + strAPDateSQL;
                            strTempVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE Status = 'P' AND Description <> 'Control Entries' AND VendorNumber = 0" + strAPDateSQL;
                            strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.Description <> 'Control Entries' AND Encumbrances.VendorNumber > 0" + strEndDateSQL;
                            strEncTempVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND VendorNumber = 0" + strEndDateSQL;
                            strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND RCB <> 'L' AND JournalEntries.VendorNumber > 0" + strOtherDateSQL + "AND JournalEntries.Type = 'D'";
                        }
                        else
                        {
                            strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE  APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' AND APJournal.VendorNumber > 0" + strDateSQL;
                            strTempVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE Status = 'P' AND Description <> 'Control Entries' AND VendorNumber = 0" + strDateSQL;
                            strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.Description <> 'Control Entries' AND Encumbrances.VendorNumber > 0" + strDateSQL;
                            strEncTempVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM Encumbrances WHERE Status = 'P' AND Description <> 'Control Entries' AND VendorNumber = 0" + strDateSQL;
                            strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND RCB <> 'L' AND JournalEntries.VendorNumber > 0" + strDateSQL + "AND JournalEntries.Type = 'D'";
                        }
                        break;

                    case "6":
                        if (IsDateRange())
                        {
                            strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' AND APJournal.VendorNumber > 0" + strAPDateSQL;
                            strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND Encumbrances.Status = 'P' AND Encumbrances.Description <> 'Control Entries' AND Encumbrances.VendorNumber > 0" + strEndDateSQL;
                            strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND JournalEntries.Status = 'P' AND RCB <> 'L' AND JournalEntries.VendorNumber > 0" + strOtherDateSQL + "AND JournalEntries.Type = 'D'";
                        }
                        else
                        {
                            strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND APJournal.Status = 'P' AND APJournal.Description <> 'Control Entries' AND APJournal.VendorNumber > 0" + strDateSQL;
                            strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND Encumbrances.Status = 'P' AND Encumbrances.Description <> 'Control Entries' AND Encumbrances.VendorNumber > 0" + strDateSQL;
                            strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.[1099] = 1 AND JournalEntries.Status = 'P' AND RCB <> 'L' AND JournalEntries.VendorNumber > 0" + strDateSQL + "AND JournalEntries.Type = 'D'";
                        }

                        break;

                    default:

                        var vendorNumber = blnFromAP ? 0 : Convert.ToInt32(frmVendorDetail.InstancePtr.cboVendors.Text.Substring(0, 5));
                        var tempVendorNumber = blnFromAP ? "" : modCustomReport.FixQuotes(Strings.Trim(Strings.Right(frmVendorDetail.InstancePtr.cboVendors.Text, frmVendorDetail.InstancePtr.cboVendors.Text.Length - 4)));

                        if (IsDateRange())
                        {
                            if (vendorNumber == 0)
                            {
                                strVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE Status = 'P' AND (VendorNumber = " + vendorNumber + " AND TempVendorName = '" + tempVendorNumber + "')" + strAPDateSQL;
                                strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = " + vendorNumber + strEndDateSQL;
                                strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = " + vendorNumber + strOtherDateSQL;
                            }
                            else
                            {
                                strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE APJournal.Status = 'P' AND APJournal.VendorNumber = " + vendorNumber + strAPDateSQL;
                                strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = " + vendorNumber + strEndDateSQL;
                                strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = " + vendorNumber + strOtherDateSQL;
                            }
                        }
                        else
                        {
                            if (blnFromAP)
                            {
                                if (lngVendor == 0)
                                {
                                    strVendorInfo = $"SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE Status = 'P' AND (VendorNumber = 0 AND TempVendorName = '{modCustomReport.FixQuotes(Strings.Trim(strVendor))}'){strDateSQL}";
                                    strEncVendorInfo = "SELECT DISTINCT VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = 0" + strDateSQL;
                                    strOtherVendorInfo = "SELECT DISTINCT VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = 0" + strDateSQL;
                                }
                                else
                                {
                                    strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE APJournal.Status = 'P' AND APJournal.VendorNumber = " + FCConvert.ToString(lngVendor) + strDateSQL;
                                    strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = " + FCConvert.ToString(lngVendor) + strDateSQL;
                                    strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = " + FCConvert.ToString(lngVendor) + strDateSQL;
                                }
                            }
                            else
                            {
                                if (vendorNumber == 0)
                                {
                                    strVendorInfo = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE Status = 'P' AND (VendorNumber = 0 AND TempVendorName = '" + tempVendorNumber + "')" + strDateSQL;
                                    strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = 0" + strDateSQL;
                                    strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = 0" + strDateSQL;
                                }
                                else
                                {
                                    strVendorInfo = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE APJournal.Status = 'P' AND APJournal.VendorNumber = " + vendorNumber + strDateSQL;
                                    strEncVendorInfo = "SELECT DISTINCT Encumbrances.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (Encumbrances INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE Encumbrances.Status = 'P' AND Encumbrances.VendorNumber = " + vendorNumber + strDateSQL;
                                    strOtherVendorInfo = "SELECT DISTINCT JournalEntries.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (JournalEntries INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber) WHERE JournalEntries.Status = 'P' AND JournalEntries.VendorNumber = " + vendorNumber + strDateSQL;
                                }
                            }
                        }

                        break;
                }

                switch (strReportType)
                {
                    case "2":
                        rsVendorInfo.OpenRecordset("SELECT DISTINCT VendorNumber, VendorName FROM (SELECT * FROM (" + strVendorInfo + ") as VendorInfo UNION ALL SELECT * FROM (" + strOtherVendorInfo + ") as OtherVendorInfo UNION ALL SELECT * FROM (" + strTempVendorInfo + ") as TempVendorInfo UNION ALL SELECT * FROM (" + strEncVendorInfo + ") as EncVendorInfo UNION ALL SELECT * FROM (" + strEncTempVendorInfo + ") as EncTempVendorInfo) as temp ORDER BY VendorName");
                        break;

                    case "1":
                    case "6":
                        rsVendorInfo.OpenRecordset("SELECT DISTINCT VendorNumber, VendorName FROM (SELECT * FROM (" + strVendorInfo + ") as VendorInfo UNION ALL SELECT * FROM (" + strOtherVendorInfo + ") as OtherVendorInfo UNION ALL SELECT * FROM (" + strEncVendorInfo + ") as EncVendorInfo) as temp ORDER BY VendorName");
                        break;

                    default:
                        rsVendorInfo.OpenRecordset("SELECT DISTINCT VendorNumber, VendorName FROM (SELECT * FROM (" + strVendorInfo + ") as VendorInfo UNION ALL SELECT * FROM (" + strOtherVendorInfo + ") as OtherVendorInfo UNION ALL SELECT * FROM (" + strTempVendorInfo + ") as TempVendorInfo UNION ALL SELECT * FROM (" + strEncVendorInfo + ") as EncVendorInfo UNION ALL SELECT * FROM (" + strEncTempVendorInfo + ") as EncTempVendorInfo) as temp ORDER BY VendorNumber");
                        break;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(strDateSQL.Trim()) && string.IsNullOrEmpty(strAPDateSQL.Trim()) && string.IsNullOrEmpty(strOtherDateSQL.Trim()) && string.IsNullOrEmpty(strEndDateSQL.Trim()))
                {
                    if (string.IsNullOrEmpty(strAccountSQL.Trim()))
                    {
                        strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P'";
                        strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P'";
                        strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND JournalEntries.Type = 'D'";
                        rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                    }
                    else
                    {
                        var acctSql = Strings.Right(strAccountSQL, strAccountSQL.Length - 4);

                        strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P' AND" + acctSql;
                        strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P' AND" + acctSql;
                        strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND" + acctSql + "AND JournalEntries.Type = 'D'";
                        rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                    }
                }
                else
                {
                    if (IsDateRange())
                    {
                        var apDateSql = Strings.Right(strAPDateSQL, strAPDateSQL.Length - 4);
                        var endDateSql = Strings.Right(strEndDateSQL, FCConvert.ToString(strEndDateSQL).Length - 4);
                        var otherDateSql = Strings.Right(strOtherDateSQL, strOtherDateSQL.Length - 4);

                        if (string.IsNullOrEmpty(strAccountSQL.Trim()))
                        {
                            strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P' AND" + apDateSql;
                            strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P' AND" + endDateSql;
                            strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND" + otherDateSql + "AND JournalEntries.Type = 'D'";
                            rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                        }
                        else
                        {
                            strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P' AND" + apDateSql + strAccountSQL;
                            strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P' AND" + endDateSql + strAccountSQL;
                            strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND" + otherDateSql + strAccountSQL + "AND JournalEntries.Type = 'D'";
                            rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                        }
                    }
                    else
                    {
                        //FC:FINAL:SBE - #4354 - fix exception (introduced in SVN revision 10062)
                        var dateSQL = strDateSQL.Length > 4 ? Strings.Right(strDateSQL, strDateSQL.Length - 4) : strDateSQL;

                        if (string.IsNullOrEmpty(strAccountSQL.Trim()))
                        {
                            strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P' AND" + dateSQL;
                            strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P' AND" + dateSQL;
                            strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND" + dateSQL + "AND JournalEntries.Type = 'D'";
                            rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                        }
                        else
                        {
                            strAccountInfo = "SELECT DISTINCT APJournalDetail.Account AS AcctNumber FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) WHERE APJournal.Status = 'P' AND" + dateSQL + strAccountSQL;
                            strEncAccountInfo = "SELECT DISTINCT EncumbranceDetail.Account AS AcctNumber FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.Status = 'P' AND" + dateSQL + strAccountSQL;
                            strOtherAccountInfo = "SELECT DISTINCT JournalEntries.Account AS AcctNumber FROM JournalEntries WHERE JournalEntries.Status = 'P' AND" + dateSQL + strAccountSQL + "AND JournalEntries.Type = 'D'";
                            rsVendorInfo.OpenRecordset("SELECT DISTINCT AcctNumber FROM (SELECT * FROM (" + strAccountInfo + ") as AccountInfo UNION ALL SELECT * FROM (" + strOtherAccountInfo + ") as OtherAccountInfo UNION ALL SELECT * FROM (" + strEncAccountInfo + ") as EncAccountInfo) as temp ORDER BY AcctNumber");
                        }
                    }
                }
            }

            // depending on what the user selected print the report or preview it
            if (blnFromAP)
            {
                frmReportViewer.InstancePtr.Init(this, string.Empty, 1, showModal: modalDialog);
            }
            else
            {
                if (frmVendorDetail.InstancePtr.blnPrint)
                    rptVendorDetail.InstancePtr.PrintReport();
                else
                    frmReportViewer.InstancePtr.Init(this);
            }
        }

        private bool IsDateRange()
        {
            return strDateRange == "D";
        }

        private static string MonthCalc(int x)
        {
            string monthCalc;
            switch (x)
            {
                case 1:
                    monthCalc = "January";

                    break;

                case 2:
                    monthCalc = "February";

                    break;

                case 3:
                    monthCalc = "March";

                    break;

                case 4:
                    monthCalc = "April";

                    break;

                case 5:
                    monthCalc = "May";

                    break;

                case 6:
                    monthCalc = "June";

                    break;

                case 7:
                    monthCalc = "July";

                    break;

                case 8:
                    monthCalc = "August";

                    break;

                case 9:
                    monthCalc = "September";

                    break;

                case 10:
                    monthCalc = "October";

                    break;

                case 11:
                    monthCalc = "November";

                    break;

                case 12:
                    monthCalc = "December";

                    break;
                default:
                    monthCalc = "";

                    break;
            }
            return monthCalc;
        }

        private void GetSql()
        {
            string strAPJournalInfo;
            string strOtherJournalInfo;

            var vendorNumber = rsVendorInfo.Get_Fields_Int32("VendorNumber");
            vendorName = modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")).ToUpper();

            if (IsAcctReport())
            {
                if (vendorNumber == 0)
                {
                    if (IsDateRange())
                    {
                        strAPJournalInfo = AcctRpt_ApJournalInfoSql_Is0_IsDaily(vendorNumber);
                        strOtherJournalInfo = AcctRpt_OtherJournalInfoSql_Is0_IsDaily(vendorNumber);
                    }
                    else
                    {
                        strAPJournalInfo = AcctRpt_ApJournalInfoSql_Is0_NotDaily(vendorNumber);
                        strOtherJournalInfo = AcctRpt_OtherJournalInfoSql_Is0_NotDaily(vendorNumber);
                    }
                }
                else
                {
                    if (IsDateRange())
                    {
                        strAPJournalInfo = AcctRpt_ApJournalInfoSql_Not0_IsDaily(vendorNumber);
                        strOtherJournalInfo = AcctRpt_OtherJournalInfoSql_Not0_IsDaily(vendorNumber);
                    }
                    else
                    {
                        strAPJournalInfo = AcctRpt_ApJournalInfoSql_Not0_NotDaily(vendorNumber);
                        strOtherJournalInfo = AcctRpt_OtherJournalInfoSql_Not0_NotDaily(vendorNumber);
                    }
                }
            }
            else
            {
                if (strReportType == "4")
                {
                    if (IsDateRange())
                    {
                        strAPJournalInfo = NonAcctRpt_ApJournalInfoSql_Is4_IsDaily();
                        strOtherJournalInfo = NonAcctRpt_OtherJournalInfoSql_Is4_IsDaily();
                    }
                    else
                    {
                        strAPJournalInfo = NonAcctRpt_ApJournalInfoSql_Is4_NotDaily();
                        strOtherJournalInfo = NonAcctRpt_OtherJournalInfoSql_Is4_NotDaily();
                    }
                }
                else
                {
                    string strTempVendorAPJournalInfo;
                    string strNonTempVendorAPJournalInfo;

                    if (IsDateRange())
                    {
                        strTempVendorAPJournalInfo = NonAcctRpt_TempVendorApJournalInfoSql_Not4_IsDaily();
                        strNonTempVendorAPJournalInfo = NonAcctRpt_NonTempVendorApJournalInfoSql_Not4_IsDaily();

                        //
                        strAPJournalInfo = NonAcctRpt_ApJournalInfoSql_Not4_IsDaily(strTempVendorAPJournalInfo, strNonTempVendorAPJournalInfo);
                        strOtherJournalInfo = NonAcctRpt_OtherJournalInfoSql_Not4_IsDaily();
                    }
                    else
                    {
                        strTempVendorAPJournalInfo = NonAcctRpt_TempVendorApJournalInfoSql_Not4_NotDaily();
                        strNonTempVendorAPJournalInfo = NonAcctRpt_NonTempVendorApJournalInfoSql_Not4_NotDaily();

                        //
                        strAPJournalInfo = NonAcctRpt_ApJournalInfoSql_Not4_NotDaily(strTempVendorAPJournalInfo, strNonTempVendorAPJournalInfo);
                        strOtherJournalInfo = NonAcctRpt_OtherJournalInfoSql_Not4_NotDaily();
                    }
                }
            }

            if (strReportType == "5")
            {
                rsAPJournalInfo.OpenRecordset("SELECT * FROM (" + strAPJournalInfo + ") as APJournalInfo ORDER BY TempVendorName, PostedDate, JournalNumber");
                rsCDJournalInfo.OpenRecordset("SELECT * FROM (" + strOtherJournalInfo + ") as OtherJournalInfo  ORDER BY TempVendorName, PostedDate, JournalNumber");
            }
            else
            {
                rsAPJournalInfo.OpenRecordset("SELECT * FROM (" + strAPJournalInfo + ") as APJournalInfo ORDER BY PostedDate, JournalNumber");
                rsCDJournalInfo.OpenRecordset("SELECT * FROM (" + strOtherJournalInfo + ") as OtherJournalInfo ORDER BY PostedDate, JournalNumber");
            }
        }

        #region SQL statements
        #region AcctRpt
        private string AcctRpt_OtherJournalInfoSql_Is0_IsDaily(int vendorNumber)
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, Account, PO as Invoice, Description, Amount, RCB, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND VendorNumber = " + vendorNumber
                   + strOtherDateSQL;
        }

        private string AcctRpt_ApJournalInfoSql_Is0_IsDaily(int vendorNumber)
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournalDetail.Account as Account, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                   + "AND APJournal.Period = JournalMaster.Period "
                   + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournal.TempVendorName = '" + vendorName + "' "
                   + "AND APJournal.VendorNumber = " + vendorNumber
                   + strAPDateSQL;
        }

        private string AcctRpt_OtherJournalInfoSql_Is0_NotDaily(int vendorNumber)
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, Account, PO as Invoice, Description, Amount, RCB, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND VendorNumber = " + vendorNumber
                   + strDateSQL;
        }

        private string AcctRpt_ApJournalInfoSql_Is0_NotDaily(int vendorNumber)
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournalDetail.Account as Account, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                   + "AND APJournal.Period = JournalMaster.Period "
                   + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND Upper(APJournal.TempVendorName) = '" + vendorName + "' AND APJournal.VendorNumber = " + vendorNumber
                   + strAPDateSQL;
        }

        private string AcctRpt_OtherJournalInfoSql_Not0_IsDaily(int vendorNumber)
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, Account, PO as Invoice, Description, Amount, RCB, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' AND Type = 'D' AND VendorNumber = " + vendorNumber
                   + strOtherDateSQL;
        }

        private string AcctRpt_ApJournalInfoSql_Not0_IsDaily(int vendorNumber)
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournalDetail.Account as Account, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber AND APJournal.Period = JournalMaster.Period "
                   + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournal.VendorNumber = " + vendorNumber
                   + strAPDateSQL;
        }

        private string AcctRpt_OtherJournalInfoSql_Not0_NotDaily(int vendorNumber)
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, Account, PO as Invoice, Description, Amount, RCB, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' "
                   + "AND Type = 'D' "
                   + "AND VendorNumber = " + vendorNumber
                   + strDateSQL;
        }

        private string AcctRpt_ApJournalInfoSql_Not0_NotDaily(int vendorNumber)
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournalDetail.Account as Account, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM ((APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster "
                   + "ON (APJournal.JournalNumber = JournalMaster.JournalNumber) "
                   + "AND (APJournal.Period = JournalMaster.Period) "
                   + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC')) "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0 AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + $"AND APJournal.VendorNumber = {vendorNumber}{strAPDateSQL}";
        }
        #endregion

        #region Non AcctRpt
        private string NonAcctRpt_OtherJournalInfoSql_Not4_IsDaily()
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, JournalEntries.VendorNumber, Description, Amount, JournalEntries.CheckNumber as CheckNumber, VendorMaster.CheckName as TempVendorName "
                   + "FROM JournalEntries "
                   + "INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber "
                   + "WHERE JournalEntries.Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND Account = '" + vendorAccountNumber + "'" + strOtherDateSQL;
        }

        private static string NonAcctRpt_ApJournalInfoSql_Not4_IsDaily(string tempVendorApJournalInfoSql, string nonTempVendorApJournalInfoSql)
        {
            return $"SELECT * FROM (SELECT * FROM ({tempVendorApJournalInfoSql}) as TempAPJournalInfo WHERE RCB <> 'L' UNION ALL SELECT * FROM ({nonTempVendorApJournalInfoSql}) as NonTempAPJournalInfo WHERE RCB <> 'L') as data";
        }

        private string NonAcctRpt_NonTempVendorApJournalInfoSql_Not4_IsDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, VendorMaster.CheckName as TempVendorName "
                   + "FROM VendorMaster "
                   + "INNER JOIN ("
                        + "APJournalDetail INNER JOIN ("
                                + "JournalMaster INNER JOIN APJournal ON (APJournal.JournalNumber = JournalMaster.JournalNumber) "
                                                                  + "AND (APJournal.Period = JournalMaster.Period) "
                                                                  + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC')"
                        + ") ON (APJournal.ID = APJournalDetail.APJournalID)"
                   + ") ON (APJournal.VendorNumber = VendorMaster.VendorNumber) "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournal.VendorNumber <> 0 "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'" + strAPDateSQL;
        }

        private string NonAcctRpt_TempVendorApJournalInfoSql_Not4_IsDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, APJournal.TempVendorName as TempVendorName "
                   + "FROM "
                   + "("
                        + "APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID"
                   + ") "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                        + "AND APJournal.Period = JournalMaster.Period "
                        + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournal.VendorNumber = 0 "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'" + strAPDateSQL;
        }

        private string NonAcctRpt_TempVendorApJournalInfoSql_Not4_NotDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, APJournal.TempVendorName as TempVendorName "
                   + "FROM "
                   + "("
                        + "APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID"
                   + ") "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                        + "AND APJournal.Period = JournalMaster.Period "
                        + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournal.VendorNumber = 0 "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'"
                   + strAPDateSQL;
        }

        private string NonAcctRpt_NonTempVendorApJournalInfoSql_Not4_NotDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, VendorMaster.CheckName as TempVendorName "
                   + "FROM VendorMaster "
                   + "INNER JOIN "
                   + "("
                      + "APJournalDetail "
                      + "INNER JOIN "
                      + "("
                        + "JournalMaster INNER JOIN APJournal ON (APJournal.JournalNumber = JournalMaster.JournalNumber) "
                        + "AND (APJournal.Period = JournalMaster.Period) "
                        + "AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC')"
                      + ") ON (APJournal.ID = APJournalDetail.APJournalID)"
                   + ") ON (APJournal.VendorNumber = VendorMaster.VendorNumber) "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournal.VendorNumber <> 0 "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'"
                   + strAPDateSQL;
        }

        private string NonAcctRpt_OtherJournalInfoSql_Not4_NotDaily()
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, JournalEntries.VendorNumber, Description, Amount, JournalEntries.CheckNumber as CheckNumber, VendorMaster.CheckName as TempVendorName "
                   + "FROM JournalEntries "
                   + "INNER JOIN VendorMaster ON JournalEntries.VendorNumber = VendorMaster.VendorNumber "
                   + "WHERE JournalEntries.Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND Account = '" + vendorAccountNumber + "'"
                   + strDateSQL;
        }

        private static string NonAcctRpt_ApJournalInfoSql_Not4_NotDaily(string strTempVendorAPJournalInfo, string strNonTempVendorAPJournalInfo)
        {
            return "SELECT * "
                 + "FROM ("
                         + "SELECT * "
                         + "FROM (" + strTempVendorAPJournalInfo + ") as TempAPJournalInfo "
                         + "WHERE RCB <> 'L' "
                         + "UNION ALL "
                         + "SELECT * "
                         + "FROM (" + strNonTempVendorAPJournalInfo + ") as NonTempAPJournalInfo "
                         + "WHERE RCB <> 'L'"
                 + ") as data";
        }

        private string NonAcctRpt_OtherJournalInfoSql_Is4_IsDaily()
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, VendorNumber, Description, Amount, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND Account = '" + vendorAccountNumber + "'" + strOtherDateSQL;
        }

        private string NonAcctRpt_ApJournalInfoSql_Is4_IsDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournal.TempVendorName as TempVendorName, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                   + "AND APJournal.Period = JournalMaster.Period AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournalDetail.RCB <> 'L' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'" + strAPDateSQL;
        }

        private string NonAcctRpt_OtherJournalInfoSql_Is4_NotDaily()
        {
            return "SELECT Period, JournalNumber, PostedDate, JournalEntriesDate as CheckDate, VendorNumber, Description, Amount, CheckNumber as CheckNumber "
                   + "FROM JournalEntries "
                   + "WHERE Status = 'P' "
                   + "AND Type='D' AND Type <> 'D' "
                   + "AND Account = '" + vendorAccountNumber + "'" + strDateSQL;
        }

        private string NonAcctRpt_ApJournalInfoSql_Is4_NotDaily()
        {
            return "SELECT APJournal.Period AS Period, APJournal.JournalNumber as JournalNumber, APJournal.PostedDate as PostedDate, APJournal.CheckDate as CheckDate, APJournal.VendorNumber as VendorNumber, APJournal.Reference as Invoice, APJournal.TempVendorName as TempVendorName, APJournalDetail.Description as Description, APJournalDetail.RCB AS RCB, APJournal.CheckNumber as CheckNumber, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance "
                   + "FROM (APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID) "
                   + "INNER JOIN JournalMaster ON APJournal.JournalNumber = JournalMaster.JournalNumber "
                   + "AND APJournal.Period = JournalMaster.Period AND ((JournalMaster.Type = 'AP' AND APJournal.CheckDate = JournalMaster.CheckDate) or JournalMaster.Type = 'AC') "
                   + "WHERE JournalMaster.Status = 'P' "
                   + "AND APJournalDetail.RCB <> 'L' "
                   + "AND IsNull(JournalMaster.CreditMemo, 0) = 0  AND IsNull(APJournal.CreditMemoCorrection, 0) = 0 "
                   + "AND APJournalDetail.Account = '" + vendorAccountNumber + "'" + strAPDateSQL;
        }
        #endregion

        #endregion

        private string GetEncumbranceSql(string strSearchString, bool blnTempVendor)
        {
            string sql;
            var searchText = modCustomReport.FixQuotes(strSearchString);

            if (IsAcctReport())
            {

                if (blnTempVendor)
                {
                    var vendorNumber = rsVendorInfo.Get_Fields_Int32("VendorNumber");

                    sql = IsDateRange()
                        ? $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, EncumbranceDetail.Account as Account, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.TempVendorName = '{searchText}' AND Encumbrances.VendorNumber = {vendorNumber}{strEndDateSQL}ORDER BY Encumbrances.PostedDate"
                        : $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, EncumbranceDetail.Account as Account, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.TempVendorName = '{searchText}' AND Encumbrances.VendorNumber = {vendorNumber}{strDateSQL}ORDER BY Encumbrances.PostedDate";
                }
                else
                {
                    sql = IsDateRange()
                        ? $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, EncumbranceDetail.Account as Account, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber = {searchText}{strEndDateSQL}ORDER BY Encumbrances.PostedDate"
                        : $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, EncumbranceDetail.Account as Account, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber = {searchText}{strDateSQL}ORDER BY Encumbrances.PostedDate";
                }
            }
            else
            {
                if (strReportType == "4")
                {
                    sql = IsDateRange()
                        ? $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, Encumbrances.TempVendorName as TempVendorName, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND EncumbranceDetail.Account = '{searchText}'{strEndDateSQL}ORDER BY Encumbrances.PostedDate"
                        : $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, Encumbrances.TempVendorName as TempVendorName, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND EncumbranceDetail.Account = '{searchText}'{strDateSQL}ORDER BY Encumbrances.PostedDate";
                }
                else
                {
                    string strTempVendorEncumbranceInfo;
                    string strNonTempVendorEncumbranceInfo;

                    if (IsDateRange())
                    {
                        strTempVendorEncumbranceInfo = $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, Encumbrances.TempVendorName as TempVendorName, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber = 0 AND EncumbranceDetail.Account = '{searchText}'{strEndDateSQL}ORDER BY Encumbrances.PostedDate";
                        strNonTempVendorEncumbranceInfo = $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated, VendorMaster.CheckName as TempVendorName FROM ((Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber <> 0 AND EncumbranceDetail.Account = '{searchText}'{strEndDateSQL}ORDER BY Encumbrances.PostedDate";
                    }
                    else
                    {
                        strTempVendorEncumbranceInfo = $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, Encumbrances.TempVendorName as TempVendorName, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber = 0 AND EncumbranceDetail.Account = '{searchText}'{strDateSQL}ORDER BY Encumbrances.PostedDate";
                        strNonTempVendorEncumbranceInfo = $"SELECT Encumbrances.Period AS Period, Encumbrances.JournalNumber as JournalNumber, Encumbrances.PostedDate as PostedDate, Encumbrances.VendorNumber as VendorNumber, Encumbrances.PO as Invoice, EncumbranceDetail.Description as Description, EncumbranceDetail.Amount as Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated as Liquidated, VendorMaster.CheckName as TempVendorName FROM ((Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) INNER JOIN VendorMaster ON Encumbrances.VendorNumber = VendorMaster.VendorNumber) WHERE (Encumbrances.Status = 'P' AND (EncumbranceDetail.Amount + EncumbranceDetail.Adjustments - EncumbranceDetail.Liquidated) > 0) AND Encumbrances.VendorNumber <> 0 AND EncumbranceDetail.Account = '{searchText}'{strDateSQL}ORDER BY Encumbrances.PostedDate";
                    }

                    sql = $"SELECT * FROM (SELECT * FROM ({strTempVendorEncumbranceInfo}) as TempVendorEncumbranceInfo UNION ALL SELECT * FROM ({strNonTempVendorEncumbranceInfo}) as NonTempVendorEncumbranceInfo) ORDER BY TempVendorName";
                }
            }

            return sql;
        }

        private string SelectJournalType()
        {
            string selectJournalType;

            if (rsAPJournalInfo.EndOfFile() != true && rsCDJournalInfo.EndOfFile() != true)
            {
                selectJournalType = rsAPJournalInfo.Get_Fields_DateTime("PostedDate") <= rsCDJournalInfo.Get_Fields_DateTime("PostedDate") ? "A" : "O";
            }
            else
            {
                selectJournalType = rsAPJournalInfo.EndOfFile() ? "O" : "A";
            }

            return selectJournalType;
        }

        private void GetAPData()
        {
            fldPeriod.Text = modValidateAccount.GetFormat_6(rsAPJournalInfo.Get_Fields("Period"), 2);

            var journalNumber = rsAPJournalInfo.Get_Fields("JournalNumber").ToString();
            fldJournal.Text = modValidateAccount.GetFormat_6(journalNumber, 4);
            GetWarrantInfo(journalNumber);

            fldDate.Text = Strings.Format(rsAPJournalInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");

            var invoiceNumber = FCConvert.ToString(rsAPJournalInfo.Get_Fields("Invoice"));

            if (IsVendorReport())
            {
                var vendorNumber = rsAPJournalInfo.Get_Fields_Int32("VendorNumber");
                var formattedVendorNumber = modValidateAccount.GetFormat_6(vendorNumber, 5);

                if (vendorNumber == 0 || strReportType == "5")
                {
                    fldAccount.Text = formattedVendorNumber + SingleSpace + rsAPJournalInfo.Get_Fields_String("TempVendorName");
                }
                else
                {
                    if (rsVendorNames.RecordCount() > 0)
                    {
                        fldAccount.Text = rsVendorNames.FindFirstRecord("VendorNumber", vendorNumber)
                            ? formattedVendorNumber + SingleSpace + rsVendorNames.Get_Fields_String("CheckName")
                            : formattedVendorNumber + UNKNOWN;
                    }
                    else
                    {
                        fldAccount.Text = formattedVendorNumber + UNKNOWN;
                    }
                }

                fldInvoice.Text = invoiceNumber;
            }
            else
            {
                fldAccount.Text = rsAPJournalInfo.Get_Fields_String("Account");
                fldInvoice.Text = invoiceNumber;
            }

            var rcb = rsAPJournalInfo.Get_Fields_String("RCB");
            fldDescription.Text = rsAPJournalInfo.Get_Fields_String("Description");
            fldRCB.Text = rcb;
            fldType.Text = "A";
            fldCheck.Text = Strings.Trim(FCConvert.ToString(rsAPJournalInfo.Get_Fields("CheckNumber")));

            decimal amount = rsAPJournalInfo.Get_Fields("Amount");
            decimal discount = rsAPJournalInfo.Get_Fields("Discount");
            decimal debitsTotal = amount - discount;
            fldAmount.Text = Strings.Format(debitsTotal, STANDARD_CURRENCY_FORMAT);
            if (debitsTotal > 0 && rcb != "C")
            {
                curDebitsTotal += debitsTotal;
            }
            else
            {
                if (debitsTotal > 0)
                    curCreditsTotal -= debitsTotal;
                else
                    if (debitsTotal <= 0 && rcb == "C")
                        curDebitsTotal += debitsTotal;
                    else
                        curCreditsTotal -= debitsTotal;
            }
        }

        private void GetWarrantInfo(string journalNumber)
        {
            var rsWarrantInfo = new clsDRWrapper();
            rsWarrantInfo.OpenRecordset($"SELECT Warrant FROM APJournal WHERE JournalNumber = {journalNumber}");
            fldWarrant.Text = rsWarrantInfo.RecordCount() == 0
                ? string.Empty
                : fldWarrant.Text = modValidateAccount.GetFormat_6(rsWarrantInfo.Get_Fields("Warrant"), 4);
        }

        private void GetOtherData()
        {
            fldPeriod.Text = modValidateAccount.GetFormat_6(rsCDJournalInfo.Get_Fields("Period"), 2);
            fldWarrant.Text = string.Empty;
            fldJournal.Text = modValidateAccount.GetFormat_6(rsCDJournalInfo.Get_Fields("JournalNumber"), 4);
            fldDate.Text = Strings.Format(rsCDJournalInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
            var invoiceNumber = FCConvert.ToString(rsCDJournalInfo.Get_Fields("Invoice"));

            if (IsVendorReport())
            {
                var vendorNumber = rsCDJournalInfo.Get_Fields_Int32("VendorNumber");
                var formattedVendorNumber = modValidateAccount.GetFormat_6(vendorNumber, 5);

                if (FCConvert.ToInt32(vendorNumber) == 0 || FCConvert.ToDouble(strReportType) == 5)
                {
                    fldAccount.Text = formattedVendorNumber + SingleSpace + rsCDJournalInfo.Get_Fields_String("TempVendorName");
                }
                else
                {
                    if (rsVendorNames.EndOfFile() != true && rsVendorNames.BeginningOfFile() != true)
                    {
                        if (rsVendorNames.FindFirstRecord("VendorNumber", rsVendorInfo.Get_Fields_Int32("VendorNumber")))
                        {
                            fldAccount.Text = formattedVendorNumber + SingleSpace + rsVendorNames.Get_Fields_String("CheckName");
                        }
                        else
                        {
                            fldAccount.Text = formattedVendorNumber + UNKNOWN;
                        }
                    }
                    else
                    {
                        fldAccount.Text = formattedVendorNumber + UNKNOWN;
                    }
                }
                fldInvoice.Text = invoiceNumber;
            }
            else
            {
                fldAccount.Text = rsCDJournalInfo.Get_Fields_String("Account");
                fldInvoice.Text = invoiceNumber;
            }
            fldDescription.Text = rsCDJournalInfo.Get_Fields_String("Description");
            var rcb = rsCDJournalInfo.Get_Fields_String("RCB");
            fldRCB.Text = rcb;
            fldType.Text = "D";
            fldCheck.Text = Strings.Trim(FCConvert.ToString(rsCDJournalInfo.Get_Fields("CheckNumber")));
            decimal journalAmount = rsCDJournalInfo.Get_Fields("Amount");
            fldAmount.Text = Strings.Format(journalAmount, STANDARD_CURRENCY_FORMAT);
            if (journalAmount > 0 && rcb != "C")
            {
                curDebitsTotal += journalAmount;
            }
            else
            {
                if (journalAmount > 0 && rcb == "C")
                    curCreditsTotal += journalAmount;
                else
                    if (journalAmount <= 0 && rcb != "C")
                        curCreditsTotal -= journalAmount;
                    else
                        curDebitsTotal += journalAmount;
            }
        }

        private void rptVendorDetail_ReportEndedAndCanceled(object sender, EventArgs e)
        {
            this.Unload();
            frmReportViewer.InstancePtr.Unload();
        }
    }
}
