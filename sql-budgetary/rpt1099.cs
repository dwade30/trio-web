﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1099.
	/// </summary>
	public partial class rpt1099 : FCSectionReport
	{
		public static rpt1099 InstancePtr
		{
			get
			{
				return (rpt1099)Sys.GetInstance(typeof(rpt1099));
			}
		}

		protected rpt1099 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rpt1099	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
		Decimal curTotalAmount;
		int intTotalForms;

		public rpt1099()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "1099 Forms";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsDefaults = new clsDRWrapper();
			rsDefaults.OpenRecordset("SELECT * FROM [1099Information]");
			rsDefaults.Edit();
			rsDefaults.Set_Fields("TotalAmountReported", curTotalAmount);
			rsDefaults.Set_Fields("TotalNumberOfFormsPrinted", intTotalForms);
			rsDefaults.Update();
			frm1099Setup.InstancePtr.Unload();
			MessageBox.Show("Total 1099 Amount  -  " + Strings.Format(curTotalAmount, "$#,##0.00"), "Total Amount", MessageBoxButtons.OK, MessageBoxIcon.Information, modal: false);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			strTemp = "SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo GROUP BY VendorNumber";
			// rsTemp.CreateStoredProcedure "1099Info", strTemp
			strTemp = "SELECT [1099Info].VendorNumber as VendorNumber FROM ((SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo GROUP BY VendorNumber) as [1099Info] INNER JOIN VendorMaster ON [1099Info].VendorNumber = VendorMaster.VendorNumber) WHERE VendorMaster.Attorney = True OR [1099Info].TotalAmount >= 600 ORDER BY VendorNumber";
			rsInfo.OpenRecordset(strTemp);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				rpt1099.InstancePtr.Cancel();
			}
			blnFirstRecord = true;
			curTotalAmount = 0;
			intTotalForms = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Decimal curTotal;
			clsDRWrapper rsTotalInfo = new clsDRWrapper();
			int intPlaceHolder;
			rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsInfo.Get_Fields_Int32("VendorNumber"));
			intTotalForms += 1;
			fldPayer.Text = Strings.Trim(frm1099Setup.InstancePtr.txtMunicipality.Text);
			fldAddress1.Text = Strings.Trim(frm1099Setup.InstancePtr.txtAddress1.Text);
			fldAddress2.Text = Strings.Trim(frm1099Setup.InstancePtr.txtAddress2.Text);
			if (Strings.Trim(frm1099Setup.InstancePtr.txtZip4.Text) != "")
			{
				fldAddress3.Text = Strings.Trim(frm1099Setup.InstancePtr.txtCity.Text) + ", " + Strings.Trim(frm1099Setup.InstancePtr.txtState.Text) + " " + Strings.Trim(frm1099Setup.InstancePtr.txtZip.Text) + "-" + Strings.Trim(frm1099Setup.InstancePtr.txtZip4.Text);
			}
			else
			{
				fldAddress3.Text = Strings.Trim(frm1099Setup.InstancePtr.txtCity.Text) + ", " + Strings.Trim(frm1099Setup.InstancePtr.txtState.Text) + " " + Strings.Trim(frm1099Setup.InstancePtr.txtZip.Text);
			}
			fldPayerFederalID.Text = Strings.Trim(frm1099Setup.InstancePtr.txtFederalID.Text);
			fldTaxID.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("TaxNumber")));
			fldPhone.Text = Strings.Trim(frm1099Setup.InstancePtr.txtPhone.Text);
			if (Convert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("1099UseCorrName")))
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))).Length > 30)
				{
					fldVendorName1.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName"))), 30);
				}
				else
				{
					fldVendorName1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondName")));
				}
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 1) == "*")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))).Length + 3 > 30)
					{
						fldVendorName2.Text = "DBA " + Strings.Mid(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 2, 30);
					}
					else
					{
						fldVendorName2.Text = "DBA " + Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))).Length - 1);
					}
				}
				else
				{
					fldVendorName2.Text = "";
				}
				fldVendorName3.Text = "";
				// If Len(Trim(rsVendorInfo.Fields["CorrespondName"])) > 26 Then
				// intPlaceHolder = InStrRev(Trim(rsVendorInfo.Fields["CorrespondName"]), " ", 26)
				// fldVendorName1 = Left(Trim(rsVendorInfo.Fields["CorrespondName"]), intPlaceHolder)
				// fldVendorName2 = Right(Trim(rsVendorInfo.Fields["CorrespondName"]), Len(Trim(rsVendorInfo.Fields["CorrespondName"])) - intPlaceHolder)
				// If Left(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), 1) = "*" Then
				// fldVendorName3 = "DBA " & Right(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), Len(Trim(rsVendorInfo.Fields["CorrespondAddress1"])) - 1)
				// Else
				// fldVendorName3 = ""
				// End If
				// Else
				// fldVendorName1 = Trim(rsVendorInfo.Fields["CorrespondName"])
				// If Left(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), 1) = "*" Then
				// If Len(Trim(rsVendorInfo.Fields["CorrespondAddress1"])) + 3 > 26 Then
				// intPlaceHolder = InStrRev(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), " ", 26)
				// fldVendorName2 = "DBA " & Right(Left(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), intPlaceHolder), Len(Left(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), intPlaceHolder)) - 1)
				// fldVendorName3 = Right(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), Len(Trim(rsVendorInfo.Fields["CorrespondAddress1"])) - intPlaceHolder)
				// Else
				// fldVendorName2 = "DBA " & Right(Trim(rsVendorInfo.Fields["CorrespondAddress1"]), Len(Trim(rsVendorInfo.Fields["CorrespondAddress1"])) - 1)
				// fldVendorName3 = ""
				// End If
				// Else
				// fldVendorName2 = ""
				// fldVendorName3 = ""
				// End If
				// End If
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1"))), 1) == "*")
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress2")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress3")));
				}
				else
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress1")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondAddress2")));
				}
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CorrespondCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CorrespondZip"))));
					}
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))).Length > 30)
				{
					fldVendorName1.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName"))), 30);
				}
				else
				{
					fldVendorName1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckName")));
				}
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 1) == "*")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))).Length + 3 > 30)
					{
						fldVendorName2.Text = "DBA " + Strings.Mid(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 2, 30);
					}
					else
					{
						fldVendorName2.Text = "DBA " + Strings.Right(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))).Length - 1);
					}
				}
				else
				{
					fldVendorName2.Text = "";
				}
				fldVendorName3.Text = "";
				// If Len(Trim(rsVendorInfo.Fields["CheckName"])) > 26 Then
				// intPlaceHolder = InStrRev(Trim(rsVendorInfo.Fields["CheckName"]), " ", 26)
				// fldVendorName1 = Left(Trim(rsVendorInfo.Fields["CheckName"]), intPlaceHolder)
				// fldVendorName2 = Right(Trim(rsVendorInfo.Fields["CheckName"]), Len(Trim(rsVendorInfo.Fields["CheckName"])) - intPlaceHolder)
				// If Left(Trim(rsVendorInfo.Fields["CheckAddress1"]), 1) = "*" Then
				// fldVendorName3 = "DBA " & Right(Trim(rsVendorInfo.Fields["CheckAddress1"]), Len(Trim(rsVendorInfo.Fields["CheckAddress1"])) - 1)
				// Else
				// fldVendorName3 = ""
				// End If
				// Else
				// fldVendorName1 = Trim(rsVendorInfo.Fields["CheckName"])
				// If Left(Trim(rsVendorInfo.Fields["CheckAddress1"]), 1) = "*" Then
				// If Len(Trim(rsVendorInfo.Fields["CheckAddress1"])) + 3 > 26 Then
				// intPlaceHolder = InStrRev(Trim(rsVendorInfo.Fields["CheckAddress1"]), " ", 26)
				// fldVendorName2 = "DBA " & Right(Left(Trim(rsVendorInfo.Fields["CheckAddress1"]), intPlaceHolder), Len(Left(Trim(rsVendorInfo.Fields["CheckAddress1"]), intPlaceHolder)) - 1)
				// fldVendorName3 = Right(Trim(rsVendorInfo.Fields["CheckAddress1"]), Len(Trim(rsVendorInfo.Fields["CheckAddress1"])) - intPlaceHolder)
				// Else
				// fldVendorName2 = "DBA " & Right(Trim(rsVendorInfo.Fields["CheckAddress1"]), Len(Trim(rsVendorInfo.Fields["CheckAddress1"])) - 1)
				// fldVendorName3 = ""
				// End If
				// Else
				// fldVendorName2 = ""
				// fldVendorName3 = ""
				// End If
				// End If
				if (Strings.Left(Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1"))), 1) == "*")
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3")));
				}
				else
				{
					fldVendorAddress1.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1")));
					fldVendorAddress2.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")));
				}
				if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))));
					}
					else
					{
						fldCityStateZip.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " + Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
					}
				}
			}
			fldVendorNumber.Text = Strings.Format(rsVendorInfo.Get_Fields_Int32("VendorNumber"), "00000");
			fldStateID.Text = Strings.Trim(frm1099Setup.InstancePtr.txtStateId.Text);
			curTotal = CalcRentTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldRent.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldRent.Text = "";
			}
			curTotal = CalcRoyaltyTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldRoyalties.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldRoyalties.Text = "";
			}
			curTotal = CalcOtherIncomeTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldOtherIncome.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldOtherIncome.Text = "";
			}
			curTotal = CalcFederalIncomeTaxWithheldTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldFederalIncomeTaxWithheld.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldFederalIncomeTaxWithheld.Text = "";
			}
			curTotal = CalcFishingBoatProceedsTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldFishingBoatProceeds.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldFishingBoatProceeds.Text = "";
			}
			curTotal = CalcMedicaidHealthCareTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldMedical.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldMedical.Text = "";
			}
			curTotal = CalcNonEmployeeCompensationTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldAmountPaid.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldAmountPaid.Text = "";
			}
			curTotal = CalcAttorneyFeeTotal_2(FCConvert.ToInt32(rsInfo.Get_Fields_Int32("VendorNumber")));
			if (curTotal != 0)
			{
				fldAttorneyFee.Text = Strings.Format(curTotal, "#,##0.00");
				curTotalAmount += curTotal;
			}
			else
			{
				fldAttorneyFee.Text = "";
			}
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcRentTotal_2(int X)
		{
			return CalcRentTotal(ref X);
		}

		private Decimal CalcRentTotal(ref int X)
		{
			Decimal CalcRentTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 1");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcRentTotal = 0;
				return CalcRentTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcRentTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcRentTotal = 0;
			}
			return CalcRentTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcRoyaltyTotal_2(int X)
		{
			return CalcRoyaltyTotal(ref X);
		}

		private Decimal CalcRoyaltyTotal(ref int X)
		{
			Decimal CalcRoyaltyTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 2");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcRoyaltyTotal = 0;
				return CalcRoyaltyTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcRoyaltyTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcRoyaltyTotal = 0;
			}
			return CalcRoyaltyTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcOtherIncomeTotal_2(int X)
		{
			return CalcOtherIncomeTotal(ref X);
		}

		private Decimal CalcOtherIncomeTotal(ref int X)
		{
			Decimal CalcOtherIncomeTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 3");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcOtherIncomeTotal = 0;
				return CalcOtherIncomeTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcOtherIncomeTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcOtherIncomeTotal = 0;
			}
			return CalcOtherIncomeTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcFederalIncomeTaxWithheldTotal_2(int X)
		{
			return CalcFederalIncomeTaxWithheldTotal(ref X);
		}

		private Decimal CalcFederalIncomeTaxWithheldTotal(ref int X)
		{
			Decimal CalcFederalIncomeTaxWithheldTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 4");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcFederalIncomeTaxWithheldTotal = 0;
				return CalcFederalIncomeTaxWithheldTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcFederalIncomeTaxWithheldTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcFederalIncomeTaxWithheldTotal = 0;
			}
			return CalcFederalIncomeTaxWithheldTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcFishingBoatProceedsTotal_2(int X)
		{
			return CalcFishingBoatProceedsTotal(ref X);
		}

		private Decimal CalcFishingBoatProceedsTotal(ref int X)
		{
			Decimal CalcFishingBoatProceedsTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 5");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcFishingBoatProceedsTotal = 0;
				return CalcFishingBoatProceedsTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcFishingBoatProceedsTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcFishingBoatProceedsTotal = 0;
			}
			return CalcFishingBoatProceedsTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcMedicaidHealthCareTotal_2(int X)
		{
			return CalcMedicaidHealthCareTotal(ref X);
		}

		private Decimal CalcMedicaidHealthCareTotal(ref int X)
		{
			Decimal CalcMedicaidHealthCareTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 6");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcMedicaidHealthCareTotal = 0;
				return CalcMedicaidHealthCareTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcMedicaidHealthCareTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcMedicaidHealthCareTotal = 0;
			}
			return CalcMedicaidHealthCareTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcNonEmployeeCompensationTotal_2(int X)
		{
			return CalcNonEmployeeCompensationTotal(ref X);
		}

		private Decimal CalcNonEmployeeCompensationTotal(ref int X)
		{
			Decimal CalcNonEmployeeCompensationTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 7");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcNonEmployeeCompensationTotal = 0;
				return CalcNonEmployeeCompensationTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcNonEmployeeCompensationTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcNonEmployeeCompensationTotal = 0;
			}
			return CalcNonEmployeeCompensationTotal;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16
		private Decimal CalcAttorneyFeeTotal_2(int X)
		{
			return CalcAttorneyFeeTotal(ref X);
		}

		private Decimal CalcAttorneyFeeTotal(ref int X)
		{
			Decimal CalcAttorneyFeeTotal = 0;
			clsDRWrapper rsRent = new clsDRWrapper();
			int counter;
			string strSQL;
			clsDRWrapper rsTaxInfo = new clsDRWrapper();
			strSQL = "";
			rsTaxInfo.OpenRecordset("SELECT * FROM TaxTitles WHERE Category = 14");
			if (rsTaxInfo.EndOfFile() != true && rsTaxInfo.BeginningOfFile() != true)
			{
				do
				{
					strSQL += "Class = '" + rsTaxInfo.Get_Fields_Int32("TaxCode") + "' OR ";
					rsTaxInfo.MoveNext();
				}
				while (rsTaxInfo.EndOfFile() != true);
			}
			if (strSQL != "")
			{
				strSQL = Strings.Left(strSQL, strSQL.Length - 4);
			}
			if (strSQL == "")
			{
				CalcAttorneyFeeTotal = 0;
				return CalcAttorneyFeeTotal;
			}
			else
			{
				rsRent.OpenRecordset("SELECT VendorNumber, SUM(Amount) as TotalAmount FROM VendorTaxInfo WHERE VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " AND (" + strSQL + ") GROUP BY VendorNumber");
			}
			if (rsRent.EndOfFile() != true && rsRent.BeginningOfFile() != true)
			{
				if (Information.IsNumeric(rsRent.Get_Fields_Decimal("TotalAmount")))
				{
					CalcAttorneyFeeTotal = FCConvert.ToDecimal(rsRent.Get_Fields_Decimal("TotalAmount"));
				}
			}
			else
			{
				CalcAttorneyFeeTotal = 0;
			}
			return CalcAttorneyFeeTotal;
		}

		private void rpt1099_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rpt1099.Caption	= "1099 Forms";
			//rpt1099.Icon	= "rpt1099.dsx":0000";
			//rpt1099.Left	= 0;
			//rpt1099.Top	= 0;
			//rpt1099.Width	= 11880;
			//rpt1099.Height	= 8595;
			//rpt1099.StartUpPosition	= 3;
			//rpt1099.SectionData	= "rpt1099.dsx":058A;
			//End Unmaped Properties
		}
	}
}
