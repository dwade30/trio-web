﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBoothBayCheckRecExport.
	/// </summary>
	partial class frmBoothBayCheckRecExport : BaseForm
	{
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBoothBayCheckRecExport));
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.dtpLowDate = new fecherFoundation.FCDateTimePicker();
            this.dtpHighDate = new fecherFoundation.FCDateTimePicker();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 95);
            this.BottomPanel.Size = new System.Drawing.Size(404, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.dtpHighDate);
            this.ClientArea.Controls.Add(this.dtpLowDate);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(424, 234);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.dtpLowDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.dtpHighDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(424, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(197, 28);
            this.HeaderText.Text = "Check Rec Export";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(96, 16);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "DATE RANGE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(197, 72);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 21);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(161, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(80, 48);
            this.cmdProcessSave.TabIndex = 3;
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // dtpLowDate
            // 
            this.dtpLowDate.AutoSize = false;
            this.dtpLowDate.CustomFormat = "MM/dd/yyyy";
            this.dtpLowDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpLowDate.Location = new System.Drawing.Point(21, 59);
            this.dtpLowDate.Mask = "00/00/0000";
            this.dtpLowDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpLowDate.Name = "dtpLowDate";
            this.dtpLowDate.Size = new System.Drawing.Size(145, 36);
            this.dtpLowDate.TabIndex = 7;
            this.dtpLowDate.Value = new System.DateTime(((long)(0)));
            // 
            // dtpHighDate
            // 
            this.dtpHighDate.AutoSize = false;
            this.dtpHighDate.CustomFormat = "MM/dd/yyyy";
            this.dtpHighDate.Format = Wisej.Web.DateTimePickerFormat.Custom;
            this.dtpHighDate.Location = new System.Drawing.Point(253, 59);
            this.dtpHighDate.Mask = "00/00/0000";
            this.dtpHighDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpHighDate.Name = "dtpHighDate";
            this.dtpHighDate.Size = new System.Drawing.Size(145, 36);
            this.dtpHighDate.TabIndex = 8;
            this.dtpHighDate.Value = new System.DateTime(((long)(0)));
            // 
            // frmBoothBayCheckRecExport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(424, 294);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBoothBayCheckRecExport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Check Rec Export";
            this.Load += new System.EventHandler(this.frmBoothBayCheckRecExport_Load);
            this.Activated += new System.EventHandler(this.frmBoothBayCheckRecExport_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBoothBayCheckRecExport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
        private FCDateTimePicker dtpHighDate;
        private FCDateTimePicker dtpLowDate;
    }
}
