﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBDAccountInquiry.
	/// </summary>
	partial class frmBDAccountInquiry : BaseForm
	{
		public fecherFoundation.FCButton cmdPrintStatus;
		public fecherFoundation.FCComboBox cmbPeriodEnd;
		public fecherFoundation.FCComboBox cmbPeriodStart;
		public fecherFoundation.FCCheckBox chkHideZeroBalance;
		public fecherFoundation.FCCheckBox chkHideNoActivity;
		public fecherFoundation.FCTextBox txtSegment4;
		public fecherFoundation.FCTextBox txtSegment3;
		public fecherFoundation.FCTextBox txtSegment2;
		public fecherFoundation.FCTextBox txtSegment1;
		public fecherFoundation.FCComboBox cmbAccountType;
		public fecherFoundation.FCButton cmdPrint;
		public FCGrid GridAccounts;
		public fecherFoundation.FCLabel lblperiodto;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBDAccountInquiry));
			Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent3 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent4 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent5 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent6 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent7 = new Wisej.Web.JavaScript.ClientEvent();
			Wisej.Web.JavaScript.ClientEvent clientEvent8 = new Wisej.Web.JavaScript.ClientEvent();
			this.cmdPrintStatus = new fecherFoundation.FCButton();
			this.cmbPeriodEnd = new fecherFoundation.FCComboBox();
			this.cmbPeriodStart = new fecherFoundation.FCComboBox();
			this.chkHideZeroBalance = new fecherFoundation.FCCheckBox();
			this.chkHideNoActivity = new fecherFoundation.FCCheckBox();
			this.txtSegment4 = new fecherFoundation.FCTextBox();
			this.txtSegment3 = new fecherFoundation.FCTextBox();
			this.txtSegment2 = new fecherFoundation.FCTextBox();
			this.txtSegment1 = new fecherFoundation.FCTextBox();
			this.cmbAccountType = new fecherFoundation.FCComboBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.GridAccounts = new fecherFoundation.FCGrid();
			this.lblperiodto = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.javaScript1 = new Wisej.Web.JavaScript(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHideZeroBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHideNoActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 637);
			this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPeriodEnd);
			this.ClientArea.Controls.Add(this.cmbPeriodStart);
			this.ClientArea.Controls.Add(this.chkHideZeroBalance);
			this.ClientArea.Controls.Add(this.chkHideNoActivity);
			this.ClientArea.Controls.Add(this.txtSegment4);
			this.ClientArea.Controls.Add(this.txtSegment3);
			this.ClientArea.Controls.Add(this.txtSegment2);
			this.ClientArea.Controls.Add(this.txtSegment1);
			this.ClientArea.Controls.Add(this.cmbAccountType);
			this.ClientArea.Controls.Add(this.GridAccounts);
			this.ClientArea.Controls.Add(this.lblperiodto);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(1088, 640);
			this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
			this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblperiodto, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridAccounts, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbAccountType, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtSegment1, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtSegment2, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtSegment3, 0);
			this.ClientArea.Controls.SetChildIndex(this.txtSegment4, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkHideNoActivity, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkHideZeroBalance, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbPeriodStart, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbPeriodEnd, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrintStatus);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintStatus, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.AutoSize = false;
			this.HeaderText.Size = new System.Drawing.Size(279, 30);
			this.HeaderText.Text = "Account Inquiry";
			// 
			// cmdPrintStatus
			// 
			this.cmdPrintStatus.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintStatus.Location = new System.Drawing.Point(964, 29);
			this.cmdPrintStatus.Name = "cmdPrintStatus";
			this.cmdPrintStatus.Size = new System.Drawing.Size(84, 24);
			this.cmdPrintStatus.TabIndex = 14;
			this.cmdPrintStatus.Text = "Print Status";
			this.cmdPrintStatus.Click += new System.EventHandler(this.cmdPrintStatus_Click);
			// 
			// cmbPeriodEnd
			// 
			this.cmbPeriodEnd.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodEnd.Location = new System.Drawing.Point(384, 90);
			this.cmbPeriodEnd.Name = "cmbPeriodEnd";
			this.cmbPeriodEnd.Size = new System.Drawing.Size(177, 40);
			this.cmbPeriodEnd.TabIndex = 11;
			this.cmbPeriodEnd.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodEnd_SelectedIndexChanged);
			// 
			// cmbPeriodStart
			// 
			this.cmbPeriodStart.BackColor = System.Drawing.SystemColors.Window;
			this.cmbPeriodStart.Location = new System.Drawing.Point(150, 90);
			this.cmbPeriodStart.Name = "cmbPeriodStart";
			this.cmbPeriodStart.Size = new System.Drawing.Size(177, 40);
			this.cmbPeriodStart.TabIndex = 10;
			this.cmbPeriodStart.SelectedIndexChanged += new System.EventHandler(this.cmbPeriodStart_SelectedIndexChanged);
			// 
			// chkHideZeroBalance
			// 
			this.chkHideZeroBalance.Location = new System.Drawing.Point(608, 98);
			this.chkHideZeroBalance.Name = "chkHideZeroBalance";
			this.chkHideZeroBalance.Size = new System.Drawing.Size(239, 23);
			this.chkHideZeroBalance.TabIndex = 9;
			this.chkHideZeroBalance.Text = "Exclude accounts with zero balance";
			this.chkHideZeroBalance.CheckedChanged += new System.EventHandler(this.chkHideZeroBalance_CheckedChanged);
			// 
			// chkHideNoActivity
			// 
			this.chkHideNoActivity.Location = new System.Drawing.Point(608, 38);
			this.chkHideNoActivity.Name = "chkHideNoActivity";
			this.chkHideNoActivity.Size = new System.Drawing.Size(222, 23);
			this.chkHideNoActivity.TabIndex = 5;
			this.chkHideNoActivity.Text = "Exclude accounts with no activity";
			this.chkHideNoActivity.CheckedChanged += new System.EventHandler(this.chkHideNoActivity_CheckedChanged);
			// 
			// txtSegment4
			// 
			this.txtSegment4.BackColor = System.Drawing.SystemColors.Window;
			clientEvent1.Event = "keypress";
			clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
			clientEvent2.Event = "keyup";
			clientEvent2.JavaScript = resources.GetString("clientEvent2.JavaScript");
			this.javaScript1.GetJavaScriptEvents(this.txtSegment4).Add(clientEvent1);
			this.javaScript1.GetJavaScriptEvents(this.txtSegment4).Add(clientEvent2);
			this.txtSegment4.Location = new System.Drawing.Point(489, 30);
			this.txtSegment4.Name = "txtSegment4";
			this.txtSegment4.Size = new System.Drawing.Size(71, 40);
			this.txtSegment4.TabIndex = 4;
			this.txtSegment4.Visible = false;
			this.txtSegment4.Validating += new System.ComponentModel.CancelEventHandler(this.txtSegment4_Validating);
			this.txtSegment4.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSegment4_KeyDown);
			this.txtSegment4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment4_KeyPress);
			// 
			// txtSegment3
			// 
			this.txtSegment3.BackColor = System.Drawing.SystemColors.Window;
			clientEvent3.Event = "keypress";
			clientEvent3.JavaScript = resources.GetString("clientEvent3.JavaScript");
			clientEvent4.Event = "keyup";
			clientEvent4.JavaScript = resources.GetString("clientEvent4.JavaScript");
			this.javaScript1.GetJavaScriptEvents(this.txtSegment3).Add(clientEvent3);
			this.javaScript1.GetJavaScriptEvents(this.txtSegment3).Add(clientEvent4);
			this.txtSegment3.Location = new System.Drawing.Point(401, 30);
			this.txtSegment3.Name = "txtSegment3";
			this.txtSegment3.Size = new System.Drawing.Size(71, 40);
			this.txtSegment3.TabIndex = 3;
			this.txtSegment3.Visible = false;
			this.txtSegment3.Validating += new System.ComponentModel.CancelEventHandler(this.txtSegment3_Validating);
			this.txtSegment3.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSegment3_KeyDown);
			this.txtSegment3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment3_KeyPress);
			// 
			// txtSegment2
			// 
			this.txtSegment2.BackColor = System.Drawing.SystemColors.Window;
			clientEvent5.Event = "keypress";
			clientEvent5.JavaScript = resources.GetString("clientEvent5.JavaScript");
			clientEvent6.Event = "keyup";
			clientEvent6.JavaScript = resources.GetString("clientEvent6.JavaScript");
			this.javaScript1.GetJavaScriptEvents(this.txtSegment2).Add(clientEvent5);
			this.javaScript1.GetJavaScriptEvents(this.txtSegment2).Add(clientEvent6);
			this.txtSegment2.Location = new System.Drawing.Point(313, 30);
			this.txtSegment2.Name = "txtSegment2";
			this.txtSegment2.Size = new System.Drawing.Size(71, 40);
			this.txtSegment2.TabIndex = 2;
			this.txtSegment2.Visible = false;
			this.txtSegment2.Validating += new System.ComponentModel.CancelEventHandler(this.txtSegment2_Validating);
			this.txtSegment2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSegment2_KeyDown);
			this.txtSegment2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment2_KeyPress);
			// 
			// txtSegment1
			// 
			this.txtSegment1.BackColor = System.Drawing.SystemColors.Window;
			clientEvent7.Event = "keypress";
			clientEvent7.JavaScript = resources.GetString("clientEvent7.JavaScript");
			clientEvent8.Event = "keyup";
			clientEvent8.JavaScript = resources.GetString("clientEvent8.JavaScript");
			this.javaScript1.GetJavaScriptEvents(this.txtSegment1).Add(clientEvent7);
			this.javaScript1.GetJavaScriptEvents(this.txtSegment1).Add(clientEvent8);
			this.txtSegment1.Location = new System.Drawing.Point(225, 30);
			this.txtSegment1.Name = "txtSegment1";
			this.txtSegment1.Size = new System.Drawing.Size(71, 40);
			this.txtSegment1.TabIndex = 1;
			this.txtSegment1.Visible = false;
			this.txtSegment1.Validating += new System.ComponentModel.CancelEventHandler(this.txtSegment1_Validating);
			this.txtSegment1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSegment1_KeyDown);
			this.txtSegment1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSegment1_KeyPress);
			// 
			// cmbAccountType
			// 
			this.cmbAccountType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbAccountType.Location = new System.Drawing.Point(150, 30);
			this.cmbAccountType.Name = "cmbAccountType";
			this.cmbAccountType.Size = new System.Drawing.Size(65, 40);
			this.cmbAccountType.TabIndex = 1001;
			this.cmbAccountType.SelectedIndexChanged += new System.EventHandler(this.cmbAccountType_SelectedIndexChanged);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(508, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(78, 48);
			this.cmdPrint.TabIndex = 6;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// GridAccounts
			// 
			this.GridAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridAccounts.Cols = 8;
			this.GridAccounts.FixedCols = 0;
			this.GridAccounts.Location = new System.Drawing.Point(30, 150);
			this.GridAccounts.Name = "GridAccounts";
			this.GridAccounts.RowHeadersVisible = false;
			this.GridAccounts.Rows = 1;
			this.GridAccounts.Size = new System.Drawing.Size(1028, 487);
			this.GridAccounts.TabIndex = 7;
			this.GridAccounts.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridAccounts_MouseMoveEvent);
			this.GridAccounts.DoubleClick += new System.EventHandler(this.GridAccounts_DblClick);
			// 
			// lblperiodto
			// 
			this.lblperiodto.Location = new System.Drawing.Point(349, 104);
			this.lblperiodto.Name = "lblperiodto";
			this.lblperiodto.Size = new System.Drawing.Size(20, 16);
			this.lblperiodto.TabIndex = 13;
			this.lblperiodto.Text = "TO";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(50, 16);
			this.Label2.TabIndex = 12;
			this.Label2.Text = "PERIODS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(40, 16);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "SHOW";
			// 
			// frmBDAccountInquiry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1088, 700);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBDAccountInquiry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Account Inquiry";
			this.Load += new System.EventHandler(this.frmBDAccountInquiry_Load);
			this.Resize += new System.EventHandler(this.frmBDAccountInquiry_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBDAccountInquiry_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHideZeroBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHideNoActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridAccounts)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private JavaScript javaScript1;
	}
}
