﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerSummarySelect.
	/// </summary>
	public partial class frmLedgerSummarySelect : BaseForm
	{
		public frmLedgerSummarySelect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
        {
            this.cmdProcessSave.Click += this.cmdProcessSave_Click;
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.vsHighAccount.ExtendLastCol = true;
            this.vsLowAccount.ExtendLastCol = true;
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerSummarySelect InstancePtr
		{
			get
			{
				return (frmLedgerSummarySelect)Sys.GetInstance(typeof(frmLedgerSummarySelect));
			}
		}

		protected frmLedgerSummarySelect _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs_AutoInitialized;

		clsDRWrapper rs
		{
			get
			{
				if (rs_AutoInitialized == null)
				{
					rs_AutoInitialized = new clsDRWrapper();
				}
				return rs_AutoInitialized;
			}
			set
			{
				rs_AutoInitialized = value;
			}
		}

		string strOriginalDescription;
		int lngOriginalCriteria;
		int lngOriginalFormat;
		bool blnFromSave;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}

		private void cboReports_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (chkDefault.CheckState == CheckState.Checked)
			{
				chkDefault.CheckState = CheckState.Unchecked;
			}
		}

		private void chkDefault_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cboReports.SelectedIndex == 0)
			{
				chkDefault.CheckState = CheckState.Unchecked;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdCancelRange_Click(object sender, System.EventArgs e)
		{
			fraReportSelection.Visible = true;
			fraRangeSelection.Visible = false;
		}

		private void cmdCancelSelection_Click(object sender, System.EventArgs e)
		{
			txtReportTitle.Text = "";
			cboCriteria.SelectedIndex = -1;
			cboFormat.SelectedIndex = -1;
			fraReportSelection.Visible = false;
			cboReports.Enabled = true;
			chkDefault.Enabled = true;
			cboReports.Focus();
			cmdFileDelete.Enabled = true;
		}

		public void cmdCancelSelection_Click()
		{
			cmdCancelSelection_Click(cmdCancelSelection, new System.EventArgs());
		}

		private void cmdCreateCriteria_Click(object sender, System.EventArgs e)
		{
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmLedgerSummarySetup.InstancePtr.Show(App.MainForm);
		}

		private void cmdCreateFormat_Click(object sender, System.EventArgs e)
		{
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmCustomizeLedgerSummary.InstancePtr.FromLedger = true;
			frmCustomizeLedgerSummary.InstancePtr.Show(App.MainForm);
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			int counter;
			if (cboReports.SelectedIndex == 0)
			{
				cboReports.Enabled = false;
				chkDefault.Enabled = false;
				fraReportSelection.Visible = true;
				cboCriteria.SelectedIndex = -1;
				cboFormat.SelectedIndex = -1;
				txtReportTitle.Focus();
			}
			else
			{
				if (chkDefault.CheckState == CheckState.Checked)
				{
					modRegistry.SaveRegistryKey("LEDSUMDEF", cboReports.Text);
				}
				rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LS' AND Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					txtReportTitle.Text = cboReports.Text;
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("ShowReportTitle")))
					{
						chkShowReportTitle.CheckState = CheckState.Checked;
					}
					else
					{
						chkShowReportTitle.CheckState = CheckState.Unchecked;
					}
					rsSearch.OpenRecordset("SELECT * FROM ReportTitles WHERE ID = " + rs.Get_Fields_Int32("CriteriaID"));
					if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
					{
						for (counter = 0; counter <= cboCriteria.Items.Count - 1; counter++)
						{
							if (FCConvert.ToString(rsSearch.Get_Fields_String("Description")) == cboCriteria.Items[counter].ToString())
							{
								cboCriteria.SelectedIndex = counter;
								break;
							}
						}
					}
					// TODO Get_Fields: Check the table for the column [FormatID] and replace with corresponding Get_Field method
					rsSearch.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE ID = " + rs.Get_Fields("FormatID"));
					if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
					{
						for (counter = 0; counter <= cboFormat.Items.Count - 1; counter++)
						{
							if (FCConvert.ToString(rsSearch.Get_Fields_String("Description")) == cboFormat.Items[counter].ToString())
							{
								cboFormat.SelectedIndex = counter;
								break;
							}
						}
					}
					cboReports.Enabled = false;
					chkDefault.Enabled = false;
					fraReportSelection.Visible = true;
					cmdProcessSave.Focus();
					cmdFileDelete.Enabled = false;
				}
				else
				{
					MessageBox.Show("Report Not Found", "Unable to find Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			strOriginalDescription = txtReportTitle.Text;
			lngOriginalCriteria = cboCriteria.SelectedIndex;
			lngOriginalFormat = cboFormat.SelectedIndex;
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdFileSaveProcess, new System.EventArgs());
		}

		private void cmdProcessNoSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (!blnFromSave)
			{
				if (strOriginalDescription != Strings.Trim(txtReportTitle.Text) || lngOriginalCriteria != cboCriteria.SelectedIndex || lngOriginalFormat != cboFormat.SelectedIndex)
				{
					answer = MessageBox.Show("There is either new or changed information in this report that will not be saved.  Are you sure you wish to continue without saving your information?", "Continue Process?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
					if (answer == DialogResult.No)
					{
						return;
					}
				}
			}
			else
			{
				blnFromSave = false;
			}
			fraReportSelection.Visible = false;
			rsSearch.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				modBudgetaryMaster.Statics.lngReportFormat = FCConvert.ToInt32(rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Unable to Find Report Format", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				fraReportSelection.Visible = true;
				return;
			}
			rsSearch.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' AND Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				modBudgetaryMaster.Statics.lngReportCriteria = FCConvert.ToInt32(rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Unable to Find Selection Criteria", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				fraReportSelection.Visible = true;
				return;
			}
			ClearRange();
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				if (FCConvert.ToBoolean(rsSearch.Get_Fields_Boolean("CheckMonthRange")) || FCConvert.ToBoolean(rsSearch.Get_Fields_Boolean("CheckAccountRange")))
				{
					if (FCConvert.ToBoolean(rsSearch.Get_Fields_Boolean("CheckAccountRange")))
					{
						fraRangeSelection.Visible = true;
						fraAccountRange.Enabled = true;
						if (FCConvert.ToString(rsSearch.Get_Fields_String("SelectedAccounts")) == "R")
						{
							vsLowAccount.Visible = true;
							vsHighAccount.Visible = true;
							lblTo[2].Visible = true;
							vsLowAccount.Focus();
							fraAccountRange.Text = "Account Range";
						}
						else if (rsSearch.Get_Fields_String("SelectedAccounts") == "S")
						{
							cboSingleFund.Visible = true;
							cboSingleFund.Focus();
							fraAccountRange.Text = "Fund";
						}
						else
						{
							cboBeginningFund.Visible = true;
							cboEndingFund.Visible = true;
							lblTo[2].Visible = true;
							cboBeginningFund.Focus();
							fraAccountRange.Text = "Fund Range";
						}
					}
					else
					{
						cboBeginningFund.Visible = true;
						cboEndingFund.Visible = true;
						lblTo[2].Visible = true;
					}
					if (FCConvert.ToBoolean(rsSearch.Get_Fields_Boolean("CheckMonthRange")))
					{
						fraRangeSelection.Visible = true;
						fraDateRange.Enabled = true;
						if (FCConvert.ToString(rsSearch.Get_Fields_String("SelectedMonths")) == "S")
						{
							cboSingleMonth.Visible = true;
							cboSingleMonth.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modRegistry.GetRegistryKey("CURRSINGLEMONTH")) - 1);
							cboSingleMonth.Focus();
							fraDateRange.Text = "Month";
						}
						else
						{
							cboBeginningMonth.Visible = true;
							cboEndingMonth.Visible = true;
							lblTo[0].Visible = true;
							cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modRegistry.GetRegistryKey("CURRSTARTMONTH")) - 1);
							cboEndingMonth.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modRegistry.GetRegistryKey("CURRENDMONTH")) - 1);
							cboBeginningMonth.Focus();
							fraDateRange.Text = "Date Range";
						}
					}
					else
					{
						cboBeginningMonth.Visible = true;
						cboEndingMonth.Visible = true;
						lblTo[0].Visible = true;
					}
				}
				else
				{
					if (FCConvert.ToString(rsSearch.Get_Fields_String("SelectedMonths")) == "A")
					{
						rsSearch.Edit();
						rsSearch.Set_Fields("BegMonth", modBudgetaryMaster.Statics.FirstMonth);
						if (modBudgetaryMaster.Statics.FirstMonth > 1)
						{
							rsSearch.Set_Fields("EndMonth", modBudgetaryMaster.Statics.FirstMonth - 1);
						}
						else
						{
							rsSearch.Set_Fields("EndMonth", 12);
						}
						rsSearch.Update(true);
					}
					modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "' AND Type = 'LS'");
					chkDefault.Enabled = true;
					cboReports.Enabled = true;
					//! Load frmWait; // show the wait form
					frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
					// center it
					frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
					frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
					frmWait.InstancePtr.Show();
					frmWait.InstancePtr.Refresh();
					//Application.DoEvents();
					frmLedgerSummary.InstancePtr.Show(App.MainForm);
					if (chkShowReportTitle.CheckState == CheckState.Checked)
					{
						frmLedgerSummary.InstancePtr.strTitle = txtReportTitle.Text;
					}
					else
					{
						frmLedgerSummary.InstancePtr.strTitle = "";
					}
					frmWait.InstancePtr.Refresh();
					//Application.DoEvents();
					frmWait.InstancePtr.Unload();
					// Me.Hide
					Close();
				}
			}
			else
			{
				MessageBox.Show("Report Not Found", "Unable to Find Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		public void cmdProcessNoSave_Click()
		{
			cmdProcessNoSave_Click(cmdProcessSave, new System.EventArgs());
		}

		private void cmdProcessRange_Click(object sender, System.EventArgs e)
		{
			if (fraDateRange.Enabled == true)
			{
				if (cboBeginningMonth.Visible == true)
				{
					if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
					{
						MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
					{
						MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					modRegistry.SaveRegistryKey("CURRSTARTMONTH", FCConvert.ToString(cboBeginningMonth.SelectedIndex + 1));
					modRegistry.SaveRegistryKey("CURRENDMONTH", FCConvert.ToString(cboEndingMonth.SelectedIndex + 1));
				}
				if (cboSingleMonth.Visible == true)
				{
					if (cboSingleMonth.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					modRegistry.SaveRegistryKey("CURRSINGLEMONTH", FCConvert.ToString(cboSingleMonth.SelectedIndex + 1));
				}
			}
			if (vsLowAccount.Visible == true)
			{
				if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
				{
					MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
				{
					MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (fraAccountRange.Enabled)
			{
				if (cboSingleFund.Visible == true)
				{
					if (cboSingleFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (cboBeginningFund.Visible == true)
				{
					if (cboBeginningFund.SelectedIndex == -1 || cboEndingFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningFund.SelectedIndex > cboEndingFund.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "' AND Type = 'LS'");
			if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
			{
				modBudgetaryAccounting.Statics.SearchResults.Edit();
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
				{
					modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegMonth", modBudgetaryMaster.Statics.FirstMonth);
					if (modBudgetaryMaster.Statics.FirstMonth > 1)
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("EndMonth", modBudgetaryMaster.Statics.FirstMonth - 1);
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("EndMonth", 12);
					}
				}
				else if (fraDateRange.Enabled == true)
				{
					if (cboSingleMonth.Visible == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegMonth", cboSingleMonth.SelectedIndex + 1);
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegMonth", cboBeginningMonth.SelectedIndex + 1);
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("EndMonth", cboEndingMonth.SelectedIndex + 1);
					}
				}
				if (fraAccountRange.Enabled == true)
				{
					if (vsLowAccount.Visible == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegAccount", vsLowAccount.TextMatrix(0, 0));
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("EndAccount", vsHighAccount.TextMatrix(0, 0));
					}
					else if (cboSingleFund.Visible == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegDeptExp", Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					}
					else if (cboBeginningFund.Visible == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("BegDeptExp", Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
						modBudgetaryAccounting.Statics.SearchResults.Set_Fields("EndDeptExp", Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					}
				}
				modBudgetaryAccounting.Statics.SearchResults.Update();
			}
			else
			{
				MessageBox.Show("Unable to Find Selection Criteria", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "' AND Type = 'LS'");
			if (chkDefault.CheckState == CheckState.Checked)
			{
				modRegistry.SaveRegistryKey("LEDSUMDEF", cboReports.Text);
			}
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			frmLedgerSummary.InstancePtr.Show(App.MainForm);
			if (chkShowReportTitle.CheckState == CheckState.Checked)
			{
				frmLedgerSummary.InstancePtr.strTitle = txtReportTitle.Text;
			}
			else
			{
				frmLedgerSummary.InstancePtr.strTitle = "";
			}
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			frmWait.InstancePtr.Unload();
			// Me.Hide
			Close();
		}

		public void cmdProcessRange_Click()
		{
			cmdProcessRange_Click(cmdFileSaveProcess, new System.EventArgs());
		}

		private void cmdProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LS' AND Title = '" + modCustomReport.FixQuotes(txtReportTitle.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A Ledger Summary Report already exists with this title.  Do you wish to overwrite this?", "Overwrite Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
				}
				else
				{
					txtReportTitle.Focus();
					return;
				}
			}
			else
			{
				rs.AddNew();
			}
			rsSearch.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' AND Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				rs.Set_Fields("CriteriaID", rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Selection Criteria could not be found", "Unable to Save Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsSearch.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				rs.Set_Fields("FormatID", rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Report Format could not be found", "Unable to Save Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rs.Set_Fields("Type", "LS");
			rs.Set_Fields("Title", Strings.Trim(txtReportTitle.Text));
			rs.Set_Fields("ShowReportTitle", chkShowReportTitle.CheckState == CheckState.Checked);
			rs.Update(true);
			blnFromSave = true;
			FillReport_2(true);
			cmdProcessNoSave_Click();
		}

		public void cmdProcessSave_Click()
		{
			cmdProcessSave_Click(cmdFileSaveProcess, new System.EventArgs());
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (Strings.Trim(Strings.UCase(txtReportTitle.Text)) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default report.  You must change the title before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtReportTitle.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LS' AND Title = '" + modCustomReport.FixQuotes(txtReportTitle.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A Ledger Summary Report already exists with this description.  Do you wish to overwrite this?", "Overwrite Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
				}
				else
				{
					txtReportTitle.Focus();
					return;
				}
			}
			else
			{
				rs.AddNew();
			}
			rsSearch.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' AND Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				rs.Set_Fields("CriteriaID", rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Selection Criteria could not be found", "Unable to Save Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsSearch.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
			if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
			{
				rs.Set_Fields("FormatID", rsSearch.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Report Format could not be found", "Unable to Save Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rs.Set_Fields("Type", "LS");
			rs.Set_Fields("Title", Strings.Trim(txtReportTitle.Text));
			rs.Set_Fields("ShowReportTitle", chkShowReportTitle.CheckState == CheckState.Checked);
			rs.Update(true);
			cmdCancelSelection_Click();
			FillReport_2(true);
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdFilePrint, new System.EventArgs());
		}

		private void frmLedgerSummarySelect_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmLedgerSummarySelect_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
				}
			}
		}

		private void frmLedgerSummarySelect_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLedgerSummarySelect.FillStyle	= 0;
			//frmLedgerSummarySelect.ScaleWidth	= 9045;
			//frmLedgerSummarySelect.ScaleHeight	= 7305;
			//frmLedgerSummarySelect.LinkTopic	= "Form2";
			//frmLedgerSummarySelect.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			string temp;
			vsLowGrid.GRID7Light = vsLowAccount;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsLowGrid.DefaultAccountType = "G";
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.DefaultAccountType = "G";
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsLowGrid.OnlyAllowDefaultType = true;
			vsHighGrid.OnlyAllowDefaultType = true;
			FillReport_2(false);
			FillCriteria_2(false);
			FillFormat_2(false);
			temp = modRegistry.GetRegistryKey("LEDSUMDEF");
			for (counter = 0; counter <= cboReports.Items.Count - 1; counter++)
			{
				if (cboReports.Items[counter].ToString() == temp)
				{
					cboReports.SelectedIndex = counter;
					break;
				}
			}
			//cboBeginningFund.Left = FCConvert.ToInt32(cboBeginningFund.Left + cboBeginningFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboBeginningFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboEndingFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboBeginningFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboEndingFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmLedgerSummarySelect_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (cboReports.SelectedIndex == 0)
			{
				MessageBox.Show("You must select a report before you may proceed.", "Invalid Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				if (cboReports.Text != "Default")
				{
					ans = MessageBox.Show("Are you sure you wish to delete this report?", "Delete Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						rs.OpenRecordset("SELECT * FROM Reports WHERE Type = 'LS' AND Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.Delete();
							rs.Update();
							cboReports.Items.RemoveAt(cboReports.SelectedIndex);
							cboReports.SelectedIndex = 0;
							MessageBox.Show("Report deleted successfully!", "Report Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				else
				{
					MessageBox.Show("You may not delete the default report.", "Invalid Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (fraReportSelection.Visible == false && fraRangeSelection.Visible == false)
			{
				cmdOK_Click();
			}
			else
			{
				cmdSave_Click();
			}
		}

		private void mnuFileSaveProcess_Click(object sender, System.EventArgs e)
		{
			if (fraReportSelection.Visible == false && fraRangeSelection.Visible == false)
			{
				cmdOK_Click();
			}
			else if (fraRangeSelection.Visible == true)
			{
				cmdProcessRange_Click();
			}
			else
			{
				cmdProcessNoSave_Click();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void FillCriteria_2(bool blnCriteriaAdded, string strDescription = "")
		{
			FillCriteria(blnCriteriaAdded, strDescription);
		}

		public void FillCriteria(bool blnCriteriaAdded, string strDescription = "")
		{
			int counter = 0;
			cboCriteria.Clear();
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LS' ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboCriteria.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (!blnCriteriaAdded)
			{
				cboCriteria.SelectedIndex = -1;
			}
			else
			{
				if (strDescription == "")
				{
					cboCriteria.SelectedIndex = cboCriteria.Items.Count - 1;
				}
				else
				{
					for (counter = 0; counter <= cboCriteria.Items.Count - 1; counter++)
					{
						if (strDescription == cboCriteria.Items[counter].ToString())
						{
							cboCriteria.SelectedIndex = counter;
							break;
						}
					}
				}
			}
		}

		public void FillFormat_2(bool blnFormatAdded, string strDescription = "")
		{
			FillFormat(blnFormatAdded, strDescription);
		}

		public void FillFormat(bool blnFormatAdded, string strDescription = "")
		{
			int counter = 0;
			cboFormat.Clear();
			rs.OpenRecordset("SELECT * FROM LedgerSummaryFormats ORDER BY Description");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboFormat.AddItem(FCConvert.ToString(rs.Get_Fields_String("Description")));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (!blnFormatAdded)
			{
				cboFormat.SelectedIndex = -1;
			}
			else
			{
				if (strDescription == "")
				{
					cboFormat.SelectedIndex = cboFormat.Items.Count - 1;
				}
				else
				{
					for (counter = 0; counter <= cboFormat.Items.Count - 1; counter++)
					{
						if (strDescription == cboFormat.Items[counter].ToString())
						{
							cboFormat.SelectedIndex = counter;
							break;
						}
					}
				}
			}
		}

		public void FillReport_2(bool blnReportAdded)
		{
			FillReport(blnReportAdded);
		}

		public void FillReport(bool blnReportAdded)
		{
			int counter = 0;
			cboReports.Clear();
			cboReports.AddItem("Create New Report");
			rs.OpenRecordset("SELECT * FROM Reports WHERE [Type] = 'LS' ORDER BY Title");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboReports.AddItem(FCConvert.ToString(rs.Get_Fields_String("Title")));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (blnReportAdded)
			{
				cboReports.SelectedIndex = cboReports.Items.Count - 1;
			}
			else
			{
				cboReports.SelectedIndex = 0;
			}
		}

		private void ClearRange()
		{
			cboBeginningFund.Visible = false;
			cboEndingFund.Visible = false;
			cboSingleFund.Visible = false;
			cboBeginningMonth.Visible = false;
			cboEndingMonth.Visible = false;
			cboSingleMonth.Visible = false;
			lblTo[0].Visible = false;
			lblTo[2].Visible = false;
			fraDateRange.Enabled = false;
			fraAccountRange.Enabled = false;
			vsLowAccount.Visible = false;
			vsHighAccount.Visible = false;
		}

		private void cboBeginningFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmdEditCriteria_Click(object sender, System.EventArgs e)
		{
			if (cboCriteria.SelectedIndex != -1)
			{
				// FC: FINAL: KV: IIT807 + FC - 8697
				this.Hide();
				modBudgetaryMaster.Statics.blnLedgerSummaryReportEdit = true;
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(cboCriteria.Text) + "' AND Type = 'LS'");
				frmLedgerSummarySetup.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MessageBox.Show("You must choose a criteria to edit before you proceed", "Unable to Edit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cmdEditFormat_Click(object sender, System.EventArgs e)
		{
			if (cboFormat.SelectedIndex != -1)
			{
				// FC: FINAL: KV: IIT807 + FC - 8697
				this.Hide();
				modBudgetaryMaster.Statics.blnLedgerSummaryReportEdit = true;
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE Description = '" + modCustomReport.FixQuotes(cboFormat.Text) + "'");
				frmCustomizeLedgerSummary.InstancePtr.FromLedger = true;
				frmCustomizeLedgerSummary.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MessageBox.Show("You must choose a format to edit before you proceed", "Unable to Edit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}
