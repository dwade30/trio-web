﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetPurchaseOrders.
	/// </summary>
	public partial class frmGetPurchaseOrders : BaseForm
	{
		public frmGetPurchaseOrders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetPurchaseOrders InstancePtr
		{
			get
			{
				return (frmGetPurchaseOrders)Sys.GetInstance(typeof(frmGetPurchaseOrders));
			}
		}

		protected frmGetPurchaseOrders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool OKFlag;
		clsDRWrapper rs2 = new clsDRWrapper();
		// vbPorter upgrade warning: Record As int	OnWrite(int, string)
		public void StartProgram(int Record)
		{
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			int counter;
			//Application.DoEvents();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
			// initialize the current account number
			Close();
			CheckAgain:
			;
			rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND PO = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
				if (modBudgetaryAccounting.Statics.SearchResults_AutoInitialized == null)
				{
					modBudgetaryAccounting.Statics.SearchResults.OpenRecordset(rs.Name());
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() == 0)
				{
					modBudgetaryAccounting.Statics.SearchResults.OpenRecordset(rs.Name());
				}
				// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
				frmPurchaseOrderDataEntry.InstancePtr.lblPurchaseOrder.Text = Strings.Format(rs.Get_Fields("PO"), "0000");
				frmPurchaseOrderDataEntry.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(rs.Get_Fields_Int32("VendorNumber"), 5);
				rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
				frmPurchaseOrderDataEntry.InstancePtr.lblVendorName.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
				frmPurchaseOrderDataEntry.InstancePtr.lblAddress1.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
				frmPurchaseOrderDataEntry.InstancePtr.lblAddress2.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
				frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
				if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"))) != "")
				{
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text = rs2.Get_Fields_String("CheckCity") + ", " + rs2.Get_Fields_String("CheckState") + " " + rs2.Get_Fields_String("CheckZip") + "-" + rs2.Get_Fields_String("CheckZip4");
				}
				else
				{
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text = rs2.Get_Fields_String("CheckCity") + ", " + rs2.Get_Fields_String("CheckState") + " " + rs2.Get_Fields_String("CheckZip");
				}
				if (Strings.Trim(frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text) == "")
				{
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text = "";
				}
				if (Strings.Trim(frmPurchaseOrderDataEntry.InstancePtr.lblAddress2.Text) == "")
				{
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress2.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text = "";
				}
				if (Strings.Trim(frmPurchaseOrderDataEntry.InstancePtr.lblAddress1.Text) == "")
				{
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress2.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress1.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress2.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress3.Text = frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text;
					frmPurchaseOrderDataEntry.InstancePtr.lblAddress4.Text = "";
				}
				//FC:FINAL:SBE - check for date
				//if (rs.Get_Fields("PODate") != IntPtr.Zero)
				if (Information.IsDate(rs.Get_Fields("PODate")))
					frmPurchaseOrderDataEntry.InstancePtr.txtDate.Text = FCConvert.ToString(rs.Get_Fields_DateTime("PODate"));
				frmPurchaseOrderDataEntry.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
				frmPurchaseOrderDataEntry.InstancePtr.txtComments.Text = FCConvert.ToString(rs.Get_Fields_String("Comments"));
				//FC:FINAL:DDU:#2887 - load form before selecting department from combobox
				frmPurchaseOrderDataEntry.InstancePtr.Show(App.MainForm);
				for (counter = 0; counter <= frmPurchaseOrderDataEntry.InstancePtr.cboDepartment.Items.Count - 1; counter++)
				{
					if (frmPurchaseOrderDataEntry.InstancePtr.cboDepartment.ItemData(counter) == FCConvert.ToInt32(rs.Get_Fields_String("Department")))
					{
						frmPurchaseOrderDataEntry.InstancePtr.cboDepartment.SelectedIndex = counter;
						break;
					}
				}
				frmPurchaseOrderDataEntry.InstancePtr.VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				frmPurchaseOrderDataEntry.InstancePtr.FirstRecordShown = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				GetDetails();
				// put detail information into the AP Data Entry form
				modBudgetaryMaster.Statics.blnPOEdit = true;
				// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO")) != Record)
				{
					do
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					}
					while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO")) != Record);
				}
				if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
				{
					frmAPDataEntry.InstancePtr.mnuProcessNextEntry.Enabled = true;
					frmAPDataEntry.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					//JEI: apply MenuItem properties to newly created Form-Buttons
					frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = true;
					frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
				}
				else
				{
					frmAPDataEntry.InstancePtr.mnuProcessNextEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					//JEI: apply MenuItem properties to newly created Form-Buttons
					frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
				}
				frmPurchaseOrderDataEntry.InstancePtr.cmdFilePrint.Enabled = true;
				frmPurchaseOrderDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = true;
				// show the form
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				modRegistry.SaveRegistryKey("CURRPURCHASEORDER", FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentPurchaseOrder));
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Open Purchase Order Found", "Non-Existent Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void GetDetails()
		{
			int counter = 0;
			rs2.OpenRecordset("SELECT * FROM PurchaseOrderDetails WHERE PurchaseOrderID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 15)
				{
					frmPurchaseOrderDataEntry.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
				}
				counter = 1;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					//Application.DoEvents();
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("Account")));
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_Decimal("Price")));
					// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("Quantity")));
					frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields_Decimal("Total")));
					rs2.MoveNext();
					counter += 1;
				}
				if (counter < frmPurchaseOrderDataEntry.InstancePtr.vs1.Rows - 1)
				{
					for (counter = counter; counter <= frmPurchaseOrderDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						//Application.DoEvents();
						frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(0));
						frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
						frmPurchaseOrderDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					}
				}
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			cmbSearchType.SelectedIndex = 0;
			//optHidden.Checked = true; // clear the search info
			txtSearch.Text = "";
			vs1.Clear();
			vs1.TextMatrix(0, 1, "P.O.");
			vs1.TextMatrix(0, 2, "Vendor");
			vs1.TextMatrix(0, 3, "Description");
			vs1.TextMatrix(0, 4, "Department");
			vs1.TextMatrix(0, 0, "ID");
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.rows - 1, 4) = 8
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 0))));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				StartProgram(modBudgetaryMaster.Statics.lngCurrentPurchaseOrder);
				// cmdGetAccountNumber_Click          'get the record
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the list of records invisible
		}

		public void cmdGet_Click()
		{
			cmdGet_Click(cmdGet, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.Statics.SearchResults = null;
			if (Conversion.Val(txtGetAccountNumber.Text) != 0)
			{
				// if there is a valid account number
				modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
				// set the account number to the one last used
				modBudgetaryMaster.Statics.blnPOEdit = true;
				// set the edit flag
				StartProgram(modBudgetaryMaster.Statics.lngCurrentPurchaseOrder);
				// call the procedure to retrieve the info
			}
			else
			{
                Close();
                modBudgetaryMaster.Statics.blnPOEdit = false;
				// else show we are creating a new vendor account
				frmPurchaseOrderDataEntry.InstancePtr.VendorNumber = 0;
				frmPurchaseOrderDataEntry.InstancePtr.FirstRecordShown = 0;
				frmPurchaseOrderDataEntry.InstancePtr.cmdFilePrint.Enabled = false;
				frmPurchaseOrderDataEntry.InstancePtr.cmdProcessNextEntry.Enabled = false;
				frmPurchaseOrderDataEntry.InstancePtr.cmdProcessPreviousEntry.Enabled = false;
				//JEI: apply MenuItem properties to newly created Form-Buttons
				frmPurchaseOrderDataEntry.InstancePtr.cmdProcessNextEntry.Enabled = false;
				frmPurchaseOrderDataEntry.InstancePtr.cmdProcessPreviousEntry.Enabled = false;
				frmPurchaseOrderDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = false;
				frmPurchaseOrderDataEntry.InstancePtr.Show(App.MainForm);
				// show the blankform
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
			// unload this form
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			Frame3.Visible = false;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string resp1 = "";
			string resp2 = "";
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (cmbSearchType.SelectedIndex != 0)
			{
				// make sure a search type is clicked
				if (cmbSearchType.SelectedIndex != 1)
				{
					if (cmbSearchType.SelectedIndex != 2)
					{
						if (cmbSearchType.SelectedIndex != 3)
						{
							if (cmbSearchType.SelectedIndex != 4)
							{
								frmWait.InstancePtr.Unload();
								//Application.DoEvents();
								MessageBox.Show("You must Choose a Search Type (Purchase Order, Vendor Number, Vendor Name, Description, or Department)", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							else
							{
								resp1 = "Department";
								// remember which type of search we are doing
							}
						}
						else
						{
							resp1 = "Description";
							// remember which type of search we are doing
						}
					}
					else
					{
						resp1 = "VendorName";
						// remember which type of search we are doing
					}
				}
				else
				{
					resp1 = "VendorNumber";
				}
			}
			else
			{
				resp1 = "PO";
			}
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				resp2 = txtSearch.Text;
				// remember the search criteria
			}
			if (resp1 == "PO")
			{
				// do the right kind of search
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND PO = " + resp2 + " ORDER BY PO");
			}
			else if (resp1 == "VendorNumber")
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND VendorNumber = " + resp2 + " ORDER BY PO");
			}
			else if (resp1 == "VendorName")
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND VendorNumber IN (SELECT VendorNumber FROM VendorMaster WHERE CheckName LIKE '" + modCustomReport.FixQuotes(resp2) + "%') ORDER BY PO");
			}
			else if (resp1 == "Description")
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND Description LIKE '" + resp2 + "%' ORDER BY PO");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE Closed = 0 AND Department LIKE '" + resp2 + "%' ORDER BY PO");
			}
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset(rs.Name());
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					cmdClear_Click();
					// clear the search
					// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					txtGetAccountNumber.Text = FCConvert.ToString(rs.Get_Fields("PO"));
					// retrieve the info
					modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					// save the account number
					cmdGetAccountNumber_Click();
				}
				else
				{
					Fill_List(ref resp1);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					// make the list of records visible
					vs1.Select(1, vs1.Cols - 1, 1, 0);
					vs1.Focus();
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetPurchaseOrders_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRPURCHASEORDER"))));
			txtGetAccountNumber.Text = FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentPurchaseOrder);
			txtGetAccountNumber.Focus();
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
			lblLastAccount.Text = FCConvert.ToString(modBudgetaryMaster.Statics.lngCurrentPurchaseOrder);
			vs1.TextMatrix(0, 1, "P.O.");
			vs1.TextMatrix(0, 2, "Vendor");
			vs1.TextMatrix(0, 3, "Description");
			vs1.TextMatrix(0, 4, "Department");
			vs1.TextMatrix(0, 0, "ID");
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.rows - 1, vs1.Cols - 1) = 8
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.05));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, 4);
			vs1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColDataType(4, FCGrid.DataTypeSettings.flexDTString);
			vs1.ColHidden(0, true);
		}

		private void frmGetPurchaseOrders_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetPurchaseOrders_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				//Application.DoEvents();
				if (this.Visible == true)
				{
					if (this.ActiveControl.GetName() == "vs1")
					{
						KeyAscii = (Keys)0;
						cmdGet_Click();
					}
					else
					{
						KeyAscii = (Keys)0;
						if (this.ActiveControl.GetName() == "txtGetAccountNumber")
						{
							cmdGetAccountNumber_Click();
							return;
						}
						if (this.ActiveControl.GetName() == "cmbSearchType")
						{
							OKFlag = true;
							optSearchType_Click(FCConvert.ToInt16(this.ActiveControl.GetIndex()));
						}
						if (this.ActiveControl.GetName() == "txtSearch")
							cmdSearch_Click();
					}
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Fill_List(ref string x)
		{
			int I;
			string strAddressLine = "";
			clsDRWrapper rsDept = new clsDRWrapper();
			clsDRWrapper rsVendor = new clsDRWrapper();
			rsVendor.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
			vs1.Rows = rs.RecordCount() + 1;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				//Application.DoEvents();
				vs1.TextMatrix(I, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
				// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
				vs1.TextMatrix(I, 1, modValidateAccount.GetFormat_6(rs.Get_Fields("PO"), 4));
				if (rsVendor.EndOfFile() != true && rsVendor.BeginningOfFile() != true)
				{
					if (rsVendor.Get_Fields_Int32("VendorNumber") != rs.Get_Fields_Int32("VendorNumber"))
					{
						rsVendor.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
					}
				}
				else
				{
					rsVendor.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
				}
				vs1.TextMatrix(I, 2, FCConvert.ToString(rsVendor.Get_Fields_String("CheckName")));
				vs1.TextMatrix(I, 3, FCConvert.ToString(rs.Get_Fields_String("Description")));
				rsDept.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rs.Get_Fields_String("Department") + "' AND convert(int, IsNull(Division, 0)) = 0", "TWBD0000.vb1");
				if (rsDept.EndOfFile() != true && rsDept.BeginningOfFile() != true)
				{
					vs1.TextMatrix(I, 4, rs.Get_Fields_String("Department") + " - " + rsDept.Get_Fields_String("LongDescription"));
				}
				else
				{
					vs1.TextMatrix(I, 4, rs.Get_Fields_String("Department") + " - UNKNOWN");
				}
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
			// vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, vs1.rows - 1, vs1.Cols - 1) = 8
			vs1.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			vs1.AutoSize(0, vs1.Cols - 1);
		}

		private void frmGetPurchaseOrders_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetPurchaseOrders.FillStyle	= 0;
			//frmGetPurchaseOrders.ScaleWidth	= 9045;
			//frmGetPurchaseOrders.ScaleHeight	= 7350;
			//frmGetPurchaseOrders.LinkTopic	= "Form2";
			//frmGetPurchaseOrders.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmGetPurchaseOrders_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.05));
			vs1.ColWidth(1, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			vs1.ColWidth(2, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(3, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(4, FCConvert.ToInt32(vs1.WidthOriginal * 0.1));
			//Frame3.CenterToContainer(this.ClientArea);
		}

		private void mnuFileExit_Click()
		{
			cmdQuit_Click(cmdQuit, EventArgs.Empty);
		}

		private void mnuFileProcess_Click()
		{
			cmdGetAccountNumber_Click();
		}

		private void optSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (frmGetAccount.InstancePtr.Visible == true)
			{
				if (OKFlag)
				{
					txtSearch.Focus();
					OKFlag = false;
				}
			}
		}

		public void optSearchType_Click(short Index)
		{
			optSearchType_CheckedChanged(Index, cmbSearchType.SelectedIndex, new System.EventArgs());
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			;
			optSearchType_CheckedChanged(index, sender, e);
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			if (cmbSearchType.SelectedIndex < 1)
			{
				cmbSearchType.Focus();
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			EditFlag = true;
			vs1.Select(vs1.Row, vs1.Cols - 1, vs1.Row, 0);
			EditFlag = false;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount;
			// if there is a record selected
			intAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vs1.TextMatrix(vs1.Row, 1))));
			// get the reocrd number
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = intAccount;
				// gets the Account Number for the item that is double clicked
				txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
				StartProgram(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 1)));
				// cmdGetAccountNumber_Click          'get the record
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the listbox invisible
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (e.KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			// if there is a record selected
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 1));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = intAccount;
					// gets the Account Number for the item that is double clicked
					txtGetAccountNumber.Text = FCConvert.ToString(intAccount);
					cmdGetAccountNumber_Click();
					// get the record
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				EditFlag = true;
				vs1.Select(vs1.Row, vs1.Cols - 1, vs1.Row, 0);
				EditFlag = false;
			}
		}

		private void optSearchType_MouseDown(int Index, object sender, System.EventArgs e)
		{
			//FC:TODO
			//MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			//int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			//float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
			//float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
			//if (Button == MouseButtonConstants.LeftButton && Shift == 0)
			//{
			//    OKFlag = true;
			//}
		}

		private void optSearchType_MouseDown(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_MouseDown(index, sender, e);
		}

		private void SetCustomFormColors()
		{
			Label3.BackColor = Color.Red;
		}
	}
}
