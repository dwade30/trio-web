﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptProjectSummary.
	/// </summary>
	partial class rptProjectSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProjectSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDNet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEncumbrances = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPending = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMonths = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblProjects = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCurrentDebits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCurrentCredits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCurrentNet1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDDebits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDCredits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDNet1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEncumbrance1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCurrentDebits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCurrentCredits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCurrentNet2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDDebits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDCredits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYTDNet2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEncumbrance2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPending1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPending2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.fldProjectTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.fldCurrentDebitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentCreditsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentNetTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDDebitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDCreditsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYTDNetTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEncumbranceTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPendingTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.linTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblProjectSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblProjectedCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldProjectedCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAmountUsed = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAmountUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAvailable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAvailable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDDebits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncumbrances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPending)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentDebits1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentCredits1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentNet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDDebits1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDCredits1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDNet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentDebits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentCredits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentNet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDDebits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDCredits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDNet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPending1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPending2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebitsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentCreditsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentNetTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDDebitsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDCreditsTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDNetTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPendingTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldCurrentDebits,
            this.fldCurrentCredits,
            this.fldCurrentNet,
            this.fldYTDDebits,
            this.fldYTDCredits,
            this.fldYTDNet,
            this.fldEncumbrances,
            this.fldPending});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 0F;
            this.fldAccount.MultiLine = false;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Visible = false;
            this.fldAccount.Width = 0.625F;
            // 
            // fldCurrentDebits
            // 
            this.fldCurrentDebits.Height = 0.1875F;
            this.fldCurrentDebits.Left = 0.625F;
            this.fldCurrentDebits.MultiLine = false;
            this.fldCurrentDebits.Name = "fldCurrentDebits";
            this.fldCurrentDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldCurrentDebits.Text = null;
            this.fldCurrentDebits.Top = 0F;
            this.fldCurrentDebits.Visible = false;
            this.fldCurrentDebits.Width = 0.625F;
            // 
            // fldCurrentCredits
            // 
            this.fldCurrentCredits.Height = 0.1875F;
            this.fldCurrentCredits.Left = 1.25F;
            this.fldCurrentCredits.MultiLine = false;
            this.fldCurrentCredits.Name = "fldCurrentCredits";
            this.fldCurrentCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldCurrentCredits.Text = null;
            this.fldCurrentCredits.Top = 0F;
            this.fldCurrentCredits.Visible = false;
            this.fldCurrentCredits.Width = 0.5625F;
            // 
            // fldCurrentNet
            // 
            this.fldCurrentNet.Height = 0.1875F;
            this.fldCurrentNet.Left = 1.8125F;
            this.fldCurrentNet.MultiLine = false;
            this.fldCurrentNet.Name = "fldCurrentNet";
            this.fldCurrentNet.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldCurrentNet.Text = null;
            this.fldCurrentNet.Top = 0F;
            this.fldCurrentNet.Visible = false;
            this.fldCurrentNet.Width = 0.5625F;
            // 
            // fldYTDDebits
            // 
            this.fldYTDDebits.Height = 0.1875F;
            this.fldYTDDebits.Left = 2.40625F;
            this.fldYTDDebits.MultiLine = false;
            this.fldYTDDebits.Name = "fldYTDDebits";
            this.fldYTDDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldYTDDebits.Text = null;
            this.fldYTDDebits.Top = 0F;
            this.fldYTDDebits.Visible = false;
            this.fldYTDDebits.Width = 0.5F;
            // 
            // fldYTDCredits
            // 
            this.fldYTDCredits.Height = 0.1875F;
            this.fldYTDCredits.Left = 2.96875F;
            this.fldYTDCredits.MultiLine = false;
            this.fldYTDCredits.Name = "fldYTDCredits";
            this.fldYTDCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldYTDCredits.Text = null;
            this.fldYTDCredits.Top = 0F;
            this.fldYTDCredits.Visible = false;
            this.fldYTDCredits.Width = 0.5F;
            // 
            // fldYTDNet
            // 
            this.fldYTDNet.Height = 0.1875F;
            this.fldYTDNet.Left = 3.375F;
            this.fldYTDNet.MultiLine = false;
            this.fldYTDNet.Name = "fldYTDNet";
            this.fldYTDNet.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldYTDNet.Text = null;
            this.fldYTDNet.Top = 0F;
            this.fldYTDNet.Visible = false;
            this.fldYTDNet.Width = 0.5F;
            // 
            // fldEncumbrances
            // 
            this.fldEncumbrances.Height = 0.1875F;
            this.fldEncumbrances.Left = 3.96875F;
            this.fldEncumbrances.MultiLine = false;
            this.fldEncumbrances.Name = "fldEncumbrances";
            this.fldEncumbrances.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldEncumbrances.Text = null;
            this.fldEncumbrances.Top = 0F;
            this.fldEncumbrances.Visible = false;
            this.fldEncumbrances.Width = 0.5F;
            // 
            // fldPending
            // 
            this.fldPending.Height = 0.1875F;
            this.fldPending.Left = 4.625F;
            this.fldPending.MultiLine = false;
            this.fldPending.Name = "fldPending";
            this.fldPending.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" +
    "o-char-set: 1";
            this.fldPending.Text = null;
            this.fldPending.Top = 0F;
            this.fldPending.Visible = false;
            this.fldPending.Width = 0.5F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.lblMonths,
            this.lblProjects,
            this.lblCurrentDebits1,
            this.lblCurrentCredits1,
            this.lblCurrentNet1,
            this.lblYTDDebits1,
            this.lblYTDCredits1,
            this.lblYTDNet1,
            this.lblEncumbrance1,
            this.Line1,
            this.lblAccount,
            this.lblCurrentDebits2,
            this.lblCurrentCredits2,
            this.lblCurrentNet2,
            this.lblYTDDebits2,
            this.lblYTDCredits2,
            this.lblYTDNet2,
            this.lblEncumbrance2,
            this.lblPending1,
            this.lblPending2});
            this.PageHeader.Height = 1.15625F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.5F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label1.Text = "Project Summary Report";
            this.Label1.Top = 0F;
            this.Label1.Width = 7.625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Label7.Text = "Label7";
            this.Label7.Top = 0.1875F;
            this.Label7.Width = 1.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Label2.Text = "Label2";
            this.Label2.Top = 0F;
            this.Label2.Width = 1.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 9.125F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "Label4";
            this.Label4.Top = 0.1875F;
            this.Label4.Width = 1.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 9.125F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "Label3";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.3125F;
            // 
            // lblMonths
            // 
            this.lblMonths.Height = 0.1875F;
            this.lblMonths.HyperLink = null;
            this.lblMonths.Left = 1.46875F;
            this.lblMonths.Name = "lblMonths";
            this.lblMonths.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.lblMonths.Text = "Label5";
            this.lblMonths.Top = 0.40625F;
            this.lblMonths.Width = 7.65625F;
            // 
            // lblProjects
            // 
            this.lblProjects.Height = 0.1875F;
            this.lblProjects.HyperLink = null;
            this.lblProjects.Left = 1.46875F;
            this.lblProjects.Name = "lblProjects";
            this.lblProjects.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.lblProjects.Text = "Label6";
            this.lblProjects.Top = 0.21875F;
            this.lblProjects.Width = 7.65625F;
            // 
            // lblCurrentDebits1
            // 
            this.lblCurrentDebits1.Height = 0.1875F;
            this.lblCurrentDebits1.Left = 0.625F;
            this.lblCurrentDebits1.Name = "lblCurrentDebits1";
            this.lblCurrentDebits1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentDebits1.Text = null;
            this.lblCurrentDebits1.Top = 0.75F;
            this.lblCurrentDebits1.Visible = false;
            this.lblCurrentDebits1.Width = 0.625F;
            // 
            // lblCurrentCredits1
            // 
            this.lblCurrentCredits1.Height = 0.1875F;
            this.lblCurrentCredits1.Left = 1.25F;
            this.lblCurrentCredits1.Name = "lblCurrentCredits1";
            this.lblCurrentCredits1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentCredits1.Text = null;
            this.lblCurrentCredits1.Top = 0.75F;
            this.lblCurrentCredits1.Visible = false;
            this.lblCurrentCredits1.Width = 0.5625F;
            // 
            // lblCurrentNet1
            // 
            this.lblCurrentNet1.Height = 0.1875F;
            this.lblCurrentNet1.Left = 1.8125F;
            this.lblCurrentNet1.Name = "lblCurrentNet1";
            this.lblCurrentNet1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentNet1.Text = null;
            this.lblCurrentNet1.Top = 0.75F;
            this.lblCurrentNet1.Visible = false;
            this.lblCurrentNet1.Width = 0.5625F;
            // 
            // lblYTDDebits1
            // 
            this.lblYTDDebits1.Height = 0.1875F;
            this.lblYTDDebits1.Left = 2.375F;
            this.lblYTDDebits1.Name = "lblYTDDebits1";
            this.lblYTDDebits1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDDebits1.Text = null;
            this.lblYTDDebits1.Top = 0.75F;
            this.lblYTDDebits1.Visible = false;
            this.lblYTDDebits1.Width = 0.5F;
            // 
            // lblYTDCredits1
            // 
            this.lblYTDCredits1.Height = 0.1875F;
            this.lblYTDCredits1.Left = 2.875F;
            this.lblYTDCredits1.Name = "lblYTDCredits1";
            this.lblYTDCredits1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDCredits1.Text = null;
            this.lblYTDCredits1.Top = 0.75F;
            this.lblYTDCredits1.Visible = false;
            this.lblYTDCredits1.Width = 0.5F;
            // 
            // lblYTDNet1
            // 
            this.lblYTDNet1.Height = 0.1875F;
            this.lblYTDNet1.Left = 3.375F;
            this.lblYTDNet1.Name = "lblYTDNet1";
            this.lblYTDNet1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDNet1.Text = null;
            this.lblYTDNet1.Top = 0.75F;
            this.lblYTDNet1.Visible = false;
            this.lblYTDNet1.Width = 0.5F;
            // 
            // lblEncumbrance1
            // 
            this.lblEncumbrance1.Height = 0.1875F;
            this.lblEncumbrance1.Left = 3.875F;
            this.lblEncumbrance1.Name = "lblEncumbrance1";
            this.lblEncumbrance1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblEncumbrance1.Text = null;
            this.lblEncumbrance1.Top = 0.75F;
            this.lblEncumbrance1.Visible = false;
            this.lblEncumbrance1.Width = 0.5F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.125F;
            this.Line1.Width = 10.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 10.5F;
            this.Line1.Y1 = 1.125F;
            this.Line1.Y2 = 1.125F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
            this.lblAccount.Text = null;
            this.lblAccount.Top = 0.9375F;
            this.lblAccount.Visible = false;
            this.lblAccount.Width = 0.625F;
            // 
            // lblCurrentDebits2
            // 
            this.lblCurrentDebits2.Height = 0.1875F;
            this.lblCurrentDebits2.Left = 0.625F;
            this.lblCurrentDebits2.Name = "lblCurrentDebits2";
            this.lblCurrentDebits2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentDebits2.Text = null;
            this.lblCurrentDebits2.Top = 0.9375F;
            this.lblCurrentDebits2.Visible = false;
            this.lblCurrentDebits2.Width = 0.625F;
            // 
            // lblCurrentCredits2
            // 
            this.lblCurrentCredits2.Height = 0.1875F;
            this.lblCurrentCredits2.Left = 1.25F;
            this.lblCurrentCredits2.Name = "lblCurrentCredits2";
            this.lblCurrentCredits2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentCredits2.Text = null;
            this.lblCurrentCredits2.Top = 0.9375F;
            this.lblCurrentCredits2.Visible = false;
            this.lblCurrentCredits2.Width = 0.5625F;
            // 
            // lblCurrentNet2
            // 
            this.lblCurrentNet2.Height = 0.1875F;
            this.lblCurrentNet2.Left = 1.8125F;
            this.lblCurrentNet2.Name = "lblCurrentNet2";
            this.lblCurrentNet2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblCurrentNet2.Text = null;
            this.lblCurrentNet2.Top = 0.9375F;
            this.lblCurrentNet2.Visible = false;
            this.lblCurrentNet2.Width = 0.5625F;
            // 
            // lblYTDDebits2
            // 
            this.lblYTDDebits2.Height = 0.1875F;
            this.lblYTDDebits2.Left = 2.375F;
            this.lblYTDDebits2.Name = "lblYTDDebits2";
            this.lblYTDDebits2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDDebits2.Text = null;
            this.lblYTDDebits2.Top = 0.9375F;
            this.lblYTDDebits2.Visible = false;
            this.lblYTDDebits2.Width = 0.5F;
            // 
            // lblYTDCredits2
            // 
            this.lblYTDCredits2.Height = 0.1875F;
            this.lblYTDCredits2.Left = 2.875F;
            this.lblYTDCredits2.Name = "lblYTDCredits2";
            this.lblYTDCredits2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDCredits2.Text = null;
            this.lblYTDCredits2.Top = 0.9375F;
            this.lblYTDCredits2.Visible = false;
            this.lblYTDCredits2.Width = 0.5F;
            // 
            // lblYTDNet2
            // 
            this.lblYTDNet2.Height = 0.1875F;
            this.lblYTDNet2.Left = 3.375F;
            this.lblYTDNet2.Name = "lblYTDNet2";
            this.lblYTDNet2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblYTDNet2.Text = null;
            this.lblYTDNet2.Top = 0.9375F;
            this.lblYTDNet2.Visible = false;
            this.lblYTDNet2.Width = 0.5F;
            // 
            // lblEncumbrance2
            // 
            this.lblEncumbrance2.Height = 0.1875F;
            this.lblEncumbrance2.Left = 3.875F;
            this.lblEncumbrance2.Name = "lblEncumbrance2";
            this.lblEncumbrance2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblEncumbrance2.Text = null;
            this.lblEncumbrance2.Top = 0.9375F;
            this.lblEncumbrance2.Visible = false;
            this.lblEncumbrance2.Width = 0.5F;
            // 
            // lblPending1
            // 
            this.lblPending1.Height = 0.1875F;
            this.lblPending1.Left = 4.40625F;
            this.lblPending1.Name = "lblPending1";
            this.lblPending1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblPending1.Text = null;
            this.lblPending1.Top = 0.75F;
            this.lblPending1.Visible = false;
            this.lblPending1.Width = 0.5F;
            // 
            // lblPending2
            // 
            this.lblPending2.Height = 0.1875F;
            this.lblPending2.Left = 4.40625F;
            this.lblPending2.Name = "lblPending2";
            this.lblPending2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.lblPending2.Text = null;
            this.lblPending2.Top = 0.9375F;
            this.lblPending2.Visible = false;
            this.lblPending2.Width = 0.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldProjectTitle,
            this.Binder});
            this.GroupHeader1.DataField = "Binder";
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // fldProjectTitle
            // 
            this.fldProjectTitle.Height = 0.1875F;
            this.fldProjectTitle.Left = 0F;
            this.fldProjectTitle.Name = "fldProjectTitle";
            this.fldProjectTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt";
            this.fldProjectTitle.Text = "Field28";
            this.fldProjectTitle.Top = 0.0625F;
            this.fldProjectTitle.Width = 4.59375F;
            // 
            // Binder
            // 
            this.Binder.DataField = "Binder";
            this.Binder.Height = 0.09375F;
            this.Binder.Left = 5.1875F;
            this.Binder.Name = "Binder";
            this.Binder.Text = "Field1";
            this.Binder.Top = 0.0625F;
            this.Binder.Visible = false;
            this.Binder.Width = 0.71875F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldCurrentDebitsTotal,
            this.fldCurrentCreditsTotal,
            this.fldCurrentNetTotal,
            this.fldYTDDebitsTotal,
            this.fldYTDCreditsTotal,
            this.fldYTDNetTotal,
            this.fldEncumbranceTotal,
            this.fldPendingTotal,
            this.linTotal,
            this.lblTotalLabel,
            this.lblProjectSummary,
            this.Line2,
            this.lblProjectedCost,
            this.fldProjectedCost,
            this.lblAmountUsed,
            this.fldAmountUsed,
            this.Line3,
            this.lblAvailable,
            this.fldAvailable});
            this.GroupFooter1.Height = 1.239583F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            // 
            // fldCurrentDebitsTotal
            // 
            this.fldCurrentDebitsTotal.Height = 0.1875F;
            this.fldCurrentDebitsTotal.Left = 1.15625F;
            this.fldCurrentDebitsTotal.MultiLine = false;
            this.fldCurrentDebitsTotal.Name = "fldCurrentDebitsTotal";
            this.fldCurrentDebitsTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldCurrentDebitsTotal.Text = null;
            this.fldCurrentDebitsTotal.Top = 0.03125F;
            this.fldCurrentDebitsTotal.Visible = false;
            this.fldCurrentDebitsTotal.Width = 0.84375F;
            // 
            // fldCurrentCreditsTotal
            // 
            this.fldCurrentCreditsTotal.Height = 0.1875F;
            this.fldCurrentCreditsTotal.Left = 2.09375F;
            this.fldCurrentCreditsTotal.MultiLine = false;
            this.fldCurrentCreditsTotal.Name = "fldCurrentCreditsTotal";
            this.fldCurrentCreditsTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldCurrentCreditsTotal.Text = null;
            this.fldCurrentCreditsTotal.Top = 0.03125F;
            this.fldCurrentCreditsTotal.Visible = false;
            this.fldCurrentCreditsTotal.Width = 0.84375F;
            // 
            // fldCurrentNetTotal
            // 
            this.fldCurrentNetTotal.Height = 0.1875F;
            this.fldCurrentNetTotal.Left = 3F;
            this.fldCurrentNetTotal.MultiLine = false;
            this.fldCurrentNetTotal.Name = "fldCurrentNetTotal";
            this.fldCurrentNetTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldCurrentNetTotal.Text = null;
            this.fldCurrentNetTotal.Top = 0.03125F;
            this.fldCurrentNetTotal.Visible = false;
            this.fldCurrentNetTotal.Width = 0.84375F;
            // 
            // fldYTDDebitsTotal
            // 
            this.fldYTDDebitsTotal.Height = 0.1875F;
            this.fldYTDDebitsTotal.Left = 3.90625F;
            this.fldYTDDebitsTotal.MultiLine = false;
            this.fldYTDDebitsTotal.Name = "fldYTDDebitsTotal";
            this.fldYTDDebitsTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldYTDDebitsTotal.Text = null;
            this.fldYTDDebitsTotal.Top = 0.03125F;
            this.fldYTDDebitsTotal.Visible = false;
            this.fldYTDDebitsTotal.Width = 0.84375F;
            // 
            // fldYTDCreditsTotal
            // 
            this.fldYTDCreditsTotal.Height = 0.1875F;
            this.fldYTDCreditsTotal.Left = 4.8125F;
            this.fldYTDCreditsTotal.MultiLine = false;
            this.fldYTDCreditsTotal.Name = "fldYTDCreditsTotal";
            this.fldYTDCreditsTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldYTDCreditsTotal.Text = null;
            this.fldYTDCreditsTotal.Top = 0.03125F;
            this.fldYTDCreditsTotal.Visible = false;
            this.fldYTDCreditsTotal.Width = 0.84375F;
            // 
            // fldYTDNetTotal
            // 
            this.fldYTDNetTotal.Height = 0.1875F;
            this.fldYTDNetTotal.Left = 5.75F;
            this.fldYTDNetTotal.MultiLine = false;
            this.fldYTDNetTotal.Name = "fldYTDNetTotal";
            this.fldYTDNetTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldYTDNetTotal.Text = null;
            this.fldYTDNetTotal.Top = 0.03125F;
            this.fldYTDNetTotal.Visible = false;
            this.fldYTDNetTotal.Width = 0.84375F;
            // 
            // fldEncumbranceTotal
            // 
            this.fldEncumbranceTotal.Height = 0.1875F;
            this.fldEncumbranceTotal.Left = 6.65625F;
            this.fldEncumbranceTotal.MultiLine = false;
            this.fldEncumbranceTotal.Name = "fldEncumbranceTotal";
            this.fldEncumbranceTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldEncumbranceTotal.Text = null;
            this.fldEncumbranceTotal.Top = 0.03125F;
            this.fldEncumbranceTotal.Visible = false;
            this.fldEncumbranceTotal.Width = 0.84375F;
            // 
            // fldPendingTotal
            // 
            this.fldPendingTotal.Height = 0.1875F;
            this.fldPendingTotal.Left = 7.59375F;
            this.fldPendingTotal.MultiLine = false;
            this.fldPendingTotal.Name = "fldPendingTotal";
            this.fldPendingTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; whit" +
    "e-space: nowrap; ddo-char-set: 1";
            this.fldPendingTotal.Text = "99,000,000.00";
            this.fldPendingTotal.Top = 0.03125F;
            this.fldPendingTotal.Visible = false;
            this.fldPendingTotal.Width = 0.84375F;
            // 
            // linTotal
            // 
            this.linTotal.Height = 0F;
            this.linTotal.Left = 0F;
            this.linTotal.LineWeight = 1F;
            this.linTotal.Name = "linTotal";
            this.linTotal.Top = 0F;
            this.linTotal.Width = 4.59375F;
            this.linTotal.X1 = 0F;
            this.linTotal.X2 = 4.59375F;
            this.linTotal.Y1 = 0F;
            this.linTotal.Y2 = 0F;
            // 
            // lblTotalLabel
            // 
            this.lblTotalLabel.Height = 0.1875F;
            this.lblTotalLabel.Left = 0.03125F;
            this.lblTotalLabel.Name = "lblTotalLabel";
            this.lblTotalLabel.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
            this.lblTotalLabel.Text = "Project Totals:";
            this.lblTotalLabel.Top = 0.03125F;
            this.lblTotalLabel.Width = 1.03125F;
            // 
            // lblProjectSummary
            // 
            this.lblProjectSummary.Height = 0.1875F;
            this.lblProjectSummary.HyperLink = null;
            this.lblProjectSummary.Left = 4.1875F;
            this.lblProjectSummary.Name = "lblProjectSummary";
            this.lblProjectSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.lblProjectSummary.Text = "Project Summary";
            this.lblProjectSummary.Top = 0.3125F;
            this.lblProjectSummary.Width = 2.21875F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 4.03125F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.5F;
            this.Line2.Width = 2.59375F;
            this.Line2.X1 = 4.03125F;
            this.Line2.X2 = 6.625F;
            this.Line2.Y1 = 0.5F;
            this.Line2.Y2 = 0.5F;
            // 
            // lblProjectedCost
            // 
            this.lblProjectedCost.Height = 0.1875F;
            this.lblProjectedCost.HyperLink = null;
            this.lblProjectedCost.Left = 4.03125F;
            this.lblProjectedCost.Name = "lblProjectedCost";
            this.lblProjectedCost.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.lblProjectedCost.Text = "Projected Cost";
            this.lblProjectedCost.Top = 0.53125F;
            this.lblProjectedCost.Width = 1.3125F;
            // 
            // fldProjectedCost
            // 
            this.fldProjectedCost.Height = 0.1875F;
            this.fldProjectedCost.Left = 5.375F;
            this.fldProjectedCost.Name = "fldProjectedCost";
            this.fldProjectedCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.fldProjectedCost.Text = "Field1";
            this.fldProjectedCost.Top = 0.53125F;
            this.fldProjectedCost.Width = 1.21875F;
            // 
            // lblAmountUsed
            // 
            this.lblAmountUsed.Height = 0.1875F;
            this.lblAmountUsed.HyperLink = null;
            this.lblAmountUsed.Left = 4.03125F;
            this.lblAmountUsed.Name = "lblAmountUsed";
            this.lblAmountUsed.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.lblAmountUsed.Text = "YTD Net";
            this.lblAmountUsed.Top = 0.71875F;
            this.lblAmountUsed.Width = 1.3125F;
            // 
            // fldAmountUsed
            // 
            this.fldAmountUsed.Height = 0.1875F;
            this.fldAmountUsed.Left = 5.375F;
            this.fldAmountUsed.Name = "fldAmountUsed";
            this.fldAmountUsed.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.fldAmountUsed.Text = "Field1";
            this.fldAmountUsed.Top = 0.71875F;
            this.fldAmountUsed.Width = 1.21875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 4.03125F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.90625F;
            this.Line3.Width = 2.59375F;
            this.Line3.X1 = 6.625F;
            this.Line3.X2 = 4.03125F;
            this.Line3.Y1 = 0.90625F;
            this.Line3.Y2 = 0.90625F;
            // 
            // lblAvailable
            // 
            this.lblAvailable.Height = 0.1875F;
            this.lblAvailable.HyperLink = null;
            this.lblAvailable.Left = 4.03125F;
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.lblAvailable.Text = "Available Amount";
            this.lblAvailable.Top = 0.9375F;
            this.lblAvailable.Width = 1.3125F;
            // 
            // fldAvailable
            // 
            this.fldAvailable.Height = 0.1875F;
            this.fldAvailable.Left = 5.375F;
            this.fldAvailable.Name = "fldAvailable";
            this.fldAvailable.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.fldAvailable.Text = "Field2";
            this.fldAvailable.Top = 0.9375F;
            this.fldAvailable.Width = 1.21875F;
            // 
            // rptProjectSummary
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportEndedAndCanceled += new System.EventHandler(this.rptProjectSummary_ReportEndedAndCanceled);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDDebits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncumbrances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPending)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentDebits1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentCredits1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentNet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDDebits1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDCredits1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDNet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentDebits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentCredits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentNet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDDebits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDCredits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYTDNet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEncumbrance2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPending1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPending2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebitsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentCreditsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentNetTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDDebitsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDCreditsTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYTDNetTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncumbranceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPendingTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDNet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbrances;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPending;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonths;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjects;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentDebits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentCredits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentNet1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDDebits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDCredits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDNet1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblEncumbrance1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentDebits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentCredits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCurrentNet2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDDebits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDCredits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblYTDNet2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblEncumbrance2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPending1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPending2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentDebitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentCreditsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentNetTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDDebitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDCreditsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDNetTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbranceTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPendingTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line linTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjectSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjectedCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectedCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmountUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountUsed;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAvailable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAvailable;
	}
}
