﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class c1099ElectronicFile
	{
		//=========================================================
		private Decimal[] curTTotals = new Decimal[16 + 1];

		public void Set_curTaxTotals(int intIndex, Decimal curTotal)
		{
			curTTotals[intIndex] = curTotal;
		}

		public Decimal Get_curTaxTotals(int intIndex)
		{
			Decimal curTaxTotals = 0;
			curTaxTotals = curTTotals[intIndex];
			return curTaxTotals;
		}
	}
}
