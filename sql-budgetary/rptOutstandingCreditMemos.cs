﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptOutstandingCreditMemos.
	/// </summary>
	public partial class rptOutstandingCreditMemos : BaseSectionReport
	{
		public static rptOutstandingCreditMemos InstancePtr
		{
			get
			{
				return (rptOutstandingCreditMemos)Sys.GetInstance(typeof(rptOutstandingCreditMemos));
			}
		}

		protected rptOutstandingCreditMemos _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOutstandingCreditMemos	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		//clsDRWrapper rsVendorInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;

		public rptOutstandingCreditMemos()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outstanding Credit Memo List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			PageCounter = 0;
			curTotal = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			rsInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE (IsNull(Amount, 0) + IsNull(Adjustments, 0) - IsNull(Liquidated, 0)) > 0");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No credit memo information found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(rsInfo.Get_Fields("PostedDate"), "MM/dd/yy");
			fldVendor.Text = Strings.Format(rsInfo.Get_Fields_Int32("VendorNumber"), "00000");
			fldDescription.Text = FCConvert.ToString(rsInfo.Get_Fields_String("Description"));
			fldReference.Text = FCConvert.ToString(rsInfo.Get_Fields_String("MemoNumber"));
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields("Amount") + rsInfo.Get_Fields_Decimal("Adjustments") - rsInfo.Get_Fields_Decimal("Liquidated"), "#,##0.00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			curTotal += rsInfo.Get_Fields("Amount") + rsInfo.Get_Fields_Decimal("Adjustments") - rsInfo.Get_Fields_Decimal("Liquidated");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		

		private void rptOutstandingCreditMemos_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
