﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreditMemoDataEntry.
	/// </summary>
	public partial class frmCreditMemoDataEntry : BaseForm
	{
		public frmCreditMemoDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// FC:FINAL:MSH - Issue #797: add handler to fields, which disable delete line btn when fields get focus
			// Add handlers to fields
			// FC:FINAL:JEI: moved outside of InitializeComponent, because if you change the form within designer
			// the exisiting handlers will be removed by VS
			//
			this.cboJournal.Enter += new EventHandler(field_GotFocus);
			this.txtDate.Enter += new EventHandler(field_GotFocus);
			this.txtDescription.Enter += new EventHandler(field_GotFocus);
			this.txtMemoNumber.Enter += new EventHandler(field_GotFocus);
			this.txtAmount.Enter += new EventHandler(field_GotFocus);
			this.txtPeriod.Enter += new EventHandler(field_GotFocus);
			this.txtVendor.Enter += new EventHandler(field_GotFocus);
			this.txtAddress_0.Enter += new EventHandler(field_GotFocus);
			this.txtAddress_1.Enter += new EventHandler(field_GotFocus);
			this.txtAddress_2.Enter += new EventHandler(field_GotFocus);
			this.txtAddress_3.Enter += new EventHandler(field_GotFocus);
			this.txtCity.Enter += new EventHandler(field_GotFocus);
			this.txtState.Enter += new EventHandler(field_GotFocus);
			this.txtZip.Enter += new EventHandler(field_GotFocus);
			this.txtZip4.Enter += new EventHandler(field_GotFocus);
			this.cboJournal.GotFocus += new EventHandler(field_GotFocus);
			this.txtDate.GotFocus += new EventHandler(field_GotFocus);
			this.txtDescription.GotFocus += new EventHandler(field_GotFocus);
			this.txtMemoNumber.GotFocus += new EventHandler(field_GotFocus);
			this.txtAmount.GotFocus += new EventHandler(field_GotFocus);
			this.txtPeriod.GotFocus += new EventHandler(field_GotFocus);
			this.txtVendor.GotFocus += new EventHandler(field_GotFocus);
			this.txtAddress_0.GotFocus += new EventHandler(field_GotFocus);
			this.txtAddress_1.GotFocus += new EventHandler(field_GotFocus);
			this.txtAddress_2.GotFocus += new EventHandler(field_GotFocus);
			this.txtAddress_3.GotFocus += new EventHandler(field_GotFocus);
			this.txtCity.GotFocus += new EventHandler(field_GotFocus);
			this.txtState.GotFocus += new EventHandler(field_GotFocus);
			this.txtZip.GotFocus += new EventHandler(field_GotFocus);
			this.txtZip4.GotFocus += new EventHandler(field_GotFocus);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreditMemoDataEntry InstancePtr
		{
			get
			{
				return (frmCreditMemoDataEntry)Sys.GetInstance(typeof(frmCreditMemoDataEntry));
			}
		}

		protected frmCreditMemoDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int TaxCol;
		int AccountCol;
		int ProjectCol;
		int AmountCol;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		bool SearchFlag;
		int NumberCol;
		// vbPorter upgrade warning: TotalAmount As double	OnWrite(short, Decimal)
		double TotalAmount;
		// vbPorter upgrade warning: AmountToPay As double	OnWrite(short, string)
		double AmountToPay;
		bool EditFlag;
		string ErrorString = "";
		bool BadAccountFlag;
		int CurrJournal;
		public int VendorNumber;
		public bool OldJournal;
		public int OldJournalNumber;
		// vbPorter upgrade warning: OldPeriod As short --> As int	OnWrite(string)
		public int OldPeriod;
		public int FirstRecordShown;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		bool blnUnload;
		public bool blnFromVendorMaster;
		public bool blnFromVendor;
		bool blnJournalLocked;
		string strComboList = "";
		bool blnDirtyFlag;
		clsGridAccount vsGrid = new clsGridAccount();
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;
		bool openFormWhenThisClosed = false;
        bool activated = false;

		public void Init(ref cAPJournal aJourn)
		{
			if (!(aJourn == null))
			{
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = aJourn.JournalNumber;
				modRegistry.SaveRegistryKey("CURRCREDMEMOJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCredMemoEntry));
				cCreditMemo credMemo;
				credMemo = aJourn.CreditMemo;
				OldJournal = true;
				OldJournalNumber = modBudgetaryMaster.Statics.CurrentCredMemoEntry;
				OldPeriod = aJourn.Period;
				btnProcessNextEntry.Enabled = false;
				btnProcessPreviousEntry.Enabled = false;
				VendorNumber = aJourn.VendorNumber;
				txtPeriod.Text = FCConvert.ToString(aJourn.Period);
				if (aJourn.VendorNumber > 0)
				{
					txtVendor.Text = modValidateAccount.GetFormat_6(aJourn.VendorNumber.ToString(), 5);
					cVendor vend;
					vend = aJourn.Vendor;
					if (!(vend == null))
					{
						txtAddress[0].Text = vend.CheckName;
						txtAddress[1].Text = vend.CheckAddress1;
						txtAddress[2].Text = vend.CheckAddress2;
						txtAddress[3].Text = vend.CheckAddress3;
						txtCity.Text = vend.CheckCity;
						txtState.Text = vend.CheckState;
						txtZip.Text = vend.CheckZip;
						txtZip4.Text = vend.CheckZip4;
					}
					else
					{
						txtAddress[0].Text = "";
						txtAddress[1].Text = "";
						txtAddress[2].Text = "";
						txtAddress[3].Text = "";
						txtCity.Text = "";
						txtState.Text = "";
						txtZip.Text = "";
						txtZip4.Text = "";
					}
				}
				else
				{
					txtAddress[0].Text = aJourn.TempVendorName;
					txtAddress[1].Text = aJourn.TempVendorAddress1;
					txtAddress[2].Text = aJourn.TempVendorAddress2;
					txtAddress[3].Text = aJourn.TempVendorAddress3;
					txtCity.Text = aJourn.TempVendorCity;
					txtState.Text = aJourn.TempVendorState;
					txtZip.Text = aJourn.TempVendorZip;
					txtZip4.Text = aJourn.TempVendorZip4;
				}
				if (!(credMemo == null))
				{
					modBudgetaryMaster.Statics.blnCredMemoEdit = true;
					if (Information.IsDate(credMemo.CreditMemoDate))
					{
						txtDate.Text = Strings.Format(credMemo.CreditMemoDate, "MM/dd/yyyy");
					}
					txtDescription.Text = credMemo.Description;
					txtAmount.Text = Strings.Format(credMemo.Amount, "#,###,##0.00");
					txtMemoNumber.Text = credMemo.MemoNumber;
					FirstRecordShown = credMemo.ID;
					cCreditMemoDetail memoDetail;
					int counter = 0;
					if (credMemo.CreditMemoDetails.ItemCount() > 15)
					{
						vs1.Rows = credMemo.CreditMemoDetails.ItemCount() + 1;
					}
					credMemo.CreditMemoDetails.MoveFirst();
					counter = 1;
					while (credMemo.CreditMemoDetails.IsCurrent())
					{
						memoDetail = (cCreditMemoDetail)credMemo.CreditMemoDetails.GetCurrentItem();
						vs1.TextMatrix(counter, 0, FCConvert.ToString(memoDetail.ID));
						vs1.TextMatrix(counter, 1, memoDetail.Account);
						vs1.TextMatrix(counter, 2, memoDetail.Ten99);
						vs1.TextMatrix(counter, 3, memoDetail.Project);
						vs1.TextMatrix(counter, 4, FCConvert.ToString(memoDetail.Amount));
						credMemo.CreditMemoDetails.MoveNext();
						counter += 1;
					}
					if (counter < vs1.Rows - 1)
					{
						int intLimit = 0;
						intLimit = counter;
						for (counter = intLimit; counter <= vs1.Rows - 1; counter++)
						{
							vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
							vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
						}
					}
				}
			}
			this.Show(App.MainForm);
		}
		// FC:FINAL:MSH - Issue #797: add handler to fields, which disable delete line btn when fields get focus
		private void field_GotFocus(object sender, EventArgs e)
		{
			vs1_UnSelect();
		}
		// FC:FINAL:MSH - Issue #797: disable delete btn and remove highlight from table
		private void vs1_UnSelect()
		{
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.UnSelectAll();
			mnuProcessDelete.Enabled = false;
		}

		private void cboJournal_TextChanged(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
				return;
			}
			lblExpense.Text = "";
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			int counter;
			txtSearch.Text = "";
			frmSearch.Visible = false;
			cboJournal.Enabled = true;
			txtVendor.Enabled = true;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = true;
			}
			txtCity.Enabled = true;
			txtState.Enabled = true;
			txtZip.Enabled = true;
			txtZip4.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			txtMemoNumber.Enabled = true;
			txtAmount.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Focus();
			SearchFlag = false;
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdRetrieve_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: tempAccount As int	OnWrite(string)
			int tempAccount = 0;
			int counter;
			string temp = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			if (lstRecords.Items[lstRecords.SelectedIndex].Text.Length > 0)
			{
				// if a record is highlighted
				tempAccount = FCConvert.ToInt32(Strings.Mid(lstRecords.Items[lstRecords.SelectedIndex].Text, 1, 5));
				// get the account number of that record
			}
			if (tempAccount != 0)
			{
				// if there is a valid account number
				rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(tempAccount));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// if there is a record
					rs.MoveLast();
					rs.MoveFirst();
					cboJournal.Enabled = true;
					txtVendor.Enabled = true;
					for (counter = 0; counter <= 3; counter++)
					{
						txtAddress[counter].Enabled = true;
					}
					txtCity.Enabled = true;
					txtState.Enabled = true;
					txtZip.Enabled = true;
					txtZip4.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					txtMemoNumber.Enabled = true;
					txtAmount.Enabled = true;
					vs1.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "00000");
					txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("CheckCity"));
					txtState.Text = FCConvert.ToString(rs.Get_Fields_String("CheckState"));
					txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip"));
					txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip4"));
					lstRecords.Clear();
					// clear the list box
					frmInfo.Visible = false;
					// make the list of records invisible
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					//Application.DoEvents();
					txtDescription.Focus();
					SearchFlag = false;
				}
			}
		}

		public void cmdRetrieve_Click()
		{
			cmdRetrieve_Click(cmdRetrieve, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			int counter;
			frmInfo.Visible = false;
			lstRecords.Clear();
			cboJournal.Enabled = true;
			txtVendor.Enabled = true;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = true;
			}
			txtCity.Enabled = true;
			txtState.Enabled = true;
			txtZip.Enabled = true;
			txtZip4.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			txtMemoNumber.Enabled = true;
			txtAmount.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Focus();
			SearchFlag = false;
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			frmSearch.Visible = false;
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmSearch.Visible = true;
				txtSearch.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM (SELECT CheckName, CheckAddress1, VendorNumber, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DataEntryMessage, CheckName as VendorSearchName FROM VendorMaster WHERE CheckName like '" + modCustomReport.FixQuotes(txtSearch.Text) + "%' AND Status <> 'D' UNION ALL SELECT CheckName, CheckAddress1, VendorNumber, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DataEntryMessage, CheckAddress1 as VendorSearchName FROM VendorMaster WHERE upper(left(CheckAddress1, " + FCConvert.ToString(1 + txtSearch.Text.Length) + ")) = '%" + Strings.UCase(modCustomReport.FixQuotes(txtSearch.Text)) + "' AND upper(left(CheckName, " + FCConvert.ToString(txtSearch.Text.Length) + ")) <> '" + Strings.UCase(modCustomReport.FixQuotes(txtSearch.Text)) + "' AND Status <> 'D') as temp ORDER BY VendorSearchName");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					txtSearch.Text = "";
					frmWait.InstancePtr.Unload();
					cboJournal.Enabled = true;
					txtVendor.Enabled = true;
					for (counter = 0; counter <= 3; counter++)
					{
						txtAddress[counter].Enabled = true;
					}
					txtCity.Enabled = true;
					txtState.Enabled = true;
					txtZip.Enabled = true;
					txtZip4.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					txtMemoNumber.Enabled = true;
					txtAmount.Enabled = true;
					vs1.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "00000");
					txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					txtCity.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckCity")));
					txtState.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckState")));
					txtZip.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip")));
					txtZip4.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4")));
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtDescription.Focus();
				}
				else
				{
					txtSearch.Text = "";
					Fill_List();
					frmWait.InstancePtr.Unload();
					frmInfo.Visible = true;
					// make the list of records visible
					// show listbox with all records
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Records Found That Match That Name", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboJournal.Enabled = true;
				txtVendor.Enabled = true;
				for (counter = 0; counter <= 3; counter++)
				{
					txtAddress[counter].Enabled = true;
				}
				txtCity.Enabled = true;
				txtState.Enabled = true;
				txtZip.Enabled = true;
				txtZip4.Enabled = true;
				txtDate.Enabled = true;
				txtDescription.Enabled = true;
				txtMemoNumber.Enabled = true;
				txtAmount.Enabled = true;
				vs1.Enabled = true;
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmCreditMemoDataEntry_Activated(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (!blnFromVendorMaster)
			{
				if (!SearchFlag)
				{
					NumberCol = 0;
					TaxCol = 2;
					AccountCol = 1;
					ProjectCol = 3;
					AmountCol = 4;
					vs1.ColWidth(NumberCol, 0);
					vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1209));
					vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4546));
					vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1119));
					vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.2000));
					//FC:FINAL:DDU:#2898 - aligned columns
					vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
					//vs1.ColAlignment(TaxCol, (FCGrid.AlignmentSettings)4);
					vs1.TextMatrix(0, TaxCol, "1099");
					vs1.TextMatrix(0, AccountCol, "Account");
					vs1.TextMatrix(0, ProjectCol, "Proj");
					vs1.TextMatrix(0, AmountCol, "Amount");
					vs1.ColFormat(AmountCol, "#,###.00");
					vs1.ColComboList(TaxCol, "D|N|1|2|3|4|5|6|7|8|9");
					// "#0;D|#1;N|#2;1|#3;2|#4;3|#5;4|#6;5|#7;6|#8;7|#9;8|#10;9"
					strComboList = "# ; " + "\t" + " |";
					if (modBudgetaryMaster.Statics.ProjectFlag)
					{
						rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
						if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
						{
							do
							{
								strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
								rsProjects.MoveNext();
							}
							while (rsProjects.EndOfFile() != true);
							strComboList = Strings.Left(strComboList, strComboList.Length - 1);
						}
						vs1.ColComboList(ProjectCol, strComboList);
					}
					FillJournalCombo();
					if (!modBudgetaryMaster.Statics.blnCredMemoEdit)
					{
						for (counter = 1; counter <= vs1.Rows - 1; counter++)
						{
							vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
						}
						txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
						txtPeriod.Text = CalculatePeriod();
						if (!OldJournal)
						{
							cboJournal.SelectedIndex = 0;
							cboSaveJournal.SelectedIndex = 0;
						}
						else
						{
							SetCombo(OldJournalNumber, ref OldPeriod);
						}
						AmountToPay = 0;
						TotalAmount = 0;
					}
					else
					{
						// FC:FINAL:VGE - #862 Journals are already filled before 'if' statement.
						// FillJournalCombo();
						SetCombo(OldJournalNumber, ref OldPeriod);
						AmountToPay = FCConvert.ToDouble(txtAmount.Text);
						CalculateTotals();
						lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
					}
					txtAmount.Focus();
					txtVendor.Focus();
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					Form_Resize();
					CurrJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					// initialize current journal
					this.Refresh();
				}
				else
				{
					SearchFlag = false;
				}
			}
			else
			{
				blnFromVendorMaster = false;
			}
			blnDirtyFlag = false;
            activated = true;
		}

		private void frmCreditMemoDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1" && KeyCode != Keys.F12)
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmCreditMemoDataEntry_Load(object sender, System.EventArgs e)
		{
            blnJournalLocked = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
            vsGrid.AccountCol = 1;
			SetCustomFormColors();
			vs1.Select(1, AccountCol);
			blnPostJournal = false;
		}

		private void frmCreditMemoDataEntry_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(TaxCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1209));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.4046));
			vs1.ColWidth(ProjectCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1119));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1287));
			fraJournalSave.CenterToContainer(this.ClientArea);
			frmInfo.CenterToContainer(this.ClientArea);
			frmSearch.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmCreditMemoDataEntry_Resize(this, new System.EventArgs());
		}

		private void FrmCreditMemoDataEntry_FormClosed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (openFormWhenThisClosed)
			{
				frmGetCreditMemoDataEntry.InstancePtr.Show(App.MainForm);
			}
		}

		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (!modBudgetaryMaster.Statics.gboolClosingProgram)
			{
				if (blnDirtyFlag)
				{
					ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
				}
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
				//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
				openFormWhenThisClosed = true;
			}
			else
			{
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
			}
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmCreditMemoDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					KeyAscii = (Keys)0;
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vs1" && this.ActiveControl.GetName() != "txtSearch")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void lstRecords_DoubleClick(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			cmdRetrieve_Click();
		}

		private void mnuFileAddVendor_Click(object sender, System.EventArgs e)
		{
			modBudgetaryMaster.Statics.blnEdit = false;
			// else show we are creating a new vendor account
			modBudgetaryMaster.Statics.blnFromCredMemo = true;
			frmVendorMaster.InstancePtr.Show(App.MainForm);
			//FC:FINAL:DDU:#2874 - show form then hide this one to focus on correct tab
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			// show the blankform
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraJournalSave.Visible == true)
			{
				cmdOKSave_Click();
			}
			else
			{
				blnUnload = false;
				SaveInfo();
			}
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			if (vs1.TextMatrix(vs1.Row, TaxCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, ProjectCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, AmountCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					DeleteFlag = true;
					temp = vs1.Row;
					// If vs1.Row < vs1.rows - 1 Then
					// vs1.Row = vs1.Row + 1
					// Else
					// vs1.Row = vs1.Row - 1
					// End If
					// FC:FINAL:VGE - #589 Removing activation of Focus on MemoNumber
					//txtMemoNumber.Focus();
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					CalculateTotals();
					lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
					DeleteFlag = false;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, AccountCol);
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, AccountCol);
				}
			}
		}

		private void mnuProcessDeleteEntry_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			clsDRWrapper rsDetail = new clsDRWrapper();
			int lngRecordToDelete = 0;
			clsDRWrapper rsDelete = new clsDRWrapper();
			int lngRecord = 0;
			clsDRWrapper rsAPJournal = new clsDRWrapper();
			// make sure they want to delete this item
			counter = MessageBox.Show("Are you sure you want to delete this journal entry?", "Delete This Journal Entry?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (counter == DialogResult.Yes)
			{
				rsDetail.Execute("DELETE FROM CreditMemoDetail WHERE CreditMemoID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"), "Budgetary");
				if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						lngRecordToDelete = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						if (lngRecordToDelete == FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")))
						{
							modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						}
						else
						{
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							do
							{
								modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							}
							while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != lngRecordToDelete);
						}
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsAPJournal.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber") + " and VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber") + " and Reference = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("MemoNumber") + " ' AND Description = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description") + "' AND Amount = " + FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount") * -1));
						if (rsAPJournal.EndOfFile() != true && rsAPJournal.BeginningOfFile() != true)
						{
							rsDetail.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + rsAPJournal.Get_Fields_Int32("ID"), "Budgetary");
							rsAPJournal.Delete();
							rsAPJournal.Update();
						}
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						GetJournalData();
						btnProcessPreviousEntry.Enabled = false;
						if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
						{
							btnProcessNextEntry.Enabled = true;
						}
						else
						{
							btnProcessNextEntry.Enabled = false;
						}
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						if (btnProcessNextEntry.Enabled)
						{
							if (btnProcessPreviousEntry.Enabled == false)
							{
								mnuProcessNextEntry_Click();
								btnProcessPreviousEntry.Enabled = false;
							}
							else
							{
								mnuProcessNextEntry_Click();
							}
						}
						else
						{
							mnuProcessPreviousEntry_Click();
							btnProcessNextEntry.Enabled = false;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					rsDetail.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Status") + "' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
					if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
					{
						rsDetail.Edit();
						rsDetail.Set_Fields("Status", "D");
						rsDetail.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsDetail.Set_Fields("StatusChangeDate", DateTime.Today);
						rsDetail.Update();
					}
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					rsDelete.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"));
					do
					{
						lngRecord = FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID"));
						rsDelete.Delete();
						rsDelete.Update();
						rsDetail.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngRecord), "Budgetary");
						// rsDelete.MoveNext
					}
					while (rsDelete.EndOfFile() != true);
					modBudgetaryAccounting.Statics.SearchResults.Delete();
					modBudgetaryAccounting.Statics.SearchResults.Update();
					//Application.DoEvents();
					frmGetEncDataEntry.InstancePtr.FillJournals();
					mnuProcessQuit_Click();
				}
			}
		}

		private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			if (VendorNumber == FirstRecordShown)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
					{
						btnProcessNextEntry.Enabled = false;
					}
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
					else
					{
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							btnProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
				}
				else
				{
					btnProcessNextEntry.Enabled = false;
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			if (btnProcessPreviousEntry.Enabled == false)
			{
				btnProcessPreviousEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessNextEntry_Click()
		{
			mnuProcessNextEntry_Click(btnProcessNextEntry, new System.EventArgs());
		}

		private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
			if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				btnProcessPreviousEntry.Enabled = false;
			}
			else
			{
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						btnProcessPreviousEntry.Enabled = false;
					}
				}
			}
			if (btnProcessNextEntry.Enabled == false)
			{
				btnProcessNextEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessPreviousEntry_Click()
		{
			mnuProcessPreviousEntry_Click(btnProcessPreviousEntry, new System.EventArgs());
		}

		private void mnuProcessQuit_Click()
		{
			Close();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			int counter;
			SearchFlag = true;
			lblExpense.Text = "";
			frmSearch.Visible = true;
			cboJournal.Enabled = false;
			txtVendor.Enabled = false;
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Enabled = false;
			}
			txtCity.Enabled = false;
			txtState.Enabled = false;
			txtZip.Enabled = false;
			txtZip4.Enabled = false;
			txtDate.Enabled = false;
			txtDescription.Enabled = false;
			txtMemoNumber.Enabled = false;
			txtAmount.Enabled = false;
			vs1.Enabled = false;
			txtSearch.Focus();
		}

		public void mnuProcessSearch_Click()
		{
			mnuProcessSearch_Click(btnProcessSearch, new System.EventArgs());
		}

		private void txtAddress_Change(int Index, object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtAddress_Change(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - if the text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type (same with issue #795)
			if (sender.GetType() == typeof(T2KOverTypeBox))
			{
				int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
				txtAddress_Change(index, sender, e);
			}
		}

		private void txtAddress_Enter(int Index, object sender, System.EventArgs e)
		{
			int counter;
			int tempIndex;
			lblExpense.Text = "";
			if (!SearchFlag)
			{
				if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
				{
					txtVendor.Focus();
				}
				else if (Conversion.Val(txtVendor.Text) != 0)
				{
					txtDescription.Focus();
				}
			}
		}

		private void txtAddress_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - if the text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type (same with issue #795)
			if (sender.GetType() == typeof(T2KOverTypeBox))
			{
				int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
				txtAddress_Enter(index, sender, e);
			}
		}

		private void txtAddress_Validate_8(int Index, bool Cancel)
		{
			txtAddress_Validate(Index, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAddress_Validate(int Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			int counter;
			if (Index < 3)
			{
				if (Strings.Trim(txtAddress[Index + 1].Text) != "")
				{
					if (txtAddress[Index].Text == "")
					{
						for (counter = Index + 1; counter <= 3; counter++)
						{
							txtAddress[counter - 1].Text = txtAddress[counter].Text;
						}
						txtAddress[3].Text = "";
					}
				}
			}
		}

		private void txtAddress_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//FC:FINAL:MSH - Issue #795: if the vendor text field is empty, sender will be equal 0 (Int32 type), not 'T2KOverTypeBox' type
			if (sender.GetType() == typeof(Global.T2KOverTypeBox))
			{
				int index = txtAddress.IndexOf((Global.T2KOverTypeBox)sender);
				txtAddress_Validate(index, sender, e);
			}
		}

		private void txtAmount_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtAmount_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtAmount.Text = "";
				txtVendor.Focus();
			}
			lblExpense.Text = "";
		}

		private void txtAmount_Validate_2(bool Cancel)
		{
			txtAmount_Validate(txtAmount, new System.ComponentModel.CancelEventArgs(false));
		}

		private void txtAmount_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtAmount.Text != "")
			{
				AmountToPay = FCConvert.ToDouble(txtAmount.Text);
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
			}
			else
			{
				AmountToPay = 0;
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
			}
		}

		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtCity_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
		}

		private void txtDate_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtDate.Text == "00/00/00")
			{
				txtDate.Text = FCConvert.ToString(DateTime.Today);
			}
		}

		private void txtDescription_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtDescription_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
			lblExpense.Text = "";
		}

		private void txtDate_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
			lblExpense.Text = "";
		}

		private void txtMemoNumber_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtMemoNumber_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
			lblExpense.Text = "";
		}

		private void txtPeriod_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtState_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
		}

		private void txtVendor_Change(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
        }

		private void txtVendor_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#585 - the buttons should be always enabled
			//btnProcessSearch.Enabled = true;
			//btnFileAddVendor.Enabled = true;          
			lblExpense.Text = "";
		}

		private void txtVendor_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.A)
			{
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				modBudgetaryMaster.Statics.blnEdit = false;
				// else show we are creating a new vendor account
				modBudgetaryMaster.Statics.blnFromCredMemo = true;
				frmVendorMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
			}
			else if (KeyCode == Keys.S)
			{
				KeyCode = 0;
				mnuProcessSearch_Click();
			}
		}

		public void txtVendor_Validate_2(bool validate)
		{
			txtVendor_Validate(txtVendor, new System.ComponentModel.CancelEventArgs(validate));
		}

		public void txtVendor_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if ((txtVendor.SelectionStart > 0 && Conversion.Val(txtVendor.Text) != 0) || blnFromVendor)
			{
				blnFromVendor = false;
				if (!Information.IsNumeric(txtVendor.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtVendor.Text = "";
					e.Cancel = true;
				}
				else
				{
					temp = txtVendor.Text;
					txtVendor.Text = Strings.Format(temp, "00000");
					if (!GetVendorInfo())
					{
						e.Cancel = true;
					}
				}
			}
			else
			{
                //FC:FINAL:AM:#4456 - don't cancel validation before the form is not activated because this will lead to side effects (click for the buttons is not fired)
                //if (!SearchFlag)
                if (!SearchFlag && activated)
                {
					if (txtVendor.Text == "" || Conversion.Val(txtVendor.Text) == 0)
					{
						e.Cancel = true;
					}
				}
			}
		}

		private bool GetVendorInfo()
		{
			bool GetVendorInfo = false;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter;
			int counter2;
			string strLabel = "";
			counter = 1;
			rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + "AND Status <> 'D'");
			if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "S")
				{
					MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtVendor.Text = "";
					GetVendorInfo = false;
					return GetVendorInfo;
				}
				txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
				txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
				txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
				txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
				txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("CheckCity"));
				txtState.Text = FCConvert.ToString(rs.Get_Fields_String("CheckState"));
				txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip"));
				txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("CheckZip4"));
				for (counter = counter; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 1, "");
					vs1.TextMatrix(counter, 2, "");
					vs1.TextMatrix(counter, 3, "");
				}
				CalculateTotals();
				lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
				if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
				{
					MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				GetVendorInfo = true;
			}
			else
			{
				MessageBox.Show("There is no Vendor that matches that number", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Text = "";
				GetVendorInfo = false;
			}
			return GetVendorInfo;
		}

		private void Fill_List()
		{
			string temp;
			int I;
			//FC:FINAL:BBE:#584 - wisej do not supported spaces and tabs in ListViewItems. Use colums instead.
			lstRecords.Columns.Clear();
			lstRecords.Columns.Add("VendorNumber", 120);
			lstRecords.Columns.Add("VendorName", lstRecords.Width - lstRecords.Columns[0].Width - 20);
			temp = "";
			// this fills the listbox with the account information so that the user can dblclick
			// an account to choose it
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				if (!rs.IsFieldNull("VendorNumber"))
				{
					temp = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
					temp = Strings.Format(temp, "00000");
					temp += "\t";
				}
				if (FCConvert.ToString(rs.Get_Fields_String("CheckName")) != "")
					temp += rs.Get_Fields_String("CheckName") + " // " + rs.Get_Fields_String("CheckAddress1");
				if (I < rs.RecordCount())
					rs.MoveNext();
				lstRecords.AddItem(temp);
				// go to the next record
			}
		}

		private void txtZip_TextChanged(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtZip_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtZip4_TextChanged(object sender, System.EventArgs e)
		{
			blnDirtyFlag = true;
		}

		private void txtZip4_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
			}
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				blnDirtyFlag = true;
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
				return;
			}
			mnuProcessDelete.Enabled = true;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (this.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = AccountCol;
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
			}
			else if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
			}
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
				return;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (vs1.Col == NumberCol)
			{
				vs1.Col = 1;
			}
			else if (vs1.Col == ProjectCol)
			{
				if (modBudgetaryMaster.Statics.ProjectFlag && strComboList != "")
				{
					vs1.EditCell();
				}
				else
				{
					vs1.Col += 1;
				}
			}
			else if (vs1.Col == TaxCol)
			{
				vs1.EditMaxLength = 1;
				if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
				{
					if (Conversion.Val(txtVendor.Text) != 0)
					{
						vs1.TextMatrix(vs1.Row, TaxCol, "D");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, TaxCol, "N");
					}
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					if (vs1.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				vs1.EditMaxLength = 10;
				if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Trim(txtVendor.Text)) == 0)
			{
				txtVendor.Focus();
				return;
			}
			if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					if (vs1.Col == AmountCol)
					{
						if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
							vs1.EditSelLength = 1;
						}
						else
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == AccountCol && KeyCode != Keys.F9 && KeyCode != Keys.F12)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = 0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (modBudgetaryMaster.Statics.ProjectFlag == false || strComboList == "")
				{
					if (vs1.Col == AmountCol)
					{
						if (vs1.EditSelStart == 0)
						{
							KeyCode = 0;
							vs1.Col = AccountCol;
						}
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				if (KeyCode == Keys.C)
				{
					KeyCode = 0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
				if (KeyCode == Keys.Insert)
				{
					KeyCode = 0;
					if (vs1.Col == AmountCol)
					{
						if (Information.IsNumeric(vs1.TextMatrix(vs1.Row, vs1.Col)))
						{
							vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) + FCConvert.ToDecimal(lblRemAmount.Text));
						}
						else
						{
							vs1.EditText = FCConvert.ToString(FCConvert.ToDecimal(lblRemAmount.Text));
						}
					}
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string temp2 = "";
			float percent;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == AmountCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
				else if (vs1.EditText == "")
				{
					vs1.EditText = "0";
				}
				else
				{
					temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
					if (temp > 0)
					{
						if (vs1.EditText.Length > temp + 2)
						{
							vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
						}
					}
					CalculateTotals();
					lblRemAmount.Text = Strings.Format((AmountToPay - TotalAmount), "#,##0.00");
				}
			}
			else if (col == ProjectCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Project Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
					if (vs1.Col == AmountCol)
					{
						if (Strings.Trim(vs1.EditText) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.EditText);
						}
					}
					else
					{
						if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
						}
					}
				}
				else
				{
					if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
					{
						TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
					}
				}
			}
		}

		private void SaveDetails(ref int lngAPRecordNumber)
		{
			int counter;
			if (modBudgetaryMaster.Statics.blnCredMemoEdit)
			{
				rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE CreditMemoID = " + FCConvert.ToString(VendorNumber));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						rs.Delete();
						rs.Update();
					}
				}
				rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngAPRecordNumber));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						rs.Delete();
						rs.Update();
					}
				}
			}
			rs.OmitNullsOnInsert = true;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = 0");
					rs.AddNew();
					// if so add it
					rs.Set_Fields("CreditMemoID", VendorNumber);
					if (Strings.Trim(vs1.TextMatrix(counter, TaxCol)) == "")
					{
						rs.Set_Fields("1099", "D");
					}
					else
					{
						rs.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
					}
					rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					rs.Set_Fields("Amount", vs1.TextMatrix(counter, AmountCol));
					rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
					rs.Update();
					// update the database
				}
				else
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
					{
						rs.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
						rs.Delete();
						rs.Update();
					}
				}
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
					rs.AddNew();
					// if so add it
					rs.Set_Fields("APJournalID", lngAPRecordNumber);
					rs.Set_Fields("Description", "Credit Memo");
					if (Strings.Trim(vs1.TextMatrix(counter, TaxCol)) == "")
					{
						rs.Set_Fields("1099", "D");
					}
					else
					{
						rs.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
					}
					rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					rs.Set_Fields("Amount", FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) * -1);
					rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjectCol));
					rs.Update();
					// update the database
				}
				else
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
					{
						rs.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = " + FCConvert.ToString(Conversion.Val(vs1.TextMatrix(counter, NumberCol))));
						rs.Delete();
						rs.Update();
					}
				}
			}
		}

		private void GetJournalData()
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			SetCombo_6(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber")), FCConvert.ToInt16(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period")));
			txtVendor.Text = modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"), 5);
			rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
			txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
			txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
			txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
			txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
			txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
			txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
			txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
			txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("CreditMemoDate") != null)
				txtDate.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("CreditMemoDate"));
			txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			txtPeriod.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Period"), "00");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			txtAmount.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("Amount"), "#,##0.00");
			AmountToPay = FCConvert.ToDouble(txtAmount.Text);
			//FC:FINAL:MSH - IntPtr.Zero changed to null, because null it's similar to vbNull from the original VB project
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("MemoNumber") != null)
			{
				txtMemoNumber.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("MemoNumber"));
			}
			else
			{
				txtMemoNumber.Text = "";
			}
			VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
			GetJournalDetails();
		}

		private void ClearData()
		{
			int counter;
			txtVendor.Text = "";
			for (counter = 0; counter <= 3; counter++)
			{
				txtAddress[counter].Text = "";
			}
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			txtAmount.Text = "";
			txtDescription.Text = "";
			txtMemoNumber.Text = "";
			VendorNumber = 0;
			AmountToPay = 0;
			//FC:FINAL:AM:#i745 - format the date
			//txtDate.Text = FCConvert.ToString(DateTime.Today);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			vs1.Clear();
			lblExpense.Text = "";
			lblRemAmount.Text = "0.00";
			vs1.Rows = 16;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
			}
			//FC:FINAL:DDU:#2898 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
			vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.TextMatrix(0, TaxCol, "1099");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, ProjectCol, "Proj");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.Select(1, 1);
		}

		private void GetJournalDetails()
		{
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			rs2.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE CreditMemoID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 15)
				{
					vs1.Rows = rs2.RecordCount() + 1;
				}
				else
				{
					vs1.Rows = 16;
				}
				counter = 1;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("1099")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields("account")));
					vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("Amount")));
					rs2.MoveNext();
					counter += 1;
				}
				if (counter < vs1.Rows - 1)
				{
					for (counter = counter; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 1, "");
						vs1.TextMatrix(counter, 2, "");
						vs1.TextMatrix(counter, 3, "");
					}
				}
			}
		}
		// vbPorter upgrade warning: intPeriod As short	OnWrite(int, double)
		private void SetCombo_6(int x, int intPeriod)
		{
			SetCombo(x, ref intPeriod);
		}

		private void SetCombo(int x, ref int intPeriod)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4)// FC:FINAL:VGE - #862 Changeset for #820 reverted here. Rest of 820 is OK. 
				    && modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboJournal.Items[counter].ToString(), 19, 2))//FC:FINAL:VGE - #820 - #820 Starting position of 'Period' corrected
				//&& modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboJournal.Items[counter].ToString(), 29, 2))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			clsDRWrapper rsCreditMemo = new clsDRWrapper();
			int lngAP = 0;
			int lngTemp = 0;
			txtVendor.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("CreditMemo", true);
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status = 'P'");
				if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
				{
					MessageBox.Show("Changes to this journal cannot be saved because it has been posted.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					blnDirtyFlag = false;
					Close();
					return;
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text))
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (VendorNumber != 0)
							{
								rsDeleteInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod) + " AND ID <> " + FCConvert.ToString(VendorNumber));
								if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
								{
									// do nothing
								}
								else
								{
									rsDeleteInfo.Execute("DELETE FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod), "Budgetary");
									cboSaveJournal.Items.RemoveAt(cboSaveJournal.SelectedIndex);
									cboJournal.Items.RemoveAt(cboJournal.SelectedIndex);
								}
							}
							for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
								{
									cboSaveJournal.SelectedIndex = counter;
									cboJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "" || VendorNumber != 0)
							{
								if (VendorNumber != 0)
								{
									rsDeleteInfo.OpenRecordset("SELECT * FROM CreditMemo WHERE JournalNumber = " + FCConvert.ToString(OldJournalNumber) + " AND Period = " + FCConvert.ToString(OldPeriod) + " AND ID <> " + FCConvert.ToString(VendorNumber));
									if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
									{
										if (Strings.Trim(txtJournalDescription.Text) == "")
										{
											fraJournalSave.Visible = true;
											lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
											lblJournalSave.Visible = false;
											cboSaveJournal.Visible = false;
											txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Credit Memo";
											lblJournalDescription.Visible = true;
											txtJournalDescription.Visible = true;
											txtJournalDescription.Focus();
											txtJournalDescription.SelectionStart = 0;
											txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
											return;
										}
										else
										{
											Master.AddNew();
											Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
											Master.Set_Fields("Status", "E");
											Master.Set_Fields("Description", txtJournalDescription.Text);
											Master.Set_Fields("Type", "AC");
											Master.Set_Fields("CreditMemo", true);
											Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
											// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
											lngTemp = FCConvert.ToInt32(Master.Get_Fields("JournalNumber"));
											Master.Update();
											cboJournal.Clear();
											cboSaveJournal.Clear();
											FillJournalCombo();
											for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
											{
												if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == lngTemp && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
												{
													cboSaveJournal.SelectedIndex = counter;
													cboJournal.SelectedIndex = counter;
													break;
												}
											}
											Master.Reset();
										}
									}
									else
									{
										Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
										Master.Edit();
										Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
										Master.Update();
										cboJournal.Clear();
										cboSaveJournal.Clear();
										FillJournalCombo();
										for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
										{
											// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
											if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
											{
												cboSaveJournal.SelectedIndex = counter;
												cboJournal.SelectedIndex = counter;
												break;
											}
										}
										Master.Reset();
									}
								}
								else
								{
									Master.AddNew();
									Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
									Master.Set_Fields("Status", "E");
									Master.Set_Fields("Description", txtJournalDescription.Text);
									Master.Set_Fields("Type", "AC");
									Master.Set_Fields("CreditMemo", true);
									Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
									// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
									lngTemp = FCConvert.ToInt32(Master.Get_Fields("JournalNumber"));
									Master.Update();
									cboJournal.Clear();
									cboSaveJournal.Clear();
									FillJournalCombo();
									for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
									{
										if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == lngTemp && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
										{
											cboSaveJournal.SelectedIndex = counter;
											cboJournal.SelectedIndex = counter;
											break;
										}
									}
									Master.Reset();
								}
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Credit Memo";
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								txtJournalDescription.Focus();
								txtJournalDescription.SelectionStart = 0;
								txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
								return;
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "AC";
			clsJournalInfo.Period = FCConvert.ToString(Conversion.Val(txtPeriod.Text));
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			if (VendorNumber != 0)
			{
				OldPeriod = FCConvert.ToInt32(txtPeriod.Text);
				rsCreditMemo.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + FCConvert.ToString(VendorNumber));
				rsCreditMemo.Edit();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rsCreditMemo.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rsCreditMemo.Set_Fields("JournalNumber", TempJournal);
				}
				rsCreditMemo.Set_Fields("VendorNumber", txtVendor.Text);
				rsCreditMemo.Set_Fields("CreditMemoDate", txtDate.Text);
				rsCreditMemo.Set_Fields("Description", txtDescription.Text);
				rsCreditMemo.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rsCreditMemo.Set_Fields("MemoNumber", txtMemoNumber.Text);
				rsCreditMemo.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
				rsCreditMemo.Set_Fields("Fund", modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(1, AccountCol))), FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
				rsCreditMemo.Set_Fields("Status", "E");
				lngAP = FCConvert.ToInt32(rsCreditMemo.Get_Fields_Int32("OriginalAPRecord"));
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + rsCreditMemo.Get_Fields_Int32("OriginalAPRecord"));
				rs.Edit();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rs.Set_Fields("JournalNumber", TempJournal);
				}
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				rs.Set_Fields("CheckDate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rs.Set_Fields("Reference", txtMemoNumber.Text);
				rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text) * -1);
				rs.Set_Fields("Status", "E");
				rs.Update();
				VendorNumber = FCConvert.ToInt32(rsCreditMemo.Get_Fields_Int32("ID"));
				rsCreditMemo.Update();
			}
			else
			{
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
				rs.AddNew();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rs.Set_Fields("JournalNumber", TempJournal);
				}
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				rs.Set_Fields("CheckDate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rs.Set_Fields("Reference", txtMemoNumber.Text);
				rs.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text) * -1);
				rs.Set_Fields("Status", "E");
				rs.Update();
				rsCreditMemo.OmitNullsOnInsert = true;
				rsCreditMemo.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = 0");
				rsCreditMemo.AddNew();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rsCreditMemo.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					rsCreditMemo.Set_Fields("JournalNumber", TempJournal);
				}
				rsCreditMemo.Set_Fields("VendorNumber", txtVendor.Text);
				rsCreditMemo.Set_Fields("CreditMemoDate", txtDate.Text);
				rsCreditMemo.Set_Fields("Description", txtDescription.Text);
				rsCreditMemo.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				rsCreditMemo.Set_Fields("MemoNumber", txtMemoNumber.Text);
				rsCreditMemo.Set_Fields("Amount", FCConvert.ToDecimal(txtAmount.Text));
				rsCreditMemo.Set_Fields("Status", "E");
				rsCreditMemo.Set_Fields("Fund", modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(1, AccountCol))), FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
				rsCreditMemo.Set_Fields("OriginalAPRecord", rs.Get_Fields_Int32("ID"));
				lngAP = FCConvert.ToInt32(rsCreditMemo.Get_Fields_Int32("OriginalAPRecord"));
				rsCreditMemo.Update();
				VendorNumber = FCConvert.ToInt32(rsCreditMemo.Get_Fields_Int32("ID"));
			}
			SaveDetails(ref lngAP);
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modRegistry.SaveRegistryKey("CURRCREDMEMOJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modRegistry.SaveRegistryKey("CURRCREDMEMOJRNL", FCConvert.ToString(TempJournal));
				modBudgetaryMaster.Statics.CurrentCredMemoEntry = TempJournal;
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
				FillJournalCombo();
			}
			if (!modBudgetaryMaster.Statics.blnCredMemoEdit)
			{
				fraJournalSave.Visible = false;
				if (cboSaveJournal.SelectedIndex == 0)
				{
					FillJournalCombo();
					SetCombo_6(TempJournal, FCConvert.ToInt16(Conversion.Val(txtPeriod.Text)));
					txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Credit Memo";
				}
				ClearData();
				txtAmount.Focus();
				txtVendor.Focus();
				blnDirtyFlag = false;
				if (blnUnload)
				{
					if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCreditMemo))
					{
						if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							blnPostJournal = true;
						}
					}
					else
					{
						blnPostJournal = false;
						if (cboSaveJournal.SelectedIndex != 0)
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
						}
					}
					Close();
				}
			}
			else
			{
				if (btnProcessNextEntry.Enabled == false || blnUnload)
				{
					blnDirtyFlag = false;
					if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCreditMemo))
					{
						if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							blnPostJournal = true;
						}
					}
					else
					{
						blnPostJournal = false;
						if (cboSaveJournal.SelectedIndex != 0)
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
						}
						else
						{
							modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
						}
					}
					Close();
				}
				else
				{
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
					mnuProcessNextEntry_Click();
					blnDirtyFlag = false;
				}
			}
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.Width = 90;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		public void cmdOKSave_Click()
		{
			cmdOKSave_Click(cmdOKSave, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' AND CreditMemo = 1 ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					//FC:FINAL:MSH - Issue #591: this part of code add 'separators' to list, but these 'separators' there aren't in VB project.
					//while (cboJournal.ListCount <= counter)
					//{
					//    cboJournal.AddItem("");
					//}
					//while (cboSaveJournal.ListCount <= counter)
					//{
					//    cboSaveJournal.AddItem("");
					//}
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			bool NoSaveFlag = false;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: strFund As string	OnWriteFCConvert.ToInt32(
			string strFund = "";
			vs1.Col = 1;
			if (this.ActiveControl.GetName() != "txtAddress")
			{
				if (this.ActiveControl.GetName() == "txtAmount")
				{
					txtAmount_Validate_2(false);
				}
				txtDescription.Focus();
			}
			else
			{
				txtAddress_Validate_8(FCConvert.ToInt16(this.ActiveControl.GetIndex()), false);
			}
			//Application.DoEvents();
			if (txtVendor.Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			else if (Conversion.Val(txtVendor.Text) == 0 && txtAddress[0].Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtAddress[0].Focus();
				return;
			}
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must fill in the Description before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (txtPeriod.Text == "")
			{
				MessageBox.Show("You must fill in the Period before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPeriod.Focus();
				return;
			}
			if (txtMemoNumber.Text == "")
			{
				MessageBox.Show("You must fill in the Memo Number before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtMemoNumber.Focus();
				return;
			}
			CalculateTotals();
			if (FCConvert.ToDouble(Strings.Format((AmountToPay - TotalAmount), "####.00")) != 0)
			{
				MessageBox.Show("The Remaining Amount must be 0.00 before you can save", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (txtAmount.Text == "" || txtAmount.Text == ".00" || txtAmount.Text == "0.00")
			{
				MessageBox.Show("There must be an Amount to Pay in a Journal Entry", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					if (vs1.TextMatrix(counter, AccountCol) == "")
					{
						NoSaveFlag = true;
						break;
					}
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
					{
						answer = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Encumbrance Data Entry");
						}
					}
					if (strFund == "")
					{
						strFund = FCConvert.ToString(modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(counter, AccountCol)));
					}
					else
					{
						if (FCConvert.ToDouble(strFund) != modBudgetaryAccounting.GetFundFromAccount(vs1.TextMatrix(counter, AccountCol)))
						{
							MessageBox.Show("All the accounts used in a credit memo must be from the same fund.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
					}
				}
			}
			if (NoSaveFlag == true)
			{
				NoSaveFlag = false;
				MessageBox.Show("Each entry must have a 1099 Code, Account, Project Number, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (cboJournal.SelectedIndex == 0)
				{
					if (modBudgetaryAccounting.LockJournal() == false)
					{
						MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					blnJournalLocked = true;
					Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						TempJournal = 1;
					}
					answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (answer == DialogResult.Cancel)
					{
						modBudgetaryAccounting.UnlockJournal();
						blnJournalLocked = false;
						if (Master.IsntAnything())
						{
							// do nothing
						}
						else
						{
							Master.Reset();
						}
						return;
					}
					else if (answer == DialogResult.No)
					{
						fraJournalSave.Visible = true;
						lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
						lblJournalSave.Visible = true;
						cboSaveJournal.Visible = true;
						//FC:FINAL:DDU:#2873 - show textbox when selecting item in combobox
						cboSaveJournal.Text = "";
						lblJournalDescription.Visible = false;
						txtJournalDescription.Visible = false;
						cboSaveJournal.Focus();
						if (Master.IsntAnything())
						{
							// do nothing
						}
						else
						{
							Master.Reset();
						}
						return;
					}
					else
					{
						fraJournalSave.Visible = true;
						lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
						lblJournalSave.Visible = false;
						cboSaveJournal.Visible = false;
						txtJournalDescription.Text = Strings.Format(DateTime.Today, "MM/dd/yy") + " Credit Memo";
						lblJournalDescription.Visible = true;
						txtJournalDescription.Visible = true;
						txtJournalDescription.Focus();
						txtJournalDescription.SelectionStart = 0;
						txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
						return;
					}
				}
				else
				{
					SaveJournal();
				}
			}
		}

		private void mnuProcessSave_Click()
		{
			blnUnload = true;
			SaveInfo();
		}
		//FC:FINAL:MSH - Issue #818 - method for handling 'Save' button and 'F12' hotkey pressing
		private void cmdProcessSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click();
		}
	}
}
