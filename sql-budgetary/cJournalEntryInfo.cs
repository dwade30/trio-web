﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cJournalEntryInfo
	{
		//=========================================================
		private string strAccount = string.Empty;
		private double dblAmount;
		private string strDescription = string.Empty;
		private string strType = string.Empty;
		private string strDate = string.Empty;

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string EntryType
		{
			set
			{
				strType = value;
			}
			get
			{
				string EntryType = "";
				EntryType = strType;
				return EntryType;
			}
		}

		public string EntryDate
		{
			set
			{
				strDate = value;
			}
			get
			{
				string EntryDate = "";
				EntryDate = strDate;
				return EntryDate;
			}
		}
	}
}
