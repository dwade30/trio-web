﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWBD0000
{
	public class cTrialBalanceReport : cDetailsReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collFunds = new cGenericCollection();
		private cGenericCollection collFunds_AutoInitialized;

		private cGenericCollection collFunds
		{
			get
			{
				if (collFunds_AutoInitialized == null)
				{
					collFunds_AutoInitialized = new cGenericCollection();
				}
				return collFunds_AutoInitialized;
			}
			set
			{
				collFunds_AutoInitialized = value;
			}
		}

		private string strBeginningFund = string.Empty;
		private string strEndingFund = string.Empty;
		private int intStartPeriod;
		private int intEndPeriod;
		private bool boolIncludeNoActivity;

		public short StartPeriod
		{
			set
			{
				intStartPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short StartPeriod = 0;
				StartPeriod = FCConvert.ToInt16(intStartPeriod);
				return StartPeriod;
			}
		}

		public short EndPeriod
		{
			set
			{
				intEndPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EndPeriod = 0;
				EndPeriod = FCConvert.ToInt16(intEndPeriod);
				return EndPeriod;
			}
		}

		public bool IncludeAccountsWithNoActivity
		{
			set
			{
				boolIncludeNoActivity = value;
			}
			get
			{
				bool IncludeAccountsWithNoActivity = false;
				IncludeAccountsWithNoActivity = boolIncludeNoActivity;
				return IncludeAccountsWithNoActivity;
			}
		}

		public cGenericCollection Funds
		{
			get
			{
				cGenericCollection Funds = null;
				Funds = collFunds;
				return Funds;
			}
		}

		public string BeginningFund
		{
			set
			{
				strBeginningFund = value;
			}
			get
			{
				string BeginningFund = "";
				BeginningFund = strBeginningFund;
				return BeginningFund;
			}
		}

		public string EndingFund
		{
			set
			{
				strEndingFund = value;
			}
			get
			{
				string EndingFund = "";
				EndingFund = strEndingFund;
				return EndingFund;
			}
		}

		public cTrialBalanceReport() : base()
		{
			strBeginningFund = "";
			strEndingFund = "";
		}

		public cGenericCollection Details
		{
			get
			{
				cGenericCollection Details = null;
				Details = collFunds;
				return Details;
			}
		}
	}
}
