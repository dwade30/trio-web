﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using System.Web.UI.WebControls;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmJournals.
    /// </summary>
    public partial class frmJournals : BaseForm
    {
        public frmJournals()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmJournals InstancePtr
        {
            get
            {
                return (frmJournals)Sys.GetInstance(typeof(frmJournals));
            }
        }

        protected frmJournals _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cJournalsView theView = new cJournalsView();
        private cJournalsView theView_AutoInitialized;

        private cJournalsView theView
        {
            get
            {
                if (theView_AutoInitialized == null)
                {
                    theView_AutoInitialized = new cJournalsView();
                }
                return theView_AutoInitialized;
            }
            set
            {
                theView_AutoInitialized = value;
            }
        }

        const int COLUnpostedID = 0;
        const int COLUnpostedSelected = 1;
        const int COLUnpostedJournalNumber = 2;
        const int COLUnpostedPeriod = 3;
        const int COLUnpostedType = 4;
        const int COLUnpostedDescription = 5;
        const int COLUnpostedStatus = 6;
        const int COLPostedID = 0;
        const int COLPostedSelected = 1;
        const int COLPostedJournalNumber = 2;
        const int colPostedPeriod = 3;
        const int colPostedType = 4;
        const int colPostedDescription = 5;
        const int colPostedEntries = 6;
        const int colPostedDate = 7;
        private bool boolRefreshing;
        private bool boolDescriptionsEdited;

        private void cmdCreate_Click(object sender, System.EventArgs e)
        {
            CreateJournal();
        }

        private void cmdDelete_Click(object sender, System.EventArgs e)
        {
            DeleteJournal();
        }

        private void CreateJournal()
        {
            // find out what type of journal
            string strType;
            strType = frmJournalType.InstancePtr.Init();
            if (strType != "")
            {
                theView.CreateJournal(strType);
            }
        }

        private void PostJournals()
        {
            cmdPost.Enabled = false;
            if (CheckForSave())
            {
                theView.PostJournals(chkBreak.CheckState == CheckState.Checked);
                frmWait.InstancePtr.Unload();
                RefreshData();
            }
            cmdPost.Enabled = true;
        }

        private void DeleteJournal()
        {
            int lngRow;
            lngRow = GridUnposted.Row;
            if (lngRow < 1)
            {
                MessageBox.Show("You must select a journal before you may proceed", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int lngId;
            string strStatus;
            int lngJournalNumber;
            lngId = FCConvert.ToInt32(Math.Round(Conversion.Val(GridUnposted.TextMatrix(lngRow, COLUnpostedID))));
            lngJournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(GridUnposted.TextMatrix(lngRow, COLUnpostedJournalNumber))));
            strStatus = GridUnposted.TextMatrix(lngRow, COLUnpostedStatus);
            if (!theView.JournalCanBeDeleted(lngId))
            {
                MessageBox.Show("You cannot delete journals with a status other than Entered", "Invalid Journal", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (lngId > 0)
            {
                if (CheckForSave())
                {
                    // vbPorter upgrade warning: ans As short, int --> As DialogResult
                    DialogResult ans;
                    ans = MessageBox.Show("You are about to delete Journal " + FCConvert.ToString(lngJournalNumber) + ".  Do you wish to continue?", "Delete Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (ans == DialogResult.No)
                    {
                        return;
                    }
                    theView.DeleteAnUnpostedJournal(lngId);
                    RefreshUnpostedGrid();
                }
            }
        }

        private void EditUnpostedJournal()
        {
            int lngRow;
            lngRow = GridUnposted.Row;
            if (lngRow < 1)
            {
                MessageBox.Show("You must select a journal to edit", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int lngId;
            int lngJournalNumber;
            string strType = "";
            lngId = FCConvert.ToInt32(Math.Round(Conversion.Val(GridUnposted.TextMatrix(lngRow, COLUnpostedID))));
            lngJournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(GridUnposted.TextMatrix(lngRow, COLUnpostedJournalNumber))));
            if (lngId > 0)
            {
                if (CheckForSave())
                {
                    theView.EditUnpostedJournal(lngId);
                    RefreshUnpostedGrid();
                }
            }
        }

        private void cmdEdit_Click(object sender, System.EventArgs e)
        {
            EditUnpostedJournal();
        }

        private void cmdPost_Click(object sender, System.EventArgs e)
        {
            PostJournals();
        }

        private void cmdPrintPosted_Click(object sender, System.EventArgs e)
        {
            PrintPostedJournals();
        }

        private void cmdPrintUnposted_Click(object sender, System.EventArgs e)
        {
            PrintUnpostedJournals();
        }

        private void PrintUnpostedJournals()
        {
            theView.PrintUnposted(chkShowLiquidatedEncumbranceActivity.CheckState == CheckState.Checked);
        }

        private void PrintPostedJournals()
        {
            theView.PrintPosted(chkShowLiquidatedEncActivityPosted.CheckState == CheckState.Checked);
        }

        private void cmdReverse_Click(object sender, System.EventArgs e)
        {
            ReverseJournal(chkCorrection.CheckState == CheckState.Checked);
        }

        private void cmdSaveDescriptions_Click(object sender, System.EventArgs e)
        {
            SaveDescriptions();
        }

        private void frmJournals_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            }
            //end switch
        }

        private void frmJournals_Load(object sender, System.EventArgs e)
        {

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            CheckIncompleteJournals();
            SetupGrids();
        }

        private void CheckIncompleteJournals()
        {
            clsDRWrapper rsJournals = new clsDRWrapper();

            rsJournals.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries, Type, Period, JournalNumber FROM TempJournalEntries GROUP BY JournalNumber, Period, Type ORDER BY JournalNumber");
            if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
            {
	            rsJournals.MoveLast();
	            rsJournals.MoveFirst();

	            if (modBudgetaryAccounting.LockJournal() == false)
	            {
		            MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to build incomplete journals.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		            return;
	            }

	            do
	            {
		            BuildIncompletedJournal(rsJournals.Get_Fields("JournalNumber"));
		            rsJournals.MoveNext();
	            }
	            while (rsJournals.EndOfFile() != true);
            }
           
            modBudgetaryAccounting.UnlockJournal();
        }

        private void BuildIncompletedJournal(int tempJournalNumber)
        {
	        clsDRWrapper Master = new clsDRWrapper();
	        clsDRWrapper rsJournalInfo = new clsDRWrapper();
	        clsDRWrapper rsJournals = new clsDRWrapper();
            int counter;
	        int TempJournal = 0;

	        try
	        {
                Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
                if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                {
                    Master.MoveLast();
                    Master.MoveFirst();
                    // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
                }
                else
                {
                    TempJournal = 1;
                }
                rsJournals.OpenRecordset("SELECT * FROM TempJournalEntries WHERE JournalNumber = " + tempJournalNumber);
                Master.AddNew();
                Master.Set_Fields("JournalNumber", TempJournal);
                Master.Set_Fields("Status", "E");
                Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                Master.Set_Fields("StatusChangeDate", DateTime.Today);
                Master.Set_Fields("Description", rsJournals.Get_Fields_String("Description"));
                if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "W")
                {
                    Master.Set_Fields("Type", "CW");
                }
                else if (rsJournals.Get_Fields("Type") == "P")
                {
                    Master.Set_Fields("Type", "PY");
                }
                else
                {
                    Master.Set_Fields("Type", "GJ");
                }
                Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(rsJournals.Get_Fields("Period"))));
                Master.Update();
                Master.Reset();
                rsJournalInfo.OmitNullsOnInsert = true;
                rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
                do
                {
                    rsJournalInfo.AddNew();
                    rsJournalInfo.Set_Fields("Type", rsJournals.Get_Fields("Type"));
                    rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("TempJournalEntriesDate"));
                    rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields_String("Description"));
                    rsJournalInfo.Set_Fields("VendorNumber", rsJournals.Get_Fields_Int32("VendorNumber"));
                    rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
                    rsJournalInfo.Set_Fields("CheckNumber", rsJournals.Get_Fields("CheckNumber"));
                    rsJournalInfo.Set_Fields("Account", rsJournals.Get_Fields("Account"));
                    rsJournalInfo.Set_Fields("Project", rsJournals.Get_Fields_String("Project"));
                    rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("Amount"));
                    rsJournalInfo.Set_Fields("WarrantNumber", rsJournals.Get_Fields_Int32("WarrantNumber"));
                    rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields("Period"));
                    rsJournalInfo.Set_Fields("RCB", rsJournals.Get_Fields_String("RCB"));
                    rsJournalInfo.Set_Fields("Status", rsJournals.Get_Fields_String("Status"));
                    rsJournalInfo.Set_Fields("PO", rsJournals.Get_Fields("PO"));
                    rsJournalInfo.Set_Fields("1099", rsJournals.Get_Fields("1099"));
                    rsJournalInfo.Update(true);
                    rsJournals.Delete();
                    rsJournals.Update();
                }
                while (rsJournals.EndOfFile() != true);
               
                modBudgetaryAccounting.UpdateCalculationNeededTable(TempJournal);
            }
            catch (Exception e)
	        {
		        
	        }
        }

        private void frmJournals_Resize(object sender, System.EventArgs e)
        {
            ResizeGrids();
        }

        private void GridPosted_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (GridPosted.Col != COLPostedSelected)
            {
                e.Cancel = true;
            }
        }

        private void GridPosted_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int lngRow = GridPosted.GetFlexRowIndex(e.RowIndex);
            int lngCol = GridPosted.GetFlexColIndex(e.ColumnIndex);
            if (lngRow > 0)
            {
                if (lngCol == COLPostedSelected)
                {
                    int lngId = 0;
                    lngId = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosted.TextMatrix(lngRow, COLPostedID))));
                    if (FCConvert.CBool(GridPosted.TextMatrix(lngRow, COLPostedSelected)))
                    {
                        theView.UnSelectPostedJournal(lngId);
                    }
                    else
                    {
                        theView.SelectPostedJournal(lngId);
                    }
                }
            }
        }

        private void GridUnposted_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!(GridUnposted.Col == COLUnpostedSelected))
            {
                if (GridUnposted.Col == COLUnpostedDescription)
                {
                    if (theView.CanEditJournalDescriptions)
                    {
                        return;
                    }
                }
                e.Cancel = true;
            }
        }

        private void GridUnposted_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int lngRow = 0;
            int lngCol = 0;
            int lngId = 0;
            if (!boolRefreshing)
            {
                lngRow = GridUnposted.GetFlexRowIndex(e.RowIndex);
                lngCol = GridUnposted.GetFlexColIndex(e.ColumnIndex);
                if (lngRow > 0)
                {
                    lngId = FCConvert.ToInt32(Math.Round(Conversion.Val(GridUnposted.TextMatrix(lngRow, COLUnpostedID))));
                    if (lngCol == COLUnpostedSelected)
                    {
                        if (FCConvert.CBool(GridUnposted.TextMatrix(lngRow, COLUnpostedSelected)))
                        {
                            theView.UnSelectUnpostedJournal(lngId);
                        }
                        else
                        {
                            theView.SelectUnpostedJournal(lngId);
                        }
                    }
                    else if (lngCol == COLUnpostedDescription)
                    {
                        if (theView.CanEditJournalDescriptions)
                        {
                            theView.UpdateUnpostedDescription(lngId, GridUnposted.EditText);
                            boolDescriptionsEdited = true;
                        }
                    }
                }
            }
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }

        private void SetupGrids()
        {
            SetupGridUnposted();
            SetupGridPosted();
        }

        private void SetupGridUnposted()
        {
            GridUnposted.Rows = 1;
            GridUnposted.ColHidden(COLUnpostedID, true);
            GridUnposted.ColDataType(COLUnpostedSelected, FCGrid.DataTypeSettings.flexDTBoolean);
            GridUnposted.TextMatrix(0, COLUnpostedJournalNumber, "Journal");
            GridUnposted.TextMatrix(0, COLUnpostedPeriod, "Period");
            GridUnposted.TextMatrix(0, COLUnpostedType, "Type");
            GridUnposted.TextMatrix(0, COLUnpostedDescription, "Description");
            GridUnposted.TextMatrix(0, COLUnpostedStatus, "Status");
            //FC:FINAL:DDU:#2901 - aligned columns
            GridUnposted.ColAlignment(COLUnpostedJournalNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridUnposted.ColAlignment(COLUnpostedPeriod, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridUnposted.ColAlignment(COLUnpostedType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridUnposted.ColAlignment(COLUnpostedDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridUnposted.ColAlignment(COLUnpostedStatus, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void SetupGridPosted()
        {
            GridPosted.Rows = 1;
            GridPosted.ColHidden(COLPostedID, true);
            GridPosted.ColDataType(COLPostedSelected, FCGrid.DataTypeSettings.flexDTBoolean);
            GridPosted.TextMatrix(0, COLPostedJournalNumber, "Journal");
            GridPosted.TextMatrix(0, colPostedPeriod, "Period");
            GridPosted.TextMatrix(0, colPostedType, "Type");
            GridPosted.TextMatrix(0, colPostedDescription, "Description");
            GridPosted.TextMatrix(0, colPostedEntries, "Entries");
            GridPosted.TextMatrix(0, colPostedDate, "Posted");
            //FC:FINAL:DDU:#2901 - aligned columns
            GridPosted.ColAlignment(COLPostedJournalNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridPosted.ColAlignment(colPostedPeriod, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridPosted.ColAlignment(colPostedType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridPosted.ColAlignment(colPostedDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridPosted.ColAlignment(colPostedEntries, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridPosted.ColAlignment(colPostedDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

        private void ResizeGrids()
        {
            ResizeUnpostedGrid();
            ResizePostedGrid();
        }

        private void ResizeUnpostedGrid()
        {
            int lngGridWidth;
            lngGridWidth = GridUnposted.WidthOriginal;
            GridUnposted.ColWidth(COLUnpostedSelected, FCConvert.ToInt32(0.05 * lngGridWidth));
            GridUnposted.ColWidth(COLUnpostedJournalNumber, FCConvert.ToInt32(0.08 * lngGridWidth));
            GridUnposted.ColWidth(COLUnpostedDescription, FCConvert.ToInt32(0.4 * lngGridWidth));
            GridUnposted.ColWidth(COLUnpostedPeriod, FCConvert.ToInt32(0.07 * lngGridWidth));
            GridUnposted.ColWidth(COLUnpostedType, FCConvert.ToInt32(0.2 * lngGridWidth));
        }

        private void ResizePostedGrid()
        {
            int lngGridWidth;
            lngGridWidth = GridPosted.WidthOriginal;
            GridPosted.ColWidth(COLPostedSelected, FCConvert.ToInt32(0.05 * lngGridWidth));
            GridPosted.ColWidth(COLPostedJournalNumber, FCConvert.ToInt32(0.08 * lngGridWidth));
            GridPosted.ColWidth(colPostedDescription, FCConvert.ToInt32(0.4 * lngGridWidth));
            GridPosted.ColWidth(colPostedPeriod, FCConvert.ToInt32(0.07 * lngGridWidth));
            GridPosted.ColWidth(colPostedType, FCConvert.ToInt32(0.2 * lngGridWidth));
        }

        private void RefreshData()
        {
            boolRefreshing = true;
            CheckSecurity();
            RefreshUnpostedGrid();
            RefreshPostedgrid();
            boolRefreshing = false;
        }

        private void CheckSecurity()
        {
            cmdReverse.Enabled = theView.CanCreateReverseJournals;
            cmdDelete.Enabled = theView.CanDeleteUnpostedJournals;
            cmdPost.Enabled = theView.CanPost;
            cmdPrintPosted.Enabled = theView.CanPrint;
            cmdPrintUnposted.Enabled = theView.CanPrint;
            cmdSaveDescriptions.Enabled = theView.CanEditJournalDescriptions;
            cmdCreate.Enabled = theView.CanCreateEditJournals;
            cmdEdit.Enabled = theView.CanCreateEditJournals;
            btnEditGJDefaultTypes.Enabled = theView.CanEditGJ;
        }

        private void RefreshUnpostedGrid()
        {
            GridUnposted.Rows = 1;
            cJournalInfo uInfo;
            theView.UnpostedJournals.MoveFirst();
            int lngRow;
            while (theView.UnpostedJournals.IsCurrent())
            {
                uInfo = (cJournalInfo)theView.UnpostedJournals.GetCurrentItem();
                GridUnposted.Rows += 1;
                lngRow = GridUnposted.Rows - 1;
                GridUnposted.TextMatrix(lngRow, COLUnpostedID, FCConvert.ToString(uInfo.Id));
                GridUnposted.TextMatrix(lngRow, COLUnpostedDescription, uInfo.Description);
                GridUnposted.TextMatrix(lngRow, COLUnpostedJournalNumber, FCConvert.ToString(uInfo.JournalNumber));
                GridUnposted.TextMatrix(lngRow, COLUnpostedPeriod, FCConvert.ToString(uInfo.Period));
                GridUnposted.TextMatrix(lngRow, COLUnpostedStatus, uInfo.StatusDescription);
                GridUnposted.TextMatrix(lngRow, COLUnpostedSelected, FCConvert.ToString(uInfo.IsSelected));
                GridUnposted.TextMatrix(lngRow, COLUnpostedType, uInfo.JournalTypeDescription);
                theView.UnpostedJournals.MoveNext();
            }
            boolDescriptionsEdited = false;
        }

        private void RefreshPostedgrid()
        {
            GridPosted.Rows = 1;
            cJournalInfo pInfo;
            theView.PostedJournals.MoveFirst();
            int lngRow;
            while (theView.PostedJournals.IsCurrent())
            {
                pInfo = (cJournalInfo)theView.PostedJournals.GetCurrentItem();
                GridPosted.Rows += 1;
                lngRow = GridPosted.Rows - 1;
                GridPosted.TextMatrix(lngRow, COLPostedID, FCConvert.ToString(pInfo.Id));
                GridPosted.TextMatrix(lngRow, colPostedDescription, pInfo.Description);
                GridPosted.TextMatrix(lngRow, colPostedEntries, FCConvert.ToString(pInfo.NumberOfEntries));
                GridPosted.TextMatrix(lngRow, COLPostedJournalNumber, FCConvert.ToString(pInfo.JournalNumber));
                GridPosted.TextMatrix(lngRow, colPostedPeriod, FCConvert.ToString(pInfo.Period));
                GridPosted.TextMatrix(lngRow, COLPostedSelected, FCConvert.ToString(pInfo.IsSelected));
                GridPosted.TextMatrix(lngRow, colPostedType, pInfo.JournalTypeDescription);
                GridPosted.TextMatrix(lngRow, colPostedDate, pInfo.PostedDate.Length > 0 ? Strings.Format(pInfo.PostedDate, "MM/dd/yyyy") : "");
                theView.PostedJournals.MoveNext();
            }
        }

        private void ReverseJournal(bool boolCorrectingEntries)
        {
            int lngRow;
            lngRow = GridPosted.Row;
            if (lngRow < 1)
            {
                MessageBox.Show("You must select a journal to reverse", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            int lngId;
            lngId = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPosted.TextMatrix(lngRow, COLPostedID))));
            if (lngId > 0)
            {
                theView.ReverseJournal(lngId, boolCorrectingEntries);
                RefreshData();
            }
        }

        private void SaveDescriptions()
        {
            GridUnposted.Row = 0;
            if (boolDescriptionsEdited)
            {
                theView.SaveUnpostedDescriptions();
            }
            else
            {
                MessageBox.Show("No changes to save", "No Changes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void mnuSaveDescriptions_Click(object sender, System.EventArgs e)
        {
            SaveDescriptions();
        }

        private bool CheckForSave()
        {
            bool CheckForSave = false;
            CheckForSave = true;
            if (boolDescriptionsEdited)
            {
                // vbPorter upgrade warning: intReturn As short, int --> As DialogResult
                DialogResult intReturn;
                intReturn = MessageBox.Show("This action may refresh journal information and lose all changes to descriptions" + "\r\n" + "Do you want to save changes first?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (intReturn == DialogResult.Cancel)
                {
                    CheckForSave = false;
                    return CheckForSave;
                }
                if (intReturn == DialogResult.Yes)
                {
                    theView.SaveUnpostedDescriptions();
                }
            }
            return CheckForSave;
        }

        private void btnEditGJDefaultTypes_Click(object sender, EventArgs e)
        {
	        frmGetGJDefault.InstancePtr.Show(App.MainForm);
        }

        private void frmJournals_Activated(object sender, EventArgs e)
        {
	        theView.Refresh();
	        RefreshData();
        }

        private void GridUnposted_DoubleClick(object sender, EventArgs e)
        {
	        EditUnpostedJournal();
        }
    }
}
