﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cExpenseDetailItem
	{
		//=========================================================
		private string strAccount = string.Empty;
		private string strFund = string.Empty;
		private string strDepartment = string.Empty;
		private string strDivision = string.Empty;
		private string strExpense = string.Empty;
		private string strObject = string.Empty;
		private string strDescription = string.Empty;
		private string strJournalDate = string.Empty;
		private string strJournalType = string.Empty;
		private int intJournalNumber;
		private string strVendor = string.Empty;
		private int lngWarrantNumber;
		private int lngCheckNumber;
		private double dblCredit;
		private double dblDebit;
		private double dblCurrentBudget;
		private double dblUnexpendedBalance;
		private string strWarrant = string.Empty;
		private int intPeriod;
		private string strRCB = string.Empty;
		private string strTransactionDate = string.Empty;

		public string TransactionDate
		{
			set
			{
				strTransactionDate = value;
			}
			get
			{
				string TransactionDate = "";
				TransactionDate = strTransactionDate;
				return TransactionDate;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}

		public string Warrant
		{
			set
			{
				strWarrant = value;
			}
			get
			{
				string Warrant = "";
				Warrant = strWarrant;
				return Warrant;
			}
		}

		public double UnexpendedBalance
		{
			set
			{
				dblUnexpendedBalance = value;
			}
			get
			{
				double UnexpendedBalance = 0;
				UnexpendedBalance = dblUnexpendedBalance;
				return UnexpendedBalance;
			}
		}

		public double CurrentBudget
		{
			set
			{
				dblCurrentBudget = value;
			}
			get
			{
				double CurrentBudget = 0;
				CurrentBudget = dblCurrentBudget;
				return CurrentBudget;
			}
		}

		public double Credit
		{
			set
			{
				dblCredit = value;
			}
			get
			{
				double Credit = 0;
				Credit = dblCredit;
				return Credit;
			}
		}

		public double Debit
		{
			set
			{
				dblDebit = value;
			}
			get
			{
				double Debit = 0;
				Debit = dblDebit;
				return Debit;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string JournalDate
		{
			set
			{
				strJournalDate = value;
			}
			get
			{
				string JournalDate = "";
				JournalDate = strJournalDate;
				return JournalDate;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public short JournalNumber
		{
			set
			{
				intJournalNumber = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short JournalNumber = 0;
				JournalNumber = FCConvert.ToInt16(intJournalNumber);
				return JournalNumber;
			}
		}

		public string Vendor
		{
			set
			{
				strVendor = value;
			}
			get
			{
				string Vendor = "";
				Vendor = strVendor;
				return Vendor;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string Expense
		{
			set
			{
				strExpense = value;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string TheObject
		{
			set
			{
				strObject = value;
			}
			get
			{
				string TheObject = "";
				TheObject = strObject;
				return TheObject;
			}
		}

		public string Division
		{
			set
			{
				strDivision = value;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}
	}
}
