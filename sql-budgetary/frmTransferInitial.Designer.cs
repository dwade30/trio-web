﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmTransferInitial.
	/// </summary>
	partial class frmTransferInitial : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCComboBox cmbAllCategories;
		public fecherFoundation.FCLabel lblAllCategories;
		public fecherFoundation.FCFrame fraTransferTo;
		public fecherFoundation.FCFrame fraSelected;
		public fecherFoundation.FCCheckBox chkElected;
		public fecherFoundation.FCCheckBox chkApproved;
		public fecherFoundation.FCCheckBox chkCommittee;
		public fecherFoundation.FCCheckBox chkManager;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTransferInitial));
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.lblAll = new fecherFoundation.FCLabel();
			this.cmbAllCategories = new fecherFoundation.FCComboBox();
			this.lblAllCategories = new fecherFoundation.FCLabel();
			this.fraTransferTo = new fecherFoundation.FCFrame();
			this.fraSelected = new fecherFoundation.FCFrame();
			this.chkElected = new fecherFoundation.FCCheckBox();
			this.chkApproved = new fecherFoundation.FCCheckBox();
			this.chkCommittee = new fecherFoundation.FCCheckBox();
			this.chkManager = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTransferTo)).BeginInit();
			this.fraTransferTo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelected)).BeginInit();
			this.fraSelected.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkElected)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApproved)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCommittee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManager)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 366);
			this.BottomPanel.Size = new System.Drawing.Size(557, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.cmbAll);
			this.ClientArea.Controls.Add(this.lblAll);
			this.ClientArea.Controls.Add(this.fraTransferTo);
			this.ClientArea.Size = new System.Drawing.Size(557, 306);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(557, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(267, 30);
			this.HeaderText.Text = "Transfer Initial Request";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All Accounts",
				"Expense Accounts",
				"Revenue Accounts"
			});
			this.cmbAll.Location = new System.Drawing.Point(210, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(183, 40);
			this.cmbAll.TabIndex = 0;
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.optAllCategories_CheckedChanged);
			// 
			// lblAll
			// 
			this.lblAll.Location = new System.Drawing.Point(30, 44);
			this.lblAll.Name = "lblAll";
			this.lblAll.Size = new System.Drawing.Size(125, 16);
			this.lblAll.TabIndex = 1;
			this.lblAll.Text = "ACCOUNT CATEGORY";
			// 
			// cmbAllCategories
			// 
			this.cmbAllCategories.AutoSize = false;
			this.cmbAllCategories.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllCategories.FormattingEnabled = true;
			this.cmbAllCategories.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbAllCategories.Location = new System.Drawing.Point(180, 30);
			this.cmbAllCategories.Name = "cmbAllCategories";
			this.cmbAllCategories.Size = new System.Drawing.Size(163, 40);
			this.cmbAllCategories.TabIndex = 2;
			this.cmbAllCategories.SelectedIndexChanged += new System.EventHandler(this.cmbAllCategories_SelectedIndexChanged);
			// 
			// lblAllCategories
			// 
			this.lblAllCategories.Location = new System.Drawing.Point(20, 44);
			this.lblAllCategories.Name = "lblAllCategories";
			this.lblAllCategories.Size = new System.Drawing.Size(122, 16);
			this.lblAllCategories.TabIndex = 3;
			this.lblAllCategories.Text = "TRANSFER AMOUNTS";
			// 
			// fraTransferTo
			// 
			this.fraTransferTo.Controls.Add(this.fraSelected);
			this.fraTransferTo.Controls.Add(this.cmbAllCategories);
			this.fraTransferTo.Controls.Add(this.lblAllCategories);
			this.fraTransferTo.Location = new System.Drawing.Point(30, 90);
			this.fraTransferTo.Name = "fraTransferTo";
			this.fraTransferTo.Size = new System.Drawing.Size(360, 230);
			this.fraTransferTo.TabIndex = 0;
			this.fraTransferTo.Text = "Transfer Amounts To";
			// 
			// fraSelected
			// 
			this.fraSelected.AppearanceKey = "groupBoxNoBorders";
			this.fraSelected.Controls.Add(this.chkElected);
			this.fraSelected.Controls.Add(this.chkApproved);
			this.fraSelected.Controls.Add(this.chkCommittee);
			this.fraSelected.Controls.Add(this.chkManager);
			this.fraSelected.Enabled = false;
			this.fraSelected.Location = new System.Drawing.Point(0, 70);
			this.fraSelected.Name = "fraSelected";
			this.fraSelected.Size = new System.Drawing.Size(207, 146);
			this.fraSelected.TabIndex = 1;
			// 
			// chkElected
			// 
			this.chkElected.AutoSize = false;
			this.chkElected.Location = new System.Drawing.Point(20, 78);
			this.chkElected.Name = "chkElected";
			this.chkElected.Size = new System.Drawing.Size(143, 24);
			this.chkElected.TabIndex = 13;
			this.chkElected.Text = "Elected Request";
			// 
			// chkApproved
			// 
			this.chkApproved.AutoSize = false;
			this.chkApproved.Location = new System.Drawing.Point(20, 112);
			this.chkApproved.Name = "chkApproved";
			this.chkApproved.Size = new System.Drawing.Size(163, 24);
			this.chkApproved.TabIndex = 6;
			this.chkApproved.Text = "Approved Amounts";
			// 
			// chkCommittee
			// 
			this.chkCommittee.AutoSize = false;
			this.chkCommittee.Location = new System.Drawing.Point(20, 44);
			this.chkCommittee.Name = "chkCommittee";
			this.chkCommittee.Size = new System.Drawing.Size(167, 24);
			this.chkCommittee.TabIndex = 5;
			this.chkCommittee.Text = "Committee Request";
			// 
			// chkManager
			// 
			this.chkManager.AutoSize = false;
			this.chkManager.Location = new System.Drawing.Point(20, 10);
			this.chkManager.Name = "chkManager";
			this.chkManager.Size = new System.Drawing.Size(153, 24);
			this.chkManager.TabIndex = 4;
			this.chkManager.Text = "Manager Request";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileProcess,
				this.mnuSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileProcess
			// 
			this.mnuFileProcess.Index = 0;
			this.mnuFileProcess.Name = "mnuFileProcess";
			this.mnuFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileProcess.Text = "Process";
			this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 1;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(30, 340);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(99, 48);
			this.cmdProcess.TabIndex = 8;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmTransferInitial
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(557, 474);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTransferInitial";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Transfer Initial Request";
			this.Load += new System.EventHandler(this.frmTransferInitial_Load);
			this.Activated += new System.EventHandler(this.frmTransferInitial_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTransferInitial_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTransferTo)).EndInit();
			this.fraTransferTo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelected)).EndInit();
			this.fraSelected.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkElected)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApproved)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCommittee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManager)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdProcess;
	}
}
