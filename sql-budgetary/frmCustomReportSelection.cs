﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomReportSelection.
	/// </summary>
	public partial class frmCustomReportSelection : BaseForm
	{
		public frmCustomReportSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomReportSelection InstancePtr
		{
			get
			{
				return (frmCustomReportSelection)Sys.GetInstance(typeof(frmCustomReportSelection));
			}
		}

		protected frmCustomReportSelection _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         1/2/03
		// This form will be used to manage the custom reports
		// created by the user.  They can simply view a list of
		// existing reports, edit a custom report, or print one of
		// the custom reports
		// ********************************************************
		public bool blnPrint;
		clsDRWrapper rsReportInfo = new clsDRWrapper();

		private void cmdCancelRange_Click(object sender, System.EventArgs e)
		{
			fraRangeSelection.Visible = false;
			cboReports.Visible = true;
		}

		private void cmdProcessRange_Click(object sender, System.EventArgs e)
		{
			if (cboBeginningMonth.Visible == true)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cboSingleMonth.Visible == true)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			rsReportInfo.Edit();
			if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "A")
			{
				rsReportInfo.Set_Fields("LowDate", modBudgetaryMaster.Statics.FirstMonth);
				if (modBudgetaryMaster.Statics.FirstMonth > 1)
				{
					rsReportInfo.Set_Fields("HighDate", modBudgetaryMaster.Statics.FirstMonth - 1);
				}
				else
				{
					rsReportInfo.Set_Fields("HighDate", 12);
				}
			}
			else if (fraDateRange.Enabled == true)
			{
				if (cboSingleMonth.Visible == true)
				{
					rsReportInfo.Set_Fields("LowDate", cboSingleMonth.SelectedIndex + 1);
				}
				else
				{
					rsReportInfo.Set_Fields("LowDate", cboBeginningMonth.SelectedIndex + 1);
					rsReportInfo.Set_Fields("HighDate", cboEndingMonth.SelectedIndex + 1);
				}
			}
			rsReportInfo.Update(true);
			if (blnPrint)
			{
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				modDuplexPrinting.DuplexPrintReport(rptCustomReport.InstancePtr);
			}
			else
			{
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				frmReportViewer.InstancePtr.Init(rptCustomReport.InstancePtr);
			}
		}

		private void frmCustomReportSelection_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomReportSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReportSelection.FillStyle	= 0;
			//frmCustomReportSelection.ScaleWidth	= 3885;
			//frmCustomReportSelection.ScaleHeight	= 2475;
			//frmCustomReportSelection.LinkTopic	= "Form2";
			//frmCustomReportSelection.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsReports = new clsDRWrapper();
			rsReports.OpenRecordset("SELECT * FROM CustomReports ORDER BY Title");
			cboReports.Clear();
			cboReports.AddItem("Create New Report");
			if (rsReports.EndOfFile() != true && rsReports.BeginningOfFile() != true)
			{
				do
				{
					cboReports.AddItem(rsReports.Get_Fields_String("Title"));
					rsReports.MoveNext();
				}
				while (rsReports.EndOfFile() != true);
			}
			cboReports.SelectedIndex = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCustomReportSelection_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsReportData = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			if (cboReports.SelectedIndex == 0)
			{
				MessageBox.Show("You must select a report before you may continue with this process.", "Select Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				ans = MessageBox.Show("Are you sure you wish to delete this report?", "Delete Report?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rsReportData.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
					rsDetailInfo.Execute("DELETE FROM CustomReportsDetail WHERE CustomReportID = " + rsReportData.Get_Fields_Int32("ID"), "Budgetary");
					rsReportData.Delete();
					rsReportData.Update();
					cboReports.Items.RemoveAt(cboReports.SelectedIndex);
					MessageBox.Show("The report has been successfully deleted", "Report Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			blnPrint = false;
			cboReports.Visible = false;
			if (cboReports.SelectedIndex > 0)
			{
				rsReportInfo.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
				if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CheckMonthRange")))
					{
						fraRangeSelection.Visible = true;
                        cboReports.Visible = false;
						fraDateRange.Enabled = true;
						if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "S")
						{
							cboSingleMonth.Visible = true;
							cboSingleMonth.Focus();
							fraDateRange.Text = "Month";
							return;
						}
						else
						{
							cboBeginningMonth.Visible = true;
							cboEndingMonth.Visible = true;
							lblTo[0].Visible = true;
							cboBeginningMonth.Focus();
							fraDateRange.Text = "Date Range";
							return;
						}
					}
				}
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				frmReportViewer.InstancePtr.Init(rptCustomReport.InstancePtr);
			}
			else
			{
				mnuFileSetup_Click();
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			blnPrint = true;
			if (cboReports.SelectedIndex > 0)
			{
				rsReportInfo.OpenRecordset("SELECT * FROM CustomReports WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
				if (rsReportInfo.EndOfFile() != true && rsReportInfo.BeginningOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsReportInfo.Get_Fields_Boolean("CheckMonthRange")))
					{
						fraRangeSelection.Visible = true;
                        cboReports.Visible = false;
						fraDateRange.Enabled = true;
						if (FCConvert.ToString(rsReportInfo.Get_Fields_String("DateRange")) == "S")
						{
							cboSingleMonth.Visible = true;
							cboSingleMonth.Focus();
							fraDateRange.Text = "Month";
							return;
						}
						else
						{
							cboBeginningMonth.Visible = true;
							cboEndingMonth.Visible = true;
							lblTo[0].Visible = true;
							cboBeginningMonth.Focus();
							fraDateRange.Text = "Date Range";
							return;
						}
					}
				}
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				modDuplexPrinting.DuplexPrintReport(rptCustomReport.InstancePtr);
			}
			else
			{
				MessageBox.Show("You must select a report before you may continue with this process.", "No Report Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuFileSetup_Click(object sender, System.EventArgs e)
		{
			frmCreateCustomReport.InstancePtr.Show(App.MainForm);
		}

		public void mnuFileSetup_Click()
		{
			mnuFileSetup_Click(mnuFileSetup, new System.EventArgs());
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cboReports_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboReports.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 250, 0);
		}

		private void cmdFilePreview_Click(object sender, EventArgs e)
		{
			if (cboReports.Visible)
			{
				mnuFilePreview_Click(sender, e);
			}
			else if (fraRangeSelection.Visible)
			{
				cmdProcessRange_Click(sender, e);
			}
		}
	}
}
