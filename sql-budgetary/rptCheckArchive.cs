﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckArchive.
	/// </summary>
	public partial class rptCheckArchive : BaseSectionReport
	{
		public static rptCheckArchive InstancePtr
		{
			get
			{
				return (rptCheckArchive)Sys.GetInstance(typeof(rptCheckArchive));
			}
		}

		protected rptCheckArchive _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsCheckInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckArchive	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsCheckInfo = new clsDRWrapper();
		bool blnFirstAccount;
		int lngTotalCount;
		// vbPorter upgrade warning: curTotalAmount As Decimal	OnWrite(short, Decimal)
		Decimal curTotalAmount;
		bool blnChecksDone;

		public rptCheckArchive()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Check Archive Listing";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstAccount)
			{
				blnFirstAccount = false;
				bool executeCheckFirstNonCheck = false;
				if (blnChecksDone)
				{
					executeCheckFirstNonCheck = true;
					goto CheckFirstNonCheck;
				}
				else
				{
					if (rsCheckInfo.EndOfFile() == true)
					{
						blnChecksDone = true;
						executeCheckFirstNonCheck = true;
						goto CheckFirstNonCheck;
					}
					else
					{
						eArgs.EOF = false;
						this.Fields["Binder"].Value = "Check";
					}
				}
				CheckFirstNonCheck:
				;
				if (executeCheckFirstNonCheck)
				{
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						this.Fields["Binder"].Value = rsInfo.Get_Fields("Type");
					}
				}
			}
			else
			{
				bool executeCheckNextNonCheck = false;
				if (blnChecksDone)
				{
					rsInfo.MoveNext();
					executeCheckNextNonCheck = true;
					goto CheckNextNonCheck;
				}
				else
				{
					rsCheckInfo.MoveNext();
					if (rsCheckInfo.EndOfFile() == true)
					{
						blnChecksDone = true;
						executeCheckNextNonCheck = true;
						goto CheckNextNonCheck;
					}
					else
					{
						eArgs.EOF = false;
						this.Fields["Binder"].Value = "Check";
					}
				}
				CheckNextNonCheck:
				;
				if (executeCheckNextNonCheck)
				{
					if (rsInfo.EndOfFile() == true)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						this.Fields["Binder"].Value = rsInfo.Get_Fields("Type");
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmArchiveViewSelection.InstancePtr.cmbTypeAll.Text == "All" != true && (frmArchiveViewSelection.InstancePtr.chkDP.CheckState != CheckState.Checked && frmArchiveViewSelection.InstancePtr.chkIN.CheckState != CheckState.Checked && frmArchiveViewSelection.InstancePtr.chkOC.CheckState != CheckState.Checked && frmArchiveViewSelection.InstancePtr.chkOD.CheckState != CheckState.Checked && frmArchiveViewSelection.InstancePtr.chkRT.CheckState != CheckState.Checked))
			{
				rsInfo.OpenRecordset("SELECT * FROM CheckRecArchive WHERE ID < 0");
			}
			else
			{
				rsInfo.OpenRecordset(frmArchiveViewSelection.InstancePtr.strOtherSQL + " ORDER BY Type, CheckNumber");
			}
			if (frmArchiveViewSelection.InstancePtr.cmbTypeAll.Text == "All" != true && (frmArchiveViewSelection.InstancePtr.chkAP.CheckState != CheckState.Checked && frmArchiveViewSelection.InstancePtr.chkPY.CheckState != CheckState.Checked))
			{
				rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecArchive WHERE ID < 0");
			}
			else
			{
				rsCheckInfo.OpenRecordset(frmArchiveViewSelection.InstancePtr.strCheckSQL + " ORDER BY Type, CheckNumber");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Cancel();
					return;
				}
			}
			blnFirstAccount = true;
			blnChecksDone = false;
			lngTotalCount = 0;
			curTotalAmount = 0;
			lblBank.Text = App.MainForm.StatusBarText3;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmArchiveViewSelection.InstancePtr.Show();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnChecksDone)
			{
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar = rsInfo.Get_Fields_String("Type");
				if (vbPorterVar == "1")
				{
					fldType.Text = "AP";
				}
				else if (vbPorterVar == "2")
				{
					fldType.Text = "PY";
				}
				else if (vbPorterVar == "3")
				{
					fldType.Text = "DP";
				}
				else if (vbPorterVar == "4")
				{
					fldType.Text = "RT";
				}
				else if (vbPorterVar == "5")
				{
					fldType.Text = "IN";
				}
				else if (vbPorterVar == "6")
				{
					fldType.Text = "OC";
				}
				else if (vbPorterVar == "7")
				{
					fldType.Text = "OD";
				}
				fldCheckDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar1 = rsInfo.Get_Fields_String("Status");
				if (vbPorterVar1 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar1 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar1 == "D")
				{
					fldCode.Text = "DLTD";
				}
				fldStatusDate.Text = FCConvert.ToString(rsInfo.Get_Fields_DateTime("StatusDate"));
				fldPayee.Text = rsInfo.Get_Fields_String("Name");
				lngTotalCount += 1;
				//FC:FINAL:MSH - can't implicitly convert to decimal (same with issue #742)
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsCheckInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar2 = rsCheckInfo.Get_Fields_String("Type");
				if (vbPorterVar2 == "1")
				{
					fldType.Text = "AP";
				}
				else if (vbPorterVar2 == "2")
				{
					fldType.Text = "PY";
				}
				else if (vbPorterVar2 == "3")
				{
					fldType.Text = "DP";
				}
				else if (vbPorterVar2 == "4")
				{
					fldType.Text = "RT";
				}
				else if (vbPorterVar2 == "5")
				{
					fldType.Text = "IN";
				}
				else if (vbPorterVar2 == "6")
				{
					fldType.Text = "OC";
				}
				else if (vbPorterVar2 == "7")
				{
					fldType.Text = "OD";
				}
				fldCheckDate.Text = Strings.Format(rsCheckInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format(rsCheckInfo.Get_Fields("Amount"), "#,##0.00");
				string vbPorterVar3 = rsCheckInfo.Get_Fields_String("Status");
				if (vbPorterVar3 == "3")
				{
					fldCode.Text = "CSHD";
				}
				else if (vbPorterVar3 == "V")
				{
					fldCode.Text = "VOID";
				}
				else if (vbPorterVar3 == "D")
				{
					fldCode.Text = "DLTD";
				}
				fldStatusDate.Text = FCConvert.ToString(rsCheckInfo.Get_Fields_DateTime("StatusDate"));
				fldPayee.Text = rsCheckInfo.Get_Fields_String("Name");
				lngTotalCount += 1;
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curTotalAmount += rsCheckInfo.Get_Fields("Amount");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotalAmount, "#,##0.00");
			fldCount.Text = lngTotalCount.ToString();
			curTotalAmount = 0;
			lngTotalCount = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (blnChecksDone)
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				string vbPorterVar = rsInfo.Get_Fields_String("Type");
				if (vbPorterVar == "3")
				{
					fldTitle.Text = "Deposits";
				}
				else if (vbPorterVar == "4")
				{
					fldTitle.Text = "Returned Checks";
				}
				else if (vbPorterVar == "5")
				{
					fldTitle.Text = "Interest";
				}
				else if (vbPorterVar == "6")
				{
					fldTitle.Text = "Other Credits";
				}
				else if (vbPorterVar == "7")
				{
					fldTitle.Text = "Other Debits";
				}
			}
			else
			{
				fldTitle.Text = "Checks";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			int intTemp = 0;
			PageCounter += 1;
			if (PageCounter == 2)
			{
				lblTitle1.Visible = false;
				lblTitle2.Visible = false;
				lblBank.Visible = false;
				Label15.Top -= 1100 / 1440F;
				Label16.Top -= 1100 / 1440F;
				Label17.Top -= 1100 / 1440F;
				Label18.Top -= 1100 / 1440F;
				Label19.Top -= 1100 / 1440F;
				Label20.Top -= 1100 / 1440F;
				Label21.Top -= 1100 / 1440F;
				Label22.Top -= 1100 / 1440F;
				Line1.Y1 -= 1100 / 1440F;
				Line1.Y2 -= 1100 / 1440F;
			}
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
			if (frmArchiveViewSelection.InstancePtr.strTitle.Length > 60)
			{
				intTemp = Strings.InStr(55, frmArchiveViewSelection.InstancePtr.strTitle, ",", CompareConstants.vbBinaryCompare);
				if (intTemp > 0)
				{
					lblTitle1.Text = Strings.Left(frmArchiveViewSelection.InstancePtr.strTitle, intTemp);
					lblTitle2.Text = Strings.Right(frmArchiveViewSelection.InstancePtr.strTitle, frmArchiveViewSelection.InstancePtr.strTitle.Length - (intTemp + 1));
				}
				else
				{
					lblTitle1.Text = frmArchiveViewSelection.InstancePtr.strTitle;
					lblTitle2.Text = "";
				}
			}
			else
			{
				lblTitle1.Text = frmArchiveViewSelection.InstancePtr.strTitle;
				lblTitle2.Text = "";
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void rptCheckArchive_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCheckArchive.Caption	= "Check Archive Listing";
			//rptCheckArchive.Icon	= "rptCheckArchive.dsx":0000";
			//rptCheckArchive.Left	= 0;
			//rptCheckArchive.Top	= 0;
			//rptCheckArchive.Width	= 11880;
			//rptCheckArchive.Height	= 8595;
			//rptCheckArchive.StartUpPosition	= 3;
			//rptCheckArchive.SectionData	= "rptCheckArchive.dsx":058A;
			//End Unmaped Properties
		}

		private void rptCheckArchive_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
