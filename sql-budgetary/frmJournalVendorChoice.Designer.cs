﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournalVendorChoice.
	/// </summary>
	partial class frmJournalVendorChoice : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public FCGrid GridVendors;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.GridVendors = new fecherFoundation.FCGrid();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVendors)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOK);
			this.BottomPanel.Location = new System.Drawing.Point(0, 438);
			this.BottomPanel.Size = new System.Drawing.Size(798, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.GridVendors);
			this.ClientArea.Size = new System.Drawing.Size(798, 378);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(798, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(91, 30);
			this.HeaderText.Text = "Vendor";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "toolbarButton";
			this.cmdCancel.Location = new System.Drawing.Point(3, 352);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(68, 23);
			this.cmdCancel.TabIndex = 2;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Visible = false;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "acceptButton";
			this.cmdOK.Location = new System.Drawing.Point(359, 30);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(79, 48);
			this.cmdOK.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Text = "Ok";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// GridVendors
			// 
			this.GridVendors.AllowSelection = false;
			this.GridVendors.AllowUserToResizeColumns = false;
			this.GridVendors.AllowUserToResizeRows = false;
			this.GridVendors.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridVendors.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridVendors.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridVendors.BackColorBkg = System.Drawing.Color.Empty;
			this.GridVendors.BackColorFixed = System.Drawing.Color.Empty;
			this.GridVendors.BackColorSel = System.Drawing.Color.Empty;
			this.GridVendors.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridVendors.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridVendors.ColumnHeadersHeight = 30;
			this.GridVendors.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridVendors.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridVendors.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridVendors.FixedCols = 0;
			this.GridVendors.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridVendors.FrozenCols = 0;
			this.GridVendors.GridColor = System.Drawing.Color.Empty;
			this.GridVendors.Location = new System.Drawing.Point(30, 30);
			this.GridVendors.Name = "GridVendors";
			this.GridVendors.ReadOnly = true;
			this.GridVendors.RowHeadersVisible = false;
			this.GridVendors.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridVendors.RowHeightMin = 0;
			this.GridVendors.Rows = 1;
			this.GridVendors.ShowColumnVisibilityMenu = false;
			this.GridVendors.Size = new System.Drawing.Size(738, 318);
			this.GridVendors.StandardTab = true;
			this.GridVendors.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridVendors.TabIndex = 0;
			this.GridVendors.DoubleClick += new System.EventHandler(this.GridVendors_DblClick);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcess
			});
			this.MainMenu1.Name = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			this.mnuProcess.Visible = false;
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmJournalVendorChoice
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(798, 546);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmJournalVendorChoice";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vendor";
			this.Load += new System.EventHandler(this.frmJournalVendorChoice_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmJournalVendorChoice_KeyDown);
			this.Resize += new System.EventHandler(this.frmJournalVendorChoice_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridVendors)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
