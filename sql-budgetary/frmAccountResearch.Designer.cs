﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAccountResearch.
	/// </summary>
	partial class frmAccountResearch : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSingleAccount;
		public fecherFoundation.FCLabel lblSingleAccount;
		public fecherFoundation.FCComboBox cmbLedger;
		public fecherFoundation.FCLabel lblLedger;
		public fecherFoundation.FCComboBox cmbAllJournals;
		public fecherFoundation.FCLabel lblAllJournals;
		public fecherFoundation.FCComboBox cmbDetail;
		public fecherFoundation.FCLabel lblDetail;
		public fecherFoundation.FCComboBox cmbDebitsCredits;
		public fecherFoundation.FCLabel lblDebitsCredits;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public fecherFoundation.FCComboBox cboBeginningFund;
		public fecherFoundation.FCComboBox cboEndingFund;
		public fecherFoundation.FCComboBox cboSingleFund;
		public FCGrid vsLowAccount;
		public FCGrid vsSingleAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCCheckBox chkDateRange;
		public fecherFoundation.FCCheckBox chkMonthRange;
		public fecherFoundation.FCFrame fraMonthRange;
		public fecherFoundation.FCComboBox cboLowMonth;
		public fecherFoundation.FCComboBox cboHighMonth;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkAP;
		public fecherFoundation.FCCheckBox chkCD;
		public fecherFoundation.FCCheckBox chkCR;
		public fecherFoundation.FCCheckBox chkGJ;
		public fecherFoundation.FCCheckBox chkPY;
		public fecherFoundation.FCCheckBox chkEN;
		public fecherFoundation.FCFrame fraJournalRange;
		public fecherFoundation.FCComboBox cboEndJournal;
		public fecherFoundation.FCComboBox cboStartJournal;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCGrid vsSort;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCButton btnFilePrint;
		public fecherFoundation.FCButton btnFilePreview;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountResearch));
			this.cmbSingleAccount = new fecherFoundation.FCComboBox();
			this.lblSingleAccount = new fecherFoundation.FCLabel();
			this.cmbLedger = new fecherFoundation.FCComboBox();
			this.lblLedger = new fecherFoundation.FCLabel();
			this.cmbAllJournals = new fecherFoundation.FCComboBox();
			this.lblAllJournals = new fecherFoundation.FCLabel();
			this.cmbDetail = new fecherFoundation.FCComboBox();
			this.lblDetail = new fecherFoundation.FCLabel();
			this.cmbDebitsCredits = new fecherFoundation.FCComboBox();
			this.lblDebitsCredits = new fecherFoundation.FCLabel();
			this.fraDeptRange = new fecherFoundation.FCFrame();
			this.cboSingleDept = new fecherFoundation.FCComboBox();
			this.cboBeginningDept = new fecherFoundation.FCComboBox();
			this.cboEndingDept = new fecherFoundation.FCComboBox();
			this.cboBeginningFund = new fecherFoundation.FCComboBox();
			this.cboEndingFund = new fecherFoundation.FCComboBox();
			this.cboSingleFund = new fecherFoundation.FCComboBox();
			this.vsLowAccount = new fecherFoundation.FCGrid();
			this.vsSingleAccount = new fecherFoundation.FCGrid();
			this.vsHighAccount = new fecherFoundation.FCGrid();
			this.lblTo_3 = new fecherFoundation.FCLabel();
			this.lblTo_1 = new fecherFoundation.FCLabel();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.fraDate = new fecherFoundation.FCFrame();
			this.chkDateRange = new fecherFoundation.FCCheckBox();
			this.fraMonthRange = new fecherFoundation.FCFrame();
			this.cboLowMonth = new fecherFoundation.FCComboBox();
			this.cboHighMonth = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtLowDate = new Global.T2KDateBox();
			this.txtHighDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.chkMonthRange = new fecherFoundation.FCCheckBox();
			this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkAP = new fecherFoundation.FCCheckBox();
			this.chkCD = new fecherFoundation.FCCheckBox();
			this.chkCR = new fecherFoundation.FCCheckBox();
			this.chkGJ = new fecherFoundation.FCCheckBox();
			this.chkPY = new fecherFoundation.FCCheckBox();
			this.chkEN = new fecherFoundation.FCCheckBox();
			this.fraJournalRange = new fecherFoundation.FCFrame();
			this.cboEndJournal = new fecherFoundation.FCComboBox();
			this.cboStartJournal = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.vsSort = new fecherFoundation.FCGrid();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.btnFilePrint = new fecherFoundation.FCButton();
			this.btnFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
			this.fraDeptRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSingleAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonthRange)).BeginInit();
			this.fraMonthRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonthRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkGJ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalRange)).BeginInit();
			this.fraJournalRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraDeptRange);
			this.ClientArea.Controls.Add(this.cmbLedger);
			this.ClientArea.Controls.Add(this.lblLedger);
			this.ClientArea.Controls.Add(this.fraDate);
			this.ClientArea.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.cmbDetail);
			this.ClientArea.Controls.Add(this.lblDetail);
			this.ClientArea.Controls.Add(this.cmbDebitsCredits);
			this.ClientArea.Controls.Add(this.lblDebitsCredits);
			this.ClientArea.Controls.Add(this.vsSort);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFilePrint);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Account Research";
			// 
			// cmbSingleAccount
			// 
			this.cmbSingleAccount.AutoSize = false;
			this.cmbSingleAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSingleAccount.FormattingEnabled = true;
			this.cmbSingleAccount.Items.AddRange(new object[] {
				"All",
                "Single Account",
                "Account Range",
				"Single Department",
				"Department Range"
			});
			this.cmbSingleAccount.Location = new System.Drawing.Point(154, 30);
			this.cmbSingleAccount.Name = "cmbSingleAccount";
			this.cmbSingleAccount.Size = new System.Drawing.Size(242, 40);
			this.cmbSingleAccount.TabIndex = 45;
			this.cmbSingleAccount.SelectedIndexChanged += new System.EventHandler(this.optSingleAccount_CheckedChanged);
			// 
			// lblSingleAccount
			// 
			this.lblSingleAccount.AutoSize = true;
			this.lblSingleAccount.Location = new System.Drawing.Point(20, 44);
			this.lblSingleAccount.Name = "lblSingleAccount";
			this.lblSingleAccount.Size = new System.Drawing.Size(77, 15);
			this.lblSingleAccount.TabIndex = 46;
			this.lblSingleAccount.Text = "ACCOUNTS";
			this.lblSingleAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbLedger
			// 
			this.cmbLedger.AutoSize = false;
			this.cmbLedger.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLedger.FormattingEnabled = true;
            this.cmbLedger.Items.AddRange(new object[] {
                "Expense",
                "Revenue",
                "General Ledger"
            });
			this.cmbLedger.Location = new System.Drawing.Point(189, 30);
			this.cmbLedger.Name = "cmbLedger";
			this.cmbLedger.Size = new System.Drawing.Size(237, 40);
			this.cmbLedger.TabIndex = 38;
			this.cmbLedger.SelectedIndexChanged += new System.EventHandler(this.cmbLedger_SelectedIndexChanged);
			// 
			// lblLedger
			// 
			this.lblLedger.AutoSize = true;
			this.lblLedger.Location = new System.Drawing.Point(30, 44);
			this.lblLedger.Name = "lblLedger";
			this.lblLedger.Size = new System.Drawing.Size(104, 15);
			this.lblLedger.TabIndex = 39;
			this.lblLedger.Text = "ACCOUNT TYPE";
			this.lblLedger.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbAllJournals
			// 
			this.cmbAllJournals.AutoSize = false;
			this.cmbAllJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllJournals.FormattingEnabled = true;
			this.cmbAllJournals.Items.AddRange(new object[] {
				"All Journals",
				"Journal Range"
			});
			this.cmbAllJournals.Location = new System.Drawing.Point(150, 30);
			this.cmbAllJournals.Name = "cmbAllJournals";
			this.cmbAllJournals.Size = new System.Drawing.Size(206, 40);
			this.cmbAllJournals.TabIndex = 8;
			this.cmbAllJournals.SelectedIndexChanged += new System.EventHandler(this.optAllJournals_CheckedChanged);
			// 
			// lblAllJournals
			// 
			this.lblAllJournals.AutoSize = true;
			this.lblAllJournals.Location = new System.Drawing.Point(20, 44);
			this.lblAllJournals.Name = "lblAllJournals";
			this.lblAllJournals.Size = new System.Drawing.Size(73, 15);
			this.lblAllJournals.TabIndex = 9;
			this.lblAllJournals.Text = "JOURNALS";
			// 
			// cmbDetail
			// 
			this.cmbDetail.AutoSize = false;
			this.cmbDetail.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDetail.FormattingEnabled = true;
			this.cmbDetail.Items.AddRange(new object[] {
				"Detail",
				"Summary"
			});
			this.cmbDetail.Location = new System.Drawing.Point(189, 521);
			this.cmbDetail.Name = "cmbDetail";
			this.cmbDetail.Size = new System.Drawing.Size(220, 40);
			this.cmbDetail.TabIndex = 40;
			// 
			// lblDetail
			// 
			this.lblDetail.AutoSize = true;
			this.lblDetail.Location = new System.Drawing.Point(30, 535);
			this.lblDetail.Name = "lblDetail";
			this.lblDetail.Size = new System.Drawing.Size(101, 15);
			this.lblDetail.TabIndex = 41;
			this.lblDetail.Text = "ACCOUNT INFO";
			this.lblDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbDebitsCredits
			// 
			this.cmbDebitsCredits.AutoSize = false;
			this.cmbDebitsCredits.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDebitsCredits.FormattingEnabled = true;
			this.cmbDebitsCredits.Items.AddRange(new object[] {
				"Debits / Credits",
				"Net"
			});
			this.cmbDebitsCredits.Location = new System.Drawing.Point(189, 583);
			this.cmbDebitsCredits.Name = "cmbDebitsCredits";
			this.cmbDebitsCredits.Size = new System.Drawing.Size(220, 40);
			this.cmbDebitsCredits.TabIndex = 42;
			// 
			// lblDebitsCredits
			// 
			this.lblDebitsCredits.AutoSize = true;
			this.lblDebitsCredits.Location = new System.Drawing.Point(30, 596);
			this.lblDebitsCredits.Name = "lblDebitsCredits";
			this.lblDebitsCredits.Size = new System.Drawing.Size(96, 15);
			this.lblDebitsCredits.TabIndex = 43;
			this.lblDebitsCredits.Text = "ACTIVITY INFO";
			this.lblDebitsCredits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraDeptRange
			// 
			this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
			this.fraDeptRange.Controls.Add(this.cboSingleDept);
			this.fraDeptRange.Controls.Add(this.cboBeginningDept);
			this.fraDeptRange.Controls.Add(this.cmbSingleAccount);
			this.fraDeptRange.Controls.Add(this.lblSingleAccount);
			this.fraDeptRange.Controls.Add(this.cboEndingDept);
			this.fraDeptRange.Controls.Add(this.cboBeginningFund);
			this.fraDeptRange.Controls.Add(this.cboEndingFund);
			this.fraDeptRange.Controls.Add(this.cboSingleFund);
			this.fraDeptRange.Controls.Add(this.vsLowAccount);
			this.fraDeptRange.Controls.Add(this.vsSingleAccount);
			this.fraDeptRange.Controls.Add(this.vsHighAccount);
			this.fraDeptRange.Controls.Add(this.lblTo_3);
			this.fraDeptRange.Controls.Add(this.lblTo_1);
			this.fraDeptRange.Controls.Add(this.lblTo_2);
			this.fraDeptRange.Location = new System.Drawing.Point(30, 90);
			this.fraDeptRange.Name = "fraDeptRange";
			this.fraDeptRange.Size = new System.Drawing.Size(415, 150);
			this.fraDeptRange.TabIndex = 37;
			this.fraDeptRange.Text = "Accounts To Be Reported";
			// 
			// cboSingleDept
			// 
			this.cboSingleDept.AutoSize = false;
			this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleDept.FormattingEnabled = true;
			this.cboSingleDept.Location = new System.Drawing.Point(20, 90);
			this.cboSingleDept.Name = "cboSingleDept";
			this.cboSingleDept.Size = new System.Drawing.Size(161, 40);
			this.cboSingleDept.TabIndex = 42;
			this.cboSingleDept.Visible = false;
			this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
			// 
			// cboBeginningDept
			// 
			this.cboBeginningDept.AutoSize = false;
			this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningDept.FormattingEnabled = true;
			this.cboBeginningDept.Location = new System.Drawing.Point(20, 90);
			this.cboBeginningDept.Name = "cboBeginningDept";
			this.cboBeginningDept.Size = new System.Drawing.Size(161, 40);
			this.cboBeginningDept.TabIndex = 44;
			this.cboBeginningDept.Visible = false;
			this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
			// 
			// cboEndingDept
			// 
			this.cboEndingDept.AutoSize = false;
			this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingDept.FormattingEnabled = true;
			this.cboEndingDept.Location = new System.Drawing.Point(235, 90);
			this.cboEndingDept.Name = "cboEndingDept";
			this.cboEndingDept.Size = new System.Drawing.Size(161, 40);
			this.cboEndingDept.TabIndex = 43;
			this.cboEndingDept.Visible = false;
			this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
			// 
			// cboBeginningFund
			// 
			this.cboBeginningFund.AutoSize = false;
			this.cboBeginningFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningFund.FormattingEnabled = true;
			this.cboBeginningFund.Location = new System.Drawing.Point(20, 90);
			this.cboBeginningFund.Name = "cboBeginningFund";
			this.cboBeginningFund.Size = new System.Drawing.Size(161, 40);
			this.cboBeginningFund.TabIndex = 41;
			this.cboBeginningFund.Visible = false;
			this.cboBeginningFund.DropDown += new System.EventHandler(this.cboBeginningFund_DropDown);
			// 
			// cboEndingFund
			// 
			this.cboEndingFund.AutoSize = false;
			this.cboEndingFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingFund.FormattingEnabled = true;
			this.cboEndingFund.Location = new System.Drawing.Point(235, 90);
			this.cboEndingFund.Name = "cboEndingFund";
			this.cboEndingFund.Size = new System.Drawing.Size(161, 40);
			this.cboEndingFund.TabIndex = 40;
			this.cboEndingFund.Visible = false;
			this.cboEndingFund.DropDown += new System.EventHandler(this.cboEndingFund_DropDown);
			// 
			// cboSingleFund
			// 
			this.cboSingleFund.AutoSize = false;
			this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleFund.FormattingEnabled = true;
			this.cboSingleFund.Location = new System.Drawing.Point(20, 90);
			this.cboSingleFund.Name = "cboSingleFund";
			this.cboSingleFund.Size = new System.Drawing.Size(161, 40);
			this.cboSingleFund.TabIndex = 39;
			this.cboSingleFund.Visible = false;
			this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
			// 
			// vsLowAccount
			// 
			this.vsLowAccount.AllowSelection = false;
			this.vsLowAccount.AllowUserToResizeColumns = false;
			this.vsLowAccount.AllowUserToResizeRows = false;
			this.vsLowAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLowAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsLowAccount.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLowAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsLowAccount.ColumnHeadersHeight = 30;
			this.vsLowAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsLowAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLowAccount.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsLowAccount.DragIcon = null;
			this.vsLowAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLowAccount.FixedCols = 0;
			this.vsLowAccount.FixedRows = 0;
			this.vsLowAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.FrozenCols = 0;
			this.vsLowAccount.GridColor = System.Drawing.Color.Empty;
			this.vsLowAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.Location = new System.Drawing.Point(20, 90);
			this.vsLowAccount.Name = "vsLowAccount";
			this.vsLowAccount.ReadOnly = true;
			this.vsLowAccount.RowHeadersVisible = false;
			this.vsLowAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLowAccount.RowHeightMin = 0;
			this.vsLowAccount.Rows = 1;
			this.vsLowAccount.ScrollTipText = null;
			this.vsLowAccount.ShowColumnVisibilityMenu = false;
			this.vsLowAccount.Size = new System.Drawing.Size(161, 42);
			this.vsLowAccount.StandardTab = true;
			this.vsLowAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLowAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLowAccount.TabIndex = 49;
			this.vsLowAccount.Visible = false;
			this.vsLowAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsLowAccount_KeyPressEdit);
			this.vsLowAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsLowAccount_ChangeEdit);
			// 
			// vsSingleAccount
			// 
			this.vsSingleAccount.AllowSelection = false;
			this.vsSingleAccount.AllowUserToResizeColumns = false;
			this.vsSingleAccount.AllowUserToResizeRows = false;
			this.vsSingleAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSingleAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsSingleAccount.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSingleAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsSingleAccount.ColumnHeadersHeight = 30;
			this.vsSingleAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsSingleAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSingleAccount.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsSingleAccount.DragIcon = null;
			this.vsSingleAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSingleAccount.FixedCols = 0;
			this.vsSingleAccount.FixedRows = 0;
			this.vsSingleAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.FrozenCols = 0;
			this.vsSingleAccount.GridColor = System.Drawing.Color.Empty;
			this.vsSingleAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.Location = new System.Drawing.Point(20, 90);
			this.vsSingleAccount.Name = "vsSingleAccount";
			this.vsSingleAccount.ReadOnly = true;
			this.vsSingleAccount.RowHeadersVisible = false;
			this.vsSingleAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSingleAccount.RowHeightMin = 0;
			this.vsSingleAccount.Rows = 1;
			this.vsSingleAccount.ScrollTipText = null;
			this.vsSingleAccount.ShowColumnVisibilityMenu = false;
			this.vsSingleAccount.Size = new System.Drawing.Size(161, 42);
			this.vsSingleAccount.StandardTab = true;
			this.vsSingleAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSingleAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSingleAccount.TabIndex = 50;
			this.vsSingleAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsSingleAccount_KeyPressEdit);
			this.vsSingleAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsSingleAccount_ChangeEdit);
			// 
			// vsHighAccount
			// 
			this.vsHighAccount.AllowSelection = false;
			this.vsHighAccount.AllowUserToResizeColumns = false;
			this.vsHighAccount.AllowUserToResizeRows = false;
			this.vsHighAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsHighAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsHighAccount.Cols = 1;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsHighAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsHighAccount.ColumnHeadersHeight = 30;
			this.vsHighAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsHighAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsHighAccount.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsHighAccount.DragIcon = null;
			this.vsHighAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsHighAccount.FixedCols = 0;
			this.vsHighAccount.FixedRows = 0;
			this.vsHighAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.FrozenCols = 0;
			this.vsHighAccount.GridColor = System.Drawing.Color.Empty;
			this.vsHighAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.Location = new System.Drawing.Point(235, 90);
			this.vsHighAccount.Name = "vsHighAccount";
			this.vsHighAccount.ReadOnly = true;
			this.vsHighAccount.RowHeadersVisible = false;
			this.vsHighAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsHighAccount.RowHeightMin = 0;
			this.vsHighAccount.Rows = 1;
			this.vsHighAccount.ScrollTipText = null;
			this.vsHighAccount.ShowColumnVisibilityMenu = false;
			this.vsHighAccount.Size = new System.Drawing.Size(161, 42);
			this.vsHighAccount.StandardTab = true;
			this.vsHighAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsHighAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsHighAccount.TabIndex = 51;
			this.vsHighAccount.Visible = false;
			this.vsHighAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsHighAccount_KeyPressEdit);
			this.vsHighAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsHighAccount_ChangeEdit);
			// 
			// lblTo_3
			// 
			this.lblTo_3.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_3.Location = new System.Drawing.Point(201, 104);
			this.lblTo_3.Name = "lblTo_3";
			this.lblTo_3.Size = new System.Drawing.Size(21, 20);
			this.lblTo_3.TabIndex = 54;
			this.lblTo_3.Text = "TO";
			this.lblTo_3.Visible = false;
			// 
			// lblTo_1
			// 
			this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_1.Location = new System.Drawing.Point(201, 104);
			this.lblTo_1.Name = "lblTo_1";
			this.lblTo_1.Size = new System.Drawing.Size(21, 20);
			this.lblTo_1.TabIndex = 53;
			this.lblTo_1.Text = "TO";
			this.lblTo_1.Visible = false;
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_2.Location = new System.Drawing.Point(201, 104);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(21, 20);
			this.lblTo_2.TabIndex = 52;
			this.lblTo_2.Text = "TO";
			this.lblTo_2.Visible = false;
			// 
			// fraDate
			// 
			this.fraDate.Controls.Add(this.chkDateRange);
			this.fraDate.Controls.Add(this.fraMonthRange);
			this.fraDate.Controls.Add(this.fraDateRange);
			this.fraDate.Controls.Add(this.chkMonthRange);
			this.fraDate.Location = new System.Drawing.Point(30, 264);
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(415, 229);
			this.fraDate.TabIndex = 22;
			this.fraDate.Text = "Date Options";
			// 
			// chkDateRange
			// 
			this.chkDateRange.Location = new System.Drawing.Point(20, 30);
			this.chkDateRange.Name = "chkDateRange";
			this.chkDateRange.Size = new System.Drawing.Size(115, 27);
			this.chkDateRange.TabIndex = 24;
			this.chkDateRange.Text = "Date Range";
			this.chkDateRange.CheckedChanged += new System.EventHandler(this.chkDateRange_CheckedChanged);
			// 
			// fraMonthRange
			// 
			this.fraMonthRange.AppearanceKey = "groupBoxNoBorders";
			this.fraMonthRange.Controls.Add(this.cboLowMonth);
			this.fraMonthRange.Controls.Add(this.cboHighMonth);
			this.fraMonthRange.Controls.Add(this.Label3);
			this.fraMonthRange.Enabled = false;
			this.fraMonthRange.Location = new System.Drawing.Point(20, 172);
			this.fraMonthRange.Name = "fraMonthRange";
			this.fraMonthRange.Size = new System.Drawing.Size(361, 40);
			this.fraMonthRange.TabIndex = 25;
			// 
			// cboLowMonth
			// 
			this.cboLowMonth.AutoSize = false;
			this.cboLowMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboLowMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLowMonth.FormattingEnabled = true;
			this.cboLowMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboLowMonth.Location = new System.Drawing.Point(0, 0);
			this.cboLowMonth.Name = "cboLowMonth";
			this.cboLowMonth.Size = new System.Drawing.Size(153, 40);
			this.cboLowMonth.TabIndex = 27;
			// 
			// cboHighMonth
			// 
			this.cboHighMonth.AutoSize = false;
			this.cboHighMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboHighMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboHighMonth.FormattingEnabled = true;
			this.cboHighMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboHighMonth.Location = new System.Drawing.Point(208, 0);
			this.cboHighMonth.Name = "cboHighMonth";
			this.cboHighMonth.Size = new System.Drawing.Size(153, 40);
			this.cboHighMonth.TabIndex = 26;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(168, 14);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(23, 22);
			this.Label3.TabIndex = 28;
			this.Label3.Text = "TO";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraDateRange
			// 
			this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
			this.fraDateRange.Controls.Add(this.txtLowDate);
			this.fraDateRange.Controls.Add(this.txtHighDate);
			this.fraDateRange.Controls.Add(this.Label2);
			this.fraDateRange.Enabled = false;
			this.fraDateRange.Location = new System.Drawing.Point(20, 70);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(376, 40);
			this.fraDateRange.TabIndex = 29;
			// 
			// txtLowDate
			// 
			this.txtLowDate.Location = new System.Drawing.Point(0, 0);
			this.txtLowDate.Mask = "00/00/0000";
			this.txtLowDate.Name = "txtLowDate";
			this.txtLowDate.Size = new System.Drawing.Size(153, 40);
			this.txtLowDate.TabIndex = 30;
			this.txtLowDate.Text = "  /  /";
			// 
			// txtHighDate
			// 
			this.txtHighDate.Location = new System.Drawing.Point(208, 0);
			this.txtHighDate.Mask = "00/00/0000";
			this.txtHighDate.Name = "txtHighDate";
			this.txtHighDate.Size = new System.Drawing.Size(153, 40);
			this.txtHighDate.TabIndex = 31;
			this.txtHighDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(168, 14);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(23, 22);
			this.Label2.TabIndex = 32;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// chkMonthRange
			// 
			this.chkMonthRange.Location = new System.Drawing.Point(20, 130);
			this.chkMonthRange.Name = "chkMonthRange";
			this.chkMonthRange.Size = new System.Drawing.Size(213, 27);
			this.chkMonthRange.TabIndex = 23;
			this.chkMonthRange.Text = "Accounting Period Range";
			this.chkMonthRange.CheckedChanged += new System.EventHandler(this.chkMonthRange_CheckedChanged);
			// 
			// chkShowLiquidatedEncumbranceActivity
			// 
			this.chkShowLiquidatedEncumbranceActivity.Location = new System.Drawing.Point(476, 392);
			this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
			this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(311, 27);
			this.chkShowLiquidatedEncumbranceActivity.TabIndex = 20;
			this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
			this.chkShowLiquidatedEncumbranceActivity.Visible = false;
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxLeftBorder";
			this.Frame2.Controls.Add(this.Frame4);
			this.Frame2.Controls.Add(this.cmbAllJournals);
			this.Frame2.Controls.Add(this.lblAllJournals);
			this.Frame2.Controls.Add(this.fraJournalRange);
			this.Frame2.Location = new System.Drawing.Point(476, 30);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(376, 324);
			this.Frame2.TabIndex = 6;
			this.Frame2.Text = "Journal Options";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.chkAP);
			this.Frame4.Controls.Add(this.chkCD);
			this.Frame4.Controls.Add(this.chkCR);
			this.Frame4.Controls.Add(this.chkGJ);
			this.Frame4.Controls.Add(this.chkPY);
			this.Frame4.Controls.Add(this.chkEN);
			this.Frame4.Location = new System.Drawing.Point(20, 150);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(336, 154);
			this.Frame4.TabIndex = 7;
			this.Frame4.Text = "Journal Types";
			// 
			// chkAP
			// 
			this.chkAP.Checked = true;
			this.chkAP.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkAP.Location = new System.Drawing.Point(20, 30);
			this.chkAP.Name = "chkAP";
			this.chkAP.Size = new System.Drawing.Size(116, 27);
			this.chkAP.TabIndex = 13;
			this.chkAP.Text = "AP Journals";
			// 
			// chkCD
			// 
			this.chkCD.Checked = true;
			this.chkCD.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkCD.Location = new System.Drawing.Point(20, 65);
			this.chkCD.Name = "chkCD";
			this.chkCD.Size = new System.Drawing.Size(117, 27);
			this.chkCD.TabIndex = 12;
			this.chkCD.Text = "CD Journals";
			// 
			// chkCR
			// 
			this.chkCR.Checked = true;
			this.chkCR.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkCR.Location = new System.Drawing.Point(20, 100);
			this.chkCR.Name = "chkCR";
			this.chkCR.Size = new System.Drawing.Size(117, 27);
			this.chkCR.TabIndex = 11;
			this.chkCR.Text = "CR Journals";
			// 
			// chkGJ
			// 
			this.chkGJ.Checked = true;
			this.chkGJ.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkGJ.Location = new System.Drawing.Point(185, 30);
			this.chkGJ.Name = "chkGJ";
			this.chkGJ.Size = new System.Drawing.Size(115, 27);
			this.chkGJ.TabIndex = 10;
			this.chkGJ.Text = "GJ Journals";
			// 
			// chkPY
			// 
			this.chkPY.Checked = true;
			this.chkPY.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkPY.Location = new System.Drawing.Point(185, 65);
			this.chkPY.Name = "chkPY";
			this.chkPY.Size = new System.Drawing.Size(116, 27);
			this.chkPY.TabIndex = 9;
			this.chkPY.Text = "PY Journals";
			// 
			// chkEN
			// 
			this.chkEN.Checked = true;
			this.chkEN.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkEN.Location = new System.Drawing.Point(185, 100);
			this.chkEN.Name = "chkEN";
			this.chkEN.Size = new System.Drawing.Size(122, 27);
			this.chkEN.TabIndex = 8;
			this.chkEN.Text = "Enc Journals";
			// 
			// fraJournalRange
			// 
			this.fraJournalRange.AppearanceKey = "groupBoxNoBorders";
			this.fraJournalRange.Controls.Add(this.cboEndJournal);
			this.fraJournalRange.Controls.Add(this.cboStartJournal);
			this.fraJournalRange.Controls.Add(this.Label1);
			this.fraJournalRange.Enabled = false;
			this.fraJournalRange.Location = new System.Drawing.Point(20, 90);
			this.fraJournalRange.Name = "fraJournalRange";
			this.fraJournalRange.Size = new System.Drawing.Size(350, 42);
			this.fraJournalRange.TabIndex = 16;
			// 
			// cboEndJournal
			// 
			this.cboEndJournal.AutoSize = false;
			this.cboEndJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndJournal.FormattingEnabled = true;
			this.cboEndJournal.Location = new System.Drawing.Point(196, 0);
			this.cboEndJournal.Name = "cboEndJournal";
			this.cboEndJournal.Size = new System.Drawing.Size(140, 40);
			this.cboEndJournal.TabIndex = 18;
			// 
			// cboStartJournal
			// 
			this.cboStartJournal.AutoSize = false;
			this.cboStartJournal.BackColor = System.Drawing.SystemColors.Window;
			this.cboStartJournal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboStartJournal.FormattingEnabled = true;
			this.cboStartJournal.Location = new System.Drawing.Point(0, 0);
			this.cboStartJournal.Name = "cboStartJournal";
			this.cboStartJournal.Size = new System.Drawing.Size(140, 40);
			this.cboStartJournal.TabIndex = 17;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(158, 14);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(23, 22);
			this.Label1.TabIndex = 19;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// vsSort
			// 
			this.vsSort.AllowSelection = false;
			this.vsSort.AllowUserToResizeColumns = false;
			this.vsSort.AllowUserToResizeRows = false;
			this.vsSort.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSort.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSort.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSort.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSort.BackColorSel = System.Drawing.Color.Empty;
			this.vsSort.Cols = 5;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSort.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsSort.ColumnHeadersHeight = 30;
			this.vsSort.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsSort.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSort.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsSort.DragIcon = null;
			this.vsSort.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSort.FixedCols = 0;
			this.vsSort.FixedRows = 0;
			this.vsSort.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSort.FrozenCols = 0;
			this.vsSort.GridColor = System.Drawing.Color.Empty;
			this.vsSort.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSort.Location = new System.Drawing.Point(7, 60);
			this.vsSort.Name = "vsSort";
			this.vsSort.ReadOnly = true;
			this.vsSort.RowHeadersVisible = false;
			this.vsSort.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSort.RowHeightMin = 0;
			this.vsSort.Rows = 50;
			this.vsSort.ScrollTipText = null;
			this.vsSort.ShowColumnVisibilityMenu = false;
			this.vsSort.Size = new System.Drawing.Size(74, 41);
			this.vsSort.StandardTab = true;
			this.vsSort.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSort.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSort.TabIndex = 21;
			this.vsSort.Visible = false;
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(30, 30);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(575, 24);
			this.lblTitle.TabIndex = 55;
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblTitle.Visible = false;
			// 
			// btnFilePrint
			// 
			this.btnFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFilePrint.AppearanceKey = "toolbarButton";
			this.btnFilePrint.Location = new System.Drawing.Point(1004, 29);
			this.btnFilePrint.Name = "btnFilePrint";
			this.btnFilePrint.Size = new System.Drawing.Size(44, 24);
			this.btnFilePrint.TabIndex = 0;
			this.btnFilePrint.Text = "Print";
			this.btnFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// btnFilePreview
			// 
			this.btnFilePreview.AppearanceKey = "acceptButton";
			this.btnFilePreview.Location = new System.Drawing.Point(425, 41);
			this.btnFilePreview.Name = "btnFilePreview";
			this.btnFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFilePreview.Size = new System.Drawing.Size(105, 48);
			this.btnFilePreview.TabIndex = 0;
			this.btnFilePreview.Text = "Preview";
			this.btnFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmAccountResearch
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmAccountResearch";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Account Research";
			this.Load += new System.EventHandler(this.frmAccountResearch_Load);
			this.Activated += new System.EventHandler(this.frmAccountResearch_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAccountResearch_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAccountResearch_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
			this.fraDeptRange.ResumeLayout(false);
			this.fraDeptRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSingleAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			this.fraDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonthRange)).EndInit();
			this.fraMonthRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonthRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkGJ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraJournalRange)).EndInit();
			this.fraJournalRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsSort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
