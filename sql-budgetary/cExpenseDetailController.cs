﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWBD0000
{
	public class cExpenseDetailController : cReportDataExporter
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;
		// 0 = CSV, 1 = Tab
		private bool boolUseDivision;
		private bool boolUseObject;

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theReport As cDetailsReport	OnRead(cExpenseDetailReport)
		public void ExportData(ref cDetailsReport theReport, string strFileName)
		{
			ClearErrors();
			StreamWriter ts = null;
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				string strSeparator = "";
				string strQ = "";
				// vbPorter upgrade warning: expReport As cExpenseDetailReport	OnWrite(cDetailsReport)
				cExpenseDetailReport expReport;
				expReport = (cExpenseDetailReport)theReport;
				strPath = Path.GetDirectoryName(strFileName);
				if (!Directory.Exists(strPath))
				{
					Information.Err().Raise(9999, null, "Path not found", null, null);
				}
				if (intFormatType == 1)
				{
					strSeparator = "\t";
					strQ = "";
				}
				else
				{
					strSeparator = ",";
					strQ = FCConvert.ToString(Convert.ToChar(34));
				}
				cExpenseDetailItem expItem;
				string strLine;
				ts = new StreamWriter(strFileName);
				strLine = strQ + "Account" + strQ + strSeparator + strQ + "Department" + strQ;
				if (boolUseDivision)
				{
					strLine += strSeparator + strQ + "Division" + strQ;
				}
				strLine += strSeparator + strQ + "Expense" + strQ;
				if (boolUseObject)
				{
					strLine += strSeparator + strQ + "Object" + strQ;
				}
				strLine += strSeparator + strQ + "Fund" + strQ;
				strLine += strSeparator + strQ + "Date Posted" + strQ;
				strLine += strSeparator + strQ + "Transaction Date" + strQ;
				strLine += strSeparator + strQ + "Journal Number" + strQ;
				strLine += strSeparator + strQ + "Description" + strQ;
				if (expReport.ShowJournalDetail)
				{
					strLine += strSeparator + strQ + "Journal Type" + strQ;
					strLine += strSeparator + strQ + "Period" + strQ;
					strLine += strSeparator + strQ + "RCB" + strQ;
					strLine += strSeparator + strQ + "Warrant" + strQ;
					strLine += strSeparator + strQ + "Check" + strQ;
					strLine += strSeparator + strQ + "Vendor" + strQ;
				}
				strLine += strSeparator + strQ + "Current Budget" + strQ;
				strLine += strSeparator + strQ + "Debits" + strQ;
				strLine += strSeparator + strQ + "Credits" + strQ;
				strLine += strSeparator + strQ + "Unexpended Balance" + strQ;
				ts.WriteLine(strLine);
				theReport.Details.MoveFirst();
				while (theReport.Details.IsCurrent())
				{
					strLine = "";
					expItem = (cExpenseDetailItem)theReport.Details.GetCurrentItem();
					strLine = strQ + expItem.Account + strQ + strSeparator + strQ + expItem.Department + strQ;
					if (boolUseDivision)
					{
						strLine += strSeparator + strQ + expItem.Division + strQ;
					}
					strLine += strSeparator + strQ + expItem.Expense + strQ;
					if (boolUseObject)
					{
						strLine += strSeparator + strQ + expItem.TheObject + strQ;
					}
					strLine += strSeparator + strQ + expItem.Fund + strQ;
					strLine += strSeparator + expItem.JournalDate;
					strLine += strSeparator + expItem.TransactionDate;
					strLine += strSeparator + FCConvert.ToString(expItem.JournalNumber);
					strLine += strSeparator + strQ + expItem.Description + strQ;
					if (expReport.ShowJournalDetail)
					{
						strLine += strSeparator + strQ + expItem.JournalType + strQ;
						strLine += strSeparator + FCConvert.ToString(expItem.Period);
						strLine += strSeparator + strQ + expItem.RCB + strQ;
						strLine += strSeparator + expItem.Warrant;
						strLine += strSeparator + FCConvert.ToString(expItem.CheckNumber);
						strLine += strSeparator + strQ + expItem.Vendor + strQ;
					}
					strLine += strSeparator + FCConvert.ToString(expItem.CurrentBudget);
					strLine += strSeparator + FCConvert.ToString(expItem.Debit);
					strLine += strSeparator + FCConvert.ToString(expItem.Credit);
					strLine += strSeparator + FCConvert.ToString(expItem.UnexpendedBalance);
					ts.WriteLine(strLine);
					theReport.Details.MoveNext();
				}
				ts.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (!(ts == null))
				{
					ts.Close();
				}
			}
		}

		public cExpenseDetailController() : base()
		{
			boolUseDivision = !modAccountTitle.Statics.ExpDivFlag;
			boolUseObject = !modAccountTitle.Statics.ObjFlag;
		}
	}
}
