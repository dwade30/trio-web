﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorInvoices.
	/// </summary>
	partial class frmVendorInvoices : BaseForm
	{
		public FCGrid gridInvoices;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.gridInvoices = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridInvoices)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 404);
			this.BottomPanel.Size = new System.Drawing.Size(645, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.gridInvoices);
			this.ClientArea.Size = new System.Drawing.Size(645, 344);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(645, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(189, 30);
			this.HeaderText.Text = "Vendor Invoices";
			// 
			// gridInvoices
			// 
			this.gridInvoices.AllowSelection = false;
			this.gridInvoices.AllowUserToResizeColumns = false;
			this.gridInvoices.AllowUserToResizeRows = false;
			this.gridInvoices.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.gridInvoices.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridInvoices.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridInvoices.BackColorBkg = System.Drawing.Color.Empty;
			this.gridInvoices.BackColorFixed = System.Drawing.Color.Empty;
			this.gridInvoices.BackColorSel = System.Drawing.Color.Empty;
			this.gridInvoices.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridInvoices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.gridInvoices.ColumnHeadersHeight = 30;
			this.gridInvoices.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridInvoices.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridInvoices.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridInvoices.ExtendLastCol = true;
			this.gridInvoices.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridInvoices.FrozenCols = 0;
			this.gridInvoices.GridColor = System.Drawing.Color.Empty;
			this.gridInvoices.Location = new System.Drawing.Point(30, 30);
			this.gridInvoices.Name = "gridInvoices";
			this.gridInvoices.ReadOnly = true;
			this.gridInvoices.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridInvoices.Rows = 2;
			this.gridInvoices.ShowColumnVisibilityMenu = false;
			this.gridInvoices.Size = new System.Drawing.Size(595, 294);
			this.gridInvoices.StandardTab = true;
			this.gridInvoices.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridInvoices.TabIndex = 0;
			this.gridInvoices.DoubleClick += new System.EventHandler(this.gridInvoices_DblClick);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 0;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmVendorInvoices
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(645, 512);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmVendorInvoices";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Vendor Invoices";
			this.Load += new System.EventHandler(this.frmVendorInvoices_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVendorInvoices_KeyDown);
			this.Resize += new System.EventHandler(this.frmVendorInvoices_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridInvoices)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
