﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptVendorErrors.
	/// </summary>
	partial class srptVendorErrors
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptVendorErrors));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldProblem1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldProblem2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProblem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProblem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldVendor,
				this.fldProblem1,
				this.fldProblem2
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldVendor
			// 
			this.fldVendor.Height = 0.1875F;
			this.fldVendor.Left = 0F;
			this.fldVendor.Name = "fldVendor";
			this.fldVendor.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldVendor.Text = "Field1";
			this.fldVendor.Top = 0F;
			this.fldVendor.Width = 2.71875F;
			// 
			// fldProblem1
			// 
			this.fldProblem1.Height = 0.1875F;
			this.fldProblem1.Left = 2.84375F;
			this.fldProblem1.Name = "fldProblem1";
			this.fldProblem1.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldProblem1.Text = "Field1";
			this.fldProblem1.Top = 0F;
			this.fldProblem1.Width = 1.65625F;
			// 
			// fldProblem2
			// 
			this.fldProblem2.Height = 0.1875F;
			this.fldProblem2.Left = 4.5625F;
			this.fldProblem2.Name = "fldProblem2";
			this.fldProblem2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldProblem2.Text = "Field1";
			this.fldProblem2.Top = 0F;
			this.fldProblem2.Width = 1.65625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Label23,
				this.Label24,
				this.Line1
			});
			this.GroupHeader1.Height = 0.5520833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 2.635417F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Label22.Text = "Vendor Problems";
			this.Label22.Top = 0F;
			this.Label22.Width = 1.208333F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label23.Text = "Vendor";
			this.Label23.Top = 0.34375F;
			this.Label23.Width = 2.71875F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 2.84375F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label24.Text = "Problems";
			this.Label24.Top = 0.34375F;
			this.Label24.Width = 2.71875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.53125F;
			this.Line1.Width = 6.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 6.46875F;
			this.Line1.Y1 = 0.53125F;
			this.Line1.Y2 = 0.53125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptVendorErrors
			// 
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.srptVendorErrors_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			((System.ComponentModel.ISupportInitialize)(this.fldVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProblem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProblem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendor;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProblem1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProblem2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
