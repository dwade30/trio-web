﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptTrialBalance.
	/// </summary>
	public partial class rptTrialBalance : BaseSectionReport
	{
		public static rptTrialBalance InstancePtr
		{
			get
			{
				return (rptTrialBalance)Sys.GetInstance(typeof(rptTrialBalance));
			}
		}

		protected rptTrialBalance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTrialBalance	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cTrialBalanceReport theReport;
		private cAccountReportGroup currentFund;
		private cAccountSummaryItem currentAccount;
		private cAccountReportGroup currentAccountGroup;
		private string strLastGroupName = "";
		private int lngPage;
		private double dblGroupDebits;
		private double dblGroupCredits;
		private double dblFundDebits;
		private double dblFundCredits;
		private double dblGroupBeginBalance;
		private double dblGroupEndBalance;
		private double dblFundBeginBalance;
		private double dblFundEndBalance;
		private double dblGroupNet;
		private double dblFundNet;
		private string strLastFund = string.Empty;
		private string strLastGroupTotalsTitle = "";
		// Public Property Get CurrentFund() As cERGReportGroup
		// If Not theReport Is Nothing Then
		// If theReport.Funds.IsCurrent Then
		// Set CurrentFund = theReport.Funds.GetCurrentItem
		// End If
		// End If
		// End Property
		public rptTrialBalance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Trial Balance";
		}

		public void Init(ref cTrialBalanceReport repObj)
		{
			theReport = repObj;
			frmReportViewer.InstancePtr.Init(this);
			lngPage = 1;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			// set up binder values for groups in report
			this.Fields.Add("GroupHeader1Binder");
			this.Fields.Add("GroupHeader2Binder");
			theReport.Funds.MoveFirst();
			currentFund = (cAccountReportGroup)theReport.Funds.GetCurrentItem();
			currentFund.Accounts.MoveFirst();
			this.Fields["GroupHeader1Binder"].Value = currentFund.GroupValue;
			strLastFund = currentFund.GroupValue;
			txtFundDescription.Text = currentFund.GroupValue + " " + currentFund.Description;
			if (currentFund.Accounts.IsCurrent())
			{
				currentAccountGroup = (cAccountReportGroup)currentFund.Accounts.GetCurrentItem();
				currentAccountGroup.Accounts.MoveFirst();
				this.Fields["GroupHeader2Binder"].Value = currentAccountGroup.GroupValue;
				txtGroupDescription.Text = currentAccountGroup.Description;
			}
			dblGroupDebits = 0;
			dblGroupCredits = 0;
			dblGroupNet = 0;
			dblGroupBeginBalance = 0;
			dblGroupEndBalance = 0;
			dblFundBeginBalance = 0;
			dblFundEndBalance = 0;
			dblFundNet = 0;
			dblFundDebits = 0;
			dblFundCredits = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			currentFund = null;
			currentAccountGroup = null;
			currentAccount = null;
			if (theReport.Funds.IsCurrent())
			{
				currentFund = (cAccountReportGroup)theReport.Funds.GetCurrentItem();
				if (currentFund.Accounts.IsCurrent())
				{
					currentAccountGroup = (cAccountReportGroup)currentFund.Accounts.GetCurrentItem();
				}
			}
			bool boolDidntUseGroup;
			while (!(currentFund == null))
			{
				while (!(currentAccountGroup == null))
				{
					if (currentAccountGroup.Accounts.IsCurrent())
					{
						currentAccount = (cAccountSummaryItem)currentAccountGroup.Accounts.GetCurrentItem();
						if (!theReport.IncludeAccountsWithNoActivity)
						{
							while (currentAccount.BeginningNet == 0 && currentAccount.Debits == 0 && currentAccount.Credits == 0)
							{
								currentAccountGroup.Accounts.MoveNext();
								if (currentAccountGroup.Accounts.IsCurrent())
								{
									currentAccount = (cAccountSummaryItem)currentAccountGroup.Accounts.GetCurrentItem();
								}
								else
								{
									// boolDidntUseGroup = True
									break;
								}
							}
						}
						else
						{
							break;
						}
						if (currentAccountGroup.Accounts.IsCurrent())
						{
							break;
						}
					}
					strLastGroupName = currentAccountGroup.Description;
					currentFund.Accounts.MoveNext();
					if (currentFund.Accounts.IsCurrent())
					{
						currentAccountGroup = (cAccountReportGroup)currentFund.Accounts.GetCurrentItem();
						currentAccountGroup.Accounts.MoveFirst();
						this.Fields["GroupHeader2Binder"].Value = currentAccountGroup.GroupValue;
						txtGroupDescription.Text = currentAccountGroup.Description;
					}
					else
					{
						currentAccountGroup = null;
					}
				}
				if (!(currentAccount == null))
				{
					break;
				}
				theReport.Funds.MoveNext();
				if (theReport.Funds.IsCurrent())
				{
					currentFund = (cAccountReportGroup)theReport.Funds.GetCurrentItem();
					currentFund.Accounts.MoveFirst();
					this.Fields["GroupHeader1Binder"].Value = currentFund.GroupValue;
					txtFundDescription.Text = currentFund.GroupValue + " " + currentFund.Description;
					if (currentFund.Accounts.IsCurrent())
					{
						currentAccountGroup = (cAccountReportGroup)currentFund.Accounts.GetCurrentItem();
						currentAccountGroup.Accounts.MoveFirst();
						this.Fields["GroupHeader2Binder"].Value = currentAccountGroup.GroupValue;
						txtGroupDescription.Text = currentAccountGroup.Description;
					}
				}
				else
				{
					currentFund = null;
				}
			}
			if (currentAccount == null)
			{
				eArgs.EOF = true;
				currentAccountGroup = null;
				currentFund = null;
			}
			else
			{
				eArgs.EOF = false;
			}
			if (!(currentFund == null) && !(currentAccountGroup == null))
			{
				// GroupHeader1.GroupValue = currentFund.GroupValue
				// txtFundDescription = currentFund.GroupValue & " " & currentFund.Description
				// GroupHeader2.GroupValue = currentAccountGroup.GroupValue
				// txtGroupDescription.Text = currentAccountGroup.Description
				// txtGroupCredits.Text = format(currentAccountGroup.Credits, "#,###,###,###,##0.00")
				// txtGroupDebits.Text = format(currentAccountGroup.Debits, "#,###,###,###,##0.00")
				// txtGroupCredits.Text = format(dblGroupCredits, "#,###,###,###,##0.00")
				// txtGroupDebits.Text = format(dblGroupDebits, "#,###,###,###,##0.00")
				// txtFundCredits.Text = format(currentFund.Credits, "#,###,###,###,##0.00")
				// txtFundDebits.Text = format(currentFund.Debits, "#,###,###,###,##0.00")
				if (currentAccountGroup.Accounts.IsCurrent())
				{
					currentAccountGroup.Accounts.MoveNext();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			this.Fields["GroupHeader1Binder"].Value = "";
			this.Fields["GroupHeader2Binder"].Value = "";
			Label8.Text = modGlobalConstants.Statics.MuniName;
			Label10.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lblUser.Text = modGlobalConstants.Statics.clsSecurityClass.Get_UserName();
			string[] strSubtitle = new string[2 + 1];
			int intIndex;
			intIndex = 0;
			if (theReport.BeginningFund != "" && theReport.EndingFund != "")
			{
				strSubtitle[intIndex] = "Funds " + theReport.BeginningFund + " to " + theReport.EndingFund;
				intIndex += 1;
			}
			if (theReport.StartPeriod > 0 && theReport.EndPeriod > 0)
			{
				strSubtitle[intIndex] = "Periods " + FCConvert.ToString(theReport.StartPeriod) + " to " + FCConvert.ToString(theReport.EndPeriod);
				intIndex += 1;
			}
			lblSubtitle1.Text = strSubtitle[0];
			lblSubTitle2.Text = strSubtitle[1];
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(currentAccount == null))
			{
				txtAccount.Text = currentAccount.Account;
				txtAccountDescription.Text = currentAccount.Description;
				txtCredits.Text = Strings.Format(currentAccount.Credits, "#,###,###,###,##0.00");
				txtDebits.Text = Strings.Format(currentAccount.Debits, "#,###,###,###,##0.00");
				if (currentAccount.IsLiability || currentAccount.IsFundBalance)
				{
					// txtBegBalanceBudget.Text = format((currentAccount.BeginningBalance + currentAccount.BudgetAdjustment) * -1, "#,###,###,###,##0.00")
					txtBegBalanceBudget.Text = Strings.Format(currentAccount.BeginningNet * -1, "#,###,###,###,##0.00");
					txtEndBalanceBudget.Text = Strings.Format(currentAccount.EndBalance * -1, "#,###,###,###,##0.00");
					dblGroupBeginBalance += (currentAccount.BeginningNet * -1);
					dblFundBeginBalance += currentAccount.BeginningNet;
					dblGroupEndBalance += (currentAccount.EndBalance * -1);
					dblFundEndBalance += currentAccount.EndBalance;
					dblFundCredits -= currentAccount.Credits;
					dblFundDebits -= currentAccount.Debits;
					dblFundNet -= currentAccount.Net;
				}
				else
				{
					// txtBegBalanceBudget.Text = format((CurrentAccount.BeginningBalance + CurrentAccount.BudgetAdjustment), "#,###,###,###,##0.00")
					txtBegBalanceBudget.Text = Strings.Format(currentAccount.BeginningNet, "#,###,###,###,##0.00");
					txtEndBalanceBudget.Text = Strings.Format(currentAccount.EndBalance, "#,###,###,###,##0.00");
					dblGroupBeginBalance += currentAccount.BeginningNet;
					dblGroupEndBalance += currentAccount.EndBalance;
					if (currentAccount.IsAsset)
					{
						dblFundBeginBalance += currentAccount.BeginningNet;
						dblFundEndBalance += currentAccount.EndBalance;
						dblFundCredits += currentAccount.Credits;
						dblFundDebits += currentAccount.Debits;
						dblFundNet += currentAccount.Net;
					}
				}
				txtNet.Text = Strings.Format(currentAccount.Net, "#,###,###,###,##0.00");
				dblGroupNet += currentAccount.Net;
				dblGroupDebits += currentAccount.Debits;
				dblGroupCredits += currentAccount.Credits;
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			txtFundBeginningBalance.Text = Strings.Format(dblFundBeginBalance, "#,###,###,###,##0.00");
			txtFundEndBalance.Text = Strings.Format(dblFundEndBalance, "#,###,###,###,##0.00");
			txtFundNet.Text = Strings.Format(dblFundNet, "#,###,###,###,##0.00");
			txtFundCredits.Text = Strings.Format(dblFundCredits, "#,###,###,###,##0.00");
			txtFundDebits.Text = Strings.Format(dblFundDebits, "#,###,###,###,##0.00");
			txtFundFooterName.Text = "Fund " + strLastFund + " Totals";
			strLastFund = this.Fields["GroupHeader1Binder"].Value.ToString();
			dblFundCredits = 0;
			dblFundDebits = 0;
			dblFundNet = 0;
			dblFundEndBalance = 0;
			dblFundBeginBalance = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			txtGroupCredits.Text = Strings.Format(dblGroupCredits, "#,###,###,###,##0.00");
			txtGroupDebits.Text = Strings.Format(dblGroupDebits, "#,###,###,###,##0.00");
			txtGroupNet.Text = Strings.Format(dblGroupNet, "#,###,###,###,##0.00");
			txtGroupBeginningBalance.Text = Strings.Format(dblGroupBeginBalance, "#,###,###,##0.00");
			txtGroupEndBalance.Text = Strings.Format(dblGroupEndBalance, "#,###,###,###,##0.00");
			txtGroupFooterName.Text = strLastGroupName + " Totals";
			dblGroupCredits = 0;
			dblGroupDebits = 0;
			dblGroupBeginBalance = 0;
			dblGroupEndBalance = 0;
			dblGroupNet = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + FCConvert.ToString(lngPage);
			lngPage += 1;
		}

		
	}
}
