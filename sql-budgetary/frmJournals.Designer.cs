﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournals.
	/// </summary>
	partial class frmJournals : BaseForm
	{
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridUnposted;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdPost;
		public fecherFoundation.FCCheckBox chkBreak;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdPrintUnposted;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCButton cmdSaveDescriptions;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCButton cmdEdit;
		public fecherFoundation.FCButton cmdCreate;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid GridPosted;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncActivityPosted;
		public fecherFoundation.FCButton cmdPrintPosted;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCButton cmdReverse;
		public fecherFoundation.FCCheckBox chkCorrection;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSaveDescriptions;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJournals));
			this.SSTab1 = new fecherFoundation.FCTabControl();
			this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cmdPrintUnposted = new fecherFoundation.FCButton();
			this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cmdPost = new fecherFoundation.FCButton();
			this.chkBreak = new fecherFoundation.FCCheckBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.cmdSaveDescriptions = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdEdit = new fecherFoundation.FCButton();
			this.cmdCreate = new fecherFoundation.FCButton();
			this.GridUnposted = new fecherFoundation.FCGrid();
			this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
			this.GridPosted = new fecherFoundation.FCGrid();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.chkShowLiquidatedEncActivityPosted = new fecherFoundation.FCCheckBox();
			this.cmdPrintPosted = new fecherFoundation.FCButton();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.cmdReverse = new fecherFoundation.FCButton();
			this.chkCorrection = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveDescriptions = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnEditGJDefaultTypes = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SSTab1.SuspendLayout();
			this.SSTab1_Page1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintUnposted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBreak)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDescriptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridUnposted)).BeginInit();
			this.SSTab1_Page2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPosted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncActivityPosted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPosted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReverse)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnEditGJDefaultTypes)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 500);
			this.BottomPanel.Visible = false;
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SSTab1);
			this.ClientArea.Size = new System.Drawing.Size(1014, 518);
			this.ClientArea.Controls.SetChildIndex(this.SSTab1, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnEditGJDefaultTypes);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnEditGJDefaultTypes, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(94, 28);
			this.HeaderText.Text = "Journals";
			// 
			// SSTab1
			// 
			this.SSTab1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.SSTab1.Controls.Add(this.SSTab1_Page1);
			this.SSTab1.Controls.Add(this.SSTab1_Page2);
			this.SSTab1.Location = new System.Drawing.Point(30, 20);
			this.SSTab1.Name = "SSTab1";
			this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
			this.SSTab1.Size = new System.Drawing.Size(943, 480);
			this.SSTab1.TabIndex = 1001;
			this.SSTab1.Text = "Unposted Journals";
			// 
			// SSTab1_Page1
			// 
			this.SSTab1_Page1.AutoScroll = true;
			this.SSTab1_Page1.Controls.Add(this.Frame2);
			this.SSTab1_Page1.Controls.Add(this.Frame1);
			this.SSTab1_Page1.Controls.Add(this.Frame3);
			this.SSTab1_Page1.Controls.Add(this.GridUnposted);
			this.SSTab1_Page1.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page1.Name = "SSTab1_Page1";
			this.SSTab1_Page1.Size = new System.Drawing.Size(941, 444);
			this.SSTab1_Page1.Text = "Unposted Journals";
			// 
			// Frame2
			// 
			this.Frame2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Frame2.AppearanceKey = "groupBoxNoBorders";
			this.Frame2.Controls.Add(this.cmdPrintUnposted);
			this.Frame2.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
			this.Frame2.Location = new System.Drawing.Point(343, 344);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(350, 96);
			this.Frame2.TabIndex = 3;
			// 
			// cmdPrintUnposted
			// 
			this.cmdPrintUnposted.AppearanceKey = "actionButton";
			this.cmdPrintUnposted.Location = new System.Drawing.Point(0, 46);
			this.cmdPrintUnposted.Name = "cmdPrintUnposted";
			this.cmdPrintUnposted.Size = new System.Drawing.Size(104, 40);
			this.cmdPrintUnposted.TabIndex = 12;
			this.cmdPrintUnposted.Text = "Print";
			this.cmdPrintUnposted.Click += new System.EventHandler(this.cmdPrintUnposted_Click);
			// 
			// chkShowLiquidatedEncumbranceActivity
			// 
			this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
			this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(256, 22);
			this.chkShowLiquidatedEncumbranceActivity.TabIndex = 11;
			this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
			// 
			// Frame1
			// 
			this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.cmdPost);
			this.Frame1.Controls.Add(this.chkBreak);
			this.Frame1.Location = new System.Drawing.Point(23, 344);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(272, 96);
			this.Frame1.TabIndex = 2;
			// 
			// cmdPost
			// 
			this.cmdPost.AppearanceKey = "actionButton";
			this.cmdPost.Location = new System.Drawing.Point(6, 46);
			this.cmdPost.Name = "cmdPost";
			this.cmdPost.Size = new System.Drawing.Size(149, 40);
			this.cmdPost.TabIndex = 10;
			this.cmdPost.Text = "Post Journal(s)";
			this.cmdPost.Click += new System.EventHandler(this.cmdPost_Click);
			// 
			// chkBreak
			// 
			this.chkBreak.Checked = true;
			this.chkBreak.CheckState = Wisej.Web.CheckState.Checked;
			this.chkBreak.Name = "chkBreak";
			this.chkBreak.Size = new System.Drawing.Size(209, 22);
			this.chkBreak.TabIndex = 9;
			this.chkBreak.Text = "Page break after each journal?";
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.cmdSaveDescriptions);
			this.Frame3.Controls.Add(this.cmdDelete);
			this.Frame3.Controls.Add(this.cmdEdit);
			this.Frame3.Controls.Add(this.cmdCreate);
			this.Frame3.Location = new System.Drawing.Point(3, 266);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(668, 68);
			this.Frame3.TabIndex = 4;
			// 
			// cmdSaveDescriptions
			// 
			this.cmdSaveDescriptions.AppearanceKey = "actionButton";
			this.cmdSaveDescriptions.Location = new System.Drawing.Point(365, 20);
			this.cmdSaveDescriptions.Name = "cmdSaveDescriptions";
			this.cmdSaveDescriptions.Size = new System.Drawing.Size(168, 40);
			this.cmdSaveDescriptions.TabIndex = 8;
			this.cmdSaveDescriptions.Text = "Save Descriptions";
			this.cmdSaveDescriptions.Click += new System.EventHandler(this.cmdSaveDescriptions_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.AppearanceKey = "actionButton";
			this.cmdDelete.Location = new System.Drawing.Point(250, 20);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(100, 40);
			this.cmdDelete.TabIndex = 7;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdEdit
			// 
			this.cmdEdit.AppearanceKey = "actionButton";
			this.cmdEdit.Location = new System.Drawing.Point(135, 20);
			this.cmdEdit.Name = "cmdEdit";
			this.cmdEdit.Size = new System.Drawing.Size(100, 40);
			this.cmdEdit.TabIndex = 6;
			this.cmdEdit.Text = "Edit";
			this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
			// 
			// cmdCreate
			// 
			this.cmdCreate.AppearanceKey = "actionButton";
			this.cmdCreate.Location = new System.Drawing.Point(20, 20);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Size = new System.Drawing.Size(100, 40);
			this.cmdCreate.TabIndex = 5;
			this.cmdCreate.Text = "Create";
			this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
			// 
			// GridUnposted
			// 
			this.GridUnposted.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridUnposted.Cols = 7;
			this.GridUnposted.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridUnposted.ExtendLastCol = true;
			this.GridUnposted.Location = new System.Drawing.Point(20, 8);
			this.GridUnposted.Name = "GridUnposted";
			this.GridUnposted.ReadOnly = false;
			this.GridUnposted.Rows = 1;
			this.GridUnposted.Size = new System.Drawing.Size(902, 257);
			this.GridUnposted.TabIndex = 1;
			this.GridUnposted.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridUnposted_BeforeEdit);
			this.GridUnposted.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridUnposted_ValidateEdit);
			this.GridUnposted.DoubleClick += new System.EventHandler(this.GridUnposted_DoubleClick);
			// 
			// SSTab1_Page2
			// 
			this.SSTab1_Page2.AutoScroll = true;
			this.SSTab1_Page2.Controls.Add(this.GridPosted);
			this.SSTab1_Page2.Controls.Add(this.Frame4);
			this.SSTab1_Page2.Controls.Add(this.Frame5);
			this.SSTab1_Page2.Location = new System.Drawing.Point(1, 35);
			this.SSTab1_Page2.Name = "SSTab1_Page2";
			this.SSTab1_Page2.Size = new System.Drawing.Size(941, 444);
			this.SSTab1_Page2.Text = "Posted Journals";
			// 
			// GridPosted
			// 
			this.GridPosted.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridPosted.Cols = 8;
			this.GridPosted.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPosted.ExtendLastCol = true;
			this.GridPosted.Location = new System.Drawing.Point(20, 10);
			this.GridPosted.Name = "GridPosted";
			this.GridPosted.ReadOnly = false;
			this.GridPosted.Rows = 1;
			this.GridPosted.Size = new System.Drawing.Size(902, 257);
			this.GridPosted.TabIndex = 14;
			this.GridPosted.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridPosted_BeforeEdit);
			this.GridPosted.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridPosted_ValidateEdit);
			// 
			// Frame4
			// 
			this.Frame4.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Frame4.AppearanceKey = "groupBoxNoBorders";
			this.Frame4.Controls.Add(this.chkShowLiquidatedEncActivityPosted);
			this.Frame4.Controls.Add(this.cmdPrintPosted);
			this.Frame4.Location = new System.Drawing.Point(13, 273);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(312, 126);
			this.Frame4.TabIndex = 13;
			// 
			// chkShowLiquidatedEncActivityPosted
			// 
			this.chkShowLiquidatedEncActivityPosted.Location = new System.Drawing.Point(0, 20);
			this.chkShowLiquidatedEncActivityPosted.Name = "chkShowLiquidatedEncActivityPosted";
			this.chkShowLiquidatedEncActivityPosted.Size = new System.Drawing.Size(256, 22);
			this.chkShowLiquidatedEncActivityPosted.TabIndex = 15;
			this.chkShowLiquidatedEncActivityPosted.Text = "Show Liquidated Encumbrance Activity";
			// 
			// cmdPrintPosted
			// 
			this.cmdPrintPosted.AppearanceKey = "actionButton";
			this.cmdPrintPosted.Location = new System.Drawing.Point(0, 66);
			this.cmdPrintPosted.Name = "cmdPrintPosted";
			this.cmdPrintPosted.Size = new System.Drawing.Size(104, 40);
			this.cmdPrintPosted.TabIndex = 16;
			this.cmdPrintPosted.Text = "Print";
			this.cmdPrintPosted.Click += new System.EventHandler(this.cmdPrintPosted_Click);
			// 
			// Frame5
			// 
			this.Frame5.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Frame5.AppearanceKey = "groupBoxNoBorders";
			this.Frame5.Controls.Add(this.cmdReverse);
			this.Frame5.Controls.Add(this.chkCorrection);
			this.Frame5.Location = new System.Drawing.Point(318, 273);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(408, 126);
			this.Frame5.TabIndex = 19;
			// 
			// cmdReverse
			// 
			this.cmdReverse.AppearanceKey = "actionButton";
			this.cmdReverse.Location = new System.Drawing.Point(0, 66);
			this.cmdReverse.Name = "cmdReverse";
			this.cmdReverse.Size = new System.Drawing.Size(104, 40);
			this.cmdReverse.TabIndex = 18;
			this.cmdReverse.Text = "Reverse";
			this.cmdReverse.Click += new System.EventHandler(this.cmdReverse_Click);
			// 
			// chkCorrection
			// 
			this.chkCorrection.Location = new System.Drawing.Point(6, 20);
			this.chkCorrection.Name = "chkCorrection";
			this.chkCorrection.Size = new System.Drawing.Size(300, 22);
			this.chkCorrection.TabIndex = 17;
			this.chkCorrection.Text = "Create reversing journal with correcting entries";
			this.ToolTip1.SetToolTip(this.chkCorrection, "Entries in the reversing journal will have an RCB setting of C");
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveExit,
            this.mnuSaveDescriptions,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Enabled = false;
			this.mnuSaveExit.Index = 0;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Visible = false;
			// 
			// mnuSaveDescriptions
			// 
			this.mnuSaveDescriptions.Index = 1;
			this.mnuSaveDescriptions.Name = "mnuSaveDescriptions";
			this.mnuSaveDescriptions.Text = "Save Descriptions";
			this.mnuSaveDescriptions.Click += new System.EventHandler(this.mnuSaveDescriptions_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 3;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// btnEditGJDefaultTypes
			// 
			this.btnEditGJDefaultTypes.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnEditGJDefaultTypes.Location = new System.Drawing.Point(841, 29);
			this.btnEditGJDefaultTypes.Name = "btnEditGJDefaultTypes";
			this.btnEditGJDefaultTypes.Shortcut = Wisej.Web.Shortcut.F3;
			this.btnEditGJDefaultTypes.Size = new System.Drawing.Size(153, 24);
			this.btnEditGJDefaultTypes.TabIndex = 2;
			this.btnEditGJDefaultTypes.Text = "Edit GJ Default Types";
			this.btnEditGJDefaultTypes.Click += new System.EventHandler(this.btnEditGJDefaultTypes_Click);
			// 
			// frmJournals
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmJournals";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Journals";
			this.Load += new System.EventHandler(this.frmJournals_Load);
			this.Activated += new System.EventHandler(this.frmJournals_Activated);
			this.Resize += new System.EventHandler(this.frmJournals_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmJournals_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.SSTab1.ResumeLayout(false);
			this.SSTab1_Page1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintUnposted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBreak)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDescriptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEdit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCreate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridUnposted)).EndInit();
			this.SSTab1_Page2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridPosted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncActivityPosted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPosted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReverse)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnEditGJDefaultTypes)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnEditGJDefaultTypes;
	}
}
