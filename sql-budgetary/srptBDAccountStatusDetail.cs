﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptBDAccountStatusDetail.
	/// </summary>
	public partial class srptBDAccountStatusDetail : FCSectionReport
	{
		public static srptBDAccountStatusDetail InstancePtr
		{
			get
			{
				return (srptBDAccountStatusDetail)Sys.GetInstance(typeof(srptBDAccountStatusDetail));
			}
		}

		protected srptBDAccountStatusDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAPJournals_AutoInitialized?.Dispose();
                rsActivityDetail_AutoInitialized?.Dispose();
				rsEncJournals_AutoInitialized?.Dispose();
				rsJournalEntries_AutoInitialized?.Dispose();
				rsSummaryActivityDetail_AutoInitialized?.Dispose();
				rsYTDActivity_AutoInitialized?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBDAccountStatusDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditTotal;
		// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitTotal;
		int lngRecordCounter;
		// vbPorter upgrade warning: curRegularMonthDebit As Decimal	OnWriteFCConvert.ToDouble(
		Decimal curRegularMonthDebit;
		// vbPorter upgrade warning: curRegularMonthCredit As Decimal	OnWriteFCConvert.ToDouble(
		Decimal curRegularMonthCredit;
		// vbPorter upgrade warning: curBudgetMonthDebit As Decimal	OnWriteFCConvert.ToDouble(
		Decimal curBudgetMonthDebit;
		// vbPorter upgrade warning: curBudgetMonthCredit As Decimal	OnWriteFCConvert.ToDouble(
		Decimal curBudgetMonthCredit;
		// vbPorter upgrade warning: curRegularMonthDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curRegularMonthDebitTotal;
		// vbPorter upgrade warning: curRegularMonthCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curRegularMonthCreditTotal;
		// vbPorter upgrade warning: curBudgetMonthDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curBudgetMonthDebitTotal;
		// vbPorter upgrade warning: curBudgetMonthCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curBudgetMonthCreditTotal;
		int BegMonth;
		bool blnFirstMonth;
		int intCounter;
		float lngTop;
		int lngTotalMonths;
		string strPeriodCheck = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsAPJournals = new clsDRWrapper();
		clsDRWrapper rsAPJournals_AutoInitialized;

		clsDRWrapper rsAPJournals
		{
			get
			{
				if (rsAPJournals_AutoInitialized == null)
				{
					rsAPJournals_AutoInitialized = new clsDRWrapper();
				}
				return rsAPJournals_AutoInitialized;
			}
			set
			{
				rsAPJournals_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsJournalEntries = new clsDRWrapper();
		clsDRWrapper rsJournalEntries_AutoInitialized;

		clsDRWrapper rsJournalEntries
		{
			get
			{
				if (rsJournalEntries_AutoInitialized == null)
				{
					rsJournalEntries_AutoInitialized = new clsDRWrapper();
				}
				return rsJournalEntries_AutoInitialized;
			}
			set
			{
				rsJournalEntries_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsEncJournals = new clsDRWrapper();
		clsDRWrapper rsEncJournals_AutoInitialized;

		clsDRWrapper rsEncJournals
		{
			get
			{
				if (rsEncJournals_AutoInitialized == null)
				{
					rsEncJournals_AutoInitialized = new clsDRWrapper();
				}
				return rsEncJournals_AutoInitialized;
			}
			set
			{
				rsEncJournals_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsYTDActivity_AutoInitialized;

		clsDRWrapper rsYTDActivity
		{
			get
			{
				if (rsYTDActivity_AutoInitialized == null)
				{
					rsYTDActivity_AutoInitialized = new clsDRWrapper();
				}
				return rsYTDActivity_AutoInitialized;
			}
			set
			{
				rsYTDActivity_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsActivityDetail_AutoInitialized;

		clsDRWrapper rsActivityDetail
		{
			get
			{
				if (rsActivityDetail_AutoInitialized == null)
				{
					rsActivityDetail_AutoInitialized = new clsDRWrapper();
				}
				return rsActivityDetail_AutoInitialized;
			}
			set
			{
				rsActivityDetail_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rsSummaryActivityDetail = new clsDRWrapper();
		clsDRWrapper rsSummaryActivityDetail_AutoInitialized;

		clsDRWrapper rsSummaryActivityDetail
		{
			get
			{
				if (rsSummaryActivityDetail_AutoInitialized == null)
				{
					rsSummaryActivityDetail_AutoInitialized = new clsDRWrapper();
				}
				return rsSummaryActivityDetail_AutoInitialized;
			}
			set
			{
				rsSummaryActivityDetail_AutoInitialized = value;
			}
		}

		bool blnFirstRecord;
		string strCurrentAccount = "";
		string[] strCurrentAccountInfo = null;
		int lngNumberOfDetailRecords;
		private bool boolShowLiquidatedEncumbranceActivity;
		private cBDAccountStatusReport theReport;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cVendorController vendCont = new cVendorController();
		private cVendorController vendCont_AutoInitialized;

		private cVendorController vendCont
		{
			get
			{
				if (vendCont_AutoInitialized == null)
				{
					vendCont_AutoInitialized = new cVendorController();
				}
				return vendCont_AutoInitialized;
			}
			set
			{
				vendCont_AutoInitialized = value;
			}
		}

		string strDateCaption = "";
		cBDAccount currentAccount;
		cGenericCollection lstDetails = new cGenericCollection();
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collDetails = new cGenericCollection();
		private cGenericCollection collDetails_AutoInitialized;

		private cGenericCollection collDetails
		// of cAccountActivityItem
		{
			get
			{
				if (collDetails_AutoInitialized == null)
				{
					collDetails_AutoInitialized = new cGenericCollection();
				}
				return collDetails_AutoInitialized;
			}
			set
			{
				collDetails_AutoInitialized = value;
			}
		}

		public srptBDAccountStatusDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				eArgs.EOF = !lstDetails.IsCurrent();
			}
		}

		private string GetMonthName(int intMonth)
		{
			string GetMonthName = "";
			string strReturn;
			strReturn = "";
			switch (intMonth)
			{
				case 1:
					{
						strReturn = "January";
						break;
					}
				case 2:
					{
						strReturn = "February";
						break;
					}
				case 3:
					{
						strReturn = "March";
						break;
					}
				case 4:
					{
						strReturn = "April";
						break;
					}
				case 5:
					{
						strReturn = "May";
						break;
					}
				case 6:
					{
						strReturn = "June";
						break;
					}
				case 7:
					{
						strReturn = "July";
						break;
					}
				case 8:
					{
						strReturn = "August";
						break;
					}
				case 9:
					{
						strReturn = "September";
						break;
					}
				case 10:
					{
						strReturn = "October";
						break;
					}
				case 11:
					{
						strReturn = "November";
						break;
					}
				case 12:
					{
						strReturn = "December";
						break;
					}
			}
			//end switch
			GetMonthName = strReturn;
			return GetMonthName;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			lngRecordCounter = 0;
			curCreditTotal = 0;
			curDebitTotal = 0;
			lngTotalMonths = 0;
			if (theReport.ActivityRangeType == 2)
			{
				strDateCaption = GetMonthName(theReport.StartPeriod) + "  -  " + GetMonthName(theReport.EndPeriod);
			}
			else
			{
				strDateCaption = "";
			}
			lblDateRange.Text = strDateCaption;
			if (currentAccount.IsRevenue())
			{
				lblYTDExp.Text = "= YTD Net";
				fldYTDEnc.Visible = false;
				lblYTDEnc.Visible = false;
			}
			else if (currentAccount.IsLedger())
			{
				lblBudget.Text = "= Beg Bal";
				lblBudAdj.Text = "= Adjust";
				lblYTDExp.Text = "= YTD Net";
				Label24.Text = "--Balance Entries--";
			}
			RetrieveInfo();
			GetDetailRecords();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lstDetails.IsCurrent())
			{
				cAccountActivityItem aaItem;
				aaItem = (cAccountActivityItem)lstDetails.GetCurrentItem();
				fldPeriod.Text = aaItem.Period.ToString();
				fldJournal.Text = aaItem.JournalNumber.ToString();
				if (aaItem.VendorNumber > 0)
				{
					if (aaItem.VendorName != "")
					{
						fldVendor.Text = FCConvert.ToString(aaItem.VendorNumber) + " " + aaItem.VendorName;
					}
					else
					{
						fldVendor.Text = FCConvert.ToString(aaItem.VendorNumber) + " Unknown";
					}
				}
				else
				{
					fldVendor.Text = "";
				}
				fldDescription.Text = aaItem.Description;
				fldRCB.Text = aaItem.RCB;
				fldType.Text = aaItem.JournalType;
				if (aaItem.Credits != 0 || aaItem.Debits != 0)
				{
					fldCredits.Text = Strings.Format(aaItem.Credits, "0.00");
					fldDebits.Text = Strings.Format(aaItem.Debits, "0.00");
				}
				else
				{
					fldCredits.Text = "";
					fldDebits.Text = "";
				}
				curDebitTotal += FCConvert.ToDecimal(aaItem.Debits);
				curCreditTotal += FCConvert.ToDecimal(aaItem.Credits);
				fldCheck.Text = aaItem.CheckNumber;
				fldDate.Text = Strings.Format(aaItem.ActivityDate, "MM/dd/yy");
				lstDetails.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			int counter;
			int intRowCounter;
			int EndMonth;
			int intDifference = 0;
			RetrieveSummaryInfo();
			curRegularMonthCreditTotal = 0;
			curRegularMonthDebitTotal = 0;
			curBudgetMonthCreditTotal = 0;
			curBudgetMonthDebitTotal = 0;
			lngTotalMonths = 0;
			EndMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (EndMonth == 1)
			{
				EndMonth = 12;
			}
			else
			{
				EndMonth -= 1;
			}
			BegMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (BegMonth < EndMonth)
			{
				intDifference = (EndMonth - BegMonth) + 1;
			}
			else if (BegMonth == EndMonth)
			{
				intDifference = 1;
			}
			else
			{
				intDifference = ((12 - BegMonth) + EndMonth) + 1;
			}
			blnFirstMonth = true;
			intCounter = 0;
			for (counter = 1; counter <= intDifference; counter++)
			{
				switch (BegMonth)
				{
					case 1:
						{
							ShowJanTotals();
							break;
						}
					case 2:
						{
							ShowFebTotals();
							break;
						}
					case 3:
						{
							ShowMarTotals();
							break;
						}
					case 4:
						{
							ShowAprTotals();
							break;
						}
					case 5:
						{
							ShowMayTotals();
							break;
						}
					case 6:
						{
							ShowJuneTotals();
							break;
						}
					case 7:
						{
							ShowJulyTotals();
							break;
						}
					case 8:
						{
							ShowAugTotals();
							break;
						}
					case 9:
						{
							ShowSeptTotals();
							break;
						}
					case 10:
						{
							ShowOctTotals();
							break;
						}
					case 11:
						{
							ShowNovTotals();
							break;
						}
					case 12:
						{
							ShowDecTotals();
							break;
						}
				}
				//end switch
				if (BegMonth == 12)
				{
					BegMonth = 1;
				}
				else
				{
					BegMonth += 1;
				}
			}
			fldTotals.Top = Label25.Top + 315 / 1440f + (lngTotalMonths * fldJanBudCredit.Height) + 150 / 1440f;
			fldTotalRegDebit.Top = Label25.Top + 315 / 1440f + (lngTotalMonths * fldJanBudCredit.Height) + 150 / 1440f;
			fldTotalRegCredit.Top = Label25.Top + 315 / 1440f + (lngTotalMonths * fldJanBudCredit.Height) + 150 / 1440f;
			fldTotalBudDebit.Top = Label25.Top + 315 / 1440f + (lngTotalMonths * fldJanBudCredit.Height) + 150 / 1440f;
			fldTotalBudCredit.Top = Label25.Top + 315 / 1440f + (lngTotalMonths * fldJanBudCredit.Height) + 150 / 1440f;
			fldTotalRegDebit.Text = Strings.Format(curRegularMonthDebitTotal, "#,##0.00");
			fldTotalRegCredit.Text = Strings.Format(curRegularMonthCreditTotal, "#,##0.00");
			fldTotalBudDebit.Text = Strings.Format(curBudgetMonthDebitTotal, "#,##0.00");
			fldTotalBudCredit.Text = Strings.Format(curBudgetMonthCreditTotal, "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			curDebitTotal = 0;
			curCreditTotal = 0;
		}

		private string FigureDebitOrCredit_2(string strType, string strRCB, double dblAmount, double dblDiscount, double dblEncumbrance)
		{
			return FigureDebitOrCredit(ref strType, strRCB, dblAmount, dblDiscount, dblEncumbrance);
		}

		private string FigureDebitOrCredit(ref string strType, string strRCB, double dblAmount, double dblDiscount, double dblEncumbrance)
		{
			string FigureDebitOrCredit = "";
			if (strType == "A")
			{
				if (strRCB != "C" && (dblAmount - dblDiscount - dblEncumbrance) > 0)
				{
					FigureDebitOrCredit = "D";
				}
				else if (strRCB != "C" && (dblAmount - dblDiscount - dblEncumbrance) < 0)
				{
					if (dblEncumbrance != 0 && dblEncumbrance > dblAmount)
					{
						FigureDebitOrCredit = "D";
					}
					else
					{
						FigureDebitOrCredit = "C";
					}
				}
				else if (strRCB == "C" && dblAmount > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			else
			{
				if (strRCB != "C" && dblAmount > 0)
				{
					FigureDebitOrCredit = "D";
				}
				else if (strRCB != "C" && strRCB != "E" && dblAmount < 0)
				{
					FigureDebitOrCredit = "C";
				}
				else if (strRCB == "C" && dblAmount > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			return FigureDebitOrCredit;
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		private double GetMonthlyDebit(ref int intMonth)
		{
			double GetMonthlyDebit = 0;
			if (rsSummaryActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strCurrentAccount, ","))
			{
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				GetMonthlyDebit = Conversion.Val(rsSummaryActivityDetail.Get_Fields("PostedDebitsTotal"));
			}
			else
			{
				GetMonthlyDebit = 0;
			}
			if (Strings.Left(strCurrentAccount, 1) != "G")
			{
				GetMonthlyDebit += GetMonthlyEncumbrance(ref intMonth);
			}
			else
			{
				GetMonthlyDebit += GetMonthlyEncumbranceDebits(ref intMonth);
			}
			return GetMonthlyDebit;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		private double GetMonthlyCredit(ref int intMonth)
		{
			double GetMonthlyCredit = 0;
			if (rsSummaryActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strCurrentAccount, ","))
			{
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				GetMonthlyCredit = Conversion.Val(rsSummaryActivityDetail.Get_Fields("PostedCreditsTotal")) * -1;
			}
			else
			{
				GetMonthlyCredit = 0;
			}
			if (Strings.Left(strCurrentAccount, 1) != "G")
			{
				// do nothing
			}
			else
			{
				GetMonthlyCredit += GetMonthlyEncumbranceCredits(ref intMonth);
			}
			return GetMonthlyCredit;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		private double GetMonthlyBudgetDebit(ref int intMonth)
		{
			double GetMonthlyBudgetDebit = 0;
			double sum1 = 0;
			// vbPorter upgrade warning: HighDate As DateTime	OnWrite(string)
			DateTime HighDate = default(DateTime);
			// vbPorter upgrade warning: LowDate As DateTime	OnWrite(string)
			DateTime LowDate = default(DateTime);
            using (clsDRWrapper rs2 = new clsDRWrapper())
            {
                if (theReport.ActivityRangeType == 1)
                {
                    LowDate = FCConvert.ToDateTime(theReport.StartDate);
                    HighDate = FCConvert.ToDateTime(theReport.EndDate);
                }

                if (theReport.ActivityRangeType == 1)
                {
                    if (theReport.DateType == 0)
                    {
                        rs2.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE Account = '" +
                                          strCurrentAccount +
                                          "' AND Status <> 'E' AND Amount > 0 AND RCB = 'B' AND Period = " +
                                          FCConvert.ToString(intMonth) + " AND PostedDate BETWEEN '" +
                                          FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "'");
                    }
                    else
                    {
                        rs2.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE Account = '" +
                                          strCurrentAccount +
                                          "' AND Status <> 'E' AND Amount > 0 AND RCB = 'B' AND Period = " +
                                          FCConvert.ToString(intMonth) + " AND JournalEntriesDate BETWEEN '" +
                                          FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "'");
                    }
                }
                else
                {
                    rs2.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE Account = '" +
                                      strCurrentAccount +
                                      "' AND Status <> 'E' AND Amount > 0 AND RCB = 'B' AND Period = " +
                                      FCConvert.ToString(intMonth));
                }

                // TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
                if (!(FCConvert.ToString(rs2.Get_Fields("Debits")) == ""))
                {
                    // TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
                    sum1 = Conversion.Val(rs2.Get_Fields("Debits"));
                }
                else
                {
                    sum1 = 0;
                }

                GetMonthlyBudgetDebit = sum1;
            }

            return GetMonthlyBudgetDebit;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		private double GetMonthlyBudgetCredit(ref int intMonth)
		{
			double GetMonthlyBudgetCredit = 0;
			double sum1 = 0;
			// vbPorter upgrade warning: HighDate As DateTime	OnWrite(string)
			DateTime HighDate = default(DateTime);
			// vbPorter upgrade warning: LowDate As DateTime	OnWrite(string)
			DateTime LowDate = default(DateTime);
            using (clsDRWrapper rs2 = new clsDRWrapper())
            {
                if (theReport.ActivityRangeType == 1)
                {
                    LowDate = FCConvert.ToDateTime(theReport.StartDate);
                    HighDate = FCConvert.ToDateTime(theReport.EndDate);
                }

                if (theReport.ActivityRangeType == 1)
                {
                    if (theReport.DateType == 0)
                    {
                        rs2.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE Account = '" +
                                          strCurrentAccount +
                                          "' AND Status <> 'E' AND Amount < 0 AND RCB = 'B' AND Period = " +
                                          FCConvert.ToString(intMonth) + " AND PostedDate BETWEEN '" +
                                          FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "'");
                    }
                    else
                    {
                        rs2.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE Account = '" +
                                          strCurrentAccount +
                                          "' AND Status <> 'E' AND Amount < 0 AND RCB = 'B' AND Period = " +
                                          FCConvert.ToString(intMonth) + " AND JournalEntriesDate BETWEEN '" +
                                          FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "'");
                    }
                }
                else
                {
                    rs2.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE Account = '" +
                                      strCurrentAccount +
                                      "' AND Status <> 'E' AND Amount < 0 AND RCB = 'B' AND Period = " +
                                      FCConvert.ToString(intMonth));
                }

                // TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
                if (!(FCConvert.ToString(rs2.Get_Fields("Credits")) == ""))
                {
                    // TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
                    sum1 = Conversion.Val(rs2.Get_Fields("Credits")) * -1;
                }
                else
                {
                    sum1 = 0;
                }

                GetMonthlyBudgetCredit = sum1;
            }

            return GetMonthlyBudgetCredit;
		}

		private double GetMonthlyEncumbrance(ref int intMonth)
		{
			double GetMonthlyEncumbrance = 0;
			if (rsSummaryActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strCurrentAccount, ","))
			{
				// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
				GetMonthlyEncumbrance = Conversion.Val(rsSummaryActivityDetail.Get_Fields("EncumbActivityTotal"));
			}
			else
			{
				GetMonthlyEncumbrance = 0;
			}
			return GetMonthlyEncumbrance;
		}

		private double GetMonthlyEncumbranceDebits(ref int intMonth)
		{
			double GetMonthlyEncumbranceDebits = 0;
			if (rsSummaryActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strCurrentAccount, ","))
			{
				// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
				GetMonthlyEncumbranceDebits = Conversion.Val(rsSummaryActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
			}
			else
			{
				GetMonthlyEncumbranceDebits = 0;
			}
			return GetMonthlyEncumbranceDebits;
		}

		private double GetMonthlyEncumbranceCredits(ref int intMonth)
		{
			double GetMonthlyEncumbranceCredits = 0;
			if (rsSummaryActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strCurrentAccount, ","))
			{
				// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
				GetMonthlyEncumbranceCredits = Conversion.Val(rsSummaryActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
			}
			else
			{
				GetMonthlyEncumbranceCredits = 0;
			}
			return GetMonthlyEncumbranceCredits;
		}

		private void ShowJanTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthJan.Top = fldMonthJan.Top;
				fldJanRegDebit.Top = fldJanRegDebit.Top;
				fldJanRegCredit.Top = fldJanRegCredit.Top;
				fldJanBudDebit.Top = fldJanBudDebit.Top;
				fldJanBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthJan.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJanRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJanRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJanBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJanBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthJan.Visible = true;
				fldJanRegCredit.Visible = true;
				fldJanRegDebit.Visible = true;
				fldJanBudDebit.Visible = true;
				fldJanBudCredit.Visible = true;
				fldMonthJan.Text = MonthCalc(1);
				fldJanRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldJanRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldJanBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldJanBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldJanRegDebit.Text = "";
				fldJanRegCredit.Text = "";
				fldJanBudDebit.Text = "";
				fldJanBudCredit.Text = "";
				fldMonthJan.Text = "";
			}
		}

		private void ShowFebTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthFeb.Top = fldMonthJan.Top;
				fldFebRegDebit.Top = fldJanRegDebit.Top;
				fldFebRegCredit.Top = fldJanRegCredit.Top;
				fldFebBudDebit.Top = fldJanBudDebit.Top;
				fldFebBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthFeb.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldFebRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldFebRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldFebBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldFebBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthFeb.Visible = true;
				fldFebRegCredit.Visible = true;
				fldFebRegDebit.Visible = true;
				fldFebBudDebit.Visible = true;
				fldFebBudCredit.Visible = true;
				fldMonthFeb.Text = MonthCalc(2);
				fldFebRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldFebRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldFebBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldFebBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldFebRegDebit.Text = "";
				fldFebRegCredit.Text = "";
				fldFebBudDebit.Text = "";
				fldFebBudCredit.Text = "";
				fldMonthFeb.Text = "";
			}
		}

		private void ShowMarTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthMar.Top = fldMonthJan.Top;
				fldMarRegDebit.Top = fldJanRegDebit.Top;
				fldMarRegCredit.Top = fldJanRegCredit.Top;
				fldMarBudDebit.Top = fldJanBudDebit.Top;
				fldMarBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthMar.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMarRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMarRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMarBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMarBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthMar.Visible = true;
				fldMarRegCredit.Visible = true;
				fldMarRegDebit.Visible = true;
				fldMarBudDebit.Visible = true;
				fldMarBudCredit.Visible = true;
				fldMonthMar.Text = MonthCalc(3);
				fldMarRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldMarRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldMarBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldMarBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldMarRegDebit.Text = "";
				fldMarRegCredit.Text = "";
				fldMarBudDebit.Text = "";
				fldMarBudCredit.Text = "";
				fldMonthMar.Text = "";
			}
		}

		private void ShowAprTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthApr.Top = fldMonthJan.Top;
				fldAprRegDebit.Top = fldJanRegDebit.Top;
				fldAprRegCredit.Top = fldJanRegCredit.Top;
				fldAprBudDebit.Top = fldJanBudDebit.Top;
				fldAprBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthApr.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAprRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAprRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAprBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAprBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthApr.Visible = true;
				fldAprRegCredit.Visible = true;
				fldAprRegDebit.Visible = true;
				fldAprBudDebit.Visible = true;
				fldAprBudCredit.Visible = true;
				fldMonthApr.Text = MonthCalc(4);
				fldAprRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldAprRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldAprBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldAprBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldAprRegDebit.Text = "";
				fldAprRegCredit.Text = "";
				fldAprBudDebit.Text = "";
				fldAprBudCredit.Text = "";
				fldMonthApr.Text = "";
			}
		}

		private void ShowMayTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthMay.Top = fldMonthJan.Top;
				fldMayRegDebit.Top = fldJanRegDebit.Top;
				fldMayRegCredit.Top = fldJanRegCredit.Top;
				fldMayBudDebit.Top = fldJanBudDebit.Top;
				fldMayBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthMay.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMayRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMayRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMayBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldMayBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthMay.Visible = true;
				fldMayRegCredit.Visible = true;
				fldMayRegDebit.Visible = true;
				fldMayBudDebit.Visible = true;
				fldMayBudCredit.Visible = true;
				fldMonthMay.Text = MonthCalc(5);
				fldMayRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldMayRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldMayBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldMayBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldMayRegDebit.Text = "";
				fldMayRegCredit.Text = "";
				fldMayBudDebit.Text = "";
				fldMayBudCredit.Text = "";
				fldMonthMay.Text = "";
			}
		}

		private void ShowJuneTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthJune.Top = fldMonthJan.Top;
				fldJuneRegDebit.Top = fldJanRegDebit.Top;
				fldJuneRegCredit.Top = fldJanRegCredit.Top;
				fldJuneBudDebit.Top = fldJanBudDebit.Top;
				fldJuneBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthJune.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJuneRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJuneRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJuneBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJuneBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthJune.Visible = true;
				fldJuneRegCredit.Visible = true;
				fldJuneRegDebit.Visible = true;
				fldJuneBudDebit.Visible = true;
				fldJuneBudCredit.Visible = true;
				fldMonthJune.Text = MonthCalc(6);
				fldJuneRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldJuneRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldJuneBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldJuneBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldJuneRegDebit.Text = "";
				fldJuneRegCredit.Text = "";
				fldJuneBudDebit.Text = "";
				fldJuneBudCredit.Text = "";
				fldMonthJune.Text = "";
			}
		}

		private void ShowJulyTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthJuly.Top = fldMonthJan.Top;
				fldJulyRegDebit.Top = fldJanRegDebit.Top;
				fldJulyRegCredit.Top = fldJanRegCredit.Top;
				fldJulyBudDebit.Top = fldJanBudDebit.Top;
				fldJulyBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthJuly.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJulyRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJulyRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJulyBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldJulyBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthJuly.Visible = true;
				fldJulyRegCredit.Visible = true;
				fldJulyRegDebit.Visible = true;
				fldJulyBudDebit.Visible = true;
				fldJulyBudCredit.Visible = true;
				fldMonthJuly.Text = MonthCalc(7);
				fldJulyRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldJulyRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldJulyBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldJulyBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldJulyRegDebit.Text = "";
				fldJulyRegCredit.Text = "";
				fldJulyBudDebit.Text = "";
				fldJulyBudCredit.Text = "";
				fldMonthJuly.Text = "";
			}
		}

		private void ShowAugTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthAug.Top = fldMonthJan.Top;
				fldAugRegDebit.Top = fldJanRegDebit.Top;
				fldAugRegCredit.Top = fldJanRegCredit.Top;
				fldAugBudDebit.Top = fldJanBudDebit.Top;
				fldAugBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthAug.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAugRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAugRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAugBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldAugBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthAug.Visible = true;
				fldAugRegCredit.Visible = true;
				fldAugRegDebit.Visible = true;
				fldAugBudDebit.Visible = true;
				fldAugBudCredit.Visible = true;
				fldMonthAug.Text = MonthCalc(8);
				fldAugRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldAugRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldAugBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldAugBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldAugRegDebit.Text = "";
				fldAugRegCredit.Text = "";
				fldAugBudDebit.Text = "";
				fldAugBudCredit.Text = "";
				fldMonthAug.Text = "";
			}
		}

		private void ShowSeptTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthSept.Top = fldMonthJan.Top;
				fldSeptRegDebit.Top = fldJanRegDebit.Top;
				fldSeptRegCredit.Top = fldJanRegCredit.Top;
				fldSeptBudDebit.Top = fldJanBudDebit.Top;
				fldSeptBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthSept.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldSeptRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldSeptRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldSeptBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldSeptBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthSept.Visible = true;
				fldSeptRegCredit.Visible = true;
				fldSeptRegDebit.Visible = true;
				fldSeptBudDebit.Visible = true;
				fldSeptBudCredit.Visible = true;
				fldMonthSept.Text = MonthCalc(9);
				fldSeptRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldSeptRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldSeptBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldSeptBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldSeptRegDebit.Text = "";
				fldSeptRegCredit.Text = "";
				fldSeptBudDebit.Text = "";
				fldSeptBudCredit.Text = "";
				fldMonthSept.Text = "";
			}
		}

		private void ShowOctTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthOct.Top = fldMonthJan.Top;
				fldOctRegDebit.Top = fldJanRegDebit.Top;
				fldOctRegCredit.Top = fldJanRegCredit.Top;
				fldOctBudDebit.Top = fldJanBudDebit.Top;
				fldOctBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthOct.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldOctRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldOctRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldOctBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldOctBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthOct.Visible = true;
				fldOctRegCredit.Visible = true;
				fldOctRegDebit.Visible = true;
				fldOctBudDebit.Visible = true;
				fldOctBudCredit.Visible = true;
				fldMonthOct.Text = MonthCalc(10);
				fldOctRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldOctRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldOctBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldOctBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldOctRegDebit.Text = "";
				fldOctRegCredit.Text = "";
				fldOctBudDebit.Text = "";
				fldOctBudCredit.Text = "";
				fldMonthOct.Text = "";
			}
		}

		private void ShowNovTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthNov.Top = fldMonthJan.Top;
				fldNovRegDebit.Top = fldJanRegDebit.Top;
				fldNovRegCredit.Top = fldJanRegCredit.Top;
				fldNovBudDebit.Top = fldJanBudDebit.Top;
				fldNovBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthNov.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldNovRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldNovRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldNovBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldNovBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthNov.Visible = true;
				fldNovRegCredit.Visible = true;
				fldNovRegDebit.Visible = true;
				fldNovBudDebit.Visible = true;
				fldNovBudCredit.Visible = true;
				fldMonthNov.Text = MonthCalc(11);
				fldNovRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldNovRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldNovBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldNovBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldNovRegDebit.Text = "";
				fldNovRegCredit.Text = "";
				fldNovBudDebit.Text = "";
				fldNovBudCredit.Text = "";
				fldMonthNov.Text = "";
			}
		}

		private void ShowDecTotals()
		{
			curRegularMonthDebit = FCConvert.ToDecimal(GetMonthlyDebit(ref BegMonth));
			curRegularMonthCredit = FCConvert.ToDecimal(GetMonthlyCredit(ref BegMonth));
			curBudgetMonthDebit = FCConvert.ToDecimal(GetMonthlyBudgetDebit(ref BegMonth));
			curBudgetMonthCredit = FCConvert.ToDecimal(GetMonthlyBudgetCredit(ref BegMonth));
			curRegularMonthCreditTotal += curRegularMonthCredit;
			curRegularMonthDebitTotal += curRegularMonthDebit;
			curBudgetMonthCreditTotal += curBudgetMonthCredit;
			curBudgetMonthDebitTotal += curBudgetMonthDebit;
			if (blnFirstMonth)
			{
				blnFirstMonth = false;
				fldMonthDec.Top = fldMonthJan.Top;
				fldDecRegDebit.Top = fldJanRegDebit.Top;
				fldDecRegCredit.Top = fldJanRegCredit.Top;
				fldDecBudDebit.Top = fldJanBudDebit.Top;
				fldDecBudCredit.Top = fldJanBudCredit.Top;
				lngTop = fldMonthJan.Top;
				intCounter += 1;
			}
			else
			{
				fldMonthDec.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldDecRegDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldDecRegCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldDecBudDebit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				fldDecBudCredit.Top = lngTop + (lngTotalMonths * fldMonthJan.Height);
				intCounter += 1;
			}
			if (curRegularMonthCredit != 0 || curRegularMonthDebit != 0 || curBudgetMonthDebit != 0 || curBudgetMonthCredit != 0)
			{
				fldMonthDec.Visible = true;
				fldDecRegCredit.Visible = true;
				fldDecRegDebit.Visible = true;
				fldDecBudDebit.Visible = true;
				fldDecBudCredit.Visible = true;
				fldMonthDec.Text = MonthCalc(12);
				fldDecRegDebit.Text = Strings.Format(curRegularMonthDebit, "#,##0.00");
				fldDecRegCredit.Text = Strings.Format(curRegularMonthCredit, "#,##0.00");
				fldDecBudDebit.Text = Strings.Format(curBudgetMonthDebit, "#,##0.00");
				fldDecBudCredit.Text = Strings.Format(curBudgetMonthCredit, "#,##0.00");
				lngTotalMonths += 1;
			}
			else
			{
				fldDecRegDebit.Text = "";
				fldDecRegCredit.Text = "";
				fldDecBudDebit.Text = "";
				fldDecBudCredit.Text = "";
				fldMonthDec.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.ParentReport.PageNumber;
			// rptCurrentAccountStatusHolder.pageNumber
			lblAccount.Text = strCurrentAccount + "  " + currentAccount.ShortDescription;
			// ReturnAccountDescription(strCurrentAccount)
			fldBudget.Text = Strings.Format(GetOriginalBudget(), "#,##0.00");
			if (Strings.Left(strCurrentAccount, 1) != "E" && Strings.Left(strCurrentAccount, 1) != "P")
			{
				fldBudgetAdjustments.Text = Strings.Format(GetBudgetAdjustments(), "#,##0.00");
			}
			else
			{
				fldBudgetAdjustments.Text = Strings.Format(GetBudgetAdjustments() * -1, "#,##0.00");
			}
			fldYTDExp.Text = Strings.Format(GetYTDNet(), "#,##0.00");
			// + CCur(fldBudgetAdjustments.Text
			fldYTDEnc.Text = Strings.Format(GetEncumbrance(), "#,##0.00");
			if (Strings.Left(strCurrentAccount, 1) != "G" && Strings.Left(strCurrentAccount, 1) != "L")
			{
				if (Strings.Left(strCurrentAccount, 1) != "R" && Strings.Left(strCurrentAccount, 1) != "V")
				{
					fldBalance.Text = Strings.Format(FCConvert.ToDecimal(fldBudget.Text) + FCConvert.ToDecimal(fldBudgetAdjustments.Text) - FCConvert.ToDecimal(fldYTDExp.Text) - FCConvert.ToDecimal(fldYTDEnc.Text), "#,##0.00");
				}
				else
				{
					fldBalance.Text = Strings.Format(FCConvert.ToDecimal(fldBudget.Text) + FCConvert.ToDecimal(fldBudgetAdjustments.Text) + FCConvert.ToDecimal(fldYTDExp.Text), "#,##0.00");
				}
			}
			else
			{
				fldBalance.Text = Strings.Format(FCConvert.ToDecimal(fldBudget.Text) + FCConvert.ToDecimal(fldBudgetAdjustments.Text) + FCConvert.ToDecimal(fldYTDExp.Text) + FCConvert.ToDecimal(fldYTDEnc.Text), "#,##0.00");
			}
		}

		private double GetOriginalBudget()
		{
			double GetOriginalBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				GetOriginalBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments()
		{
			double GetBudgetAdjustments = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
				GetBudgetAdjustments = Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				GetYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
			}
			else
			{
				GetYTDDebit = 0;
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				GetYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
			}
			else
			{
				GetYTDCredit = 0;
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit() - GetYTDCredit();
			return GetYTDNet;
		}

		private double GetEncumbrance()
		{
			double GetEncumbrance = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
				GetEncumbrance = Conversion.Val(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private double GetEncumbranceDebits()
		{
			double GetEncumbranceDebits = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
				GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
			}
			else
			{
				GetEncumbranceDebits = 0;
			}
			return GetEncumbranceDebits;
		}

		private double GetEncumbranceCredits()
		{
			double GetEncumbranceCredits = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strCurrentAccount))
			{
				// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
				GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
			}
			else
			{
				GetEncumbranceCredits = 0;
			}
			return GetEncumbranceCredits;
		}

		private void GetDetailRecords()
		{
			int HighMonth = 0;
			int LowMonth = 0;
			// vbPorter upgrade warning: HighDate As DateTime	OnWrite(string)
			DateTime HighDate = default(DateTime);
			// vbPorter upgrade warning: LowDate As DateTime	OnWrite(string)
			DateTime LowDate = default(DateTime);
			//clsDRWrapper rs2 = new clsDRWrapper();
			//clsDRWrapper rs3 = new clsDRWrapper();
			//clsDRWrapper rs4 = new clsDRWrapper();
			//clsDRWrapper rs5 = new clsDRWrapper();
			int counter;
			int counter2;
			if (theReport.ActivityRangeType == 2)
			{
				HighMonth = theReport.EndPeriod;
				LowMonth = theReport.StartPeriod;
			}
			else if (theReport.ActivityRangeType == 1)
			{
				LowDate = FCConvert.ToDateTime(theReport.StartDate);
				HighDate = FCConvert.ToDateTime(theReport.EndDate);
			}
			else
			{
				HighMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
				LowMonth = HighMonth;
				if (HighMonth == 1)
				{
					HighMonth = 12;
				}
				else
				{
					HighMonth -= 1;
				}
			}
			if (LowMonth > HighMonth)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (theReport.ActivityRangeType == 1)
			{
				if (theReport.DateType == 0)
				{
					rsAPJournals.OpenRecordset("SELECT APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, APJournalDetail.RCB as RCB, APJournalDetail.Description as Description, APJournal.JournalNumber as JournalNumber, APJournal.CheckNumber as CheckNumber, APJournal.Period as Period, APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournal.TempVendorAddress1 as TempVendorAddress1, APJournal.TempVendorAddress2 as TempVendorAddress2, APJournal.TempVendorAddress3 as TempVendorAddress3, APJournal.TempVendorCity as TempVendorCity, APJournal.TempVendorState as TempVendorState, APJournal.TempVendorZip as TempVendorZip, APJournal.TempVendorZip4 as TempVendorZip4, APJournal.CheckDate as TransDate, APJournal.PostedDate as PostedDate, APJournal.ID as [ID], APJournalDetail.ID as DetailNumber FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) " + "WHERE APJournalDetail.Account = '" + strCurrentAccount + "' AND APJournal.Status = 'P' AND APJournal.PostedDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY APJournal.Period, APJournal.PostedDate, APJournal.JournalNumber", "Budgetary");
					rsJournalEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE Account = '" + strCurrentAccount + "' AND Status = 'P' AND PostedDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY Period, PostedDate, JournalNumber", "Budgetary");
					rsEncJournals.OpenRecordset("SELECT EncumbranceDetail.Amount as Amount, EncumbranceDetail.Description as Description, Encumbrances.JournalNumber as JournalNumber, Encumbrances.Period as Period, Encumbrances.VendorNumber as VendorNumber, Encumbrances.TempVendorName as TempVendorName, Encumbrances.TempVendorAddress1 as TempVendorAddress1, Encumbrances.TempVendorAddress2 as TempVendorAddress2, Encumbrances.TempVendorAddress3 as TempVendorAddress3, Encumbrances.TempVendorCity as TempVendorCity, Encumbrances.TempVendorState as TempVendorState, Encumbrances.TempVendorZip as TempVendorZip, Encumbrances.TempVendorZip4 as TempVendorZip4, Encumbrances.EncumbrancesDate as TransDate, Encumbrances.PostedDate as PostedDate, Encumbrances.ID as [ID], EncumbranceDetail.ID as DetailNumber FROM (EncumbranceDetail INNER JOIN Encumbrances ON EncumbranceDetail.EncumbranceID = Encumbrances.ID) " + "WHERE EncumbranceDetail.Account = '" + strCurrentAccount + "' AND Encumbrances.Status = 'P' AND Encumbrances.PostedDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY Encumbrances.Period, Encumbrances.PostedDate, Encumbrances.JournalNumber", "Budgetary");
				}
				else
				{
					rsAPJournals.OpenRecordset("SELECT APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, APJournalDetail.RCB as RCB, APJournalDetail.Description as Description, APJournal.JournalNumber as JournalNumber, APJournal.CheckNumber as CheckNumber, APJournal.Period as Period, APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournal.TempVendorAddress1 as TempVendorAddress1, APJournal.TempVendorAddress2 as TempVendorAddress2, APJournal.TempVendorAddress3 as TempVendorAddress3, APJournal.TempVendorCity as TempVendorCity, APJournal.TempVendorState as TempVendorState, APJournal.TempVendorZip as TempVendorZip, APJournal.TempVendorZip4 as TempVendorZip4, APJournal.CheckDate as TransDate, APJournal.PostedDate as PostedDate, APJournal.ID as [ID], APJournalDetail.ID as DetailNumber FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) " + "WHERE APJournalDetail.Account = '" + strCurrentAccount + "' AND APJournal.Status = 'P' AND APJournal.CheckDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY APJournal.Period, APJournal.CheckDate, APJournal.JournalNumber");
					rsJournalEntries.OpenRecordset("SELECT *, JournalEntriesDate as TransDate FROM JournalEntries WHERE Account = '" + strCurrentAccount + "' AND Status = 'P' AND JournalEntriesDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY Period, JournalEntriesDate, JournalNumber");
					rsEncJournals.OpenRecordset("SELECT EncumbranceDetail.Amount as Amount, EncumbranceDetail.Description as Description, Encumbrances.JournalNumber as JournalNumber, Encumbrances.Period as Period, Encumbrances.VendorNumber as VendorNumber, Encumbrances.TempVendorName as TempVendorName, Encumbrances.TempVendorAddress1 as TempVendorAddress1, Encumbrances.TempVendorAddress2 as TempVendorAddress2, Encumbrances.TempVendorAddress3 as TempVendorAddress3, Encumbrances.TempVendorCity as TempVendorCity, Encumbrances.TempVendorState as TempVendorState, Encumbrances.TempVendorZip as TempVendorZip, Encumbrances.TempVendorZip4 as TempVendorZip4, Encumbrances.EncumbrancesDate as TransDate, Encumbrances.PostedDate as PostedDate, Encumbrances.ID as [ID], EncumbranceDetail.ID as DetailNumber FROM (EncumbranceDetail INNER JOIN Encumbrances ON EncumbranceDetail.EncumbranceID = Encumbrances.ID) " + "WHERE EncumbranceDetail.Account = '" + strCurrentAccount + "' AND Encumbrances.Status = 'P' AND Encumbrances.EncumbrancesDate BETWEEN '" + FCConvert.ToString(LowDate) + "' AND '" + FCConvert.ToString(HighDate) + "' ORDER BY Encumbrances.Period, Encumbrances.EncumbrancesDate, Encumbrances.JournalNumber");
				}
			}
			else
			{
				rsAPJournals.OpenRecordset("SELECT APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, APJournalDetail.Encumbrance as Encumbrance, APJournalDetail.RCB as RCB, APJournalDetail.Description as Description, APJournal.JournalNumber as JournalNumber, APJournal.CheckNumber as CheckNumber" + ", APJournal.Period as Period, APJournal.VendorNumber as VendorNumber, APJournal.TempVendorName as TempVendorName, APJournal.TempVendorAddress1 as TempVendorAddress1, APJournal.TempVendorAddress2 as TempVendorAddress2, APJournal.TempVendorAddress3 as TempVendorAddress3, APJournal.TempVendorCity as TempVendorCity, APJournal.TempVendorState as TempVendorState, APJournal.TempVendorZip as TempVendorZip, APJournal.TempVendorZip4 as TempVendorZip4, APJournal.CheckDate as TransDate, APJournal.PostedDate as PostedDate, APJournal.ID as [ID], APJournalDetail.ID as DetailNumber FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE APJournalDetail.Account = '" + strCurrentAccount + "' AND APJournal.Status = 'P' AND (APJournal.Period >= " + FCConvert.ToString(LowMonth) + strPeriodCheck + " APJournal.Period <= " + FCConvert.ToString(HighMonth) + ") ORDER BY APJournal.Period, APJournal.PostedDate, APJournal.JournalNumber");
				rsJournalEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE Account = '" + strCurrentAccount + "' AND Status = 'P' AND (Period >= " + FCConvert.ToString(LowMonth) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonth) + ") ORDER BY  PostedDate, JournalNumber");
				rsEncJournals.OpenRecordset("SELECT EncumbranceDetail.Amount as Amount, EncumbranceDetail.Description as Description, Encumbrances.JournalNumber as JournalNumber, Encumbrances.Period as Period, Encumbrances.VendorNumber as VendorNumber, Encumbrances.TempVendorName as TempVendorName, Encumbrances.TempVendorAddress1 as TempVendorAddress1, Encumbrances.TempVendorAddress2 as TempVendorAddress2, Encumbrances.TempVendorAddress3 as TempVendorAddress3, Encumbrances.TempVendorCity as TempVendorCity, Encumbrances.TempVendorState as TempVendorState, Encumbrances.TempVendorZip as TempVendorZip, Encumbrances.TempVendorZip4 as TempVendorZip4, Encumbrances.EncumbrancesDate as TransDate, Encumbrances.PostedDate as PostedDate, Encumbrances.ID as [ID], EncumbranceDetail.ID as DetailNumber " + "FROM (EncumbranceDetail INNER JOIN Encumbrances ON EncumbranceDetail.EncumbranceID = Encumbrances.ID) WHERE EncumbranceDetail.Account = '" + strCurrentAccount + "' AND Encumbrances.Status = 'P' AND (Encumbrances.Period >= " + FCConvert.ToString(LowMonth) + strPeriodCheck + " Encumbrances.Period <= " + FCConvert.ToString(HighMonth) + ") ORDER BY Encumbrances.Period, Encumbrances.PostedDate, Encumbrances.JournalNumber");
			}
			// frmCurrentAccountStatus.vsSort.rows = rsEncJournals.RecordCount + rsJournalEntries.RecordCount + rsAPJournals.RecordCount
			lngNumberOfDetailRecords = rsEncJournals.RecordCount() + rsJournalEntries.RecordCount() + rsAPJournals.RecordCount();
			cAccountActivityItem aaItem;
			cVendor vend;
			while (!rsAPJournals.EndOfFile())
			{
				//Application.DoEvents();
				aaItem = new cAccountActivityItem();
				aaItem.JournalType = "AP";
				if (theReport.DateType == 0)
				{
					aaItem.ActivityDate = FCConvert.ToString(rsAPJournals.Get_Fields_DateTime("PostedDate"));
				}
				else
				{
					aaItem.ActivityDate = FCConvert.ToString(rsAPJournals.Get_Fields_DateTime("CheckDate"));
				}
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				aaItem.JournalNumber = FCConvert.ToInt32(rsAPJournals.Get_Fields("JournalNumber"));
				aaItem.Description = FCConvert.ToString(rsAPJournals.Get_Fields_String("Description"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				aaItem.Period = FCConvert.ToInt32(rsAPJournals.Get_Fields("Period"));
				aaItem.RCB = FCConvert.ToString(rsAPJournals.Get_Fields_String("RCB"));
				aaItem.Account = strCurrentAccount;
				aaItem.VendorNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsAPJournals.Get_Fields_Int32("VendorNumber"))));
				if (aaItem.VendorNumber > 0)
				{
					vend = vendCont.GetVendorByVendorNumber(aaItem.VendorNumber);
					if (!(vend == null))
					{
						aaItem.VendorName = vend.CheckName;
					}
					else
					{
						aaItem.VendorName = "Unknown";
					}
				}
				else
				{
					aaItem.VendorName = FCConvert.ToString(rsAPJournals.Get_Fields_String("TempVendorName"));
				}
				//FC:FINAL:MSH - Issue #622: Added method for converting data, because the system can't implicitly convert decimal to double
				//if (Strings.LCase(FigureDebitOrCredit_2(rsAPJournals.Get_Fields("Type"), aaItem.RCB, rsAPJournals.Get_Fields("amount"), 0, 0)) == "d")
				//{
				//    aaItem.Debits = rsAPJournals.Get_Fields("amount");
				//}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				if (Strings.LCase(FigureDebitOrCredit_2(rsAPJournals.Get_Fields("Type"), aaItem.RCB, FCConvert.ToDouble(rsAPJournals.Get_Fields("amount")), 0.0, 0.0)) == "d")
				{
					//FC:FINAL:MSH - Issue #622: Added method for converting data, because the system can't implicitly convert decimal to double
					//aaItem.Debits = rsAPJournals.Get_Fields("amount");
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					aaItem.Debits = FCConvert.ToDouble(rsAPJournals.Get_Fields("amount"));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					aaItem.Credits = rsAPJournals.Get_Fields("amount") * -1;
				}
				lstDetails.AddItem(aaItem);
				rsAPJournals.MoveNext();
			}
			// SELECT * FROM JournalEntries WHERE Account = '" & strCurrentAccount & "' AND Status = 'P' AND (Period >= " & LowMonth & strPeriodCheck & " Period <= " & HighMonth & ") ORDER BY Period, PostedDate, JournalNumber")
			while (!rsJournalEntries.EndOfFile())
			{
				//Application.DoEvents();
				aaItem = new cAccountActivityItem();
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (Strings.LCase(FCConvert.ToString(rsJournalEntries.Get_Fields("Type"))) == "g")
				{
					aaItem.JournalType = "GJ";
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (Strings.LCase(FCConvert.ToString(rsJournalEntries.Get_Fields("Type"))) == "p")
				{
					aaItem.JournalType = "PY";
				}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (Strings.LCase(FCConvert.ToString(rsJournalEntries.Get_Fields("Type"))) == "w")
				{
					aaItem.JournalType = "CR";
				}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (Strings.LCase(FCConvert.ToString(rsJournalEntries.Get_Fields("Type"))) == "c")
				{
					aaItem.JournalType = "CR";
				}
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							else if (Strings.LCase(FCConvert.ToString(rsJournalEntries.Get_Fields("Type"))) == "d")
				{
					aaItem.JournalType = "CD";
				}
				aaItem.Account = strCurrentAccount;
				if (theReport.DateType == 0)
				{
					aaItem.ActivityDate = FCConvert.ToString(rsJournalEntries.Get_Fields_DateTime("PostedDate"));
				}
				else
				{
					aaItem.ActivityDate = FCConvert.ToString(rsJournalEntries.Get_Fields_DateTime("JournalEntriesDate"));
				}
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				aaItem.CheckNumber = FCConvert.ToString(rsJournalEntries.Get_Fields("CheckNumber"));
				if (fldType.Text == "PY" && fldRCB.Text == "E")
				{
					aaItem.Debits = 0;
					aaItem.Credits = 0;
				}
				else
				{
					//FC:FINAL:MSH - Issue #622: Added method for converting data, because the system can't implicitly convert decimal to double
					//if (Strings.LCase(FigureDebitOrCredit_2(rsJournalEntries.Get_Fields("Type"), aaItem.RCB, rsJournalEntries.Get_Fields("amount"), 0, 0)) == "d")
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					if (Strings.LCase(FigureDebitOrCredit_2(rsJournalEntries.Get_Fields("Type"), aaItem.RCB, FCConvert.ToDouble(rsJournalEntries.Get_Fields("amount")), 0.0, 0.0)) == "d")
					{
						//FC:FINAL:MSH - Issue #622: Added method for converting data, because the system can't implicitly convert decimal to double
						//aaItem.Debits = rsJournalEntries.Get_Fields("amount");
						// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
						aaItem.Debits = FCConvert.ToDouble(rsJournalEntries.Get_Fields("amount"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
						aaItem.Credits = rsJournalEntries.Get_Fields("amount") * -1;
					}
				}
				aaItem.Description = FCConvert.ToString(rsJournalEntries.Get_Fields_String("Description"));
				// TODO Get_Fields: Check the table for the column [journalnumber] and replace with corresponding Get_Field method
				aaItem.JournalNumber = FCConvert.ToInt32(rsJournalEntries.Get_Fields("journalnumber"));
				// TODO Get_Fields: Check the table for the column [period] and replace with corresponding Get_Field method
				aaItem.Period = FCConvert.ToInt32(rsJournalEntries.Get_Fields("period"));
				aaItem.RCB = FCConvert.ToString(rsJournalEntries.Get_Fields_String("RCB"));
				lstDetails.AddItem(aaItem);
				rsJournalEntries.MoveNext();
			}
			while (!rsEncJournals.EndOfFile())
			{
				//Application.DoEvents();
				aaItem = new cAccountActivityItem();
				aaItem.JournalType = "EN";
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				aaItem.Period = FCConvert.ToInt32(rsEncJournals.Get_Fields("Period"));
				aaItem.Account = strCurrentAccount;
				aaItem.RCB = FCConvert.ToString(rsEncJournals.Get_Fields_String("RCB"));
				if (theReport.DateType == 0)
				{
					aaItem.ActivityDate = FCConvert.ToString(rsEncJournals.Get_Fields_DateTime("PostedDate"));
				}
				else
				{
					aaItem.ActivityDate = FCConvert.ToString(rsEncJournals.Get_Fields_DateTime("EncumbrancesDate"));
				}
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				aaItem.CheckNumber = FCConvert.ToString(rsEncJournals.Get_Fields("CheckNumber"));
				aaItem.VendorNumber = FCConvert.ToInt32(rsEncJournals.Get_Fields_Int32("VendorNumber"));
				// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsEncJournals.Get_Fields("amount")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					//FC:FINAL:MSH - issue #625: can't implicitly convert decimal to double
					aaItem.Debits = rsEncJournals.Get_Fields_Double("amount");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
					aaItem.Credits = rsEncJournals.Get_Fields("amount") * -1;
				}
				if (aaItem.VendorNumber == 0)
				{
					aaItem.VendorName = FCConvert.ToString(rsEncJournals.Get_Fields_String("tempvendorname"));
				}
				else
				{
					vend = vendCont.GetVendorByVendorNumber(aaItem.VendorNumber);
					if (!(vend == null))
					{
						aaItem.VendorName = vend.CheckName;
					}
					else
					{
						aaItem.VendorName = "Unknown";
					}
				}
				// TODO Get_Fields: Check the table for the column [Journalnumber] and replace with corresponding Get_Field method
				aaItem.JournalNumber = FCConvert.ToInt32(rsEncJournals.Get_Fields("Journalnumber"));
				aaItem.ActivityDate = FCConvert.ToString(rsEncJournals.Get_Fields_DateTime("PostedDate"));
				aaItem.Description = FCConvert.ToString(rsEncJournals.Get_Fields_String("Description"));
				lstDetails.AddItem(aaItem);
				rsEncJournals.MoveNext();
			}
			cAcctActItemComparer comparer = new cAcctActItemComparer();
			comparer.FiscalStartPeriod = FCConvert.ToInt16(BegMonth);
			cCollectionHelper collHelp = new cCollectionHelper();
			FCCollection tempColl = new FCCollection();
			tempColl = collHelp.Sort(lstDetails.Items, comparer);
			lstDetails.ClearList();
			lstDetails.Items = tempColl;
			lstDetails.MoveFirst();
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent;
			int LowDateCurrent;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			string strTable = "";
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (Strings.Left(strCurrentAccount, 1) == "E")
			{
				strTable = "ExpenseReportInfo";
			}
			else if (Strings.Left(strCurrentAccount, 1) == "G")
			{
				strTable = "LedgerReportInfo";
			}
			else if (Strings.Left(strCurrentAccount, 1) == "R")
			{
				strTable = "RevenueReportInfo";
			}
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
			rsActivityDetail.OpenRecordset("SELECT Account, Period, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account, Period");
		}

		private void RetrieveSummaryInfo()
		{
			int HighDate = 0;
			int LowDate;
			int HighDateCurrent;
			int LowDateCurrent;
			string strPeriodCheck = "";
			string strPeriodCheckCurrent = "";
			string strTable = "";
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (Strings.Left(strCurrentAccount, 1) == "E")
			{
				strTable = "ExpenseReportInfo";
			}
			else if (Strings.Left(strCurrentAccount, 1) == "G")
			{
				strTable = "LedgerReportInfo";
			}
			else if (Strings.Left(strCurrentAccount, 1) == "R")
			{
				strTable = "RevenueReportInfo";
			}
			rsSummaryActivityDetail.OpenRecordset("SELECT Account, Period, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account, Period");
		}

		public void Init(ref cBDAccountStatusReport curReport)
		{
			theReport = curReport;
			boolShowLiquidatedEncumbranceActivity = curReport.ShowLiquidateEncumbranceActivity;
			currentAccount = (cBDAccount)curReport.Accounts.GetCurrentItem();
			strCurrentAccount = currentAccount.Account;
			BegMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
		}

		
	}
}
