﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptExpObjDetail.
	/// </summary>
	public partial class rptExpObjDetail : BaseSectionReport
	{
		public static rptExpObjDetail InstancePtr
		{
			get
			{
				return (rptExpObjDetail)Sys.GetInstance(typeof(rptExpObjDetail));
			}
		}

		protected rptExpObjDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsExpObjInfo.Dispose();
				rsDetail.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptExpObjDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsExpObjInfo = new clsDRWrapper();
		clsDRWrapper rsDetail = new clsDRWrapper();
		string strSql = "";
		bool blnFirstRecord;
		string strOrderBySQL = "";
		// vbPorter upgrade warning: curObjDebitsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curObjDebitsTotal;
		// vbPorter upgrade warning: curObjCreditsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curObjCreditsTotal;
		// vbPorter upgrade warning: curFinalDebitsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalDebitsTotal;
		// vbPorter upgrade warning: curFinalCreditsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalCreditsTotal;

		public rptExpObjDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Exp / Obj Detail Report";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			bool executeCheckFirstFile = false;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (!modAccountTitle.Statics.ObjFlag)
				{
					rsDetail.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Expense = '" + rsExpObjInfo.Get_Fields_String("Expense") + "' AND Object = '" + rsExpObjInfo.Get_Fields_String("Object") + "'AND Status = 'P' AND (" + strSql + ") " + strOrderBySQL);
				}
				else
				{
					rsDetail.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Expense = '" + rsExpObjInfo.Get_Fields_String("Expense") + "' AND Status = 'P' AND (" + strSql + ") " + strOrderBySQL);
				}
				executeCheckFirstFile = true;
				goto CheckFirstFile;
			}
			else
			{
				rsDetail.MoveNext();
				executeCheckFirstFile = true;
				goto CheckFirstFile;
			}
			CheckFirstFile:
			;
			if (executeCheckFirstFile)
			{
				if (rsDetail.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					rsExpObjInfo.MoveNext();
					if (rsExpObjInfo.EndOfFile() != true)
					{
						if (!modAccountTitle.Statics.ObjFlag)
						{
							rsDetail.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Expense = '" + rsExpObjInfo.Get_Fields_String("Expense") + "' AND Object = '" + rsExpObjInfo.Get_Fields_String("Object") + "'AND Status = 'P' AND (" + strSql + ") " + strOrderBySQL);
						}
						else
						{
							rsDetail.OpenRecordset("SELECT * FROM ExpenseDetailInfo WHERE Expense = '" + rsExpObjInfo.Get_Fields_String("Expense") + "' AND Status = 'P' AND (" + strSql + ") " + strOrderBySQL);
						}
						executeCheckFirstFile = true;
						goto CheckFirstFile;
					}
					else
					{
						eArgs.EOF = true;
					}
				}
			}
			if (!eArgs.EOF)
			{
				if (!modAccountTitle.Statics.ObjFlag)
				{
					this.Fields["Binder"].Value = rsExpObjInfo.Get_Fields_String("Expense") + rsExpObjInfo.Get_Fields_String("Object");
				}
				else
				{
					this.Fields["Binder"].Value = rsExpObjInfo.Get_Fields_String("Expense");
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strPeriodCheck = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 15100 / 1440f;
			blnFirstRecord = true;
			curObjDebitsTotal = 0;
			curObjCreditsTotal = 0;
			curFinalDebitsTotal = 0;
			curFinalCreditsTotal = 0;
			lblAccounts.Text = "Expense: " + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "    ";
			if (!modAccountTitle.Statics.ObjFlag)
			{
				if (frmExpObjDetailSetup.InstancePtr.cmbRangeObject.Text == "All")
				{
					lblAccounts.Text += "Object(s): ALL";
					strSql = "(Expense = '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "')";
				}
				else if (frmExpObjDetailSetup.InstancePtr.cmbRangeObject.Text == "Single Object")
				{
					lblAccounts.Text += "Object(s): " + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboSingleObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2))));
					strSql = "(Expense = '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "' AND Object = '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboSingleObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "')";
				}
				else
				{
					lblAccounts.Text += "Object(s): " + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboBeginningObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + " To " + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboEndingObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2))));
					strSql = "(Expense = '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "' AND (Object >= '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboBeginningObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "' AND Object <= '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboEndingObject.Text, FCConvert.ToInt32(Conversion.Val(Strings.Right(modAccountTitle.Statics.Exp, 2)))) + "'))";
				}
				rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE " + strSql + " AND IsNull(Object, 0) <> 0 ORDER BY Expense, Object");
				GroupFooter1.Visible = true;
			}
			else
			{
				strSql = "(Expense = '" + Strings.Left(frmExpObjDetailSetup.InstancePtr.cboExpense.Text, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)))) + "')";
				rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE " + strSql + " ORDER BY Expense, Object");
				GroupFooter1.Visible = false;
			}
			if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 0)
			{
				lblDates.Text = "Accounting Period(s): ";
			}
			else if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 1)
			{
				lblDates.Text = "Posted Date(s): ";
			}
			else
			{
				lblDates.Text = "Transaction Date(s): ";
			}
			strPeriodCheck = "AND";
			if (frmExpObjDetailSetup.InstancePtr.cmbRange.Text == "All")
			{
				lblDates.Text += "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			else if (frmExpObjDetailSetup.InstancePtr.cmbRange.SelectedIndex == 1)
			{
				if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 0)
				{
					lblDates.Text += frmExpObjDetailSetup.InstancePtr.cboSingleMonth.Text;
					strSql = "Period = " + FCConvert.ToString(frmExpObjDetailSetup.InstancePtr.cboSingleMonth.SelectedIndex + 1);
				}
				else
				{
					lblDates.Text += frmExpObjDetailSetup.InstancePtr.txtSingleDate.Text;
					if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 1)
					{
						strSql = "PostedDate = '" + frmExpObjDetailSetup.InstancePtr.txtSingleDate.Text + "'";
					}
					else
					{
						strSql = "TransDate = '" + frmExpObjDetailSetup.InstancePtr.txtSingleDate.Text + "'";
					}
				}
			}
			else
			{
				if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 0)
				{
					lblDates.Text += frmExpObjDetailSetup.InstancePtr.cboBeginningMonth.Text + " To " + frmExpObjDetailSetup.InstancePtr.cboEndingMonth.Text;
					if (frmExpObjDetailSetup.InstancePtr.cboBeginningMonth.SelectedIndex > frmExpObjDetailSetup.InstancePtr.cboEndingMonth.SelectedIndex)
					{
						strPeriodCheck = "OR";
					}
					strSql = "Period >= " + FCConvert.ToString(frmExpObjDetailSetup.InstancePtr.cboBeginningMonth.SelectedIndex + 1) + " " + strPeriodCheck + " " + " Period <= " + FCConvert.ToString(frmExpObjDetailSetup.InstancePtr.cboEndingMonth.SelectedIndex + 1);
				}
				else
				{
					lblDates.Text += frmExpObjDetailSetup.InstancePtr.txtStartDate.Text + " To " + frmExpObjDetailSetup.InstancePtr.txtEndDate.Text;
					if (DateAndTime.DateValue(frmExpObjDetailSetup.InstancePtr.txtStartDate.Text).ToOADate() > DateAndTime.DateValue(frmExpObjDetailSetup.InstancePtr.txtEndDate.Text).ToOADate())
					{
						strPeriodCheck = "OR";
					}
					if (frmExpObjDetailSetup.InstancePtr.cmbTrans.SelectedIndex == 1)
					{
						strSql = "PostedDate >= '" + frmExpObjDetailSetup.InstancePtr.txtStartDate.Text + "' " + strPeriodCheck + " PostedDate <= '" + frmExpObjDetailSetup.InstancePtr.txtEndDate.Text + "'";
					}
					else
					{
						strSql = "TransDate >= '" + frmExpObjDetailSetup.InstancePtr.txtStartDate.Text + "' " + strPeriodCheck + " TransDate <= '" + frmExpObjDetailSetup.InstancePtr.txtEndDate.Text + "'";
					}
				}
			}
			if (frmExpObjDetailSetup.InstancePtr.cboJournalNumberPriority.SelectedIndex == 0)
			{
				if (frmExpObjDetailSetup.InstancePtr.cboPeriodPriority.SelectedIndex == 1)
				{
					strOrderBySQL = "ORDER BY JournalNumber, Period, Department, Division";
				}
				else
				{
					strOrderBySQL = "ORDER BY JournalNumber, Department, Division, Period";
				}
			}
			else if (frmExpObjDetailSetup.InstancePtr.cboPeriodPriority.SelectedIndex == 0)
			{
				if (frmExpObjDetailSetup.InstancePtr.cboJournalNumberPriority.SelectedIndex == 1)
				{
					strOrderBySQL = "ORDER BY Period, JournalNumber, Department, Division";
				}
				else
				{
					strOrderBySQL = "ORDER BY Period, Department, Division, JournalNumber";
				}
			}
			else
			{
				if (frmExpObjDetailSetup.InstancePtr.cboJournalNumberPriority.SelectedIndex == 1)
				{
					strOrderBySQL = "ORDER BY Department, Division, JournalNumber, Period";
				}
				else
				{
					strOrderBySQL = "ORDER BY Department, Division, Period, JournalNumber";
				}
			}
			// troges16
			modBudgetaryAccounting.CalculateAccountInfo();
			// CalculateAccountInfo False, True, False, "E"
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsVendorInfo = new clsDRWrapper();
			fldPostedDate.Text = Strings.Format(rsDetail.Get_Fields_DateTime("PostedDate"), "MM/dd/yy");
			fldTransDate.Text = Strings.Format(rsDetail.Get_Fields_DateTime("TransDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPeriod.Text = Strings.Format(rsDetail.Get_Fields("Period"), "00");
			fldRCB.Text = rsDetail.Get_Fields_String("RCB");
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			fldType.Text = FCConvert.ToString(rsDetail.Get_Fields("Type"));
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = Strings.Format(rsDetail.Get_Fields("JournalNumber"), "0000");
			// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
			if (Conversion.Val(rsDetail.Get_Fields("Warrant")) != 0)
			{
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				fldWarrant.Text = Strings.Format(rsDetail.Get_Fields("Warrant"), "0000");
			}
			else
			{
				fldWarrant.Text = "";
			}
			// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
			if (Conversion.Val(rsDetail.Get_Fields("CheckNumber")) != 0)
			{
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheck.Text = FCConvert.ToString(rsDetail.Get_Fields("CheckNumber"));
			}
			else
			{
				fldCheck.Text = "";
			}
			if (Conversion.Val(rsDetail.Get_Fields_Int32("VendorNumber")) != 0)
			{
				rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rsDetail.Get_Fields_Int32("VendorNumber"));
				if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
				{
					fldVendor.Text = Strings.Format(rsDetail.Get_Fields_Int32("VendorNumber"), "00000") + " - " + rsVendorInfo.Get_Fields_String("CheckName");
				}
				else
				{
					fldVendor.Text = Strings.Format(rsDetail.Get_Fields_Int32("VendorNumber"), "00000") + " - UNKNOWN";
				}
			}
			else
			{
				fldVendor.Text = "";
			}
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = rsDetail.Get_Fields_String("Account");
			if (FigureDebitOrCredit() == "D")
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				fldDebits.Text = Strings.Format(rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
				fldCredits.Text = "";
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curObjDebitsTotal += (rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curFinalDebitsTotal += (rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance"));
			}
			else
			{
				fldDebits.Text = "";
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				fldCredits.Text = Strings.Format((rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1, "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curObjCreditsTotal += ((rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1);
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curFinalCreditsTotal += ((rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) * -1);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldObjDebitTotal.Text = Strings.Format(curObjDebitsTotal, "#,##0.00");
			fldObjCreditTotal.Text = Strings.Format(curObjCreditsTotal, "#,##0.00");
			curObjDebitsTotal = 0;
			curObjCreditsTotal = 0;
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (!modAccountTitle.Statics.ObjFlag)
			{
				lblExpObj.Text = rsExpObjInfo.Get_Fields_String("Expense") + "-" + rsExpObjInfo.Get_Fields_String("Object") + "   " + rsExpObjInfo.Get_Fields_String("LongDescription");
			}
			else
			{
				lblExpObj.Text = rsExpObjInfo.Get_Fields_String("Expense") + "   " + rsExpObjInfo.Get_Fields_String("LongDescription");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldFinalDebitTotal.Text = Strings.Format(curFinalDebitsTotal, "#,##0.00");
			fldFinalCreditTotal.Text = Strings.Format(curFinalCreditsTotal, "#,##0.00");
		}

		private string FigureDebitOrCredit()
		{
			string FigureDebitOrCredit = "";
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsDetail.Get_Fields("Type")) == "A")
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && (rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) > 0)
				{
					FigureDebitOrCredit = "D";
				}
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && (rsDetail.Get_Fields("Amount") - rsDetail.Get_Fields("Discount") - rsDetail.Get_Fields_Decimal("Encumbrance")) < 0)
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (rsDetail.Get_Fields_Decimal("Encumbrance") != 0 && rsDetail.Get_Fields_Decimal("Encumbrance") > rsDetail.Get_Fields("Amount"))
					{
						FigureDebitOrCredit = "D";
					}
					else
					{
						FigureDebitOrCredit = "C";
					}
				}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) > 0)
				{
					FigureDebitOrCredit = "D";
				}
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				else if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "C" && FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) != "E" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) < 0)
				{
					FigureDebitOrCredit = "C";
				}
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					else if (FCConvert.ToString(rsDetail.Get_Fields_String("RCB")) == "C" && FCConvert.ToInt32(rsDetail.Get_Fields("Amount")) > 0)
				{
					FigureDebitOrCredit = "C";
				}
				else
				{
					FigureDebitOrCredit = "D";
				}
			}
			return FigureDebitOrCredit;
		}

		private void rptExpObjDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptExpObjDetail.Caption	= "Exp / Obj Detail Report";
			//rptExpObjDetail.Icon	= "rptExpObjDetail.dsx":0000";
			//rptExpObjDetail.Left	= 0;
			//rptExpObjDetail.Top	= 0;
			//rptExpObjDetail.Width	= 11880;
			//rptExpObjDetail.Height	= 8595;
			//rptExpObjDetail.StartUpPosition	= 3;
			//rptExpObjDetail.SectionData	= "rptExpObjDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
