﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cTrialBalanceView
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController bactController = new cBDAccountController();
		private cBDAccountController bactController_AutoInitialized;

		private cBDAccountController bactController
		{
			get
			{
				if (bactController_AutoInitialized == null)
				{
					bactController_AutoInitialized = new cBDAccountController();
				}
				return bactController_AutoInitialized;
			}
			set
			{
				bactController_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection listFunds = new cGenericCollection();
		private cGenericCollection listFunds_AutoInitialized;

		private cGenericCollection listFunds
		{
			get
			{
				if (listFunds_AutoInitialized == null)
				{
					listFunds_AutoInitialized = new cGenericCollection();
				}
				return listFunds_AutoInitialized;
			}
			set
			{
				listFunds_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDReportService bdrs = new cBDReportService();
		private cBDReportService bdrs_AutoInitialized;

		private cBDReportService bdrs
		{
			get
			{
				if (bdrs_AutoInitialized == null)
				{
					bdrs_AutoInitialized = new cBDReportService();
				}
				return bdrs_AutoInitialized;
			}
			set
			{
				bdrs_AutoInitialized = value;
			}
		}

		private bool boolShowAllFunds;
		private bool boolShowRangeOfFunds;
		private bool boolShowAllPeriods;
		private bool boolShowRangeofPeriods;
		private bool boolShowZeroAccounts;
		private int intFirstPeriod;
		private int intLastPeriod;
		private int intStartPeriod;
		private int intEndPeriod;
		private string strStartFund = "";
		private string strEndFund = "";
		private cFund currentStartFund;
		private cFund currentEndFund;

		public bool ShowRangeOfFunds
		{
			set
			{
				boolShowRangeOfFunds = value;
				if (value)
				{
					boolShowAllFunds = false;
				}
			}
			get
			{
				bool ShowRangeOfFunds = false;
				ShowRangeOfFunds = boolShowRangeOfFunds;
				return ShowRangeOfFunds;
			}
		}

		public bool ShowAllPeriods
		{
			set
			{
				boolShowAllPeriods = value;
				if (value)
				{
					boolShowRangeofPeriods = false;
				}
			}
			get
			{
				bool ShowAllPeriods = false;
				ShowAllPeriods = boolShowAllPeriods;
				return ShowAllPeriods;
			}
		}

		public bool ShowRangeOfPeriods
		{
			set
			{
				boolShowRangeofPeriods = value;
				if (value)
				{
					boolShowAllPeriods = false;
				}
			}
			get
			{
				bool ShowRangeOfPeriods = false;
				ShowRangeOfPeriods = boolShowRangeofPeriods;
				return ShowRangeOfPeriods;
			}
		}

		public bool ShowZeroAccounts
		{
			set
			{
				boolShowZeroAccounts = value;
			}
			get
			{
				bool ShowZeroAccounts = false;
				ShowZeroAccounts = boolShowZeroAccounts;
				return ShowZeroAccounts;
			}
		}

		public bool ShowAllFunds
		{
			set
			{
				boolShowAllFunds = value;
				if (value)
				{
					boolShowRangeOfFunds = false;
				}
			}
			get
			{
				bool ShowAllFunds = false;
				ShowAllFunds = boolShowAllFunds;
				return ShowAllFunds;
			}
		}

		public cGenericCollection Funds
		{
			get
			{
				cGenericCollection Funds = null;
				Funds = listFunds;
				return Funds;
			}
		}

		public short StartPeriod
		{
			set
			{
				intStartPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short StartPeriod = 0;
				StartPeriod = FCConvert.ToInt16(intStartPeriod);
				return StartPeriod;
			}
		}

		public short EndPeriod
		{
			set
			{
				intEndPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EndPeriod = 0;
				EndPeriod = FCConvert.ToInt16(intEndPeriod);
				return EndPeriod;
			}
		}

		public cFund StartFund
		{
			get
			{
				cFund StartFund = null;
				StartFund = currentStartFund;
				return StartFund;
			}
		}

		public void SetCurrentStartFund(int lngId)
		{
			listFunds.MoveFirst();
			cFund tempFund;
			while (listFunds.IsCurrent())
			{
				tempFund = (cFund)listFunds.GetCurrentItem();
				if (tempFund.ID == lngId)
				{
					currentStartFund = tempFund;
					break;
				}
				listFunds.MoveNext();
			}
		}

		public cFund EndFund
		{
			get
			{
				cFund EndFund = null;
				EndFund = currentEndFund;
				return EndFund;
			}
		}

		public void SetCurrentEndFund(int lngId)
		{
			listFunds.MoveFirst();
			cFund tempFund;
			while (listFunds.IsCurrent())
			{
				tempFund = (cFund)listFunds.GetCurrentItem();
				if (tempFund.ID == lngId)
				{
					currentEndFund = tempFund;
					break;
				}
				listFunds.MoveNext();
			}
		}

		public cTrialBalanceView() : base()
		{
			listFunds = bactController.GetFunds();
			listFunds.MoveFirst();
			if (listFunds.IsCurrent())
			{
				currentStartFund = (cFund)listFunds.GetCurrentItem();
				listFunds.MoveLast();
				currentEndFund = (cFund)listFunds.GetCurrentItem();
			}
			boolShowAllFunds = true;
			boolShowRangeOfFunds = false;
			boolShowAllPeriods = true;
			boolShowRangeofPeriods = false;
			boolShowZeroAccounts = false;
			intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			if (intFirstPeriod == 1)
			{
				intLastPeriod = 12;
			}
			else
			{
				intLastPeriod = intFirstPeriod - 1;
			}
			intStartPeriod = intFirstPeriod;
			intEndPeriod = intLastPeriod;
		}

		public void PrintReport()
		{
			cTrialBalanceReport tbr = new cTrialBalanceReport();
			if (boolShowAllPeriods)
			{
				tbr.StartPeriod = 0;
				tbr.EndPeriod = 0;
			}
			else
			{
				tbr.StartPeriod = FCConvert.ToInt16(intStartPeriod);
				tbr.EndPeriod = FCConvert.ToInt16(intEndPeriod);
			}
			tbr.IncludeAccountsWithNoActivity = boolShowZeroAccounts;
			if (boolShowAllFunds)
			{
				tbr.BeginningFund = "";
				tbr.EndingFund = "";
			}
			else
			{
				if (!(currentStartFund == null))
				{
					tbr.BeginningFund = currentStartFund.Fund;
				}
				if (!(currentEndFund == null))
				{
					tbr.EndingFund = currentEndFund.Fund;
				}
			}
			bdrs.ShowTrialBalanceReport(ref tbr);
			if (bdrs.HadError)
			{
				MessageBox.Show(FCConvert.ToString(bdrs.LastErrorNumber) + "  " + bdrs.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else if (tbr.Funds.ItemCount() < 1)
			{
				MessageBox.Show("No items found matching this criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void ExportReport()
		{
			cTrialBalanceReport tbr = new cTrialBalanceReport();
			if (boolShowAllPeriods)
			{
				tbr.StartPeriod = 0;
				tbr.EndPeriod = 0;
			}
			else
			{
				tbr.StartPeriod = FCConvert.ToInt16(intStartPeriod);
				tbr.EndPeriod = FCConvert.ToInt16(intEndPeriod);
			}
			tbr.IncludeAccountsWithNoActivity = boolShowZeroAccounts;
			if (boolShowAllFunds)
			{
				tbr.BeginningFund = "";
				tbr.EndingFund = "";
			}
			else
			{
				if (!(currentStartFund == null))
				{
					tbr.BeginningFund = currentStartFund.Fund;
				}
				if (!(currentEndFund == null))
				{
					tbr.EndingFund = currentEndFund.Fund;
				}
			}
			bdrs.ExportTrialBalanceCSV(ref tbr);
			if (bdrs.HadError)
			{
				MessageBox.Show(FCConvert.ToString(bdrs.LastErrorNumber) + "  " + bdrs.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else if (tbr.Funds.ItemCount() < 1)
			{
				MessageBox.Show("No items found matching this criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
