﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptVendorDetail.
	/// </summary>
	partial class rptVendorDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptVendorDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInvoice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccountRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblInvoice = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblVendorCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldVendorCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.VendorBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblVendor = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDebits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCredits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.srptOutstandingEncumbrances = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInvoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendorCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.VendorBinder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDate,
				this.fldAccount,
				this.fldDescription,
				this.fldRCB,
				this.fldType,
				this.fldCheck,
				this.fldAmount,
				this.fldWarrant,
				this.fldInvoice
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0.125F;
			this.fldPeriod.MultiLine = false;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldPeriod.Text = "Field36";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.8125F;
			this.fldJournal.MultiLine = false;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldJournal.Text = "Field36";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.34375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 1.1875F;
			this.fldDate.MultiLine = false;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldDate.Text = "Field36";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.59375F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 1.84375F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldAccount.Text = "P 001-0001-0001-0001-01";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.65625F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 4.46875F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldDescription.Text = "Field36";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.25F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.1875F;
			this.fldRCB.Left = 5.75F;
			this.fldRCB.MultiLine = false;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldRCB.Text = "Field36";
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.3125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 6.09375F;
			this.fldType.MultiLine = false;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldType.Text = "Field36";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 6.4375F;
			this.fldCheck.MultiLine = false;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCheck.Text = "10928709";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.625F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 7.0625F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldAmount.Text = "99,999,999.00";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 0.78125F;
			// 
			// fldWarrant
			// 
			this.fldWarrant.Height = 0.1875F;
			this.fldWarrant.Left = 0.4375F;
			this.fldWarrant.MultiLine = false;
			this.fldWarrant.Name = "fldWarrant";
			this.fldWarrant.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldWarrant.Text = "Field36";
			this.fldWarrant.Top = 0F;
			this.fldWarrant.Width = 0.34375F;
			// 
			// fldInvoice
			// 
			this.fldInvoice.Height = 0.1875F;
			this.fldInvoice.Left = 3.53125F;
			this.fldInvoice.MultiLine = false;
			this.fldInvoice.Name = "fldInvoice";
			this.fldInvoice.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; white-space: nowrap; ddo" + "-char-set: 1";
			this.fldInvoice.Text = null;
			this.fldInvoice.Top = 0F;
			this.fldInvoice.Width = 0.90625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblDateRange,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Field13,
				this.Line1,
				this.Field26,
				this.lblPeriod,
				this.lblAccount,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field33,
				this.lblAmount,
				this.lblAccountRange,
				this.Field35,
				this.lblInvoice
			});
			this.PageHeader.Height = 0.8958333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.78125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Vendor Detail  Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.5F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.78125F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label6";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 4.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0.8125F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field13.Tag = " ";
			this.Field13.Text = "Jrnl";
			this.Field13.Top = 0.6875F;
			this.Field13.Width = 0.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 7.90625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.90625F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 1.1875F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field26.Tag = " ";
			this.Field26.Text = "Date";
			this.Field26.Top = 0.6875F;
			this.Field26.Width = 0.59375F;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Height = 0.1875F;
			this.lblPeriod.Left = 0.125F;
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblPeriod.Tag = " ";
			this.lblPeriod.Text = "Per";
			this.lblPeriod.Top = 0.6875F;
			this.lblPeriod.Width = 0.25F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.Left = 1.84375F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Tag = " ";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.6875F;
			this.lblAccount.Width = 1.59375F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 4.46875F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Description";
			this.Field29.Top = 0.6875F;
			this.Field29.Width = 1.25F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 6.09375F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field30.Tag = " ";
			this.Field30.Text = "Type";
			this.Field30.Top = 0.6875F;
			this.Field30.Width = 0.34375F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 5.75F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field31.Tag = " ";
			this.Field31.Text = "RCB";
			this.Field31.Top = 0.6875F;
			this.Field31.Width = 0.3125F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 6.4375F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field33.Tag = " ";
			this.Field33.Text = "Check";
			this.Field33.Top = 0.6875F;
			this.Field33.Width = 0.625F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.Left = 6.8125F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblAmount.Tag = " ";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.6875F;
			this.lblAmount.Width = 1.03125F;
			// 
			// lblAccountRange
			// 
			this.lblAccountRange.Height = 0.1875F;
			this.lblAccountRange.HyperLink = null;
			this.lblAccountRange.Left = 1.78125F;
			this.lblAccountRange.Name = "lblAccountRange";
			this.lblAccountRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblAccountRange.Text = "Label6";
			this.lblAccountRange.Top = 0.40625F;
			this.lblAccountRange.Width = 4.5F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 0.40625F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field35.Tag = " ";
			this.Field35.Text = "Wrnt";
			this.Field35.Top = 0.6875F;
			this.Field35.Width = 0.34375F;
			// 
			// lblInvoice
			// 
			this.lblInvoice.Height = 0.1875F;
			this.lblInvoice.Left = 3.53125F;
			this.lblInvoice.Name = "lblInvoice";
			this.lblInvoice.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblInvoice.Tag = " ";
			this.lblInvoice.Text = "Invoice";
			this.lblInvoice.Top = 0.6875F;
			this.lblInvoice.Width = 0.90625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label9,
				this.fldGrandTotal,
				this.Line4,
				this.lblVendorCount,
				this.fldVendorCount
			});
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 5.84375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.Label9.Text = "Final Total";
			this.Label9.Top = 0.0625F;
			this.Label9.Width = 0.8125F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 6.8125F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGrandTotal.Text = "Field37";
			this.fldGrandTotal.Top = 0.0625F;
			this.fldGrandTotal.Width = 1.03125F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 6.28125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.0625F;
			this.Line4.Width = 1.5625F;
			this.Line4.X1 = 6.28125F;
			this.Line4.X2 = 7.84375F;
			this.Line4.Y1 = 0.0625F;
			this.Line4.Y2 = 0.0625F;
			// 
			// lblVendorCount
			// 
			this.lblVendorCount.Height = 0.1875F;
			this.lblVendorCount.HyperLink = null;
			this.lblVendorCount.Left = 2.28125F;
			this.lblVendorCount.Name = "lblVendorCount";
			this.lblVendorCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblVendorCount.Text = "Vendors Listed ";
			this.lblVendorCount.Top = 0.0625F;
			this.lblVendorCount.Width = 1F;
			// 
			// fldVendorCount
			// 
			this.fldVendorCount.Height = 0.1875F;
			this.fldVendorCount.Left = 3.3125F;
			this.fldVendorCount.Name = "fldVendorCount";
			this.fldVendorCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldVendorCount.Text = "Field37";
			this.fldVendorCount.Top = 0.0625F;
			this.fldVendorCount.Width = 0.625F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.VendorBinder,
				this.lblVendor
			});
			this.GroupHeader1.DataField = "VendorBinder";
			this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.FirstDetail;
			this.GroupHeader1.Height = 0.1979167F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// VendorBinder
			// 
			this.VendorBinder.DataField = "VendorBinder";
			this.VendorBinder.Height = 0.125F;
			this.VendorBinder.Left = 5.78125F;
			this.VendorBinder.Name = "VendorBinder";
			this.VendorBinder.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.VendorBinder.Text = "Field37";
			this.VendorBinder.Top = 0.03125F;
			this.VendorBinder.Visible = false;
			this.VendorBinder.Width = 0.875F;
			// 
			// lblVendor
			// 
			this.lblVendor.DataField = "GroupTitle";
			this.lblVendor.Height = 0.1875F;
			this.lblVendor.Left = 0F;
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.lblVendor.Text = null;
			this.lblVendor.Top = 0F;
			this.lblVendor.Width = 7.21875F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.fldTotal,
				this.lblDebits,
				this.fldDebits,
				this.lblCredits,
				this.fldCredits,
				this.Line3,
				this.srptOutstandingEncumbrances
			});
			this.GroupFooter1.Height = 0.6770833F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 6.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label8.Text = "AP Total";
			this.Label8.Top = 0F;
			this.Label8.Width = 0.53125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.8125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field37";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.03125F;
			// 
			// lblDebits
			// 
			this.lblDebits.Height = 0.1875F;
			this.lblDebits.HyperLink = null;
			this.lblDebits.Left = 6.25F;
			this.lblDebits.Name = "lblDebits";
			this.lblDebits.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.lblDebits.Text = "Debits";
			this.lblDebits.Top = 0.28125F;
			this.lblDebits.Width = 0.53125F;
			// 
			// fldDebits
			// 
			this.fldDebits.CanShrink = true;
			this.fldDebits.Height = 0.1875F;
			this.fldDebits.Left = 6.8125F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = "Field37";
			this.fldDebits.Top = 0.28125F;
			this.fldDebits.Width = 1.03125F;
			// 
			// lblCredits
			// 
			this.lblCredits.Height = 0.1875F;
			this.lblCredits.HyperLink = null;
			this.lblCredits.Left = 6.25F;
			this.lblCredits.Name = "lblCredits";
			this.lblCredits.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.lblCredits.Text = "Credits";
			this.lblCredits.Top = 0.46875F;
			this.lblCredits.Width = 0.53125F;
			// 
			// fldCredits
			// 
			this.fldCredits.CanShrink = true;
			this.fldCredits.Height = 0.1875F;
			this.fldCredits.Left = 6.8125F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = "Field37";
			this.fldCredits.Top = 0.46875F;
			this.fldCredits.Width = 1.03125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 6.28125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 1.5625F;
			this.Line3.X1 = 6.28125F;
			this.Line3.X2 = 7.84375F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// srptOutstandingEncumbrances
			// 
			this.srptOutstandingEncumbrances.CloseBorder = false;
			this.srptOutstandingEncumbrances.Height = 0.09375F;
			this.srptOutstandingEncumbrances.Left = 0F;
			this.srptOutstandingEncumbrances.Name = "srptOutstandingEncumbrances";
			this.srptOutstandingEncumbrances.Report = null;
			this.srptOutstandingEncumbrances.Top = 0.1875F;
			this.srptOutstandingEncumbrances.Width = 7.96875F;
			// 
			// rptVendorDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptVendorDetail_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInvoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInvoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendorCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldVendorCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.VendorBinder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInvoice;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountRange;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblInvoice;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblVendorCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorCount;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox VendorBinder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblVendor;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptOutstandingEncumbrances;
	}
}
