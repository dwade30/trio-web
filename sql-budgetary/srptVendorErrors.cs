﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Budgetary.Models;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptVendorErrors.
	/// </summary>
	public partial class srptVendorErrors : FCSectionReport
	{
		public static srptVendorErrors InstancePtr
		{
			get
			{
				return (srptVendorErrors)Sys.GetInstance(typeof(srptVendorErrors));
			}
		}

		protected srptVendorErrors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptVendorErrors	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private List<VendorErrorReportData> reportData; 
		bool blnFirstRecord;
		private int dataCounter = 0;

		public srptVendorErrors(List<VendorErrorReportData> reportData)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();

			this.reportData = reportData;
			blnFirstRecord = true;
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				dataCounter++;
				eArgs.EOF = dataCounter >= reportData.Count;
			}
		}

		
		private void Detail_Format(object sender, EventArgs e)
		{
			
			fldVendor.Text = Strings.Format(reportData[dataCounter].VendorNumber, "00000") + "  " + reportData[dataCounter].VendorName;
			if (reportData[dataCounter].TaxNumber.Trim() == "" && (reportData[dataCounter].CheckCity.Trim() == "" || reportData[dataCounter].CheckState.Trim() == "" || reportData[dataCounter].CheckZip.Trim() == ""))
			{
				fldProblem1.Text = "Fix City / State / Zip Code";
				fldProblem2.Text = "Missing Tax ID Number";
			}
			else if (reportData[dataCounter].TaxNumber.Trim() == "")
			{
				fldProblem2.Text = "";
				fldProblem1.Text = "Missing Tax ID Number";
			}
			else
			{
				fldProblem1.Text = "Fix City / State / Zip Code";
				fldProblem2.Text = "";
			}
		}

		private void srptVendorErrors_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
