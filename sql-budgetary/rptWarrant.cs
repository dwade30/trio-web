﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrant.
	/// </summary>
	public partial class rptWarrant : BaseSectionReport
	{
		public static rptWarrant InstancePtr
		{
			get
			{
				return (rptWarrant)Sys.GetInstance(typeof(rptWarrant));
			}
		}

		protected rptWarrant _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsDetailInfo.Dispose();
				rsVendorInfo.Dispose();
				rsJournalInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// Obj = 1
		//   0	rptWarrant	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		// vbPorter upgrade warning: curJournalTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalTotal;
		// vbPorter upgrade warning: curJournalEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curJournalEncTotal;
		// vbPorter upgrade warning: curVendorTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorTotal;
		// vbPorter upgrade warning: curVendorEncTotal As Decimal	OnWrite(Decimal, short)
		Decimal curVendorEncTotal;
		Decimal curWarrantTotal;
		Decimal curWarrantEncTotal;
		Decimal curPrepaidTotal;
		Decimal curEFTTotal;
		Decimal curCurrentTotal;
		bool blnFirstRecord;
		bool blnTempVendors;
		//clsDRWrapper rsTempVendorInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		int intTempVendorCounter;
		string strJournalSQL;
		string strSQL = "";

		public rptWarrant()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "AP Warrant";
		}
		// n
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
					{
						intTempVendorCounter = 0;
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY CheckNumber, CreditMemoRecord");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
					else
					{
						rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY CheckNumber, CreditMemoRecord");
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
					}
					strJournalSQL = rsJournalInfo.Name();
				}
				else
				{
					rsDetailInfo.MoveNext();
					if (rsDetailInfo.EndOfFile() == true)
					{
						rsJournalInfo.MoveNext();
						bool executeGetNextDetail = false;
						bool executeCheckNextJournal = false;
						bool executeCheckNextVendor = false;
						if (rsJournalInfo.EndOfFile() == true)
						{
							rsVendorInfo.MoveNext();
							executeCheckNextVendor = true;
							goto CheckNextVendor;
						}
						else
						{
							executeGetNextDetail = true;
							goto GetNextDetail;
						}
						GetNextDetail:
						;
						if (executeGetNextDetail)
						{
							rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
							if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
							{
								this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
								this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
								eArgs.EOF = false;
								return;
							}
							else
							{
								rsJournalInfo.MoveNext();
								executeCheckNextJournal = true;
								goto CheckNextJournal;
							}
							executeGetNextDetail = false;
						}
						CheckNextJournal:
						;
						if (executeCheckNextJournal)
						{
							if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
							{
								executeGetNextDetail = true;
								goto GetNextDetail;
							}
							else
							{
								rsVendorInfo.MoveNext();
								goto CheckNextVendor;
							}
							executeCheckNextJournal = false;
						}
						CheckNextVendor:
						;
						if (executeCheckNextVendor)
						{
							if (rsVendorInfo.EndOfFile() == true)
							{
								eArgs.EOF = true;
								return;
							}
							else
							{
								GetFirstVendor:
								;
								if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "' ORDER BY CheckNumber, CreditMemoRecord");
								}
								else
								{
									rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber") + " ORDER BY CheckNumber, CreditMemoRecord");
								}
								executeCheckNextJournal = true;
								goto CheckNextJournal;
							}
							executeCheckNextVendor = false;
						}
					}
					else
					{
						this.Fields["VendorBinder"].Value = rsVendorInfo.Get_Fields_Int32("VendorNumber") + rsVendorInfo.Get_Fields_String("VendorName");
						this.Fields["JournalBinder"].Value = rsJournalInfo.Get_Fields_Int32("ID");
						eArgs.EOF = false;
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_ActiveReport_FetchData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                //FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
                iLabel1:
                if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
                {
                    iLabel2:
                    rsInfo.OpenRecordset("SELECT * FROM Reprint WHERE Type = 'W' ORDER BY ReportOrder");
                    iLabel3:
                    if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                    {
                        iLabel4:
                        do
                        {
                            iLabel5:
                            if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ReportOrder")) < 9)
                            {
                                iLabel6:
                                rsInfo.Edit();
                                iLabel7:
                                rsInfo.Set_Fields("ReportOrder",
                                    FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportOrder")) + 1);
                                iLabel8:
                                rsInfo.Update();
                                iLabel9:
                                rsInfo.MoveNext();
                            }
                            else
                            {
                                iLabel10:
                                rsInfo.Delete();
                                rsInfo.Update();
                                iLabel11: ;
                            }

                            iLabel12: ;
                            iLabel13: ;
                        } while (rsInfo.EndOfFile() != true);

                        iLabel14: ;
                    }

                    iLabel15: ;
                    iLabel16:
                    rsInfo.AddNew();
                    iLabel17:
                    rsInfo.Set_Fields("Type", "W");
                    iLabel18:
                    rsInfo.Set_Fields("ReportOrder", 1);
                    // TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    iLabel19:
                    rsInfo.Set_Fields("WarrantNumber", frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant"));
                    iLabel20:
                    rsInfo.Update(true);
                    // TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    iLabel21:
                    rsMasterJournal.OpenRecordset(
                        "SELECT * FROM JournalMaster WHERE Status = 'R' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" +
                        frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant") + "')");
                    iLabel22:
                    if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
                    {
                        iLabel23:
                        do
                        {
                            frmWarrant.InstancePtr.clsJournalInfo = new clsPostingJournalInfo();
                            // TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            frmWarrant.InstancePtr.clsJournalInfo.JournalNumber =
                                FCConvert.ToInt32(rsMasterJournal.Get_Fields("JournalNumber"));
                            // TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
                            frmWarrant.InstancePtr.clsJournalInfo.JournalType =
                                FCConvert.ToString(rsMasterJournal.Get_Fields("Type"));
                            // TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
                            frmWarrant.InstancePtr.clsJournalInfo.Period =
                                FCConvert.ToString(rsMasterJournal.Get_Fields("Period"));
                            frmWarrant.InstancePtr.clsJournalInfo.CheckDate =
                                FCConvert.ToString(rsMasterJournal.Get_Fields_DateTime("CheckDate"));
                            frmWarrant.InstancePtr.clsPostInfo.AddJournal(frmWarrant.InstancePtr.clsJournalInfo);
                            iLabel24:
                            rsMasterJournal.Edit();
                            iLabel25:
                            rsMasterJournal.Set_Fields("Status", "W");
                            iLabel26:
                            rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                            iLabel27:
                            rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
                            iLabel28:
                            rsMasterJournal.Update(true);
                            iLabel29:
                            rsMasterJournal.MoveNext();
                            iLabel30: ;
                        } while (rsMasterJournal.EndOfFile() != true);

                        iLabel31: ;
                    }

                    iLabel32: ;
                }

                //return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In rptWarrant_ActiveReport_ReportEnd", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsMasterJournal.Dispose();
				rsInfo.Dispose();
            }
            //FC:FINAL:SBE - #844 - if only one warrant record was printed, then we have to close also the form.
            if (frmWarrant.InstancePtr.rsInfo.RecordCount() <= 1)
            {
                //FC:FINAL:MSH - Issue #721: 'Close()' replaced by 'Hide()', so we need to close 'frmWarrant' to display it correctly next time.
                frmWarrant.InstancePtr.Close();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				PageCounter = 0;
				Label2.Text = modGlobalConstants.Statics.MuniName;
				Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
				//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
				if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
				{
					// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
					Label6.Text = "Warrant " + frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant");
					// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
					strSQL = "APJournal.Returned <> 'R' AND APJournal.Returned <> 'D' AND APJournal.Warrant = '" + frmWarrant.InstancePtr.rsInfo.Get_Fields("Warrant") + "' ";
					Label21.Visible = false;
				}
				else
				{
					Label6.Text = "Warrant " + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrant.InstancePtr.cboReport.Text, 9, 4)));
					strSQL = "APJournal.Returned <> 'R' AND APJournal.Returned <> 'D' AND APJournal.Warrant = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrant.InstancePtr.cboReport.Text, 9, 4))) + "' ";
					Label21.Visible = true;
				}
				blnFirstRecord = true;
				blnTempVendors = false;
				// Me.Document.Printer.PrinterName = frmWarrant.strPrinterName
				FillTemp();
				if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Cancel();
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_ActiveReport_ReportStart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptAccountsPayable))
			{
				if (!frmWarrant.InstancePtr.blnAsked)
				{
					if (MessageBox.Show("Would you like to post the journal(s)?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						frmWarrant.InstancePtr.blnPostJournal = true;
					}
					else
					{
						frmWarrant.InstancePtr.blnPostJournal = false;
					}
					frmWarrant.InstancePtr.blnAsked = true;
				}
			}
		}

		public void Init()
		{
			//FC:FINAL:BBE:#i679 - show report viewer Modeless
			//frmReportViewer.InstancePtr.Init(this, frmWarrant.InstancePtr.strPrinterName, FCConvert.ToInt32(FormShowEnum.Modal, false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true, true);
			frmReportViewer.InstancePtr.Init(this, frmWarrant.InstancePtr.strPrinterName, FCConvert.ToInt16(FCForm.FormShowEnum.Modeless), false, false, "Pages", false, string.Empty, "TRIO Software", false, true, string.Empty, true, true);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fldDetailDescription.Text = rsDetailInfo.Get_Fields_String("Description");
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = rsDetailInfo.Get_Fields_String("Account");
				fldProject.Text = rsDetailInfo.Get_Fields_String("Project");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				fldAmount.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
				fldEncumbrance.Text = Strings.Format(rsDetailInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
				ShowTitle();
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curJournalTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				curJournalEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curVendorTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				curVendorEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curWarrantTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				curWarrantEncTotal += rsDetailInfo.Get_Fields_Decimal("Encumbrance");
				if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("PrepaidCheck")))
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curPrepaidTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				}
				else if (rsJournalInfo.Get_Fields_Boolean("EFTCheck"))
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curEFTTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curCurrentTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_Detail_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				fldVendorAmountTotal.Text = Strings.Format(curVendorTotal, "#,##0.00");
				iLabel2:
				fldVendorEncumbranceTotal.Text = "";
				// format(curVendorEncTotal, "#,##0.00")
				iLabel3:
				curVendorEncTotal = 0;
				iLabel4:
				curVendorTotal = 0;
				iLabel5:
				if (rsVendorInfo.EndOfFile())
				{
					iLabel6:
					Line2.Visible = false;
					iLabel7:
					;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_GroupFooter2_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter3_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (ShowJournalTotal())
				{
					Line3.Visible = true;
					fldJournalTitle.Visible = true;
					fldJournalAmountTotal.Text = Strings.Format(curJournalTotal, "#,##0.00");
					fldJournalEncumbranceTotal.Text = "";
					// format(curJournalEncTotal, "#,##0.00")
				}
				else
				{
					Line3.Visible = false;
					fldJournalTitle.Visible = false;
					fldJournalAmountTotal.Text = "";
					fldJournalEncumbranceTotal.Text = "";
				}
				curJournalEncTotal = 0;
				curJournalTotal = 0;
				strJournalSQL = rsJournalInfo.Name();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_GroupFooter3_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			//clsDRWrapper rsVendorName = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsVendorInfo.Get_Fields_String("VendorName");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_GroupHeader2_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupHeader3_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				fldJournal.Text = modValidateAccount.GetFormat_6(rsJournalInfo.Get_Fields("JournalNumber"), 4);
				fldDescription.Text = rsJournalInfo.Get_Fields_String("Description");
				fldReference.Text = rsJournalInfo.Get_Fields_String("Reference");
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				fldCheckNumber.Text = FCConvert.ToString(rsJournalInfo.Get_Fields("CheckNumber"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldMonth.Text = Strings.Format(rsJournalInfo.Get_Fields("Period"), "00");
				if (rsJournalInfo.Get_Fields_Boolean("Seperate") == true)
				{
					fldSeperate.Visible = true;
				}
				else
				{
					fldSeperate.Visible = false;
				}
				//FC:FINAL:MSH - Issue #720: values reordered, so indexes changed
				if (frmWarrant.InstancePtr.cmbReprint.SelectedIndex == 0)
				{
					rsJournalInfo.Edit();
					if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
					{
						rsJournalInfo.Set_Fields("Status", "W");
					}
					rsJournalInfo.Update(true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_PageHeader3_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				PageCounter += 1;
				iLabel2:
				Label4.Text = "Page " + FCConvert.ToString(PageCounter);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_PageHeader_Format", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				this.Fields.Add("VendorBinder");
				iLabel2:
				this.Fields.Add("JournalBinder");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_DataInitialize", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowTitle()
		{
			int intHolder = 0;
			try
			{
				// On Error GoTo ErrorHandler
				// If Left(rsDetailInfo.Fields["Account"], 1) = "E" Or Left(rsDetailInfo.Fields["Account"], 1) = "R" Or Left(rsDetailInfo.Fields["Account"], 1) = "G" Then
				if (frmWarrant.InstancePtr.intPreviewChoice == 1)
				{
					fldTitle.Text = "";
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modAccountTitle.DetermineAccountTitle(rsDetailInfo.Get_Fields("Account"), ref frmWarrant.InstancePtr.lblTitle);
				}
				if (frmWarrant.InstancePtr.intPreviewChoice == 2)
				{
					fldTitle.Text = frmWarrant.InstancePtr.lblTitle.Text;
				}
				else if (frmWarrant.InstancePtr.intPreviewChoice == 3)
				{
					;
				}
				else if (frmWarrant.InstancePtr.intPreviewChoice == 3)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Left(frmWarrant.InstancePtr.lblTitle.Text, intHolder - 1);
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrant.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrant.InstancePtr.lblTitle.Text, frmWarrant.InstancePtr.lblTitle.Text.Length - intHolder - 1);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_ShowTitle", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsSignatureInfo = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fldPrepaidAmountTotal.Text = Strings.Format(curPrepaidTotal, "#,##0.00");
                fldCurrentAmountTotal.Text = Strings.Format(curCurrentTotal, "#,##0.00");
                fldEFTAmountTotal.Text = Strings.Format(curEFTTotal, "#,##0.00");
                fldWarrantAmountTotal.Text = Strings.Format(curWarrantTotal, "#,##0.00");
                fldWarrantEncumbranceTotal.Text = "";
                // format(curWarrantEncTotal, "#,##0.00")
                rsSignatureInfo.OpenRecordset("SELECT * FROM WarrantMessage");
                if (rsSignatureInfo.EndOfFile() != true && rsSignatureInfo.BeginningOfFile() != true)
                {
                    fldWarrantSignature1.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 1, 80);
                    fldWarrantSignature2.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 81, 80);
                    fldWarrantSignature3.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 161, 80);
                    fldWarrantSignature4.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 241, 80);
                    fldWarrantSignature5.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 321, 80);
                    fldWarrantSignature6.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 401, 80);
                    fldWarrantSignature7.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 481, 80);
                    fldWarrantSignature8.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 561, 80);
                    fldWarrantSignature9.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 641, 80);
                    fldWarrantSignature10.Text =
                        Strings.Mid(FCConvert.ToString(rsSignatureInfo.Get_Fields_String("Message")), 721, 80);
                    if (Strings.Trim(fldWarrantSignature1.Text) == "")
                    {
                        fldWarrantSignature1.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature2.Text) == "")
                    {
                        fldWarrantSignature2.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature3.Text) == "")
                    {
                        fldWarrantSignature3.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature4.Text) == "")
                    {
                        fldWarrantSignature4.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature5.Text) == "")
                    {
                        fldWarrantSignature5.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature6.Text) == "")
                    {
                        fldWarrantSignature6.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature7.Text) == "")
                    {
                        fldWarrantSignature7.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature8.Text) == "")
                    {
                        fldWarrantSignature8.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature9.Text) == "")
                    {
                        fldWarrantSignature9.Visible = false;
                    }

                    if (Strings.Trim(fldWarrantSignature10.Text) == "")
                    {
                        fldWarrantSignature10.Visible = false;
                    }
                }
                else
                {
                    fldWarrantSignature1.Visible = false;
                    fldWarrantSignature2.Visible = false;
                    fldWarrantSignature3.Visible = false;
                    fldWarrantSignature4.Visible = false;
                    fldWarrantSignature5.Visible = false;
                    fldWarrantSignature6.Visible = false;
                    fldWarrantSignature7.Visible = false;
                    fldWarrantSignature8.Visible = false;
                    fldWarrantSignature9.Visible = false;
                    fldWarrantSignature10.Visible = false;
                }

                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In rptWarrant_ReportFooter_Format", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsSignatureInfo.Dispose();
            }
		}

		private bool ShowJournalTotal()
		{
			bool ShowJournalTotal = false;
			clsDRWrapper rsCheck = new clsDRWrapper();
			int intCheck;
            try
            {
                // On Error GoTo ErrorHandler
                intCheck = Strings.InStr(1, strJournalSQL, "WHERE", CompareConstants.vbBinaryCompare);
                rsCheck.OpenRecordset("SELECT DISTINCT ID FROM APJournal " +
                                      Strings.Mid(strJournalSQL, intCheck, strJournalSQL.Length - (intCheck + 38)));
                if (rsCheck.RecordCount() > 1)
                {
                    ShowJournalTotal = true;
                }
                else
                {
                    ShowJournalTotal = false;
                }

                return ShowJournalTotal;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In rptWarrant_ShowJournalTotal", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsCheck.Dispose();
            }
			return ShowJournalTotal;
		}

		private void FillTemp()
		{
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
				// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
				strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries' ORDER BY TempVendorName";
				// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
				{
					if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
					{
						strTemp += " ORDER BY VendorNumber";
					}
					else
					{
						strTemp += " ORDER BY VendorName";
					}
				}
				else
				{
					strTemp += " ORDER BY VendorNumber";
				}
				rsVendorInfo.OpenRecordset(strTemp);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_FillTemp", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		

		private void rptWarrant_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
	}
}
