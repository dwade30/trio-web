﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCheckBalance.
	/// </summary>
	partial class frmCheckBalance : BaseForm
	{
		public fecherFoundation.FCButton cmdBalance;
		public fecherFoundation.FCFrame fraNewBalance;
		public fecherFoundation.FCTextBox txtNewBalance;
		public fecherFoundation.FCButton cmdChange;
		public fecherFoundation.FCButton cmdCancel2;
		public FCGrid vs1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblStatementDate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckBalance));
			this.cmdBalance = new fecherFoundation.FCButton();
			this.fraNewBalance = new fecherFoundation.FCFrame();
			this.txtNewBalance = new fecherFoundation.FCTextBox();
			this.cmdChange = new fecherFoundation.FCButton();
			this.cmdCancel2 = new fecherFoundation.FCButton();
			this.vs1 = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblStatementDate = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraNewBalance)).BeginInit();
			this.fraNewBalance.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdChange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdBalance);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraNewBalance);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblStatementDate);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "Balance Screen";
			// 
			// cmdBalance
			// 
			this.cmdBalance.AppearanceKey = "acceptButton";
			this.cmdBalance.Location = new System.Drawing.Point(421, 30);
			this.cmdBalance.Name = "cmdBalance";
			this.cmdBalance.Size = new System.Drawing.Size(259, 48);
			this.cmdBalance.TabIndex = 4;
			this.cmdBalance.Text = "Change Beginning Balance";
			this.cmdBalance.Click += new System.EventHandler(this.cmdBalance_Click);
			// 
			// fraNewBalance
			// 
			this.fraNewBalance.Anchor = Wisej.Web.AnchorStyles.None;
			this.fraNewBalance.BackColor = System.Drawing.Color.FromName("@window");
			this.fraNewBalance.Controls.Add(this.txtNewBalance);
			this.fraNewBalance.Controls.Add(this.cmdChange);
			this.fraNewBalance.Controls.Add(this.cmdCancel2);
			this.fraNewBalance.Location = new System.Drawing.Point(412, 195);
			this.fraNewBalance.Name = "fraNewBalance";
			this.fraNewBalance.Size = new System.Drawing.Size(220, 155);
			this.fraNewBalance.Text = "Enter New Balance";
			this.fraNewBalance.Visible = false;
			// 
			// txtNewBalance
			// 
			this.txtNewBalance.BackColor = System.Drawing.SystemColors.Window;
			this.txtNewBalance.Location = new System.Drawing.Point(20, 30);
			this.txtNewBalance.Name = "txtNewBalance";
			this.txtNewBalance.Size = new System.Drawing.Size(180, 40);
			this.txtNewBalance.TabIndex = 9;
			this.txtNewBalance.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewBalance_KeyPress);
			// 
			// cmdChange
			// 
			this.cmdChange.AppearanceKey = "actionButton";
			this.cmdChange.Location = new System.Drawing.Point(20, 90);
			this.cmdChange.Name = "cmdChange";
			this.cmdChange.Size = new System.Drawing.Size(80, 40);
			this.cmdChange.TabIndex = 1;
			this.cmdChange.Text = "Change";
			this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
			// 
			// cmdCancel2
			// 
			this.cmdCancel2.AppearanceKey = "actionButton";
			this.cmdCancel2.Location = new System.Drawing.Point(120, 90);
			this.cmdCancel2.Name = "cmdCancel2";
			this.cmdCancel2.Size = new System.Drawing.Size(80, 40);
			this.cmdCancel2.TabIndex = 2;
			this.cmdCancel2.Text = "Cancel";
			this.cmdCancel2.Click += new System.EventHandler(this.cmdCancel2_Click);
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 3;
			this.vs1.ColumnHeadersVisible = false;
			this.vs1.ExtendLastCol = true;
			this.vs1.FixedCols = 0;
			this.vs1.FixedRows = 0;
			this.vs1.Location = new System.Drawing.Point(30, 66);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 18;
			this.vs1.Size = new System.Drawing.Size(1018, 420);
			this.vs1.TabIndex = 8;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(110, 16);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "STATEMENT DATE";
			// 
			// lblStatementDate
			// 
			this.lblStatementDate.Location = new System.Drawing.Point(175, 30);
			this.lblStatementDate.Name = "lblStatementDate";
			this.lblStatementDate.Size = new System.Drawing.Size(124, 14);
			this.lblStatementDate.TabIndex = 5;
			// 
			// frmCheckBalance
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCheckBalance";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Balance Screen";
			this.Load += new System.EventHandler(this.frmCheckBalance_Load);
			this.Activated += new System.EventHandler(this.frmCheckBalance_Activated);
			this.Resize += new System.EventHandler(this.frmCheckBalance_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckBalance_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraNewBalance)).EndInit();
			this.fraNewBalance.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdChange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion
    }
}
