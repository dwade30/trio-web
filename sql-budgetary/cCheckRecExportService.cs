﻿//Fecher vbPorter - Version 1.0.0.70
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
//using System.Windows.Forms;
using System.IO;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;

namespace TWBD0000
{
    public class cCheckRecExportService
    {

        //=========================================================
        string strLastError = "";
        int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }

        // vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
        public bool HadError
        {
            get
            {
                
                return lngLastError != 0;
                
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        private void SetError(int lngErrorNumber, string strErrorMessage)
        {
            lngLastError = lngErrorNumber;
            strLastError = strErrorMessage;
        }
        public void ExportBoothbayCheckRec(ref cCheckRecOption checkOpt)
        {
            ClearErrors();
            if (!(checkOpt == null))
            {
                if (ValidateDateRange(checkOpt.StartDate, checkOpt.EndDate))
                {
                    CreateBoothbayExport(ref checkOpt);
                }
                else
                {
                    SetError(9999, "Invalid date range");
                }
            }
            else
            {
                SetError(9999, "Null options object");
            }
        }
        public void ExportSimpleCheckRec( cCheckRecOption checkOpt)
        {
            ClearErrors();
            if (!(checkOpt == null))
            {

                if (ValidateDateRange(checkOpt.StartDate, checkOpt.EndDate))
                {
                    CreateSimpleCheckRec(checkOpt);
                }
                else
                {
                    SetError(9999, "Invalid date range");
                }
            }
            else
            {
                SetError(9999, "Null options object");
            }
        }
        private bool ValidateDateRange(string strStartDate, string strEndDate)
        {
            bool ValidateDateRange = false;
            bool boolReturn;
            boolReturn = false;
            if (Information.IsDate(strStartDate))
            {
                if (Information.IsDate(strEndDate))
                {
                    if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Parse(strStartDate), DateTime.Parse(strEndDate)) >= 0)
                    {
                        boolReturn = true;
                    }
                }
            }
            ValidateDateRange = boolReturn;
            return ValidateDateRange;
        }
        private void CreateSimpleCheckRec( cCheckRecOption checkOpt)
        {
            try
            {   // On Error GoTo ErrorHandler               
                var strFilePath = FCFileSystem.Statics.UserDataFolder;
                var strFileName = checkOpt.FileName;
                if (File.Exists(Path.Combine(strFilePath, strFileName)))
                {
                    File.Delete(Path.Combine(strFilePath, strFileName));
                }
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strOutput = "";
                rsLoad.OpenRecordset("SELECT * FROM CheckRecMaster WHERE (CheckDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(checkOpt.StartDate)) + "' AND CheckDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(checkOpt.EndDate)) + "') AND (Type = '1' or Type = '2') AND Status <> 'V' AND CheckNumber <> 0", "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    using (var streamWriter = File.CreateText(Path.Combine(strFilePath, strFileName)))
                    {
                        strOutput = "Payee,Date,Check number,Check amount";
                        streamWriter.WriteLine(strOutput);
                        while (!rsLoad.EndOfFile())
                        {
                            //Application.DoEvents();
                            strOutput = FCConvert.ToString(Convert.ToChar(34)) + fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("Name"))) + FCConvert.ToString(Convert.ToChar(34)) + "," + Strings.Format(rsLoad.Get_Fields("Checkdate"), "MM/dd/yyyy") + "," + FCConvert.ToString(Conversion.Val(FCConvert.ToString(rsLoad.Get_Fields("Checknumber")))) + "," + rsLoad.Get_Fields_Decimal("amount").FormatAsMoney();
                            streamWriter.WriteLine(strOutput);
                            rsLoad.MoveNext();
                        }
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    FCUtils.DownloadAndOpen( Path.Combine(strFilePath, strFileName),strFileName);
                }
                else
                {
                    SetError(10000, "There is no check rec information to export");
                }
                return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
        }

        private void CreateBoothbayExport(ref cCheckRecOption checkOpt)
        {
            ClearErrors();
            try
            {   // On Error GoTo ErrorHandler
                var strFilePath = FCFileSystem.Statics.UserDataFolder;
                var strFileName = checkOpt.FileName;
                string strOutput = "";
                clsDRWrapper rs = new clsDRWrapper();
               
                rs.OpenRecordset("SELECT * FROM CheckRecMaster WHERE (CheckDate >= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(checkOpt.StartDate)) + "' AND CheckDate <= '" + FCConvert.ToString(fecherFoundation.DateAndTime.DateValue(checkOpt.EndDate)) + "') AND (Type = '1' or Type = '2') AND Status <> 'V' AND CheckNumber <> 0", "Budgetary");
                if (!rs.EndOfFile())
                {
                    using (var streamWriter = File.CreateText(Path.Combine(strFilePath, strFileName)))
                    {
                        while (!rs.EndOfFile())
                        {
                            //Application.DoEvents();
                            strOutput = Strings.StrDup(11 - fecherFoundation.Strings.Trim((rs.Get_Fields("Amount") * 100).ToString()).Length, "0") + FCConvert.ToString(rs.Get_Fields("Amount") * 100);
                            strOutput += Strings.Format(rs.Get_Fields("CheckDate"), "mmddyy");
                            if (FCConvert.ToString(rs.Get_Fields("Name")).Length >= 40)
                            {
                                strOutput += Strings.Left(FCConvert.ToString(rs.Get_Fields("Name")), 40);
                            }
                            else
                            {
                                strOutput += Strings.StrDup(40 - fecherFoundation.Strings.Trim(FCConvert.ToString(rs.Get_Fields("Name"))).Length, " ") + rs.Get_Fields("Name");
                            }
                            strOutput += Strings.StrDup(10 - fecherFoundation.Strings.Trim(rs.Get_Fields("CheckNumber").ToString()).Length, "0") + rs.Get_Fields("CheckNumber");

                            streamWriter.WriteLine(strOutput);
                            rs.MoveNext();
                        }
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    //FC:FINAL:BSE:#4339 should download, not open file
                    FCUtils.Download( Path.Combine(strFilePath, strFileName),strFileName);
                }
                else
                {
                    SetError(10000, "There is no check rec information to export");
                    return;
                }

                return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                SetError(fecherFoundation.Information.Err(ex).Number, fecherFoundation.Information.Err(ex).Description);
            }
        }


    }
}