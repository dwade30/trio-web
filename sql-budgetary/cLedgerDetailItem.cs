﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cLedgerDetailItem
	{
		//=========================================================
		private string strAccount = string.Empty;
		private string strFund = string.Empty;
		// Private strDepartment As String
		// Private strDivision As String
		private string strSuffix = string.Empty;
		private string strAccountObject = string.Empty;
		private string strDescription = string.Empty;
		private string strJournalDate = string.Empty;
		private string strJournalType = string.Empty;
		private int intJournalNumber;
		private string strVendor = string.Empty;
		private int lngWarrantNumber;
		private int lngCheckNumber;
		private double dblCredit;
		private double dblDebit;
		private string strWarrant = string.Empty;
		private int intPeriod;
		private string strRCB = string.Empty;
		private string strTransactionDate = string.Empty;
		private double dblBalanceCredit;
		private double dblBalanceDebit;
		private double dblNetBegin;
		private double dblNetEnd;

		public double NetBalance
		{
			set
			{
				dblNetEnd = value;
			}
			get
			{
				double NetBalance = 0;
				NetBalance = dblNetEnd;
				return NetBalance;
			}
		}

		public double BeginningNet
		{
			set
			{
				dblNetBegin = value;
			}
			get
			{
				double BeginningNet = 0;
				BeginningNet = dblNetBegin;
				return BeginningNet;
			}
		}

		public string Suffix
		{
			set
			{
				strSuffix = value;
			}
			get
			{
				string Suffix = "";
				Suffix = strSuffix;
				return Suffix;
			}
		}

		public double BalanceCredit
		{
			set
			{
				dblBalanceCredit = value;
			}
			get
			{
				double BalanceCredit = 0;
				BalanceCredit = dblBalanceCredit;
				return BalanceCredit;
			}
		}

		public double BalanceDebit
		{
			set
			{
				dblBalanceDebit = value;
			}
			get
			{
				double BalanceDebit = 0;
				BalanceDebit = dblBalanceDebit;
				return BalanceDebit;
			}
		}

		public string TransactionDate
		{
			set
			{
				strTransactionDate = value;
			}
			get
			{
				string TransactionDate = "";
				TransactionDate = strTransactionDate;
				return TransactionDate;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}

		public string Warrant
		{
			set
			{
				strWarrant = value;
			}
			get
			{
				string Warrant = "";
				Warrant = strWarrant;
				return Warrant;
			}
		}

		public double Credit
		{
			set
			{
				dblCredit = value;
			}
			get
			{
				double Credit = 0;
				Credit = dblCredit;
				return Credit;
			}
		}

		public double Debit
		{
			set
			{
				dblDebit = value;
			}
			get
			{
				double Debit = 0;
				Debit = dblDebit;
				return Debit;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string JournalDate
		{
			set
			{
				strJournalDate = value;
			}
			get
			{
				string JournalDate = "";
				JournalDate = strJournalDate;
				return JournalDate;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public short JournalNumber
		{
			set
			{
				intJournalNumber = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short JournalNumber = 0;
				JournalNumber = FCConvert.ToInt16(intJournalNumber);
				return JournalNumber;
			}
		}

		public string Vendor
		{
			set
			{
				strVendor = value;
			}
			get
			{
				string Vendor = "";
				Vendor = strVendor;
				return Vendor;
			}
		}

		public int CheckNumber
		{
			set
			{
				lngCheckNumber = value;
			}
			get
			{
				int CheckNumber = 0;
				CheckNumber = lngCheckNumber;
				return CheckNumber;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string AccountObject
		{
			set
			{
				strAccountObject = value;
			}
			get
			{
				string AccountObject = "";
				AccountObject = strAccountObject;
				return AccountObject;
			}
		}
	}
}
