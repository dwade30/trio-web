﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWBD0000
{
	public class cLedgerDetailController : cReportDataExporter
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		private int intFormatType;
		// 0 = CSV, 1 = Tab
		private bool boolUseSuffix;

		public cLedgerDetailController() : base()
		{
			boolUseSuffix = !modAccountTitle.Statics.YearFlag;
		}

		public short FormatType
		{
			set
			{
				intFormatType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FormatType = 0;
				FormatType = FCConvert.ToInt16(intFormatType);
				return FormatType;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}
		// vbPorter upgrade warning: theReport As cDetailsReport	OnRead(cLedgerDetailReport)
		public void ExportData(ref cDetailsReport theReport, string strFileName)
		{
			StreamWriter ts = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				string strPath;
				string strSeparator = "";
				string strQ = "";
				// vbPorter upgrade warning: glReport As cLedgerDetailReport	OnWrite(cDetailsReport)
				cLedgerDetailReport glReport;
				glReport = (cLedgerDetailReport)theReport;
				strPath = Path.GetDirectoryName(strFileName);
				if (!Directory.Exists(strPath))
				{
					Information.Err().Raise(9999, null, "Path not found", null, null);
				}
				if (intFormatType == 1)
				{
					strSeparator = "\t";
					strQ = "";
				}
				else
				{
					strSeparator = ",";
					strQ = FCConvert.ToString(Convert.ToChar(34));
				}
				cLedgerDetailItem glItem;
				string strLine;
				ts = new StreamWriter(strFileName);
				strLine = strQ + "Account" + strQ;
				strLine += strSeparator + strQ + "Fund" + strQ;
				strLine += strSeparator + strQ + "Acct" + strQ;
				if (boolUseSuffix)
				{
					strLine += strSeparator + strQ + "Suffix" + strQ;
				}
				strLine += strSeparator + strQ + "Date Posted" + strQ;
				strLine += strSeparator + strQ + "Transaction Date" + strQ;
				strLine += strSeparator + strQ + "Journal Number" + strQ;
				strLine += strSeparator + strQ + "Description" + strQ;
				if (glReport.ShowJournalDetail)
				{
					strLine += strSeparator + strQ + "Journal Type" + strQ;
					strLine += strSeparator + strQ + "Period" + strQ;
					strLine += strSeparator + strQ + "RCB" + strQ;
					strLine += strSeparator + strQ + "Warrant" + strQ;
					strLine += strSeparator + strQ + "Check" + strQ;
					strLine += strSeparator + strQ + "Vendor" + strQ;
				}
				strLine += strSeparator + strQ + "Beginning Balance" + strQ;
				strLine += strSeparator + strQ + "Debits" + strQ;
				strLine += strSeparator + strQ + "Credits" + strQ;
				strLine += strSeparator + strQ + "Balance Debit" + strQ;
				strLine += strSeparator + strQ + "Balance Credit" + strQ;
				strLine += strSeparator + strQ + "Net Balance" + strQ;
				ts.WriteLine(strLine);
				theReport.Details.MoveFirst();
				while (theReport.Details.IsCurrent())
				{
					strLine = "";
					glItem = (cLedgerDetailItem)theReport.Details.GetCurrentItem();
					strLine = strQ + glItem.Account + strQ;
					strLine += strSeparator + strQ + glItem.Fund + strQ;
					strLine += strSeparator + strQ + glItem.AccountObject + strQ;
					if (boolUseSuffix)
					{
						strLine += strSeparator + strQ + glItem.Suffix + strQ;
					}
					strLine += strSeparator + glItem.JournalDate;
					strLine += strSeparator + glItem.TransactionDate;
					strLine += strSeparator + FCConvert.ToString(glItem.JournalNumber);
					strLine += strSeparator + strQ + glItem.Description + strQ;
					if (glReport.ShowJournalDetail)
					{
						strLine += strSeparator + strQ + glItem.JournalType + strQ;
						strLine += strSeparator + FCConvert.ToString(glItem.Period);
						strLine += strSeparator + strQ + glItem.RCB + strQ;
						strLine += strSeparator + glItem.Warrant;
						strLine += strSeparator + FCConvert.ToString(glItem.CheckNumber);
						strLine += strSeparator + strQ + glItem.Vendor + strQ;
					}
					strLine += strSeparator + FCConvert.ToString(glItem.BeginningNet);
					strLine += strSeparator + FCConvert.ToString(glItem.Debit);
					strLine += strSeparator + FCConvert.ToString(glItem.Credit);
					strLine += strSeparator + FCConvert.ToString(glItem.BalanceDebit);
					strLine += strSeparator + FCConvert.ToString(glItem.BalanceCredit);
					strLine += strSeparator + FCConvert.ToString(glItem.NetBalance);
					ts.WriteLine(strLine);
					theReport.Details.MoveNext();
				}
				ts.Close();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (!(ts == null))
				{
					ts.Close();
				}
			}
		}
	}
}
