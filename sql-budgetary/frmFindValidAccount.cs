﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmFindValidAccount.
	/// </summary>
	public partial class frmFindValidAccount : BaseForm
	{
		public frmFindValidAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFindValidAccount InstancePtr
		{
			get
			{
				return (frmFindValidAccount)Sys.GetInstance(typeof(frmFindValidAccount));
			}
		}

		protected frmFindValidAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     David Wade
		// Date           1/6/04
		// This form will be a global form that may be used
		// by towns to search for valid accounts
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rsDeptDiv = new clsDRWrapper();
		clsDRWrapper rsExpObj = new clsDRWrapper();
		clsDRWrapper rsRev = new clsDRWrapper();
		clsDRWrapper rsLedger = new clsDRWrapper();
		string strTypesToShow = "";

		private void frmFindValidAccount_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs2.ColWidth(0, FCConvert.ToInt32(vs2.WidthOriginal * 0.2128851));
			vs2.ColWidth(2, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(1, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			// set column widths for the valid accounts flexgrid
			vs2.ColWidth(3, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(4, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(5, 0);
			vs2.TextMatrix(0, 0, "Account");
			vs2.TextMatrix(0, 1, "Dept / Fund");
			vs2.TextMatrix(0, 2, "Div / Acct");
			// set valid accounts flexgrid column headings
			vs2.TextMatrix(0, 3, "Rev / Exp");
			vs2.TextMatrix(0, 4, "Obj");
			vs2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, 4);
			vs2.Row = 1;
			vs2.Focus();
			this.Refresh();
		}

		private void frmFindValidAccount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFindValidAccount.FillStyle	= 0;
			//frmFindValidAccount.ScaleWidth	= 9045;
			//frmFindValidAccount.ScaleHeight	= 7350;
			//frmFindValidAccount.LinkTopic	= "Form2";
			//frmFindValidAccount.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			rsDeptDiv.OpenRecordset("SELECT * FROM DeptDivTitles");
			rsExpObj.OpenRecordset("SELECT * FROM ExpObjTitles");
			rsLedger.OpenRecordset("SELECT * FROM LedgerTitles");
			rsRev.OpenRecordset("SELECT * FROM RevTitles");
			GetData();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmFindValidAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				modBudgetaryMaster.Statics.strReturnedValidAccount = "";
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmFindValidAccount_Resize(object sender, System.EventArgs e)
		{
			vs2.ColWidth(0, FCConvert.ToInt32(vs2.WidthOriginal * 0.2128851));
			vs2.ColWidth(2, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(1, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			// set column widths for the valid accounts flexgrid
			vs2.ColWidth(3, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(4, FCConvert.ToInt32(vs2.WidthOriginal * 0.1778151));
			vs2.ColWidth(5, 0);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			modBudgetaryMaster.Statics.strReturnedValidAccount = "";
			Close();
		}

		private void vs2_DblClick(object sender, System.EventArgs e)
		{
			modBudgetaryMaster.Statics.strReturnedValidAccount = vs2.TextMatrix(vs2.Row, 0);
			Close();
		}

		private void vs2_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int counter;
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs2.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs2.Row == vs2.Rows - 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.E)
			{
				KeyCode = 0;
				vs2.Row = 1;
				vs2.TopRow = 1;
			}
			else if (KeyCode == Keys.G)
			{
				KeyCode = 0;
				for (counter = 1; counter <= vs2.Rows - 1; counter++)
				{
					if (Strings.Left(vs2.TextMatrix(counter, 0), 1) == "G")
					{
						vs2.Row = counter;
						vs2.TopRow = counter;
						break;
					}
				}
			}
			else if (KeyCode == Keys.R)
			{
				KeyCode = 0;
				for (counter = 1; counter <= vs2.Rows - 1; counter++)
				{
					if (Strings.Left(vs2.TextMatrix(counter, 0), 1) == "R")
					{
						vs2.Row = counter;
						vs2.TopRow = counter;
						break;
					}
				}
			}
		}

		private void vs2_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			string strLabel = "";
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				modBudgetaryMaster.Statics.strReturnedValidAccount = vs2.TextMatrix(vs2.Row, 0);
				Close();
			}
		}

		private void GetData()
		{
			int counter;
			string temp = "";
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building Valid Accounts List";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			this.Refresh();
			if (strTypesToShow == "A")
			{
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM AccountMaster WHERE left(Account, 1) = '" + strTypesToShow + "' AND Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs2.Rows = rs.RecordCount() + 1;
				if (vs2.Rows < 25)
				{
					vs2.Height = vs2.RowHeight(0) * vs2.Rows + 75;
				}
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					//Application.DoEvents();
					if (FCConvert.ToString(rs.Get_Fields_String("FirstAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("SecondAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("ThirdAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("FourthAccountField")) != "")
					{
						temp = rs.Get_Fields_String("AccountType") + " " + rs.Get_Fields_String("FirstAccountField") + "-" + rs.Get_Fields_String("SecondAccountField") + "-" + rs.Get_Fields_String("ThirdAccountField") + "-" + rs.Get_Fields_String("FourthAccountField");
					}
					else if (FCConvert.ToString(rs.Get_Fields_String("FirstAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("SecondAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("ThirdAccountField")) != "")
					{
						temp = rs.Get_Fields_String("AccountType") + " " + rs.Get_Fields_String("FirstAccountField") + "-" + rs.Get_Fields_String("SecondAccountField") + "-" + rs.Get_Fields_String("ThirdAccountField");
					}
					else if (FCConvert.ToString(rs.Get_Fields_String("FirstAccountField")) != "" && FCConvert.ToString(rs.Get_Fields_String("SecondAccountField")) != "")
					{
						temp = rs.Get_Fields_String("AccountType") + " " + rs.Get_Fields_String("FirstAccountField") + "-" + rs.Get_Fields_String("SecondAccountField");
					}
					if (FCConvert.ToString(rs.Get_Fields_String("AccountType")) == "E")
					{
						GetExp(temp);
					}
					else if (rs.Get_Fields_String("AccountType") == "R")
					{
						GetRev(temp);
					}
					else
					{
						GetLedger(temp);
					}
					vs2.TextMatrix(counter, 0, temp);
					if (vs2.Row < vs2.Rows - 1)
					{
						vs2.Row += 1;
					}
					rs.MoveNext();
				}
			}
			frmWait.InstancePtr.Unload();
		}

		private void GetExp(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Dept;
			string Div = "";
			string tempDiv;
			string tempObj;
			string Expense = "";
			string Object = "";
			string tempAccount;
			tempDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			tempObj = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 1));
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1));
				Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
				if (!modAccountTitle.Statics.ObjFlag)
				{
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
					Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
				}
			}
			else
			{
				Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
				if (!modAccountTitle.Statics.ObjFlag)
				{
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
					Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
				}
			}
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs2.TextMatrix(vs2.Row, 1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + Div, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vs2.TextMatrix(vs2.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + Object, ",");
								if (rsExpObj.NoMatch != true)
								{
									vs2.TextMatrix(vs2.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
									return;
								}
								else
								{
									vs2.TextMatrix(vs2.Row, 4, "Undefined");
									vs2.TextMatrix(vs2.Row, 3, "Undefined");
									vs2.TextMatrix(vs2.Row, 2, "Undefined");
									vs2.TextMatrix(vs2.Row, 1, "Undefined");
									return;
								}
							}
							else
							{
								vs2.TextMatrix(vs2.Row, 4, "Undefined");
								vs2.TextMatrix(vs2.Row, 3, "Undefined");
								vs2.TextMatrix(vs2.Row, 2, "Undefined");
								vs2.TextMatrix(vs2.Row, 1, "Undefined");
								return;
							}
						}
						else
						{
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vs2.TextMatrix(vs2.Row, 4, "Undefined");
								vs2.TextMatrix(vs2.Row, 3, "Undefined");
								vs2.TextMatrix(vs2.Row, 2, "Undefined");
								vs2.TextMatrix(vs2.Row, 1, "Undefined");
								return;
							}
						}
					}
					else
					{
						vs2.TextMatrix(vs2.Row, 4, "Undefined");
						vs2.TextMatrix(vs2.Row, 3, "Undefined");
						vs2.TextMatrix(vs2.Row, 2, "Undefined");
						vs2.TextMatrix(vs2.Row, 1, "Undefined");
						return;
					}
				}
				else
				{
					vs2.TextMatrix(vs2.Row, 4, "Undefined");
					vs2.TextMatrix(vs2.Row, 3, "Undefined");
					vs2.TextMatrix(vs2.Row, 2, "Undefined");
					vs2.TextMatrix(vs2.Row, 1, "Undefined");
					return;
				}
			}
			else
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs2.TextMatrix(vs2.Row, 1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					if (!modAccountTitle.Statics.ObjFlag)
					{
						rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
						if (rsExpObj.NoMatch != true)
						{
							vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
							rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + Object, ",");
							if (rsExpObj.NoMatch != true)
							{
								vs2.TextMatrix(vs2.Row, 4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vs2.TextMatrix(vs2.Row, 4, "Undefined");
								vs2.TextMatrix(vs2.Row, 3, "Undefined");
								vs2.TextMatrix(vs2.Row, 2, "Undefined");
								vs2.TextMatrix(vs2.Row, 1, "Undefined");
								return;
							}
						}
						else
						{
							vs2.TextMatrix(vs2.Row, 4, "Undefined");
							vs2.TextMatrix(vs2.Row, 3, "Undefined");
							vs2.TextMatrix(vs2.Row, 2, "Undefined");
							vs2.TextMatrix(vs2.Row, 1, "Undefined");
							return;
						}
					}
					else
					{
						rsExpObj.FindFirstRecord2("Expense, Object", Expense + "," + tempObj, ",");
						if (rsExpObj.NoMatch != true)
						{
							vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vs2.TextMatrix(vs2.Row, 4, "Undefined");
							vs2.TextMatrix(vs2.Row, 3, "Undefined");
							vs2.TextMatrix(vs2.Row, 2, "Undefined");
							vs2.TextMatrix(vs2.Row, 1, "Undefined");
							return;
						}
					}
				}
				else
				{
					vs2.TextMatrix(vs2.Row, 4, "Undefined");
					vs2.TextMatrix(vs2.Row, 3, "Undefined");
					vs2.TextMatrix(vs2.Row, 2, "Undefined");
					vs2.TextMatrix(vs2.Row, 1, "Undefined");
					return;
				}
			}
		}

		private void GetRev(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Dept;
			string Div = "";
			string tempExpDiv;
			string Revenue = "";
			string tempAccount;
			tempExpDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)) + 1));
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 1));
				Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
			}
			else
			{
				Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
			}
			if (!modAccountTitle.Statics.RevDivFlag)
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempExpDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs2.TextMatrix(vs2.Row, 1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + Div, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vs2.TextMatrix(vs2.Row, 2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						rsRev.FindFirstRecord2("Department, Division, Revenue", Dept + "," + Div + "," + Revenue, ",");
						if (rsRev.NoMatch != true)
						{
							vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vs2.TextMatrix(vs2.Row, 4, "Undefined");
							vs2.TextMatrix(vs2.Row, 3, "Undefined");
							vs2.TextMatrix(vs2.Row, 2, "Undefined");
							vs2.TextMatrix(vs2.Row, 1, "Undefined");
							return;
						}
					}
					else
					{
						vs2.TextMatrix(vs2.Row, 4, "Undefined");
						vs2.TextMatrix(vs2.Row, 3, "Undefined");
						vs2.TextMatrix(vs2.Row, 2, "Undefined");
						vs2.TextMatrix(vs2.Row, 1, "Undefined");
						return;
					}
				}
				else
				{
					vs2.TextMatrix(vs2.Row, 4, "Undefined");
					vs2.TextMatrix(vs2.Row, 3, "Undefined");
					vs2.TextMatrix(vs2.Row, 2, "Undefined");
					vs2.TextMatrix(vs2.Row, 1, "Undefined");
					return;
				}
			}
			else
			{
				rsDeptDiv.FindFirstRecord2("Department, Division", Dept + "," + tempExpDiv, ",");
				if (rsDeptDiv.NoMatch != true)
				{
					vs2.TextMatrix(vs2.Row, 1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
					rsRev.FindFirstRecord2("Department, Revenue", Dept + "," + Revenue, ",");
					if (rsRev.NoMatch != true)
					{
						vs2.TextMatrix(vs2.Row, 3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs2.TextMatrix(vs2.Row, 4, "Undefined");
						vs2.TextMatrix(vs2.Row, 3, "Undefined");
						vs2.TextMatrix(vs2.Row, 2, "Undefined");
						vs2.TextMatrix(vs2.Row, 1, "Undefined");
						return;
					}
				}
				else
				{
					vs2.TextMatrix(vs2.Row, 4, "Undefined");
					vs2.TextMatrix(vs2.Row, 3, "Undefined");
					vs2.TextMatrix(vs2.Row, 2, "Undefined");
					vs2.TextMatrix(vs2.Row, 1, "Undefined");
					return;
				}
			}
		}

		private void GetLedger(string x)
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			string Fund;
			string Acct;
			string tempAcct;
			string Year = "";
			string tempAccount;
			tempAcct = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			tempAccount = Strings.Right(x, x.Length - 2);
			Fund = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
			tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1));
			Acct = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
			if (!modAccountTitle.Statics.YearFlag)
			{
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + 1));
				Year = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
			}
			rsLedger.FindFirstRecord2("Fund, Account", Fund + "," + tempAcct, ",");
			if (rsLedger.NoMatch != true)
			{
				vs2.TextMatrix(vs2.Row, 1, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
				if (!modAccountTitle.Statics.YearFlag)
				{
					rsLedger.FindFirstRecord2("Fund, Account, Year", Fund + "," + Acct + "," + Year, ",");
					if (rsLedger.NoMatch != true)
					{
						vs2.TextMatrix(vs2.Row, 2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs2.TextMatrix(vs2.Row, 4, "Undefined");
						vs2.TextMatrix(vs2.Row, 3, "Undefined");
						vs2.TextMatrix(vs2.Row, 2, "Undefined");
						vs2.TextMatrix(vs2.Row, 1, "Undefined");
						return;
					}
				}
				else
				{
					rsLedger.FindFirstRecord2("Fund, Account", Fund + "," + Acct, ",");
					if (rsLedger.NoMatch != true)
					{
						vs2.TextMatrix(vs2.Row, 2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
						return;
					}
					else
					{
						vs2.TextMatrix(vs2.Row, 4, "Undefined");
						vs2.TextMatrix(vs2.Row, 3, "Undefined");
						vs2.TextMatrix(vs2.Row, 2, "Undefined");
						vs2.TextMatrix(vs2.Row, 1, "Undefined");
						return;
					}
				}
			}
			else
			{
				vs2.TextMatrix(vs2.Row, 4, "Undefined");
				vs2.TextMatrix(vs2.Row, 3, "Undefined");
				vs2.TextMatrix(vs2.Row, 2, "Undefined");
				vs2.TextMatrix(vs2.Row, 1, "Undefined");
				return;
			}
		}

		public void Init(string strAcctType = "A")
		{
			strTypesToShow = strAcctType;
			frmFindValidAccount.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
		}
	}
}
