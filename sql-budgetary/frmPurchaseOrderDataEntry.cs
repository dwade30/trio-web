﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Budgetary.Enums;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurchaseOrderDataEntry.
	/// </summary>
	public partial class frmPurchaseOrderDataEntry : BaseForm
	{
		public frmPurchaseOrderDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTownAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtShipToAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_0, 0);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_1, 1);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_2, 2);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_3, 3);
			this.txtShipToAddress.AddControlArrayElement(txtShipToAddress_0, 0);
			this.txtShipToAddress.AddControlArrayElement(txtShipToAddress_1, 1);
			this.txtShipToAddress.AddControlArrayElement(txtShipToAddress_2, 2);
			this.txtShipToAddress.AddControlArrayElement(txtShipToAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurchaseOrderDataEntry InstancePtr
		{
			get
			{
				return (frmPurchaseOrderDataEntry)Sys.GetInstance(typeof(frmPurchaseOrderDataEntry));
			}
		}

		protected frmPurchaseOrderDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int DescriptionCol;
		int AccountCol;
		int PriceCol;
		int QuantityCol;
		int AmountCol;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		bool SearchFlag;
		int NumberCol;
		// vbPorter upgrade warning: TotalAmount As double	OnWrite(short, Decimal)
		double TotalAmount;
		double AmountToPay;
		bool EditFlag;
		string ErrorString = "";
		bool BadAccountFlag;
		int CurrJournal;
		public int VendorNumber;
		public int FirstRecordShown;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		bool blnUnload;
		bool blnJournalLocked;
		string strComboList = "";
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsGridAccount vsGrid = new clsGridAccount();
		public bool blnFromVendor;
		public bool blnFromVendorMaster;
		int lngIDToPrint;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			int counter;
			txtSearch.Text = "";
			frmSearch.Visible = false;
			cboDepartment.Enabled = true;
			txtVendor.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			txtComments.Enabled = true;
			vs1.Enabled = true;
			txtVendor.Focus();
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			fraTownInfo.Visible = false;
		}

		public void cmdCancelPrint_Click()
		{
			cmdCancelPrint_Click(cmdCancelPrint, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			modRegistry.SaveRegistryKey("TownName", txtTownName.Text);
			modRegistry.SaveRegistryKey("TownAddress1", txtTownAddress[0].Text);
			modRegistry.SaveRegistryKey("TownAddress2", txtTownAddress[1].Text);
			modRegistry.SaveRegistryKey("TownAddress3", txtTownAddress[2].Text);
			modRegistry.SaveRegistryKey("TownAddress4", txtTownAddress[3].Text);
			modRegistry.SaveRegistryKey("ShipToTownName", txtShipToName.Text);
			modRegistry.SaveRegistryKey("ShipToTownAddress1", txtShipToAddress[0].Text);
			modRegistry.SaveRegistryKey("ShipToTownAddress2", txtShipToAddress[1].Text);
			modRegistry.SaveRegistryKey("ShipToTownAddress3", txtShipToAddress[2].Text);
			modRegistry.SaveRegistryKey("ShipToTownAddress4", txtShipToAddress[3].Text);
			cmdCancelPrint_Click();
			//Application.DoEvents();
			rptNewPurchaseOrder.InstancePtr.lngID = lngIDToPrint;
			// VendorNumber
			frmReportViewer.InstancePtr.Init(rptNewPurchaseOrder.InstancePtr);
			// , , vbModal
		}

		private void cmdRetrieve_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: tempAccount As int	OnWrite(string)
			int tempAccount = 0;
			int counter;
			string temp = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			if (lstRecords.Items[lstRecords.SelectedIndex].Text.Length > 0)
			{
				// if a record is highlighted
				tempAccount = FCConvert.ToInt32(Strings.Mid(lstRecords.Items[lstRecords.SelectedIndex].Text, 1, 5));
				// get the account number of that record
			}
			if (tempAccount != 0)
			{
				// if there is a valid account number
				rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(tempAccount));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// if there is a record
					rs.MoveLast();
					rs.MoveFirst();
					cboDepartment.Enabled = true;
					txtVendor.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					vs1.Enabled = true;
					txtComments.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "00000");
					lblVendorName.Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					lblAddress1.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					lblAddress2.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					lblAddress3.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4"))) != "")
					{
						lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip") + "-" + rs.Get_Fields_String("CheckZip4");
					}
					else
					{
						lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip");
					}
					if (Strings.Trim(lblAddress3.Text) == "")
					{
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					if (Strings.Trim(lblAddress2.Text) == "")
					{
						lblAddress2.Text = lblAddress3.Text;
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					if (Strings.Trim(lblAddress1.Text) == "")
					{
						lblAddress2.Text = lblAddress3.Text;
						lblAddress1.Text = lblAddress2.Text;
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					lstRecords.Clear();
					// clear the list box
					frmInfo.Visible = false;
					// make the list of records invisible
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtDescription.Focus();
				}
			}
		}

		public void cmdRetrieve_Click()
		{
			cmdRetrieve_Click(cmdRetrieve, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			int counter;
			frmInfo.Visible = false;
			lstRecords.Clear();
			cboDepartment.Enabled = true;
			txtVendor.Enabled = true;
			txtDate.Enabled = true;
			txtDescription.Enabled = true;
			vs1.Enabled = true;
			txtComments.Enabled = true;
			txtVendor.Focus();
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			frmSearch.Visible = false;
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				frmSearch.Visible = true;
				txtSearch.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM (SELECT CheckName, CheckAddress1, VendorNumber, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DataEntryMessage, CheckName as VendorSearchName FROM VendorMaster WHERE CheckName like '" + modCustomReport.FixQuotes(txtSearch.Text) + "%' AND Status <> 'D' UNION ALL SELECT CheckName, CheckAddress1, VendorNumber, CheckAddress2, CheckAddress3, CheckCity, CheckState, CheckZip, CheckZip4, DataEntryMessage, CheckAddress1 as VendorSearchName FROM VendorMaster WHERE upper(left(CheckAddress1, " + FCConvert.ToString(1 + txtSearch.Text.Length) + ")) = '*" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND upper(left(CheckName, " + FCConvert.ToString(txtSearch.Text.Length) + ")) <> '" + modCustomReport.FixQuotes(Strings.UCase(txtSearch.Text)) + "' AND Status <> 'D') as temp ORDER BY VendorSearchName");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					txtSearch.Text = "";
					frmWait.InstancePtr.Unload();
					cboDepartment.Enabled = true;
					txtVendor.Enabled = true;
					txtDate.Enabled = true;
					txtDescription.Enabled = true;
					vs1.Enabled = true;
					txtComments.Enabled = true;
					temp = Conversion.Str(rs.Get_Fields_Int32("VendorNumber"));
					txtVendor.Text = Strings.Format(temp, "0000");
					lblVendorName.Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
					lblAddress1.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
					lblAddress2.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
					lblAddress3.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
					if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4"))) != "")
					{
						lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip") + "-" + rs.Get_Fields_String("CheckZip4");
					}
					else
					{
						lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip");
					}
					if (Strings.Trim(lblAddress3.Text) == "")
					{
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					if (Strings.Trim(lblAddress2.Text) == "")
					{
						lblAddress2.Text = lblAddress3.Text;
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					if (Strings.Trim(lblAddress1.Text) == "")
					{
						lblAddress2.Text = lblAddress3.Text;
						lblAddress1.Text = lblAddress2.Text;
						lblAddress3.Text = lblAddress4.Text;
						lblAddress4.Text = "";
					}
					if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
					{
						MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtDescription.Focus();
				}
				else
				{
					txtSearch.Text = "";
					Fill_List();
					frmWait.InstancePtr.Unload();
					frmInfo.Visible = true;
					// make the list of records visible
					// show listbox with all records
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Records Found That Match That Name", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboDepartment.Enabled = true;
				txtVendor.Enabled = true;
				txtDate.Enabled = true;
				txtDescription.Enabled = true;
				vs1.Enabled = true;
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmPurchaseOrderDataEntry_Activated(object sender, System.EventArgs e)
		{
			string temp = "";
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (!blnFromVendorMaster)
			{
				if (!SearchFlag)
				{
					NumberCol = 0;
					DescriptionCol = 1;
					AccountCol = 2;
					PriceCol = 3;
					QuantityCol = 4;
					AmountCol = 5;
					vs1.ColWidth(NumberCol, 0);
					vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
					vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.27));
					vs1.ColWidth(PriceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
					vs1.ColWidth(QuantityCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
					vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1187));
					//FC:FINAL:DDU:#2900 - aligned columns
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
					vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					vs1.ColAlignment(PriceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.ColAlignment(QuantityCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
					vs1.TextMatrix(0, DescriptionCol, "Description");
					vs1.TextMatrix(0, AccountCol, "Account");
					vs1.TextMatrix(0, PriceCol, "Price");
					vs1.TextMatrix(0, QuantityCol, "Quantity");
					vs1.TextMatrix(0, AmountCol, "Amount");
					vs1.ColFormat(AmountCol, "#,###.00");
					vs1.ColFormat(PriceCol, "#,###.0000");
					vs1.ColFormat(QuantityCol, "#,###.0000");
					if (!modBudgetaryMaster.Statics.blnPOEdit)
					{
						for (counter = 1; counter <= vs1.Rows - 1; counter++)
						{
							vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, PriceCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, QuantityCol, FCConvert.ToString(0));
							vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
						}
						txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
						TotalAmount = 0;
					}
					else
					{
						CalculateTotals();
					}
					txtVendor.Focus();
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					Form_Resize();
					this.Refresh();
				}
				else
				{
					SearchFlag = false;
				}
			}
			else
			{
				blnFromVendorMaster = false;
			}
		}

		private void frmPurchaseOrderDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmPurchaseOrderDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurchaseOrderDataEntry.FillStyle	= 0;
			//frmPurchaseOrderDataEntry.ScaleWidth	= 9045;
			//frmPurchaseOrderDataEntry.ScaleHeight	= 7500;
			//frmPurchaseOrderDataEntry.LinkTopic	= "Form2";
			//frmPurchaseOrderDataEntry.PaletteMode	= 1  'UseZOrder;
			//txtComments.TextRTF	= $"frmPurchaseOrderDataEntry.frx":508A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = 2;
			vsGrid.AllowSplits = modRegionalTown.IsRegionalTown();
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			RetrieveInfo();
			FillDepartmentCombo();
		}

		private void frmPurchaseOrderDataEntry_Resize(object sender, System.EventArgs e)
		{
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.3));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.27));
			vs1.ColWidth(PriceCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(QuantityCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.14));
			vs1.ColWidth(AmountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.1187));
			fraTownInfo.CenterToContainer(this.ClientArea);
			frmInfo.CenterToContainer(this.ClientArea);
			frmSearch.CenterToContainer(this.ClientArea);
			fraAccountBalance.CenterToContainer(this.ClientArea);
		}

		public void Form_Resize()
		{
			frmPurchaseOrderDataEntry_Resize(this, new System.EventArgs());
		}

		private void Form_QueryUnload(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			//FC:FINAL:DDU:#1082 - hide form before showing another to focus well
			//this.Hide();
			frmGetPurchaseOrders.InstancePtr.Show(App.MainForm);
		}

		private void frmPurchaseOrderDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "vs1" && this.ActiveControl.GetName() != "txtSearch")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void lblExpense_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			string strAcct = "";
			if (Button == MouseButtonConstants.RightButton)
			{
				strAcct = vs1.TextMatrix(vs1.Row, AccountCol);
				if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(strAcct, 1) != "E" && Strings.Left(strAcct, 1) != "P")
					{
						// do nothing
					}
					else
					{
						lblBalAccount.Text = "Account: " + strAcct;
						lblBalNetBudget.Text = Strings.Format(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct), "#,##0.00");
						lblBalPostedYTDNet.Text = Strings.Format(GetYTDNet(strAcct) + GetEncumbrance(strAcct), "#,##0.00");
						lblBalPendingYTDNet.Text = Strings.Format(GetPendingYTDNet(strAcct), "#,##0.00");
						lblBalBalance.Text = Strings.Format(FCConvert.ToDouble(lblBalNetBudget.Text) - FCConvert.ToDouble(lblBalPostedYTDNet.Text) - FCConvert.ToDouble(lblBalPendingYTDNet.Text), "#,##0.00");
						fraAccountBalance.Visible = true;
					}
				}
			}
		}

		private void lstRecords_DoubleClick(object sender, System.EventArgs e)
		{
			cmdRetrieve_Click();
		}

		private void mnuFileAddVendor_Click(object sender, System.EventArgs e)
		{
			// FC: FINAL: KV: IIT807 + FC - 8697
			this.Hide();
			modBudgetaryMaster.Statics.blnEdit = false;
			// else show we are creating a new vendor account
			modBudgetaryMaster.Statics.blnFromPO = true;
			frmVendorMaster.InstancePtr.Show(App.MainForm);
			// show the blankform
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			int counter;
			bool NoSaveFlag = false;
			vs1.Col = 1;
			txtDescription.Focus();
			//Application.DoEvents();
			if (txtVendor.Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			else if (Conversion.Val(txtVendor.Text) == 0)
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must fill in the Description before you can save.", "Invalid Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			CalculateTotals();
			if (TotalAmount <= 0)
			{
				MessageBox.Show("There must be an Amount to Pay in a Purchase Order", "Invalid Purchase Order", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					if (vs1.TextMatrix(counter, AccountCol) == "" || vs1.TextMatrix(counter, DescriptionCol) == "")
					{
						NoSaveFlag = true;
						break;
					}
				}
			}
			if (NoSaveFlag == true)
			{
				NoSaveFlag = false;
				MessageBox.Show("Each entry must have a Description, Account, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				lngIDToPrint = VendorNumber;
				txtTownName.Text = modRegistry.GetRegistryKey("TownName");
				txtTownAddress[0].Text = modRegistry.GetRegistryKey("TownAddress1");
				txtTownAddress[1].Text = modRegistry.GetRegistryKey("TownAddress2");
				txtTownAddress[2].Text = modRegistry.GetRegistryKey("TownAddress3");
				txtTownAddress[3].Text = modRegistry.GetRegistryKey("TownAddress4");
				txtShipToName.Text = modRegistry.GetRegistryKey("ShipToTownName");
				txtShipToAddress[0].Text = modRegistry.GetRegistryKey("ShipToTownAddress1");
				txtShipToAddress[1].Text = modRegistry.GetRegistryKey("ShipToTownAddress2");
				txtShipToAddress[2].Text = modRegistry.GetRegistryKey("ShipToTownAddress3");
				txtShipToAddress[3].Text = modRegistry.GetRegistryKey("ShipToTownAddress4");
				fraTownInfo.Visible = true;
			}
		}

		public void mnuFilePrint_Click()
		{
			mnuFilePrint_Click(cmdFilePrint, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0" && vs1.TextMatrix(vs1.Row, PriceCol) == "0" && vs1.TextMatrix(vs1.Row, QuantityCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, AmountCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					//FC:FINAL:VGE - #607 Data from selected field is being carried over to row bellow. Disabling data grid to prevent.
					vs1.Enabled = false;
					DeleteFlag = true;
					temp = vs1.Row;
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, PriceCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, QuantityCol, FCConvert.ToString(0));
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					CalculateTotals();
					DeleteFlag = false;
					//FC:FINAL:VGE - #607 Reenabling data grid.
					vs1.Enabled = true;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DescriptionCol);
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DescriptionCol);
				}
			}
		}

		private void mnuProcessDeleteEntry_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			clsDRWrapper rsDetail = new clsDRWrapper();
			int lngRecordToDelete = 0;
			clsDRWrapper rsJournalEntriesLeft = new clsDRWrapper();
			// make sure they want to delete this item
			counter = MessageBox.Show("Are you sure you want to delete this purchase order?", "Delete This Purchase Order?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (counter == DialogResult.Yes)
			{
				rsDetail.Execute("DELETE FROM PurchaseOrderDetails WHERE PurchaseOrderID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"), "Budgetary");
				if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						lngRecordToDelete = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						if (lngRecordToDelete == FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"))) 
						{
							modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						}
						else
						{
							FirstRecordShown = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
							do
							{
								modBudgetaryAccounting.Statics.SearchResults.MoveNext();
							}
							while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != lngRecordToDelete);
						}
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
						GetJournalData();
						cmdProcessPreviousEntry.Enabled = false;
						if (modBudgetaryAccounting.Statics.SearchResults.RecordCount() > 1)
						{
							cmdProcessNextEntry.Enabled = true;
						}
						else
						{
							cmdProcessNextEntry.Enabled = false;
						}
					}
					else
					{
						modBudgetaryAccounting.Statics.SearchResults.Delete();
						modBudgetaryAccounting.Statics.SearchResults.Update();
						if (cmdProcessNextEntry.Enabled)
						{
							if (cmdProcessPreviousEntry.Enabled == false)
							{
								mnuProcessNextEntry_Click();
								cmdProcessPreviousEntry.Enabled = false;
							}
							else
							{
								mnuProcessNextEntry_Click();
							}
						}
						else
						{
							mnuProcessPreviousEntry_Click();
							cmdProcessNextEntry.Enabled = false;
						}
					}
				}
				else
				{
					modBudgetaryAccounting.Statics.SearchResults.Delete();
					modBudgetaryAccounting.Statics.SearchResults.Update();
					//Application.DoEvents();
					mnuProcessQuit_Click();
				}
			}
		}

		private void mnuProcessNextEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			if (VendorNumber == FirstRecordShown)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
					if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
					{
						cmdProcessNextEntry.Enabled = false;
					}
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true)
				{
					if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							cmdProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
					else
					{
						if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() == true)
						{
							cmdProcessNextEntry.Enabled = false;
						}
						modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					}
				}
				else
				{
					cmdProcessNextEntry.Enabled = false;
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
				}
			}
			if (cmdProcessPreviousEntry.Enabled == false)
			{
				cmdProcessPreviousEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessNextEntry_Click()
		{
			mnuProcessNextEntry_Click(cmdProcessNextEntry, new System.EventArgs());
		}

		private void mnuProcessPreviousEntry_Click(object sender, System.EventArgs e)
		{
			vs1.Row = 1;
			//Application.DoEvents();
			txtVendor.Focus();
			modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
			if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
			{
				modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				cmdProcessPreviousEntry.Enabled = false;
			}
			else
			{
				if (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) == FirstRecordShown)
				{
					modBudgetaryAccounting.Statics.SearchResults.MovePrevious();
					if (modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() == true)
					{
						modBudgetaryAccounting.Statics.SearchResults.MoveNext();
						cmdProcessPreviousEntry.Enabled = false;
					}
				}
			}
			if (cmdProcessNextEntry.Enabled == false)
			{
				cmdProcessNextEntry.Enabled = true;
			}
			GetJournalData();
		}

		public void mnuProcessPreviousEntry_Click()
		{
			mnuProcessPreviousEntry_Click(cmdProcessPreviousEntry, new System.EventArgs());
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click()
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			int counter;
			lblExpense.Text = "";
			frmSearch.Visible = true;
			cboDepartment.Enabled = false;
			txtVendor.Enabled = false;
			txtDate.Enabled = false;
			txtDescription.Enabled = false;
			vs1.Enabled = false;
			txtComments.Enabled = false;
			txtSearch.Focus();
		}

		public void mnuProcessSearch_Click()
		{
			mnuProcessSearch_Click(mnuProcessSearch, new System.EventArgs());
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtDate.Text == "00/00/00")
			{
				txtDate.Text = FCConvert.ToString(DateTime.Today);
			}
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtVendor_Enter(object sender, System.EventArgs e)
		{
			mnuProcessSearch.Enabled = true;
			mnuFileAddVendor.Enabled = true;
			lblExpense.Text = "";
            //FC:FINAL:SBE - #3096 - set the cursor at position 0. Cursor position is checked in Validating event, and wrong action is performed if we don't have the same selection as in the original application
            if (txtVendor.Text.Length > 0)
            {
                txtVendor.Select(0, 1);
            }
		}

		private void txtVendor_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.A)
			{
				// FC: FINAL: KV: IIT807 + FC - 8697
				this.Hide();
				modBudgetaryMaster.Statics.blnEdit = false;
				// else show we are creating a new vendor account
				modBudgetaryMaster.Statics.blnFromPO = true;
				frmVendorMaster.InstancePtr.Show(App.MainForm);
				// show the blankform
			}
			else if (KeyCode == Keys.S)
			{
				KeyCode = 0;
				mnuProcessSearch_Click();
			}
		}

		private void txtVendor_Leave(object sender, System.EventArgs e)
		{
			mnuProcessSearch.Enabled = false;
			mnuFileAddVendor.Enabled = false;
		}

		public void txtVendor_Validate(bool validate)
		{
			txtVendor_Validate(txtVendor, new System.ComponentModel.CancelEventArgs(validate));
		}

		public void txtVendor_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if ((txtVendor.SelectionStart > 0 && Conversion.Val(txtVendor.Text) != 0) || blnFromVendor)
			{
				blnFromVendor = false;
				if (!Information.IsNumeric(txtVendor.Text))
				{
					MessageBox.Show("You may only enter a number in this field", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtVendor.Text = "";
					e.Cancel = true;
				}
				else
				{
					temp = txtVendor.Text;
					txtVendor.Text = Strings.Format(temp, "0000");
					if (!GetVendorInfo())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private bool GetVendorInfo()
		{
			bool GetVendorInfo = false;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter;
			int counter2;
			string strLabel = "";
			counter = 1;
			rs.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(txtVendor.Text)) + "AND Status <> 'D'");
			if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
			{
				if (FCConvert.ToString(rs.Get_Fields_String("Status")) == "S")
				{
					MessageBox.Show("This vendor may not be used at this time because it has a status of S", "Invalid Vendor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtVendor.Text = "";
					GetVendorInfo = false;
					return GetVendorInfo;
				}
				lblVendorName.Text = FCConvert.ToString(rs.Get_Fields_String("CheckName"));
				lblAddress1.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress1"));
				lblAddress2.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress2"));
				lblAddress3.Text = FCConvert.ToString(rs.Get_Fields_String("CheckAddress3"));
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("CheckZip4"))) != "")
				{
					lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip") + "-" + rs.Get_Fields_String("CheckZip4");
				}
				else
				{
					lblAddress4.Text = rs.Get_Fields_String("CheckCity") + ", " + rs.Get_Fields_String("CheckState") + " " + rs.Get_Fields_String("CheckZip");
				}
				if (Strings.Trim(lblAddress3.Text) == "")
				{
					lblAddress3.Text = lblAddress4.Text;
					lblAddress4.Text = "";
				}
				if (Strings.Trim(lblAddress2.Text) == "")
				{
					lblAddress2.Text = lblAddress3.Text;
					lblAddress3.Text = lblAddress4.Text;
					lblAddress4.Text = "";
				}
				if (Strings.Trim(lblAddress1.Text) == "")
				{
					lblAddress2.Text = lblAddress3.Text;
					lblAddress1.Text = lblAddress2.Text;
					lblAddress3.Text = lblAddress4.Text;
					lblAddress4.Text = "";
				}
				for (counter = counter; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 1, "");
					vs1.TextMatrix(counter, 2, "");
					vs1.TextMatrix(counter, 3, FCConvert.ToString(0));
					vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
				}
				CalculateTotals();
				if (FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")) != "")
				{
					MessageBox.Show(FCConvert.ToString(rs.Get_Fields_String("DataEntryMessage")), "Data Entry Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				GetVendorInfo = true;
			}
			else
			{
				lblVendorName.Text = "";
				lblAddress2.Text = "";
				lblAddress1.Text = "";
				lblAddress3.Text = "";
				lblAddress4.Text = "";
				MessageBox.Show("There is no Vendor that matches that number", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Text = "";
				GetVendorInfo = false;
			}
			return GetVendorInfo;
		}

		private void Fill_List()
		{
			string temp;
			int I;
			//FC:FINAL:BBE:#584 - wisej do not supported spaces and tabs in ListViewItems. Use colums instead.
			lstRecords.Columns.Clear();
			lstRecords.Columns.Add("VendorNumber", 120);
			lstRecords.Columns.Add("VendorName", lstRecords.Width - lstRecords.Columns[0].Width - 20);
			temp = "";
			// this fills the listbox with the account information so that the user can dblclick
			// an account to choose it
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				//Application.DoEvents();
				if (!rs.IsFieldNull("VendorNumber"))
				{
					temp = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
					temp = Strings.Format(temp, "0000");
					temp += "\t";
				}
				if (FCConvert.ToString(rs.Get_Fields_String("CheckName")) != "")
					temp += rs.Get_Fields_String("CheckName") + " // " + rs.Get_Fields_String("CheckAddress1");
				if (I < rs.RecordCount())
					rs.MoveNext();
				lstRecords.AddItem(temp);
				// go to the next record
			}
		}

		private void txtZip_KeyPress(ref short KeyAscii)
		{
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
		}

		private void txtZip4_KeyPress(ref short KeyAscii)
		{
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
				Decimal curBalance;
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
				if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
				{
					if (Strings.InStr(1, vs1.EditText, "_", CompareConstants.vbBinaryCompare) == 0)
					{
						if (Strings.Left(vs1.EditText, 1) != "E" && Strings.Left(vs1.EditText, 1) != "P")
						{
							// do nothing
						}
						else
						{
							curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.EditText) - GetBudgetAdjustments(vs1.EditText));
							curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.EditText) + GetEncumbrance(vs1.EditText));
							curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.EditText));
							lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
						}
					}
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
			Decimal curBalance;
			mnuProcessDelete.Enabled = true;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
			{
				if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
					{
						// do nothing
					}
					else
					{
						curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
						lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
					}
				}
			}
			if (this.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DescriptionCol;
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			else if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			vs1_RowColChange(sender, e);
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					if (vs1.Col == DescriptionCol)
					{
						if (vs1.EditText == "")
						{
							vs1.EditText = txtDescription.Text;
						}
					}
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, PriceCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, QuantityCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: curBalance As Decimal	OnWrite(double, Decimal)
			Decimal curBalance;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2)
			{
				if (Strings.InStr(1, vs1.TextMatrix(vs1.Row, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0)
				{
					if (Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "E" && Strings.Left(vs1.TextMatrix(vs1.Row, AccountCol), 1) != "P")
					{
						// do nothing
					}
					else
					{
						curBalance = FCConvert.ToDecimal(GetOriginalBudget(vs1.TextMatrix(vs1.Row, AccountCol)) - GetBudgetAdjustments(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)) + GetEncumbrance(vs1.TextMatrix(vs1.Row, AccountCol)));
						curBalance -= FCConvert.ToDecimal(GetPendingYTDNet(vs1.TextMatrix(vs1.Row, AccountCol)));
						lblExpense.Text = lblExpense.Text + "  -  " + Strings.Format(curBalance, "#,##0.00");
					}
				}
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = 1;
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.Col == AmountCol || vs1.Col == PriceCol || vs1.Col == QuantityCol)
			{
				vs1.EditMaxLength = 10;
				if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.Col != AccountCol)
			{
				if (vs1.Editable == FCGrid.EditableSettings.flexEDNone)
				{
					// do nothing
				}
				else
				{
					if (vs1.Col == AmountCol || vs1.Col == PriceCol || vs1.Col == QuantityCol)
					{
						if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
							vs1.EditSelLength = 1;
						}
						else
						{
							vs1.EditCell();
							vs1.EditSelStart = 0;
						}
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == AccountCol && KeyCode != Keys.F9)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = 0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col < AmountCol)
				{
					if (vs1.Col == DescriptionCol)
					{
						if (vs1.EditText == "")
						{
							vs1.EditText = txtDescription.Text;
						}
					}
					vs1.Col += 1;
				}
				else
				{
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Row += 1;
						vs1.Col = 0;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, PriceCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, QuantityCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						vs1.Row += 1;
						vs1.Col = 0;
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				if (KeyCode == Keys.C)
				{
					KeyCode = 0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string temp2 = "";
			float percent;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == AmountCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
				else if (vs1.EditText == "")
				{
					vs1.EditText = "0";
				}
				else
				{
					temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
					if (temp > 0)
					{
						if (vs1.EditText.Length > temp + 2)
						{
							vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
						}
					}
					if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
					{
						if (!ValidateBalance_6(vs1.TextMatrix(row, AccountCol), FCConvert.ToDecimal(vs1.EditText)))
						{
							e.Cancel = true;
							vs1.EditText = "";
							return;
						}
					}
					if (vs1.TextMatrix(row, AmountCol) != vs1.EditText)
					{
						if (Conversion.Val(vs1.TextMatrix(row, QuantityCol)) != 0)
						{
							vs1.TextMatrix(row, PriceCol, FCConvert.ToString(modGlobal.Round_8(FCConvert.ToDouble(vs1.EditText) / FCConvert.ToDouble(vs1.TextMatrix(row, QuantityCol)), 4)));
						}
						else
						{
							vs1.TextMatrix(row, PriceCol, vs1.EditText);
							vs1.TextMatrix(row, QuantityCol, FCConvert.ToString(1));
						}
					}
					CalculateTotals();
				}
			}
			else if (col == PriceCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
				else if (vs1.EditText == "")
				{
					vs1.EditText = "0";
				}
				else
				{
					temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
					if (temp > 0)
					{
						if (vs1.EditText.Length > temp + 4)
						{
							vs1.EditText = Strings.Left(vs1.EditText, temp + 4);
						}
					}
					vs1.TextMatrix(row, AmountCol, FCConvert.ToString(modGlobal.Round_8(FCConvert.ToDouble(vs1.EditText) * FCConvert.ToDouble(vs1.TextMatrix(row, QuantityCol)), 2)));
					if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
					{
						if (!ValidateBalance_6(vs1.TextMatrix(row, AccountCol), FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol))))
						{
							e.Cancel = true;
							vs1.EditText = "";
							return;
						}
					}
					CalculateTotals();
				}
			}
			else if (col == QuantityCol)
			{
				if (!Information.IsNumeric(vs1.EditText) && vs1.EditText != "")
				{
					MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
				else if (vs1.EditText == "")
				{
					vs1.EditText = "0";
				}
				else
				{
					temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
					if (temp > 0)
					{
						if (vs1.EditText.Length > temp + 4)
						{
							vs1.EditText = Strings.Left(vs1.EditText, temp + 4);
						}
					}
					vs1.TextMatrix(row, AmountCol, FCConvert.ToString(modGlobal.Round_8(FCConvert.ToDouble(vs1.EditText) * FCConvert.ToDouble(vs1.TextMatrix(row, PriceCol)), 2)));
					if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
					{
						if (!ValidateBalance_6(vs1.TextMatrix(row, AccountCol), FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol))))
						{
							e.Cancel = true;
							vs1.EditText = "";
							return;
						}
					}
					CalculateTotals();
				}
			}
			else if (col == AccountCol)
			{
				if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 1 || FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) == 2 || modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
				{
					if (!ValidateBalance_6(vs1.EditText, FCConvert.ToDecimal(vs1.TextMatrix(row, AmountCol))))
					{
						e.Cancel = true;
						vs1.EditText = "";
						return;
					}
				}
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
					if (vs1.Col == AmountCol)
					{
						if (Strings.Trim(vs1.EditText) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.EditText);
						}
					}
					else
					{
						if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
						{
							TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
						}
					}
				}
				else
				{
					if (Strings.Trim(vs1.TextMatrix(counter, AmountCol)) != "")
					{
						TotalAmount += FCConvert.ToDouble(vs1.TextMatrix(counter, AmountCol));
					}
				}
			}
			lblTotalAmount.Text = Strings.Format(TotalAmount, "#,##0.00");
		}

		private void SaveDetails()
		{
			int counter;
			rs.OmitNullsOnInsert = true;
			if (modBudgetaryMaster.Statics.blnPOEdit)
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrderDetails WHERE PurchaseOrderID = " + FCConvert.ToString(VendorNumber));
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						rs.Delete();
						rs.Update();
					}
				}
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					rs.OpenRecordset("SELECT * FROM PurchaseOrderDetails");
					rs.AddNew();
					// if so add it
					rs.Set_Fields("PurchaseOrderID", VendorNumber);
					rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
					rs.Set_Fields("Account", vs1.TextMatrix(counter, AccountCol));
					rs.Set_Fields("Total", vs1.TextMatrix(counter, AmountCol));
					rs.Set_Fields("Price", vs1.TextMatrix(counter, PriceCol));
					rs.Set_Fields("Quantity", vs1.TextMatrix(counter, QuantityCol));
					rs.Update();
					// update the database
				}
			}
		}

		private void GetJournalData()
		{
			clsDRWrapper rs2 = new clsDRWrapper();
			txtVendor.Text = modValidateAccount.GetFormat_6(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"), 5);
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			lblPurchaseOrder.Text = Strings.Format(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PO"), "0000");
			rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("VendorNumber"));
			lblVendorName.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
			lblAddress1.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
			lblAddress2.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
			lblAddress3.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
			if (Strings.Trim(FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"))) != "")
			{
				lblAddress4.Text = rs2.Get_Fields_String("CheckCity") + ", " + rs2.Get_Fields_String("CheckState") + " " + rs2.Get_Fields_String("CheckZip") + "-" + rs2.Get_Fields_String("CheckZip4");
			}
			else
			{
				lblAddress4.Text = rs2.Get_Fields_String("CheckCity") + ", " + rs2.Get_Fields_String("CheckState") + " " + rs2.Get_Fields_String("CheckZip");
			}
			if (Strings.Trim(lblAddress3.Text) == "")
			{
				lblAddress3.Text = lblAddress4.Text;
				lblAddress4.Text = "";
			}
			if (Strings.Trim(lblAddress2.Text) == "")
			{
				lblAddress2.Text = lblAddress3.Text;
				lblAddress3.Text = lblAddress4.Text;
				lblAddress4.Text = "";
			}
			if (Strings.Trim(lblAddress1.Text) == "")
			{
				lblAddress2.Text = lblAddress3.Text;
				lblAddress1.Text = lblAddress2.Text;
				lblAddress3.Text = lblAddress4.Text;
				lblAddress4.Text = "";
			}
			//FC:FINAL:SBE - check for date
			//if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PODate") != IntPtr.Zero)
			if (Information.IsDate(modBudgetaryAccounting.Statics.SearchResults.Get_Fields("PODate")))
				txtDate.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_DateTime("PODate"));
			txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
			txtComments.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Comments"));
			VendorNumber = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
			GetJournalDetails();
		}

		private void ClearData()
		{
			int counter;
			txtVendor.Text = "";
			lblPurchaseOrder.Text = "";
			lblVendorName.Text = "";
			lblAddress1.Text = "";
			lblAddress2.Text = "";
			lblAddress3.Text = "";
			lblAddress4.Text = "";
			txtDescription.Text = "";
			txtComments.Text = "";
			VendorNumber = 0;
			TotalAmount = 0;
			//txtDate.Text = FCConvert.ToString(DateTime.Today);
			// FC:FINAL:VGE - #877 Formatting current date to "MM/dd/yyyy"
			txtDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
			vs1.Clear();
			lblExpense.Text = "";
			lblTotalAmount.Text = "0.00";
			cboDepartment.SelectedIndex = -1;
			vs1.Rows = 12;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, PriceCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, QuantityCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
			}
			//FC:FINAL:DDU:#2900 - aligned columns
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, AmountCol, 4);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(PriceCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(QuantityCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, PriceCol, "Price");
			vs1.TextMatrix(0, QuantityCol, "Quantity");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(PriceCol, "#,###.0000");
			vs1.ColFormat(QuantityCol, "#,###.0000");
			vs1.Select(1, 1);
		}

		private void GetJournalDetails()
		{
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			int counter2;
			string strLabel = "";
			rs2.OpenRecordset("SELECT * FROM PurchaseOrderDetails WHERE PurchaseOrderID = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 11)
				{
					vs1.Rows = rs2.RecordCount() + 1;
				}
				else
				{
					vs1.Rows = 12;
				}
				counter = 1;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
					vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("Account")));
					vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields_Decimal("Price")));
					// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields("Quantity")));
					vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields_Decimal("Total")));
					rs2.MoveNext();
					counter += 1;
				}
				if (counter < vs1.Rows - 1)
				{
					for (counter = counter; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 4, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 1, "");
						vs1.TextMatrix(counter, 2, "");
						vs1.TextMatrix(counter, 3, FCConvert.ToString(0));
						vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
					}
				}
			}
		}

		private void SaveJournal()
		{
			int TempJournal;
			int ans;
			int counter;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			txtVendor.Focus();
			rs.OmitNullsOnInsert = true;
			if (VendorNumber != 0)
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + FCConvert.ToString(VendorNumber));
				rs.Edit();
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				rs.Set_Fields("PODate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("Comments", txtComments.Text);
				if (cboDepartment.SelectedIndex >= 0)
				{
					rs.Set_Fields("Department", modValidateAccount.GetFormat_6(FCConvert.ToString(cboDepartment.ItemData(cboDepartment.SelectedIndex)), FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))));
				}
				else
				{
					rs.Set_Fields("Department", "");
				}
				rs.Set_Fields("Closed", false);
				rs.Update();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				rs.Set_Fields("PODate", txtDate.Text);
				rs.Set_Fields("Description", txtDescription.Text);
				if (cboDepartment.SelectedIndex >= 0)
				{
					rs.Set_Fields("Department", modValidateAccount.GetFormat_6(FCConvert.ToString(cboDepartment.ItemData(cboDepartment.SelectedIndex)), FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))));
				}
				else
				{
					rs.Set_Fields("Department", "");
				}
				rs.Set_Fields("Comments", txtComments.Text);
				rs.Set_Fields("PO", lblPurchaseOrder.Text);
				rs.Set_Fields("Closed", false);
				rs.Update();
				VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				modBudgetaryMaster.IncrementPO();
				modBudgetaryMaster.UnlockPO();
				MessageBox.Show("This entry was saved with PO# " + lblPurchaseOrder.Text, "Info Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			SaveDetails();
			modBudgetaryMaster.Statics.lngCurrentPurchaseOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(lblPurchaseOrder.Text)));
			if (!modBudgetaryMaster.Statics.blnPOEdit)
			{
				if (MessageBox.Show("Would you like to print the purchase order?", "Print Purchase Order?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					mnuFilePrint_Click();
					// rptNewPurchaseOrder.lngID = VendorNumber
					// frmReportViewer.Init rptNewPurchaseOrder
				}
				ClearData();
				txtVendor.Focus();
				if (blnUnload)
				{
					Close();
				}
			}
			else
			{
				if (cmdProcessNextEntry.Enabled == false || blnUnload)
				{
					Close();
				}
				else
				{
					mnuProcessNextEntry_Click();
				}
			}
		}

		private void FillDepartmentCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			rsJournalPeriod.OpenRecordset("SELECT * FROM DeptDivTitles WHERE convert(int, IsNull(Division, 0)) = 0 ORDER BY Department");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					cboDepartment.AddItem(rsJournalPeriod.Get_Fields_String("Department") + " - " + rsJournalPeriod.Get_Fields_String("LongDescription"));
					cboDepartment.ItemData(cboDepartment.ListCount - 1, FCConvert.ToInt32(rsJournalPeriod.Get_Fields_String("Department")));
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			bool NoSaveFlag = false;
			int TempJournal;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			int TempPO;
			vs1.Col = 1;
			//FC:FINAL:IPI - #1033 - setting the focus on a control should not be done before showing the MessageBox; control's validation including grid cells validation is anyway working
			//txtDescription.Focus();
			//Application.DoEvents();
			if (txtVendor.Text == "")
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			else if (Conversion.Val(txtVendor.Text) == 0)
			{
				MessageBox.Show("You must fill in the Vendor data before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtVendor.Focus();
				return;
			}
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must fill in the Description before you can save.", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			CalculateTotals();
			if (TotalAmount <= 0)
			{
				MessageBox.Show("There must be an Amount to Pay in a Journal Entry", "Invalid Journal Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					if (vs1.TextMatrix(counter, AccountCol) == "" || vs1.TextMatrix(counter, DescriptionCol) == "")
					{
						NoSaveFlag = true;
						break;
					}
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
					{
						answer = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (answer == DialogResult.No)
						{
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Encumbrance Data Entry");
						}
					}
				}
			}
			if (NoSaveFlag == true)
			{
				NoSaveFlag = false;
				MessageBox.Show("Each entry must have a Description, Account, and an Amount to be valid", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (VendorNumber == 0)
				{
					if (modBudgetaryMaster.LockPO() == false)
					{
						MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						lblPurchaseOrder.Text = FCConvert.ToString(modBudgetaryMaster.GetNextPO());
					}
				}
				SaveJournal();
			}
		}

		private double GetOriginalBudget(string strAcct)
		{
			double GetOriginalBudget = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
				GetOriginalBudget = Conversion.Val(rsYTDActivity.Get_Fields("OriginalBudgetTotal"));
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments(string strAcct)
		{
			double GetBudgetAdjustments = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
				GetBudgetAdjustments = Conversion.Val(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private double GetPendingYTDDebit(string strAcct)
		{
			double GetPendingYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
				GetPendingYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PendingDebitsTotal"));
			}
			else
			{
				GetPendingYTDDebit = 0;
			}
			return GetPendingYTDDebit;
		}

		private double GetPendingYTDCredit(string strAcct)
		{
			double GetPendingYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
				GetPendingYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PendingCreditsTotal")) * -1;
			}
			else
			{
				GetPendingYTDCredit = 0;
			}
			return GetPendingYTDCredit;
		}

		private double GetPendingYTDNet(string strAcct)
		{
			double GetPendingYTDNet = 0;
			GetPendingYTDNet = GetPendingYTDDebit(strAcct) - GetPendingYTDCredit(strAcct);
			return GetPendingYTDNet;
		}

		private double GetYTDDebit(string strAcct)
		{
			double GetYTDDebit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
				GetYTDDebit = Conversion.Val(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
			}
			else
			{
				GetYTDDebit = 0;
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit(string strAcct)
		{
			double GetYTDCredit = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
				GetYTDCredit = Conversion.Val(rsYTDActivity.Get_Fields("PostedCreditsTotal")) * -1;
			}
			else
			{
				GetYTDCredit = 0;
			}
			return GetYTDCredit;
		}

		private double GetYTDNet(string strAcct)
		{
			double GetYTDNet = 0;
			GetYTDNet = GetYTDDebit(strAcct) - GetYTDCredit(strAcct);
			return GetYTDNet;
		}

		private double GetEncumbrance(string strAcct)
		{
			double GetEncumbrance = 0;
			if (rsYTDActivity.FindFirstRecord("Account", strAcct))
			{
				// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
				GetEncumbrance = Conversion.Val(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheck = "";
			string strTable;
			LowDate = modBudgetaryMaster.Statics.FirstMonth;
			if (modBudgetaryMaster.Statics.FirstMonth == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate = LowDate - 1;
			}
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			strTable = "ExpenseReportInfo";
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal , SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM " + strTable + " WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Account");
		}
		// vbPorter upgrade warning: curAmt As Decimal	OnWrite(double, Decimal)
		// vbPorter upgrade warning: 'Return' As bool	OnWriteFCConvert.ToInt16
		private bool ValidateBalance_6(string strAcct, Decimal curAmt)
		{
			return ValidateBalance(strAcct, ref curAmt);
		}

		private bool ValidateBalance(string strAcct, ref Decimal curAmt)
		{
			bool ValidateBalance = false;
			int intLevel = 0;
			// vbPorter upgrade warning: curNetBudget As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curNetBudget;
			// vbPorter upgrade warning: curYTDPostedNet As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curYTDPostedNet;
			// vbPorter upgrade warning: curYTDPendingNet As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curYTDPendingNet;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (modSecurity.ValidPermissions_6(this, (int)BudgetarySecurityItems.AccountsPayableOverSpend, false) == false)
			{
				intLevel = 1;
			}
			else
			{
				intLevel = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("BalanceValidation"))));
			}
			if (Strings.Left(strAcct, 1) != "E")
			{
				ValidateBalance = FCConvert.ToBoolean(1);
			}
			else
			{
				curNetBudget = FCConvert.ToDecimal(GetOriginalBudget(strAcct) - GetBudgetAdjustments(strAcct));
				curYTDPostedNet = FCConvert.ToDecimal(GetYTDNet(strAcct) + GetEncumbrance(strAcct));
				curYTDPendingNet = FCConvert.ToDecimal(GetPendingYTDNet(strAcct));
				if (curNetBudget - curYTDPostedNet - curYTDPendingNet - curAmt < 0)
				{
					if (intLevel == 1)
					{
						MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "You may not enter this amount because it would put this account over budget.", "Account Over Budget", MessageBoxButtons.OK, MessageBoxIcon.Information);
						ValidateBalance = FCConvert.ToBoolean(0);
					}
					else if (intLevel == 2)
					{
						ans = MessageBox.Show("Net Budget: " + Strings.Format(curNetBudget, "#,##0.00") + "\r\n" + "Posted YTD Net: " + Strings.Format(curYTDPostedNet, "#,##0.00") + "\r\n" + "Pending Net Activity: " + Strings.Format(curYTDPendingNet, "#,##0.00") + "\r\n" + "\r\n" + "Entering this amount would put this account over budget.  Do you wish to continue?", "Account Over Budget", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							ValidateBalance = FCConvert.ToBoolean(1);
						}
						else
						{
							ValidateBalance = FCConvert.ToBoolean(0);
						}
					}
					else
					{
						ValidateBalance = FCConvert.ToBoolean(1);
					}
				}
				else
				{
					ValidateBalance = FCConvert.ToBoolean(1);
				}
			}
			return ValidateBalance;
		}
	}
}
