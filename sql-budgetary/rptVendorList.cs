﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptVendorList.
	/// </summary>
	public partial class rptVendorList : BaseSectionReport
	{
		public static rptVendorList InstancePtr
		{
			get
			{
				return (rptVendorList)Sys.GetInstance(typeof(rptVendorList));
			}
		}

		protected rptVendorList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptVendorList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		clsDRWrapper rsVendorInfo = new clsDRWrapper();

		public rptVendorList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Vendor List";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsVendorInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
					return;
				}
			}
			else
			{
				rsVendorInfo.MoveNext();
				if (rsVendorInfo.EndOfFile() != true)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
					return;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmVendorListSetup.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrder = "";
			string strStatusSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmVendorListSetup.InstancePtr.chkYTDAmount.CheckState == CheckState.Unchecked)
			{
				Label14.Text = "";
			}
			blnFirstRecord = true;
			if (frmVendorListSetup.InstancePtr.cmbSpecificClass.SelectedIndex == 1)
			{
				if (frmVendorListSetup.InstancePtr.cmbStatusAll.SelectedIndex == 0)
				{
					if (frmVendorListSetup.InstancePtr.cmbRange.SelectedIndex == 0)
					{
						strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as [1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate, VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' ";
					}
					else
					{
						if (frmVendorListSetup.InstancePtr.cmbNumber.SelectedIndex == 1)
						{
							strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as 1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate , VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' AND CheckName >= '" + frmVendorListSetup.InstancePtr.txtLow.Text + "' AND CheckName <= '" + frmVendorListSetup.InstancePtr.txtHigh.Text + "' ";
						}
						else
						{
							strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as 1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate , VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' AND VendorNumber >= " + frmVendorListSetup.InstancePtr.txtLow.Text + " AND VendorNumber <= " + frmVendorListSetup.InstancePtr.txtHigh.Text + " ";
						}
					}
				}
				else
				{
					strStatusSQL = "(";
					if (frmVendorListSetup.InstancePtr.chkActive.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'A' OR ";
					}
					if (frmVendorListSetup.InstancePtr.chkSuspended.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'S' OR ";
					}
					if (frmVendorListSetup.InstancePtr.chkDeleted.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'D' OR ";
					}
					strStatusSQL = Strings.Left(strStatusSQL, strStatusSQL.Length - 4) + ") ";
					if (frmVendorListSetup.InstancePtr.cmbRange.SelectedIndex == 0)
					{
						strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as [1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate , VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE " + strStatusSQL + "AND VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' ";
					}
					else
					{
						if (frmVendorListSetup.InstancePtr.cmbNumber.SelectedIndex == 1)
						{
							strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as 1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate , VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE " + strStatusSQL + "AND VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' AND CheckName >= '" + frmVendorListSetup.InstancePtr.txtLow.Text + "' AND CheckName <= '" + frmVendorListSetup.InstancePtr.txtHigh.Text + "' ";
						}
						else
						{
							strOrder = "SELECT VendorMaster.VendorNumber as VendorNumber, VendorMaster.CheckName as CheckName, VendorMaster.Attorney AS Attorney, VendorMaster.Contact as Contact, VendorMaster.Status as Status, VendorMaster.Class as Class, VendorMaster.[1099] as 1099], VendorMaster.CheckAddress1 as CheckAddress1, VendorMaster.CheckAddress2 as CheckAddress2, VendorMaster.CheckAddress3 as CheckAddress3, VendorMaster.CheckCity as CheckCity, VendorMaster.CheckState as CheckState, VendorMaster.CheckZip as CheckZip, VendorMaster.CheckZip4 as CheckZip4, VendorMaster.TaxNumber as TaxNumber, VendorMaster.DataEntryMessage as DataEntryMessage, VendorMaster.InsuranceRequired as InsuranceRequired, VendorMaster.InsuranceVerifiedDate as InsuranceVerifiedDate , VendorMaster.TelephoneNumber as TelephoneNumber, VendorMaster.FaxNumber as FaxNumber, VendorMaster.Message as Message  " + "FROM (VendorMaster INNER JOIN VendorClass ON VendorMaster.VendorNumber = VendorClass.VendorNumber) WHERE " + strStatusSQL + "AND VendorClass.Class = '" + frmVendorListSetup.InstancePtr.cboClass.Text + "' AND VendorNumber >= " + frmVendorListSetup.InstancePtr.txtLow.Text + " AND VendorNumber <= " + frmVendorListSetup.InstancePtr.txtHigh.Text + " ";
						}
					}
				}
			}
			else
			{
				if (frmVendorListSetup.InstancePtr.cmbStatusAll.SelectedIndex == 0)
				{
					if (frmVendorListSetup.InstancePtr.cmbRange.SelectedIndex == 0)
					{
						strOrder = "SELECT * FROM VendorMaster ";
					}
					else
					{
						if (frmVendorListSetup.InstancePtr.cmbNumber.SelectedIndex == 1)
						{
							strOrder = "SELECT * FROM VendorMaster WHERE CheckName >= '" + frmVendorListSetup.InstancePtr.txtLow.Text + "' AND CheckName <= '" + frmVendorListSetup.InstancePtr.txtHigh.Text + "' ";
						}
						else
						{
							strOrder = "SELECT * FROM VendorMaster WHERE VendorNumber >= " + frmVendorListSetup.InstancePtr.txtLow.Text + " AND VendorNumber <= " + frmVendorListSetup.InstancePtr.txtHigh.Text + " ";
						}
					}
				}
				else
				{
					strStatusSQL = "(";
					if (frmVendorListSetup.InstancePtr.chkActive.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'A' OR ";
					}
					if (frmVendorListSetup.InstancePtr.chkSuspended.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'S' OR ";
					}
					if (frmVendorListSetup.InstancePtr.chkDeleted.CheckState == CheckState.Checked)
					{
						strStatusSQL += "VendorMaster.Status = 'D' OR ";
					}
					strStatusSQL = Strings.Left(strStatusSQL, strStatusSQL.Length - 4) + ") ";
					if (frmVendorListSetup.InstancePtr.cmbRange.SelectedIndex == 0)
					{
						strOrder = "SELECT * FROM VendorMaster WHERE " + strStatusSQL;
					}
					else
					{
						if (frmVendorListSetup.InstancePtr.cmbNumber.SelectedIndex == 1)
						{
							strOrder = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + "AND CheckName >= '" + frmVendorListSetup.InstancePtr.txtLow.Text + "' AND CheckName <= '" + frmVendorListSetup.InstancePtr.txtHigh.Text + "' ";
						}
						else
						{
							strOrder = "SELECT * FROM VendorMaster WHERE " + strStatusSQL + "AND VendorNumber >= " + frmVendorListSetup.InstancePtr.txtLow.Text + " AND VendorNumber <= " + frmVendorListSetup.InstancePtr.txtHigh.Text + " ";
						}
					}
				}
			}
			if (frmVendorListSetup.InstancePtr.cmbMessageNo.SelectedIndex == 1)
			{
				if (Strings.Right(strOrder, 13) == "VendorMaster ")
				{
					strOrder += "WHERE rtrim(VendorMaster.DataEntryMessage) <> '' ";
				}
				else
				{
					strOrder += "AND rtrim(VendorMaster.DataEntryMessage) <> '' ";
				}
			}
			if (frmVendorListSetup.InstancePtr.cmbInsuranceDateYes.SelectedIndex == 0)
			{
				if (Strings.Right(strOrder, 13) == "VendorMaster ")
				{
					strOrder += "WHERE VendorMaster.InsuranceRequired = 1 AND InsuranceVerifiedDate <= '" + frmVendorListSetup.InstancePtr.txtInsuranceVerifiedDate.Text + "' ";
				}
				else
				{
					strOrder += "AND VendorMaster.InsuranceRequired = 1 AND InsuranceVerifiedDate <= '" + frmVendorListSetup.InstancePtr.txtInsuranceVerifiedDate.Text + "' ";
				}
			}
			if (frmVendorListSetup.InstancePtr.cmbNumber.SelectedIndex == 1)
			{
				strOrder += "ORDER BY VendorMaster.CheckName";
			}
			else
			{
				strOrder += "ORDER BY VendorMaster.VendorNumber";
			}
			rsVendorInfo.OpenRecordset(strOrder);
			if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				Cancel();
				MessageBox.Show("No vendors were found that matched the search criteria", "No Vendors Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			Decimal curTotal = 0.0M;
            using (clsDRWrapper rsAdjustments = new clsDRWrapper())
            {
                int intCounter = 0;
                Field2.Visible = true;
                Field3.Visible = true;
                Line2.Visible = true;
                Field4.Visible = true;
                if (frmVendorListSetup.InstancePtr.chkDataEntryMessage.CheckState == CheckState.Checked)
                {
                    if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage"))) != "")
                    {
                        lblDataEntryMessage.Visible = true;
                        linDataEntryMessage.Visible = true;
                        rtfDataEntryMessage.Visible = true;
                        //FC:FINAL:DSE WordWrap is not functioning in RichTextBox
                        //rtfDataEntryMessage.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage")));
                        rtfDataEntryMessage.SetHtmlText(
                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("DataEntryMessage"))));
                    }
                    else
                    {
                        lblDataEntryMessage.Visible = false;
                        linDataEntryMessage.Visible = false;
                        rtfDataEntryMessage.Visible = false;
                    }
                }
                else
                {
                    lblDataEntryMessage.Visible = false;
                    linDataEntryMessage.Visible = false;
                    rtfDataEntryMessage.Visible = false;
                }

                if (frmVendorListSetup.InstancePtr.chkVendorMessage.CheckState == CheckState.Checked)
                {
                    if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Message"))) != "")
                    {
                        lblMessage.Visible = true;
                        linMessage.Visible = true;
                        rtfMessage.Visible = true;
                        //FC:FINAL:DSE WordWrap is not functioning in RichTextBox
                        //rtfMessage.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Message")));
                        rtfMessage.SetHtmlText(
                            Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("Message"))));
                    }
                    else
                    {
                        lblMessage.Visible = false;
                        linMessage.Visible = false;
                        rtfMessage.Visible = false;
                    }
                }
                else
                {
                    lblMessage.Visible = false;
                    linMessage.Visible = false;
                    rtfMessage.Visible = false;
                }

                if (frmVendorListSetup.InstancePtr.chkInsuranceVerifieddate.CheckState == CheckState.Checked)
                {
                    lblInsuranceVerifiedDate.Visible = true;
                    fldInsuranceVerifiedDate.Visible = true;
                }
                else
                {
                    lblInsuranceVerifiedDate.Visible = false;
                    fldInsuranceVerifiedDate.Visible = false;
                }

                if (frmVendorListSetup.InstancePtr.chkYTDAmount.CheckState == CheckState.Checked)
                {
                    curTotal = GetYTDAmount_2(FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")));
                    if (curTotal == 0 && frmVendorListSetup.InstancePtr.cmbNo.SelectedIndex == 0 || curTotal != 0 &&
                        frmVendorListSetup.InstancePtr.chkOnlyZeroAmounts.CheckState == CheckState.Checked)
                    {
                        Field1.Text = "";
                        fldVendor.Text = "";
                        fldName.Text = "";
                        fldContact.Text = "";
                        fldStatus.Text = "";
                        fldClass.Text = "";
                        fldTax.Text = "";
                        fldYTDAmount.Text = "";
                        fldAddress1.Text = "";
                        fldAddress2.Text = "";
                        fldAddress3.Text = "";
                        fldAddress4.Text = "";
                        fldTaxID.Text = "";
                        Field2.Visible = false;
                        Field3.Visible = false;
                        Line2.Visible = false;
                        Field4.Visible = false;
                        lblInsuranceVerifiedDate.Visible = false;
                        fldInsuranceVerifiedDate.Visible = false;
                        fldInsuranceRequired.Text = "";
                        fldClassCode1.Text = "";
                        fldClassCode2.Text = "";
                        fldClassCode3.Text = "";
                        fldClassCode4.Text = "";
                        fldClassCode5.Text = "";
                        fldAdjustment1.Text = "";
                        fldAdjustment2.Text = "";
                        fldAdjustment3.Text = "";
                        fldAdjustment4.Text = "";
                        fldAdjustment5.Text = "";
                        lblPhoneNumber.Text = "";
                        lblFaxNumber.Text = "";
                        fldPhoneNumber.Text = "";
                        fldFaxNumber.Text = "";
                        lblDataEntryMessage.Visible = false;
                        linDataEntryMessage.Visible = false;
                        rtfDataEntryMessage.Visible = false;
                        lblMessage.Visible = false;
                        linMessage.Visible = false;
                        rtfMessage.Visible = false;
                        return;
                    }
                }

                if (frmVendorListSetup.InstancePtr.chkAdjustments.CheckState == CheckState.Checked)
                {
                    rsAdjustments.OpenRecordset("SELECT * FROM Adjustments WHERE VendorNumber = " +
                                                rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                    if (rsAdjustments.EndOfFile() != true && rsAdjustments.BeginningOfFile() != true)
                    {
                        // do nothing
                    }
                    else
                    {
                        if (frmVendorListSetup.InstancePtr.cmbAdjustmentsYes.SelectedIndex == 1)
                        {
                            Field1.Text = "";
                            fldVendor.Text = "";
                            fldName.Text = "";
                            fldContact.Text = "";
                            fldStatus.Text = "";
                            fldClass.Text = "";
                            fldTax.Text = "";
                            fldYTDAmount.Text = "";
                            fldAddress1.Text = "";
                            fldAddress2.Text = "";
                            fldAddress3.Text = "";
                            fldAddress4.Text = "";
                            fldTaxID.Text = "";
                            Field2.Visible = false;
                            Field3.Visible = false;
                            Line2.Visible = false;
                            Field4.Visible = false;
                            lblInsuranceVerifiedDate.Visible = false;
                            fldInsuranceVerifiedDate.Visible = false;
                            fldInsuranceRequired.Text = "";
                            fldClassCode1.Text = "";
                            fldClassCode2.Text = "";
                            fldClassCode3.Text = "";
                            fldClassCode4.Text = "";
                            fldClassCode5.Text = "";
                            fldAdjustment1.Text = "";
                            fldAdjustment2.Text = "";
                            fldAdjustment3.Text = "";
                            fldAdjustment4.Text = "";
                            fldAdjustment5.Text = "";
                            lblPhoneNumber.Text = "";
                            lblFaxNumber.Text = "";
                            fldPhoneNumber.Text = "";
                            fldFaxNumber.Text = "";
                            lblDataEntryMessage.Visible = false;
                            linDataEntryMessage.Visible = false;
                            rtfDataEntryMessage.Visible = false;
                            lblMessage.Visible = false;
                            linMessage.Visible = false;
                            rtfMessage.Visible = false;
                            return;
                        }
                    }
                }

                fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5);
                fldName.Text = rsVendorInfo.Get_Fields_String("CheckName");
                fldContact.Text = rsVendorInfo.Get_Fields_String("Contact");
                fldStatus.Text = rsVendorInfo.Get_Fields_String("Status");
                fldClass.Text = rsVendorInfo.Get_Fields_String("Class");
                // TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
                if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields("1099")))
                {
                    if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("Attorney")))
                    {
                        fldTax.Text = "R";
                    }
                    else
                    {
                        fldTax.Text = "Y";
                    }
                }
                else
                {
                    fldTax.Text = "N";
                }

                if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("InsuranceRequired")))
                {
                    fldInsuranceRequired.Text = "Y";
                }
                else
                {
                    fldInsuranceRequired.Text = "N";
                }

                if (frmVendorListSetup.InstancePtr.chkAddress.CheckState == CheckState.Checked)
                {
                    fldAddress1.Text =
                        Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress1")));
                    fldAddress2.Text =
                        Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress2")));
                    fldAddress3.Text =
                        Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckAddress3")));
                    if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckState"))) != "")
                    {
                        if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
                        {
                            fldAddress4.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " +
                                                            Strings.Trim(FCConvert.ToString(
                                                                rsVendorInfo.Get_Fields_String("CheckState"))) + " " +
                                                            Strings.Trim(
                                                                FCConvert.ToString(
                                                                    rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" +
                                                            Strings.Trim(
                                                                FCConvert.ToString(
                                                                    rsVendorInfo.Get_Fields_String("CheckZip4"))));
                        }
                        else
                        {
                            fldAddress4.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + ", " +
                                                            Strings.Trim(FCConvert.ToString(
                                                                rsVendorInfo.Get_Fields_String("CheckState"))) + " " +
                                                            Strings.Trim(
                                                                FCConvert.ToString(
                                                                    rsVendorInfo.Get_Fields_String("CheckZip"))));
                        }
                    }
                    else
                    {
                        if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
                        {
                            fldAddress4.Text = Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " +
                                                            Strings.Trim(
                                                                FCConvert.ToString(
                                                                    rsVendorInfo.Get_Fields_String("CheckZip"))) + "-" +
                                                            Strings.Trim(
                                                                FCConvert.ToString(
                                                                    rsVendorInfo.Get_Fields_String("CheckZip4"))));
                        }
                        else
                        {
                            fldAddress4.Text =
                                Strings.Trim(rsVendorInfo.Get_Fields_String("CheckCity") + " " +
                                             Strings.Trim(
                                                 FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip"))));
                        }
                    }
                }
                else
                {
                    fldAddress1.Text = "";
                    fldAddress2.Text = "";
                    fldAddress3.Text = "";
                    fldAddress4.Text = "";
                }

                if (Strings.Trim(fldAddress3.Text) == "")
                {
                    fldAddress3.Text = fldAddress4.Text;
                    fldAddress4.Text = "";
                }

                if (Strings.Trim(fldAddress2.Text) == "")
                {
                    fldAddress2.Text = fldAddress3.Text;
                    fldAddress3.Text = "";
                }

                if (Strings.Trim(fldAddress1.Text) == "")
                {
                    fldAddress1.Text = fldAddress2.Text;
                    fldAddress2.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkTaxID.CheckState == CheckState.Checked)
                {
                    Field1.Text = "Tax:";
                    fldTaxID.Text = Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("TaxNumber")));
                }
                else
                {
                    Field1.Text = "";
                    fldTaxID.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkInsuranceVerifieddate.CheckState == CheckState.Checked)
                {
                    lblInsuranceVerifiedDate.Text = "Ins. Verified Date:";
                    fldInsuranceVerifiedDate.Text =
                        Strings.Format(rsVendorInfo.Get_Fields_DateTime("InsuranceVerifiedDate"), "MM/dd/yyyy");
                }
                else
                {
                    lblInsuranceVerifiedDate.Text = "";
                    fldInsuranceVerifiedDate.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkYTDAmount.CheckState == CheckState.Checked)
                {
                    fldYTDAmount.Text = Strings.Format(curTotal, "#,##0.00");
                }
                else
                {
                    fldYTDAmount.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkPhone.CheckState == CheckState.Checked)
                {
                    fldPhoneNumber.Text = rsVendorInfo.Get_Fields_String("TelephoneNumber");
                    lblPhoneNumber.Text = "Phone #:";
                }
                else
                {
                    fldPhoneNumber.Text = "";
                    lblPhoneNumber.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkFax.CheckState == CheckState.Checked)
                {
                    fldFaxNumber.Text = rsVendorInfo.Get_Fields_String("FaxNumber");
                    lblFaxNumber.Text = "Fax #:";
                }
                else
                {
                    fldFaxNumber.Text = "";
                    lblFaxNumber.Text = "";
                }

                if (frmVendorListSetup.InstancePtr.chkAdjustments.CheckState == CheckState.Checked)
                {
                    intCounter = 1;
                    if (rsAdjustments.EndOfFile() != true && rsAdjustments.BeginningOfFile() != true)
                    {
                        do
                        {
                            switch (intCounter)
                            {
                                case 1:
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    fldClassCode1.Text = FCConvert.ToString(rsAdjustments.Get_Fields("Code"));
                                    if (rsAdjustments.Get_Fields_Boolean("Positive"))
                                    {
                                        fldAdjustment1.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment"), "#,##0.00");
                                    }
                                    else
                                    {
                                        fldAdjustment1.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment") * -1,
                                                "#,##0.00");
                                    }

                                    break;
                                }
                                case 2:
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    fldClassCode2.Text = FCConvert.ToString(rsAdjustments.Get_Fields("Code"));
                                    if (rsAdjustments.Get_Fields_Boolean("Positive"))
                                    {
                                        fldAdjustment2.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment"), "#,##0.00");
                                    }
                                    else
                                    {
                                        fldAdjustment2.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment") * -1,
                                                "#,##0.00");
                                    }

                                    break;
                                }
                                case 3:
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    fldClassCode3.Text = FCConvert.ToString(rsAdjustments.Get_Fields("Code"));
                                    if (rsAdjustments.Get_Fields_Boolean("Positive"))
                                    {
                                        fldAdjustment3.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment"), "#,##0.00");
                                    }
                                    else
                                    {
                                        fldAdjustment3.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment") * -1,
                                                "#,##0.00");
                                    }

                                    break;
                                }
                                case 4:
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    fldClassCode4.Text = FCConvert.ToString(rsAdjustments.Get_Fields("Code"));
                                    if (rsAdjustments.Get_Fields_Boolean("Positive"))
                                    {
                                        fldAdjustment4.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment"), "#,##0.00");
                                    }
                                    else
                                    {
                                        fldAdjustment4.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment") * -1,
                                                "#,##0.00");
                                    }

                                    break;
                                }
                                case 5:
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    fldClassCode5.Text = FCConvert.ToString(rsAdjustments.Get_Fields("Code"));
                                    if (rsAdjustments.Get_Fields_Boolean("Positive"))
                                    {
                                        fldAdjustment5.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment"), "#,##0.00");
                                    }
                                    else
                                    {
                                        fldAdjustment5.Text =
                                            Strings.Format(rsAdjustments.Get_Fields_Decimal("Adjustment") * -1,
                                                "#,##0.00");
                                    }

                                    break;
                                }
                            }

                            //end switch
                            intCounter += 1;
                            rsAdjustments.MoveNext();
                        } while (rsAdjustments.EndOfFile() != true);
                    }

                    switch (intCounter)
                    {
                        case 1:
                        {
                            Field2.Visible = false;
                            Field3.Visible = false;
                            Line2.Visible = false;
                            Field4.Visible = false;
                            fldClassCode1.Text = "";
                            fldClassCode2.Text = "";
                            fldClassCode3.Text = "";
                            fldClassCode4.Text = "";
                            fldClassCode5.Text = "";
                            fldAdjustment1.Text = "";
                            fldAdjustment2.Text = "";
                            fldAdjustment3.Text = "";
                            fldAdjustment4.Text = "";
                            fldAdjustment5.Text = "";
                            break;
                        }
                        case 2:
                        {
                            fldClassCode2.Text = "";
                            fldClassCode3.Text = "";
                            fldClassCode4.Text = "";
                            fldClassCode5.Text = "";
                            fldAdjustment2.Text = "";
                            fldAdjustment3.Text = "";
                            fldAdjustment4.Text = "";
                            fldAdjustment5.Text = "";
                            break;
                        }
                        case 3:
                        {
                            fldClassCode3.Text = "";
                            fldClassCode4.Text = "";
                            fldClassCode5.Text = "";
                            fldAdjustment3.Text = "";
                            fldAdjustment4.Text = "";
                            fldAdjustment5.Text = "";
                            break;
                        }
                        case 4:
                        {
                            fldClassCode4.Text = "";
                            fldClassCode5.Text = "";
                            fldAdjustment4.Text = "";
                            fldAdjustment5.Text = "";
                            break;
                        }
                        case 5:
                        {
                            fldClassCode5.Text = "";
                            fldAdjustment5.Text = "";
                            break;
                        }
                    }

                    //end switch
                }
                else
                {
                    Field2.Visible = false;
                    Field3.Visible = false;
                    Line2.Visible = false;
                    Field4.Visible = false;
                    fldClassCode1.Text = "";
                    fldClassCode2.Text = "";
                    fldClassCode3.Text = "";
                    fldClassCode4.Text = "";
                    fldClassCode5.Text = "";
                    fldAdjustment1.Text = "";
                    fldAdjustment2.Text = "";
                    fldAdjustment3.Text = "";
                    fldAdjustment4.Text = "";
                    fldAdjustment5.Text = "";
                }
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(short, double)
		private Decimal GetYTDAmount_2(int lngVendor)
		{
			return GetYTDAmount(ref lngVendor);
		}

		private Decimal GetYTDAmount(ref int lngVendor)
		{
			Decimal GetYTDAmount = 0;
            using (clsDRWrapper rsYTDInfo = new clsDRWrapper())
            {
                rsYTDInfo.OpenRecordset(
                    "SELECT SUM(APJournalDetail.Amount) as TotalAmount, SUM(Discount) as TotalDiscount FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE VendorNumber = " +
                    FCConvert.ToString(lngVendor) + " AND Status = 'P'");
                if (rsYTDInfo.EndOfFile() != true && rsYTDInfo.BeginningOfFile() != true)
                {
                    if (rsYTDInfo.IsFieldNull("TotalAmount"))
                    {
                        GetYTDAmount = 0;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [TotalDiscount] not found!! (maybe it is an alias?)
                        GetYTDAmount = FCConvert.ToDecimal(rsYTDInfo.Get_Fields_Decimal("TotalAmount") -
                                                           rsYTDInfo.Get_Fields("TotalDiscount"));
                    }
                }
                else
                {
                    GetYTDAmount = 0;
                }
            }

            return GetYTDAmount;
		}

		//public void Init()
		//{
		//	modDuplexPrinting.DuplexPrintReport(this);
		//}
	}
}
