﻿//Fecher vbPorter - Version 1.0.0.27
using System.Collections.Generic;

namespace TWBD0000
{
	public class cERGDictionaryGroup
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strGroupValue = string.Empty;
		private Dictionary<object, object> collExpenses = new Dictionary<object, object>();
		private Dictionary<object, object> collRevenues = new Dictionary<object, object>();
		private Dictionary<object, object> collGLs = new Dictionary<object, object>();

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string GroupValue
		{
			set
			{
				strGroupValue = value;
			}
			get
			{
				string GroupValue = "";
				GroupValue = strGroupValue;
				return GroupValue;
			}
		}

		public Dictionary<object, object> Expenses
		{
			get
			{
				Dictionary<object, object> Expenses = new Dictionary<object, object>();
				Expenses = collExpenses;
				return Expenses;
			}
		}

		public Dictionary<object, object> Revenues
		{
			get
			{
				Dictionary<object, object> Revenues = new Dictionary<object, object>();
				Revenues = collRevenues;
				return Revenues;
			}
		}

		public Dictionary<object, object> GeneralLedgers
		{
			get
			{
				Dictionary<object, object> GeneralLedgers = new Dictionary<object, object>();
				GeneralLedgers = collGLs;
				return GeneralLedgers;
			}
		}
	}
}
