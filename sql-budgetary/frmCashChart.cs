﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using GrapeCity.ActiveReports.Chart;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChart.
	/// </summary>
	public partial class frmCashChart : BaseForm
	{
		public GrapeCity.ActiveReports.SectionReportModel.ChartControl mscInfo;

		public frmCashChart()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.mscInfo = new GrapeCity.ActiveReports.SectionReportModel.ChartControl();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCashChart InstancePtr
		{
			get
			{
				return (frmCashChart)Sys.GetInstance(typeof(frmCashChart));
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         9/10/02
		// This form will be used to display different cash charts
		// the town creates
		// ********************************************************
		// vbPorter upgrade warning: arrData As object(,)	OnWrite(string(), Decimal(), short())
		object[,] arrData;
		// vbPorter upgrade warning: intMonths As short --> As int	OnWrite(int, double)
		int intMonths;
		int intLines;
		string strExpenseSQL;
		string strExpenseAPSQL;
		string strRevenueSQL;
		string strRevenueAPSQL;
		string strCashSQL;
		string strCashAPSQL;
		string strTaxSQL;
		string strTaxAPSQL;
		string strLiabilitySQL;
		string strLiabilityAPSQL;
		// vbPorter upgrade warning: red As Variant, short --> As int	OnWriteFCConvert.ToInt16
		// vbPorter upgrade warning: green As Variant, short --> As int	OnWriteFCConvert.ToInt16
		int red, green, blue;
		int intSeries;
		// vbPorter upgrade warning: arrTotals As object()	OnWrite(string(), Decimal(), object())
		object[] arrTotals = null;
		string[] arrTitles = null;

		private void ExportJpeg(GrapeCity.ActiveReports.SectionReportModel.ChartControl chart, PictureBox pBox)
		{
			using (GrapeCity.ActiveReports.SectionReport SectionReport = new GrapeCity.ActiveReports.SectionReport())
			{
				GrapeCity.ActiveReports.SectionReportModel.ChartControl chartCopy = new GrapeCity.ActiveReports.SectionReportModel.ChartControl();
				CopyChart(chart, ref chartCopy);
				chartCopy.Width = 6.5F;
				chartCopy.Height = 5.875F;
				GrapeCity.ActiveReports.SectionReportModel.Detail Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
				Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
					chartCopy
				});
				SectionReport.Sections.Add(Detail);
				SectionReport.Run();
				GrapeCity.ActiveReports.Document.Section.Page _pg = SectionReport.CurrentPage;
				_pg.Units = GrapeCity.ActiveReports.Document.Section.Units.Pixels;
				Bitmap bmp = new Bitmap(FCConvert.ToInt32(_pg.Width), FCConvert.ToInt32(_pg.Height));
				{
					Graphics g = Graphics.FromImage(bmp);
					g.FillRectangle(new SolidBrush(Color.White), 0, 0, bmp.Width, bmp.Height);
					_pg.Draw(g, new RectangleF(0, 0, _pg.Width, _pg.Height));
					if (pBox.Image != null)
					{
						pBox.Image.Dispose();
						pBox.Image = null;
					}
					pBox.Image = bmp;
					pBox.Update();
				}
			}
		}

		private void cboChartType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:JEI changing of Line Color and Pen style only works with Line charts
			this.gridSeries.Visible = false;
			switch (cboChartType.SelectedIndex)
			{
				case 0:
					{
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Bar3D;
						}
						break;
					}
				case 1:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dBar;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Bar2D;
						}
						break;
					}
				case 2:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType3dLine;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Line3D;
						}
						this.gridSeries.Visible = true;
						break;
					}
				case 3:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dLine;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Line;
						}
						this.gridSeries.Visible = true;
						break;
					}
				case 4:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType3dArea;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Area3D;
						}
						break;
					}
				case 5:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dArea;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Area;
						}
						break;
					}
				case 6:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType3dStep;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.StackedBar3D;
						}
						break;
					}
				case 7:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dStep;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.StackedBar;
						}
						break;
					}
				case 8:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType3dCombination;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.StackedArea3D;
						}
						break;
					}
				case 9:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dCombination;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.StackedArea;
						}
						break;
					}
				case 10:
					{
						//mscInfo.ChartType = VtChChartType.VtChChartType2dPie;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Doughnut;
						}
						break;
					}
				case 11:
					{
						//mscInfo.Series.Capacity = 1;
						//FC:TODO:AM - Chart control
						//mscInfo.ChartData = arrTotals;
						//mscInfo.ChartType = VtChChartType.VtChChartType2dPie;
						foreach (GrapeCity.ActiveReports.Chart.Series item in mscInfo.Series)
						{
							item.Type = GrapeCity.ActiveReports.Chart.ChartType.Doughnut3D;
						}
						break;
					}
			}
			//end switch
			//for (counter = 1; counter <= intLines; counter++)
			//{
			//    //mscInfo.Column = FCConvert.ToInt16(counter);
			//    //mscInfo.ColumnLabel = arrTitles[counter];
			//    mscInfo.Series[counter - 1].Name = arrTitles[counter];
			//}
			ExportJpeg(mscInfo, chartPreview);
			this.Refresh();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraLineStyle.Visible = false;
		}

		private void cmdColor_Click(object sender, System.EventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				// The CommonDialog control is named dlgChart.
				// CommonDialog object
				//- dlgChart.CancelError = true;
				vOnErrorGoToLabel = curOnErrorGoToLabel_ErrHandler;
				/* On Error GoTo ErrHandler */// Set the Flags property.
				// dlgChart_Save.Flags = vbPorterConverter.cdlCCRGBInit	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// Display the Color dialog box.
				dlgChart.ShowDialog();
				red = RedFromRGB(dlgChart.Color);
				green = GreenFromRGB(dlgChart.Color);
				blue = BlueFromRGB(dlgChart.Color);
				txtColor.BackColor = ColorTranslator.FromOle(Information.RGB(FCConvert.ToInt16(red), FCConvert.ToInt16(green), FCConvert.ToInt16(blue)));
				ErrHandler:
				;
				return;
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrHandler:
						//? goto ErrHandler;
						break;
				}
			}
		}

		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			// NOTE: Only the 2D and 3D line charts use the
			// Pen object. All other types use the Brush.
			//FC:TODO:AM - Chart control
			//mscInfo.Plot.SeriesCollection[intSeries].Pen.Style = cboPenStyle.SelectedIndex+1;
			//if (red!=-1) {
			//	if (mscInfo.chartType!=VtChChartType.VtChChartType2dLine || mscInfo.chartType!=VtChChartType.VtChChartType3dLine) {
			//		mscInfo.Plot.SeriesCollection(intSeries).DataPoints(-1).Brush.FillColor.Set(red, green, blue);
			//	} else {
			//		mscInfo.Plot.SeriesCollection(intSeries).Pen.VtColor.Set(red, green, blue);
			//	}
			//}
			fraLineStyle.Visible = false;
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCashChart.InstancePtr);
		}

		private void frmCashChart_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			int counter;
			clsDRWrapper rsTitleInfo = new clsDRWrapper();
			object XAxis;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			Decimal curTotal;
			int counter2;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (arrData != null)
			{
				FCUtils.EraseSafe(arrData);
			}
			if (arrTotals != null)
			{
				FCUtils.EraseSafe(arrTotals);
			}
			strExpenseSQL = "";
			strRevenueSQL = "";
			strCashSQL = "";
			strTaxSQL = "";
			strExpenseAPSQL = "";
			strRevenueAPSQL = "";
			strCashAPSQL = "";
			strTaxAPSQL = "";
			strLiabilitySQL = "";
			strLiabilityAPSQL = "";
			cboChartType.Clear();
			cboChartType.AddItem("3d Bar");
			// 0
			cboChartType.AddItem("2d Bar");
			// 1
			cboChartType.AddItem("3d Line");
			// 2
			cboChartType.AddItem("2d Line");
			// 3
			cboChartType.AddItem("3d Area");
			// 4
			cboChartType.AddItem("2d Area");
			// 5
			cboChartType.AddItem("3d Step");
			// 6
			cboChartType.AddItem("2d Step");
			// 7
			cboChartType.AddItem("3d Combination");
			// 8
			cboChartType.AddItem("2d Combination");
			// 9
			cboChartType.AddItem("2d Pie (Monthly)");
			// 14
			cboChartType.AddItem("2d Pie (Totals)");
			// 14
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				intMonths = 12;
			}
			else if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "R")
			{
				intMonths = FCConvert.ToInt32((modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth") - modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
				if (intMonths < 0)
				{
					intMonths *= -1;
				}
				intMonths += 1;
			}
			else
			{
				intMonths = 1;
			}
			rs.OpenRecordset("SELECT * FROM ItemsInChart WHERE Chartkey = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID"));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				ArrayExtensions.ResizeArray(ref arrData, intMonths + 1, rs.RecordCount() + 1);
				Array.Resize(ref arrTotals, rs.RecordCount() + 1);
				Array.Resize(ref arrTitles, rs.RecordCount() + 1);
			}
			else
			{
				MessageBox.Show("No Records Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			FillMonths();
			counter = 1;
			do
			{
				FillData_6(FCConvert.ToInt16(counter), FCConvert.ToInt32(rs.Get_Fields_Int32("GraphItemID")));
				strExpenseSQL = "";
				strRevenueSQL = "";
				strCashSQL = "";
				strTaxSQL = "";
				strExpenseAPSQL = "";
				strRevenueAPSQL = "";
				strCashAPSQL = "";
				strTaxAPSQL = "";
				strLiabilitySQL = "";
				strLiabilityAPSQL = "";
				counter += 1;
				rs.MoveNext();
			}
			while (rs.EndOfFile() != true);
			intLines = rs.RecordCount();
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				arrTotals[0] = "Totals";
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					curTotal = 0;
					for (counter2 = 0; counter2 <= intMonths - 1; counter2++)
					{
						curTotal += FCConvert.ToDecimal(arrData[counter2, counter]);
					}
					arrTotals[counter] = curTotal;
				}
			}
			else
			{
				arrTotals[0] = "Totals";
				curTotal = 0;
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					arrTotals[counter] = arrData[intMonths - 1, counter];
					curTotal = 0;
				}
			}
			CreateChartData(rs, rsTitleInfo);
			//cboChartType.SelectedIndex = 1;
			//mscInfo.ChartData.Visible = true;
			this.Refresh();
			cboChartType.Enabled = true;
			cmdPreview.Enabled = true;
		}

		private void CreateChartData(clsDRWrapper rs, clsDRWrapper rsTitleInfo)
		{
			rs.MoveFirst();
			GrapeCity.ActiveReports.Chart.ChartArea chartArea = new GrapeCity.ActiveReports.Chart.ChartArea(true);
			chartArea.Name = "chartarea1";
			mscInfo.ChartAreas.Add(chartArea);
			mscInfo.Border.Style = GrapeCity.ActiveReports.BorderLineStyle.None;
			mscInfo.ColorPalette = ColorPalette.Default;
			mscInfo.ChartAreas[0].Axes[0].Position = 0;
			mscInfo.ChartAreas[0].Axes[0].AxisType = GrapeCity.ActiveReports.Chart.AxisType.Categorical;
			mscInfo.ChartAreas[0].Axes[0].LabelsAtBottom = true;
			mscInfo.ChartAreas[0].Axes[0].LabelFont.Angle = -90;
			mscInfo.ChartAreas[0].Axes["AxisY"].Position = 0;
			mscInfo.ChartAreas[0].Axes["AxisY"].AxisType = GrapeCity.ActiveReports.Chart.AxisType.Numerical;
			mscInfo.ChartAreas[0].Axes["AxisY"].LabelFormat = "{0:#,##0.00}";
			mscInfo.ChartAreas[0].Axes["AxisY"].SmartLabels = false;
			mscInfo.ChartAreas[0].Axes["AxisY2"].Position = 0;
			mscInfo.ChartAreas[0].Axes["AxisY2"].AxisType = GrapeCity.ActiveReports.Chart.AxisType.Numerical;
			mscInfo.ChartAreas[0].Axes["AxisY2"].LabelFormat = "{0:#,##0.00}";
			mscInfo.ChartAreas[0].Axes["AxisY2"].SmartLabels = false;
			mscInfo.ChartAreas[0].Axes["AxisY2"].Visible = true;
			mscInfo.ChartAreas[0].Axes["AxisY2"].LabelsVisible = true;
			//Titles
			GrapeCity.ActiveReports.Chart.Title title = new GrapeCity.ActiveReports.Chart.Title();
			title.Text = modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Title");
			title.Alignment = GrapeCity.ActiveReports.Chart.Alignment.Center;
			title.Border.Line.Weight = 0;
			mscInfo.Titles.Add(title);
			//Legends
			GrapeCity.ActiveReports.Chart.Legend l = new GrapeCity.ActiveReports.Chart.Legend();
			l.Alignment = GrapeCity.ActiveReports.Chart.Alignment.TopLeft;
			l.DockArea = mscInfo.ChartAreas[0];
			l.Visible = true;
			l.DockInsideArea = true;
			l.GridLayout.Columns = rs.RecordCount();
			mscInfo.Legends.Add(l);
			//add series
			for (int counter = 0; counter < rs.RecordCount(); counter++)
			{
				rsTitleInfo.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + rs.Get_Fields_Int32("GraphItemID"));
				arrTitles[counter] = FCConvert.ToString(rsTitleInfo.Get_Fields_String("Title"));
				mscInfo.Series.Add(new GrapeCity.ActiveReports.Chart.Series() {
					ChartArea = mscInfo.ChartAreas[0]
				});
				FillStyleGrid(mscInfo.Series[counter], arrTitles[counter]);
				mscInfo.Series[counter].Legend = l;
				rs.MoveNext();
			}
			double maxValue = 0.0;
			List<string> legendItems = new List<string>();
			for (int i = 0; i < arrData.GetLength(0); i++)
			{
				object XValue = null;
				for (int j = 0; j < arrData.GetLength(1); j++)
				{
					if (arrData[i, j] != null)
					{
						//XValue
						if (j == 0)
						{
							XValue = arrData[i, j];
						}
						else
						{
							//YValues
							double yValue = 0;
							for (int k = 1; k < arrData.GetLength(1); k++)
							{
								GrapeCity.ActiveReports.Chart.DataPoint seriesLokalPoint = new GrapeCity.ActiveReports.Chart.DataPoint();
								seriesLokalPoint.XValue = XValue;
								if (!legendItems.Contains(arrTitles[k - 1]))
								{
									legendItems.Add(arrTitles[k - 1]);
									seriesLokalPoint.DisplayInLegend = true;
									seriesLokalPoint.LegendText = arrTitles[k - 1];
								}
								double.TryParse(arrData[i, k].ToString(), out yValue);
								seriesLokalPoint.YValues = new GrapeCity.ActiveReports.Chart.DoubleArray(new double[] {
									yValue
								});
								seriesLokalPoint.Line = new GrapeCity.ActiveReports.Chart.Graphics.Line();
								mscInfo.Series[k - 1].Points.Add(seriesLokalPoint);
								maxValue = Math.Max(maxValue, Math.Abs(yValue));
							}
							break;
						}
					}
				}
				if (XValue == null)
				{
					break;
				}
			}
			double factor = GetStepValue(maxValue);
			mscInfo.ChartAreas[0].Axes["AxisY"].MajorTick.Step = factor;
			mscInfo.ChartAreas[0].Axes["AxisY2"].MajorTick.Step = factor;
			mscInfo.ChartAreas[0].Axes["AxisY"].MinorTick.Step = factor;
			mscInfo.ChartAreas[0].Axes["AxisY2"].MinorTick.Step = factor;
		}

		private void FillStyleGrid(Series series, string title)
		{
			if (series.Line == null)
			{
				series.Line = new GrapeCity.ActiveReports.Chart.Graphics.Line();
			}
			GrapeCity.ActiveReports.Chart.Graphics.LineStyle lineStyle = series.Line.Style;
			int index = this.gridSeries.Rows.Add();
			this.gridSeries[colSeriesName, index].Value = title;
			this.gridSeries[colPenStyle, index].Value = "Solid";
			this.gridSeries[colLineColor, index].Style.BackColor = series.Line.Color;
			this.gridSeries[colSeriesName, index].Tag = series;
		}

		private void gridSeries_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == colLineColor.Index)
			{
				dlgChart.ShowDialog();
				red = RedFromRGB(dlgChart.Color);
				green = GreenFromRGB(dlgChart.Color);
				blue = BlueFromRGB(dlgChart.Color);
				gridSeries[colLineColor, e.RowIndex].Style.BackColor = ColorTranslator.FromOle(Information.RGB(FCConvert.ToInt16(red), FCConvert.ToInt16(green), FCConvert.ToInt16(blue)));
				Series series = this.gridSeries[colSeriesName, e.RowIndex].Tag as Series;
				if (series != null)
				{
					series.Line.Color = gridSeries[colLineColor, e.RowIndex].Style.BackColor;
					ExportJpeg(mscInfo, chartPreview);
				}
			}
		}

		private void gridSeries_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == colPenStyle.Index && e.RowIndex > 0)
			{
				var val = (gridSeries[colPenStyle, e.RowIndex] as DataGridViewComboBoxCell).Value;
				Series series = this.gridSeries[colSeriesName, e.RowIndex].Tag as Series;
				if (series != null)
				{
					series.Line.Style = (GrapeCity.ActiveReports.Chart.Graphics.LineStyle)Enum.Parse(typeof(GrapeCity.ActiveReports.Chart.Graphics.LineStyle), gridSeries[colPenStyle, e.RowIndex].Value.ToString());
					ExportJpeg(mscInfo, chartPreview);
				}
			}
		}

		private double GetStepValue(double maxValue)
		{
			double stepValue = maxValue / 5;
			string finalStepValueString = string.Empty;
			double finalStepValueDouble = 0;
			if (stepValue > 1)
			{
				if (stepValue > 10)
				{
					int stepValueInt = FCConvert.ToInt32(stepValue);
					int firstNumber = FCConvert.ToInt32(stepValueInt.ToString()[0].ToString());
					//if second number is greater 4 we increade the first number by 1
					if (FCConvert.ToInt32(stepValueInt.ToString()[1].ToString()) > 4)
					{
						firstNumber++;
					}
					else
					{
						firstNumber--;
					}
					finalStepValueString += firstNumber;
					for (int i = 1; i < stepValueInt.ToString().Length; i++)
					{
						finalStepValueString += "0";
					}
					double.TryParse(finalStepValueString, out finalStepValueDouble);
				}
				else
				{
					finalStepValueDouble = 2;
				}
			}
			else
			{
				finalStepValueDouble = 0.2;
			}
			return finalStepValueDouble;
		}

		public void CopyChart(GrapeCity.ActiveReports.SectionReportModel.ChartControl orig, ref GrapeCity.ActiveReports.SectionReportModel.ChartControl copy)
		{
			if (copy == null)
			{
				copy = new GrapeCity.ActiveReports.SectionReportModel.ChartControl();
			}
			for (int i = 0; i < orig.ChartAreas.Count; i++)
			{
				ChartArea c = new ChartArea(true);
				c.Name = orig.ChartAreas[i].Name;
				copy.ChartAreas.Add(c);
				for (int j = 0; j < orig.ChartAreas[i].Axes.Count; j++)
				{
					c.Axes[j].Position = orig.ChartAreas[i].Axes[j].Position;
					c.Axes[j].AxisType = orig.ChartAreas[i].Axes[j].AxisType;
					c.Axes[j].LabelsAtBottom = orig.ChartAreas[i].Axes[j].LabelsAtBottom;
					c.Axes[j].LabelFont.Angle = orig.ChartAreas[i].Axes[j].LabelFont.Angle;
					c.Axes[j].LabelFormat = orig.ChartAreas[i].Axes[j].LabelFormat;
					c.Axes[j].SmartLabels = orig.ChartAreas[i].Axes[j].SmartLabels;
					c.Axes[j].LabelsVisible = orig.ChartAreas[i].Axes[j].LabelsVisible;
					c.Axes[j].MajorTick.Step = orig.ChartAreas[i].Axes[j].MajorTick.Step;
					c.Axes[j].MinorTick.Step = orig.ChartAreas[i].Axes[j].MinorTick.Step;
				}
			}
			for (int k = 0; k < orig.Legends.Count; k++)
			{
				GrapeCity.ActiveReports.Chart.Legend l = new GrapeCity.ActiveReports.Chart.Legend();
				l.Alignment = orig.Legends[k].Alignment;
				l.DockArea = copy.ChartAreas[orig.Legends[k].DockArea.Name];
				l.Visible = orig.Legends[k].Visible;
				l.DockInsideArea = orig.Legends[k].DockInsideArea;
				l.GridLayout.Columns = orig.Legends[k].GridLayout.Columns;
				copy.Legends.Add(l);
			}
			for (int l = 0; l < orig.Titles.Count; l++)
			{
				//Titles
				GrapeCity.ActiveReports.Chart.Title title = new GrapeCity.ActiveReports.Chart.Title();
				title.Text = orig.Titles[l].Text;
				title.Font = (FontInfo)orig.Titles[l].Font.Clone();
				title.Alignment = orig.Titles[l].Alignment;
				copy.Titles.Add(title);
			}
			for (int m = 0; m < orig.Series.Count; m++)
			{
				Series s = new Series();
				s.ChartArea = copy.ChartAreas[orig.Series[m].ChartArea.Name];
				s.Legend = copy.Legends[0];
				s.Type = orig.Series[m].Type;
				copy.Series.Add(s);
				for (int n = 0; n < orig.Series[m].Points.Count; n++)
				{
					GrapeCity.ActiveReports.Chart.DataPoint p = new DataPoint();
					p.XValue = orig.Series[m].Points[n].XValue;
					p.DisplayInLegend = orig.Series[m].Points[n].DisplayInLegend;
					p.LegendText = orig.Series[m].Points[n].LegendText;
					p.YValues = (DoubleArray)orig.Series[m].Points[n].YValues.Clone();
					copy.Series[m].Points.Add(p);
				}
			}
		}

		private void frmCashChart_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCashChart.FillStyle	= 0;
			//frmCashChart.ScaleWidth	= 9045;
			//frmCashChart.ScaleHeight	= 7395;
			//frmCashChart.LinkTopic	= "Form2";
			//frmCashChart.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCashChart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCashChart.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			modDuplexPrinting.DuplexPrintReport(rptCashChart.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private void FillMonths()
		{
			int counter;
			int intMonth = 0;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
			{
				intMonth = modBudgetaryMaster.Statics.FirstMonth;
			}
			else
			{
				intMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
			}
			for (counter = 0; counter <= intMonths - 1; counter++)
			{
				arrData[counter, 0] = MonthCalc(intMonth);
				if (intMonth == 12)
				{
					intMonth = 1;
				}
				else
				{
					intMonth += 1;
				}
			}
		}
		// vbPorter upgrade warning: intItem As short	OnWriteFCConvert.ToInt32(
		private void FillData_6(short intItem, int lngDataKey)
		{
			FillData(ref intItem, ref lngDataKey);
		}

		private void FillData(ref short intItem, ref int lngDataKey)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			int counter;
			int intCurrentMonth = 0;
			rsData.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(lngDataKey));
			if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
			{
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "A")
				{
					intCurrentMonth = modBudgetaryMaster.Statics.FirstMonth;
				}
				else
				{
					intCurrentMonth = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				}
				for (counter = 0; counter <= intMonths - 1; counter++)
				{
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					string vbPorterVar = FCConvert.ToString(rsData.Get_Fields("Type"));
					if (vbPorterVar == "E")
					{
						arrData[counter, intItem] = GetExpenseData(lngDataKey, intCurrentMonth);
					}
					else if (vbPorterVar == "R")
					{
						arrData[counter, intItem] = GetRevenueData(ref lngDataKey, ref intCurrentMonth);
					}
					else if (vbPorterVar == "C")
					{
						arrData[counter, intItem] = GetCashData(ref lngDataKey, ref intCurrentMonth);
					}
					else if (vbPorterVar == "T")
					{
						arrData[counter, intItem] = GetTaxData(ref lngDataKey, ref intCurrentMonth);
					}
					if (intCurrentMonth == 12)
					{
						intCurrentMonth = 1;
					}
					else
					{
						intCurrentMonth += 1;
					}
				}
			}
			else
			{
				for (counter = 0; counter <= intMonths; counter++)
				{
					arrData[counter, intItem] = 0;
				}
			}
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		private Decimal GetExpenseData(int lngKey, int intMonth)
		{
			Decimal GetExpenseData = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			// vbPorter upgrade warning: intExpStart As short --> As int	OnWriteFCConvert.ToDouble(
			int intExpStart = 0;
			string[] strAccounts = null;
			int counter;
			if (strExpenseSQL == "")
			{
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					intExpStart = FCConvert.ToInt32(4 + Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)));
				}
				else
				{
					intExpStart = FCConvert.ToInt32(5 + Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
				}
				rsData.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(lngKey));
				if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "ALL")
				{
					strExpenseSQL = "left(Account, 1) = 'E'";
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "SD")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strExpenseSQL = "left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "SE")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strExpenseSQL = "left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E' AND mid(APJournalDetail.Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "RD")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseSQL = "left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "RE")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseSQL = "left(Account, 1) = 'E' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND substring(Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E' AND mid(APJournalDetail.Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND mid(APJournalDetail.Account, " + FCConvert.ToString(intExpStart) + ", " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "RA")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseSQL = "left(Account, 1) = 'E' AND Account >= '" + rsData.Get_Fields("Low") + "' AND Account <= '" + rsData.Get_Fields("High") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strExpenseAPSQL = "left(APJournalDetail.Account, 1) = 'E' AND APJournalDetail.Account >= '" + rsData.Get_Fields("Low") + "' AND APJournalDetail.Account <= '" + rsData.Get_Fields("High") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "SA")
				{
					strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedAccounts")), "  ", -1, CompareConstants.vbBinaryCompare);
					for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
					{
						strExpenseSQL += "Account = '" + strAccounts[counter] + "' OR ";
						strExpenseAPSQL += "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
					}
					strExpenseSQL = Strings.Left(strExpenseSQL, strExpenseSQL.Length - 4);
					strExpenseAPSQL = Strings.Left(strExpenseAPSQL, strExpenseAPSQL.Length - 4);
				}
			}
			GetExpenseData = FCConvert.ToDecimal(GetYTDNet_2("E", intMonth) * -1);
			return GetExpenseData;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		private Decimal GetRevenueData(ref int lngKey, ref int intMonth)
		{
			Decimal GetRevenueData = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			string[] strAccounts = null;
			int counter;
			if (strRevenueSQL == "")
			{
				rsData.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(lngKey));
				if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "ALL")
				{
					strRevenueSQL = "left(Account, 1) = 'R'";
					strRevenueAPSQL = "left(APJournalDetail.Account, 1) = 'R'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "SD")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strRevenueSQL = "left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					strRevenueAPSQL = "left(APJournalDetail.Account, 1) = 'R' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + rsData.Get_Fields("Low") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "RD")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strRevenueSQL = "left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strRevenueAPSQL = "left(APJournalDetail.Account, 1) = 'R' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + rsData.Get_Fields("Low") + "' AND mid(APJournalDetail.Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + rsData.Get_Fields("High") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "RA")
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strRevenueSQL = "left(Account, 1) = 'R' AND Account >= '" + rsData.Get_Fields("Low") + "' AND Account <= '" + rsData.Get_Fields("High") + "'";
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					strRevenueAPSQL = "left(APJournalDetail.Account, 1) = 'R' AND APJournalDetail.Account >= '" + rsData.Get_Fields("Low") + "' AND APJournalDetail.Account <= '" + rsData.Get_Fields("High") + "'";
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "SA")
				{
					strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedAccounts")), "  ", -1, CompareConstants.vbBinaryCompare);
					for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
					{
						strRevenueSQL = strExpenseSQL + "Account = '" + strAccounts[counter] + "' OR ";
						strRevenueAPSQL = strExpenseAPSQL + "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
					}
					strRevenueSQL = Strings.Left(strRevenueSQL, strRevenueSQL.Length - 4);
					strRevenueAPSQL = Strings.Left(strRevenueAPSQL, strRevenueAPSQL.Length - 4);
				}
			}
			GetRevenueData = FCConvert.ToDecimal(GetYTDNet_2("R", intMonth) * -1);
			return GetRevenueData;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		private Decimal GetCashData(ref int lngKey, ref int intMonth)
		{
			Decimal GetCashData = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			string[] strAccounts = null;
			int counter;
			if (strCashSQL == "")
			{
				rsData.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(lngKey));
				if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "TA")
				{
					strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedAccounts")), "  ", -1, CompareConstants.vbBinaryCompare);
					for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
					{
						strCashSQL += "Account = '" + strAccounts[counter] + "' OR ";
						strCashAPSQL += "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
					}
					strCashSQL = Strings.Left(strCashSQL, strCashSQL.Length - 4);
					strCashAPSQL = Strings.Left(strCashAPSQL, strCashAPSQL.Length - 4);
				}
				else if (FCConvert.ToString(rsData.Get_Fields_String("AccountSelectionType")) == "NA")
				{
					strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedAccounts")), "  ", -1, CompareConstants.vbBinaryCompare);
					for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
					{
						strCashSQL += "Account = '" + strAccounts[counter] + "' OR ";
						strCashAPSQL += "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
					}
					strCashSQL = Strings.Left(strCashSQL, strCashSQL.Length - 4);
					strCashAPSQL = Strings.Left(strCashAPSQL, strCashAPSQL.Length - 4);
					strAccounts = new string[0 + 1];
					strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedLiabilities")), "  ", -1, CompareConstants.vbBinaryCompare);
					for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
					{
						strLiabilitySQL += "Account = '" + strAccounts[counter] + "' OR ";
						strLiabilityAPSQL += "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
					}
					strLiabilitySQL = Strings.Left(strLiabilitySQL, strLiabilitySQL.Length - 4);
					strLiabilityAPSQL = Strings.Left(strLiabilityAPSQL, strLiabilityAPSQL.Length - 4);
				}
			}
			if (strLiabilitySQL == "")
			{
				GetCashData = FCConvert.ToDecimal(GetYTDNet_2("C", intMonth));
			}
			else
			{
				GetCashData = FCConvert.ToDecimal(GetYTDNet_2("C", intMonth) - GetYTDNet_2("L", intMonth));
			}
			return GetCashData;
		}
		// vbPorter upgrade warning: intMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
		private Decimal GetTaxData(ref int lngKey, ref int intMonth)
		{
			Decimal GetTaxData = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			string[] strAccounts = null;
			int counter;
			if (strTaxSQL == "")
			{
				rsData.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + FCConvert.ToString(lngKey));
				strAccounts = Strings.Split(FCConvert.ToString(rsData.Get_Fields_String("SelectedAccounts")), "  ", -1, CompareConstants.vbBinaryCompare);
				for (counter = 0; counter <= Information.UBound(strAccounts, 1); counter++)
				{
					strTaxSQL += "Account = '" + strAccounts[counter] + "' OR ";
					strTaxAPSQL += "APJournalDetail.Account = '" + strAccounts[counter] + "' OR ";
				}
				strTaxSQL = Strings.Left(strTaxSQL, strTaxSQL.Length - 4);
				strTaxAPSQL = Strings.Left(strTaxAPSQL, strTaxAPSQL.Length - 4);
			}
			GetTaxData = FCConvert.ToDecimal(GetYTDNet_2("T", intMonth));
			return GetTaxData;
		}

		private double GetYTDDebit(ref string strType, ref int intPeriod)
		{
			double GetYTDDebit = 0;
			int temp;
			double sum1 = 0;
			double sum2 = 0;
			double sum3 = 0;
			double sum4 = 0;
			int HighDate = 0;
			int LowDate = 0;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			clsDRWrapper rs3 = new clsDRWrapper();
			clsDRWrapper rs4 = new clsDRWrapper();
			clsDRWrapper rs5 = new clsDRWrapper();
			string strAcct = "";
			string strAPAcct = "";
			if (strType == "E")
			{
				strAcct = strExpenseSQL;
				strAPAcct = strExpenseAPSQL;
			}
			else if (strType == "R")
			{
				strAcct = strRevenueSQL;
				strAPAcct = strRevenueAPSQL;
			}
			else if (strType == "C")
			{
				strAcct = strCashSQL;
				strAPAcct = strCashAPSQL;
			}
			else if (strType == "L")
			{
				strAcct = strLiabilitySQL;
				strAPAcct = strLiabilityAPSQL;
			}
			else if (strType == "T")
			{
				strAcct = strTaxSQL;
				strAPAcct = strTaxAPSQL;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				// do nothing
			}
			else
			{
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					LowDate = intPeriod;
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
				{
					LowDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				}
				else
				{
					LowDate = modBudgetaryMaster.Statics.FirstMonth;
				}
				HighDate = intPeriod;
				if (LowDate > HighDate)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				rs2.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "D", false, false) + " AND Period = " + FCConvert.ToString(intPeriod));
				rs3.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Debits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "D", false, false) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod));
				rs5.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "D", true, false) + " AND Period = " + FCConvert.ToString(intPeriod));
				rs4.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Debits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "D", true, false) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod));
			}
			else
			{
				rs2.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "D", false, false) + " AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
				rs3.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Debits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "D", false, false) + " AND (APJournal.Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " APJournal.Period <= " + FCConvert.ToString(HighDate) + ")");
				rs5.OpenRecordset("SELECT SUM(Amount) AS Debits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "D", true, false) + " AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
				rs4.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Debits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "D", true, false) + " AND (APJournal.Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " APJournal.Period <= " + FCConvert.ToString(HighDate) + ")");
			}
			// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs2.Get_Fields("Debits")) == ""))
			{
				// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
				sum1 = FCConvert.ToDouble(rs2.Get_Fields("Debits"));
			}
			else
			{
				sum1 = 0;
			}
			// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs3.Get_Fields("Debits")) == ""))
			{
				// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
				sum2 = FCConvert.ToDouble(rs3.Get_Fields("Debits"));
			}
			else
			{
				sum2 = 0;
			}
			// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs5.Get_Fields("Debits")) == ""))
			{
				// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
				sum3 = FCConvert.ToDouble(rs5.Get_Fields("Debits"));
			}
			else
			{
				sum3 = 0;
			}
			// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs4.Get_Fields("Debits")) == ""))
			{
				// TODO Get_Fields: Field [Debits] not found!! (maybe it is an alias?)
				sum4 = FCConvert.ToDouble(FCConvert.ToString(rs4.Get_Fields("Debits")));
			}
			else
			{
				sum4 = 0;
			}
			GetYTDDebit = sum1 + sum2 - sum3 - sum4;
			return GetYTDDebit;
		}

		private double GetYTDCredit(ref string strType, ref int intPeriod)
		{
			double GetYTDCredit = 0;
			int temp;
			double sum1 = 0;
			double sum2 = 0;
			double sum3 = 0;
			double sum4 = 0;
			int HighDate = 0;
			int LowDate = 0;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			clsDRWrapper rs3 = new clsDRWrapper();
			clsDRWrapper rs4 = new clsDRWrapper();
			clsDRWrapper rs5 = new clsDRWrapper();
			string strAcct = "";
			string strAPAcct = "";
			if (strType == "E")
			{
				strAcct = strExpenseSQL;
				strAPAcct = strExpenseAPSQL;
			}
			else if (strType == "R")
			{
				strAcct = strRevenueSQL;
				strAPAcct = strRevenueAPSQL;
			}
			else if (strType == "C")
			{
				strAcct = strCashSQL;
				strAPAcct = strCashAPSQL;
			}
			else if (strType == "L")
			{
				strAcct = strLiabilitySQL;
				strAPAcct = strLiabilityAPSQL;
			}
			else if (strType == "T")
			{
				strAcct = strTaxSQL;
				strAPAcct = strTaxAPSQL;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				// do nothing
			}
			else
			{
				if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
				{
					LowDate = intPeriod;
				}
				else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
				{
					LowDate = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"));
				}
				else
				{
					LowDate = modBudgetaryMaster.Statics.FirstMonth;
				}
				HighDate = intPeriod;
				if (LowDate > HighDate)
				{
					strPeriodCheck = "OR";
				}
				else
				{
					strPeriodCheck = "AND";
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				rs2.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "C", true, false) + " AND Period = " + FCConvert.ToString(intPeriod));
				rs5.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "C", false, false) + " AND Period = " + FCConvert.ToString(intPeriod));
				rs3.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Credits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "C", false, false) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod));
				rs4.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Credits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAPAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "C", true, false) + " AND APJournal.Period = " + FCConvert.ToString(intPeriod));
			}
			else
			{
				rs2.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "C", true, false) + " AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
				rs5.OpenRecordset("SELECT SUM(Amount) AS Credits FROM JournalEntries WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("O", "C", false, false) + " AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
				rs3.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Credits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "C", false, false) + " AND (APJournal.Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " APJournal.Period <= " + FCConvert.ToString(HighDate) + ")");
				rs4.OpenRecordset("SELECT SUM(APJournalDetail.Amount) AS Credits FROM (APJournalDetail INNER JOIN APJournal ON APJournalDetail.APJournalID = APJournal.ID) WHERE (" + strAcct + ") AND " + modBudgetaryAccounting.GetDebCredSQL("A", "C", true, false) + " AND (APJournal.Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " APJournal.Period <= " + FCConvert.ToString(HighDate) + ")");
			}
			// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs2.Get_Fields("Credits")) == ""))
			{
				// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
				sum1 = FCConvert.ToDouble(rs2.Get_Fields("Credits"));
			}
			else
			{
				sum1 = 0;
			}
			// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs5.Get_Fields("Credits")) == ""))
			{
				// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
				sum3 = FCConvert.ToDouble(rs5.Get_Fields("Credits")) * -1;
			}
			else
			{
				sum3 = 0;
			}
			// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs3.Get_Fields("Credits")) == ""))
			{
				// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
				sum2 = FCConvert.ToDouble(rs3.Get_Fields("Credits")) * -1;
			}
			else
			{
				sum2 = 0;
			}
			// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
			if (!(FCConvert.ToString(rs4.Get_Fields("Credits")) == ""))
			{
				// TODO Get_Fields: Field [Credits] not found!! (maybe it is an alias?)
				sum4 = FCConvert.ToDouble(rs4.Get_Fields("Credits"));
			}
			else
			{
				sum4 = 0;
			}
			GetYTDCredit = sum1 + sum2 + sum3 + sum4;
			return GetYTDCredit;
		}

		private double GetYTDNet_2(string strAcct, int intPeriod)
		{
			return GetYTDNet(ref strAcct, ref intPeriod);
		}

		private double GetYTDNet(ref string strAcct, ref int intPeriod)
		{
			double GetYTDNet = 0;
			int temp;
			GetYTDNet = GetYTDDebit(ref strAcct, ref intPeriod) - GetYTDCredit(ref strAcct, ref intPeriod);
			return GetYTDNet;
		}
		// vbPorter upgrade warning: rgb As int --> As Color
		// vbPorter upgrade warning: 'Return' As short	OnWrite(int, bool)
		public short RedFromRGB(Color rgb)
		{
			short RedFromRGB = 0;
			// The ampersand after &HFF coerces the number as a
			// long, preventing Visual Basic from evaluating the
			// number as a negative value. The logical And is
			// used to return bit values.
			RedFromRGB = FCConvert.ToInt16(0xFF & ColorTranslator.ToOle(rgb));
			return RedFromRGB;
		}
		// vbPorter upgrade warning: rgb As int --> As Color
		public short GreenFromRGB(Color rgb)
		{
			short GreenFromRGB = 0;
			// The result of the And operation is divided by
			// 256, to return the value of the middle bytes.
			// Note the use of the Integer divisor.
			GreenFromRGB = FCConvert.ToInt16(FCUtils.iDiv((0xFF00 & ColorTranslator.ToOle(rgb)), 256));
			return GreenFromRGB;
		}
		// vbPorter upgrade warning: rgb As int --> As Color
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short BlueFromRGB(Color rgb)
		{
			short BlueFromRGB = 0;
			// This function works like the GreenFromRGB above,
			// except you don't need the ampersand. The
			// number is already a long. The result divided by
			// 65536 to obtain the highest bytes.
			BlueFromRGB = FCConvert.ToInt16(FCUtils.iDiv((0xFF0000 & ColorTranslator.ToOle(rgb)), 65536));
			return BlueFromRGB;
		}
	}
}
