﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCurrentAccountStatus.
	/// </summary>
	public partial class frmCurrentAccountStatus : BaseForm
	{
		public frmCurrentAccountStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_3, 3);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			cmbSingleAccount.SelectedIndex = 1;
			cmbTown.SelectedIndex = 0;
			cmbLedger.SelectedIndex = 0;
			cmbAll.SelectedIndex = 0;
			cmbPostedDate.SelectedIndex = 0;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCurrentAccountStatus InstancePtr
		{
			get
			{
				return (frmCurrentAccountStatus)Sys.GetInstance(typeof(frmCurrentAccountStatus));
			}
		}

		protected frmCurrentAccountStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         06/14/2002
		// This form will be used to set up what the user wants to
		// view in the Current Account Status report
		// ********************************************************
		string strLowOrHigh = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//public clsDRWrapper rsAccountInfo = new clsDRWrapper();
		public clsDRWrapper rsAccountInfo_AutoInitialized;

		public clsDRWrapper rsAccountInfo
		{
			get
			{
				if (rsAccountInfo_AutoInitialized == null)
				{
					rsAccountInfo_AutoInitialized = new clsDRWrapper();
				}
				return rsAccountInfo_AutoInitialized;
			}
			set
			{
				rsAccountInfo_AutoInitialized = value;
			}
		}

		public bool blnHaveData;
		string strDefaultType;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		//private clsGridAccount vsSingleGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}

		private clsGridAccount vsSingleGrid_AutoInitialized;

		private clsGridAccount vsSingleGrid
		{
			get
			{
				if (vsSingleGrid_AutoInitialized == null)
				{
					vsSingleGrid_AutoInitialized = new clsGridAccount();
				}
				return vsSingleGrid_AutoInitialized;
			}
			set
			{
				vsSingleGrid_AutoInitialized = value;
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (vsSingleAccount.EditText != "")
			{
				vsSingleAccount.TextMatrix(0, 0, vsSingleAccount.EditText);
			}
			if (vsLowAccount.EditText != "")
			{
				vsLowAccount.TextMatrix(0, 0, vsLowAccount.EditText);
			}
			if (vsHighAccount.EditText != "")
			{
				vsHighAccount.TextMatrix(0, 0, vsHighAccount.EditText);
			}
			if (cmbAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text) || !Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("Your low date must be earlier than your high date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(FCConvert.ToDateTime(txtLowDate.Text).Month), FCConvert.ToInt16(FCConvert.ToDateTime(txtHighDate.Text).Month)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbAll.SelectedIndex == 2)
			{
				if (cboLowMonth.SelectedIndex == -1 || cboHighMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of accounting months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboLowMonth.SelectedIndex + 1), FCConvert.ToInt16(cboHighMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 2)
			{
				if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid beginning account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsLowAccount.Focus();
					return;
				}
				if (Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid ending account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsHighAccount.Focus();
					return;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 1)
			{
				if (Strings.InStr(1, vsSingleAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsSingleAccount.Focus();
					return;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 1)
			{
				if (cmbLedger.SelectedIndex == 0)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' ORDER BY Account");
				}
				else if (cmbLedger.SelectedIndex == 1)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 2)
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account >= '" + vsLowAccount.TextMatrix(0, 0) + "' AND Account <= '" + vsHighAccount.TextMatrix(0, 0) + "' ORDER BY Account");
			}
			else if (cmbSingleAccount.SelectedIndex == 1)
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vsSingleAccount.TextMatrix(0, 0) + "' ORDER BY Account");
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") = '" + Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
				}
				else if (cmbLedger.SelectedIndex == 1)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
				}
			}
			else
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") >= '" + Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") <= '" + Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
				}
				else if (cmbLedger.SelectedIndex == 1)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
				}
				else
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
				}
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No accounts found that match the criteria you specified", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// If Not optSingleAccount Then
			// MsgBox "Use the F7 key to view the previous account and F8 to view the next account out of the range you selected to view.", vbInformation, "F7"
			// End If
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCurrentAccountStatusHolder.InstancePtr);
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmCurrentAccountStatus_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vsSingleAccount.Select(0, 0);
			vsSingleAccount.Focus();
			this.Refresh();
		}

		private void frmCurrentAccountStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
				}
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSSINGLEACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsSingleAccount, vsSingleAccount.Row, vsSingleAccount.Col, KeyCode, Shift, vsSingleAccount.EditSelStart, vsSingleAccount.EditText, vsSingleAccount.EditSelLength);
				}
			}
		}

		private void frmCurrentAccountStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCurrentAccountStatus.FillStyle	= 0;
			//frmCurrentAccountStatus.ScaleWidth	= 9300;
			//frmCurrentAccountStatus.ScaleHeight	= 6930;
			//frmCurrentAccountStatus.LinkTopic	= "Form2";
			//frmCurrentAccountStatus.LockControls	= -1  'True;
			//frmCurrentAccountStatus.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rs = new clsDRWrapper();
			int counter = 0;
			vsLowGrid.GRID7Light = vsLowAccount;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsSingleGrid.GRID7Light = vsSingleAccount;
			//FC:FINAL:MSH - Issue #545: add handlers, which will add to FCGrid.EditingControl handlers for 'TextChanged' event.
			vsLowGrid.GRID7Light.EditingControlShowing += GRID7Light_EditingControlShowing;
			vsHighGrid.GRID7Light.EditingControlShowing += GRID7Light_EditingControlShowing;
			vsSingleGrid.GRID7Light.EditingControlShowing += GRID7Light_EditingControlShowing;
			vsLowGrid.DefaultAccountType = "E";
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.DefaultAccountType = "E";
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsSingleGrid.DefaultAccountType = "E";
			vsSingleGrid.AccountCol = -1;
			strDefaultType = "E";
			//cboBeginningDept.Left = FCConvert.ToInt32(cboBeginningDept.Left + cboBeginningDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
			//cboSingleDept.Left = FCConvert.ToInt32(cboSingleDept.Left + (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboBeginningDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboEndingDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			//cboBeginningFund.Left = cboBeginningFund.Left + cboBeginningFund.Width - (3 * 115 + 350);
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (3 * 115 + 350)) - (0.5 * (cboSingleFund.Width - (3 * 115 + 350))));
			//cboBeginningFund.Width = 3 * 115 + 350;
			//cboEndingFund.Width = 3 * 115 + 350;
			//cboSingleFund.Width = 3 * 115 + 350;
			FillTownFunds();
			cmbTown.Enabled = true;
			cmbTown.SelectedIndex = 0;
			blnHaveData = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}
		//FC:FINAL:MSH - Issue #545: add 'TextChanged' handler to EditingControl when the last one is show
		private void GRID7Light_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				e.Control.TextChanged -= vsSingleAccountEditingControl_TextChanged;
				e.Control.TextChanged += vsSingleAccountEditingControl_TextChanged;
			}
		}
		//FC:FINAL:MSH - Issue #545: Change AccountType, when account type changed in Account fields.
		private void vsSingleAccountEditingControl_TextChanged(object sender, EventArgs e)
		{
			var t_Box = sender as TextBox;
			if (!String.IsNullOrWhiteSpace(t_Box.Text))
			{
				string symbol = t_Box.Text.Split(new string[] {
					" "
				}, StringSplitOptions.RemoveEmptyEntries)[0].Trim().ToUpper();
				switch (symbol)
				{
					case "E":
						{
							cmbLedger.SelectedIndex = 0;
							break;
						}
					case "G":
						{
							cmbLedger.SelectedIndex = 2;
							break;
						}
					case "R":
						{
							cmbLedger.SelectedIndex = 1;
							break;
						}
					default:
						{
							break;
						}
				}
			}
		}

		private void frmCurrentAccountStatus_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (vsSingleAccount.EditText != "")
			{
				vsSingleAccount.TextMatrix(0, 0, vsSingleAccount.EditText);
			}
			if (vsLowAccount.EditText != "")
			{
				vsLowAccount.TextMatrix(0, 0, vsLowAccount.EditText);
			}
			if (vsHighAccount.EditText != "")
			{
				vsHighAccount.TextMatrix(0, 0, vsHighAccount.EditText);
			}
			if (cmbAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text) || !Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("Your low date must be earlier than your high date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(FCConvert.ToDateTime(txtLowDate.Text).Month), FCConvert.ToInt16(FCConvert.ToDateTime(txtHighDate.Text).Month)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbAll.SelectedIndex == 2)
			{
				if (cboLowMonth.SelectedIndex == -1 || cboHighMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of accounting months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboLowMonth.SelectedIndex + 1), FCConvert.ToInt16(cboHighMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 2)
			{
				if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
				{
					MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
				{
					MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 0)
			{
				if (Strings.InStr(1, vsSingleAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsSingleAccount.Focus();
					return;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboSingleFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Fund you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (cboSingleDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 4)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboBeginningFund.SelectedIndex == -1 || cboEndingFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Funds you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningFund.SelectedIndex > cboEndingFund.SelectedIndex)
					{
						MessageBox.Show("Your beginning Fund must be lower then your ending Fund", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (cboBeginningDept.SelectedIndex == -1 || cboEndingDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningDept.SelectedIndex > cboEndingDept.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (cmbTown.SelectedIndex == 0)
			{
				if (cmbSingleAccount.SelectedIndex == 0)
				{
					if (cmbLedger.SelectedIndex == 0)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
					}
				}
				else if (cmbSingleAccount.SelectedIndex == 2)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account >= '" + vsLowAccount.TextMatrix(0, 0) + "' AND Account <= '" + vsHighAccount.TextMatrix(0, 0) + "' ORDER BY Account");
				}
				else if (cmbSingleAccount.SelectedIndex == 1)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vsSingleAccount.TextMatrix(0, 0) + "' ORDER BY Account");
				}
				else if (cmbSingleAccount.SelectedIndex == 3)
				{
					if (cmbLedger.SelectedIndex == 2)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") = '" + Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
				}
				else
				{
					if (cmbLedger.SelectedIndex == 2)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") >= '" + Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") <= '" + Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
				}
			}
			else
			{
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No accounts found that match the criteria you specified", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCurrentAccountStatusHolder.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (vsSingleAccount.EditText != "")
			{
				vsSingleAccount.TextMatrix(0, 0, vsSingleAccount.EditText);
			}
			if (vsLowAccount.EditText != "")
			{
				vsLowAccount.TextMatrix(0, 0, vsLowAccount.EditText);
			}
			if (vsHighAccount.EditText != "")
			{
				vsHighAccount.TextMatrix(0, 0, vsHighAccount.EditText);
			}
			if (cmbAll.SelectedIndex == 1)
			{
				if (!Information.IsDate(txtLowDate.Text) || !Information.IsDate(txtHighDate.Text))
				{
					MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
				{
					MessageBox.Show("Your low date must be earlier than your high date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(FCConvert.ToDateTime(txtLowDate.Text).Month), FCConvert.ToInt16(FCConvert.ToDateTime(txtHighDate.Text).Month)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbAll.SelectedIndex == 2)
			{
				if (cboLowMonth.SelectedIndex == -1 || cboHighMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of accounting months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboLowMonth.SelectedIndex + 1), FCConvert.ToInt16(cboHighMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbSingleAccount.SelectedIndex == 2)
			{
				if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
				{
					MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
				{
					MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 1)
			{
				if (Strings.InStr(1, vsSingleAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("You must enter a valid account before you may continue", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsSingleAccount.Focus();
					return;
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboSingleFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Fund you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (cboSingleDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 4)
			{
				if (cmbLedger.SelectedIndex == 2)
				{
					if (cboBeginningFund.SelectedIndex == -1 || cboEndingFund.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Funds you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningFund.SelectedIndex > cboEndingFund.SelectedIndex)
					{
						MessageBox.Show("Your beginning Fund must be lower then your ending Fund", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (cboBeginningDept.SelectedIndex == -1 || cboEndingDept.SelectedIndex == -1)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningDept.SelectedIndex > cboEndingDept.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			if (cmbTown.SelectedIndex == 0)
			{
				if (cmbSingleAccount.SelectedIndex == 0)
				{
					if (cmbLedger.SelectedIndex == 0)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'G' ORDER BY Account");
					}
				}
				else if (cmbSingleAccount.SelectedIndex == 2)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account >= '" + vsLowAccount.TextMatrix(0, 0) + "' AND Account <= '" + vsHighAccount.TextMatrix(0, 0) + "' ORDER BY Account");
				}
				else if (cmbSingleAccount.SelectedIndex == 1)
				{
					rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + vsSingleAccount.TextMatrix(0, 0) + "' ORDER BY Account");
				}
				else if (cmbSingleAccount.SelectedIndex == 3)
				{
					if (cmbLedger.SelectedIndex == 2)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") = '" + Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
				}
				else
				{
					if (cmbLedger.SelectedIndex == 2)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'G' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") >= '" + Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") <= '" + Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "' ORDER BY Account");
					}
					else if (cmbLedger.SelectedIndex == 1)
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'R' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE Left(Account, 1) = 'E' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) + "' ORDER BY Account");
					}
				}
			}
			else
			{
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No accounts found that match the criteria you specified", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			//rptCurrentAccountStatusHolder.InstancePtr.Hide();
			modDuplexPrinting.DuplexPrintReport(rptCurrentAccountStatusHolder.InstancePtr);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.SelectedIndex == 0)
			{
				fraMonthRange.Enabled = false;
				fraDateRange.Enabled = false;
				txtLowDate.Text = "";
				txtHighDate.Text = "";
				cboLowMonth.SelectedIndex = -1;
				cboHighMonth.SelectedIndex = -1;
			}
			else if (cmbAll.SelectedIndex == 1)
			{
				fraDateRange.Enabled = true;
				fraMonthRange.Enabled = false;
				cboLowMonth.SelectedIndex = -1;
				cboHighMonth.SelectedIndex = -1;
			}
			else if (cmbAll.SelectedIndex == 2)
			{
				fraMonthRange.Enabled = true;
				fraDateRange.Enabled = false;
				txtLowDate.Text = "";
				txtHighDate.Text = "";
			}
		}

		private void optExpense_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbTown.SelectedIndex == 0)
			{
				//optSingleDepartment.Text = "Single Department";
				if (!cmbSingleAccount.Items.Contains("Single Department"))
				{
					cmbSingleAccount.Items.Insert(3, "Single Department");
				}
				int index = cmbSingleAccount.Items.IndexOf("Single Fund");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				//optDepartmentRange.Text = "Department Range";
				if (!cmbSingleAccount.Items.Contains("Department Range"))
				{
					cmbSingleAccount.Items.Insert(4, "Department Range");
				}
				index = cmbSingleAccount.Items.IndexOf("Fund Range");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				cmbSingleAccount.SelectedIndex = 1;
				strDefaultType = "E";
				vsLowGrid.DefaultAccountType = "E";
				vsHighGrid.DefaultAccountType = "E";
				vsSingleGrid.DefaultAccountType = "E";
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Focus();
				//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
				//Support.SendKeys("{E}", false);
				modNewAccountBox.Statics.strAccount = "E";
				vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.ESegmentSize);
				blnHaveData = false;
			}
			else
			{
				//optSingleDepartment.Text = "Single Fund";
				if (!cmbSingleAccount.Items.Contains("Single Fund"))
				{
					cmbSingleAccount.Items.Insert(3, "Single Fund");
				}
				int index = cmbSingleAccount.Items.IndexOf("Single Department");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				//optDepartmentRange.Text = "Fund Range";
				if (!cmbSingleAccount.Items.Contains("Fund Range"))
				{
					cmbSingleAccount.Items.Insert(4, "Fund Range");
				}
				index = cmbSingleAccount.Items.IndexOf("Department Range");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				cmbSingleAccount.SelectedIndex = 1;
				strDefaultType = "P";
				vsLowGrid.DefaultAccountType = "P";
				vsHighGrid.DefaultAccountType = "P";
				vsSingleGrid.DefaultAccountType = "P";
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				if (vsSingleAccount.Visible)
				{
					vsSingleAccount.Focus();
				}
				//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
				//Support.SendKeys("{P}", false);
				modNewAccountBox.Statics.strAccount = "P";
				vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.PSegmentSize);
				blnHaveData = false;
			}
		}

		public void optExpense_Click()
		{
			optExpense_CheckedChanged(cmbLedger, new System.EventArgs());
		}

		private void optLedger_CheckedChanged(object sender, System.EventArgs e)
		{
			if (this.cmbLedger.SelectedIndex == 2)
			{
				if (cmbTown.SelectedIndex == 0)
				{
					//optSingleDepartment.Text = "Single Fund";
					if (!cmbSingleAccount.Items.Contains("Single Fund"))
					{
						cmbSingleAccount.Items.Insert(3, "Single Fund");
					}
					int index = cmbSingleAccount.Items.IndexOf("Single Department");
					if (index != -1)
					{
						cmbSingleAccount.Items.RemoveAt(index);
					}
					//optDepartmentRange.Text = "Fund Range";
					if (!cmbSingleAccount.Items.Contains("Fund Range"))
					{
						cmbSingleAccount.Items.Insert(4, "Fund Range");
					}
					index = cmbSingleAccount.Items.IndexOf("Department Range");
					if (index != -1)
					{
						cmbSingleAccount.Items.RemoveAt(index);
					}
					cmbSingleAccount.SelectedIndex = 1;
					strDefaultType = "G";
					vsLowGrid.DefaultAccountType = "G";
					vsHighGrid.DefaultAccountType = "G";
					vsSingleGrid.DefaultAccountType = "G";
					vsLowAccount.TextMatrix(0, 0, "");
					vsHighAccount.TextMatrix(0, 0, "");
					vsSingleAccount.TextMatrix(0, 0, "");
					vsSingleAccount.Focus();
					//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
					//Support.SendKeys("{G}", false);
					modNewAccountBox.Statics.strAccount = "G";
					vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.GSegmentSize);
					blnHaveData = false;
				}
				else
				{
					//optSingleDepartment.Text = "Single Fund";
					if (!cmbSingleAccount.Items.Contains("Single Fund"))
					{
						cmbSingleAccount.Items.Insert(3, "Single Fund");
					}
					int index = cmbSingleAccount.Items.IndexOf("Single Department");
					if (index != -1)
					{
						cmbSingleAccount.Items.RemoveAt(index);
					}
					//optDepartmentRange.Text = "Fund Range";
					if (!cmbSingleAccount.Items.Contains("Fund Range"))
					{
						cmbSingleAccount.Items.Insert(4, "Fund Range");
					}
					index = cmbSingleAccount.Items.IndexOf("Department Range");
					if (index != -1)
					{
						cmbSingleAccount.Items.RemoveAt(index);
					}
					cmbSingleAccount.SelectedIndex = 1;
					strDefaultType = "L";
					vsLowGrid.DefaultAccountType = "L";
					vsHighGrid.DefaultAccountType = "L";
					vsSingleGrid.DefaultAccountType = "L";
					vsLowAccount.TextMatrix(0, 0, "");
					vsHighAccount.TextMatrix(0, 0, "");
					vsSingleAccount.TextMatrix(0, 0, "");
					vsSingleAccount.Focus();
					//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
					//Support.SendKeys("{L}", false);
					modNewAccountBox.Statics.strAccount = "L";
					vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.LSegmentSize);
					blnHaveData = false;
				}
			}
			else if (this.cmbLedger.SelectedIndex == 1)
			{
				optRevenue_Click();
			}
			else if (this.cmbLedger.SelectedIndex == 0)
			{
				optExpense_Click();
			}
		}

		public void optLedger_Click()
		{
			optLedger_CheckedChanged(cmbLedger, new System.EventArgs());
		}

		private void optRevenue_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbTown.SelectedIndex == 0)
			{
				//optSingleDepartment.Text = "Single Department";
				if (!cmbSingleAccount.Items.Contains("Single Department"))
				{
					cmbSingleAccount.Items.Insert(3, "Single Department");
				}
				int index = cmbSingleAccount.Items.IndexOf("Single Fund");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				//optDepartmentRange.Text = "Department Range";
				if (!cmbSingleAccount.Items.Contains("Department Range"))
				{
					cmbSingleAccount.Items.Insert(4, "Department Range");
				}
				index = cmbSingleAccount.Items.IndexOf("Fund Range");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				cmbSingleAccount.SelectedIndex = 1;
				strDefaultType = "R";
				vsLowGrid.DefaultAccountType = "R";
				vsHighGrid.DefaultAccountType = "R";
				vsSingleGrid.DefaultAccountType = "R";
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Focus();
				//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
				//Support.SendKeys("{R}", false);
				modNewAccountBox.Statics.strAccount = "R";
				vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.RSegmentSize);
				blnHaveData = false;
			}
			else
			{
				//optSingleDepartment.Text = "Single Fund";
				if (!cmbSingleAccount.Items.Contains("Single Fund"))
				{
					cmbSingleAccount.Items.Insert(3, "Single Fund");
				}
				int index = cmbSingleAccount.Items.IndexOf("Single Department");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				//optDepartmentRange.Text = "Fund Range";
				if (!cmbSingleAccount.Items.Contains("Fund Range"))
				{
					cmbSingleAccount.Items.Insert(4, "Fund Range");
				}
				index = cmbSingleAccount.Items.IndexOf("Department Range");
				if (index != -1)
				{
					cmbSingleAccount.Items.RemoveAt(index);
				}
				cmbSingleAccount.SelectedIndex = 1;
				strDefaultType = "V";
				vsLowGrid.DefaultAccountType = "V";
				vsHighGrid.DefaultAccountType = "V";
				vsSingleGrid.DefaultAccountType = "V";
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Focus();
				//FC:FINAL:AM:#i764 - SendKeys is not supported on the web
				//Support.SendKeys("{V}", false);
				modNewAccountBox.Statics.strAccount = "V";
				vsSingleAccount.EditText = modNewAccountBox.AccountFormatSetup_2(modNewAccountBox.Statics.VSegmentSize);
				blnHaveData = false;
			}
		}

		public void optRevenue_Click()
		{
			optRevenue_CheckedChanged(cmbLedger, new System.EventArgs());
		}

		private void optSingleAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbSingleAccount.SelectedIndex == 1)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsSingleAccount.Visible = true;
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				vsSingleAccount.Focus();
			}
			else if (cmbSingleAccount.SelectedIndex == 0)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
			}
			else if (cmbSingleAccount.SelectedIndex == 2)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = true;
				vsHighAccount.Visible = true;
				lblTo[1].Visible = true;
				vsLowAccount.Focus();
			}
			else if (cmbSingleAccount.SelectedIndex == 3)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				lblTo[2].Visible = false;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				if (cmbLedger.SelectedIndex == 2)
				{
					FillTownFunds();
					cboSingleFund.Visible = true;
					cboSingleFund.Focus();
				}
				else
				{
					cboSingleDept.Visible = true;
					cboSingleDept.Focus();
				}
			}
			else if (cmbSingleAccount.SelectedIndex == 4)
			{
				lblTo[2].Visible = true;
				cboSingleDept.Visible = false;
				cboSingleFund.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsSingleAccount.TextMatrix(0, 0, "");
				vsSingleAccount.Visible = false;
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				if (cmbLedger.SelectedIndex == 2)
				{
					FillTownFunds();
					cboBeginningFund.Visible = true;
					cboEndingFund.Visible = true;
					cboBeginningFund.Focus();
				}
				else
				{
					cboBeginningDept.Visible = true;
					cboEndingDept.Visible = true;
					cboBeginningDept.Focus();
				}
			}
		}

		private void SetCustomFormColors()
		{
			//HeaderText .ForeColor = Color.Blue;
			//lblTitle.ForeColor = Color.Blue;
		}

		private void cboBeginningDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboBeginningFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void optTown_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbLedger.SelectedIndex == 0)
			{
				vsLowGrid.DefaultAccountType = "E";
				vsHighGrid.DefaultAccountType = "E";
				vsSingleGrid.DefaultAccountType = "E";
				strDefaultType = "E";
				optExpense_Click();
			}
			else if (cmbLedger.SelectedIndex == 1)
			{
				vsLowGrid.DefaultAccountType = "R";
				vsHighGrid.DefaultAccountType = "R";
				vsSingleGrid.DefaultAccountType = "R";
				strDefaultType = "R";
				optRevenue_Click();
			}
			else
			{
				vsLowGrid.DefaultAccountType = "G";
				vsHighGrid.DefaultAccountType = "G";
				vsSingleGrid.DefaultAccountType = "G";
				strDefaultType = "G";
				optLedger_Click();
			}
		}

		private void vsHighAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsHighAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsHighAccount.TextMatrix(0, 0), ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsHighAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsHighAccount.EditSelStart == 0)
			{
				if (cmbLedger.SelectedIndex == 0)
				{
					if (cmbTown.SelectedIndex == 0)
					{
						if (keyAscii == 71 || keyAscii == 103 || keyAscii == 82 || keyAscii == 114 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
						{
							keyAscii = 0;
						}
					}
					else
					{
						if (keyAscii == 71 || keyAscii == 103 || keyAscii == 82 || keyAscii == 114 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 69 || keyAscii == 101 || keyAscii == 86 || keyAscii == 118)
						{
							keyAscii = 0;
						}
					}
				}
				else if (cmbLedger.SelectedIndex == 1)
				{
					if (cmbTown.SelectedIndex == 0)
					{
						if (keyAscii == 71 || keyAscii == 103 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
						{
							keyAscii = 0;
						}
					}
					else
					{
						if (keyAscii == 71 || keyAscii == 103 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 82 || keyAscii == 114)
						{
							keyAscii = 0;
						}
					}
				}
				else
				{
					if (cmbTown.SelectedIndex == 0)
					{
						if (keyAscii == 82 || keyAscii == 114 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 76 || keyAscii == 108 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
						{
							keyAscii = 0;
						}
					}
					else
					{
						if (keyAscii == 82 || keyAscii == 114 || keyAscii == 69 || keyAscii == 101 || keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109 || keyAscii == 71 || keyAscii == 103 || keyAscii == 80 || keyAscii == 112 || keyAscii == 86 || keyAscii == 118)
						{
							keyAscii = 0;
						}
					}
				}
			}
		}

		private void vsLowAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsLowAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsLowAccount.TextMatrix(0, 0), ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsLowAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsLowAccount.EditSelStart == 0)
			{
				if (keyAscii == 71 || keyAscii == 103)
				{
					if (cmbTown.Enabled)
					{
						cmbTown.SelectedIndex = 0;
						cmbLedger.SelectedIndex = 2;
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (keyAscii == 69 || keyAscii == 101)
				{
					cmbTown.SelectedIndex = 0;
					cmbLedger.SelectedIndex = 0;
				}
				else if (keyAscii == 82 || keyAscii == 114)
				{
					if (cmbTown.Enabled)
					{
						cmbTown.SelectedIndex = 0;
						cmbLedger.SelectedIndex = 1;
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (keyAscii == 76 || keyAscii == 108)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 80 || keyAscii == 112)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 86 || keyAscii == 118)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109)
				{
					keyAscii = 0;
				}
			}
		}

		private void vsSingleAccount_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vsSingleAccount.IsCurrentCellInEditMode)
			{
				modAccountTitle.DetermineAccountTitle(vsSingleAccount.EditText, ref lblTitle);
				HeaderText.Text = (lblTitle.Text != string.Empty) ? lblTitle : HeaderText.Text;
			}
		}

		private void vsSingleAccount_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 27)
			{
				keyAscii = 0;
				Close();
			}
			if (vsSingleAccount.EditSelStart == 0)
			{
				if (keyAscii == 71 || keyAscii == 103)
				{
					cmbTown.SelectedIndex = 0;
					cmbLedger.SelectedIndex = 2;
				}
				else if (keyAscii == 69 || keyAscii == 101)
				{
					if (cmbTown.Enabled)
					{
						cmbTown.SelectedIndex = 0;
						cmbLedger.SelectedIndex = 0;
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (keyAscii == 82 || keyAscii == 114)
				{
					if (cmbTown.Enabled)
					{
						cmbTown.SelectedIndex = 0;
						cmbLedger.SelectedIndex = 1;
					}
					else
					{
						keyAscii = 0;
					}
				}
				else if (keyAscii == 76 || keyAscii == 108)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 80 || keyAscii == 112)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 86 || keyAscii == 118)
				{
					keyAscii = 0;
				}
				else if (keyAscii == 65 || keyAscii == 97 || keyAscii == 77 || keyAscii == 109)
				{
					keyAscii = 0;
				}
			}
		}

		private void FillTownFunds()
		{
			clsDRWrapper rs = new clsDRWrapper();
			int counter = 0;
			cboBeginningFund.Clear();
			cboEndingFund.Clear();
			cboSingleFund.Clear();
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboBeginningFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboEndingFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
		}
	}
}
