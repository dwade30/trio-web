﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournalsTotals.
	/// </summary>
	public partial class rptJournalTotals : BaseSectionReport
	{
		public rptJournalTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "rptJournalTotals";
		}

		public static rptJournalTotals InstancePtr
		{
			get
			{
				return (rptJournalTotals)Sys.GetInstance(typeof(rptJournalTotals));
			}
		}

		protected rptJournalTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsFundInfo.Dispose();
				rsExpInfo.Dispose();
				rsRevInfo.Dispose();
				rsGLInfo.Dispose();
				rsCashInfo.Dispose();
				rsEncInfo.Dispose();
				rsDueFromInfo.Dispose();
				rsDueToInfo.Dispose();
				rsGeneralInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptJournalTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsFundInfo = new clsDRWrapper();
		clsDRWrapper rsExpInfo = new clsDRWrapper();
		clsDRWrapper rsRevInfo = new clsDRWrapper();
		clsDRWrapper rsGLInfo = new clsDRWrapper();
		clsDRWrapper rsCashInfo = new clsDRWrapper();
		clsDRWrapper rsEncInfo = new clsDRWrapper();
		clsDRWrapper rsDueFromInfo = new clsDRWrapper();
		clsDRWrapper rsDueToInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		string strType = "";
		// vbPorter upgrade warning: curTotalExp As Decimal	OnWrite(Decimal, short)
		Decimal curTotalExp;
		// vbPorter upgrade warning: curTotalRev As Decimal	OnWrite(Decimal, short)
		Decimal curTotalRev;
		// vbPorter upgrade warning: curTotalGL As Decimal	OnWrite(Decimal, short)
		Decimal curTotalGL;
		// vbPorter upgrade warning: curTotalCash As Decimal	OnWrite(Decimal, short)
		Decimal curTotalCash;
		// vbPorter upgrade warning: curTotalDueTo As Decimal	OnWrite(Decimal, short)
		Decimal curTotalDueTo;
		// vbPorter upgrade warning: curTotalDueFrom As Decimal	OnWrite(Decimal, short)
		Decimal curTotalDueFrom;
		// vbPorter upgrade warning: curTotalEnc As Decimal	OnWrite(Decimal, short)
		Decimal curTotalEnc;
		string MiscAccount = "";
		string PayableAccount = "";
		string PayrollAccount = "";
		string DueToAccount = "";
		string DueFromAccount = "";
		string EncAccount = "";
		string ExpAccount = "";
		string RevAccount = "";
		string strGLSQL = "";
		string strCashSQL = "";
		string strEncSQL = "";
		string strDueToSQL = "";
		string strDueFromSQL = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strSQL;
			string strTest = "";
			string strJournalSQL = "";
			CheckNext:
			;
			eArgs.EOF = rsFundInfo.EndOfFile();
			if (eArgs.EOF)
			{
				return;
			}
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			strSQL = "SELECT Department FROM DeptDivTitles WHERE Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "' AND Fund = '" + rsFundInfo.Get_Fields("Fund") + "'";
			if (strType == "EN")
			{
				strJournalSQL = "SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2);
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'E' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'R' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strGLSQL);
				if (strCashSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strCashSQL);
				}
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strEncSQL);
				}
				if (strDueToSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueToSQL);
				}
				if (strDueFromSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueFromSQL);
				}
			}
			else if (strType == "AP" || strType == "AC")
			{
				if (strType == "AP")
				{
					strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'P' AND CheckDate = '" + frmPosting.InstancePtr.vsCheckDates.TextMatrix(rptPosting.InstancePtr.intRowHolder, 0) + "' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2);
				}
				else
				{
					strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2);
				}
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal, SUM(Encumbrance) as EncumbranceTotal FROM APJournalDetail WHERE Encumbrance <> (Amount - Discount) AND left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strGLSQL);
				if (strCashSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strCashSQL);
				}
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strEncSQL);
				}
				if (strDueToSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueToSQL);
				}
				if (strDueFromSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueFromSQL);
				}
			}
			else
			{
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE (Type <> 'P' OR RCB <> 'E') AND left(Account, 1) = 'E' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'R' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")");
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), '" + rsFundInfo.Get_Fields("Fund") + "'" + strGLSQL);
				if (strCashSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), '" + rsFundInfo.Get_Fields("Fund") + "'" + strCashSQL);
				}
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), '" + rsFundInfo.Get_Fields("Fund") + "'" + strEncSQL);
				}
				if (strDueToSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueToSQL);
				}
				if (strDueFromSQL != "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2) + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), '" + rsFundInfo.Get_Fields("Fund") + "'" + strDueFromSQL);
				}
			}
			// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
			if (FCConvert.ToInt32(rsExpInfo.Get_Fields("EntryCount")) == 0 && FCConvert.ToInt32(rsRevInfo.Get_Fields("EntryCount")) == 0 && FCConvert.ToInt32(rsGLInfo.Get_Fields("EntryCount")) == 0 && FCConvert.ToInt32(rsCashInfo.Get_Fields("EntryCount")) == 0)
			{
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
					if (FCConvert.ToInt32(rsEncInfo.Get_Fields("EntryCount")) == 0)
					{
						goto CheckDueToFrom;
					}
					else
					{
						goto InfoFound;
					}
				}
				CheckDueToFrom:
				;
				if (strDueToSQL != "")
				{
					// if this is not empty then the records sets are filled and need to be tested
					// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [EntryCount] not found!! (maybe it is an alias?)
					if (FCConvert.ToInt32(rsDueToInfo.Get_Fields("EntryCount")) == 0 && FCConvert.ToInt32(rsDueFromInfo.Get_Fields("EntryCount")) == 0)
					{
						rsFundInfo.MoveNext();
						goto CheckNext;
					}
					else
					{
						// do nothing
					}
				}
				else
				{
					rsFundInfo.MoveNext();
					goto CheckNext;
				}
			}
			InfoFound:
			;
			// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
			Field1.Text = FCConvert.ToString(rsFundInfo.Get_Fields("Fund"));
			if (strType == "AP" || strType == "AC")
			{
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
				curTotalExp += FCConvert.ToDecimal(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsExpInfo.Get_Fields("DiscountTotal")) - Conversion.Val(rsExpInfo.Get_Fields("EncumbranceTotal")));
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				curTotalRev += FCConvert.ToDecimal(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsRevInfo.Get_Fields("DiscountTotal")));
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				curTotalGL += FCConvert.ToDecimal(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsGLInfo.Get_Fields("DiscountTotal")));
				if (strCashSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalCash += FCConvert.ToDecimal(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsCashInfo.Get_Fields("DiscountTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field8.Text = Strings.Format(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsCashInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field8.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueToSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalDueTo += FCConvert.ToDecimal(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueToInfo.Get_Fields("DiscountTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field9.Text = Strings.Format(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueToInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field9.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueFromSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalDueFrom += FCConvert.ToDecimal(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueFromInfo.Get_Fields("DiscountTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field10.Text = Strings.Format(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueFromInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field10.Text = Strings.Format(0, "#,##0.00");
				}
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalEnc += FCConvert.ToDecimal(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsEncInfo.Get_Fields("DiscountTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field11.Text = Strings.Format(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsEncInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field11.Text = Strings.Format(0, "#,##0.00");
				}
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
				Field2.Text = Strings.Format(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsExpInfo.Get_Fields("DiscountTotal")) - Conversion.Val(rsExpInfo.Get_Fields("EncumbranceTotal")), "#,##0.00");
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				Field3.Text = Strings.Format(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsRevInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO Get_Fields: Field [DiscountTotal] not found!! (maybe it is an alias?)
				Field4.Text = Strings.Format(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsGLInfo.Get_Fields("DiscountTotal")), "#,##0.00");
			}
			else
			{
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalExp += FCConvert.ToDecimal(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")));
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalRev += FCConvert.ToDecimal(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")));
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalGL += FCConvert.ToDecimal(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")));
				if (strCashSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalCash += FCConvert.ToDecimal(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field8.Text = Strings.Format(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field8.Text = Strings.Format(0, "#,##0.00");
				}
				if (strEncSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalEnc += FCConvert.ToDecimal(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field11.Text = Strings.Format(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field11.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueToSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalDueTo += FCConvert.ToDecimal(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field9.Text = Strings.Format(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field9.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueFromSQL != "")
				{
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalDueFrom += FCConvert.ToDecimal(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")));
					// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field10.Text = Strings.Format(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field10.Text = Strings.Format(0, "#,##0.00");
				}
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field2.Text = Strings.Format(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")), "#,##0.00");
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field3.Text = Strings.Format(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")), "#,##0.00");
				// TODO Get_Fields: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field4.Text = Strings.Format(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")), "#,##0.00");
			}
			rsFundInfo.MoveNext();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rptPosting.InstancePtr.PageCounter = 0;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles");
			rsGeneralInfo.OpenRecordset("SELECT * FROM StandardAccounts");
			rsGeneralInfo.FindFirstRecord("Code", "CM");
			if (rsGeneralInfo.NoMatch)
			{
				MiscAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				MiscAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "CA");
			if (rsGeneralInfo.NoMatch)
			{
				PayableAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				PayableAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "CP");
			if (rsGeneralInfo.NoMatch)
			{
				PayrollAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				PayrollAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "EC");
			if (rsGeneralInfo.NoMatch)
			{
				ExpAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				ExpAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "RC");
			if (rsGeneralInfo.NoMatch)
			{
				RevAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				RevAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "EO");
			if (rsGeneralInfo.NoMatch)
			{
				EncAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				EncAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "DT");
			if (rsGeneralInfo.NoMatch)
			{
				DueToAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				DueToAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "DF");
			if (rsGeneralInfo.NoMatch)
			{
				DueFromAccount = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				DueFromAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			strGLSQL = "";
			strCashSQL = "";
			strEncSQL = "";
			strDueToSQL = "";
			strDueFromSQL = "";
			if (Conversion.Val(MiscAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + MiscAccount + "'";
					strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + MiscAccount + "'";
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + MiscAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + MiscAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
				}
			}
			if (Conversion.Val(PayableAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayableAccount + "'";
					if (strCashSQL == "")
					{
						strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "'";
					}
					else
					{
						strCashSQL += " OR substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "'";
					}
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayableAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					if (strCashSQL == "")
					{
						strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
					else
					{
						strCashSQL += " OR (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
				}
			}
			if (Conversion.Val(PayrollAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayrollAccount + "'";
					if (strCashSQL == "")
					{
						strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "'";
					}
					else
					{
						strCashSQL += " OR substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "'";
					}
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayrollAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					if (strCashSQL == "")
					{
						strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
					else
					{
						strCashSQL += " OR (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
				}
			}
			if (Conversion.Val(EncAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + EncAccount + "'";
				strEncSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + EncAccount + "'";
			}
			if (Conversion.Val(RevAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + RevAccount + "'";
			}
			if (Conversion.Val(ExpAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + ExpAccount + "'";
			}
			if (Conversion.Val(DueToAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + DueToAccount + "'";
				strDueToSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + DueToAccount + "'";
			}
			if (Conversion.Val(DueFromAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + DueFromAccount + "'";
				strDueFromSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + DueFromAccount + "'";
			}
			if (strCashSQL != "")
			{
				strCashSQL += ")";
			}
			rsGeneralInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			strType = FCConvert.ToString(rsGeneralInfo.Get_Fields("Type"));
			if (strType == "EN")
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
			}
			else if (strType == "AP" || strType == "AC")
			{
				if (strType == "AP")
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND CheckDate = '" + frmPosting.InstancePtr.vsCheckDates.TextMatrix(rptPosting.InstancePtr.intRowHolder, 0) + "' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
				}
				else
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
				}
			}
			curTotalExp = 0;
			curTotalRev = 0;
			curTotalGL = 0;
			curTotalCash = 0;
			curTotalDueTo = 0;
			curTotalDueFrom = 0;
			curTotalEnc = 0;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (frmPosting.InstancePtr.chkBreak.CheckState == CheckState.Unchecked)
			{
				rptJournalTotals.InstancePtr.GroupFooter1.Height = 1300 / 1440F;
			}
			else
			{
				rptJournalTotals.InstancePtr.GroupFooter1.Height = 390 / 1440F;
			}
			Field5.Text = Strings.Format(curTotalExp, "#,##0.00");
			Field6.Text = Strings.Format(curTotalRev, "#,##0.00");
			Field7.Text = Strings.Format(curTotalGL, "#,##0.00");
			Field12.Text = Strings.Format(curTotalCash, "#,##0.00");
			Field13.Text = Strings.Format(curTotalDueTo, "#,##0.00");
			Field14.Text = Strings.Format(curTotalDueFrom, "#,##0.00");
			Field15.Text = Strings.Format(curTotalEnc, "#,##0.00");
		}

		private void rptJournalTotals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptJournalTotals.Icon	= "rptJournalTotals.dsx":0000";
			//rptJournalTotals.Left	= 0;
			//rptJournalTotals.Top	= 0;
			//rptJournalTotals.Width	= 11880;
			//rptJournalTotals.Height	= 8595;
			//rptJournalTotals.StartUpPosition	= 3;
			//rptJournalTotals.SectionData	= "rptJournalTotals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
