﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevertJournal.
	/// </summary>
	partial class frmRevertJournal : BaseForm
	{
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRevertJournal));
			this.cboJournals = new fecherFoundation.FCComboBox();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdDelete);
			this.BottomPanel.Location = new System.Drawing.Point(0, 254);
			this.BottomPanel.Size = new System.Drawing.Size(1076, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboJournals);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(1076, 194);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1076, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(212, 30);
			this.HeaderText.Text = "Revert AP Journal";
			// 
			// cboJournals
			// 
			this.cboJournals.AutoSize = false;
			this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
			this.cboJournals.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboJournals.FormattingEnabled = true;
			this.cboJournals.Location = new System.Drawing.Point(30, 66);
			this.cboJournals.Name = "cboJournals";
			this.cboJournals.Size = new System.Drawing.Size(221, 40);
			this.cboJournals.TabIndex = 2;
			this.cboJournals.DropDown += new System.EventHandler(this.cboJournals_DropDown);
			// 
			// cmdDelete
			// 
			this.cmdDelete.AppearanceKey = "acceptButton";
			this.cmdDelete.Location = new System.Drawing.Point(178, 36);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdDelete.Size = new System.Drawing.Size(148, 48);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Revert Journal";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(491, 16);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "SELECT THE JOURNAL YOU WOULD LIKE TO REVERT AND CLICK THE REVERT BUTTON. ";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 126);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(57, 16);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "WARNING";
			// 
			// Label2
			// 
			this.Label2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Label2.Location = new System.Drawing.Point(157, 126);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(889, 48);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "YOU MAY ONLY REVERT AN AP JOURNAL IF THE JOURNAL HAS NOT BEEN POSTED.  RUNNING TH" + "IS PROCESS MAY REQUIRE A DIFFERENT WARRANT NUMBER AND / OR CHECK NUMBERS";
			// 
			// frmRevertJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1076, 362);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmRevertJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Revert AP Journal";
			this.Load += new System.EventHandler(this.frmRevertJournal_Load);
			this.Activated += new System.EventHandler(this.frmRevertJournal_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRevertJournal_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
