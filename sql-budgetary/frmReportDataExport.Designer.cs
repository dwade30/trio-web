﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReportDataExport.
	/// </summary>
	partial class frmReportDataExport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCsv;
		public fecherFoundation.FCLabel lblCsv;
		public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtFilename;
		public fecherFoundation.FCLabel lblFileName;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReportDataExport));
			this.cmbCsv = new fecherFoundation.FCComboBox();
			this.lblCsv = new fecherFoundation.FCLabel();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtFilename = new fecherFoundation.FCTextBox();
			this.lblFileName = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.txtExtension = new fecherFoundation.FCTextBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 226);
			this.BottomPanel.Size = new System.Drawing.Size(465, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtExtension);
			this.ClientArea.Controls.Add(this.cmbCsv);
			this.ClientArea.Controls.Add(this.lblCsv);
			this.ClientArea.Controls.Add(this.cmdBrowse);
			this.ClientArea.Controls.Add(this.txtFilename);
			this.ClientArea.Controls.Add(this.lblFileName);
			this.ClientArea.Size = new System.Drawing.Size(465, 166);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(465, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(83, 30);
			this.HeaderText.Text = "Export";
			// 
			// cmbCsv
			// 
			this.cmbCsv.AutoSize = false;
			this.cmbCsv.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCsv.FormattingEnabled = true;
			this.cmbCsv.Items.AddRange(new object[] {
				"CSV",
				"Tab Delimited"
			});
			this.cmbCsv.Location = new System.Drawing.Point(155, 126);
			this.cmbCsv.Name = "cmbCsv";
			this.cmbCsv.Size = new System.Drawing.Size(150, 40);
			this.cmbCsv.TabIndex = 7;
			this.cmbCsv.SelectedIndexChanged += new System.EventHandler(this.CmbCsv_SelectedIndexChanged);
			// 
			// lblCsv
			// 
			this.lblCsv.Location = new System.Drawing.Point(30, 140);
			this.lblCsv.Name = "lblCsv";
			this.lblCsv.Size = new System.Drawing.Size(55, 16);
			this.lblCsv.TabIndex = 8;
			this.lblCsv.Text = "FORMAT";
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(201, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(67, 48);
			this.cmdSave.TabIndex = 5;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// cmdBrowse
			// 
			this.cmdBrowse.AppearanceKey = "actionButton";
			this.cmdBrowse.Location = new System.Drawing.Point(339, 119);
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Size = new System.Drawing.Size(75, 40);
			this.cmdBrowse.TabIndex = 1;
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.Visible = false;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			// 
			// txtFilename
			// 
			this.txtFilename.AutoSize = false;
			this.txtFilename.BackColor = System.Drawing.SystemColors.Window;
			this.txtFilename.LinkItem = null;
			this.txtFilename.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtFilename.LinkTopic = null;
			this.txtFilename.Location = new System.Drawing.Point(30, 66);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(322, 40);
			this.txtFilename.TabIndex = 0;
			this.txtFilename.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// lblFileName
			// 
			this.lblFileName.Location = new System.Drawing.Point(30, 30);
			this.lblFileName.Name = "lblFileName";
			this.lblFileName.Size = new System.Drawing.Size(73, 16);
			this.lblFileName.TabIndex = 7;
			this.lblFileName.Text = "FILENAME";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Exit";
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// txtExtension
			// 
			this.txtExtension.AutoSize = false;
			this.txtExtension.Enabled = false;
			this.txtExtension.LinkItem = null;
			this.txtExtension.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtExtension.LinkTopic = null;
			this.txtExtension.Location = new System.Drawing.Point(352, 66);
			this.txtExtension.LockedOriginal = true;
			this.txtExtension.Name = "txtExtension";
			this.txtExtension.ReadOnly = true;
			this.txtExtension.Size = new System.Drawing.Size(79, 40);
			this.txtExtension.TabIndex = 9;
			// 
			// frmReportDataExport
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(465, 334);
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmReportDataExport";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Export";
			this.Load += new System.EventHandler(this.frmReportDataExport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReportDataExport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCTextBox txtExtension;
	}
}
