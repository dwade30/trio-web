﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCDDataEntry.
	/// </summary>
	public partial class frmCDDataEntry : BaseForm
	{
		private Control lastActiveControl = null;

		public frmCDDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCDDataEntry InstancePtr
		{
			get
			{
				return (frmCDDataEntry)Sys.GetInstance(typeof(frmCDDataEntry));
			}
		}

		protected frmCDDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int DateCol;
		int DescriptionCol;
		int VendorCol;
		int WarrantCol;
		// column variables
		int CheckCol;
		int TaxCol;
		int AccountCol;
		int ProjCol;
		int AmountCol;
		double TotalAmount;
		public int ChosenJournal;
		// vbPorter upgrade warning: ChosenPeriod As short --> As int	OnWrite(string, int)
		public int ChosenPeriod;
		string ErrorString = "";
		// Error message to show if account is invalid
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		bool BadAccountFlag;
		string strEncOffAccount = "";
		string strRevCtrAccount = "";
		string strExpCtrAccount = "";
		bool blnUnload;
		bool blnDirty;
		bool blnJournalLocked;
		string strComboList;
		clsGridAccount vsGrid = new clsGridAccount();
		int intBank;
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;
		bool openFormWhenThisClosed = false;

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private void frmCDDataEntry_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			txtPeriod.Focus();
			txtPeriod.SelectionStart = 0;
			txtPeriod.SelectionLength = txtPeriod.Text.Length;
			this.vs1.EditingControlShowing -= Vs1_EditingControlShowing;
			this.vs1.EditingControlShowing += Vs1_EditingControlShowing;
			this.Refresh();
		}
		//FC:FINAL:MSH - Issue #436: add handlers if edit control of vs1 table is activated
		private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - Issue #436: in VB vs1_KeyDownEdit event is executed when key pressed in edit area
				e.Control.KeyDown -= vs1_KeyDownEdit;
				e.Control.KeyDown += vs1_KeyDownEdit;
			}
		}

		private void frmCDDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
					vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
			}
		}

		private void frmCDDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCDDataEntry.FillStyle	= 0;
			//frmCDDataEntry.ScaleWidth	= 9300;
			//frmCDDataEntry.ScaleHeight	= 7425;
			//frmCDDataEntry.LinkTopic	= "Form2";
			//frmCDDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsProjects = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			intBank = 0;
			rsInfo.OpenRecordset("SELECT * FROM Banks WHERE Name <> ''");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				cboBanks.Clear();
				if (rsInfo.RecordCount() == 1)
				{
					intBank = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
				}
				do
				{
					cboBanks.AddItem(rsInfo.Get_Fields_Int32("ID") + " - " + rsInfo.Get_Fields_String("Name"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			blnJournalLocked = false;
			NumberCol = 0;
			DateCol = 1;
			DescriptionCol = 2;
			VendorCol = 3;
			WarrantCol = 4;
			// initialize column variables
			CheckCol = 5;
			AccountCol = 6;
			TaxCol = 7;
			ProjCol = 8;
			AmountCol = 9;
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(DateCol, FCConvert.ToInt32(0.09 * vs1.WidthOriginal));
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.19 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.19 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(TaxCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(VendorCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			// format flex grids
			vs1.ColWidth(WarrantCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(CheckCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, VendorCol, "Vndr#");
			vs1.TextMatrix(0, WarrantCol, "Wrnt#");
			vs1.TextMatrix(0, CheckCol, "Check#");
			vs1.TextMatrix(0, ProjCol, "Proj");
			vs1.TextMatrix(0, TaxCol, "1099");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, AmountCol, "Debits");
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
			vs1.ColComboList(TaxCol, "D|N|1|2|3|4|5|6|7|8|9");
			// "#0;D|#1;N|#2;1|#3;2|#4;3|#5;4|#6;5|#7;6|#8;7|#9;8|#10;9"
			//FC:FINAL:DDU:#1192 - aligned grid columns
			vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(VendorCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(WarrantCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vs1.ColAlignment(TaxCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			strComboList = "# ; " + "\t" + " |";
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vs1.ColComboList(ProjCol, strComboList);
			}
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DateCol, 0, AmountCol, 4);
			//FC:FINAL:AM:#i554 - set the correct format and alignment
			//vs1.ColFormat(AmountCol, "#,###.00");
			vs1.ColFormat(AmountCol, "#,##0.00");
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColEditMask(DateCol, "0#/0#/0#");
			FillJournalCombo();
			if (!modBudgetaryMaster.Statics.blnCDEdit)
			{
				// if we are not updateing then
				if (intBank == 0)
				{
					intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank"))));
				}
				if (intBank == 0)
				{
					fraBanks.Visible = true;
					vs1.Height -= fraBanks.Height + 10;
					if (cboBanks.Items.Count > 0)
					{
						cboBanks.SelectedIndex = 0;
					}
				}
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, DescriptionCol, "");
					vs1.TextMatrix(counter, AccountCol, "");
					vs1.TextMatrix(counter, VendorCol, "");
					vs1.TextMatrix(counter, WarrantCol, "");
					vs1.TextMatrix(counter, CheckCol, "");
					vs1.TextMatrix(counter, ProjCol, "");
					vs1.TextMatrix(counter, TaxCol, "");
					vs1.TextMatrix(counter, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
					// give initial data to the grid
					vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(0));
				}
				txtPeriod.Text = CalculatePeriod();
				// calculate the period
				if (ChosenJournal == 0)
				{
					cboJournal.SelectedIndex = 0;
					// put journal number in the text box
					cboSaveJournal.SelectedIndex = 0;
				}
				else
				{
					SetCombo(ChosenJournal, ref ChosenPeriod);
					txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
				}
			}
			else
			{
				// if we are updating
				if (intBank == 0)
				{
					intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank"))));
				}
				if (intBank == 0)
				{
					rsInfo.OpenRecordset("SELECT DISTINCT BankNumber FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(ChosenJournal));
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					intBank = FCConvert.ToInt32(rsInfo.Get_Fields("BankNumber"));
					SetBankCombo(ref intBank);
					fraBanks.Visible = true;
					vs1.Height -= fraBanks.Height + 10;
				}
				SetCombo(ChosenJournal, ref ChosenPeriod);
				txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
				CalculateTotals();
				lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			blnDirty = false;
		}

		private void frmCDDataEntry_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(DateCol, FCConvert.ToInt32(0.09 * vs1.WidthOriginal));
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.19 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.19 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(TaxCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(VendorCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			// format flex grids
			vs1.ColWidth(WarrantCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			vs1.ColWidth(CheckCol, FCConvert.ToInt32(0.08 * vs1.WidthOriginal));
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				//FC:FINAL:SBE - do not change row height
				//vs1.RowHeight(counter, FCConvert.ToInt32(vs1.HeightOriginal * 0.06));
			}
			fraBanks.CenterToContainer(this.ClientArea, true, false);
			fraJournalSave.CenterToContainer(this.ClientArea);
		}

		private void Form_Closed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (openFormWhenThisClosed)
			{
				frmGetCDDataEntry.InstancePtr.Show(App.MainForm);
			}
		}

		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (blnDirty)
			{
				ans = MessageBox.Show("You have changed, but not saved some of the data on this screen.  Would you like to save now?", "Save Now?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
				if (ans == DialogResult.Yes)
				{
					e.Cancel = true;
					mnuProcessSave_Click();
					return;
				}
				else if (ans == DialogResult.No)
				{
					if (blnJournalLocked)
					{
						modBudgetaryAccounting.UnlockJournal();
					}
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					//FC:FINAL:MSH - Issue #426: showing new form before closing current form not always set focus to the new form.
					// Hiding current form before closing gives opportunity to set focus correctly
					//this.Hide();
					openFormWhenThisClosed = true;
				}
				else
				{
					e.Cancel = true;
					return;
				}
			}
			else
			{
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
				//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
				//FC:FINAL:MSH - Issue #426: showing new form before closing current form not always set focus to the new form.
				// Hiding current form before closing gives opportunity to set focus correctly
				//this.Hide();
				openFormWhenThisClosed = true;
			}
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void frmCDDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmCDDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
				{
					if (frmCDDataEntry.InstancePtr.ActiveControl.GetName() == "cboJournal")
					{
						KeyAscii = (Keys)0;
						vs1.Focus();
					}
					else
					{
						KeyAscii = (Keys)0;
						Support.SendKeys("{TAB}", false);
					}
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#4476 - when pressing F3 the last control is not correct
            //if (this.lastActiveControl == null || this.lastActiveControl != this.vs1)
			//{
			//	return;
			//}
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			// delete a detail item
			int temp = 0;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && vs1.TextMatrix(vs1.Row, VendorCol) == "" && vs1.TextMatrix(vs1.Row, WarrantCol) == "" && vs1.TextMatrix(vs1.Row, CheckCol) == "" && Convert.ToDateTime(vs1.TextMatrix(vs1.Row, DateCol)).ToOADate() == DateTime.Today.ToOADate() && vs1.TextMatrix(vs1.Row, ProjCol) == "" && vs1.TextMatrix(vs1.Row, AmountCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				// highlight th erow to delete
				vs1.Select(vs1.Row, AmountCol, vs1.Row, NumberCol);
				// verify they want to delete it
				temp = vs1.Row;
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					// if yes then
					DeleteFlag = true;
					//FC:FINAL:SBE - #433 - enable/disable grid during row deletion. RowCol change is executed if grid is enabled, and exception is thrown on CurrentCellChanged called by itself
					vs1.Enabled = false;
					if (vs1.Row < vs1.Rows - 1)
					{
						// move the cursor
						vs1.Row += 1;
					}
					else
					{
						vs1.Row -= 1;
					}
					if (Conversion.Val(vs1.TextMatrix(temp, NumberCol)) != 0)
					{
						rsDelete.Execute("DELETE FROM JournalEntries WHERE ID = " + vs1.TextMatrix(temp, NumberCol), "Budgetary");
					}
					//FC:FINAL:SBE - #433 - Number col is not visible (width is equal to 0), and it is changed to DateCol in RowColChanged event.
					//vs1.Col = NumberCol;
					vs1.Col = DateCol;
					vs1.RemoveItem(temp);
					// delete the row
					vs1.Rows += 1;
					// add a new row to the bottom
					vs1.TextMatrix(vs1.Rows - 1, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(vs1.Rows - 1, AmountCol, FCConvert.ToString(0));
					// initialize datra on new row
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, FCConvert.ToString(0));
					CalculateTotals();
					// recalculate totals
					lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
					DeleteFlag = false;
					//FC:FINAL:SBE - #433 - enable/disable grid during row deletion. RowCol change is executed if grid is enabled, and exception is thrown on CurrentCellChanged called by itself
					vs1.Enabled = true;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					// if they dont want to then do nothign
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(btnMenuSave, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void txtPeriod_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtPeriod_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void txtPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtPeriod.Text == "")
			{
				txtPeriod.Text = CalculatePeriod();
			}
			else if (!Information.IsNumeric(txtPeriod.Text))
			{
				MessageBox.Show("You must enter a number in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else if (Conversion.Val(txtPeriod.Text) > 12)
			{
				MessageBox.Show("You must enter a number less then 12 in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else
			{
				temp = txtPeriod.Text;
				txtPeriod.Text = modValidateAccount.GetFormat_6(temp, 2);
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			blnDirty = true;
			if (vs1.Col == AccountCol)
			{
				lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (frmCDDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (vs1.Col == NumberCol)
			{
				// if the number column has focus move it over to the date column
				vs1.Col = DateCol;
			}
			else if (vs1.Col != AccountCol)
			{
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			btnProcessDelete.Enabled = true;
		}

		private void FormControl_Leave(object sender, EventArgs e)
		{
			//btnProcessDelete.Enabled = false;
			this.lastActiveControl = sender as Control;
		}

		private void vs1_RowColChange()
		{
			vs1_RowColChange(vs1, EventArgs.Empty);
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditMaxLength = 0;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			if (vs1.Col == DateCol || vs1.Col == DescriptionCol)
			{
				ToolTip1.SetToolTip(vs1, "Press Enter to Duplicate Information from Last Row");
			}
			else
			{
				ToolTip1.SetToolTip(vs1, "");
			}
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DateCol;
			}
			else if (vs1.Col == TaxCol)
			{
				if (Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol)) == 0)
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				vs1.EditMaxLength = 1;
				if (vs1.TextMatrix(vs1.Row, TaxCol) == "")
				{
					if (Conversion.Val(vs1.TextMatrix(vs1.Row, VendorCol)) == 0)
					{
						vs1.TextMatrix(vs1.Row, TaxCol, "D");
					}
					else
					{
						vs1.TextMatrix(vs1.Row, TaxCol, "N");
					}
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					if (vs1.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
			else if (vs1.Col == ProjCol)
			{
				if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
				{
					vs1.Col += 1;
				}
				else
				{
					vs1.EditCell();
				}
			}
			else if (vs1.Col == VendorCol)
			{
				vs1.EditMaxLength = 5;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.Col == WarrantCol)
			{
				vs1.EditMaxLength = 4;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.Col == CheckCol)
			{
				vs1.EditMaxLength = 6;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else
			{
				if (vs1.Col == AmountCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.Col != AccountCol)
			{
				// if they dont click in the account column then give the grid focus
				if (vs1.Col == AmountCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == AccountCol && e.KeyCode != Keys.F9)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = Keys.D0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = Keys.D0;
					Support.SendKeys("{TAB}", false);
				}
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = Keys.D0;
				if (vs1.Col < AmountCol)
				{
					if (vs1.Col == DateCol || vs1.Col == DescriptionCol)
					{
						// if there is no information then bring it down
						if (vs1.Row > 1)
						{
							//FC:FINAL:MSH - Issue #436: setting a value by using TextMatrix is replaced by setting a value to the EditText, because 
							// value from EditText will replace value in cell when EditableControl will be closed
							//vs1.TextMatrix(vs1.Row, vs1.Col, vs1.TextMatrix(vs1.Row - 1, vs1.Col));
							vs1.EditText = vs1.TextMatrix(vs1.Row - 1, vs1.Col);
						}
					}
					// if we are not on the last column then move over a column
					//FC:FINAL:MSH - Issue #436: increment moved, because in VB6 incrementing of Col value doesn't have affect, but here after incrementing of
					// Col value will be called vs1_ValidateEdit, in which value of Col changed, but value of EditText not changed
					vs1.Col += 1;
				}
				else
				{
					// if we are on the last column then if there is another row move down to it
					if (vs1.Row < vs1.Rows - 1)
					{
						vs1.Col = 0;
						vs1.Row += 1;
					}
					else
					{
						vs1.AddItem("");
						// if there is no other row then create a new one
						vs1.TextMatrix(vs1.Row + 1, AmountCol, FCConvert.ToString(0));
						vs1.TextMatrix(vs1.Row + 1, NumberCol, FCConvert.ToString(0));
						// initialize data on new row
						vs1.TextMatrix(vs1.Row + 1, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Row + 1, TaxCol, 4);
						// move to the next row
						vs1.Col = DateCol;
						vs1.Row += 1;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				// if the left key is hit then if we are in the amount column
				if (vs1.Col == AmountCol)
				{
					// if we dont use project numbers then
					if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
					{
						// move back 2 columns
						KeyCode = Keys.D0;
						vs1.Col -= 2;
					}
				}
			}
			else if (vs1.Col == AmountCol)
			{
				if (KeyCode == Keys.C)
				{
					// if the c key is hit then erase the value
					KeyCode = Keys.D0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string holder = "";
			string temp2 = "";
			//FC:FINAL:MSH - Issue #436: saving correct indexes of Row and Col, for which validating executed and using them in method
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == DateCol)
			{
				if (!Information.IsDate(vs1.EditText))
				{
					MessageBox.Show("You must enter a valid date in this field", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
				}
			}
			else if (col == TaxCol)
			{
				if (vs1.EditText == "n" || vs1.EditText == "d")
				{
					temp2 = vs1.EditText;
					vs1.EditText = Strings.UCase(temp2);
				}
				if (!Information.IsNumeric(vs1.EditText))
				{
					if (vs1.EditText == "")
					{
						vs1.EditText = "D";
					}
					else if (vs1.EditText != "N" && vs1.EditText != "D")
					{
						MessageBox.Show("You may only enter a D, N, or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
				}
				else
				{
					if (FCConvert.ToDouble(vs1.EditText) < 1)
					{
						MessageBox.Show("You may only enter a D, N, or the numbers 1 - 9 in this field", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
				}
			}
			else if (col == AmountCol)
			{
				if (Strings.Trim(vs1.EditText) != "")
				{
					if (!Information.IsNumeric(vs1.EditText))
					{
						MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
					else
					{
						// If CDbl(vs1.EditText) < 0 Then
						// MsgBox "You must enter a positive amount in this field", vbExclamation, "Invalid Number"
						// Cancel = True
						// vs1.EditText = ""
						// Exit Sub
						// End If
						temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
						if (temp != 0)
						{
							if (vs1.EditText.Length > temp + 2)
							{
								vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
							}
						}
						CalculateTotals();
						lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
					}
				}
				else
				{
					if (vs1.EditText == "")
					{
						vs1.EditText = "0";
					}
				}
			}
			else if (col == VendorCol || col == WarrantCol)
			{
				if (vs1.EditText != "")
				{
					if (!Information.IsNumeric(vs1.EditText))
					{
						MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
						vs1.EditText = "";
					}
					else
					{
						holder = vs1.EditText;
						vs1.EditText = modValidateAccount.GetFormat_6(holder, 4);
					}
				}
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalAmount = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
                    //FC:FINAL:SBE - #4478 - use EditText only if cell is in EditMode, otherwise use TextMatrix. Also don't check for column, because it is already changed when Enter is pressed in EditMode
                    //if (vs1.Col == AmountCol)
                    if (vs1.IsCurrentCellInEditMode)
                    {
                        TotalAmount += Conversion.Val(vs1.EditText);
                    }
                    else
                    {
                        TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
                    }
				}
				else
				{
					TotalAmount += Conversion.Val(vs1.TextMatrix(counter, AmountCol));
				}
			}
		}
		// vbPorter upgrade warning: intPeriod As short	OnWriteFCConvert.ToInt32(
		private void SetCombo(int x, ref int intPeriod)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}
		//private void cboJournal_DropDown(object sender, System.EventArgs e)
		//{
		//    modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		//}
		private void SaveJournal()
		{
			int counter;
			int TempJournal = 0;
			int ans;
			string strDescription = "";
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			int lngCheckJournal = 0;
			txtPeriod.Focus();
			if (cboSaveJournal.SelectedIndex == 0)
			{
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "CD");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status = 'P'");
				if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
				{
					MessageBox.Show("Changes to this journal cannot be saved because it has been posted.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					blnDirty = false;
					Close();
					return;
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text))
				{
					executeAddTag = true;
					goto AddTag;
				}
				else
				{
					if (modBudgetaryMaster.Statics.blnCDEdit)
					{
						if (ChosenJournal != Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)))
						{
							rsDeleteInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(ChosenJournal) + " AND Period = " + FCConvert.ToString(ChosenPeriod));
							if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
							{
								rsDeleteInfo.Edit();
								rsDeleteInfo.Set_Fields("Status", "D");
								rsDeleteInfo.Update();
							}
							for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
							{
								if (modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenJournal), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
								{
									cboSaveJournal.Items.RemoveAt(counter);
									cboJournal.Items.RemoveAt(counter);
								}
							}
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
					Master.Edit();
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
					Master.Update();
					cboJournal.Clear();
					cboSaveJournal.Clear();
					FillJournalCombo();
					for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
						{
							cboSaveJournal.SelectedIndex = counter;
							cboJournal.SelectedIndex = counter;
							break;
						}
					}
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modRegistry.SaveRegistryKey("CURRCDJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryMaster.Statics.CurrentCDEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modRegistry.SaveRegistryKey("CURRCDJRNL", FCConvert.ToString(TempJournal));
				modBudgetaryMaster.Statics.CurrentCDEntry = TempJournal;
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "CD";
			clsJournalInfo.Period = txtPeriod.Text;
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			if (modBudgetaryMaster.Statics.blnCDEdit)
			{
				// Dave 1/11/06
				// rs.OpenRecordset ("SELECT * FROM JournalEntries WHERE Type = 'D' and JournalNumber = " & ChosenJournal & " AND Period = " & ChosenPeriod & " AND Status = 'E'")
				// If rs.EndOfFile <> True And rs.BeginningOfFile <> True Then
				// rs.MoveLast
				// rs.MoveFirst
				// Do Until rs.EndOfFile
				// rs.Delete
				// rs.MoveNext
				// Loop
				// End If
				ChosenPeriod = FCConvert.ToInt32(txtPeriod.Text);
			}
			rs.OmitNullsOnInsert = true;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, AmountCol) != "0")
				{
					if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0)
					{
						rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
						rs.AddNew();
						rs.Set_Fields("RCB", "R");
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = " + vs1.TextMatrix(counter, NumberCol));
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.Edit();
						}
						else
						{
							rs.AddNew();
							rs.Set_Fields("RCB", "R");
						}
					}
					rs.Set_Fields("Type", "D");
					rs.Set_Fields("JournalEntriesDate", vs1.TextMatrix(counter, DateCol));
					rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
					rs.Set_Fields("VendorNumber", vs1.TextMatrix(counter, VendorCol));
					if (cboSaveJournal.SelectedIndex != 0)
					{
						rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						rs.Set_Fields("JournalNumber", TempJournal);
					}
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					lngCheckJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
					rs.Set_Fields("Period", txtPeriod.Text);
					rs.Set_Fields("WarrantNumber", vs1.TextMatrix(counter, WarrantCol));
					rs.Set_Fields("CheckNumber", vs1.TextMatrix(counter, CheckCol));
					rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
					rs.Set_Fields("1099", vs1.TextMatrix(counter, TaxCol));
					rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					rs.Set_Fields("Amount", vs1.TextMatrix(counter, AmountCol));
					rs.Set_Fields("BankNumber", intBank);
					if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) < 0 && chkCorrection.CheckState == CheckState.Checked)
					{
						rs.Set_Fields("RCB", "C");
					}
					rs.Set_Fields("Status", "E");
					rs.Update();
					vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
				}
			}
			blnDirty = false;
			if (blnUnload)
			{
				if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCashDisbursement))
				{
					if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						blnPostJournal = true;
					}
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
				Close();
			}
			else
			{
				cboJournal.Clear();
				cboSaveJournal.Clear();
				FillJournalCombo();
				for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
				{
					if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == lngCheckJournal)
					{
						cboSaveJournal.SelectedIndex = counter;
						cboJournal.SelectedIndex = counter;
						break;
					}
				}
				txtJournalDescription.Text = "";
				fraJournalSave.Visible = false;
				MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//cboSaveJournal.Width = 825;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'CD' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnData;
			blnData = false;
			vs1.Select(1, 0);
			if (Conversion.Val(txtPeriod.Text) == 0)
			{
				MessageBox.Show("You must enter an accounting period before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPeriod.Focus();
				return;
			}
			if (fraBanks.Visible == true)
			{
				if (cboBanks.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a bank before you may continue with this process.", "Select Bank", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboBanks.Focus();
					return;
				}
				else
				{
					intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Strings.Left(cboBanks.Text, 2)))));
				}
			}
			CalculateTotals();
			// If TotalAmount < 0 Then
			// MsgBox "You may not have a negative total for this type of journal.", vbInformation, "Invalid Amount"
			// Exit Sub
			// End If
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (Conversion.Val(vs1.TextMatrix(counter, AmountCol)) != 0)
				{
					blnData = true;
					if (vs1.TextMatrix(counter, DescriptionCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, DescriptionCol);
						vs1_RowColChange();
						return;
					}
					else if (vs1.TextMatrix(counter, AccountCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, AccountCol);
						vs1_RowColChange();
						return;
					}
					else if (vs1.TextMatrix(counter, VendorCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, VendorCol);
						vs1_RowColChange();
						return;
					}
					else if (vs1.TextMatrix(counter, WarrantCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, WarrantCol);
						vs1_RowColChange();
						return;
					}
					else if (vs1.TextMatrix(counter, CheckCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, CheckCol);
						vs1_RowColChange();
						return;
					}
					else if (vs1.TextMatrix(counter, TaxCol) == "")
					{
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, TaxCol);
						vs1_RowColChange();
						return;
					}
				}
				if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
				{
					ans = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						vs1.Select(counter, AccountCol);
						vs1.Focus();
						return;
					}
					else
					{
						modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "Cash Disbursement Data Entry");
					}
				}
			}
			if (!blnData)
			{
				MessageBox.Show("You must make at least one entry before you may save this.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					FCFileSystem.FileClose(30);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					cboSaveJournal.Focus();
					return;
				}
				else
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					txtJournalDescription.Focus();
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}
		// vbPorter upgrade warning: intB As short	OnWriteFCConvert.ToInt32(
		private void SetBankCombo(ref int intB)
		{
			int counter;
			for (counter = 0; counter <= cboBanks.Items.Count - 1; counter++)
			{
				if (Conversion.Val(Strings.Trim(Strings.Left(cboBanks.Items[counter].ToString(), 2))) == intB)
				{
					cboBanks.SelectedIndex = counter;
					break;
				}
			}
		}

		public void Init(ref cJournal journ)
		{
			if (!(journ == null))
			{
				modBudgetaryMaster.Statics.blnCDEdit = true;
				ChosenJournal = journ.JournalNumber;
				ChosenPeriod = journ.Period;
				if (journ.JournalEntries.ItemCount() > 15)
				{
					vs1.Rows = journ.JournalEntries.ItemCount() + 1;
				}
				
				txtPeriod.Text = modValidateAccount.GetFormat_6(journ.Period.ToString(), 2);
				journ.JournalEntries.MoveFirst();
				int counter = 0;
				counter = 0;
				cJournalEntry jEntry;
				while (journ.JournalEntries.IsCurrent())
				{
					counter += 1;
					jEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
					vs1.TextMatrix(counter, 0, FCConvert.ToString(jEntry.ID));
					vs1.TextMatrix(counter, 1, Strings.Format(jEntry.JournalEntriesDate, "MM/dd/yyyy"));
					vs1.TextMatrix(counter, 2, jEntry.Description);
					vs1.TextMatrix(counter, 3, jEntry.VendorNumber.ToString().PadLeft(5, '0'));
					vs1.TextMatrix(counter, 4, jEntry.WarrantNumber.ToString().PadLeft(4, '0'));
					vs1.TextMatrix(counter, 5, jEntry.CheckNumber);
					vs1.TextMatrix(counter, 6, jEntry.Account);
					vs1.TextMatrix(counter, 7, jEntry.Code1099);
					vs1.TextMatrix(counter, 8, jEntry.Project);
					vs1.TextMatrix(counter, 9, jEntry.Amount);
					
					journ.JournalEntries.MoveNext();
				}
				if (journ.JournalEntries.ItemCount() < 15)
				{
					for (counter = journ.JournalEntries.ItemCount() + 1; counter <= vs1.Rows - 1; counter++)
					{
						vs1.TextMatrix(counter, 0, "0");
						vs1.TextMatrix(counter, 1, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
						vs1.TextMatrix(counter, 9, "0");
					}
				}
				CalculateTotals();
				lblNetTotal.Text = Strings.FormatCurrency(TotalAmount, 2);
				modRegistry.SaveRegistryKey("CURRCDJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry));
				cboJournal.Enabled = false;
			}
			else
			{
				modBudgetaryMaster.Statics.blnCDEdit = false;
				ChosenJournal = 0;
				ChosenPeriod = 0;
			}
			this.Show(App.MainForm);
		}
	}
}
