﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCompleteJournals.
	/// </summary>
	partial class frmCompleteJournals : BaseForm
	{
		public fecherFoundation.FCGrid vsJournals;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileComplete;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompleteJournals));
			this.vsJournals = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileComplete = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnFileComplete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileComplete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileComplete);
			this.BottomPanel.Location = new System.Drawing.Point(0, 359);
			this.BottomPanel.Size = new System.Drawing.Size(762, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsJournals);
			this.ClientArea.Size = new System.Drawing.Size(762, 299);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(762, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(491, 30);
			this.HeaderText.Text = "Please select the journal(s) to be completed";
			// 
			// vsJournals
			// 
			this.vsJournals.AllowSelection = false;
			this.vsJournals.AllowUserToResizeColumns = false;
			this.vsJournals.AllowUserToResizeRows = false;
			this.vsJournals.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsJournals.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsJournals.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsJournals.BackColorBkg = System.Drawing.Color.Empty;
			this.vsJournals.BackColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.BackColorSel = System.Drawing.Color.Empty;
			this.vsJournals.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsJournals.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsJournals.ColumnHeadersHeight = 30;
			this.vsJournals.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsJournals.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsJournals.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsJournals.FixedCols = 0;
			this.vsJournals.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsJournals.FrozenCols = 0;
			this.vsJournals.GridColor = System.Drawing.Color.Empty;
			this.vsJournals.Location = new System.Drawing.Point(30, 30);
			this.vsJournals.Name = "vsJournals";
			this.vsJournals.ReadOnly = true;
			this.vsJournals.RowHeadersVisible = false;
			this.vsJournals.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsJournals.RowHeightMin = 0;
			this.vsJournals.Rows = 50;
			this.vsJournals.ShowColumnVisibilityMenu = false;
			this.vsJournals.Size = new System.Drawing.Size(702, 239);
			this.vsJournals.StandardTab = true;
			this.vsJournals.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsJournals.TabIndex = 0;
			this.vsJournals.KeyDown += new Wisej.Web.KeyEventHandler(this.vsJournals_KeyDownEvent);
			this.vsJournals.Click += new System.EventHandler(this.vsJournals_ClickEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileComplete,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileComplete
			// 
			this.mnuFileComplete.Index = 0;
			this.mnuFileComplete.Name = "mnuFileComplete";
			this.mnuFileComplete.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileComplete.Text = "Complete Journals";
			this.mnuFileComplete.Click += new System.EventHandler(this.mnuFileComplete_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnFileComplete
			// 
			this.btnFileComplete.AppearanceKey = "acceptButton";
			this.btnFileComplete.Location = new System.Drawing.Point(273, 30);
			this.btnFileComplete.Name = "btnFileComplete";
			this.btnFileComplete.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileComplete.Size = new System.Drawing.Size(192, 48);
			this.btnFileComplete.TabIndex = 0;
			this.btnFileComplete.Text = "Complete Journals";
			this.btnFileComplete.Click += new System.EventHandler(this.mnuFileComplete_Click);
			// 
			// frmCompleteJournals
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(762, 467);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCompleteJournals";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Build Incomplete Journals";
			this.Load += new System.EventHandler(this.frmCompleteJournals_Load);
			this.Activated += new System.EventHandler(this.frmCompleteJournals_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCompleteJournals_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileComplete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnFileComplete;
	}
}
