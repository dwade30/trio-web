﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedApplication.Budgetary;
using TWBD0000.Process1099;

namespace TWBD0000.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<frmSelectTaxFormType>().As<IModalView<ISelectTaxFormTypeViewModel>>();
        }
    }
}
