﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEOYProcessing.
	/// </summary>
	partial class frmEOYProcessing : BaseForm
	{
		public fecherFoundation.FCComboBox cmbNo;
		public fecherFoundation.FCLabel lblNo;
		public fecherFoundation.FCFrame fraEncumbrances;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCGrid vsEncumbrances;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFileListBox flbData;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCPictureBox imgCarryOver;
		public fecherFoundation.FCPictureBox imgCloseUnliquidated;
		public fecherFoundation.FCLabel lblCreateArchive;
		public fecherFoundation.FCLabel lblCloseControl;
		public fecherFoundation.FCLabel lblPostClosing;
		public fecherFoundation.FCLabel lblClearBudgets;
		public fecherFoundation.FCLabel lblResetNumbers;
		public fecherFoundation.FCLabel lblSave1099;
		public fecherFoundation.FCLabel lblClearDetail;
		public fecherFoundation.FCPictureBox imgCreateArchive;
		public fecherFoundation.FCPictureBox imgCloseControl;
		public fecherFoundation.FCPictureBox imgPostClosing;
		public fecherFoundation.FCPictureBox imgClearBudgets;
		public fecherFoundation.FCPictureBox imgResetNumbers;
		public fecherFoundation.FCPictureBox imgSave1099;
		public fecherFoundation.FCPictureBox imgClearDetail;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPictureBox imgCreateArchiveProcessing;
		public fecherFoundation.FCPictureBox imgCloseControlProcessing;
		public fecherFoundation.FCPictureBox imgPostClosingProcessing;
		public fecherFoundation.FCPictureBox imgClearBudgetProcessing;
		public fecherFoundation.FCPictureBox imgSave1099Processing;
		public fecherFoundation.FCPictureBox imgResetNumbersProcessing;
		public fecherFoundation.FCPictureBox imgClearDetailsProcessing;
		public fecherFoundation.FCPictureBox imgCloseUnliquidatedProcessing;
		public fecherFoundation.FCLabel lblCloseUnliquidated;
		public fecherFoundation.FCPictureBox imgCarryOverProcessing;
		public fecherFoundation.FCLabel lblCarryOver;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCPictureBox Image2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblWarning;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbNo = new fecherFoundation.FCComboBox();
            this.lblNo = new fecherFoundation.FCLabel();
            this.fraEncumbrances = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.vsEncumbrances = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.flbData = new fecherFoundation.FCFileListBox();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.imgCarryOver = new fecherFoundation.FCPictureBox();
            this.imgCloseUnliquidated = new fecherFoundation.FCPictureBox();
            this.lblCreateArchive = new fecherFoundation.FCLabel();
            this.lblCloseControl = new fecherFoundation.FCLabel();
            this.lblPostClosing = new fecherFoundation.FCLabel();
            this.lblClearBudgets = new fecherFoundation.FCLabel();
            this.lblResetNumbers = new fecherFoundation.FCLabel();
            this.lblSave1099 = new fecherFoundation.FCLabel();
            this.lblClearDetail = new fecherFoundation.FCLabel();
            this.imgCreateArchive = new fecherFoundation.FCPictureBox();
            this.imgCloseControl = new fecherFoundation.FCPictureBox();
            this.imgPostClosing = new fecherFoundation.FCPictureBox();
            this.imgClearBudgets = new fecherFoundation.FCPictureBox();
            this.imgResetNumbers = new fecherFoundation.FCPictureBox();
            this.imgSave1099 = new fecherFoundation.FCPictureBox();
            this.imgClearDetail = new fecherFoundation.FCPictureBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.imgCreateArchiveProcessing = new fecherFoundation.FCPictureBox();
            this.imgCloseControlProcessing = new fecherFoundation.FCPictureBox();
            this.imgPostClosingProcessing = new fecherFoundation.FCPictureBox();
            this.imgClearBudgetProcessing = new fecherFoundation.FCPictureBox();
            this.imgSave1099Processing = new fecherFoundation.FCPictureBox();
            this.imgResetNumbersProcessing = new fecherFoundation.FCPictureBox();
            this.imgClearDetailsProcessing = new fecherFoundation.FCPictureBox();
            this.imgCloseUnliquidatedProcessing = new fecherFoundation.FCPictureBox();
            this.lblCloseUnliquidated = new fecherFoundation.FCLabel();
            this.imgCarryOverProcessing = new fecherFoundation.FCPictureBox();
            this.lblCarryOver = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.Image2 = new fecherFoundation.FCPictureBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblWarning = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEncumbrances)).BeginInit();
            this.fraEncumbrances.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsEncumbrances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCarryOver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseUnliquidated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPostClosing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearBudgets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResetNumbers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSave1099)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateArchiveProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseControlProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPostClosingProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearBudgetProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSave1099Processing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResetNumbersProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearDetailsProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseUnliquidatedProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCarryOverProcessing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 575);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraEncumbrances);
            this.ClientArea.Controls.Add(this.flbData);
            this.ClientArea.Controls.Add(this.cmbNo);
            this.ClientArea.Controls.Add(this.lblNo);
            this.ClientArea.Controls.Add(this.imgCarryOver);
            this.ClientArea.Controls.Add(this.imgCloseUnliquidated);
            this.ClientArea.Controls.Add(this.lblCreateArchive);
            this.ClientArea.Controls.Add(this.lblCloseControl);
            this.ClientArea.Controls.Add(this.lblPostClosing);
            this.ClientArea.Controls.Add(this.lblClearBudgets);
            this.ClientArea.Controls.Add(this.lblResetNumbers);
            this.ClientArea.Controls.Add(this.lblSave1099);
            this.ClientArea.Controls.Add(this.lblClearDetail);
            this.ClientArea.Controls.Add(this.imgCreateArchive);
            this.ClientArea.Controls.Add(this.imgCloseControl);
            this.ClientArea.Controls.Add(this.imgPostClosing);
            this.ClientArea.Controls.Add(this.imgClearBudgets);
            this.ClientArea.Controls.Add(this.imgResetNumbers);
            this.ClientArea.Controls.Add(this.imgSave1099);
            this.ClientArea.Controls.Add(this.imgClearDetail);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.imgCreateArchiveProcessing);
            this.ClientArea.Controls.Add(this.imgCloseControlProcessing);
            this.ClientArea.Controls.Add(this.imgPostClosingProcessing);
            this.ClientArea.Controls.Add(this.imgClearBudgetProcessing);
            this.ClientArea.Controls.Add(this.imgSave1099Processing);
            this.ClientArea.Controls.Add(this.imgResetNumbersProcessing);
            this.ClientArea.Controls.Add(this.imgClearDetailsProcessing);
            this.ClientArea.Controls.Add(this.imgCloseUnliquidatedProcessing);
            this.ClientArea.Controls.Add(this.lblCloseUnliquidated);
            this.ClientArea.Controls.Add(this.imgCarryOverProcessing);
            this.ClientArea.Controls.Add(this.lblCarryOver);
            this.ClientArea.Controls.Add(this.Image1);
            this.ClientArea.Controls.Add(this.Image2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblWarning);
            this.ClientArea.Size = new System.Drawing.Size(1014, 518);
            this.ClientArea.Controls.SetChildIndex(this.lblWarning, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.Image2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Image1, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCarryOver, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCarryOverProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCloseUnliquidated, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCloseUnliquidatedProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgClearDetailsProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgResetNumbersProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgSave1099Processing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgClearBudgetProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgPostClosingProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCloseControlProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCreateArchiveProcessing, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgClearDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgSave1099, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgResetNumbers, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgClearBudgets, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgPostClosing, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCloseControl, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCreateArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblClearDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSave1099, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblResetNumbers, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblClearBudgets, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPostClosing, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCloseControl, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCreateArchive, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCloseUnliquidated, 0);
            this.ClientArea.Controls.SetChildIndex(this.imgCarryOver, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblNo, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbNo, 0);
            this.ClientArea.Controls.SetChildIndex(this.flbData, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraEncumbrances, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(256, 28);
            this.HeaderText.Text = "End of Year Processing";
            // 
            // cmbNo
            // 
            this.cmbNo.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.cmbNo.Location = new System.Drawing.Point(274, 518);
            this.cmbNo.Name = "cmbNo";
            this.cmbNo.Size = new System.Drawing.Size(121, 40);
            this.cmbNo.TabIndex = 6;
            this.cmbNo.SelectedIndexChanged += new System.EventHandler(this.optNo_CheckedChanged);
            // 
            // lblNo
            // 
            this.lblNo.Location = new System.Drawing.Point(30, 532);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(175, 16);
            this.lblNo.TabIndex = 7;
            this.lblNo.Text = "CARRY OVER ENCUMBRANCES";
            // 
            // fraEncumbrances
            // 
            this.fraEncumbrances.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraEncumbrances.BackColor = System.Drawing.Color.FromName("@window");
            this.fraEncumbrances.Controls.Add(this.cmdCancel);
            this.fraEncumbrances.Controls.Add(this.cmdOK);
            this.fraEncumbrances.Controls.Add(this.cmdSelectAll);
            this.fraEncumbrances.Controls.Add(this.cmdClearAll);
            this.fraEncumbrances.Controls.Add(this.vsEncumbrances);
            this.fraEncumbrances.Controls.Add(this.Label2);
            this.fraEncumbrances.Location = new System.Drawing.Point(30, 82);
            this.fraEncumbrances.Name = "fraEncumbrances";
            this.fraEncumbrances.Size = new System.Drawing.Size(964, 493);
            this.fraEncumbrances.TabIndex = 0;
            this.fraEncumbrances.Text = "Select Encumbrances";
            this.fraEncumbrances.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(124, 433);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(84, 40);
            this.cmdCancel.TabIndex = 23;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 433);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(84, 40);
            this.cmdOK.TabIndex = 22;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(20, 373);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(84, 40);
            this.cmdSelectAll.TabIndex = 21;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdClearAll.AppearanceKey = "actionButton";
            this.cmdClearAll.Location = new System.Drawing.Point(124, 373);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(84, 40);
            this.cmdClearAll.TabIndex = 20;
            this.cmdClearAll.Text = "Clear All";
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // vsEncumbrances
            // 
            this.vsEncumbrances.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsEncumbrances.Cols = 17;
            this.vsEncumbrances.ColumnHeadersVisible = false;
            this.vsEncumbrances.FixedRows = 0;
            this.vsEncumbrances.Location = new System.Drawing.Point(20, 66);
            this.vsEncumbrances.Name = "vsEncumbrances";
            this.vsEncumbrances.Rows = 0;
            this.vsEncumbrances.Size = new System.Drawing.Size(924, 287);
            this.vsEncumbrances.TabIndex = 24;
            this.vsEncumbrances.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vsEncumbrances_RowExpanded);
            this.vsEncumbrances.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsEncumbrances_RowCollapsed);
            this.vsEncumbrances.Click += new System.EventHandler(this.vsEncumbrances_ClickEvent);
            this.vsEncumbrances.KeyDown += new Wisej.Web.KeyEventHandler(this.vsEncumbrances_KeyDownEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(970, 16);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "PLEASE SELECT THE ENCUMBRANCE ENTRIES YOU WOULD LIKE BROUGHT OVER TO THE NEXT FIS" +
    "CAL YEAR.  WHEN YOU ARE DONE CLICK THE \"OK\" BUTTON TO CONTINUE";
            // 
            // flbData
            // 
            this.flbData.Location = new System.Drawing.Point(465, 363);
            this.flbData.Name = "flbData";
            this.flbData.Size = new System.Drawing.Size(155, 91);
            this.flbData.TabIndex = 5;
            this.flbData.Visible = false;
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(371, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(190, 48);
            this.cmdProcess.TabIndex = 6;
            this.cmdProcess.Text = "Process End of Year";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // imgCarryOver
            // 
            this.imgCarryOver.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCarryOver.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgCarryOver.Location = new System.Drawing.Point(30, 479);
            this.imgCarryOver.Name = "imgCarryOver";
            this.imgCarryOver.Size = new System.Drawing.Size(28, 29);
            this.imgCarryOver.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCarryOver.Visible = false;
            // 
            // imgCloseUnliquidated
            // 
            this.imgCloseUnliquidated.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCloseUnliquidated.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgCloseUnliquidated.Location = new System.Drawing.Point(30, 206);
            this.imgCloseUnliquidated.Name = "imgCloseUnliquidated";
            this.imgCloseUnliquidated.Size = new System.Drawing.Size(28, 29);
            this.imgCloseUnliquidated.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCloseUnliquidated.Visible = false;
            // 
            // lblCreateArchive
            // 
            this.lblCreateArchive.Location = new System.Drawing.Point(93, 173);
            this.lblCreateArchive.Name = "lblCreateArchive";
            this.lblCreateArchive.Size = new System.Drawing.Size(104, 16);
            this.lblCreateArchive.TabIndex = 19;
            this.lblCreateArchive.Text = "CREATE ARCHIVE";
            // 
            // lblCloseControl
            // 
            this.lblCloseControl.Location = new System.Drawing.Point(93, 251);
            this.lblCloseControl.Name = "lblCloseControl";
            this.lblCloseControl.Size = new System.Drawing.Size(280, 16);
            this.lblCloseControl.TabIndex = 18;
            this.lblCloseControl.Text = "CLOSE CONTROL ACCOUNTS TO FUND BALANCE";
            // 
            // lblPostClosing
            // 
            this.lblPostClosing.Location = new System.Drawing.Point(93, 290);
            this.lblPostClosing.Name = "lblPostClosing";
            this.lblPostClosing.Size = new System.Drawing.Size(150, 16);
            this.lblPostClosing.TabIndex = 17;
            this.lblPostClosing.Text = "POST CLOSING JOURNAL";
            // 
            // lblClearBudgets
            // 
            this.lblClearBudgets.Location = new System.Drawing.Point(93, 368);
            this.lblClearBudgets.Name = "lblClearBudgets";
            this.lblClearBudgets.Size = new System.Drawing.Size(220, 16);
            this.lblClearBudgets.TabIndex = 16;
            this.lblClearBudgets.Text = "CLEAR LAST YEAR BUDGET AMOUNTS";
            // 
            // lblResetNumbers
            // 
            this.lblResetNumbers.Location = new System.Drawing.Point(93, 407);
            this.lblResetNumbers.Name = "lblResetNumbers";
            this.lblResetNumbers.Size = new System.Drawing.Size(230, 16);
            this.lblResetNumbers.TabIndex = 15;
            this.lblResetNumbers.Text = "RESET JOURNAL & WARRANT NUMBERS";
            // 
            // lblSave1099
            // 
            this.lblSave1099.Location = new System.Drawing.Point(93, 329);
            this.lblSave1099.Name = "lblSave1099";
            this.lblSave1099.Size = new System.Drawing.Size(150, 16);
            this.lblSave1099.TabIndex = 14;
            this.lblSave1099.Text = "SAVE 1099 INFORMATION";
            // 
            // lblClearDetail
            // 
            this.lblClearDetail.Location = new System.Drawing.Point(93, 446);
            this.lblClearDetail.Name = "lblClearDetail";
            this.lblClearDetail.Size = new System.Drawing.Size(150, 16);
            this.lblClearDetail.TabIndex = 13;
            this.lblClearDetail.Text = "CLEAR LAST YEAR DETAIL";
            // 
            // imgCreateArchive
            // 
            this.imgCreateArchive.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCreateArchive.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgCreateArchive.Location = new System.Drawing.Point(30, 167);
            this.imgCreateArchive.Name = "imgCreateArchive";
            this.imgCreateArchive.Size = new System.Drawing.Size(28, 29);
            this.imgCreateArchive.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCreateArchive.Visible = false;
            // 
            // imgCloseControl
            // 
            this.imgCloseControl.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCloseControl.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgCloseControl.Location = new System.Drawing.Point(30, 245);
            this.imgCloseControl.Name = "imgCloseControl";
            this.imgCloseControl.Size = new System.Drawing.Size(28, 29);
            this.imgCloseControl.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCloseControl.Visible = false;
            // 
            // imgPostClosing
            // 
            this.imgPostClosing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgPostClosing.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgPostClosing.Location = new System.Drawing.Point(30, 284);
            this.imgPostClosing.Name = "imgPostClosing";
            this.imgPostClosing.Size = new System.Drawing.Size(28, 29);
            this.imgPostClosing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgPostClosing.Visible = false;
            // 
            // imgClearBudgets
            // 
            this.imgClearBudgets.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgClearBudgets.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgClearBudgets.Location = new System.Drawing.Point(30, 362);
            this.imgClearBudgets.Name = "imgClearBudgets";
            this.imgClearBudgets.Size = new System.Drawing.Size(28, 29);
            this.imgClearBudgets.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgClearBudgets.Visible = false;
            // 
            // imgResetNumbers
            // 
            this.imgResetNumbers.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgResetNumbers.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgResetNumbers.Location = new System.Drawing.Point(30, 401);
            this.imgResetNumbers.Name = "imgResetNumbers";
            this.imgResetNumbers.Size = new System.Drawing.Size(28, 29);
            this.imgResetNumbers.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgResetNumbers.Visible = false;
            // 
            // imgSave1099
            // 
            this.imgSave1099.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgSave1099.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgSave1099.Location = new System.Drawing.Point(30, 323);
            this.imgSave1099.Name = "imgSave1099";
            this.imgSave1099.Size = new System.Drawing.Size(28, 29);
            this.imgSave1099.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgSave1099.Visible = false;
            // 
            // imgClearDetail
            // 
            this.imgClearDetail.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgClearDetail.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.imgClearDetail.Location = new System.Drawing.Point(30, 440);
            this.imgClearDetail.Name = "imgClearDetail";
            this.imgClearDetail.Size = new System.Drawing.Size(28, 29);
            this.imgClearDetail.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgClearDetail.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Label1.Location = new System.Drawing.Point(30, 131);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(50, 16);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "STATUS";
            // 
            // imgCreateArchiveProcessing
            // 
            this.imgCreateArchiveProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCreateArchiveProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgCreateArchiveProcessing.Location = new System.Drawing.Point(30, 167);
            this.imgCreateArchiveProcessing.Name = "imgCreateArchiveProcessing";
            this.imgCreateArchiveProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgCreateArchiveProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCreateArchiveProcessing.Visible = false;
            // 
            // imgCloseControlProcessing
            // 
            this.imgCloseControlProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCloseControlProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgCloseControlProcessing.Location = new System.Drawing.Point(30, 245);
            this.imgCloseControlProcessing.Name = "imgCloseControlProcessing";
            this.imgCloseControlProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgCloseControlProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCloseControlProcessing.Visible = false;
            // 
            // imgPostClosingProcessing
            // 
            this.imgPostClosingProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgPostClosingProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgPostClosingProcessing.Location = new System.Drawing.Point(30, 284);
            this.imgPostClosingProcessing.Name = "imgPostClosingProcessing";
            this.imgPostClosingProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgPostClosingProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgPostClosingProcessing.Visible = false;
            // 
            // imgClearBudgetProcessing
            // 
            this.imgClearBudgetProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgClearBudgetProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgClearBudgetProcessing.Location = new System.Drawing.Point(30, 362);
            this.imgClearBudgetProcessing.Name = "imgClearBudgetProcessing";
            this.imgClearBudgetProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgClearBudgetProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgClearBudgetProcessing.Visible = false;
            // 
            // imgSave1099Processing
            // 
            this.imgSave1099Processing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgSave1099Processing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgSave1099Processing.Location = new System.Drawing.Point(30, 323);
            this.imgSave1099Processing.Name = "imgSave1099Processing";
            this.imgSave1099Processing.Size = new System.Drawing.Size(28, 29);
            this.imgSave1099Processing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgSave1099Processing.Visible = false;
            // 
            // imgResetNumbersProcessing
            // 
            this.imgResetNumbersProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgResetNumbersProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgResetNumbersProcessing.Location = new System.Drawing.Point(30, 401);
            this.imgResetNumbersProcessing.Name = "imgResetNumbersProcessing";
            this.imgResetNumbersProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgResetNumbersProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgResetNumbersProcessing.Visible = false;
            // 
            // imgClearDetailsProcessing
            // 
            this.imgClearDetailsProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgClearDetailsProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgClearDetailsProcessing.Location = new System.Drawing.Point(30, 440);
            this.imgClearDetailsProcessing.Name = "imgClearDetailsProcessing";
            this.imgClearDetailsProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgClearDetailsProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgClearDetailsProcessing.Visible = false;
            // 
            // imgCloseUnliquidatedProcessing
            // 
            this.imgCloseUnliquidatedProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCloseUnliquidatedProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgCloseUnliquidatedProcessing.Location = new System.Drawing.Point(30, 206);
            this.imgCloseUnliquidatedProcessing.Name = "imgCloseUnliquidatedProcessing";
            this.imgCloseUnliquidatedProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgCloseUnliquidatedProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCloseUnliquidatedProcessing.Visible = false;
            // 
            // lblCloseUnliquidated
            // 
            this.lblCloseUnliquidated.Location = new System.Drawing.Point(93, 212);
            this.lblCloseUnliquidated.Name = "lblCloseUnliquidated";
            this.lblCloseUnliquidated.Size = new System.Drawing.Size(230, 16);
            this.lblCloseUnliquidated.TabIndex = 11;
            this.lblCloseUnliquidated.Text = "CLOSE UNLIQUIDATED ENCUMBRANCES";
            // 
            // imgCarryOverProcessing
            // 
            this.imgCarryOverProcessing.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgCarryOverProcessing.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.imgCarryOverProcessing.Location = new System.Drawing.Point(30, 479);
            this.imgCarryOverProcessing.Name = "imgCarryOverProcessing";
            this.imgCarryOverProcessing.Size = new System.Drawing.Size(28, 29);
            this.imgCarryOverProcessing.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgCarryOverProcessing.Visible = false;
            // 
            // lblCarryOver
            // 
            this.lblCarryOver.Location = new System.Drawing.Point(93, 485);
            this.lblCarryOver.Name = "lblCarryOver";
            this.lblCarryOver.Size = new System.Drawing.Size(330, 16);
            this.lblCarryOver.TabIndex = 10;
            this.lblCarryOver.Text = "CARRY OVER SELECTED UNLIQUIDATED ENCUMBRANCES";
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.ImageSource = "icon - process in progress - active?color=#7EDBE7";
            this.Image1.Location = new System.Drawing.Point(30, 82);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(28, 29);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // Image2
            // 
            this.Image2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image2.ImageSource = "icon - process complete - active?color=#7EDBE7";
            this.Image2.Location = new System.Drawing.Point(200, 82);
            this.Image2.Name = "Image2";
            this.Image2.Size = new System.Drawing.Size(28, 29);
            this.Image2.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(78, 88);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 16);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "= PROCESSING";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(248, 88);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(75, 16);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "= COMPLETE";
            // 
            // lblWarning
            // 
            this.lblWarning.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.lblWarning.Location = new System.Drawing.Point(30, 30);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(784, 32);
            this.lblWarning.TabIndex = 7;
            this.lblWarning.Text = "THIS PROCESS MAY TAKE UP TO 30 MINUTES. DO NOT CLOSE THIS WINDOW ONCE THE PROCESS" +
    " HAS STARTED UNTIL IT COMPLETES WITHOUT FIRST CONTACTING TRIO SUPPORT.";
            // 
            // frmEOYProcessing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEOYProcessing";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "End of Year Processing";
            this.Activated += new System.EventHandler(this.frmEOYProcessing_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEOYProcessing_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEncumbrances)).EndInit();
            this.fraEncumbrances.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsEncumbrances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCarryOver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseUnliquidated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPostClosing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearBudgets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResetNumbers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSave1099)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCreateArchiveProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseControlProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPostClosingProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearBudgetProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSave1099Processing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResetNumbersProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClearDetailsProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCloseUnliquidatedProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCarryOverProcessing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image2)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
