﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using SharedApplication.Extensions;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmHancockCountyPayrollImport.
	/// </summary>
	public partial class frmHancockCountyPayrollImport : BaseForm
	{
		public frmHancockCountyPayrollImport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmHancockCountyPayrollImport InstancePtr
		{
			get
			{
				return (frmHancockCountyPayrollImport)Sys.GetInstance(typeof(frmHancockCountyPayrollImport));
			}
		}

		protected frmHancockCountyPayrollImport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public string strFileName = "";
		int SelectCol;
		int DescriptionCol;
		public string strBadAccounts = string.Empty;

		private class HancockCountyPayrollImportStruct
		{
			public string Account;
			public DateTime? CheckDate;
			public Decimal Amount;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public HancockCountyPayrollImportStruct()
			{
				this.Account = "";
				this.CheckDate = null;
				this.Amount = 0;
			}
		};

		private void frmHancockCountyPayrollImport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmHancockCountyPayrollImport_Load(object sender, System.EventArgs e)
		{
			SelectCol = 0;
			DescriptionCol = 1;
			FormatGrid();
			FillGrid();
			cmbAllOrSelected.SelectedIndex = 0;
		}

		private void FormatGrid()
		{
			vsBranches.ColWidth(SelectCol, FCConvert.ToInt32(vsBranches.WidthOriginal * 0.2));
			vsBranches.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void FillGrid()
		{
			string[] strCheckInfo = null;
			StreamReader tsInfo;
			string strFileInfo = "";
			int counter;
			// vbPorter upgrade warning: blnFound As short --> As int	OnWrite(bool)
			bool recordFound;
			tsInfo = File.OpenText(strFileName);
			while (tsInfo.EndOfStream == false)
			{
				strFileInfo = Strings.Trim(tsInfo.ReadLine());
				strCheckInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strCheckInfo, 1) >= 8)
				{
					if (!(Information.IsNumeric(strCheckInfo[7]) || Information.IsNumeric(strCheckInfo[8])))
					{
						// do nothing
					}
					else
					{
						recordFound = false;
						for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
						{
							if (FCConvert.ToString(vsBranches.RowData(counter)) == Strings.Trim(strCheckInfo[3]))
							{
								recordFound = true;
								break;
							}
						}
						if (!recordFound)
						{
							vsBranches.Rows += 1;
							vsBranches.TextMatrix(vsBranches.Rows - 1, SelectCol, false);
							vsBranches.TextMatrix(vsBranches.Rows - 1, DescriptionCol, "Branch " + Strings.Trim(strCheckInfo[3]));
							vsBranches.RowData(vsBranches.Rows - 1, Strings.Trim(strCheckInfo[3]));
						}
					}
				}
			}
			vsBranches.Select(0, DescriptionCol);
			vsBranches.Sort = FCGrid.SortSettings.flexSortGenericAscending;

			tsInfo.Close();
		}

		private void frmHancockCountyPayrollImport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
			{
				vsBranches.TextMatrix(counter, SelectCol, true);
			}
		}

		private void mnuFileUnselectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
			{
				vsBranches.TextMatrix(counter, SelectCol, false);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			bool blnBranchSelected;
			int counter;
			blnBranchSelected = false;
			if (cmbAllOrSelected.SelectedIndex == 0)
			{
				blnBranchSelected = true;
			}
			else
			{
				for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsBranches.TextMatrix(counter, SelectCol)))
					{
						blnBranchSelected = true;
						break;
					}
				}
			}
			if (blnBranchSelected)
			{
				ProcessImportFile();
			}
		}

		private void ProcessImportFile()
		{
			int counter;
			int intRecords;
			clsDRWrapper rsEntries = new clsDRWrapper();
			int lngJournal;
			clsDRWrapper Master = new clsDRWrapper();
			string[] strCheckInfo = null;
			StreamReader tsInfo;
			string strFileInfo = "";
			int intSplit = 0;
			HancockCountyPayrollImportStruct[] hpiInfo = null;
			int intTotalRecords;
			bool blnFound = false;
			try
			{
				// On Error GoTo ErrorHandler
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Importing Data...");
				tsInfo = File.OpenText(strFileName);
				intRecords = 0;
				lngJournal = 0;
				strBadAccounts = "";
				intTotalRecords = 0;
				while (tsInfo.EndOfStream == false)
				{
					//Application.DoEvents();
					strFileInfo = Strings.Trim(tsInfo.ReadLine());
					strCheckInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(strCheckInfo, 1) >= 8)
					{
						if (!(Information.IsNumeric(strCheckInfo[7]) || Information.IsNumeric(strCheckInfo[8])))
						{
							// do nothing
						}
						else
						{
							if (!ValidBranch(Strings.Trim(strCheckInfo[3])))
							{
								goto NextRecord;
							}
							intRecords += 1;
							if (lngJournal != 0)
							{
								// do nothing
							}
							else
							{
								// get journal number
								if (modBudgetaryAccounting.LockJournal() == false)
								{
									// add entries to incomplete journals table
								}
								else
								{
									Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
									if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
									{
										Master.MoveLast();
										Master.MoveFirst();
										// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
										lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
									}
									else
									{
										lngJournal = 1;
									}
									Master.AddNew();
									Master.Set_Fields("JournalNumber", lngJournal);
									Master.Set_Fields("Status", "E");
									Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
									Master.Set_Fields("StatusChangeDate", DateTime.Today);
									Master.Set_Fields("Description", Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4) + " Payroll Import");
									Master.Set_Fields("Type", "GJ");
									Master.Set_Fields("Period", DateAndTime.DateValue(Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4)).Month);
									Master.Update();
									Master.Reset();
									modBudgetaryAccounting.UnlockJournal();
								}
							}
							if (intTotalRecords == 0)
							{
								intSplit = Strings.InStr(3, strCheckInfo[5], " ", CompareConstants.vbBinaryCompare);
								Array.Resize(ref hpiInfo, intTotalRecords + 1);
								//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
								hpiInfo[intTotalRecords] = new HancockCountyPayrollImportStruct();
								if (intSplit != 0)
								{
									hpiInfo[intTotalRecords].Account = Strings.Trim(Strings.Left(strCheckInfo[5], intSplit));
								}
								else
								{
									hpiInfo[intTotalRecords].Account = Strings.Trim(strCheckInfo[5]);
								}
								hpiInfo[intTotalRecords].CheckDate = DateAndTime.DateValue(Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4));
								if (Strings.Trim(strCheckInfo[7]) != "")
								{
									hpiInfo[intTotalRecords].Amount = FCConvert.ToDecimal(strCheckInfo[7]);
								}
								else
								{
									hpiInfo[intTotalRecords].Amount = FCConvert.ToDecimal(strCheckInfo[8]) * -1;
								}
								intTotalRecords += 1;
							}
							else
							{
								blnFound = false;
								intSplit = Strings.InStr(3, strCheckInfo[5], " ", CompareConstants.vbBinaryCompare);
								for (counter = 0; counter <= intTotalRecords - 1; counter++)
								{
									if (intSplit != 0)
									{
										if (hpiInfo[counter].Account == Strings.Trim(Strings.Left(strCheckInfo[5], intSplit)) && hpiInfo[counter].CheckDate == DateAndTime.DateValue(Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4)))
										{
											if (Strings.Trim(strCheckInfo[7]) != "")
											{
												hpiInfo[counter].Amount += FCConvert.ToDecimal(strCheckInfo[7]);
											}
											else
											{
												hpiInfo[counter].Amount -= FCConvert.ToDecimal(strCheckInfo[8]);
											}
											blnFound = true;
											break;
										}
									}
									else
									{
										if (hpiInfo[counter].Account == Strings.Trim(strCheckInfo[5]) && hpiInfo[counter].CheckDate == DateAndTime.DateValue(Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4)))
										{
											if (Strings.Trim(strCheckInfo[7]) != "")
											{
												hpiInfo[counter].Amount += FCConvert.ToDecimal(strCheckInfo[7]);
											}
											else
											{
												hpiInfo[counter].Amount -= FCConvert.ToDecimal(strCheckInfo[8]);
											}
											blnFound = true;
											break;
										}
									}
								}
								if (!blnFound)
								{
									Array.Resize(ref hpiInfo, intTotalRecords + 1);
									//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
									hpiInfo[intTotalRecords] = new HancockCountyPayrollImportStruct();
									if (intSplit != 0)
									{
										hpiInfo[intTotalRecords].Account = Strings.Trim(Strings.Left(strCheckInfo[5], intSplit));
									}
									else
									{
										hpiInfo[intTotalRecords].Account = Strings.Trim(strCheckInfo[5]);
									}
									hpiInfo[intTotalRecords].CheckDate = DateAndTime.DateValue(Strings.Mid(strCheckInfo[1], 5, 2) + "/" + Strings.Right(strCheckInfo[1], 2) + "/" + Strings.Left(strCheckInfo[1], 4));
									if (Strings.Trim(strCheckInfo[7]) != "")
									{
										hpiInfo[intTotalRecords].Amount = FCConvert.ToDecimal(strCheckInfo[7]);
									}
									else
									{
										hpiInfo[intTotalRecords].Amount = FCConvert.ToDecimal(strCheckInfo[8]) * -1;
									}
									intTotalRecords += 1;
								}
							}
						}
					}
					NextRecord:
					;
				}
				rsEntries.OmitNullsOnInsert = true;
				for (counter = 0; counter <= intTotalRecords - 1; counter++)
				{
					//Application.DoEvents();
					rsEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields("JournalEntriesDate", hpiInfo[counter].CheckDate);
					rsEntries.Set_Fields("Description", "Payroll Import");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", hpiInfo[counter].Account);
					if (modValidateAccount.AccountValidate(Strings.Trim(hpiInfo[counter].Account)) == false)
					{
						strBadAccounts += Strings.Trim(hpiInfo[counter].Account) + "\r\n";
					}
					rsEntries.Set_Fields("Amount", hpiInfo[counter].Amount);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", hpiInfo[counter].CheckDate.Value.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				frmWait.InstancePtr.Unload();
				if (intRecords > 0)
				{
					MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(lngJournal), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("No entries were found to import.", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				if (Strings.Trim(strBadAccounts) != "")
				{
					rptHancockCountyBadAccounts.InstancePtr.strBadAccts = strBadAccounts;
					frmReportViewer.InstancePtr.Init(rptHancockCountyBadAccounts.InstancePtr);
				}
				Close();
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description, "Error Occurred", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		
		private bool ValidBranch(string strBranch)
		{
			bool ValidBranch = false;
			int counter;
			ValidBranch = false;
			if (cmbAllOrSelected.SelectedIndex == 0)
			{
				ValidBranch = true;
			}
			else
			{
				for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
				{
					if (FCConvert.ToString(vsBranches.RowData(counter)) == strBranch)
					{
						ValidBranch = FCUtils.CBool(vsBranches.TextMatrix(counter, SelectCol));
						break;
					}
				}
			}
			return ValidBranch;
		}

		private void cmbAllOrSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAllOrSelected.SelectedIndex == 0)
			{
				int counter;
				for (counter = 0; counter <= vsBranches.Rows - 1; counter++)
				{
					vsBranches.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
				}
				fraBranch.Enabled = false;
				vsBranches.Enabled = false;
				cmdFileSelectAll.Enabled = false;
				cmdFileUnselectAll.Enabled = false;
			}
			else
			{
				fraBranch.Enabled = true;
				vsBranches.Enabled = true;
				cmdFileSelectAll.Enabled = true;
				cmdFileUnselectAll.Enabled = true;
			}
		}

		

		private void vsBranches_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				keyAscii = 0;
				if (FCUtils.CBool(vsBranches.TextMatrix(vsBranches.Row, SelectCol)) == true)
				{
					vsBranches.TextMatrix(vsBranches.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					vsBranches.TextMatrix(vsBranches.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}

		private void frmHancockCountyPayrollImport_Resize(object sender, EventArgs e)
		{
			vsBranches.ColWidth(SelectCol, FCConvert.ToInt32(vsBranches.WidthOriginal * 0.2));
		}

		private void vsBranches_Click(object sender, EventArgs e)
		{
			if (FCUtils.CBool(vsBranches.TextMatrix(vsBranches.Row, SelectCol)) == true)
			{
				vsBranches.TextMatrix(vsBranches.Row, SelectCol, FCConvert.ToString(false));
			}
			else
			{
				vsBranches.TextMatrix(vsBranches.Row, SelectCol, FCConvert.ToString(true));
			}
		}
	}
}
