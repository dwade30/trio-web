﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCreateACHFiles.
	/// </summary>
	public partial class frmCreateACHFiles : BaseForm
	{
		public frmCreateACHFiles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreateACHFiles InstancePtr
		{
			get
			{
				return (frmCreateACHFiles)Sys.GetInstance(typeof(frmCreateACHFiles));
			}
		}

		protected frmCreateACHFiles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsACHSetup tACHSetup = new clsACHSetup();
		private clsACHSetup tACHSetup_AutoInitialized;

		private clsACHSetup tACHSetup
		{
			get
			{
				if (tACHSetup_AutoInitialized == null)
				{
					tACHSetup_AutoInitialized = new clsACHSetup();
				}
				return tACHSetup_AutoInitialized;
			}
			set
			{
				tACHSetup_AutoInitialized = value;
			}
		}

		private string strACHEmployerIDPrefix = string.Empty;
		private bool boolDontRespondToClick;
		private bool boolInitRun;
		bool boolBalanced;

		private void cmdPrenote_Click()
		{
			CreatePrenotes();
		}

		private void frmCreateACHFiles_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCreateACHFiles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateACHFiles.FillStyle	= 0;
			//frmCreateACHFiles.ScaleWidth	= 5880;
			//frmCreateACHFiles.ScaleHeight	= 4440;
			//frmCreateACHFiles.LinkTopic	= "Form2";
			//frmCreateACHFiles.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			// boolMadePrenoteFile = False
			strACHEmployerIDPrefix = "1";
			boolBalanced = false;
			clsDRWrapper rsData = new clsDRWrapper();
			// GET IF THERE IS TO BE A PREFIX ADDED TO THE EMPLOYER NUMBER ON THE ACH
			// ELECTRONIC FILE.
			switch (FCConvert.ToInt32(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ACHFederalIDPrefix"))))
			{
				case 1:
					{
						strACHEmployerIDPrefix = "1";
						break;
					}
				case 9:
					{
						strACHEmployerIDPrefix = "9";
						break;
					}
				default:
					{
						strACHEmployerIDPrefix = " ";
						break;
					}
			}
			//end switch
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("BalancedACHFile")))
			{
				boolBalanced = true;
			}
			else
			{
				boolBalanced = false;
			}
			T2KEffectiveEntryDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			FillJournalCombo();
			FillBankCombo();
		}

		public void Init(bool blnPrenote = false)
		{
			// dtPayDateChosen = 0
			// intPayRunChosen = 0
			// boolInitRun = boolFirstRun
			if (blnPrenote)
			{
				if (cmbACH.Items.Contains("ACH File"))
				{
					cmbACH.Items.Remove("ACH File");
				}
				cmbACH.SelectedIndex = 0;
				fraACHFile.Enabled = false;
				lblJournal.Enabled = false;
				cboJournal.SelectedIndex = -1;
				cboJournal.Enabled = false;
			}
			else
			{
				cmbACH.SelectedIndex = 1;
				cmbACH.SelectedIndex = 0;
			}
			this.Show(FormShowEnum.Modal, App.MainForm);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillJournalCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			boolDontRespondToClick = true;
			// There are two different ways
			clsLoad.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND Status <> 'E' AND Status <> 'V' and  Status <> 'D' ORDER BY JournalNumber DESC", "twbd0000.vb1");
			cboJournal.Clear();
			while (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				cboJournal.AddItem(Strings.Format(clsLoad.Get_Fields("JournalNumber"), "0000") + " - " + clsLoad.Get_Fields_String("Description"));
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				cboJournal.ItemData(cboJournal.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("JournalNumber")));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboJournal.Items.Count > 0)
			{
				cboJournal.SelectedIndex = 0;
			}
			else
			{
				cboJournal.Enabled = false;
			}
			if (clsLoad.EndOfFile() && clsLoad.BeginningOfFile())
			{
				MessageBox.Show("There are no records to create entries for");
			}
		}

		private void FillBankCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			boolDontRespondToClick = true;
			// There are two different ways
			clsLoad.OpenRecordset("SELECT * FROM Banks WHERE rTrim(Name) <> '' ORDER BY Name", "twbd0000.vb1");
			cboBanks.Clear();
			while (!clsLoad.EndOfFile())
			{
				cboBanks.AddItem(clsLoad.Get_Fields_String("Name"));
				cboBanks.ItemData(cboBanks.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
				clsLoad.MoveNext();
			}
			boolDontRespondToClick = false;
			if (cboBanks.Items.Count > 0)
			{
				cboBanks.SelectedIndex = 0;
			}
			else
			{
				cboBanks.Enabled = false;
			}
			if (clsLoad.EndOfFile() && clsLoad.BeginningOfFile())
			{
				MessageBox.Show("There are no records to create entries for");
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBank = new clsDRWrapper();
			int intBank = 0;
			if (Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ACHFederalID"))) == "")
			{
				MessageBox.Show("You must setup ACH information in File Maintenance > Customize before you may continue.", "Invalid ACH Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ACHTownName"))) == "")
			{
				MessageBox.Show("You must setup ACH information in File Maintenance > Customize before you may continue.", "Invalid ACH Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateDestinationName"))) == "")
			{
				MessageBox.Show("You must setup ACH information in File Maintenance > Customize before you may continue.", "Invalid ACH Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateDestinationRoutingNumber"))) == "")
			{
				MessageBox.Show("You must setup ACH information in File Maintenance > Customize before you may continue.", "Invalid ACH Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (cmbACH.Text == "ACH File")
			{
				if (cboJournal.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a journal before you may continue.", "Invalid Journal", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				rsBank.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(cboJournal.ItemData(cboJournal.SelectedIndex)), "TWBD0000.vb1");
				if (rsBank.EndOfFile() != true && rsBank.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					intBank = FCConvert.ToInt32(rsBank.Get_Fields("BankNumber"));
				}
				else
				{
					intBank = 0;
				}
			}
			else
			{
				if (cboBanks.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a bank before you may continue.", "Invalid Bank", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				else
				{
					intBank = cboBanks.ItemData(cboBanks.SelectedIndex);
				}
			}
			rsBank.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(intBank), "TWBD0000.vb1");
			if (rsBank.EndOfFile() != true && rsBank.BeginningOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rsBank.Get_Fields_String("RoutingNumber"))) == "" || Strings.Trim(FCConvert.ToString(rsBank.Get_Fields_String("BankAccountNumber"))) == "")
				{
					// Or Trim(rsBank.Fields["AccountType"]) = ""
					MessageBox.Show("Before the ACH file can be created, a valid routing and bank account number for this bank must be set up under File Maintenance > Bank Names.", "Invalid Bank Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else
			{
				MessageBox.Show("Unable to find bank selected.", "Invalid Bank", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (!Information.IsDate(T2KEffectiveEntryDate.Text))
			{
				MessageBox.Show("You must enter an effective date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (cmbACH.Text == "ACH File")
			{
				if (CreateACHFiles())
				{
					Close();
				}
			}
			else
			{
				if (CreatePrenotes())
				{
					Close();
				}
			}
		}

		private bool CreatePrenotes()
		{
			bool CreatePrenotes = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsACHFileController tFileCon = new clsACHFileController();
				clsACHFile tFile;
				bool boolHasEntries;
				string strEffectiveEntryDate;
				boolHasEntries = false;
				bool boolSomethingHadEntries;
				boolSomethingHadEntries = false;
				string strFileNameList;
				bool boolForcePreNote;
				boolForcePreNote = cmbAllEFTVendors.SelectedIndex == 0;
				strFileNameList = "";
				strEffectiveEntryDate = T2KEffectiveEntryDate.Text;
				if (!Information.IsDate(strEffectiveEntryDate))
				{
					strEffectiveEntryDate = Strings.Format(DateTime.Now, "MM/dd/yyyy");
				}
				clsACHSetupController tLoad = new clsACHSetupController();
				tLoad.Load(tACHSetup, FCConvert.ToInt16(cboBanks.ItemData(cboBanks.SelectedIndex)));
				// do grouped recipients
				tFile = tFileCon.CreateACHFile(false, true, boolBalanced, DateAndTime.DateValue(strEffectiveEntryDate), FCConvert.ToInt16(cboBanks.ItemData(cboBanks.SelectedIndex)), 0, boolForcePreNote);
				if (tFile == null && tFileCon.LastError != "")
				{
					MessageBox.Show("Could not create file." + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return CreatePrenotes;
				}
				if (!(tFile == null))
				{
					tFile.FileInfo().FileName = Path.Combine(FCFileSystem.Statics.UserDataFolder, txtFileName.Text);
					if (tFile.Batches.Count > 0)
					{
						foreach (clsACHBatch tBatch in tFile.Batches)
						{
							if (tBatch.Details().Count > 0)
							{
								boolHasEntries = true;
								boolSomethingHadEntries = true;
								break;
							}
						}
						// tBatch
						if (boolHasEntries)
						{
							if (!tFileCon.WriteACHFile(ref tFile))
							{
							}
							else
							{
							}
						}
					}
				}
				CreatePrenotes = true;
				if (boolSomethingHadEntries)
				{
                    //FC:FINAL:AM:#3146 - download file
                    //MessageBox.Show("Prenote file created.", "Created Prenotes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(tFile.FileInfo().FileName);
					rptACH.InstancePtr.Init(DateAndTime.DateValue(strEffectiveEntryDate), 0, FCConvert.ToInt16(cboBanks.ItemData(cboBanks.SelectedIndex)), tFile, this.Modal);
				}
				else
				{
					MessageBox.Show("No file was created because no prenote entries were found", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return CreatePrenotes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreatePrenotes;
		}

		private bool CreateACHFiles()
		{
			bool CreateACHFiles = false;
			try
			{
				// On Error GoTo ErrorHandler
				// go through all trust records
				clsACHFileController tFileCon = new clsACHFileController();
				clsACHFile tFile;
				bool boolHasEntries;
				string strEffectiveEntryDate;
				boolHasEntries = false;
				bool boolSomethingHadEntries;
				boolSomethingHadEntries = false;
				string strFileNameList;
				clsDRWrapper rsMaster = new clsDRWrapper();
				strFileNameList = "";
				strEffectiveEntryDate = T2KEffectiveEntryDate.Text;
				if (!Information.IsDate(strEffectiveEntryDate))
				{
					MessageBox.Show("You must enter a valid effective entry date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CreateACHFiles;
				}
				rsMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(cboJournal.ItemData(cboJournal.SelectedIndex)), "TWBD0000.vb1");
				clsACHSetupController tLoad = new clsACHSetupController();
				// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
				tLoad.Load(tACHSetup, FCConvert.ToInt16(rsMaster.Get_Fields("BankNumber")));
				// do grouped recipients
				// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
				tFile = tFileCon.CreateACHFile(true, false, boolBalanced, DateAndTime.DateValue(strEffectiveEntryDate), FCConvert.ToInt16(rsMaster.Get_Fields("BankNumber")), 0, false, FCConvert.ToInt16(cboJournal.ItemData(cboJournal.SelectedIndex)));
				if (tFile == null)
				{
					if (tFileCon.LastError != string.Empty)
					{
						MessageBox.Show("Could not create file." + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return CreateACHFiles;
					}
				}
				else
				{
					tFile.FileInfo().FileName = FCFileSystem.Statics.UserDataFolder + "\\" + txtFileName.Text;
					if (tFile.Batches.Count > 0)
					{
						foreach (clsACHBatch tBatch in tFile.Batches)
						{
							if (tBatch.Details().Count > 0)
							{
								boolHasEntries = true;
								boolSomethingHadEntries = true;
								break;
							}
						}
						// tBatch
						if (boolHasEntries)
						{
							if (!tFileCon.WriteACHFile(ref tFile))
							{
								MessageBox.Show("File creation failed for " + tFile.FileInfo().FileName + "\r\n" + tFileCon.LastError, "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Information);
								return CreateACHFiles;
							}
							else
							{
							}
						}
					}
				}
				CreateACHFiles = true;
				if (boolSomethingHadEntries)
				{
					MessageBox.Show("ACH file created.", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					rptACH.InstancePtr.Init(DateAndTime.DateValue(strEffectiveEntryDate), FCConvert.ToInt16(cboJournal.ItemData(cboJournal.SelectedIndex)), FCConvert.ToInt16(rsMaster.Get_Fields("BankNumber")), tFile, this.Modal);
				}
				else
				{
					MessageBox.Show("No ACH file created. There were no EFT entries or vendors found for this period.", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return CreateACHFiles;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateACHFiles;
		}

		private void cmbACH_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbACH.Text == "ACH File")
			{
				optACH_CheckedChanged(sender, e);
			}
			else
			{
				optPrenote_CheckedChanged(sender, e);
			}
		}

		private void optACH_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrenoteFile.Enabled = false;
			if (cmbAllEFTVendors.Items.Contains("All EFT Vendors"))
			{
				cmbAllEFTVendors.Items.Remove("All EFT Vendors");
			}
			fraACHFile.Enabled = true;
			lblJournal.Enabled = true;
			if (cboJournal.Items.Count > 0)
			{
				cboJournal.SelectedIndex = 0;
			}
			cboJournal.Enabled = true;
			cboBanks.SelectedIndex = -1;
			cboBanks.Enabled = false;
			lblBank.Enabled = false;
		}

		private void optPrenote_CheckedChanged(object sender, System.EventArgs e)
		{
			fraPrenoteFile.Enabled = true;
			if (!cmbAllEFTVendors.Items.Contains("All EFT Vendors"))
			{
				cmbAllEFTVendors.Items.Insert(0, "All EFT Vendors");
			}
			if (!cmbAllEFTVendors.Items.Contains("Only EFT Vendors marked as Prenote"))
			{
				cmbAllEFTVendors.Items.Insert(1, "Only EFT Vendors marked as Prenote");
			}
			cmbAllEFTVendors.SelectedIndex = 1;
			fraACHFile.Enabled = false;
			lblJournal.Enabled = false;
			cboJournal.SelectedIndex = -1;
			cboJournal.Enabled = false;
			if (cboBanks.Items.Count > 0)
			{
				cboBanks.SelectedIndex = 0;
			}
			lblBank.Enabled = true;
			cboBanks.Enabled = true;
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(sender, e);
		}
	}
}
