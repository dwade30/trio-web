﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptEncJournals.
	/// </summary>
	public partial class rptEncJournals : BaseSectionReport
	{
		public static rptEncJournals InstancePtr
		{
			get
			{
				return (rptEncJournals)Sys.GetInstance(typeof(rptEncJournals));
			}
		}

		protected rptEncJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsGeneralInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEncJournals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitTotal;
		// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditTotal;
		bool blnFirstControl;

		public rptEncJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsInfo.EndOfFile())
			{
				TryAgain:
				;
				rsGeneralInfo.MoveNext();
				if (rsGeneralInfo.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
					rsInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + rsGeneralInfo.Get_Fields_Int32("ID"));
					if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
					{
						rsInfo.MoveLast();
						rsInfo.MoveFirst();
					}
					else
					{
						goto TryAgain;
					}
				}
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            using (clsDRWrapper rsDesc = new clsDRWrapper())
            {
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " +
                                     FCConvert.ToString(Conversion.Val(this.UserData)));
                if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                {
                    lblDescription.Text = rsDesc.Get_Fields_String("Description");
                }
                else
                {
                    lblDescription.Text = "UNKNOWN";
                }

                Label6.Text = "Journal No. " + this.UserData + "       Post Date: " +
                              DateTime.Today.ToShortDateString() + "       Type: EN";
                if (rptPosting.InstancePtr.blnClosingEntry)
                {
                    rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " +
                                                FCConvert.ToString(Conversion.Val(this.UserData)));
                }
                else
                {
                    rsGeneralInfo.OpenRecordset(("SELECT * FROM Encumbrances WHERE JournalNumber = " +
                                                 FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " +
                                                 frmPosting.InstancePtr.vsJournals.TextMatrix(
                                                     rptPosting.InstancePtr.intRowHolder, 2)) + " ORDER BY ID");
                }

                rsInfo.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " +
                                     rsGeneralInfo.Get_Fields_Int32("ID"));
                if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
                {
                    rsInfo.MoveLast();
                    rsInfo.MoveFirst();
                }

                if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
                {
                    rsGeneralInfo.MoveLast();
                    rsGeneralInfo.MoveFirst();
                }

                curDebitTotal = 0;
                curCreditTotal = 0;
                blnFirstControl = false;
                Line2.Visible = false;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnFirstControl == false)
			{
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
				{
					Line2.Visible = true;
					blnFirstControl = true;
				}
			}
			else
			{
				if (Line2.Visible == true)
				{
					Line2.Visible = false;
				}
			}
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			Field1.Text = Strings.Format(rsGeneralInfo.Get_Fields("Period"), "00");
			Field3.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			Field6.Text = rsInfo.Get_Fields_String("Account");
			Field2.Text = Strings.Format(rsGeneralInfo.Get_Fields_DateTime("EncumbrancesDate"), "MM/dd/yyyy");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				Field14.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				Field8.Text = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
				Field14.Text = "";
			}
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) > 0)
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curDebitTotal += rsInfo.Get_Fields("Amount");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					curCreditTotal += (rsInfo.Get_Fields("Amount") * -1);
				}
			}
			// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
			Field13.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("PO"));
			Field9.Text = rsInfo.Get_Fields_String("Project");
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
			{
				Field11.Text = "";
			}
			else
			{
				Field11.Text = modValidateAccount.GetFormat_6(rsGeneralInfo.Get_Fields_Int32("VendorNumber"), 5);
			}
			rsInfo.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			rptSubTotals.Report = rptJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptEncJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptEncJournals.Icon	= "rptEncJournals.dsx":0000";
			//rptEncJournals.Left	= 0;
			//rptEncJournals.Top	= 0;
			//rptEncJournals.Width	= 11880;
			//rptEncJournals.Height	= 8595;
			//rptEncJournals.StartUpPosition	= 3;
			//rptEncJournals.SectionData	= "rptEncJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
