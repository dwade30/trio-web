﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantRecapAccountOrder.
	/// </summary>
	public partial class rptWarrantRecapAccountOrder : BaseSectionReport
	{
		public static rptWarrantRecapAccountOrder InstancePtr
		{
			get
			{
				return (rptWarrantRecapAccountOrder)Sys.GetInstance(typeof(rptWarrantRecapAccountOrder));
			}
		}

		protected rptWarrantRecapAccountOrder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantRecapAccountOrder	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		Decimal curWarrantTotal;
		Decimal curPrepaidTotal;
		Decimal curCurrentTotal;
		bool blnFirstRecord;
		bool blnTempVendors;
		//clsDRWrapper rsTempVendorInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		//clsDRWrapper rsJournalInfo = new clsDRWrapper();
		//clsDRWrapper rsDetailInfo = new clsDRWrapper();
		int intTempVendorCounter;
		string strSQL = "";
		// vbPorter upgrade warning: curAccountTotal As Decimal	OnWrite(short, Decimal)
		Decimal curAccountTotal;

		public rptWarrantRecapAccountOrder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Warrant Recap";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                if (blnFirstRecord)
                {
                    blnFirstRecord = false;
                    if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
                    {
                        if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) != "P")
                        {
                            // rsVendorInfo.Edit
                            // rsVendorInfo.Fields["Status"] = "X"
                            // rsVendorInfo.Update True
                            rsTemp.Execute(
                                "update apjournal set status = 'X' where id = " + rsVendorInfo.Get_Fields_Int32("id"),
                                "Budgetary");
                        }
                    }
                }
                else
                {
                    rsVendorInfo.MoveNext();
                    if (rsVendorInfo.EndOfFile() != true)
                    {
                        if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
                        {
                            if (FCConvert.ToString(rsVendorInfo.Get_Fields_String("Status")) != "P")
                            {
                                // rsVendorInfo.Edit
                                // rsVendorInfo.Fields["Status"] = "X"
                                // rsVendorInfo.Update True
                                rsTemp.Execute(
                                    "update apjournal set status = 'X' where id = " +
                                    rsVendorInfo.Get_Fields_Int32("id"), "Budgetary");
                            }
                        }

                        eArgs.EOF = false;
                    }
                    else
                    {
                        eArgs.EOF = true;
                    }
                }

                if (!eArgs.EOF)
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    this.Fields["Binder"].Value = rsVendorInfo.Get_Fields("Account");
                }
            }
        }

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Reprint WHERE Type = 'R' ORDER BY ReportOrder");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ReportOrder")) < 9)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReportOrder", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportOrder")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						else
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("Type", "R");
				rsInfo.Set_Fields("ReportOrder", 1);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsInfo.Set_Fields("WarrantNumber", frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant"));
				rsInfo.Update(true);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'W' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant") + "')");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					do
					{
						rsMasterJournal.Edit();
						rsMasterJournal.Set_Fields("Status", "X");
						rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
						rsMasterJournal.Update();
						// True
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
				}
			}
			frmWarrantRecap.InstancePtr.Unload();
			rsInfo.Dispose();
			rsMasterJournal.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				Label6.Text = "Warrant " + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant");
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				strSQL = "APJournal.Warrant = '" + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant") + "' ";
				Label21.Visible = false;
			}
			else
			{
				Label6.Text = "Warrant " + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrantRecap.InstancePtr.cboReport.Text, 9, 4)));
				strSQL = "APJournal.Warrant = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrantRecap.InstancePtr.cboReport.Text, 9, 4))) + "' ";
				Label21.Visible = true;
			}
			blnFirstRecord = true;
			blnTempVendors = false;
			// Me.Document.Printer.PrinterName = frmWarrantRecap.strPrinterName
			FillTemp();
			curAccountTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsVendorName = new clsDRWrapper())
            {
                if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
                {
                    fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) +
                                     " " + rsVendorInfo.Get_Fields_String("TempVendorName");
                }
                else
                {
                    rsVendorName.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                               rsVendorInfo.Get_Fields_Int32("VendorNumber"));
                    if (rsVendorName.EndOfFile() != true && rsVendorName.BeginningOfFile() != true)
                    {
                        fldVendor.Text =
                            modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " +
                            rsVendorName.Get_Fields_String("CheckName");
                    }
                    else
                    {
                        fldVendor.Text =
                            modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " +
                            "UNKNOWN";
                    }
                }

                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                fldAmount.Text =
                    Strings.Format((rsVendorInfo.Get_Fields("Amount") - rsVendorInfo.Get_Fields("Discount")),
                        "#,##0.00");
                ShowTitle();
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                curWarrantTotal += rsVendorInfo.Get_Fields("Amount") - rsVendorInfo.Get_Fields("Discount");
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                curAccountTotal += rsVendorInfo.Get_Fields("Amount") - rsVendorInfo.Get_Fields("Discount");
                if (FCConvert.ToBoolean(rsVendorInfo.Get_Fields_Boolean("PrepaidCheck")))
                {
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                    curPrepaidTotal += rsVendorInfo.Get_Fields("Amount") - rsVendorInfo.Get_Fields("Discount");
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
                    curCurrentTotal += rsVendorInfo.Get_Fields("Amount") - rsVendorInfo.Get_Fields("Discount");
                }
            }
        }

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldCurrentTotal.Text = Strings.Format(curCurrentTotal, "#,##0.00");
			fldPrepaidTotal.Text = Strings.Format(curPrepaidTotal, "#,##0.00");
			fldTotal.Text = Strings.Format(curWarrantTotal, "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldAccountTotal.Text = Strings.Format(curAccountTotal, "#,##0.00");
			curAccountTotal = 0;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ShowTitle()
		{
			int intHolder = 0;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			if (Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields("Account")), 1) == "E" || Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields("Account")), 1) == "R" || Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields("Account")), 1) == "G")
			{
				if (frmWarrantRecap.InstancePtr.intPreviewChoice == 1)
				{
					fldTitle.Text = "";
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modAccountTitle.DetermineAccountTitle(rsVendorInfo.Get_Fields("Account"), ref frmWarrantRecap.InstancePtr.lblTitle);
				}
				if (frmWarrantRecap.InstancePtr.intPreviewChoice == 2)
				{
					fldTitle.Text = frmWarrantRecap.InstancePtr.lblTitle.Text;
				}
				else if (frmWarrantRecap.InstancePtr.intPreviewChoice == 3)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Left(frmWarrantRecap.InstancePtr.lblTitle.Text, intHolder - 1);
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsVendorInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder - 1);
					}
				}
			}
		}

		private void FillTemp()
		{
			rsVendorInfo.OpenRecordset("SELECT apjournal.id, VendorNumber, Status, TempVendorName, APJournalDetail.Amount as Amount, APJournalDetail.Discount as Discount, Account, Warrant, PrepaidCheck FROM APJournal INNER JOIN APJournalDetail ON APJournal.ID = APJournalDetail.APJournalID WHERE " + strSQL + " ORDER BY Account, VendorNumber");
		}

		
	}
}
