﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmOutlineLedgerQuery.
	/// </summary>
	partial class frmOutlineLedgerQuery : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtDelete;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdDelete;
		public fecherFoundation.FCLabel lblDelete;
		public fecherFoundation.FCGrid vs1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutlineLedgerQuery));
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtDelete = new fecherFoundation.FCTextBox();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.lblDelete = new fecherFoundation.FCLabel();
            this.vs1 = new fecherFoundation.FCGrid();
            this.cmdFundDelete = new fecherFoundation.FCButton();
            this.cmdFundAdd = new fecherFoundation.FCButton();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFundDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFundAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 498);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFundDelete);
            this.TopPanel.Controls.Add(this.cmdFundAdd);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFundAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFundDelete, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(235, 28);
            this.HeaderText.Text = "General Ledger Titles";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.FromName("@window");
            this.Frame1.Controls.Add(this.txtDelete);
            this.Frame1.Controls.Add(this.cmdCancel);
            this.Frame1.Controls.Add(this.cmdDelete);
            this.Frame1.Controls.Add(this.lblDelete);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(270, 186);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Delete Fund";
            this.Frame1.Visible = false;
            // 
            // txtDelete
            // 
            this.txtDelete.BackColor = System.Drawing.SystemColors.Window;
            this.txtDelete.Location = new System.Drawing.Point(20, 66);
            this.txtDelete.Name = "txtDelete";
            this.txtDelete.Size = new System.Drawing.Size(88, 40);
            this.txtDelete.TabIndex = 4;
            this.txtDelete.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtDelete.Validating += new System.ComponentModel.CancelEventHandler(this.txtDelete_Validating);
            this.txtDelete.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDelete_KeyPress);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(105, 126);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(70, 40);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.AppearanceKey = "actionButton";
            this.cmdDelete.Location = new System.Drawing.Point(20, 126);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(70, 40);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // lblDelete
            // 
            this.lblDelete.Location = new System.Drawing.Point(20, 30);
            this.lblDelete.Name = "lblDelete";
            this.lblDelete.Size = new System.Drawing.Size(230, 16);
            this.lblDelete.TabIndex = 5;
            this.lblDelete.Text = "WHICH FUND DO YOU WISH TO DELETE";
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 9;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.vs1.ExtendLastCol = true;
            this.vs1.Location = new System.Drawing.Point(30, 30);
            this.vs1.Name = "vs1";
            this.vs1.Size = new System.Drawing.Size(1045, 468);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 0;
            this.vs1.Visible = false;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
            this.vs1.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.Vs1_EditingControlShowing);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ClickEvent);
            this.vs1.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
            this.vs1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            // 
            // cmdFundDelete
            // 
            this.cmdFundDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFundDelete.Location = new System.Drawing.Point(1006, 29);
            this.cmdFundDelete.Name = "cmdFundDelete";
            this.cmdFundDelete.Size = new System.Drawing.Size(96, 24);
            this.cmdFundDelete.TabIndex = 11;
            this.cmdFundDelete.Text = "Delete Fund";
            this.cmdFundDelete.Click += new System.EventHandler(this.mnuFundDelete_Click);
            // 
            // cmdFundAdd
            // 
            this.cmdFundAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFundAdd.Location = new System.Drawing.Point(919, 29);
            this.cmdFundAdd.Name = "cmdFundAdd";
            this.cmdFundAdd.Size = new System.Drawing.Size(81, 24);
            this.cmdFundAdd.TabIndex = 10;
            this.cmdFundAdd.Text = "Add Fund";
            this.cmdFundAdd.Click += new System.EventHandler(this.mnuFundAdd_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(254, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
            this.cmdProcessSave.TabIndex = 3;
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmOutlineLedgerQuery
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmOutlineLedgerQuery";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "General Ledger Titles";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmOutlineLedgerQuery_Load);
            this.Activated += new System.EventHandler(this.frmOutlineLedgerQuery_Activated);
            this.Resize += new System.EventHandler(this.frmOutlineLedgerQuery_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOutlineLedgerQuery_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmOutlineLedgerQuery_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFundDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFundAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}

        
        #endregion

        public FCButton cmdFundDelete;
		public FCButton cmdFundAdd;
		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
