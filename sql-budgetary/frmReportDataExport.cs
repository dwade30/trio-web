﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReportDataExport.
	/// </summary>
	public partial class frmReportDataExport : BaseForm
	{
		public frmReportDataExport(string exportFileName)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx(exportFileName);
		}

		private void InitializeComponentEx(string exportFileName)
		{
			this.txtFilename.Text = exportFileName;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReportDataExport InstancePtr
		{
			get
			{
				return (frmReportDataExport)Sys.GetInstance(typeof(frmReportDataExport));
			}
		}

		protected frmReportDataExport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolUpdating;
		cReportExportView theView;

		public void SetViewModel(ref cReportExportView vm)
		{
			theView = vm;
			//FC:FINAL:ASZ:#701 - adding event handler to theView 
			theView.ValuesChanged -= new cReportExportView.ValuesChangedEventHandler(TheView_ValuesChanged);
			theView.ValuesChanged += new cReportExportView.ValuesChangedEventHandler(TheView_ValuesChanged);
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:BBE:#i701 - save file to temp folder, and download to client
			theView.Filename = txtFilename.Text;
			theView.Browse();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			theView = null;
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			theView.Browse();
			theView.ExportData();
			if (theView.HadError)
			{
				MessageBox.Show("Error " + FCConvert.ToString(theView.LastErrorNumber) + " " + theView.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				//FC:FINAL:BBE:#i701 - download the export file
				//MessageBox.Show("File saved as " + theView.Filename, "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(theView.Filename, txtFilename.Text + txtExtension.Text);
				Close();
			}
		}

		private void frmReportDataExport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReportDataExport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReportDataExport.FillStyle	= 0;
			//frmReportDataExport.ScaleWidth	= 6030;
			//frmReportDataExport.ScaleHeight	= 3765;
			//frmReportDataExport.LinkTopic	= "Form2";
			//frmReportDataExport.LockControls	= -1  'True;
			//frmReportDataExport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// SetFixedSize Me, TRIOWINDOWSIZEMEDIUM
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void ReShow()
		{
			if (!(theView == null))
			{
				boolUpdating = true;
				if (theView.FormatType == 1)
				{
					cmbCsv.SelectedIndex = 1;
					txtExtension.Text = ".txt";
				}
				else
				{
					cmbCsv.SelectedIndex = 0;
					txtExtension.Text = ".csv";
				}
				//txtFilename.Text = theView.Filename;
				boolUpdating = false;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void CmbCsv_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbCsv.SelectedIndex == 0)
			{
				optCsv_CheckedChanged(sender, e);
			}
			else if (cmbCsv.SelectedIndex == 1)
			{
				optTabs_CheckedChanged(sender, e);
			}
		}

		private void optCsv_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				theView.FormatType = 0;
				txtExtension.Text = ".csv";
			}
		}

		private void optTabs_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!boolUpdating)
			{
				theView.FormatType = 1;
				txtExtension.Text = ".txt";
			}
		}

		private void TheView_ValuesChanged()
		{
			ReShow();
		}
	}
}
