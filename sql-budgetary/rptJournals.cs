﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournals.
	/// </summary>
	public partial class rptJournals : BaseSectionReport
	{
		public static rptJournals InstancePtr
		{
			get
			{
				return (rptJournals)Sys.GetInstance(typeof(rptJournals));
			}
		}

		protected rptJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsControlInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptJournals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		// vbPorter upgrade warning: curDebitTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitTotal;
		// vbPorter upgrade warning: curCreditTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditTotal;
		bool blnFirstControl;
		clsDRWrapper rsControlInfo = new clsDRWrapper();
		bool blnFirstRecord;
		bool blnLineShown;

		public rptJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckAgain:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (blnFirstControl)
				{
					eArgs.EOF = rsControlInfo.EndOfFile();
					if (!eArgs.EOF)
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) == 0)
						{
							goto CheckAgain;
						}
					}
				}
				else
				{
					if (rsInfo.EndOfFile())
					{
						blnFirstRecord = true;
						blnFirstControl = true;
						goto CheckAgain;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) == 0)
						{
							goto CheckAgain;
						}
					}
				}
			}
			else
			{
				if (blnFirstControl)
				{
					rsControlInfo.MoveNext();
					eArgs.EOF = rsControlInfo.EndOfFile();
					if (!eArgs.EOF)
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) == 0)
						{
							goto CheckAgain;
						}
					}
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile())
					{
						blnFirstRecord = true;
						blnFirstControl = true;
						goto CheckAgain;
					}
					else
					{
						eArgs.EOF = false;
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) == 0)
						{
							goto CheckAgain;
						}
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDesc = new clsDRWrapper();
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				//Application.DoEvents();
				if (rptPosting.InstancePtr.blnClosingEntry)
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB <> 'L' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)));
					rsControlInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB = 'L' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)));
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB <> 'L' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
					rsControlInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB = 'L' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)) + " AND Period = " + frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 2));
				}
				if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
				{
					rsInfo.MoveLast();
					rsInfo.MoveFirst();
				}
				blnFirstRecord = true;
				blnLineShown = false;
				rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(this.UserData)));
				if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
				{
					lblDescription.Text = rsDesc.Get_Fields_String("Description");
				}
				else
				{
					lblDescription.Text = "UNKNOWN";
				}
				if (rptPosting.InstancePtr.blnClosingEntry)
				{
					Label6.Text = "Journal No. " + this.UserData + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: GJ";
				}
				else
				{
					Label6.Text = "Journal No. " + this.UserData + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: " + Strings.Left(frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 3), 2);
				}
				curDebitTotal = 0;
				curCreditTotal = 0;
				blnFirstControl = false;
				Line2.Visible = false;
				rsDesc.Dispose();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Journal Start Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (blnFirstControl)
				{
					if (!blnLineShown)
					{
						Line2.Visible = true;
						blnLineShown = true;
					}
					else
					{
						Line2.Visible = false;
					}
				}
				if (!blnFirstControl)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					Field1.Text = Strings.Format(rsInfo.Get_Fields("Period"), "00");
					Field2.Text = Strings.Format(rsInfo.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yyyy");
					Field3.Text = rsInfo.Get_Fields_String("Description");
					Field4.Text = rsInfo.Get_Fields_String("RCB");
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "W")
					{
						Field5.Text = "C";
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						Field5.Text = FCConvert.ToString(rsInfo.Get_Fields("Type"));
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					Field6.Text = rsInfo.Get_Fields_String("Account");
					Field7.Text = rsInfo.Get_Fields_String("Project");
					if (FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "E" && FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (rsInfo.Get_Fields("Amount") * -1);
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL" && (FCConvert.ToString(rsInfo.Get_Fields("Type")) != "P" || FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "E"))
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += rsInfo.Get_Fields("Amount");
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += rsInfo.Get_Fields("Amount");
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (rsInfo.Get_Fields("Amount") * -1);
							}
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					Field1.Text = Strings.Format(rsControlInfo.Get_Fields("Period"), "00");
					Field2.Text = rsControlInfo.Get_Fields_DateTime("JournalEntriesDate").ToString();
					Field3.Text = rsControlInfo.Get_Fields_String("Description");
					Field4.Text = rsControlInfo.Get_Fields_String("RCB");
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsControlInfo.Get_Fields("Type")) == "W")
					{
						Field5.Text = "C";
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						Field5.Text = rsControlInfo.Get_Fields("Type");
					}
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					Field6.Text = rsControlInfo.Get_Fields_String("Account");
					Field7.Text = rsControlInfo.Get_Fields_String("Project");
					if (FCConvert.ToString(rsControlInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsControlInfo.Get_Fields_String("RCB")) != "E" && FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) < 0)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsControlInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (rsControlInfo.Get_Fields("Amount") * -1);
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsControlInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += rsControlInfo.Get_Fields("Amount");
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) < 0)
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsControlInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += rsControlInfo.Get_Fields("Amount");
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsControlInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (rsControlInfo.Get_Fields("Amount") * -1);
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Journal Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			//Application.DoEvents();
			if (rptPosting.InstancePtr.blnClosingEntry)
			{
				if (curDebitTotal != curCreditTotal)
				{
					lblBalance.Visible = true;
				}
				else
				{
					lblBalance.Visible = false;
				}
			}
			else
			{
				if (curDebitTotal != curCreditTotal || Strings.Right(frmPosting.InstancePtr.vsJournals.TextMatrix(rptPosting.InstancePtr.intRowHolder, 3), 5) == "(OBF)")
				{
					lblBalance.Visible = true;
				}
				else
				{
					lblBalance.Visible = false;
				}
			}
			fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			rptSubTotals.Report = rptJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptJournals.Icon	= "rptJournals.dsx":0000";
			//rptJournals.Left	= 0;
			//rptJournals.Top	= 0;
			//rptJournals.Width	= 11880;
			//rptJournals.Height	= 8595;
			//rptJournals.StartUpPosition	= 3;
			//rptJournals.SectionData	= "rptJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
