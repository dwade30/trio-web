//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAddCheckDep.
	/// </summary>
	partial class frmAddCheckDep : BaseForm
	{
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblBank;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCToolStripMenuItem mnuRows;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddCheckDep));
            this.vs1 = new fecherFoundation.FCGrid();
            this.lblBank = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRows = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.vs1Header = new Wisej.Web.Panel();
            this.vs1HeaderText = new Wisej.Web.Label();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.vs1Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 555);
            this.BottomPanel.Size = new System.Drawing.Size(754, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vs1Header);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.lblBank);
            this.ClientArea.Size = new System.Drawing.Size(754, 495);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(754, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(236, 30);
            this.HeaderText.Text = "Add Check / Deposit";
            // 
            // vs1
            // 
            this.vs1.AllowSelection = false;
            this.vs1.AllowUserToResizeColumns = false;
            this.vs1.AllowUserToResizeRows = false;
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
            this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
            this.vs1.BackColorBkg = System.Drawing.Color.Empty;
            this.vs1.BackColorFixed = System.Drawing.Color.Empty;
            this.vs1.BackColorSel = System.Drawing.Color.Empty;
            this.vs1.Cols = 8;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vs1.ColumnHeadersHeight = 30;
            this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.vs1.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
            this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vs1.FixedCols = 0;
            this.vs1.ExtendLastCol = true;
            this.vs1.FixedRows = 0;
            this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
            this.vs1.FrozenCols = 0;
            this.vs1.GridColor = System.Drawing.Color.Empty;
            this.vs1.Location = new System.Drawing.Point(30, 96);
            this.vs1.Name = "vs1";
            this.vs1.ReadOnly = true;
            this.vs1.RowHeadersVisible = false;
            this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vs1.RowHeightMin = 0;
            this.vs1.Rows = 49;
            this.vs1.ShowColumnVisibilityMenu = false;
            this.vs1.Size = new System.Drawing.Size(694, 381);
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 1;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
            this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vs1_MouseDownEvent);
            this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(Vs1_EditingControlShowing);
            // 
            // lblBank
            // 
            this.lblBank.Location = new System.Drawing.Point(30, 30);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(282, 16);
            this.lblBank.TabIndex = 0;
            this.lblBank.Text = "LABEL1";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
                this.mnuFileSave,
                this.mnuProcessSave,
                this.Seperator,
                this.mnuProcessQuit
            });
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save  ";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit ";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // mnuRows
            // 
            this.mnuRows.Index = -1;
            this.mnuRows.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
                this.mnuAddRow,
                this.mnuDeleteRow
            });
            this.mnuRows.Name = "mnuRows";
            this.mnuRows.Text = "Rows";
            this.mnuRows.Visible = false;
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Text = "Insert Row";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 1;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Text = "Delete Row";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(333, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 0;
            //FC:FINAL:MSH - issue #701: restore missed shortcut property for handling F12 key pressing
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // vs1Header
            // 
            this.vs1Header.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
            this.vs1Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.vs1Header.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.vs1Header.Controls.Add(this.vs1HeaderText);
            this.vs1Header.Location = new System.Drawing.Point(30, 66);
            this.vs1Header.Name = "vs1Header";
            this.vs1Header.Size = new System.Drawing.Size(694, 30);
            this.vs1Header.TabIndex = 2;
            this.vs1Header.Text = "-- STATUS --";
            // 
            // vs1HeaderText
            // 
            this.vs1HeaderText.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
            this.vs1HeaderText.Location = new System.Drawing.Point(0, 0);
            this.vs1HeaderText.Name = "vs1HeaderText";
            this.vs1HeaderText.Size = new System.Drawing.Size(693, 29);
            this.vs1HeaderText.TabIndex = 0;
            this.vs1HeaderText.Text = "-- STATUS --";
            this.vs1HeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmAddCheckDep
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(754, 663);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAddCheckDep";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add Check / Deposit";
            this.Load += new System.EventHandler(this.frmAddCheckDep_Load);
            this.Activated += new System.EventHandler(this.frmAddCheckDep_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAddCheckDep_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddCheckDep_KeyPress);
            this.Resize += new System.EventHandler(this.frmAddCheckDep_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.vs1Header.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion

        public FCButton cmdProcess;
		private Panel vs1Header;
		private Label vs1HeaderText;
	}
}