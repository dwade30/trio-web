﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frm1099Restore.
	/// </summary>
	partial class frm1099Restore : BaseForm
	{
		public fecherFoundation.FCButton cmdRestore;
		public fecherFoundation.FCComboBox cboYear;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm1099Restore));
			this.cmdRestore = new fecherFoundation.FCButton();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRestore)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 240);
			this.BottomPanel.Size = new System.Drawing.Size(498, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdRestore);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(498, 180);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(498, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(317, 30);
			this.HeaderText.Text = "Restore Archive Extract File";
			// 
			// cmdRestore
			// 
			this.cmdRestore.AppearanceKey = "acceptButton";
			this.cmdRestore.Location = new System.Drawing.Point(30, 126);
			this.cmdRestore.Name = "cmdRestore";
			this.cmdRestore.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdRestore.Size = new System.Drawing.Size(103, 48);
			this.cmdRestore.TabIndex = 2;
			this.cmdRestore.Text = "Restore";
			this.cmdRestore.Click += new System.EventHandler(this.cmdRestore_Click);
			// 
			// cboYear
			// 
			this.cboYear.AutoSize = false;
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboYear.FormattingEnabled = true;
			this.cboYear.Location = new System.Drawing.Point(30, 66);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(103, 40);
			this.cboYear.TabIndex = 0;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(361, 16);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "PLEASE SELECT THE YEAR\'S DATA YOU WOULD LIKE RESTORED";
			// 
			// frm1099Restore
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(498, 348);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frm1099Restore";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Restore Archive Extract File";
			this.Load += new System.EventHandler(this.frm1099Restore_Load);
			this.Activated += new System.EventHandler(this.frm1099Restore_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frm1099Restore_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRestore)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
