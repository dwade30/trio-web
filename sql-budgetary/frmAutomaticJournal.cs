﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAutomaticJournals.
	/// </summary>
	public partial class frmAutomaticJournals : BaseForm
	{
		public frmAutomaticJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAutomaticJournals InstancePtr
		{
			get
			{
				return (frmAutomaticJournals)Sys.GetInstance(typeof(frmAutomaticJournals));
			}
		}

		protected frmAutomaticJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         12/27/02
		// This form will be used to complete journals that came
		// from cash receipting or payroll and could not be completed
		// from those modules because of journal locks
		// ********************************************************
		int CompleteCol;
		int KeyCol;
		int TypeCol;
		// variables for grid columns
		int AmountCol;
		int CountCol;
		int PeriodCol;
		clsDRWrapper rsJournals = new clsDRWrapper();
		bool blnTaxCommitmentExists;
		bool blnSupplementalExists;
		int lngYear;
		string strRECommitmentAccount = "";
		string strPPCommitmentAccount = "";
		string strRESupplementalAccount = "";
		string strPPSupplementalAccount = "";
		string strREReceivableAccount = "";
		string strPPReceivableAccount = "";
		string strRESuppReceivableAccount = "";
		string strPPSuppReceivableAccount = "";
		string strLienReceivableAccount = "";
		string strPPFund = "";
		string strREFund = "";
		string strPPSuppFund = "";
		string strRESuppFund = "";

		private void frmAutomaticJournals_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (vsJournals.Rows == 1)
			{
				Close();
			}
			this.Refresh();
		}

		private void frmAutomaticJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAutomaticJournals.FillStyle	= 0;
			//frmAutomaticJournals.ScaleWidth	= 9045;
			//frmAutomaticJournals.ScaleHeight	= 6930;
			//frmAutomaticJournals.LinkTopic	= "Form2";
			//frmAutomaticJournals.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter = 0;
			clsDRWrapper rsInfo = new clsDRWrapper();
			// initialize column variables and grid values
			CompleteCol = 0;
			KeyCol = 1;
			PeriodCol = 2;
			TypeCol = 3;
			AmountCol = 4;
			CountCol = 5;
			//vsJournals.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsJournals.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsJournals.TextMatrix(0, CompleteCol, "Complete");
			vsJournals.TextMatrix(0, KeyCol, "Jrnl");
			vsJournals.TextMatrix(0, PeriodCol, "Period");
			vsJournals.TextMatrix(0, TypeCol, "Type");
			vsJournals.TextMatrix(0, AmountCol, "Amount");
			vsJournals.TextMatrix(0, CountCol, "Entries");
			vsJournals.ColWidth(CompleteCol, 900);
			vsJournals.ColWidth(KeyCol, 0);
			vsJournals.ColWidth(PeriodCol, 800);
			vsJournals.ColWidth(TypeCol, 900);
			vsJournals.ColWidth(AmountCol, 1500);
			vsJournals.ColWidth(CountCol, 800);
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RM'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Real Estate Commitment Account before you may continue.", "No Real Estate Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strRECommitmentAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
				}
			}
			else
			{
				MessageBox.Show("You must set up a Real Estate Commitment Account before you may continue.", "No Real Estate Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strRECommitmentAccount) + "'");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strRECommitmentAccount) + "' AND Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				strREFund = FCConvert.ToString(rsInfo.Get_Fields("Fund"));
			}
			else
			{
				MessageBox.Show("Could not find the Real Estate Fund", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'LR'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Lien Receivable Account before you may continue.", "No Lien Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strLienReceivableAccount = "G " + strREFund + "-" + rsInfo.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2);
				}
			}
			else
			{
				MessageBox.Show("You must set up a Lien Receivable Account before you may continue.", "No Lien Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PM'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Personal Property Commitment Account before you may continue.", "No Personal Property Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strPPCommitmentAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
				}
			}
			else
			{
				MessageBox.Show("You must set up a Personal Property Commitment Account before you may continue.", "No Personal Property Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strPPCommitmentAccount) + "'");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strPPCommitmentAccount) + "' AND Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				strPPFund = FCConvert.ToString(rsInfo.Get_Fields("Fund"));
			}
			else
			{
				MessageBox.Show("Could not find the Personal Property Fund", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PS'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Personal Property Supplemental Account before you may continue.", "No Personal Property Supplemental Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strPPSupplementalAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
				}
			}
			else
			{
				MessageBox.Show("You must set up a Personal Property Supplemental Account before you may continue.", "No Perosnal Property Supplemental Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strPPSupplementalAccount) + "'");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strPPSupplementalAccount) + "' AND Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				strPPSuppFund = FCConvert.ToString(rsInfo.Get_Fields("Fund"));
			}
			else
			{
				MessageBox.Show("Could not find the Personal Property Fund", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RS'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Real Estate Supplemental Account before you may continue.", "No Real Estate Supplemental Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strRESupplementalAccount = FCConvert.ToString(rsInfo.Get_Fields("Account"));
				}
			}
			else
			{
				MessageBox.Show("You must set up a Real Estate Supplemental Account before you may continue.", "No Real Estate Supplemental Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strRESupplementalAccount) + "'");
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + modBudgetaryAccounting.GetDepartment(strRESupplementalAccount) + "' AND Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'");
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				strRESuppFund = FCConvert.ToString(rsInfo.Get_Fields("Fund"));
			}
			else
			{
				MessageBox.Show("Could not find the Real Estate Fund", "No Records Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'RR'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Real Estate Receivable Account before you may continue.", "No Real Estate Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strREReceivableAccount = "G " + strREFund + "-" + rsInfo.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2);
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strRESuppReceivableAccount = "G " + strRESuppFund + "-" + rsInfo.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2);
				}
			}
			else
			{
				MessageBox.Show("You must set up a Real Estate Receivable Account before you may continue.", "No Real Estate Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Code = 'PR'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Account")) == "")
				{
					MessageBox.Show("You must set up a Personal Property Receivable Account before you may continue.", "No Personal Property Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strPPReceivableAccount = "G " + strPPFund + "-" + rsInfo.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2);
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					strPPSuppReceivableAccount = "G " + strPPSuppFund + "-" + rsInfo.Get_Fields("Account") + "-" + Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2);
				}
			}
			else
			{
				MessageBox.Show("You must set up a Personal Property Receivable Account before you may continue.", "No Personal Property Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			vsJournals.ColDataType(CompleteCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsJournals.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsJournals.OpenRecordset("SELECT * FROM TaxCommitment ORDER BY ActivityDate DESC");
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				rsJournals.MoveLast();
				rsJournals.MoveFirst();
				vsJournals.Rows = rsJournals.RecordCount() + 1;
				counter = 1;
				do
				{
					vsJournals.TextMatrix(counter, CompleteCol, FCConvert.ToString(false));
					vsJournals.TextMatrix(counter, TypeCol, "GJ");
					vsJournals.TextMatrix(counter, KeyCol, FCConvert.ToString(rsJournals.Get_Fields_Int32("ID")));
					vsJournals.TextMatrix(counter, PeriodCol, Strings.Format(rsJournals.Get_Fields_DateTime("ActivityDate").Month, "00"));
					// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
					vsJournals.TextMatrix(counter, AmountCol, Strings.Format(rsJournals.Get_Fields("RE") + rsJournals.Get_Fields("PP"), "#,##0.00"));
					vsJournals.TextMatrix(counter, CountCol, "2");
					counter += 1;
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
			}
			else
			{
				MessageBox.Show("There are no automatic journals found.", "No Automatic Journals", MessageBoxButtons.OK, MessageBoxIcon.Information);
				vsJournals.Rows = 1;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			Form_Resize();
		}

		private void frmAutomaticJournals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAutomaticJournals_Resize(object sender, System.EventArgs e)
		{
			if (vsJournals.Rows > 10)
			{
				vsJournals.Height = 432;
			}
			else
			{
				vsJournals.Height = (40 * vsJournals.Rows - 1) + 32;
			}
		}

		public void Form_Resize()
		{
			frmAutomaticJournals_Resize(this, new System.EventArgs());
		}

		private void mnuFileComplete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper Master = new clsDRWrapper();
			int counter;
			int TempJournal = 0;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			bool blnJournalsSelected;
			if (modBudgetaryAccounting.LockJournal() == false)
			{
				MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				FCFileSystem.FileClose(30);
				return;
			}
			blnJournalsSelected = false;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, CompleteCol)) == true)
				{
					blnJournalsSelected = true;
					break;
				}
			}
			if (!blnJournalsSelected)
			{
				MessageBox.Show("You must select at least one journal before you may continue with this process.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			counter = 1;
			do
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, CompleteCol)) == true)
				{
					Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						TempJournal = 1;
					}
					rsJournals.OpenRecordset("SELECT * FROM TaxCommitment WHERE ID = " + vsJournals.TextMatrix(counter, KeyCol));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngYear = FCConvert.ToInt32(rsJournals.Get_Fields("Year"));
					Master.AddNew();
					Master.Set_Fields("JournalNumber", TempJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					if (FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "R")
					{
						Master.Set_Fields("Description", FCConvert.ToString(lngYear) + " Tax Commitment");
					}
					else if (rsJournals.Get_Fields_String("TaxType") == "S")
					{
						Master.Set_Fields("Description", FCConvert.ToString(lngYear) + " Supplemental");
					}
					else if (rsJournals.Get_Fields_String("TaxType") == "L")
					{
						Master.Set_Fields("Description", FCConvert.ToString(lngYear) + " Lien Transfer");
					}
					else
					{
						Master.Set_Fields("Description", FCConvert.ToString(lngYear) + " Lien Removal");
					}
					Master.Set_Fields("Type", "GJ");
					Master.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
					Master.Update();
					Master.Reset();
					rsJournalInfo.OmitNullsOnInsert = true;
					rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
					if (FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "R")
					{
						if (strRECommitmentAccount == strPPCommitmentAccount)
						{
							rsJournalInfo.AddNew();
							rsJournalInfo.Set_Fields("Type", "G");
							rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
							rsJournalInfo.Set_Fields("RCB", "R");
							rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
							rsJournalInfo.Set_Fields("Account", strPPCommitmentAccount);
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Amount", (rsJournals.Get_Fields("PP") + rsJournals.Get_Fields("RE")) * -1);
							rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
							rsJournalInfo.Set_Fields("Status", "E");
							rsJournalInfo.Update(true);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("RE")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								rsJournalInfo.Set_Fields("Account", strRECommitmentAccount);
								// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", FCConvert.ToDouble(rsJournals.Get_Fields("RE")) * -1);
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("PP")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								rsJournalInfo.Set_Fields("Account", strPPCommitmentAccount);
								// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", FCConvert.ToDouble(rsJournals.Get_Fields("PP")) * -1);
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
						}
						if (strREReceivableAccount == strPPReceivableAccount)
						{
							rsJournalInfo.AddNew();
							rsJournalInfo.Set_Fields("Type", "G");
							rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
							rsJournalInfo.Set_Fields("RCB", "R");
							rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							if (Strings.Right(strPPReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
							{
								rsJournalInfo.Set_Fields("Account", strPPReceivableAccount);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Account", Strings.Left(strPPReceivableAccount, strPPReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("PP") + rsJournals.Get_Fields("RE"));
							rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
							rsJournalInfo.Set_Fields("Status", "E");
							rsJournalInfo.Update(true);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("RE")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								if (Strings.Right(strREReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
								{
									rsJournalInfo.Set_Fields("Account", strREReceivableAccount);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
									rsJournalInfo.Set_Fields("Account", Strings.Left(strREReceivableAccount, strPPReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
								}
								// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("RE"));
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("PP")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Tax Commitment");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								if (Strings.Right(strPPReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
								{
									rsJournalInfo.Set_Fields("Account", strPPReceivableAccount);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
									rsJournalInfo.Set_Fields("Account", Strings.Left(strPPReceivableAccount, strPPReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
								}
								// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("PP"));
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
						}
					}
					else if (rsJournals.Get_Fields_String("TaxType") == "S")
					{
						if (strRESupplementalAccount == strPPSupplementalAccount)
						{
							rsJournalInfo.AddNew();
							rsJournalInfo.Set_Fields("Type", "G");
							rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
							rsJournalInfo.Set_Fields("RCB", "R");
							rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
							rsJournalInfo.Set_Fields("Account", strPPSupplementalAccount);
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Amount", (rsJournals.Get_Fields("PP") + rsJournals.Get_Fields("RE")) * -1);
							rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
							rsJournalInfo.Set_Fields("Status", "E");
							rsJournalInfo.Update(true);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("RE")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								rsJournalInfo.Set_Fields("Account", strRESupplementalAccount);
								// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", FCConvert.ToDouble(rsJournals.Get_Fields("RE")) * -1);
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("PP")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								rsJournalInfo.Set_Fields("Account", strPPSupplementalAccount);
								// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", FCConvert.ToDouble(rsJournals.Get_Fields("PP")) * -1);
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
						}
						if (strRESuppReceivableAccount == strPPSuppReceivableAccount)
						{
							rsJournalInfo.AddNew();
							rsJournalInfo.Set_Fields("Type", "G");
							rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
							rsJournalInfo.Set_Fields("RCB", "R");
							rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							if (Strings.Right(strPPReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
							{
								rsJournalInfo.Set_Fields("Account", strPPSuppReceivableAccount);
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Account", Strings.Left(strPPSuppReceivableAccount, strPPSuppReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("PP") + rsJournals.Get_Fields("RE"));
							rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
							rsJournalInfo.Set_Fields("Status", "E");
							rsJournalInfo.Update(true);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("RE")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								if (Strings.Right(strRESuppReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
								{
									rsJournalInfo.Set_Fields("Account", strRESuppReceivableAccount);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
									rsJournalInfo.Set_Fields("Account", Strings.Left(strRESuppReceivableAccount, strRESuppReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
								}
								// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("RE"));
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
							// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
							if (FCConvert.ToInt32(rsJournals.Get_Fields("PP")) != 0)
							{
								rsJournalInfo.AddNew();
								rsJournalInfo.Set_Fields("Type", "G");
								rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Supplemental");
								rsJournalInfo.Set_Fields("RCB", "R");
								rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
								// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
								if (Strings.Right(strPPSuppReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
								{
									rsJournalInfo.Set_Fields("Account", strPPSuppReceivableAccount);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
									rsJournalInfo.Set_Fields("Account", Strings.Left(strPPSuppReceivableAccount, strPPSuppReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
								}
								// TODO Get_Fields: Check the table for the column [PP] and replace with corresponding Get_Field method
								rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("PP"));
								rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
								rsJournalInfo.Set_Fields("Status", "E");
								rsJournalInfo.Update(true);
							}
						}
					}
					else if (FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "L" || FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "M" || FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "WL" || FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "WM")
					{
						rsJournalInfo.AddNew();
						rsJournalInfo.Set_Fields("Type", "G");
						rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
						if (FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Lien Transfer");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Lien Removal");
						}
						rsJournalInfo.Set_Fields("RCB", "R");
						rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						if (Strings.Right(strREReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
						{
							rsJournalInfo.Set_Fields("Account", strREReceivableAccount);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Account", Strings.Left(strREReceivableAccount, strPPReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
						}
						// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Amount", FCConvert.ToDouble(rsJournals.Get_Fields("RE")) * -1);
						rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
						rsJournalInfo.Set_Fields("Status", "E");
						rsJournalInfo.Update(true);
						rsJournalInfo.AddNew();
						rsJournalInfo.Set_Fields("Type", "G");
						rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("ActivityDate"));
						if (FCConvert.ToString(rsJournals.Get_Fields_String("TaxType")) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Lien Transfer");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields("Year") + " Lien Removal");
						}
						rsJournalInfo.Set_Fields("RCB", "R");
						rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						if (Strings.Right(strLienReceivableAccount, 2) == Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2))
						{
							rsJournalInfo.Set_Fields("Account", strLienReceivableAccount);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
							rsJournalInfo.Set_Fields("Account", Strings.Left(strLienReceivableAccount, strLienReceivableAccount.Length - 2) + Strings.Right(FCConvert.ToString(rsJournals.Get_Fields("Year")), 2));
						}
						// TODO Get_Fields: Check the table for the column [RE] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("RE"));
						rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields_DateTime("ActivityDate").Month);
						rsJournalInfo.Set_Fields("Status", "E");
						rsJournalInfo.Update(true);
					}
					rsJournals.Delete();
					rsJournals.Update();
					vsJournals.TextMatrix(counter, KeyCol, modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4));
					counter += 1;
				}
				else
				{
					vsJournals.RemoveItem(counter);
				}
			}
			while (counter <= vsJournals.Rows - 1);
			modBudgetaryAccounting.UnlockJournal();
			vsJournals.ColWidth(KeyCol, 600);
			btnFileComplete.Enabled = false;
			vsJournals.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vsJournals.Editable = FCGrid.EditableSettings.flexEDNone;
			vsJournals.FocusRect = FCGrid.FocusRectSettings.flexFocusNone;
			if (vsJournals.Rows > 1)
			{
				vsJournals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, KeyCol, vsJournals.Rows - 1, KeyCol, Color.Blue);
			}
			vsJournals.Enabled = false;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(vsJournals.TextMatrix(counter, KeyCol))));
			}
			MessageBox.Show("The journals have been created successfully.  The journal numbers they were saved to are shown on the grid.  You may now edit or post these journals.", "Journals Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsJournals_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsJournals.Row > 0)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, CompleteCol)) == true)
				{
					vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(false));
				}
				else
				{
					vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsJournals.Row > 0)
				{
					if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, CompleteCol)) == true)
					{
						vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(false));
					}
					else
					{
						vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(true));
					}
				}
			}
		}
	}
}
