﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	public class clsACHSetupController
	{
		//=========================================================
		// vbPorter upgrade warning: intBankNumber As short	OnWriteFCConvert.ToInt32(
		public bool Load(clsACHSetup tACHSetup, short intBankNumber)
		{
			bool Load = false;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsBank = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsLoad.OpenRecordset("select * from Budgetary", "twbd0000.vb1");
				rsBank.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(intBankNumber), "TWBD0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					tACHSetup.EmployerAccount = Strings.Trim(FCConvert.ToString(rsBank.Get_Fields_String("BankAccountNumber")));
					if (FCConvert.ToString(rsBank.Get_Fields_String("AccountType")) == "S")
					{
						tACHSetup.EmployerAccountType = 37;
					}
					else
					{
						tACHSetup.EmployerAccountType = 27;
					}
					tACHSetup.EmployerID = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ACHFederalID")));
					tACHSetup.EmployerName = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ACHTownName")));
					tACHSetup.EmployerRT = Strings.Trim(FCConvert.ToString(rsBank.Get_Fields_String("RoutingNumber")));
					if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginName"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginName = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginName")));
					}
					else
					{
						tACHSetup.ImmediateOriginName = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ACHTownName")));
					}
					if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginODFINumber"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginODFI = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginODFINumber")));
					}
					else
					{
						tACHSetup.ImmediateOriginODFI = tACHSetup.EmployerRT;
					}
					if (Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginRoutingNumber"))) != string.Empty)
					{
						tACHSetup.ImmediateOriginRT = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginRoutingNumber")));
					}
					else
					{
						tACHSetup.ImmediateOriginRT = tACHSetup.EmployerRT;
					}
					tACHSetup.ImmediateDestinationName = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateDestinationName")));
					tACHSetup.ImmediateDestinationRT = Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateDestinationRoutingNumber")));
				}
				else
				{
					rsLoad.OpenRecordset("select * from GLOBALVARIABLES", "SystemSettings");
					tACHSetup.EmployerName = rsLoad.Get_Fields_String("citytown") + " of " + rsLoad.Get_Fields_String("muniname");
					tACHSetup.EmployerAccountType = 27;
					return Load;
				}
				Load = true;
				return Load;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return Load;
		}

		public bool Save(ref clsACHSetup tACHSetup)
		{
			bool Save = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from tblACHInformation");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("achbankrt", tACHSetup.ImmediateDestinationRT);
				rsSave.Set_Fields("achbankname", tACHSetup.ImmediateDestinationName);
				rsSave.Set_Fields("ImmediateOriginRT", tACHSetup.ImmediateOriginRT);
				rsSave.Set_Fields("odfinum", tACHSetup.ImmediateOriginODFI);
				rsSave.Set_Fields("immediateOriginName", tACHSetup.ImmediateOriginName);
				rsSave.Set_Fields("employeraccount", tACHSetup.EmployerAccount);
				rsSave.Set_Fields("employerAccountType", tACHSetup.EmployerAccountType);
				rsSave.Set_Fields("employerid", tACHSetup.EmployerID);
				rsSave.Set_Fields("EmployerName", tACHSetup.EmployerName);
				rsSave.Set_Fields("EmployerRT", tACHSetup.EmployerRT);
				rsSave.Update();
				Save = true;
				return Save;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return Save;
		}
	}
}
