﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorListSetup.
	/// </summary>
	public partial class frmVendorListSetup : BaseForm
	{
		public frmVendorListSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAdjustmentsYes.SelectedIndex = 0;
			cmbInsuranceDateYes.SelectedIndex = 1;
			cmbMessageNo.SelectedIndex = 0;
			cmbNo.SelectedIndex = 1;
			cmbNumber.SelectedIndex = 1;
			cmbRange.SelectedIndex = 0;
			cmbSpecificClass.SelectedIndex = 0;
			cmbStatusAll.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendorListSetup InstancePtr
		{
			get
			{
				return (frmVendorListSetup)Sys.GetInstance(typeof(frmVendorListSetup));
			}
		}

		protected frmVendorListSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         7/17/02
		// This screen will be used by towns to set up what and how
		// information is shown on the vendor list report
		// ********************************************************
		private void chkAdjustments_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAdjustments.CheckState == CheckState.Checked)
			{
				fraZeroAdjustments.Enabled = true;
				cmbAdjustmentsYes.Enabled = true;
				Label4.Enabled = true;
			}
			else
			{
				fraZeroAdjustments.Enabled = false;
				cmbAdjustmentsYes.Enabled = false;
				Label4.Enabled = false;
			}
		}

		private void chkDataEntryMessage_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDataEntryMessage.CheckState == CheckState.Checked)
			{
				fraDataEntryMessage.Enabled = true;
				cmbMessageNo.Enabled = true;
				Label5.Enabled = true;
			}
			else
			{
				fraDataEntryMessage.Enabled = false;
				cmbMessageNo.Enabled = false;
				Label5.Enabled = false;
			}
		}

		private void chkInsuranceVerifieddate_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkInsuranceVerifieddate.CheckState == CheckState.Checked)
			{
				fraInsuranceRequired.Enabled = true;
				lblInsuranceVerifiedDate.Enabled = true;
				txtInsuranceVerifiedDate.Enabled = true;
				cmbInsuranceDateYes.Enabled = true;
			}
			else
			{
				fraInsuranceRequired.Enabled = false;
				lblInsuranceVerifiedDate.Enabled = false;
				txtInsuranceVerifiedDate.Enabled = false;
				cmbInsuranceDateYes.Enabled = false;
			}
		}

		private void chkYTDAmount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkYTDAmount.CheckState == CheckState.Checked)
			{
				fraZeroYTDAmount.Enabled = true;
				cmbNo.Enabled = true;
				if (cmbNo.SelectedIndex == 1)
				{
					chkOnlyZeroAmounts.Enabled = true;
				}
				else
				{
					chkOnlyZeroAmounts.Enabled = false;
				}
				Label3.Enabled = true;
			}
			else
			{
				fraZeroYTDAmount.Enabled = false;
				cmbNo.Enabled = false;
				chkOnlyZeroAmounts.Enabled = false;
				Label3.Enabled = false;
			}
		}

		private void frmVendorListSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmVendorListSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendorListSetup.FillStyle	= 0;
			//frmVendorListSetup.ScaleWidth	= 9045;
			//frmVendorListSetup.ScaleHeight	= 7410;
			//frmVendorListSetup.LinkTopic	= "Form2";
			//frmVendorListSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			cboClass.SelectedIndex = 0;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmVendorListSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkActive.CheckState == CheckState.Unchecked && chkSuspended.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 status before you may proceed", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (chkInsuranceVerifieddate.CheckState == CheckState.Checked)
			{
				if (cmbInsuranceDateYes.SelectedIndex == 0)
				{
					if (!Information.IsDate(Strings.Trim(txtInsuranceVerifiedDate.Text)))
					{
						MessageBox.Show("You must enter a valid date for Insurance Verified before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInsuranceVerifiedDate.Focus();
						return;
					}
				}
			}
			//this.Close();
			frmReportViewer.InstancePtr.Init(rptVendorList.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbStatusAll.SelectedIndex == 1)
			{
				if (chkActive.CheckState == CheckState.Unchecked && chkSuspended.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 status before you may proceed", "Invalid Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (chkInsuranceVerifieddate.CheckState == CheckState.Checked)
			{
				if (cmbInsuranceDateYes.SelectedIndex == 0)
				{
					if (!Information.IsDate(Strings.Trim(txtInsuranceVerifiedDate.Text)))
					{
						MessageBox.Show("You must enter a valid date for Insurance Verified before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInsuranceVerifiedDate.Focus();
						return;
					}
				}
			}
			//rptVendorList.InstancePtr.Hide();
			//rptVendorList.InstancePtr.Init();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			txtLow.Text = "";
			txtHigh.Text = "";
			Label1.Enabled = false;
			Label2.Enabled = false;
			fraRange.Enabled = false;
		}

		private void optAllClass_CheckedChanged(object sender, System.EventArgs e)
		{
			cboClass.SelectedIndex = 0;
			fraClass.Enabled = false;
			cboClass.Enabled = false;
		}

		private void optInsuranceDateNo_CheckedChanged(object sender, System.EventArgs e)
		{
			txtInsuranceVerifiedDate.Text = "";
			txtInsuranceVerifiedDate.Enabled = false;
		}

		private void optInsuranceDateYes_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbInsuranceDateYes.SelectedIndex == 0)
			{
				txtInsuranceVerifiedDate.Enabled = true;
				txtInsuranceVerifiedDate.Focus();
			}
			else
			{
				optInsuranceDateNo_CheckedChanged(sender, e);
			}
		}

		private void optNo_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbNo.SelectedIndex == 0)
			{
				chkOnlyZeroAmounts.CheckState = CheckState.Unchecked;
				chkOnlyZeroAmounts.Enabled = false;
			}
			else
			{
				optYes_CheckedChanged(sender, e);
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				fraRange.Enabled = true;
				Label1.Enabled = true;
				Label2.Enabled = true;
				txtLow.Focus();
			}
		}

		private void optSpecificClass_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbSpecificClass.SelectedIndex == 0)
			{
				optAllClass_CheckedChanged(sender, e);
			}
			else if (cmbSpecificClass.SelectedIndex == 1)
			{
				fraClass.Enabled = true;
				cboClass.Enabled = true;
			}
		}

		private void optStatusAll_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbStatusAll.SelectedIndex == 0)
			{
				chkActive.CheckState = CheckState.Unchecked;
				chkSuspended.CheckState = CheckState.Unchecked;
				chkDeleted.CheckState = CheckState.Unchecked;
				chkActive.Enabled = false;
				chkSuspended.Enabled = false;
				chkDeleted.Enabled = false;
				fraStatus.Enabled = false;
			}
			else if (cmbStatusAll.SelectedIndex == 1)
			{
				optStatusSelect_CheckedChanged(sender, e);
			}
		}

		private void optStatusSelect_CheckedChanged(object sender, System.EventArgs e)
		{
			fraStatus.Enabled = true;
			chkActive.Enabled = true;
			chkSuspended.Enabled = true;
			chkDeleted.Enabled = true;
		}

		private void optYes_CheckedChanged(object sender, System.EventArgs e)
		{
			chkOnlyZeroAmounts.Enabled = true;
		}

		private void txtLow_Enter(object sender, System.EventArgs e)
		{
			if (cmbNumber.SelectedIndex == 0)
			{
				txtLow.MaxLength = 5;
			}
			else
			{
				txtLow.MaxLength = 0;
			}
		}

		private void txtLow_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbNumber.SelectedIndex == 0)
			{
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtHigh_Enter(object sender, System.EventArgs e)
		{
			if (cmbNumber.SelectedIndex == 0)
			{
				txtHigh.MaxLength = 5;
			}
			else
			{
				txtHigh.MaxLength = 0;
			}
		}

		private void txtHigh_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbNumber.SelectedIndex == 0)
			{
				if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(sender, e);
		}

        //FC:FINAL:AM:#2911 - allow numeric based on combo selection
        private void CmbNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNumber.SelectedIndex == 0)
            {
                this.txtLow.AllowOnlyNumericInput();
                this.txtHigh.AllowOnlyNumericInput();
            }
            else
            {
                this.txtLow.RemoveClientEvents();
                this.txtHigh.RemoveClientEvents();
            }
        }
    }
}
