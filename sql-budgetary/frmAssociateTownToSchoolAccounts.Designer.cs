﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAssociateTownToSchoolAccounts.
	/// </summary>
	partial class frmAssociateTownToSchoolAccounts : BaseForm
	{
		public FCGrid vs1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmAssociateTownToSchoolAccounts));
			this.vs1 = new FCGrid();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// vs1
			//
			this.vs1.Name = "vs1";
			this.vs1.Enabled = true;
			this.vs1.TabIndex = 0;
			this.vs1.Location = new System.Drawing.Point(187, 50);
			this.vs1.Size = new System.Drawing.Size(236, 437);
			this.vs1.Cols = 2;
			this.vs1.Editable = FCGrid.EditableSettings.flexEDNone;
			this.vs1.FixedCols = 1;
			this.vs1.FixedRows = 1;
			this.vs1.FrozenCols = 0;
			this.vs1.FrozenRows = 0;
			this.vs1.Rows = 1;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			//this.vs1.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vs1.OcxState")));
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			//
			// mnuProcessSave
			//
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuProcessQuit
			//
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuProcess
			});
			//
			// frmAssociateTownToSchoolAccounts
			//
			this.ClientSize = new System.Drawing.Size(610, 491);
			this.ClientArea.Controls.Add(this.vs1);
			this.Menu = this.MainMenu1;
			this.Name = "frmAssociateTownToSchoolAccounts";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Activated += new System.EventHandler(this.frmAssociateTownToSchoolAccounts_Activated);
			this.Load += new System.EventHandler(this.frmAssociateTownToSchoolAccounts_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAssociateTownToSchoolAccounts_KeyPress);
			this.Text = "Associate Accounts";
			this.vs1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
