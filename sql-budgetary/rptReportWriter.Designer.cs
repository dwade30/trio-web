﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptReportWriter.
	/// </summary>
	partial class rptCustomReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblIncludes = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMonths = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeading9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldYTDDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTDCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBudget = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYTD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSpent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linSubTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIncludes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMonths)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldYTDDebits,
				this.fldYTDCredits,
				this.fldCurrentDebits,
				this.fldCurrentCredits,
				this.fldTotalTitle,
				this.fldTitle,
				this.fldBudget,
				this.fldCurrent,
				this.fldYTD,
				this.fldBalance,
				this.fldAccountTitle,
				this.fldSpent,
				this.linSubTotal
			});
			this.Detail.Height = 0.4270833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblHeading1,
				this.lblHeading2,
				this.lblHeading3,
				this.lblHeading4,
				this.Line1,
				this.lblIncludes,
				this.lblHeading5,
				this.lblMonths,
				this.lblHeading6,
				this.lblHeading7,
				this.lblHeading8,
				this.lblHeading9
			});
			this.PageHeader.Height = 1F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label12
			});
			this.PageFooter.Name = "PageFooter";
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.21875F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 1.5F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblTitle.Text = "Check Register";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblHeading1
			// 
			this.lblHeading1.Height = 0.1875F;
			this.lblHeading1.HyperLink = null;
			this.lblHeading1.Left = 3.21875F;
			this.lblHeading1.Name = "lblHeading1";
			this.lblHeading1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading1.Text = "Bud / Beg Bal";
			this.lblHeading1.Top = 0.78125F;
			this.lblHeading1.Width = 0.90625F;
			// 
			// lblHeading2
			// 
			this.lblHeading2.Height = 0.1875F;
			this.lblHeading2.HyperLink = null;
			this.lblHeading2.Left = 4.1875F;
			this.lblHeading2.Name = "lblHeading2";
			this.lblHeading2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading2.Text = "Current Activity";
			this.lblHeading2.Top = 0.78125F;
			this.lblHeading2.Width = 0.90625F;
			// 
			// lblHeading3
			// 
			this.lblHeading3.Height = 0.1875F;
			this.lblHeading3.HyperLink = null;
			this.lblHeading3.Left = 5.15625F;
			this.lblHeading3.Name = "lblHeading3";
			this.lblHeading3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading3.Text = "YTD Activity";
			this.lblHeading3.Top = 0.78125F;
			this.lblHeading3.Width = 0.90625F;
			// 
			// lblHeading4
			// 
			this.lblHeading4.Height = 0.1875F;
			this.lblHeading4.HyperLink = null;
			this.lblHeading4.Left = 6.125F;
			this.lblHeading4.Name = "lblHeading4";
			this.lblHeading4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading4.Text = "Balance";
			this.lblHeading4.Top = 0.78125F;
			this.lblHeading4.Width = 0.90625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.21875F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.96875F;
			this.Line1.Width = 4.78125F;
			this.Line1.X1 = 3.21875F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 0.96875F;
			this.Line1.Y2 = 0.96875F;
			// 
			// lblIncludes
			// 
			this.lblIncludes.Height = 0.1875F;
			this.lblIncludes.HyperLink = null;
			this.lblIncludes.Left = 1.5F;
			this.lblIncludes.Name = "lblIncludes";
			this.lblIncludes.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblIncludes.Text = "Label12";
			this.lblIncludes.Top = 0.40625F;
			this.lblIncludes.Width = 4.6875F;
			// 
			// lblHeading5
			// 
			this.lblHeading5.Height = 0.1875F;
			this.lblHeading5.HyperLink = null;
			this.lblHeading5.Left = 7.09375F;
			this.lblHeading5.Name = "lblHeading5";
			this.lblHeading5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading5.Text = "Percent";
			this.lblHeading5.Top = 0.78125F;
			this.lblHeading5.Width = 0.90625F;
			// 
			// lblMonths
			// 
			this.lblMonths.Height = 0.1875F;
			this.lblMonths.HyperLink = null;
			this.lblMonths.Left = 1.5F;
			this.lblMonths.Name = "lblMonths";
			this.lblMonths.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblMonths.Text = "Label12";
			this.lblMonths.Top = 0.21875F;
			this.lblMonths.Width = 4.6875F;
			// 
			// lblHeading6
			// 
			this.lblHeading6.Height = 0.1875F;
			this.lblHeading6.HyperLink = null;
			this.lblHeading6.Left = 3.21875F;
			this.lblHeading6.Name = "lblHeading6";
			this.lblHeading6.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading6.Text = "YTD Debits";
			this.lblHeading6.Top = 0.78125F;
			this.lblHeading6.Width = 0.90625F;
			// 
			// lblHeading7
			// 
			this.lblHeading7.Height = 0.1875F;
			this.lblHeading7.HyperLink = null;
			this.lblHeading7.Left = 4.1875F;
			this.lblHeading7.Name = "lblHeading7";
			this.lblHeading7.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading7.Text = "YTD Credits";
			this.lblHeading7.Top = 0.78125F;
			this.lblHeading7.Width = 0.90625F;
			// 
			// lblHeading8
			// 
			this.lblHeading8.Height = 0.1875F;
			this.lblHeading8.HyperLink = null;
			this.lblHeading8.Left = 5.15625F;
			this.lblHeading8.Name = "lblHeading8";
			this.lblHeading8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading8.Text = "Current Debits";
			this.lblHeading8.Top = 0.78125F;
			this.lblHeading8.Width = 0.90625F;
			// 
			// lblHeading9
			// 
			this.lblHeading9.Height = 0.1875F;
			this.lblHeading9.HyperLink = null;
			this.lblHeading9.Left = 6.125F;
			this.lblHeading9.Name = "lblHeading9";
			this.lblHeading9.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblHeading9.Text = "Current Credits";
			this.lblHeading9.Top = 0.78125F;
			this.lblHeading9.Width = 0.90625F;
			// 
			// fldYTDDebits
			// 
			this.fldYTDDebits.CanGrow = false;
			this.fldYTDDebits.Height = 0.1875F;
			this.fldYTDDebits.Left = 3.21875F;
			this.fldYTDDebits.MultiLine = false;
			this.fldYTDDebits.Name = "fldYTDDebits";
			this.fldYTDDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldYTDDebits.Text = null;
			this.fldYTDDebits.Top = 0.03125F;
			this.fldYTDDebits.Width = 0.90625F;
			// 
			// fldYTDCredits
			// 
			this.fldYTDCredits.CanGrow = false;
			this.fldYTDCredits.Height = 0.1875F;
			this.fldYTDCredits.Left = 4.1875F;
			this.fldYTDCredits.MultiLine = false;
			this.fldYTDCredits.Name = "fldYTDCredits";
			this.fldYTDCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldYTDCredits.Text = "Field1";
			this.fldYTDCredits.Top = 0.03125F;
			this.fldYTDCredits.Width = 0.90625F;
			// 
			// fldCurrentDebits
			// 
			this.fldCurrentDebits.CanGrow = false;
			this.fldCurrentDebits.Height = 0.1875F;
			this.fldCurrentDebits.Left = 5.15625F;
			this.fldCurrentDebits.MultiLine = false;
			this.fldCurrentDebits.Name = "fldCurrentDebits";
			this.fldCurrentDebits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCurrentDebits.Text = "Field2";
			this.fldCurrentDebits.Top = 0.03125F;
			this.fldCurrentDebits.Width = 0.90625F;
			// 
			// fldCurrentCredits
			// 
			this.fldCurrentCredits.CanGrow = false;
			this.fldCurrentCredits.Height = 0.1875F;
			this.fldCurrentCredits.Left = 6.125F;
			this.fldCurrentCredits.MultiLine = false;
			this.fldCurrentCredits.Name = "fldCurrentCredits";
			this.fldCurrentCredits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCurrentCredits.Text = "Field3";
			this.fldCurrentCredits.Top = 0.03125F;
			this.fldCurrentCredits.Width = 0.90625F;
			// 
			// fldTotalTitle
			// 
			this.fldTotalTitle.CanGrow = false;
			this.fldTotalTitle.Height = 0.1875F;
			this.fldTotalTitle.Left = 0.375F;
			this.fldTotalTitle.MultiLine = false;
			this.fldTotalTitle.Name = "fldTotalTitle";
			this.fldTotalTitle.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldTotalTitle.Text = "Field1";
			this.fldTotalTitle.Top = 0.03125F;
			this.fldTotalTitle.Width = 2.78125F;
			// 
			// fldTitle
			// 
			this.fldTitle.CanGrow = false;
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 0F;
			this.fldTitle.MultiLine = false;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldTitle.Text = "Field1";
			this.fldTitle.Top = 0.03125F;
			this.fldTitle.Width = 3.15625F;
			// 
			// fldBudget
			// 
			this.fldBudget.CanGrow = false;
			this.fldBudget.Height = 0.1875F;
			this.fldBudget.Left = 3.21875F;
			this.fldBudget.MultiLine = false;
			this.fldBudget.Name = "fldBudget";
			this.fldBudget.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldBudget.Text = null;
			this.fldBudget.Top = 0.03125F;
			this.fldBudget.Width = 0.90625F;
			// 
			// fldCurrent
			// 
			this.fldCurrent.CanGrow = false;
			this.fldCurrent.Height = 0.1875F;
			this.fldCurrent.Left = 4.1875F;
			this.fldCurrent.MultiLine = false;
			this.fldCurrent.Name = "fldCurrent";
			this.fldCurrent.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCurrent.Text = "Field1";
			this.fldCurrent.Top = 0.03125F;
			this.fldCurrent.Width = 0.90625F;
			// 
			// fldYTD
			// 
			this.fldYTD.CanGrow = false;
			this.fldYTD.Height = 0.1875F;
			this.fldYTD.Left = 5.15625F;
			this.fldYTD.MultiLine = false;
			this.fldYTD.Name = "fldYTD";
			this.fldYTD.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldYTD.Text = "Field2";
			this.fldYTD.Top = 0.03125F;
			this.fldYTD.Width = 0.90625F;
			// 
			// fldBalance
			// 
			this.fldBalance.CanGrow = false;
			this.fldBalance.Height = 0.1875F;
			this.fldBalance.Left = 6.125F;
			this.fldBalance.MultiLine = false;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldBalance.Text = "Field3";
			this.fldBalance.Top = 0.03125F;
			this.fldBalance.Width = 0.90625F;
			// 
			// fldAccountTitle
			// 
			this.fldAccountTitle.CanGrow = false;
			this.fldAccountTitle.Height = 0.1875F;
			this.fldAccountTitle.Left = 0.1875F;
			this.fldAccountTitle.MultiLine = false;
			this.fldAccountTitle.Name = "fldAccountTitle";
			this.fldAccountTitle.Style = "font-family: \'Tahoma\'; font-size: 7pt; white-space: nowrap; ddo-char-set: 0";
			this.fldAccountTitle.Text = "Field1";
			this.fldAccountTitle.Top = 0.03125F;
			this.fldAccountTitle.Width = 2.96875F;
			// 
			// fldSpent
			// 
			this.fldSpent.CanGrow = false;
			this.fldSpent.Height = 0.1875F;
			this.fldSpent.Left = 7.09375F;
			this.fldSpent.MultiLine = false;
			this.fldSpent.Name = "fldSpent";
			this.fldSpent.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldSpent.Text = "Field3";
			this.fldSpent.Top = 0.03125F;
			this.fldSpent.Width = 0.90625F;
			// 
			// linSubTotal
			// 
			this.linSubTotal.Height = 0F;
			this.linSubTotal.Left = 3.21875F;
			this.linSubTotal.LineWeight = 1F;
			this.linSubTotal.Name = "linSubTotal";
			this.linSubTotal.Top = 0.03125F;
			this.linSubTotal.Visible = false;
			this.linSubTotal.Width = 4.78125F;
			this.linSubTotal.X1 = 3.21875F;
			this.linSubTotal.X2 = 8F;
			this.linSubTotal.Y1 = 0.03125F;
			this.linSubTotal.Y2 = 0.03125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2.71875F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-weight: bold; text-align: center";
			this.Label12.Text = "+ = Debit          - = Credit";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 2.0625F;
			// 
			// rptCustomReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIncludes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMonths)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeading9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTDCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYTD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSpent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTDCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBudget;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYTD;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpent;
		private GrapeCity.ActiveReports.SectionReportModel.Line linSubTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIncludes;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonths;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeading9;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
	}
}
