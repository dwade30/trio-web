﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCompleteJournals.
	/// </summary>
	public partial class frmCompleteJournals : BaseForm
	{
		public frmCompleteJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCompleteJournals InstancePtr
		{
			get
			{
				return (frmCompleteJournals)Sys.GetInstance(typeof(frmCompleteJournals));
			}
		}

		protected frmCompleteJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         12/27/02
		// This form will be used to complete journals that came
		// from cash receipting or payroll and could not be completed
		// from those modules because of journal locks
		// ********************************************************
		int CompleteCol;
		int KeyCol;
		int TypeCol;
		// variables for grid columns
		int AmountCol;
		int CountCol;
		int PeriodCol;
		clsDRWrapper rsJournals = new clsDRWrapper();

		private void frmCompleteJournals_Activated(object sender, System.EventArgs e)
		{
			if (vsJournals.Rows == 1)
			{
				MessageBox.Show("There are no incomplete journals found", "No Incomplete Journals", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCompleteJournals_Load(object sender, System.EventArgs e)
		{

			int counter = 0;
			CompleteCol = 0;
			KeyCol = 1;
			PeriodCol = 2;
			TypeCol = 3;
			AmountCol = 4;
			CountCol = 5;
			vsJournals.TextMatrix(0, CompleteCol, "Complete");
			vsJournals.TextMatrix(0, KeyCol, "Jrnl");
			vsJournals.TextMatrix(0, PeriodCol, "Period");
			vsJournals.TextMatrix(0, TypeCol, "Type");
			vsJournals.TextMatrix(0, AmountCol, "Amount");
			vsJournals.TextMatrix(0, CountCol, "Entries");
			vsJournals.ColWidth(CompleteCol, 900);
			vsJournals.ColWidth(KeyCol, 0);
			vsJournals.ColWidth(PeriodCol, 800);
			vsJournals.ColWidth(TypeCol, 900);
			vsJournals.ColWidth(AmountCol, 1500);
			vsJournals.ColWidth(CountCol, 800);
			vsJournals.ColDataType(CompleteCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsJournals.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rsJournals.OpenRecordset("SELECT SUM(Amount) as TotalAmount, COUNT(Amount) as TotalEntries, Type, Period, JournalNumber FROM TempJournalEntries GROUP BY JournalNumber, Period, Type ORDER BY JournalNumber");
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				rsJournals.MoveLast();
				rsJournals.MoveFirst();
				vsJournals.Rows = rsJournals.RecordCount() + 1;
				counter = 1;
				do
				{
					vsJournals.TextMatrix(counter, CompleteCol, FCConvert.ToString(false));
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "W")
					{
						vsJournals.TextMatrix(counter, TypeCol, "CR");
					}
					else if (rsJournals.Get_Fields("Type") == "P")
					{
						vsJournals.TextMatrix(counter, TypeCol, "PY");
					}
					else
					{
						vsJournals.TextMatrix(counter, TypeCol, "GJ");
					}
					vsJournals.TextMatrix(counter, KeyCol, FCConvert.ToString(rsJournals.Get_Fields("JournalNumber")));
					vsJournals.TextMatrix(counter, PeriodCol, FCConvert.ToString(rsJournals.Get_Fields("Period")));
					vsJournals.TextMatrix(counter, AmountCol, FCConvert.ToString(rsJournals.Get_Fields_Decimal("TotalAmount")));
					vsJournals.TextMatrix(counter, CountCol, FCConvert.ToString(rsJournals.Get_Fields("TotalEntries")));
					counter += 1;
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
			}
			else
			{
				vsJournals.Rows = 1;
				return;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCompleteJournals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileComplete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper Master = new clsDRWrapper();
			int counter;
			int TempJournal = 0;
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			bool blnJournalsSelected;
			if (modBudgetaryAccounting.LockJournal() == false)
			{
				MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				FCFileSystem.FileClose(30);
				return;
			}
			blnJournalsSelected = false;
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, CompleteCol)) == true)
				{
					blnJournalsSelected = true;
					break;
				}
			}
			if (!blnJournalsSelected)
			{
				MessageBox.Show("You must select at least one journal before you may continue with this process.", "No Journals Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(counter, CompleteCol)) == true)
				{
					Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						TempJournal = 1;
					}
					rsJournals.OpenRecordset("SELECT * FROM TempJournalEntries WHERE JournalNumber = " + vsJournals.TextMatrix(counter, KeyCol));
					Master.AddNew();
					Master.Set_Fields("JournalNumber", TempJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					Master.Set_Fields("Description", rsJournals.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsJournals.Get_Fields("Type")) == "W")
					{
						Master.Set_Fields("Type", "CW");
					}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsJournals.Get_Fields("Type") == "P")
					{
						Master.Set_Fields("Type", "PY");
					}
					else
					{
						Master.Set_Fields("Type", "GJ");
					}
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(rsJournals.Get_Fields("Period"))));
					Master.Update();
					Master.Reset();
					rsJournalInfo.OmitNullsOnInsert = true;
					rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
					do
					{
						rsJournalInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Type", rsJournals.Get_Fields("Type"));
						rsJournalInfo.Set_Fields("JournalEntriesDate", rsJournals.Get_Fields_DateTime("TempJournalEntriesDate"));
						rsJournalInfo.Set_Fields("Description", rsJournals.Get_Fields_String("Description"));
						rsJournalInfo.Set_Fields("VendorNumber", rsJournals.Get_Fields_Int32("VendorNumber"));
						rsJournalInfo.Set_Fields("JournalNumber", TempJournal);
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("CheckNumber", rsJournals.Get_Fields("CheckNumber"));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Account", rsJournals.Get_Fields("Account"));
						rsJournalInfo.Set_Fields("Project", rsJournals.Get_Fields_String("Project"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Amount", rsJournals.Get_Fields("Amount"));
						rsJournalInfo.Set_Fields("WarrantNumber", rsJournals.Get_Fields_Int32("WarrantNumber"));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("Period", rsJournals.Get_Fields("Period"));
						rsJournalInfo.Set_Fields("RCB", rsJournals.Get_Fields_String("RCB"));
						rsJournalInfo.Set_Fields("Status", rsJournals.Get_Fields_String("Status"));
						// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("PO", rsJournals.Get_Fields("PO"));
						// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
						rsJournalInfo.Set_Fields("1099", rsJournals.Get_Fields("1099"));
						rsJournalInfo.Update(true);
						rsJournals.Delete();
						rsJournals.Update();
					}
					while (rsJournals.EndOfFile() != true);
					vsJournals.TextMatrix(counter, KeyCol, modValidateAccount.GetFormat_6(FCConvert.ToString(TempJournal), 4));
				}
				else
				{
					vsJournals.RemoveItem(counter);
				}
			}
			for (counter = 1; counter <= vsJournals.Rows - 1; counter++)
			{
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(vsJournals.TextMatrix(counter, KeyCol))));
			}
			modBudgetaryAccounting.UnlockJournal();
			vsJournals.ColWidth(KeyCol, 600);
			btnFileComplete.Enabled = false;
			vsJournals.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vsJournals.Editable = FCGrid.EditableSettings.flexEDNone;
			vsJournals.FocusRect = FCGrid.FocusRectSettings.flexFocusNone;
			if (vsJournals.Rows > 1)
			{
				vsJournals.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, KeyCol, vsJournals.Rows - 1, KeyCol, Color.Blue);
			}
			vsJournals.Enabled = false;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsJournals_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsJournals.Row > 0)
			{
				if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, CompleteCol)) == true)
				{
					vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(false));
				}
				else
				{
					vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(true));
				}
			}
		}

		private void vsJournals_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsJournals.Row > 0)
				{
					if (FCUtils.CBool(vsJournals.TextMatrix(vsJournals.Row, CompleteCol)) == true)
					{
						vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(false));
					}
					else
					{
						vsJournals.TextMatrix(vsJournals.Row, CompleteCol, FCConvert.ToString(true));
					}
				}
			}
		}
	}
}
