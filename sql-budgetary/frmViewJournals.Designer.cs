﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmViewJournals.
	/// </summary>
	partial class frmViewJournals : BaseForm
	{
		public fecherFoundation.FCGrid vs2;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblEntries;
		public fecherFoundation.FCLabel lblDetails;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.vs2 = new fecherFoundation.FCGrid();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblEntries = new fecherFoundation.FCLabel();
			this.lblDetails = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 490);
			this.BottomPanel.Size = new System.Drawing.Size(841, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vs2);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblEntries);
			this.ClientArea.Controls.Add(this.lblDetails);
			this.ClientArea.Size = new System.Drawing.Size(841, 430);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(841, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(225, 30);
			this.HeaderText.Text = "View A / P Journals";
			// 
			// vs2
			// 
			this.vs2.AllowSelection = false;
			this.vs2.AllowUserToResizeColumns = false;
			this.vs2.AllowUserToResizeRows = false;
			this.vs2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs2.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs2.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs2.BackColorBkg = System.Drawing.Color.Empty;
			this.vs2.BackColorFixed = System.Drawing.Color.Empty;
			this.vs2.BackColorSel = System.Drawing.Color.Empty;
			this.vs2.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs2.ColumnHeadersHeight = 30;
			this.vs2.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs2.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs2.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs2.FixedCols = 0;
			this.vs2.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs2.FrozenCols = 0;
			this.vs2.GridColor = System.Drawing.Color.Empty;
			this.vs2.Location = new System.Drawing.Point(30, 392);
			this.vs2.Name = "vs2";
			this.vs2.ReadOnly = true;
			this.vs2.RowHeadersVisible = false;
			this.vs2.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs2.RowHeightMin = 0;
			this.vs2.Rows = 1;
			this.vs2.ShowColumnVisibilityMenu = false;
			this.vs2.Size = new System.Drawing.Size(781, 22);
			this.vs2.StandardTab = true;
			this.vs2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs2.TabIndex = 1;
			this.vs2.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs2_RowExpanded);
			this.vs2.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs2_RowCollapsed);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle4;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.FixedRows = 1;
			this.vs1.FixedCols = 1;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 65);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 1;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(781, 272);
			this.vs1.StandardTab = true;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vs1.TabIndex = 0;
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vs1_MouseMoveEvent);
			this.vs1.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowExpanded);
			this.vs1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vs1_RowCollapsed);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// lblEntries
			// 
			this.lblEntries.AutoSize = true;
			this.lblEntries.Location = new System.Drawing.Point(30, 30);
			this.lblEntries.Name = "lblEntries";
			this.lblEntries.Size = new System.Drawing.Size(73, 15);
			this.lblEntries.TabIndex = 3;
			this.lblEntries.Text = "JOURNALS";
			this.lblEntries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDetails
			// 
			this.lblDetails.AutoSize = true;
			this.lblDetails.Location = new System.Drawing.Point(30, 357);
			this.lblDetails.Name = "lblDetails";
			this.lblDetails.Size = new System.Drawing.Size(59, 15);
			this.lblDetails.TabIndex = 2;
			this.lblDetails.Text = "DETAILS";
			this.lblDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmViewJournals
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(841, 598);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmViewJournals";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "View A / P Journals";
			this.Load += new System.EventHandler(this.frmViewJournals_Load);
			this.Activated += new System.EventHandler(this.frmViewJournals_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmViewJournals_KeyPress);
			this.Resize += new System.EventHandler(this.frmViewJournals_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
