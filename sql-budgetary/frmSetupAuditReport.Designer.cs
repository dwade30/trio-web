﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupAuditReport.
	/// </summary>
	partial class frmSetupAuditReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDateAll;
		public fecherFoundation.FCLabel lblDateAll;
		public fecherFoundation.FCComboBox cmbScreenAll;
		public fecherFoundation.FCLabel lblScreenAll;
		public fecherFoundation.FCComboBox cmbBanksAll;
		public fecherFoundation.FCLabel lblBanksAll;
		public fecherFoundation.FCFrame Frame3;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboScreen;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cboBanks;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupAuditReport));
			this.cmbDateAll = new fecherFoundation.FCComboBox();
			this.lblDateAll = new fecherFoundation.FCLabel();
			this.cmbScreenAll = new fecherFoundation.FCComboBox();
			this.lblScreenAll = new fecherFoundation.FCLabel();
			this.cmbBanksAll = new fecherFoundation.FCComboBox();
			this.lblBanksAll = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtLowDate = new Global.T2KDateBox();
			this.txtHighDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboScreen = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.cboBanks = new fecherFoundation.FCComboBox();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(149, 30);
			this.HeaderText.Text = "Audit Report";
			// 
			// cmbDateAll
			// 
			this.cmbDateAll.AutoSize = false;
			this.cmbDateAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDateAll.FormattingEnabled = true;
			this.cmbDateAll.Items.AddRange(new object[] {
				"All",
				"Date Range"
			});
			this.cmbDateAll.Location = new System.Drawing.Point(164, 30);
			this.cmbDateAll.Name = "cmbDateAll";
			this.cmbDateAll.Size = new System.Drawing.Size(316, 40);
			this.cmbDateAll.TabIndex = 14;
			this.cmbDateAll.SelectedIndexChanged += new System.EventHandler(this.optDateAll_CheckedChanged);
			// 
			// lblDateAll
			// 
			this.lblDateAll.Location = new System.Drawing.Point(20, 44);
			this.lblDateAll.Name = "lblDateAll";
			this.lblDateAll.Size = new System.Drawing.Size(76, 16);
			this.lblDateAll.TabIndex = 15;
			this.lblDateAll.Text = "DATA RANGE";
			// 
			// cmbScreenAll
			// 
			this.cmbScreenAll.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.cmbScreenAll.AutoSize = false;
			this.cmbScreenAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbScreenAll.FormattingEnabled = true;
			this.cmbScreenAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbScreenAll.Location = new System.Drawing.Point(134, 30);
			this.cmbScreenAll.Name = "cmbScreenAll";
			this.cmbScreenAll.Size = new System.Drawing.Size(346, 40);
			this.cmbScreenAll.TabIndex = 8;
			this.cmbScreenAll.SelectedIndexChanged += new System.EventHandler(this.optScreenAll_CheckedChanged);
			// 
			// lblScreenAll
			// 
			this.lblScreenAll.Location = new System.Drawing.Point(20, 44);
			this.lblScreenAll.Name = "lblScreenAll";
			this.lblScreenAll.Size = new System.Drawing.Size(46, 16);
			this.lblScreenAll.TabIndex = 9;
			this.lblScreenAll.Text = "SCREEN";
			// 
			// cmbBanksAll
			// 
			this.cmbBanksAll.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.cmbBanksAll.AutoSize = false;
			this.cmbBanksAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBanksAll.FormattingEnabled = true;
			this.cmbBanksAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbBanksAll.Location = new System.Drawing.Point(121, 30);
			this.cmbBanksAll.Name = "cmbBanksAll";
			this.cmbBanksAll.Size = new System.Drawing.Size(359, 40);
			this.cmbBanksAll.TabIndex = 3;
			this.cmbBanksAll.SelectedIndexChanged += new System.EventHandler(this.optBanksAll_CheckedChanged);
			// 
			// lblBanksAll
			// 
			this.lblBanksAll.Location = new System.Drawing.Point(20, 44);
			this.lblBanksAll.Name = "lblBanksAll";
			this.lblBanksAll.Size = new System.Drawing.Size(33, 16);
			this.lblBanksAll.TabIndex = 4;
			this.lblBanksAll.Text = "BANK";
			// 
			// Frame3
			// 
			this.Frame3.BackColor = System.Drawing.Color.FromName("@window");
			this.Frame3.Controls.Add(this.txtLowDate);
			this.Frame3.Controls.Add(this.txtHighDate);
			this.Frame3.Controls.Add(this.Label2);
			this.Frame3.Controls.Add(this.cmbDateAll);
			this.Frame3.Controls.Add(this.lblDateAll);
			this.Frame3.Location = new System.Drawing.Point(30, 370);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(500, 150);
			this.Frame3.TabIndex = 10;
			this.Frame3.Text = "Date Range";
			// 
			// txtLowDate
			// 
			this.txtLowDate.Enabled = false;
			this.txtLowDate.Location = new System.Drawing.Point(164, 90);
			this.txtLowDate.Mask = "00/00/0000";
			this.txtLowDate.Name = "txtLowDate";
			this.txtLowDate.Size = new System.Drawing.Size(115, 40);
			this.txtLowDate.TabIndex = 14;
			this.txtLowDate.Text = "  /  /";
			// 
			// txtHighDate
			// 
			this.txtHighDate.Enabled = false;
			this.txtHighDate.Location = new System.Drawing.Point(367, 90);
			this.txtHighDate.Mask = "00/00/0000";
			this.txtHighDate.Name = "txtHighDate";
			this.txtHighDate.Size = new System.Drawing.Size(115, 40);
			this.txtHighDate.TabIndex = 15;
			this.txtHighDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Enabled = false;
			this.Label2.Location = new System.Drawing.Point(312, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(20, 16);
			this.Label2.TabIndex = 16;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Frame2
			// 
			this.Frame2.BackColor = System.Drawing.Color.FromName("@window");
			this.Frame2.Controls.Add(this.cboScreen);
			this.Frame2.Controls.Add(this.cmbScreenAll);
			this.Frame2.Controls.Add(this.lblScreenAll);
			this.Frame2.Location = new System.Drawing.Point(30, 200);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(500, 150);
			this.Frame2.TabIndex = 5;
			this.Frame2.Text = "Screen";
			// 
			// cboScreen
			// 
			this.cboScreen.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.cboScreen.AutoSize = false;
			this.cboScreen.BackColor = System.Drawing.SystemColors.Window;
			this.cboScreen.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboScreen.Enabled = false;
			this.cboScreen.FormattingEnabled = true;
			this.cboScreen.Location = new System.Drawing.Point(134, 90);
			this.cboScreen.Name = "cboScreen";
			this.cboScreen.Size = new System.Drawing.Size(346, 40);
			this.cboScreen.TabIndex = 8;
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.FromName("@window");
			this.Frame1.Controls.Add(this.cboBanks);
			this.Frame1.Controls.Add(this.cmbBanksAll);
			this.Frame1.Controls.Add(this.lblBanksAll);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(500, 150);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Bank";
			// 
			// cboBanks
			// 
			this.cboBanks.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.cboBanks.AutoSize = false;
			this.cboBanks.BackColor = System.Drawing.SystemColors.Window;
			this.cboBanks.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBanks.Enabled = false;
			this.cboBanks.FormattingEnabled = true;
			this.cboBanks.Location = new System.Drawing.Point(121, 90);
			this.cboBanks.Name = "cboBanks";
			this.cboBanks.Size = new System.Drawing.Size(359, 40);
			this.cboBanks.TabIndex = 4;
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(467, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(145, 48);
			this.cmdFilePreview.TabIndex = 4;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmSetupAuditReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupAuditReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Audit Report";
			this.Load += new System.EventHandler(this.frmSetupAuditReport_Load);
			this.Activated += new System.EventHandler(this.frmSetupAuditReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAuditReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdFilePreview;
	}
}
