﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeSelected.
	/// </summary>
	public partial class frmPurgeSelected : BaseForm
	{
		public frmPurgeSelected()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgeSelected InstancePtr
		{
			get
			{
				return (frmPurgeSelected)Sys.GetInstance(typeof(frmPurgeSelected));
			}
		}

		protected frmPurgeSelected _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// Dave 12/14/2006---------------------------------------------------
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		// -------------------------------------------------------------------
		private void frmPurgeSelected_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPurgeSelected_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgeSelected.FillStyle	= 0;
			//frmPurgeSelected.ScaleWidth	= 3885;
			//frmPurgeSelected.ScaleHeight	= 2280;
			//frmPurgeSelected.LinkTopic	= "Form2";
			//frmPurgeSelected.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmPurgeSelected_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsArchiveInfo = new clsDRWrapper();
			int intTotal = 0;
			int intPurged = 0;
			if (!Information.IsDate(txtPurgeDate.Text))
			{
				MessageBox.Show("You must enter a valid date before you may proceed.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtPurgeDate.Focus();
				return;
			}
			ans = MessageBox.Show("This type of purge will not affect your beginning balance.  Once you purge these items you will not be able to get them back.  Are you sure you wish to purge?", "Purge Items?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
			if (ans == DialogResult.Yes)
			{
				rsArchiveInfo.OpenRecordset("SELECT * FROM CheckRecArchive");
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					intTotal = rsInfo.RecordCount();
				}
				else
				{
					MessageBox.Show("No Records Found To Purge", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rsArchiveInfo.OmitNullsOnInsert = true;
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckDate < '" + FCConvert.ToString(DateAndTime.DateValue(txtPurgeDate.Text)) + "' ORDER BY Status, Type");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					intPurged = rsInfo.RecordCount();
					rsInfo.MoveFirst();
					do
					{
						rsArchiveInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("CheckNumber", rsInfo.Get_Fields("CheckNumber"));
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("Type", rsInfo.Get_Fields("Type"));
						rsArchiveInfo.Set_Fields("CheckDate", rsInfo.Get_Fields_DateTime("CheckDate"));
						rsArchiveInfo.Set_Fields("Name", rsInfo.Get_Fields_String("Name"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("Amount", rsInfo.Get_Fields("Amount"));
						rsArchiveInfo.Set_Fields("Status", "D");
						rsArchiveInfo.Set_Fields("StatusDate", rsInfo.Get_Fields_DateTime("StatusDate"));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("BankNumber", rsInfo.Get_Fields("BankNumber"));
						rsArchiveInfo.Set_Fields("ArchiveDate", DateTime.Today);
						rsArchiveInfo.Set_Fields("DirectDeposit", rsInfo.Get_Fields_Boolean("DirectDeposit"));
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						clsReportChanges.AddChange("Check: " + rsInfo.Get_Fields("CheckNumber") + "  Date: " + rsInfo.Get_Fields_DateTime("CheckDate") + "  Payee: " + rsInfo.Get_Fields_String("Name") + "  Amount: " + rsInfo.Get_Fields("Amount") + "  - purged");
						rsArchiveInfo.Update(true);
						rsInfo.Delete();
						rsInfo.Update();
					}
					while (rsInfo.EndOfFile() != true);
					clsReportChanges.SaveToAuditChangesTable("Check Rec Purge Selected", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
				}
				MessageBox.Show(Strings.Format(intTotal, "00") + " Records Read" + "\r\n" + "\r\n" + Strings.Format(intTotal - intPurged, "00") + " Records Written" + "\r\n" + "\r\n" + Strings.Format(intPurged, "00") + " Records Purged", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
		}

		private void SetCustomFormColors()
		{
			Label1.ForeColor = Color.Red;
		}
	}
}
