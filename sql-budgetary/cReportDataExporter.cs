﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public interface cReportDataExporter
	{
		//=========================================================
		//FC:FINAL:BBE:#i701 - In C# an interface can only have public methods.
		//string strLastError
		//{
		//    get;
		//    set;
		//}
		//int lngLastError
		//{
		//    get;
		//    set;
		//}
		string LastErrorMessage
		{
			get;
		}

		int LastErrorNumber
		{
			get;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		bool HadError
		{
			get;
		}

		short FormatType
		{
			set;
			get;
		}
		// 0 = CSV, 1 = Tab
		void ExportData(ref cDetailsReport theReport, string strFileName);
	}
}
