﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCashChartSelect.
	/// </summary>
	public partial class frmCashChartSelect : BaseForm
	{
		public frmCashChartSelect()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCashChartSelect InstancePtr
		{
			get
			{
				return (frmCashChartSelect)Sys.GetInstance(typeof(frmCashChartSelect));
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		string strOriginalDescription = "";
		int lngOriginalCriteria;
		int lngOriginalFormat;
		bool blnFromSave;

		private void chkDefault_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cboReports.SelectedIndex == 0)
			{
				chkDefault.CheckState = CheckState.Unchecked;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdCancelSelection_Click(object sender, System.EventArgs e)
		{
			txtReportTitle.Text = "";
			vsChartItems.Rows = 1;
			fraReportSelection.Visible = false;
			cboReports.Visible = true;
			chkDefault.Visible = true;
			cboReports.Focus();
		}

		private void cmdEdit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsEdit = new clsDRWrapper();
			rsEdit.OpenRecordset("SELECT * FROM CashCharts WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
			if (rsEdit.EndOfFile() != true && rsEdit.BeginningOfFile() != true)
			{
				frmCashChartingSetup.InstancePtr.blnReportAlreadyExists = true;
				frmCashChartingSetup.InstancePtr.OldChartKey = FCConvert.ToInt32(rsEdit.Get_Fields_Int32("ID"));
			}
			else
			{
				MessageBox.Show("Chart Not Found", "No Chart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			frmCashChartingSetup.InstancePtr.Show(App.MainForm);
			fraReportSelection.Visible = false;
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsSearch = new clsDRWrapper();
			int counter;
			clsDRWrapper rsTitles = new clsDRWrapper();
			if (cboReports.SelectedIndex == 0)
			{
				frmCashChartingSetup.InstancePtr.blnReportAlreadyExists = false;
				frmCashChartingSetup.InstancePtr.OldChartKey = 0;
				frmCashChartingSetup.InstancePtr.Show(App.MainForm);
				Close();
			}
			else
			{
				if (chkDefault.CheckState == CheckState.Checked)
				{
					modRegistry.SaveRegistryKey("CASHCHRT", cboReports.Text);
				}
				txtReportTitle.Text = cboReports.Text;
				rs.OpenRecordset("SELECT * FROM CashCharts WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rsSearch.OpenRecordset("SELECT * FROM ItemsInChart WHERE Chartkey = " + rs.Get_Fields_Int32("ID"));
					vsChartItems.Rows = 1;
					if (rsSearch.EndOfFile() != true && rsSearch.BeginningOfFile() != true)
					{
						do
						{
							rsTitles.OpenRecordset("SELECT * FROM GraphItems WHERE ID = " + rsSearch.Get_Fields_Int32("GraphItemID"));
							vsChartItems.AddItem(rsTitles.Get_Fields_String("Title"));
							rsSearch.MoveNext();
						}
						while (rsSearch.EndOfFile() != true);
					}
					fraReportSelection.Visible = true;
					cboReports.Visible = false;
					chkDefault.Visible = false;
					//mnuProcess.Focus();
				}
				else
				{
					MessageBox.Show("Chart Not Found", "Unable to find Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdOK_Click()
		{
			cmdOK_Click(cmdFileProcess, new System.EventArgs());
		}

		private void cmdProcessNoSave_Click(object sender, System.EventArgs e)
		{
			fraReportSelection.Visible = false;
			modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM CashCharts WHERE Title = '" + modCustomReport.FixQuotes(cboReports.Text) + "'");
			if (modBudgetaryAccounting.Statics.SearchResults.EndOfFile() != true && modBudgetaryAccounting.Statics.SearchResults.BeginningOfFile() != true)
			{
				chkDefault.Visible = true;
				cboReports.Visible = true;
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
				// center it
				frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				//FC:FINA:KV:IIT807+FC-8697
				this.Hide();
				frmCashChart.InstancePtr.Show(App.MainForm);
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
			}
			else
			{
				MessageBox.Show("Chart Not Found", "Unable to Find Chart", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		public void cmdProcessNoSave_Click()
		{
			cmdProcessNoSave_Click(cmdFileProcess, new System.EventArgs());
		}

		private void frmCashChartSelect_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmCashChartSelect_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCashChartSelect.FillStyle	= 0;
			//frmCashChartSelect.ScaleWidth	= 9045;
			//frmCashChartSelect.ScaleHeight	= 7410;
			//frmCashChartSelect.LinkTopic	= "Form2";
			//frmCashChartSelect.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			string temp;
			FillReport_2(false);
			vsChartItems.TextMatrix(0, 0, "Item Title");
			vsChartItems.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsChartItems.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsChartItems.ColWidth(0, FCConvert.ToInt32(vsChartItems.WidthOriginal / 3.0));
			temp = modRegistry.GetRegistryKey("CASHCHRT");
			for (counter = 0; counter <= cboReports.Items.Count - 1; counter++)
			{
				if (cboReports.Items[counter].ToString() == temp)
				{
					cboReports.SelectedIndex = counter;
					break;
				}
			}
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCashChartSelect_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			if (fraReportSelection.Visible == true)
			{
				cmdProcessNoSave_Click();
			}
			else
			{
				cmdOK_Click();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void FillReport_2(bool blnReportAdded)
		{
			FillReport(blnReportAdded);
		}

		public void FillReport(bool blnReportAdded)
		{
			int counter = 0;
			cboReports.Clear();
			cboReports.AddItem("Create New Chart");
			rs.OpenRecordset("SELECT * FROM CashCharts ORDER BY ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 1;
				while (rs.EndOfFile() != true)
				{
					cboReports.AddItem(FCConvert.ToString(rs.Get_Fields_String("Title")));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (blnReportAdded)
			{
				cboReports.SelectedIndex = cboReports.Items.Count - 1;
			}
			else
			{
				cboReports.SelectedIndex = 0;
			}
		}
	}
}
