﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerSummary.
	/// </summary>
	public partial class frmLedgerSummary : BaseForm
	{
		public frmLedgerSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerSummary InstancePtr
		{
			get
			{
				return (frmLedgerSummary)Sys.GetInstance(typeof(frmLedgerSummary));
			}
		}

		protected frmLedgerSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		clsDRWrapper rs4 = new clsDRWrapper();
		clsDRWrapper rs5 = new clsDRWrapper();
		clsDRWrapper rs6 = new clsDRWrapper();
		clsDRWrapper rs7 = new clsDRWrapper();
		clsDRWrapper UsedAccounts = new clsDRWrapper();
		bool BudgetFlag;
		bool AdjustmentFlag;
		bool NetBudgetFlag;
		bool CurrentDCFlag;
		bool CurrentNetFlag;
		bool YTDDCFlag;
		bool YTDNetFlag;
		bool EncumbranceFlag;
		bool PendingFlag;
		bool BalanceFlag;
		bool BalanceDCFlag;
		int CurrentRow;
		string currentFund = "";
		string currentAccount = "";
		string CurrentSuffix = "";
		int CurrentCol;
		int OriginalBudgetCol;
		int BudgetAdjustmentCol;
		int NetBudgetCol;
		int CurrentDebitCol;
		int CurrentCreditCol;
		int CurrentNetCol;
		int YTDDebitCol;
		int YTDCreditCol;
		int YTDNetCol;
		int EncumbranceCol;
		int PendingCol;
		int BalanceCol;
		int BalanceDebitCol;
		int BalanceCreditCol;
		string strPeriodCheck;
		int intLowestLevel;
		bool blnRunCollapseEvent;
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsBudgetInfo = new clsDRWrapper();
		bool blnSwapValues;
		public string strTitle = "";

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrintPreview_Click(object sender, System.EventArgs e)
		{
			//rptLedgerSummary.InstancePtr.Hide();
			frmReportViewer.InstancePtr.Init(rptLedgerSummary.InstancePtr);
		}

		private void frmLedgerSummary_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			clsDRWrapper Descriptions = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rs.OpenRecordset("SELECT * FROM LedgerSummaryFormats WHERE ID = " + FCConvert.ToString(modBudgetaryMaster.Statics.lngReportFormat));
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
			{
				lblTitle.Text = "Fund(s)";
				lblRangeDept.Text = "ALL";
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
			{
				lblTitle.Text = "Fund(s)";
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp"));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				lblTitle.Text = "Fund(s)";
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp"));
			}
			else
			{
				lblTitle.Text = "Accounts";
				lblRangeDept.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + " - " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount"));
			}
			strPeriodCheck = "AND";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")));
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
			{
				lblMonths.Text = MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + " to " + MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")));
				if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth") > modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))
				{
					strPeriodCheck = "OR";
				}
			}
			else
			{
				lblMonths.Text = "ALL";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 1)
				{
					strPeriodCheck = "OR";
				}
			}
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Building Report", true);
			frmWait.InstancePtr.Show();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnFromPreview)
			{
				FormatGrid();
				frmWait.InstancePtr.prgProgress.Value = 30;
				frmWait.InstancePtr.Refresh();
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo();
				// If rs.Fields["IncludePending"] Or rs.Fields["SeperatePending"] Then
				// CalculateAccountInfo True, True, True, "G"
				// Else
				// CalculateAccountInfo True, True, False, "G"
				// End If
				frmWait.InstancePtr.prgProgress.Value = 60;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Retrieving Information";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				RetrieveInfo();
				frmWait.InstancePtr.prgProgress.Value = 100;
				vs1.Rows += 1;
				vs1.TextMatrix(vs1.Rows - 1, 1, "-");
				CurrentRow = vs1.Rows - 1;
				PrepareTemp();
				vs1.Rows -= 1;
				CurrentRow = 0;
				FundReport();
				if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
				{
					InsertTitles();
				}
				SetOutlineLevels();
				intLowestLevel = GetLowLevel();
				SetOutlineLevelsBasedOnLowestLevel();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Calculating Totals";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				FillInInformation();
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				GetSubTotals();
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				GetCompleteTotals();
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Refresh();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Formatting Report";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				SetColors();
				vs1.RowHeightMax = 220;
				frmWait.InstancePtr.prgProgress.Value = 50;
				frmWait.InstancePtr.Refresh();
				blnRunCollapseEvent = false;
				for (counter = 5; counter >= 1; counter--)
				{
					for (counter2 = 2; counter2 <= vs1.Rows - 1; counter2++)
					{
						//Application.DoEvents();
						if (vs1.RowOutlineLevel(counter2) == counter)
						{
							if (vs1.IsSubtotal(counter2))
							{
								vs1.IsCollapsed(counter2, FCGrid.CollapsedSettings.flexOutlineCollapsed);
							}
						}
					}
				}
				blnRunCollapseEvent = true;
				vs1_Collapsed();
			}
			else
			{
				modBudgetaryMaster.Statics.blnFromPreview = false;
			}
			if (FCConvert.ToString(rs.Get_Fields_String("Use")) == "P")
			{
				cmdPrintPreview.Enabled = true;
			}
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Unload();
			this.Refresh();
			vs1.Visible = true;
			lblTitle.Visible = true;
			lblRangeDept.Visible = true;
			lblMonthLabel.Visible = true;
			lblMonths.Visible = true;
		}

		private void frmLedgerSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLedgerSummary.FillStyle	= 0;
			//frmLedgerSummary.ScaleWidth	= 9480;
			//frmLedgerSummary.ScaleHeight	= 7500;
			//frmLedgerSummary.AutoRedraw	= -1  'True;
			//frmLedgerSummary.LinkTopic	= "Form2";
			//frmLedgerSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			OriginalBudgetCol = -1;
			BudgetAdjustmentCol = -1;
			NetBudgetCol = -1;
			CurrentDebitCol = -1;
			CurrentCreditCol = -1;
			CurrentNetCol = -1;
			YTDDebitCol = -1;
			YTDCreditCol = -1;
			YTDNetCol = -1;
			EncumbranceCol = -1;
			PendingCol = -1;
			BalanceCol = -1;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM:#i653 - add the expand button
			vs1.AddExpandButton();
			vs1.RowHeadersWidth = 10;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DeleteTemp();
		}

		private void frmLedgerSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmdPrintPreview.Enabled)
			{
				//rptLedgerSummary.InstancePtr.Hide();
				frmReportViewer.InstancePtr.Init(rptLedgerSummary.InstancePtr);
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			if (cmdPrintPreview.Enabled == true)
			{
				//rptLedgerSummary.InstancePtr.Hide();
				modDuplexPrinting.DuplexPrintReport(rptLedgerSummary.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height = 0;
			bool FirstFlag = false;
			bool SecondFlag = false;
			bool ThirdFlag = false;
			bool FourthFlag = false;
			bool FifthFlag;
			if (blnRunCollapseEvent)
			{
				for (counter = 2; counter <= vs1.Rows - 1; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FirstFlag = true;
						}
						else
						{
							rows += 1;
							FirstFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 2)
					{
						if (FirstFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							SecondFlag = true;
						}
						else
						{
							rows += 1;
							SecondFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 3)
					{
						if (FirstFlag == true || SecondFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							ThirdFlag = true;
						}
						else
						{
							rows += 1;
							ThirdFlag = false;
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 4)
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true)
						{
							// do nothing
						}
						else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							FourthFlag = true;
						}
						else
						{
							rows += 1;
							FourthFlag = false;
						}
					}
					else
					{
						if (FirstFlag == true || SecondFlag == true || ThirdFlag == true || FourthFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				//FC:FINAL:BBE - fixed row height in web version
				//if (FCConvert.ToString(rs.Get_Fields("Font")) == "S")
				//{
				//    height = ((rows + 2) * 220) + 300;
				//    if (height < frmExpenseSummary.InstancePtr.Height * 0.6645)
				//    {
				//        if (vs1.Height != height)
				//        {
				//            vs1.Height = height + 75;
				//        }
				//    }
				//    else
				//    {
				//        if (vs1.Height < frmExpenseSummary.InstancePtr.Height * 0.6645)
				//        {
				//            vs1.Height = FCConvert.ToInt32(frmExpenseSummary.InstancePtr.Height * 0.6645 + 75);
				//        }
				//    }
				//}
				//else
				//{
				//    height = (rows + 2) * vs1.RowHeight(0);
				//    if (height < frmExpenseSummary.InstancePtr.Height * 0.6645)
				//    {
				//        if (vs1.Height != height)
				//        {
				//            vs1.Height = height + 75;
				//        }
				//    }
				//    else
				//    {
				//        if (vs1.Height < frmExpenseSummary.InstancePtr.Height * 0.6645)
				//        {
				//            vs1.Height = FCConvert.ToInt32(frmExpenseSummary.InstancePtr.Height * 0.6645 + 75);
				//        }
				//    }
				//}
			}
		}

		private void FormatGrid()
		{
			int counter;
			//FC:FINAL:AM:#i653 - merge the cells
			//vs1.MergeRow(0, true);
			vs1.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			counter = 2;
			vs1.Cols = 20;
			vs1.MergeRow(0, true, 0, 19);
			vs1.TextMatrix(1, 1, "Account");
            vs1.ColWidth(1, FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "L" ? 5435 : 4375);
            if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Budget")))
			{
				vs1.TextMatrix(0, counter, "Beginning");
				vs1.TextMatrix(1, counter, " Balance");
	        	vs1.ColWidth(counter, 1750);
                vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                BudgetFlag = true;
				counter += 1;
			}
			else
			{
				BudgetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BudgetAdjustment")))
			{
				vs1.TextMatrix(0, counter, "  Beg  Bal");
				vs1.TextMatrix(1, counter, "Adjustments");
				vs1.ColWidth(counter, 1750);
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                AdjustmentFlag = true;
				counter += 1;
			}
			else
			{
				AdjustmentFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("NetBudget")))
			{
				vs1.TextMatrix(0, counter, "Beg  Bal");
				vs1.TextMatrix(1, counter, "Net");
				vs1.ColWidth(counter, 1750);	
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                NetBudgetFlag = true;
				counter += 1;
			}
			else
			{
				NetBudgetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentDebitCredit")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentNet")))
				{
					vs1.TextMatrix(0, counter, "Current Month");
					vs1.TextMatrix(0, counter + 1, "Current Month");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Current Month");
					vs1.TextMatrix(0, counter + 1, "Current Month");
				}
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1750);
				}
				else
				{
					vs1.ColWidth(counter + 1, 2000);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                CurrentDCFlag = true;
				counter += 2;
			}
			else
			{
				CurrentDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("CurrentDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "Current Month");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Current Month");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                CurrentNetFlag = true;
				counter += 1;
			}
			else
			{
				CurrentNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
				{
					vs1.TextMatrix(0, counter, "Year To Date                      ");
					//vs1.TextMatrix(0, counter + 1, "Year To Date                      ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Year To Date          ");
					//vs1.TextMatrix(0, counter + 1, "Year To Date          ");
				}
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1750);
				}
				else
				{
					vs1.ColWidth(counter + 1, 2000);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDDCFlag = true;
				counter += 2;
			}
			else
			{
				YTDDCFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDNet")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("YTDDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "Year To Date                      ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Year To Date");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                YTDNetFlag = true;
				counter += 1;
			}
			else
			{
				YTDNetFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("OSEncumbrance")))
			{
				vs1.TextMatrix(0, counter, "Outstanding");
				vs1.TextMatrix(1, counter, "Encumbrance");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                EncumbranceFlag = true;
				counter += 1;
			}
			else
			{
				EncumbranceFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("SeperatePending")))
			{
				vs1.TextMatrix(0, counter, "Pending");
				vs1.TextMatrix(1, counter, "Activity");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                PendingFlag = true;
				counter += 1;
			}
			else
			{
				PendingFlag = false;
			}
			// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
			if (FCConvert.ToBoolean(rs.Get_Fields("Balance")))
			{
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BalanceDebitCredit")))
				{
					vs1.TextMatrix(0, counter, "Balance                           ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Balance");
				}
				vs1.TextMatrix(1, counter, "Net");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                BalanceFlag = true;
				counter += 1;
			}
			else
			{
				BalanceFlag = false;
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("BalanceDebitCredit")))
			{
				// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
				if (FCConvert.ToBoolean(rs.Get_Fields("Balance")))
				{
					vs1.TextMatrix(0, counter, "Balance                           ");
					//vs1.TextMatrix(0, counter + 1, "Balance                           ");
				}
				else
				{
					vs1.TextMatrix(0, counter, "Balance          ");
					//vs1.TextMatrix(0, counter + 1, "Balance          ");
				}
				vs1.TextMatrix(1, counter, "Debits");
				vs1.TextMatrix(1, counter + 1, "Credits");
				if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter, 1750);
				}
				else
				{
					vs1.ColWidth(counter, 2000);
				}
				vs1.ColFormat(counter, "#,##0.00");
                vs1.ColAlignment(counter, FCGrid.AlignmentSettings.flexAlignRightCenter);
                if (FCConvert.ToString(rs.Get_Fields_String("Font")) == "S")
				{
					vs1.ColWidth(counter + 1, 1750);
				}
				else
				{
					vs1.ColWidth(counter + 1, 2000);
				}
				vs1.ColFormat(counter + 1, "#,##0.00");
                vs1.ColAlignment(counter + 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                BalanceDCFlag = true;
				counter += 2;
			}
			else
			{
				BalanceDCFlag = false;
			}
			vs1.Cols = counter;
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 1, 1, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 1, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 1, 1, vs1.Cols - 1, true);
		}

		private string Suffix(ref string x)
		{
			string Suffix = "";
			Suffix = Strings.Mid(x, 5 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
			return Suffix;
		}

		private string Fund(ref string x)
		{
			string Fund = "";
			Fund = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
			return Fund;
		}

		private string Account(ref string x)
		{
			string Account = "";
			Account = Strings.Mid(x, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			return Account;
		}

		private void ShowMonths()
		{
			// add rows for months if monthly totals are selected
			int Level = 0;
			int counter;
			string TempStr;
			int counter2;
			// vbPorter upgrade warning: intCounterLimit As short --> As int	OnWriteFCConvert.ToDouble(
			int intCounterLimit = 0;
			int intMonth = 0;
			TempStr = "";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				Level = vs1.RowOutlineLevel(vs1.Rows - 1) + 1;
				vs1.Rows += 1;
				for (counter = 1; counter <= Level; counter++)
				{
					TempStr += "    ";
				}
				TempStr += "    " + CalculateMonth(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
				vs1.TextMatrix(vs1.Rows - 1, 1, TempStr);
				vs1.RowOutlineLevel(vs1.Rows - 1, FCConvert.ToInt16(Level));
				vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000);
				vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xE0E0E0);
			}
			else
			{
				Level = vs1.RowOutlineLevel(vs1.Rows - 1) + 1;
				if (Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) > Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))
				{
					intCounterLimit = FCConvert.ToInt32((12 - Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) + 1);
				}
				else
				{
					intCounterLimit = FCConvert.ToInt32((Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) - Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))) + 1);
				}
				intMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
				for (counter2 = 1; counter2 <= intCounterLimit; counter2++)
				{
					TempStr = "";
					vs1.Rows += 1;
					for (counter = 1; counter <= Level; counter++)
					{
						TempStr += "    ";
					}
					TempStr += "    " + CalculateMonth(intMonth);
					vs1.TextMatrix(vs1.Rows - 1, 1, TempStr);
					vs1.RowOutlineLevel(vs1.Rows - 1, FCConvert.ToInt16(Level));
					vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0xE0E0E0);
					if (intMonth < 12)
					{
						intMonth += 1;
					}
					else
					{
						intMonth = 1;
					}
				}
			}
		}
		// vbPorter upgrade warning: x As short --> As int	OnWrite(double, int)
		private string CalculateMonth(int x)
		{
			string CalculateMonth = "";
			switch (x)
			{
				case 1:
					{
						CalculateMonth = "January";
						break;
					}
				case 2:
					{
						CalculateMonth = "February";
						break;
					}
				case 3:
					{
						CalculateMonth = "March";
						break;
					}
				case 4:
					{
						CalculateMonth = "April";
						break;
					}
				case 5:
					{
						CalculateMonth = "May";
						break;
					}
				case 6:
					{
						CalculateMonth = "June";
						break;
					}
				case 7:
					{
						CalculateMonth = "July";
						break;
					}
				case 8:
					{
						CalculateMonth = "August";
						break;
					}
				case 9:
					{
						CalculateMonth = "September";
						break;
					}
				case 10:
					{
						CalculateMonth = "October";
						break;
					}
				case 11:
					{
						CalculateMonth = "November";
						break;
					}
				case 12:
					{
						CalculateMonth = "December";
						break;
					}
			}
			//end switch
			return CalculateMonth;
		}

		private void FundReport()
		{
			clsDRWrapper rsRanges = new clsDRWrapper();
			clsDRWrapper LedgerDescriptions = new clsDRWrapper();
			bool blnDescFound = false;
			string strOldType;
			LedgerDescriptions.OpenRecordset("SELECT * FROM LedgerTitles ORDER BY Fund, Account, Year");
			strOldType = "";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "S")
			{
				rs2.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "'");
				if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
				{
					rs2.MoveLast();
					rs2.MoveFirst();
					vs1.Rows += 1;
					if (LedgerDescriptions.FindFirstRecord2("Fund, Account", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
					{
						if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
						}
					}
					else
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + " - " + "UNKNOWN");
					}
					if (FundIsOOB_2(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
					{
						vs1.TextMatrix(vs1.Rows - 1, 1, vs1.TextMatrix(vs1.Rows - 1, 1) + "  (OOB)");
					}
					vs1.RowOutlineLevel(vs1.Rows - 1, 1);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
					vs1.IsSubtotal(vs1.Rows - 1, true);
					if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
					{
						rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY SecondAccountField, ThirdAccountField");
					}
					else
					{
						rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' ORDER BY GLAccountType, SecondAccountField, ThirdAccountField");
					}
					if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
					{
						rs2.MoveLast();
						rs2.MoveFirst();
						while (rs2.EndOfFile() != true)
						{
							if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
							{
								if (strOldType != FCConvert.ToString(rs2.Get_Fields_String("GLAccountType")))
								{
									AddGLTypeTitleLine_6(strOldType, rs2.Get_Fields_String("GLAccountType"));
								}
								strOldType = FCConvert.ToString(rs2.Get_Fields_String("GLAccountType"));
							}
							vs1.Rows += 1;
							if (!modAccountTitle.Statics.YearFlag)
							{
								blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
							}
							else
							{
								blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
							}
							if (blnDescFound)
							{
								if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
								}
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
							}
							vs1.RowOutlineLevel(vs1.Rows - 1, 2);
							vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
							if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
							{
								vs1.IsSubtotal(vs1.Rows - 1, true);
								ShowMonths();
							}
							rs2.MoveNext();
						}
					}
					if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
					{
						if (strOldType == "")
						{
							CreateGLTypeTitleLine_2("Assets");
							CreateGLTypeTitleLine_2("Liabilities");
							CreateGLTypeTitleLine_2("Fund Balance");
						}
						else if (strOldType == "1")
						{
							CreateGLTypeTitleLine_2("Liabilities");
							CreateGLTypeTitleLine_2("Fund Balance");
						}
						else if (strOldType == "2")
						{
							CreateGLTypeTitleLine_2("Fund Balance");
						}
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "D")
			{
				// more than 1 department
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND FirstAccountField >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp") + "' AND FirstAccountField <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						strOldType = "";
						vs1.Rows += 1;
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						if (FundIsOOB_2(rs5.Get_Fields_String("FirstAccountField")))
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, vs1.TextMatrix(vs1.Rows - 1, 1) + "  (OOB)");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField, ThirdAccountField");
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY GLAccountType, SecondAccountField, ThirdAccountField");
						}
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									if (strOldType != FCConvert.ToString(rs2.Get_Fields_String("GLAccountType")))
									{
										AddGLTypeTitleLine_6(strOldType, rs2.Get_Fields_String("GLAccountType"));
									}
									strOldType = FCConvert.ToString(rs2.Get_Fields_String("GLAccountType"));
								}
								vs1.Rows += 1;
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
								{
									vs1.IsSubtotal(vs1.Rows - 1, true);
									ShowMonths();
								}
								rs2.MoveNext();
							}
						}
						if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							if (strOldType == "")
							{
								CreateGLTypeTitleLine_2("Assets");
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "1")
							{
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "2")
							{
								CreateGLTypeTitleLine_2("Fund Balance");
							}
						}
						rs5.MoveNext();
					}
				}
			}
			else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "A")
			{
				// All Departments
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						strOldType = "";
						vs1.Rows += 1;
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						if (FundIsOOB_2(rs5.Get_Fields_String("FirstAccountField")))
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, vs1.TextMatrix(vs1.Rows - 1, 1) + "  (OOB)");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY SecondAccountField, ThirdAccountField");
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' ORDER BY GLAccountType, SecondAccountField, ThirdAccountField");
						}
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									if (strOldType != FCConvert.ToString(rs2.Get_Fields_String("GLAccountType")))
									{
										AddGLTypeTitleLine_6(strOldType, rs2.Get_Fields_String("GLAccountType"));
									}
									strOldType = FCConvert.ToString(rs2.Get_Fields_String("GLAccountType"));
								}
								vs1.Rows += 1;
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
								{
									vs1.IsSubtotal(vs1.Rows - 1, true);
									ShowMonths();
								}
								rs2.MoveNext();
							}
						}
						if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							if (strOldType == "")
							{
								CreateGLTypeTitleLine_2("Assets");
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "1")
							{
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "2")
							{
								CreateGLTypeTitleLine_2("Fund Balance");
							}
						}
						rs5.MoveNext();
					}
				}
			}
			else
			{
				// range of accounts
				rs5.OpenRecordset("SELECT DISTINCT FirstAccountField FROM Temp WHERE AccountType = 'G' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "'");
				if (rs5.EndOfFile() != true && rs5.BeginningOfFile() != true)
				{
					rs5.MoveLast();
					rs5.MoveFirst();
					while (rs5.EndOfFile() != true)
					{
						strOldType = "";
						vs1.Rows += 1;
						if (LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), ","))
						{
							if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
							}
							else
							{
								vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + LedgerDescriptions.Get_Fields_String("LongDescription"));
							}
						}
						else
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, rs5.Get_Fields_String("FirstAccountField") + " - " + "UNKNOWN");
						}
						if (FundIsOOB_2(rs5.Get_Fields_String("FirstAccountField")))
						{
							vs1.TextMatrix(vs1.Rows - 1, 1, vs1.TextMatrix(vs1.Rows - 1, 1) + "  (OOB)");
						}
						vs1.RowOutlineLevel(vs1.Rows - 1, 1);
						vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x80000009);
						vs1.IsSubtotal(vs1.Rows - 1, true);
						if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY SecondAccountField, ThirdAccountField");
						}
						else
						{
							rs2.OpenRecordset("SELECT * FROM Temp WHERE AccountType = 'G' AND FirstAccountField = '" + rs5.Get_Fields_String("FirstAccountField") + "' AND Account >= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount") + "' AND Account <= '" + modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount") + "' ORDER BY GLAccountType, SecondAccountField, ThirdAccountField");
						}
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							while (rs2.EndOfFile() != true)
							{
								if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
								{
									if (strOldType != FCConvert.ToString(rs2.Get_Fields_String("GLAccountType")))
									{
										AddGLTypeTitleLine_6(strOldType, rs2.Get_Fields_String("GLAccountType"));
									}
									strOldType = FCConvert.ToString(rs2.Get_Fields_String("GLAccountType"));
								}
								vs1.Rows += 1;
								if (!modAccountTitle.Statics.YearFlag)
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account, Year", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField") + "," + rs2.Get_Fields_String("ThirdAccountField"), ",");
								}
								else
								{
									blnDescFound = LedgerDescriptions.FindFirstRecord2("Fund, Account", rs5.Get_Fields_String("FirstAccountField") + "," + rs2.Get_Fields_String("SecondAccountField"), ",");
								}
								if (blnDescFound)
								{
									if (FCConvert.ToString(rs.Get_Fields_String("DescriptionLength")) == "S")
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("ShortDescription"));
									}
									else
									{
										vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + LedgerDescriptions.Get_Fields_String("LongDescription"));
									}
								}
								else
								{
									vs1.TextMatrix(vs1.Rows - 1, 1, "    " + rs2.Get_Fields_String("SecondAccountField") + "-" + rs2.Get_Fields_String("ThirdAccountField") + "  " + "UNKNOWN");
								}
								vs1.RowOutlineLevel(vs1.Rows - 1, 2);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, 0x8000000F);
								if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
								{
									vs1.IsSubtotal(vs1.Rows - 1, true);
									ShowMonths();
								}
								rs2.MoveNext();
							}
						}
						if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
						{
							if (strOldType == "")
							{
								CreateGLTypeTitleLine_2("Assets");
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "1")
							{
								CreateGLTypeTitleLine_2("Liabilities");
								CreateGLTypeTitleLine_2("Fund Balance");
							}
							else if (strOldType == "2")
							{
								CreateGLTypeTitleLine_2("Fund Balance");
							}
						}
						rs5.MoveNext();
					}
				}
			}
		}

		private void AddGLTypeTitleLine_6(string strOldType, string strNewType)
		{
			AddGLTypeTitleLine(ref strOldType, ref strNewType);
		}

		private void AddGLTypeTitleLine(ref string strOldType, ref string strNewType)
		{
			if (strOldType == "")
			{
				if (strNewType == "1")
				{
					CreateGLTypeTitleLine_2("Assets");
				}
				else if (strNewType == "2")
				{
					CreateGLTypeTitleLine_2("Assets");
					CreateGLTypeTitleLine_2("Liabilities");
				}
				else if (strNewType == "3")
				{
					CreateGLTypeTitleLine_2("Assets");
					CreateGLTypeTitleLine_2("Liabilities");
					CreateGLTypeTitleLine_2("Fund Balance");
				}
			}
			else if (strOldType == "1")
			{
				if (strNewType == "2")
				{
					CreateGLTypeTitleLine_2("Liabilities");
				}
				else if (strNewType == "3")
				{
					CreateGLTypeTitleLine_2("Liabilities");
					CreateGLTypeTitleLine_2("Fund Balance");
				}
			}
			else if (strOldType == "2")
			{
				if (strNewType == "3")
				{
					CreateGLTypeTitleLine_2("Fund Balance");
				}
			}
		}

		private void CreateGLTypeTitleLine_2(string strDescription)
		{
			CreateGLTypeTitleLine(ref strDescription);
		}

		private void CreateGLTypeTitleLine(ref string strDescription)
		{
			vs1.Rows += 1;
			vs1.TextMatrix(vs1.Rows - 1, 1, strDescription);
			vs1.RowOutlineLevel(vs1.Rows - 1, 2);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void PrepareTemp()
		{
			string strSQL1;
			string strSQL2;
			string strSQL3;
			string strSQL4 = "";
			string strSQL5 = "";
			string strSQL6;
			string strTotalSQL;
			string strTotalSQL2;
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, double, Decimal)
			Decimal curTotal;
			bool blnDataToShow = false;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strCurrentAccount = "";
			string strSQLFields;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			DeleteTemp();
			strSQL1 = "AccountType, Account, ";
			strSQL2 = "substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) AS FirstAccountField, ";
			strSQL3 = "substring(Account, 4 + convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)), convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) AS SecondAccountField, ";
			strSQLFields = "AccountType, Account, FirstAccountField, SecondAccountField";
			if (!modAccountTitle.Statics.YearFlag)
			{
				strSQL4 = "substring(Account, 5 + convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2)) + convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2)), convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 5, 2))) AS ThirdAccountField, ";
				strSQL5 = "";
				strSQLFields += ", ThirdAccountField";
			}
			else
			{
				strSQL4 = "";
				strSQL5 = "";
			}
			strSQL6 = "left(Account, 1) AS AccountType, Account, ";
			strTotalSQL = "SELECT DISTINCT " + strSQL1 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL = Strings.Left(strTotalSQL, strTotalSQL.Length - 2);
			strTotalSQL2 = "SELECT DISTINCT " + strSQL6 + strSQL2 + strSQL3 + strSQL4 + strSQL5;
			strTotalSQL2 = Strings.Left(strTotalSQL2, strTotalSQL2.Length - 2);
			if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowZeroBalance") != true)
			{
				// rs3.CreateStoredProcedure "LedgerAccounts", strTotalSQL2 & " FROM APJournalDetail WHERE left(Account, 1) = 'G' UNION " & strTotalSQL2 & " FROM EncumbranceDetail WHERE left(Account, 1) = 'G' UNION " & strTotalSQL2 & " FROM JournalEntries WHERE left(Account, 1) = 'G' UNION " & strTotalSQL & " FROM AccountMaster WHERE left(Account, 1) = 'G' AND CurrentBudget <> 0 ORDER BY Account"
				strCurrentAccount = "";
				rs3.OpenRecordset("SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'G' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'G' AND CurrentBudget <> 0 ) as LedgerAccounts ORDER BY Account");
				if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
				{
					rsTemp.OpenRecordset("SELECT * FROM Temp WHERE ID = 0");
					do
					{
						curTotal = 0;
						blnDataToShow = false;
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (strCurrentAccount != FCConvert.ToString(rs3.Get_Fields("Account")))
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strCurrentAccount = FCConvert.ToString(rs3.Get_Fields("Account"));
						}
						else
						{
							goto CheckNextAccount;
						}
						currentFund = FCConvert.ToString(rs3.Get_Fields_String("FirstAccountField"));
						currentAccount = FCConvert.ToString(rs3.Get_Fields_String("SecondAccountField"));
						if (!modAccountTitle.Statics.YearFlag)
						{
							CurrentSuffix = FCConvert.ToString(rs3.Get_Fields_String("ThirdAccountField"));
						}
						else
						{
							CurrentSuffix = "";
						}
						if (BudgetFlag || NetBudgetFlag || AdjustmentFlag)
						{
							curTotal = FCConvert.ToDecimal(GetOriginalBudget());
							if (NetBudgetFlag || AdjustmentFlag)
							{
								curTotal += FCConvert.ToDecimal(GetBudgetAdjustments());
							}
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}

						if (CurrentDCFlag)
						{
							curTotal = FCConvert.ToDecimal(GetCurrentDebits());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
							curTotal = FCConvert.ToDecimal(GetCurrentCredits());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (CurrentNetFlag && !CurrentDCFlag)
						{
							curTotal = FCConvert.ToDecimal(GetCurrentNet());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (YTDDCFlag)
						{
							curTotal = FCConvert.ToDecimal(GetYTDDebit());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
							curTotal = FCConvert.ToDecimal(GetYTDCredit());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (YTDNetFlag && !YTDDCFlag)
						{
							curTotal = FCConvert.ToDecimal(GetYTDNet());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (EncumbranceFlag)
						{
							curTotal = FCConvert.ToDecimal(GetEncumbrance());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (PendingFlag)
						{
							curTotal = FCConvert.ToDecimal(GetPending());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						if (BalanceFlag)
						{
							curTotal = FCConvert.ToDecimal(GetBalance());
							if (curTotal != 0)
							{
								blnDataToShow = true;
								goto SaveAccount;
							}
						}
						SaveAccount:
						;
						if (blnDataToShow)
						{
							rsTemp.AddNew();
							rsTemp.Set_Fields("AccountType", rs3.Get_Fields_String("AccountType"));
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsTemp.Set_Fields("Account", rs3.Get_Fields("Account"));
							rsTemp.Set_Fields("FirstAccountField", rs3.Get_Fields_String("FirstAccountField"));
							rsTemp.Set_Fields("SecondAccountField", rs3.Get_Fields_String("SecondAccountField"));
							if (!modAccountTitle.Statics.YearFlag)
							{
								rsTemp.Set_Fields("ThirdAccountField", rs3.Get_Fields_String("ThirdAccountField"));
							}
							rsTemp.Update();
						}
						CheckNextAccount:
						;
						rs3.MoveNext();
					}
					while (rs3.EndOfFile() != true);
				}
			}
			else
			{				
				rs3.Execute("INSERT INTO Temp (" + strSQLFields + ") SELECT * FROM (" + strTotalSQL2 + " FROM APJournalDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM EncumbranceDetail WHERE left(Account, 1) = 'G' UNION " + strTotalSQL2 + " FROM JournalEntries WHERE left(Account, 1) = 'G' UNION " + strTotalSQL + " FROM AccountMaster WHERE left(Account, 1) = 'G' AND Valid = 1 ) as LedgerAccounts", "Budgetary");
			}
			if (modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{				
                rs3.Execute("UPDATE Temp SET Temp.GLAccountType = AccountMaster.GLAccountType FROM Temp INNER JOIN AccountMaster ON Temp.Account = AccountMaster.Account", "Budgetary");
            }
			strPeriodCheck = strPeriodCheckHolder;
		}

		private void DeleteTemp()
		{
			rs3.Execute("DELETE FROM Temp", "Budgetary");
		}

		private void FillInInformation()
		{
			double temp = 0;
			int intDataCounter;
			bool blnDataInRow = false;
			int lngCounter;
			// vbPorter upgrade warning: curBalAmt As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curBalAmt;
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Refresh();
			//FC:FINAL:BBE - font size used in original for samll row height
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 1, vs1.Rows - 1, vs1.Cols - 1, 7);
			blnSwapValues = false;
			lngCounter = 2;
			OriginalBudgetCol = 0;
			BudgetAdjustmentCol = 0;
			NetBudgetCol = 0;
			CurrentDebitCol = 0;
			CurrentCreditCol = 0;
			YTDDebitCol = 0;
			YTDCreditCol = 0;
			BalanceCol = 0;
			BalanceCreditCol = 0;
			BalanceDebitCol = 0;
			EncumbranceCol = 0;
			PendingCol = 0;
			YTDNetCol = 0;
			CurrentNetCol = 0;
			for (CurrentRow = 2; CurrentRow <= vs1.Rows - 1; CurrentRow++)
			{
				//Application.DoEvents();
				if (CurrentRow == vs1.Rows)
				{
					break;
				}
				if (Strings.UCase(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))) == "LIABILITIES")
				{
					blnSwapValues = true;
				}
				else if (Strings.UCase(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))) == "ASSETS")
				{
					blnSwapValues = false;
				}
				vs1.RowData(CurrentRow, blnSwapValues);
				if (CurrentRow >= lngCounter + 20)
				{
					lngCounter = CurrentRow;
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CurrentRow) / (vs1.Rows - 1)) * 100);
					frmWait.InstancePtr.Refresh();
				}
				if (vs1.RowOutlineLevel(CurrentRow) == 1)
				{
					currentFund = Strings.Left(vs1.TextMatrix(CurrentRow, 1), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))));
					currentAccount = "";
					CurrentSuffix = "";
				}
				else if (Strings.Left(vs1.TextMatrix(CurrentRow, 1), 3) == "   " && Information.IsNumeric(Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))))
				{
					currentAccount = Strings.Left(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
					CurrentSuffix = Strings.Mid(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)), 2 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)))));
				}
				if (vs1.RowOutlineLevel(CurrentRow) >= intLowestLevel)
				{
					if (!modAccountTitle.Statics.YearFlag)
					{
						vs1.TextMatrix(CurrentRow, 2, "G " + currentFund + "-" + currentAccount + "-" + CurrentSuffix);
					}
					else
					{
						vs1.TextMatrix(CurrentRow, 2, "G " + currentFund + "-" + currentAccount);
					}
					CurrentCol = 2;
					temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
					if (temp == 0 && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
					{
						vs1.TextMatrix(CurrentRow, 2, "");
					}
					else
					{
						if (BudgetFlag)
						{
							OriginalBudgetCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetOriginalBudget()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetOriginalBudget() * -1));
							}
							CurrentCol += 1;
						}
						if (AdjustmentFlag)
						{
							BudgetAdjustmentCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetBudgetAdjustments()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetBudgetAdjustments() * -1));
							}
							CurrentCol += 1;
						}
						if (NetBudgetFlag)
						{
							NetBudgetCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetNetBudget()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetNetBudget() * -1));
							}
							CurrentCol += 1;
						}
						if (CurrentDCFlag)
						{
							CurrentDebitCol = CurrentCol;
							vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentDebits()));
							CurrentCol += 1;
							CurrentCreditCol = CurrentCol;
							vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentCredits()));
							CurrentCol += 1;
						}
						if (CurrentNetFlag)
						{
							CurrentNetCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentNet()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetCurrentNet() * -1));
							}
							CurrentCol += 1;
						}
						if (YTDDCFlag)
						{
							YTDDebitCol = CurrentCol;
							vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDDebit()));
							CurrentCol += 1;
							YTDCreditCol = CurrentCol;
							vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDCredit()));
							CurrentCol += 1;
						}
						if (YTDNetFlag)
						{
							YTDNetCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDNet()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetYTDNet() * -1));
							}
							CurrentCol += 1;
						}
						if (EncumbranceFlag)
						{
							EncumbranceCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetEncumbrance()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetEncumbrance() * -1));
							}
							CurrentCol += 1;
						}
						if (PendingFlag)
						{
							PendingCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetPending()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetPending() * -1));
							}
							CurrentCol += 1;
						}
						if (BalanceFlag)
						{
							BalanceCol = CurrentCol;
							if (!blnSwapValues)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetBalance()));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(GetBalance() * -1));
							}
							CurrentCol += 1;
						}
						if (BalanceDCFlag)
						{
							BalanceDebitCol = CurrentCol;
							BalanceCreditCol = CurrentCol + 1;
							curBalAmt = FCConvert.ToDecimal(GetBalance());
							if (curBalAmt < 0)
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, "0.00");
								vs1.TextMatrix(CurrentRow, CurrentCol + 1, FCConvert.ToString(curBalAmt * -1));
							}
							else
							{
								vs1.TextMatrix(CurrentRow, CurrentCol, FCConvert.ToString(curBalAmt));
								vs1.TextMatrix(CurrentRow, CurrentCol + 1, "0.00");
							}
							CurrentCol += 2;
						}
						if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
						{
							blnDataInRow = false;
							for (intDataCounter = 2; intDataCounter <= vs1.Cols - 1; intDataCounter++)
							{
								if (Conversion.Val(vs1.TextMatrix(CurrentRow, intDataCounter)) != 0)
								{
									blnDataInRow = true;
									break;
								}
							}
							if (blnDataInRow)
							{
								// do nothing
							}
							else
							{
								vs1.RemoveItem(CurrentRow);
								CurrentRow -= 1;
							}
						}
					}
				}
			}
		}

		private double GetCurrentDebits()
		{
			double GetCurrentDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
				}
				else
				{
					GetCurrentDebits = 0;
				}
				if (!EncumbranceFlag)
				{
					GetCurrentDebits += GetEncumbranceDebits();
				}
				return GetCurrentDebits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentDebits = 0;
			}
			if (!EncumbranceFlag)
			{
				GetCurrentDebits += GetEncumbranceDebits();
			}
			return GetCurrentDebits;
		}

		private double GetCurrentCredits()
		{
			double GetCurrentCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
				}
				else
				{
					GetCurrentCredits = 0;
				}
				GetCurrentCredits *= -1;
				if (!EncumbranceFlag)
				{
					GetCurrentCredits += GetEncumbranceCredits();
				}
				return GetCurrentCredits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
			}
			else
			{
				GetCurrentCredits = 0;
			}
			GetCurrentCredits *= -1;
			if (!EncumbranceFlag)
			{
				GetCurrentCredits += GetEncumbranceCredits();
			}
			return GetCurrentCredits;
		}

		private double GetCurrentNet()
		{
			double GetCurrentNet = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				GetCurrentNet = 0;
				return GetCurrentNet;
			}
			if (CurrentDCFlag)
			{
				GetCurrentNet = Conversion.Val(vs1.TextMatrix(CurrentRow, CurrentDebitCol)) - Conversion.Val(vs1.TextMatrix(CurrentRow, CurrentCreditCol));
			}
			else
			{
				GetCurrentNet = GetCurrentDebits() - GetCurrentCredits();
			}
			return GetCurrentNet;
		}

		private double GetYTDDebit()
		{
			double GetYTDDebit = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetYTDDebit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedDebitsTotal"));
							}
							else
							{
								GetYTDDebit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
				if (!EncumbranceFlag)
				{
					GetYTDDebit += GetEncumbranceDebits();
				}
				if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
				{
					GetYTDDebit += GetPendingDebits();
				}
				return GetYTDDebit;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedDebitsTotal"));
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDDebit = 0;
			}
			if (!EncumbranceFlag)
			{
				GetYTDDebit += GetEncumbranceDebits();
			}
			if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit()
		{
			double GetYTDCredit = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetYTDCredit = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PostedCreditsTotal"));
							}
							else
							{
								GetYTDCredit = 0;
							}
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
				GetYTDCredit *= -1;
				if (!EncumbranceFlag)
				{
					GetYTDCredit += GetEncumbranceCredits();
				}
				if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
				{
					GetYTDCredit += GetPendingCredits();
				}
				return GetYTDCredit;
			}
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = FCConvert.ToDouble(rsYTDActivity.Get_Fields("PostedCreditsTotal"));
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
			}
			else
			{
				GetYTDCredit = 0;
			}
			GetYTDCredit *= -1;
			if (!EncumbranceFlag)
			{
				GetYTDCredit += GetEncumbranceCredits();
			}
			if (!PendingFlag && FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")))
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private double GetYTDNet()
		{
			double GetYTDNet = 0;
			int temp;
			// temp = InStr(1, vs1.TextMatrix(CurrentRow, 1), "-")
			// If temp = 0 Then
			// GetYTDNet = 0
			// Exit Function
			// End If
			if (YTDDCFlag)
			{
				GetYTDNet = Conversion.Val(vs1.TextMatrix(CurrentRow, YTDDebitCol)) - Conversion.Val(vs1.TextMatrix(CurrentRow, YTDCreditCol));
			}
			else
			{
				GetYTDNet = GetYTDDebit() - GetYTDCredit();
			}
			return GetYTDNet;
		}

		private short LowMonthCalc(string x)
		{
			short LowMonthCalc = 0;
			if (x == "January")
			{
				LowMonthCalc = 1;
			}
			else if (x == "February")
			{
				LowMonthCalc = 2;
			}
			else if (x == "March")
			{
				LowMonthCalc = 3;
			}
			else if (x == "April")
			{
				LowMonthCalc = 4;
			}
			else if (x == "May")
			{
				LowMonthCalc = 5;
			}
			else if (x == "June")
			{
				LowMonthCalc = 6;
			}
			else if (x == "July")
			{
				LowMonthCalc = 7;
			}
			else if (x == "August")
			{
				LowMonthCalc = 8;
			}
			else if (x == "September")
			{
				LowMonthCalc = 9;
			}
			else if (x == "October")
			{
				LowMonthCalc = 10;
			}
			else if (x == "November")
			{
				LowMonthCalc = 11;
			}
			else if (x == "December")
			{
				LowMonthCalc = 12;
			}
			return LowMonthCalc;
		}

		private short HighMonthCalc(string x)
		{
			short HighMonthCalc = 0;
			if (x == "January")
			{
				HighMonthCalc = 1;
			}
			else if (x == "February")
			{
				HighMonthCalc = 2;
			}
			else if (x == "March")
			{
				HighMonthCalc = 3;
			}
			else if (x == "April")
			{
				HighMonthCalc = 4;
			}
			else if (x == "May")
			{
				HighMonthCalc = 5;
			}
			else if (x == "June")
			{
				HighMonthCalc = 6;
			}
			else if (x == "July")
			{
				HighMonthCalc = 7;
			}
			else if (x == "August")
			{
				HighMonthCalc = 8;
			}
			else if (x == "September")
			{
				HighMonthCalc = 9;
			}
			else if (x == "October")
			{
				HighMonthCalc = 10;
			}
			else if (x == "November")
			{
				HighMonthCalc = 11;
			}
			else if (x == "December")
			{
				HighMonthCalc = 12;
			}
			return HighMonthCalc;
		}
		// vbPorter upgrade warning: x As short --> As int	OnWrite(int, double)
		private string MonthCalc(int x)
		{
			string MonthCalc = "";
			switch (x)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}

		private double GetEncumbrance()
		{
			double GetEncumbrance = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
								GetEncumbrance = FCConvert.ToDouble(rsActivityDetail.Get_Fields("EncumbActivityTotal"));
							}
							else
							{
								GetEncumbrance = 0;
							}
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
				return GetEncumbrance;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = FCConvert.ToDouble(rsYTDActivity.Get_Fields("EncumbActivityTotal"));
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
			}
			else
			{
				GetEncumbrance = 0;
			}
			return GetEncumbrance;
		}

		private double GetEncumbranceDebits()
		{
			double GetEncumbranceDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
							}
							else
							{
								GetEncumbranceDebits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
							}
							else
							{
								GetEncumbranceDebits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceDebits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceDebitsTotal"));
							}
							else
							{
								GetEncumbranceDebits = 0;
							}
						}
					}
				}
				else
				{
					GetEncumbranceDebits = 0;
				}
				return GetEncumbranceDebits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							GetEncumbranceDebits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							GetEncumbranceDebits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [EncumbranceDebitsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceDebits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceDebitsTotal"));
						}
						else
						{
							GetEncumbranceDebits = 0;
						}
					}
				}
			}
			else
			{
				GetEncumbranceDebits = 0;
			}
			return GetEncumbranceDebits;
		}

		private double GetEncumbranceCredits()
		{
			double GetEncumbranceCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
							}
							else
							{
								GetEncumbranceCredits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
							}
							else
							{
								GetEncumbranceCredits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
								GetEncumbranceCredits = Conversion.Val(rsActivityDetail.Get_Fields("EncumbranceCreditsTotal")) * -1;
							}
							else
							{
								GetEncumbranceCredits = 0;
							}
						}
					}
				}
				else
				{
					GetEncumbranceCredits = 0;
				}
				return GetEncumbranceCredits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							GetEncumbranceCredits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							GetEncumbranceCredits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
						GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [EncumbranceCreditsTotal] not found!! (maybe it is an alias?)
							GetEncumbranceCredits = Conversion.Val(rsYTDActivity.Get_Fields("EncumbranceCreditsTotal")) * -1;
						}
						else
						{
							GetEncumbranceCredits = 0;
						}
					}
				}
			}
			else
			{
				GetEncumbranceCredits = 0;
			}
			return GetEncumbranceCredits;
		}

		private double GetPending()
		{
			double GetPending = 0;
			GetPending = GetPendingDebits() - GetPendingCredits();
			return GetPending;
		}

		private double GetPendingCredits()
		{
			double GetPendingCredits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingCreditsTotal"));
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
				}
				else
				{
					GetPendingCredits = 0;
				}
				GetPendingCredits *= -1;
				return GetPendingCredits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingCreditsTotal"));
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			GetPendingCredits *= -1;
			return GetPendingCredits;
		}

		private double GetPendingDebits()
		{
			double GetPendingDebits = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (currentAccount == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else if (CurrentSuffix == "")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = FCConvert.ToDouble(rsActivityDetail.Get_Fields("PendingDebitsTotal"));
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
				}
				else
				{
					GetPendingDebits = 0;
				}
				return GetPendingDebits;
			}
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = FCConvert.ToDouble(rsCurrentActivity.Get_Fields("PendingDebitsTotal"));
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private void GetCompleteTotals()
		{
			int counter;
			int counter2;
			double total = 0;
			vs1.Rows += 1;
			vs1.TextMatrix(vs1.Rows - 1, 1, "Final Totals");
			for (counter = 2; counter <= vs1.Cols - 1; counter++)
			{
				total = 0;
				for (counter2 = 2; counter2 <= vs1.Rows - 2; counter2++)
				{
					if (vs1.RowOutlineLevel(counter2) == 1)
					{
						total += FCConvert.ToDouble(vs1.TextMatrix(counter2, counter));
					}
				}
				vs1.TextMatrix(vs1.Rows - 1, counter, FCConvert.ToString(total));
			}
			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.Blue);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vs1.Rows - 1, 0, vs1.Rows - 1, vs1.Cols - 1, Color.White);
			vs1.RowOutlineLevel(vs1.Rows - 1, 1);
			vs1.IsSubtotal(vs1.Rows - 1, true);
		}

		private void InsertTitles()
		{
			clsDRWrapper rsRanges = new clsDRWrapper();
			int intOutlineLevel;
			string strName = "";
			int lngCurrentRow;
			string strDescription = "";
			int counter = 0;
			int intJump = 0;
			bool blnAdded = false;
			bool blnFirstFund;
			int intloop;
			intOutlineLevel = 1;
			lngCurrentRow = 2;
			CheckLoop:
			;
			if (strName == "")
			{
				strName = "A";
				strDescription = "Assets";
				intJump = 1;
			}
			else if (strName == "A")
			{
				strName = "L";
				strDescription = "Liabilities";
				intJump = 2;
			}
			else if (strName == "L")
			{
				strName = "F";
				strDescription = "Fund Balance";
				intJump = 3;
			}
			else
			{
				for (intloop = 1; intloop <= vs1.Rows - 1; intloop++)
				{
					if (vs1.TextMatrix(intloop, 1) == "Assets" || vs1.TextMatrix(intloop, 1) == "Liabilities" || vs1.TextMatrix(intloop, 1) == "Fund Balance")
					{
						vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, intloop, 0, intloop, vs1.Cols - 1, true);
						//FC:FINAL:BBE - font size used in original for samll row height
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpFontSize, intloop, 0, intloop, vs1.Cols - 1, vs1.Font.Size + 1);
					}
				}
				return;
			}
			blnFirstFund = true;
			rsRanges.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = '" + strName + "'");
			// show the big heading for the section
			if (strName == "A")
			{
				counter = 2;
				while (counter <= vs1.Rows - 1)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						lngCurrentRow = counter + intJump;
						vs1.AddItem("", lngCurrentRow);
						vs1.TextMatrix(lngCurrentRow, 1, strDescription);
						vs1.RowOutlineLevel(lngCurrentRow, 2);
						vs1.IsSubtotal(lngCurrentRow, true);
					}
					counter += 1;
				}
			}
			else
			{
				blnAdded = false;
				counter = 2;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
						if (FCConvert.ToDouble(rsRanges.Get_Fields("Low")) <= FCConvert.ToDouble(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && Information.IsNumeric(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && blnAdded != true)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, strDescription);
							vs1.RowOutlineLevel(lngCurrentRow, 2);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								vs1.TextMatrix(lngCurrentRow, 1, strDescription);
								vs1.RowOutlineLevel(lngCurrentRow, 2);
								vs1.IsSubtotal(lngCurrentRow, true);
								counter += 1;
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, strDescription);
					vs1.RowOutlineLevel(lngCurrentRow, 2);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			// if there is a level 2 subtotal show it
			if (Conversion.Val(rsRanges.Get_Fields_String("High1")) != 0)
			{
				if (strName == "A")
				{
					counter = 2;
					while (counter <= vs1.Rows - 1)
					{
						if (vs1.RowOutlineLevel(counter) == 1)
						{
							blnAdded = true;
							lngCurrentRow = counter + 2;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name1"))));
							vs1.RowOutlineLevel(lngCurrentRow, 3);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
						counter += 1;
					}
				}
				else
				{
					blnAdded = false;
					counter = 2;
					blnFirstFund = true;
					while (counter <= vs1.Rows - 1)
					{
						if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
						{
							if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low1")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
							{
								blnAdded = true;
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name1"))));
								vs1.RowOutlineLevel(lngCurrentRow, 3);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
						}
						else if (vs1.RowOutlineLevel(counter) == 1)
						{
							if (!blnFirstFund)
							{
								if (!blnAdded)
								{
									lngCurrentRow = counter;
									vs1.AddItem("", lngCurrentRow);
									counter += 1;
									vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name1"))));
									vs1.RowOutlineLevel(lngCurrentRow, 3);
									vs1.IsSubtotal(lngCurrentRow, true);
								}
								else
								{
									blnAdded = false;
								}
							}
							else
							{
								blnFirstFund = false;
							}
						}
						counter += 1;
					}
					if (!blnAdded)
					{
						lngCurrentRow = counter;
						vs1.AddItem("", lngCurrentRow);
						vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name1"))));
						vs1.RowOutlineLevel(lngCurrentRow, 3);
						vs1.IsSubtotal(lngCurrentRow, true);
					}
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High11")) != 0)
			{
				if (strName == "A")
				{
					counter = 2;
					while (counter <= vs1.Rows - 1)
					{
						if (vs1.RowOutlineLevel(counter) == 1)
						{
							lngCurrentRow = counter + 3;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name11"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
						counter += 1;
					}
				}
				else
				{
					blnAdded = false;
					counter = 2;
					blnFirstFund = true;
					while (counter <= vs1.Rows - 1)
					{
						if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
						{
							if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low11")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
							{
								blnAdded = true;
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name11"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
						}
						else if (vs1.RowOutlineLevel(counter) == 1)
						{
							if (!blnFirstFund)
							{
								if (!blnAdded)
								{
									lngCurrentRow = counter;
									vs1.AddItem("", lngCurrentRow);
									counter += 1;
									vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name11"))));
									vs1.RowOutlineLevel(lngCurrentRow, 4);
									vs1.IsSubtotal(lngCurrentRow, true);
								}
								else
								{
									blnAdded = false;
								}
							}
							else
							{
								blnFirstFund = false;
							}
						}
						counter += 1;
					}
					if (!blnAdded)
					{
						lngCurrentRow = counter;
						vs1.AddItem("", lngCurrentRow);
						vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name11"))));
						vs1.RowOutlineLevel(lngCurrentRow, 4);
						vs1.IsSubtotal(lngCurrentRow, true);
					}
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High12")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low12")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name12"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name12"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name12"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High13")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low13")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name13"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name13"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name13"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High2")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low2")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name2"))));
							vs1.RowOutlineLevel(lngCurrentRow, 3);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name2"))));
								vs1.RowOutlineLevel(lngCurrentRow, 3);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name2"))));
					vs1.RowOutlineLevel(lngCurrentRow, 3);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High21")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low21")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name21"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name21"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name21"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High22")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low22")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name22"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name22"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name22"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High23")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low23")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name23"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name23"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name23"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High3")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low3")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name3"))));
							vs1.RowOutlineLevel(lngCurrentRow, 3);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name3"))));
								vs1.RowOutlineLevel(lngCurrentRow, 3);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name3"))));
					vs1.RowOutlineLevel(lngCurrentRow, 3);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High31")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low31")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name31"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name31"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name31"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High32")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low32")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name32"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name32"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name32"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High33")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low33")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name33"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name33"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name33"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High4")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low4")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name4"))));
							vs1.RowOutlineLevel(lngCurrentRow, 3);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name4"))));
								vs1.RowOutlineLevel(lngCurrentRow, 3);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name4"))));
					vs1.RowOutlineLevel(lngCurrentRow, 3);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High41")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low41")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name41"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name41"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name41"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High42")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low42")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name42"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name42"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name42"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			if (Conversion.Val(rsRanges.Get_Fields_String("High43")) != 0)
			{
				blnAdded = false;
				counter = 2;
				blnFirstFund = true;
				while (counter <= vs1.Rows - 1)
				{
					if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   " && !modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						if (FCConvert.ToInt32(rsRanges.Get_Fields_String("Low43")) <= FCConvert.ToInt32(Strings.Left(Strings.Trim(vs1.TextMatrix(counter, 1)), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) && !blnAdded)
						{
							blnAdded = true;
							lngCurrentRow = counter;
							vs1.AddItem("", lngCurrentRow);
							counter += 1;
							vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name43"))));
							vs1.RowOutlineLevel(lngCurrentRow, 4);
							vs1.IsSubtotal(lngCurrentRow, true);
						}
					}
					else if (vs1.RowOutlineLevel(counter) == 1)
					{
						if (!blnFirstFund)
						{
							if (!blnAdded)
							{
								lngCurrentRow = counter;
								vs1.AddItem("", lngCurrentRow);
								counter += 1;
								vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name43"))));
								vs1.RowOutlineLevel(lngCurrentRow, 4);
								vs1.IsSubtotal(lngCurrentRow, true);
							}
							else
							{
								blnAdded = false;
							}
						}
						else
						{
							blnFirstFund = false;
						}
					}
					counter += 1;
				}
				if (!blnAdded)
				{
					lngCurrentRow = counter;
					vs1.AddItem("", lngCurrentRow);
					vs1.TextMatrix(lngCurrentRow, 1, Strings.Trim(FCConvert.ToString(rsRanges.Get_Fields_String("Name43"))));
					vs1.RowOutlineLevel(lngCurrentRow, 4);
					vs1.IsSubtotal(lngCurrentRow, true);
				}
			}
			goto CheckLoop;
		}

		private void SetOutlineLevels()
		{
			int counter;
			int intOutlineLevel = 1;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   ")
				{
					if (!modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						vs1.RowOutlineLevel(counter, FCConvert.ToInt16(intOutlineLevel + 1));
					}
					else
					{
						vs1.RowOutlineLevel(counter, FCConvert.ToInt16(intOutlineLevel + 2));
					}
				}
				else
				{
					intOutlineLevel = vs1.RowOutlineLevel(counter);
				}
			}
		}

		private void SetOutlineLevelsBasedOnLowestLevel()
		{
			int counter;
			int intOutlineLevel;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (Strings.Left(vs1.TextMatrix(counter, 1), 3) == "   ")
				{
					if (modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(counter, 1))))
					{
						vs1.RowOutlineLevel(counter, FCConvert.ToInt16(intLowestLevel + 1));
					}
					else
					{
						vs1.RowOutlineLevel(counter, FCConvert.ToInt16(intLowestLevel));
					}
				}
				else
				{
					// do nothing
				}
			}
		}
		// vbPorter upgrade warning: Col As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: Row As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: outlinelevel As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(short, Decimal)
		private Double CalculateSubtotals(ref int Col, ref int Row, ref int outlinelevel)
		{
			Double CalculateSubtotalsRet = 0;
			int RowCounter;
			int lngOutlineLevelToUse = 0;
			RowCounter = Row + 1;
			if (RowCounter > vs1.Rows - 1)
			{
				CalculateSubtotalsRet = 0;
				return CalculateSubtotalsRet;
			}
			while (vs1.RowOutlineLevel(RowCounter) > outlinelevel)
			{
				if (RowCounter < vs1.Rows - 1)
				{
					if (vs1.RowOutlineLevel(RowCounter) + 1 < vs1.RowOutlineLevel(RowCounter + 1))
					{
						lngOutlineLevelToUse = vs1.RowOutlineLevel(RowCounter + 1) - 1;
					}
					else
					{
						lngOutlineLevelToUse = vs1.RowOutlineLevel(RowCounter);
					}
				}
				if (Strings.Trim(vs1.TextMatrix(RowCounter, Col)) == "" || Strings.Trim(vs1.TextMatrix(RowCounter, Col)) == "0")
				{
					vs1.TextMatrix(RowCounter, Col, FCConvert.ToString(CalculateSubtotals(ref Col, ref RowCounter, ref lngOutlineLevelToUse)));
				}
				if (!modBudgetaryMaster.IsMonth_2(Strings.Trim(vs1.TextMatrix(RowCounter, 1))) && vs1.RowOutlineLevel(RowCounter) == outlinelevel + 1)
				{
					if (Col != CurrentCreditCol && Col != CurrentDebitCol && Col != YTDCreditCol && Col != YTDDebitCol && Col != BalanceCreditCol && Col != BalanceDebitCol)
					{
						//FC:FINAL:DSE:#864 outline is 1 based
						//if (outlinelevel == 0 && FCConvert.CBool(vs1.RowData(RowCounter)) == true)
						if (outlinelevel == 1 && FCConvert.ToBoolean(vs1.RowData(RowCounter)))
						{
							CalculateSubtotalsRet -= FCConvert.ToDouble(vs1.TextMatrix(RowCounter, Col));
						}
						else
						{
							CalculateSubtotalsRet += FCConvert.ToDouble(vs1.TextMatrix(RowCounter, Col));
						}
					}
					else
					{
						CalculateSubtotalsRet += FCConvert.ToDouble(vs1.TextMatrix(RowCounter, Col));
					}
				}
				RowCounter += 1;
				if (RowCounter > vs1.Rows - 1)
				{
					return CalculateSubtotalsRet;
				}
			}
			return CalculateSubtotalsRet;
		}

		private void GetSubTotals()
		{
			int counter;
			int lngOutlineLevelToUse = 0;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if ((Strings.Trim(vs1.TextMatrix(counter, 2)) == "" || Strings.Trim(vs1.TextMatrix(counter, 2)) == "0") && vs1.RowOutlineLevel(counter) < intLowestLevel)
				{
					if (counter < vs1.Rows - 1)
					{
						if (vs1.RowOutlineLevel(counter) + 1 < vs1.RowOutlineLevel(counter + 1))
						{
							lngOutlineLevelToUse = vs1.RowOutlineLevel(counter + 1) - 1;
						}
						else
						{
							lngOutlineLevelToUse = vs1.RowOutlineLevel(counter);
						}
					}
					if (BudgetFlag)
					{
						vs1.TextMatrix(counter, OriginalBudgetCol, FCConvert.ToString(CalculateSubtotals(ref OriginalBudgetCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (AdjustmentFlag)
					{
						vs1.TextMatrix(counter, BudgetAdjustmentCol, FCConvert.ToString(CalculateSubtotals(ref BudgetAdjustmentCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (NetBudgetFlag)
					{
						vs1.TextMatrix(counter, NetBudgetCol, FCConvert.ToString(CalculateSubtotals(ref NetBudgetCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (CurrentDCFlag)
					{
						vs1.TextMatrix(counter, CurrentDebitCol, FCConvert.ToString(CalculateSubtotals(ref CurrentDebitCol, ref counter, ref lngOutlineLevelToUse)));
						vs1.TextMatrix(counter, CurrentCreditCol, FCConvert.ToString(CalculateSubtotals(ref CurrentCreditCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (CurrentNetFlag)
					{
						vs1.TextMatrix(counter, CurrentNetCol, FCConvert.ToString(CalculateSubtotals(ref CurrentNetCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (YTDDCFlag)
					{
						vs1.TextMatrix(counter, YTDDebitCol, FCConvert.ToString(CalculateSubtotals(ref YTDDebitCol, ref counter, ref lngOutlineLevelToUse)));
						vs1.TextMatrix(counter, YTDCreditCol, FCConvert.ToString(CalculateSubtotals(ref YTDCreditCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (YTDNetFlag)
					{
						vs1.TextMatrix(counter, YTDNetCol, FCConvert.ToString(CalculateSubtotals(ref YTDNetCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (EncumbranceFlag)
					{
						vs1.TextMatrix(counter, EncumbranceCol, FCConvert.ToString(CalculateSubtotals(ref EncumbranceCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (PendingFlag)
					{
						vs1.TextMatrix(counter, PendingCol, FCConvert.ToString(CalculateSubtotals(ref PendingCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (BalanceFlag)
					{
						vs1.TextMatrix(counter, BalanceCol, FCConvert.ToString(CalculateSubtotals(ref BalanceCol, ref counter, ref lngOutlineLevelToUse)));
					}
					if (BalanceDCFlag)
					{
						vs1.TextMatrix(counter, BalanceDebitCol, FCConvert.ToString(CalculateSubtotals(ref BalanceDebitCol, ref counter, ref lngOutlineLevelToUse)));
						vs1.TextMatrix(counter, BalanceCreditCol, FCConvert.ToString(CalculateSubtotals(ref BalanceCreditCol, ref counter, ref lngOutlineLevelToUse)));
					}
				}
			}
		}

		private void SetColors()
		{
			int counter;
			//for (counter = 2; counter <= vs1.Rows - 2; counter++)
			//{
			//	//FC:FINAL:BBE - first Level for RowOutlineLevel corrected to 1
			//	//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(vs1.RowOutlineLevel(counter)));
			//	vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(vs1.RowOutlineLevel(counter) - 1));
			//}
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, counter, 0, counter, vs1.Cols - 1, modColorScheme.SetGridColor(99));
			modColorScheme.ColorGrid(vs1, 2, vs1.Rows - 2, 0, vs1.Cols - 1);
			modColorScheme.ColorGrid(vs1, vs1.Rows - 1, vs1.Rows - 1, 0, vs1.Cols - 1, true);
		}

		private int GetLowLevel()
		{
			int GetLowLevel = 1;
			int counter;
			GetLowLevel = 1;
			for (counter = 2; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) > GetLowLevel)
				{
					GetLowLevel = vs1.RowOutlineLevel(counter);
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Totals")) == "M")
			{
				GetLowLevel -= 1;
			}
			return GetLowLevel;
		}

		private double GetBalance()
		{
			double GetBalance = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				GetBalance = 0;
				return GetBalance;
			}
			if (!EncumbranceFlag)
			{
				GetBalance = GetNetBudget() + GetYTDDebit() - GetYTDCredit();
			}
			else
			{
				GetBalance = GetNetBudget() + GetYTDDebit() - GetYTDCredit() + GetEncumbrance();
			}
			if (PendingFlag)
			{
				GetBalance += GetPending();
			}
			return GetBalance;
		}

		private double GetNetBudget()
		{
			double GetNetBudget = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				GetNetBudget = 0;
				return GetNetBudget;
			}
			GetNetBudget = GetOriginalBudget() + GetBudgetAdjustments();
			return GetNetBudget;
		}

		private double GetOriginalBudget()
		{
			double GetOriginalBudget = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				GetOriginalBudget = 0;
				return GetOriginalBudget;
			}
			if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsBudgetInfo.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
					}
					else
					{
						if (rsBudgetInfo.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsBudgetInfo.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
					}
					else
					{
						if (rsBudgetInfo.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsBudgetInfo.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsBudgetInfo.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
					}
					else
					{
						if (rsBudgetInfo.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = FCConvert.ToDouble(rsBudgetInfo.Get_Fields("OriginalBudgetTotal"));
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
			}
			else
			{
				GetOriginalBudget = 0;
			}
			return GetOriginalBudget;
		}

		private double GetBudgetAdjustments()
		{
			double GetBudgetAdjustments = 0;
			int temp;
			temp = Strings.InStr(1, vs1.TextMatrix(CurrentRow, 1), "-", CompareConstants.vbBinaryCompare);
			if (temp == 0)
			{
				if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail")) == "Fu")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, Period", currentFund + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Ac")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Period", currentFund + "," + currentAccount + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("LowDetail") == "Su")
					{
						// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsActivityDetail.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsActivityDetail.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsActivityDetail.Get_Fields_String("Suffix")) == CurrentSuffix && FCConvert.ToInt32(rsActivityDetail.Get_Fields("Period")) == HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1))))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							if (rsActivityDetail.FindFirstRecord2("Fund, LedgerAccount, Suffix, Period", currentFund + "," + currentAccount + "," + CurrentSuffix + "," + FCConvert.ToString(HighMonthCalc(Strings.Trim(vs1.TextMatrix(CurrentRow, 1)))), ","))
							{
								// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
								GetBudgetAdjustments = FCConvert.ToDouble(rsActivityDetail.Get_Fields("BudgetAdjustmentsTotal"));
							}
							else
							{
								GetBudgetAdjustments = 0;
							}
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
				return GetBudgetAdjustments;
			}
			// Dave 3/5/07 Changed rsCurrentactivity to rsYTDActivity in if statement
			if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
			{
				if (currentAccount == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord("Fund", currentFund))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else if (CurrentSuffix == "")
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount", currentFund + "," + currentAccount, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsYTDActivity.Get_Fields("Fund")) == currentFund && FCConvert.ToString(rsYTDActivity.Get_Fields_String("LedgerAccount")) == currentAccount && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Suffix")) == CurrentSuffix)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Fund, LedgerAccount, Suffix", currentFund + "," + currentAccount + "," + CurrentSuffix, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = FCConvert.ToDouble(rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal"));
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
			}
			else
			{
				GetBudgetAdjustments = 0;
			}
			return GetBudgetAdjustments;
		}

		private void RetrieveInfo()
		{
			int HighDate = 0;
			int LowDate;
			string strPeriodCheckHolder;
			string strEncumbData = "";
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth"))));
			}
			else
			{
				HighDate = HighMonthCalc(MonthCalc(FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth"))));
			}
			LowDate = LowMonthCalc(MonthCalc(modBudgetaryMaster.Statics.FirstMonth));
			strPeriodCheckHolder = strPeriodCheck;
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("IncludePending")) || FCConvert.ToBoolean(rs.Get_Fields_Boolean("SeperatePending")))
			{
				strEncumbData = "SUM(EncumbActivity) + SUM(PendingEncumbActivity) as EncumbActivityTotal";
			}
			else
			{
				strEncumbData = "SUM(EncumbActivity) as EncumbActivityTotal";
			}
			// If SearchResults.Fields["LowDetail"] = "Su" Then
			rsYTDActivity.OpenRecordset("SELECT Fund, LedgerAccount, Suffix, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Fund, LedgerAccount, Suffix");
			rsBudgetInfo.OpenRecordset("SELECT Fund, LedgerAccount, Suffix, SUM(OriginalBudget) AS OriginalBudgetTotal FROM LedgerReportInfo GROUP BY Fund, LedgerAccount, Suffix");
			strPeriodCheck = strPeriodCheckHolder;
			if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
			{
				rsCurrentActivity.OpenRecordset("SELECT Fund, LedgerAccount, Suffix, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + " GROUP BY Fund, LedgerAccount, Suffix");
				rsActivityDetail.OpenRecordset("SELECT Period, Fund, LedgerAccount, Suffix, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + " GROUP BY Fund, LedgerAccount, Suffix, Period");
			}
			else
			{
				rsCurrentActivity.OpenRecordset("SELECT Fund, LedgerAccount, Suffix, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + " GROUP BY Fund, LedgerAccount, Suffix");
				rsActivityDetail.OpenRecordset("SELECT Period, Fund, LedgerAccount, Suffix, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, " + strEncumbData + ", SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " + FCConvert.ToString(LowMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")))))) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighMonthCalc(MonthCalc(FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")))))) + " GROUP BY Fund, LedgerAccount, Suffix, Period");
			}
			// ElseIf SearchResults.Fields["LowDetail"] = "Ac" Then
			// rsYTDActivity.OpenRecordset "SELECT Fund, LedgerAccount, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " & LowDate & strPeriodCheck & " Period <= " & HighDate & ")) GROUP BY Fund, LedgerAccount"
			// strPeriodCheck = strPeriodCheckHolder
			// If SearchResults.Fields["SelectedMonths"] = "S" Then
			// rsCurrentActivity.OpenRecordset "SELECT Fund, LedgerAccount, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & " GROUP BY Fund, LedgerAccount"
			// rsActivityDetail.OpenRecordset "SELECT Period, Fund, LedgerAccount, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & " GROUP BY Fund, LedgerAccount, Period"
			// Else
			// rsCurrentActivity.OpenRecordset "SELECT Fund, LedgerAccount, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["EndMonth"]))) & " GROUP BY Fund, LedgerAccount"
			// rsActivityDetail.OpenRecordset "SELECT Period, Fund, LedgerAccount, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["EndMonth"]))) & " GROUP BY Fund, LedgerAccount, Period"
			// End If
			// ElseIf SearchResults.Fields["LowDetail"] = "Fu" Then
			// rsYTDActivity.OpenRecordset "SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE (Period = 0 OR (Period >= " & LowDate & strPeriodCheck & " Period <= " & HighDate & ")) GROUP BY Fund"
			// strPeriodCheck = strPeriodCheckHolder
			// If SearchResults.Fields["SelectedMonths"] = "S" Then
			// rsCurrentActivity.OpenRecordset "SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & " GROUP BY Fund"
			// rsActivityDetail.OpenRecordset "SELECT Period, Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & " GROUP BY Fund, Period"
			// Else
			// rsCurrentActivity.OpenRecordset "SELECT Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["EndMonth"]))) & " GROUP BY Fund"
			// rsActivityDetail.OpenRecordset "SELECT Period, Fund, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(EncumbranceDebits) as EncumbranceDebitsTotal, SUM(EncumbranceCredits) as EncumbranceCreditsTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo WHERE Period >= " & LowMonthCalc(MonthCalc(Val(SearchResults.Fields["BegMonth"]))) & strPeriodCheck & " Period <= " & HighMonthCalc(MonthCalc(Val(SearchResults.Fields["EndMonth"]))) & " GROUP BY Fund, Period"
			// End If
			// End If
		}

		private bool FundIsOOB_2(string strFundToCheck)
		{
			return FundIsOOB(ref strFundToCheck);
		}

		private bool FundIsOOB(ref string strFundToCheck)
		{
			bool FundIsOOB = false;
			clsDRWrapper rsAssets = new clsDRWrapper();
			clsDRWrapper rsLiabilities = new clsDRWrapper();
			clsDRWrapper rsFundBalance = new clsDRWrapper();
			clsDRWrapper rsRangeInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curAssetsBalance As Decimal	OnWrite(double, short)
			Decimal curAssetsBalance = 0.0M;
			// vbPorter upgrade warning: curLiabilitiesBalance As Decimal	OnWrite(double, short)
			Decimal curLiabilitiesBalance = 0.0M;
			// vbPorter upgrade warning: curFundBalanceBalance As Decimal	OnWrite(double, short)
			Decimal curFundBalanceBalance = 0.0M;
			if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts)
			{
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'A'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					rsAssets.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund");
					if (rsAssets.EndOfFile() != true && rsAssets.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curAssetsBalance = FCConvert.ToDecimal(rsAssets.Get_Fields("OriginalBudgetTotal") + rsAssets.Get_Fields("BudgetAdjustmentsTotal") + rsAssets.Get_Fields("PostedDebitsTotal") + rsAssets.Get_Fields("EncumbActivityTotal") + rsAssets.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curAssetsBalance = 0;
					}
				}
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					rsLiabilities.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund");
					if (rsLiabilities.EndOfFile() != true && rsLiabilities.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curLiabilitiesBalance = FCConvert.ToDecimal(rsLiabilities.Get_Fields("OriginalBudgetTotal") + rsLiabilities.Get_Fields("BudgetAdjustmentsTotal") + rsLiabilities.Get_Fields("PostedDebitsTotal") + rsLiabilities.Get_Fields("EncumbActivityTotal") + rsLiabilities.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curLiabilitiesBalance = 0;
					}
				}
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'F'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					rsFundBalance.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND LedgerAccount >= '" + rsRangeInfo.Get_Fields("Low") + "' AND LedgerAccount <= '" + rsRangeInfo.Get_Fields("High") + "' GROUP BY Fund");
					if (rsFundBalance.EndOfFile() != true && rsFundBalance.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curFundBalanceBalance = FCConvert.ToDecimal(rsFundBalance.Get_Fields("OriginalBudgetTotal") + rsFundBalance.Get_Fields("BudgetAdjustmentsTotal") + rsFundBalance.Get_Fields("PostedDebitsTotal") + rsFundBalance.Get_Fields("EncumbActivityTotal") + rsFundBalance.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curFundBalanceBalance = 0;
					}
				}
			}
			else
			{
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'A'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					rsAssets.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND Account IN (SELECT Account FROM Temp WHERE GLAccountType = '1') GROUP BY Fund");
					if (rsAssets.EndOfFile() != true && rsAssets.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curAssetsBalance = FCConvert.ToDecimal(rsAssets.Get_Fields("OriginalBudgetTotal") + rsAssets.Get_Fields("BudgetAdjustmentsTotal") + rsAssets.Get_Fields("PostedDebitsTotal") + rsAssets.Get_Fields("EncumbActivityTotal") + rsAssets.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curAssetsBalance = 0;
					}
				}
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'L'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					rsLiabilities.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND Account IN (SELECT Account FROM Temp WHERE GLAccountType = '2') GROUP BY Fund");
					if (rsLiabilities.EndOfFile() != true && rsLiabilities.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curLiabilitiesBalance = FCConvert.ToDecimal(rsLiabilities.Get_Fields("OriginalBudgetTotal") + rsLiabilities.Get_Fields("BudgetAdjustmentsTotal") + rsLiabilities.Get_Fields("PostedDebitsTotal") + rsLiabilities.Get_Fields("EncumbActivityTotal") + rsLiabilities.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curLiabilitiesBalance = 0;
					}
				}
				rsRangeInfo.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = 'F'");
				if (rsRangeInfo.EndOfFile() != true && rsRangeInfo.BeginningOfFile() != true)
				{
					rsFundBalance.OpenRecordset("SELECT Fund, SUM(OriginalBudget) as OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal FROM LedgerReportInfo WHERE Fund = '" + strFundToCheck + "' AND Account IN (SELECT Account FROM Temp WHERE GLAccountType = '3') GROUP BY Fund");
					if (rsFundBalance.EndOfFile() != true && rsFundBalance.BeginningOfFile() != true)
					{
						// GetNetBudget + GetYTDDebit - GetYTDCredit + GetEncumbrance
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						curFundBalanceBalance = FCConvert.ToDecimal(rsFundBalance.Get_Fields("OriginalBudgetTotal") + rsFundBalance.Get_Fields("BudgetAdjustmentsTotal") + rsFundBalance.Get_Fields("PostedDebitsTotal") + rsFundBalance.Get_Fields("EncumbActivityTotal") + rsFundBalance.Get_Fields("PostedCreditsTotal"));
					}
					else
					{
						curFundBalanceBalance = 0;
					}
				}
			}
			if (curAssetsBalance == ((curLiabilitiesBalance * -1) + (curFundBalanceBalance * -1)))
			{
				FundIsOOB = false;
			}
			else
			{
				FundIsOOB = true;
			}
			return FundIsOOB;
		}
	}
}
