﻿namespace TWBD0000
{
    partial class SelectPrintItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcHeader1 = new fecherFoundation.FCHeader();
            this.comboBox1 = new Wisej.Web.ComboBox();
            this.button1 = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // fcHeader1
            // 
            this.fcHeader1.AppearanceKey = "Header";
            this.fcHeader1.AutoSize = true;
            this.fcHeader1.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.fcHeader1.Location = new System.Drawing.Point(30, 26);
            this.fcHeader1.Name = "fcHeader1";
            this.fcHeader1.Size = new System.Drawing.Size(126, 30);
            this.fcHeader1.TabIndex = 0;
            this.fcHeader1.Text = "fcHeader1";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.comboBox1.Location = new System.Drawing.Point(30, 80);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(530, 40);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.AppearanceKey = "acceptButton";
            this.button1.Location = new System.Drawing.Point(245, 162);
            this.button1.Name = "button1";
            this.button1.Shortcut = Wisej.Web.Shortcut.F12;
            this.button1.Size = new System.Drawing.Size(100, 48);
            this.button1.TabIndex = 2;
            this.button1.Text = "Print";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SelectPrintItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 230);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.fcHeader1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectPrintItem";
            this.Text = "Select Item to Print";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private fecherFoundation.FCHeader fcHeader1;
        private Wisej.Web.ComboBox comboBox1;
        private Wisej.Web.Button button1;
    }
}