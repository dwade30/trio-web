﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetBreakdown.
	/// </summary>
	public partial class frmBudgetBreakdown : BaseForm
	{
		public frmBudgetBreakdown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBudgetBreakdown InstancePtr
		{
			get
			{
				return (frmBudgetBreakdown)Sys.GetInstance(typeof(frmBudgetBreakdown));
			}
		}

		protected frmBudgetBreakdown _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/11/2002
		// This form will be used to give accounts monthly budgets
		// ********************************************************
		double curCurrentTotal;
		string[] strMonths = new string[11 + 1];
		int AmountRow;
		int PercentRow;
		int CurrentRow;
		bool blnUnload;
		bool blnDontRunRowColChange;
		string strShowType = "";

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			blnDontRunRowColChange = true;
			if (cmbExpense.SelectedIndex == 0)
			{
				if (cmbTown.SelectedIndex == 0)
				{
					rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'E' ORDER BY Account");
				}
				else
				{
					rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'P' ORDER BY Account");
				}
			}
			else
			{
				if (cmbTown.SelectedIndex == 0)
				{
					rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'R' ORDER BY Account");
				}
				else
				{
					rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(Account, 1) = 'V' ORDER BY Account");
				}
			}
			if (rsBudgetInfo.EndOfFile() != true && rsBudgetInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					vsAccounts.AddItem(rsBudgetInfo.Get_Fields_Int32("ID") + "\t" + rsBudgetInfo.Get_Fields("Account"));
					if (BudgetBreakdownExists(ref rsBudgetInfo))
					{
						vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Rows - 1, 1, vsAccounts.Rows - 1, 1, Color.Blue);
					}
					else
					{
						vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Rows - 1, 1, vsAccounts.Rows - 1, 1, Color.Red);
					}
					rsBudgetInfo.MoveNext();
				}
				while (rsBudgetInfo.EndOfFile() != true);
			}
			else
			{
				MessageBox.Show("No Accounts of this type were found", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			Label1.Visible = true;
			Label2.Visible = true;
			Label3.Visible = true;
			Label4.Visible = true;
			Label5.Visible = true;
			Label6.Visible = true;
			Label7.Visible = true;
			Label8.Visible = true;
			Label9.Visible = true;
			Label11.Visible = true;
			//Application.DoEvents();
			txtTotalBudget.Visible = true;
			lblAmount.Visible = true;
			vsBreakdown.Visible = true;
			vsBreakDown2.Visible = true;
			mnuProcessEven.Enabled = true;
			btnApplyEven.Visible = true;
			mnuProcessClear.Enabled = true;
			btnClearBreakdown.Visible = true;
			vsAccounts.Visible = true;
			lblTitle.Visible = true;
			//Application.DoEvents();
			mnuProcessSave.Enabled = true;
			mnuFileSave.Enabled = true;
			//Application.DoEvents();
			fraAccountType.Visible = false;
			cmbPercent.Visible = true;
			lblPercent.Visible = true;
			//Application.DoEvents();
			mnuProcessCalcPercent.Enabled = true;
			vsAccounts.Focus();
			vsAccounts.Select(1, 1);
			blnDontRunRowColChange = false;
			vsAccounts_RowColChange(vsAccounts, EventArgs.Empty);
		}

		private void frmBudgetBreakdown_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBudgetBreakdown_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmBudgetBreakdown_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBudgetBreakdown.FillStyle	= 0;
			//frmBudgetBreakdown.ScaleWidth	= 9045;
			//frmBudgetBreakdown.ScaleHeight	= 7470;
			//frmBudgetBreakdown.LinkTopic	= "Form2";
			//frmBudgetBreakdown.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			AmountRow = 1;
			PercentRow = 2;
			strMonths[0] = "January";
			strMonths[1] = "February";
			strMonths[2] = "March";
			strMonths[3] = "April";
			strMonths[4] = "May";
			strMonths[5] = "June";
			strMonths[6] = "July";
			strMonths[7] = "August";
			strMonths[8] = "September";
			strMonths[9] = "October";
			strMonths[10] = "November";
			strMonths[11] = "December";
			cmbTown.Enabled = true;
			cmbTown.SelectedIndex = 0;
			vsAccounts.ColWidth(0, 0);
            //FC:FINAL:DDU:#i549 fix column dimension
            vsAccounts.ExtendLastCol = true;
			vsAccounts.TextMatrix(0, 1, "Account");
			//vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			for (counter = 0; counter <= 5; counter++)
			{
				vsBreakdown.ColWidth(counter, 1000);
				vsBreakDown2.ColWidth(counter, 1000);
			}
			counter = modBudgetaryMaster.Statics.FirstMonth;
			vsBreakdown.TextMatrix(0, 0, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(0, 1, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(0, 2, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(0, 3, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(0, 4, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(0, 5, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 0, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 1, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 2, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 3, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 4, strMonths[counter - 1]);
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(0, 5, strMonths[counter - 1]);
			//vsBreakdown.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBreakdown.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsBreakdown.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//vsBreakDown2.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBreakdown.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsBreakDown2.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			txtTotalBudget.Text = "0.00";
			lblAmount.Text = "0.00";
			cmbExpense.SelectedIndex = 0;

			//FC:FINAL:DDU:#2986 - set columns alignment
			vsAccounts.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBreakdown.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakdown.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakdown.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakdown.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakdown.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakdown.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBreakDown2.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void frmBudgetBreakdown_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBudgetBreakdown_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void ResizeGrids()
		{
			int x;
			int lngGridWidth;
			lngGridWidth = vsBreakdown.WidthOriginal;
			for (x = 0; x <= 5; x++)
			{
				vsBreakdown.ColWidth(x, FCConvert.ToInt32(0.155 * lngGridWidth));
				vsBreakDown2.ColWidth(x, FCConvert.ToInt32(0.155 * lngGridWidth));
			}
			// x
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void mnuProcessCalcAmounts_Click(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: dblTotal As double	OnWrite(short, Decimal)
			double dblTotal;
			txtTotalBudget.Focus();
			dblTotal = 0;
			for (counter = 0; counter <= 5; counter++)
			{
				//vsBreakdown.TextMatrix(AmountRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(vsBreakdown.TextMatrix(PercentRow, counter)) / 100) * curCurrentTotal), 0), "#,##0"));
				//vsBreakDown2.TextMatrix(AmountRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(vsBreakDown2.TextMatrix(PercentRow, counter)) / 100) * curCurrentTotal), 0), "#,##0"));
				// FC:FINAL:VGE - #854 Treating empty strings as zeros
				vsBreakdown.TextMatrix(AmountRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(string.IsNullOrEmpty(vsBreakdown.TextMatrix(PercentRow, counter)) ? "0.0" : vsBreakdown.TextMatrix(PercentRow, counter)) / 100) * curCurrentTotal), 0), "#,##0"));
				vsBreakDown2.TextMatrix(AmountRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(string.IsNullOrEmpty(vsBreakDown2.TextMatrix(PercentRow, counter)) ? "0.0" : vsBreakDown2.TextMatrix(PercentRow, counter)) / 100) * curCurrentTotal), 0), "#,##0"));
				dblTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(AmountRow, counter)) + FCConvert.ToDouble(vsBreakDown2.TextMatrix(AmountRow, counter));
			}
			if (Conversion.Val(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)) == 0)
			{
				vsBreakdown.TextMatrix(AmountRow, 0, Strings.Format(FCConvert.ToDouble(vsBreakdown.TextMatrix(AmountRow, 0)) + (curCurrentTotal - dblTotal), "#,##0"));
			}
			if (strShowType != "B")
			{
				vsBreakdown.Focus();
			}
		}

		public void mnuProcessCalcAmounts_Click()
		{
			mnuProcessCalcAmounts_Click(mnuProcessCalcAmounts, new System.EventArgs());
		}

		private void mnuProcessCalcPercent_Click(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: dblTotal As double	OnWrite(short, Decimal)
			double dblTotal;
			txtTotalBudget.Focus();
			dblTotal = 0;
			for (counter = 0; counter <= 5; counter++)
			{
				if (curCurrentTotal == 0)
				{
					vsBreakdown.TextMatrix(PercentRow, counter, Strings.Format("0", "#.00"));
					vsBreakDown2.TextMatrix(PercentRow, counter, Strings.Format("0", "#.00"));
				}
				else
				{
					vsBreakdown.TextMatrix(PercentRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(vsBreakdown.TextMatrix(AmountRow, counter)) / FCConvert.ToDouble(curCurrentTotal)) * 100), 2), "#.00"));
					vsBreakDown2.TextMatrix(PercentRow, counter, Strings.Format(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(vsBreakDown2.TextMatrix(AmountRow, counter)) / FCConvert.ToDouble(curCurrentTotal)) * 100), 2), "#.00"));
				}
				dblTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(PercentRow, counter)) + FCConvert.ToDouble(vsBreakDown2.TextMatrix(PercentRow, counter));
			}
			if (Strings.Right(lblAmount.Text, 1) != "%")
			{
				if (FCConvert.ToDouble(lblAmount.Text) == 0)
				{
					vsBreakdown.TextMatrix(PercentRow, 0, Strings.Format(FCConvert.ToDouble(vsBreakdown.TextMatrix(PercentRow, 0)) + (100 - dblTotal), "#.00"));
				}
			}
			else
			{
				if (Conversion.Val(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)) == 0)
				{
					vsBreakdown.TextMatrix(AmountRow, 0, Strings.Format(vsBreakdown.TextMatrix(AmountRow, 0) + (FCConvert.ToDouble(curCurrentTotal) - dblTotal), "#,##0"));
				}
			}
			if (strShowType != "B")
			{
				vsBreakdown.Focus();
			}
		}

		public void mnuProcessCalcPercent_Click()
		{
			mnuProcessCalcPercent_Click(mnuProcessCalcPercent, new System.EventArgs());
		}

		private void mnuProcessClear_Click(object sender, System.EventArgs e)
		{
			int counter;
			txtTotalBudget.Focus();
			for (counter = 0; counter <= 5; counter++)
			{
				vsBreakdown.TextMatrix(1, counter, FCConvert.ToString(0));
				vsBreakdown.TextMatrix(2, counter, FCConvert.ToString(0));
			}
			for (counter = 0; counter <= 5; counter++)
			{
				vsBreakDown2.TextMatrix(1, counter, FCConvert.ToString(0));
				vsBreakDown2.TextMatrix(2, counter, FCConvert.ToString(0));
			}
			vsBreakdown.Focus();
			CalculateRemainingAmount();
		}

		private void mnuProcessEven_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(Decimal, short)
			double curTotal;
			// vbPorter upgrade warning: dblMonthTotal As double	OnWrite(Decimal, double)
			double dblMonthTotal;
			// vbPorter upgrade warning: curTemp As Decimal	OnWriteFCConvert.ToDouble(	OnReadFCConvert.ToDouble(
			double curTemp;
			int CurrentGrid = 0;
			int CurrentRow;
			int CurrentCol;
			int counter;
			if (frmBudgetBreakdown.InstancePtr.ActiveControl.GetName() == "vsBreakdown")
			{
				CurrentGrid = 1;
				CurrentRow = vsBreakdown.Row;
				CurrentCol = vsBreakdown.Col;
			}
			else
			{
				CurrentGrid = 2;
				CurrentRow = vsBreakDown2.Row;
				CurrentCol = vsBreakDown2.Col;
			}
			txtTotalBudget.Focus();
			if (cmbPercent.Text == "Amount")
			{
				curTotal = curCurrentTotal;
			}
			else
			{
				curTotal = 100;
			}
			dblMonthTotal = FCConvert.ToDouble(curTotal / 12);
			if (cmbPercent.Text == "Amount")
			{
				curTemp = modGlobal.Round_6(dblMonthTotal, 0);
				dblMonthTotal = FCConvert.ToDouble(curTemp);
				vsBreakdown.TextMatrix(1, 1, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakdown.TextMatrix(1, 2, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakdown.TextMatrix(1, 3, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakdown.TextMatrix(1, 4, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakdown.TextMatrix(1, 5, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 0, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 1, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 2, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 3, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 4, Strings.Format(dblMonthTotal, "#,##0"));
				vsBreakDown2.TextMatrix(1, 5, Strings.Format(dblMonthTotal, "#,##0"));
				dblMonthTotal += (FCConvert.ToDouble(curTotal) - (12 * dblMonthTotal));
				if (dblMonthTotal < 0)
				{
					vsBreakdown.TextMatrix(1, 0, Strings.Format(0, "#,##0"));
					dblMonthTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(1, 1));
					if (dblMonthTotal < 0)
					{
						vsBreakdown.TextMatrix(1, 1, Strings.Format(0, "#,##0"));
						dblMonthTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(1, 2));
						if (dblMonthTotal < 0)
						{
							vsBreakdown.TextMatrix(1, 2, Strings.Format(0, "#,##0"));
							dblMonthTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(1, 3));
							vsBreakdown.TextMatrix(1, 3, Strings.Format(dblMonthTotal, "#,##0"));
						}
						else
						{
							vsBreakdown.TextMatrix(1, 2, Strings.Format(dblMonthTotal, "#,##0"));
						}
					}
					else
					{
						vsBreakdown.TextMatrix(1, 1, Strings.Format(dblMonthTotal, "#,##0"));
					}
				}
				else
				{
					vsBreakdown.TextMatrix(1, 0, Strings.Format(dblMonthTotal, "#,##0"));
				}
				for (counter = 0; counter <= 5; counter++)
				{
					vsBreakdown.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
					vsBreakDown2.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
				}
			}
			else
			{
				curTemp = modGlobal.Round_6(dblMonthTotal, 2);
				dblMonthTotal = FCConvert.ToDouble(curTemp);
				vsBreakdown.TextMatrix(2, 1, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakdown.TextMatrix(2, 2, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakdown.TextMatrix(2, 3, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakdown.TextMatrix(2, 4, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakdown.TextMatrix(2, 5, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 0, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 1, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 2, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 3, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 4, Strings.Format(dblMonthTotal, "#.00"));
				vsBreakDown2.TextMatrix(2, 5, Strings.Format(dblMonthTotal, "#.00"));
				dblMonthTotal += (FCConvert.ToDouble(curTotal) - (12 * dblMonthTotal));
				vsBreakdown.TextMatrix(2, 0, Strings.Format(dblMonthTotal, "#.00"));
				for (counter = 0; counter <= 5; counter++)
				{
					vsBreakdown.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
					vsBreakDown2.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
				}
				// mnuProcessCalcAmounts_Click
			}
			if (CurrentGrid == 1)
			{
				vsBreakdown.Focus();
			}
			else
			{
				vsBreakDown2.Focus();
			}
			CalculateRemainingAmount();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private bool BudgetBreakdownExists(ref clsDRWrapper rsInfo)
		{
			bool BudgetBreakdownExists = false;
			// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
			//FC:FINAL:DSE:#866 In VB6, variable is of type Currency, that is with fixed decimals
			//double curTotal;
			Decimal curTotal;
			curTotal = 0;
			if (rsInfo.Get_Fields_Boolean("PercentFlag"))
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("JanPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("FebPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("MarPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("AprPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("MayPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("JunePercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("JulyPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("AugPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("SeptPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("OctPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("NovPercent"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Double("DecPercent"));
				if (curTotal == 100)
				{
					BudgetBreakdownExists = true;
				}
				else
				{
					BudgetBreakdownExists = false;
				}
			}
			else
			{
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("JanBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("FebBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("MarBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("AprBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("MayBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("JuneBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("JulyBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("AugBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("SeptBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("OctBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("NovBudget"));
				curTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("DecBudget"));
				if (curTotal > 0 && curTotal == FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("ApprovedAmount")))
				{
					BudgetBreakdownExists = true;
				}
				else
				{
					BudgetBreakdownExists = false;
				}
			}
			return BudgetBreakdownExists;
		}

		private void optAmount_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			vsBreakdown.Enabled = true;
			vsBreakDown2.Enabled = true;
			mnuProcessCalcPercent.Enabled = true;
			mnuProcessCalcAmounts.Enabled = false;
			mnuProcessClear.Enabled = true;
			mnuProcessEven.Enabled = true;
			btnApplyEven.Visible = true;
			btnClearBreakdown.Visible = true;
			mnuProcessCalcAmounts_Click();
			for (counter = 0; counter <= 5; counter++)
			{
				vsBreakdown.TextMatrix(PercentRow, counter, ".00");
				vsBreakDown2.TextMatrix(PercentRow, counter, ".00");
			}
			CalculateRemainingAmount();
			strShowType = "A";
		}

		public void optAmount_Click()
		{
			optAmount_CheckedChanged(cmbPercent, new System.EventArgs());
		}

		private void optBoth_CheckedChanged(object sender, System.EventArgs e)
		{
			if (strShowType == "P")
			{
				strShowType = "B";
				mnuProcessCalcAmounts_Click();
			}
			else
			{
				strShowType = "B";
				mnuProcessCalcPercent_Click();
				mnuProcessCalcAmounts_Click();
			}
			cmbPercent.Focus();
			mnuProcessCalcAmounts.Enabled = false;
			mnuProcessCalcPercent.Enabled = false;
			mnuProcessClear.Enabled = false;
			mnuProcessEven.Enabled = false;
			btnApplyEven.Visible = false;
			btnClearBreakdown.Visible = false;
			vsBreakdown.Enabled = false;
			vsBreakDown2.Enabled = false;
			CalculateRemainingAmount();
		}

		private void optPercent_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			vsBreakdown.Enabled = true;
			vsBreakDown2.Enabled = true;
			mnuProcessCalcPercent.Enabled = false;
			mnuProcessCalcAmounts.Enabled = true;
			mnuProcessClear.Enabled = true;
			mnuProcessEven.Enabled = true;
			btnApplyEven.Visible = true;
			btnClearBreakdown.Visible = true;
			mnuProcessCalcPercent_Click();
			for (counter = 0; counter <= 5; counter++)
			{
				vsBreakdown.TextMatrix(AmountRow, counter, "0");
				vsBreakDown2.TextMatrix(AmountRow, counter, "0");
			}
			CalculateRemainingAmount();
			strShowType = "P";
		}

		public void optPercent_Click()
		{
			optPercent_CheckedChanged(cmbPercent, new System.EventArgs());
		}

		private void txtTotalBudget_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//FC:FINAL:DSE #i569 Exception if text is empty
			if (!String.IsNullOrEmpty(txtTotalBudget.Text))
			{
				curCurrentTotal = FCConvert.ToDouble(txtTotalBudget.Text);
			}
			else
			{
				curCurrentTotal = 0;
			}
			CalculateRemainingAmount();
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			int counter;
			if (!blnDontRunRowColChange)
			{
				if (vsAccounts.TextMatrix(vsAccounts.Row, 0) != "")
				{
					rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = " + vsAccounts.TextMatrix(vsAccounts.Row, 0));
				}
				if (ColorTranslator.FromOle(vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1)) == Color.Blue)
				{
					if (vsAccounts.TextMatrix(vsAccounts.Row, 0) != "")
					{
						curCurrentTotal = FCConvert.ToDouble(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
						txtTotalBudget.Text = Strings.Format(curCurrentTotal, "#,##0.00");
					}
					FillBudgetBreakdown(ref rsBudgetInfo);
				}
				else
				{
					if (vsAccounts.TextMatrix(vsAccounts.Row, 0) != "")
					{
						curCurrentTotal = FCConvert.ToDouble(rsBudgetInfo.Get_Fields_Decimal("ApprovedAmount"));
						txtTotalBudget.Text = Strings.Format(curCurrentTotal, "#,##0.00");
					}
					for (counter = 0; counter <= 5; counter++)
					{
						vsBreakdown.TextMatrix(1, counter, Strings.Format("0", "#,##0"));
						vsBreakdown.TextMatrix(2, counter, Strings.Format("0", "#.00"));
					}
					for (counter = 0; counter <= 5; counter++)
					{
						vsBreakDown2.TextMatrix(1, counter, Strings.Format("0", "#,##0"));
						vsBreakDown2.TextMatrix(2, counter, Strings.Format("0", "#.00"));
					}
				}
				CalculateRemainingAmount();
				modAccountTitle.DetermineAccountTitle(vsAccounts.TextMatrix(vsAccounts.Row, 1), ref lblTitle);
			}
		}

		private void FillBudgetBreakdown(ref clsDRWrapper rsInfo)
		{
			double[] curMonthTotals = new double[11 + 1];
			int counter;
			string strFormat = "";
			if (rsInfo.Get_Fields_Boolean("PercentFlag"))
			{
				cmbPercent.Text = "Percent";
				CurrentRow = PercentRow;
				strFormat = "#.00";
				curMonthTotals[0] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("JanPercent"));
				curMonthTotals[1] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("FebPercent"));
				curMonthTotals[2] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("MarPercent"));
				curMonthTotals[3] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("AprPercent"));
				curMonthTotals[4] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("MayPercent"));
				curMonthTotals[5] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("JunePercent"));
				curMonthTotals[6] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("JulyPercent"));
				curMonthTotals[7] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("AugPercent"));
				curMonthTotals[8] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("SeptPercent"));
				curMonthTotals[9] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("OctPercent"));
				curMonthTotals[10] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("NovPercent"));
				curMonthTotals[11] = FCConvert.ToDouble(rsInfo.Get_Fields_Double("DecPercent"));
			}
			else
			{
				cmbPercent.Text = "Amount";
				CurrentRow = AmountRow;
				strFormat = "#,##0";
				curMonthTotals[0] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("JanBudget"));
				curMonthTotals[1] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("FebBudget"));
				curMonthTotals[2] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("MarBudget"));
				curMonthTotals[3] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("AprBudget"));
				curMonthTotals[4] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("MayBudget"));
				curMonthTotals[5] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("JuneBudget"));
				curMonthTotals[6] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("JulyBudget"));
				curMonthTotals[7] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("AugBudget"));
				curMonthTotals[8] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("SeptBudget"));
				curMonthTotals[9] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("OctBudget"));
				curMonthTotals[10] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("NovBudget"));
				curMonthTotals[11] = FCConvert.ToDouble(rsInfo.Get_Fields_Decimal("DecBudget"));
			}
			counter = modBudgetaryMaster.Statics.FirstMonth;
			vsBreakdown.TextMatrix(CurrentRow, 0, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(CurrentRow, 1, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(CurrentRow, 2, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(CurrentRow, 3, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(CurrentRow, 4, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakdown.TextMatrix(CurrentRow, 5, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 0, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 1, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 2, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 3, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 4, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (counter == 12)
			{
				counter = 1;
			}
			else
			{
				counter += 1;
			}
			vsBreakDown2.TextMatrix(CurrentRow, 5, Strings.Format(curMonthTotals[counter - 1], strFormat));
			if (CurrentRow == PercentRow)
			{
				for (counter = 0; counter <= 5; counter++)
				{
					vsBreakdown.TextMatrix(AmountRow, counter, Strings.Format("0", "#,##0"));
					vsBreakDown2.TextMatrix(AmountRow, counter, Strings.Format("0", "#,##0"));
				}
			}
			else
			{
				for (counter = 0; counter <= 5; counter++)
				{
					vsBreakdown.TextMatrix(PercentRow, counter, Strings.Format("0", "#.00"));
					vsBreakDown2.TextMatrix(PercentRow, counter, Strings.Format("0", "#.00"));
				}
			}
		}

		private void vsBreakdown_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsBreakdown.TextMatrix(vsBreakdown.Row, vsBreakdown.Col) == "")
			{
				vsBreakdown.TextMatrix(vsBreakdown.Row, vsBreakdown.Col, "0");
			}
			CalculateRemainingAmount();
		}

		private void vsBreakdown_Enter(object sender, System.EventArgs e)
		{
			vsBreakdown.Col = 0;
			if (cmbPercent.Text == "Amount")
			{
				vsBreakdown.Row = 1;
			}
			else
			{
				vsBreakdown.Row = 2;
			}
			vsBreakdown.EditCell();
			if (vsBreakdown.TextMatrix(vsBreakdown.Row, vsBreakdown.Col) == "0")
			{
				vsBreakdown.EditSelStart = 0;
				vsBreakdown.EditSelLength = 1;
			}
		}

		private void vsBreakdown_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
			{
				if (vsBreakdown.Row == PercentRow)
				{
					if (keyAscii == 46)
					{
						// do nothing
					}
					else
					{
						keyAscii = 0;
					}
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsBreakDown2_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
			{
				if (vsBreakDown2.Row == PercentRow)
				{
					if (keyAscii == 46)
					{
						// do nothing
					}
					else
					{
						keyAscii = 0;
					}
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsBreakdown_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int counter;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsBreakdown.GetFlexRowIndex(e.RowIndex);
			int col = vsBreakdown.GetFlexColIndex(e.ColumnIndex);
			if (vsBreakdown.TextMatrix(row, col) != vsBreakdown.EditText)
			{
				if (cmbPercent.Text == "Amount")
				{
					if (vsBreakdown.EditText != "")
					{
						// If CCur(vsBreakdown.EditText) > CCur(lblAmount.Caption) Then
						// MsgBox "The amount you entered was greater than the remaining amount", vbExclamation, "Invalid Amount"
						// Cancel = True
						// Exit Sub
						// Else
						if (FCConvert.ToDouble(vsBreakdown.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
					}
					else
					{
						// If Val(vsBreakdown.EditText) > CCur(lblAmount.Caption) Then
						// MsgBox "The amount you entered was greater than the remaining amount", vbExclamation, "Invalid Amount"
						// Cancel = True
						// Exit Sub
						// Else
						if (Conversion.Val(vsBreakdown.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
					}
					for (counter = 0; counter <= 5; counter++)
					{
						vsBreakdown.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
						vsBreakDown2.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
					}
				}
				else
				{
					if (Strings.Right(lblAmount.Text, 1) == "%")
					{
						// If Val(vsBreakdown.EditText) > Val(Left(lblAmount.Caption, Len(lblAmount.Caption) - 1)) Then
						// MsgBox "The amount you entered was greater than the remaining amount", vbExclamation, "Invalid Amount"
						// Cancel = True
						// Exit Sub
						// Else
						if (Conversion.Val(vsBreakdown.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
					}
					else
					{
						// If Val(vsBreakdown.EditText) > Val(lblAmount.Caption) Then
						// MsgBox "The amount you entered was greater than the remaining amount", vbExclamation, "Invalid Amount"
						// Cancel = True
						// Exit Sub
						// Else
						if (Conversion.Val(vsBreakdown.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
					}
					for (counter = 0; counter <= 5; counter++)
					{
						vsBreakdown.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
						vsBreakDown2.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
					}
				}
			}
		}

		private void vsBreakDown2_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsBreakDown2.TextMatrix(vsBreakDown2.Row, vsBreakDown2.Col) == "")
			{
				vsBreakDown2.TextMatrix(vsBreakDown2.Row, vsBreakDown2.Col, "0");
			}
			CalculateRemainingAmount();
		}

		private void CalculateRemainingAmount()
		{
			double dblTotal;
			int counter;
			dblTotal = 0;
			if (cmbPercent.Text == "Amount")
			{
				for (counter = 0; counter <= 5; counter++)
				{
					if (Strings.Trim(vsBreakdown.TextMatrix(1, counter)) == "")
					{
						vsBreakdown.TextMatrix(1, counter, FCConvert.ToString(0));
					}
					dblTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(1, counter));
				}
				for (counter = 0; counter <= 5; counter++)
				{
					if (Strings.Trim(vsBreakDown2.TextMatrix(1, counter)) == "")
					{
						vsBreakDown2.TextMatrix(1, counter, FCConvert.ToString(0));
					}
					dblTotal += FCConvert.ToDouble(vsBreakDown2.TextMatrix(1, counter));
				}
				lblAmount.Text = Strings.Format(FCConvert.ToDouble(curCurrentTotal) - dblTotal, "#,##0.00");
			}
			else
			{
				for (counter = 0; counter <= 5; counter++)
				{
					if (Strings.Trim(vsBreakdown.TextMatrix(2, counter)) == "")
					{
						vsBreakdown.TextMatrix(2, counter, FCConvert.ToString(0));
					}
					dblTotal += FCConvert.ToDouble(vsBreakdown.TextMatrix(2, counter));
				}
				for (counter = 0; counter <= 5; counter++)
				{
					if (Strings.Trim(vsBreakDown2.TextMatrix(2, counter)) == "")
					{
						vsBreakDown2.TextMatrix(2, counter, FCConvert.ToString(0));
					}
					dblTotal += FCConvert.ToDouble(vsBreakDown2.TextMatrix(2, counter));
				}
				if (curCurrentTotal != 0)
				{
					lblAmount.Text = Strings.Format((100 - dblTotal) / 100, "#.00%");
				}
				else
				{
					lblAmount.Text = Strings.Format(0, "#.00%");
				}
			}
		}

		private void vsBreakdown_ClickEvent(object sender, System.EventArgs e)
		{
			if (cmbPercent.Text == "Amount")
			{
				vsBreakdown.Row = AmountRow;
			}
			else
			{
				vsBreakdown.Row = PercentRow;
			}
			vsBreakdown.EditCell();
			if (vsBreakdown.TextMatrix(vsBreakdown.Row, vsBreakdown.Col) == "0")
			{
				vsBreakdown.EditSelStart = 0;
				vsBreakdown.EditSelLength = 1;
			}
		}

		private void vsBreakdown_RowColChange(object sender, System.EventArgs e)
		{
			if (cmbPercent.Text == "Amount")
			{
				vsBreakdown.Row = AmountRow;
			}
			else
			{
				vsBreakdown.Row = PercentRow;
			}
			vsBreakdown.EditCell();
			if (vsBreakdown.TextMatrix(vsBreakdown.Row, vsBreakdown.Col) == "0")
			{
				vsBreakdown.EditSelStart = 0;
				vsBreakdown.EditSelLength = 1;
			}
			if (vsBreakdown.Col == vsBreakdown.Cols - 1)
			{
				vsBreakdown.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsBreakdown.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsBreakDown2_ClickEvent(object sender, System.EventArgs e)
		{
			if (cmbPercent.Text == "Amount")
			{
				vsBreakdown.Row = AmountRow;
			}
			else
			{
				vsBreakdown.Row = PercentRow;
			}
			vsBreakDown2.EditCell();
			if (vsBreakDown2.TextMatrix(vsBreakDown2.Row, vsBreakDown2.Col) == "0")
			{
				vsBreakDown2.EditSelStart = 0;
				vsBreakDown2.EditSelLength = 1;
			}
		}

		private void vsBreakDown2_Enter(object sender, System.EventArgs e)
		{
			vsBreakDown2.Col = 0;
			if (cmbPercent.Text == "Amount")
			{
				vsBreakDown2.Row = 1;
			}
			else
			{
				vsBreakDown2.Row = 2;
			}
			vsBreakDown2.EditCell();
			if (vsBreakDown2.TextMatrix(vsBreakDown2.Row, vsBreakDown2.Col) == "0")
			{
				vsBreakDown2.EditSelStart = 0;
				vsBreakDown2.EditSelLength = 1;
			}
		}

		private void vsBreakDown2_RowColChange(object sender, System.EventArgs e)
		{
			if (cmbPercent.Text == "Amount")
			{
				vsBreakDown2.Row = AmountRow;
			}
			else
			{
				vsBreakDown2.Row = PercentRow;
			}
			vsBreakDown2.EditCell();
			if (vsBreakDown2.TextMatrix(vsBreakDown2.Row, vsBreakDown2.Col) == "0")
			{
				vsBreakDown2.EditSelStart = 0;
				vsBreakDown2.EditSelLength = 1;
			}
			if (vsBreakDown2.Col == vsBreakDown2.Cols - 1)
			{
				vsBreakDown2.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsBreakDown2.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsBreakDown2_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int counter;
			//FC:FINAL:MSH - save and use correct indexes of the cell
			//int row = vsBreakDown2.Row;
			//int col = vsBreakDown2.Col;
			int row = vsBreakDown2.GetFlexRowIndex(e.RowIndex);
			int col = vsBreakDown2.GetFlexColIndex(e.ColumnIndex);
			if (!blnDontRunRowColChange)
			{
				if (vsBreakDown2.TextMatrix(row, col) != vsBreakDown2.EditText)
				{
					if (cmbPercent.Text == "Amount")
					{
						if (FCConvert.ToDouble(vsBreakDown2.EditText) > FCConvert.ToDouble(lblAmount.Text))
						{
							MessageBox.Show("The amount you entered was greater than the remaining amount", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						else if (FCConvert.ToDouble(vsBreakDown2.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						for (counter = 0; counter <= 5; counter++)
						{
							vsBreakdown.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
							vsBreakDown2.TextMatrix(PercentRow, counter, FCConvert.ToString(0));
						}
					}
					else
					{
						if (Conversion.Val(vsBreakDown2.EditText) > Conversion.Val(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)))
						{
							MessageBox.Show("The amount you entered was greater than the remaining amount", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						else if (Conversion.Val(vsBreakDown2.EditText) < 0)
						{
							MessageBox.Show("You may only enter amounts greater than 0.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						for (counter = 0; counter <= 5; counter++)
						{
							vsBreakdown.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
							vsBreakDown2.TextMatrix(AmountRow, counter, FCConvert.ToString(0));
						}
					}
				}
			}
		}

		private void SetCustomFormColors()
		{
			lblTitle.ForeColor = Color.Blue;
			Label2.BackColor = Color.Blue;
			Label1.BackColor = Color.Red;
			Label10.ForeColor = Color.Red;
		}

		private void SaveInfo()
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curMonthTotals As Variant --> As string
			string[,] curMonthTotals = new string[2 + 1, 11 + 1];
			int counter = 0;
			bool blnAmountShown = false;
			if (FCConvert.ToDouble(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)) == 0 || FCConvert.ToDouble(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)) == curCurrentTotal || lblAmount.Text == "100.00%")
			{
				if (cmbPercent.Text == "Amount")
				{
					blnAmountShown = true;
					optPercent_Click();
				}
				else
				{
					blnAmountShown = false;
				}
				counter = modBudgetaryMaster.Statics.FirstMonth;
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 0);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 0);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 1);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 1);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 2);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 2);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 3);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 3);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 4);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 4);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakdown.TextMatrix(AmountRow, 5);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakdown.TextMatrix(PercentRow, 5);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 0);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 0);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 1);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 1);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 2);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 2);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 3);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 3);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 4);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 4);
				if (counter == 12)
				{
					counter = 1;
				}
				else
				{
					counter += 1;
				}
				curMonthTotals[AmountRow - 1, counter - 1] = vsBreakDown2.TextMatrix(AmountRow, 5);
				curMonthTotals[PercentRow - 1, counter - 1] = vsBreakDown2.TextMatrix(PercentRow, 5);
				rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = " + vsAccounts.TextMatrix(vsAccounts.Row, 0));
				rsBudgetInfo.Edit();
				// If optAmount.Value = True Then
				// rsBudgetInfo.Fields["JanBudget"] = curMonthTotals(0, 0)
				// rsBudgetInfo.Fields["FebBudget"] = curMonthTotals(0, 1)
				// rsBudgetInfo.Fields["MarBudget"] = curMonthTotals(0, 2)
				// rsBudgetInfo.Fields["AprBudget"] = curMonthTotals(0, 3)
				// rsBudgetInfo.Fields["MayBudget"] = curMonthTotals(0, 4)
				// rsBudgetInfo.Fields["JuneBudget"] = curMonthTotals(0, 5)
				// rsBudgetInfo.Fields["JulyBudget"] = curMonthTotals(0, 6)
				// rsBudgetInfo.Fields["AugBudget"] = curMonthTotals(0, 7)
				// rsBudgetInfo.Fields["SeptBudget"] = curMonthTotals(0, 8)
				// rsBudgetInfo.Fields["OctBudget"] = curMonthTotals(0, 9)
				// rsBudgetInfo.Fields["NovBudget"] = curMonthTotals(0, 10)
				// rsBudgetInfo.Fields["DecBudget"] = curMonthTotals(0, 11)
				// 
				// rsBudgetInfo.Fields["JanPercent"] = Round(curMonthTotals(0, 0) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["FebPercent"] = Round(curMonthTotals(0, 1) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["MarPercent"] = Round(curMonthTotals(0, 2) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["AprPercent"] = Round(curMonthTotals(0, 3) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["MayPercent"] = Round(curMonthTotals(0, 4) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["JunePercent"] = Round(curMonthTotals(0, 5) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["JulyPercent"] = Round(curMonthTotals(0, 6) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["AugPercent"] = Round(curMonthTotals(0, 7) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["SeptPercent"] = Round(curMonthTotals(0, 8) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["OctPercent"] = Round(curMonthTotals(0, 9) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["NovPercent"] = Round(curMonthTotals(0, 10) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["DecPercent"] = Round(curMonthTotals(0, 11) / curCurrentTotal, 2)
				// rsBudgetInfo.Fields["PercentFlag"] = False
				// rsBudgetInfo.Fields["ApprovedAmount"] = curCurrentTotal
				// Else
				rsBudgetInfo.Set_Fields("JanBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 0]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("FebBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 1]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("MarBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 2]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("AprBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 3]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("MayBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 4]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("JuneBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 5]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("JulyBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 6]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("AugBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 7]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("SeptBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 8]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("OctBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 9]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("NovBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 10]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("DecBudget", FCConvert.ToDouble(modGlobal.Round_8(FCConvert.ToDouble((FCConvert.ToDouble(curMonthTotals[1, 11]) / 100) * curCurrentTotal), 0)));
				rsBudgetInfo.Set_Fields("JanPercent", FCConvert.ToDouble(curMonthTotals[1, 0]));
				rsBudgetInfo.Set_Fields("FebPercent", FCConvert.ToDouble(curMonthTotals[1, 1]));
				rsBudgetInfo.Set_Fields("MarPercent", FCConvert.ToDouble(curMonthTotals[1, 2]));
				rsBudgetInfo.Set_Fields("AprPercent", FCConvert.ToDouble(curMonthTotals[1, 3]));
				rsBudgetInfo.Set_Fields("MayPercent", FCConvert.ToDouble(curMonthTotals[1, 4]));
				rsBudgetInfo.Set_Fields("JunePercent", FCConvert.ToDouble(curMonthTotals[1, 5]));
				rsBudgetInfo.Set_Fields("JulyPercent", FCConvert.ToDouble(curMonthTotals[1, 6]));
				rsBudgetInfo.Set_Fields("AugPercent", FCConvert.ToDouble(curMonthTotals[1, 7]));
				rsBudgetInfo.Set_Fields("SeptPercent", FCConvert.ToDouble(curMonthTotals[1, 8]));
				rsBudgetInfo.Set_Fields("OctPercent", FCConvert.ToDouble(curMonthTotals[1, 9]));
				rsBudgetInfo.Set_Fields("NovPercent", FCConvert.ToDouble(curMonthTotals[1, 10]));
				rsBudgetInfo.Set_Fields("DecPercent", FCConvert.ToDouble(curMonthTotals[1, 11]));
				rsBudgetInfo.Set_Fields("PercentFlag", true);
				rsBudgetInfo.Set_Fields("ApprovedAmount", curCurrentTotal);
				// End If
				rsBudgetInfo.Update();
				if (blnAmountShown)
				{
					optAmount_Click();
					vsBreakdown.TextMatrix(AmountRow, 0, FCConvert.ToDouble(vsBreakdown.TextMatrix(AmountRow, 0)) + FCConvert.ToDouble(lblAmount.Text));
					lblAmount.Text = "0.00";
				}
				if (FCConvert.ToDouble(Strings.Left(lblAmount.Text, lblAmount.Text.Length - 1)) == 0 && curCurrentTotal != 0)
				{
					vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1, vsAccounts.Row, 1, Color.Blue);
				}
				else
				{
					vsAccounts.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsAccounts.Row, 1, vsAccounts.Row, 1, Color.Red);
				}
				vsAccounts.Focus();
			}
			else
			{
				if (Strings.Left(lblAmount.Text, 1) == "-")
				{
					MessageBox.Show("You have used more then you are allowed to for this budget.  You must correct the problem before you may save the breakdown", "Money Remaining", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show("You must use the entire budget before you may save the breakdown", "Money Remaining", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			if (blnUnload)
			{
				mnuProcessQuit_Click();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (fraAccountType.Visible)
			{
				btnProcess.Text = "Save";
				cmdProcess_Click(sender, e);
			}
			else
			{
				mnuProcessSave_Click(sender, e);
			}
		}

		private void btnApplyEven_Click(object sender, EventArgs e)
		{
			mnuProcessEven_Click(sender, e);
		}

		private void btnClearBreakdown_Click(object sender, EventArgs e)
		{
			mnuProcessClear_Click(sender, e);
		}

		private void cmbPercent_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbPercent.Text == "Percent")
			{
				optPercent_CheckedChanged(sender, e);
			}
			else if (cmbPercent.Text == "Amount")
			{
				optAmount_CheckedChanged(sender, e);
			}
			else if (cmbPercent.Text == "Show Both")
			{
				optBoth_CheckedChanged(sender, e);
			}
		}
	}
}
