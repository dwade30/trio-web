﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseSummarySetup.
	/// </summary>
	partial class frmExpenseSummarySetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public fecherFoundation.FCComboBox cmbDeptDetail;
		public fecherFoundation.FCLabel lblDeptDetail;
		public fecherFoundation.FCComboBox cmbDepartment;
		public fecherFoundation.FCLabel lblDepartment;
		public fecherFoundation.FCComboBox cmbRegular;
		public fecherFoundation.FCLabel lblRegular;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCCheckBox chkShowZeroBalanceAccounts;
		public fecherFoundation.FCFrame fraPageBreaks;
		public fecherFoundation.FCCheckBox chkDivision;
		public fecherFoundation.FCCheckBox chkDepartment;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCButton cmdCancelPrint;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboSingleFund;
		public fecherFoundation.FCComboBox cboBeginningExpense;
		public fecherFoundation.FCComboBox cboEndingExpense;
		public fecherFoundation.FCComboBox cboSingleExpense;
		public fecherFoundation.FCCheckBox chkCheckAccountRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCFrame fraTotals;
		public fecherFoundation.FCCheckBox chkHideExpense;
		public fecherFoundation.FCLabel lblDescription;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpenseSummarySetup));
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbAllAccounts = new fecherFoundation.FCComboBox();
			this.lblAllAccounts = new fecherFoundation.FCLabel();
			this.cmbDeptDetail = new fecherFoundation.FCComboBox();
			this.lblDeptDetail = new fecherFoundation.FCLabel();
			this.cmbDepartment = new fecherFoundation.FCComboBox();
			this.lblDepartment = new fecherFoundation.FCLabel();
			this.cmbRegular = new fecherFoundation.FCComboBox();
			this.lblRegular = new fecherFoundation.FCLabel();
			this.chkShowZeroBalanceAccounts = new fecherFoundation.FCCheckBox();
			this.fraPageBreaks = new fecherFoundation.FCFrame();
			this.chkDivision = new fecherFoundation.FCCheckBox();
			this.chkDepartment = new fecherFoundation.FCCheckBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.cmdCancelPrint = new fecherFoundation.FCButton();
			this.fraMonths = new fecherFoundation.FCFrame();
			this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
			this.cboEndingMonth = new fecherFoundation.FCComboBox();
			this.cboBeginningMonth = new fecherFoundation.FCComboBox();
			this.cboSingleMonth = new fecherFoundation.FCComboBox();
			this.lblTo_0 = new fecherFoundation.FCLabel();
			this.fraDeptRange = new fecherFoundation.FCFrame();
			this.cboSingleFund = new fecherFoundation.FCComboBox();
			this.cboBeginningExpense = new fecherFoundation.FCComboBox();
			this.cboEndingExpense = new fecherFoundation.FCComboBox();
			this.cboSingleExpense = new fecherFoundation.FCComboBox();
			this.chkCheckAccountRange = new fecherFoundation.FCCheckBox();
			this.cboBeginningDept = new fecherFoundation.FCComboBox();
			this.cboEndingDept = new fecherFoundation.FCComboBox();
			this.cboSingleDept = new fecherFoundation.FCComboBox();
			this.vsLowAccount = new fecherFoundation.FCGrid();
			this.vsHighAccount = new fecherFoundation.FCGrid();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.lblTo_3 = new fecherFoundation.FCLabel();
			this.lblTo_1 = new fecherFoundation.FCLabel();
			this.fraTotals = new fecherFoundation.FCFrame();
			this.chkHideExpense = new fecherFoundation.FCCheckBox();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).BeginInit();
			this.fraPageBreaks.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDivision)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
			this.fraMonths.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
			this.fraDeptRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTotals)).BeginInit();
			this.fraTotals.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHideExpense)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkShowZeroBalanceAccounts);
			this.ClientArea.Controls.Add(this.fraPageBreaks);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.fraMonths);
			this.ClientArea.Controls.Add(this.fraDeptRange);
			this.ClientArea.Controls.Add(this.cmbDeptDetail);
			this.ClientArea.Controls.Add(this.lblDeptDetail);
			this.ClientArea.Controls.Add(this.cmbDepartment);
			this.ClientArea.Controls.Add(this.lblDepartment);
			this.ClientArea.Controls.Add(this.fraTotals);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdCancelPrint);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.cmdCancelPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(415, 30);
			this.HeaderText.Text = "Expense Summary Selection Criteria";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbRange
			// 
			this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Single Month",
            "Range of Months"});
			this.cmbRange.Location = new System.Drawing.Point(210, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(181, 40);
			this.cmbRange.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbRange, null);
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(20, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(157, 16);
			this.lblRange.TabIndex = 3;
			this.lblRange.Text = "MONTH(S) TO REPORT";
			this.ToolTip1.SetToolTip(this.lblRange, null);
			// 
			// cmbAllAccounts
			// 
			this.cmbAllAccounts.Items.AddRange(new object[] {
            "All",
            "Account Range",
            "Single Department",
            "Department Range",
            "Single Fund"});
			this.cmbAllAccounts.Location = new System.Drawing.Point(250, 30);
			this.cmbAllAccounts.Name = "cmbAllAccounts";
			this.cmbAllAccounts.Size = new System.Drawing.Size(181, 40);
			this.cmbAllAccounts.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbAllAccounts, null);
			this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.optAllAccounts_CheckedChanged);
			// 
			// lblAllAccounts
			// 
			this.lblAllAccounts.AutoSize = true;
			this.lblAllAccounts.Location = new System.Drawing.Point(20, 44);
			this.lblAllAccounts.Name = "lblAllAccounts";
            this.lblAllAccounts.Size = new System.Drawing.Size(205, 16);
			this.lblAllAccounts.TabIndex = 4;
			this.lblAllAccounts.Text = "ACCOUNTS TO BE REPORTED";
			this.ToolTip1.SetToolTip(this.lblAllAccounts, null);
			// 
			// cmbDeptDetail
			// 
			this.cmbDeptDetail.Items.AddRange(new object[] {
            "Department",
            "Division",
            "Expense",
            "Object"});
			this.cmbDeptDetail.Location = new System.Drawing.Point(245, 440);
			this.cmbDeptDetail.Name = "cmbDeptDetail";
			this.cmbDeptDetail.Size = new System.Drawing.Size(176, 40);
			this.cmbDeptDetail.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.cmbDeptDetail, null);
			this.cmbDeptDetail.SelectedIndexChanged += new System.EventHandler(this.optDeptDetail_CheckedChanged);
			// 
			// lblDeptDetail
			// 
			this.lblDeptDetail.AutoSize = true;
			this.lblDeptDetail.Location = new System.Drawing.Point(50, 454);
			this.lblDeptDetail.Name = "lblDeptDetail";
            this.lblDeptDetail.Size = new System.Drawing.Size(160, 16);
            this.lblDeptDetail.TabIndex = 49;
			this.lblDeptDetail.Text = "LOWEST DETAIL LEVEL";
			this.ToolTip1.SetToolTip(this.lblDeptDetail, null);
			// 
			// cmbDepartment
			// 
			this.cmbDepartment.Items.AddRange(new object[] {
            "Department",
            "Expense"});
			this.cmbDepartment.Location = new System.Drawing.Point(644, 440);
			this.cmbDepartment.Name = "cmbDepartment";
			this.cmbDepartment.Size = new System.Drawing.Size(176, 40);
			this.cmbDepartment.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.cmbDepartment, null);
			this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.optDepartment_CheckedChanged);
			// 
			// lblDepartment
			// 
			this.lblDepartment.AutoSize = true;
			this.lblDepartment.Location = new System.Drawing.Point(471, 454);
			this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(134, 16);
			this.lblDepartment.TabIndex = 8;
			this.lblDepartment.Text = "MAJOR SEQUENCE";
			this.ToolTip1.SetToolTip(this.lblDepartment, null);
			// 
			// cmbRegular
			// 
			this.cmbRegular.Items.AddRange(new object[] {
            "Regular Totals",
            "Monthly Totals"});
			this.cmbRegular.Location = new System.Drawing.Point(188, 30);
			this.cmbRegular.Name = "cmbRegular";
			this.cmbRegular.Size = new System.Drawing.Size(160, 40);
			this.cmbRegular.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.cmbRegular, null);
			// 
			// lblRegular
			// 
			this.lblRegular.AutoSize = true;
			this.lblRegular.Location = new System.Drawing.Point(24, 44);
			this.lblRegular.Name = "lblRegular";
            this.lblRegular.Size = new System.Drawing.Size(120, 16);
			this.lblRegular.Text = "TOTALS FORMAT";
			this.ToolTip1.SetToolTip(this.lblRegular, null);
			// 
			// chkShowZeroBalanceAccounts
			// 
			this.chkShowZeroBalanceAccounts.Location = new System.Drawing.Point(30, 635);
			this.chkShowZeroBalanceAccounts.Name = "chkShowZeroBalanceAccounts";
            this.chkShowZeroBalanceAccounts.Size = new System.Drawing.Size(201, 23);
			this.chkShowZeroBalanceAccounts.TabIndex = 10;
			this.chkShowZeroBalanceAccounts.Text = "Show Zero Balance Accounts";
			this.ToolTip1.SetToolTip(this.chkShowZeroBalanceAccounts, "This setting shows specific fields - not all formatting options will be available" +
        ".");
			// 
			// fraPageBreaks
			// 
			this.fraPageBreaks.Controls.Add(this.chkDivision);
			this.fraPageBreaks.Controls.Add(this.chkDepartment);
			this.fraPageBreaks.Location = new System.Drawing.Point(30, 510);
			this.fraPageBreaks.Name = "fraPageBreaks";
			this.fraPageBreaks.Size = new System.Drawing.Size(147, 102);
			this.fraPageBreaks.TabIndex = 7;
			this.fraPageBreaks.Text = "Page Breaks";
			this.ToolTip1.SetToolTip(this.fraPageBreaks, null);
			// 
			// chkDivision
			// 
			this.chkDivision.Location = new System.Drawing.Point(20, 61);
			this.chkDivision.Name = "chkDivision";
            this.chkDivision.Size = new System.Drawing.Size(73, 23);
			this.chkDivision.TabIndex = 1;
			this.chkDivision.Text = "Division";
			this.ToolTip1.SetToolTip(this.chkDivision, null);
			this.chkDivision.CheckedChanged += new System.EventHandler(this.chkDivision_CheckedChanged);
			// 
			// chkDepartment
			// 
			this.chkDepartment.Location = new System.Drawing.Point(20, 30);
			this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Size = new System.Drawing.Size(96, 23);
			this.chkDepartment.TabIndex = 2;
			this.chkDepartment.Text = "Department";
			this.ToolTip1.SetToolTip(this.chkDepartment, null);
			this.chkDepartment.CheckedChanged += new System.EventHandler(this.chkDepartment_CheckedChanged);
			// 
			// txtDescription
			// 
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(217, 30);
			this.txtDescription.MaxLength = 25;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(201, 40);
			this.txtDescription.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			// 
			// cmdCancelPrint
			// 
			this.cmdCancelPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdCancelPrint.Location = new System.Drawing.Point(1050, 28);
			this.cmdCancelPrint.Name = "cmdCancelPrint";
			this.cmdCancelPrint.Size = new System.Drawing.Size(59, 24);
			this.cmdCancelPrint.TabIndex = 30;
			this.cmdCancelPrint.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancelPrint, null);
			this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
			// 
			// fraMonths
			// 
			this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
			this.fraMonths.Controls.Add(this.chkCheckDateRange);
			this.fraMonths.Controls.Add(this.cmbRange);
			this.fraMonths.Controls.Add(this.lblRange);
			this.fraMonths.Controls.Add(this.cboEndingMonth);
			this.fraMonths.Controls.Add(this.cboBeginningMonth);
			this.fraMonths.Controls.Add(this.cboSingleMonth);
			this.fraMonths.Controls.Add(this.lblTo_0);
			this.fraMonths.Location = new System.Drawing.Point(30, 232);
			this.fraMonths.Name = "fraMonths";
			this.fraMonths.Size = new System.Drawing.Size(411, 178);
			this.fraMonths.TabIndex = 3;
            this.fraMonths.FormatCaption = false;
			this.fraMonths.Text = "Month(s) To Report";
			this.ToolTip1.SetToolTip(this.fraMonths, null);
			// 
			// chkCheckDateRange
			// 
			this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
			this.chkCheckDateRange.Location = new System.Drawing.Point(20, 80);
			this.chkCheckDateRange.Name = "chkCheckDateRange";
            this.chkCheckDateRange.Size = new System.Drawing.Size(146, 23);
			this.chkCheckDateRange.TabIndex = 2;
			this.chkCheckDateRange.Text = "Select at report time";
			this.ToolTip1.SetToolTip(this.chkCheckDateRange, null);
			this.chkCheckDateRange.Visible = false;
			this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
			// 
			// cboEndingMonth
			// 
			this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboEndingMonth.Location = new System.Drawing.Point(231, 118);
			this.cboEndingMonth.Name = "cboEndingMonth";
			this.cboEndingMonth.Size = new System.Drawing.Size(160, 40);
			this.cboEndingMonth.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.cboEndingMonth, null);
			this.cboEndingMonth.Visible = false;
			// 
			// cboBeginningMonth
			// 
			this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboBeginningMonth.Location = new System.Drawing.Point(20, 118);
			this.cboBeginningMonth.Name = "cboBeginningMonth";
			this.cboBeginningMonth.Size = new System.Drawing.Size(160, 40);
			this.cboBeginningMonth.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cboBeginningMonth, null);
			this.cboBeginningMonth.Visible = false;
			// 
			// cboSingleMonth
			// 
			this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
			this.cboSingleMonth.Location = new System.Drawing.Point(20, 118);
			this.cboSingleMonth.Name = "cboSingleMonth";
			this.cboSingleMonth.Size = new System.Drawing.Size(160, 40);
			this.cboSingleMonth.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.cboSingleMonth, null);
			this.cboSingleMonth.Visible = false;
			// 
			// lblTo_0
			// 
			this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_0.Location = new System.Drawing.Point(198, 132);
			this.lblTo_0.Name = "lblTo_0";
			this.lblTo_0.Size = new System.Drawing.Size(21, 19);
			this.lblTo_0.TabIndex = 4;
			this.lblTo_0.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_0, null);
			this.lblTo_0.Visible = false;
			// 
			// fraDeptRange
			// 
			this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
			this.fraDeptRange.Controls.Add(this.cboSingleFund);
			this.fraDeptRange.Controls.Add(this.cmbAllAccounts);
			this.fraDeptRange.Controls.Add(this.lblAllAccounts);
			this.fraDeptRange.Controls.Add(this.cboBeginningExpense);
			this.fraDeptRange.Controls.Add(this.cboEndingExpense);
			this.fraDeptRange.Controls.Add(this.cboSingleExpense);
			this.fraDeptRange.Controls.Add(this.chkCheckAccountRange);
			this.fraDeptRange.Controls.Add(this.cboBeginningDept);
			this.fraDeptRange.Controls.Add(this.cboEndingDept);
			this.fraDeptRange.Controls.Add(this.cboSingleDept);
			this.fraDeptRange.Controls.Add(this.vsLowAccount);
			this.fraDeptRange.Controls.Add(this.vsHighAccount);
			this.fraDeptRange.Controls.Add(this.lblTo_2);
			this.fraDeptRange.Controls.Add(this.lblTo_3);
			this.fraDeptRange.Controls.Add(this.lblTo_1);
			this.fraDeptRange.Location = new System.Drawing.Point(471, 232);
			this.fraDeptRange.Name = "fraDeptRange";
			this.fraDeptRange.Size = new System.Drawing.Size(451, 178);
			this.fraDeptRange.TabIndex = 4;
			this.fraDeptRange.Text = "Accounts To Be Reported";
			this.ToolTip1.SetToolTip(this.fraDeptRange, null);
			// 
			// cboSingleFund
			// 
			this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleFund.Location = new System.Drawing.Point(20, 118);
			this.cboSingleFund.Name = "cboSingleFund";
			this.cboSingleFund.Size = new System.Drawing.Size(160, 40);
			this.cboSingleFund.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cboSingleFund, null);
			this.cboSingleFund.Visible = false;
			this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
			// 
			// cboBeginningExpense
			// 
			this.cboBeginningExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningExpense.Location = new System.Drawing.Point(20, 118);
			this.cboBeginningExpense.Name = "cboBeginningExpense";
			this.cboBeginningExpense.Size = new System.Drawing.Size(160, 40);
			this.cboBeginningExpense.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.cboBeginningExpense, null);
			this.cboBeginningExpense.Visible = false;
			this.cboBeginningExpense.DropDown += new System.EventHandler(this.cboBeginningExpense_DropDown);
			// 
			// cboEndingExpense
			// 
			this.cboEndingExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingExpense.Location = new System.Drawing.Point(231, 118);
			this.cboEndingExpense.Name = "cboEndingExpense";
			this.cboEndingExpense.Size = new System.Drawing.Size(160, 40);
			this.cboEndingExpense.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.cboEndingExpense, null);
			this.cboEndingExpense.Visible = false;
			this.cboEndingExpense.DropDown += new System.EventHandler(this.cboEndingExpense_DropDown);
			// 
			// cboSingleExpense
			// 
			this.cboSingleExpense.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleExpense.Location = new System.Drawing.Point(20, 118);
			this.cboSingleExpense.Name = "cboSingleExpense";
			this.cboSingleExpense.Size = new System.Drawing.Size(160, 40);
			this.cboSingleExpense.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.cboSingleExpense, null);
			this.cboSingleExpense.Visible = false;
			this.cboSingleExpense.DropDown += new System.EventHandler(this.cboSingleExpense_DropDown);
			// 
			// chkCheckAccountRange
			// 
			this.chkCheckAccountRange.BackColor = System.Drawing.SystemColors.Menu;
			this.chkCheckAccountRange.Location = new System.Drawing.Point(20, 80);
			this.chkCheckAccountRange.Name = "chkCheckAccountRange";
            this.chkCheckAccountRange.Size = new System.Drawing.Size(146, 23);
			this.chkCheckAccountRange.TabIndex = 2;
			this.chkCheckAccountRange.Text = "Select at report time";
			this.ToolTip1.SetToolTip(this.chkCheckAccountRange, null);
			this.chkCheckAccountRange.Visible = false;
			this.chkCheckAccountRange.CheckedChanged += new System.EventHandler(this.chkCheckAccountRange_CheckedChanged);
			// 
			// cboBeginningDept
			// 
			this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningDept.Location = new System.Drawing.Point(20, 118);
			this.cboBeginningDept.Name = "cboBeginningDept";
			this.cboBeginningDept.Size = new System.Drawing.Size(160, 40);
			this.cboBeginningDept.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.cboBeginningDept, null);
			this.cboBeginningDept.Visible = false;
			this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
			// 
			// cboEndingDept
			// 
			this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingDept.Location = new System.Drawing.Point(231, 118);
			this.cboEndingDept.Name = "cboEndingDept";
			this.cboEndingDept.Size = new System.Drawing.Size(160, 40);
			this.cboEndingDept.TabIndex = 19;
			this.ToolTip1.SetToolTip(this.cboEndingDept, null);
			this.cboEndingDept.Visible = false;
			this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
			// 
			// cboSingleDept
			// 
			this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDept.Location = new System.Drawing.Point(20, 118);
			this.cboSingleDept.Name = "cboSingleDept";
			this.cboSingleDept.Size = new System.Drawing.Size(160, 40);
			this.cboSingleDept.TabIndex = 20;
			this.ToolTip1.SetToolTip(this.cboSingleDept, null);
			this.cboSingleDept.Visible = false;
			this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
			// 
			// vsLowAccount
			// 
			this.vsLowAccount.Cols = 1;
			this.vsLowAccount.ColumnHeadersVisible = false;
			this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLowAccount.FixedCols = 0;
			this.vsLowAccount.FixedRows = 0;
			this.vsLowAccount.Location = new System.Drawing.Point(20, 118);
			this.vsLowAccount.Name = "vsLowAccount";
			this.vsLowAccount.ReadOnly = false;
			this.vsLowAccount.RowHeadersVisible = false;
			this.vsLowAccount.Rows = 1;
			this.vsLowAccount.Size = new System.Drawing.Size(160, 42);
			this.vsLowAccount.TabIndex = 45;
			this.ToolTip1.SetToolTip(this.vsLowAccount, null);
			this.vsLowAccount.Visible = false;
			// 
			// vsHighAccount
			// 
			this.vsHighAccount.Cols = 1;
			this.vsHighAccount.ColumnHeadersVisible = false;
			this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsHighAccount.FixedCols = 0;
			this.vsHighAccount.FixedRows = 0;
			this.vsHighAccount.Location = new System.Drawing.Point(231, 118);
			this.vsHighAccount.Name = "vsHighAccount";
			this.vsHighAccount.ReadOnly = false;
			this.vsHighAccount.RowHeadersVisible = false;
			this.vsHighAccount.Rows = 1;
			this.vsHighAccount.Size = new System.Drawing.Size(160, 42);
			this.vsHighAccount.TabIndex = 46;
			this.ToolTip1.SetToolTip(this.vsHighAccount, null);
			this.vsHighAccount.Visible = false;
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_2.Location = new System.Drawing.Point(198, 132);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(21, 19);
			this.lblTo_2.TabIndex = 4;
			this.lblTo_2.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_2, null);
			this.lblTo_2.Visible = false;
			// 
			// lblTo_3
			// 
			this.lblTo_3.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_3.Location = new System.Drawing.Point(198, 132);
			this.lblTo_3.Name = "lblTo_3";
			this.lblTo_3.Size = new System.Drawing.Size(21, 19);
			this.lblTo_3.TabIndex = 38;
			this.lblTo_3.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_3, null);
			this.lblTo_3.Visible = false;
			// 
			// lblTo_1
			// 
			this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_1.Location = new System.Drawing.Point(198, 134);
			this.lblTo_1.Name = "lblTo_1";
			this.lblTo_1.Size = new System.Drawing.Size(21, 19);
			this.lblTo_1.TabIndex = 35;
			this.lblTo_1.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_1, null);
			this.lblTo_1.Visible = false;
			// 
			// fraTotals
			// 
			this.fraTotals.BackColor = System.Drawing.SystemColors.Menu;
			this.fraTotals.Controls.Add(this.chkHideExpense);
			this.fraTotals.Controls.Add(this.cmbRegular);
			this.fraTotals.Controls.Add(this.lblRegular);
			this.fraTotals.Location = new System.Drawing.Point(30, 90);
			this.fraTotals.Name = "fraTotals";
			this.fraTotals.Size = new System.Drawing.Size(368, 122);
			this.fraTotals.TabIndex = 2;
			this.fraTotals.Text = "Totals Format";
			this.ToolTip1.SetToolTip(this.fraTotals, null);
			// 
			// chkHideExpense
			// 
			this.chkHideExpense.BackColor = System.Drawing.SystemColors.Menu;
			this.chkHideExpense.Location = new System.Drawing.Point(20, 84);
			this.chkHideExpense.Name = "chkHideExpense";
            this.chkHideExpense.Size = new System.Drawing.Size(149, 23);
			this.chkHideExpense.TabIndex = 2;
			this.chkHideExpense.Text = "Hide Expense Totals";
			this.ToolTip1.SetToolTip(this.chkHideExpense, null);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 44);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(124, 16);
			this.lblDescription.Text = "SELECTION CRITERIA";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(120, 48);
			this.cmdProcessSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmExpenseSummarySetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmExpenseSummarySetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Expense Summary Selection Criteria";
			this.ToolTip1.SetToolTip(this, null);
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmExpenseSummarySetup_Load);
			this.Activated += new System.EventHandler(this.frmExpenseSummarySetup_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmExpenseSummarySetup_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmExpenseSummarySetup_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPageBreaks)).EndInit();
			this.fraPageBreaks.ResumeLayout(false);
			this.fraPageBreaks.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDivision)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDepartment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
			this.fraMonths.ResumeLayout(false);
			this.fraMonths.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
			this.fraDeptRange.ResumeLayout(false);
			this.fraDeptRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTotals)).EndInit();
			this.fraTotals.ResumeLayout(false);
			this.fraTotals.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHideExpense)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdProcessSave;
	}
}
