﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCurrentAccountStatus.
	/// </summary>
	partial class frmCurrentAccountStatus : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSingleAccount;
		public fecherFoundation.FCLabel lblSingleAccount;
		public fecherFoundation.FCComboBox cmbLedger;
		public fecherFoundation.FCLabel lblLedger;
		public fecherFoundation.FCComboBox cmbTown;
		public fecherFoundation.FCLabel lblTown;
		public fecherFoundation.FCComboBox cmbPostedDate;
		public fecherFoundation.FCLabel lblPostedDate;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCFrame fraDeptRange;
		public fecherFoundation.FCComboBox cboBeginningDept;
		public fecherFoundation.FCComboBox cboEndingDept;
		public fecherFoundation.FCComboBox cboSingleDept;
		public fecherFoundation.FCComboBox cboBeginningFund;
		public fecherFoundation.FCComboBox cboEndingFund;
		public fecherFoundation.FCComboBox cboSingleFund;
		public FCGrid vsLowAccount;
		public FCGrid vsSingleAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_3;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblTo_2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public fecherFoundation.FCGrid vsSort;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCFrame fraMonthRange;
		public fecherFoundation.FCComboBox cboHighMonth;
		public fecherFoundation.FCComboBox cboLowMonth;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCButton cmdFilePrint;
		public fecherFoundation.FCButton cmdFilePreview;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCurrentAccountStatus));
			this.cmbSingleAccount = new fecherFoundation.FCComboBox();
			this.lblSingleAccount = new fecherFoundation.FCLabel();
			this.cmbLedger = new fecherFoundation.FCComboBox();
			this.lblLedger = new fecherFoundation.FCLabel();
			this.cmbTown = new fecherFoundation.FCComboBox();
			this.lblTown = new fecherFoundation.FCLabel();
			this.cmbPostedDate = new fecherFoundation.FCComboBox();
			this.lblPostedDate = new fecherFoundation.FCLabel();
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.lblAll = new fecherFoundation.FCLabel();
			this.fraDeptRange = new fecherFoundation.FCFrame();
			this.lblTo_3 = new fecherFoundation.FCLabel();
			this.lblTo_1 = new fecherFoundation.FCLabel();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.cboBeginningDept = new fecherFoundation.FCComboBox();
			this.cboSingleDept = new fecherFoundation.FCComboBox();
			this.cboBeginningFund = new fecherFoundation.FCComboBox();
			this.cboSingleFund = new fecherFoundation.FCComboBox();
			this.vsLowAccount = new fecherFoundation.FCGrid();
			this.vsSingleAccount = new fecherFoundation.FCGrid();
			this.cboEndingDept = new fecherFoundation.FCComboBox();
			this.cboEndingFund = new fecherFoundation.FCComboBox();
			this.vsHighAccount = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
			this.vsSort = new fecherFoundation.FCGrid();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraDate = new fecherFoundation.FCFrame();
			this.fraMonthRange = new fecherFoundation.FCFrame();
			this.cboHighMonth = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.cboLowMonth = new fecherFoundation.FCComboBox();
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtLowDate = new Global.T2KDateBox();
			this.txtHighDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).BeginInit();
			this.fraDeptRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSingleAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMonthRange)).BeginInit();
			this.fraMonthRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 540);
			this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPrint);
			this.ClientArea.Controls.Add(this.cmdQuit);
			this.ClientArea.Controls.Add(this.fraDate);
			this.ClientArea.Controls.Add(this.vsSort);
			this.ClientArea.Controls.Add(this.fraDeptRange);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
			this.ClientArea.Controls.Add(this.lblTitle);
			this.ClientArea.Size = new System.Drawing.Size(1068, 480);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(1068, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(266, 30);
			this.HeaderText.Text = "Current Account Status";
			// 
			// cmbSingleAccount
			// 
			this.cmbSingleAccount.AutoSize = false;
			this.cmbSingleAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSingleAccount.FormattingEnabled = true;
			this.cmbSingleAccount.Items.AddRange(new object[] {
				"All",
				"Single Account",
				"Account Range",
				"Single Department",
				"Department Range"
			});
			this.cmbSingleAccount.Location = new System.Drawing.Point(154, 30);
			this.cmbSingleAccount.Name = "cmbSingleAccount";
			this.cmbSingleAccount.Size = new System.Drawing.Size(282, 40);
			this.cmbSingleAccount.TabIndex = 33;
			this.cmbSingleAccount.SelectedIndexChanged += new System.EventHandler(this.optSingleAccount_CheckedChanged);
			// 
			// lblSingleAccount
			// 
			this.lblSingleAccount.AutoSize = true;
			this.lblSingleAccount.Location = new System.Drawing.Point(20, 44);
			this.lblSingleAccount.Name = "lblSingleAccount";
			this.lblSingleAccount.Size = new System.Drawing.Size(77, 15);
			this.lblSingleAccount.TabIndex = 34;
			this.lblSingleAccount.Text = "ACCOUNTS";
			this.lblSingleAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbLedger
			// 
			this.cmbLedger.AutoSize = false;
			this.cmbLedger.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLedger.FormattingEnabled = true;
			this.cmbLedger.Items.AddRange(new object[] {
				"Expense",
				"Revenue",
				"General Ledger"
			});
			this.cmbLedger.Location = new System.Drawing.Point(179, 30);
			this.cmbLedger.Name = "cmbLedger";
			this.cmbLedger.Size = new System.Drawing.Size(160, 40);
			this.cmbLedger.TabIndex = 2;
			this.cmbLedger.SelectedIndexChanged += new System.EventHandler(this.optLedger_CheckedChanged);
			// 
			// lblLedger
			// 
			this.lblLedger.AutoSize = true;
			this.lblLedger.Location = new System.Drawing.Point(20, 44);
			this.lblLedger.Name = "lblLedger";
			this.lblLedger.Size = new System.Drawing.Size(104, 15);
			this.lblLedger.TabIndex = 3;
			this.lblLedger.Text = "ACCOUNT TYPE";
			this.lblLedger.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbTown
			// 
			this.cmbTown.AutoSize = false;
			this.cmbTown.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTown.FormattingEnabled = true;
			this.cmbTown.Items.AddRange(new object[] {
				"Town Accounts"
			});
			this.cmbTown.Location = new System.Drawing.Point(179, 30);
			this.cmbTown.Name = "cmbTown";
			this.cmbTown.Size = new System.Drawing.Size(160, 40);
			this.cmbTown.TabIndex = 0;
			this.cmbTown.Visible = false;
			this.cmbTown.SelectedIndexChanged += new System.EventHandler(this.optTown_CheckedChanged);
			// 
			// lblTown
			// 
			this.lblTown.AutoSize = true;
			this.lblTown.Location = new System.Drawing.Point(20, 44);
			this.lblTown.Name = "lblTown";
			this.lblTown.Size = new System.Drawing.Size(104, 15);
			this.lblTown.TabIndex = 1;
			this.lblTown.Text = "ACCOUNT TYPE";
			this.lblTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblTown.Visible = false;
			// 
			// cmbPostedDate
			// 
			this.cmbPostedDate.AutoSize = false;
			this.cmbPostedDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPostedDate.FormattingEnabled = true;
			this.cmbPostedDate.Items.AddRange(new object[] {
				"Posted Date",
				"Activity Date"
			});
			this.cmbPostedDate.Location = new System.Drawing.Point(173, 0);
			this.cmbPostedDate.Name = "cmbPostedDate";
			this.cmbPostedDate.Size = new System.Drawing.Size(232, 40);
			this.cmbPostedDate.TabIndex = 6;
			// 
			// lblPostedDate
			// 
			this.lblPostedDate.AutoSize = true;
			this.lblPostedDate.Location = new System.Drawing.Point(0, 0);
			this.lblPostedDate.Name = "lblPostedDate";
			this.lblPostedDate.Size = new System.Drawing.Size(88, 15);
			this.lblPostedDate.TabIndex = 6;
			this.lblPostedDate.Text = "DATE RANGE";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All Activity",
				"Date Range",
				"Accounting Period Range"
			});
			this.cmbAll.Location = new System.Drawing.Point(193, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(232, 40);
			this.cmbAll.TabIndex = 9;
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.optAll_CheckedChanged);
			// 
			// lblAll
			// 
			this.lblAll.AutoSize = true;
			this.lblAll.Location = new System.Drawing.Point(20, 44);
			this.lblAll.Name = "lblAll";
			this.lblAll.Size = new System.Drawing.Size(140, 15);
			this.lblAll.TabIndex = 10;
			this.lblAll.Text = "REPORTING OPTIONS";
			// 
			// fraDeptRange
			// 
			this.fraDeptRange.BackColor = System.Drawing.SystemColors.Menu;
			this.fraDeptRange.Controls.Add(this.cmbSingleAccount);
			this.fraDeptRange.Controls.Add(this.lblSingleAccount);
			this.fraDeptRange.Controls.Add(this.lblTo_3);
			this.fraDeptRange.Controls.Add(this.lblTo_1);
			this.fraDeptRange.Controls.Add(this.lblTo_2);
			this.fraDeptRange.Controls.Add(this.cboBeginningDept);
			this.fraDeptRange.Controls.Add(this.cboSingleDept);
			this.fraDeptRange.Controls.Add(this.cboBeginningFund);
			this.fraDeptRange.Controls.Add(this.cboSingleFund);
			this.fraDeptRange.Controls.Add(this.vsLowAccount);
			this.fraDeptRange.Controls.Add(this.vsSingleAccount);
			this.fraDeptRange.Controls.Add(this.cboEndingDept);
			this.fraDeptRange.Controls.Add(this.cboEndingFund);
			this.fraDeptRange.Controls.Add(this.vsHighAccount);
			this.fraDeptRange.Location = new System.Drawing.Point(30, 150);
			this.fraDeptRange.Name = "fraDeptRange";
			this.fraDeptRange.Size = new System.Drawing.Size(456, 148);
			this.fraDeptRange.TabIndex = 25;
			this.fraDeptRange.Text = "Accounts To Be Reported";
			// 
			// lblTo_3
			// 
			this.lblTo_3.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_3.Location = new System.Drawing.Point(221, 104);
			this.lblTo_3.Name = "lblTo_3";
			this.lblTo_3.Size = new System.Drawing.Size(21, 19);
			this.lblTo_3.TabIndex = 42;
			this.lblTo_3.Text = "TO";
			this.lblTo_3.Visible = false;
			// 
			// lblTo_1
			// 
			this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_1.Location = new System.Drawing.Point(221, 104);
			this.lblTo_1.Name = "lblTo_1";
			this.lblTo_1.Size = new System.Drawing.Size(21, 19);
			this.lblTo_1.TabIndex = 41;
			this.lblTo_1.Text = "TO";
			this.lblTo_1.Visible = false;
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_2.Location = new System.Drawing.Point(221, 104);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(21, 19);
			this.lblTo_2.TabIndex = 40;
			this.lblTo_2.Text = "TO";
			this.lblTo_2.Visible = false;
			// 
			// cboBeginningDept
			// 
			this.cboBeginningDept.AutoSize = false;
			this.cboBeginningDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningDept.FormattingEnabled = true;
			this.cboBeginningDept.Location = new System.Drawing.Point(20, 90);
			this.cboBeginningDept.Name = "cboBeginningDept";
			this.cboBeginningDept.Size = new System.Drawing.Size(181, 40);
			this.cboBeginningDept.TabIndex = 32;
			this.cboBeginningDept.Visible = false;
			this.cboBeginningDept.DropDown += new System.EventHandler(this.cboBeginningDept_DropDown);
			// 
			// cboSingleDept
			// 
			this.cboSingleDept.AutoSize = false;
			this.cboSingleDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleDept.FormattingEnabled = true;
			this.cboSingleDept.Location = new System.Drawing.Point(20, 90);
			this.cboSingleDept.Name = "cboSingleDept";
			this.cboSingleDept.Size = new System.Drawing.Size(181, 40);
			this.cboSingleDept.TabIndex = 30;
			this.cboSingleDept.Visible = false;
			this.cboSingleDept.DropDown += new System.EventHandler(this.cboSingleDept_DropDown);
			// 
			// cboBeginningFund
			// 
			this.cboBeginningFund.AutoSize = false;
			this.cboBeginningFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningFund.FormattingEnabled = true;
			this.cboBeginningFund.Location = new System.Drawing.Point(20, 90);
			this.cboBeginningFund.Name = "cboBeginningFund";
			this.cboBeginningFund.Size = new System.Drawing.Size(181, 40);
			this.cboBeginningFund.TabIndex = 29;
			this.cboBeginningFund.Visible = false;
			this.cboBeginningFund.DropDown += new System.EventHandler(this.cboBeginningFund_DropDown);
			// 
			// cboSingleFund
			// 
			this.cboSingleFund.AutoSize = false;
			this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleFund.FormattingEnabled = true;
			this.cboSingleFund.Location = new System.Drawing.Point(20, 90);
			this.cboSingleFund.Name = "cboSingleFund";
			this.cboSingleFund.Size = new System.Drawing.Size(181, 40);
			this.cboSingleFund.TabIndex = 27;
			this.cboSingleFund.Visible = false;
			this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
			// 
			// vsLowAccount
			// 
			this.vsLowAccount.AllowSelection = false;
			this.vsLowAccount.AllowUserToResizeColumns = false;
			this.vsLowAccount.AllowUserToResizeRows = false;
			this.vsLowAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLowAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsLowAccount.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLowAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsLowAccount.ColumnHeadersHeight = 30;
			this.vsLowAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsLowAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLowAccount.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsLowAccount.DragIcon = null;
			this.vsLowAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLowAccount.ExtendLastCol = true;
			this.vsLowAccount.FixedCols = 0;
			this.vsLowAccount.FixedRows = 0;
			this.vsLowAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.FrozenCols = 0;
			this.vsLowAccount.GridColor = System.Drawing.Color.Empty;
			this.vsLowAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.Location = new System.Drawing.Point(20, 90);
			this.vsLowAccount.Name = "vsLowAccount";
			this.vsLowAccount.ReadOnly = true;
			this.vsLowAccount.RowHeadersVisible = false;
			this.vsLowAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLowAccount.RowHeightMin = 0;
			this.vsLowAccount.Rows = 1;
			this.vsLowAccount.ScrollTipText = null;
			this.vsLowAccount.ShowColumnVisibilityMenu = false;
			this.vsLowAccount.Size = new System.Drawing.Size(181, 42);
			this.vsLowAccount.StandardTab = true;
			this.vsLowAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLowAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLowAccount.TabIndex = 37;
			this.vsLowAccount.Visible = false;
			this.vsLowAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsLowAccount_KeyPressEdit);
			this.vsLowAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsLowAccount_ChangeEdit);
			// 
			// vsSingleAccount
			// 
			this.vsSingleAccount.AllowSelection = false;
			this.vsSingleAccount.AllowUserToResizeColumns = false;
			this.vsSingleAccount.AllowUserToResizeRows = false;
			this.vsSingleAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSingleAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsSingleAccount.Cols = 1;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSingleAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsSingleAccount.ColumnHeadersHeight = 30;
			this.vsSingleAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsSingleAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSingleAccount.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsSingleAccount.DragIcon = null;
			this.vsSingleAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSingleAccount.ExtendLastCol = true;
			this.vsSingleAccount.FixedCols = 0;
			this.vsSingleAccount.FixedRows = 0;
			this.vsSingleAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.FrozenCols = 0;
			this.vsSingleAccount.GridColor = System.Drawing.Color.Empty;
			this.vsSingleAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSingleAccount.Location = new System.Drawing.Point(20, 90);
			this.vsSingleAccount.Name = "vsSingleAccount";
			this.vsSingleAccount.ReadOnly = true;
			this.vsSingleAccount.RowHeadersVisible = false;
			this.vsSingleAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSingleAccount.RowHeightMin = 0;
			this.vsSingleAccount.Rows = 1;
			this.vsSingleAccount.ScrollTipText = null;
			this.vsSingleAccount.ShowColumnVisibilityMenu = false;
			this.vsSingleAccount.Size = new System.Drawing.Size(181, 42);
			this.vsSingleAccount.StandardTab = true;
			this.vsSingleAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSingleAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSingleAccount.TabIndex = 38;
			this.vsSingleAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsSingleAccount_KeyPressEdit);
			this.vsSingleAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsSingleAccount_ChangeEdit);
			// 
			// cboEndingDept
			// 
			this.cboEndingDept.AutoSize = false;
			this.cboEndingDept.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingDept.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingDept.FormattingEnabled = true;
			this.cboEndingDept.Location = new System.Drawing.Point(255, 90);
			this.cboEndingDept.Name = "cboEndingDept";
			this.cboEndingDept.Size = new System.Drawing.Size(181, 40);
			this.cboEndingDept.TabIndex = 31;
			this.cboEndingDept.Visible = false;
			this.cboEndingDept.DropDown += new System.EventHandler(this.cboEndingDept_DropDown);
			// 
			// cboEndingFund
			// 
			this.cboEndingFund.AutoSize = false;
			this.cboEndingFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingFund.FormattingEnabled = true;
			this.cboEndingFund.Location = new System.Drawing.Point(255, 90);
			this.cboEndingFund.Name = "cboEndingFund";
			this.cboEndingFund.Size = new System.Drawing.Size(181, 40);
			this.cboEndingFund.TabIndex = 28;
			this.cboEndingFund.Visible = false;
			this.cboEndingFund.DropDown += new System.EventHandler(this.cboEndingFund_DropDown);
			// 
			// vsHighAccount
			// 
			this.vsHighAccount.AllowSelection = false;
			this.vsHighAccount.AllowUserToResizeColumns = false;
			this.vsHighAccount.AllowUserToResizeRows = false;
			this.vsHighAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsHighAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsHighAccount.Cols = 1;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsHighAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsHighAccount.ColumnHeadersHeight = 30;
			this.vsHighAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsHighAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsHighAccount.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsHighAccount.DragIcon = null;
			this.vsHighAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsHighAccount.ExtendLastCol = true;
			this.vsHighAccount.FixedCols = 0;
			this.vsHighAccount.FixedRows = 0;
			this.vsHighAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.FrozenCols = 0;
			this.vsHighAccount.GridColor = System.Drawing.Color.Empty;
			this.vsHighAccount.GridColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.Location = new System.Drawing.Point(255, 90);
			this.vsHighAccount.Name = "vsHighAccount";
			this.vsHighAccount.ReadOnly = true;
			this.vsHighAccount.RowHeadersVisible = false;
			this.vsHighAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsHighAccount.RowHeightMin = 0;
			this.vsHighAccount.Rows = 1;
			this.vsHighAccount.ScrollTipText = null;
			this.vsHighAccount.ShowColumnVisibilityMenu = false;
			this.vsHighAccount.Size = new System.Drawing.Size(181, 42);
			this.vsHighAccount.StandardTab = true;
			this.vsHighAccount.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsHighAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsHighAccount.TabIndex = 39;
			this.vsHighAccount.Visible = false;
			this.vsHighAccount.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsHighAccount_KeyPressEdit);
			this.vsHighAccount.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsHighAccount_ChangeEdit);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.lblTown);
			this.Frame1.Controls.Add(this.cmbTown);
			this.Frame1.Controls.Add(this.lblLedger);
			this.Frame1.Controls.Add(this.cmbLedger);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(359, 90);
			this.Frame1.TabIndex = 19;
			this.Frame1.Text = "Account Type";
			// 
			// chkShowLiquidatedEncumbranceActivity
			// 
			this.chkShowLiquidatedEncumbranceActivity.Location = new System.Drawing.Point(30, 628);
			this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
			this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(311, 27);
			this.chkShowLiquidatedEncumbranceActivity.TabIndex = 16;
			this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
			// 
			// vsSort
			// 
			this.vsSort.AllowSelection = false;
			this.vsSort.AllowUserToResizeColumns = false;
			this.vsSort.AllowUserToResizeRows = false;
			this.vsSort.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSort.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSort.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSort.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSort.BackColorSel = System.Drawing.Color.Empty;
			this.vsSort.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSort.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsSort.ColumnHeadersHeight = 30;
			this.vsSort.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsSort.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSort.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsSort.DragIcon = null;
			this.vsSort.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSort.FixedCols = 0;
			this.vsSort.FixedRows = 0;
			this.vsSort.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSort.FrozenCols = 0;
			this.vsSort.GridColor = System.Drawing.Color.Empty;
			this.vsSort.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSort.Location = new System.Drawing.Point(34, 469);
			this.vsSort.Name = "vsSort";
			this.vsSort.ReadOnly = true;
			this.vsSort.RowHeadersVisible = false;
			this.vsSort.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSort.RowHeightMin = 0;
			this.vsSort.Rows = 50;
			this.vsSort.ScrollTipText = null;
			this.vsSort.ShowColumnVisibilityMenu = false;
			this.vsSort.Size = new System.Drawing.Size(74, 39);
			this.vsSort.StandardTab = true;
			this.vsSort.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSort.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSort.TabIndex = 13;
			this.vsSort.Visible = false;
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.ForeColor = System.Drawing.Color.White;
			this.cmdQuit.Location = new System.Drawing.Point(503, 276);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(135, 48);
			this.cmdQuit.TabIndex = 15;
			this.cmdQuit.Text = "Quit";
			this.cmdQuit.Visible = false;
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "actionButton";
			this.cmdPrint.ForeColor = System.Drawing.Color.White;
			this.cmdPrint.Location = new System.Drawing.Point(503, 198);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(135, 48);
			this.cmdPrint.TabIndex = 14;
			this.cmdPrint.Text = "Print / Preview";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// fraDate
			// 
			this.fraDate.Controls.Add(this.fraMonthRange);
			this.fraDate.Controls.Add(this.cmbAll);
			this.fraDate.Controls.Add(this.lblAll);
			this.fraDate.Controls.Add(this.fraDateRange);
			this.fraDate.Location = new System.Drawing.Point(30, 328);
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(443, 270);
			this.fraDate.TabIndex = 2;
			this.fraDate.Text = "Reporting Options";
			// 
			// fraMonthRange
			// 
			this.fraMonthRange.AppearanceKey = "groupBoxNoBorders";
			this.fraMonthRange.Controls.Add(this.cboHighMonth);
			this.fraMonthRange.Controls.Add(this.Label3);
			this.fraMonthRange.Controls.Add(this.cboLowMonth);
			this.fraMonthRange.Enabled = false;
			this.fraMonthRange.Location = new System.Drawing.Point(20, 210);
			this.fraMonthRange.Name = "fraMonthRange";
			this.fraMonthRange.Size = new System.Drawing.Size(382, 60);
			this.fraMonthRange.TabIndex = 8;
			// 
			// cboHighMonth
			// 
			this.cboHighMonth.AutoSize = false;
			this.cboHighMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboHighMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboHighMonth.FormattingEnabled = true;
			this.cboHighMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboHighMonth.Location = new System.Drawing.Point(213, 0);
			this.cboHighMonth.Name = "cboHighMonth";
			this.cboHighMonth.Size = new System.Drawing.Size(160, 40);
			this.cboHighMonth.TabIndex = 11;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(176, 14);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(23, 21);
			this.Label3.TabIndex = 9;
			this.Label3.Text = "TO";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cboLowMonth
			// 
			this.cboLowMonth.AutoSize = false;
			this.cboLowMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboLowMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboLowMonth.FormattingEnabled = true;
			this.cboLowMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboLowMonth.Location = new System.Drawing.Point(0, 0);
			this.cboLowMonth.Name = "cboLowMonth";
			this.cboLowMonth.Size = new System.Drawing.Size(160, 40);
			this.cboLowMonth.TabIndex = 10;
			// 
			// fraDateRange
			// 
			this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
			this.fraDateRange.Controls.Add(this.txtLowDate);
			this.fraDateRange.Controls.Add(this.cmbPostedDate);
			this.fraDateRange.Controls.Add(this.lblPostedDate);
			this.fraDateRange.Controls.Add(this.txtHighDate);
			this.fraDateRange.Controls.Add(this.Label2);
			this.fraDateRange.Enabled = false;
			this.fraDateRange.Location = new System.Drawing.Point(20, 90);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(417, 100);
			this.fraDateRange.TabIndex = 3;
			// 
			// txtLowDate
			// 
			this.txtLowDate.Location = new System.Drawing.Point(0, 60);
			this.txtLowDate.Mask = "00/00/0000";
			this.txtLowDate.Name = "txtLowDate";
			this.txtLowDate.Size = new System.Drawing.Size(160, 40);
			this.txtLowDate.TabIndex = 4;
			this.txtLowDate.Text = "  /  /";
			// 
			// txtHighDate
			// 
			this.txtHighDate.Location = new System.Drawing.Point(213, 60);
			this.txtHighDate.Mask = "00/00/0000";
			this.txtHighDate.Name = "txtHighDate";
			this.txtHighDate.Size = new System.Drawing.Size(160, 40);
			this.txtHighDate.TabIndex = 5;
			this.txtHighDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(176, 74);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(23, 21);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(30, 30);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(575, 23);
			this.lblTitle.TabIndex = 12;
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblTitle.Visible = false;
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(995, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(43, 25);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(274, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(147, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmCurrentAccountStatus
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1068, 648);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCurrentAccountStatus";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Current Account Status";
			this.Load += new System.EventHandler(this.frmCurrentAccountStatus_Load);
			this.Activated += new System.EventHandler(this.frmCurrentAccountStatus_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCurrentAccountStatus_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCurrentAccountStatus_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDeptRange)).EndInit();
			this.fraDeptRange.ResumeLayout(false);
			this.fraDeptRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSingleAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			this.fraDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMonthRange)).EndInit();
			this.fraMonthRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
