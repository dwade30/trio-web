﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReturnCheck.
	/// </summary>
	public partial class frmReturnCheck : BaseForm
	{
		public frmReturnCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.txtAddress.AddControlArrayElement(txtAddress_0, 0);
			this.txtAddress.AddControlArrayElement(txtAddress_1, 1);
			this.txtAddress.AddControlArrayElement(txtAddress_2, 2);
			this.txtAddress.AddControlArrayElement(txtAddress_3, 3);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReturnCheck InstancePtr
		{
			get
			{
				return (frmReturnCheck)Sys.GetInstance(typeof(frmReturnCheck));
			}
		}

		protected frmReturnCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Dave Wade
		// Date           12/31/2003
		// This form will be used to return an AP check
		// ********************************************************
		int KeyCol;
		int DescriptionCol;
		int ReferenceCol;
		int AmountCol;
		clsDRWrapper Master = new clsDRWrapper();
		clsDRWrapper rs = new clsDRWrapper();
		int VendorNumber;
		bool blnJournalLocked;
		int intPeriod;
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;

		private void frmReturnCheck_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			FillJournalCombo();
			cboJournal.SelectedIndex = 0;
			cboSaveJournal.SelectedIndex = 0;
			if (vsInfo.Rows <= 7)
			{
				//FC:FINAL:MSH - issue #887: changed height of the Grid to hide scrollbars
				//vsInfo.HeightOriginal = (vsInfo.RowHeight(0) * vsInfo.Rows) + 75;
				vsInfo.HeightOriginal = (vsInfo.RowHeight(0) * vsInfo.Rows) + 175;
			}
			else
			{
				//FC:FINAL:MSH - issue #887: changed height of the Grid to hide scrollbars
				//vsInfo.HeightOriginal = (vsInfo.RowHeight(0) * 7) + 75;
				vsInfo.HeightOriginal = (vsInfo.RowHeight(0) * 7) + 175;
			}
			this.Refresh();
		}

		private void frmReturnCheck_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReturnCheck.FillStyle	= 0;
			//frmReturnCheck.ScaleWidth	= 5880;
			//frmReturnCheck.ScaleHeight	= 4245;
			//frmReturnCheck.LinkTopic	= "Form2";
			//frmReturnCheck.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			blnJournalLocked = false;
			KeyCol = 0;
			DescriptionCol = 1;
			ReferenceCol = 2;
			AmountCol = 3;
			vsInfo.TextMatrix(0, DescriptionCol, "Description");
			vsInfo.TextMatrix(0, ReferenceCol, "Reference");
			vsInfo.TextMatrix(0, AmountCol, "Amount");
            vsInfo.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsInfo.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsInfo.ColHidden(KeyCol, true);
			vsInfo.ColWidth(DescriptionCol, 2500);
			vsInfo.ColWidth(ReferenceCol, 1500);
			for (counter = 1; counter <= 12; counter++)
			{
				cboSavePeriod.AddItem(Strings.Format(counter, "00"));
			}
			cboSavePeriod.SelectedIndex = DateTime.Today.Month - 1;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			blnPostJournal = false;
		}

		private void frmReturnCheck_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'AC' ORDER BY JournalNumber DESC");
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(rsJournalPeriod.Get_Fields("JournalNumber"), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			string strTempAcct = "";
			intPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(txtPeriod.Text)));
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
					lblSavePeriod.Visible = false;
					cboSavePeriod.Visible = false;
					cboSaveJournal.Focus();
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					lblSavePeriod.Visible = true;
					cboSavePeriod.Visible = true;
					txtJournalDescription.Text = "Check Return - " + txtCheck.Text;
					txtJournalDescription.Focus();
					txtJournalDescription.SelectionStart = 0;
					txtJournalDescription.SelectionLength = txtJournalDescription.Text.Length;
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(cmdProcessSave, new System.EventArgs());
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void txtPeriod_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtPeriod.Text == "")
			{
				txtPeriod.Text = CalculatePeriod();
			}
			else if (!Information.IsNumeric(txtPeriod.Text))
			{
				MessageBox.Show("You must enter a number in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else if (Conversion.Val(txtPeriod.Text) > 12)
			{
				MessageBox.Show("You must enter a number less then 12 in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else
			{
				temp = txtPeriod.Text;
				txtPeriod.Text = modValidateAccount.GetFormat_6(temp, 2);
			}
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			if (cboSavePeriod.Visible == true)
			{
				intPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(cboSavePeriod.Text)));
			}
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:DSE:#525 fix design
			cboSaveJournal.Width = 90;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			int counter;
			clsDRWrapper rsEncData = new clsDRWrapper();
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			clsDRWrapper rsCreditData = new clsDRWrapper();
			clsDRWrapper rsCorrRecords = new clsDRWrapper();
			clsDRWrapper rsOriginalJournalInfo = new clsDRWrapper();
			int lngOriginalJournalNumber = 0;
			int intBankNumber = 0;
			txtVendor.Focus();
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "AC");
				Master.Set_Fields("Period", intPeriod);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(Master.Get_Fields("Period")) != intPeriod)
				{
					if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
					{
						executeAddTag = true;
						goto AddTag;
					}
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (FCConvert.ToInt32(Master.Get_Fields("Period")) != intPeriod)
				{
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(intPeriod));
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", "Add Entries?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
							{
								// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
								if (Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2)) == intPeriod)
								{
									cboSaveJournal.SelectedIndex = counter;
									cboJournal.SelectedIndex = counter;
									break;
								}
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
					else
					{
						ans = MessageBox.Show("This Journal Number is used for entries in Accounting Period " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))))) + ".  Do you wish to use this Journal Number?", "Update Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							if (Strings.Trim(txtJournalDescription.Text) != "")
							{
								executeAddTag = true;
								goto AddTag;
							}
							else
							{
								fraJournalSave.Visible = true;
								lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
								lblJournalSave.Visible = false;
								cboSaveJournal.Visible = false;
								lblJournalDescription.Visible = true;
								txtJournalDescription.Visible = true;
								lblSavePeriod.Visible = true;
								cboSavePeriod.Visible = true;
								txtJournalDescription.Focus();
								return;
							}
						}
						else
						{
							cboJournal.SelectedIndex = 0;
							mnuProcessSave_Click();
							return;
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					Master.AddNew();
					Master.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("Description", txtJournalDescription.Text);
					Master.Set_Fields("Type", "AC");
					Master.Set_Fields("Period", intPeriod);
					Master.Update();
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			clsJournalInfo.JournalType = "AC";
			clsJournalInfo.Period = FCConvert.ToString(intPeriod);
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = 0");
			for (counter = 1; counter <= vsInfo.Rows - 1; counter++)
			{
				rs.AddNew();
				if (cboSaveJournal.SelectedIndex != 0)
				{
					rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
				}
				else
				{
					rs.Set_Fields("JournalNumber", TempJournal);
				}
				rs.Set_Fields("VendorNumber", txtVendor.Text);
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
				{
					rs.Set_Fields("TempVendorName", txtAddress[0].Text);
					rs.Set_Fields("TempVendorAddress1", txtAddress[1].Text);
					rs.Set_Fields("TempVendorAddress2", txtAddress[2].Text);
					rs.Set_Fields("TempVendorAddress3", txtAddress[3].Text);
					rs.Set_Fields("TempVendorCity", txtCity.Text);
					rs.Set_Fields("TempVendorState", txtState.Text);
					rs.Set_Fields("TempVendorZip", txtZip.Text);
					rs.Set_Fields("TempVendorZip4", txtZip4.Text);
				}
				rs.Set_Fields("Period", intPeriod);
				rs.Set_Fields("Description", vsInfo.TextMatrix(counter, DescriptionCol));
				rs.Set_Fields("Reference", vsInfo.TextMatrix(counter, ReferenceCol));
				rs.Set_Fields("Amount", FCConvert.ToDecimal(vsInfo.TextMatrix(counter, AmountCol)) * -1);
				rs.Set_Fields("Returned", "D");
				rs.Set_Fields("CheckNumber", txtCheck.Text);
				rs.Set_Fields("CheckDate", DateTime.Today);
				rs.Set_Fields("Status", "E");
				rsEncData.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + vsInfo.TextMatrix(counter, KeyCol));
				if (rsEncData.EndOfFile() != true && rsEncData.BeginningOfFile() != true)
				{
					rs.Set_Fields("EncumbranceRecord", FCConvert.ToString(Conversion.Val(rsEncData.Get_Fields_Int32("EncumbranceRecord"))));
					rs.Set_Fields("UseAlternateCash", rsEncData.Get_Fields_Boolean("UseAlternateCash"));
					rs.Set_Fields("AlternateCashAccount", rsEncData.Get_Fields_String("AlternateCashAccount"));
					// return credit memo money
					if (Conversion.Val(rsEncData.Get_Fields_Int32("CreditMemoRecord") + "") != 0)
					{
						clsDRWrapper rsCreditDetail = new clsDRWrapper();
						// vbPorter upgrade warning: curDetailAmount As Decimal	OnWrite(short, Decimal)
						Decimal curDetailAmount;
						rsCreditData.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + rsEncData.Get_Fields_Int32("CreditMemoRecord"));
						if (rsCreditData.EndOfFile() != true && rsCreditData.BeginningOfFile() != true)
						{
							rsCreditData.Edit();
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							rsCreditData.Set_Fields("Liquidated", rsCreditData.Get_Fields_Decimal("Liquidated") + rsEncData.Get_Fields("Amount"));
							rsCreditData.Update();
						}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						curDetailAmount = FCConvert.ToDecimal(rsEncData.Get_Fields("Amount"));
						rsCreditDetail.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE Liquidated <> 0 AND CreditMemoID = " + rsCreditData.Get_Fields_Int32("ID"));
						if (rsCreditDetail.EndOfFile() != true && rsCreditDetail.BeginningOfFile() != true)
						{
							do
							{
								if (curDetailAmount * -1 <= rsCreditDetail.Get_Fields_Decimal("Liquidated"))
								{
									rsCreditDetail.Edit();
									rsCreditDetail.Set_Fields("Liquidated", rsCreditDetail.Get_Fields_Decimal("Liquidated") + curDetailAmount);
									curDetailAmount = 0;
									rsCreditDetail.Update();
									break;
								}
								else
								{
									rsCreditDetail.Edit();
									curDetailAmount += rsCreditDetail.Get_Fields_Decimal("Liquidated");
									rsCreditDetail.Set_Fields("Liquidated", 0);
									rsCreditDetail.Update();
								}
								rsCreditDetail.MoveNext();
							}
							while (rsCreditDetail.EndOfFile() != true);
						}
					}
					rsEncData.Edit();
					rsEncData.Set_Fields("Returned", "R");
					rsEncData.Update();
					rsCorrRecords.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + vsInfo.TextMatrix(counter, KeyCol));
					if (rsCorrRecords.EndOfFile() != true && rsCorrRecords.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngOriginalJournalNumber = FCConvert.ToInt32(rsCorrRecords.Get_Fields("JournalNumber"));
						do
						{
							rsCorrRecords.Edit();
							rsCorrRecords.Set_Fields("Returned", "R");
							rsCorrRecords.Update();
							rsCorrRecords.MoveNext();
						}
						while (rsCorrRecords.EndOfFile() != true);
					}
				}
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsCheckInfo.OpenRecordset("SELECT * FROM TempCheckFile WHERE WarrantNumber = " + rsEncData.Get_Fields("Warrant") + " AND CheckNumber = " + txtCheck.Text);
				if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
				{
					rsCheckInfo.Edit();
					rsCheckInfo.Set_Fields("Status", "V");
					rsCheckInfo.Update(false);
				}
				rs.Update();
				VendorNumber = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				SaveDetails(ref counter);
			}
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
			}
			rsOriginalJournalInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngOriginalJournalNumber));
			if (rsOriginalJournalInfo.EndOfFile() != true && rsOriginalJournalInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
				intBankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOriginalJournalInfo.Get_Fields("BankNumber"))));
			}
			else
			{
				intBankNumber = 0;
			}
			if (intBankNumber != 0)
			{
				rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + txtCheck.Text + " AND BankNumber = " + FCConvert.ToString(intBankNumber));
			}
			else
			{
				rsCheckInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + txtCheck.Text);
			}
			if (rsCheckInfo.EndOfFile() != true && rsCheckInfo.BeginningOfFile() != true)
			{
				rsCheckInfo.Edit();
				rsCheckInfo.Set_Fields("Status", "V");
				rsCheckInfo.Update(false);
			}
			if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptAccountsPayableCorrections))
			{
				if (MessageBox.Show("Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					blnPostJournal = true;
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
			}
			else
			{
				blnPostJournal = false;
				if (cboSaveJournal.SelectedIndex != 0)
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
				}
				else
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
				}
			}
			Close();
		}
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void SaveDetails(ref int intRow)
		{
			int counter;
			Decimal TotalAmount;
			clsDRWrapper rsCorrect = new clsDRWrapper();
			clsDRWrapper rsDetailInfo = new clsDRWrapper();
			clsDRWrapper rsEncInfo = new clsDRWrapper();
			clsDRWrapper rsJournalInfo = new clsDRWrapper();
			rsDetailInfo.OmitNullsOnInsert = true;
			rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE ID = 0");
			rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(Conversion.Val(vsInfo.TextMatrix(intRow, KeyCol))));
			rsCorrect.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + vsInfo.TextMatrix(intRow, KeyCol));
			do
			{
				rsDetailInfo.AddNew();
				// if so add it
				rsDetailInfo.Set_Fields("APJournalID", VendorNumber);
				if (("VOID - " + rsCorrect.Get_Fields_String("Description")).Length > 25)
				{
					rsDetailInfo.Set_Fields("Description", "VOID - " + Strings.Left(FCConvert.ToString(rsCorrect.Get_Fields_String("Description")), 18));
				}
				else
				{
					rsDetailInfo.Set_Fields("Description", "VOID - " + rsCorrect.Get_Fields_String("Description"));
				}
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("account", rsCorrect.Get_Fields("Account"));
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("Amount", (FCConvert.ToDecimal(rsCorrect.Get_Fields("Amount")) * -1));
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("Discount", (FCConvert.ToDecimal(rsCorrect.Get_Fields("Discount")) * -1));
				rsDetailInfo.Set_Fields("Encumbrance", (FCConvert.ToDecimal(rsCorrect.Get_Fields_Decimal("Encumbrance")) * -1));
				rsDetailInfo.Set_Fields("Project", rsCorrect.Get_Fields_String("Project"));
				// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
				rsDetailInfo.Set_Fields("1099", rsCorrect.Get_Fields("1099"));
				// TextMatrix(counter, TaxCol)
				rsDetailInfo.Set_Fields("RCB", "C");
				if (rsCorrect.EndOfFile() != true && rsCorrect.BeginningOfFile() != true)
				{
					rsDetailInfo.Set_Fields("EncumbranceDetailRecord", FCConvert.ToString(Conversion.Val(rsCorrect.Get_Fields_Int32("EncumbranceDetailRecord"))));
				}
				else
				{
					rsDetailInfo.Set_Fields("EncumbranceDetailRecord", 0);
				}
				rsDetailInfo.Update();
				// update the database
				rsCorrect.MoveNext();
			}
			while (rsCorrect.EndOfFile() != true);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (blnJournalLocked)
			{
				modBudgetaryAccounting.UnlockJournal();
			}
			frmGetGJCorrDataEntry.InstancePtr.Show(App.MainForm);
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}
	}
}
