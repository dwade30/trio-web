﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupTownRevenueReport.
	/// </summary>
	public partial class frmSetupTownRevenueReport : BaseForm
	{
		public frmSetupTownRevenueReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupTownRevenueReport InstancePtr
		{
			get
			{
				return (frmSetupTownRevenueReport)Sys.GetInstance(typeof(frmSetupTownRevenueReport));
			}
		}

		protected frmSetupTownRevenueReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupTownRevenueReport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupTownRevenueReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupTownRevenueReport.FillStyle	= 0;
			//frmSetupTownRevenueReport.ScaleWidth	= 5880;
			//frmSetupTownRevenueReport.ScaleHeight	= 3810;
			//frmSetupTownRevenueReport.LinkTopic	= "Form2";
			//frmSetupTownRevenueReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupTownRevenueReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cboStartMonth.SelectedIndex == -1 || cboEndMonth.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a beginning Month and an ending Month before you may continue.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboEndMonth.SelectedIndex < cboStartMonth.SelectedIndex)
			{
				MessageBox.Show("Your ending date must be later then your beginning date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmReportViewer.InstancePtr.Init(rptTownRevenueReport.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cboStartMonth.SelectedIndex == -1 || cboEndMonth.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a beginning Month and an ending Month before you may continue.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboEndMonth.SelectedIndex < cboStartMonth.SelectedIndex)
			{
				MessageBox.Show("Your ending date must be later then your beginning date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptTownRevenueReport.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
