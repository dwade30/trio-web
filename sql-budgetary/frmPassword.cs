﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using Wisej.Core;
using Global;
using fecherFoundation;
using System;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPassword.
	/// </summary>
	public partial class frmPassword : BaseForm
	{
		public frmPassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPassword InstancePtr
		{
			get
			{
				return (frmPassword)Sys.GetInstance(typeof(frmPassword));
			}
		}

		protected frmPassword _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private void frmPassword_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtPassword.Focus();
		}

		private void frmPassword_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPassword.ScaleWidth	= 3885;
			//frmPassword.ScaleHeight	= 2280;
			//frmPassword.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void txtPassword_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (Strings.UCase(txtPassword.Text) == "EASP")
				{
					txtPassword.Text = "";
					frmAccountSetup.InstancePtr.Show(App.MainForm);
					Close();
				}
				else
				{
					MessageBox.Show("Invalid Password", "Wrong Password", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtPassword.Text = "";
					Close();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
