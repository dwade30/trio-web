﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;

namespace TWBD0000
{
	public class modGlobal
	{
		public const string DEFAULTDATABASE = "TWBD0000.vb1";
		
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "FormExist";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return FormExist;
			}
			ResumeCode:
			;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}
		

		

		

		public static string PadToString(object intValue, short intlength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intlength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}


		
		// vbPorter upgrade warning: dValue As double	OnWrite(Decimal, double, string)
		public static double Round_6(double dValue, short iDigits)
		{
			return Round(dValue, iDigits);
		}

		public static double Round_8(double dValue, short iDigits)
		{
			return Round(dValue, iDigits);
		}

		public static double Round(double dValue, short iDigits)
		{
			double Round = 0;
			//FC:FINAL:BBE:#i568 - Due to rounding errors, use System.Math.Round instead
			//Round = FCConvert.ToInt32(Conversion.Val(dValue * (Math.Pow(10, iDigits))) + 0.5) / (Math.Pow(10, iDigits));
			Round = Math.Round(dValue, iDigits);
			return Round;
		}


        public class StaticVariables
        {
            public string gstrLastYearBilled;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
            }
        }

	}
}
