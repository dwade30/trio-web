﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmJournalVendorChoice.
	/// </summary>
	public partial class frmJournalVendorChoice : BaseForm
	{
		public frmJournalVendorChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collVendors = new cGenericCollection();
		private cGenericCollection collVendors_AutoInitialized;

		private cGenericCollection collVendors
		{
			get
			{
				if (collVendors_AutoInitialized == null)
				{
					collVendors_AutoInitialized = new cGenericCollection();
				}
				return collVendors_AutoInitialized;
			}
			set
			{
				collVendors_AutoInitialized = value;
			}
		}
		// vbPorter upgrade warning: lngChosenID As int	OnWrite(short, string)
		private int lngChosenID;
		// vbPorter upgrade warning: intReturn As short --> As int	OnWrite(DialogResult)
		private int intReturn;

		public int ChosenID
		{
			get
			{
				int ChosenID = 0;
				ChosenID = lngChosenID;
				return ChosenID;
			}
		}

		public cGenericCollection Entries
		{
			set
			{
				collVendors.ClearList();
				value.MoveFirst();
				while (value.IsCurrent())
				{
					collVendors.AddItem(value.GetCurrentItem());
					value.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ShowDialog()
		{
			short ShowDialog = 0;
			lngChosenID = 0;
			this.Show(FormShowEnum.Modal, FCMainForm.InstancePtr);
			ShowDialog = FCConvert.ToInt16(intReturn);
			return ShowDialog;
		}

		private void FillGrid()
		{
			collVendors.MoveFirst();
			GridVendors.Rows = 1;
			int lngRow;
			cVendorOption vo;
			while (collVendors.IsCurrent())
			{
				vo = (cVendorOption)collVendors.GetCurrentItem();
				GridVendors.Rows += 1;
				lngRow = GridVendors.Rows - 1;
				GridVendors.TextMatrix(lngRow, 0, FCConvert.ToString(vo.ID));
				GridVendors.TextMatrix(lngRow, 1, FCConvert.ToString(vo.VendorNumber));
				GridVendors.TextMatrix(lngRow, 2, vo.VendorName);
				GridVendors.TextMatrix(lngRow, 3, vo.Description);
				GridVendors.TextMatrix(lngRow, 4, Strings.Format(vo.Amount, "#,###,###,##0.00"));
				collVendors.MoveNext();
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			CancelSelection();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			SelectCurrent();
		}

		private void CancelSelection()
		{
			intReturn = FCConvert.ToInt32(DialogResult.Cancel);
			Close();
		}

		private void SelectCurrent()
		{
			if (GridVendors.Row > 0)
			{
				intReturn = FCConvert.ToInt32(DialogResult.OK);
				lngChosenID = FCConvert.ToInt32(GridVendors.TextMatrix(GridVendors.Row, 0));
				Close();
			}
		}

		private void frmJournalVendorChoice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmJournalVendorChoice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmJournalVendorChoice.FillStyle	= 0;
			//frmJournalVendorChoice.ScaleWidth	= 5880;
			//frmJournalVendorChoice.ScaleHeight	= 3990;
			//frmJournalVendorChoice.LinkTopic	= "Form2";
			//frmJournalVendorChoice.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void SetupGrid()
		{
			GridVendors.Rows = 1;
			GridVendors.Cols = 5;
			GridVendors.TextMatrix(0, 1, "Vendor");
			GridVendors.TextMatrix(0, 2, "Vendor Name");
			GridVendors.TextMatrix(0, 3, "Description");
			GridVendors.TextMatrix(0, 4, "Amount");
			GridVendors.ColHidden(0, true);
			//FC:FINAL:MSH - issue #1185: added left alignment for data in first column and right alignment for header of the fourth column
			GridVendors.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridVendors.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//FC:FINAL:MSH - issue #1185: restored missing property for resizing last columns and filling empty space(as in original)
			GridVendors.ExtendLastCol = true;
		}

		private void ResizeGrid()
		{
			int lngWidth;
			lngWidth = GridVendors.WidthOriginal;
			GridVendors.ColWidth(1, FCConvert.ToInt32(lngWidth * 0.1));
			GridVendors.ColWidth(2, FCConvert.ToInt32(lngWidth * 0.4));
			GridVendors.ColWidth(3, FCConvert.ToInt32(lngWidth * 0.3));
		}

		private void frmJournalVendorChoice_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void GridVendors_DblClick(object sender, System.EventArgs e)
		{
			if (GridVendors.Rows > 0)
			{
				SelectCurrent();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
