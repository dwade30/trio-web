﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cFundExpRevValue
	{
		//=========================================================
		private double dblFund;
		private double dblExpenses;
		private double dblRevenues;
		private string strFund = string.Empty;

		public double FundAmount
		{
			set
			{
				dblFund = value;
			}
			get
			{
				double FundAmount = 0;
				FundAmount = dblFund;
				return FundAmount;
			}
		}

		public double ExpenseAmount
		{
			set
			{
				dblExpenses = value;
			}
			get
			{
				double ExpenseAmount = 0;
				ExpenseAmount = dblExpenses;
				return ExpenseAmount;
			}
		}

		public double RevenueAmount
		{
			set
			{
				dblRevenues = value;
			}
			get
			{
				double RevenueAmount = 0;
				RevenueAmount = dblRevenues;
				return RevenueAmount;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}
	}
}
