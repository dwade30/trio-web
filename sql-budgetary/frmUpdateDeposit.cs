﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmUpdateDeposit.
	/// </summary>
	public partial class frmUpdateDeposit : BaseForm
	{
		public frmUpdateDeposit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdateDeposit InstancePtr
		{
			get
			{
				return (frmUpdateDeposit)Sys.GetInstance(typeof(frmUpdateDeposit));
			}
		}

		protected frmUpdateDeposit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/1/02
		// This form will be used by towns to add new checks and
		// deposits that have been processed to the check rec file
		// so it can be calculated into the check rec process
		// ********************************************************
		int BankCol;
		int AmountCol;
		int CodeCol;
		int StatusDateCol;
		int PayeeCol;
		int CheckDateCol;
		int KeyCol;
		double[] dblColPercents = new double[7 + 1];
		bool blnMenuFlag;
		bool blnUnload;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cmdProcess_Click()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			int counter2;
			rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster");
			//FC:FINAL:MSH - Issue #740: Values in table start from Row = 1, not from Row = 2
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				for (counter2 = 0; counter2 <= vs1.Cols - 1; counter2++)
				{
					if (vs1.TextMatrix(counter, counter2) == "")
					{
						MessageBox.Show("You are missing information in your record.  Please enter the information or delete the record then try again.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, counter2);
						return;
					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillNewValue(this);
			}
			// Thsi function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Check Rec Update Deposit", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()));
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClassFromGrid();
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ----------------------------------------------------------------
			//FC:FINAL:MSH - Issue #740: Values in table start from Row = 1, not from Row = 2
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = " + vs1.TextMatrix(counter, KeyCol));
				if (rsInfo.EndOfFile() != true)
				{
					rsInfo.Edit();
					rsInfo.Set_Fields("CheckNumber", "0");
					rsInfo.Set_Fields("Type", "3");
					rsInfo.Set_Fields("CheckDate", Strings.Trim(vs1.TextMatrix(counter, CheckDateCol)));
					rsInfo.Set_Fields("Name", Strings.Trim(vs1.TextMatrix(counter, PayeeCol)));
					rsInfo.Set_Fields("Amount", Strings.Trim(vs1.TextMatrix(counter, AmountCol)));
					if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == Strings.Trim(vs1.TextMatrix(counter, CodeCol)))
					{
						// do nothing
					}
					else
					{
						rsInfo.Set_Fields("StatusDate", DateTime.Today);
					}
					//FC:FINAL:MSH - Issue #740 - The 'Status' can be equal { "1", "2", "3", "V", "D" }, so we can simplify this part of code.
					/*
                    if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) < 4)
                    {
                        rsInfo.Set_Fields("Status", Strings.Trim(vs1.TextMatrix(counter, CodeCol)));
                    }
                    else if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) == 4)
                    {
                        rsInfo.Set_Fields("Status", "V");
                        modBudgetaryMaster.WriteAuditRecord_8("Deposit for " + rsInfo.Get_Fields_String("Name") + " on " + rsInfo.Get_Fields_DateTime("CheckDate") + " status set to V", "Update Deposit");
                    }
                    else
                    {
                        rsInfo.Set_Fields("Status", "D");
                        modBudgetaryMaster.WriteAuditRecord_8("Deposit for " + rsInfo.Get_Fields_String("Name") + " on " + rsInfo.Get_Fields_DateTime("CheckDate") + " status set to D", "Update Deposit");
                    }
                    */
					var tempStr = Strings.Trim(vs1.TextMatrix(counter, CodeCol));
					rsInfo.Set_Fields("Status", tempStr);
					if (String.Compare(tempStr, "V") == 0)
					{
						modBudgetaryMaster.WriteAuditRecord_8("Deposit for " + rsInfo.Get_Fields_String("Name") + " on " + rsInfo.Get_Fields_DateTime("CheckDate") + " status set to V", "Update Deposit");
					}
					else
					{
						modBudgetaryMaster.WriteAuditRecord_8("Deposit for " + rsInfo.Get_Fields_String("Name") + " on " + rsInfo.Get_Fields_DateTime("CheckDate") + " status set to D", "Update Deposit");
					}
					rsInfo.Set_Fields("BankNumber", Strings.Trim(vs1.TextMatrix(counter, BankCol)));
					rsInfo.Update(true);
				}
			}
			MessageBox.Show("Save Successful", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				Close();
			}
		}

		private void frmUpdateDeposit_Activated(object sender, System.EventArgs e)
		{
			if (vs1.Rows <= 1)
			{
				MessageBox.Show("No deposits to update", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
				return;
			}
			if (modGlobal.FormExist(this))
			{
				return;
			}
			//FC:FINAL:MSH - Issue #740 - Select column in first row of the table.
			vs1.Select(1, AmountCol);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			blnMenuFlag = false;
			this.Refresh();
		}

		private void frmUpdateDeposit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmUpdateDeposit_Load(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			// define column variables
			KeyCol = 0;
			BankCol = 1;
			AmountCol = 2;
			CodeCol = 3;
			StatusDateCol = 4;
			PayeeCol = 5;
			CheckDateCol = 6;
			vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.TextMatrix(0, BankCol, "Bank");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, CodeCol, "Code");
			vs1.TextMatrix(0, StatusDateCol, "Date");
			vs1.TextMatrix(0, PayeeCol, "Payee");
			vs1.TextMatrix(0, CheckDateCol, "Date");
			// set datatype for date columns
			vs1.ColDataType(AmountCol, FCGrid.DataTypeSettings.flexDTCurrency);
			// set format type for columns
			vs1.ColFormat(AmountCol, "#,###.00");
			// set up editmask for date columns
			vs1.ColEditMask(StatusDateCol, "00/00/00");
			// set up column sizes
			vs1.ColWidth(KeyCol, 0);
			vs1.ColWidth(BankCol, 500);
			vs1.ColWidth(AmountCol, 1200);
			vs1.ColWidth(CodeCol, 600);
			vs1.ColWidth(StatusDateCol, 800);
			vs1.ColWidth(PayeeCol, 3400);
			vs1.ColWidth(CheckDateCol, 800);
			dblColPercents[0] = 0;
			dblColPercents[1] = 0.0556179;
			dblColPercents[2] = 0.1948314;
			dblColPercents[3] = 0.0974157;
			dblColPercents[4] = 0.0893258;
			dblColPercents[5] = 0.3825842;
			dblColPercents[6] = 0.0893258;
			// set up combo lists for type and code columns
			vs1.ColComboList(CodeCol, "#1;1" + "\t" + "Issued|#2;2" + "\t" + "Outstanding|#3;3" + "\t" + "Cashed / Cleared|#4;V" + "\t" + "Voided|#5;D" + "\t" + "Deleted");
			// show a border between the titles and the data input section of the grid
			vs1.Select(1, 0, 1, vs1.Cols - 1);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			HeaderText.Text = App.MainForm.StatusBarText3;
			rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Type = '" + 3 + "' AND BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " ORDER BY CheckNumber, CheckDate");
			counter = 1;
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				vs1.Rows = rsInfo.RecordCount() + 1;
				do
				{
					vs1.TextMatrix(counter, KeyCol, rsInfo.Get_Fields_Int32("ID"));
					vs1.TextMatrix(counter, BankCol, rsInfo.Get_Fields("BankNumber"));
					vs1.TextMatrix(counter, AmountCol, rsInfo.Get_Fields("Amount"));
					vs1.TextMatrix(counter, CodeCol, rsInfo.Get_Fields_String("Status"));
					vs1.TextMatrix(counter, StatusDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("StatusDate"), "MM/dd/yy"));
					vs1.TextMatrix(counter, PayeeCol, rsInfo.Get_Fields_String("Name"));
					vs1.TextMatrix(counter, CheckDateCol, Strings.Format(rsInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yy"));
					counter += 1;
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			else
			{
				vs1.Rows = 1;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			// Dave 12/14/2006---------------------------------------------------
			// This is a function you will need to use in each form if you have a grid to add which grid cells you want to keep track of changes for
			FillControlInformationClassFromGrid();
			// This will initialize the old data so we have somethign to compare the new data against when you save
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			// ---------------------------------------------------------------------
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vs1.ColAllowedKeys(AmountCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
		}

		private void frmUpdateDeposit_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 6; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPercents[counter]));
			}
			//FC:FINAL:AM: set the location of the status label
			vs1HeaderText.Left = vs1.Columns[0].Width + vs1.Columns[1].Width + vs1.Columns[2].Width;
			//FC:FINAL:ASZ - use anchors: 
			//vs1.Height = vs1.RowHeight(0) * 17 + 75;
		}

		private void frmUpdateDeposit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
				// ElseIf KeyAscii = 13 Then
				// KeyAscii = 0
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			//FC:FINAL:DSE #i687 Exception when changing column (validation should not come)
			vs1.EndEdit();
			if (vs1.Col == AmountCol)
			{
				vs1.Col = PayeeCol;
			}
			else
			{
				vs1.Col = AmountCol;
			}
			cmdProcess_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			if (vs1.Col == AmountCol)
			{
				vs1.Col = PayeeCol;
			}
			else
			{
				vs1.Col = AmountCol;
			}
			cmdProcess_Click();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.GetFlexColIndex(e.ColumnIndex) == CheckDateCol)
			{
				vs1.EditMask = "0#/0#/0#";
			}
			else
			{
				vs1.EditMask = "";
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Left)
			{
				if (vs1.Col == PayeeCol)
				{
					if (vs1.EditSelStart == 0)
					{
						KeyCode = 0;
						vs1.Col = CodeCol;
					}
				}
			}
			else if (KeyCode == Keys.Right)
			{
				if (vs1.Col == CodeCol)
				{
					KeyCode = 0;
					vs1.Col = PayeeCol;
				}
			}
			else if (KeyCode == Keys.Return)
			{
				if (vs1.Col == PayeeCol)
				{
					if (Strings.Trim(vs1.EditText) == "")
					{
						if (vs1.Row > vs1.FixedRows)
						{
							vs1.EditText = vs1.TextMatrix(vs1.Row - 1, vs1.Col);
						}
					}
				}
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				Support.SendKeys("{TAB}", false);
				return;
			}
			if (vs1.Col == AmountCol)
			{
				if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 46 && keyAscii != 8 && keyAscii != 27)
				{
					keyAscii = 0;
				}
			}
		}

		private void vs1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				blnMenuFlag = true;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionByRow;
				vs1.Row = vs1.MouseRow;
				//this.PopupMenu(mnuRows, this, e.Location);                
				mnuRows.ShowDropDown(this, e.X, e.Y);
				//Application.DoEvents();
				blnMenuFlag = false;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways && blnMenuFlag == false)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
			if (vs1.Col == StatusDateCol)
			{
				vs1.Col += 1;
			}
			if (vs1.Col == PayeeCol)
			{
				vs1.EditMaxLength = 35;
			}
			else
			{
				vs1.EditMaxLength = 0;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == CheckDateCol)
			{
				if (!Information.IsDate(vs1.EditText))
				{
					vs1.EditText = "";
					vs1.EditMask = "";
					vs1.TextMatrix(row, CheckDateCol, "");
					if (row > vs1.FixedRows)
					{
						vs1.EditText = vs1.TextMatrix(row - 1, col);
					}
					e.Cancel = false;
				}
			}
			else if (col == CodeCol)
			{
				if (vs1.EditText == "")
				{
					return;
				}
			}
			else if (col == AmountCol)
			{
				if (!Information.IsNumeric(vs1.EditText))
				{
					e.Cancel = true;
					return;
				}
			}
		}

		private void SetCustomFormColors()
		{
			//lblBank.ForeColor = Color.Blue;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			int intRows;
			int intCols;
			for (intRows = 1; intRows <= vs1.Rows - 1; intRows++)
			{
				for (intCols = 1; intCols <= vs1.Cols - 1; intCols++)
				{
					if (intCols != 0)
					{
						Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
						//FC:FINAL:ASZ: create instance
						clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
						clsControlInfo[intTotalNumberOfControls].ControlName = "vs1";
						clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
						clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
						clsControlInfo[intTotalNumberOfControls].GridCol = intCols;
						clsControlInfo[intTotalNumberOfControls].DataDescription = "Date: " + vs1.TextMatrix(intRows, CheckDateCol) + "  Payee: " + vs1.TextMatrix(intRows, PayeeCol) + "  Amount: " + vs1.TextMatrix(intRows, AmountCol) + " " + Strings.Trim(vs1.TextMatrix(0, intCols) + " " + vs1.TextMatrix(1, intCols));
						clsControlInfo[intTotalNumberOfControls].UseKeyInsteadOfRowCol = true;
						clsControlInfo[intTotalNumberOfControls].KeyValue = vs1.TextMatrix(intRows, KeyCol).ToIntegerValue();
						clsControlInfo[intTotalNumberOfControls].KeyCol = KeyCol;
						intTotalNumberOfControls += 1;
					}
				}
			}
		}
	}
}
