﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeProjectSummary.
	/// </summary>
	partial class frmCustomizeProjectSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbShowPending;
		public fecherFoundation.FCLabel lblShowPending;
		public fecherFoundation.FCComboBox cmbShowOutstandingEncumbrance;
		public fecherFoundation.FCLabel lblShowOutstanding;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCFrame fraSelectedMonths;
		public fecherFoundation.FCCheckBox chkSelectedNet;
		public fecherFoundation.FCCheckBox chkSelectedDebitsCredits;
		public fecherFoundation.FCFrame fraYTD;
		public fecherFoundation.FCCheckBox chkYTDNet;
		public fecherFoundation.FCCheckBox chkYTDDebitsCredits;
		public fecherFoundation.FCLabel lblNumber;
		public fecherFoundation.FCLabel lblCount;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCButton cmdProcessSave;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeProjectSummary));
            this.cmbShowPending = new fecherFoundation.FCComboBox();
            this.lblShowPending = new fecherFoundation.FCLabel();
            this.cmbShowOutstandingEncumbrance = new fecherFoundation.FCComboBox();
            this.lblShowOutstanding = new fecherFoundation.FCLabel();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.fraSelectedMonths = new fecherFoundation.FCFrame();
            this.chkSelectedNet = new fecherFoundation.FCCheckBox();
            this.chkSelectedDebitsCredits = new fecherFoundation.FCCheckBox();
            this.fraYTD = new fecherFoundation.FCFrame();
            this.chkYTDNet = new fecherFoundation.FCCheckBox();
            this.chkYTDDebitsCredits = new fecherFoundation.FCCheckBox();
            this.lblNumber = new fecherFoundation.FCLabel();
            this.lblCount = new fecherFoundation.FCLabel();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).BeginInit();
            this.fraSelectedMonths.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).BeginInit();
            this.fraYTD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(366, 30);
            this.HeaderText.Text = "Project Summary Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbShowPending
            // 
            this.cmbShowPending.Items.AddRange(new object[] {
            "Show Pending Activity Separately",
            "Include Pending Activity in YTD Account Information",
            "Do Not Include Pending Activity"});
            this.cmbShowPending.Location = new System.Drawing.Point(271, 153);
            this.cmbShowPending.Name = "cmbShowPending";
            this.cmbShowPending.Size = new System.Drawing.Size(451, 40);
            this.cmbShowPending.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.cmbShowPending, null);
            this.cmbShowPending.SelectedIndexChanged += new System.EventHandler(this.cmbShowPending_CheckedChanged);
            // 
            // lblShowPending
            // 
            this.lblShowPending.AutoSize = true;
            this.lblShowPending.Location = new System.Drawing.Point(20, 167);
            this.lblShowPending.Name = "lblShowPending";
            this.lblShowPending.Size = new System.Drawing.Size(133, 16);
            this.lblShowPending.TabIndex = 20;
            this.lblShowPending.Text = "PENDING ACTIVITY";
            this.lblShowPending.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblShowPending, null);
            // 
            // cmbShowOutstandingEncumbrance
            // 
            this.cmbShowOutstandingEncumbrance.Items.AddRange(new object[] {
            "Show Encumbrances Separately",
            "Include Encumbrances in Regular Account Information"});
            this.cmbShowOutstandingEncumbrance.Location = new System.Drawing.Point(271, 213);
            this.cmbShowOutstandingEncumbrance.Name = "cmbShowOutstandingEncumbrance";
            this.cmbShowOutstandingEncumbrance.Size = new System.Drawing.Size(451, 40);
            this.cmbShowOutstandingEncumbrance.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.cmbShowOutstandingEncumbrance, null);
            this.cmbShowOutstandingEncumbrance.SelectedIndexChanged += new System.EventHandler(this.cmbShowOutstanding_CheckedChanged);
            // 
            // lblShowOutstanding
            // 
            this.lblShowOutstanding.AutoSize = true;
            this.lblShowOutstanding.Location = new System.Drawing.Point(20, 232);
            this.lblShowOutstanding.Name = "lblShowOutstanding";
            this.lblShowOutstanding.Size = new System.Drawing.Size(223, 16);
            this.lblShowOutstanding.TabIndex = 22;
            this.lblShowOutstanding.Text = "OUTSTANDING ENCUMBRANCES";
            this.lblShowOutstanding.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblShowOutstanding, null);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // fraInformation
            // 
            this.fraInformation.AppearanceKey = "groupBoxLeftBorder";
            this.fraInformation.Controls.Add(this.fraSelectedMonths);
            this.fraInformation.Controls.Add(this.cmbShowPending);
            this.fraInformation.Controls.Add(this.lblShowPending);
            this.fraInformation.Controls.Add(this.cmbShowOutstandingEncumbrance);
            this.fraInformation.Controls.Add(this.lblShowOutstanding);
            this.fraInformation.Controls.Add(this.fraYTD);
            this.fraInformation.Controls.Add(this.lblNumber);
            this.fraInformation.Controls.Add(this.lblCount);
            this.fraInformation.Location = new System.Drawing.Point(30, 76);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(742, 273);
            this.fraInformation.TabIndex = 14;
            this.fraInformation.Text = "Information To Be Reported";
            this.ToolTip1.SetToolTip(this.fraInformation, null);
            // 
            // fraSelectedMonths
            // 
            this.fraSelectedMonths.Controls.Add(this.chkSelectedNet);
            this.fraSelectedMonths.Controls.Add(this.chkSelectedDebitsCredits);
            this.fraSelectedMonths.Location = new System.Drawing.Point(386, 30);
            this.fraSelectedMonths.Name = "fraSelectedMonths";
            this.fraSelectedMonths.Size = new System.Drawing.Size(336, 103);
            this.fraSelectedMonths.TabIndex = 18;
            this.fraSelectedMonths.FormatCaption = false;
            this.fraSelectedMonths.Text = "Selected Month(s) Account Information";
            this.ToolTip1.SetToolTip(this.fraSelectedMonths, null);
            // 
            // chkSelectedNet
            // 
            this.chkSelectedNet.Location = new System.Drawing.Point(20, 65);
            this.chkSelectedNet.Name = "chkSelectedNet";
            this.chkSelectedNet.Size = new System.Drawing.Size(80, 23);
            this.chkSelectedNet.TabIndex = 7;
            this.chkSelectedNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkSelectedNet, null);
            this.chkSelectedNet.CheckedChanged += new System.EventHandler(this.chkSelectedNet_CheckedChanged);
            // 
            // chkSelectedDebitsCredits
            // 
            this.chkSelectedDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkSelectedDebitsCredits.Name = "chkSelectedDebitsCredits";
            this.chkSelectedDebitsCredits.Size = new System.Drawing.Size(117, 23);
            this.chkSelectedDebitsCredits.TabIndex = 6;
            this.chkSelectedDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkSelectedDebitsCredits, "You need to have 2 items available to show this item");
            this.chkSelectedDebitsCredits.CheckedChanged += new System.EventHandler(this.chkSelectedDebitsCredits_CheckedChanged);
            // 
            // fraYTD
            // 
            this.fraYTD.Controls.Add(this.chkYTDNet);
            this.fraYTD.Controls.Add(this.chkYTDDebitsCredits);
            this.fraYTD.Location = new System.Drawing.Point(145, 30);
            this.fraYTD.Name = "fraYTD";
            this.fraYTD.Size = new System.Drawing.Size(221, 103);
            this.fraYTD.TabIndex = 17;
            this.fraYTD.FormatCaption = false;
            this.fraYTD.Text = "YTD Account Information";
            this.ToolTip1.SetToolTip(this.fraYTD, null);
            // 
            // chkYTDNet
            // 
            this.chkYTDNet.Location = new System.Drawing.Point(20, 65);
            this.chkYTDNet.Name = "chkYTDNet";
            this.chkYTDNet.Size = new System.Drawing.Size(80, 23);
            this.chkYTDNet.TabIndex = 5;
            this.chkYTDNet.Text = "Net Total";
            this.ToolTip1.SetToolTip(this.chkYTDNet, null);
            this.chkYTDNet.CheckedChanged += new System.EventHandler(this.chkYTDNet_CheckedChanged);
            // 
            // chkYTDDebitsCredits
            // 
            this.chkYTDDebitsCredits.Location = new System.Drawing.Point(20, 30);
            this.chkYTDDebitsCredits.Name = "chkYTDDebitsCredits";
            this.chkYTDDebitsCredits.Size = new System.Drawing.Size(117, 23);
            this.chkYTDDebitsCredits.TabIndex = 4;
            this.chkYTDDebitsCredits.Text = "Debits / Credits";
            this.ToolTip1.SetToolTip(this.chkYTDDebitsCredits, "You need to have 2 items available to show this item");
            this.chkYTDDebitsCredits.CheckedChanged += new System.EventHandler(this.chkYTDDebitsCredits_CheckedChanged);
            // 
            // lblNumber
            // 
            this.lblNumber.BorderStyle = 1;
            this.lblNumber.Location = new System.Drawing.Point(20, 94);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(46, 22);
            this.lblNumber.TabIndex = 21;
            this.lblNumber.Text = "8";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblNumber, null);
            // 
            // lblCount
            // 
            this.lblCount.Location = new System.Drawing.Point(20, 30);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(116, 58);
            this.lblCount.TabIndex = 20;
            this.lblCount.Text = "NUMBER OF ITEMS YOU MAY ADD TO THIS REPORT";
            this.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblCount, null);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(198, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(201, 40);
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(121, 16);
            this.lblDescription.TabIndex = 13;
            this.lblDescription.Text = "REPORT FORMAT";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(274, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(122, 48);
            this.cmdProcessSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdProcessSave, null);
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmCustomizeProjectSummary
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCustomizeProjectSummary";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Project Summary Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeProjectSummary_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeProjectSummary_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeProjectSummary_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeProjectSummary_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            this.fraInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedMonths)).EndInit();
            this.fraSelectedMonths.ResumeLayout(false);
            this.fraSelectedMonths.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectedDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraYTD)).EndInit();
            this.fraYTD.ResumeLayout(false);
            this.fraYTD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYTDDebitsCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
