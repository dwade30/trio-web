﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerDetailSetup.
	/// </summary>
	partial class frmLedgerDetailSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPostedDate;
		public fecherFoundation.FCLabel lblPostedDate;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTo;
		public fecherFoundation.FCCheckBox chkShowZeroBalanceAccounts;
		public fecherFoundation.FCCheckBox chkMonthlySubtotals;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCFrame fraMonths;
		public fecherFoundation.FCCheckBox chkCheckDateRange;
		public fecherFoundation.FCComboBox cboEndingMonth;
		public fecherFoundation.FCComboBox cboBeginningMonth;
		public fecherFoundation.FCComboBox cboSingleMonth;
		public fecherFoundation.FCLabel lblTo_0;
		public fecherFoundation.FCFrame fraFundRange;
		public fecherFoundation.FCComboBox cboBeginningFund;
		public fecherFoundation.FCComboBox cboEndingFund;
		public fecherFoundation.FCComboBox cboSingleFund;
		public fecherFoundation.FCCheckBox chkCheckAccountRange;
		public FCGrid vsLowAccount;
		public FCGrid vsHighAccount;
		public fecherFoundation.FCLabel lblTo_1;
		public fecherFoundation.FCLabel lblDescription;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLedgerDetailSetup));
			this.cmbPostedDate = new fecherFoundation.FCComboBox();
			this.lblPostedDate = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.cmbAllAccounts = new fecherFoundation.FCComboBox();
			this.chkShowZeroBalanceAccounts = new fecherFoundation.FCCheckBox();
			this.chkMonthlySubtotals = new fecherFoundation.FCCheckBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.fraMonths = new fecherFoundation.FCFrame();
			this.chkCheckDateRange = new fecherFoundation.FCCheckBox();
			this.cboEndingMonth = new fecherFoundation.FCComboBox();
			this.cboBeginningMonth = new fecherFoundation.FCComboBox();
			this.cboSingleMonth = new fecherFoundation.FCComboBox();
			this.lblTo_0 = new fecherFoundation.FCLabel();
			this.fraFundRange = new fecherFoundation.FCFrame();
			this.cboBeginningFund = new fecherFoundation.FCComboBox();
			this.cboEndingFund = new fecherFoundation.FCComboBox();
			this.cboSingleFund = new fecherFoundation.FCComboBox();
			this.chkCheckAccountRange = new fecherFoundation.FCCheckBox();
			this.vsLowAccount = new fecherFoundation.FCGrid();
			this.vsHighAccount = new fecherFoundation.FCGrid();
			this.lblTo_1 = new fecherFoundation.FCLabel();
			this.lblTo_2 = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdCancelPrint = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonthlySubtotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).BeginInit();
			this.fraMonths.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFundRange)).BeginInit();
			this.fraFundRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancelPrint);
			this.ClientArea.Controls.Add(this.chkShowZeroBalanceAccounts);
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.chkMonthlySubtotals);
			this.ClientArea.Controls.Add(this.cmbPostedDate);
			this.ClientArea.Controls.Add(this.lblPostedDate);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.fraMonths);
			this.ClientArea.Controls.Add(this.fraFundRange);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(330, 30);
			this.HeaderText.Text = "Ledger Detail Search Criteria";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbPostedDate
			// 
			this.cmbPostedDate.AutoSize = false;
			this.cmbPostedDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPostedDate.FormattingEnabled = true;
			this.cmbPostedDate.Items.AddRange(new object[] {
			"Journal Number",
			"Posted Date"});
			this.cmbPostedDate.Location = new System.Drawing.Point(215, 320);
			this.cmbPostedDate.Name = "cmbPostedDate";
			this.cmbPostedDate.Size = new System.Drawing.Size(195, 40);
			this.cmbPostedDate.TabIndex = 30;
			this.ToolTip1.SetToolTip(this.cmbPostedDate, null);
			// 
			// lblPostedDate
			// 
			this.lblPostedDate.AutoSize = true;
			this.lblPostedDate.Location = new System.Drawing.Point(30, 334);
			this.lblPostedDate.Name = "lblPostedDate";
			this.lblPostedDate.Size = new System.Drawing.Size(136, 15);
			this.lblPostedDate.TabIndex = 31;
			this.lblPostedDate.Text = "DETAIL SORT ORDER";
			this.ToolTip1.SetToolTip(this.lblPostedDate, null);
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Single Month",
				"Range of Months"
			});
			this.cmbRange.Location = new System.Drawing.Point(20, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(360, 40);
			this.cmbRange.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.cmbRange, null);
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_CheckedChanged);
			// 
			// cmbAllAccounts
			// 
			this.cmbAllAccounts.AutoSize = false;
			this.cmbAllAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllAccounts.FormattingEnabled = true;
			this.cmbAllAccounts.Items.AddRange(new object[] {
				"All",
				"Account Range",
				"Single Fund",
				"Fund Range"
			});
			this.cmbAllAccounts.Location = new System.Drawing.Point(20, 30);
			this.cmbAllAccounts.Name = "cmbAllAccounts";
			this.cmbAllAccounts.Size = new System.Drawing.Size(360, 40);
			this.cmbAllAccounts.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.cmbAllAccounts, null);
			this.cmbAllAccounts.SelectedIndexChanged += new System.EventHandler(this.cmbAllAccounts_CheckedChanged);
			// 
			// chkShowZeroBalanceAccounts
			// 
			this.chkShowZeroBalanceAccounts.Location = new System.Drawing.Point(30, 416);
			this.chkShowZeroBalanceAccounts.Name = "chkShowZeroBalanceAccounts";
			this.chkShowZeroBalanceAccounts.Size = new System.Drawing.Size(243, 26);
			this.chkShowZeroBalanceAccounts.TabIndex = 29;
			this.chkShowZeroBalanceAccounts.Text = "Show Zero Balance Accounts";
			this.ToolTip1.SetToolTip(this.chkShowZeroBalanceAccounts, "This setting shows specific fields - not all formatting options will be available" + ".");
			// 
			// chkMonthlySubtotals
			// 
			this.chkMonthlySubtotals.Location = new System.Drawing.Point(30, 380);
			this.chkMonthlySubtotals.Name = "chkMonthlySubtotals";
			this.chkMonthlySubtotals.Size = new System.Drawing.Size(204, 26);
			this.chkMonthlySubtotals.TabIndex = 28;
			this.chkMonthlySubtotals.Text = "Show Monthly Subtotals";
			this.ToolTip1.SetToolTip(this.chkMonthlySubtotals, null);
			// 
			// txtDescription
			// 
			this.txtDescription.MaxLength = 25;
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(215, 30);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(201, 40);
			this.txtDescription.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			// 
			// fraMonths
			// 
			this.fraMonths.BackColor = System.Drawing.SystemColors.Menu;
			this.fraMonths.Controls.Add(this.chkCheckDateRange);
			this.fraMonths.Controls.Add(this.cmbRange);
			this.fraMonths.Controls.Add(this.cboEndingMonth);
			this.fraMonths.Controls.Add(this.cboBeginningMonth);
			this.fraMonths.Controls.Add(this.cboSingleMonth);
			this.fraMonths.Controls.Add(this.lblTo_0);
			this.fraMonths.Location = new System.Drawing.Point(30, 100);
			this.fraMonths.Name = "fraMonths";
			this.fraMonths.Size = new System.Drawing.Size(400, 196);
			this.fraMonths.TabIndex = 20;
            this.fraMonths.FormatCaption = false;
            this.fraMonths.Text = "Month(s) To Report";
			this.ToolTip1.SetToolTip(this.fraMonths, null);
			// 
			// chkCheckDateRange
			// 
			this.chkCheckDateRange.BackColor = System.Drawing.SystemColors.Menu;
			this.chkCheckDateRange.Location = new System.Drawing.Point(20, 90);
			this.chkCheckDateRange.Name = "chkCheckDateRange";
			this.chkCheckDateRange.Size = new System.Drawing.Size(174, 26);
			this.chkCheckDateRange.TabIndex = 4;
			this.chkCheckDateRange.Text = "Select at report time";
			this.ToolTip1.SetToolTip(this.chkCheckDateRange, null);
			this.chkCheckDateRange.Visible = false;
			this.chkCheckDateRange.CheckedChanged += new System.EventHandler(this.chkCheckDateRange_CheckedChanged);
			// 
			// cboEndingMonth
			// 
			this.cboEndingMonth.AutoSize = false;
			this.cboEndingMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingMonth.FormattingEnabled = true;
			this.cboEndingMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboEndingMonth.Location = new System.Drawing.Point(230, 136);
			this.cboEndingMonth.Name = "cboEndingMonth";
			this.cboEndingMonth.Size = new System.Drawing.Size(150, 40);
			this.cboEndingMonth.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.cboEndingMonth, null);
			this.cboEndingMonth.Visible = false;
			// 
			// cboBeginningMonth
			// 
			this.cboBeginningMonth.AutoSize = false;
			this.cboBeginningMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningMonth.FormattingEnabled = true;
			this.cboBeginningMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboBeginningMonth.Location = new System.Drawing.Point(20, 136);
			this.cboBeginningMonth.Name = "cboBeginningMonth";
			this.cboBeginningMonth.Size = new System.Drawing.Size(150, 40);
			this.cboBeginningMonth.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.cboBeginningMonth, null);
			this.cboBeginningMonth.Visible = false;
			// 
			// cboSingleMonth
			// 
			this.cboSingleMonth.AutoSize = false;
			this.cboSingleMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleMonth.FormattingEnabled = true;
			this.cboSingleMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboSingleMonth.Location = new System.Drawing.Point(20, 136);
			this.cboSingleMonth.Name = "cboSingleMonth";
			this.cboSingleMonth.Size = new System.Drawing.Size(150, 40);
			this.cboSingleMonth.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.cboSingleMonth, null);
			this.cboSingleMonth.Visible = false;
			// 
			// lblTo_0
			// 
			this.lblTo_0.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_0.Location = new System.Drawing.Point(190, 150);
			this.lblTo_0.Name = "lblTo_0";
			this.lblTo_0.Size = new System.Drawing.Size(21, 19);
			this.lblTo_0.TabIndex = 21;
			this.lblTo_0.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_0, null);
			this.lblTo_0.Visible = false;
			// 
			// fraFundRange
			// 
			this.fraFundRange.BackColor = System.Drawing.SystemColors.Menu;
			this.fraFundRange.Controls.Add(this.cboBeginningFund);
			this.fraFundRange.Controls.Add(this.cmbAllAccounts);
			this.fraFundRange.Controls.Add(this.cboEndingFund);
			this.fraFundRange.Controls.Add(this.cboSingleFund);
			this.fraFundRange.Controls.Add(this.chkCheckAccountRange);
			this.fraFundRange.Controls.Add(this.vsLowAccount);
			this.fraFundRange.Controls.Add(this.vsHighAccount);
			this.fraFundRange.Controls.Add(this.lblTo_1);
			this.fraFundRange.Controls.Add(this.lblTo_2);
			this.fraFundRange.Location = new System.Drawing.Point(460, 100);
			this.fraFundRange.Name = "fraFundRange";
			this.fraFundRange.Size = new System.Drawing.Size(470, 196);
			this.fraFundRange.TabIndex = 18;
			this.fraFundRange.Text = "Accounts To Be Reported";
			this.ToolTip1.SetToolTip(this.fraFundRange, null);
			// 
			// cboBeginningFund
			// 
			this.cboBeginningFund.AutoSize = false;
			this.cboBeginningFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboBeginningFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboBeginningFund.FormattingEnabled = true;
			this.cboBeginningFund.Location = new System.Drawing.Point(20, 136);
			this.cboBeginningFund.Name = "cboBeginningFund";
			this.cboBeginningFund.Size = new System.Drawing.Size(190, 40);
			this.cboBeginningFund.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.cboBeginningFund, null);
			this.cboBeginningFund.Visible = false;
			this.cboBeginningFund.DropDown += new System.EventHandler(this.cboBeginningFund_DropDown);
			// 
			// cboEndingFund
			// 
			this.cboEndingFund.AutoSize = false;
			this.cboEndingFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboEndingFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEndingFund.FormattingEnabled = true;
			this.cboEndingFund.Location = new System.Drawing.Point(260, 136);
			this.cboEndingFund.Name = "cboEndingFund";
			this.cboEndingFund.Size = new System.Drawing.Size(190, 40);
			this.cboEndingFund.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.cboEndingFund, null);
			this.cboEndingFund.Visible = false;
			this.cboEndingFund.DropDown += new System.EventHandler(this.cboEndingFund_DropDown);
			// 
			// cboSingleFund
			// 
			this.cboSingleFund.AutoSize = false;
			this.cboSingleFund.BackColor = System.Drawing.SystemColors.Window;
			this.cboSingleFund.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboSingleFund.FormattingEnabled = true;
			this.cboSingleFund.Location = new System.Drawing.Point(20, 136);
			this.cboSingleFund.Name = "cboSingleFund";
			this.cboSingleFund.Size = new System.Drawing.Size(190, 40);
			this.cboSingleFund.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.cboSingleFund, null);
			this.cboSingleFund.Visible = false;
			this.cboSingleFund.DropDown += new System.EventHandler(this.cboSingleFund_DropDown);
			// 
			// chkCheckAccountRange
			// 
			this.chkCheckAccountRange.BackColor = System.Drawing.SystemColors.Menu;
			this.chkCheckAccountRange.Location = new System.Drawing.Point(20, 80);
			this.chkCheckAccountRange.Name = "chkCheckAccountRange";
			this.chkCheckAccountRange.Size = new System.Drawing.Size(174, 26);
			this.chkCheckAccountRange.TabIndex = 12;
			this.chkCheckAccountRange.Text = "Select at report time";
			this.ToolTip1.SetToolTip(this.chkCheckAccountRange, null);
			this.chkCheckAccountRange.Visible = false;
			this.chkCheckAccountRange.CheckedChanged += new System.EventHandler(this.chkCheckAccountRange_CheckedChanged);
			// 
			// vsLowAccount
			// 
			this.vsLowAccount.AllowSelection = false;
			this.vsLowAccount.AllowUserToResizeColumns = false;
			this.vsLowAccount.AllowUserToResizeRows = false;
			this.vsLowAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLowAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsLowAccount.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLowAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsLowAccount.ColumnHeadersHeight = 30;
			this.vsLowAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsLowAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLowAccount.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsLowAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLowAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLowAccount.ExtendLastCol = true;
			this.vsLowAccount.FixedCols = 0;
			this.vsLowAccount.FixedRows = 0;
			this.vsLowAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLowAccount.FrozenCols = 0;
			this.vsLowAccount.GridColor = System.Drawing.Color.Empty;
			this.vsLowAccount.Location = new System.Drawing.Point(20, 135);
			this.vsLowAccount.Name = "vsLowAccount";
			this.vsLowAccount.RowHeadersVisible = false;
			this.vsLowAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLowAccount.RowHeightMin = 0;
			this.vsLowAccount.Rows = 1;
			this.vsLowAccount.ShowColumnVisibilityMenu = false;
			this.vsLowAccount.Size = new System.Drawing.Size(120, 42);
			this.vsLowAccount.StandardTab = true;
			this.vsLowAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLowAccount.TabIndex = 23;
			this.ToolTip1.SetToolTip(this.vsLowAccount, null);
			this.vsLowAccount.Visible = false;
			// 
			// vsHighAccount
			// 
			this.vsHighAccount.AllowSelection = false;
			this.vsHighAccount.AllowUserToResizeColumns = false;
			this.vsHighAccount.AllowUserToResizeRows = false;
			this.vsHighAccount.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsHighAccount.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorBkg = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.BackColorSel = System.Drawing.Color.Empty;
			this.vsHighAccount.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsHighAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsHighAccount.ColumnHeadersHeight = 30;
			this.vsHighAccount.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsHighAccount.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsHighAccount.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsHighAccount.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsHighAccount.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsHighAccount.ExtendLastCol = true;
			this.vsHighAccount.FixedCols = 0;
			this.vsHighAccount.FixedRows = 0;
			this.vsHighAccount.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsHighAccount.FrozenCols = 0;
			this.vsHighAccount.GridColor = System.Drawing.Color.Empty;
			this.vsHighAccount.Location = new System.Drawing.Point(200, 135);
			this.vsHighAccount.Name = "vsHighAccount";
			this.vsHighAccount.RowHeadersVisible = false;
			this.vsHighAccount.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsHighAccount.RowHeightMin = 0;
			this.vsHighAccount.Rows = 1;
			this.vsHighAccount.ShowColumnVisibilityMenu = false;
			this.vsHighAccount.Size = new System.Drawing.Size(120, 42);
			this.vsHighAccount.StandardTab = true;
			this.vsHighAccount.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsHighAccount.TabIndex = 24;
			this.ToolTip1.SetToolTip(this.vsHighAccount, null);
			this.vsHighAccount.Visible = false;
			// 
			// lblTo_1
			// 
			this.lblTo_1.BackColor = System.Drawing.Color.Transparent;
			this.lblTo_1.Location = new System.Drawing.Point(160, 150);
			this.lblTo_1.Name = "lblTo_1";
			this.lblTo_1.Size = new System.Drawing.Size(21, 19);
			this.lblTo_1.TabIndex = 19;
			this.lblTo_1.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_1, null);
			this.lblTo_1.Visible = false;
			// 
			// lblTo_2
			// 
			this.lblTo_2.BackColor = System.Drawing.SystemColors.Menu;
			this.lblTo_2.Location = new System.Drawing.Point(227, 150);
			this.lblTo_2.Name = "lblTo_2";
			this.lblTo_2.Size = new System.Drawing.Size(21, 19);
			this.lblTo_2.TabIndex = 25;
			this.lblTo_2.Text = "TO";
			this.ToolTip1.SetToolTip(this.lblTo_2, null);
			this.lblTo_2.Visible = false;
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 44);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(124, 17);
			this.lblDescription.TabIndex = 22;
			this.lblDescription.Text = "SELECTION CRITERIA";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// cmdCancelPrint
			// 
			this.cmdCancelPrint.AppearanceKey = "actionButton";
			this.cmdCancelPrint.Location = new System.Drawing.Point(195, 472);
			this.cmdCancelPrint.Name = "cmdCancelPrint";
			this.cmdCancelPrint.Size = new System.Drawing.Size(100, 48);
			this.cmdCancelPrint.TabIndex = 19;
			this.cmdCancelPrint.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancelPrint, null);
			this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 472);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(135, 48);
			this.cmdSave.TabIndex = 18;
			this.cmdSave.Text = "Save Criteria";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmLedgerDetailSetup
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLedgerDetailSetup";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Ledger Detail Search Criteria";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmLedgerDetailSetup_Load);
			this.Activated += new System.EventHandler(this.frmLedgerDetailSetup_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLedgerDetailSetup_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLedgerDetailSetup_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowZeroBalanceAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMonthlySubtotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMonths)).EndInit();
			this.fraMonths.ResumeLayout(false);
			this.fraMonths.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFundRange)).EndInit();
			this.fraFundRange.ResumeLayout(false);
			this.fraFundRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCheckAccountRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsLowAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsHighAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdCancelPrint;
		public FCButton cmdSave;
		public FCLabel lblTo_2;
	}
}
