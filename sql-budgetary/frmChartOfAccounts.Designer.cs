﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChartOfAccounts.
	/// </summary>
	partial class frmChartOfAccounts : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDepartment;
		public fecherFoundation.FCLabel lblDepartment;
		public fecherFoundation.FCCheckBox chkRegionalOption;
		public fecherFoundation.FCButton cmdProcess;
		public fecherFoundation.FCButton cmdFilePrint;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChartOfAccounts));
			this.cmbDepartment = new fecherFoundation.FCComboBox();
			this.lblDepartment = new fecherFoundation.FCLabel();
			this.chkRegionalOption = new fecherFoundation.FCCheckBox();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRegionalOption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 384);
			this.BottomPanel.Size = new System.Drawing.Size(564, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcess);
			this.ClientArea.Controls.Add(this.cmbDepartment);
			this.ClientArea.Controls.Add(this.lblDepartment);
			this.ClientArea.Controls.Add(this.chkRegionalOption);
			this.ClientArea.Size = new System.Drawing.Size(564, 324);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(564, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(238, 30);
			this.HeaderText.Text = "Select Account Type";
			// 
			// cmbDepartment
			// 
			this.cmbDepartment.AutoSize = false;
			this.cmbDepartment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDepartment.FormattingEnabled = true;
			this.cmbDepartment.Items.AddRange(new object[] {
				"Department",
				"Expense",
				"General Ledger",
				"Revenue"
			});
			this.cmbDepartment.Location = new System.Drawing.Point(190, 30);
			this.cmbDepartment.Name = "cmbDepartment";
			this.cmbDepartment.Size = new System.Drawing.Size(166, 40);
			this.cmbDepartment.TabIndex = 0;
			this.cmbDepartment.Text = "Department";
			this.cmbDepartment.SelectedIndexChanged += new System.EventHandler(this.optDepartment_CheckedChanged);
			// 
			// lblDepartment
			// 
			this.lblDepartment.AutoSize = true;
			this.lblDepartment.Location = new System.Drawing.Point(30, 44);
			this.lblDepartment.Name = "lblDepartment";
			this.lblDepartment.Size = new System.Drawing.Size(104, 15);
			this.lblDepartment.TabIndex = 1;
			this.lblDepartment.Text = "ACCOUNT TYPE";
			this.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkRegionalOption
			// 
			this.chkRegionalOption.Location = new System.Drawing.Point(30, 87);
			this.chkRegionalOption.Name = "chkRegionalOption";
			this.chkRegionalOption.Size = new System.Drawing.Size(189, 27);
			this.chkRegionalOption.TabIndex = 2;
			this.chkRegionalOption.Text = "Show 1XX Depts Only";
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(30, 136);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(157, 48);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Print / Preview";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(490, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(44, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmChartOfAccounts
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(564, 492);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChartOfAccounts";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Select Account Type";
			this.Load += new System.EventHandler(this.frmChartOfAccounts_Load);
			this.Activated += new System.EventHandler(this.frmChartOfAccounts_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChartOfAccounts_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkRegionalOption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
