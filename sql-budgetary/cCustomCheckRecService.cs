﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	public class cCustomCheckRecService
	{
		//=========================================================
		private struct CheckRecInfo
		{
			// vbPorter upgrade warning: CheckNumber As int	OnWrite(string)
			public int CheckNumber;
			// vbPorter upgrade warning: CheckDate As DateTime	OnWrite(string)
			public DateTime CheckDate;
			public string CheckName;
			// vbPorter upgrade warning: CheckAmount As Decimal	OnWrite(string)
			public Decimal CheckAmount;
			public bool DirectDeposit;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CheckRecInfo(int unusedParam)
			{
				this.CheckNumber = 0;
				this.CheckDate = DateTime.FromOADate(0);
				this.CheckAmount = 0;
				this.CheckName = string.Empty;
				this.DirectDeposit = false;
			}
		};

		private string strLastError = string.Empty;
		private int lngLastError;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cCheckRecMasterController checkrecCont = new cCheckRecMasterController();
		private cCheckRecMasterController checkrecCont_AutoInitialized;

		private cCheckRecMasterController checkrecCont
		{
			get
			{
				if (checkrecCont_AutoInitialized == null)
				{
					checkrecCont_AutoInitialized = new cCheckRecMasterController();
				}
				return checkrecCont_AutoInitialized;
			}
			set
			{
				checkrecCont_AutoInitialized = value;
			}
		}

		public void SomersetCustomCheckRecAP()
		{
			StreamReader tsInfo = null;
			string strCurDir;
			string strFilePath = "";
			bool boolFileOpen = false;
			string strFileInfo = "";
			int lngCheckNumber = 0;
			int lngBankID;
			cGenericCollection listChecks = new cGenericCollection();
			cGenericCollection listProcessedChecks = new cGenericCollection();
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				FCCommonDialog ofd = new FCCommonDialog();
				string strFileName;
				//FC:TODO:PJ File-Handling (Options)
				//ofd.Options = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir;
				ofd.Filter = "*.csv";
				ofd.FileName = "*.csv";
				if (ofd.ShowOpen())
				{
					strFilePath = ofd.FileName;
				}
				else
				{
					return;
				}
				ofd = null;
				// lngBankID = Val(GetBankVariable("APBank"))
				frmChooseBDBank getBankForm = new frmChooseBDBank();
				lngBankID = getBankForm.Init(FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank")))));
				if (lngBankID < 1)
				{
					MessageBox.Show("No bank selected", "No Bank Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				strFileName = Path.GetFileName(strFilePath);
				tsInfo = File.OpenText(strFilePath);
				boolFileOpen = true;
				while (tsInfo.EndOfStream == false)
				{
					//Application.DoEvents();
					strFileInfo = tsInfo.ReadLine();
					lngCheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(strFileInfo)));
					if (lngCheckNumber > 0)
					{
						listChecks.AddItem(lngCheckNumber);
					}
				}
				boolFileOpen = false;
				tsInfo.Close();
				listChecks.MoveFirst();
				cCheckRecMaster crmaster;
				while (listChecks.IsCurrent())
				{
					//Application.DoEvents();
					lngCheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(listChecks.GetCurrentItem())));
					checkrecCont.SetCheckToCashed(lngCheckNumber, FCConvert.ToString(1), lngBankID);
					crmaster = checkrecCont.GetCheckRecMasterByCheckAndBank(lngCheckNumber, lngBankID);
					if (!(crmaster == null))
					{
						listProcessedChecks.AddItem(crmaster);
					}
					listChecks.MoveNext();
				}
				if (listProcessedChecks.ItemCount() > 0)
				{
					rptCheckRecList.InstancePtr.Init(ref listProcessedChecks, "Imported Check List File " + strFileName);
				}
				else
				{
					MessageBox.Show("No checks processed", "No Checks", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
				if (boolFileOpen)
				{
					tsInfo.Close();
				}
			}
		}

		private void SetError(int lngError, string strErrorMessage)
		{
			lngLastError = lngError;
			strLastError = strErrorMessage;
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public void OrringtonCustomCheckRecAP()
		{
			string strCurDir;
			string strFilePath = "";
			string[] strInfo = null;
			// vbPorter upgrade warning: strCheckInfo As string()	OnRead(int, DateTime)
			string[] strCheckInfo = null;
			StreamReader tsInfo;
			string strFileInfo = "";
			int counter;
			int counter2;
			int counter3;
			int intFirstQuote = 0;
			int intSecondQuote = 0;
			CheckRecInfo[] criCheckInfo = null;
			int lngRecordCounter;
			int intBank;
			clsDRWrapper rsCheckRecInfo = new clsDRWrapper();
			string strTemp = "";
			strCurDir = FCFileSystem.Statics.UserDataFolder;
			Information.Err().Clear();
			FCCommonDialog ofd = new FCCommonDialog();
			//FC:TODO:PJ File-Handling (Options)
			//ofd.Options = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir;
			ofd.Filter = "*.csv";
			ofd.FileName = "*.csv";
			if (ofd.ShowOpen())
			{
				strFilePath = ofd.FileName;
			}
			else
			{
				return;
			}
			tsInfo = File.OpenText(strFilePath);
			lngRecordCounter = 0;
			while (tsInfo.EndOfStream == false)
			{
				//Application.DoEvents();
				strFileInfo = tsInfo.ReadLine();
				while (Strings.Left(strFileInfo, 1) != ",")
				{
					strFileInfo = Strings.Right(strFileInfo, strFileInfo.Length - 1);
					if (strFileInfo == "")
					{
						goto GetPastCheck;
					}
				}
				while (Strings.Left(strFileInfo, 1) == ",")
				{
					strFileInfo = Strings.Right(strFileInfo, strFileInfo.Length - 1);
				}
				GetPastCheck:
				;
				intFirstQuote = 0;
				while (Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare) != 0)
				{
					intFirstQuote = Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare);
					intSecondQuote = Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare);
					strTemp = Strings.Mid(strFileInfo, intFirstQuote + 1, intSecondQuote - (intFirstQuote + 1)).Replace(",", "");
					strFileInfo = Strings.Left(strFileInfo, intFirstQuote) + strTemp + Strings.Right(strFileInfo, strFileInfo.Length - (intSecondQuote - 1));
					intFirstQuote = intSecondQuote;
				}
				//Application.DoEvents();
				strCheckInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strCheckInfo, 1) >= 6)
				{
					if (Information.IsNumeric(strCheckInfo[0]) && Information.IsDate(strCheckInfo[1]) && Strings.Trim(strCheckInfo[3].Replace(FCConvert.ToString(Convert.ToChar(34)), "")) != "")
					{
						Array.Resize(ref criCheckInfo, lngRecordCounter + 1);
						//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
						criCheckInfo[lngRecordCounter] = new CheckRecInfo(0);
						criCheckInfo[lngRecordCounter].CheckNumber = FCConvert.ToInt32(strCheckInfo[0]);
						criCheckInfo[lngRecordCounter].CheckDate = FCConvert.ToDateTime(strCheckInfo[1]);
						criCheckInfo[lngRecordCounter].CheckName = Strings.Trim(strCheckInfo[3].Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
						criCheckInfo[lngRecordCounter].CheckAmount = FCConvert.ToDecimal(Strings.Trim(strCheckInfo[11].Replace(FCConvert.ToString(Convert.ToChar(34)), "")));
						lngRecordCounter += 1;
					}
				}
			}
			intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank"))));
			// kk09042015 trobd-1020  GetVariable returning "" if Banks were not set up
			rsCheckRecInfo.OmitNullsOnInsert = true;
			rsCheckRecInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = 0");
			if (lngRecordCounter == 0)
			{
				MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "0 AP Records Added", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				for (counter = 0; counter <= Information.UBound(criCheckInfo, 1); counter++)
				{
					//Application.DoEvents();
					rsCheckRecInfo.AddNew();
					rsCheckRecInfo.Set_Fields("CheckNumber", criCheckInfo[counter].CheckNumber);
					rsCheckRecInfo.Set_Fields("CheckDate", criCheckInfo[counter].CheckDate);
					rsCheckRecInfo.Set_Fields("Name", criCheckInfo[counter].CheckName);
					rsCheckRecInfo.Set_Fields("Amount", criCheckInfo[counter].CheckAmount);
					rsCheckRecInfo.Set_Fields("Type", "1");
					rsCheckRecInfo.Set_Fields("Status", "1");
					rsCheckRecInfo.Set_Fields("StatusDate", DateTime.Today);
					rsCheckRecInfo.Set_Fields("BankNumber", intBank);
					rsCheckRecInfo.Update();
				}
				MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + FCConvert.ToString(Information.UBound(criCheckInfo, 1) + 1) + " AP Records Added", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		public void OrringtonCustomCheckRecPY()
		{
			string strCurDir;
			string strFilePath = "";
			string[] strInfo = null;
			// vbPorter upgrade warning: strCheckInfo As string()	OnRead(int, DateTime)
			string[] strCheckInfo = null;
			StreamReader tsInfo;
			string strFileInfo = "";
			int counter;
			int counter2;
			int counter3;
			int intFirstQuote = 0;
			int intSecondQuote = 0;
			CheckRecInfo[] criCheckInfo = null;
			int lngRecordCounter;
			int intBank;
			clsDRWrapper rsCheckRecInfo = new clsDRWrapper();
			string strTemp = "";
			strCurDir = FCFileSystem.Statics.UserDataFolder;
			Information.Err().Clear();
			FCCommonDialog ofd = new FCCommonDialog();
			//FC:TODO:PJ File-Handling (Options)
			//ofd.Options = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir;
			ofd.Filter = "*.csv";
			ofd.FileName = "*.csv";
			if (ofd.ShowOpen())
			{
				strFilePath = ofd.FileName;
			}
			else
			{
				return;
			}
			tsInfo = File.OpenText(strFilePath);
			lngRecordCounter = 0;
			while (tsInfo.EndOfStream == false)
			{
				//Application.DoEvents();
				strFileInfo = Strings.Trim(tsInfo.ReadLine());
				while (Strings.Left(strFileInfo, 1) == ",")
				{
					strFileInfo = Strings.Right(strFileInfo, strFileInfo.Length - 1);
				}
				GetPastCheck:
				;
				intFirstQuote = 0;
				while (Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare) != 0)
				{
					intFirstQuote = Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare);
					intSecondQuote = Strings.InStr(intFirstQuote + 1, strFileInfo, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbBinaryCompare);
					strTemp = Strings.Mid(strFileInfo, intFirstQuote + 1, intSecondQuote - (intFirstQuote + 1)).Replace(",", "");
					strFileInfo = Strings.Left(strFileInfo, intFirstQuote) + strTemp + Strings.Right(strFileInfo, strFileInfo.Length - (intSecondQuote - 1));
					intFirstQuote = intSecondQuote;
				}
				//Application.DoEvents();
				strCheckInfo = Strings.Split(strFileInfo, ",", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(strCheckInfo, 1) >= 9)
				{
					if (Information.IsNumeric(strCheckInfo[0]) && Information.IsDate(strCheckInfo[2]) && Strings.Trim(strCheckInfo[4].Replace(FCConvert.ToString(Convert.ToChar(34)), "")) != "")
					{
						Array.Resize(ref criCheckInfo, lngRecordCounter + 1);
						criCheckInfo[lngRecordCounter].CheckNumber = FCConvert.ToInt32(strCheckInfo[0]);
						criCheckInfo[lngRecordCounter].CheckDate = FCConvert.ToDateTime(strCheckInfo[2]);
						criCheckInfo[lngRecordCounter].CheckName = Strings.Trim(strCheckInfo[4].Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
						criCheckInfo[lngRecordCounter].CheckAmount = FCConvert.ToDecimal(Strings.Trim(strCheckInfo[11].Replace(FCConvert.ToString(Convert.ToChar(34)), "")));
						if (Information.IsNumeric(Strings.Trim(strCheckInfo[9].Replace("\"", " "))))
						{
							if (Conversion.Val(Strings.Trim(strCheckInfo[9].Replace("\"", " "))) != 0)
							{
								criCheckInfo[lngRecordCounter].CheckAmount = FCConvert.ToDecimal(Strings.Trim(strCheckInfo[9].Replace("\"", " ")));
								criCheckInfo[lngRecordCounter].DirectDeposit = false;
							}
							else
							{
								criCheckInfo[lngRecordCounter].CheckAmount = FCConvert.ToDecimal(Strings.Trim(strCheckInfo[8].Replace("\"", " ")));
								criCheckInfo[lngRecordCounter].DirectDeposit = true;
							}
						}
						else
						{
							criCheckInfo[lngRecordCounter].CheckAmount = FCConvert.ToDecimal(Strings.Trim(strCheckInfo[8].Replace("\"", " ")));
							criCheckInfo[lngRecordCounter].DirectDeposit = true;
						}
						lngRecordCounter += 1;
					}
				}
			}
			intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank"))));
			// kk09042015 trobd-1020  GetVariable returning "" if Banks were not set up
			rsCheckRecInfo.OmitNullsOnInsert = true;
			rsCheckRecInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE ID = 0");
			for (counter = 0; counter <= Information.UBound(criCheckInfo, 1); counter++)
			{
				//Application.DoEvents();
				rsCheckRecInfo.AddNew();
				rsCheckRecInfo.Set_Fields("CheckNumber", criCheckInfo[counter].CheckNumber);
				rsCheckRecInfo.Set_Fields("CheckDate", criCheckInfo[counter].CheckDate);
				rsCheckRecInfo.Set_Fields("Name", criCheckInfo[counter].CheckName);
				if (criCheckInfo[counter].DirectDeposit)
				{
					rsCheckRecInfo.Set_Fields("Amount", 0);
				}
				else
				{
					rsCheckRecInfo.Set_Fields("Amount", criCheckInfo[counter].CheckAmount);
				}
				rsCheckRecInfo.Set_Fields("DirectDeposit", criCheckInfo[counter].DirectDeposit);
				rsCheckRecInfo.Set_Fields("Type", "2");
				rsCheckRecInfo.Set_Fields("Status", "1");
				rsCheckRecInfo.Set_Fields("StatusDate", DateTime.Today);
				rsCheckRecInfo.Set_Fields("BankNumber", intBank);
				rsCheckRecInfo.Update();
			}
			MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + FCConvert.ToString(Information.UBound(criCheckInfo, 1) + 1) + " AP Records Added", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
	}
}
