﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1096Laser.
	/// </summary>
	partial class rpt1096Laser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1096Laser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldMunicipality = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldContact = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFederalID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNumberOfForms = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMISCX = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNECX = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.fldTotalFederalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldMunicipality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNumberOfForms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMISCX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNECX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalFederalTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldMunicipality,
            this.fldAddress1,
            this.fldAddress2,
            this.fldCityStateZip,
            this.fldContact,
            this.fldPhone,
            this.fldEmail,
            this.fldFax,
            this.fldFederalID,
            this.fldNumberOfForms,
            this.fldTotalAmount,
            this.fldMISCX,
            this.fldNECX,
            this.fldTotalFederalTax});
            this.Detail.Height = 5.5F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldMunicipality
            // 
            this.fldMunicipality.Height = 0.1875F;
            this.fldMunicipality.Left = 0.302F;
            this.fldMunicipality.Name = "fldMunicipality";
            this.fldMunicipality.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldMunicipality.Text = "Field1";
            this.fldMunicipality.Top = 1.28125F;
            this.fldMunicipality.Width = 2.84375F;
            // 
            // fldAddress1
            // 
            this.fldAddress1.Height = 0.1875F;
            this.fldAddress1.Left = 0.302F;
            this.fldAddress1.Name = "fldAddress1";
            this.fldAddress1.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldAddress1.Text = "Field1";
            this.fldAddress1.Top = 1.71875F;
            this.fldAddress1.Width = 2.84375F;
            // 
            // fldAddress2
            // 
            this.fldAddress2.Height = 0.1875F;
            this.fldAddress2.Left = 0.302F;
            this.fldAddress2.Name = "fldAddress2";
            this.fldAddress2.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldAddress2.Text = "Field1";
            this.fldAddress2.Top = 1.90625F;
            this.fldAddress2.Width = 2.84375F;
            // 
            // fldCityStateZip
            // 
            this.fldCityStateZip.Height = 0.1875F;
            this.fldCityStateZip.Left = 0.302F;
            this.fldCityStateZip.Name = "fldCityStateZip";
            this.fldCityStateZip.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldCityStateZip.Text = "Field1";
            this.fldCityStateZip.Top = 2.21875F;
            this.fldCityStateZip.Width = 2.84375F;
            // 
            // fldContact
            // 
            this.fldContact.CanGrow = false;
            this.fldContact.Height = 0.1875F;
            this.fldContact.Left = 0.302F;
            this.fldContact.Name = "fldContact";
            this.fldContact.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldContact.Text = null;
            this.fldContact.Top = 2.5625F;
            this.fldContact.Width = 2.28125F;
            // 
            // fldPhone
            // 
            this.fldPhone.CanGrow = false;
            this.fldPhone.Height = 0.1875F;
            this.fldPhone.Left = 3.1875F;
            this.fldPhone.Name = "fldPhone";
            this.fldPhone.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldPhone.Text = "Field1";
            this.fldPhone.Top = 2.5625F;
            this.fldPhone.Width = 1.75F;
            // 
            // fldEmail
            // 
            this.fldEmail.CanGrow = false;
            this.fldEmail.Height = 0.1875F;
            this.fldEmail.Left = 0.302F;
            this.fldEmail.MultiLine = false;
            this.fldEmail.Name = "fldEmail";
            this.fldEmail.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldEmail.Text = "Field1";
            this.fldEmail.Top = 2.90625F;
            this.fldEmail.Width = 2.28125F;
            // 
            // fldFax
            // 
            this.fldFax.CanGrow = false;
            this.fldFax.Height = 0.1875F;
            this.fldFax.Left = 3.1875F;
            this.fldFax.Name = "fldFax";
            this.fldFax.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldFax.Text = "Field1";
            this.fldFax.Top = 2.90625F;
            this.fldFax.Width = 1.75F;
            // 
            // fldFederalID
            // 
            this.fldFederalID.Height = 0.1875F;
            this.fldFederalID.Left = 0.302F;
            this.fldFederalID.Name = "fldFederalID";
            this.fldFederalID.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldFederalID.Text = "Field1";
            this.fldFederalID.Top = 3.25F;
            this.fldFederalID.Width = 1.75F;
            // 
            // fldNumberOfForms
            // 
            this.fldNumberOfForms.Height = 0.1875F;
            this.fldNumberOfForms.Left = 3.188F;
            this.fldNumberOfForms.Name = "fldNumberOfForms";
            this.fldNumberOfForms.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldNumberOfForms.Text = "Field1";
            this.fldNumberOfForms.Top = 3.25F;
            this.fldNumberOfForms.Width = 0.625F;
            // 
            // fldTotalAmount
            // 
            this.fldTotalAmount.Height = 0.1875F;
            this.fldTotalAmount.Left = 5.9F;
            this.fldTotalAmount.Name = "fldTotalAmount";
            this.fldTotalAmount.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldTotalAmount.Text = "Field1";
            this.fldTotalAmount.Top = 3.25F;
            this.fldTotalAmount.Width = 1.03125F;
            // 
            // fldMISCX
            // 
            this.fldMISCX.DataField = "";
            this.fldMISCX.DistinctField = "";
            this.fldMISCX.Height = 0.1875F;
            this.fldMISCX.Left = 0.77F;
            this.fldMISCX.Name = "fldMISCX";
            this.fldMISCX.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldMISCX.SummaryGroup = "";
            this.fldMISCX.Text = "X";
            this.fldMISCX.Top = 4.6875F;
            this.fldMISCX.Width = 0.25F;
            // 
            // fldNECX
            // 
            this.fldNECX.DataField = "";
            this.fldNECX.DistinctField = "";
            this.fldNECX.Height = 0.1875F;
            this.fldNECX.Left = 1.14F;
            this.fldNECX.Name = "fldNECX";
            this.fldNECX.Style = "font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldNECX.SummaryGroup = "";
            this.fldNECX.Text = "X";
            this.fldNECX.Top = 4.687F;
            this.fldNECX.Width = 0.25F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // fldTotalFederalTax
            // 
            this.fldTotalFederalTax.DataField = "";
            this.fldTotalFederalTax.DistinctField = "";
            this.fldTotalFederalTax.Height = 0.1875F;
            this.fldTotalFederalTax.Left = 4.34F;
            this.fldTotalFederalTax.Name = "fldTotalFederalTax";
            this.fldTotalFederalTax.Style = "color: #000000; font-family: \'Roman 10cpi\'; ddo-char-set: 1";
            this.fldTotalFederalTax.SummaryGroup = "";
            this.fldTotalFederalTax.Text = "Field1";
            this.fldTotalFederalTax.Top = 3.25F;
            this.fldTotalFederalTax.Width = 1.03125F;
            // 
            // rpt1096Laser
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldMunicipality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNumberOfForms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMISCX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNECX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalFederalTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMunicipality;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldContact;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEmail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfForms;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMISCX;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNECX;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalFederalTax;
    }
}
