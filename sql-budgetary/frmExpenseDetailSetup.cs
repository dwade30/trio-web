﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmExpenseDetailSetup.
	/// </summary>
	public partial class frmExpenseDetailSetup : BaseForm
	{
		public frmExpenseDetailSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbRange.SelectedIndex = 0;
			this.cmbAllAccounts.SelectedIndex = 0;
			this.cmbDetailSortOrder.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmExpenseDetailSetup InstancePtr
		{
			get
			{
				return (frmExpenseDetailSetup)Sys.GetInstance(typeof(frmExpenseDetailSetup));
			}
		}

		protected frmExpenseDetailSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         2/21/2002
		// This form will be used by people to select the search criteria
		// they wish to use to get information for the Expense Detail
		// Report
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs_AutoInitialized;

		clsDRWrapper rs
		{
			get
			{
				if (rs_AutoInitialized == null)
				{
					rs_AutoInitialized = new clsDRWrapper();
				}
				return rs_AutoInitialized;
			}
			set
			{
				rs_AutoInitialized = value;
			}
		}

		int FirstMonth;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}

		private void chkCheckAccountRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckAccountRange.CheckState == CheckState.Checked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					vsLowAccount.Visible = false;
					vsHighAccount.Visible = false;
					vsLowAccount.TextMatrix(0, 0, "");
					vsHighAccount.TextMatrix(0, 0, "");
					lblTo[1].Visible = false;
					lblTo[2].Visible = false;
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					cboSingleDept.Visible = false;
					cboSingleDept.SelectedIndex = -1;
				}
				else if (cmbAllAccounts.SelectedIndex == 4)
				{
					cboSingleFund.Visible = false;
					cboSingleFund.SelectedIndex = -1;
				}
				else
				{
					cboBeginningDept.Visible = false;
					cboEndingDept.Visible = false;
					cboBeginningDept.SelectedIndex = -1;
					cboEndingDept.SelectedIndex = -1;
					lblTo[1].Visible = false;
					lblTo[2].Visible = false;
				}
			}
			else
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					vsLowAccount.Visible = true;
					vsHighAccount.Visible = true;
					lblTo[1].Visible = true;
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					cboSingleDept.Visible = true;
				}
				else if (cmbAllAccounts.SelectedIndex == 4)
				{
					cboSingleFund.Visible = true;
				}
				else
				{
					cboBeginningDept.Visible = true;
					cboEndingDept.Visible = true;
					lblTo[2].Visible = true;
				}
			}
		}

		private void chkCheckDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckDateRange.CheckState == CheckState.Checked)
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = false;
					cboSingleMonth.SelectedIndex = -1;
				}
				else
				{
					cboBeginningMonth.Visible = false;
					cboEndingMonth.Visible = false;
					lblTo[0].Visible = false;
					cboBeginningMonth.SelectedIndex = -1;
					cboEndingMonth.SelectedIndex = -1;
				}
			}
			else
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = true;
					cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
				else
				{
					cboBeginningMonth.Visible = true;
					cboEndingMonth.Visible = true;
					lblTo[0].Visible = true;
					cboBeginningMonth.SelectedIndex = FirstMonth - 1;
					cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
			}
		}

		private void chkDepartment_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDivision.CheckState == CheckState.Checked)
			{
				if (chkDepartment.CheckState == CheckState.Unchecked)
				{
					chkDepartment.CheckState = CheckState.Checked;
					MessageBox.Show("You may not choose to have page breaks on divisions without having page breaks on departments.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void chkDivision_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDivision.CheckState == CheckState.Checked)
			{
				chkDepartment.CheckState = CheckState.Checked;
			}
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			if (!modBudgetaryMaster.Statics.blnExpenseDetailEdit)
			{
				frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);
			}
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			clsDRWrapper rs2 = new clsDRWrapper();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a Description for this Selection Criteria before you may proceed.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			//FC:FINAL:CHN - issue #1027: date range missing invalid.
			// "Range of Months" have index 2 at ComboBox
			if (cmbRange.SelectedIndex == 2 && chkCheckDateRange.CheckState != CheckState.Checked)// was: cmbRange.SelectedIndex == 0 && chkCheckDateRange.CheckState != CheckState.Checked
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.SelectedIndex == 1 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (chkCheckAccountRange.CheckState == CheckState.Unchecked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
					{
						MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
					{
						MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					if (cboSingleDept.SelectedIndex == -1 && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 4)
				{
					if (cboSingleFund.SelectedIndex == -1 && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify which Fund you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					if ((cboBeginningDept.SelectedIndex == -1 || cboEndingDept.SelectedIndex == -1) && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningDept.SelectedIndex > cboEndingDept.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "' AND Type = 'ED'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A selection criteria already exists with this description.  Do you wish to overwrite this?", "Overwrite Selection Criteria?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("Description", txtDescription.Text);
			rs.Set_Fields("Type", "ED");
			if (chkMonthlySubtotals.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("MonthlySubtotals", true);
			}
			else
			{
				rs.Set_Fields("MonthlySubtotals", false);
			}
			if (chkShowZeroBalanceAccounts.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("ShowZeroBalance", true);
			}
			else
			{
				rs.Set_Fields("ShowZeroBalance", false);
			}
			if (cmbRange.SelectedIndex == 1)
			{
				rs.Set_Fields("SelectedMonths", "S");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboSingleMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			//FC:FINAL:CHN - issue #1027: date range missing invalid.
			else if (cmbRange.SelectedIndex == 2)// was cmbRange.SelectedIndex == 0
			{
				rs.Set_Fields("SelectedMonths", "R");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboBeginningMonth.SelectedIndex + 1);
					rs.Set_Fields("EndMonth", cboEndingMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			else
			{
				rs.Set_Fields("SelectedMonths", "A");
				rs.Set_Fields("CheckMonthRange", false);
			}
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				rs.Set_Fields("SelectedAccounts", "A");
				rs.Set_Fields("CheckAccountRange", false);
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				rs.Set_Fields("SelectedAccounts", "R");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegAccount", vsLowAccount.TextMatrix(0, 0));
					rs.Set_Fields("EndAccount", vsHighAccount.TextMatrix(0, 0));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				rs.Set_Fields("SelectedAccounts", "S");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 4)
			{
				rs.Set_Fields("SelectedAccounts", "F");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else
			{
				rs.Set_Fields("SelectedAccounts", "D");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))));
					rs.Set_Fields("EndDeptExp", Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			if (cmbDetailSortOrder.SelectedIndex == 0)
			{
				rs.Set_Fields("SortOrder", "J");
			}
			else
			{
				rs.Set_Fields("SortOrder", "P");
			}
			if (chkDepartment.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("DepartmentBreak", true);
			}
			else
			{
				rs.Set_Fields("DepartmentBreak", false);
			}
			if (chkDivision.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("DivisionBreak", true);
			}
			else
			{
				rs.Set_Fields("DivisionBreak", false);
			}
			rs.Update();
			if (!modBudgetaryMaster.Statics.blnExpenseDetailEdit)
			{
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
				// center it
				frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'ED' AND Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
				frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);
				frmExpenseDetailSelect.InstancePtr.FillCriteria(true, Strings.Trim(txtDescription.Text));
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				ResetForm();
				Close();
			}
			else
			{
				ResetForm();
				Close();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmExpenseDetailSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmExpenseDetailSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
				}
			}
		}

		private void frmExpenseDetailSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmExpenseDetailSetup.FillStyle	= 0;
			//frmExpenseDetailSetup.ScaleWidth	= 8880;
			//frmExpenseDetailSetup.ScaleHeight	= 6930;
			//frmExpenseDetailSetup.LinkTopic	= "Form2";
			//frmExpenseDetailSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			vsLowGrid.GRID7Light = vsLowAccount;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsLowGrid.DefaultAccountType = "E";
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.DefaultAccountType = "E";
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsLowGrid.OnlyAllowDefaultType = true;
			vsHighGrid.OnlyAllowDefaultType = true;
			//FC:FINAL:BBE - Redesign
			//cboBeginningDept.Left = FCConvert.ToInt32(cboBeginningDept.Left + cboBeginningDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
			//cboSingleDept.Left = FCConvert.ToInt32(cboSingleDept.Left + (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleDept.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboBeginningDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboEndingDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleDept.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboSingleFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				chkDivision.Enabled = false;
			}
			else
			{
				chkDivision.Enabled = true;
			}
			cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			vsLowAccount.TextMatrix(0, 0, "");
			vsHighAccount.TextMatrix(0, 0, "");
			FirstMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			cboBeginningMonth.SelectedIndex = FirstMonth - 1;
			cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			if (modBudgetaryMaster.Statics.blnExpenseDetailEdit || modBudgetaryMaster.Statics.blnExpenseDetailReportEdit)
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
					{
						chkMonthlySubtotals.CheckState = CheckState.Checked;
					}
					else
					{
						chkMonthlySubtotals.CheckState = CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowZeroBalance")))
					{
						chkShowZeroBalanceAccounts.CheckState = CheckState.Checked;
					}
					else
					{
						chkShowZeroBalanceAccounts.CheckState = CheckState.Unchecked;
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						cmbRange.SelectedIndex = 1;
						cboSingleMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
					{
						cmbRange.SelectedIndex = 2;
						cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						cboEndingMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbRange.SelectedIndex = 0;
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
					{
						cmbAllAccounts.SelectedIndex = 0;
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
					{
						cmbAllAccounts.SelectedIndex = 1;
						vsLowAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount")));
						vsHighAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount")));
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
					{
						cmbAllAccounts.SelectedIndex = 2;
						for (counter = 0; counter <= cboSingleDept.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboSingleDept.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboSingleDept.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "F")
					{
						cmbAllAccounts.SelectedIndex = 4;
						for (counter = 0; counter <= cboSingleFund.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboSingleFund.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboSingleFund.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbAllAccounts.SelectedIndex = 3;
						for (counter = 0; counter <= cboBeginningDept.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboBeginningDept.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboBeginningDept.SelectedIndex = counter;
								break;
							}
						}
						for (counter = 0; counter <= cboEndingDept.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboEndingDept.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp")))
							{
								cboEndingDept.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						cmbDetailSortOrder.SelectedIndex = 1;
					}
					else
					{
						cmbDetailSortOrder.SelectedIndex = 0;
					}
					if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DepartmentBreak") == true)
					{
						chkDepartment.CheckState = CheckState.Checked;
					}
					if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("DivisionBreak") == true)
					{
						chkDivision.CheckState = CheckState.Checked;
					}
				}
			}
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			modBudgetaryMaster.Statics.blnExpenseDetailReportEdit = false;
			if (modBudgetaryMaster.Statics.blnExpenseDetailEdit)
			{
				frmGetExpenseDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmExpenseDetailSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (!modBudgetaryMaster.Statics.blnExpenseDetailEdit)
				{
					frmExpenseDetailSelect.InstancePtr.Show(App.MainForm);
				}
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void cmbAllAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = false;
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				vsLowAccount.Visible = true;
				vsHighAccount.Visible = true;
				lblTo[1].Visible = true;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = true;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				lblTo[1].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				cboBeginningDept.Visible = true;
				cboEndingDept.Visible = true;
				lblTo[2].Visible = true;
				cboSingleDept.Visible = false;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				lblTo[1].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
			else if (cmbAllAccounts.SelectedIndex == 4)
			{
				cboBeginningDept.Visible = false;
				cboEndingDept.Visible = false;
				cboBeginningDept.SelectedIndex = -1;
				cboEndingDept.SelectedIndex = -1;
				lblTo[2].Visible = false;
				cboSingleDept.Visible = false;
				cboSingleDept.SelectedIndex = -1;
				cboSingleFund.Visible = true;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
		}

		private void cmbRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = false;
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.SelectedIndex = -1;
				cboSingleMonth.Visible = false;
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.Visible = true;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			else if (cmbRange.SelectedIndex == 2)
			{
				cboBeginningMonth.Visible = true;
				cboEndingMonth.Visible = true;
				lblTo[0].Visible = true;
				cboSingleMonth.Visible = false;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboBeginningMonth.SelectedIndex = FirstMonth - 1;
				cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
		}

		private void ResetForm()
		{
			txtDescription.Text = "";
			vsHighAccount.TextMatrix(0, 0, "");
			vsLowAccount.TextMatrix(0, 0, "");
			cboSingleDept.SelectedIndex = -1;
			cboSingleDept.Visible = false;
			cboSingleFund.SelectedIndex = -1;
			cboSingleFund.Visible = false;
			cboBeginningDept.SelectedIndex = -1;
			cboBeginningDept.Visible = false;
			cboEndingDept.SelectedIndex = -1;
			cboEndingDept.Visible = false;
			lblTo[2].Visible = false;
			cmbRange.SelectedIndex = 0;
			cboBeginningMonth.Visible = false;
			cboEndingMonth.Visible = false;
			lblTo[1].Visible = false;
			cmbAllAccounts.SelectedIndex = 0;
			fraMonths.Visible = true;
			cmdSave.Enabled = true;
			cmdCancelPrint.Enabled = true;
			fraDeptRange.Visible = true;
			fraPageBreaks.Visible = true;
			chkDivision.CheckState = CheckState.Unchecked;
			chkDepartment.CheckState = CheckState.Unchecked;
			if (modAccountTitle.Statics.ExpDivFlag)
			{
				chkDivision.Enabled = false;
			}
			else
			{
				chkDivision.Enabled = true;
			}
		}

		private void cboBeginningDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboEndingDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboSingleDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}
	}
}
