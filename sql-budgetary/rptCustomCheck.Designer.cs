﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCustomCheck.
	/// </summary>
	partial class rptCustomCheck
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomCheck));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.GridMisc = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.SubReport2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.SubReport3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1,
				this.GridMisc,
				this.SubReport2,
				this.SubReport3
			});
			this.Detail.Height = 10.47917F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// SubReport1
			// 
			this.SubReport1.CanShrink = false;
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 3.40625F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0F;
			this.SubReport1.Width = 7.75F;
			// 
			// GridMisc
			// 
			this.GridMisc.Height = 0.125F;
			this.GridMisc.Left = 0.375F;
			this.GridMisc.Name = "GridMisc";
			this.GridMisc.Top = 3.5625F;
			this.GridMisc.Visible = false;
			this.GridMisc.Width = 5.8125F;
			// 
			// SubReport2
			// 
			this.SubReport2.CanShrink = false;
			this.SubReport2.CloseBorder = false;
			this.SubReport2.Height = 3.5F;
			this.SubReport2.Left = 0F;
			this.SubReport2.Name = "SubReport2";
			this.SubReport2.Report = null;
			this.SubReport2.Top = 3.40625F;
			this.SubReport2.Width = 7.75F;
			// 
			// SubReport3
			// 
			this.SubReport3.CanGrow = false;
			this.SubReport3.CanShrink = false;
			this.SubReport3.CloseBorder = false;
			this.SubReport3.Height = 3.5F;
			this.SubReport3.Left = 0F;
			this.SubReport3.Name = "SubReport3";
			this.SubReport3.Report = null;
			this.SubReport3.Top = 6.90625F;
			this.SubReport3.Width = 7.75F;
			// 
			// rptCustomCheck
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.75F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptCustomCheck_ReportEndedAndCanceled);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl GridMisc;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport2;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport3;
	}
}
