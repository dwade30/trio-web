﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerRanges.
	/// </summary>
	public partial class frmLedgerRanges : BaseForm
	{
		public frmLedgerRanges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerRanges InstancePtr
		{
			get
			{
				return (frmLedgerRanges)Sys.GetInstance(typeof(frmLedgerRanges));
			}
		}

		protected frmLedgerRanges _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int FirstLowBoundCol;
		int FirstHighBoundCol;
		int SecondLowBoundCol;
		int SecondHighBoundCol;
		int ThirdLowBoundCol;
		int ThirdHighBoundCol;
		int ThirdLabelCol;
		int AcctLength;
		double AssetLow;
		double AssetHigh;
		double LiabilityLow;
		double LiabilityHigh;
		bool Checker;
		bool EditFlag;
		bool RowFlag;
		bool ClearFlag;
		double FundLow;
		double FundHigh;
		double[] AssetLevel2High = new double[4 + 1];
		double[] LiabilityLevel2High = new double[4 + 1];
		double[] FundLevel2High = new double[4 + 1];
		clsDRWrapper rs = new clsDRWrapper();
		float[] ColPercents = new float[8 + 1];
		bool blnUnload;

		private void frmLedgerRanges_Activated(object sender, System.EventArgs e)
		{
			int counter;
			int counter2 = 0;
			string temp = "";
			string tempAccount;
			string tempAccount2;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			FirstLowBoundCol = 2;
			FirstHighBoundCol = 3;
			SecondLowBoundCol = 4;
			SecondHighBoundCol = 5;
			ThirdLowBoundCol = 6;
			ThirdHighBoundCol = 7;
			ThirdLabelCol = 8;
			AcctLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))));
			ColPercents[0] = 0.0459106f;
			ColPercents[1] = 0.1110023f;
			ColPercents[2] = 0.1110023f;
			ColPercents[3] = 0.1110023f;
			ColPercents[4] = 0.1250023f;
			ColPercents[5] = 0.1110023f;
			ColPercents[6] = 0.1110023f;
			ColPercents[7] = 0.1018219f;
			ColPercents[8] = 0.14f;
			//FC:FINAL:AM:#i564 - add expand button
			vs1.AddExpandButton();
			tempAccount = modValidateAccount.GetFormat("1", ref AcctLength);
			tempAccount2 = modValidateAccount.GetFormat("0", ref AcctLength);
			// put headings in the flexgrid and set the outline levels
			vs1.TextMatrix(1, FirstLowBoundCol, tempAccount);
			switch (AcctLength)
			{
			// then format it to the length of the division number
				case 1:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "3");
						break;
					}
				case 2:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "33");
						break;
					}
				case 3:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "333");
						break;
					}
				case 4:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "3333");
						break;
					}
				case 5:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "33333");
						break;
					}
				case 6:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "333333");
						break;
					}
				case 7:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "3333333");
						break;
					}
				case 8:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "33333333");
						break;
					}
				case 9:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "333333333");
						break;
					}
				case 10:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "3333333333");
						break;
					}
				case 11:
					{
						vs1.TextMatrix(1, FirstHighBoundCol, "33333333333");
						break;
					}
			}
			//end switch
			vs1.TextMatrix(1, SecondLowBoundCol, "        Assets        ");
			//FC:FINAL:AM:#420 - remove colors
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 0, 1, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, 1, ThirdLabelCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 1, 0, 1, ThirdLabelCol, true);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, 1, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(1, 0);
			vs1.IsSubtotal(1, true);
			vs1.TextMatrix(2, SecondLowBoundCol, vs1.TextMatrix(0, FirstLowBoundCol));
			vs1.TextMatrix(2, SecondHighBoundCol, vs1.TextMatrix(0, FirstHighBoundCol));
			vs1.TextMatrix(2, ThirdLowBoundCol, "Level 2");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 2, 0, 2, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 0, 2, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 2, FirstLowBoundCol, 2, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(2, 1);
			vs1.IsSubtotal(2, true);
			vs1.TextMatrix(3, ThirdLowBoundCol, vs1.TextMatrix(0, FirstLowBoundCol));
			vs1.TextMatrix(3, ThirdHighBoundCol, vs1.TextMatrix(0, FirstHighBoundCol));
			vs1.TextMatrix(3, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 3, FirstLowBoundCol, 3, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(3, 2);
			vs1.TextMatrix(4, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(4, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(4, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 4, FirstLowBoundCol, 4, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(4, 2);
			vs1.TextMatrix(5, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(5, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(5, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 5, FirstLowBoundCol, 5, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(5, 2);
			vs1.TextMatrix(6, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(6, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(6, ThirdLowBoundCol, "Level 2");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 6, 0, 6, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, 0, 6, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 6, FirstLowBoundCol, 6, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(6, 1);
			vs1.IsSubtotal(6, true);
			vs1.TextMatrix(7, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(7, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(7, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 7, FirstLowBoundCol, 7, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(7, 2);
			vs1.TextMatrix(8, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(8, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(8, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 8, FirstLowBoundCol, 8, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(8, 2);
			vs1.TextMatrix(9, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(9, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(9, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 9, FirstLowBoundCol, 9, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(9, 2);
			vs1.TextMatrix(10, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(10, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(10, ThirdLowBoundCol, "Level 2");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 10, 0, 10, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 10, 0, 10, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 10, FirstLowBoundCol, 9, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(10, 1);
			vs1.IsSubtotal(10, true);
			vs1.TextMatrix(11, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(11, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(11, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 11, FirstLowBoundCol, 11, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(11, 2);
			vs1.TextMatrix(12, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(12, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(12, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 12, FirstLowBoundCol, 12, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(12, 2);
			vs1.TextMatrix(13, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(13, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(13, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 13, FirstLowBoundCol, 13, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(13, 2);
			vs1.TextMatrix(14, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(14, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(14, ThirdLowBoundCol, "Level 2");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 14, 0, 14, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 14, 0, 14, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 14, FirstLowBoundCol, 13, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(14, 1);
			vs1.IsSubtotal(14, true);
			vs1.TextMatrix(15, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(15, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(15, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 15, FirstLowBoundCol, 15, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(15, 2);
			vs1.TextMatrix(16, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(16, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(16, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 16, FirstLowBoundCol, 16, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(16, 2);
			vs1.TextMatrix(17, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(17, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(17, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 17, FirstLowBoundCol, 17, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(17, 2);
			switch (AcctLength)
			{
			// then format it to the length of the division number
				case 1:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "4");
						break;
					}
				case 2:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "34");
						break;
					}
				case 3:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "334");
						break;
					}
				case 4:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "3334");
						break;
					}
				case 5:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "33334");
						break;
					}
				case 6:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "333334");
						break;
					}
				case 7:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "3333334");
						break;
					}
				case 8:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "33333334");
						break;
					}
				case 9:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "333333334");
						break;
					}
				case 10:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "3333333334");
						break;
					}
				case 11:
					{
						vs1.TextMatrix(18, FirstLowBoundCol, "33333333334");
						break;
					}
			}
			//end switch
			switch (AcctLength)
			{
			// then format it to the length of the division number
				case 1:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "6");
						break;
					}
				case 2:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "66");
						break;
					}
				case 3:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "666");
						break;
					}
				case 4:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "6666");
						break;
					}
				case 5:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "66666");
						break;
					}
				case 6:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "666666");
						break;
					}
				case 7:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "6666666");
						break;
					}
				case 8:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "66666666");
						break;
					}
				case 9:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "666666666");
						break;
					}
				case 10:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "6666666666");
						break;
					}
				case 11:
					{
						vs1.TextMatrix(18, FirstHighBoundCol, "66666666666");
						break;
					}
			}
			//end switch
			vs1.TextMatrix(18, SecondLowBoundCol, "     Liabilities     ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 18, 0, 18, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 18, 0, 18, ThirdLabelCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 18, FirstLowBoundCol, 18, ThirdLabelCol - 1, 5);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 18, 0, 18, ThirdLabelCol, true);
			vs1.RowOutlineLevel(18, 0);
			vs1.IsSubtotal(18, true);
			vs1.TextMatrix(19, SecondLowBoundCol, vs1.TextMatrix(18, FirstLowBoundCol));
			vs1.TextMatrix(19, SecondHighBoundCol, vs1.TextMatrix(18, FirstHighBoundCol));
			vs1.TextMatrix(19, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 19, 0, 19, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 19, 0, 19, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, FirstLowBoundCol, 19, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(19, 1);
			vs1.IsSubtotal(19, true);
			vs1.TextMatrix(20, ThirdLowBoundCol, vs1.TextMatrix(18, FirstLowBoundCol));
			vs1.TextMatrix(20, ThirdHighBoundCol, vs1.TextMatrix(18, FirstHighBoundCol));
			vs1.TextMatrix(20, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 20, FirstLowBoundCol, 20, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(20, 2);
			vs1.TextMatrix(21, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(21, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(21, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 21, FirstLowBoundCol, 21, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(21, 2);
			vs1.TextMatrix(22, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(22, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(22, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 22, FirstLowBoundCol, 22, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(22, 2);
			vs1.TextMatrix(23, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(23, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(23, ThirdLowBoundCol, " Level 2 ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 23, 0, 23, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 23, 0, 23, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 23, FirstLowBoundCol, 23, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(23, 1);
			vs1.IsSubtotal(23, true);
			vs1.TextMatrix(24, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(24, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(24, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 24, FirstLowBoundCol, 24, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(24, 2);
			vs1.TextMatrix(25, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(25, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(25, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 25, FirstLowBoundCol, 25, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(25, 2);
			vs1.TextMatrix(26, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(26, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(26, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 26, FirstLowBoundCol, 26, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(26, 2);
			vs1.TextMatrix(27, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(27, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(27, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 27, 0, 27, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 27, 0, 27, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 27, FirstLowBoundCol, 27, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(27, 1);
			vs1.IsSubtotal(27, true);
			vs1.TextMatrix(28, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(28, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(28, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 28, FirstLowBoundCol, 28, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(28, 2);
			vs1.TextMatrix(29, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(29, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(29, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 29, FirstLowBoundCol, 29, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(29, 2);
			vs1.TextMatrix(30, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(30, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(30, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 30, FirstLowBoundCol, 30, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(30, 2);
			vs1.TextMatrix(31, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(31, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(31, ThirdLowBoundCol, " Level 2 ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 31, 0, 31, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 31, 0, 31, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 31, FirstLowBoundCol, 31, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(31, 1);
			vs1.IsSubtotal(31, true);
			vs1.TextMatrix(32, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(32, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(32, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 32, FirstLowBoundCol, 32, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(32, 2);
			vs1.TextMatrix(33, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(33, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(33, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 33, FirstLowBoundCol, 33, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(33, 2);
			vs1.TextMatrix(34, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(34, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(34, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 34, FirstLowBoundCol, 34, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(34, 2);
			switch (AcctLength)
			{
			// then format it to the length of the division number
				case 1:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "7");
						break;
					}
				case 2:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "67");
						break;
					}
				case 3:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "667");
						break;
					}
				case 4:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "6667");
						break;
					}
				case 5:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "66667");
						break;
					}
				case 6:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "666667");
						break;
					}
				case 7:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "6666667");
						break;
					}
				case 8:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "66666667");
						break;
					}
				case 9:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "666666667");
						break;
					}
				case 10:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "6666666667");
						break;
					}
				case 11:
					{
						vs1.TextMatrix(35, FirstLowBoundCol, "66666666667");
						break;
					}
			}
			//end switch
			switch (AcctLength)
			{
			// then format it to the length of the division number
				case 1:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "9");
						FundHigh = 9;
						break;
					}
				case 2:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "99");
						FundHigh = 99;
						break;
					}
				case 3:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "999");
						FundHigh = 999;
						break;
					}
				case 4:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "9999");
						FundHigh = 9999;
						break;
					}
				case 5:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "99999");
						FundHigh = 99999;
						break;
					}
				case 6:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "999999");
						FundHigh = 999999;
						break;
					}
				case 7:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "9999999");
						FundHigh = 9999999;
						break;
					}
				case 8:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "99999999");
						FundHigh = 99999999;
						break;
					}
				case 9:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "999999999");
						FundHigh = 999999999;
						break;
					}
				case 10:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "9999999999");
						FundHigh = 9999999999.0;
						break;
					}
				case 11:
					{
						vs1.TextMatrix(35, FirstHighBoundCol, "99999999999");
						FundHigh = 99999999999.0;
						break;
					}
			}
			//end switch
			vs1.TextMatrix(35, SecondLowBoundCol, " Fund Balance ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 35, 0, 35, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 35, 0, 35, ThirdLabelCol, 0x80000018);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 35, FirstLowBoundCol, 35, ThirdLabelCol - 1, 5);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 35, 0, 35, ThirdLabelCol, true);
			vs1.RowOutlineLevel(35, 0);
			vs1.IsSubtotal(35, true);
			vs1.TextMatrix(36, SecondLowBoundCol, vs1.TextMatrix(35, FirstLowBoundCol));
			vs1.TextMatrix(36, SecondHighBoundCol, vs1.TextMatrix(35, FirstHighBoundCol));
			vs1.TextMatrix(36, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 36, 0, 36, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 36, 0, 36, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 36, FirstLowBoundCol, 36, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(36, 1);
			vs1.IsSubtotal(36, true);
			vs1.TextMatrix(37, ThirdLowBoundCol, vs1.TextMatrix(35, FirstLowBoundCol));
			vs1.TextMatrix(37, ThirdHighBoundCol, vs1.TextMatrix(35, FirstHighBoundCol));
			vs1.TextMatrix(37, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 37, FirstLowBoundCol, 37, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(37, 2);
			vs1.TextMatrix(38, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(38, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(38, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 38, FirstLowBoundCol, 38, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(38, 2);
			vs1.TextMatrix(39, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(39, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(39, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 39, FirstLowBoundCol, 39, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(39, 2);
			vs1.TextMatrix(40, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(40, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(40, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 40, 0, 40, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 40, 0, 40, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 40, FirstLowBoundCol, 40, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(40, 1);
			vs1.IsSubtotal(40, true);
			vs1.TextMatrix(41, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(41, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(41, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 41, FirstLowBoundCol, 41, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(41, 2);
			vs1.TextMatrix(42, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(42, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(42, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 42, FirstLowBoundCol, 42, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(42, 2);
			vs1.TextMatrix(43, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(43, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(43, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 43, FirstLowBoundCol, 43, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(43, 2);
			vs1.TextMatrix(44, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(44, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(44, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 44, 0, 44, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 44, 0, 44, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 44, FirstLowBoundCol, 44, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(44, 1);
			vs1.IsSubtotal(44, true);
			vs1.TextMatrix(45, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(45, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(45, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 45, FirstLowBoundCol, 45, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(45, 2);
			vs1.TextMatrix(46, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(46, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(46, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 46, FirstLowBoundCol, 46, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(46, 2);
			vs1.TextMatrix(47, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(47, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(47, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 47, FirstLowBoundCol, 47, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(47, 2);
			vs1.TextMatrix(48, SecondLowBoundCol, tempAccount2);
			vs1.TextMatrix(48, SecondHighBoundCol, tempAccount2);
			vs1.TextMatrix(48, ThirdLowBoundCol, "      Level 2      ");
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 48, 0, 48, ThirdLabelCol, 0x80000017);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 48, 0, 48, ThirdLabelCol, 0x80000016);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 48, FirstLowBoundCol, 48, ThirdLabelCol - 3, 5);
			vs1.RowOutlineLevel(48, 1);
			vs1.IsSubtotal(48, true);
			vs1.TextMatrix(49, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(49, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(49, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 49, FirstLowBoundCol, 49, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(49, 2);
			vs1.TextMatrix(50, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(50, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(50, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 50, FirstLowBoundCol, 50, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(50, 2);
			vs1.TextMatrix(51, ThirdLowBoundCol, tempAccount2);
			vs1.TextMatrix(51, ThirdHighBoundCol, tempAccount2);
			vs1.TextMatrix(51, ThirdLabelCol, "Level 3");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 51, FirstLowBoundCol, 51, ThirdLabelCol - 1, 5);
			vs1.RowOutlineLevel(51, 2);
			for (counter = 1; counter <= 3; counter++)
			{
				if (counter == 1)
				{
					temp = "A";
					counter2 = 1;
				}
				else if (counter == 2)
				{
					temp = "L";
					counter2 = 18;
				}
				else
				{
					temp = "F";
					counter2 = 35;
				}
				rs.OpenRecordset("SELECT * FROM LedgerRanges WHERE RecordName = '" + temp + "'");
				if (rs.BeginningOfFile() != true && rs.EndOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					if (counter == 1)
					{
						// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
						AssetHigh = Conversion.Val(rs.Get_Fields("High"));
						if (Conversion.Val(rs.Get_Fields_String("High1")) == 0)
						{
							// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
							AssetLevel2High[0] = Conversion.Val(rs.Get_Fields("High"));
						}
						else
						{
							AssetLevel2High[0] = Conversion.Val(rs.Get_Fields_String("High1"));
						}
						AssetLevel2High[1] = Conversion.Val(rs.Get_Fields_String("High2"));
						AssetLevel2High[2] = Conversion.Val(rs.Get_Fields_String("High3"));
						AssetLevel2High[3] = Conversion.Val(rs.Get_Fields_String("High4"));
					}
					else if (counter == 2)
					{
						// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
						LiabilityHigh = Conversion.Val(rs.Get_Fields("High"));
						if (Conversion.Val(rs.Get_Fields_String("High1")) == 0)
						{
							// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
							LiabilityLevel2High[0] = Conversion.Val(rs.Get_Fields("High"));
						}
						else
						{
							LiabilityLevel2High[0] = Conversion.Val(rs.Get_Fields_String("High1"));
						}
						LiabilityLevel2High[1] = Conversion.Val(rs.Get_Fields_String("High2"));
						LiabilityLevel2High[2] = Conversion.Val(rs.Get_Fields_String("High3"));
						LiabilityLevel2High[3] = Conversion.Val(rs.Get_Fields_String("High4"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
						FundHigh = Conversion.Val(rs.Get_Fields("High"));
						if (Conversion.Val(rs.Get_Fields_String("High1")) == 0)
						{
							// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
							FundLevel2High[0] = Conversion.Val(rs.Get_Fields("High"));
						}
						else
						{
							FundLevel2High[0] = Conversion.Val(rs.Get_Fields_String("High1"));
						}
						FundLevel2High[1] = Conversion.Val(rs.Get_Fields_String("High2"));
						FundLevel2High[2] = Conversion.Val(rs.Get_Fields_String("High3"));
						FundLevel2High[3] = Conversion.Val(rs.Get_Fields_String("High4"));
					}
					// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter2, FirstHighBoundCol, FCConvert.ToString(rs.Get_Fields("High")));
					// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
					vs1.TextMatrix(counter2, FirstLowBoundCol, FCConvert.ToString(rs.Get_Fields("Low")));
					if (Conversion.Val(rs.Get_Fields_String("High1")) == 0)
					{
						// TODO Get_Fields: Check the table for the column [High] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter2 + 1, SecondHighBoundCol, FCConvert.ToString(rs.Get_Fields("High")));
						// TODO Get_Fields: Check the table for the column [Low] and replace with corresponding Get_Field method
						vs1.TextMatrix(counter2 + 1, SecondLowBoundCol, FCConvert.ToString(rs.Get_Fields("Low")));
					}
					else
					{
						vs1.TextMatrix(counter2 + 1, SecondHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High1")));
						vs1.TextMatrix(counter2 + 1, SecondLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low1")));
					}
					vs1.TextMatrix(counter2 + 1, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Name1")));
					if (Conversion.Val(rs.Get_Fields_String("High11")) == 0)
					{
						vs1.TextMatrix(counter2 + 2, ThirdHighBoundCol, vs1.TextMatrix(counter2 + 1, SecondHighBoundCol));
						vs1.TextMatrix(counter2 + 2, ThirdLowBoundCol, vs1.TextMatrix(counter2 + 1, SecondLowBoundCol));
					}
					else
					{
						vs1.TextMatrix(counter2 + 2, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High11")));
						vs1.TextMatrix(counter2 + 2, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low11")));
					}
					vs1.TextMatrix(counter2 + 2, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name11")));
					vs1.TextMatrix(counter2 + 3, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High12")));
					vs1.TextMatrix(counter2 + 3, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low12")));
					vs1.TextMatrix(counter2 + 3, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name12")));
					vs1.TextMatrix(counter2 + 4, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High13")));
					vs1.TextMatrix(counter2 + 4, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low13")));
					vs1.TextMatrix(counter2 + 4, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name13")));
					vs1.TextMatrix(counter2 + 5, SecondHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High2")));
					vs1.TextMatrix(counter2 + 5, SecondLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low2")));
					vs1.TextMatrix(counter2 + 5, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Name2")));
					if (Conversion.Val(rs.Get_Fields_String("High21")) == 0)
					{
						vs1.TextMatrix(counter2 + 6, ThirdHighBoundCol, vs1.TextMatrix(counter2 + 5, SecondHighBoundCol));
						vs1.TextMatrix(counter2 + 6, ThirdLowBoundCol, vs1.TextMatrix(counter2 + 5, SecondLowBoundCol));
					}
					else
					{
						vs1.TextMatrix(counter2 + 6, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High21")));
						vs1.TextMatrix(counter2 + 6, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low21")));
					}
					vs1.TextMatrix(counter2 + 6, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name21")));
					vs1.TextMatrix(counter2 + 7, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High22")));
					vs1.TextMatrix(counter2 + 7, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low22")));
					vs1.TextMatrix(counter2 + 7, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name22")));
					vs1.TextMatrix(counter2 + 8, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High23")));
					vs1.TextMatrix(counter2 + 8, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low23")));
					vs1.TextMatrix(counter2 + 8, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name23")));
					vs1.TextMatrix(counter2 + 9, SecondHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High3")));
					vs1.TextMatrix(counter2 + 9, SecondLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low3")));
					vs1.TextMatrix(counter2 + 9, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Name3")));
					if (Conversion.Val(rs.Get_Fields_String("High31")) == 0)
					{
						vs1.TextMatrix(counter2 + 10, ThirdHighBoundCol, vs1.TextMatrix(counter2 + 9, SecondHighBoundCol));
						vs1.TextMatrix(counter2 + 10, ThirdLowBoundCol, vs1.TextMatrix(counter2 + 9, SecondLowBoundCol));
					}
					else
					{
						vs1.TextMatrix(counter2 + 10, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High31")));
						vs1.TextMatrix(counter2 + 10, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low31")));
					}
					vs1.TextMatrix(counter2 + 10, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name31")));
					vs1.TextMatrix(counter2 + 11, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High32")));
					vs1.TextMatrix(counter2 + 11, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low32")));
					vs1.TextMatrix(counter2 + 11, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name32")));
					vs1.TextMatrix(counter2 + 12, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High33")));
					vs1.TextMatrix(counter2 + 12, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low33")));
					vs1.TextMatrix(counter2 + 12, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name33")));
					vs1.TextMatrix(counter2 + 13, SecondHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High4")));
					vs1.TextMatrix(counter2 + 13, SecondLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low4")));
					vs1.TextMatrix(counter2 + 13, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Name4")));
					if (Conversion.Val(rs.Get_Fields_String("High41")) == 0)
					{
						vs1.TextMatrix(counter2 + 14, ThirdHighBoundCol, vs1.TextMatrix(counter2 + 13, SecondHighBoundCol));
						vs1.TextMatrix(counter2 + 14, ThirdLowBoundCol, vs1.TextMatrix(counter2 + 13, SecondLowBoundCol));
					}
					else
					{
						vs1.TextMatrix(counter2 + 14, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High41")));
						vs1.TextMatrix(counter2 + 14, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low41")));
					}
					vs1.TextMatrix(counter2 + 14, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name41")));
					vs1.TextMatrix(counter2 + 15, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High42")));
					vs1.TextMatrix(counter2 + 15, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low42")));
					vs1.TextMatrix(counter2 + 15, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name42")));
					vs1.TextMatrix(counter2 + 16, ThirdHighBoundCol, FCConvert.ToString(rs.Get_Fields_String("High43")));
					vs1.TextMatrix(counter2 + 16, ThirdLowBoundCol, FCConvert.ToString(rs.Get_Fields_String("Low43")));
					vs1.TextMatrix(counter2 + 16, ThirdLabelCol, FCConvert.ToString(rs.Get_Fields_String("Name43")));
				}
			}
			for (counter = 1; counter <= 51; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 1)
				{
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			for (counter = 1; counter <= 51; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 0)
				{
					vs1.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
			frmWait.InstancePtr.Unload();
			vs1.Visible = true;
            modColorScheme.ColorGrid(vs1);
			//Form_Resize();
			vs1.Focus();
			vs1.Select(1, FirstHighBoundCol);
			//FC:FINAL:MSH - Issue #840 - need to call 'Form_Resize', because without open tab 'Resize' executed twice 
			// and with opened tab 'Resize' method is called only once before table initialized
			Form_Resize();
			this.Refresh();
		}

		private void frmLedgerRanges_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Tab)
			{
				KeyCode = (Keys)0;
				Support.SendKeys("{ENTER}", false);
			}
			else if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmLedgerRanges_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLedgerRanges.FillStyle	= 0;
			//frmLedgerRanges.ScaleWidth	= 9045;
			//frmLedgerRanges.ScaleHeight	= 7260;
			//frmLedgerRanges.LinkTopic	= "Form2";
			//frmLedgerRanges.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:AM:#i564 - attach resize event handler in load
			this.Resize += new System.EventHandler(this.frmLedgerRanges_Resize);
		}

		private void frmLedgerRanges_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 8; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * ColPercents[counter]));
			}
			vs1_Collapsed();
		}

		public void Form_Resize()
		{
			frmLedgerRanges_Resize(this, new System.EventArgs());
		}

		private void frmLedgerRanges_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
				// ElseIf KeyAscii = 13 Then
				// KeyAscii = 0
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			//Application.DoEvents();
			if (vs1.RowOutlineLevel(vs1.Row) == 0)
			{
				if (vs1.Col >= FirstHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
			else if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col >= SecondHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
			else
			{
				if (vs1.Col >= ThirdHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.RowOutlineLevel(vs1.Row) == 0)
			{
				vs1.Col = FirstHighBoundCol;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col != ThirdLowBoundCol && vs1.Col != ThirdHighBoundCol)
				{
					vs1.Col = SecondHighBoundCol;
				}
				else if (vs1.Col == ThirdHighBoundCol)
				{
					vs1.Col = ThirdLowBoundCol;
				}
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else
			{
				if (vs1.Col != ThirdLabelCol)
				{
					vs1.Col = ThirdHighBoundCol;
				}
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int counter;
			int rows = 0;
			int height;
			bool FirstFlag = false;
			bool SecondFlag = false;
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == 0)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						FirstFlag = true;
					}
					else
					{
						rows += 1;
						FirstFlag = false;
					}
				}
				else if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (FirstFlag == true)
					{
						// do nothing
					}
					else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						SecondFlag = true;
					}
					else
					{
						rows += 1;
						SecondFlag = false;
					}
				}
				else
				{
					if (FirstFlag == true || SecondFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			//FC:FINAL:BBE - use Anchoring
			//height = rows * vs1.RowHeight(0);
			//if (height < frmLedgerRanges.InstancePtr.Height * 0.7375)
			//{
			//    if (vs1.Height != height)
			//    {
			//        vs1.Height = height + 75;
			//    }
			//}
			//else
			//{
			//    if (vs1.Height < frmLedgerRanges.InstancePtr.Height * 0.7375)
			//    {
			//        vs1.Height = FCConvert.ToInt32(frmLedgerRanges.InstancePtr.Height * 0.7375 + 75);
			//    }
			//}
		}

        private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= new KeyEventHandler(vs1_KeyDownEdit);
                e.Control.KeyDown += new KeyEventHandler(vs1_KeyDownEdit);
            }
        }

        private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int TempCol = 0;
			int tempLevel = 0;
			int temp = 0;
			Keys KeyCode = e.KeyCode;
			if (vs1.RowOutlineLevel(vs1.Row) == 0)
			{
				TempCol = FirstHighBoundCol;
				tempLevel = 0;
			}
			else if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				TempCol = SecondHighBoundCol;
				tempLevel = 1;
			}
			else
			{
				TempCol = ThirdHighBoundCol;
				tempLevel = 2;
			}
            //FC:FINAL:AM:#2955 - always set the EditFlag
			//if (vs1.EditSelStart > 0)
			{
				EditFlag = true;
			}
			if (vs1.Col == TempCol)
			{
				vs1.EditMaxLength = AcctLength;
				if (vs1.EditText.Length == AcctLength)
				{
					// if the cell is the length of the division number
					if (vs1.EditSelStart == AcctLength)
					{
						// dont move the cursor
						if (KeyCode == Keys.Left || KeyCode == Keys.Back)
						{
							return;
						}
						KeyCode = 0;
						// dont send the key through
						if (vs1.RowOutlineLevel(vs1.Row) == 0)
						{
							temp = GetNextRow();
							if (temp != 0)
							{
								RowFlag = true;
								vs1.Row = temp;
								vs1.Col = ChooseCorrectCol(0, TempCol, temp);
								Support.SendKeys("{LEFT}", false);
								RowFlag = false;
							}
						}
						else
						{
							vs1.Col += 1;
							Support.SendKeys("{LEFT}", false);
						}
					}
				}
			}
			else if (vs1.Col == TempCol + 1)
			{
				vs1.EditMaxLength = 15;
			}
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				if (vs1.Col == TempCol && tempLevel != 0)
				{
					vs1.Col += 1;
				}
				else
				{
					temp = GetNextRow();
					if (temp != 0)
					{
						RowFlag = true;
						vs1.Row = temp;
						if (vs1.RowOutlineLevel(temp) == 0)
						{
							vs1.Col = FirstHighBoundCol;
						}
						else if (vs1.RowOutlineLevel(temp) == 1)
						{
							vs1.Col = SecondHighBoundCol;
						}
						else
						{
							vs1.Col = ThirdHighBoundCol;
						}
						RowFlag = false;
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
			else if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 36)
				{
					KeyCode = 0;
					if (vs1.IsCollapsed(18) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						vs1.Row = 18;
					}
					else if (vs1.IsCollapsed(31) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						vs1.Row = 31;
					}
					else
					{
						vs1.Row = 34;
					}
				}
				else if (vs1.Row > 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row) == 0)
					{
						if (vs1.IsCollapsed(vs1.Row - 4) == FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							temp = vs1.Row - 1;
						}
						else if (vs1.IsCollapsed(vs1.Row - 18) == FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							temp = vs1.Row - 4;
						}
						else
						{
							temp = vs1.Row - 18;
						}
					}
					else if (vs1.RowOutlineLevel(vs1.Row) == 1)
					{
						if (vs1.Row == 2)
						{
							temp = 0;
						}
						else
						{
							if (vs1.IsCollapsed(vs1.Row - 4) == FCGrid.CollapsedSettings.flexOutlineExpanded)
							{
								temp = vs1.Row - 1;
							}
							else
							{
								temp = vs1.Row - 4;
							}
						}
					}
					else
					{
						temp = vs1.Row - 1;
					}
					RowFlag = true;
					vs1.Col = ChooseCorrectCol(vs1.RowOutlineLevel(vs1.Row), vs1.Col, temp);
					RowFlag = false;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row < vs1.Rows - 1)
				{
					if (vs1.RowOutlineLevel(vs1.Row) == 0)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							temp = vs1.Row + 1;
						}
						else if (vs1.Row == 35)
						{
							return;
						}
						else
						{
							temp = vs1.Row + 18;
						}
					}
					else if (vs1.RowOutlineLevel(vs1.Row) == 1)
					{
						if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							temp = vs1.Row + 1;
						}
						else
						{
							if (vs1.Row == 48)
							{
								return;
							}
							else
							{
								temp = vs1.Row + 4;
							}
						}
					}
					else
					{
						temp = vs1.Row + 1;
					}
					RowFlag = true;
					vs1.Col = ChooseCorrectCol(vs1.RowOutlineLevel(vs1.Row), vs1.Col, temp);
					RowFlag = false;
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int temp = 0;
			if (!RowFlag)
			{
				//FC:FINAL:AM:#415 - don't edit the first col
				if (vs1.Col != 1)
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				if (vs1.RowOutlineLevel(vs1.Row) == 0)
				{
					if (vs1.Row == 35)
					{
						if (vs1.IsCollapsed(35) == FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							RowFlag = true;
							vs1.Row += 1;
							vs1.Col = SecondHighBoundCol;
							vs1.EditCell();
							vs1.EditSelStart = 0;
							RowFlag = false;
						}
						else
						{
							if (vs1.IsCollapsed(18) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
							{
								RowFlag = true;
								vs1.Row = 18;
								vs1.Col = FirstHighBoundCol;
								vs1.EditCell();
								vs1.EditSelStart = 0;
								RowFlag = false;
							}
							else if (vs1.IsCollapsed(31) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
							{
								RowFlag = true;
								vs1.Row = 31;
								vs1.Col = SecondHighBoundCol;
								vs1.EditCell();
								vs1.EditSelStart = 0;
								RowFlag = false;
							}
							else
							{
								RowFlag = true;
								vs1.Row = 34;
								vs1.Col = ThirdHighBoundCol;
								vs1.EditCell();
								vs1.EditSelStart = 0;
								RowFlag = false;
							}
						}
					}
					else
					{
						vs1.Col = FirstHighBoundCol;
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else if (vs1.RowOutlineLevel(vs1.Row) == 1)
				{
					if (vs1.Col != ThirdLowBoundCol && vs1.Col != ThirdHighBoundCol && vs1.Col != 7)
					{
						vs1.Col = SecondHighBoundCol;
					}
					else if (vs1.Col == ThirdHighBoundCol)
					{
						vs1.Col = ThirdLowBoundCol;
						vs1.TextMatrix(vs1.Row, vs1.Col, Strings.Trim(vs1.TextMatrix(vs1.Row, vs1.Col)));
					}
					else if (vs1.Col == 8)
					{
						temp = GetNextRow();
						if (temp != 0)
						{
							RowFlag = true;
							vs1.Row = temp;
							if (vs1.RowOutlineLevel(vs1.Row) == 0)
							{
								vs1.Col = FirstHighBoundCol;
							}
							else if (vs1.RowOutlineLevel(vs1.Row) == 1)
							{
								vs1.Col = SecondHighBoundCol;
							}
							else
							{
								vs1.Col = ThirdHighBoundCol;
							}
							RowFlag = false;
						}
						else
						{
							RowFlag = true;
							vs1.Col = ThirdLowBoundCol;
							RowFlag = false;
							vs1.EditSelStart = vs1.TextMatrix(vs1.Row, vs1.Col).Length;
							return;
						}
					}
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					if (vs1.Col != ThirdLabelCol)
					{
						vs1.Col = ThirdHighBoundCol;
					}
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.RowOutlineLevel(vs1.Row) == 0)
			{
				if (vs1.Col >= FirstHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
			else if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.Col >= SecondHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
			else
			{
				if (vs1.Col >= ThirdHighBoundCol + 1)
				{
					vs1.EditMaxLength = 15;
				}
				else
				{
					vs1.EditMaxLength = AcctLength;
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				string tempformat = "";
				int CheckColumn = 0;
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vs1.GetFlexRowIndex(e.RowIndex);
				int col = vs1.GetFlexColIndex(e.ColumnIndex);
				if (vs1.RowOutlineLevel(row) == 0)
				{
					CheckColumn = FirstHighBoundCol;
				}
				else if (vs1.RowOutlineLevel(row) == 1)
				{
					CheckColumn = SecondHighBoundCol;
				}
				else
				{
					CheckColumn = ThirdHighBoundCol;
				}
				if (col == CheckColumn)
				{
					if (!Information.IsNumeric(vs1.EditText) || Conversion.Val(vs1.EditText) < 0)
					{
						MessageBox.Show("You must enter a number greater than 0 in this field", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
					else
					{
						vs1.EditText = modValidateAccount.GetFormat(vs1.EditText, ref AcctLength);
						if (vs1.RowOutlineLevel(row) == 0)
						{
							if (row == 1)
							{
								if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(row, FirstLowBoundCol)))
								{
									MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									if (Conversion.Val(vs1.EditText) + 4 > Math.Pow(10, AcctLength) - 1)
									{
										MessageBox.Show("You must leave room to set ranges for Assets, Liabilities, and Fund Balances", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
									else
									{
										AssetLow = Conversion.Val(vs1.TextMatrix(row, FirstLowBoundCol));
										if (AssetHigh != Conversion.Val(vs1.EditText))
										{
											AssetHigh = Conversion.Val(vs1.EditText);
											ClearData(FCConvert.ToInt16(row));
											ClearData(FCConvert.ToInt16(row + 17));
											ClearData(FCConvert.ToInt16(row + 34));
											vs1.TextMatrix(row + 1, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(row, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(row + 1, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
											AssetLevel2High[0] = Conversion.Val(vs1.TextMatrix(row + 1, SecondHighBoundCol));
											vs1.TextMatrix(row + 2, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(row, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(row + 2, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
										}
										vs1.TextMatrix(18, FirstLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
										LiabilityLow = Conversion.Val(vs1.EditText);
										if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(18, FirstLowBoundCol), vs1.TextMatrix(18, FirstHighBoundCol), true) >= 0)
										{
											vs1.TextMatrix(18, FirstHighBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.TextMatrix(18, FirstLowBoundCol)) + 1), ref AcctLength));
											LiabilityHigh = (FCConvert.ToInt32(vs1.TextMatrix(18, FirstLowBoundCol)) + 1);
											vs1.TextMatrix(18, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(18, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstHighBoundCol), ref AcctLength));
											if (Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol)) != 0)
											{
												LiabilityLevel2High[0] = Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol));
											}
											else
											{
												LiabilityLevel2High[0] = LiabilityHigh;
											}
											vs1.TextMatrix(20, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(20, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstHighBoundCol), ref AcctLength));
											vs1.TextMatrix(35, FirstLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.TextMatrix(18, FirstHighBoundCol)) + 1), ref AcctLength));
											FundLow = Conversion.Val(vs1.EditText);
											vs1.TextMatrix(35, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(35, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstHighBoundCol), ref AcctLength));
											FundLevel2High[0] = Conversion.Val(vs1.TextMatrix(35, SecondHighBoundCol));
											vs1.TextMatrix(37, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(37, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstHighBoundCol), ref AcctLength));
										}
										else
										{
											vs1.TextMatrix(19, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(19, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstHighBoundCol), ref AcctLength));
											if (Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol)) != 0)
											{
												LiabilityLevel2High[0] = Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol));
											}
											else
											{
												LiabilityLevel2High[0] = LiabilityHigh;
											}
											vs1.TextMatrix(20, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(20, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(18, FirstHighBoundCol), ref AcctLength));
										}
									}
								}
							}
							else if (row == 18)
							{
								if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(row, FirstLowBoundCol)))
								{
									MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									if (Conversion.Val(vs1.EditText) + 2 > Math.Pow(10, AcctLength) - 1)
									{
										MessageBox.Show("You must leave room to set ranges for Assets, Liabilities, and Fund Balances", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
									else
									{
										LiabilityLow = Conversion.Val(vs1.TextMatrix(row, FirstLowBoundCol));
										if (LiabilityHigh != Conversion.Val(vs1.EditText))
										{
											LiabilityHigh = Conversion.Val(vs1.EditText);
											ClearData(FCConvert.ToInt16(row));
											ClearData(FCConvert.ToInt16(row + 17));
											vs1.TextMatrix(row + 1, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(row, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(row + 1, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
											if (Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol)) != 0)
											{
												LiabilityLevel2High[0] = Conversion.Val(vs1.TextMatrix(18, SecondHighBoundCol));
											}
											else
											{
												LiabilityLevel2High[0] = LiabilityHigh;
											}
											vs1.TextMatrix(row + 2, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(row, FirstLowBoundCol), ref AcctLength));
											vs1.TextMatrix(row + 2, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
										}
										vs1.TextMatrix(35, FirstLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
										FundLow = Conversion.Val(vs1.EditText);
										vs1.TextMatrix(36, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstLowBoundCol), ref AcctLength));
										vs1.TextMatrix(36, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstHighBoundCol), ref AcctLength));
										FundLevel2High[0] = Conversion.Val(vs1.TextMatrix(35, SecondHighBoundCol));
										vs1.TextMatrix(37, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstLowBoundCol), ref AcctLength));
										vs1.TextMatrix(37, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(35, FirstHighBoundCol), ref AcctLength));
									}
								}
							}
						}
						else if (vs1.RowOutlineLevel(row) == 1)
						{
							if (row < 18 && EditFlag)
							{
								e.Cancel = CheckAssets(row);
							}
							else if (row < 35 && EditFlag)
							{
								e.Cancel = CheckLiabilities(row);
							}
							else if (EditFlag)
							{
								e.Cancel = CheckFunds(row);
							}
						}
						else
						{
							if (row < 18 && EditFlag)
							{
								e.Cancel = CheckThirdLevelAssets(row);
							}
							else if (row < 35 && EditFlag)
							{
								e.Cancel = CheckThirdLevelLiabilities(row);
							}
							else if (EditFlag)
							{
								e.Cancel = CheckThirdLevelFunds(row);
							}
						}
					}
				}
				else
				{
					if (Strings.Trim(vs1.EditText) == "")
					{
						MessageBox.Show("There must be a Description to go with every range", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetNextRow()
		{
			short GetNextRow = 0;
			int counter;
			bool FirstFlag = false;
			bool SecondFlag = false;
			if (vs1.RowOutlineLevel(vs1.Row) == 0)
			{
				if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					FirstFlag = true;
				}
			}
			else if (vs1.RowOutlineLevel(vs1.Row) == 1)
			{
				if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					SecondFlag = true;
				}
			}
			counter = vs1.Row + 1;
			while (counter < vs1.Rows)
			{
				if (vs1.RowOutlineLevel(counter) == 2)
				{
					if (FirstFlag || SecondFlag)
					{
						// do nothing
					}
					else
					{
						break;
					}
				}
				else if (vs1.RowOutlineLevel(counter) == 1)
				{
					if (FirstFlag)
					{
						// do nothing
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
				counter += 1;
				if (counter == vs1.Rows)
				{
					GetNextRow = 0;
					return GetNextRow;
				}
			}
			if (counter == vs1.Rows)
			{
				GetNextRow = 0;
			}
			else
			{
				GetNextRow = FCConvert.ToInt16(counter);
			}
			return GetNextRow;
		}

		private void ClearData(short x)
		{
			int counter;
			string temp;
			temp = modValidateAccount.GetFormat("0", ref AcctLength);
			if (x != 35)
			{
				for (counter = x + 1; counter <= x + 16; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						vs1.TextMatrix(counter, SecondLowBoundCol, temp);
						vs1.TextMatrix(counter, SecondHighBoundCol, temp);
					}
					else
					{
						vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
						vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
					}
				}
			}
			else
			{
				for (counter = x + 3; counter <= x + 14; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						vs1.TextMatrix(counter, SecondLowBoundCol, temp);
						vs1.TextMatrix(counter, SecondHighBoundCol, temp);
					}
					else
					{
						vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
						vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
					}
				}
			}
		}

		private void ClearSmallData(int x)
		{
			int counter;
			string temp;
			temp = modValidateAccount.GetFormat("0", ref AcctLength);
			for (counter = x + 1; counter <= x + 3; counter++)
			{
				vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
				vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
			}
		}

		private void ClearRestOfData(int x)
		{
			int counter;
			string temp;
			temp = modValidateAccount.GetFormat("0", ref AcctLength);
			if (x < 18)
			{
				for (counter = x + 4; counter <= 17; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						vs1.TextMatrix(counter, SecondLowBoundCol, temp);
						vs1.TextMatrix(counter, SecondHighBoundCol, temp);
					}
					else
					{
						vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
						vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
					}
				}
			}
			else if (x < 35)
			{
				for (counter = x + 4; counter <= 34; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						vs1.TextMatrix(counter, SecondLowBoundCol, temp);
						vs1.TextMatrix(counter, SecondHighBoundCol, temp);
					}
					else
					{
						vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
						vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
					}
				}
			}
			else
			{
				for (counter = x + 4; counter <= 51; counter++)
				{
					if (vs1.RowOutlineLevel(counter) == 1)
					{
						vs1.TextMatrix(counter, SecondLowBoundCol, temp);
						vs1.TextMatrix(counter, SecondHighBoundCol, temp);
					}
					else
					{
						vs1.TextMatrix(counter, ThirdLowBoundCol, temp);
						vs1.TextMatrix(counter, ThirdHighBoundCol, temp);
					}
				}
			}
		}

		private bool CheckAssets(int Row)
		{
			bool CheckAssets = false;
			EditFlag = false;
			if (FCConvert.ToDouble(vs1.EditText) > AssetHigh)
			{
				MessageBox.Show("Your level 2 ranges cannot exceed your Asset range", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckAssets = true;
				return CheckAssets;
			}
			else if (FCConvert.ToDouble(vs1.EditText) == 0)
			{
				// do nothing
			}
			else if (FCConvert.ToDouble(vs1.EditText) == AssetHigh - 1)
			{
				MessageBox.Show("You must leave at least a range of 2 to create another level 2 subrange", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckAssets = true;
				return CheckAssets;
			}
			else if (Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)) == 0)
			{
				MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckAssets = true;
				return CheckAssets;
			}
			else
			{
				if (Row == 2)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckAssets = true;
						return CheckAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (AssetLevel2High[0] != Conversion.Val(vs1.EditText))
						{
							AssetLevel2High[0] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							ClearSmallData(Row + 4);
							ClearSmallData(Row + 8);
							ClearSmallData(Row + 12);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckAssets = false;
							return CheckAssets;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength));
							AssetLevel2High[1] = AssetHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
						else
						{
							if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondLowBoundCol), vs1.TextMatrix(Row + 8, SecondHighBoundCol), true) < 0)
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									AssetLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									if (FCConvert.ToDouble(vs1.TextMatrix(Row + 12, SecondLowBoundCol)) == 0)
									{
										ClearRestOfData(Row + 4);
									}
									else
									{
										vs1.TextMatrix(Row + 8, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 8, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength));
										AssetLevel2High[2] = AssetHigh;
										ClearRestOfData(Row + 8);
										vs1.TextMatrix(Row + 9, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 9, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									}
								}
								else
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondHighBoundCol), ref AcctLength));
									AssetLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									ClearRestOfData(Row + 4);
								}
							}
						}
						ResetValues(Row + 4);
					}
				}
				else if (Row == 6)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckAssets = true;
						return CheckAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (AssetLevel2High[1] != Conversion.Val(vs1.EditText))
						{
							AssetLevel2High[1] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							ClearSmallData(Row + 4);
							ClearSmallData(Row + 8);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckAssets = false;
							return CheckAssets;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0 || fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength));
							AssetLevel2High[2] = AssetHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
							ClearRestOfData(Row + 4);
						}
						else
						{
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
						ResetValues(Row + 4);
					}
				}
				else if (Row == 10)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckAssets = true;
						return CheckAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (AssetLevel2High[2] != Conversion.Val(vs1.EditText))
						{
							AssetLevel2High[2] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							ClearSmallData(Row + 4);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckAssets = false;
							return CheckAssets;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength));
						}
						AssetLevel2High[3] = AssetHigh;
						vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
						vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						ResetValues(Row + 4);
					}
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckAssets = true;
						return CheckAssets;
					}
					else
					{
						if (AssetLevel2High[3] != AssetHigh)
						{
							AssetLevel2High[3] = AssetHigh;
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength));
						}
						vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(AssetHigh), ref AcctLength);
					}
				}
			}
			CheckAssets = false;
			return CheckAssets;
		}

		private bool CheckLiabilities(int Row)
		{
			bool CheckLiabilities = false;
			EditFlag = false;
			if (FCConvert.ToDouble(vs1.EditText) > LiabilityHigh)
			{
				MessageBox.Show("Your level 2 ranges cannot exceed your Liability range", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckLiabilities = true;
				return CheckLiabilities;
			}
			else if (FCConvert.ToDouble(vs1.EditText) == 0)
			{
				// do nothing
			}
			else if (FCConvert.ToDouble(vs1.EditText) == LiabilityHigh - 1)
			{
				MessageBox.Show("You must leave at least a range of 2 to create another level 2 subrange", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckLiabilities = true;
				return CheckLiabilities;
			}
			else if (Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)) == 0)
			{
				MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckLiabilities = true;
				return CheckLiabilities;
			}
			else
			{
				if (Row == 19)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLiabilities = true;
						return CheckLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (LiabilityLevel2High[0] != Conversion.Val(vs1.EditText))
						{
							LiabilityLevel2High[0] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckLiabilities = false;
							return CheckLiabilities;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength));
							LiabilityLevel2High[1] = LiabilityHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
						else
						{
							if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondLowBoundCol), vs1.TextMatrix(Row + 8, SecondHighBoundCol), true) < 0)
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									LiabilityLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									if (FCConvert.ToDouble(vs1.TextMatrix(Row + 12, SecondLowBoundCol)) == 0)
									{
										ClearRestOfData(Row + 4);
									}
									else
									{
										vs1.TextMatrix(Row + 8, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 8, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength));
										LiabilityLevel2High[2] = LiabilityHigh;
										ClearRestOfData(Row + 8);
										vs1.TextMatrix(Row + 9, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 9, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									}
								}
								else
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondHighBoundCol), ref AcctLength));
									LiabilityLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									ClearRestOfData(Row + 4);
								}
							}
						}
					}
				}
				else if (Row == 23)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLiabilities = true;
						return CheckLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (LiabilityLevel2High[1] != Conversion.Val(vs1.EditText))
						{
							LiabilityLevel2High[1] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckLiabilities = false;
							return CheckLiabilities;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0 || fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength));
							LiabilityLevel2High[2] = LiabilityHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
							ClearRestOfData(Row + 4);
						}
						else
						{
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
					}
				}
				else if (Row == 27)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLiabilities = true;
						return CheckLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (LiabilityLevel2High[2] != Conversion.Val(vs1.EditText))
						{
							LiabilityLevel2High[2] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckLiabilities = false;
							return CheckLiabilities;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength));
						}
						LiabilityLevel2High[3] = LiabilityHigh;
						vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
						vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
					}
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckLiabilities = true;
						return CheckLiabilities;
					}
					else
					{
						if (LiabilityLevel2High[3] != LiabilityHigh)
						{
							LiabilityLevel2High[3] = LiabilityHigh;
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength));
						}
						vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(LiabilityHigh), ref AcctLength);
					}
				}
			}
			CheckLiabilities = false;
			return CheckLiabilities;
		}

		private bool CheckFunds(int Row)
		{
			bool CheckFunds = false;
			EditFlag = false;
			if (FCConvert.ToDouble(vs1.EditText) > FundHigh)
			{
				MessageBox.Show("Your level 2 ranges cannot exceed your Fund range", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckFunds = true;
				return CheckFunds;
			}
			else if (FCConvert.ToDouble(vs1.EditText) == 0)
			{
				// do nothing
			}
			else if (FCConvert.ToDouble(vs1.EditText) == FundHigh - 1)
			{
				MessageBox.Show("You must leave at least a range of 2 to create another level 2 subrange", "Invalid Level 2 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckFunds = true;
				return CheckFunds;
			}
			else if (Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)) == 0)
			{
				MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				CheckFunds = true;
				return CheckFunds;
			}
			else
			{
				if (Row == 36)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckFunds = true;
						return CheckFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (FundLevel2High[0] != Conversion.Val(vs1.EditText))
						{
							FundLevel2High[0] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckFunds = false;
							return CheckFunds;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength));
							FundLevel2High[1] = FundHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
						else
						{
							if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondLowBoundCol), vs1.TextMatrix(Row + 8, SecondHighBoundCol), true) < 0)
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									FundLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									if (FCConvert.ToDouble(vs1.TextMatrix(Row + 12, SecondLowBoundCol)) == 0)
									{
										ClearRestOfData(Row + 4);
									}
									else
									{
										vs1.TextMatrix(Row + 8, SecondLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 8, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength));
										FundLevel2High[2] = FundHigh;
										ClearRestOfData(Row + 8);
										vs1.TextMatrix(Row + 9, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondLowBoundCol), ref AcctLength));
										vs1.TextMatrix(Row + 9, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 8, SecondHighBoundCol), ref AcctLength));
									}
								}
								else
								{
									vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 12, SecondHighBoundCol), ref AcctLength));
									FundLevel2High[1] = Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol));
									vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
									vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
									ClearRestOfData(Row + 4);
								}
							}
						}
					}
				}
				else if (Row == 40)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckFunds = true;
						return CheckFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (FundLevel2High[1] != Conversion.Val(vs1.EditText))
						{
							FundLevel2High[1] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckFunds = false;
							return CheckFunds;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0 || FCConvert.ToDouble(vs1.TextMatrix(Row + 8, SecondLowBoundCol)) == 0 || fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row + 4, SecondHighBoundCol), vs1.TextMatrix(Row + 4, SecondLowBoundCol), true) <= 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength));
							FundLevel2High[2] = FundHigh;
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
							ClearRestOfData(Row + 4);
						}
						else
						{
							vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
						}
					}
				}
				else if (Row == 44)
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckFunds = true;
						return CheckFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundHigh)
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (FundLevel2High[2] != Conversion.Val(vs1.EditText))
						{
							FundLevel2High[2] = Conversion.Val(vs1.EditText);
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.EditText, ref AcctLength));
						}
						if (ClearFlag)
						{
							ClearRestOfData(Row);
							ClearFlag = false;
							CheckFunds = false;
							return CheckFunds;
						}
						vs1.TextMatrix(Row + 4, SecondLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
						if (Conversion.Val(vs1.TextMatrix(Row + 4, SecondHighBoundCol)) == 0)
						{
							vs1.TextMatrix(Row + 4, SecondHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength));
						}
						FundLevel2High[3] = FundHigh;
						vs1.TextMatrix(Row + 5, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondLowBoundCol), ref AcctLength));
						vs1.TextMatrix(Row + 5, ThirdHighBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row + 4, SecondHighBoundCol), ref AcctLength));
					}
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, SecondLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckFunds = true;
						return CheckFunds;
					}
					else
					{
						if (FundLevel2High[3] != FundHigh)
						{
							FundLevel2High[3] = FundHigh;
							ClearSmallData(Row);
							vs1.TextMatrix(Row + 1, ThirdLowBoundCol, modValidateAccount.GetFormat(vs1.TextMatrix(Row, SecondLowBoundCol), ref AcctLength));
							vs1.TextMatrix(Row + 1, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength));
						}
						vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(FundHigh), ref AcctLength);
					}
				}
			}
			CheckFunds = false;
			return CheckFunds;
		}

		private bool CheckThirdLevelAssets(int Row)
		{
			bool CheckThirdLevelAssets = false;
			int counter;
			EditFlag = false;
			if (Row < 6)
			{
				if (FCConvert.ToDouble(vs1.EditText) > AssetLevel2High[0])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == AssetLevel2High[0] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelAssets = true;
						return CheckThirdLevelAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetLevel2High[0])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[0]), ref AcctLength);
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[0]), ref AcctLength));
								CheckThirdLevelAssets = false;
								return CheckThirdLevelAssets;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[0]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelAssets = false;
									return CheckThirdLevelAssets;
								}
							}
						}
					}
				}
			}
			else if (Row < 10)
			{
				if (FCConvert.ToDouble(vs1.EditText) > AssetLevel2High[1])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == AssetLevel2High[1] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelAssets = true;
						return CheckThirdLevelAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetLevel2High[1])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[1]), ref AcctLength);
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[1]), ref AcctLength));
								CheckThirdLevelAssets = false;
								return CheckThirdLevelAssets;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[1]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelAssets = false;
									return CheckThirdLevelAssets;
								}
							}
						}
					}
				}
			}
			else if (Row < 14)
			{
				if (FCConvert.ToDouble(vs1.EditText) > AssetLevel2High[2])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == AssetLevel2High[2] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelAssets = true;
						return CheckThirdLevelAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetLevel2High[2])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						if (vs1.RowOutlineLevel(Row + 1) == 1)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[2]), ref AcctLength);
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[2]), ref AcctLength));
								CheckThirdLevelAssets = false;
								return CheckThirdLevelAssets;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[2]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelAssets = false;
									return CheckThirdLevelAssets;
								}
							}
						}
					}
				}
			}
			else
			{
				if (FCConvert.ToDouble(vs1.EditText) > AssetLevel2High[3])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == AssetLevel2High[3] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelAssets = true;
					return CheckThirdLevelAssets;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelAssets = true;
						return CheckThirdLevelAssets;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < AssetLevel2High[3])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[3]), ref AcctLength);
							CheckThirdLevelAssets = false;
							return CheckThirdLevelAssets;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[3]), ref AcctLength));
								CheckThirdLevelAssets = false;
								return CheckThirdLevelAssets;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(AssetLevel2High[3]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelAssets = false;
									return CheckThirdLevelAssets;
								}
							}
						}
					}
				}
			}
			return CheckThirdLevelAssets;
		}

		private void ClearRestOfSmallData(ref int x)
		{
			string temp;
			temp = modValidateAccount.GetFormat("0", ref AcctLength);
			if (x + 1 == vs1.Rows)
			{
				return;
			}
			else
			{
				x += 1;
				while (vs1.RowOutlineLevel(x) == 2)
				{
					vs1.TextMatrix(x, ThirdLowBoundCol, temp);
					vs1.TextMatrix(x, ThirdHighBoundCol, temp);
					x += 1;
					if (x == vs1.Rows)
					{
						return;
					}
				}
			}
		}

		private bool CheckThirdLevelLiabilities(int Row)
		{
			bool CheckThirdLevelLiabilities = false;
			int counter;
			EditFlag = false;
			if (Row < 23)
			{
				if (FCConvert.ToDouble(vs1.EditText) > LiabilityLevel2High[0])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == LiabilityLevel2High[0] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelLiabilities = true;
						return CheckThirdLevelLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityLevel2High[0])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[0]), ref AcctLength);
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[0]), ref AcctLength));
								CheckThirdLevelLiabilities = false;
								return CheckThirdLevelLiabilities;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[0]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelLiabilities = false;
									return CheckThirdLevelLiabilities;
								}
							}
						}
					}
				}
			}
			else if (Row < 27)
			{
				if (FCConvert.ToDouble(vs1.EditText) > LiabilityLevel2High[1])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == LiabilityLevel2High[1] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelLiabilities = true;
						return CheckThirdLevelLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityLevel2High[1])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[1]), ref AcctLength);
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[1]), ref AcctLength));
								CheckThirdLevelLiabilities = false;
								return CheckThirdLevelLiabilities;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[1]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelLiabilities = false;
									return CheckThirdLevelLiabilities;
								}
							}
						}
					}
				}
			}
			else if (Row < 31)
			{
				if (FCConvert.ToDouble(vs1.EditText) > LiabilityLevel2High[2])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == LiabilityLevel2High[2] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelLiabilities = true;
						return CheckThirdLevelLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityLevel2High[2])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						if (vs1.RowOutlineLevel(Row + 1) == 1)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[2]), ref AcctLength);
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[2]), ref AcctLength));
								CheckThirdLevelLiabilities = false;
								return CheckThirdLevelLiabilities;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[2]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelLiabilities = false;
									return CheckThirdLevelLiabilities;
								}
							}
						}
					}
				}
			}
			else
			{
				if (FCConvert.ToDouble(vs1.EditText) > LiabilityLevel2High[3])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == LiabilityLevel2High[3] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelLiabilities = true;
					return CheckThirdLevelLiabilities;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelLiabilities = true;
						return CheckThirdLevelLiabilities;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < LiabilityLevel2High[3])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[3]), ref AcctLength);
							CheckThirdLevelLiabilities = false;
							return CheckThirdLevelLiabilities;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[3]), ref AcctLength));
								CheckThirdLevelLiabilities = false;
								return CheckThirdLevelLiabilities;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(LiabilityLevel2High[3]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelLiabilities = false;
									return CheckThirdLevelLiabilities;
								}
							}
						}
					}
				}
			}
			return CheckThirdLevelLiabilities;
		}

		private bool CheckThirdLevelFunds(int Row)
		{
			bool CheckThirdLevelFunds = false;
			int counter;
			EditFlag = false;
			if (Row < 40)
			{
				if (FCConvert.ToDouble(vs1.EditText) > FundLevel2High[0])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == FundLevel2High[0] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelFunds = true;
						return CheckThirdLevelFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundLevel2High[0])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[0]), ref AcctLength);
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[0]), ref AcctLength));
								CheckThirdLevelFunds = false;
								return CheckThirdLevelFunds;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[0]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelFunds = false;
									return CheckThirdLevelFunds;
								}
							}
						}
					}
				}
			}
			else if (Row < 44)
			{
				if (FCConvert.ToDouble(vs1.EditText) > FundLevel2High[1])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == FundLevel2High[1] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelFunds = true;
						return CheckThirdLevelFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundLevel2High[1])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[1]), ref AcctLength);
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[1]), ref AcctLength));
								CheckThirdLevelFunds = false;
								return CheckThirdLevelFunds;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[1]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelFunds = false;
									return CheckThirdLevelFunds;
								}
							}
						}
					}
				}
			}
			else if (Row < 48)
			{
				if (FCConvert.ToDouble(vs1.EditText) > FundLevel2High[2])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == FundLevel2High[2] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelFunds = true;
						return CheckThirdLevelFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundLevel2High[2])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						if (vs1.RowOutlineLevel(Row + 1) == 1)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[2]), ref AcctLength);
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[2]), ref AcctLength));
								CheckThirdLevelFunds = false;
								return CheckThirdLevelFunds;
							}
							else
							{
								if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[2]), ref AcctLength));
									ClearRestOfSmallData(ref Row);
								}
								else
								{
									CheckThirdLevelFunds = false;
									return CheckThirdLevelFunds;
								}
							}
						}
					}
				}
			}
			else
			{
				if (FCConvert.ToDouble(vs1.EditText) > FundLevel2High[3])
				{
					MessageBox.Show("Your level 3 ranges cannot exceed your Level 2 range", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (FCConvert.ToDouble(vs1.EditText) == 0)
				{
					// do nothing
				}
				else if (FCConvert.ToDouble(vs1.EditText) == FundLevel2High[3] - 1)
				{
					MessageBox.Show("You must leave at least a range of 2 to create another level 3 subrange", "Invalid Level 3 Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else if (Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)) == 0)
				{
					MessageBox.Show("You must declare the ranges in order", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					CheckThirdLevelFunds = true;
					return CheckThirdLevelFunds;
				}
				else
				{
					if (Conversion.Val(vs1.EditText) <= Conversion.Val(vs1.TextMatrix(Row, ThirdLowBoundCol)))
					{
						MessageBox.Show("Your upper range must be greater then your lower range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CheckThirdLevelFunds = true;
						return CheckThirdLevelFunds;
					}
					else
					{
						if (Conversion.Val(vs1.EditText) < FundLevel2High[3])
						{
							// do nothing
						}
						else
						{
							ClearFlag = true;
						}
						if (ClearFlag)
						{
							ClearRestOfSmallData(ref Row);
							ClearFlag = false;
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						if (vs1.RowOutlineLevel(Row + 1) != 2)
						{
							vs1.EditText = modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[3]), ref AcctLength);
							CheckThirdLevelFunds = false;
							return CheckThirdLevelFunds;
						}
						else
						{
							Row += 1;
							vs1.TextMatrix(Row, ThirdLowBoundCol, modValidateAccount.GetFormat(FCConvert.ToString(FCConvert.ToInt32(vs1.EditText) + 1), ref AcctLength));
							if (Row >= vs1.Rows - 1)
							{
								vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[3]), ref AcctLength));
								CheckThirdLevelFunds = false;
								return CheckThirdLevelFunds;
							}
							else
							{
								if (Conversion.Val(vs1.TextMatrix(Row, ThirdHighBoundCol)) == 0 || (vs1.RowOutlineLevel(Row + 1) == 2 && Conversion.Val(vs1.TextMatrix(Row + 1, ThirdLowBoundCol)) == 0) || vs1.RowOutlineLevel(Row + 1) == 1)
								{
									vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[3]), ref AcctLength));
									CheckThirdLevelFunds = false;
									return CheckThirdLevelFunds;
								}
								else
								{
									if (fecherFoundation.Strings.CompareString(vs1.TextMatrix(Row, ThirdHighBoundCol), vs1.TextMatrix(Row, ThirdLowBoundCol), true) <= 0)
									{
										vs1.TextMatrix(Row, ThirdHighBoundCol, modValidateAccount.GetFormat(Conversion.Str(FundLevel2High[3]), ref AcctLength));
										ClearRestOfSmallData(ref Row);
									}
									else
									{
										CheckThirdLevelFunds = false;
										return CheckThirdLevelFunds;
									}
								}
							}
						}
					}
				}
			}
			return CheckThirdLevelFunds;
		}

		private void ResetValues(int Row)
		{
			int max = 0;
			if (Row < 18)
			{
				max = 13;
			}
			else if (Row < 35)
			{
				max = 30;
			}
			else
			{
				max = 47;
			}
			for (Row = Row; Row <= max; Row += 4)
			{
				vs1.TextMatrix(Row + 1, ThirdLowBoundCol, vs1.TextMatrix(Row, SecondLowBoundCol));
				vs1.TextMatrix(Row + 1, ThirdHighBoundCol, vs1.TextMatrix(Row, SecondHighBoundCol));
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short ChooseCorrectCol(int OldLevel, int OldCol, int NewRow)
		{
			short ChooseCorrectCol = 0;
			if (OldLevel == 0)
			{
				if (OldCol == FirstHighBoundCol)
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(FirstHighBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondHighBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdHighBoundCol);
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondLowBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLowBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLabelCol);
					}
				}
			}
			else if (OldLevel == 1)
			{
				if (OldCol == SecondHighBoundCol)
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(FirstHighBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondHighBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdHighBoundCol);
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondLowBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLowBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLabelCol);
					}
				}
			}
			else
			{
				if (OldCol == ThirdHighBoundCol)
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(FirstHighBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondHighBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdHighBoundCol);
					}
				}
				else
				{
					if (vs1.RowOutlineLevel(NewRow) == 0)
					{
						ChooseCorrectCol = FCConvert.ToInt16(SecondLowBoundCol);
					}
					else if (vs1.RowOutlineLevel(NewRow) == 1)
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLowBoundCol);
					}
					else
					{
						ChooseCorrectCol = FCConvert.ToInt16(ThirdLabelCol);
					}
				}
			}
			return ChooseCorrectCol;
		}

		private string SpaceOut(string x)
		{
			string SpaceOut = "";
			if (x.Length < 18)
			{
				while (x.Length < 18)
				{
					x = " " + x + " ";
				}
			}
			SpaceOut = x;
			return SpaceOut;
		}

		private void SaveInfo()
		{
			int counter;
			string temp = "";
			int counter2 = 0;
			string SQLValues = "";
			int counter3;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strLow = "";
			string strHigh = "";
			rsTemp.Execute("DELETE FROM LedgerRanges", "Budgetary");
			for (counter = 1; counter <= 3; counter++)
			{
				//Application.DoEvents();
				if (counter == 1)
				{
					temp = "A";
					counter2 = 1;
				}
				else if (counter == 2)
				{
					temp = "L";
					counter2 = 18;
				}
				else
				{
					temp = "F";
					counter2 = 35;
				}
				SQLValues = "(";
				SQLValues += "'" + temp + "', '" + vs1.TextMatrix(counter2, FirstLowBoundCol) + "', '" + vs1.TextMatrix(counter2, FirstHighBoundCol) + "', ";
				strLow = vs1.TextMatrix(counter2, FirstLowBoundCol);
				strHigh = vs1.TextMatrix(counter2, FirstHighBoundCol);
				for (counter3 = 1; counter3 <= 16; counter3++)
				{
					//Application.DoEvents();
					if (vs1.RowOutlineLevel(counter2 + counter3) == 1)
					{
						if (strLow == vs1.TextMatrix(counter2 + counter3, SecondLowBoundCol) && strHigh == vs1.TextMatrix(counter2 + counter3, SecondHighBoundCol))
						{
							SQLValues += "'" + Strings.Trim(vs1.TextMatrix(counter2 + counter3, ThirdLowBoundCol)) + "', '" + modValidateAccount.GetFormat_6(FCConvert.ToString(0), FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "', '" + modValidateAccount.GetFormat_6(FCConvert.ToString(0), FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "', ";
						}
						else
						{
							SQLValues += "'" + Strings.Trim(vs1.TextMatrix(counter2 + counter3, ThirdLowBoundCol)) + "', '" + vs1.TextMatrix(counter2 + counter3, SecondLowBoundCol) + "', '" + vs1.TextMatrix(counter2 + counter3, SecondHighBoundCol) + "', ";
							strLow = vs1.TextMatrix(counter2 + counter3, SecondLowBoundCol);
							strHigh = vs1.TextMatrix(counter2 + counter3, SecondHighBoundCol);
						}
					}
					else
					{
						if (strLow == vs1.TextMatrix(counter2 + counter3, ThirdLowBoundCol) && strHigh == vs1.TextMatrix(counter2 + counter3, ThirdHighBoundCol))
						{
							SQLValues += "'" + Strings.Trim(vs1.TextMatrix(counter2 + counter3, ThirdLabelCol)) + "', '" + modValidateAccount.GetFormat_6(FCConvert.ToString(0), FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "', '" + modValidateAccount.GetFormat_6(FCConvert.ToString(0), FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "', ";
						}
						else
						{
							SQLValues += "'" + Strings.Trim(vs1.TextMatrix(counter2 + counter3, ThirdLabelCol)) + "', '" + vs1.TextMatrix(counter2 + counter3, ThirdLowBoundCol) + "', '" + vs1.TextMatrix(counter2 + counter3, ThirdHighBoundCol) + "', ";
							strLow = vs1.TextMatrix(counter2 + counter3, ThirdLowBoundCol);
							strHigh = vs1.TextMatrix(counter2 + counter3, ThirdHighBoundCol);
						}
					}
				}
				SQLValues = Strings.Mid(SQLValues, 1, SQLValues.Length - 2);
				SQLValues += ")";
				rsTemp.Execute("INSERT INTO LedgerRanges VALUES " + SQLValues, "Budgetary");
			}
			frmWait.InstancePtr.Unload();
			if (blnUnload)
			{
				Close();
			}
			else
			{
				MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}
	}
}
