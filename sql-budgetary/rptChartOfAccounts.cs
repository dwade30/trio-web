﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptChartOfAccounts.
	/// </summary>
	public partial class rptChartOfAccounts : BaseSectionReport
	{
		public static rptChartOfAccounts InstancePtr
		{
			get
			{
				return (rptChartOfAccounts)Sys.GetInstance(typeof(rptChartOfAccounts));
			}
		}

		protected rptChartOfAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccounts.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptChartOfAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		clsDRWrapper rsAccounts = new clsDRWrapper();
		string strAccountType = "";
		bool blnFirstRecord;
		string strCurrentDept = "";
		string strCurrentDiv = "";
		string strOldDept = "";
		string strOldDiv = "";

		public rptChartOfAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Chart of Accounts";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsAccounts.EndOfFile() != true)
				{
					if (strAccountType == "R")
					{
						this.Fields["Binder"].Value = rsAccounts.Get_Fields_String("Department") + rsAccounts.Get_Fields_String("Division");
						strOldDept = strCurrentDept;
						strOldDiv = strCurrentDiv;
						strCurrentDept = FCConvert.ToString(rsAccounts.Get_Fields_String("Department"));
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							strCurrentDiv = FCConvert.ToString(rsAccounts.Get_Fields_String("Division"));
						}
					}
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				rsAccounts.MoveNext();
				if (rsAccounts.EndOfFile() != true)
				{
					if (strAccountType == "R")
					{
						this.Fields["Binder"].Value = rsAccounts.Get_Fields_String("Department") + rsAccounts.Get_Fields_String("Division");
						strOldDept = strCurrentDept;
						strOldDiv = strCurrentDiv;
						strCurrentDept = FCConvert.ToString(rsAccounts.Get_Fields_String("Department"));
						if (!modAccountTitle.Statics.RevDivFlag)
						{
							strCurrentDiv = FCConvert.ToString(rsAccounts.Get_Fields_String("Division"));
						}
					}
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmChartOfAccounts.InstancePtr.cmbDepartment.SelectedIndex == 0)
			{
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					lblTitle1.Text = "Department";
					lblAccoutType.Text = "Dept";
				}
				else
				{
					lblTitle1.Text = "Department / Division";
					lblAccoutType.Text = "Dept / Div";
				}
				strAccountType = "D";
				if (modRegionalTown.IsRegionalTown())
				{
					if (frmChartOfAccounts.InstancePtr.chkRegionalOption.CheckState == CheckState.Checked)
					{
						rsAccounts.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Left(Department, 1) = '1' ORDER BY Department, Division");
					}
					else
					{
						rsAccounts.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department, Division");
					}
				}
				else
				{
					rsAccounts.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department, Division");
				}
				lblExtraInfo.Text = "Fund";
			}
			else if (frmChartOfAccounts.InstancePtr.cmbDepartment.SelectedIndex == 1)
			{
				if (modAccountTitle.Statics.ObjFlag)
				{
					lblTitle1.Text = "Expense";
					lblAccoutType.Text = "Exp";
				}
				else
				{
					lblTitle1.Text = "Expense / Object";
					lblAccoutType.Text = "Exp / Obj";
				}
				strAccountType = "E";
				rsAccounts.OpenRecordset("SELECT * FROM ExpObjTitles ORDER BY Expense, Object");
				lblExtraInfo.Text = "1099";
			}
			else if (frmChartOfAccounts.InstancePtr.cmbDepartment.SelectedIndex == 2)
			{
				lblTitle1.Text = "General Ledger";
				strAccountType = "G";
				rsAccounts.OpenRecordset("SELECT * FROM LedgerTitles ORDER BY Fund, Account, Year");
				lblAccoutType.Text = "Fund / Acct";
				lblExtraInfo.Text = "";
			}
			else if (frmChartOfAccounts.InstancePtr.cmbDepartment.SelectedIndex == 3)
			{
				lblTitle1.Text = "Revenues";
				strAccountType = "R";
				if (modAccountTitle.Statics.RevDivFlag)
				{
					rsAccounts.OpenRecordset("SELECT * FROM RevTitles ORDER BY Department, Revenue");
					lblAccoutType.Text = "Dept / Rev";
				}
				else
				{
					rsAccounts.OpenRecordset("SELECT * FROM RevTitles ORDER BY Department, Division, Revenue");
					lblAccoutType.Text = "Dept / Div / Rev";
				}
				lblExtraInfo.Text = "";
			}
			blnFirstRecord = true;
			if (rsAccounts.EndOfFile() != true && rsAccounts.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmChartOfAccounts.InstancePtr.Show(App.MainForm);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldAccountType As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldExtraInfo As object	OnWrite(object, string)
			// vbPorter upgrade warning: fldDescription As object	OnWrite(object, string)
			if (strAccountType == "D")
			{
				if (Conversion.Val(rsAccounts.Get_Fields_String("Division")) == 0)
				{
					fldAccountType.Text = rsAccounts.Get_Fields_String("Department");
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					fldExtraInfo.Text = FCConvert.ToString(rsAccounts.Get_Fields("Fund"));
					fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
				}
				else
				{
					fldAccountType.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))), " ") + "--" + rsAccounts.Get_Fields_String("Division");
					fldExtraInfo.Text = "";
					fldDescription.Text = "     " + rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Regular);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Regular);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Regular);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Regular);
				}
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
			}
			else if (strAccountType == "E")
			{
				if (Conversion.Val(rsAccounts.Get_Fields_String("Object")) == 0)
				{
					fldAccountType.Text = rsAccounts.Get_Fields_String("Expense");
					fldExtraInfo.Text = "";
					fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
				}
				else
				{
					fldAccountType.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2))), " ") + "--" + rsAccounts.Get_Fields_String("Object");
					// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
					fldExtraInfo.Text = FCConvert.ToString(rsAccounts.Get_Fields("Tax"));
					fldDescription.Text = "     " + rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Regular);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Regular);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Regular);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Regular);
				}
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
			}
			else if (strAccountType == "G")
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				if (Conversion.Val(rsAccounts.Get_Fields("Account")) == 0)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					fldAccountType.Text = FCConvert.ToString(rsAccounts.Get_Fields("Fund"));
					fldExtraInfo.Visible = false;
					fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
				}
				else
				{
					if (modAccountTitle.Statics.YearFlag)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						fldAccountType.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))), " ") + "--" + rsAccounts.Get_Fields("Account");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
						fldAccountType.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2))), " ") + "--" + rsAccounts.Get_Fields("Account") + "--" + rsAccounts.Get_Fields("Year");
					}
					fldExtraInfo.Visible = false;
					fldDescription.Text = "     " + rsAccounts.Get_Fields_String("ShortDescription");
					fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Regular);
					fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Regular);
					fldDescription.Font = new Font(fldDescription.Font, FontStyle.Regular);
					fldHeading.Font = new Font(fldHeading.Font, FontStyle.Regular);
				}
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
			}
			else if (strAccountType == "R")
			{
				fldAccountType.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 1, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 4), " ") + "--" + rsAccounts.Get_Fields_String("Revenue");
				fldExtraInfo.Visible = false;
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					fldDescription.Text = "          " + rsAccounts.Get_Fields_String("ShortDescription");
				}
				else
				{
					fldDescription.Text = "     " + rsAccounts.Get_Fields_String("ShortDescription");
				}
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
			}
			else if (strAccountType == "SF")
			{
				// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
				fldAccountType.Text = FCConvert.ToString(rsAccounts.Get_Fields("Fund"));
				fldExtraInfo.Text = rsAccounts.Get_Fields_String("DueToFromFundCode");
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SP")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("Program");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SN")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("Function");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SO")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("Object");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SC")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("CostCenter");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SR")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("Revenue");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
			else if (strAccountType == "SB")
			{
				fldAccountType.Text = rsAccounts.Get_Fields_String("BalanceSheetCode") + "-" + rsAccounts.Get_Fields_String("Suffix");
				fldExtraInfo.Text = "";
				fldDescription.Text = rsAccounts.Get_Fields_String("ShortDescription");
				fldHeading.Text = rsAccounts.Get_Fields_String("LongDescription");
				fldAccountType.Font = new Font(fldAccountType.Font, FontStyle.Bold);
				fldExtraInfo.Font = new Font(fldExtraInfo.Font, FontStyle.Bold);
				fldDescription.Font = new Font(fldDescription.Font, FontStyle.Bold);
				fldHeading.Font = new Font(fldHeading.Font, FontStyle.Bold);
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsDeptInfo = new clsDRWrapper();
			if (strAccountType == "R")
			{
				if (strOldDept != strCurrentDept)
				{
					rsDeptInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strCurrentDept + "' AND Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "'");
					if (rsDeptInfo.EndOfFile() != true && rsDeptInfo.BeginningOfFile() != true)
					{
						fldHeadingAccountType1.Text = rsDeptInfo.Get_Fields_String("Department");
						fldHeadingDescription1.Text = rsDeptInfo.Get_Fields_String("ShortDescription");
						fldHeadingHeading1.Text = rsDeptInfo.Get_Fields_String("LongDescription");
						fldHeadingExtraInfo1.Visible = false;
					}
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rsDeptInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strCurrentDept + "' AND Division = '" + strCurrentDiv + "'");
						if (rsDeptInfo.EndOfFile() != true && rsDeptInfo.BeginningOfFile() != true)
						{
							fldHeadingAccountType2.Visible = true;
							fldHeadingDescription2.Visible = true;
							fldHeadingHeading2.Visible = true;
							fldHeadingAccountType2.Font = new Font(fldHeadingAccountType2.Font, FontStyle.Bold);
							fldHeadingDescription2.Font = new Font(fldHeadingDescription2.Font, FontStyle.Bold);
							fldHeadingHeading2.Font = new Font(fldHeadingHeading2.Font, FontStyle.Bold);
							fldHeadingAccountType2.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))), " ") + "--" + rsDeptInfo.Get_Fields_String("Division");
							fldHeadingDescription2.Text = "     " + rsDeptInfo.Get_Fields_String("ShortDescription");
							fldHeadingHeading2.Text = rsDeptInfo.Get_Fields_String("LongDescription");
							fldHeadingExtraInfo2.Visible = false;
						}
						else
						{
							fldHeadingAccountType2.Visible = false;
							fldHeadingDescription2.Visible = false;
							fldHeadingHeading2.Visible = false;
							fldHeadingExtraInfo2.Visible = false;
						}
					}
					else
					{
						fldHeadingAccountType2.Visible = false;
						fldHeadingDescription2.Visible = false;
						fldHeadingHeading2.Visible = false;
						fldHeadingExtraInfo2.Visible = false;
					}
				}
				else
				{
					fldHeadingAccountType2.Visible = false;
					fldHeadingDescription2.Visible = false;
					fldHeadingHeading2.Visible = false;
					fldHeadingExtraInfo2.Visible = false;
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rsDeptInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + strCurrentDept + "' AND Division = '" + strCurrentDiv + "'");
						if (rsDeptInfo.EndOfFile() != true && rsDeptInfo.BeginningOfFile() != true)
						{
							fldHeadingAccountType1.Text = Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))), " ") + "--" + rsDeptInfo.Get_Fields_String("Division");
							fldHeadingDescription1.Text = "     " + rsDeptInfo.Get_Fields_String("ShortDescription");
							fldHeadingHeading1.Text = rsDeptInfo.Get_Fields_String("LongDescription");
							fldHeadingExtraInfo1.Visible = false;
						}
						else
						{
							fldHeadingAccountType1.Visible = false;
							fldHeadingDescription1.Visible = false;
							fldHeadingHeading1.Visible = false;
							fldHeadingExtraInfo1.Visible = false;
						}
					}
					else
					{
						fldHeadingAccountType1.Visible = false;
						fldHeadingDescription1.Visible = false;
						fldHeadingHeading1.Visible = false;
						fldHeadingExtraInfo1.Visible = false;
					}
				}
			}
			else
			{
				fldHeadingAccountType1.Visible = false;
				fldHeadingAccountType2.Visible = false;
				fldHeadingDescription1.Visible = false;
				fldHeadingDescription2.Visible = false;
				fldHeadingHeading1.Visible = false;
				fldHeadingHeading2.Visible = false;
				fldHeadingExtraInfo1.Visible = false;
				fldHeadingExtraInfo2.Visible = false;
			}
			rsDeptInfo.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void rptChartOfAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptChartOfAccounts.Caption	= "Chart of Accounts";
			//rptChartOfAccounts.Icon	= "rptChartOfAccounts.dsx":0000";
			//rptChartOfAccounts.Left	= 0;
			//rptChartOfAccounts.Top	= 0;
			//rptChartOfAccounts.Width	= 11880;
			//rptChartOfAccounts.Height	= 8595;
			//rptChartOfAccounts.StartUpPosition	= 3;
			//rptChartOfAccounts.SectionData	= "rptChartOfAccounts.dsx":058A;
			//End Unmaped Properties
		}

		private void rptChartOfAccounts_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
