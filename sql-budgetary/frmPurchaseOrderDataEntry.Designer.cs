//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurchaseOrderDataEntry.
	/// </summary>
	partial class frmPurchaseOrderDataEntry : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtTownAddress;
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtShipToAddress;
		public fecherFoundation.FCRichTextBox txtComments;
		public fecherFoundation.FCFrame fraTownInfo;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancelPrint;
		public Global.T2KOverTypeBox txtTownName;
		public Global.T2KOverTypeBox txtTownAddress_0;
		public Global.T2KOverTypeBox txtTownAddress_1;
		public Global.T2KOverTypeBox txtTownAddress_2;
		public Global.T2KOverTypeBox txtTownAddress_3;
		public Global.T2KOverTypeBox txtShipToName;
		public Global.T2KOverTypeBox txtShipToAddress_0;
		public Global.T2KOverTypeBox txtShipToAddress_1;
		public Global.T2KOverTypeBox txtShipToAddress_2;
		public Global.T2KOverTypeBox txtShipToAddress_3;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraAccountBalance;
		public fecherFoundation.FCButton cmdBalanceOK;
		public fecherFoundation.FCLabel lblBalPendingYTDNet;
		public fecherFoundation.FCLabel lblBalBalance;
		public fecherFoundation.FCLabel lblBalPostedYTDNet;
		public fecherFoundation.FCLabel lblBalNetBudget;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblBalAccount;
		public fecherFoundation.FCFrame frmSearch;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCFrame frmInfo;
		public fecherFoundation.FCListBox lstRecords;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCButton cmdRetrieve;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblRecordNumber;
		public fecherFoundation.FCComboBox cboDepartment;
		public Global.T2KOverTypeBox txtVendor;
		public Global.T2KOverTypeBox txtDescription;
		public Global.T2KDateBox txtDate;
		public FCGrid vs1;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel lblAddress4;
		public fecherFoundation.FCLabel lblAddress3;
		public fecherFoundation.FCLabel lblAddress2;
		public fecherFoundation.FCLabel lblAddress1;
		public fecherFoundation.FCLabel lblVendorName;
		public fecherFoundation.FCLabel lblTotalAmount;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPurchaseOrder;
		public fecherFoundation.FCLabel lblExpense;
		public fecherFoundation.FCLabel lblDate1;
		public fecherFoundation.FCLabel lblPO;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblPeriod;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAccounts;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator5;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteEntry;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator6;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddVendor;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator7;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPreviousEntry;
		public fecherFoundation.FCToolStripMenuItem mnuProcessNextEntry;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator4;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurchaseOrderDataEntry));
			this.txtComments = new fecherFoundation.FCRichTextBox();
			this.fraTownInfo = new fecherFoundation.FCFrame();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancelPrint = new fecherFoundation.FCButton();
			this.txtTownName = new Global.T2KOverTypeBox();
			this.txtTownAddress_0 = new Global.T2KOverTypeBox();
			this.txtTownAddress_1 = new Global.T2KOverTypeBox();
			this.txtTownAddress_2 = new Global.T2KOverTypeBox();
			this.txtTownAddress_3 = new Global.T2KOverTypeBox();
			this.txtShipToName = new Global.T2KOverTypeBox();
			this.txtShipToAddress_0 = new Global.T2KOverTypeBox();
			this.txtShipToAddress_1 = new Global.T2KOverTypeBox();
			this.txtShipToAddress_2 = new Global.T2KOverTypeBox();
			this.txtShipToAddress_3 = new Global.T2KOverTypeBox();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraAccountBalance = new fecherFoundation.FCFrame();
			this.cmdBalanceOK = new fecherFoundation.FCButton();
			this.lblBalPendingYTDNet = new fecherFoundation.FCLabel();
			this.lblBalBalance = new fecherFoundation.FCLabel();
			this.lblBalPostedYTDNet = new fecherFoundation.FCLabel();
			this.lblBalNetBudget = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblBalAccount = new fecherFoundation.FCLabel();
			this.frmSearch = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.frmInfo = new fecherFoundation.FCFrame();
			this.lstRecords = new fecherFoundation.FCListBox();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.cmdRetrieve = new fecherFoundation.FCButton();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblRecordNumber = new fecherFoundation.FCLabel();
			this.cboDepartment = new fecherFoundation.FCComboBox();
			this.txtVendor = new Global.T2KOverTypeBox();
			this.txtDescription = new Global.T2KOverTypeBox();
			this.txtDate = new Global.T2KDateBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.Label11 = new fecherFoundation.FCLabel();
			this.lblAddress4 = new fecherFoundation.FCLabel();
			this.lblAddress3 = new fecherFoundation.FCLabel();
			this.lblAddress2 = new fecherFoundation.FCLabel();
			this.lblAddress1 = new fecherFoundation.FCLabel();
			this.lblVendorName = new fecherFoundation.FCLabel();
			this.lblTotalAmount = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblPurchaseOrder = new fecherFoundation.FCLabel();
			this.lblExpense = new fecherFoundation.FCLabel();
			this.lblDate1 = new fecherFoundation.FCLabel();
			this.lblPO = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileSeperator5 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessDeleteEntry = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator6 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAddVendor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessAccounts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator7 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPreviousEntry = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessNextEntry = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator4 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnFileSave = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdProcessNextEntry = new fecherFoundation.FCButton();
			this.cmdProcessPreviousEntry = new fecherFoundation.FCButton();
			this.cmdVendorSearch = new fecherFoundation.FCButton();
			this.cmdAddVendor = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtComments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).BeginInit();
			this.fraTownInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).BeginInit();
			this.fraAccountBalance.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).BeginInit();
			this.frmSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).BeginInit();
			this.frmInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNextEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPreviousEntry)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVendorSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddVendor)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 540);
			this.BottomPanel.Size = new System.Drawing.Size(865, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtComments);
			this.ClientArea.Controls.Add(this.fraTownInfo);
			this.ClientArea.Controls.Add(this.fraAccountBalance);
			this.ClientArea.Controls.Add(this.frmSearch);
			this.ClientArea.Controls.Add(this.frmInfo);
			this.ClientArea.Controls.Add(this.cboDepartment);
			this.ClientArea.Controls.Add(this.txtVendor);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.Label11);
			this.ClientArea.Controls.Add(this.lblAddress4);
			this.ClientArea.Controls.Add(this.lblAddress3);
			this.ClientArea.Controls.Add(this.lblAddress2);
			this.ClientArea.Controls.Add(this.lblAddress1);
			this.ClientArea.Controls.Add(this.lblVendorName);
			this.ClientArea.Controls.Add(this.lblTotalAmount);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblPurchaseOrder);
			this.ClientArea.Controls.Add(this.lblExpense);
			this.ClientArea.Controls.Add(this.lblDate1);
			this.ClientArea.Controls.Add(this.lblPO);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblVendor);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Size = new System.Drawing.Size(865, 480);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAddVendor);
			this.TopPanel.Controls.Add(this.cmdVendorSearch);
			this.TopPanel.Controls.Add(this.cmdProcessPreviousEntry);
			this.TopPanel.Controls.Add(this.cmdProcessNextEntry);
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(865, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessNextEntry, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessPreviousEntry, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdVendorSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddVendor, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(198, 30);
			this.HeaderText.Text = "Purchase Orders";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// txtComments
			// 
			this.txtComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.txtComments.Location = new System.Drawing.Point(30, 590);
			this.txtComments.Multiline = true;
			this.txtComments.Name = "txtComments";
			this.txtComments.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.txtComments.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.txtComments.SelTabCount = null;
			this.txtComments.Size = new System.Drawing.Size(402, 80);
			this.txtComments.TabIndex = 58;
			this.ToolTip1.SetToolTip(this.txtComments, null);
			// 
			// fraTownInfo
			// 
			this.fraTownInfo.BackColor = System.Drawing.Color.White;
			this.fraTownInfo.Controls.Add(this.cmdOK);
			this.fraTownInfo.Controls.Add(this.cmdCancelPrint);
			this.fraTownInfo.Controls.Add(this.txtTownName);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_0);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_1);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_2);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_3);
			this.fraTownInfo.Controls.Add(this.txtShipToName);
			this.fraTownInfo.Controls.Add(this.txtShipToAddress_0);
			this.fraTownInfo.Controls.Add(this.txtShipToAddress_1);
			this.fraTownInfo.Controls.Add(this.txtShipToAddress_2);
			this.fraTownInfo.Controls.Add(this.txtShipToAddress_3);
			this.fraTownInfo.Controls.Add(this.Label6);
			this.fraTownInfo.Controls.Add(this.Label5);
			this.fraTownInfo.Controls.Add(this.Label3);
			this.fraTownInfo.Controls.Add(this.Label2);
			this.fraTownInfo.Location = new System.Drawing.Point(826, 30);
			this.fraTownInfo.Name = "fraTownInfo";
			this.fraTownInfo.Size = new System.Drawing.Size(603, 410);
			this.fraTownInfo.TabIndex = 19;
			this.fraTownInfo.Text = "Enter Town Information";
			this.ToolTip1.SetToolTip(this.fraTownInfo, null);
			this.fraTownInfo.Visible = false;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(237, 366);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(70, 40);
			this.cmdOK.TabIndex = 14;
			this.cmdOK.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdOK, null);
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancelPrint
			// 
			this.cmdCancelPrint.AppearanceKey = "actionButton";
			this.cmdCancelPrint.ForeColor = System.Drawing.Color.White;
			this.cmdCancelPrint.Location = new System.Drawing.Point(313, 366);
			this.cmdCancelPrint.Name = "cmdCancelPrint";
			this.cmdCancelPrint.Size = new System.Drawing.Size(70, 40);
			this.cmdCancelPrint.TabIndex = 15;
			this.cmdCancelPrint.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancelPrint, null);
			this.cmdCancelPrint.Click += new System.EventHandler(this.cmdCancelPrint_Click);
			// 
			// txtTownName
			// 
			this.txtTownName.AutoSize = false;
			this.txtTownName.LinkItem = null;
			this.txtTownName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTownName.LinkTopic = null;
			this.txtTownName.Location = new System.Drawing.Point(155, 66);
			this.txtTownName.MaxLength = 35;
			this.txtTownName.Name = "txtTownName";
			this.txtTownName.Size = new System.Drawing.Size(209, 40);
			this.txtTownName.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtTownName, null);
			// 
			// txtTownAddress_0
			// 
			this.txtTownAddress_0.AutoSize = false;
			this.txtTownAddress_0.LinkItem = null;
			this.txtTownAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTownAddress_0.LinkTopic = null;
			this.txtTownAddress_0.Location = new System.Drawing.Point(155, 126);
			this.txtTownAddress_0.MaxLength = 35;
			this.txtTownAddress_0.Name = "txtTownAddress_0";
			this.txtTownAddress_0.Size = new System.Drawing.Size(209, 40);
			this.txtTownAddress_0.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtTownAddress_0, null);
			// 
			// txtTownAddress_1
			// 
			this.txtTownAddress_1.AutoSize = false;
			this.txtTownAddress_1.LinkItem = null;
			this.txtTownAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTownAddress_1.LinkTopic = null;
			this.txtTownAddress_1.Location = new System.Drawing.Point(155, 186);
			this.txtTownAddress_1.MaxLength = 35;
			this.txtTownAddress_1.Name = "txtTownAddress_1";
			this.txtTownAddress_1.Size = new System.Drawing.Size(209, 40);
			this.txtTownAddress_1.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.txtTownAddress_1, null);
			// 
			// txtTownAddress_2
			// 
			this.txtTownAddress_2.AutoSize = false;
			this.txtTownAddress_2.LinkItem = null;
			this.txtTownAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTownAddress_2.LinkTopic = null;
			this.txtTownAddress_2.Location = new System.Drawing.Point(155, 246);
			this.txtTownAddress_2.MaxLength = 35;
			this.txtTownAddress_2.Name = "txtTownAddress_2";
			this.txtTownAddress_2.Size = new System.Drawing.Size(209, 40);
			this.txtTownAddress_2.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.txtTownAddress_2, null);
			// 
			// txtTownAddress_3
			// 
			this.txtTownAddress_3.AutoSize = false;
			this.txtTownAddress_3.LinkItem = null;
			this.txtTownAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTownAddress_3.LinkTopic = null;
			this.txtTownAddress_3.Location = new System.Drawing.Point(155, 306);
			this.txtTownAddress_3.MaxLength = 35;
			this.txtTownAddress_3.Name = "txtTownAddress_3";
			this.txtTownAddress_3.Size = new System.Drawing.Size(209, 40);
			this.txtTownAddress_3.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.txtTownAddress_3, null);
			// 
			// txtShipToName
			// 
			this.txtShipToName.AutoSize = false;
			this.txtShipToName.LinkItem = null;
			this.txtShipToName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtShipToName.LinkTopic = null;
			this.txtShipToName.Location = new System.Drawing.Point(386, 66);
			this.txtShipToName.MaxLength = 35;
			this.txtShipToName.Name = "txtShipToName";
			this.txtShipToName.Size = new System.Drawing.Size(209, 40);
			this.txtShipToName.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtShipToName, null);
			// 
			// txtShipToAddress_0
			// 
			this.txtShipToAddress_0.AutoSize = false;
			this.txtShipToAddress_0.LinkItem = null;
			this.txtShipToAddress_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtShipToAddress_0.LinkTopic = null;
			this.txtShipToAddress_0.Location = new System.Drawing.Point(386, 126);
			this.txtShipToAddress_0.MaxLength = 35;
			this.txtShipToAddress_0.Name = "txtShipToAddress_0";
			this.txtShipToAddress_0.Size = new System.Drawing.Size(209, 40);
			this.txtShipToAddress_0.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtShipToAddress_0, null);
			// 
			// txtShipToAddress_1
			// 
			this.txtShipToAddress_1.AutoSize = false;
			this.txtShipToAddress_1.LinkItem = null;
			this.txtShipToAddress_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtShipToAddress_1.LinkTopic = null;
			this.txtShipToAddress_1.Location = new System.Drawing.Point(386, 186);
			this.txtShipToAddress_1.MaxLength = 35;
			this.txtShipToAddress_1.Name = "txtShipToAddress_1";
			this.txtShipToAddress_1.Size = new System.Drawing.Size(209, 40);
			this.txtShipToAddress_1.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtShipToAddress_1, null);
			// 
			// txtShipToAddress_2
			// 
			this.txtShipToAddress_2.AutoSize = false;
			this.txtShipToAddress_2.LinkItem = null;
			this.txtShipToAddress_2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtShipToAddress_2.LinkTopic = null;
			this.txtShipToAddress_2.Location = new System.Drawing.Point(386, 246);
			this.txtShipToAddress_2.MaxLength = 35;
			this.txtShipToAddress_2.Name = "txtShipToAddress_2";
			this.txtShipToAddress_2.Size = new System.Drawing.Size(209, 40);
			this.txtShipToAddress_2.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.txtShipToAddress_2, null);
			// 
			// txtShipToAddress_3
			// 
			this.txtShipToAddress_3.AutoSize = false;
			this.txtShipToAddress_3.LinkItem = null;
			this.txtShipToAddress_3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtShipToAddress_3.LinkTopic = null;
			this.txtShipToAddress_3.Location = new System.Drawing.Point(386, 306);
			this.txtShipToAddress_3.MaxLength = 35;
			this.txtShipToAddress_3.Name = "txtShipToAddress_3";
			this.txtShipToAddress_3.Size = new System.Drawing.Size(209, 40);
			this.txtShipToAddress_3.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.txtShipToAddress_3, null);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(386, 30);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(58, 16);
			this.Label6.TabIndex = 3;
			this.Label6.Text = "SHIP TO";
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(155, 30);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(51, 16);
			this.Label5.TabIndex = 0;
			this.Label5.Text = "BILL TO";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 80);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(42, 16);
			this.Label3.TabIndex = 1;
			this.Label3.Text = "NAME";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 140);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(65, 16);
			this.Label2.TabIndex = 5;
			this.Label2.Text = "ADDRESS";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// fraAccountBalance
			// 
			this.fraAccountBalance.BackColor = System.Drawing.Color.White;
			this.fraAccountBalance.Controls.Add(this.cmdBalanceOK);
			this.fraAccountBalance.Controls.Add(this.lblBalPendingYTDNet);
			this.fraAccountBalance.Controls.Add(this.lblBalBalance);
			this.fraAccountBalance.Controls.Add(this.lblBalPostedYTDNet);
			this.fraAccountBalance.Controls.Add(this.lblBalNetBudget);
			this.fraAccountBalance.Controls.Add(this.Label10);
			this.fraAccountBalance.Controls.Add(this.Label9);
			this.fraAccountBalance.Controls.Add(this.Label8);
			this.fraAccountBalance.Controls.Add(this.Label7);
			this.fraAccountBalance.Controls.Add(this.lblBalAccount);
			this.fraAccountBalance.Location = new System.Drawing.Point(806, 30);
			this.fraAccountBalance.Name = "fraAccountBalance";
			this.fraAccountBalance.Size = new System.Drawing.Size(378, 262);
			this.fraAccountBalance.TabIndex = 17;
			this.fraAccountBalance.Text = "Account Balance";
			this.ToolTip1.SetToolTip(this.fraAccountBalance, null);
			this.fraAccountBalance.Visible = false;
			// 
			// cmdBalanceOK
			// 
			this.cmdBalanceOK.AppearanceKey = "actionButton";
			this.cmdBalanceOK.ForeColor = System.Drawing.Color.White;
			this.cmdBalanceOK.Location = new System.Drawing.Point(66, 210);
			this.cmdBalanceOK.Name = "cmdBalanceOK";
			this.cmdBalanceOK.Size = new System.Drawing.Size(85, 40);
			this.cmdBalanceOK.TabIndex = 9;
			this.cmdBalanceOK.Text = "OK";
			this.ToolTip1.SetToolTip(this.cmdBalanceOK, null);
			// 
			// lblBalPendingYTDNet
			// 
			this.lblBalPendingYTDNet.Location = new System.Drawing.Point(194, 138);
			this.lblBalPendingYTDNet.Name = "lblBalPendingYTDNet";
			this.lblBalPendingYTDNet.Size = new System.Drawing.Size(115, 16);
			this.lblBalPendingYTDNet.TabIndex = 6;
			this.lblBalPendingYTDNet.Text = "PENDING YTD NET";
			this.lblBalPendingYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalPendingYTDNet, null);
			// 
			// lblBalBalance
			// 
			this.lblBalBalance.Location = new System.Drawing.Point(248, 174);
			this.lblBalBalance.Name = "lblBalBalance";
			this.lblBalBalance.Size = new System.Drawing.Size(61, 16);
			this.lblBalBalance.TabIndex = 8;
			this.lblBalBalance.Text = "BALANCE";
			this.lblBalBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalBalance, null);
			// 
			// lblBalPostedYTDNet
			// 
			this.lblBalPostedYTDNet.Location = new System.Drawing.Point(200, 102);
			this.lblBalPostedYTDNet.Name = "lblBalPostedYTDNet";
			this.lblBalPostedYTDNet.Size = new System.Drawing.Size(109, 16);
			this.lblBalPostedYTDNet.TabIndex = 4;
			this.lblBalPostedYTDNet.Text = "POSTED YTD NET";
			this.lblBalPostedYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalPostedYTDNet, null);
			// 
			// lblBalNetBudget
			// 
			this.lblBalNetBudget.Location = new System.Drawing.Point(223, 66);
			this.lblBalNetBudget.Name = "lblBalNetBudget";
			this.lblBalNetBudget.Size = new System.Drawing.Size(86, 16);
			this.lblBalNetBudget.TabIndex = 2;
			this.lblBalNetBudget.Text = "99,999,999.00";
			this.lblBalNetBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblBalNetBudget, null);
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(29, 138);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(115, 16);
			this.Label10.TabIndex = 5;
			this.Label10.Text = "PENDING YTD NET";
			this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label10, null);
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(83, 174);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(61, 16);
			this.Label9.TabIndex = 7;
			this.Label9.Text = "BALANCE";
			this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label9, null);
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(35, 102);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(109, 16);
			this.Label8.TabIndex = 3;
			this.Label8.Text = "POSTED YTD NET";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label8, null);
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 66);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(124, 16);
			this.Label7.TabIndex = 1;
			this.Label7.Text = "NET BUDGET";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.Label7, null);
			// 
			// lblBalAccount
			// 
			this.lblBalAccount.Location = new System.Drawing.Point(20, 30);
			this.lblBalAccount.Name = "lblBalAccount";
			this.lblBalAccount.Size = new System.Drawing.Size(64, 16);
			this.lblBalAccount.TabIndex = 0;
			this.lblBalAccount.Text = "ACCOUNT";
			this.lblBalAccount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblBalAccount, null);
			// 
			// frmSearch
			// 
			this.frmSearch.BackColor = System.Drawing.Color.White;
			this.frmSearch.Controls.Add(this.cmdCancel);
			this.frmSearch.Controls.Add(this.cmdSearch);
			this.frmSearch.Controls.Add(this.txtSearch);
			this.frmSearch.Location = new System.Drawing.Point(800, 30);
			this.frmSearch.Name = "frmSearch";
			this.frmSearch.Size = new System.Drawing.Size(298, 152);
			this.frmSearch.TabIndex = 40;
			this.frmSearch.Text = "Vendor Search";
			this.ToolTip1.SetToolTip(this.frmSearch, null);
			this.frmSearch.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(122, 90);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(94, 40);
			this.cmdCancel.TabIndex = 1;
			this.cmdCancel.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.cmdCancel, null);
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.AppearanceKey = "actionButton";
			this.cmdSearch.ForeColor = System.Drawing.Color.White;
			this.cmdSearch.Location = new System.Drawing.Point(20, 90);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(88, 40);
			this.cmdSearch.TabIndex = 0;
			this.cmdSearch.Text = "Search";
			this.ToolTip1.SetToolTip(this.cmdSearch, null);
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.LinkItem = null;
			this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearch.LinkTopic = null;
			this.txtSearch.Location = new System.Drawing.Point(20, 30);
			this.txtSearch.MaxLength = 35;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(258, 40);
			this.txtSearch.TabIndex = 41;
			this.ToolTip1.SetToolTip(this.txtSearch, null);
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// frmInfo
			// 
			this.frmInfo.BackColor = System.Drawing.Color.White;
			this.frmInfo.Controls.Add(this.lstRecords);
			this.frmInfo.Controls.Add(this.cmdReturn);
			this.frmInfo.Controls.Add(this.cmdRetrieve);
			this.frmInfo.Controls.Add(this.Label4);
			this.frmInfo.Controls.Add(this.lblRecordNumber);
			this.frmInfo.Location = new System.Drawing.Point(793, 10);
			this.frmInfo.Name = "frmInfo";
			this.frmInfo.Size = new System.Drawing.Size(721, 430);
			this.frmInfo.TabIndex = 18;
			this.frmInfo.Text = "Multiple Records";
			this.ToolTip1.SetToolTip(this.frmInfo, null);
			this.frmInfo.Visible = false;
			// 
			// lstRecords
			// 
			this.lstRecords.Appearance = 0;
			this.lstRecords.BackColor = System.Drawing.SystemColors.Window;
			this.lstRecords.Location = new System.Drawing.Point(20, 66);
			this.lstRecords.MultiSelect = 0;
			this.lstRecords.Name = "lstRecords";
			this.lstRecords.Size = new System.Drawing.Size(681, 290);
			this.lstRecords.Sorted = false;
			this.lstRecords.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lstRecords, null);
			this.lstRecords.DoubleClick += new System.EventHandler(this.lstRecords_DoubleClick);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.ForeColor = System.Drawing.Color.White;
			this.cmdReturn.Location = new System.Drawing.Point(170, 372);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(67, 40);
			this.cmdReturn.TabIndex = 2;
			this.cmdReturn.Text = "Cancel ";
			this.ToolTip1.SetToolTip(this.cmdReturn, null);
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// cmdRetrieve
			// 
			this.cmdRetrieve.AppearanceKey = "actionButton";
			this.cmdRetrieve.ForeColor = System.Drawing.Color.White;
			this.cmdRetrieve.Location = new System.Drawing.Point(20, 372);
			this.cmdRetrieve.Name = "cmdRetrieve";
			this.cmdRetrieve.Size = new System.Drawing.Size(142, 40);
			this.cmdRetrieve.TabIndex = 1;
			this.cmdRetrieve.Text = "Retrieve Record";
			this.ToolTip1.SetToolTip(this.cmdRetrieve, null);
			this.cmdRetrieve.Click += new System.EventHandler(this.cmdRetrieve_Click);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(158, 30);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(42, 14);
			this.Label4.TabIndex = 39;
			this.Label4.Text = "NAME";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// lblRecordNumber
			// 
			this.lblRecordNumber.Location = new System.Drawing.Point(20, 30);
			this.lblRecordNumber.Name = "lblRecordNumber";
			this.lblRecordNumber.Size = new System.Drawing.Size(68, 14);
			this.lblRecordNumber.TabIndex = 38;
			this.lblRecordNumber.Text = "VENDOR #";
			this.ToolTip1.SetToolTip(this.lblRecordNumber, null);
			// 
			// cboDepartment
			// 
			this.cboDepartment.AutoSize = false;
			this.cboDepartment.BackColor = System.Drawing.SystemColors.Window;
			this.cboDepartment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDepartment.FormattingEnabled = true;
			this.cboDepartment.Location = new System.Drawing.Point(177, 186);
			this.cboDepartment.Name = "cboDepartment";
			this.cboDepartment.Size = new System.Drawing.Size(310, 40);
			this.cboDepartment.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.cboDepartment, null);
			// 
			// txtVendor
			// 
			this.txtVendor.AutoSize = false;
			this.txtVendor.LinkItem = null;
			this.txtVendor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtVendor.LinkTopic = null;
			this.txtVendor.Location = new System.Drawing.Point(641, 30);
			this.txtVendor.MaxLength = 5;
			this.txtVendor.Name = "txtVendor";
			this.txtVendor.Size = new System.Drawing.Size(83, 40);
			this.txtVendor.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.txtVendor, "Enter S for Vendor Search or A for Add Vendor");
			this.txtVendor.KeyDown += new Wisej.Web.KeyEventHandler(this.txtVendor_KeyDownEvent);
			this.txtVendor.Enter += new System.EventHandler(this.txtVendor_Enter);
			this.txtVendor.Leave += new System.EventHandler(this.txtVendor_Leave);
			this.txtVendor.Validating += new System.ComponentModel.CancelEventHandler(this.txtVendor_Validate);
			// 
			// txtDescription
			// 
			this.txtDescription.AutoSize = false;
			this.txtDescription.LinkItem = null;
			this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDescription.LinkTopic = null;
			this.txtDescription.Location = new System.Drawing.Point(177, 126);
			this.txtDescription.MaxLength = 25;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(310, 40);
			this.txtDescription.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtDescription, null);
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(177, 66);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.MaxLength = 10;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 5;
			this.txtDate.Text = "  /  /";
			this.ToolTip1.SetToolTip(this.txtDate, null);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 265);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 16;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(546, 232);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.vs1, null);
			this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
			this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
			this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(30, 560);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(77, 16);
			this.Label11.TabIndex = 57;
			this.Label11.Text = "COMMENTS";
			this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label11, null);
			// 
			// lblAddress4
			// 
			this.lblAddress4.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAddress4.Location = new System.Drawing.Point(754, 204);
			this.lblAddress4.Name = "lblAddress4";
			this.lblAddress4.Size = new System.Drawing.Size(350, 16);
			this.lblAddress4.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.lblAddress4, null);
			// 
			// lblAddress3
			// 
			this.lblAddress3.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAddress3.Location = new System.Drawing.Point(754, 164);
			this.lblAddress3.Name = "lblAddress3";
			this.lblAddress3.Size = new System.Drawing.Size(350, 16);
			this.lblAddress3.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.lblAddress3, null);
			// 
			// lblAddress2
			// 
			this.lblAddress2.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAddress2.Location = new System.Drawing.Point(754, 124);
			this.lblAddress2.Name = "lblAddress2";
			this.lblAddress2.Size = new System.Drawing.Size(350, 16);
			this.lblAddress2.TabIndex = 16;
			this.ToolTip1.SetToolTip(this.lblAddress2, null);
			// 
			// lblAddress1
			// 
			this.lblAddress1.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAddress1.Location = new System.Drawing.Point(754, 84);
			this.lblAddress1.Name = "lblAddress1";
			this.lblAddress1.Size = new System.Drawing.Size(350, 16);
			this.lblAddress1.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.lblAddress1, null);
			// 
			// lblVendorName
			// 
			this.lblVendorName.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblVendorName.Location = new System.Drawing.Point(754, 44);
			this.lblVendorName.Name = "lblVendorName";
			this.lblVendorName.Size = new System.Drawing.Size(350, 16);
			this.lblVendorName.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.lblVendorName, null);
			// 
			// lblTotalAmount
			// 
			this.lblTotalAmount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblTotalAmount.Location = new System.Drawing.Point(318, 517);
			this.lblTotalAmount.Name = "lblTotalAmount";
			this.lblTotalAmount.Size = new System.Drawing.Size(114, 16);
			this.lblTotalAmount.TabIndex = 13;
			this.lblTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ToolTip1.SetToolTip(this.lblTotalAmount, null);
			// 
			// Label1
			// 
			this.Label1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.Label1.Location = new System.Drawing.Point(254, 517);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(58, 16);
			this.Label1.TabIndex = 12;
			this.Label1.Text = "TOTAL";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// lblPurchaseOrder
			// 
			this.lblPurchaseOrder.Location = new System.Drawing.Point(177, 30);
			this.lblPurchaseOrder.Name = "lblPurchaseOrder";
			this.lblPurchaseOrder.Size = new System.Drawing.Size(118, 16);
			this.lblPurchaseOrder.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.lblPurchaseOrder, null);
			// 
			// lblExpense
			// 
			this.lblExpense.Location = new System.Drawing.Point(30, 246);
			this.lblExpense.Name = "lblExpense";
			this.lblExpense.Size = new System.Drawing.Size(564, 20);
			this.lblExpense.TabIndex = 10;
			this.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblExpense, "Right click to see account balance");
			this.lblExpense.MouseUp += new Wisej.Web.MouseEventHandler(this.lblExpense_MouseUp);
			// 
			// lblDate1
			// 
			this.lblDate1.Location = new System.Drawing.Point(30, 80);
			this.lblDate1.Name = "lblDate1";
			this.lblDate1.Size = new System.Drawing.Size(31, 16);
			this.lblDate1.TabIndex = 4;
			this.lblDate1.Text = "DATE";
			this.ToolTip1.SetToolTip(this.lblDate1, null);
			// 
			// lblPO
			// 
			this.lblPO.Location = new System.Drawing.Point(30, 30);
			this.lblPO.Name = "lblPO";
			this.lblPO.Size = new System.Drawing.Size(35, 16);
			this.lblPO.TabIndex = 0;
			this.lblPO.Text = "P.O.";
			this.ToolTip1.SetToolTip(this.lblPO, null);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 140);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(77, 16);
			this.lblDescription.TabIndex = 6;
			this.lblDescription.Text = "DESCRIPTION";
			this.ToolTip1.SetToolTip(this.lblDescription, null);
			// 
			// lblVendor
			// 
			this.lblVendor.Location = new System.Drawing.Point(523, 44);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(58, 16);
			this.lblVendor.TabIndex = 2;
			this.lblVendor.Text = "VENDOR";
			this.ToolTip1.SetToolTip(this.lblVendor, null);
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(30, 200);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(97, 16);
			this.lblPeriod.TabIndex = 8;
			this.lblPeriod.Text = "DEPARTMENT";
			this.ToolTip1.SetToolTip(this.lblPeriod, null);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessDelete,
				this.mnuProcessDeleteEntry
			});
			this.MainMenu1.Name = null;
			// 
			// mnuFileSeperator5
			// 
			this.mnuFileSeperator5.Index = 0;
			this.mnuFileSeperator5.Name = "mnuFileSeperator5";
			this.mnuFileSeperator5.Text = "-";
			// 
			// mnuProcessDelete
			// 
			this.mnuProcessDelete.Enabled = false;
			this.mnuProcessDelete.Index = 1;
			this.mnuProcessDelete.Name = "mnuProcessDelete";
			this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuProcessDelete.Text = "Delete Detail Item";
			this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
			// 
			// mnuProcessDeleteEntry
			// 
			this.mnuProcessDeleteEntry.Enabled = false;
			this.mnuProcessDeleteEntry.Index = 2;
			this.mnuProcessDeleteEntry.Name = "mnuProcessDeleteEntry";
			this.mnuProcessDeleteEntry.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuProcessDeleteEntry.Text = "Delete Journal Entry";
			this.mnuProcessDeleteEntry.Click += new System.EventHandler(this.mnuProcessDeleteEntry_Click);
			// 
			// mnuFileSeperator6
			// 
			this.mnuFileSeperator6.Index = 3;
			this.mnuFileSeperator6.Name = "mnuFileSeperator6";
			this.mnuFileSeperator6.Text = "-";
			// 
			// mnuProcessSearch
			// 
			this.mnuProcessSearch.Enabled = false;
			this.mnuProcessSearch.Index = 4;
			this.mnuProcessSearch.Name = "mnuProcessSearch";
			this.mnuProcessSearch.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuProcessSearch.Text = "Vendor Search";
			this.mnuProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
			// 
			// mnuFileAddVendor
			// 
			this.mnuFileAddVendor.Enabled = false;
			this.mnuFileAddVendor.Index = 5;
			this.mnuFileAddVendor.Name = "mnuFileAddVendor";
			this.mnuFileAddVendor.Text = "Add Vendor";
			this.mnuFileAddVendor.Click += new System.EventHandler(this.mnuFileAddVendor_Click);
			// 
			// mnuProcessAccounts
			// 
			this.mnuProcessAccounts.Enabled = false;
			this.mnuProcessAccounts.Index = -1;
			this.mnuProcessAccounts.Name = "mnuProcessAccounts";
			this.mnuProcessAccounts.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuProcessAccounts.Text = "Show Valid Accounts";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSeperator7
			// 
			this.mnuFileSeperator7.Index = -1;
			this.mnuFileSeperator7.Name = "mnuFileSeperator7";
			this.mnuFileSeperator7.Text = "-";
			// 
			// mnuProcessPreviousEntry
			// 
			this.mnuProcessPreviousEntry.Index = -1;
			this.mnuProcessPreviousEntry.Name = "mnuProcessPreviousEntry";
			this.mnuProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuProcessPreviousEntry.Text = "Previous Journal Entry";
			this.mnuProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
			// 
			// mnuProcessNextEntry
			// 
			this.mnuProcessNextEntry.Index = -1;
			this.mnuProcessNextEntry.Name = "mnuProcessNextEntry";
			this.mnuProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuProcessNextEntry.Text = "Next Journal Entry";
			this.mnuProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = -1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = -1;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print Purchase Order";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFileSeperator4
			// 
			this.mnuFileSeperator4.Index = -1;
			this.mnuFileSeperator4.Name = "mnuFileSeperator4";
			this.mnuFileSeperator4.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = -1;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = -1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = -1;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnFileSave
			// 
			this.btnFileSave.AppearanceKey = "acceptButton";
			this.btnFileSave.Location = new System.Drawing.Point(306, 30);
			this.btnFileSave.Name = "btnFileSave";
			this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnFileSave.Size = new System.Drawing.Size(74, 48);
			this.btnFileSave.TabIndex = 0;
			this.btnFileSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnFileSave, null);
			this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(682, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(142, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print Purchase Order";
			this.ToolTip1.SetToolTip(this.cmdFilePrint, null);
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdProcessNextEntry
			// 
			this.cmdProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessNextEntry.AppearanceKey = "toolbarButton";
			this.cmdProcessNextEntry.Location = new System.Drawing.Point(550, 29);
			this.cmdProcessNextEntry.Name = "cmdProcessNextEntry";
			this.cmdProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdProcessNextEntry.Size = new System.Drawing.Size(129, 24);
			this.cmdProcessNextEntry.TabIndex = 2;
			this.cmdProcessNextEntry.Text = "Next Journal Entry";
			this.ToolTip1.SetToolTip(this.cmdProcessNextEntry, null);
			this.cmdProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
			// 
			// cmdProcessPreviousEntry
			// 
			this.cmdProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessPreviousEntry.AppearanceKey = "toolbarButton";
			this.cmdProcessPreviousEntry.Location = new System.Drawing.Point(396, 29);
			this.cmdProcessPreviousEntry.Name = "cmdProcessPreviousEntry";
			this.cmdProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdProcessPreviousEntry.Size = new System.Drawing.Size(150, 24);
			this.cmdProcessPreviousEntry.TabIndex = 3;
			this.cmdProcessPreviousEntry.Text = "Previous Journal Entry";
			this.ToolTip1.SetToolTip(this.cmdProcessPreviousEntry, null);
			this.cmdProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
			// 
			// cmdVendorSearch
			// 
			this.cmdVendorSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdVendorSearch.AppearanceKey = "toolbarButton";
			this.cmdVendorSearch.Location = new System.Drawing.Point(285, 29);
			this.cmdVendorSearch.Name = "cmdVendorSearch";
			this.cmdVendorSearch.Size = new System.Drawing.Size(107, 24);
			this.cmdVendorSearch.TabIndex = 4;
			this.cmdVendorSearch.Text = "Vendor Search";
			this.ToolTip1.SetToolTip(this.cmdVendorSearch, null);
			this.cmdVendorSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
			// 
			// cmdAddVendor
			// 
			this.cmdAddVendor.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddVendor.AppearanceKey = "toolbarButton";
			this.cmdAddVendor.Location = new System.Drawing.Point(194, 29);
			this.cmdAddVendor.Name = "cmdAddVendor";
			this.cmdAddVendor.Size = new System.Drawing.Size(87, 24);
			this.cmdAddVendor.TabIndex = 5;
			this.cmdAddVendor.Text = "Add Vendor";
			this.ToolTip1.SetToolTip(this.cmdAddVendor, null);
			this.cmdAddVendor.Click += new System.EventHandler(this.mnuFileAddVendor_Click);
			// 
			// frmPurchaseOrderDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(865, 648);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmPurchaseOrderDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purchase Orders";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmPurchaseOrderDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmPurchaseOrderDataEntry_Activated);
			this.FormClosed += new FormClosedEventHandler(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPurchaseOrderDataEntry_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurchaseOrderDataEntry_KeyPress);
			this.Resize += new System.EventHandler(this.frmPurchaseOrderDataEntry_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtComments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).EndInit();
			this.fraTownInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancelPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtShipToAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).EndInit();
			this.fraAccountBalance.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmSearch)).EndInit();
			this.frmSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.frmInfo)).EndInit();
			this.frmInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessNextEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPreviousEntry)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdVendorSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddVendor)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnFileSave;
		public FCButton cmdFilePrint;
		public FCButton cmdProcessNextEntry;
		public FCButton cmdProcessPreviousEntry;
		private FCButton cmdAddVendor;
		private FCButton cmdVendorSearch;
	}
}