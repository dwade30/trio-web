﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeRevenueDetail.
	/// </summary>
	partial class frmCustomizeRevenueDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWide;
		public fecherFoundation.FCLabel lblWide;
		public fecherFoundation.FCComboBox cmbLong;
		public fecherFoundation.FCLabel lblLong;
		public fecherFoundation.FCComboBox cmbDisplay;
		public fecherFoundation.FCLabel lblDisplay;
		public fecherFoundation.FCComboBox cmbPendingSummary;
		public fecherFoundation.FCLabel lblPendingSummary;
		public fecherFoundation.FCComboBox cmbYTDNet;
		public fecherFoundation.FCLabel lblYTDNet;
		public fecherFoundation.FCFrame fraPreferences;
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCCheckBox chkShowLiquidatedEncumbranceActivity;
		public FCCommonDialog cdgFont;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblDescription;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomizeRevenueDetail));
            this.cmbWide = new fecherFoundation.FCComboBox();
            this.lblWide = new fecherFoundation.FCLabel();
            this.cmbLong = new fecherFoundation.FCComboBox();
            this.lblLong = new fecherFoundation.FCLabel();
            this.cmbDisplay = new fecherFoundation.FCComboBox();
            this.lblDisplay = new fecherFoundation.FCLabel();
            this.cmbPendingSummary = new fecherFoundation.FCComboBox();
            this.lblPendingSummary = new fecherFoundation.FCLabel();
            this.cmbYTDNet = new fecherFoundation.FCComboBox();
            this.lblYTDNet = new fecherFoundation.FCLabel();
            this.fraPreferences = new fecherFoundation.FCFrame();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.chkShowLiquidatedEncumbranceActivity = new fecherFoundation.FCCheckBox();
            this.cdgFont = new fecherFoundation.FCCommonDialog();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).BeginInit();
            this.fraPreferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPreferences);
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(346, 30);
            this.HeaderText.Text = "Revenue Detail Report Format";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbWide
            // 
            this.cmbWide.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.cmbWide.Location = new System.Drawing.Point(172, 80);
            this.cmbWide.Name = "cmbWide";
            this.cmbWide.Size = new System.Drawing.Size(200, 40);
            this.cmbWide.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbWide, null);
            // 
            // lblWide
            // 
            this.lblWide.AutoSize = true;
            this.lblWide.Location = new System.Drawing.Point(20, 94);
            this.lblWide.Name = "lblWide";
            this.lblWide.Size = new System.Drawing.Size(93, 15);
            this.lblWide.TabIndex = 3;
            this.lblWide.Text = "PAPER WIDTH";
            this.ToolTip1.SetToolTip(this.lblWide, null);
            // 
            // cmbLong
            // 
            this.cmbLong.Items.AddRange(new object[] {
            "Long Descriptions",
            "Short Descriptions"});
            this.cmbLong.Location = new System.Drawing.Point(172, 30);
            this.cmbLong.Name = "cmbLong";
            this.cmbLong.Size = new System.Drawing.Size(200, 40);
            this.ToolTip1.SetToolTip(this.cmbLong, null);
            // 
            // lblLong
            // 
            this.lblLong.AutoSize = true;
            this.lblLong.Location = new System.Drawing.Point(20, 44);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(100, 15);
            this.lblLong.TabIndex = 1;
            this.lblLong.Text = "DESCRIPTIONS";
            this.ToolTip1.SetToolTip(this.lblLong, null);
            // 
            // cmbDisplay
            // 
            this.cmbDisplay.Items.AddRange(new object[] {
            "Print",
            "Display"});
            this.cmbDisplay.Location = new System.Drawing.Point(172, 130);
            this.cmbDisplay.Name = "cmbDisplay";
            this.cmbDisplay.Size = new System.Drawing.Size(200, 40);
            this.cmbDisplay.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbDisplay, null);
            // 
            // lblDisplay
            // 
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Location = new System.Drawing.Point(20, 144);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(88, 15);
            this.lblDisplay.TabIndex = 5;
            this.lblDisplay.Text = "REPORT USE";
            this.ToolTip1.SetToolTip(this.lblDisplay, null);
            // 
            // cmbPendingSummary
            // 
            this.cmbPendingSummary.Items.AddRange(new object[] {
            "Show Detail",
            "Show Summary Only",
            "Do Not Include"});
            this.cmbPendingSummary.Location = new System.Drawing.Point(230, 80);
            this.cmbPendingSummary.Name = "cmbPendingSummary";
            this.cmbPendingSummary.Size = new System.Drawing.Size(200, 40);
            this.cmbPendingSummary.TabIndex = 29;
            this.ToolTip1.SetToolTip(this.cmbPendingSummary, null);
            // 
            // lblPendingSummary
            // 
            this.lblPendingSummary.AutoSize = true;
            this.lblPendingSummary.Location = new System.Drawing.Point(20, 94);
            this.lblPendingSummary.Name = "lblPendingSummary";
            this.lblPendingSummary.Size = new System.Drawing.Size(123, 15);
            this.lblPendingSummary.TabIndex = 30;
            this.lblPendingSummary.Text = "PENDING ACTIVITY";
            this.ToolTip1.SetToolTip(this.lblPendingSummary, null);
            // 
            // cmbYTDNet
            // 
            this.cmbYTDNet.Items.AddRange(new object[] {
            "Debits / Credits",
            "Net Total"});
            this.cmbYTDNet.Location = new System.Drawing.Point(230, 30);
            this.cmbYTDNet.Name = "cmbYTDNet";
            this.cmbYTDNet.Size = new System.Drawing.Size(200, 40);
            this.cmbYTDNet.TabIndex = 31;
            this.ToolTip1.SetToolTip(this.cmbYTDNet, null);
            this.cmbYTDNet.SelectedIndexChanged += new System.EventHandler(this.cmbYTDNet_CheckedChanged);
            // 
            // lblYTDNet
            // 
            this.lblYTDNet.AutoSize = true;
            this.lblYTDNet.Location = new System.Drawing.Point(20, 44);
            this.lblYTDNet.Name = "lblYTDNet";
            this.lblYTDNet.Size = new System.Drawing.Size(158, 15);
            this.lblYTDNet.TabIndex = 32;
            this.lblYTDNet.Text = "ACCOUNT INFORMATION";
            this.ToolTip1.SetToolTip(this.lblYTDNet, null);
            // 
            // fraPreferences
            // 
            this.fraPreferences.AppearanceKey = "groupBoxLeftBorder";
            this.fraPreferences.Controls.Add(this.cmbLong);
            this.fraPreferences.Controls.Add(this.lblLong);
            this.fraPreferences.Controls.Add(this.cmbWide);
            this.fraPreferences.Controls.Add(this.lblWide);
            this.fraPreferences.Controls.Add(this.cmbDisplay);
            this.fraPreferences.Controls.Add(this.lblDisplay);
            this.fraPreferences.Location = new System.Drawing.Point(30, 100);
            this.fraPreferences.Name = "fraPreferences";
            this.fraPreferences.Size = new System.Drawing.Size(600, 186);
            this.fraPreferences.TabIndex = 19;
            this.fraPreferences.Text = "General Preferences";
            this.ToolTip1.SetToolTip(this.fraPreferences, null);
            // 
            // fraInformation
            // 
            this.fraInformation.Controls.Add(this.chkShowLiquidatedEncumbranceActivity);
            this.fraInformation.Controls.Add(this.cmbPendingSummary);
            this.fraInformation.Controls.Add(this.lblPendingSummary);
            this.fraInformation.Controls.Add(this.cmbYTDNet);
            this.fraInformation.Controls.Add(this.lblYTDNet);
            this.fraInformation.Location = new System.Drawing.Point(30, 292);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(450, 186);
            this.fraInformation.TabIndex = 16;
            this.fraInformation.Text = "Information To Be Reported";
            this.ToolTip1.SetToolTip(this.fraInformation, null);
            // 
            // chkShowLiquidatedEncumbranceActivity
            // 
            this.chkShowLiquidatedEncumbranceActivity.Location = new System.Drawing.Point(20, 140);
            this.chkShowLiquidatedEncumbranceActivity.Name = "chkShowLiquidatedEncumbranceActivity";
            this.chkShowLiquidatedEncumbranceActivity.Size = new System.Drawing.Size(311, 27);
            this.chkShowLiquidatedEncumbranceActivity.TabIndex = 28;
            this.chkShowLiquidatedEncumbranceActivity.Text = "Show Liquidated Encumbrance Activity";
            this.ToolTip1.SetToolTip(this.chkShowLiquidatedEncumbranceActivity, null);
            this.chkShowLiquidatedEncumbranceActivity.CheckedChanged += new System.EventHandler(this.chkShowLiquidatedEncumbranceActivity_CheckedChanged);
            // 
            // cdgFont
            // 
            this.cdgFont.Name = "cdgFont";
            this.cdgFont.Size = new System.Drawing.Size(0, 0);
            this.ToolTip1.SetToolTip(this.cdgFont, null);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(226, 30);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(201, 40);
            this.txtDescription.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(155, 18);
            this.lblDescription.TabIndex = 15;
            this.lblDescription.Text = "FORMAT DESCRIPTION";
            this.ToolTip1.SetToolTip(this.lblDescription, null);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(472, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(135, 48);
            this.cmdSave.TabIndex = 20;
            this.cmdSave.Text = "Save & Exit";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmCustomizeRevenueDetail
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCustomizeRevenueDetail";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Revenue Detail Report Format";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCustomizeRevenueDetail_Load);
            this.Activated += new System.EventHandler(this.frmCustomizeRevenueDetail_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomizeRevenueDetail_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomizeRevenueDetail_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreferences)).EndInit();
            this.fraPreferences.ResumeLayout(false);
            this.fraPreferences.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            this.fraInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLiquidatedEncumbranceActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdSave;
	}
}
