﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJDataEntry.
	/// </summary>
	public partial class frmGetGJDataEntry : BaseForm
	{
		public frmGetGJDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetGJDataEntry InstancePtr
		{
			get
			{
				return (frmGetGJDataEntry)Sys.GetInstance(typeof(frmGetGJDataEntry));
			}
		}

		protected frmGetGJDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool EmptyFlag;

		public void StartProgram()
		{
			int counter;
			clsDRWrapper rsPeriod = new clsDRWrapper();
			bool blnPayrollJournal = false;
			int intPeriod;
			int lngJournal;
			//Application.DoEvents();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			lngJournal = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
			intPeriod = cboEntry.ItemData(cboEntry.SelectedIndex);
			rsPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "Budgetary");
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsPeriod.Get_Fields("Type")) == "PY")
			{
				blnPayrollJournal = true;
			}
			else
			{
				blnPayrollJournal = false;
			}
			modBudgetaryMaster.Statics.CurrentGJEntry = lngJournal;
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND (Type = 'G' OR Type = 'P') AND Status = 'E' AND Period = " + FCConvert.ToString(intPeriod) + " ORDER BY ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				modBudgetaryMaster.Statics.blnGJEdit = true;
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				frmGJDataEntry.InstancePtr.ChosenJournal = FCConvert.ToInt32(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmGJDataEntry.InstancePtr.ChosenPeriod = FCConvert.ToInt32(rs.Get_Fields("Period"));
				frmGJDataEntry.InstancePtr.blnPayroll = blnPayrollJournal;
				if (rs.RecordCount() > 15)
				{
					frmGJDataEntry.InstancePtr.vs1.Rows = rs.RecordCount() + 1;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmGJDataEntry.InstancePtr.txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("Period")), 2);
				frmGJDataEntry.InstancePtr.btnFileLoadType.Enabled = false;
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(rs.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yy"));
					frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
					if (FCConvert.ToBoolean(rs.Get_Fields("CarryForward")))
					{
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, "F");
					}
					else
					{
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs.Get_Fields_String("RCB")));
					}
					frmGJDataEntry.InstancePtr.vs1.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs.Get_Fields("account")));
					frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					if (rs.Get_Fields("Amount") < 0)
					{
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs.Get_Fields("Amount") * -1));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs.Get_Fields("Amount")));
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
					}
					if (rs.EndOfFile() != true)
					{
						rs.MoveNext();
					}
				}
				if (rs.RecordCount() < 15)
				{
					for (counter = rs.RecordCount() + 1; counter <= frmGJDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, "0");
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(DateTime.Today, "MM/dd/yy"));
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, "R");
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, "0");
						frmGJDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, "0");
					}
				}
				frmGJDataEntry.InstancePtr.Show(App.MainForm);
				// show the form
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				cmbUpdate.SelectedIndex = 1;
				EmptyFlag = true;
				cmdGetAccountNumber_Click();
				return;
			}
			// Me.Hide
			frmWait.InstancePtr.Unload();
			Close();
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN: Fix incorrect hiding Tabs.
			// Close();
			clsDRWrapper rsPeriod = new clsDRWrapper();
			frmGJDataEntry.InstancePtr.boolShowDataEntryForm = true;
			if (cboEntry.SelectedIndex != 0)
			{
				// if there is a valid account number
				if (cmbUpdate.SelectedIndex == 1)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnGJEdit = false;
					frmGJDataEntry.InstancePtr.ChosenJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))));
					frmGJDataEntry.InstancePtr.ChosenPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 19, 2))));
					rsPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 19, 2))));
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsPeriod.Get_Fields("Type")) == "PY")
					{
						frmGJDataEntry.InstancePtr.blnPayroll = true;
					}
					else
					{
						frmGJDataEntry.InstancePtr.blnPayroll = false;
					}
					frmGJDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
					//FC:FINAL:CHN: Fix incorrect hiding Tabs.
					//Hide();
				}
				else
				{
					modBudgetaryMaster.Statics.CurrentGJEntry = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
					// set the account number to the one last used
					StartProgram();
					// call the procedure to retrieve the info
				}
			}
			else
			{
				if (cmbUpdate.SelectedIndex == 1)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnGJEdit = false;
					frmGJDataEntry.InstancePtr.ChosenJournal = 0;
					frmGJDataEntry.InstancePtr.ChosenPeriod = 0;
					frmGJDataEntry.InstancePtr.blnPayroll = false;
					frmGJDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
					//FC:FINAL:CHN: Fix incorrect hiding Tabs.
					//Hide();
				}
				else
				{
					MessageBox.Show("You may only input to a new journal.", "Invalid Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click()
		{
			frmGetGJDataEntry.InstancePtr.Close(); // unload this form
		}

		private void frmGetGJDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (!EmptyFlag)
			{
				lblLastAccount.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry), 4);
				FillCombo_2(false);
				cboEntry.SelectedIndex = 0;
				cboEntry.Focus();
			}
			if (!EmptyFlag)
			{
				modBudgetaryMaster.Statics.CurrentGJEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRGJJRNL"))));
				cmbUpdate.SelectedIndex = 1;
			}
			else
			{
				EmptyFlag = false;
			}
		}

		private void frmGetGJDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				KeyAscii = (Keys)0;
				if (frmGetGJDataEntry.InstancePtr.ActiveControl.GetName() == "cboEntry")
					cmdGetAccountNumber_Click();
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				frmGetGJDataEntry.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetGJDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetGJDataEntry.ScaleWidth	= 9045;
			//frmGetGJDataEntry.ScaleHeight	= 7365;
			//frmGetGJDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboEntry.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboEntry.Items[counter].ToString(), 4))
				{
					cboEntry.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboEntry_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEntry.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (e.CloseReason == FCCloseReason.FormControlMenu)			{
				//MDIParent.InstancePtr.Focus();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void optInput_CheckedChanged(object sender, System.EventArgs e)
		{
			FillCombo_2(false);
			cboEntry.SelectedIndex = 0;
		}

		private void FillCombo_2(bool blnEdit)
		{
			FillCombo(ref blnEdit);
		}

		private void FillCombo(ref bool blnEdit)
		{
			int counter;
			cboEntry.Clear();
			cboEntry.AddItem("0000 - New Journal Entry");
			if (blnEdit)
			{
				rs.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND (Type = 'GJ' OR Type = 'PY') ORDER BY JournalNumber DESC");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND (Type = 'GJ' OR Type = 'PY') AND left(Description, 14) <> 'Enc Adjust for' ORDER BY JournalNumber DESC");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboEntry.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4) + " - " + "Per " + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboEntry.ItemData(cboEntry.NewIndex, FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Period"))));
					rs.MoveNext();
				}
			}
		}

		private void optUpdate_CheckedChanged(object sender, System.EventArgs e)
		{
			FillCombo_2(true);
			cboEntry.SelectedIndex = 0;
		}
	}
}
