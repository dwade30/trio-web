﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGJDataEntry.
	/// </summary>
	public partial class frmGJDataEntry : BaseForm
	{
		public frmGJDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGJDataEntry InstancePtr
		{
			get
			{
				return (frmGJDataEntry)Sys.GetInstance(typeof(frmGJDataEntry));
			}
		}

		protected frmGJDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NumberCol;
		int DateCol;
		int DescriptionCol;
		int RCBCol;
		int AccountCol;
		int ProjCol;
		int DebitCol;
		int CreditCol;
		// vbPorter upgrade warning: TotalDebit As double	OnWrite(short, Decimal)
		double TotalDebit;
		// vbPorter upgrade warning: TotalCredit As double	OnWrite(short, Decimal)
		double TotalCredit;
		// vbPorter upgrade warning: ChosenJournal As int	OnWrite(string, int)
		public int ChosenJournal;
		// vbPorter upgrade warning: ChosenPeriod As short --> As int	OnWrite(short, string)
		public int ChosenPeriod;
		clsDRWrapper rs = new clsDRWrapper();
		bool EditFlag;
		bool DeleteFlag;
		clsDRWrapper Master = new clsDRWrapper();
		string ErrorString = "";
		bool BadAccountFlag;
		string strEncOffAccount = "";
		string strRevCtrAccount = "";
		string strExpCtrAccount = "";
		public bool blnPayroll;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnUnload;
		bool blnJournalLocked;
		string strComboList;
		bool blnSaved;
		clsGridAccount vsGrid = new clsGridAccount();
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;
		public bool boolShowDataEntryForm;
		bool openFormWhenThisClose = false;
		public void Init(cJournal journ)
		{
			boolShowDataEntryForm = false;
			if (!(journ == null))
			{
				modBudgetaryMaster.Statics.CurrentGJEntry = journ.JournalNumber;
				int lngRow;
				cJournalEntry jEntry;
				modBudgetaryMaster.Statics.blnGJEdit = false;
				ChosenJournal = journ.JournalNumber;
				ChosenPeriod = journ.Period;
				if (Strings.LCase(journ.JournalType) == "py")
				{
					blnPayroll = true;
				}
				else
				{
					blnPayroll = false;
				}
				if (journ.JournalEntries.ItemCount() > 0)
				{
					modBudgetaryMaster.Statics.blnGJEdit = true;
					modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentGJEntry));
					if (journ.JournalEntries.ItemCount() > 15)
					{
						vs1.Rows = journ.JournalEntries.ItemCount() + 1;
					}
					else
					{
						for (lngRow = journ.JournalEntries.ItemCount() + 1; lngRow <= vs1.Rows - 1; lngRow++)
						{
							vs1.TextMatrix(lngRow, 0, "0");
							vs1.TextMatrix(lngRow, 1, Strings.Format(DateTime.Today, "MM/dd/yy"));
							vs1.TextMatrix(lngRow, 3, "R");
							vs1.TextMatrix(lngRow, 6, "0");
							vs1.TextMatrix(lngRow, 7, "0");
						}
					}
					txtPeriod.Text = modValidateAccount.GetFormat_6(journ.Period.ToString(), 2);
					btnFileLoadType.Enabled = false;
					journ.JournalEntries.MoveFirst();
					lngRow = 0;
					//FC:FINAL:DDU:#1189 - aligned grid column
					vs1.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    while (journ.JournalEntries.IsCurrent())
					{
						lngRow += 1;
						jEntry = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
						vs1.TextMatrix(lngRow, 0, FCConvert.ToString(jEntry.ID));
						vs1.TextMatrix(lngRow, 1, Strings.Format(jEntry.JournalEntriesDate, "MM/dd/yy"));
						vs1.TextMatrix(lngRow, 2, jEntry.Description);
						if (jEntry.CarryForward)
						{
							vs1.TextMatrix(lngRow, 3, "F");
						}
						else
						{
							vs1.TextMatrix(lngRow, 3, jEntry.RCB);
						}
						vs1.TextMatrix(lngRow, 4, jEntry.Account);
						vs1.TextMatrix(lngRow, 5, jEntry.Project);
						if (jEntry.Amount < 0)
						{
							vs1.TextMatrix(lngRow, 6, 0);
							vs1.TextMatrix(lngRow, 7, jEntry.Amount * -1);
						}
						else
						{
							vs1.TextMatrix(lngRow, 6, jEntry.Amount);
							vs1.TextMatrix(lngRow, 7, 0);
						}
						journ.JournalEntries.MoveNext();
					}
				}
				else
				{
					// simply leave blank
				}
			}
			else
			{
				modBudgetaryMaster.Statics.blnGJEdit = false;
				ChosenJournal = 0;
				ChosenPeriod = 0;
				blnPayroll = false;
			}
			this.Show(App.MainForm);
		}
		//FC:FINAL:MSH - Issue #760: Handler, which check value of 'SelectedIndex' when drop-down portion will be closed. 'SelectedIndexChanged' will invoke only when selected another value from comboBox, not current.
		private void cboSaveJournal_DropDownClosed(object sender, EventArgs e)
		{
			if (cboSaveJournal.SelectedIndex == 0)
			{
				txtJournalDescription.Visible = true;
				lblJournalDescription.Visible = true;
				lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
				txtJournalDescription.Focus();
			}
			else
			{
				txtJournalDescription.Visible = false;
				lblJournalDescription.Visible = false;
				lblSaveInstructions.Text = "Please click the OK button to save";
				cmdOKSave.Focus();
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraDefaultType.Visible = false;
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			clsDRWrapper rsDefaultDetail = new clsDRWrapper();
			int counter = 0;
			int lngChosenType = 0;
			clsDRWrapper rsDuplicate = new clsDRWrapper();
			if (cboDefaultType.Visible == true)
			{
				ans = MessageBox.Show("Any information you have entered so far in this screen will be lost if you load a default type.  Do you wish to continue?", "Load Default Type?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					rsDefaultInfo.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + modCustomReport.FixQuotes(cboDefaultType.Text) + "'");
					if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
					{
						rsDefaultDetail.OpenRecordset("SELECT * FROM GJDefaultDetail WHERE GJDefaultMasterID = " + rsDefaultInfo.Get_Fields_Int32("ID") + " ORDER BY ID");
						if (rsDefaultDetail.EndOfFile() != true && rsDefaultDetail.BeginningOfFile() != true)
						{
							vs1.Rows = rsDefaultDetail.RecordCount() + 1;
							counter = 1;
							do
							{
								vs1.TextMatrix(counter, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
								vs1.TextMatrix(counter, NumberCol, 0);
								vs1.TextMatrix(counter, DescriptionCol, FCConvert.ToString(rsDefaultDetail.Get_Fields_String("Description")));
								vs1.TextMatrix(counter, RCBCol, FCConvert.ToString(rsDefaultDetail.Get_Fields_String("RCB")));
								//FC:FINAL:DDU:#1189 - aligned grid column
								vs1.ColAlignment(RCBCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								vs1.TextMatrix(counter, AccountCol, FCConvert.ToString(rsDefaultDetail.Get_Fields("account")));
								vs1.TextMatrix(counter, ProjCol, FCConvert.ToString(rsDefaultDetail.Get_Fields_String("Project")));
								// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
								if (rsDefaultDetail.Get_Fields("Amount") < 0)
								{
									vs1.TextMatrix(counter, DebitCol, 0);
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(counter, CreditCol, rsDefaultDetail.Get_Fields("Amount") * -1);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
									vs1.TextMatrix(counter, DebitCol, rsDefaultDetail.Get_Fields("Amount"));
									vs1.TextMatrix(counter, CreditCol, 0);
								}
								counter += 1;
								rsDefaultDetail.MoveNext();
							}
							while (rsDefaultDetail.EndOfFile() != true);
						}
						else
						{
							MessageBox.Show("Default type not found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
					else
					{
						MessageBox.Show("Default type not found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			else
			{
				if (Strings.Trim(txtTypeDescription.Text) == "")
				{
					MessageBox.Show("You must enter a description before you may proceed.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtTypeDescription.Focus();
					return;
				}
				rsDuplicate.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE Description = '" + Strings.Trim(txtTypeDescription.Text) + "'");
				if (rsDuplicate.EndOfFile() != true && rsDuplicate.BeginningOfFile() != true)
				{
					MessageBox.Show("The description you entered is used for another default type.  You must enter a different description before you may continue.", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtTypeDescription.Focus();
					return;
				}
				rs.OmitNullsOnInsert = true;
				rs.OpenRecordset("SELECT * FROM GJDefaultMaster WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", Strings.Trim(txtTypeDescription.Text));
				rs.Update();
				lngChosenType = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol)) != 0 || FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol)) != 0 || vs1.TextMatrix(counter, DescriptionCol) != "" || vs1.TextMatrix(counter, AccountCol) != "")
					{
						rs.OpenRecordset("SELECT * FROM GJDefaultDetail WHERE ID = 0");
						rs.AddNew();
						rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
						rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
						rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
						rs.Set_Fields("RCB", vs1.TextMatrix(counter, RCBCol));
						rs.Set_Fields("Amount", Conversion.Val(vs1.TextMatrix(counter, DebitCol)) - Conversion.Val(vs1.TextMatrix(counter, CreditCol)));
						rs.Set_Fields("GJDefaultMasterID", lngChosenType);
						rs.Update();
					}
				}
				MessageBox.Show("Default type saved successfully", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			fraDefaultType.Visible = false;
		}

		private void frmGJDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			CalculateTotals();
			lblNetTotal.Text = Strings.FormatCurrency((TotalDebit - TotalCredit), 2);
			if (txtPeriod.Enabled)
			{
				txtPeriod.Focus();
				txtPeriod.SelectionStart = 0;
				txtPeriod.SelectionLength = txtPeriod.Text.Length;
			}
		}

		private void frmGJDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (this.Visible)
			{
				if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
				{
					if (vs1.Col == AccountCol && vs1.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
					{
						modNewAccountBox.CheckFormKeyDown(vs1, vs1.Row, vs1.Col, KeyCode, Shift, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
						vs1.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
					}
				}
			}
		}

		private void frmGJDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGJDataEntry.FillStyle	= 0;
			//frmGJDataEntry.ScaleWidth	= 9300;
			//frmGJDataEntry.ScaleHeight	= 7395;
			//frmGJDataEntry.LinkTopic	= "Form2";
			//frmGJDataEntry.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			int counter2;
			string strLabel = "";
			clsDRWrapper rsEncAdj = new clsDRWrapper();
			clsDRWrapper rsProjects = new clsDRWrapper();
			blnJournalLocked = false;
			NumberCol = 0;
			DateCol = 1;
			DescriptionCol = 2;
			RCBCol = 3;
			AccountCol = 4;
			ProjCol = 5;
			DebitCol = 6;
			CreditCol = 7;
			vsGrid.GRID7Light = vs1;
            vsGrid.DefaultAccountType = "E";
			vsGrid.AccountCol = FCConvert.ToInt16(AccountCol);
			blnSaved = false;
			vs1.ColWidth(NumberCol, 0);
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.25 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.23 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.066 * vs1.WidthOriginal));
			vs1.ColWidth(RCBCol, FCConvert.ToInt32(0.066 * vs1.WidthOriginal));
			vs1.ColWidth(DebitCol, FCConvert.ToInt32(0.14 * vs1.WidthOriginal));
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, DateCol, "Date");
			vs1.TextMatrix(0, ProjCol, "Proj");
			vs1.TextMatrix(0, AccountCol, "Account");
			vs1.TextMatrix(0, DebitCol, "Debit");
			vs1.TextMatrix(0, CreditCol, "Credit");
			vs1.TextMatrix(0, RCBCol, "RCBF");
			//FC:FINAL:DDU:#1189 - aligned grid column
			vs1.ColAlignment(DateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(RCBCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(ProjCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(DebitCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(CreditCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, DateCol, 0, CreditCol, 4);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			// vs1.ColFormat(DateCol) = "Short Date"
			vs1.ColFormat(DebitCol, "#,###.00");
			vs1.ColFormat(CreditCol, "#,###.00");
			vs1.ColEditMask(DateCol, "0#/0#/0#");
            vs1.ColAllowedKeys(DebitCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
            vs1.ColAllowedKeys(CreditCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
            strComboList = "# ; " + "\t" + " |";
			if (modBudgetaryMaster.Statics.ProjectFlag)
			{
				rsProjects.OpenRecordset("SELECT * FROM ProjectMaster");
				if (rsProjects.EndOfFile() != true && rsProjects.BeginningOfFile() != true)
				{
					do
					{
						strComboList += "'" + rsProjects.Get_Fields_String("ProjectCode") + ";" + rsProjects.Get_Fields_String("ProjectCode") + "\t" + rsProjects.Get_Fields_String("LongDescription") + "|";
						rsProjects.MoveNext();
					}
					while (rsProjects.EndOfFile() != true);
					strComboList = Strings.Left(strComboList, strComboList.Length - 1);
				}
				vs1.ColComboList(ProjCol, strComboList);
			}
			FillJournalCombo();
			LoadDefaultCombo();
			if (!modBudgetaryMaster.Statics.blnGJEdit)
			{
				blnPayroll = false;
				for (counter = 1; counter <= vs1.Rows - 1; counter++)
				{
					vs1.TextMatrix(counter, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(counter, DebitCol, 0);
					vs1.TextMatrix(counter, CreditCol, 0);
					vs1.TextMatrix(counter, RCBCol, "R");
					vs1.TextMatrix(counter, NumberCol, 0);
					vs1.TextMatrix(counter, DescriptionCol, "");
					vs1.TextMatrix(counter, AccountCol, "");
					vs1.TextMatrix(counter, ProjCol, "");
				}
				txtPeriod.Text = CalculatePeriod();
				if (ChosenJournal == 0)
				{
					cboJournal.SelectedIndex = 0;
					// put journal number in the text box
					cboSaveJournal.SelectedIndex = 0;
				}
				else
				{
					SetCombo(ChosenJournal, ref ChosenPeriod);
					txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
				}
			}
			else
			{
				CalculateTotals();
				lblNetTotal.Text = Strings.FormatCurrency((TotalDebit - TotalCredit), 2);
				rsEncAdj.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(ChosenJournal) + " AND Period = " + FCConvert.ToString(ChosenPeriod) + " AND RCB <> 'E'");
				if (rsEncAdj.EndOfFile() != true && rsEncAdj.BeginningOfFile() != true)
				{
					lblReadOnly.Visible = false;
					vs1.Enabled = true;
					// cboJournal.Enabled = True
					txtPeriod.Enabled = true;
					btnFileSave.Enabled = true;
				}
				else
				{
					lblReadOnly.Visible = true;
					vs1.Enabled = false;
					cboJournal.Enabled = false;
					txtPeriod.Enabled = true;
					btnFileSave.Enabled = true;
				}
				FillJournalCombo();
				SetCombo(ChosenJournal, ref ChosenPeriod);
				txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2);
			}
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, RCBCol, vs1.Rows - 1, RCBCol, 4);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			this.vs1.EditingControlShowing -= vs1_EditingControlShowing;
			this.vs1.EditingControlShowing += vs1_EditingControlShowing;
		}
		//FC:FINAL:MSH - Issue #756: add handler for KeyDown event in EditControl of the table
		private void vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - Issue #756: in VB6 vs1_KeyDownEdit event is executed when key pressed in edit area
				e.Control.KeyDown -= vs1_KeyDownEdit;
				e.Control.KeyDown += vs1_KeyDownEdit;
			}
		}

		private void frmGJDataEntry_Resize(object sender, System.EventArgs e)
		{
			int counter;
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.25 * vs1.WidthOriginal));
			vs1.ColWidth(AccountCol, FCConvert.ToInt32(0.23 * vs1.WidthOriginal));
			vs1.ColWidth(ProjCol, FCConvert.ToInt32(0.066 * vs1.WidthOriginal));
			vs1.ColWidth(RCBCol, FCConvert.ToInt32(0.066 * vs1.WidthOriginal));
			vs1.ColWidth(DebitCol, FCConvert.ToInt32(0.14 * vs1.WidthOriginal));
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				//vs1.RowHeight(counter, FCConvert.ToInt32(vs1.Height * 0.06));
			}
			fraDefaultType.CenterToContainer(this.ClientArea);
			fraJournalSave.CenterToContainer(this.ClientArea);
		}

		private void FrmGJDataEntry_FormClosed(object sender, FormClosedEventArgs e)
		{
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (openFormWhenThisClose)
			{
				frmGetGJDataEntry.InstancePtr.Show(App.MainForm);
			}
			//FC:FINAL:DDU:#2944 - moved code from queryunload
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (!modBudgetaryMaster.Statics.gboolClosingProgram)
			{
				if (!blnSaved)
				{
					ans = MessageBox.Show("Are you sure you wish to exit this screen?", "Exit Screen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						e.Cancel = true;
						return;
					}
				}
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
				if (boolShowDataEntryForm)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					//FC:FINAL:DDU:#1082 - hide form before showing another to focus well
					//this.Hide();
					openFormWhenThisClose = true;
				}
			}
			else
			{
				if (blnJournalLocked)
				{
					modBudgetaryAccounting.UnlockJournal();
				}
			}
		}

		private void frmGJDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraJournalSave.Visible == true)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					fraJournalSave.Visible = false;
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				if (frmGJDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
				{
					if (frmGJDataEntry.InstancePtr.ActiveControl.GetName() == "cboJournal")
					{
						KeyAscii = (Keys)0;
						vs1.Focus();
					}
					else
					{
						KeyAscii = (Keys)0;
						Support.SendKeys("{TAB}", false);
					}
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileLoadType_Click(object sender, System.EventArgs e)
		{
			fraDefaultType.Text = "Select Default Type";
			cboDefaultType.Visible = true;
			txtTypeDescription.Visible = false;
			fraDefaultType.Visible = true;
			cboDefaultType.SelectedIndex = 0;
			cboDefaultType.Focus();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			if (Conversion.Val(txtPeriod.Text) != 0)
			{
				SaveInfo();
			}
			else
			{
				MessageBox.Show("You must enter a valid period before you may continue", "Invalid Accounting Period", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuFileSaveType_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnData;
			int counter;
			blnData = false;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol)) != 0 || FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol)) != 0 || vs1.TextMatrix(counter, DescriptionCol) != "" || vs1.TextMatrix(counter, AccountCol) != "" || (modBudgetaryMaster.Statics.ProjectFlag && vs1.TextMatrix(counter, ProjCol) != ""))
				{
					blnData = true;
				}
				if (vs1.TextMatrix(counter, AccountCol) != "")
				{
					if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
					{
						ans = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.No)
						{
							vs1.Select(counter, AccountCol);
							vs1.Focus();
							return;
						}
						else
						{
							modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "General Journal Default Data Entry");
						}
					}
				}
			}
			if (!blnData)
			{
				MessageBox.Show("You must make at least one entry before you may save this", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			fraDefaultType.Text = "Enter Default Description";
			cboDefaultType.Visible = false;
			txtTypeDescription.Text = "";
			txtTypeDescription.Visible = true;
			fraDefaultType.Visible = true;
			txtTypeDescription.Focus();
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As short, int --> As DialogResult
			DialogResult counter;
			int temp = 0;
			int intFocusRow;
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (vs1.TextMatrix(vs1.Row, DescriptionCol) == "" && vs1.TextMatrix(vs1.Row, AccountCol) == "" && Convert.ToDateTime(vs1.TextMatrix(vs1.Row, DateCol)).ToOADate() == DateTime.Today.ToOADate() && vs1.TextMatrix(vs1.Row, ProjCol) == "" && vs1.TextMatrix(vs1.Row, DebitCol) == "0" && vs1.TextMatrix(vs1.Row, CreditCol) == "0")
			{
				// do nothing
			}
			else
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.Select(vs1.Row, CreditCol, vs1.Row, NumberCol);
				counter = MessageBox.Show("Are you sure you want to delete this line?", "Delete This Line?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (counter == DialogResult.Yes)
				{
					DeleteFlag = true;
					temp = vs1.Row;
					// If vs1.Row < vs1.rows - 1 Then
					// intFocusRow = vs1.Row + 1
					// Else
					// intFocusRow = vs1.Row - 1
					// End If
					txtPeriod.Focus();
					if (Conversion.Val(vs1.TextMatrix(temp, NumberCol)) != 0)
					{
						rsDelete.Execute("DELETE FROM JournalEntries WHERE ID = " + vs1.TextMatrix(temp, NumberCol), "Budgetary");
					}
					vs1.RemoveItem(temp);
					vs1.Rows += 1;
					vs1.TextMatrix(vs1.Rows - 1, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(vs1.Rows - 1, DebitCol, 0);
					vs1.TextMatrix(vs1.Rows - 1, CreditCol, 0);
					vs1.TextMatrix(vs1.Rows - 1, NumberCol, 0);
					vs1.TextMatrix(vs1.Rows - 1, RCBCol, "R");
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Rows - 1, RCBCol, 4);
					CalculateTotals();
					lblNetTotal.Text = Strings.FormatCurrency((TotalDebit - TotalCredit), 2);
					DeleteFlag = false;
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(temp, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
				else
				{
					vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
					vs1.Select(vs1.Row, DateCol);
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			if (Conversion.Val(txtPeriod.Text) != 0)
			{
				SaveInfo();
			}
			else
			{
				MessageBox.Show("You must enter a valid period before you may continue", "Invalid Accounting Period", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private string CalculatePeriod()
		{
			string CalculatePeriod = "";
			int temp = 0;
			if (modGlobalConstants.Statics.gstrArchiveYear != "")
			{
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					temp = 12;
				}
				else
				{
					temp = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else
			{
				temp = DateTime.Now.Month;
				// calculate the period by which month it is
			}
			if (temp < 10)
			{
				CalculatePeriod = "0" + Strings.Trim(Conversion.Str(temp));
			}
			else
			{
				CalculatePeriod = Strings.Trim(Conversion.Str(temp));
			}
			return CalculatePeriod;
		}

		private void txtPeriod_Enter(object sender, System.EventArgs e)
		{
			if (fraJournalSave.Visible == true)
			{
				txtJournalDescription.Focus();
			}
			lblExpense.Text = "";
			txtPeriod.SelectionStart = 0;
			txtPeriod.SelectionLength = txtPeriod.Text.Length;
		}

		private void txtPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string temp = "";
			if (txtPeriod.Text == "")
			{
				txtPeriod.Text = CalculatePeriod();
			}
			else if (!Information.IsNumeric(txtPeriod.Text))
			{
				MessageBox.Show("You must enter a number in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else if (Conversion.Val(txtPeriod.Text) > 12)
			{
				MessageBox.Show("You must enter a number less then 12 in this field", "Invalid Period", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtPeriod.Text = "";
			}
			else
			{
				temp = txtPeriod.Text;
				txtPeriod.Text = modValidateAccount.GetFormat_6(temp, 2);
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
		{
			if (vs1.IsCurrentCellInEditMode)
			{
				if (vs1.Col == AccountCol)
				{
					lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.EditText);
				}
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (fraJournalSave.Visible == true)
			{
				txtJournalDescription.Focus();
			}
			if (frmGJDataEntry.InstancePtr.ActiveControl.GetName() != "vs1")
			{
				return;
			}
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			btnProcessDelete.Enabled = true;
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DateCol;
			}
			else if (vs1.Col != AccountCol && (vs1.Col != RCBCol || vs1.TextMatrix(vs1.Row, vs1.Col) != "L"))
			{
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vs1[e.ColumnIndex, e.RowIndex];
            if (vs1.GetFlexColIndex(e.ColumnIndex) == RCBCol)
			{
				//FC:FINAL:MSH - Issue #754: return delay time for tooltip if mouse over the RCBCol
				if (ToolTip1.AutoPopDelay <= 0)
				{
					ToolTip1.AutoPopDelay = 5000;
					ToolTip1.AutomaticDelay = 500;
					ToolTip1.InitialDelay = 500;
					ToolTip1.Update();
				}
				//ToolTip1.SetToolTip(vs1, "R - Regular,  C - Correction,  B - Budget Adjustment,  F - Carry Forward");
				cell.ToolTipText = "R - Regular,  C - Correction,  B - Budget Adjustment,  F - Carry Forward";
			}
			else
			{
				//FC:FINAL:MSH - Issue #754: erase delay of tooltip
				if (ToolTip1.AutoPopDelay != 0)
				{
					ToolTip1.AutoPopDelay = 0;
					ToolTip1.AutomaticDelay = 0;
					ToolTip1.InitialDelay = 0;
					ToolTip1.Update();
				}
                //ToolTip1.SetToolTip(vs1, string.Empty);
                cell.ToolTipText = "";
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			int counter;
			if (vs1.Row < 1)
				return;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			lblExpense.Text = modAccountTitle.ReturnAccountDescription(vs1.TextMatrix(vs1.Row, AccountCol));
			vs1.EditMaxLength = 0;
			if (vs1.Col == NumberCol)
			{
				vs1.Col = DateCol;
			}
			else if (vs1.Col == ProjCol)
			{
				if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
				{
					vs1.Col += 1;
				}
				else
				{
					vs1.EditCell();
				}
			}
			else if (vs1.Col == RCBCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "L")
				{
					vs1.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vs1.EditMaxLength = 1;
					vs1.EditCell();
					vs1.EditSelStart = 0;
					vs1.EditSelLength = 1;
				}
			}
			else if (vs1.Col == DescriptionCol)
			{
				vs1.EditMaxLength = 25;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
			else
			{
				if (vs1.Col == DebitCol || vs1.Col == CreditCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					vs1.EditCell();
					vs1.EditSelStart = 0;
				}
			}
			if (vs1.Row == vs1.Rows - 1 && vs1.Col == vs1.Cols - 1)
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vs1.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.Col != AccountCol)
			{
				if (vs1.Col == DebitCol || vs1.Col == CreditCol)
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(vs1.Row, vs1.Col)) == 0)
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
						vs1.EditSelLength = 1;
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
				else
				{
					if (vs1.Col == RCBCol && vs1.TextMatrix(vs1.Row, vs1.Col) == "L")
					{
						// do nothing
					}
					else
					{
						vs1.EditCell();
						vs1.EditSelStart = 0;
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vs1.Col == AccountCol && e.KeyCode != Keys.F9)
			{
				if (FCConvert.ToInt32(KeyCode) == 37 && vs1.EditSelStart == 0)
				{
					KeyCode = 0;
					vs1.Col -= 1;
				}
				else if (FCConvert.ToInt32(KeyCode) == 39 && vs1.EditSelStart == vs1.EditMaxLength)
				{
					KeyCode = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
			//FC:FINAL:MSH - Incorrect conversion from VB. In VB vbKeyReturn equal 'Enter' key.
			//if (KeyCode == Keys.Return)
			if (KeyCode == Keys.Enter)
			{
				KeyCode = 0;
				if (vs1.Col < CreditCol)
				{
					if (vs1.Col == DateCol || vs1.Col == DescriptionCol)
					{
						if (vs1.Row > 1)
						{
							//FC:FINAL:MSH - Issue #756: in original app value in TextMatrix equal value in EditControl, but here value will be setted to 
							// cell after closing EditControl, so we must check value in EditText, not in vs1.TextMatrix(vs1.Row, vs1.Col)
							//if ((vs1.Col == DescriptionCol && vs1.TextMatrix(vs1.Row, vs1.Col) == "") || vs1.Col == DateCol)
							if ((vs1.Col == DescriptionCol && string.IsNullOrEmpty(vs1.EditText)) || vs1.Col == DateCol)
							{
								//FC:FINAL:MSH - Issue #756: setting a value by using TextMatrix is replaced by setting a value to the EditText, because 
								// value from EditText will replace value in cell when EditableControl will be closed
								//vs1.TextMatrix(vs1.Row, vs1.Col, vs1.TextMatrix(vs1.Row - 1, vs1.Col));
								vs1.EditText = vs1.TextMatrix(vs1.Row - 1, vs1.Col);
							}
						}
					}
                    //FC:FINAL:MSH - Issue #756: increment moved, because in VB6 incrementing of Col value doesn't have affect, but here after incrementing of
                    // Col value will be called vs1_ValidateEdit, in which value of Col changed, but value of EditText not changed
                    //FC:FINAL:AM:#2854 - validate first the current cell
                    //vs1.Col += 1;
                    if (vs1.CommitEdit())
                    {
                        vs1.Col += 1;
                    }
				}
				else
				{
					//FC:FINAL:DSE:#673 Use RowsCount to check if current row is the last row
					//if (vs1.Row < vs1.Rows - 1)
					if (vs1.Row < vs1.RowCount - 1)
					{
						vs1.Col = 0;
						vs1.Row += 1;
					}
					else
					{
						vs1.AddItem("");
						vs1.TextMatrix(vs1.Row + 1, DebitCol, 0);
						vs1.TextMatrix(vs1.Row + 1, CreditCol, 0);
						vs1.TextMatrix(vs1.Row + 1, RCBCol, "R");
						vs1.TextMatrix(vs1.Row + 1, NumberCol, 0);
						//FC:FINAL:MSH - Issue #756: Incorrect date format for String.Format
						vs1.TextMatrix(vs1.Row + 1, DateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
						//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vs1.Row + 1, RCBCol, 4);
						vs1.Col = DateCol;
						vs1.Row += 1;
					}
				}
			}
			else if (KeyCode == Keys.Left)
			{
				if (vs1.Col == DebitCol)
				{
					if (!modBudgetaryMaster.Statics.ProjectFlag || strComboList == "")
					{
						KeyCode = 0;
						vs1.Col -= 2;
					}
				}
			}
			else if (vs1.Col == CreditCol || vs1.Col == DebitCol)
			{
				if (e.KeyCode == Keys.C)
				{
					// if the c ID is hit then erase the value
					KeyCode = 0;
					vs1.EditText = "";
					Support.SendKeys("{BACKSPACE}", false);
				}
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int temp = 0;
			string holder = "";
			// FC:FINAL:MSH - Issue #756: saving correct indexes of Row and Col, for which validating executed and using them in method
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == DateCol)
			{
				if (!Information.IsDate(vs1.EditText))
				{
					MessageBox.Show("You must enter a valid date in this field", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					vs1.EditText = "";
                    vs1.TextMatrix(vs1.Row, vs1.Col, "");
                }
			}
			else if (col == DebitCol || col == CreditCol)
			{
				if (Strings.Trim(vs1.EditText) != "")
				{
                    if (!Information.IsNumeric(vs1.EditText))
                    {
                        MessageBox.Show("You must enter a number in this field", "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:AM:#3194 - set the cell value too
                        //e.Cancel = true;
                        vs1.EditText = "";
                        vs1.TextMatrix(vs1.Row, vs1.Col, "");
                    }
                    else
                    {
						temp = Strings.InStr(vs1.EditText, ".", CompareConstants.vbBinaryCompare);
						if (temp != 0)
						{
							if (vs1.EditText.Length > temp + 2)
							{
								vs1.EditText = Strings.Left(vs1.EditText, temp + 2);
							}
						}
						CalculateTotals();
						lblNetTotal.Text = Strings.FormatCurrency((TotalDebit - TotalCredit), 2);
					}
				}
				else
				{
					if (vs1.EditText == "")
					{
						vs1.EditText = "0";
					}
				}
			}
			else if (col == RCBCol)
			{
				if (vs1.EditText == "")
				{
					vs1.EditText = "R";
				}
				else if (Strings.UCase(vs1.EditText) == "R" || Strings.UCase(vs1.EditText) == "C" || Strings.UCase(vs1.EditText) == "B" || Strings.UCase(vs1.EditText) == "F")
				{
					vs1.EditText = Strings.UCase(vs1.EditText);
				}
				else
				{
					MessageBox.Show("You may only enter an R, C, B, or F in this field", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:AM:#3194 - set the cell value too
                    //e.Cancel = true;
                    vs1.EditText = "";
                    vs1.TextMatrix(vs1.Row, vs1.Col, "");
                }
			}
		}

		private void CalculateTotals()
		{
			int counter;
			TotalDebit = 0;
			TotalCredit = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (counter == vs1.Row)
				{
					if (vs1.Col == DebitCol)
					{
						if (Information.IsNumeric(vs1.EditText))
						{
							TotalDebit += FCConvert.ToDouble(vs1.EditText);
						}
						if (Information.IsNumeric(vs1.TextMatrix(counter, CreditCol)))
						{
							TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
						}
					}
					else if (vs1.Col == CreditCol)
					{
						if (Information.IsNumeric(vs1.EditText))
						{
							TotalCredit += FCConvert.ToDouble(vs1.EditText);
						}
						if (Information.IsNumeric(vs1.TextMatrix(counter, DebitCol)))
						{
							TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
						}
					}
					else
					{
						if (Information.IsNumeric(vs1.TextMatrix(counter, CreditCol)))
						{
							TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
						}
						if (Information.IsNumeric(vs1.TextMatrix(counter, DebitCol)))
						{
							TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
						}
					}
				}
				else
				{
					if (Information.IsNumeric(vs1.TextMatrix(counter, CreditCol)))
					{
						TotalCredit += FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol));
					}
					if (Information.IsNumeric(vs1.TextMatrix(counter, DebitCol)))
					{
						TotalDebit += FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol));
					}
				}
			}
		}
		// vbPorter upgrade warning: intPeriod As short	OnWriteFCConvert.ToInt32(
		private void SetCombo(int x, ref int intPeriod)
		{
			int counter;
			for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(intPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
				{
					cboJournal.SelectedIndex = counter;
					cboSaveJournal.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.SelectedIndex = cboJournal.SelectedIndex;
			lblExpense.Text = "";
		}

		private void cboJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void cboJournal_Enter(object sender, System.EventArgs e)
		{
			lblExpense.Text = "";
		}

		private void cmdCancelSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			blnJournalLocked = false;
			if (Master.IsntAnything())
			{
				// do nothing
			}
			else
			{
				Master.Reset();
			}
			fraJournalSave.Visible = false;
			btnFileSave.Enabled = true;
			btnFileSaveType.Enabled = true;
			btnFileSaveType.Enabled = true;
			btnProcessDelete.Enabled = true;
			if (cboDefaultType.Items.Count > 0)
			{
				btnFileLoadType.Enabled = true;
			}
		}

		private void cmdOKSave_Click(object sender, System.EventArgs e)
		{
			SaveJournal();
		}

		private void cboSaveJournal_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			cboSaveJournal.Width = 550;
			if (fraJournalSave.Visible == true)
			{
				if (cboSaveJournal.SelectedIndex == 0)
				{
					txtJournalDescription.Visible = true;
					lblJournalDescription.Visible = true;
					lblSaveInstructions.Text = "Please enter a description for the journal and click the OK button";
					txtJournalDescription.Focus();
				}
				else
				{
					txtJournalDescription.Visible = false;
					lblJournalDescription.Visible = false;
					lblSaveInstructions.Text = "Please click the OK button to save";
					cmdOKSave.Focus();
				}
			}
		}

		private void cboSaveJournal_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSaveJournal.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void SaveJournal()
		{
			int TempJournal = 0;
			int counter;
			int ans;
			clsDRWrapper rsDeleteInfo = new clsDRWrapper();
			int lngCheckJournal = 0;
			Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (cboSaveJournal.SelectedIndex == 0)
			{
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", TempJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", txtJournalDescription.Text);
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
			}
			else
			{
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Status = 'P'");
				if (Master.BeginningOfFile() != true && Master.EndOfFile() != true)
				{
					MessageBox.Show("Changes to this journal cannot be saved because it has been posted.", "Unable To Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					blnSaved = true;
					Close();
					return;
				}
				Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
				bool executeAddTag = false;
				if (cboSaveJournal.SelectedIndex != 0 && Strings.Trim(txtJournalDescription.Text) != "")
				{
					executeAddTag = true;
					goto AddTag;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				if (Master.Get_Fields("Period") != Conversion.Val(txtPeriod.Text))
				{
					// Master.OpenRecordset ("SELECT * FROM JournalMaster WHERE JournalNumber = " & Val(Left(cboSaveJournal.list(cboSaveJournal.ListIndex), 4)) & " AND Period = " & Val(txtPeriod.Text))
					// If Master.EndOfFile <> True And Master.BeginningOfFile <> True Then
					// ans = MsgBox("This Journal Number is used for entries in Accounting Period " & Val(Mid(cboSaveJournal.list(cboSaveJournal.ListIndex), 19, 2)) & ".  There is a Journal with this Journal Number for the Accounting Period you are using.  Would you like to add your entries to this Journal?", vbQuestion + vbYesNo, "Add Entries?")
					// If ans = vbYes Then
					// If blnGJEdit Then
					// rsDeleteInfo.Execute ("DELETE FROM JournalMaster WHERE JournalNumber = " & Val(Left(cboSaveJournal.list(cboSaveJournal.ListIndex), 4)) & " AND Period = " & Val(Mid(cboSaveJournal.list(cboSaveJournal.ListIndex), 19, 2)))
					// cboSaveJournal.RemoveItem cboSaveJournal.ListIndex
					// cboJournal.RemoveItem cboJournal.ListIndex
					// End If
					// For counter = 1 To cboJournal.ListCount - 1
					// If Val(Left(cboSaveJournal.list(counter), 4)) = Master.Fields["JournalNumber"] And Val(Mid(cboSaveJournal.list(counter), 19, 2)) = Val(txtPeriod.Text) Then
					// cboSaveJournal.ListIndex = counter
					// cboJournal.ListIndex = counter
					// Exit For
					// End If
					// Next
					// Else
					// cboJournal.ListIndex = 0
					// mnuProcessSave_Click
					// Exit Sub
					// End If
					// Else
					// ans = MsgBox("This Journal Number is used for entries in Accounting Period " & Val(Mid(cboSaveJournal.list(cboSaveJournal.ListIndex), 19, 2)) & ".  Do you wish to use this Journal Number?", vbQuestion + vbYesNo, "Update Journal?")
					// If ans = vbYes Then
					// If Trim(txtJournalDescription.Text) <> "" Or blnGJEdit Then
					executeAddTag = true;
					goto AddTag;
					// Else
					// Master.Addnew
					// Master.Fields["JournalNumber"] = Val(Left(cboSaveJournal.list(cboSaveJournal.ListIndex), 4))
					// Master.Fields["Status"] = "E"
					// Master.Fields["Description"] = txtJournalDescription
					// Master.Fields["Type"] = "GJ"
					// Master.Fields["Period"] = Val(txtPeriod.Text)
					// Master.Update
					// Master.Reset
					// End If
					// Else
					// fraJournalSave.Visible = True
					// lblSaveInstructions = "Please type in a description for the journal you are going to save and click the OK button"
					// lblJournalSave.Visible = False
					// cboSaveJournal.Visible = False
					// lblJournalDescription.Visible = True
					// txtJournalDescription.Visible = True
					// txtJournalDescription.SetFocus
					// Exit Sub
					// End If
					// Else
					// cboJournal.ListIndex = 0
					// mnuProcessSave_Click
					// Exit Sub
					// End If
					// End If
				}
				else
				{
					if (modBudgetaryMaster.Statics.blnGJEdit)
					{
						if (ChosenJournal != Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4)))
						{
							rsDeleteInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(ChosenJournal) + " AND Period = " + FCConvert.ToString(ChosenPeriod));
							if (rsDeleteInfo.EndOfFile() != true && rsDeleteInfo.BeginningOfFile() != true)
							{
								rsDeleteInfo.Edit();
								rsDeleteInfo.Set_Fields("Status", "D");
								rsDeleteInfo.Update();
							}
							for (counter = 0; counter <= cboJournal.Items.Count - 1; counter++)
							{
								if (modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenJournal), 4) == Strings.Left(cboJournal.Items[counter].ToString(), 4) && modValidateAccount.GetFormat_6(FCConvert.ToString(ChosenPeriod), 2) == Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2))
								{
									cboSaveJournal.Items.RemoveAt(counter);
									cboJournal.Items.RemoveAt(counter);
								}
							}
						}
					}
				}
				AddTag:
				;
				if (executeAddTag)
				{
					// If blnGJEdit Then
					Master.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))) + " AND Period = " + FCConvert.ToString(Conversion.Val(Strings.Mid(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 19, 2))));
					Master.Edit();
					Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(txtPeriod.Text)));
					Master.Update();
					cboJournal.Clear();
					cboSaveJournal.Clear();
					FillJournalCombo();
					for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == Master.Get_Fields("JournalNumber") && Conversion.Val(Strings.Mid(cboSaveJournal.Items[counter].ToString(), 19, 2)) == Conversion.Val(txtPeriod.Text))
						{
							cboSaveJournal.SelectedIndex = counter;
							cboJournal.SelectedIndex = counter;
							break;
						}
					}
					Master.Execute("UPDATE JournalEntries SET Period = " + FCConvert.ToString(Conversion.Val(txtPeriod.Text)) + " WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))), "TWBD0000.vb1");
					Master.Reset();
				}
				modBudgetaryAccounting.UnlockJournal();
				blnJournalLocked = false;
				if (Master.IsntAnything())
				{
					// do nothing
				}
				else
				{
					Master.Reset();
				}
			}
			if (cboSaveJournal.SelectedIndex != 0)
			{
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
				modBudgetaryMaster.Statics.CurrentGJEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				modRegistry.SaveRegistryKey("CURRGJJRNL", FCConvert.ToString(TempJournal));
				modBudgetaryMaster.Statics.CurrentGJEntry = TempJournal;
			}
			if (modBudgetaryMaster.Statics.blnGJEdit)
			{
				// rs.OpenRecordset ("SELECT * FROM JournalEntries WHERE (Type = 'G' OR Type = 'P') and JournalNumber = " & ChosenJournal & " AND Period = " & ChosenPeriod & " AND Status = 'E'")
				// If rs.EndOfFile <> True And rs.BeginningOfFile <> True Then
				// rs.MoveLast
				// rs.MoveFirst
				// Do Until rs.EndOfFile
				// rs.Delete
				// rs.MoveNext
				// Loop
				// End If
				ChosenPeriod = FCConvert.ToInt32(txtPeriod.Text);
			}
			clsPostInfo.ClearJournals();
			clsJournalInfo = new clsPostingJournalInfo();
			if (cboSaveJournal.SelectedIndex != 0)
			{
				clsJournalInfo.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboSaveJournal.Items[cboSaveJournal.SelectedIndex].ToString(), 4))));
			}
			else
			{
				clsJournalInfo.JournalNumber = TempJournal;
			}
			if (blnPayroll)
			{
				clsJournalInfo.JournalType = "PY";
			}
			else
			{
				clsJournalInfo.JournalType = "GJ";
			}
			clsJournalInfo.Period = txtPeriod.Text;
			clsJournalInfo.CheckDate = "";
			clsPostInfo.AddJournal(clsJournalInfo);
			rs.OmitNullsOnInsert = true;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, CreditCol).ToDecimalValue() != 0 || vs1.TextMatrix(counter, DebitCol).ToDecimalValue() != 0)
				{
					if (Conversion.Val(vs1.TextMatrix(counter, NumberCol)) == 0)
					{
						rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
						rs.AddNew();
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = " + vs1.TextMatrix(counter, NumberCol));
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.Edit();
						}
						else
						{
							rs.AddNew();
						}
					}
					if (blnPayroll)
					{
						rs.Set_Fields("Type", "P");
					}
					else
					{
						rs.Set_Fields("Type", "G");
					}
					rs.Set_Fields("JournalEntriesDate", vs1.TextMatrix(counter, DateCol));
					rs.Set_Fields("Description", vs1.TextMatrix(counter, DescriptionCol));
					if (cboSaveJournal.SelectedIndex != 0)
					{
						rs.Set_Fields("JournalNumber", FCConvert.ToString(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						rs.Set_Fields("JournalNumber", TempJournal);
					}
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					lngCheckJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
					rs.Set_Fields("Period", txtPeriod.Text);
					rs.Set_Fields("Project", vs1.TextMatrix(counter, ProjCol));
					rs.Set_Fields("account", vs1.TextMatrix(counter, AccountCol));
					if (Strings.Trim(Strings.UCase(vs1.TextMatrix(counter, RCBCol))) == "F")
					{
						rs.Set_Fields("RCB", "B");
						rs.Set_Fields("CarryForward", true);
					}
					else
					{
						rs.Set_Fields("RCB", vs1.TextMatrix(counter, RCBCol));
						rs.Set_Fields("CarryForward", false);
					}
					rs.Set_Fields("Amount", Conversion.Val(vs1.TextMatrix(counter, DebitCol).Replace(",", "")) - Conversion.Val(vs1.TextMatrix(counter, CreditCol).Replace(",", "")));
					rs.Set_Fields("Status", "E");
					rs.Update();
					vs1.TextMatrix(counter, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
				}
			}
			if (blnUnload)
			{
				if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptGeneralJournal))
				{
					if (MessageBox.Show("Save Successful.  Would you like to post this journal?", "Information Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						blnPostJournal = true;
					}
				}
				else
				{
					blnPostJournal = false;
					if (cboSaveJournal.SelectedIndex != 0)
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboJournal.Items[cboJournal.SelectedIndex].ToString(), 4))));
					}
					else
					{
						modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(TempJournal));
					}
				}
				blnSaved = true;
				Close();
			}
			else
			{
				cboJournal.Clear();
				cboSaveJournal.Clear();
				FillJournalCombo();
				for (counter = 1; counter <= cboJournal.Items.Count - 1; counter++)
				{
					if (Conversion.Val(Strings.Left(cboSaveJournal.Items[counter].ToString(), 4)) == lngCheckJournal)
					{
						cboSaveJournal.SelectedIndex = counter;
						cboJournal.SelectedIndex = counter;
						break;
					}
				}
				txtJournalDescription.Text = "";
				fraJournalSave.Visible = false;
				MessageBox.Show("Save Successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				btnFileSave.Enabled = true;
				btnFileSaveType.Enabled = true;
				btnProcessDelete.Enabled = true;
				if (cboDefaultType.Items.Count > 0)
				{
					btnFileLoadType.Enabled = true;
				}
			}
		}

		private void FillJournalCombo()
		{
			clsDRWrapper rsJournalPeriod = new clsDRWrapper();
			int counter;
			cboJournal.AddItem("Auto");
			cboSaveJournal.AddItem("Auto");
			if (!blnPayroll)
			{
				if (vs1.Enabled)
				{
					rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'GJ' AND left(Description, 14) <> 'Enc Adjust for' ORDER BY JournalNumber DESC");
				}
				else
				{
					rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'GJ' ORDER BY JournalNumber DESC");
				}
			}
			else
			{
				rsJournalPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'PY' ORDER BY JournalNumber DESC");
			}
			if (rsJournalPeriod.EndOfFile() != true && rsJournalPeriod.BeginningOfFile() != true)
			{
				rsJournalPeriod.MoveLast();
				rsJournalPeriod.MoveFirst();
				for (counter = 1; counter <= rsJournalPeriod.RecordCount(); counter++)
				{
					//FC:FINAL:MSH - Issue #759: removed adding of empty rows to combobox
					//if (cboJournal.ListCount <= counter)
					//{
					//    cboJournal.AddItem("");
					//}
					//if (cboSaveJournal.ListCount <= counter)
					//{
					//    cboSaveJournal.AddItem("");
					//}
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsJournalPeriod.Get_Fields("Period")) < 10)
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per 0" + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboSaveJournal.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rsJournalPeriod.Get_Fields("JournalNumber")), 4) + " - " + "  Acct Per " + rsJournalPeriod.Get_Fields("Period") + "     " + rsJournalPeriod.Get_Fields_String("Description"));
					}
					rsJournalPeriod.MoveNext();
				}
			}
		}

		private string Department(ref string x)
		{
			string Department = "";
			Department = Strings.Mid(x, 3, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)))));
			return Department;
		}

		private void SetCustomFormColors()
		{
			lblExpense.ForeColor = Color.Blue;
			lblReadOnly.ForeColor = Color.Red;
		}

		private void SaveInfo()
		{
			int counter;
			int TempJournal = 0;
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnData;
			blnData = false;
			// txtPeriod.SetFocus
			vs1.Row = 0;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (FCConvert.ToDouble(vs1.TextMatrix(counter, CreditCol)) != 0 || FCConvert.ToDouble(vs1.TextMatrix(counter, DebitCol)) != 0)
				{
					blnData = true;
					if (vs1.TextMatrix(counter, DescriptionCol) == "" || vs1.TextMatrix(counter, AccountCol) == "" || vs1.TextMatrix(counter, RCBCol) == "")
					{
						vs1.Select(counter, DescriptionCol);
						MessageBox.Show("You must fill in all the data for each entry before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					if (FCConvert.ToDouble(vs1.TextMatrix(counter, NumberCol)) != 0)
					{
						MessageBox.Show("Missing data, use F3 to delete line if necessary.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, DebitCol);
						return;
					}
				}
				if (modBudgetaryMaster.CheckIfControlAccount(vs1.TextMatrix(counter, AccountCol)))
				{
					ans = MessageBox.Show("Account " + vs1.TextMatrix(counter, AccountCol) + " is set up as a Control Account.  Are you sure you wish to use this account?", "Use Control Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.No)
					{
						vs1.Select(counter, AccountCol);
						vs1.Focus();
						return;
					}
					else
					{
						modBudgetaryMaster.WriteAuditRecord_8("Used Control Account " + vs1.TextMatrix(counter, AccountCol), "General Journal Data Entry");
					}
				}
				if (Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) == "G" || Strings.Left(vs1.TextMatrix(counter, AccountCol), 1) == "L")
				{
					if (vs1.TextMatrix(counter, RCBCol) == "F")
					{
						MessageBox.Show("You may not enter an RCBF of F for a Ledger account.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vs1.Select(counter, RCBCol);
						return;
					}
				}
			}
			if (!blnData)
			{
				MessageBox.Show("You must make at least one entry before you may save this", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cboJournal.SelectedIndex == 0)
			{
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				blnJournalLocked = true;
				Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					TempJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					TempJournal = 1;
				}
				answer = MessageBox.Show("This Entry will be saved in Journal " + FCConvert.ToString(TempJournal) + ".  Is this OK?", "Save New Journal", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Cancel)
				{
					modBudgetaryAccounting.UnlockJournal();
					blnJournalLocked = false;
					if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					return;
				}
				else if (answer == DialogResult.No)
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please select the jounal you wish to save this entry in";
					lblJournalSave.Visible = true;
					cboSaveJournal.Visible = true;
					//FC:FINAL:DDU:#2873 - show textbox when selecting item in combobox
                    cboSaveJournal.SelectedIndex = 0;
                    lblJournalDescription.Visible = false;
					txtJournalDescription.Visible = false;
                    cboSaveJournal_SelectedIndexChanged(cboSaveJournal, System.EventArgs.Empty);
                    if (Master.IsntAnything())
					{
						// do nothing
					}
					else
					{
						Master.Reset();
					}
					cboSaveJournal.Focus();
					btnFileSave.Enabled = false;
					btnFileSaveType.Enabled = false;
					//Application.DoEvents();
					btnProcessDelete.Enabled = false;
					btnFileLoadType.Enabled = false;
					return;
				}
				else
				{
					fraJournalSave.Visible = true;
					lblSaveInstructions.Text = "Please type in a description for the journal you are going to save and click the OK button";
					lblJournalSave.Visible = false;
					cboSaveJournal.Visible = false;
					lblJournalDescription.Visible = true;
					txtJournalDescription.Visible = true;
					txtJournalDescription.Focus();
					btnFileSave.Enabled = false;
					btnFileSaveType.Enabled = false;
					//Application.DoEvents();
					btnProcessDelete.Enabled = false;
					btnFileLoadType.Enabled = false;
					return;
				}
			}
			else
			{
				SaveJournal();
			}
		}

		private void LoadDefaultCombo()
		{
			clsDRWrapper rsDefaultInfo = new clsDRWrapper();
			rsDefaultInfo.OpenRecordset("SELECT * FROM GJDefaultMaster");
			if (rsDefaultInfo.EndOfFile() != true && rsDefaultInfo.BeginningOfFile() != true)
			{
				do
				{
					cboDefaultType.AddItem(rsDefaultInfo.Get_Fields_String("Description"));
					rsDefaultInfo.MoveNext();
				}
				while (rsDefaultInfo.EndOfFile() != true);
			}
			else
			{
				btnFileLoadType.Enabled = false;
			}
		}
	}
}
