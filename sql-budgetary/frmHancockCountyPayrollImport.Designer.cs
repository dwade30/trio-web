﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmHancockCountyPayrollImport.
	/// </summary>
	partial class frmHancockCountyPayrollImport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAllOrSelected;
		public fecherFoundation.FCFrame fraBranch;
		public FCGrid vsBranches;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHancockCountyPayrollImport));
			this.cmbAllOrSelected = new fecherFoundation.FCComboBox();
			this.fraBranch = new fecherFoundation.FCFrame();
			this.vsBranches = new fecherFoundation.FCGrid();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.cmdFileUnselectAll = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBranch)).BeginInit();
			this.fraBranch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBranches)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileUnselectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 478);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbAllOrSelected);
			this.ClientArea.Controls.Add(this.fraBranch);
			this.ClientArea.Size = new System.Drawing.Size(1078, 606);
			this.ClientArea.Controls.SetChildIndex(this.fraBranch, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbAllOrSelected, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileUnselectAll);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileUnselectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.AutoSize = false;
			this.HeaderText.Size = new System.Drawing.Size(341, 28);
			this.HeaderText.Text = "Import Payroll Information";
			// 
			// cmbAllOrSelected
			// 
			this.cmbAllOrSelected.Items.AddRange(new object[] {
            "All",
            "Selected Branches"});
			this.cmbAllOrSelected.Location = new System.Drawing.Point(30, 30);
			this.cmbAllOrSelected.Name = "cmbAllOrSelected";
			this.cmbAllOrSelected.Size = new System.Drawing.Size(250, 40);
			this.cmbAllOrSelected.TabIndex = 0;
			this.cmbAllOrSelected.SelectedIndexChanged += new System.EventHandler(this.cmbAllOrSelected_SelectedIndexChanged);
			// 
			// fraBranch
			// 
			this.fraBranch.AppearanceKey = "groupBoxNoBorders";
			this.fraBranch.Controls.Add(this.vsBranches);
			this.fraBranch.Enabled = false;
			this.fraBranch.Location = new System.Drawing.Point(30, 100);
			this.fraBranch.Name = "fraBranch";
			this.fraBranch.Size = new System.Drawing.Size(876, 378);
			this.fraBranch.TabIndex = 2;
			// 
			// vsBranches
			// 
			this.vsBranches.ColumnHeadersVisible = false;
			this.vsBranches.ExtendLastCol = true;
			this.vsBranches.FixedCols = 0;
			this.vsBranches.FixedRows = 0;
			this.vsBranches.Location = new System.Drawing.Point(0, 20);
			this.vsBranches.Name = "vsBranches";
			this.vsBranches.RowHeadersVisible = false;
			this.vsBranches.Rows = 0;
			this.vsBranches.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
			this.vsBranches.Size = new System.Drawing.Size(876, 358);
			this.vsBranches.TabIndex = 3;
			this.vsBranches.Click += new System.EventHandler(this.vsBranches_Click);
			this.vsBranches.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsBranches_KeyPressEvent);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.Enabled = false;
			this.cmdFileSelectAll.Location = new System.Drawing.Point(946, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(71, 24);
			this.cmdFileSelectAll.TabIndex = 1;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// cmdFileUnselectAll
			// 
			this.cmdFileUnselectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileUnselectAll.Enabled = false;
			this.cmdFileUnselectAll.Location = new System.Drawing.Point(1021, 29);
			this.cmdFileUnselectAll.Name = "cmdFileUnselectAll";
			this.cmdFileUnselectAll.Size = new System.Drawing.Size(86, 24);
			this.cmdFileUnselectAll.TabIndex = 2;
			this.cmdFileUnselectAll.Text = "Unselect All";
			this.cmdFileUnselectAll.Click += new System.EventHandler(this.mnuFileUnselectAll_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(474, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(110, 48);
			this.cmdSave.TabIndex = 7;
			this.cmdSave.Text = "Process";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmHancockCountyPayrollImport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmHancockCountyPayrollImport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Import Payroll Information";
			this.Load += new System.EventHandler(this.frmHancockCountyPayrollImport_Load);
			this.Activated += new System.EventHandler(this.frmHancockCountyPayrollImport_Activated);
			this.Resize += new System.EventHandler(this.frmHancockCountyPayrollImport_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmHancockCountyPayrollImport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraBranch)).EndInit();
			this.fraBranch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsBranches)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileUnselectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdSave;
		public FCButton cmdFileSelectAll;
		public FCButton cmdFileUnselectAll;
	}
}
