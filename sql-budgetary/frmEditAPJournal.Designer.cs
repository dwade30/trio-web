﻿using Global;
using Wisej.Web;

namespace TWBD0000
{
    partial class frmEditAPJournal : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (theView != null)
                {
                    theView.BanksChanged -= theView_BanksChanged;
                    theView.JournalChanged -= theView_JournalChanged;
                }
                if (components != null)
                {
                    components.Dispose();
                }
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.cmbBanks = new fecherFoundation.FCComboBox();
			this.cmbPeriod = new fecherFoundation.FCComboBox();
			this.cmdOK = new fecherFoundation.FCButton();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.t2kPayableDate = new Global.T2KDateBox();
			this.lblBank = new fecherFoundation.FCLabel();
			this.lblPayableDate = new fecherFoundation.FCLabel();
			this.lblPeriod = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPayableDate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOK);
			this.BottomPanel.Location = new System.Drawing.Point(0, 315);
			this.BottomPanel.Size = new System.Drawing.Size(607, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblJournal);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Controls.Add(this.lblPeriod);
			this.ClientArea.Controls.Add(this.lblPayableDate);
			this.ClientArea.Controls.Add(this.lblBank);
			this.ClientArea.Controls.Add(this.t2kPayableDate);
			this.ClientArea.Controls.Add(this.cmbPeriod);
			this.ClientArea.Controls.Add(this.cmbBanks);
			this.ClientArea.Controls.Add(this.txtDescription);
			this.ClientArea.Size = new System.Drawing.Size(627, 427);
			this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbBanks, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbPeriod, 0);
			this.ClientArea.Controls.SetChildIndex(this.t2kPayableDate, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblBank, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPayableDate, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblPeriod, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblDescription, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblJournal, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(627, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(234, 30);
			this.HeaderText.Text = "New Invoice Journal";
			// 
			// cmbBanks
			// 
			this.cmbBanks.Location = new System.Drawing.Point(32, 275);
			this.cmbBanks.Name = "cmbBanks";
			this.cmbBanks.Size = new System.Drawing.Size(310, 40);
			this.cmbBanks.TabIndex = 4;
			// 
			// cmbPeriod
			// 
			this.cmbPeriod.Location = new System.Drawing.Point(32, 126);
			this.cmbPeriod.Name = "cmbPeriod";
			this.cmbPeriod.Size = new System.Drawing.Size(76, 40);
			this.cmbPeriod.TabIndex = 2;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "acceptButton";
			this.cmdOK.Location = new System.Drawing.Point(250, 30);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(92, 48);
			this.cmdOK.TabIndex = 9;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(32, 53);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(538, 40);
			this.txtDescription.TabIndex = 1;
			// 
			// t2kPayableDate
			// 
			this.t2kPayableDate.AutoSize = false;
			this.t2kPayableDate.Location = new System.Drawing.Point(32, 200);
			this.t2kPayableDate.Name = "t2kPayableDate";
			this.t2kPayableDate.Size = new System.Drawing.Size(144, 40);
			this.t2kPayableDate.TabIndex = 3;
			// 
			// lblBank
			// 
			this.lblBank.Location = new System.Drawing.Point(32, 255);
			this.lblBank.Name = "lblBank";
			this.lblBank.Size = new System.Drawing.Size(202, 19);
			this.lblBank.TabIndex = 7;
			this.lblBank.Text = "BANK";
			// 
			// lblPayableDate
			// 
			this.lblPayableDate.Location = new System.Drawing.Point(32, 180);
			this.lblPayableDate.Name = "lblPayableDate";
			this.lblPayableDate.Size = new System.Drawing.Size(202, 19);
			this.lblPayableDate.TabIndex = 4;
			this.lblPayableDate.Text = "PAYABLE DATE";
			// 
			// lblPeriod
			// 
			this.lblPeriod.Location = new System.Drawing.Point(32, 106);
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Size = new System.Drawing.Size(202, 19);
			this.lblPeriod.TabIndex = 10;
			this.lblPeriod.Text = "PERIOD";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(32, 33);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(202, 19);
			this.lblDescription.TabIndex = 1;
			this.lblDescription.Text = "DESCRIPTION";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(209, 8);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(202, 19);
			this.lblJournal.TabIndex = 12;
			// 
			// frmEditAPJournal
			// 
			this.ClientSize = new System.Drawing.Size(627, 487);
			this.Name = "frmEditAPJournal";
			this.Text = "Create New Invoice Journal";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kPayableDate)).EndInit();
			this.ResumeLayout(false);

        }



        #endregion

        private fecherFoundation.FCButton cmdOK;
        private fecherFoundation.FCComboBox cmbPeriod;
		private fecherFoundation.FCComboBox cmbBanks;
        private fecherFoundation.FCTextBox txtDescription;
        private T2KDateBox t2kPayableDate;
		private fecherFoundation.FCLabel lblBank;
        private fecherFoundation.FCLabel lblPayableDate;
        private fecherFoundation.FCLabel lblDescription;
        private fecherFoundation.FCLabel lblPeriod;
        private fecherFoundation.FCLabel lblJournal;
    }
}