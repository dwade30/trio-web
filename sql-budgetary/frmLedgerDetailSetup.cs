﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmLedgerDetailSetup.
	/// </summary>
	public partial class frmLedgerDetailSetup : BaseForm
	{
		public frmLedgerDetailSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:BBE - default selection - All
			this.cmbRange.Text = "All";
			this.cmbAllAccounts.SelectedIndex = 0;
			this.cmbPostedDate.Text = "Journal Number";
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLedgerDetailSetup InstancePtr
		{
			get
			{
				return (frmLedgerDetailSetup)Sys.GetInstance(typeof(frmLedgerDetailSetup));
			}
		}

		protected frmLedgerDetailSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         2/21/2002
		// This form will be used by people to select the search criteria
		// they wish to use to get information for the Expense Detail
		// Report
		// ********************************************************
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs_AutoInitialized;

		clsDRWrapper rs
		{
			get
			{
				if (rs_AutoInitialized == null)
				{
					rs_AutoInitialized = new clsDRWrapper();
				}
				return rs_AutoInitialized;
			}
			set
			{
				rs_AutoInitialized = value;
			}
		}

		int FirstMonth;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsLowGrid = new clsGridAccount();
		private clsGridAccount vsLowGrid_AutoInitialized;

		private clsGridAccount vsLowGrid
		{
			get
			{
				if (vsLowGrid_AutoInitialized == null)
				{
					vsLowGrid_AutoInitialized = new clsGridAccount();
				}
				return vsLowGrid_AutoInitialized;
			}
			set
			{
				vsLowGrid_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsGridAccount vsHighGrid = new clsGridAccount();
		private clsGridAccount vsHighGrid_AutoInitialized;

		private clsGridAccount vsHighGrid
		{
			get
			{
				if (vsHighGrid_AutoInitialized == null)
				{
					vsHighGrid_AutoInitialized = new clsGridAccount();
				}
				return vsHighGrid_AutoInitialized;
			}
			set
			{
				vsHighGrid_AutoInitialized = value;
			}
		}

		private void chkCheckAccountRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckAccountRange.CheckState == CheckState.Checked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					vsLowAccount.Visible = false;
					vsHighAccount.Visible = false;
					vsLowAccount.TextMatrix(0, 0, "");
					vsHighAccount.TextMatrix(0, 0, "");
					lblTo[1].Visible = false;
					lblTo[2].Visible = false;
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					cboSingleFund.Visible = false;
					cboSingleFund.SelectedIndex = -1;
				}
				else
				{
					cboBeginningFund.Visible = false;
					cboEndingFund.Visible = false;
					cboBeginningFund.SelectedIndex = -1;
					cboEndingFund.SelectedIndex = -1;
					lblTo[1].Visible = false;
					lblTo[2].Visible = false;
				}
			}
			else
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					vsLowAccount.Visible = true;
					vsHighAccount.Visible = true;
					lblTo[1].Visible = true;
					lblTo[2].Visible = false;
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					cboSingleFund.Visible = true;
				}
				else
				{
					cboBeginningFund.Visible = true;
					cboEndingFund.Visible = true;
					lblTo[1].Visible = false;
					lblTo[2].Visible = true;
				}
			}
		}

		private void chkCheckDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckDateRange.CheckState == CheckState.Checked)
			{
				if (cmbRange.Text == "Single Month")
				{
					cboSingleMonth.Visible = false;
					cboSingleMonth.SelectedIndex = -1;
				}
				else
				{
					cboBeginningMonth.Visible = false;
					cboEndingMonth.Visible = false;
					lblTo[0].Visible = false;
					cboBeginningMonth.SelectedIndex = -1;
					cboEndingMonth.SelectedIndex = -1;
				}
			}
			else
			{
				if (cmbRange.Text == "Single Month")
				{
					cboSingleMonth.Visible = true;
					cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
				else
				{
					cboBeginningMonth.Visible = true;
					cboEndingMonth.Visible = true;
					lblTo[0].Visible = true;
					cboBeginningMonth.SelectedIndex = FirstMonth - 1;
					cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
			}
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			if (!modBudgetaryMaster.Statics.blnLedgerDetailEdit)
			{
				frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
			}
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			clsDRWrapper rs2 = new clsDRWrapper();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a Description for this Selection Criteria before you may proceed.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (cmbRange.Text == "Range of Months" && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.Text == "Single Month" && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (chkCheckAccountRange.CheckState == CheckState.Unchecked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					if (Strings.InStr(1, vsLowAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsLowAccount.TextMatrix(0, 0) == "" || Strings.InStr(1, vsHighAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) != 0 || vsHighAccount.TextMatrix(0, 0) == "")
					{
						MessageBox.Show("You must specify the range of Accounts you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (fecherFoundation.Strings.CompareString(vsLowAccount.TextMatrix(0, 0), vsHighAccount.TextMatrix(0, 0), true) > 0)
					{
						MessageBox.Show("Your beginning account must be less than your ending account", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					if (cboSingleFund.SelectedIndex == -1 && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify which Department you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					if ((cboBeginningFund.SelectedIndex == -1 || cboEndingFund.SelectedIndex == -1) && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify the range of Departments you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningFund.SelectedIndex > cboEndingFund.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LD' AND Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A selection criteria already exists with this description.  Do you wish to overwrite this?", "Overwrite Criteria?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("Description", txtDescription.Text);
			if (chkMonthlySubtotals.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("MonthlySubtotals", true);
			}
			else
			{
				rs.Set_Fields("MonthlySubtotals", false);
			}
			if (chkShowZeroBalanceAccounts.CheckState == CheckState.Checked)
			{
				rs.Set_Fields("ShowZeroBalance", true);
			}
			else
			{
				rs.Set_Fields("ShowZeroBalance", false);
			}
			rs.Set_Fields("Type", "LD");
			if (cmbRange.Text == "Single Month")
			{
				rs.Set_Fields("SelectedMonths", "S");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboSingleMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			else if (cmbRange.Text == "Range of Months")
			{
				rs.Set_Fields("SelectedMonths", "R");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboBeginningMonth.SelectedIndex + 1);
					rs.Set_Fields("EndMonth", cboEndingMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			else
			{
				rs.Set_Fields("SelectedMonths", "A");
				rs.Set_Fields("CheckMonthRange", false);
			}
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				rs.Set_Fields("SelectedAccounts", "A");
				rs.Set_Fields("CheckAccountRange", false);
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				rs.Set_Fields("SelectedAccounts", "R");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegAccount", vsLowAccount.TextMatrix(0, 0));
					rs.Set_Fields("EndAccount", vsHighAccount.TextMatrix(0, 0));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				rs.Set_Fields("SelectedAccounts", "S");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else
			{
				rs.Set_Fields("SelectedAccounts", "D");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboBeginningFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					rs.Set_Fields("EndDeptExp", Strings.Left(cboEndingFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			if (cmbPostedDate.Text == "Journal Number")
			{
				rs.Set_Fields("SortOrder", "J");
			}
			else
			{
				rs.Set_Fields("SortOrder", "P");
			}
			rs.Update();
			if (!modBudgetaryMaster.Statics.blnLedgerDetailEdit)
			{
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
				// center it
				frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'LD' AND Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
				frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
				frmLedgerDetailSelect.InstancePtr.FillCriteria(true, Strings.Trim(txtDescription.Text));
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				ResetForm();
				Close();
			}
			else
			{
				ResetForm();
				Close();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmLedgerDetailSetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmLedgerDetailSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VSLOWACCOUNT")
			{
				modNewAccountBox.CheckFormKeyDown(vsLowAccount, vsLowAccount.Row, vsLowAccount.Col, KeyCode, Shift, vsLowAccount.EditSelStart, vsLowAccount.EditText, vsLowAccount.EditSelLength);
			}
			else if (Strings.UCase(this.ActiveControl.GetName()) == "VSHIGHACCOUNT")
			{
				if (KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(vsHighAccount, vsHighAccount.Row, vsHighAccount.Col, KeyCode, Shift, vsHighAccount.EditSelStart, vsHighAccount.EditText, vsHighAccount.EditSelLength);
				}
			}
		}

		private void frmLedgerDetailSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLedgerDetailSetup.FillStyle	= 0;
			//frmLedgerDetailSetup.ScaleWidth	= 9045;
			//frmLedgerDetailSetup.ScaleHeight	= 6930;
			//frmLedgerDetailSetup.LinkTopic	= "Form2";
			//frmLedgerDetailSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			vsLowGrid.GRID7Light = vsLowAccount;
			vsHighGrid.GRID7Light = vsHighAccount;
			vsLowGrid.DefaultAccountType = "G";
			vsLowGrid.AccountCol = -1;
			vsLowGrid.Validation = false;
			vsHighGrid.DefaultAccountType = "G";
			vsHighGrid.AccountCol = -1;
			vsHighGrid.Validation = false;
			vsLowGrid.OnlyAllowDefaultType = true;
			vsHighGrid.OnlyAllowDefaultType = true;
			//FC:FINAL:BBE - Redesign
			//cboBeginningFund.Left = FCConvert.ToInt32(cboBeginningFund.Left + cboBeginningFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350));
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350)) - (0.5 * (cboSingleFund.Width - (Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350))));
			//cboBeginningFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboEndingFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			//cboSingleFund.Width = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) * 115 + 350);
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboBeginningFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboEndingFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			vsLowAccount.TextMatrix(0, 0, "");
			vsHighAccount.TextMatrix(0, 0, "");
			FirstMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			cboBeginningMonth.SelectedIndex = FirstMonth - 1;
			cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			if (modBudgetaryMaster.Statics.blnLedgerDetailEdit || modBudgetaryMaster.Statics.blnLedgerDetailReportEdit)
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("MonthlySubtotals")))
					{
						chkMonthlySubtotals.CheckState = CheckState.Checked;
					}
					else
					{
						chkMonthlySubtotals.CheckState = CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("ShowZeroBalance")))
					{
						chkShowZeroBalanceAccounts.CheckState = CheckState.Checked;
					}
					else
					{
						chkShowZeroBalanceAccounts.CheckState = CheckState.Unchecked;
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SortOrder")) == "P")
					{
						cmbPostedDate.Text = "Posted Date";
					}
					else
					{
						cmbPostedDate.Text = "Journal Number";
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						cmbRange.Text = "Single Month";
						cboSingleMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
					{
						cmbRange.Text = "Range of Months";
						cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						cboEndingMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbRange.Text = "All";
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
					{
						cmbAllAccounts.SelectedIndex = 0;
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "R")
					{
						cmbAllAccounts.SelectedIndex = 1;
						vsLowAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegAccount")));
						vsHighAccount.TextMatrix(0, 0, FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndAccount")));
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
					{
						cmbAllAccounts.SelectedIndex = 2;
						for (counter = 0; counter <= cboSingleFund.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboSingleFund.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboSingleFund.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbAllAccounts.SelectedIndex = 3;
						for (counter = 0; counter <= cboBeginningFund.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboBeginningFund.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboBeginningFund.SelectedIndex = counter;
								break;
							}
						}
						for (counter = 0; counter <= cboEndingFund.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboEndingFund.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp")))
							{
								cboEndingFund.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
				}
			}
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			modBudgetaryMaster.Statics.blnLedgerDetailReportEdit = false;
		}

		private void frmLedgerDetailSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (!modBudgetaryMaster.Statics.blnLedgerDetailEdit)
				{
					frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
				}
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			if (!modBudgetaryMaster.Statics.blnLedgerDetailEdit)
			{
				frmLedgerDetailSelect.InstancePtr.Show(App.MainForm);
			}
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void cmbAllAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				lblTo[2].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = false;
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				cboSingleFund.Visible = false;
				cboSingleFund.SelectedIndex = -1;
				vsLowAccount.Visible = true;
				vsHighAccount.Visible = true;
				lblTo[1].Visible = true;
				lblTo[2].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				cboBeginningFund.Visible = false;
				cboEndingFund.Visible = false;
				cboBeginningFund.SelectedIndex = -1;
				cboEndingFund.SelectedIndex = -1;
				cboSingleFund.Visible = true;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				lblTo[2].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				cboBeginningFund.Visible = true;
				cboEndingFund.Visible = true;
				lblTo[2].Visible = true;
				cboSingleFund.Visible = false;
				vsLowAccount.TextMatrix(0, 0, "");
				vsHighAccount.TextMatrix(0, 0, "");
				vsLowAccount.Visible = false;
				vsHighAccount.Visible = false;
				lblTo[1].Visible = false;
				chkCheckAccountRange.CheckState = CheckState.Unchecked;
				chkCheckAccountRange.Visible = true;
			}
		}

		private void cmbRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.Text == "Range of Months")
			{
				cboBeginningMonth.Visible = true;
				cboEndingMonth.Visible = true;
				lblTo[0].Visible = true;
				cboSingleMonth.Visible = false;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboBeginningMonth.SelectedIndex = FirstMonth - 1;
				cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			else if (cmbRange.Text == "Single Month")
			{
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.Visible = true;
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = true;
				cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			}
			else if (cmbRange.Text == "All")
			{
				chkCheckDateRange.CheckState = CheckState.Unchecked;
				chkCheckDateRange.Visible = false;
				cboBeginningMonth.Visible = false;
				cboBeginningMonth.SelectedIndex = -1;
				cboEndingMonth.Visible = false;
				cboEndingMonth.SelectedIndex = -1;
				lblTo[0].Visible = false;
				cboSingleMonth.SelectedIndex = -1;
				cboSingleMonth.Visible = false;
			}
		}

		private void ResetForm()
		{
			txtDescription.Text = "";
			vsHighAccount.TextMatrix(0, 0, "");
			vsLowAccount.TextMatrix(0, 0, "");
			cboSingleFund.SelectedIndex = -1;
			cboSingleFund.Visible = false;
			cboBeginningFund.SelectedIndex = -1;
			cboBeginningFund.Visible = false;
			cboEndingFund.SelectedIndex = -1;
			cboEndingFund.Visible = false;
			cmbRange.Text = "All";
			cboBeginningMonth.Visible = false;
			cboEndingMonth.Visible = false;
			lblTo[1].Visible = false;
			lblTo[2].Visible = false;
			cmbAllAccounts.SelectedIndex = 0;
			fraMonths.Visible = true;
			cmdSave.Enabled = true;
			cmdCancelPrint.Enabled = true;
			fraFundRange.Visible = true;
		}

		private void cboBeginningFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboEndingFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}
	}
}
