﻿//Fecher vbPorter - Version 1.0.0.27
using Global;
using System;
using fecherFoundation;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCurrentAccountStatusHolder.
	/// </summary>
	public partial class rptCurrentAccountStatusHolder : BaseSectionReport
	{
		public static rptCurrentAccountStatusHolder InstancePtr
		{
			get
			{
				return (rptCurrentAccountStatusHolder)Sys.GetInstance(typeof(rptCurrentAccountStatusHolder));
			}
		}

		protected rptCurrentAccountStatusHolder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCurrentAccountStatusHolder	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;

		public rptCurrentAccountStatusHolder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Current Account Status";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord == true)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				frmCurrentAccountStatus.InstancePtr.rsAccountInfo.MoveNext();
				eArgs.EOF = frmCurrentAccountStatus.InstancePtr.rsAccountInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: subAccountInfo As Variant --> As rptCurrentAccountStatus3
			//rptCurrentAccountStatus3 subAccountInfo = null; // - "AutoDim"
			//rptCurrentAccountStatus3.InstancePtr.Hide();
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			rptCurrentAccountStatus3.InstancePtr.Init(frmCurrentAccountStatus.InstancePtr.rsAccountInfo.Get_Fields("Account"));
			subAccountInfo.Report = rptCurrentAccountStatus3.InstancePtr;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		
	}
}
