﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintPurchaseOrder.
	/// </summary>
	partial class frmPrintPurchaseOrder : BaseForm
	{
		public System.Collections.Generic.List<Global.T2KOverTypeBox> txtTownAddress;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCCheckBox chkAdjustments;
		public fecherFoundation.FCFrame fraTownInfo;
		public Global.T2KOverTypeBox txtTownName;
		public Global.T2KOverTypeBox txtTownAddress_0;
		public Global.T2KOverTypeBox txtTownAddress_1;
		public Global.T2KOverTypeBox txtTownAddress_2;
		public Global.T2KOverTypeBox txtTownAddress_3;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label2;
		public FCGrid vsEncumbrances;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintPurchaseOrder));
			this.chkAdjustments = new fecherFoundation.FCCheckBox();
			this.fraTownInfo = new fecherFoundation.FCFrame();
			this.txtTownName = new Global.T2KOverTypeBox();
			this.txtTownAddress_0 = new Global.T2KOverTypeBox();
			this.txtTownAddress_1 = new Global.T2KOverTypeBox();
			this.txtTownAddress_2 = new Global.T2KOverTypeBox();
			this.txtTownAddress_3 = new Global.T2KOverTypeBox();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.vsEncumbrances = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).BeginInit();
			this.fraTownInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEncumbrances)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 470);
			this.BottomPanel.Size = new System.Drawing.Size(757, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkAdjustments);
			this.ClientArea.Controls.Add(this.fraTownInfo);
			this.ClientArea.Controls.Add(this.vsEncumbrances);
			this.ClientArea.Size = new System.Drawing.Size(757, 410);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(757, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(243, 30);
			this.HeaderText.Text = "Print Purchase Order";
			// 
			// chkAdjustments
			// 
			this.chkAdjustments.Location = new System.Drawing.Point(30, 30);
			this.chkAdjustments.Name = "chkAdjustments";
			this.chkAdjustments.Size = new System.Drawing.Size(175, 26);
			this.chkAdjustments.TabIndex = 9;
			this.chkAdjustments.Text = "Include Adjustments";
			// 
			// fraTownInfo
			// 
			this.fraTownInfo.Controls.Add(this.txtTownName);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_0);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_1);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_2);
			this.fraTownInfo.Controls.Add(this.txtTownAddress_3);
			this.fraTownInfo.Controls.Add(this.Label1_0);
			this.fraTownInfo.Controls.Add(this.Label2);
			this.fraTownInfo.Location = new System.Drawing.Point(30, 385);
			this.fraTownInfo.Name = "fraTownInfo";
			this.fraTownInfo.Size = new System.Drawing.Size(345, 290);
			this.fraTownInfo.TabIndex = 1;
			this.fraTownInfo.Text = "Enter Town Information";
			// 
			// txtTownName
			// 
			this.txtTownName.MaxLength = 35;
			this.txtTownName.Location = new System.Drawing.Point(115, 30);
			this.txtTownName.Name = "txtTownName";
			this.txtTownName.Size = new System.Drawing.Size(210, 40);
			this.txtTownName.TabIndex = 2;
			// 
			// txtTownAddress_0
			// 
			this.txtTownAddress_0.MaxLength = 35;
			this.txtTownAddress_0.Location = new System.Drawing.Point(115, 80);
			this.txtTownAddress_0.Name = "txtTownAddress_0";
			this.txtTownAddress_0.Size = new System.Drawing.Size(210, 40);
			this.txtTownAddress_0.TabIndex = 3;
			// 
			// txtTownAddress_1
			// 
			this.txtTownAddress_1.MaxLength = 35;
			this.txtTownAddress_1.Location = new System.Drawing.Point(115, 130);
			this.txtTownAddress_1.Name = "txtTownAddress_1";
			this.txtTownAddress_1.Size = new System.Drawing.Size(210, 40);
			this.txtTownAddress_1.TabIndex = 4;
			// 
			// txtTownAddress_2
			// 
			this.txtTownAddress_2.MaxLength = 35;
			this.txtTownAddress_2.Location = new System.Drawing.Point(115, 180);
			this.txtTownAddress_2.Name = "txtTownAddress_2";
			this.txtTownAddress_2.Size = new System.Drawing.Size(210, 40);
			this.txtTownAddress_2.TabIndex = 5;
			// 
			// txtTownAddress_3
			// 
			this.txtTownAddress_3.MaxLength = 35;
			this.txtTownAddress_3.Location = new System.Drawing.Point(115, 230);
			this.txtTownAddress_3.Name = "txtTownAddress_3";
			this.txtTownAddress_3.Size = new System.Drawing.Size(209, 40);
			this.txtTownAddress_3.TabIndex = 6;
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(20, 44);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(40, 16);
			this.Label1_0.TabIndex = 8;
			this.Label1_0.Text = "NAME";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(60, 16);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "ADDRESS";
			// 
			// vsEncumbrances
			// 
			this.vsEncumbrances.AllowSelection = false;
			this.vsEncumbrances.AllowUserToResizeColumns = false;
			this.vsEncumbrances.AllowUserToResizeRows = false;
			this.vsEncumbrances.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEncumbrances.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEncumbrances.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEncumbrances.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEncumbrances.BackColorSel = System.Drawing.Color.Empty;
			this.vsEncumbrances.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsEncumbrances.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsEncumbrances.ColumnHeadersHeight = 30;
			this.vsEncumbrances.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEncumbrances.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsEncumbrances.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsEncumbrances.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsEncumbrances.ExtendLastCol = true;
			this.vsEncumbrances.FixedCols = 0;
			this.vsEncumbrances.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsEncumbrances.FrozenCols = 0;
			this.vsEncumbrances.GridColor = System.Drawing.Color.Empty;
			this.vsEncumbrances.Location = new System.Drawing.Point(30, 66);
			this.vsEncumbrances.Name = "vsEncumbrances";
			this.vsEncumbrances.ReadOnly = true;
			this.vsEncumbrances.RowHeadersVisible = false;
			this.vsEncumbrances.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEncumbrances.RowHeightMin = 0;
			this.vsEncumbrances.Rows = 1;
			this.vsEncumbrances.ShowColumnVisibilityMenu = false;
			this.vsEncumbrances.Size = new System.Drawing.Size(596, 299);
			this.vsEncumbrances.StandardTab = true;
			this.vsEncumbrances.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEncumbrances.TabIndex = 0;
			this.vsEncumbrances.CellClick += new DataGridViewCellEventHandler(this.vsEncumbrances_ClickEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.Location = new System.Drawing.Point(327, 30);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(100, 48);
			this.cmdPreview.TabIndex = 0;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmPrintPurchaseOrder
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(757, 578);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPrintPurchaseOrder";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Purchase Order";
			this.Load += new System.EventHandler(this.frmPrintPurchaseOrder_Load);
			this.Activated += new System.EventHandler(this.frmPrintPurchaseOrder_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPrintPurchaseOrder_KeyPress);
			this.Resize += new System.EventHandler(this.frmPrintPurchaseOrder_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraTownInfo)).EndInit();
			this.fraTownInfo.ResumeLayout(false);
			this.fraTownInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTownName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTownAddress_3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEncumbrances)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdPreview;
	}
}
