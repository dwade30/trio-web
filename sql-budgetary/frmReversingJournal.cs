﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.Generic;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmReversingJournal.
	/// </summary>
	public partial class frmReversingJournal : BaseForm
	{
		public frmReversingJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReversingJournal InstancePtr
		{
			get
			{
				return (frmReversingJournal)Sys.GetInstance(typeof(frmReversingJournal));
			}
		}

		protected frmReversingJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
		clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
		bool blnPostJournal;

		private void frmReversingJournal_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmReversingJournal_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReversingJournal.FillStyle	= 0;
			//frmReversingJournal.ScaleWidth	= 5880;
			//frmReversingJournal.ScaleHeight	= 4305;
			//frmReversingJournal.LinkTopic	= "Form2";
			//frmReversingJournal.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			FillCombo();
			if (cboJournals.Items.Count > 0)
			{
				cboJournals.SelectedIndex = 0;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmReversingJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
			if (blnPostJournal)
			{
				//Application.DoEvents();
				clsPostInfo.AllowPreview = true;
				clsPostInfo.PostJournals();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FillCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM JournalMaster WHERE (Type <> 'AP' and Type <> 'AC' and Type <> 'EN') AND left(Description, 14) <> 'Enc Adjust for' AND Status = 'P' ORDER BY JournalNumber");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					cboJournals.AddItem(Strings.Format(rs.Get_Fields("JournalNumber"), "0000") + "   " + rs.Get_Fields("Type") + "   " + rs.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboJournals.ItemData(cboJournals.NewIndex, FCConvert.ToInt32(rs.Get_Fields("JournalNumber")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int lngJournal = 0;
			clsDRWrapper Master = new clsDRWrapper();
			clsDRWrapper rsOldMaster = new clsDRWrapper();
			bool blnCreate = false;
            var blnCorrect = chkCorrection.CheckState == CheckState.Checked;
            bool isAnEOYAPJournal = false;
            var journalsDictionary = new Dictionary<int, int>();
            int intOldJournal = 0;

			if (cboJournals.SelectedIndex < 0)
			{
				MessageBox.Show("You must select a journal before you may create a reversing entry for it.", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			rsOldMaster.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(cboJournals.ItemData(cboJournals.SelectedIndex)));
			if (modBudgetaryAccounting.LockJournal() == false)
			{
				// add entries to incomplete journals table
			}
			else
			{
				Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
				if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
				{
					Master.MoveLast();
					Master.MoveFirst();
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
				}
				else
				{
					lngJournal = 1;
				}
				Master.AddNew();
				Master.Set_Fields("JournalNumber", lngJournal);
				Master.Set_Fields("Status", "E");
				Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
				Master.Set_Fields("StatusChangeDate", DateTime.Today);
				Master.Set_Fields("Description", "Reversing Entry for Journal " + Strings.Format(rsOldMaster.Get_Fields_Int32("JournalNumber").ToString(), "0000"));
				Master.Set_Fields("Type", "GJ");
				Master.Set_Fields("Period", rsOldMaster.Get_Fields_Int32("Period"));
                if (rsOldMaster.Get_Fields_Boolean("AutomaticJournal"))
                {
                    Master.Set_Fields("AutomaticJournal",true);
                    if (rsOldMaster.Get_Fields_String("Description") == "EOY AP Cash Adjustments")
                    {
                        isAnEOYAPJournal = true;
                    }
                }
				clsPostInfo.ClearJournals();
				clsJournalInfo = new clsPostingJournalInfo();
				clsJournalInfo.JournalNumber = lngJournal;
				clsJournalInfo.JournalType = "GJ";
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				clsJournalInfo.Period = FCConvert.ToString(rsOldMaster.Get_Fields("Period"));
				clsJournalInfo.CheckDate = "";
				clsPostInfo.AddJournal(clsJournalInfo);
				Master.Update();
				Master.Reset();
				modBudgetaryAccounting.UnlockJournal();
			}
			if (lngJournal != 0)
			{
				rsOldMaster.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(cboJournals.ItemData(cboJournals.SelectedIndex)));
				if (rsOldMaster.EndOfFile() != true && rsOldMaster.BeginningOfFile() != true)
				{
					Master.OpenRecordset("SELECT * FROM JournalEntries");
					do
					{
						blnCreate = false;
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (Strings.Left(FCConvert.ToString(rsOldMaster.Get_Fields("Account")), 1) != "G")
						{
							blnCreate = true;
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if ((modBudgetaryMaster.GetAccountNumber_2(rsOldMaster.Get_Fields("Account")) != modBudgetaryAccounting.Statics.strExpCtrAccount && modBudgetaryMaster.GetAccountNumber_2(rsOldMaster.Get_Fields("Account")) != modBudgetaryAccounting.Statics.strRevCtrAccount) || FCConvert.ToString(rsOldMaster.Get_Fields_String("RCB")) != "L")
							{
								blnCreate = true;
							}
							else
							{
								blnCreate = false;
							}
						}
						if (blnCreate)
						{
                            if(isAnEOYAPJournal)
                            {
                                if (rsOldMaster.Get_Fields_String("Description").PadRight(15).Left(15) ==
                                    "Prior Year Jrnl")
                                {

                                }
                            }
							Master.AddNew();
							Master.Set_Fields("Type", "G");
							Master.Set_Fields("JournalEntriesDate", rsOldMaster.Get_Fields_DateTime("JournalEntriesDate"));
							Master.Set_Fields("Description", rsOldMaster.Get_Fields_String("Description"));
							Master.Set_Fields("JournalNumber", lngJournal);
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							Master.Set_Fields("Account", rsOldMaster.Get_Fields("Account"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Master.Set_Fields("Amount", rsOldMaster.Get_Fields("Amount") * -1);
							Master.Set_Fields("WarrantNumber", 0);
							// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
							Master.Set_Fields("Period", rsOldMaster.Get_Fields("Period"));
							if (blnCorrect)
							{
								Master.Set_Fields("RCB", "C");
							}
							else
							{
								if (FCConvert.ToString(rsOldMaster.Get_Fields_String("RCB")) == "B")
								{
									Master.Set_Fields("RCB", "B");
								}
								else
								{
									Master.Set_Fields("RCB", "R");
								}
							}
							Master.Set_Fields("Status", "E");
							Master.Update();
						}
						rsOldMaster.MoveNext();
					}
					while (rsOldMaster.EndOfFile() != true);
				}

                if (journalsDictionary.Count > 0)
                {
                    int intOldYear = 0;
                    if (!String.IsNullOrWhiteSpace(modGlobalConstants.Statics.gstrArchiveYear))
                    {
                        intOldYear = FCConvert.ToInt32(modGlobalConstants.Statics.gstrArchiveYear) - 1;
                    }
                    else
                    {
                        var rsArchiveYear = new clsDRWrapper();
                        rsArchiveYear.OpenRecordset("Select * from Archives where archivetype = 'Archive' order by ArchiveID desc", "SystemSettings");
                        if (!rsArchiveYear.EndOfFile())
                        {
                            intOldYear = rsArchiveYear.Get_Fields_Int32("ArchiveID");
                        }
                        else
                        {
                            intOldYear = 0;
                        }

                        if (intOldYear > 0)
                        {
                            var autPastYear = new cArchiveUtility();
                            string strArchivePath = autPastYear.DeriveGroupName("Archive", intOldYear);
                            var rsPreviousYear = new clsDRWrapper();
                            rsPreviousYear.GroupName = strArchivePath;
                            var intJourn = 0;
                            var listJourns = journalsDictionary.ToList();
                            foreach (var journNum in listJourns)
                            {
                                intOldJournal = journNum.Value;
                                rsPreviousYear.Execute("update archiveapamounts set transferred = 0 where journalnumber = " + intOldJournal.ToString(), "Budgetary");
                            }
                            {
                                
                            }
                        }
                    }
                }
				if (rsOldMaster.RecordCount() > 0)
				{
					if (modBudgetaryMaster.AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptReversingJournal))
					{
						if (MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(lngJournal) + ".  Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							blnPostJournal = true;
						}
						else
						{
							blnPostJournal = false;
						}
					}
					else
					{
						blnPostJournal = false;
						MessageBox.Show("Process Complete!  " + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(lngJournal), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					blnPostJournal = false;
					MessageBox.Show("No entries were found to import.", "No Entries", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				Close();
			}
			else
			{
				MessageBox.Show("Unable to get a new journal number.  Please try again.", "Unable to get Journal Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
