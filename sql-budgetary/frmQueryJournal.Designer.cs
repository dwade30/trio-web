﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmQueryJournal.
	/// </summary>
	partial class frmQueryJournal : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraCriteria;
		public fecherFoundation.FCTextBox txtWarrant;
		public fecherFoundation.FCComboBox cboWarrantCompare;
		public fecherFoundation.FCFrame fraStatus;
		public fecherFoundation.FCFrame fraSelectStatus;
		public fecherFoundation.FCCheckBox chkV;
		public fecherFoundation.FCCheckBox chkX;
		public fecherFoundation.FCCheckBox chkR;
		public fecherFoundation.FCCheckBox chkE;
		public fecherFoundation.FCCheckBox chkC;
		public fecherFoundation.FCCheckBox chkW;
		public fecherFoundation.FCCheckBox chkP;
		public fecherFoundation.FCComboBox cboCompare;
		public fecherFoundation.FCTextBox txtJournal;
		public fecherFoundation.FCTextBox txtVendor;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblReference;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblJournal;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		public fecherFoundation.FCLabel lblInstructions;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQueryJournal));
			this.cmbAll = new fecherFoundation.FCComboBox();
			this.lblAll = new fecherFoundation.FCLabel();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraCriteria = new fecherFoundation.FCFrame();
			this.txtWarrant = new fecherFoundation.FCTextBox();
			this.cboWarrantCompare = new fecherFoundation.FCComboBox();
			this.cboCompare = new fecherFoundation.FCComboBox();
			this.txtJournal = new fecherFoundation.FCTextBox();
			this.txtVendor = new fecherFoundation.FCTextBox();
			this.txtDescription = new fecherFoundation.FCTextBox();
			this.txtReference = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblReference = new fecherFoundation.FCLabel();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.lblVendor = new fecherFoundation.FCLabel();
			this.lblJournal = new fecherFoundation.FCLabel();
			this.fraStatus = new fecherFoundation.FCFrame();
			this.fraSelectStatus = new fecherFoundation.FCFrame();
			this.chkV = new fecherFoundation.FCCheckBox();
			this.chkX = new fecherFoundation.FCCheckBox();
			this.chkR = new fecherFoundation.FCCheckBox();
			this.chkE = new fecherFoundation.FCCheckBox();
			this.chkC = new fecherFoundation.FCCheckBox();
			this.chkW = new fecherFoundation.FCCheckBox();
			this.chkP = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.btnView = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).BeginInit();
			this.fraCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).BeginInit();
			this.fraStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectStatus)).BeginInit();
			this.fraSelectStatus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkV)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnView)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnView);
			this.BottomPanel.Location = new System.Drawing.Point(0, 382);
			this.BottomPanel.Size = new System.Drawing.Size(810, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraCriteria);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.fraStatus);
			this.ClientArea.Size = new System.Drawing.Size(810, 322);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(810, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(212, 30);
			this.HeaderText.Text = "View AP Journals";
			// 
			// cmbAll
			// 
			this.cmbAll.AutoSize = false;
			this.cmbAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAll.FormattingEnabled = true;
			this.cmbAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbAll.Location = new System.Drawing.Point(148, 30);
			this.cmbAll.Name = "cmbAll";
			this.cmbAll.Size = new System.Drawing.Size(130, 40);
			this.cmbAll.TabIndex = 18;
			this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
			// 
			// lblAll
			// 
			this.lblAll.Location = new System.Drawing.Point(20, 44);
			this.lblAll.Name = "lblAll";
			this.lblAll.Size = new System.Drawing.Size(93, 16);
			this.lblAll.TabIndex = 19;
			this.lblAll.Text = "SELECT STATUS";
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(713, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(47, 24);
			this.cmdClear.TabIndex = 9;
			this.cmdClear.Text = "Clear";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraCriteria
			// 
			this.fraCriteria.Controls.Add(this.txtWarrant);
			this.fraCriteria.Controls.Add(this.cboWarrantCompare);
			this.fraCriteria.Controls.Add(this.cboCompare);
			this.fraCriteria.Controls.Add(this.txtJournal);
			this.fraCriteria.Controls.Add(this.txtVendor);
			this.fraCriteria.Controls.Add(this.txtDescription);
			this.fraCriteria.Controls.Add(this.txtReference);
			this.fraCriteria.Controls.Add(this.Label1);
			this.fraCriteria.Controls.Add(this.lblReference);
			this.fraCriteria.Controls.Add(this.lblDescription);
			this.fraCriteria.Controls.Add(this.lblVendor);
			this.fraCriteria.Controls.Add(this.lblJournal);
			this.fraCriteria.Location = new System.Drawing.Point(30, 66);
			this.fraCriteria.Name = "fraCriteria";
			this.fraCriteria.Size = new System.Drawing.Size(426, 335);
			this.fraCriteria.TabIndex = 0;
			this.fraCriteria.Text = "View Criteria";
			// 
			// txtWarrant
			// 
			this.txtWarrant.MaxLength = 4;
			this.txtWarrant.AutoSize = false;
			this.txtWarrant.BackColor = System.Drawing.SystemColors.Window;
			this.txtWarrant.Location = new System.Drawing.Point(336, 30);
			this.txtWarrant.Name = "txtWarrant";
			this.txtWarrant.Size = new System.Drawing.Size(70, 40);
			this.txtWarrant.TabIndex = 2;
			this.txtWarrant.Validating += new System.ComponentModel.CancelEventHandler(this.txtWarrant_Validating);
			// 
			// cboWarrantCompare
			// 
			this.cboWarrantCompare.AutoSize = false;
			this.cboWarrantCompare.BackColor = System.Drawing.SystemColors.Window;
			this.cboWarrantCompare.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboWarrantCompare.FormattingEnabled = true;
			this.cboWarrantCompare.Items.AddRange(new object[] {
				"Greater Than",
				"Equal To",
				"Less Than"
			});
			this.cboWarrantCompare.Location = new System.Drawing.Point(168, 30);
			this.cboWarrantCompare.Name = "cboWarrantCompare";
			this.cboWarrantCompare.Size = new System.Drawing.Size(147, 40);
			this.cboWarrantCompare.TabIndex = 1;
			// 
			// cboCompare
			// 
			this.cboCompare.AutoSize = false;
			this.cboCompare.BackColor = System.Drawing.SystemColors.Window;
			this.cboCompare.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCompare.FormattingEnabled = true;
			this.cboCompare.Items.AddRange(new object[] {
				"Greater Than",
				"Equal To",
				"Less Than"
			});
			this.cboCompare.Location = new System.Drawing.Point(168, 90);
			this.cboCompare.Name = "cboCompare";
			this.cboCompare.Size = new System.Drawing.Size(147, 40);
			this.cboCompare.TabIndex = 3;
			// 
			// txtJournal
			// 
			this.txtJournal.MaxLength = 4;
			this.txtJournal.AutoSize = false;
			this.txtJournal.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournal.Location = new System.Drawing.Point(336, 90);
			this.txtJournal.Name = "txtJournal";
			this.txtJournal.Size = new System.Drawing.Size(70, 40);
			this.txtJournal.TabIndex = 4;
			this.txtJournal.Validating += new System.ComponentModel.CancelEventHandler(this.txtJournal_Validating);
			// 
			// txtVendor
			// 
			this.txtVendor.MaxLength = 5;
			this.txtVendor.AutoSize = false;
			this.txtVendor.BackColor = System.Drawing.SystemColors.Window;
			this.txtVendor.Location = new System.Drawing.Point(168, 150);
			this.txtVendor.Name = "txtVendor";
			this.txtVendor.Size = new System.Drawing.Size(147, 40);
			this.txtVendor.TabIndex = 5;
			this.txtVendor.Validating += new System.ComponentModel.CancelEventHandler(this.txtVendor_Validating);
			// 
			// txtDescription
			// 
			this.txtDescription.MaxLength = 25;
			this.txtDescription.AutoSize = false;
			this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
			this.txtDescription.Location = new System.Drawing.Point(168, 210);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(228, 40);
			this.txtDescription.TabIndex = 6;
			// 
			// txtReference
			// 
			this.txtReference.MaxLength = 15;
			this.txtReference.AutoSize = false;
			this.txtReference.BackColor = System.Drawing.SystemColors.Window;
			this.txtReference.Location = new System.Drawing.Point(168, 270);
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(147, 40);
			this.txtReference.TabIndex = 7;
			this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validating);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(72, 16);
			this.Label1.TabIndex = 26;
			this.Label1.Text = "WARRANT #";
			// 
			// lblReference
			// 
			this.lblReference.Location = new System.Drawing.Point(20, 284);
			this.lblReference.Name = "lblReference";
			this.lblReference.Size = new System.Drawing.Size(80, 16);
			this.lblReference.TabIndex = 14;
			this.lblReference.Text = "REFERENCE #";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(20, 224);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(80, 16);
			this.lblDescription.TabIndex = 13;
			this.lblDescription.Text = "DESCRIPTION";
			// 
			// lblVendor
			// 
			this.lblVendor.Location = new System.Drawing.Point(20, 164);
			this.lblVendor.Name = "lblVendor";
			this.lblVendor.Size = new System.Drawing.Size(65, 16);
			this.lblVendor.TabIndex = 12;
			this.lblVendor.Text = "VENDOR #";
			// 
			// lblJournal
			// 
			this.lblJournal.Location = new System.Drawing.Point(20, 104);
			this.lblJournal.Name = "lblJournal";
			this.lblJournal.Size = new System.Drawing.Size(70, 16);
			this.lblJournal.TabIndex = 11;
			this.lblJournal.Text = "JOURNAL #";
			// 
			// fraStatus
			// 
			this.fraStatus.Controls.Add(this.fraSelectStatus);
			this.fraStatus.Controls.Add(this.cmbAll);
			this.fraStatus.Controls.Add(this.lblAll);
			this.fraStatus.Location = new System.Drawing.Point(476, 66);
			this.fraStatus.Name = "fraStatus";
			this.fraStatus.Size = new System.Drawing.Size(296, 335);
			this.fraStatus.TabIndex = 16;
			this.fraStatus.Text = "Status";
			// 
			// fraSelectStatus
			// 
			this.fraSelectStatus.AppearanceKey = "groupBoxNoBorders";
			this.fraSelectStatus.Controls.Add(this.chkV);
			this.fraSelectStatus.Controls.Add(this.chkX);
			this.fraSelectStatus.Controls.Add(this.chkR);
			this.fraSelectStatus.Controls.Add(this.chkE);
			this.fraSelectStatus.Controls.Add(this.chkC);
			this.fraSelectStatus.Controls.Add(this.chkW);
			this.fraSelectStatus.Controls.Add(this.chkP);
			this.fraSelectStatus.Enabled = false;
			this.fraSelectStatus.Location = new System.Drawing.Point(0, 70);
			this.fraSelectStatus.Name = "fraSelectStatus";
			this.fraSelectStatus.Size = new System.Drawing.Size(249, 258);
			this.fraSelectStatus.TabIndex = 17;
			// 
			// chkV
			// 
			this.chkV.AutoSize = false;
			this.chkV.Location = new System.Drawing.Point(20, 54);
			this.chkV.Name = "chkV";
			this.chkV.Size = new System.Drawing.Size(198, 24);
			this.chkV.TabIndex = 27;
			this.chkV.Text = "Warrant Preview Printed";
			// 
			// chkX
			// 
			this.chkX.AutoSize = false;
			this.chkX.Location = new System.Drawing.Point(20, 190);
			this.chkX.Name = "chkX";
			this.chkX.Size = new System.Drawing.Size(190, 24);
			this.chkX.TabIndex = 25;
			this.chkX.Text = "Warrant Recap Printed";
			// 
			// chkR
			// 
			this.chkR.AutoSize = false;
			this.chkR.Location = new System.Drawing.Point(20, 122);
			this.chkR.Name = "chkR";
			this.chkR.Size = new System.Drawing.Size(190, 24);
			this.chkR.TabIndex = 24;
			this.chkR.Text = "Check Register Printed";
			// 
			// chkE
			// 
			this.chkE.AutoSize = false;
			this.chkE.Location = new System.Drawing.Point(20, 20);
			this.chkE.Name = "chkE";
			this.chkE.Size = new System.Drawing.Size(85, 24);
			this.chkE.TabIndex = 21;
			this.chkE.Text = "Entered";
			// 
			// chkC
			// 
			this.chkC.AutoSize = false;
			this.chkC.Location = new System.Drawing.Point(20, 88);
			this.chkC.Name = "chkC";
			this.chkC.Size = new System.Drawing.Size(135, 24);
			this.chkC.TabIndex = 20;
			this.chkC.Text = "Checks Printed";
			// 
			// chkW
			// 
			this.chkW.AutoSize = false;
			this.chkW.Location = new System.Drawing.Point(20, 156);
			this.chkW.Name = "chkW";
			this.chkW.Size = new System.Drawing.Size(140, 24);
			this.chkW.TabIndex = 19;
			this.chkW.Text = "Warrant Printed";
			// 
			// chkP
			// 
			this.chkP.AutoSize = false;
			this.chkP.Location = new System.Drawing.Point(20, 224);
			this.chkP.Name = "chkP";
			this.chkP.Size = new System.Drawing.Size(75, 24);
			this.chkP.TabIndex = 18;
			this.chkP.Text = "Posted";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 0;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.BackColor = System.Drawing.Color.Transparent;
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(485, 16);
			this.lblInstructions.TabIndex = 15;
			this.lblInstructions.Text = "SELECT THE AP JOURNAL INFORMATION YOU WANT, THEN CLICK THE VIEW BUTTON";
			// 
			// btnView
			// 
			this.btnView.AppearanceKey = "acceptButton";
			this.btnView.Location = new System.Drawing.Point(273, 30);
			this.btnView.Name = "btnView";
			this.btnView.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnView.Size = new System.Drawing.Size(64, 48);
			this.btnView.TabIndex = 1;
			this.btnView.Text = "View";
			this.btnView.Click += new System.EventHandler(this.cmdView_Click);
			// 
			// frmQueryJournal
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(810, 490);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmQueryJournal";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "View AP Journals";
			this.Load += new System.EventHandler(this.frmQueryJournal_Load);
			this.Activated += new System.EventHandler(this.frmQueryJournal_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmQueryJournal_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).EndInit();
			this.fraCriteria.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatus)).EndInit();
			this.fraStatus.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelectStatus)).EndInit();
			this.fraSelectStatus.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkV)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnView)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnView;
	}
}
