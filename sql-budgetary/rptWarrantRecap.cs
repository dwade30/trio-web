﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptWarrantRecap.
	/// </summary>
	public partial class rptWarrantRecap : BaseSectionReport
	{
		public static rptWarrantRecap InstancePtr
		{
			get
			{
				return (rptWarrantRecap)Sys.GetInstance(typeof(rptWarrantRecap));
			}
		}

		protected rptWarrantRecap _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsVendorInfo.Dispose();
				rsJournalInfo.Dispose();
				rsDetailInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptWarrantRecap	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		Decimal curWarrantTotal;
		Decimal curPrepaidTotal;
		Decimal curCurrentTotal;
		bool blnFirstRecord;
		bool blnTempVendors;
		//clsDRWrapper rsTempVendorInfo = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();
		clsDRWrapper rsJournalInfo = new clsDRWrapper();
		clsDRWrapper rsDetailInfo = new clsDRWrapper();
		int intTempVendorCounter;
		string strJournalSQL = "";
		string strSQL = "";

		public rptWarrantRecap()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Warrant Recap";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
				{
					intTempVendorCounter = 0;
					rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "'");
					rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
					if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
					{
						if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
						{
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("Status", "X");
							rsJournalInfo.Update(true);
						}
					}
				}
				else
				{
					rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber"));
					rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
					if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
					{
						if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
						{
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("Status", "X");
							rsJournalInfo.Update(true);
						}
					}
				}
				strJournalSQL = rsJournalInfo.Name();
			}
			else
			{
				rsDetailInfo.MoveNext();
				if (rsDetailInfo.EndOfFile() == true)
				{
					rsJournalInfo.MoveNext();
					bool executeCheckNextVendor = false;
					bool executeCheckNextJournal = false;
					bool executeGetNextDetail = false;
					if (rsJournalInfo.EndOfFile() == true)
					{
						rsVendorInfo.MoveNext();
						executeCheckNextVendor = true;
						goto CheckNextVendor;
					}
					else
					{
						executeGetNextDetail = true;
						goto GetNextDetail;
					}
					CheckNextVendor:
					;
					if (executeCheckNextVendor)
					{
						if (rsVendorInfo.EndOfFile() == true)
						{
							eArgs.EOF = true;
							return;
						}
						else
						{
							GetFirstVendor:
							;
							if (FCConvert.ToInt32(rsVendorInfo.Get_Fields_Int32("VendorNumber")) == 0)
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND TempVendorName = '" + modCustomReport.FixQuotes(rsVendorInfo.Get_Fields_String("VendorName")) + "'");
							}
							else
							{
								rsJournalInfo.OpenRecordset("SELECT * FROM APJournal WHERE " + strSQL + "AND VendorNumber = " + rsVendorInfo.Get_Fields_Int32("VendorNumber"));
							}
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
					CheckNextJournal:
					;
					if (executeCheckNextJournal)
					{
						if (rsJournalInfo.EndOfFile() != true && rsJournalInfo.BeginningOfFile() != true)
						{
							executeGetNextDetail = true;
							goto GetNextDetail;
						}
						else
						{
							rsVendorInfo.MoveNext();
							executeCheckNextVendor = true;
							goto CheckNextVendor;
						}
					}
					GetNextDetail:
					;
					if (executeGetNextDetail)
					{
						rsDetailInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rsJournalInfo.Get_Fields_Int32("ID"));
						if (rsDetailInfo.EndOfFile() != true && rsDetailInfo.BeginningOfFile() != true)
						{
							if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
							{
								if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
								{
									rsJournalInfo.Edit();
									rsJournalInfo.Set_Fields("Status", "X");
									rsJournalInfo.Update(true);
								}
							}
							eArgs.EOF = false;
							return;
						}
						else
						{
							rsJournalInfo.MoveNext();
							executeCheckNextJournal = true;
							goto CheckNextJournal;
						}
					}
				}
				else
				{
					if (FCConvert.ToString(rsJournalInfo.Get_Fields_String("Status")) != "P")
					{
						if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
						{
							rsJournalInfo.Edit();
							rsJournalInfo.Set_Fields("Status", "X");
							rsJournalInfo.Update(true);
						}
					}
					eArgs.EOF = false;
					return;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMasterJournal = new clsDRWrapper();
			if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				rsInfo.OpenRecordset("SELECT * FROM Reprint WHERE Type = 'R' ORDER BY ReportOrder");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ReportOrder")) < 9)
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("ReportOrder", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("ReportOrder")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						else
						{
							rsInfo.Delete();
							rsInfo.Update();
						}
					}
					while (rsInfo.EndOfFile() != true);
				}
				rsInfo.AddNew();
				rsInfo.Set_Fields("Type", "R");
				rsInfo.Set_Fields("ReportOrder", 1);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsInfo.Set_Fields("WarrantNumber", frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant"));
				rsInfo.Update(true);
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				rsMasterJournal.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'W' AND JournalNumber IN (SELECT DISTINCT JournalNumber FROM APJournal WHERE Warrant = '" + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant") + "')");
				if (rsMasterJournal.EndOfFile() != true && rsMasterJournal.BeginningOfFile() != true)
				{
					do
					{
						rsMasterJournal.Edit();
						rsMasterJournal.Set_Fields("Status", "X");
						rsMasterJournal.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						rsMasterJournal.Set_Fields("StatusChangeDate", DateTime.Today);
						rsMasterJournal.Update(true);
						rsMasterJournal.MoveNext();
					}
					while (rsMasterJournal.EndOfFile() != true);
				}
			}
			frmWarrantRecap.InstancePtr.Unload();
			rsInfo.Dispose();
			rsMasterJournal.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int counter;
			//clsDRWrapper rsWarrantInfo = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (frmWarrantRecap.InstancePtr.cmbInitial.SelectedIndex == 0)
			{
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				Label6.Text = "Warrant " + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant");
				// TODO Get_Fields: Check the table for the column [Warrant] and replace with corresponding Get_Field method
				strSQL = "APJournal.Warrant = '" + frmWarrantRecap.InstancePtr.rsInfo.Get_Fields("Warrant") + "' ";
				Label21.Visible = false;
			}
			else
			{
				Label6.Text = "Warrant " + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrantRecap.InstancePtr.cboReport.Text, 9, 4)));
				strSQL = "APJournal.Warrant = '" + FCConvert.ToString(Conversion.Val(Strings.Mid(frmWarrantRecap.InstancePtr.cboReport.Text, 9, 4))) + "' ";
				Label21.Visible = true;
			}
			blnFirstRecord = true;
			blnTempVendors = false;
			// Me.Document.Printer.PrinterName = frmWarrantRecap.strPrinterName
			FillTemp();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldVendor.Text = modValidateAccount.GetFormat_6(rsVendorInfo.Get_Fields_Int32("VendorNumber"), 5) + " " + rsVendorInfo.Get_Fields_String("VendorName");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			fldAmount.Text = Strings.Format((rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount")), "#,##0.00");
			ShowTitle();
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
			curWarrantTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			if (FCConvert.ToBoolean(rsJournalInfo.Get_Fields_Boolean("PrepaidCheck")))
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curPrepaidTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Discount] and replace with corresponding Get_Field method
				curCurrentTotal += rsDetailInfo.Get_Fields("Amount") - rsDetailInfo.Get_Fields("Discount");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldCurrentTotal.Text = Strings.Format(curCurrentTotal, "#,##0.00");
			fldPrepaidTotal.Text = Strings.Format(curPrepaidTotal, "#,##0.00");
			fldTotal.Text = Strings.Format(curWarrantTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void ShowTitle()
		{
			int intHolder = 0;
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "E" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "R" || Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
			{
				if (frmWarrantRecap.InstancePtr.intPreviewChoice == 1)
				{
					fldTitle.Text = "";
					return;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					modAccountTitle.DetermineAccountTitle(rsDetailInfo.Get_Fields("Account"), ref frmWarrantRecap.InstancePtr.lblTitle);
				}
				if (frmWarrantRecap.InstancePtr.intPreviewChoice == 2)
				{
					fldTitle.Text = frmWarrantRecap.InstancePtr.lblTitle.Text;
				}
				else if (frmWarrantRecap.InstancePtr.intPreviewChoice == 3)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Left(frmWarrantRecap.InstancePtr.lblTitle.Text, intHolder - 1);
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					if (Strings.Left(FCConvert.ToString(rsDetailInfo.Get_Fields("Account")), 1) == "G")
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "/", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder);
					}
					else
					{
						intHolder = Strings.InStr(1, frmWarrantRecap.InstancePtr.lblTitle.Text, "-", CompareConstants.vbBinaryCompare);
						fldTitle.Text = Strings.Right(frmWarrantRecap.InstancePtr.lblTitle.Text, frmWarrantRecap.InstancePtr.lblTitle.Text.Length - intHolder - 1);
					}
				}
				// 
			}
		}

		private void FillTemp()
		{
			//clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				strTemp = "SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0 ORDER BY VendorMaster.CheckName";
				// rsTemp.CreateStoredProcedure "VendorInfo", strTemp
				strTemp = "SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries' ORDER BY TempVendorName";
				// rsTemp.CreateStoredProcedure "TempVendorInfo", strTemp
				strTemp = "SELECT * FROM (SELECT DISTINCT APJournal.VendorNumber AS VendorNumber, VendorMaster.CheckName as VendorName FROM (APJournal INNER JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber) WHERE " + strSQL + "AND APJournal.VendorNumber > 0) as VendorInfo UNION ALL SELECT * FROM (SELECT DISTINCT VendorNumber, TempVendorName as VendorName FROM APJournal WHERE " + strSQL + "AND VendorNumber = 0 AND Description <> 'Control Entries') as TempVendorInfo";
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
				{
					if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) == 1)
					{
						strTemp += " ORDER BY VendorNumber";
					}
					else
					{
						strTemp += " ORDER BY VendorName";
					}
				}
				else
				{
					strTemp += " ORDER BY VendorNumber";
				}
				rsVendorInfo.OpenRecordset(strTemp);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In rptWarrant_FillTemp", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
