﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPurgeChart.
	/// </summary>
	partial class frmPurgeChart : BaseForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cboCharts = new fecherFoundation.FCComboBox();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.cboCharts);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(144, 30);
			this.HeaderText.Text = "Purge Chart";
			// 
			// cboCharts
			// 
			this.cboCharts.AutoSize = false;
			this.cboCharts.BackColor = System.Drawing.SystemColors.Window;
			this.cboCharts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCharts.FormattingEnabled = true;
			this.cboCharts.Location = new System.Drawing.Point(30, 30);
			this.cboCharts.Name = "cboCharts";
			this.cboCharts.Size = new System.Drawing.Size(300, 40);
			this.cboCharts.TabIndex = 0;
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 100);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(110, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Process";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmPurgeChart
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgeChart";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Chart";
			this.Load += new System.EventHandler(this.frmPurgeChart_Load);
			this.Activated += new System.EventHandler(this.frmPurgeChart_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgeChart_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public fecherFoundation.FCComboBox cboCharts;
		private FCButton cmdSave;
	}
}
