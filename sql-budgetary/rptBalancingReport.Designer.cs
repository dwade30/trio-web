﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBalancingReport.
	/// </summary>
	partial class rptBalancingReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBalancingReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatusDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPayee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldBegBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBegBalanceAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedDepositsCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedDeposits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedDepositsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnedChecksCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnedChecks = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnedChecksAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterestCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterestAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherCreditsCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherCreditsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedChecksCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedChecks = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashedChecksAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDebitsCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDebitsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldStatementTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatementTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingDepositsCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingDeposits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingDepositsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingChecksCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingChecks = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingChecksAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCheckBookStatement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheckBookStatementAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDepositsCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDeposits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOtherDepositsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedChecksCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedChecks = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedChecksAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCurrentCheckbook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentCheckbookAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingOtherCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOutstandingOtherAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedOtherCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedOther = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldIssuedOtherAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBank = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStatement = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatusDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBegBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBegBalanceAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDepositsCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDeposits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDepositsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecksCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecksAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCreditsCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCreditsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecksCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecksAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebitsCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebitsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDepositsCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDeposits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDepositsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecksCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecksAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckBookStatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckBookStatementAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDepositsCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDeposits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDepositsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecksCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecksAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCheckbook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCheckbookAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOtherCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOtherAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOtherCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOther)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOtherAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBank)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCheck,
				this.fldType,
				this.fldCheckDate,
				this.fldAmount,
				this.fldCode,
				this.fldStatusDate,
				this.fldPayee
			});
			this.Detail.Height = 0.25F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 0.09375F;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCheck.Text = "Field1";
			this.fldCheck.Top = 0.03125F;
			this.fldCheck.Width = 0.8125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 1F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "Field2";
			this.fldType.Top = 0.03125F;
			this.fldType.Width = 0.4375F;
			// 
			// fldCheckDate
			// 
			this.fldCheckDate.Height = 0.1875F;
			this.fldCheckDate.Left = 1.46875F;
			this.fldCheckDate.Name = "fldCheckDate";
			this.fldCheckDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldCheckDate.Text = "Field1";
			this.fldCheckDate.Top = 0.03125F;
			this.fldCheckDate.Width = 0.71875F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 2.28125F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldAmount.Text = "Field1";
			this.fldAmount.Top = 0.03125F;
			this.fldAmount.Width = 1.0625F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 3.625F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldCode.Text = "Field1";
			this.fldCode.Top = 0.03125F;
			this.fldCode.Width = 0.4375F;
			// 
			// fldStatusDate
			// 
			this.fldStatusDate.Height = 0.1875F;
			this.fldStatusDate.Left = 4.0625F;
			this.fldStatusDate.Name = "fldStatusDate";
			this.fldStatusDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldStatusDate.Text = "Field1";
			this.fldStatusDate.Top = 0.03125F;
			this.fldStatusDate.Width = 0.65625F;
			// 
			// fldPayee
			// 
			this.fldPayee.Height = 0.1875F;
			this.fldPayee.Left = 4.84375F;
			this.fldPayee.Name = "fldPayee";
			this.fldPayee.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldPayee.Text = "Field1";
			this.fldPayee.Top = 0.03125F;
			this.fldPayee.Width = 2.59375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanShrink = true;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBegBalance,
				this.fldBegBalanceAmount,
				this.fldCashedDepositsCount,
				this.fldCashedDeposits,
				this.fldCashedDepositsAmount,
				this.fldReturnedChecksCount,
				this.fldReturnedChecks,
				this.fldReturnedChecksAmount,
				this.fldInterestCount,
				this.fldInterest,
				this.fldInterestAmount,
				this.fldOtherCreditsCount,
				this.fldOtherCredits,
				this.fldOtherCreditsAmount,
				this.fldCashedChecksCount,
				this.fldCashedChecks,
				this.fldCashedChecksAmount,
				this.fldOtherDebitsCount,
				this.fldOtherDebits,
				this.fldOtherDebitsAmount,
				this.Line4,
				this.fldStatementTotal,
				this.fldStatementTotalAmount,
				this.fldOutstandingDepositsCount,
				this.fldOutstandingDeposits,
				this.fldOutstandingDepositsAmount,
				this.fldOutstandingChecksCount,
				this.fldOutstandingChecks,
				this.fldOutstandingChecksAmount,
				this.Line5,
				this.fldCheckBookStatement,
				this.fldCheckBookStatementAmount,
				this.fldOtherDepositsCount,
				this.fldOtherDeposits,
				this.fldOtherDepositsAmount,
				this.fldIssuedChecksCount,
				this.fldIssuedChecks,
				this.fldIssuedChecksAmount,
				this.Line6,
				this.fldCurrentCheckbook,
				this.fldCurrentCheckbookAmount,
				this.fldOutstandingOtherCount,
				this.fldOutstandingOther,
				this.fldOutstandingOtherAmount,
				this.fldIssuedOtherCount,
				this.fldIssuedOther,
				this.fldIssuedOtherAmount
			});
			this.ReportFooter.Height = 3.604167F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldBegBalance
			// 
			this.fldBegBalance.CanShrink = true;
			this.fldBegBalance.Height = 0.1875F;
			this.fldBegBalance.Left = 1.875F;
			this.fldBegBalance.Name = "fldBegBalance";
			this.fldBegBalance.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldBegBalance.Text = "Field2";
			this.fldBegBalance.Top = 0.09375F;
			this.fldBegBalance.Width = 2.125F;
			// 
			// fldBegBalanceAmount
			// 
			this.fldBegBalanceAmount.CanShrink = true;
			this.fldBegBalanceAmount.Height = 0.1875F;
			this.fldBegBalanceAmount.Left = 4.0625F;
			this.fldBegBalanceAmount.Name = "fldBegBalanceAmount";
			this.fldBegBalanceAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldBegBalanceAmount.Text = "Field3";
			this.fldBegBalanceAmount.Top = 0.09375F;
			this.fldBegBalanceAmount.Width = 1.0625F;
			// 
			// fldCashedDepositsCount
			// 
			this.fldCashedDepositsCount.CanShrink = true;
			this.fldCashedDepositsCount.Height = 0.1875F;
			this.fldCashedDepositsCount.Left = 5.15625F;
			this.fldCashedDepositsCount.Name = "fldCashedDepositsCount";
			this.fldCashedDepositsCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCashedDepositsCount.Text = "Field4";
			this.fldCashedDepositsCount.Top = 0.28125F;
			this.fldCashedDepositsCount.Width = 0.65625F;
			// 
			// fldCashedDeposits
			// 
			this.fldCashedDeposits.CanShrink = true;
			this.fldCashedDeposits.Height = 0.1875F;
			this.fldCashedDeposits.Left = 1.875F;
			this.fldCashedDeposits.Name = "fldCashedDeposits";
			this.fldCashedDeposits.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldCashedDeposits.Text = "Field2";
			this.fldCashedDeposits.Top = 0.28125F;
			this.fldCashedDeposits.Width = 2.125F;
			// 
			// fldCashedDepositsAmount
			// 
			this.fldCashedDepositsAmount.CanShrink = true;
			this.fldCashedDepositsAmount.Height = 0.1875F;
			this.fldCashedDepositsAmount.Left = 4.0625F;
			this.fldCashedDepositsAmount.Name = "fldCashedDepositsAmount";
			this.fldCashedDepositsAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCashedDepositsAmount.Text = "Field3";
			this.fldCashedDepositsAmount.Top = 0.28125F;
			this.fldCashedDepositsAmount.Width = 1.0625F;
			// 
			// fldReturnedChecksCount
			// 
			this.fldReturnedChecksCount.CanShrink = true;
			this.fldReturnedChecksCount.Height = 0.1875F;
			this.fldReturnedChecksCount.Left = 5.15625F;
			this.fldReturnedChecksCount.Name = "fldReturnedChecksCount";
			this.fldReturnedChecksCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldReturnedChecksCount.Text = "Field7";
			this.fldReturnedChecksCount.Top = 0.46875F;
			this.fldReturnedChecksCount.Width = 0.65625F;
			// 
			// fldReturnedChecks
			// 
			this.fldReturnedChecks.CanShrink = true;
			this.fldReturnedChecks.Height = 0.1875F;
			this.fldReturnedChecks.Left = 1.875F;
			this.fldReturnedChecks.Name = "fldReturnedChecks";
			this.fldReturnedChecks.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldReturnedChecks.Text = "Field2";
			this.fldReturnedChecks.Top = 0.46875F;
			this.fldReturnedChecks.Width = 2.125F;
			// 
			// fldReturnedChecksAmount
			// 
			this.fldReturnedChecksAmount.CanShrink = true;
			this.fldReturnedChecksAmount.Height = 0.1875F;
			this.fldReturnedChecksAmount.Left = 4.0625F;
			this.fldReturnedChecksAmount.Name = "fldReturnedChecksAmount";
			this.fldReturnedChecksAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldReturnedChecksAmount.Text = "Field3";
			this.fldReturnedChecksAmount.Top = 0.46875F;
			this.fldReturnedChecksAmount.Width = 1.0625F;
			// 
			// fldInterestCount
			// 
			this.fldInterestCount.CanShrink = true;
			this.fldInterestCount.Height = 0.1875F;
			this.fldInterestCount.Left = 5.15625F;
			this.fldInterestCount.Name = "fldInterestCount";
			this.fldInterestCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldInterestCount.Text = "Field10";
			this.fldInterestCount.Top = 0.65625F;
			this.fldInterestCount.Width = 0.65625F;
			// 
			// fldInterest
			// 
			this.fldInterest.CanShrink = true;
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 1.875F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldInterest.Text = "Field2";
			this.fldInterest.Top = 0.65625F;
			this.fldInterest.Width = 2.125F;
			// 
			// fldInterestAmount
			// 
			this.fldInterestAmount.CanShrink = true;
			this.fldInterestAmount.Height = 0.1875F;
			this.fldInterestAmount.Left = 4.0625F;
			this.fldInterestAmount.Name = "fldInterestAmount";
			this.fldInterestAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldInterestAmount.Text = "Field3";
			this.fldInterestAmount.Top = 0.65625F;
			this.fldInterestAmount.Width = 1.0625F;
			// 
			// fldOtherCreditsCount
			// 
			this.fldOtherCreditsCount.CanShrink = true;
			this.fldOtherCreditsCount.Height = 0.1875F;
			this.fldOtherCreditsCount.Left = 5.15625F;
			this.fldOtherCreditsCount.Name = "fldOtherCreditsCount";
			this.fldOtherCreditsCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherCreditsCount.Text = "Field13";
			this.fldOtherCreditsCount.Top = 0.84375F;
			this.fldOtherCreditsCount.Width = 0.65625F;
			// 
			// fldOtherCredits
			// 
			this.fldOtherCredits.CanShrink = true;
			this.fldOtherCredits.Height = 0.1875F;
			this.fldOtherCredits.Left = 1.875F;
			this.fldOtherCredits.Name = "fldOtherCredits";
			this.fldOtherCredits.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOtherCredits.Text = "Field2";
			this.fldOtherCredits.Top = 0.84375F;
			this.fldOtherCredits.Width = 2.125F;
			// 
			// fldOtherCreditsAmount
			// 
			this.fldOtherCreditsAmount.CanShrink = true;
			this.fldOtherCreditsAmount.Height = 0.1875F;
			this.fldOtherCreditsAmount.Left = 4.0625F;
			this.fldOtherCreditsAmount.Name = "fldOtherCreditsAmount";
			this.fldOtherCreditsAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherCreditsAmount.Text = "Field3";
			this.fldOtherCreditsAmount.Top = 0.84375F;
			this.fldOtherCreditsAmount.Width = 1.0625F;
			// 
			// fldCashedChecksCount
			// 
			this.fldCashedChecksCount.CanShrink = true;
			this.fldCashedChecksCount.Height = 0.1875F;
			this.fldCashedChecksCount.Left = 5.15625F;
			this.fldCashedChecksCount.Name = "fldCashedChecksCount";
			this.fldCashedChecksCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCashedChecksCount.Text = "Field16";
			this.fldCashedChecksCount.Top = 1.03125F;
			this.fldCashedChecksCount.Width = 0.65625F;
			// 
			// fldCashedChecks
			// 
			this.fldCashedChecks.CanShrink = true;
			this.fldCashedChecks.Height = 0.1875F;
			this.fldCashedChecks.Left = 1.875F;
			this.fldCashedChecks.Name = "fldCashedChecks";
			this.fldCashedChecks.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldCashedChecks.Text = "Field2";
			this.fldCashedChecks.Top = 1.03125F;
			this.fldCashedChecks.Width = 2.125F;
			// 
			// fldCashedChecksAmount
			// 
			this.fldCashedChecksAmount.CanShrink = true;
			this.fldCashedChecksAmount.Height = 0.1875F;
			this.fldCashedChecksAmount.Left = 4.0625F;
			this.fldCashedChecksAmount.Name = "fldCashedChecksAmount";
			this.fldCashedChecksAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCashedChecksAmount.Text = "Field3";
			this.fldCashedChecksAmount.Top = 1.03125F;
			this.fldCashedChecksAmount.Width = 1.0625F;
			// 
			// fldOtherDebitsCount
			// 
			this.fldOtherDebitsCount.CanShrink = true;
			this.fldOtherDebitsCount.Height = 0.1875F;
			this.fldOtherDebitsCount.Left = 5.15625F;
			this.fldOtherDebitsCount.Name = "fldOtherDebitsCount";
			this.fldOtherDebitsCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherDebitsCount.Text = "Field19";
			this.fldOtherDebitsCount.Top = 1.21875F;
			this.fldOtherDebitsCount.Width = 0.65625F;
			// 
			// fldOtherDebits
			// 
			this.fldOtherDebits.CanShrink = true;
			this.fldOtherDebits.Height = 0.1875F;
			this.fldOtherDebits.Left = 1.875F;
			this.fldOtherDebits.Name = "fldOtherDebits";
			this.fldOtherDebits.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOtherDebits.Text = "Field2";
			this.fldOtherDebits.Top = 1.21875F;
			this.fldOtherDebits.Width = 2.125F;
			// 
			// fldOtherDebitsAmount
			// 
			this.fldOtherDebitsAmount.CanShrink = true;
			this.fldOtherDebitsAmount.Height = 0.1875F;
			this.fldOtherDebitsAmount.Left = 4.0625F;
			this.fldOtherDebitsAmount.Name = "fldOtherDebitsAmount";
			this.fldOtherDebitsAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherDebitsAmount.Text = "Field3";
			this.fldOtherDebitsAmount.Top = 1.21875F;
			this.fldOtherDebitsAmount.Width = 1.0625F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 1.875F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 1.40625F;
			this.Line4.Width = 3.96875F;
			this.Line4.X1 = 1.875F;
			this.Line4.X2 = 5.84375F;
			this.Line4.Y1 = 1.40625F;
			this.Line4.Y2 = 1.40625F;
			// 
			// fldStatementTotal
			// 
			this.fldStatementTotal.CanShrink = true;
			this.fldStatementTotal.Height = 0.1875F;
			this.fldStatementTotal.Left = 1.875F;
			this.fldStatementTotal.Name = "fldStatementTotal";
			this.fldStatementTotal.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldStatementTotal.Text = "Field2";
			this.fldStatementTotal.Top = 1.4375F;
			this.fldStatementTotal.Width = 2.125F;
			// 
			// fldStatementTotalAmount
			// 
			this.fldStatementTotalAmount.CanShrink = true;
			this.fldStatementTotalAmount.Height = 0.1875F;
			this.fldStatementTotalAmount.Left = 4.0625F;
			this.fldStatementTotalAmount.Name = "fldStatementTotalAmount";
			this.fldStatementTotalAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldStatementTotalAmount.Text = "Field3";
			this.fldStatementTotalAmount.Top = 1.4375F;
			this.fldStatementTotalAmount.Width = 1.0625F;
			// 
			// fldOutstandingDepositsCount
			// 
			this.fldOutstandingDepositsCount.CanShrink = true;
			this.fldOutstandingDepositsCount.Height = 0.1875F;
			this.fldOutstandingDepositsCount.Left = 5.15625F;
			this.fldOutstandingDepositsCount.Name = "fldOutstandingDepositsCount";
			this.fldOutstandingDepositsCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingDepositsCount.Text = "Field31";
			this.fldOutstandingDepositsCount.Top = 1.71875F;
			this.fldOutstandingDepositsCount.Width = 0.65625F;
			// 
			// fldOutstandingDeposits
			// 
			this.fldOutstandingDeposits.CanShrink = true;
			this.fldOutstandingDeposits.Height = 0.1875F;
			this.fldOutstandingDeposits.Left = 1.875F;
			this.fldOutstandingDeposits.Name = "fldOutstandingDeposits";
			this.fldOutstandingDeposits.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOutstandingDeposits.Text = "Field2";
			this.fldOutstandingDeposits.Top = 1.71875F;
			this.fldOutstandingDeposits.Width = 2.125F;
			// 
			// fldOutstandingDepositsAmount
			// 
			this.fldOutstandingDepositsAmount.CanShrink = true;
			this.fldOutstandingDepositsAmount.Height = 0.1875F;
			this.fldOutstandingDepositsAmount.Left = 4.0625F;
			this.fldOutstandingDepositsAmount.Name = "fldOutstandingDepositsAmount";
			this.fldOutstandingDepositsAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingDepositsAmount.Text = "Field3";
			this.fldOutstandingDepositsAmount.Top = 1.71875F;
			this.fldOutstandingDepositsAmount.Width = 1.0625F;
			// 
			// fldOutstandingChecksCount
			// 
			this.fldOutstandingChecksCount.CanShrink = true;
			this.fldOutstandingChecksCount.Height = 0.1875F;
			this.fldOutstandingChecksCount.Left = 5.15625F;
			this.fldOutstandingChecksCount.Name = "fldOutstandingChecksCount";
			this.fldOutstandingChecksCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingChecksCount.Text = "Field34";
			this.fldOutstandingChecksCount.Top = 1.90625F;
			this.fldOutstandingChecksCount.Width = 0.65625F;
			// 
			// fldOutstandingChecks
			// 
			this.fldOutstandingChecks.CanShrink = true;
			this.fldOutstandingChecks.Height = 0.1875F;
			this.fldOutstandingChecks.Left = 1.875F;
			this.fldOutstandingChecks.Name = "fldOutstandingChecks";
			this.fldOutstandingChecks.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOutstandingChecks.Text = "Field2";
			this.fldOutstandingChecks.Top = 1.90625F;
			this.fldOutstandingChecks.Width = 2.125F;
			// 
			// fldOutstandingChecksAmount
			// 
			this.fldOutstandingChecksAmount.CanShrink = true;
			this.fldOutstandingChecksAmount.Height = 0.1875F;
			this.fldOutstandingChecksAmount.Left = 4.0625F;
			this.fldOutstandingChecksAmount.Name = "fldOutstandingChecksAmount";
			this.fldOutstandingChecksAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingChecksAmount.Text = "Field3";
			this.fldOutstandingChecksAmount.Top = 1.90625F;
			this.fldOutstandingChecksAmount.Width = 1.0625F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 1.875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.28125F;
			this.Line5.Width = 3.96875F;
			this.Line5.X1 = 1.875F;
			this.Line5.X2 = 5.84375F;
			this.Line5.Y1 = 2.28125F;
			this.Line5.Y2 = 2.28125F;
			// 
			// fldCheckBookStatement
			// 
			this.fldCheckBookStatement.CanShrink = true;
			this.fldCheckBookStatement.Height = 0.1875F;
			this.fldCheckBookStatement.Left = 1.875F;
			this.fldCheckBookStatement.Name = "fldCheckBookStatement";
			this.fldCheckBookStatement.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldCheckBookStatement.Text = "Field2";
			this.fldCheckBookStatement.Top = 2.3125F;
			this.fldCheckBookStatement.Width = 2.125F;
			// 
			// fldCheckBookStatementAmount
			// 
			this.fldCheckBookStatementAmount.CanShrink = true;
			this.fldCheckBookStatementAmount.Height = 0.1875F;
			this.fldCheckBookStatementAmount.Left = 4.0625F;
			this.fldCheckBookStatementAmount.Name = "fldCheckBookStatementAmount";
			this.fldCheckBookStatementAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCheckBookStatementAmount.Text = "Field3";
			this.fldCheckBookStatementAmount.Top = 2.3125F;
			this.fldCheckBookStatementAmount.Width = 1.0625F;
			// 
			// fldOtherDepositsCount
			// 
			this.fldOtherDepositsCount.CanShrink = true;
			this.fldOtherDepositsCount.Height = 0.1875F;
			this.fldOtherDepositsCount.Left = 5.15625F;
			this.fldOtherDepositsCount.Name = "fldOtherDepositsCount";
			this.fldOtherDepositsCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherDepositsCount.Text = "Field40";
			this.fldOtherDepositsCount.Top = 2.59375F;
			this.fldOtherDepositsCount.Width = 0.65625F;
			// 
			// fldOtherDeposits
			// 
			this.fldOtherDeposits.CanShrink = true;
			this.fldOtherDeposits.Height = 0.1875F;
			this.fldOtherDeposits.Left = 1.875F;
			this.fldOtherDeposits.Name = "fldOtherDeposits";
			this.fldOtherDeposits.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOtherDeposits.Text = "Field2";
			this.fldOtherDeposits.Top = 2.59375F;
			this.fldOtherDeposits.Width = 2.125F;
			// 
			// fldOtherDepositsAmount
			// 
			this.fldOtherDepositsAmount.CanShrink = true;
			this.fldOtherDepositsAmount.Height = 0.1875F;
			this.fldOtherDepositsAmount.Left = 4.0625F;
			this.fldOtherDepositsAmount.Name = "fldOtherDepositsAmount";
			this.fldOtherDepositsAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOtherDepositsAmount.Text = "Field3";
			this.fldOtherDepositsAmount.Top = 2.59375F;
			this.fldOtherDepositsAmount.Width = 1.0625F;
			// 
			// fldIssuedChecksCount
			// 
			this.fldIssuedChecksCount.CanShrink = true;
			this.fldIssuedChecksCount.Height = 0.1875F;
			this.fldIssuedChecksCount.Left = 5.15625F;
			this.fldIssuedChecksCount.Name = "fldIssuedChecksCount";
			this.fldIssuedChecksCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldIssuedChecksCount.Text = "Field43";
			this.fldIssuedChecksCount.Top = 2.78125F;
			this.fldIssuedChecksCount.Width = 0.65625F;
			// 
			// fldIssuedChecks
			// 
			this.fldIssuedChecks.CanShrink = true;
			this.fldIssuedChecks.Height = 0.1875F;
			this.fldIssuedChecks.Left = 1.875F;
			this.fldIssuedChecks.Name = "fldIssuedChecks";
			this.fldIssuedChecks.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldIssuedChecks.Text = "Field2";
			this.fldIssuedChecks.Top = 2.78125F;
			this.fldIssuedChecks.Width = 2.125F;
			// 
			// fldIssuedChecksAmount
			// 
			this.fldIssuedChecksAmount.CanShrink = true;
			this.fldIssuedChecksAmount.Height = 0.1875F;
			this.fldIssuedChecksAmount.Left = 4.0625F;
			this.fldIssuedChecksAmount.Name = "fldIssuedChecksAmount";
			this.fldIssuedChecksAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldIssuedChecksAmount.Text = "Field3";
			this.fldIssuedChecksAmount.Top = 2.78125F;
			this.fldIssuedChecksAmount.Width = 1.0625F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 1.875F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 3.15625F;
			this.Line6.Width = 3.96875F;
			this.Line6.X1 = 1.875F;
			this.Line6.X2 = 5.84375F;
			this.Line6.Y1 = 3.15625F;
			this.Line6.Y2 = 3.15625F;
			// 
			// fldCurrentCheckbook
			// 
			this.fldCurrentCheckbook.CanShrink = true;
			this.fldCurrentCheckbook.Height = 0.1875F;
			this.fldCurrentCheckbook.Left = 1.875F;
			this.fldCurrentCheckbook.Name = "fldCurrentCheckbook";
			this.fldCurrentCheckbook.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldCurrentCheckbook.Text = "Field2";
			this.fldCurrentCheckbook.Top = 3.1875F;
			this.fldCurrentCheckbook.Width = 2.125F;
			// 
			// fldCurrentCheckbookAmount
			// 
			this.fldCurrentCheckbookAmount.CanShrink = true;
			this.fldCurrentCheckbookAmount.Height = 0.1875F;
			this.fldCurrentCheckbookAmount.Left = 4.0625F;
			this.fldCurrentCheckbookAmount.Name = "fldCurrentCheckbookAmount";
			this.fldCurrentCheckbookAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldCurrentCheckbookAmount.Text = "Field3";
			this.fldCurrentCheckbookAmount.Top = 3.1875F;
			this.fldCurrentCheckbookAmount.Width = 1.0625F;
			// 
			// fldOutstandingOtherCount
			// 
			this.fldOutstandingOtherCount.CanShrink = true;
			this.fldOutstandingOtherCount.Height = 0.1875F;
			this.fldOutstandingOtherCount.Left = 5.15625F;
			this.fldOutstandingOtherCount.Name = "fldOutstandingOtherCount";
			this.fldOutstandingOtherCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingOtherCount.Text = "Field34";
			this.fldOutstandingOtherCount.Top = 2.09375F;
			this.fldOutstandingOtherCount.Width = 0.65625F;
			// 
			// fldOutstandingOther
			// 
			this.fldOutstandingOther.CanShrink = true;
			this.fldOutstandingOther.Height = 0.1875F;
			this.fldOutstandingOther.Left = 1.875F;
			this.fldOutstandingOther.Name = "fldOutstandingOther";
			this.fldOutstandingOther.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldOutstandingOther.Text = "Field2";
			this.fldOutstandingOther.Top = 2.09375F;
			this.fldOutstandingOther.Width = 2.125F;
			// 
			// fldOutstandingOtherAmount
			// 
			this.fldOutstandingOtherAmount.CanShrink = true;
			this.fldOutstandingOtherAmount.Height = 0.1875F;
			this.fldOutstandingOtherAmount.Left = 4.0625F;
			this.fldOutstandingOtherAmount.Name = "fldOutstandingOtherAmount";
			this.fldOutstandingOtherAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldOutstandingOtherAmount.Text = "Field3";
			this.fldOutstandingOtherAmount.Top = 2.09375F;
			this.fldOutstandingOtherAmount.Width = 1.0625F;
			// 
			// fldIssuedOtherCount
			// 
			this.fldIssuedOtherCount.CanShrink = true;
			this.fldIssuedOtherCount.Height = 0.1875F;
			this.fldIssuedOtherCount.Left = 5.15625F;
			this.fldIssuedOtherCount.Name = "fldIssuedOtherCount";
			this.fldIssuedOtherCount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldIssuedOtherCount.Text = "Field43";
			this.fldIssuedOtherCount.Top = 2.96875F;
			this.fldIssuedOtherCount.Width = 0.65625F;
			// 
			// fldIssuedOther
			// 
			this.fldIssuedOther.CanShrink = true;
			this.fldIssuedOther.Height = 0.1875F;
			this.fldIssuedOther.Left = 1.875F;
			this.fldIssuedOther.Name = "fldIssuedOther";
			this.fldIssuedOther.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldIssuedOther.Text = "Field2";
			this.fldIssuedOther.Top = 2.96875F;
			this.fldIssuedOther.Width = 2.125F;
			// 
			// fldIssuedOtherAmount
			// 
			this.fldIssuedOtherAmount.CanShrink = true;
			this.fldIssuedOtherAmount.Height = 0.1875F;
			this.fldIssuedOtherAmount.Left = 4.0625F;
			this.fldIssuedOtherAmount.Name = "fldIssuedOtherAmount";
			this.fldIssuedOtherAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.fldIssuedOtherAmount.Text = "Field3";
			this.fldIssuedOtherAmount.Top = 2.96875F;
			this.fldIssuedOtherAmount.Width = 1.0625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblTitle1,
				this.lblBank,
				this.lblStatement,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Line1,
				this.Label22
			});
			this.PageHeader.Height = 1.364583F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.375F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Bank Reconciliation";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblTitle1
			// 
			this.lblTitle1.Height = 0.21875F;
			this.lblTitle1.HyperLink = null;
			this.lblTitle1.Left = 0F;
			this.lblTitle1.Name = "lblTitle1";
			this.lblTitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblTitle1.Text = "Expense";
			this.lblTitle1.Top = 0.375F;
			this.lblTitle1.Width = 7.5F;
			// 
			// lblBank
			// 
			this.lblBank.Height = 0.1875F;
			this.lblBank.HyperLink = null;
			this.lblBank.Left = 0.96875F;
			this.lblBank.Name = "lblBank";
			this.lblBank.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblBank.Text = "Label15";
			this.lblBank.Top = 0.71875F;
			this.lblBank.Width = 2.6875F;
			// 
			// lblStatement
			// 
			this.lblStatement.Height = 0.1875F;
			this.lblStatement.HyperLink = null;
			this.lblStatement.Left = 4.59375F;
			this.lblStatement.Name = "lblStatement";
			this.lblStatement.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblStatement.Text = "Label16";
			this.lblStatement.Top = 0.71875F;
			this.lblStatement.Width = 2.5625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.09375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Check";
			this.Label15.Top = 1.15625F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 1F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label16.Text = "Type";
			this.Label16.Top = 1.15625F;
			this.Label16.Width = 0.375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.46875F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label17.Text = "Date";
			this.Label17.Top = 1.15625F;
			this.Label17.Width = 0.71875F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.28125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label18.Text = "Amount";
			this.Label18.Top = 1.15625F;
			this.Label18.Width = 1.0625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.5625F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label19.Text = "Code";
			this.Label19.Top = 1.15625F;
			this.Label19.Width = 0.5F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.0625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label20.Text = "Date";
			this.Label20.Top = 1.15625F;
			this.Label20.Width = 0.65625F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.84375F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label21.Text = "Payee";
			this.Label21.Top = 1.15625F;
			this.Label21.Width = 2.59375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.34375F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 7.5F;
			this.Line1.X2 = 0.03125F;
			this.Line1.Y1 = 1.34375F;
			this.Line1.Y2 = 1.34375F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.53125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label22.Text = "--Status--";
			this.Label22.Top = 0.96875F;
			this.Label22.Width = 1.0625F;
			// 
			// PageFooter
			// 
			this.PageFooter.CanShrink = true;
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Binder,
				this.fldTitle,
				this.Line2,
				this.Line3
			});
			this.GroupHeader1.DataField = "TypeBinder";
			this.GroupHeader1.Height = 0.2395833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// Binder
			// 
			this.Binder.DataField = "TypeBinder";
			this.Binder.Height = 0.19F;
			this.Binder.Left = 6.1875F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.0625F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.78125F;
			// 
			// fldTitle
			// 
			this.fldTitle.CanShrink = true;
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 2.875F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.fldTitle.Text = "Field1";
			this.fldTitle.Top = 0.03125F;
			this.fldTitle.Width = 1.71875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 1.46875F;
			this.Line2.X1 = 4.625F;
			this.Line2.X2 = 6.09375F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.125F;
			this.Line3.Width = 1.46875F;
			this.Line3.X1 = 1.375F;
			this.Line3.X2 = 2.84375F;
			this.Line3.Y1 = 0.125F;
			this.Line3.Y2 = 0.125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1
			});
			this.GroupHeader2.DataField = "StatusBinder";
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// Field1
			// 
			this.Field1.DataField = "StatusBinder";
			this.Field1.Height = 0.19F;
			this.Field1.Left = 0.3125F;
			this.Field1.Name = "Field1";
			this.Field1.Text = "StatusBinder";
			this.Field1.Top = 0.0625F;
			this.Field1.Visible = false;
			this.Field1.Width = 0.78125F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label23,
				this.Label24,
				this.fldCount,
				this.fldTotal
			});
			this.GroupFooter2.Height = 0.5729167F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label23.Text = "Count";
			this.Label23.Top = 0.09375F;
			this.Label23.Width = 0.5625F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 3.84375F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label24.Text = "Total";
			this.Label24.Top = 0.09375F;
			this.Label24.Width = 0.5F;
			// 
			// fldCount
			// 
			this.fldCount.Height = 0.1875F;
			this.fldCount.Left = 2.59375F;
			this.fldCount.Name = "fldCount";
			this.fldCount.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldCount.Text = "Field1";
			this.fldCount.Top = 0.09375F;
			this.fldCount.Width = 0.53125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 4.40625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.09375F;
			this.fldTotal.Width = 0.9375F;
			// 
			// rptBalancingReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.3F;
			this.PageSettings.Margins.Right = 0.3F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptBalancingReport_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatusDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPayee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBegBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBegBalanceAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDepositsCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDeposits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedDepositsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecksCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnedChecksAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCreditsCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherCreditsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecksCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashedChecksAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebitsCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDebitsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDepositsCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDeposits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingDepositsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecksCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingChecksAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckBookStatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheckBookStatementAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDepositsCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDeposits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOtherDepositsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecksCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedChecksAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCheckbook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentCheckbookAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOtherCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOutstandingOtherAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOtherCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOther)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIssuedOtherAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBank)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatusDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayee;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBegBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBegBalanceAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedDepositsCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedDeposits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedDepositsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnedChecksCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnedChecks;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnedChecksAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterestCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterestAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherCreditsCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherCreditsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedChecksCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedChecks;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashedChecksAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDebitsCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDebitsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatementTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatementTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingDepositsCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingDeposits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingDepositsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingChecksCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingChecks;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingChecksAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckBookStatement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheckBookStatementAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDepositsCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDeposits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherDepositsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedChecksCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedChecks;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedChecksAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentCheckbook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentCheckbookAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingOtherCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOutstandingOtherAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedOtherCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedOther;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIssuedOtherAmount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBank;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStatement;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
	}
}
