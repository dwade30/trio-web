﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeStatementDate.
	/// </summary>
	partial class frmChangeStatementDate : BaseForm
	{
		public fecherFoundation.FCButton cmdCalendar2;
		public fecherFoundation.FCButton cmdProcess;
		public Global.T2KDateBox txtStatementDate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblOldDate;
		public fecherFoundation.FCLabel lblOld;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeStatementDate));
            this.cmdCalendar2 = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.txtStatementDate = new Global.T2KDateBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblOldDate = new fecherFoundation.FCLabel();
            this.lblOld = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatementDate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 264);
            this.BottomPanel.Size = new System.Drawing.Size(510, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdProcess);
            this.ClientArea.Controls.Add(this.cmdCalendar2);
            this.ClientArea.Controls.Add(this.txtStatementDate);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblOldDate);
            this.ClientArea.Controls.Add(this.lblOld);
            this.ClientArea.Size = new System.Drawing.Size(510, 204);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(510, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(183, 30);
            this.HeaderText.Text = "Statement Date";
            // 
            // cmdCalendar2
            // 
            this.cmdCalendar2.AppearanceKey = "imageButton";
            this.cmdCalendar2.ImageSource = "icon - calendar?color=#707884";
            this.cmdCalendar2.Location = new System.Drawing.Point(373, 66);
            this.cmdCalendar2.Name = "cmdCalendar2";
            this.cmdCalendar2.Size = new System.Drawing.Size(40, 40);
            this.cmdCalendar2.TabIndex = 6;
            this.cmdCalendar2.Click += new System.EventHandler(this.cmdCalendar2_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(30, 124);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(104, 48);
            this.cmdProcess.TabIndex = 4;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // txtStatementDate
            // 
            this.txtStatementDate.Location = new System.Drawing.Point(235, 66);
            this.txtStatementDate.Mask = "##/##/####";
            this.txtStatementDate.MaxLength = 10;
            this.txtStatementDate.Name = "txtStatementDate";
            this.txtStatementDate.Size = new System.Drawing.Size(116, 40);
            this.txtStatementDate.TabIndex = 2;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 80);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(135, 16);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "NEW STATEMENT DATE";
            // 
            // lblOldDate
            // 
            this.lblOldDate.Location = new System.Drawing.Point(235, 30);
            this.lblOldDate.Name = "lblOldDate";
            this.lblOldDate.Size = new System.Drawing.Size(116, 16);
            this.lblOldDate.TabIndex = 1;
            // 
            // lblOld
            // 
            this.lblOld.Location = new System.Drawing.Point(30, 30);
            this.lblOld.Name = "lblOld";
            this.lblOld.Size = new System.Drawing.Size(135, 16);
            this.lblOld.TabIndex = 7;
            this.lblOld.Text = "OLD STATEMENT DATE";
            // 
            // frmChangeStatementDate
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(510, 264);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChangeStatementDate";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Statement Date";
            this.Load += new System.EventHandler(this.frmChangeStatementDate_Load);
            this.Activated += new System.EventHandler(this.frmChangeStatementDate_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmChangeStatementDate_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCalendar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatementDate)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
