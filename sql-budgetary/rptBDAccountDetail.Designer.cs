﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBDAccountDetail.
	/// </summary>
	partial class rptBDAccountDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBDAccountDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblUser = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubtitle1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccountDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJournalType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtPeriod,
				this.txtJournal,
				this.txtCheck,
				this.txtDate,
				this.txtJournalType,
				this.txtCredits,
				this.txtDebits,
				this.txtDescription
			});
			this.Detail.Height = 0.1569444F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label4,
				this.Label7,
				this.Label8,
				this.lblPage,
				this.Label10,
				this.lblUser,
				this.lblSubtitle1,
				this.lblSubTitle2,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19
			});
			this.PageHeader.Height = 0.9166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtAccountDescription
			});
			this.GroupHeader1.DataField = "GroupHeader1Binder";
			this.GroupHeader1.Height = 0.1770833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.5F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label4.Text = "Account Detail";
			this.Label4.Top = 0F;
			this.Label4.Width = 7.1875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Label8";
			this.Label8.Top = 0F;
			this.Label8.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.666667F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblPage.Text = "Label9";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 8.666667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label10.Text = "Label10";
			this.Label10.Top = 0F;
			this.Label10.Width = 1.3125F;
			// 
			// lblUser
			// 
			this.lblUser.Height = 0.1875F;
			this.lblUser.HyperLink = null;
			this.lblUser.Left = 0F;
			this.lblUser.Name = "lblUser";
			this.lblUser.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblUser.Text = "Label15";
			this.lblUser.Top = 0.375F;
			this.lblUser.Width = 1.5F;
			// 
			// lblSubtitle1
			// 
			this.lblSubtitle1.Height = 0.1875F;
			this.lblSubtitle1.HyperLink = null;
			this.lblSubtitle1.Left = 1.5F;
			this.lblSubtitle1.Name = "lblSubtitle1";
			this.lblSubtitle1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblSubtitle1.Text = null;
			this.lblSubtitle1.Top = 0.1875F;
			this.lblSubtitle1.Width = 7.1875F;
			// 
			// lblSubTitle2
			// 
			this.lblSubTitle2.Height = 0.1875F;
			this.lblSubTitle2.HyperLink = null;
			this.lblSubTitle2.Left = 1.5F;
			this.lblSubTitle2.Name = "lblSubTitle2";
			this.lblSubTitle2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblSubTitle2.Text = null;
			this.lblSubTitle2.Top = 0.375F;
			this.lblSubTitle2.Width = 7.1875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 1.458333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label11.Text = "Period";
			this.Label11.Top = 0.75F;
			this.Label11.Width = 0.5208333F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 2F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label12.Text = "Journal";
			this.Label12.Top = 0.75F;
			this.Label12.Width = 0.5833333F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 2.625F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label13.Text = "Check";
			this.Label13.Top = 0.75F;
			this.Label13.Width = 0.5833333F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label14.Text = "Date";
			this.Label14.Top = 0.75F;
			this.Label14.Width = 0.7708333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 4F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label15.Text = "Journal Type";
			this.Label15.Top = 0.75F;
			this.Label15.Width = 1.270833F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.3125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label16.Text = "Credits";
			this.Label16.Top = 0.75F;
			this.Label16.Width = 0.8958333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 6.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label17.Text = "Debits";
			this.Label17.Top = 0.75F;
			this.Label17.Width = 0.8958333F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 7.1875F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label18.Text = "Description";
			this.Label18.Top = 0.75F;
			this.Label18.Width = 2.645833F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 8.5pt; font-weight: bold";
			this.Label19.Text = "Account";
			this.Label19.Top = 0.75F;
			this.Label19.Width = 1.395833F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.15625F;
			this.txtAccount.Left = 0F;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-size: 8.5pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.416667F;
			// 
			// txtAccountDescription
			// 
			this.txtAccountDescription.Height = 0.15625F;
			this.txtAccountDescription.Left = 1.458333F;
			this.txtAccountDescription.Name = "txtAccountDescription";
			this.txtAccountDescription.Style = "font-size: 8.5pt";
			this.txtAccountDescription.Text = null;
			this.txtAccountDescription.Top = 0F;
			this.txtAccountDescription.Width = 7.604167F;
			// 
			// txtPeriod
			// 
			this.txtPeriod.Height = 0.15625F;
			this.txtPeriod.Left = 1.458333F;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Style = "font-size: 8.5pt";
			this.txtPeriod.Text = null;
			this.txtPeriod.Top = 0F;
			this.txtPeriod.Width = 0.4791667F;
			// 
			// txtJournal
			// 
			this.txtJournal.Height = 0.15625F;
			this.txtJournal.Left = 2F;
			this.txtJournal.Name = "txtJournal";
			this.txtJournal.Style = "font-size: 8.5pt";
			this.txtJournal.Text = null;
			this.txtJournal.Top = 0F;
			this.txtJournal.Width = 0.6041667F;
			// 
			// txtCheck
			// 
			this.txtCheck.Height = 0.15625F;
			this.txtCheck.Left = 2.625F;
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Style = "font-size: 8.5pt";
			this.txtCheck.Text = null;
			this.txtCheck.Top = 0F;
			this.txtCheck.Width = 0.6041667F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.15625F;
			this.txtDate.Left = 3.25F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-size: 8.5pt";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 0.7291667F;
			// 
			// txtJournalType
			// 
			this.txtJournalType.Height = 0.15625F;
			this.txtJournalType.Left = 4F;
			this.txtJournalType.Name = "txtJournalType";
			this.txtJournalType.Style = "font-size: 8.5pt";
			this.txtJournalType.Text = null;
			this.txtJournalType.Top = 0F;
			this.txtJournalType.Width = 1.291667F;
			// 
			// txtCredits
			// 
			this.txtCredits.Height = 0.15625F;
			this.txtCredits.Left = 5.3125F;
			this.txtCredits.Name = "txtCredits";
			this.txtCredits.Style = "font-size: 8.5pt";
			this.txtCredits.Text = null;
			this.txtCredits.Top = 0F;
			this.txtCredits.Width = 0.9166667F;
			// 
			// txtDebits
			// 
			this.txtDebits.Height = 0.15625F;
			this.txtDebits.Left = 6.25F;
			this.txtDebits.Name = "txtDebits";
			this.txtDebits.Style = "font-size: 8.5pt";
			this.txtDebits.Text = null;
			this.txtDebits.Top = 0F;
			this.txtDebits.Width = 0.9166667F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.15625F;
			this.txtDescription.Left = 7.1875F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-size: 8.5pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 2.666667F;
			// 
			// rptBDAccountDetail
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.989583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccountDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJournalType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUser;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubtitle1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccountDescription;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
