//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmVendorDetail.
	/// </summary>
	partial class frmVendorDetail : BaseForm
	{
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCFrame fraVendorSelect;
		public fecherFoundation.FCComboBox cboVendors;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCFrame fraSearch;
		public fecherFoundation.FCTextBox txtCriteria;
		public fecherFoundation.FCButton cmdRunSearch;
		public fecherFoundation.FCButton cmdBackToSelect;
		public fecherFoundation.FCFrame fraMultiple;
		public fecherFoundation.FCGrid vsVendors;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCGrid vsWhere;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCFrame fraAccountRange;
		public fecherFoundation.FCButton cmdAccountCancel;
		public fecherFoundation.FCButton cmdAccountProcess;
		public AccountInputEditor vsLowAccount;
		public AccountInputEditor vsHighAccount;
		public AccountInputEditor vsSingleAccount;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendorDetail));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.fraVendorSelect = new fecherFoundation.FCFrame();
            this.cmdOK = new fecherFoundation.FCButton();
            this.cboVendors = new fecherFoundation.FCComboBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.fraSearch = new fecherFoundation.FCFrame();
            this.txtCriteria = new fecherFoundation.FCTextBox();
            this.cmdRunSearch = new fecherFoundation.FCButton();
            this.cmdBackToSelect = new fecherFoundation.FCButton();
            this.fraMultiple = new fecherFoundation.FCFrame();
            this.vsVendors = new fecherFoundation.FCGrid();
            this.cmdGet = new fecherFoundation.FCButton();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdPreview = new fecherFoundation.FCButton();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.fraAccountRange = new fecherFoundation.FCFrame();
            this.cmdAccountCancel = new fecherFoundation.FCButton();
            this.cmdAccountProcess = new fecherFoundation.FCButton();
            this.vsLowAccount = new Global.AccountInputEditor();
            this.vsHighAccount = new Global.AccountInputEditor();
            this.vsSingleAccount = new Global.AccountInputEditor();
            this.lblTo = new fecherFoundation.FCLabel();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVendorSelect)).BeginInit();
            this.fraVendorSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
            this.fraSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRunSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBackToSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultiple)).BeginInit();
            this.fraMultiple.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsVendors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).BeginInit();
            this.fraAccountRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccountCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccountProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 512);
            this.BottomPanel.Size = new System.Drawing.Size(1042, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraVendorSelect);
            this.ClientArea.Controls.Add(this.fraSearch);
            this.ClientArea.Controls.Add(this.fraMultiple);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraAccountRange);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Size = new System.Drawing.Size(1042, 452);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1042, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(161, 30);
            this.HeaderText.Text = "Vendor Detail";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Delete Saved Report",
            "Show Saved Report",
            "Create New Report"});
            this.cmbReport.Location = new System.Drawing.Point(20, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(281, 40);
            this.cmbReport.TabIndex = 26;
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // fraVendorSelect
            // 
            this.fraVendorSelect.BackColor = System.Drawing.Color.FromName("@window");
            this.fraVendorSelect.Controls.Add(this.cmdOK);
            this.fraVendorSelect.Controls.Add(this.cboVendors);
            this.fraVendorSelect.Controls.Add(this.cmdSearch);
            this.fraVendorSelect.Controls.Add(this.cmdCancel);
            this.fraVendorSelect.Location = new System.Drawing.Point(300, 220);
            this.fraVendorSelect.Name = "fraVendorSelect";
            this.fraVendorSelect.Size = new System.Drawing.Size(466, 150);
            this.fraVendorSelect.TabIndex = 12;
            this.fraVendorSelect.Text = "Select Vendor";
            this.ToolTip1.SetToolTip(this.fraVendorSelect, null);
            this.fraVendorSelect.Visible = false;
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(103, 90);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 40);
            this.cmdOK.TabIndex = 17;
            this.cmdOK.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOK, null);
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cboVendors
            // 
            this.cboVendors.BackColor = System.Drawing.SystemColors.Window;
            this.cboVendors.Location = new System.Drawing.Point(20, 30);
            this.cboVendors.Name = "cboVendors";
            this.cboVendors.Size = new System.Drawing.Size(429, 40);
            this.cboVendors.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.cboVendors, null);
            this.cboVendors.Validating += new System.ComponentModel.CancelEventHandler(this.cboVendors_Validating);
            this.cboVendors.KeyPress += new Wisej.Web.KeyPressEventHandler(this.cboVendors_KeyPress);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(196, 90);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(75, 40);
            this.cmdSearch.TabIndex = 14;
            this.cmdSearch.Text = "Search";
            this.ToolTip1.SetToolTip(this.cmdSearch, null);
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(289, 90);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 40);
            this.cmdCancel.TabIndex = 13;
            this.cmdCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdCancel, null);
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // fraSearch
            // 
            this.fraSearch.BackColor = System.Drawing.Color.FromName("@window");
            this.fraSearch.Controls.Add(this.txtCriteria);
            this.fraSearch.Controls.Add(this.cmdRunSearch);
            this.fraSearch.Controls.Add(this.cmdBackToSelect);
            this.fraSearch.Location = new System.Drawing.Point(300, 220);
            this.fraSearch.Name = "fraSearch";
            this.fraSearch.Size = new System.Drawing.Size(335, 150);
            this.fraSearch.TabIndex = 4;
            this.fraSearch.Text = "Search For Vendor Name";
            this.ToolTip1.SetToolTip(this.fraSearch, null);
            this.fraSearch.Visible = false;
            // 
            // txtCriteria
            // 
            this.txtCriteria.BackColor = System.Drawing.SystemColors.Window;
            this.txtCriteria.Location = new System.Drawing.Point(20, 30);
            this.txtCriteria.Name = "txtCriteria";
            this.txtCriteria.Size = new System.Drawing.Size(295, 40);
            this.txtCriteria.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtCriteria, null);
            this.txtCriteria.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCriteria_KeyPress);
            // 
            // cmdRunSearch
            // 
            this.cmdRunSearch.AppearanceKey = "actionButton";
            this.cmdRunSearch.Location = new System.Drawing.Point(20, 90);
            this.cmdRunSearch.Name = "cmdRunSearch";
            this.cmdRunSearch.Size = new System.Drawing.Size(75, 40);
            this.cmdRunSearch.TabIndex = 6;
            this.cmdRunSearch.Text = "Search";
            this.ToolTip1.SetToolTip(this.cmdRunSearch, null);
            this.cmdRunSearch.Click += new System.EventHandler(this.cmdRunSearch_Click);
            // 
            // cmdBackToSelect
            // 
            this.cmdBackToSelect.AppearanceKey = "actionButton";
            this.cmdBackToSelect.Location = new System.Drawing.Point(240, 90);
            this.cmdBackToSelect.Name = "cmdBackToSelect";
            this.cmdBackToSelect.Size = new System.Drawing.Size(75, 40);
            this.cmdBackToSelect.TabIndex = 5;
            this.cmdBackToSelect.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdBackToSelect, null);
            this.cmdBackToSelect.Click += new System.EventHandler(this.cmdBackToSelect_Click);
            // 
            // fraMultiple
            // 
            this.fraMultiple.BackColor = System.Drawing.Color.FromName("@window");
            this.fraMultiple.Controls.Add(this.vsVendors);
            this.fraMultiple.Controls.Add(this.cmdGet);
            this.fraMultiple.Controls.Add(this.cmdReturn);
            this.fraMultiple.Location = new System.Drawing.Point(300, 220);
            this.fraMultiple.Name = "fraMultiple";
            this.fraMultiple.Size = new System.Drawing.Size(420, 260);
            this.fraMultiple.TabIndex = 8;
            this.fraMultiple.Text = "Multiple Records";
            this.ToolTip1.SetToolTip(this.fraMultiple, null);
            this.fraMultiple.Visible = false;
            // 
            // vsVendors
            // 
            this.vsVendors.FixedCols = 0;
            this.vsVendors.Location = new System.Drawing.Point(20, 30);
            this.vsVendors.Name = "vsVendors";
            this.vsVendors.RowHeadersVisible = false;
            this.vsVendors.Rows = 1;
            this.vsVendors.ShowFocusCell = false;
            this.vsVendors.Size = new System.Drawing.Size(380, 150);
            this.vsVendors.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.vsVendors, null);
            this.vsVendors.DoubleClick += new System.EventHandler(this.vsVendors_DblClick);
            this.vsVendors.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsVendors_KeyPressEvent);
            // 
            // cmdGet
            // 
            this.cmdGet.AppearanceKey = "actionButton";
            this.cmdGet.Location = new System.Drawing.Point(250, 200);
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Size = new System.Drawing.Size(150, 40);
            this.cmdGet.TabIndex = 10;
            this.cmdGet.Text = "Retrieve Record";
            this.ToolTip1.SetToolTip(this.cmdGet, null);
            this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(20, 200);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(210, 40);
            this.cmdReturn.TabIndex = 9;
            this.cmdReturn.Text = "Return to Search Screen";
            this.ToolTip1.SetToolTip(this.cmdReturn, null);
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // Frame3
            // 
            this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame3.AppearanceKey = "groupBoxNoBorders";
            this.Frame3.Controls.Add(this.fraMessage);
            this.Frame3.Controls.Add(this.vsLayout);
            this.Frame3.Controls.Add(this.Image1);
            this.Frame3.Controls.Add(this.Label1);
            this.Frame3.Controls.Add(this.Label2);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(958, 223);
            this.Frame3.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // fraMessage
            // 
            this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(78, 72);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(846, 92);
            this.fraMessage.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.fraMessage, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(188, 40);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(500, 16);
            this.Label3.TabIndex = 36;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // vsLayout
            // 
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.Enabled = false;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.Location = new System.Drawing.Point(78, 72);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.ReadOnly = false;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.Rows = 1;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(846, 92);
            this.vsLayout.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.vsLayout, null);
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // Image1
            // 
            this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(30, 30);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(894, 22);
            this.ToolTip1.SetToolTip(this.Image1, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 72);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(9, 60);
            this.Label1.TabIndex = 40;
            this.Label1.Text = "L E F T";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(49, 72);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(9, 86);
            this.Label2.TabIndex = 39;
            this.Label2.Text = "M A R G I N";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // cmdPreview
            // 
            this.cmdPreview.AppearanceKey = "acceptButton";
            this.cmdPreview.Location = new System.Drawing.Point(451, 30);
            this.cmdPreview.Name = "cmdPreview";
            this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPreview.Size = new System.Drawing.Size(141, 48);
            this.cmdPreview.TabIndex = 30;
            this.cmdPreview.Text = "Select Vendor";
            this.ToolTip1.SetToolTip(this.cmdPreview, null);
            this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 444);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(754, 250);
            this.fraWhere.TabIndex = 19;
            this.fraWhere.Text = "Search Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.ShowFocusCell = false;
            this.vsWhere.Size = new System.Drawing.Size(716, 202);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.ChangeEdit += new System.EventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(861, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(140, 24);
            this.cmdClear.TabIndex = 21;
            this.cmdClear.Text = "Clear Search Criteria";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // fraAccountRange
            // 
            this.fraAccountRange.BackColor = System.Drawing.Color.FromName("@window");
            this.fraAccountRange.Controls.Add(this.cmdAccountCancel);
            this.fraAccountRange.Controls.Add(this.cmdAccountProcess);
            this.fraAccountRange.Controls.Add(this.vsLowAccount);
            this.fraAccountRange.Controls.Add(this.vsHighAccount);
            this.fraAccountRange.Controls.Add(this.vsSingleAccount);
            this.fraAccountRange.Controls.Add(this.lblTo);
            this.fraAccountRange.FormatCaption = false;
            this.fraAccountRange.Location = new System.Drawing.Point(300, 220);
            this.fraAccountRange.Name = "fraAccountRange";
            this.fraAccountRange.Size = new System.Drawing.Size(380, 150);
            this.fraAccountRange.TabIndex = 35;
            this.fraAccountRange.Text = "Select Account(s)";
            this.ToolTip1.SetToolTip(this.fraAccountRange, null);
            this.fraAccountRange.Visible = false;
            // 
            // cmdAccountCancel
            // 
            this.cmdAccountCancel.AppearanceKey = "actionButton";
            this.cmdAccountCancel.Location = new System.Drawing.Point(198, 90);
            this.cmdAccountCancel.Name = "cmdAccountCancel";
            this.cmdAccountCancel.Size = new System.Drawing.Size(90, 40);
            this.cmdAccountCancel.TabIndex = 5;
            this.cmdAccountCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdAccountCancel, null);
            this.cmdAccountCancel.Click += new System.EventHandler(this.cmdAccountCancel_Click);
            // 
            // cmdAccountProcess
            // 
            this.cmdAccountProcess.AppearanceKey = "actionButton";
            this.cmdAccountProcess.Location = new System.Drawing.Point(88, 90);
            this.cmdAccountProcess.Name = "cmdAccountProcess";
            this.cmdAccountProcess.Size = new System.Drawing.Size(90, 40);
            this.cmdAccountProcess.TabIndex = 4;
            this.cmdAccountProcess.Text = "Process";
            this.ToolTip1.SetToolTip(this.cmdAccountProcess, null);
            this.cmdAccountProcess.Click += new System.EventHandler(this.cmdAccountProcess_Click);
            // 
            // vsLowAccount
            // 
            this.vsLowAccount.Location = new System.Drawing.Point(20, 30);
            this.vsLowAccount.Name = "vsLowAccount";
            this.vsLowAccount.ReadOnly = false;
            this.vsLowAccount.Size = new System.Drawing.Size(150, 40);
            this.vsLowAccount.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.vsLowAccount, null);
            this.vsLowAccount.Visible = false;
            // 
            // vsHighAccount
            // 
            this.vsHighAccount.Location = new System.Drawing.Point(220, 30);
            this.vsHighAccount.Name = "vsHighAccount";
            this.vsHighAccount.ReadOnly = false;
            this.vsHighAccount.Size = new System.Drawing.Size(150, 40);
            this.vsHighAccount.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.vsHighAccount, null);
            this.vsHighAccount.Visible = false;
            // 
            // vsSingleAccount
            // 
            this.vsSingleAccount.Location = new System.Drawing.Point(112, 30);
            this.vsSingleAccount.Name = "vsSingleAccount";
            this.vsSingleAccount.ReadOnly = false;
            this.vsSingleAccount.Size = new System.Drawing.Size(150, 40);
            this.vsSingleAccount.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.vsSingleAccount, null);
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.SystemColors.Menu;
            this.lblTo.Location = new System.Drawing.Point(180, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 16);
            this.lblTo.TabIndex = 3;
            this.lblTo.Text = "TO";
            this.ToolTip1.SetToolTip(this.lblTo, null);
            this.lblTo.Visible = false;
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Enabled = false;
            this.fraSort.Location = new System.Drawing.Point(350, 224);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(300, 190);
            this.fraSort.TabIndex = 28;
            this.fraSort.Text = "Fields To Sort By";
            this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstSort.Enabled = false;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(260, 140);
            this.lstSort.TabIndex = 29;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Enabled = false;
            this.fraFields.Location = new System.Drawing.Point(30, 224);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(300, 190);
            this.fraFields.TabIndex = 31;
            this.fraFields.Text = "Fields To Display";
            this.ToolTip1.SetToolTip(this.fraFields, "Double Click to select all");
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(260, 140);
            this.lstFields.TabIndex = 32;
            this.ToolTip1.SetToolTip(this.lstFields, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.cboSavedReport);
            this.Frame2.Controls.Add(this.cmbReport);
            this.Frame2.Controls.Add(this.cmdAdd);
            this.Frame2.Enabled = false;
            this.Frame2.Location = new System.Drawing.Point(670, 224);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(321, 190);
            this.Frame2.TabIndex = 22;
            this.Frame2.Text = "Report";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(281, 40);
            this.cboSavedReport.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.cboSavedReport, null);
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Enabled = false;
            this.cmdAdd.Location = new System.Drawing.Point(20, 130);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(281, 40);
            this.cmdAdd.TabIndex = 23;
            this.cmdAdd.Text = "Add Custom Report to Library";
            this.ToolTip1.SetToolTip(this.cmdAdd, null);
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuLayout});
            this.MainMenu1.Name = null;
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = 0;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuFileSeperator2,
            this.mnuFilePrint,
            this.mnuPreview,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 2;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Visible = false;
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuPreview
            // 
            this.mnuPreview.Index = 3;
            this.mnuPreview.Name = "mnuPreview";
            this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPreview.Text = "Select Vendor";
            this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 4;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmVendorDetail
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1042, 620);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmVendorDetail";
            this.Text = "Vendor Detail";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmVendorDetail_Load);
            this.Activated += new System.EventHandler(this.frmVendorDetail_Activated);
            this.Resize += new System.EventHandler(this.frmVendorDetail_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVendorDetail_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendorDetail_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVendorSelect)).EndInit();
            this.fraVendorSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
            this.fraSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRunSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBackToSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultiple)).EndInit();
            this.fraMultiple.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsVendors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).EndInit();
            this.fraAccountRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccountCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccountProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdOK;
	}
}