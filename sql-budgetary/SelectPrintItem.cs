﻿using System;
using System.Collections.Generic;
using Wisej.Web;

namespace TWBD0000
{
    public partial class SelectPrintItem : Form
    {
        public event EventHandler PrintItem;

        public SelectPrintItem()
        {
            InitializeComponent();
        }

        public void AddItem(string item)
        {
            this.comboBox1.Items.Add(item);
        }

        public void ClearItems()
        {
            this.comboBox1.Items.Clear();
        }

        public int CurrentItem
        {
            get
            {
                return this.comboBox1.SelectedIndex;
            }
            set
            {
                this.comboBox1.SelectedIndex = value;
            }
        }

        public bool AllowMultiplePrints
        {
            set
            {
                this.comboBox1.Enabled = value;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            fcHeader1.Text = comboBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.PrintItem != null)
            {
                this.PrintItem(this, e);
            }
            if (this.comboBox1.SelectedIndex < this.comboBox1.Items.Count - 1)
            {
                this.comboBox1.SelectedIndex++;
            }
            else if (this.comboBox1.SelectedIndex == this.comboBox1.Items.Count - 1 && !this.comboBox1.Enabled)
            {
                this.Close();
            }
        }
    }
}
