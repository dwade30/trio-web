﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptAccountCloseout.
	/// </summary>
	partial class AccountCloseoutReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountCloseoutReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCloseoutAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCloseoutAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.srptSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCloseoutAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCloseoutAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDescription,
				this.fldBalance,
				this.fldCloseoutAccount,
				this.fldAccount
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 1.5625F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldDescription.Text = "Field3";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 4.1875F;
			// 
			// fldBalance
			// 
			this.fldBalance.Height = 0.1875F;
			this.fldBalance.Left = 5.78125F;
			this.fldBalance.Name = "fldBalance";
			this.fldBalance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldBalance.Text = "Field4";
			this.fldBalance.Top = 0F;
			this.fldBalance.Width = 0.96875F;
			// 
			// fldCloseoutAccount
			// 
			this.fldCloseoutAccount.Height = 0.1875F;
			this.fldCloseoutAccount.Left = 6.84375F;
			this.fldCloseoutAccount.Name = "fldCloseoutAccount";
			this.fldCloseoutAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldCloseoutAccount.Text = "Field5";
			this.fldCloseoutAccount.Top = 0F;
			this.fldCloseoutAccount.Width = 1.15625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldAccount.Text = "Field2";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.53125F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblDescription,
				this.lblBalance,
				this.Line1,
				this.lblAccount,
				this.lblCloseoutAccount
			});
			this.PageHeader.Height = 0.7083333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = " Account Closeout Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.78125F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1875F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 1.5625F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblDescription.Text = "Description";
			this.lblDescription.Top = 0.5F;
			this.lblDescription.Width = 4.1875F;
			// 
			// lblBalance
			// 
			this.lblBalance.Height = 0.1875F;
			this.lblBalance.HyperLink = null;
			this.lblBalance.Left = 5.78125F;
			this.lblBalance.Name = "lblBalance";
			this.lblBalance.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.lblBalance.Text = "Balance";
			this.lblBalance.Top = 0.5F;
			this.lblBalance.Width = 0.96875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 1.53125F;
			// 
			// lblCloseoutAccount
			// 
			this.lblCloseoutAccount.Height = 0.1875F;
			this.lblCloseoutAccount.HyperLink = null;
			this.lblCloseoutAccount.Left = 6.84375F;
			this.lblCloseoutAccount.Name = "lblCloseoutAccount";
			this.lblCloseoutAccount.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblCloseoutAccount.Text = "Closeout Account";
			this.lblCloseoutAccount.Top = 0.5F;
			this.lblCloseoutAccount.Width = 1.15625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.srptSummary
			});
			this.GroupFooter1.Height = 0.28125F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// srptSummary
			// 
			this.srptSummary.CloseBorder = false;
			this.srptSummary.Height = 0.09375F;
			this.srptSummary.Left = 0F;
			this.srptSummary.Name = "srptSummary";
			this.srptSummary.Report = null;
			this.srptSummary.Top = 0.1875F;
			this.srptSummary.Width = 8F;
			// 
			// AccountCloseoutReport
			// 
			this.MasterReport = false;
			this.Name = "AccountCloseoutReport";
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.AccountCloseoutReport_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCloseoutAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCloseoutAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBalance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCloseoutAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCloseoutAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSummary;
	}
}
