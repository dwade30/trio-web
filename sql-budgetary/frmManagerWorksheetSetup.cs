﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetWorksheetSetup.
	/// </summary>
	public partial class frmBudgetWorksheetSetup : BaseForm
	{
		public frmBudgetWorksheetSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAllAccounts.SelectedIndex = 0;
			cmbNo.SelectedIndex = 1;
			cmbRevenue.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBudgetWorksheetSetup InstancePtr
		{
			get
			{
				return (frmBudgetWorksheetSetup)Sys.GetInstance(typeof(frmBudgetWorksheetSetup));
			}
		}

		protected frmBudgetWorksheetSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         4/5/2002
		// This form will be used to setup any request worksheets
		// other than the initial request worksheet
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();
		public string strReportType = "";
		public bool blnPrint;
		public bool blnExpenseAccountsExist;
		public bool blnRevenueAccountsExist;

		private void cboSingleDept_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int counter = 0;
			if (cmbAllAccounts.SelectedIndex == 1)
			{
				if (cboSingleDept.SelectedIndex != -1)
				{
					cboSingleDivision.Clear();
					rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Division");
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.MoveLast();
						rs.MoveFirst();
						counter = 0;
						while (rs.EndOfFile() != true)
						{
							cboSingleDivision.AddItem(rs.Get_Fields_String("Division") + " - " + rs.Get_Fields_String("ShortDescription"));
							counter += 1;
							rs.MoveNext();
						}
					}
				}
				cboSingleDivision.Enabled = true;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			string temp = "";
			string tempAccountType = "";
			clsDRWrapper rsInfoExists = new clsDRWrapper();
			if (Strings.Trim(txtReportTitle.Text) == "")
			{
				MessageBox.Show("You must enter a report title before you may continue.", "Invalid Report Title", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (cmbAllAccounts.SelectedIndex == 3)
			{
				if (fecherFoundation.Strings.CompareString(Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), true) >= 0)
				{
					MessageBox.Show("Your Ending Department must be greater than your Beginning Department", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cboCurrentYear.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a current year before you may proceed", "Invalid Current Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboCurrentYear.Focus();
				return;
			}
			if (cboBudgetYear.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a budget year before you may proceed", "Invalid Budget Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				cboBudgetYear.Focus();
				return;
			}
			if (cmbRevenue.SelectedIndex == 1)
			{
				tempAccountType = "E";
			}
			else if (cmbRevenue.SelectedIndex == 2)
			{
				tempAccountType = "R";
			}
			else
			{
				tempAccountType = "T";
			}
			//rptRequestExpenseWorkSheet.InstancePtr.Hide();
			//rptRequestRevenueWorksheet.InstancePtr.Hide();
			rsInfoExists.OpenRecordset("SELECT * FROM Budget");
			if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				modBudgetaryMaster.InitializeBudgetTable();
			}
			//Application.DoEvents();
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnExpenseAccountsExist = true;
				}
				else
				{
					blnExpenseAccountsExist = false;
				}
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnRevenueAccountsExist = true;
				}
				else
				{
					blnRevenueAccountsExist = false;
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "' AND SecondAccountField = '" + Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnExpenseAccountsExist = true;
				}
				else
				{
					blnExpenseAccountsExist = false;
				}
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "' AND SecondAccountField = '" + Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnRevenueAccountsExist = true;
				}
				else
				{
					blnRevenueAccountsExist = false;
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "' AND FirstAccountField <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnExpenseAccountsExist = true;
				}
				else
				{
					blnExpenseAccountsExist = false;
				}
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField >= '" + Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "' AND FirstAccountField <= '" + Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnRevenueAccountsExist = true;
				}
				else
				{
					blnRevenueAccountsExist = false;
				}
			}
			else
			{
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'E' AND FirstAccountField = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnExpenseAccountsExist = true;
				}
				else
				{
					blnExpenseAccountsExist = false;
				}
				rsInfoExists.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = 'R' AND FirstAccountField = '" + Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))) + "'");
				if (rsInfoExists.EndOfFile() != true && rsInfoExists.BeginningOfFile() != true)
				{
					blnRevenueAccountsExist = true;
				}
				else
				{
					blnRevenueAccountsExist = false;
				}
			}
			if (tempAccountType == "E" || tempAccountType == "P")
			{
				if (blnExpenseAccountsExist)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else if (tempAccountType == "R" || tempAccountType == "V")
			{
				if (blnRevenueAccountsExist)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else
			{
				if (blnRevenueAccountsExist || blnExpenseAccountsExist)
				{
					// do nothing
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (tempAccountType == "E")
			{
				if (cmbAllAccounts.SelectedIndex == 0)
				{
					temp = "A";
					rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp);
				}
				else if (cmbAllAccounts.SelectedIndex == 1)
				{
					temp = "V";
					rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), string.Empty, Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))));
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					temp = "R";
					rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
				else
				{
					temp = "D";
					rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
			}
			else if (tempAccountType == "R")
			{
				if (cmbAllAccounts.SelectedIndex == 0)
				{
					temp = "A";
					rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp);
				}
				else if (cmbAllAccounts.SelectedIndex == 1)
				{
					temp = "V";
					rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), null, Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))));
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					temp = "R";
					rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
				else
				{
					temp = "D";
					rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
				}
			}
			else if (tempAccountType == "T")
			{
				if (blnExpenseAccountsExist)
				{
					if (cmbAllAccounts.SelectedIndex == 0)
					{
						temp = "A";
						rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp);
					}
					else if (cmbAllAccounts.SelectedIndex == 1)
					{
						temp = "V";
						rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), string.Empty, Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))));
					}
					else if (cmbAllAccounts.SelectedIndex == 3)
					{
						temp = "R";
						rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
					}
					else
					{
						temp = "D";
						rptRequestExpenseWorkSheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), tempAccountType, strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkExpTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
					}
				}
				else
				{
					if (cmbAllAccounts.SelectedIndex == 0)
					{
						temp = "A";
						rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), "R", strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp);
					}
					else if (cmbAllAccounts.SelectedIndex == 1)
					{
						temp = "V";
						rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), "R", strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), string.Empty, Strings.Left(cboSingleDivision.Text, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))));
					}
					else if (cmbAllAccounts.SelectedIndex == 3)
					{
						temp = "R";
						rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), "R", strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboBeginningDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))), Strings.Left(cboEndingDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
					}
					else
					{
						temp = "D";
						rptRequestRevenueWorksheet.InstancePtr.Init(chkOnlyShowChangedAmounts.CheckState == CheckState.Checked, chkIncludeBudgetAdjustments.CheckState == CheckState.Checked, Strings.Trim(txtReportTitle.Text), "R", strReportType, cboBudgetYear.Text == cboCurrentYear.Text, FCConvert.ToInt32(cboBudgetYear.Text), FCConvert.ToInt32(cboCurrentYear.Text), cmbNo.SelectedIndex == 1, chkDeptTotal.CheckState == CheckState.Checked, chkDivTotal.CheckState == CheckState.Checked, chkDepartment.CheckState == CheckState.Checked, chkDivision.CheckState == CheckState.Checked, temp, Strings.Left(cboSingleDept.Text, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2))));
					}
				}
			}
			//Application.DoEvents();
			//FC:FINAL:AM:#i446 - don't close the form
			//Close();
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void frmBudgetWorksheetSetup_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			//cboBeginningDept.Left = cboBeginningDept.Left + cboBeginningDept.Width - (3 * 115 + 350);
			//cboSingleDept.Left = FCConvert.ToInt32(cboSingleDept.Left + (cboSingleDept.Width - (3 * 115 + 350)) - (0.5 * (cboSingleDept.Width - (3 * 115 + 350))));
			//cboSingleDivision.Left = cboSingleDept.Left;
			//cboBeginningDept.Width = 3 * 115 + 350;
			//cboEndingDept.Width = 3 * 115 + 350;
			//cboSingleDept.Width = 3 * 115 + 350;
			//cboSingleDivision.Width = 4 * 115 + 350;
			for (counter = DateTime.Today.Year - 4; counter <= DateTime.Today.Year + 4; counter++)
			{
				cboCurrentYear.AddItem(counter.ToString());
				cboBudgetYear.AddItem(counter.ToString());
			}
			cboCurrentYear.SelectedIndex = 4;
			cboBudgetYear.SelectedIndex = 5;
			if (strReportType == "I")
			{
				fraBudgetYear.Visible = false;
				fraCurrentYear.Visible = false;
			}
			else
			{
				fraBudgetYear.Visible = true;
				fraCurrentYear.Visible = true;
			}
			if (strReportType == "I")
			{
				txtReportTitle.Text = "Initial Request Worksheet";
			}
			else if (strReportType == "M")
			{
				txtReportTitle.Text = "Manager Request Worksheet";
			}
			else if (strReportType == "C")
			{
				txtReportTitle.Text = "Committee Request Worksheet";
			}
			else if (strReportType == "E")
			{
				txtReportTitle.Text = "Elected Request Worksheet";
			}
			else
			{
				txtReportTitle.Text = "Approved Amounts Worksheet";
			}
			this.Refresh();
		}

		private void frmBudgetWorksheetSetup_Load(object sender, System.EventArgs e)
		{
			FillDepartmentCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBudgetWorksheetSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			blnPrint = false;
			cmdProcess_Click();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			blnPrint = true;
			cmdProcess_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkExpTotal.Enabled = true;
		}

		private void optDivision_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningDept.Visible = false;
			cboEndingDept.Visible = false;
			cboBeginningDept.SelectedIndex = -1;
			cboEndingDept.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleDept.Visible = true;
			cboSingleDivision.Visible = true;
			cboSingleDivision.Enabled = false;
			lblDepartment.Text = "Department";
			lblDivision.Text = "Division";
			lblDepartment.Visible = true;
			lblDivision.Visible = true;
		}

		private void optAllAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningDept.Visible = false;
			cboEndingDept.Visible = false;
			cboBeginningDept.SelectedIndex = -1;
			cboEndingDept.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleDept.Visible = false;
			cboSingleDept.SelectedIndex = -1;
			cboSingleDivision.Visible = false;
			cboSingleDivision.SelectedIndex = -1;
			lblDepartment.Visible = false;
			lblDivision.Visible = false;
		}

		private void optExpense_CheckedChanged(object sender, System.EventArgs e)
		{
			chkExpTotal.Enabled = true;
		}

		private void optRevenue_CheckedChanged(object sender, System.EventArgs e)
		{
			chkExpTotal.Enabled = false;
		}

		private void optSingleDepartment_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningDept.Visible = false;
			cboEndingDept.Visible = false;
			cboBeginningDept.SelectedIndex = -1;
			cboEndingDept.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleDept.Visible = true;
			cboSingleDept.SelectedIndex = -1;
			cboSingleDivision.SelectedIndex = -1;
			cboSingleDivision.Visible = false;
			lblDepartment.Visible = false;
			lblDivision.Visible = false;
		}

		private void optDepartmentRange_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningDept.Visible = true;
			cboEndingDept.Visible = true;
			lblTo[2].Visible = true;
			cboSingleDept.Visible = false;
			cboSingleDept.SelectedIndex = -1;
			cboSingleDivision.SelectedIndex = -1;
			cboSingleDivision.Visible = false;
			lblDepartment.Visible = false;
			lblDivision.Visible = false;
		}

		private void cboBeginningDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboEndingDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleDept_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDept.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboSingleDivision_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleDivision.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void FillDepartmentCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			int counter = 0;
			cboBeginningDept.Clear();
			cboEndingDept.Clear();
			cboSingleDept.Clear();
			if (!modAccountTitle.Statics.ExpDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Department");
			}
			else
			{
				//optDivision.Enabled = false;
				if (cmbAllAccounts.Items.Contains("Single Division"))
				{
					cmbAllAccounts.Items.Remove("Single Division");
				}
				rs.OpenRecordset("SELECT * FROM DeptDivTitles ORDER BY Department");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleDept.AddItem(rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
		}

		private void cmbAllAccounts_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				optAllAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				optDivision_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				optSingleDepartment_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				optDepartmentRange_CheckedChanged(sender, e);
			}
		}

		private void cmbRevenue_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbRevenue.SelectedIndex == 2)
			{
				optRevenue_CheckedChanged(sender, e);
			}
			else if (cmbRevenue.SelectedIndex == 1)
			{
				optExpense_CheckedChanged(sender, e);
			}
			else if (cmbRevenue.SelectedIndex == 0)
			{
				optAll_CheckedChanged(sender, e);
			}
		}
		//
		// Private Sub optTown_Click()
		// optAllAccounts = True
		// optExpense = True
		// FillDepartmentCombo
		// optDivision.Text = "Single Division"
		// optSingleDepartment.Text = "Single Department"
		// optDepartmentRange.Text = "Department Range"
		//
		// chkFund = vbUnchecked
		// chkProgram = vbUnchecked
		// chkFunction = vbUnchecked
		// chkFundTotal = vbUnchecked
		// chkProgramTotal = vbUnchecked
		// chkFunctionTotal = vbUnchecked
		// chkObjectTotal = vbUnchecked
		// fraTownPageBreaks.Visible = True
		// fraTownTotals.Visible = True
		// End Sub
	}
}
