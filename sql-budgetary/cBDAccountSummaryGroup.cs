﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cBDAccountSummaryGroup
	{
		//=========================================================
		private cBDAccount theAccount;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection accountDetails = new cGenericCollection();
		private cGenericCollection accountDetails_AutoInitialized;

		private cGenericCollection accountDetails
		{
			get
			{
				if (accountDetails_AutoInitialized == null)
				{
					accountDetails_AutoInitialized = new cGenericCollection();
				}
				return accountDetails_AutoInitialized;
			}
			set
			{
				accountDetails_AutoInitialized = value;
			}
		}

		public cGenericCollection Details
		{
			set
			{
				accountDetails = value;
			}
			get
			{
				cGenericCollection Details = null;
				Details = accountDetails;
				return Details;
			}
		}

		public cBDAccount Account
		{
			set
			{
				theAccount = value;
			}
			get
			{
				cBDAccount Account = null;
				Account = theAccount;
				return Account;
			}
		}
	}
}
