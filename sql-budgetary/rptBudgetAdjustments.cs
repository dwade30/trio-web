﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptBudgetAdjustments.
	/// </summary>
	public partial class rptBudgetAdjustments : BaseSectionReport
	{
		public static rptBudgetAdjustments InstancePtr
		{
			get
			{
				return (rptBudgetAdjustments)Sys.GetInstance(typeof(rptBudgetAdjustments));
			}
		}

		protected rptBudgetAdjustments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBudgetAdjustments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		string strAccountType = "";
		string strAccountRange = "";
		string strLowDepartment = "";
		string strHighDepartment = "";
		// vbPorter upgrade warning: curDebitsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curDebitsTotal;
		// vbPorter upgrade warning: curCreditsTotal As Decimal	OnWrite(short, Decimal)
		Decimal curCreditsTotal;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;

		public rptBudgetAdjustments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Budget / Beg Bal Adjustments";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strTypeSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			curDebitsTotal = 0;
			curCreditsTotal = 0;
			blnFirstRecord = true;
			if (strAccountType == "A")
			{
				strTypeSQL = "(left(Account, 1) = 'E' or left(Account, 1) = 'R')";
				lblTitle1.Text = "All Accounts";
			}
			else if (strAccountType == "E")
			{
				strTypeSQL = "left(Account, 1) = 'E'";
				lblTitle1.Text = "Town Expense Accounts";
			}
			else if (strAccountType == "R")
			{
				strTypeSQL = "left(Account, 1) = 'R'";
				lblTitle1.Text = "Town Revenue Accounts";
			}
			else if (strAccountType == "G")
			{
				strTypeSQL = "left(Account, 1) = 'G'";
				lblTitle1.Text = "Town General Ledger Accounts";
			}
			if (strAccountType == "A")
			{
				rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB = 'B' ORDER BY JournalEntriesDate, JournalNumber");
				lblTitle2.Text = "All Departments/Funds";
			}
			else if (strAccountType == "R" || strAccountType == "E")
			{
				if (strAccountRange == "A")
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "All Departments";
				}
				else if (strAccountRange == "R")
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + strLowDepartment + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + strHighDepartment + "' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "Departments " + strLowDepartment + " To " + strHighDepartment;
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") >= '" + strLowDepartment + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2))) + ") <= '" + strHighDepartment + "' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "Department " + strLowDepartment;
				}
			}
			else if (strAccountType == "G")
			{
				if (strAccountRange == "A")
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "All Funds";
				}
				else if (strAccountRange == "R")
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") >= '" + strLowDepartment + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") <= '" + strHighDepartment + "' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "Funds " + strLowDepartment + " To " + strHighDepartment;
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") >= '" + strLowDepartment + "' AND substring(Account, 3, " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ") <= '" + strHighDepartment + "' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "Fund " + strLowDepartment;
				}
			}
			else
			{
				if (strAccountRange == "A")
				{
					rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE " + strTypeSQL + " AND RCB = 'B' ORDER BY JournalEntriesDate, JournalNumber");
					lblTitle2.Text = "All Funds";
				}
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmBudgetAdjustments.InstancePtr.Dispose();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsInfo.Get_Fields("Account")) + "  " + modAccountTitle.ReturnAccountDescription(FCConvert.ToString(rsInfo.Get_Fields("Account")));
			fldDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yyyy");
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			fldJournal.Text = modValidateAccount.GetFormat_6(rsInfo.Get_Fields("JournalNumber"), 4);
			fldDescription.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
			if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) >= 0)
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldDebits.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
				fldCredits.Text = "";
				//FC:FINAL:MSH - can't implicitly convert to decimal (same with issue #742)
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curDebitsTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				fldCredits.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
				fldDebits.Text = "";
				//FC:FINAL:MSH - can't implicitly convert to decimal (same with issue #742)
				// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
				curCreditsTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount") * -1);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldDebitTotal.Text = Strings.Format(curDebitsTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditsTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(string strAcctType, string strAcctRange, string strStartDept = "", string strEndDept = "")
		{
			strAccountType = strAcctType;
			strAccountRange = strAcctRange;
			strLowDepartment = strStartDept;
			strHighDepartment = strEndDept;
			if (frmBudgetAdjustments.InstancePtr.blnPrint)
			{
				rptBudgetAdjustments.InstancePtr.PrintReport();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void rptBudgetAdjustments_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBudgetAdjustments.Caption	= "Budget / Beg Bal Adjustments";
			//rptBudgetAdjustments.Icon	= "rptBudgetAdjustments.dsx":0000";
			//rptBudgetAdjustments.Left	= 0;
			//rptBudgetAdjustments.Top	= 0;
			//rptBudgetAdjustments.Width	= 11880;
			//rptBudgetAdjustments.Height	= 8595;
			//rptBudgetAdjustments.StartUpPosition	= 3;
			//rptBudgetAdjustments.SectionData	= "rptBudgetAdjustments.dsx":058A;
			//End Unmaped Properties
		}

		private void rptBudgetAdjustments_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
