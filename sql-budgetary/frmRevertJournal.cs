﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevertJournal.
	/// </summary>
	public partial class frmRevertJournal : BaseForm
	{
		public frmRevertJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRevertJournal InstancePtr
		{
			get
			{
				return (frmRevertJournal)Sys.GetInstance(typeof(frmRevertJournal));
			}
		}

		protected frmRevertJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         6/13/02
		// This form will be used to delete any unposted journals
		// the user might need to get rid of
		// ********************************************************
		clsDRWrapper rsDelete = new clsDRWrapper();

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			string strType = "";
			clsDRWrapper rsDetail = new clsDRWrapper();
			int intCount;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			string strJournal;
			clsDRWrapper rsCheckOther = new clsDRWrapper();
			clsDRWrapper rsCheckRec = new clsDRWrapper();
			int intStart;
			int intLength;
			if (cboJournals.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a journal before you may proceed", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strJournal = Strings.Left(cboJournals.Items[cboJournals.SelectedIndex].ToString(), 4);
			ans = MessageBox.Show("You are about to revert Journal " + strJournal + " to Entered status.  Do you wish to continue?", "Revert Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				return;
			}
			modBudgetaryMaster.WriteAuditRecord_8("Reverted Journal " + strJournal, "Revert Journal");
			rsDelete.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			rsCheckOther.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Period = " + rsDelete.Get_Fields("Period") + " AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
			if (rsCheckOther.EndOfFile() != true && rsCheckOther.BeginningOfFile() != true)
			{
				rsCheckOther.Delete();
				rsCheckOther.Update();
			}
			rsDelete.Edit();
			rsDelete.Set_Fields("Status", "E");
			rsDelete.Set_Fields("CheckDate", null);
			rsDelete.Set_Fields("StatusChangeDate", DateTime.Today);
			rsDelete.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
			rsDelete.Set_Fields("StatusChangeOpID", modBudgetaryAccounting.Statics.User);
			rsDelete.Update();
			intCount = 0;
			rsDelete.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
			do
			{
				rsDelete.Edit();
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				rsDetail.Execute("DELETE FROM TempCheckFile WHERE substring(Journals, 58, 15) LIKE '%" + rsDelete.Get_Fields("JournalNumber") + "%'", "Budgetary");
				// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsDelete.Get_Fields_String("Status")) != "V" && FCConvert.ToString(rsDelete.Get_Fields_String("Status")) != "E" && Strings.Trim(FCConvert.ToString(rsDelete.Get_Fields("CheckNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					rsCheckRec.Execute("DELETE FROM CheckRecMaster WHERE Type = '1' and CheckNumber = " + FCConvert.ToString(Conversion.Val(rsDelete.Get_Fields("CheckNumber"))) + " AND CheckDate = '" + rsDelete.Get_Fields_DateTime("CheckDate") + "'", "Budgetary");
				}
				rsDelete.Set_Fields("Status", "E");
				if (rsDelete.Get_Fields_Boolean("PrepaidCheck") == false)
				{
					rsDelete.Set_Fields("CheckNumber", "");
				}
				rsDelete.Set_Fields("Warrant", "");
				rsDelete.Set_Fields("CheckDate", null);
				rsDelete.Update();
				intCount += 1;
				rsDelete.MoveNext();
			}
			while (rsDelete.EndOfFile() != true);
			modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
			MessageBox.Show("Revert Completed Successfully" + "\r\n" + "\r\n" + "     " + FCConvert.ToString(intCount) + " Entries Reverted", "Journal Reverted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			SetCombo();
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdDelete, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmRevertJournal_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			SetCombo();
			this.Refresh();
		}

		private void cboJournals_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournals.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void frmRevertJournal_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRevertJournal.FillStyle	= 0;
			//frmRevertJournal.ScaleWidth	= 5880;
			//frmRevertJournal.ScaleHeight	= 4320;
			//frmRevertJournal.LinkTopic	= "Form2";
			//frmRevertJournal.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmRevertJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// MDIParent.InstancePtr.Focus();
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void SetCombo()
		{
			clsDRWrapper rsDetail = new clsDRWrapper();
			cboJournals.Clear();
			rsDelete.OpenRecordset("SELECT * FROM JournalMaster WHERE Type = 'AP' AND (Status <> 'E' and Status <> 'D' and Status <> 'P') ORDER BY JournalNumber");
			if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboJournals.AddItem(Strings.Format(rsDelete.Get_Fields("JournalNumber"), "0000") + "  -  " + rsDelete.Get_Fields_String("Description"));
					rsDelete.MoveNext();
				}
				while (rsDelete.EndOfFile() != true);
				cboJournals.SelectedIndex = 0;
			}
		}

		private void SetCustomFormColors()
		{
			Label3.ForeColor = Color.Red;
		}
	}
}
