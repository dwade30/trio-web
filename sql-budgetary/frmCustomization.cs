﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Budgetary.Enums;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomization.
	/// </summary>
	public partial class frmCustomization : BaseForm
	{
		public frmCustomization()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			//FC:FINAL:BBE - default selection - All
			this.cmbOption.SelectedIndex = 0;
            //FC:FINAL:SBE - #413 - set combobox ItemData as in the original application
            //this.cboCheckFormat.Items.AddRange(new object[] {
            //"1 - Standard",
            //"2 - Laser",
            //"3 - Custom"});
            //this.cboCheckFormat.AddItem("1 - Standard");
            //this.cboCheckFormat.ItemData(this.cboCheckFormat.NewIndex, 1);
            this.cboCheckFormat.AddItem("1 - Laser");
			this.cboCheckFormat.ItemData(this.cboCheckFormat.NewIndex, 2);
			this.cboCheckFormat.AddItem("2 - Custom");
			this.cboCheckFormat.ItemData(this.cboCheckFormat.NewIndex, 4);

            txt1099Adjustment.AllowOnlyNumericInput(true);
            txtCheckAdjustment.AllowOnlyNumericInput(true);
            txtReportAdjustment.AllowOnlyNumericInput(true);
            txtLabelAdjustment.AllowOnlyNumericInput(true);
            txtNextPO.AllowOnlyNumericInput();
        }
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		bool blnUnload;
		int NumberCol;
		int SelectCol;
		int DescriptionCol;
		int CashFundCol;
		int OverrideAPCol;
		int OverrideCRCol;
		int OverridePYCol;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingsController setCont = new cSettingsController();
		private cSettingsController setCont_AutoInitialized;

		private cSettingsController setCont
		{
			get
			{
				if (setCont_AutoInitialized == null)
				{
					setCont_AutoInitialized = new cSettingsController();
				}
				return setCont_AutoInitialized;
			}
			set
			{
				setCont_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cSettingUtility setUtil = new cSettingUtility();
		private cSettingUtility setUtil_AutoInitialized;

		private cSettingUtility setUtil
		{
			get
			{
				if (setUtil_AutoInitialized == null)
				{
					setUtil_AutoInitialized = new cSettingUtility();
				}
				return setUtil_AutoInitialized;
			}
			set
			{
				setUtil_AutoInitialized = value;
			}
		}
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDSettings bdSettings = new cBDSettings();
		private cBDSettings bdSettings_AutoInitialized;

		private cBDSettings bdSettings
		{
			get
			{
				if (bdSettings_AutoInitialized == null)
				{
					bdSettings_AutoInitialized = new cBDSettings();
				}
				return bdSettings_AutoInitialized;
			}
			set
			{
				bdSettings_AutoInitialized = value;
			}
		}

		public cBDSettings Settings
		{
			set
			{
				bdSettings = value;
			}
		}

		private void cboBalanceValidation_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboBalanceValidation.SelectedIndex == 2)
			{
				cmdChooseAccounts.Visible = false;
			}
			else
			{
				cmdChooseAccounts.Visible = true;
			}
		}

		private void chkAPReference_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAPReference.CheckState == CheckState.Checked)
			{
				chkSameVendor.Enabled = true;
			}
			else
			{
				chkSameVendor.CheckState = CheckState.Unchecked;
				chkSameVendor.Enabled = false;
			}
		}

		private void chkAutoIncrementPO_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAutoIncrementPO.CheckState == CheckState.Checked)
			{
				fraAutoIncrementPO.Enabled = true;
				lblNextPO.Enabled = true;
				txtNextPO.Enabled = true;
			}
			else
			{
				fraAutoIncrementPO.Enabled = false;
				lblNextPO.Enabled = false;
				txtNextPO.Enabled = false;
			}
		}

		private void chkDue_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDue.CheckState == CheckState.Checked)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					chkDue.CheckState = CheckState.Unchecked;
					MessageBox.Show("You may not use Due To/From accounts because you are not using a suffix in your General Ledger Accounts", "Unable to use Due To/From", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					cmbOption.SelectedIndex = 0;
					//FC:FINAL:CHN: Return missing adding item to ComboBox.
					if (!cmbOption.Items.Contains("Due To / From"))
					{
						cmbOption.AddItem("Due To / From", cmbOption.Items.Count - 1);
					}
				}
			}
			else
			{
				//Option5.Enabled = false;
				if (cmbOption.Items.Contains("Due To / From"))
				{
					cmbOption.Items.Remove("Due To / From");
				}
			}
		}

		private void cmdChooseAccounts_Click(object sender, System.EventArgs e)
		{
			//FC: FINAL: RPU: Center the form to the ClientArea before show it
			frmSelectAccounts.InstancePtr.CenterToContainer(this.ClientArea);
			frmSelectAccounts.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
		}

		private void cmdPrintACHPrenoteFile_Click(object sender, System.EventArgs e)
		{
			frmCreateACHFiles.InstancePtr.Init(true);
		}

		private void cmdResetSave_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.UnlockJournal();
			modBudgetaryMaster.UnlockWarrant();
			modBudgetaryMaster.UnlockPO();
			MessageBox.Show("Locks cleared!", "Locks Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void cmdTestCheck_Click(object sender, System.EventArgs e)
		{
			//rptNewSampleCheck.InstancePtr.ShowDialog();
			frmReportViewer.InstancePtr.Init(rptNewSampleCheck.InstancePtr, "", 1, this.Modal);
		}

		private void frmCustomization_Activated(object sender, System.EventArgs e)
		{
			int counter;
			string temp = "";
			clsDRWrapper rsJournalNumber = new clsDRWrapper();
			clsDRWrapper rsBankNumbers = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			// If UCase(MuniName) <> "TRIOVILLE" Then
			// Option4.Enabled = False
			// End If
			string strTemp;
			int intTemp;
			int x;
			intTemp = 0;
			strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
			if (Strings.LCase(strTemp) == "no")
			{
				intTemp = 0;
			}
			else if (Strings.LCase(strTemp) == "bank")
			{
				intTemp = 1;
			}
			else if (Strings.LCase(strTemp) == "check type")
			{
				intTemp = 2;
			}
			for (x = 0; x <= cmbTrackLastCheckNumber.Items.Count - 1; x++)
			{
				if (cmbTrackLastCheckNumber.ItemData(x) == intTemp)
				{
					cmbTrackLastCheckNumber.SelectedIndex = x;
					break;
				}
			}
			// x
			rsJournalNumber.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
			if (rsJournalNumber.EndOfFile() != true && rsJournalNumber.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				txtJournal.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(FCConvert.ToInt32(rsJournalNumber.Get_Fields("JournalNumber")) + 1), 4);
			}
			else
			{
				txtJournal.Text = "0001";
			}
			rsJournalNumber.OpenRecordset("SELECT TOP 1 * FROM WarrantMaster ORDER BY WarrantNumber DESC");
			if (rsJournalNumber.EndOfFile() != true && rsJournalNumber.BeginningOfFile() != true)
			{
				txtWarrant.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(FCConvert.ToInt32(rsJournalNumber.Get_Fields_Int32("WarrantNumber")) + 1), 4);
			}
			else
			{
				txtWarrant.Text = "0001";
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("ValidAccountUse")) != 0 && Conversion.Val(modBudgetaryAccounting.GetBDVariable("ValidAccountUse")) <= 3)
			{
				cboValidAccount.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("ValidAccountUse")) - 1);
			}
			else
			{
				cboValidAccount.SelectedIndex = 2;
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) != 0 && Conversion.Val(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) <= 3)
			{
				cboBalanceValidation.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("BalanceValidation")) - 1);
			}
			else
			{
				cboBalanceValidation.SelectedIndex = 2;
			}
			if (cboBalanceValidation.SelectedIndex == 2)
			{
				cmdChooseAccounts.Visible = false;
			}
			else
			{
				cmdChooseAccounts.Visible = true;
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) != 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) - 1 == 0 ||
                    Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) - 1 == 1)
				{
					cboCheckFormat.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) - 2);
					btnFileCustomCheck.Enabled = false;
				}
				else if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("CheckFormat")) - 1 == 2)
				{
					MessageBox.Show("Check format no longer supported.  Please call TRIO.", "Invalid Check Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cboCheckFormat.SelectedIndex = 0;
					btnFileCustomCheck.Enabled = false;
				}
				else
				{
					cboCheckFormat.SelectedIndex = 1;
					btnFileCustomCheck.Enabled = true;
				}
			}
			else
			{
				cboCheckFormat.SelectedIndex = 0;
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("PreviewPrint")) != 0)
			{
				cboPreviewAccount.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("PreviewPrint")) - 1);
			}
			else
			{
				cboPreviewAccount.SelectedIndex = 4;
			}
			cboPreviewSchoolAccount.SelectedIndex = 2;
			cboEncCarryForward.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("EncCarryForward")));
			txt1099Adjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENT")));
			txt1099AdjustmentHorizontal.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("1099LASERADJUSTMENTHORIZONTAL")));
			txtCheckAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("CHECKLASERADJUSTMENT")));
			txtLabelAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("LABELLASERADJUSTMENT")));
			txtReportAdjustment.Text = FCConvert.ToString(Conversion.Val(modRegistry.GetRegistryKey("REPORTADJUSTMENT")));			
			rsBankNumbers.OpenRecordset("SELECT * FROM Banks ORDER BY BankNumber");
			if (rsBankNumbers.EndOfFile() != true && rsBankNumbers.BeginningOfFile() != true)
			{
				do
				{
					if (Strings.Trim(FCConvert.ToString(rsBankNumbers.Get_Fields_String("Name"))) == "")
					{
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboAPBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + "NONE");
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboPayrollBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + "NONE");
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboCRBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + "NONE");
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboAPBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + rsBankNumbers.Get_Fields_String("Name"));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboPayrollBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + rsBankNumbers.Get_Fields_String("Name"));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						cboCRBank.AddItem(rsBankNumbers.Get_Fields("BankNumber") + " - " + rsBankNumbers.Get_Fields_String("Name"));
					}
					rsBankNumbers.MoveNext();
				}
				while (rsBankNumbers.EndOfFile() != true);
			}
			if (cboAPBank.Items.Count > 1)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("APBank")) != 0)
				{
					cboAPBank.SelectedIndex = FCConvert.ToInt32(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBankVariable("APBank"))));
				}
				else
				{
					cboAPBank.SelectedIndex = 0;
				}
			}
			else
			{
				cboAPBank.SelectedIndex = 0;
			}
			if (cboCRBank.Items.Count > 1)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("CRBank")) != 0)
				{
					cboCRBank.SelectedIndex = FCConvert.ToInt32(Strings.Trim(FCConvert.ToString(modBudgetaryAccounting.GetBankVariable("CRBank"))));
				}
				else
				{
					cboCRBank.SelectedIndex = 0;
				}
			}
			else
			{
				cboCRBank.SelectedIndex = 0;
			}
			if (cboPayrollBank.Items.Count > 1)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBankVariable("PayrollBank")) != 0)
				{
					cboPayrollBank.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.GetBankVariable("PayrollBank"));
				}
				else
				{
					cboPayrollBank.SelectedIndex = 0;
				}
			}
			else
			{
				cboPayrollBank.SelectedIndex = 0;
			}
			if (cboAPSequence.Items.Count > 0)
			{
				if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) != 0)
				{
					cboAPSequence.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("APSeq")) - 1);
				}
				else
				{
					cboAPSequence.SelectedIndex = 0;
				}
			}
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("WarrantPrevSigLine")) == "Y")
			{
				chkWarrantSignature.CheckState = CheckState.Checked;
			}
			else
			{
				chkWarrantSignature.CheckState = CheckState.Unchecked;
			}
			
			txtReturnAddress1.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ReturnAddress1"));
			txtReturnAddress2.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ReturnAddress2"));
			txtReturnAddress3.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ReturnAddress3"));
			txtReturnAddress4.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ReturnAddress4"));
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("BalancedACHFile")) == true)
			{
				chkBalanceACHFile.CheckState = CheckState.Checked;
			}
			else
			{
				chkBalanceACHFile.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("ACHFederalIDPrefix")) == 1)
			{
				cboFederalIDPrefix.SelectedIndex = 1;
			}
			else if (FCConvert.ToInt32(modBudgetaryAccounting.GetBDVariable("ACHFederalIDPrefix")) == 9)
			{
				cboFederalIDPrefix.SelectedIndex = 2;
			}
			else
			{
				cboFederalIDPrefix.SelectedIndex = 0;
			}
			txtACHFederalID.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ACHFederalID"));
			txtACHTownName.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ACHTownName"));
			txtImmediateDestinationName.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateDestinationName"));
			txtImmediateDestinationRoutingNumber.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateDestinationRoutingNumber"));
			txtImmediateOriginName.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateOriginName"));
			txtImmediateOriginODFINumber.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateOriginODFINumber"));
			txtImmediateOriginRoutingNumber.Text = FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ImmediateOriginRoutingNumber"));
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("WarrantPrevDeptSummary")))
			{
				chkWarrantDeptSummary.CheckState = CheckState.Checked;
			}
			else
			{
				chkWarrantDeptSummary.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AllowManualWarrantNumbers")))
			{
				chkManualWarrantNumbers.CheckState = CheckState.Checked;
			}
			else
			{
				chkManualWarrantNumbers.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("EncByDept")))
			{
				chkEncumbranceByDepartment.CheckState = CheckState.Checked;
			}
			else
			{
				chkEncumbranceByDepartment.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("RecapOrderByAccounts")))
			{
				chkRecapOrderByAccount.CheckState = CheckState.Checked;
			}
			else
			{
				chkRecapOrderByAccount.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutomaticInterest")))
			{
				chkAutomaticInterestEntries.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutomaticInterestEntries.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("LandscapeWarrant")))
			{
				chkWarrantLandscape.CheckState = CheckState.Checked;
			}
			else
			{
				chkWarrantLandscape.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("PrintSignature")))
			{
				chkSignature.CheckState = CheckState.Checked;
				// cmdChooseFile.Visible = True
			}
			else
			{
				chkSignature.CheckState = CheckState.Unchecked;
				// cmdChooseFile.Visible = False
			}
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("ShadeReports")) == "Y")
			{
				chkShade.CheckState = CheckState.Checked;
			}
			else
			{
				chkShade.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("Due")) == "Y")
			{
				chkDue.CheckState = CheckState.Checked;
			}
			else
			{
				chkDue.CheckState = CheckState.Unchecked;
			}
			SetupFundGrid();
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("ValidAccountBudgetProcess")) == true)
			{
				chkBudgetAccounts.CheckState = CheckState.Checked;
			}
			else
			{
				chkBudgetAccounts.CheckState = CheckState.Unchecked;
			}
			
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutonumberPO")) == true)
			{
				chkAutoIncrementPO.CheckState = CheckState.Checked;
				txtNextPO.Text = FCConvert.ToString(modBudgetaryMaster.GetNextPO());
			}
			else
			{
				chkAutoIncrementPO.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("CheckRef")) == "N")
			{
				chkAPReference.CheckState = CheckState.Unchecked;
				chkSameVendor.CheckState = CheckState.Unchecked;
			}
			else
			{
				chkAPReference.CheckState = CheckState.Checked;
				if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("SameVendorOnly")))
				{
					chkSameVendor.CheckState = CheckState.Checked;
				}
				else
				{
					chkSameVendor.CheckState = CheckState.Unchecked;
				}
			}
			if (Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) != 0)
			{
				cboFirstMonth.SelectedIndex = FCConvert.ToInt32(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart")) - 1);
			}
			else
			{
				cboFirstMonth.SelectedIndex = 0;
			}
			if (FCConvert.ToString(modBudgetaryAccounting.GetBDVariable("UseProjectNumber")) == "Y")
			{
				chkProjects.CheckState = CheckState.Checked;
			}
			else
			{
				chkProjects.CheckState = CheckState.Unchecked;
			}
            lblTownAccountOption.Enabled = true;
            cboPreviewAccount.Enabled = true;
			lblSchoolAccountOption.Enabled = false;
			cboPreviewSchoolAccount.Enabled = false;
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostAP")) == true)
			{
				chkAutoPostAP.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostAP.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostGJ")) == true)
			{
				chkAutoPostGJ.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostGJ.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCR")) == true)
			{
				chkAutoPostCR.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCR.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCM")) == true)
			{
				chkAutoPostCM.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCM.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostEN")) == true)
			{
				chkAutoPostEN.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostEN.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostAPCorr")) == true)
			{
				chkAutoPostAPCorr.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostAPCorr.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCD")) == true)
			{
				chkAutoPostCD.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCD.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostReversingJournals")) == true)
			{
				chkAutoPostReversingJournals.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostReversingJournals.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostBudgetTransfer")) == true)
			{
				chkAutoPostBudgetTrnsfer.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostBudgetTrnsfer.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCreateOpeningAdjustments")) == true)
			{
				chkAutoPostCreateOpeningAdjustments.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCreateOpeningAdjustments.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostARBills")) == true)
			{
				chkAutoPostARBills.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostARBills.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCRDailyAudit")) == true)
			{
				chkAutoPostCRDailyAudit.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCRDailyAudit.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostFADepreciation")) == true)
			{
				chkAutoPostFADepreciation.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostFADepreciation.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostMARR")) == true)
			{
				chkAutoPostMVRapidRenewal.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostMVRapidRenewal.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostPYProcesses")) == true)
			{
				chkAutoPostPYProcesses.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostPYProcesses.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCLLiens")) == true)
			{
				chkAutoPostCLLiens.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCLLiens.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostCLTA")) == true)
			{
				chkAutoPostCLTaxAcquired.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostCLTaxAcquired.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostBLCommitments")) == true)
			{
				chkAutoPostBLCommitments.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostBLCommitments.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostBLSupplementals")) == true)
			{
				chkAutoPostBLSupplementals.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostBLSupplementals.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostUBBills")) == true)
			{
				chkAutoPostUTBills.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostUTBills.CheckState = CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("AutoPostUBLiens")) == true)
			{
				chkAutoPostUTLiens.CheckState = CheckState.Checked;
			}
			else
			{
				chkAutoPostUTLiens.CheckState = CheckState.Unchecked;
			}

			if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions((int)BudgetarySecurityItems.WarrantMessage)) == "F")
			{
				btnEditWarrantMessage.Enabled = true;
			}
			else
			{
				btnEditWarrantMessage.Enabled = false;
			}
			this.Refresh();
		}

		private void SetupFundGrid()
		{
			clsDRWrapper rs = new clsDRWrapper();
			string strComboList;
			NumberCol = 0;
			SelectCol = 1;
			DescriptionCol = 2;
			CashFundCol = 3;
			OverrideAPCol = 4;
			OverrideCRCol = 5;
			OverridePYCol = 6;
			//strComboList = "";
			// FC:FINAL:VGE - #817 Empty element added (by adding | separator)
			strComboList = "|";
			vsFunds.TextMatrix(0, SelectCol, "Select");
			vsFunds.TextMatrix(0, DescriptionCol, "Fund");
			vsFunds.TextMatrix(0, CashFundCol, "Cash Fund");
			vsFunds.TextMatrix(0, OverrideAPCol, "AP Override");
			vsFunds.TextMatrix(0, OverrideCRCol, "CR Override");
			vsFunds.TextMatrix(0, OverridePYCol, "PY Override");
			vsFunds.ColWidth(SelectCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.10));
			vsFunds.ColWidth(DescriptionCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.35));
			vsFunds.ColWidth(CashFundCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverrideAPCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverrideCRCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverridePYCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColHidden(NumberCol, true);
			vsFunds.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsFunds.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "' ORDER BY Fund");
			//FC:FINAL:DSE  #i566 ColComboList resets some grid values, so first get only the ComboList string
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					strComboList += rs.Get_Fields("Fund") + "|";
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			strComboList = Strings.Left(strComboList, strComboList.Length - 1);
			vsFunds.ColComboList(CashFundCol, strComboList);
			vsFunds.ColComboList(OverrideAPCol, strComboList);
			vsFunds.ColComboList(OverrideCRCol, strComboList);
			vsFunds.ColComboList(OverridePYCol, strComboList);
			//FC:FINAL:DSE #i566 ColComboList resets some grid values, so reparse the RecordSet for the rows
			rs.MoveFirst();
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsFunds.AddItem("");
					vsFunds.TextMatrix(vsFunds.Rows - 1, NumberCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					vsFunds.TextMatrix(vsFunds.Rows - 1, SelectCol, FCConvert.ToString(rs.Get_Fields_Boolean("UseDueToFrom")));
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					vsFunds.TextMatrix(vsFunds.Rows - 1, DescriptionCol, rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("LongDescription"));
					vsFunds.TextMatrix(vsFunds.Rows - 1, CashFundCol, FCConvert.ToString(rs.Get_Fields_String("CashFund")));
					vsFunds.TextMatrix(vsFunds.Rows - 1, OverrideAPCol, FCConvert.ToString(rs.Get_Fields_String("APOverrideCashFund")));
					vsFunds.TextMatrix(vsFunds.Rows - 1, OverrideCRCol, FCConvert.ToString(rs.Get_Fields_String("CROverrideCashFund")));
					vsFunds.TextMatrix(vsFunds.Rows - 1, OverridePYCol, FCConvert.ToString(rs.Get_Fields_String("PYOverrideCashFund")));
					if (!FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseDueToFrom")))
					{
						vsFunds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFunds.Rows - 1, 0, vsFunds.Rows - 1, vsFunds.Cols - 1, 0xC0C0C0);
					}
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void frmCustomization_Resize(object sender, System.EventArgs e)
		{
			vsFunds.ColWidth(SelectCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.10));
			vsFunds.ColWidth(DescriptionCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.35));
			vsFunds.ColWidth(CashFundCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverrideAPCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverrideCRCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
			vsFunds.ColWidth(OverridePYCol, FCConvert.ToInt32(vsFunds.WidthOriginal * 0.15));
		}

		private void optOptions_Click(int Index, object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN: Change using selected index on selected text.
			string selectedText = cmbOption.Text;
			// switch (Index)
			switch (selectedText)
			{
				case "General":
					{
						//FC:FINAL:BBE - moved from Option1_CheckedChanged
						//"General"
						fraGeneralOptions.Visible = true;
						fraPrintingOptions.Visible = false;
						fraAPOptions.Visible = false;
						fraACHOptions.Visible = false;
						fraDueToFrom.Visible = false;
						fraPosting.Visible = false;
						break;
					}
				case "Accounts Payable":
					{
						//FC:FINAL:BBE - moved from Option3_CheckedChanged
						//"Accounts Payable"
						fraGeneralOptions.Visible = false;
						fraPrintingOptions.Visible = false;
						fraAPOptions.Visible = true;
						fraACHOptions.Visible = false;
						fraDueToFrom.Visible = false;
						fraPosting.Visible = false;
						break;
					}
				case "Printing":
					{
						//FC:FINAL:BBE - moved from Option2_CheckedChanged
						//"Printing"
						fraGeneralOptions.Visible = false;
						fraPrintingOptions.Visible = true;
						fraAPOptions.Visible = false;
						fraACHOptions.Visible = false;
						fraDueToFrom.Visible = false;
						fraPosting.Visible = false;
						break;
					}
				case "ACH":

                    {
						//FC:FINAL:BBE - moved from Option4_CheckedChanged
						//"ACH"
						fraGeneralOptions.Visible = false;
						fraPrintingOptions.Visible = false;
						fraAPOptions.Visible = false;
						fraACHOptions.Visible = true;
						fraDueToFrom.Visible = false;
						fraPosting.Visible = false;
						break;
					}
				case "Due To / From":
					{
						//FC:FINAL:BBE - moved from Option5_CheckedChanged
						//"Due To / From"
						fraGeneralOptions.Visible = false;
						fraPrintingOptions.Visible = false;
						fraAPOptions.Visible = false;
						fraACHOptions.Visible = false;
						fraDueToFrom.Visible = true;
						fraPosting.Visible = false;
						break;
					}
				case "Posting":
					{
						//FC:FINAL:BBE - moved from Option6_CheckedChanged
						//"Posting"
						fraGeneralOptions.Visible = false;
						fraPrintingOptions.Visible = false;
						fraAPOptions.Visible = false;
						fraACHOptions.Visible = false;
						fraDueToFrom.Visible = false;
						fraPosting.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void cmbOption_Click(object sender, System.EventArgs e)
		{
			int index = cmbOption.SelectedIndex;
			optOptions_Click(index, sender, e);
		}
		//FC:FINAL:BBE - move to optOptions_Click
		//private void Option5_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = false;
		//    fraPrintingOptions.Visible = false;
		//    fraAPOptions.Visible = false;
		//    fraACHOptions.Visible = false;
		//    fraDueToFrom.Visible = true;
		//    fraPosting.Visible = false;
		//}
		private void frmCustomization_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int counter;
			string temp = "";
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuProcessSave_Click();
			}
			else if (KeyCode == Keys.F9)
			{
				KeyCode = (Keys)0;
				mnuFileSave_Click();
			}
		}

		private void frmCustomization_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomization.FillStyle	= 0;
			//frmCustomization.ScaleWidth	= 9300;
			//frmCustomization.ScaleHeight	= 7515;
			//frmCustomization.LinkTopic	= "Form2";
			//frmCustomization.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			LoadFederalIDPrexixCombo();
			SetupcmbTrackLastCheckNumber();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
		}

		private void frmCustomization_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void btnFileCustomCheck_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: frmCreateCustomCheckFormat.InstancePtr.ShowDialog();
			frmCreateCustomCheckFormat.InstancePtr.Show(App.MainForm);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		public void mnuFileSave_Click()
		{
			mnuFileSave_Click(cmdSave, new System.EventArgs());
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:PB - issue #2815: changed to false
			blnUnload = false;
			SaveInfo();
		}

		public void mnuProcessSave_Click()
		{
			//FC:FINAL:BBE - Save and Exit menu is eliminated when redesign
			//mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}
		//FC:FINAL:BBE - move to optOptions_Click
		//private void Option1_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = true;
		//    fraPrintingOptions.Visible = false;
		//    fraAPOptions.Visible = false;
		//    fraACHOptions.Visible = false;
		//    fraDueToFrom.Visible = false;
		//    fraPosting.Visible = false;
		//}
		//private void Option2_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = false;
		//    fraPrintingOptions.Visible = true;
		//    fraAPOptions.Visible = false;
		//    fraACHOptions.Visible = false;
		//    fraDueToFrom.Visible = false;
		//    fraPosting.Visible = false;
		//}
		//private void Option3_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = false;
		//    fraPrintingOptions.Visible = false;
		//    fraAPOptions.Visible = true;
		//    fraACHOptions.Visible = false;
		//    fraDueToFrom.Visible = false;
		//    fraPosting.Visible = false;
		//}
		//private void Option4_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = false;
		//    fraPrintingOptions.Visible = false;
		//    fraAPOptions.Visible = false;
		//    fraACHOptions.Visible = true;
		//    fraDueToFrom.Visible = false;
		//    fraPosting.Visible = false;
		//}
		//private void Option6_CheckedChanged(object sender, System.EventArgs e)
		//{
		//    fraGeneralOptions.Visible = false;
		//    fraPrintingOptions.Visible = false;
		//    fraAPOptions.Visible = false;
		//    fraACHOptions.Visible = false;
		//    fraDueToFrom.Visible = false;
		//    fraPosting.Visible = true;
		//}
		private void txt1099Adjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCheckAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLabelAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtJournal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtJournal.Text))
			{
				MessageBox.Show("You may only enter a number in this field", "Invalid Journal Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtJournal.Text = "";
			}
			else
			{
				txtJournal.Text = modValidateAccount.GetFormat_6(txtJournal.Text, 4);
			}
		}

		private void txtNextPO_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtReportAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Insert && KeyAscii != Keys.Delete && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtWarrant_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!Information.IsNumeric(txtWarrant.Text))
			{
				MessageBox.Show("You may only enter a number in this field", "Invalid Journal Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				e.Cancel = true;
				txtWarrant.Text = "";
			}
			else
			{
				txtWarrant.Text = modValidateAccount.GetFormat_6(txtWarrant.Text, 4);
			}
		}

		private void SaveInfo()
		{
			clsDRWrapper rsFundInfo = new clsDRWrapper();
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			clsDRWrapper rsDelete = new clsDRWrapper();
			int counter;
			int counter2;
			if (cmbOption.SelectedIndex == 3)
			{
				cmbOption.Focus();
			}
			else if (cmbOption.SelectedIndex == 2)
			{
				cmbOption.Focus();
			}
			else if (cmbOption.SelectedIndex == 4)
			{
				cmbOption.Focus();
			}
			else if (cmbOption.SelectedIndex == 1)
			{
				cmbOption.Focus();
			}
			else if (cmbOption.SelectedIndex == 0)
			{
				cmbOption.Focus();
			}
			else
			{
				cmbOption.Focus();
			}
			if (!Information.IsNumeric(txt1099Adjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your 1099 laser printer line adjustment before you may proceed.", "Invalid 1099 Line Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!Information.IsNumeric(txtCheckAdjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your check laser printer line adjustment before you may proceed.", "Invalid Check Line Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!Information.IsNumeric(txtReportAdjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your summary / detail report adjustment before you may proceed.", "Invalid Report Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!Information.IsNumeric(txtLabelAdjustment.Text))
			{
				MessageBox.Show("You must enter a numeric number in for your label laser printer line adjustment before you may proceed.", "Invalid Label Line Adjustment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (chkAutoIncrementPO.CheckState == CheckState.Checked)
			{
				if (Conversion.Val(txtNextPO.Text) < 1)
				{
					MessageBox.Show("You must enter a number greater than 0 for your next PO before you may continue.", "Invalid Next PO", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (chkDue.CheckState == CheckState.Checked)
			{
				for (counter = 1; counter <= vsFunds.Rows - 1; counter++)
				{
					if (FCUtils.CBool(vsFunds.TextMatrix(counter, SelectCol)) == true)
					{
						if (vsFunds.TextMatrix(counter, CashFundCol) == "")
						{
							MessageBox.Show("If you choose to use the Due To/From option for a fund you must select a cash fund. Please deselect Fund " + Strings.Left(vsFunds.TextMatrix(counter, DescriptionCol), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + " or select the cash fund for Fund " + Strings.Left(vsFunds.TextMatrix(counter, DescriptionCol), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + ".");
							return;
						}
					}
				}
			}
			string strTemp;
			strTemp = "No";
			if (cmbTrackLastCheckNumber.SelectedIndex >= 0)
			{
				switch (cmbTrackLastCheckNumber.ItemData(cmbTrackLastCheckNumber.SelectedIndex))
				{
					case 0:
						{
							strTemp = "No";
							break;
						}
					case 1:
						{
							strTemp = "Bank";
							break;
						}
					case 2:
						{
							strTemp = "Check Type";
							break;
						}
				}
				//end switch
			}
			setCont.SaveSetting(strTemp, "UseBanksLastCheckNumber", "", "", "", "");
			modBudgetaryAccounting.UpdateBDVariable("ValidAccountUse", cboValidAccount.SelectedIndex + 1);
			modBudgetaryAccounting.UpdateBDVariable("BalanceValidation", cboBalanceValidation.SelectedIndex + 1);
			
			if (cboBalanceValidation.SelectedIndex == 2)
			{
				rsFundInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' or AccountType = 'P') AND ValidateBalance = 1");
				if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
				{
					do
					{
						rsFundInfo.Edit();
						rsFundInfo.Set_Fields("ValidateBalance", false);
						rsFundInfo.Update(false);
						rsFundInfo.MoveNext();
					}
					while (rsFundInfo.EndOfFile() != true);
				}
			}
			else
			{
				rsFundInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE (AccountType = 'E' OR AccountType = 'P') AND ValidateBalance = 1");
				if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					ans = MessageBox.Show("You have selected to validate balances but, did not select any accounts that you wanted to use it for.  Do you wish to validate balances on all your accounts?", "Validate Balances on All Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						rsFundInfo.Execute("UPDATE AccountMaster SET ValidateBalance = 1 WHERE (AccountType = 'E' or AccountType = 'P')", "Budgetary");
					}
					else
					{
						ans = MessageBox.Show("You will not be able to validate balances until you choose accounts which should be validated.  Do you wish to select accounts at this time?", "Select Accounts?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							frmSelectAccounts.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
						}
					}
				}
			}
			modBudgetaryAccounting.UpdateBDVariable("PreviewPrint", cboPreviewAccount.SelectedIndex + 1);
			modBudgetaryAccounting.UpdateBDVariable("WarrantPrintOption", cboPreviewAccount.SelectedIndex + 1);
			modBudgetaryAccounting.UpdateBDVariable("APSeq", cboAPSequence.SelectedIndex + 1);
			modBudgetaryAccounting.UpdateBDVariable("FiscalStart", modValidateAccount.GetFormat_6(FCConvert.ToString(cboFirstMonth.SelectedIndex + 1), 2));
			modBudgetaryAccounting.UpdateBDVariable("CheckFormat", Strings.Trim(cboCheckFormat.ItemData(cboCheckFormat.SelectedIndex).ToString()));
			modBudgetaryAccounting.UpdateBDVariable("BalancedACHFile", chkBalanceACHFile.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("ACHFederalIDPrefix", cboFederalIDPrefix.ItemData(cboFederalIDPrefix.SelectedIndex));
			modBudgetaryAccounting.UpdateBDVariable("ACHFederalID", Strings.Trim(txtACHFederalID.Text));
			modBudgetaryAccounting.UpdateBDVariable("ACHTownName", Strings.Trim(txtACHTownName.Text));
			modBudgetaryAccounting.UpdateBDVariable("ImmediateDestinationName", Strings.Trim(txtImmediateDestinationName.Text));
			modBudgetaryAccounting.UpdateBDVariable("ImmediateDestinationRoutingNumber", Strings.Trim(txtImmediateDestinationRoutingNumber.Text));
			modBudgetaryAccounting.UpdateBDVariable("ImmediateOriginName", Strings.Trim(txtImmediateOriginName.Text));
			modBudgetaryAccounting.UpdateBDVariable("ImmediateOriginODFINumber", Strings.Trim(txtImmediateOriginODFINumber.Text));
			modBudgetaryAccounting.UpdateBDVariable("ImmediateOriginRoutingNumber", Strings.Trim(txtImmediateOriginRoutingNumber.Text));
			modBudgetaryAccounting.UpdateBDVariable("ReturnAddress1", Strings.Trim(txtReturnAddress1.Text));
			modBudgetaryAccounting.UpdateBDVariable("ReturnAddress2", Strings.Trim(txtReturnAddress2.Text));
			modBudgetaryAccounting.UpdateBDVariable("ReturnAddress3", Strings.Trim(txtReturnAddress3.Text));
			modBudgetaryAccounting.UpdateBDVariable("ReturnAddress4", Strings.Trim(txtReturnAddress4.Text));
			modBudgetaryAccounting.UpdateBDVariable("AutoPostAP", chkAutoPostAP.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostGJ", chkAutoPostGJ.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCR", chkAutoPostCR.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCM", chkAutoPostCM.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostEN", chkAutoPostEN.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostAPCorr", chkAutoPostAPCorr.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCD", chkAutoPostCD.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostReversingJournals", chkAutoPostReversingJournals.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostBudgetTransfer", chkAutoPostBudgetTrnsfer.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCreateOpeningAdjustments", chkAutoPostCreateOpeningAdjustments.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostARBills", chkAutoPostARBills.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCRDailyAudit", chkAutoPostCRDailyAudit.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostFADepreciation", chkAutoPostFADepreciation.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostMARR", chkAutoPostMVRapidRenewal.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostPYProcesses", chkAutoPostPYProcesses.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCLLiens", chkAutoPostCLLiens.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostCLTA", chkAutoPostCLTaxAcquired.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostBLCommitments", chkAutoPostBLCommitments.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostBLSupplementals", chkAutoPostBLSupplementals.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostUBBills", chkAutoPostUTBills.CheckState == CheckState.Checked);
			modBudgetaryAccounting.UpdateBDVariable("AutoPostUBLiens", chkAutoPostUTLiens.CheckState == CheckState.Checked);
			bdSettings.ACHFederalID = Strings.Trim(txtACHFederalID.Text);
			bdSettings.ACHFederalIDPrefix = FCConvert.ToInt16(cboFederalIDPrefix.ItemData(cboFederalIDPrefix.SelectedIndex));
			bdSettings.ACHTownName = Strings.Trim(txtACHTownName.Text);
			bdSettings.AllowManualWarrantNumbers = chkManualWarrantNumbers.CheckState == CheckState.Checked;
			bdSettings.APSeq = FCConvert.ToString(cboAPSequence.SelectedIndex + 1);
			bdSettings.AutomaticInterest = chkAutomaticInterestEntries.CheckState == CheckState.Checked;
			bdSettings.AutoNumberPO = chkAutoIncrementPO.CheckState == CheckState.Checked;
			bdSettings.AutoPostAP = chkAutoPostAP.CheckState == CheckState.Checked;
			bdSettings.AutoPostAPCorr = chkAutoPostAPCorr.CheckState == CheckState.Checked;
			bdSettings.AutoPostARBills = chkAutoPostARBills.CheckState == CheckState.Checked;
			bdSettings.AutoPostBLCommitments = chkAutoPostBLCommitments.CheckState == CheckState.Checked;
			bdSettings.AutoPostBLSupplementals = chkAutoPostBLSupplementals.CheckState == CheckState.Checked;
			bdSettings.AutoPostBudgetTransfer = chkAutoPostBudgetTrnsfer.CheckState == CheckState.Checked;
			bdSettings.AutoPostCD = chkAutoPostCD.CheckState == CheckState.Checked;
			bdSettings.AutoPostCLLiens = chkAutoPostCLLiens.CheckState == CheckState.Checked;
			bdSettings.AutoPostCLTA = chkAutoPostCLTaxAcquired.CheckState == CheckState.Checked;
			bdSettings.AutoPostCM = chkAutoPostCM.CheckState == CheckState.Checked;
			bdSettings.AutoPostCR = chkAutoPostCR.CheckState == CheckState.Checked;
			bdSettings.AutoPostCRDailyAudit = chkAutoPostCRDailyAudit.CheckState == CheckState.Checked;
			bdSettings.AutoPostCreateOpeningAdjustments = chkAutoPostCreateOpeningAdjustments.CheckState == CheckState.Checked;
			bdSettings.AutoPostEN = chkAutoPostEN.CheckState == CheckState.Checked;
			bdSettings.AutoPostFADepreciation = chkAutoPostFADepreciation.CheckState == CheckState.Checked;
			bdSettings.AutoPostGJ = chkAutoPostGJ.CheckState == CheckState.Checked;
			bdSettings.AutoPostMARR = chkAutoPostMVRapidRenewal.CheckState == CheckState.Checked;
			bdSettings.AutoPostPYProcesses = chkAutoPostPYProcesses.CheckState == CheckState.Checked;
			bdSettings.AutoPostReversingJournals = chkAutoPostReversingJournals.CheckState == CheckState.Checked;
			bdSettings.AutoPostUBBills = chkAutoPostUTBills.CheckState == CheckState.Checked;
			bdSettings.AutoPostUBLiens = chkAutoPostUTLiens.CheckState == CheckState.Checked;
			bdSettings.BalancedACHFile = chkBalanceACHFile.CheckState == CheckState.Checked;
			bdSettings.BalanceValidation = FCConvert.ToInt16(cboBalanceValidation.SelectedIndex + 1);
			bdSettings.CheckFormat = FCConvert.ToInt32(Strings.Trim(cboCheckFormat.ItemData(cboCheckFormat.SelectedIndex).ToString()));
			bdSettings.CheckLaserAdjustment = Conversion.Val(txtCheckAdjustment.Text);
			if (chkAPReference.CheckState == CheckState.Unchecked)
			{
				bdSettings.CheckRef = "N";
				bdSettings.SameVendorOnly = false;
			}
			else
            {
                bdSettings.CheckRef = "Y";
                bdSettings.SameVendorOnly = chkSameVendor.CheckState == CheckState.Checked;
            }
			bdSettings.Due = chkDue.CheckState == CheckState.Checked ? "Y" : "N";
			bdSettings.EncByDept = chkEncumbranceByDepartment.CheckState == CheckState.Checked;
			bdSettings.EncCarryForward = FCConvert.ToInt16(cboEncCarryForward.SelectedIndex);
			bdSettings.FiscalStart = modValidateAccount.GetFormat_6(FCConvert.ToString(cboFirstMonth.SelectedIndex + 1), 2);
			bdSettings.ImmediateDestinationName = Strings.Trim(txtImmediateDestinationName.Text);
			bdSettings.ImmediateDestinationRoutingNumber = Strings.Trim(txtImmediateDestinationRoutingNumber.Text);
			bdSettings.ImmediateOriginName = Strings.Trim(txtImmediateOriginName.Text);
			bdSettings.ImmediateOriginODFINumber = Strings.Trim(txtImmediateOriginODFINumber.Text);
			bdSettings.ImmediateOriginRoutingNumber = Strings.Trim(txtImmediateOriginRoutingNumber.Text);
			bdSettings.LabelLaserAdjustment = Conversion.Val(txtLabelAdjustment.Text);
			bdSettings.LandscapeWarrant = chkWarrantLandscape.CheckState == CheckState.Checked;
			bdSettings.PrintSignature = chkSignature.CheckState == CheckState.Checked;
			bdSettings.RecapOrderByAccounts = chkRecapOrderByAccount.CheckState == CheckState.Checked;
			bdSettings.ReportAdjustment = Conversion.Val(txtReportAdjustment.Text);
			bdSettings.ReturnAddress1 = Strings.Trim(txtReturnAddress1.Text);
			bdSettings.ReturnAddress2 = Strings.Trim(txtReturnAddress2.Text);
			bdSettings.ReturnAddress3 = Strings.Trim(txtReturnAddress3.Text);
			bdSettings.ReturnAddress4 = Strings.Trim(txtReturnAddress4.Text);
			bdSettings.ShadeReports = chkShade.CheckState == CheckState.Unchecked ? "N" : "Y";
			bdSettings.UseProjectNumber = chkProjects.CheckState == CheckState.Unchecked ? "N" : "Y";
			bdSettings.ValidAccountBudgetProcess = chkBudgetAccounts.CheckState == CheckState.Checked;
			bdSettings.ValidAccountUse = FCConvert.ToString(cboValidAccount.SelectedIndex + 1);
			bdSettings.WarrantPrevDeptSummary = chkWarrantDeptSummary.CheckState == CheckState.Checked;
			bdSettings.WarrantPrintOption = FCConvert.ToString(cboPreviewAccount.SelectedIndex + 1);
			btnFileCustomCheck.Enabled = Conversion.Val(Strings.Trim(cboCheckFormat.ItemData(cboCheckFormat.SelectedIndex).ToString())) == 4;
			modRegistry.SaveRegistryKey("1099LASERADJUSTMENT", FCConvert.ToString(Conversion.Val(txt1099Adjustment.Text)));
			modRegistry.SaveRegistryKey("1099LASERADJUSTMENTHORIZONTAL", FCConvert.ToString(Conversion.Val(txt1099AdjustmentHorizontal.Text)));
			modRegistry.SaveRegistryKey("CHECKLASERADJUSTMENT", FCConvert.ToString(Conversion.Val(txtCheckAdjustment.Text)));
			modRegistry.SaveRegistryKey("LABELLASERADJUSTMENT", FCConvert.ToString(Conversion.Val(txtLabelAdjustment.Text)));
			modRegistry.SaveRegistryKey("REPORTADJUSTMENT", FCConvert.ToString(Conversion.Val(txtReportAdjustment.Text)));
			modBudgetaryMaster.Statics.FirstMonth = cboFirstMonth.SelectedIndex + 1;
			if (Strings.Right(cboAPBank.Items[cboAPBank.SelectedIndex].ToString(), cboAPBank.Items[cboAPBank.SelectedIndex].ToString().Length - 4) == "NONE")
			{
				modBudgetaryMaster.UpdateBankVariable("APBank", 0);
			}
			else
			{
				modBudgetaryMaster.UpdateBankVariable("APBank", cboAPBank.SelectedIndex);
			}
			if (Strings.Right(cboCRBank.Items[cboCRBank.SelectedIndex].ToString(), cboCRBank.Items[cboCRBank.SelectedIndex].ToString().Length - 4) == "NONE")
			{
				modBudgetaryMaster.UpdateBankVariable("CRBank", 0);
			}
			else
			{
				modBudgetaryMaster.UpdateBankVariable("CRBank", cboCRBank.SelectedIndex);
			}
			if (Strings.Right(cboPayrollBank.Items[cboPayrollBank.SelectedIndex].ToString(), cboPayrollBank.Items[cboPayrollBank.SelectedIndex].ToString().Length - 4) == "NONE")
			{
				modBudgetaryMaster.UpdateBankVariable("PayrollBank", 0);
			}
			else
			{
				modBudgetaryMaster.UpdateBankVariable("PayrollBank", cboPayrollBank.SelectedIndex);
			}
			if (chkWarrantSignature.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("WarrantPrevSigLine", "N");
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("WarrantPrevSigLine", "Y");
			}
			if (chkAutomaticInterestEntries.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("AutomaticInterest", false);
				modBudgetaryAccounting.Statics.gboolAutoInterest = false;
				rsDelete.Execute("DELETE FROM BankCashAccounts WHERE Interest = 1", "TWBD0000.vb1");
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("AutomaticInterest", true);
				modBudgetaryAccounting.Statics.gboolAutoInterest = true;
			}
			if (chkWarrantDeptSummary.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("WarrantPrevDeptSummary", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("WarrantPrevDeptSummary", true);
			}
			if (chkManualWarrantNumbers.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("AllowManualWarrantNumbers", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("AllowManualWarrantNumbers", true);
			}
			if (chkEncumbranceByDepartment.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("EncByDept", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("EncByDept", true);
			}
			if (chkRecapOrderByAccount.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("RecapOrderByAccounts", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("RecapOrderByAccounts", true);
			}
			
			if (chkWarrantLandscape.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("LandscapeWarrant", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("LandscapeWarrant", true);
			}
			if (chkBudgetAccounts.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("ValidAccountBudgetProcess", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("ValidAccountBudgetProcess", true);
			}
			if (chkShade.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("ShadeReports", "N");
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("ShadeReports", "Y");
			}
			if (chkSignature.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("PrintSignature", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("PrintSignature", true);
			}
			modBudgetaryAccounting.UpdateBDVariable("EncCarryForward", cboEncCarryForward.SelectedIndex);
			if (chkAutoIncrementPO.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("AutonumberPO", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("AutonumberPO", true);
				rsFundInfo.OpenRecordset("SELECT * FROM POLock");
				if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
				{
					rsFundInfo.Edit();
				}
				else
				{
					rsFundInfo.AddNew();
				}
				rsFundInfo.Set_Fields("NextPO", FCConvert.ToString(Conversion.Val(txtNextPO.Text)));
				rsFundInfo.Update();
			}
			if (chkDue.CheckState == CheckState.Unchecked)
			{
				rsFundInfo.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "' AND UseDueToFrom = 1");
				if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
				{
					do
					{
						rsFundInfo.Edit();
						rsFundInfo.Set_Fields("UseDueToFrom", false);
						rsFundInfo.Set_Fields("CashFund", "");
						rsFundInfo.Set_Fields("APOverrideCashFund", "");
						rsFundInfo.Set_Fields("CROverrideCashFund", "");
						rsFundInfo.Set_Fields("PYOverrideCashFund", "");
						rsFundInfo.Update(false);
						rsFundInfo.MoveNext();
					}
					while (rsFundInfo.EndOfFile() != true);
				}
				modBudgetaryAccounting.UpdateBDVariable("Due", "N");
			}
			else
			{
				bool blnFundsSelected = false;
				blnFundsSelected = false;
				for (counter = 1; counter <= vsFunds.Rows - 1; counter++)
				{
					rsFundInfo.OpenRecordset("SELECT * FROM LedgerTitles WHERE ID = " + vsFunds.TextMatrix(counter, NumberCol));
					rsFundInfo.Edit();
					if (FCUtils.CBool(vsFunds.TextMatrix(counter, SelectCol)) == true)
					{
						blnFundsSelected = true;
						//rsFundInfo.Set_Fields("UseDueToFrom", vsFunds.TextMatrix(counter, SelectCol));
						// FC:FINAL:VGE - #868 Using FCUtils.CBool from condition.
						rsFundInfo.Set_Fields("UseDueToFrom", FCUtils.CBool(vsFunds.TextMatrix(counter, SelectCol)));
						rsFundInfo.Set_Fields("CashFund", vsFunds.TextMatrix(counter, CashFundCol));
						rsFundInfo.Set_Fields("APOverrideCashFund", vsFunds.TextMatrix(counter, OverrideAPCol));
						rsFundInfo.Set_Fields("CROverrideCashFund", vsFunds.TextMatrix(counter, OverrideCRCol));
						rsFundInfo.Set_Fields("PYOverrideCashFund", vsFunds.TextMatrix(counter, OverridePYCol));
					}
					else
					{
						rsFundInfo.Set_Fields("UseDueToFrom", false);
						rsFundInfo.Set_Fields("CashFund", "");
						rsFundInfo.Set_Fields("APOverrideCashFund", "");
						rsFundInfo.Set_Fields("CROverrideCashFund", "");
						rsFundInfo.Set_Fields("PYOverrideCashFund", "");
					}
					rsFundInfo.Update(false);
				}
				if (!blnFundsSelected)
				{
					ans = MessageBox.Show("You have selected to used Due To / From, but did not select any funds that you wanted to use it for.  Do you wish to make all your funds use Due To / From using default accounts set up in GL Control Accounts?", "All Funds Use Due To / From?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						string strCashFund = "";
						string strDueToAcct = "";
						string strDueFromAcct = "";
						rsFundInfo.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "'");
						if (rsFundInfo.EndOfFile() != true && rsFundInfo.BeginningOfFile() != true)
						{
							do
							{
								rsFundInfo.Edit();
								rsFundInfo.Set_Fields("UseDueToFrom", true);
								rsFundInfo.Set_Fields("CashFund", modBudgetaryAccounting.Statics.strDueToFromCashFund);
								rsFundInfo.Set_Fields("APOverrideCashFund", "");
								rsFundInfo.Set_Fields("CROverrideCashFund", "");
								rsFundInfo.Set_Fields("PYOverrideCashFund", "");
								rsFundInfo.Update(false);
								rsFundInfo.MoveNext();
							}
							while (rsFundInfo.EndOfFile() != true);
						}
					}
					else
					{
						ans = MessageBox.Show("You will not be able to use Due To / From until you choose funds which should use this.  Do you wish to select funds at this time?", "Select Funds?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (ans == DialogResult.Yes)
						{
							cmbOption.SelectedIndex = 0;
							return;
						}
					}
				}
				modBudgetaryAccounting.UpdateBDVariable("Due", "Y");
			}
			if (chkAPReference.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("CheckRef", "N");
				modBudgetaryAccounting.UpdateBDVariable("SameVendorOnly", false);
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("CheckRef", "Y");
				if (chkSameVendor.CheckState == CheckState.Checked)
				{
					modBudgetaryAccounting.UpdateBDVariable("SameVendorOnly", true);
				}
				else
				{
					modBudgetaryAccounting.UpdateBDVariable("SameVendorOnly", false);
				}
			}
			if (chkProjects.CheckState == CheckState.Unchecked)
			{
				modBudgetaryAccounting.UpdateBDVariable("UseProjectNumber", "N");
				modBudgetaryMaster.Statics.ProjectFlag = false;
			}
			else
			{
				modBudgetaryAccounting.UpdateBDVariable("UseProjectNumber", "Y");
				modBudgetaryMaster.Statics.ProjectFlag = true;
			}
			modValidateAccount.Statics.ValidAcctCheck = cboValidAccount.SelectedIndex + 1;
			MessageBox.Show("Save Successful", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			if (blnUnload)
			{
				Close();
			}
            //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
            App.MainForm.ReloadNavigationMenu();
		}

		private void cboAPBank_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboAPBank.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboPayrollBank_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboPayrollBank.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboCRBank_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboCRBank.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cboCheckFormat_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboCheckFormat.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void LoadFederalIDPrexixCombo()
		{
			cboFederalIDPrefix.Clear();
			cboFederalIDPrefix.AddItem(" ");
			cboFederalIDPrefix.ItemData(cboFederalIDPrefix.NewIndex, 0);
			cboFederalIDPrefix.AddItem("1");
			cboFederalIDPrefix.ItemData(cboFederalIDPrefix.NewIndex, 1);
			cboFederalIDPrefix.AddItem("9");
			cboFederalIDPrefix.ItemData(cboFederalIDPrefix.NewIndex, 9);
		}

		private void vsFunds_ClickEvent(object sender, Wisej.Web.DataGridViewCellEventArgs e)
		{
			//FC:FINAl:SBE - #815 - workaround for ITG issue. Use MouseRow and MouseCol to have the correct values on cell click
			if (vsFunds.MouseCol == SelectCol)
			{
				vsFunds.Editable = FCGrid.EditableSettings.flexEDNone;
				if (FCUtils.CBool(vsFunds.TextMatrix(vsFunds.MouseRow, SelectCol)) == true)
				{
					vsFunds.TextMatrix(vsFunds.MouseRow, SelectCol, FCConvert.ToString(false));
					vsFunds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFunds.MouseRow, 0, vsFunds.MouseRow, vsFunds.Cols - 1, 0xC0C0C0);
				}
				else
				{
					vsFunds.TextMatrix(vsFunds.MouseRow, SelectCol, FCConvert.ToString(true));
					vsFunds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFunds.MouseRow, 0, vsFunds.MouseRow, vsFunds.Cols - 1, Color.White);
				}
			}
			else
			{
				if (FCUtils.CBool(vsFunds.TextMatrix(vsFunds.MouseRow, SelectCol)) == true)
				{
					vsFunds.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsFunds.EditCell();
					if (vsFunds.MouseCol == CashFundCol || vsFunds.MouseCol == OverrideAPCol || vsFunds.MouseCol == OverrideCRCol || vsFunds.MouseCol == OverridePYCol)
					{
						vsFunds.EditSelStart = 0;
					}
				}
				else
				{
					vsFunds.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsFunds_KeyUpEvent(object sender, Wisej.Web.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space)
			{
				if (vsFunds.Col == SelectCol)
				{
					if (FCUtils.CBool(vsFunds.TextMatrix(vsFunds.Row, SelectCol)) == true)
					{
						vsFunds.TextMatrix(vsFunds.Row, SelectCol, FCConvert.ToString(false));
						vsFunds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFunds.Row, 0, vsFunds.Row, vsFunds.Cols - 1, 0xC0C0C0);
					}
					else
					{
						vsFunds.TextMatrix(vsFunds.Row, SelectCol, FCConvert.ToString(true));
						vsFunds.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsFunds.Row, 0, vsFunds.Row, vsFunds.Cols - 1, Color.White);
					}
				}
			}
		}

		private void vsFunds_RowColChange(object sender, System.EventArgs e)
		{
			if (vsFunds.Col == SelectCol)
			{
				vsFunds.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				if (FCUtils.CBool(vsFunds.TextMatrix(vsFunds.Row, SelectCol)) == true)
				{
					vsFunds.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsFunds.EditCell();
					if (vsFunds.Col == CashFundCol || vsFunds.Col == OverrideAPCol || vsFunds.Col == OverrideCRCol || vsFunds.Col == OverridePYCol)
					{
						vsFunds.EditSelStart = 0;
					}
				}
				else
				{
					vsFunds.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsFunds_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsFunds.GetFlexRowIndex(e.RowIndex);
			int col = vsFunds.GetFlexColIndex(e.ColumnIndex);
			if (col == SelectCol && row > 0)
			{
				//if (vsFunds.EditText == FCGrid.CellCheckedSettings.flexChecked)
				if (vsFunds.EditText == "1")
				{
					vsFunds.TextMatrix(row, CashFundCol, modBudgetaryAccounting.Statics.strDueToFromCashFund);
					vsFunds.TextMatrix(row, OverrideAPCol, "");
					vsFunds.TextMatrix(row, OverrideCRCol, "");
					vsFunds.TextMatrix(row, OverridePYCol, "");
				}
				else
				{
					vsFunds.TextMatrix(row, CashFundCol, "");
					vsFunds.TextMatrix(row, OverrideAPCol, "");
					vsFunds.TextMatrix(row, OverrideCRCol, "");
					vsFunds.TextMatrix(row, OverridePYCol, "");
				}
			}
		}

		private void SetupcmbTrackLastCheckNumber()
		{
			cmbTrackLastCheckNumber.Clear();
			cmbTrackLastCheckNumber.AddItem("No");
			cmbTrackLastCheckNumber.ItemData(0, 0);
			cmbTrackLastCheckNumber.AddItem("By Bank");
			cmbTrackLastCheckNumber.ItemData(1, 1);
			cmbTrackLastCheckNumber.AddItem("By Type");
			cmbTrackLastCheckNumber.ItemData(2, 2);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFileSave_Click(sender, EventArgs.Empty);
		}

		private void btnSelectTownLogo_Click(object sender, EventArgs e)
		{
			frmChooseImage.InstancePtr.Show(App.MainForm);
		}

		private void btnEditWarrantMessage_Click(object sender, EventArgs e)
		{
			frmWarrantMessage.InstancePtr.Show(App.MainForm);
		}
	}
}
