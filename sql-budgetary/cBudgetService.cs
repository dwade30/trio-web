﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cBudgetService
	{
		//=========================================================
		public void UpdateBudgetTable()
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			clsDRWrapper rsYTDActivity = new clsDRWrapper();
			clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
			clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
			string temp = "";
			string format = "";
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			bool blnOnlyNonZeroYTDNet = false;
			string strSql = "";
			clsDRWrapper rsAcctInfo = new clsDRWrapper();
			ans = MessageBox.Show("Do you only want accounts with a YTD Net that is not 0?", "Zero YTD Net?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Updating Budget Table", true);
			frmWait.InstancePtr.prgProgress.Value = 0;
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			if (ans == DialogResult.Yes)
			{
				blnOnlyNonZeroYTDNet = true;
				// troges126
				modBudgetaryAccounting.CalculateAccountInfo();
				// CalculateAccountInfo True, True, False, "E"
				// CalculateAccountInfo True, True, False, "R"
				// CalculateAccountInfo True, True, False, "G"
				rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account");
				rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
				rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account");
			}
			else
			{
				blnOnlyNonZeroYTDNet = false;
			}
			rsBudgetInfo.OmitNullsOnInsert = true;
			rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = 0");
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT AccountMaster.Account as AccountCheck FROM AccountMaster LEFT JOIN BUDGET ON AccountMaster.Account = Budget.Account WHERE Valid = 1 AND AccountType <> 'G' AND AccountType <> 'L' AND rtrim(Budget.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					if (!blnOnlyNonZeroYTDNet)
					{
						rsBudgetInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
						rsBudgetInfo.Update();
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (UpdateBudgetTableGetYTDNet_2(rsAccountInfo.Get_Fields("Account"), rsYTDActivity, rsRevYTDActivity, rsLedgerYTDActivity) != 0)
						{
							rsBudgetInfo.AddNew();
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
							rsBudgetInfo.Update();
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 15;
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT APJournalDetail.Account as AccountCheck FROM APJournalDetail LEFT JOIN BUDGET ON APJournalDetail.Account = Budget.Account WHERE left(APJournalDetail.Account, 1) <> 'G' AND left(APJournalDetail.Account, 1) <> 'L' AND rtrim(Budget.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					if (!blnOnlyNonZeroYTDNet)
					{
						rsBudgetInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
						rsBudgetInfo.Update();
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (UpdateBudgetTableGetYTDNet_2(rsAccountInfo.Get_Fields("Account"), rsYTDActivity, rsRevYTDActivity, rsLedgerYTDActivity) != 0)
						{
							rsBudgetInfo.AddNew();
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
							rsBudgetInfo.Update();
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 30;
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT JournalEntries.Account as AccountCheck FROM JournalEntries LEFT JOIN BUDGET ON JournalEntries.Account = Budget.Account WHERE left(JournalEntries.Account, 1) <> 'G' AND left(JournalEntries.Account, 1) <> 'L' AND rtrim(Budget.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					if (!blnOnlyNonZeroYTDNet)
					{
						rsBudgetInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
						rsBudgetInfo.Update();
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (UpdateBudgetTableGetYTDNet_2(rsAccountInfo.Get_Fields("Account"), rsYTDActivity, rsRevYTDActivity, rsLedgerYTDActivity) != 0)
						{
							rsBudgetInfo.AddNew();
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
							rsBudgetInfo.Update();
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 45;
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT EncumbranceDetail.Account as AccountCheck FROM EncumbranceDetail LEFT JOIN BUDGET ON EncumbranceDetail.Account = Budget.Account WHERE left(EncumbranceDetail.Account, 1) <> 'G' AND left(EncumbranceDetail.Account, 1) <> 'L' AND rtrim(Budget.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					if (!blnOnlyNonZeroYTDNet)
					{
						rsBudgetInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
						rsBudgetInfo.Update();
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (UpdateBudgetTableGetYTDNet_2(rsAccountInfo.Get_Fields("Account"), rsYTDActivity, rsRevYTDActivity, rsLedgerYTDActivity) != 0)
						{
							rsBudgetInfo.AddNew();
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
							rsBudgetInfo.Update();
						}
					}
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 60;
			rsBudgetInfo.OpenRecordset("SELECT * FROM AccountMaster WHERE ID = 0");
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT APJournalDetail.Account as AccountCheck FROM APJournalDetail LEFT JOIN AccountMaster ON APJournalDetail.Account = AccountMaster.Account WHERE left(APJournalDetail.Account, 1) = 'E' AND rtrim(AccountMaster.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					rsBudgetInfo.AddNew();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
					format = modAccountTitle.Statics.Exp;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					temp = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					temp = Strings.Right(temp, temp.Length - 2);
					rsBudgetInfo.Set_Fields("AccountType", "E");
					rsBudgetInfo.Set_Fields("FirstAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
					temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						format = Strings.Right(format, format.Length - 4);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (temp.Length - (Conversion.Val(Strings.Left(format, 2)) + 1) > 0)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						}
						else
						{
							temp = "";
						}
						if (!modAccountTitle.Statics.ObjFlag)
						{
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						}
						rsBudgetInfo.Set_Fields("FourthAccountField", "");
					}
					else
					{
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("FourthAccountField", temp);
						}
					}
					rsBudgetInfo.Set_Fields("DateCreated", DateTime.Today);
					rsBudgetInfo.Set_Fields("Valid", 1);
					rsBudgetInfo.Update();
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 64;
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT JournalEntries.Account as AccountCheck FROM JournalEntries LEFT JOIN AccountMaster ON JournalEntries.Account = AccountMaster.Account WHERE left(JournalEntries.Account, 1) = 'E' AND rtrim(AccountMaster.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					rsBudgetInfo.AddNew();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
					format = modAccountTitle.Statics.Exp;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					temp = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					temp = Strings.Right(temp, temp.Length - 2);
					rsBudgetInfo.Set_Fields("AccountType", "E");
					rsBudgetInfo.Set_Fields("FirstAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
					temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						format = Strings.Right(format, format.Length - 4);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (temp.Length - (Conversion.Val(Strings.Left(format, 2)) + 1) > 0)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						}
						else
						{
							temp = "";
						}
						if (!modAccountTitle.Statics.ObjFlag)
						{
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						}
						rsBudgetInfo.Set_Fields("FourthAccountField", "");
					}
					else
					{
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("FourthAccountField", temp);
						}
					}
					rsBudgetInfo.Set_Fields("DateCreated", DateTime.Today);
					rsBudgetInfo.Set_Fields("Valid", 1);
					rsBudgetInfo.Update();
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 67;
			rsAccountInfo.OpenRecordset("SELECT DISTINCT AccountCheck as Account FROM (SELECT EncumbranceDetail.Account as AccountCheck FROM EncumbranceDetail LEFT JOIN AccountMaster ON EncumbranceDetail.Account = AccountMaster.Account WHERE left(EncumbranceDetail.Account, 1) = 'E' AND rtrim(AccountMaster.Account) = '') as temp");
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					rsBudgetInfo.AddNew();
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					rsBudgetInfo.Set_Fields("Account", rsAccountInfo.Get_Fields("Account"));
					format = modAccountTitle.Statics.Exp;
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					temp = FCConvert.ToString(rsAccountInfo.Get_Fields("Account"));
					temp = Strings.Right(temp, temp.Length - 2);
					rsBudgetInfo.Set_Fields("AccountType", "E");
					rsBudgetInfo.Set_Fields("FirstAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
					temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
					if (modAccountTitle.Statics.ExpDivFlag)
					{
						format = Strings.Right(format, format.Length - 4);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (temp.Length - (Conversion.Val(Strings.Left(format, 2)) + 1) > 0)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						}
						else
						{
							temp = "";
						}
						if (!modAccountTitle.Statics.ObjFlag)
						{
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						}
						rsBudgetInfo.Set_Fields("FourthAccountField", "");
					}
					else
					{
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("SecondAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
						format = Strings.Right(format, format.Length - 2);
						rsBudgetInfo.Set_Fields("ThirdAccountField", Strings.Left(temp, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2))))));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							temp = Strings.Right(temp, temp.Length - (FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(format, 2)))) + 1));
							format = Strings.Right(format, format.Length - 2);
							rsBudgetInfo.Set_Fields("FourthAccountField", temp);
						}
					}
					rsBudgetInfo.Set_Fields("DateCreated", DateTime.Today);
					rsBudgetInfo.Set_Fields("Valid", 1);
					rsBudgetInfo.Update();
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.prgProgress.Value = 73;
			if (FCConvert.ToBoolean(modBudgetaryAccounting.GetBDVariable("ValidAccountBudgetProcess")))
			{
				rsBudgetInfo.Execute("DELETE FROM Budget WHERE Account IN (SELECT Account FROM AccountMaster WHERE Valid <> 1)", "TWBD0000.vb1");
			}
			frmWait.InstancePtr.prgProgress.Value = 100;
			frmWait.InstancePtr.Unload();
		}

		private double UpdateBudgetTableGetYTDNet_2(string strAcct, clsDRWrapper rsYTDActivity, clsDRWrapper rsRevYTDActivity, clsDRWrapper rsLedgerYTDActivity)
		{
			return UpdateBudgetTableGetYTDNet(ref strAcct, ref rsYTDActivity, ref rsRevYTDActivity, ref rsLedgerYTDActivity);
		}

		private double UpdateBudgetTableGetYTDNet(ref string strAcct, ref clsDRWrapper rsYTDActivity, ref clsDRWrapper rsRevYTDActivity, ref clsDRWrapper rsLedgerYTDActivity)
		{
			double UpdateBudgetTableGetYTDNet = 0;
			UpdateBudgetTableGetYTDNet = UpdateBudgetTableGetYTDDebit(ref strAcct, ref rsYTDActivity, ref rsRevYTDActivity, ref rsLedgerYTDActivity) - UpdateBudgetTableGetYTDCredit(ref strAcct, ref rsYTDActivity, ref rsRevYTDActivity, ref rsLedgerYTDActivity);
			return UpdateBudgetTableGetYTDNet;
		}

		private double UpdateBudgetTableGetYTDDebit(ref string strAcct, ref clsDRWrapper rsYTDActivity, ref clsDRWrapper rsRevYTDActivity, ref clsDRWrapper rsLedgerYTDActivity)
		{
			double UpdateBudgetTableGetYTDDebit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (rsYTDActivity.FindFirstRecord("Account", strAcct))
					{
						UpdateBudgetTableGetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						UpdateBudgetTableGetYTDDebit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDDebit = 0;
				}
			}
			else if (Strings.Left(strAcct, 1) == "R")
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						UpdateBudgetTableGetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						UpdateBudgetTableGetYTDDebit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDDebit = 0;
				}
			}
			else if (Strings.Left(strAcct, 1) == "G")
			{
				if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
				{
					if (rsLedgerYTDActivity.FindFirstRecord("Account", strAcct))
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						UpdateBudgetTableGetYTDDebit = rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						UpdateBudgetTableGetYTDDebit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDDebit = 0;
				}
			}
			return UpdateBudgetTableGetYTDDebit;
		}

		private double UpdateBudgetTableGetYTDCredit(ref string strAcct, ref clsDRWrapper rsYTDActivity, ref clsDRWrapper rsRevYTDActivity, ref clsDRWrapper rsLedgerYTDActivity)
		{
			double UpdateBudgetTableGetYTDCredit = 0;
			if (Strings.Left(strAcct, 1) == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (rsYTDActivity.FindFirstRecord("Account", strAcct))
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						UpdateBudgetTableGetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal") * -1;
					}
					else
					{
						UpdateBudgetTableGetYTDCredit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDCredit = 0;
				}
			}
			else if (Strings.Left(strAcct, 1) == "R")
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
					{
						UpdateBudgetTableGetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal") * -1;
					}
					else
					{
						UpdateBudgetTableGetYTDCredit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDCredit = 0;
				}
			}
			else if (Strings.Left(strAcct, 1) == "G")
			{
				if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
				{
					if (rsLedgerYTDActivity.FindFirstRecord("Account", strAcct))
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						UpdateBudgetTableGetYTDCredit = rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal") * -1;
					}
					else
					{
						UpdateBudgetTableGetYTDCredit = 0;
					}
				}
				else
				{
					UpdateBudgetTableGetYTDCredit = 0;
				}
			}
			return UpdateBudgetTableGetYTDCredit;
		}
	}
}
