﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using System.Linq;
namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetAPDataEntry.
	/// </summary>
	public partial class frmGetAPDataEntry : BaseForm
	{
		public frmGetAPDataEntry()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAPDataEntry InstancePtr
		{
			get
			{
				return (frmGetAPDataEntry)Sys.GetInstance(typeof(frmGetAPDataEntry));
			}
		}

		protected frmGetAPDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();
		bool EditFlag;
		bool SearchFlag;
		int[] recordNum = null;
		bool OKFlag;
		string SearchType = "";
		int RecordCol;
		int JournalCol;
		int VendorNumberCol;
		int VendorNameCol;
		int DescriptionCol;
		int ReferenceCol;
		int AmountCol;
		// vbPorter upgrade warning: Record As int	OnWrite(string, int)
		public void StartProgram_6(int Record, bool blnJournal = false)
		{
			StartProgram(Record, blnJournal);
		}

		public void StartProgram(int Record, bool blnJournal = false)
		{
			int counter;
			//! Load frmWait; // show the wait form
            Sys.ClearInstanceOfType(typeof(frmAPDataEntry));
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			// set status back to entered status so they will be forced to run another warrant preview
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rs.Execute("UPDATE APJournal SET Status = 'E' WHERE Status = 'V' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"), "Budgetary");
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			rs.Execute("UPDATE JournalMaster SET Status = 'E' WHERE Status = 'V' AND JournalNumber = " + modBudgetaryAccounting.Statics.SearchResults.Get_Fields("JournalNumber"), "Budgetary");
			rs.OpenRecordset("SELECT * FROM APJournal WHERE ID = " + FCConvert.ToString(Record));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				frmAPDataEntry.InstancePtr.txtVendor.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")), 5);
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) == 0)
				{
					frmAPDataEntry.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorName"));
					frmAPDataEntry.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress1"));
					frmAPDataEntry.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress2"));
					frmAPDataEntry.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorAddress3"));
					frmAPDataEntry.InstancePtr.txtCity.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorCity"));
					frmAPDataEntry.InstancePtr.txtState.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorState"));
					frmAPDataEntry.InstancePtr.txtZip.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip"));
					frmAPDataEntry.InstancePtr.txtZip4.Text = FCConvert.ToString(rs.Get_Fields_String("TempVendorZip4"));
				}
				else
				{
					rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + rs.Get_Fields_Int32("VendorNumber"));
					frmAPDataEntry.InstancePtr.txtAddress[0].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckName"));
					frmAPDataEntry.InstancePtr.txtAddress[1].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress1"));
					frmAPDataEntry.InstancePtr.txtAddress[2].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress2"));
					frmAPDataEntry.InstancePtr.txtAddress[3].Text = FCConvert.ToString(rs2.Get_Fields_String("CheckAddress3"));
					frmAPDataEntry.InstancePtr.txtCity.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckCity"));
					frmAPDataEntry.InstancePtr.txtState.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckState"));
					frmAPDataEntry.InstancePtr.txtZip.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip"));
					frmAPDataEntry.InstancePtr.txtZip4.Text = FCConvert.ToString(rs2.Get_Fields_String("CheckZip4"));
				}
				if (!rs.IsFieldNull("Payable"))
					frmAPDataEntry.InstancePtr.txtPayable.Text = Strings.Format(rs.Get_Fields_DateTime("Payable"), "MM/dd/yyyy");
				if (!rs.IsFieldNull("Until"))
					frmAPDataEntry.InstancePtr.txtUntil.Text = Strings.Format(rs.Get_Fields_DateTime("Until"), "MM/dd/yyyy");
				if (!rs.IsFieldNull("Frequency"))
					// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
					frmAPDataEntry.InstancePtr.cboFrequency.Text = FCConvert.ToString(rs.Get_Fields("Frequency"));
				frmAPDataEntry.InstancePtr.txtDescription.Text = FCConvert.ToString(rs.Get_Fields_String("Description"));
				
				frmAPDataEntry.InstancePtr.txtAmount.Text = Strings.Format(rs.Get_Fields("Amount"), "#,##0.00");
				frmAPDataEntry.InstancePtr.blnJournalEdit = blnJournal;
                if (FCConvert.ToString(rs.Get_Fields("Period")) != "")
                    frmAPDataEntry.InstancePtr.txtPeriod.Text = FCConvert.ToString(rs.Get_Fields("Period"));
				if (FCConvert.ToString(rs.Get_Fields_String("Reference")) != "")
					frmAPDataEntry.InstancePtr.txtReference.Text = FCConvert.ToString(rs.Get_Fields_String("Reference"));
                if (FCConvert.ToString(rs.Get_Fields("CheckNumber")) != "")
				{
                    frmAPDataEntry.InstancePtr.txtCheck.Text = FCConvert.ToString(rs.Get_Fields("CheckNumber"));
				}
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseAlternateCash")))
				{
					frmAPDataEntry.InstancePtr.chkUseAltCashAccount.CheckState = CheckState.Checked;
					frmAPDataEntry.InstancePtr.vsAltCashAccount.Enabled = true;
					frmAPDataEntry.InstancePtr.vsAltCashAccount.TextMatrix(0, 0, FCConvert.ToString(rs.Get_Fields_String("AlternateCashAccount")));
				}
				else
				{
					frmAPDataEntry.InstancePtr.chkUseAltCashAccount.CheckState = CheckState.Unchecked;
					frmAPDataEntry.InstancePtr.vsAltCashAccount.TextMatrix(0, 0, "");
					frmAPDataEntry.InstancePtr.vsAltCashAccount.Enabled = false;
				}
				frmAPDataEntry.InstancePtr.chkSeperate.CheckState = (rs.Get_Fields_Boolean("Seperate") == true ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				frmAPDataEntry.InstancePtr.apJournalId = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				frmAPDataEntry.InstancePtr.FirstRecordShown = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				frmAPDataEntry.InstancePtr.ChosenEncumbrance = FCConvert.ToInt32(rs.Get_Fields_Int32("EncumbranceRecord"));
				frmAPDataEntry.InstancePtr.ChosenPurchaseOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("PurchaseOrderID"))));
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("PurchaseOrderID")) != 0)
				{
					frmAPDataEntry.InstancePtr.PurchaseOrderFlag = true;
				}
				GetDetails();
				// put detail information into the AP Data Entry form
				modBudgetaryMaster.Statics.blnAPEdit = true;
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				while (FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("ID")) != Record)
				{
					//Application.DoEvents();
					modBudgetaryAccounting.Statics.SearchResults.MoveNext();
				}
				frmAPDataEntry.InstancePtr.OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
				frmAPDataEntry.InstancePtr.SetReadOnly();
				//FC:FINAL:MSH - Issue #708: showing new form before closing current form not always set focus to the new form.
				// Hiding current form before closing gives opportunity to set focus correctly
				this.Hide();
				frmAPDataEntry.InstancePtr.Show(App.MainForm);
				// show the form
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
				frmWait.InstancePtr.Unload();
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				MessageBox.Show("No Account Found", "Non-Existent Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			Close();
		}
		//private void cboEntry_DropDown(object sender, System.EventArgs e)
		//{
		//    modAPIsConst.SendMessageByNum(cboEntry.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		//}
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN: Fix changing selecting text at ComboBox.
			// cmbSearchType.SelectedIndex = 5;
			cmbSearchType.Text = "";
			//optHidden.Checked = true;
			// clear the search info
			txtSearch.Text = "";
			vs1.Clear();
			vs1.TextMatrix(0, JournalCol, "Jrnl#");
			vs1.TextMatrix(0, VendorNumberCol, "Vendor#");
			vs1.TextMatrix(0, VendorNameCol, "Vendor Name");
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, ReferenceCol, "Reference");
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, 4);
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			int intAccount;
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, JournalCol));
			// get the account number of that record
			if (intAccount != 0)
			{
				// if there is a valid account number
				modBudgetaryMaster.Statics.CurrentAPEntry = intAccount;
				// gets the Account Number for the item that is double clicked
				SetCombo(intAccount);
				// put the record number into the text box
				StartProgram_6(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, RecordCol)), SearchType == "JournalNum");
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the list of records invisible
		}

        private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsReset = new clsDRWrapper();
			Sys.ClearInstanceOfType(typeof(frmAPDataEntry));
            if (cboEntry.SelectedIndex != 0)
            {
                // if there is a valid account number
                // set status back to entered status so they will be forced to run another warrant preview
                rsReset.Execute("UPDATE APJournal SET Status = 'E' WHERE Status = 'V' AND JournalNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)))), "Budgetary");
                rsReset.Execute("UPDATE JournalMaster SET Status = 'E' WHERE Status = 'V' AND JournalNumber = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)))), "Budgetary");
                modBudgetaryMaster.Statics.CurrentAPEntry = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
                // save the account number
                modRegistry.SaveRegistryKey("CURRAPJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry));
                frmAPDataEntry.InstancePtr.OldJournal = true;
                frmAPDataEntry.InstancePtr.OldJournalNumber = modBudgetaryMaster.Statics.CurrentAPEntry;
                // blnAPEdit = True
            }
            else
            {
                frmAPDataEntry.InstancePtr.OldJournal = false;
                frmAPDataEntry.InstancePtr.OldJournalNumber = 0;
                // blnAPEdit = False
            }
            modBudgetaryMaster.Statics.blnAPEdit = false;
            frmAPDataEntry.InstancePtr.EncumbranceFlag = false;
            frmAPDataEntry.InstancePtr.PurchaseOrderFlag = false;
            frmAPDataEntry.InstancePtr.FromVendorMaster = false;
            frmAPDataEntry.InstancePtr.mnuProcessNextEntry.Enabled = false;
            frmAPDataEntry.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
            frmAPDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = false;
            //JEI: apply MenuItem properties to newly created Form-Buttons
            frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
            frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
            frmAPDataEntry.InstancePtr.apJournalId = 0;
            frmAPDataEntry.InstancePtr.blnJournalEdit = false;
            if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
            {
                // do nothing
            }
            else
            {
                modBudgetaryAccounting.Statics.SearchResults.Reset();
            }
            var apEditViewModel = new cEditAPViewModel();
            apEditViewModel.LoadAPJournalMaster(frmAPDataEntry.InstancePtr.OldJournalNumber);
            if (!apEditViewModel.AllValidated())
            {
                var editAPJournal = new frmEditAPJournal();
                editAPJournal.InitializeScreen(ref apEditViewModel);
            }
            if (!apEditViewModel.Cancelled)
            {
                var apJourn = new cAPJournal();
                apJourn.Description = "";
                apJourn.JournalNumber = apEditViewModel.APJournalInfo.JournalNumber;
                apJourn.Period = FCConvert.ToInt16(apEditViewModel.APJournalInfo.Period);
                apJourn.PayableDate = apEditViewModel.APJournalInfo.PayableDate;
           
                Close();
				frmAPDataEntry.InstancePtr.Init(ref apJourn);
            }
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdReturn_Click(object sender, System.EventArgs e)
		{
			vs1.Clear();
			Frame3.Visible = false;
			// make the listbox invisible
			cmdClear_Click();
			// clear the search
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string SearchCriteria = "";
			bool BadSearchFlag = false;
			//! Load frmWait; // shwo the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Searching";
			frmWait.InstancePtr.Refresh();
			frmWait.InstancePtr.Show();
			if (cmbSearchType.SelectedIndex != 0)
			{
				// make sure a search type is clicked
				if (cmbSearchType.SelectedIndex != 1)
				{
					if (cmbSearchType.SelectedIndex != 2)
					{
						if (cmbSearchType.SelectedIndex != 3)
						{
							if (cmbSearchType.SelectedIndex != 4)
							{
								frmWait.InstancePtr.Unload();
								//Application.DoEvents();
								MessageBox.Show("You must choose a Search Type (Vendor Name, Vendor Number, Journal Number, Reference or Description).", "Search Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							else
							{
								SearchType = "Reference";
							}
						}
						else
						{
							SearchType = "Description";
							// remember which type of search we are doing
						}
					}
					else
					{
						SearchType = "VendorName";
					}
				}
				else
				{
					SearchType = "VendorNum";
				}
			}
			else
			{
				SearchType = "JournalNum";
			}
			if (txtSearch.Text == "")
			{
				// make sure there is some criteria to search for
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("You must type in a search criteria.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				SearchCriteria = txtSearch.Text;
				// remember the search criteria
			}
			if (SearchType == "JournalNum")
			{
				// do the right kind of search
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + SearchCriteria + " and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				rs.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + SearchCriteria + " and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "VendorNum")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM APJournal WHERE VendorNumber = " + SearchCriteria + " and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				rs.OpenRecordset("SELECT * FROM APJournal WHERE VendorNumber = " + SearchCriteria + " and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "VendorName")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0 AND VendorNumber IN (SELECT VendorNumber FROM VendorMaster WHERE CheckName Like '" + modCustomReport.FixQuotes(SearchCriteria) + "%')");
				rs.OpenRecordset("SELECT * FROM APJournal WHERE (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0 AND VendorNumber IN (SELECT VendorNumber FROM VendorMaster WHERE CheckName Like '" + modCustomReport.FixQuotes(SearchCriteria) + "%')");
				// SearchResults.OpenRecordset ("SELECT *, APJournal.Status AS Status FROM APJournal LEFT JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber WHERE VendorMaster.CheckName Like '" & SearchCriteria & "%' and APJournal.Status = 'E'")
				// rs.OpenRecordset ("SELECT * FROM APJournal LEFT JOIN VendorMaster ON APJournal.VendorNumber = VendorMaster.VendorNumber WHERE VendorMaster.CheckName Like '" & SearchCriteria & "%' and APJournal.Status = 'E'")
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else if (SearchType == "Description")
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM APJournal WHERE Description like '" + SearchCriteria + "%' and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				rs.OpenRecordset("SELECT * FROM APJournal WHERE Description like '" + SearchCriteria + "%' and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			else
			{
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM APJournal WHERE Reference like '" + SearchCriteria + "%' and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				rs.OpenRecordset("SELECT * FROM APJournal WHERE Reference like '" + SearchCriteria + "%' and (Status = 'E' or Status = 'V') AND IsNull(CreditMemoRecord, 0) = 0");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					BadSearchFlag = true;
				}
			}
			if (!BadSearchFlag)
			{
				// if there is a record
				rs.MoveLast();
				rs.MoveFirst();
				modBudgetaryAccounting.Statics.SearchResults.MoveLast();
				modBudgetaryAccounting.Statics.SearchResults.MoveFirst();
				// SearchFlag = True
				frmWait.InstancePtr.Unload();
				if (rs.RecordCount() == 1)
				{
					// if there is only 1 record
					frmAPDataEntry.InstancePtr.mnuProcessNextEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = true;
					//JEI: apply MenuItem properties to newly created Form-Buttons
					frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					recordNum = new int[1 + 1];
					recordNum[0] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					cmdClear_Click();
					// clear the search
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					SetCombo(FCConvert.ToInt32(rs.Get_Fields("JournalNumber")));
					// retrieve the info
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					modBudgetaryMaster.Statics.CurrentAPEntry = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
					// save the account number
					StartProgram_6(recordNum[0], SearchType == "JournalNum");
					// call the procedure to retrieve the info
				}
				else
				{
					frmAPDataEntry.InstancePtr.mnuProcessNextEntry.Enabled = true;
					frmAPDataEntry.InstancePtr.mnuProcessPreviousEntry.Enabled = false;
					frmAPDataEntry.InstancePtr.mnuProcessDeleteEntry.Enabled = true;
					//JEI: apply MenuItem properties to newly created Form-Buttons
					frmAPDataEntry.InstancePtr.btnProcessNextEntry.Enabled = true;
					frmAPDataEntry.InstancePtr.btnProcessPreviousEntry.Enabled = false;
					Fill_List(ref SearchType);
					// else show the user all possible records
					frmWait.InstancePtr.Unload();
					Frame3.Visible = true;
					// make the list of records visible
					vs1.Select(1, 5, 1, 0);
					vs1.Focus();
				}
			}
			else
			{
				// else tell them that no records were found that matched the criteria
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				BadSearchFlag = false;
				MessageBox.Show("No Records Found That Match The Criteria", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetAPDataEntry_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmGetAPDataEntry_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Up)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{UP}", false);
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (Frame3.Visible == true)
				{
					KeyCode = (Keys)0;
					vs1.Focus();
					Support.SendKeys("{DOWN}", false);
				}
			}
		}

		private void frmGetAPDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				if (frmGetAPDataEntry.InstancePtr.ActiveControl.GetName() == "vs1")
				{
					// do nothing
				}
				else
				{
					KeyAscii = (Keys)0;
					if (frmGetAPDataEntry.InstancePtr.ActiveControl.GetName() == "cboEntry")
					{
						cmdGetAccountNumber_Click();
						return;
					}
					if (frmGetAPDataEntry.InstancePtr.ActiveControl.GetName() == "txtSearch")
						cmdSearch_Click();
					if (frmGetAPDataEntry.InstancePtr.Visible == true)
					{
						if (frmGetAPDataEntry.InstancePtr.ActiveControl.GetName() == "optSearchType")
						{
							OKFlag = true;
							optSearchType_Click(FCConvert.ToInt16(frmGetAPDataEntry.InstancePtr.ActiveControl.GetIndex()));
						}
					}
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				if (Frame3.Visible == true)
				{
					cmdClear_Click();
					Frame3.Visible = false;
				}
				else
				{
					frmGetAPDataEntry.InstancePtr.Close();
					//MDIParent.InstancePtr.Focus();
				}
			}
			else if (KeyAscii == Keys.F13)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void GetDetails()
		{
			int counter = 0;
			int counter2;
			int ACounter = 0;
			rs3.OpenRecordset("SELECT EncumbranceDetail.Account AS Account, EncumbranceDetail.Amount AS Amount, EncumbranceDetail.Adjustments AS Adjustments, EncumbranceDetail.Liquidated AS Liquidated, EncumbranceDetail.ID AS [ID] FROM (Encumbrances INNER JOIN EncumbranceDetail ON Encumbrances.ID = EncumbranceDetail.EncumbranceID) WHERE Encumbrances.ID = " + rs.Get_Fields_Int32("EncumbranceRecord") + " ORDER BY EncumbranceDetail.ID");
			if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
			{
				rs3.MoveLast();
				rs3.MoveFirst();
				frmAPDataEntry.InstancePtr.EncumbranceFlag = true;
				modBudgetaryMaster.Statics.EncumbranceDetail = new double[rs3.RecordCount() + 1];
				modBudgetaryMaster.Statics.EncumbranceDetailRecords = new int[rs3.RecordCount() + 1];
			}
			else
			{
				frmAPDataEntry.InstancePtr.EncumbranceFlag = false;
			}
			rs2.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + rs.Get_Fields_Int32("ID") + " ORDER BY ID");
			if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
			{
				// if there is any default information
				rs2.MoveLast();
				rs2.MoveFirst();
				if (rs2.RecordCount() > 15)
				{
					frmAPDataEntry.InstancePtr.vs1.Rows = rs2.RecordCount() + 1;
				}
				counter = 1;
				ACounter = 0;
				frmAPDataEntry.InstancePtr.NumberOfEncumbrances = 0;
				while (rs2.EndOfFile() != true)
				{
					// get all the default information there is
                    frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs2.Get_Fields_Int32("ID")));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, FCConvert.ToString(rs2.Get_Fields_String("Description")));
                    frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs2.Get_Fields("account")));
                    frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs2.Get_Fields("1099")));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs2.Get_Fields_String("Project")));
                    frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs2.Get_Fields("Amount")));
                    frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs2.Get_Fields("Discount")));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs2.Get_Fields_Decimal("Encumbrance")));
					if (rs2.Get_Fields_Decimal("Encumbrance") > 0)
					{
						for (counter2 = 1; counter2 <= rs3.RecordCount(); counter2++)
						{
                            if (rs3.Get_Fields("account") == rs2.Get_Fields("account"))
							{
                                modBudgetaryMaster.Statics.EncumbranceDetail[ACounter] = rs3.Get_Fields("Amount") + rs3.Get_Fields_Decimal("Adjustments") - rs3.Get_Fields_Decimal("Liquidated");
								modBudgetaryMaster.Statics.EncumbranceDetailRecords[ACounter] = FCConvert.ToInt32(rs3.Get_Fields_Int32("ID"));
								ACounter += 1;
								frmAPDataEntry.InstancePtr.NumberOfEncumbrances += 1;
								rs3.MoveFirst();
								break;
							}
							else
							{
								rs3.MoveNext();
							}
						}
					}
					rs2.MoveNext();
					counter += 1;
				}
				if (counter <= frmAPDataEntry.InstancePtr.vs1.Rows - 1)
				{
					for (counter = counter; counter <= frmAPDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
						frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
						frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
						frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
					}
				}
			}
			else
			{
				for (counter = 1; counter <= frmAPDataEntry.InstancePtr.vs1.Rows - 1; counter++)
				{
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(0));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(0));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(0));
					frmAPDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(0));
				}
			}
		}

		private void Fill_List(ref string x)
		{
			int I;
			recordNum = new int[rs.RecordCount() + 1];
			vs1.Rows = rs.RecordCount() + 1;
			for (I = 1; I <= rs.RecordCount(); I++)
			{
				// for each record found put info into a listbox
				recordNum[I - 1] = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				if (FCConvert.ToInt32(rs.Get_Fields_Int32("VendorNumber")) != 0)
				{
					rs2.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " + FCConvert.ToString(Conversion.Val(rs.Get_Fields_Int32("VendorNumber"))));
					vs1.TextMatrix(I, RecordCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, JournalCol, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vs1.TextMatrix(I, VendorNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vs1.TextMatrix(I, VendorNameCol, FCConvert.ToString(rs2.Get_Fields_String("CheckName")));
					vs1.TextMatrix(I, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
					vs1.TextMatrix(I, ReferenceCol, FCConvert.ToString(rs.Get_Fields_String("Reference")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, AmountCol, Strings.Format(rs.Get_Fields("Amount"), "#,##0.00"));
				}
				else
				{
					vs1.TextMatrix(I, RecordCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, JournalCol, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vs1.TextMatrix(I, VendorNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vs1.TextMatrix(I, VendorNameCol, FCConvert.ToString(rs.Get_Fields_String("TempVendorName")));
					vs1.TextMatrix(I, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
					vs1.TextMatrix(I, ReferenceCol, FCConvert.ToString(rs.Get_Fields_String("Reference")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vs1.TextMatrix(I, AmountCol, Strings.Format(rs.Get_Fields("Amount"), "#,##0.00"));
				}
				if (I < rs.RecordCount())
					rs.MoveNext();
			}
		}

		private void ResizeSearchGrid()
		{
			int lngGridWidth;
			lngGridWidth = vs1.WidthOriginal;
			vs1.ColWidth(JournalCol, FCConvert.ToInt32(0.06 * lngGridWidth));
			vs1.ColWidth(VendorNumberCol, FCConvert.ToInt32(0.09 * lngGridWidth));
			vs1.ColWidth(VendorNameCol, FCConvert.ToInt32(0.4 * lngGridWidth));
			vs1.ColWidth(DescriptionCol, FCConvert.ToInt32(0.21 * lngGridWidth));
			vs1.ColWidth(ReferenceCol, FCConvert.ToInt32(0.11 * lngGridWidth));
			// vs1.ColWidth(AmountCol) = 0.06 * lngGridWidth
		}

		private void frmGetAPDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetAPDataEntry.ScaleWidth	= 9045;
			//frmGetAPDataEntry.ScaleHeight	= 7290;
			//frmGetAPDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			int counter;
			RecordCol = 0;
			JournalCol = 1;
			VendorNumberCol = 2;
			VendorNameCol = 3;
			DescriptionCol = 4;
			ReferenceCol = 5;
			AmountCol = 6;
			lblLastAccount.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.CurrentAPEntry), 4);
			FillJournals();
			modBudgetaryMaster.Statics.CurrentAPEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRAPJRNL"))));
			vs1.TextMatrix(0, JournalCol, "Jrnl#");
			vs1.TextMatrix(0, VendorNumberCol, "Vendor#");
			vs1.TextMatrix(0, VendorNameCol, "Vendor Name");
			vs1.TextMatrix(0, DescriptionCol, "Description");
			vs1.TextMatrix(0, ReferenceCol, "Reference");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.ColWidth(RecordCol, 0);
			vs1.ColWidth(JournalCol, 800);
			vs1.ColWidth(VendorNumberCol, 1000);
			vs1.ColWidth(VendorNameCol, 3500);
			vs1.ColWidth(DescriptionCol, 3000);
			vs1.ColWidth(ReferenceCol, 1630);
			vs1.ColWidth(AmountCol, 1500);
			vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 6, 4);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetAPDataEntry_Resize(object sender, System.EventArgs e)
		{
			ResizeSearchGrid();
			Frame3.CenterToContainer(this.ClientArea);
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbSearchType.SelectedIndex == 0 || cmbSearchType.SelectedIndex == 1)
			{
				if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			EditFlag = true;
			vs1.Select(vs1.Row, 5, vs1.Row, 0);
			EditFlag = false;
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			int intAccount;
			// if there is a record selected
			intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, JournalCol));
			// get the reocrd ID
			if (intAccount != 0)
			{
				// if it is a valid account number
				modBudgetaryMaster.Statics.CurrentAPEntry = intAccount;
				// gets the Account Number for the item that is double clicked
				SetCombo(intAccount);
				StartProgram_6(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, RecordCol)), SearchType == "JournalNum");
			}
			cmdClear_Click();
			// clear the search
			Frame3.Visible = false;
			// make the listbox invisible
		}

		private void vs1_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Up)
			{
				if (vs1.Row == 1)
				{
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Down)
			{
				if (vs1.Row == vs1.Rows - 1)
				{
					KeyCode = 0;
				}
			}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int intAccount = 0;
			int keyAscii = Strings.Asc(e.KeyChar);
			// if there is a record selected
			if (keyAscii == 13)
			{
				keyAscii = 0;
				intAccount = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, JournalCol));
				// get the reocrd number
				if (intAccount != 0)
				{
					// if it is a valid account number
					modBudgetaryMaster.Statics.CurrentAPEntry = intAccount;
					// gets the Account Number for the item that is double clicked
					SetCombo(intAccount);
					StartProgram_6(FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, RecordCol)), SearchType == "JournalNum");
				}
				cmdClear_Click();
				// clear the search
				Frame3.Visible = false;
				// make the listbox invisible
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (!EditFlag)
			{
				EditFlag = true;
				vs1.Select(vs1.Row, 5, vs1.Row, 0);
				EditFlag = false;
			}
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboEntry.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboEntry.Items[counter].ToString(), 4))
				{
					cboEntry.SelectedIndex = counter;
					return;
				}
			}
		}

		private void txtSearch_Enter(object sender, System.EventArgs e)
		{
			if (cmbSearchType.SelectedIndex != 0 && cmbSearchType.SelectedIndex != 1 && cmbSearchType.SelectedIndex != 2 && cmbSearchType.SelectedIndex != 3 && cmbSearchType.SelectedIndex != 4)
			{
				cmbSearchType.SelectedIndex = 0;
			}
		}

		private void optSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (OKFlag)
			{
				txtSearch.Focus();
				OKFlag = false;
			}
		}

		public void optSearchType_Click(short Index)
		{
			optSearchType_CheckedChanged(Index, cmbSearchType, new System.EventArgs());
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_CheckedChanged(index, sender, e);
		}

		private void optSearchType_MouseDown(int Index, object sender, System.EventArgs e)
		{
			//FC:TODO
			//MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			//int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			//float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
			//float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
			//if (Button == MouseButtonConstants.LeftButton && Shift == 0)
			//{
			//    OKFlag = true;
			//}
		}

		private void optSearchType_MouseDown(object sender, System.EventArgs e)
		{
			int index = cmbSearchType.SelectedIndex;
			optSearchType_MouseDown(index, sender, e);
		}

		public void FillJournals()
		{
			int counter;
            var rsTemp = new clsDRWrapper();

			cboEntry.Clear();
			cboEntry.Items.Insert(0, "0000 - New Journal Entry");
            cboEntry.ItemData(0, 0);
			rs.OpenRecordset("SELECT * FROM JournalMaster WHERE (Status = 'E' or Status = 'V') AND Type = 'AP' ORDER BY JournalNumber DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
                    rsTemp.OpenRecordset("select count(id) as checkcount from apjournal where journalnumber = " + rs.Get_Fields_Int32("JournalNumber") + " and not isnull(checknumber,'') = ''", "Budgetary");
					if (FCConvert.ToInt32(rs.Get_Fields_Int32("Period")) < 10)
					{
						cboEntry.Items.Insert(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("JournalNumber")), 4) + " - " + "Per " + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
					}
					else
					{
						cboEntry.Items.Insert(counter, modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("JournalNumber")), 4) + " - " + "Per " + rs.Get_Fields_Int32("Period") + " - " + rs.Get_Fields_String("Description"));
					}
                    if (rsTemp.Get_Fields_Int32("checkcount") > 0)
                    {
                        cboEntry.ItemData(counter, 0);
                    }
                    else
                    {
                        cboEntry.ItemData(counter, 1);
                    }
					rs.MoveNext();
				}
			}
			cboEntry.SelectedIndex = 0;
		}

        private void CboEntry_Click(object sender, EventArgs e)
        {
           if (cboEntry.ListIndex >= 0)
            {
                if (cboEntry.ItemData(cboEntry.ListIndex) > 0)
                {
                    btnUpdateJournalInformation.Enabled = true;
                }
                else
                {
                    btnUpdateJournalInformation.Enabled = false;
                }
            }
            else
            {
                btnUpdateJournalInformation.Enabled = false;
            }
        }

        private void btnUpdateJournalInformation_Click(object sender, EventArgs e)
        {
            UpdateSelectedJournalInformation();
        }

        private void UpdateSelectedJournalInformation()
        {
            if (cboEntry.ListIndex >= 0)
            {
                if (cboEntry.ItemData(cboEntry.ListIndex) == 1)
                {
                    var apView = new cEditAPViewModel();
                    var journalNumber = 0;                   
                    journalNumber = FCConvert.ToInt32(cboEntry.Items[cboEntry.ListIndex].ToString().Substring(0, 4));
                    apView.LoadAPJournalMaster(journalNumber);
                    apView.AllowEditing();
                    var editAPJournal = new frmEditAPJournal();
                    editAPJournal.InitializeScreen(ref apView);
                }
            }
        }
    }
}
