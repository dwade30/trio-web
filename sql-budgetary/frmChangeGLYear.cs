﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Commands;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmChangeGLYear.
	/// </summary>
	public partial class frmChangeGLYear : BaseForm
	{
		public frmChangeGLYear()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangeGLYear InstancePtr
		{
			get
			{
				return (frmChangeGLYear)Sys.GetInstance(typeof(frmChangeGLYear));
			}
		}

		protected frmChangeGLYear _InstancePtr = null;

		private void frmChangeGLYear_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmChangeGLYear_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			LoadGLYears();
		}

		private void frmChangeGLYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadGLYears()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
			cboGLYear.Clear();
			cboGLYear.AddItem("Current");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				do
				{
					cboGLYear.AddItem(rsTemp.Get_Fields_Int32("ArchiveID").ToString());
					rsTemp.MoveNext();
				}
				while (rsTemp.EndOfFile() != true);
			}
			cboGLYear.SelectedIndex = 0;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cArchiveUtility autPastYear = new cArchiveUtility();
			clsDRWrapper rsChange = new clsDRWrapper();
			clsDRWrapper rsGlobalVariables = new clsDRWrapper();

			if (cboGLYear.SelectedIndex > 0)
			{
				string strTemp = "";
				strTemp = autPastYear.DeriveGroupName("Archive", FCConvert.ToInt16(FCConvert.ToDouble(cboGLYear.Text)));
				rsChange.GroupName = strTemp;
				if (rsChange.DBExists("Budgetary"))
				{
					rsChange.DefaultGroup = strTemp;
					modGlobalConstants.Statics.gstrArchiveYear = cboGLYear.Text;
					App.MainForm.StatusBarText2 = "GL Year: " + modGlobalConstants.Statics.gstrArchiveYear;
					CDBS();
                    modAccountTitle.SetAccountFormats();                   
                    modNewAccountBox.GetFormats();
                    modGlobalConstants.Statics.MuniName = modBudgetaryMaster.Statics.gGlobalVariable.MuniName + "-" +
                                                          modGlobalConstants.Statics.gstrArchiveYear;
                }
				else
				{
					MessageBox.Show("Could not connect to Budgetary archive database for " + cboGLYear.Text, "Bad Connection Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else
			{
				rsChange.DefaultGroup = "Live";
				modGlobalConstants.Statics.gstrArchiveYear = "";
				App.MainForm.StatusBarText2 = "GL Year: Current";
                modAccountTitle.SetAccountFormats();
                modNewAccountBox.GetFormats();
                modGlobalConstants.Statics.MuniName = modBudgetaryMaster.Statics.gGlobalVariable.MuniName;
            }
            App.MainForm.NavigationMenu.Clear();
            MDIParent.InstancePtr.Init();
            StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
            {
	            ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
				ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsChange.GroupName
			});
			Close();
		}

		private void CDBS()
		{
			cBudgetaryDB bDB = new cBudgetaryDB();
			cVersionInfo tVer;
			if (!bDB.CheckVersion())
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(bDB.LastErrorNumber) + "  " + bDB.LastErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			tVer = bDB.GetVersion();
            cCentralDocumentDB cdDB = new cCentralDocumentDB();
            cdDB.CheckVersion();
			cCentralDataDB dataDB = new cCentralDataDB();
            dataDB.CheckVersion();
        }
	}
}
