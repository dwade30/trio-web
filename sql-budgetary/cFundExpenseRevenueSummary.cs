﻿//Fecher vbPorter - Version 1.0.0.27
using System.Collections.Generic;

namespace TWBD0000
{
	public class cFundExpenseRevenueSummary
	{
		//=========================================================
		private double dblExpenses;
		private double dblRevenues;
		private string strFund = string.Empty;
		private Dictionary<object, object> departmentDictionary = new Dictionary<object, object>();

		public Dictionary<object, object> Departments
		{
			get
			{
				Dictionary<object, object> Departments = new Dictionary<object, object>();
				Departments = departmentDictionary;
				return Departments;
			}
		}

		public double ExpenseAmount
		{
			set
			{
				dblExpenses = value;
			}
			get
			{
				double ExpenseAmount = 0;
				ExpenseAmount = dblExpenses;
				return ExpenseAmount;
			}
		}

		public double RevenueAmount
		{
			set
			{
				dblRevenues = value;
			}
			get
			{
				double RevenueAmount = 0;
				RevenueAmount = dblRevenues;
				return RevenueAmount;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}
	}
}
