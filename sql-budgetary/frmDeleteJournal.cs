﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmDeleteJournal.
	/// </summary>
	public partial class frmDeleteJournal : BaseForm
	{
		public frmDeleteJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDeleteJournal InstancePtr
		{
			get
			{
				return (frmDeleteJournal)Sys.GetInstance(typeof(frmDeleteJournal));
			}
		}

		protected frmDeleteJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         6/13/02
		// This form will be used to delete any unposted journals
		// the user might need to get rid of
		// ********************************************************
		clsDRWrapper rsDelete = new clsDRWrapper();

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			string strType;
			int lngRecord = 0;
			clsDRWrapper rsDetail = new clsDRWrapper();
			int intCount;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			string strJournal;
			if (cboJournals.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a journal before you may proceed", "No Journal Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strJournal = Strings.Left(cboJournals.Items[cboJournals.SelectedIndex].ToString(), 4);
			ans = MessageBox.Show("You are about to delete Journal " + strJournal + ".  Do you wish to continue?", "Delete Journal?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.No)
			{
				return;
			}
			modGlobalFunctions.AddCYAEntry_26("BD", "Deleted journal " + strJournal + " on " + DateTime.Today.ToShortDateString(), "Delete Unposted Journal");
			rsDelete.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
			rsDelete.Edit();
			rsDelete.Set_Fields("Status", "D");
			rsDelete.Set_Fields("StatusChangeDate", DateTime.Today);
			rsDelete.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
			rsDelete.Set_Fields("StatusChangeOpID", modBudgetaryAccounting.Statics.User);
			rsDelete.Update();
			intCount = 0;
			// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
			strType = FCConvert.ToString(rsDelete.Get_Fields("Type"));
			if (strType == "EN")
			{
				rsDelete.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				do
				{
					lngRecord = FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID"));
					rsDelete.Delete();
					rsDelete.Update();
					rsDetail.OpenRecordset("SELECT * FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(lngRecord));
					intCount += rsDetail.RecordCount();
					rsDetail.Execute("DELETE FROM EncumbranceDetail WHERE EncumbranceID = " + FCConvert.ToString(lngRecord), "Budgetary");
					// rsDelete.MoveNext
				}
				while (rsDelete.EndOfFile() != true);
			}
			else if (strType == "AP")
			{
				modBudgetaryMaster.RemoveExistingCreditMemos_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				rsDelete.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				do
				{
					lngRecord = FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID"));
					rsDelete.Delete();
					rsDelete.Update();
					rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngRecord));
					intCount += rsDetail.RecordCount();
					rsDetail.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngRecord), "Budgetary");
					// rsDelete.MoveNext
				}
				while (rsDelete.EndOfFile() != true);
			}
			else if (strType == "AC")
			{
				rsDelete.OpenRecordset("SELECT * FROM CreditMemo WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				do
				{
					lngRecord = FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID"));
					rsDelete.Delete();
					rsDelete.Update();
					rsDetail.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE CreditMemoID = " + FCConvert.ToString(lngRecord));
					intCount += rsDetail.RecordCount();
					rsDetail.Execute("DELETE FROM CreditMemoDetail WHERE CreditMemoID = " + FCConvert.ToString(lngRecord), "Budgetary");
					// rsDelete.MoveNext
				}
				while (rsDelete.EndOfFile() != true);
				rsDelete.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'E' AND JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				do
				{
					lngRecord = FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID"));
					rsDelete.Delete();
					rsDelete.Update();
					rsDetail.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngRecord));
					rsDetail.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(lngRecord), "Budgetary");
					// rsDelete.MoveNext
				}
				while (rsDelete.EndOfFile() != true);
			}
			else
			{
				rsDetail.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				intCount += rsDetail.RecordCount();
				rsDetail.Execute("DELETE FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboJournals.Text, 4))), "Budgetary");
			}
			modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(Conversion.Val(Strings.Left(cboJournals.Text, 4))), true);
			MessageBox.Show("Purge Completed Successfully" + "\r\n" + "\r\n" + "     " + FCConvert.ToString(intCount) + " Records Deleted", "Journal Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			SetCombo();
		}

		public void cmdDelete_Click()
		{
			cmdDelete_Click(cmdFilePrint, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmDeleteJournal_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			SetCombo();
			this.Refresh();
		}

		private void cboJournals_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboJournals.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void frmDeleteJournal_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeleteJournal.FillStyle	= 0;
			//frmDeleteJournal.ScaleWidth	= 5880;
			//frmDeleteJournal.ScaleHeight	= 3810;
			//frmDeleteJournal.LinkTopic	= "Form2";
			//frmDeleteJournal.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmDeleteJournal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Focus();
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			cmdDelete_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void SetCombo()
		{
			clsDRWrapper rsDetail = new clsDRWrapper();
			cboJournals.Clear();
			rsDelete.OpenRecordset("SELECT * FROM JournalMaster WHERE (Type <> 'AC' or CreditMemo = 1) AND Status = 'E' ORDER BY JournalNumber");
			if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsDelete.Get_Fields("Type")) == "GJ")
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						rsDetail.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + rsDelete.Get_Fields("JournalNumber") + " AND RCB = 'E'");
						if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							cboJournals.AddItem(Strings.Format(rsDelete.Get_Fields("JournalNumber"), "0000") + "  -  " + rsDelete.Get_Fields_String("Description"));
						}
					}
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					else if (rsDelete.Get_Fields("Type") == "AP")
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						rsDetail.OpenRecordset("SELECT * FROM APJournal WHERE JournalNumber = " + rsDelete.Get_Fields("JournalNumber") + " AND PrintedIndividual = 1");
						if (rsDetail.EndOfFile() != true && rsDetail.BeginningOfFile() != true)
						{
							// do nothing
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							cboJournals.AddItem(Strings.Format(rsDelete.Get_Fields("JournalNumber"), "0000") + "  -  " + rsDelete.Get_Fields_String("Description"));
						}
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						cboJournals.AddItem(Strings.Format(rsDelete.Get_Fields("JournalNumber"), "0000") + "  -  " + rsDelete.Get_Fields_String("Description"));
					}
					rsDelete.MoveNext();
				}
				while (rsDelete.EndOfFile() != true);
				if (cboJournals.Items.Count > 0)
				{
					cboJournals.SelectedIndex = 0;
				}
			}
		}

		private void SetCustomFormColors()
		{
			Label3.ForeColor = Color.Red;
		}
	}
}
