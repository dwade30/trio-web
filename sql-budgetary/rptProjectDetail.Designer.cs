﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptProjectDetail.
	/// </summary>
	partial class rptProjectDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptProjectDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPending = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMonths = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblProjects = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblExplanation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.fldProjectTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ProjectBinder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblProjectTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldProjectDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldProjectCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblProjectSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblProjectedCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldProjectedCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAmountUsed = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAmountUsed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAvailable = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAvailable = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPending)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJournal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDebits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExplanation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectBinder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectDebits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectCredits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectedCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPeriod,
            this.fldJournal,
            this.fldDate,
            this.fldDescription,
            this.fldRCB,
            this.fldType,
            this.fldDebits,
            this.fldCredits,
            this.fldAccount,
            this.fldPending});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldPeriod
            // 
            this.fldPeriod.Height = 0.1875F;
            this.fldPeriod.Left = 0.15625F;
            this.fldPeriod.MultiLine = false;
            this.fldPeriod.Name = "fldPeriod";
            this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; white-space: nowrap; " +
    "ddo-char-set: 1";
            this.fldPeriod.Text = "00";
            this.fldPeriod.Top = 0F;
            this.fldPeriod.Width = 0.3125F;
            // 
            // fldJournal
            // 
            this.fldJournal.Height = 0.1875F;
            this.fldJournal.Left = 0.53125F;
            this.fldJournal.MultiLine = false;
            this.fldJournal.Name = "fldJournal";
            this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; white-space: nowrap; " +
    "ddo-char-set: 1";
            this.fldJournal.Text = "0000";
            this.fldJournal.Top = 0F;
            this.fldJournal.Width = 0.4375F;
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 1.03125F;
            this.fldDate.MultiLine = false;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; white-space: nowrap; " +
    "ddo-char-set: 1";
            this.fldDate.Text = "08/08/05";
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.8125F;
            // 
            // fldDescription
            // 
            this.fldDescription.Height = 0.1875F;
            this.fldDescription.Left = 3.6875F;
            this.fldDescription.MultiLine = false;
            this.fldDescription.Name = "fldDescription";
            this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; white-space: nowrap; ddo-char-set: 1";
            this.fldDescription.Text = "Field36";
            this.fldDescription.Top = 0F;
            this.fldDescription.Width = 1.8125F;
            // 
            // fldRCB
            // 
            this.fldRCB.Height = 0.1875F;
            this.fldRCB.Left = 5.5625F;
            this.fldRCB.MultiLine = false;
            this.fldRCB.Name = "fldRCB";
            this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; white-space: nowrap; " +
    "ddo-char-set: 1";
            this.fldRCB.Text = "Field36";
            this.fldRCB.Top = 0F;
            this.fldRCB.Width = 0.375F;
            // 
            // fldType
            // 
            this.fldType.Height = 0.1875F;
            this.fldType.Left = 6F;
            this.fldType.MultiLine = false;
            this.fldType.Name = "fldType";
            this.fldType.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; white-space: nowrap; " +
    "ddo-char-set: 1";
            this.fldType.Text = "Field36";
            this.fldType.Top = 0F;
            this.fldType.Width = 0.46875F;
            // 
            // fldDebits
            // 
            this.fldDebits.Height = 0.1875F;
            this.fldDebits.Left = 6.53125F;
            this.fldDebits.MultiLine = false;
            this.fldDebits.Name = "fldDebits";
            this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; white-space: nowrap; d" +
    "do-char-set: 1";
            this.fldDebits.Text = "Field36";
            this.fldDebits.Top = 0F;
            this.fldDebits.Width = 1.0625F;
            // 
            // fldCredits
            // 
            this.fldCredits.Height = 0.1875F;
            this.fldCredits.Left = 7.625F;
            this.fldCredits.MultiLine = false;
            this.fldCredits.Name = "fldCredits";
            this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; white-space: nowrap; d" +
    "do-char-set: 1";
            this.fldCredits.Text = "Field36";
            this.fldCredits.Top = 0F;
            this.fldCredits.Width = 1.0625F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 1.90625F;
            this.fldAccount.MultiLine = false;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 12pt; white-space: nowrap; ddo-char-set: 1";
            this.fldAccount.Text = "Field36";
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 1.6875F;
            // 
            // fldPending
            // 
            this.fldPending.Height = 0.1875F;
            this.fldPending.Left = 0.03125F;
            this.fldPending.MultiLine = false;
            this.fldPending.Name = "fldPending";
            this.fldPending.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; wh" +
    "ite-space: nowrap; ddo-char-set: 1";
            this.fldPending.Text = "*";
            this.fldPending.Top = 0F;
            this.fldPending.Visible = false;
            this.fldPending.Width = 0.15625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label7,
            this.Label2,
            this.Label4,
            this.Label3,
            this.lblMonths,
            this.lblProjects,
            this.lblJournal,
            this.Line1,
            this.lblDate,
            this.lblPeriod,
            this.lblDescription,
            this.lblType,
            this.lblRCB,
            this.lblDebits,
            this.lblCredits,
            this.lblAccount});
            this.PageHeader.Height = 0.9270833F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.5F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label1.Text = "Project Detail Report";
            this.Label1.Top = 0F;
            this.Label1.Width = 7.625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Label7.Text = "Label7";
            this.Label7.Top = 0.1875F;
            this.Label7.Width = 1.5F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Label2.Text = "Label2";
            this.Label2.Top = 0F;
            this.Label2.Width = 1.5F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 9.125F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "Label4";
            this.Label4.Top = 0.1875F;
            this.Label4.Width = 1.3125F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 9.125F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "Label3";
            this.Label3.Top = 0F;
            this.Label3.Width = 1.3125F;
            // 
            // lblMonths
            // 
            this.lblMonths.Height = 0.1875F;
            this.lblMonths.HyperLink = null;
            this.lblMonths.Left = 1.46875F;
            this.lblMonths.Name = "lblMonths";
            this.lblMonths.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.lblMonths.Text = "Label5";
            this.lblMonths.Top = 0.40625F;
            this.lblMonths.Width = 7.65625F;
            // 
            // lblProjects
            // 
            this.lblProjects.Height = 0.1875F;
            this.lblProjects.HyperLink = null;
            this.lblProjects.Left = 1.46875F;
            this.lblProjects.Name = "lblProjects";
            this.lblProjects.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
            this.lblProjects.Text = "Label6";
            this.lblProjects.Top = 0.21875F;
            this.lblProjects.Width = 7.65625F;
            // 
            // lblJournal
            // 
            this.lblJournal.Height = 0.1875F;
            this.lblJournal.Left = 0.53125F;
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.lblJournal.Tag = " ";
            this.lblJournal.Text = "Jrnl";
            this.lblJournal.Top = 0.71875F;
            this.lblJournal.Width = 0.4375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.90625F;
            this.Line1.Width = 10.46875F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 10.46875F;
            this.Line1.Y1 = 0.90625F;
            this.Line1.Y2 = 0.90625F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.Left = 1.03125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.lblDate.Tag = " ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 0.71875F;
            this.lblDate.Width = 0.8125F;
            // 
            // lblPeriod
            // 
            this.lblPeriod.Height = 0.1875F;
            this.lblPeriod.Left = 0.15625F;
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.lblPeriod.Tag = " ";
            this.lblPeriod.Text = "Per";
            this.lblPeriod.Top = 0.71875F;
            this.lblPeriod.Width = 0.3125F;
            // 
            // lblDescription
            // 
            this.lblDescription.Height = 0.1875F;
            this.lblDescription.Left = 3.6875F;
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.lblDescription.Tag = " ";
            this.lblDescription.Text = "Description";
            this.lblDescription.Top = 0.71875F;
            this.lblDescription.Width = 1.8125F;
            // 
            // lblType
            // 
            this.lblType.Height = 0.1875F;
            this.lblType.Left = 6F;
            this.lblType.Name = "lblType";
            this.lblType.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.lblType.Tag = " ";
            this.lblType.Text = "Type";
            this.lblType.Top = 0.71875F;
            this.lblType.Width = 0.46875F;
            // 
            // lblRCB
            // 
            this.lblRCB.Height = 0.1875F;
            this.lblRCB.Left = 5.5625F;
            this.lblRCB.Name = "lblRCB";
            this.lblRCB.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.lblRCB.Tag = " ";
            this.lblRCB.Text = "RCB";
            this.lblRCB.Top = 0.71875F;
            this.lblRCB.Width = 0.375F;
            // 
            // lblDebits
            // 
            this.lblDebits.Height = 0.1875F;
            this.lblDebits.Left = 6.53125F;
            this.lblDebits.Name = "lblDebits";
            this.lblDebits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 1";
            this.lblDebits.Tag = " ";
            this.lblDebits.Text = "Debits";
            this.lblDebits.Top = 0.71875F;
            this.lblDebits.Width = 1.0625F;
            // 
            // lblCredits
            // 
            this.lblCredits.Height = 0.1875F;
            this.lblCredits.Left = 7.625F;
            this.lblCredits.Name = "lblCredits";
            this.lblCredits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 1";
            this.lblCredits.Tag = " ";
            this.lblCredits.Text = "Credits";
            this.lblCredits.Top = 0.71875F;
            this.lblCredits.Width = 1.0625F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.Left = 1.90625F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 1";
            this.lblAccount.Tag = " ";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.71875F;
            this.lblAccount.Width = 1.6875F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblExplanation});
            this.PageFooter.Height = 0.2604167F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblExplanation
            // 
            this.lblExplanation.Height = 0.21875F;
            this.lblExplanation.HyperLink = null;
            this.lblExplanation.Left = 4.15625F;
            this.lblExplanation.Name = "lblExplanation";
            this.lblExplanation.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 1";
            this.lblExplanation.Text = "* = Pending Transaction";
            this.lblExplanation.Top = 0.03125F;
            this.lblExplanation.Width = 2.21875F;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldProjectTitle,
            this.ProjectBinder});
            this.GroupHeader1.DataField = "ProjectBinder";
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // fldProjectTitle
            // 
            this.fldProjectTitle.Height = 0.1875F;
            this.fldProjectTitle.Left = 0F;
            this.fldProjectTitle.Name = "fldProjectTitle";
            this.fldProjectTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold";
            this.fldProjectTitle.Text = "Field28";
            this.fldProjectTitle.Top = 0.0625F;
            this.fldProjectTitle.Width = 4.59375F;
            // 
            // ProjectBinder
            // 
            this.ProjectBinder.DataField = "ProjectBinder";
            this.ProjectBinder.Height = 0.09375F;
            this.ProjectBinder.Left = 5.1875F;
            this.ProjectBinder.Name = "ProjectBinder";
            this.ProjectBinder.Text = "Field1";
            this.ProjectBinder.Top = 0.0625F;
            this.ProjectBinder.Visible = false;
            this.ProjectBinder.Width = 0.71875F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line3,
            this.lblProjectTotals,
            this.fldProjectDebits,
            this.fldProjectCredits,
            this.lblProjectSummary,
            this.Line4,
            this.lblProjectedCost,
            this.fldProjectedCost,
            this.lblAmountUsed,
            this.fldAmountUsed,
            this.Line5,
            this.lblAvailable,
            this.fldAvailable});
            this.GroupFooter1.Height = 1.125F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 4.875F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 3.90625F;
            this.Line3.X1 = 8.78125F;
            this.Line3.X2 = 4.875F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // lblProjectTotals
            // 
            this.lblProjectTotals.Height = 0.1875F;
            this.lblProjectTotals.HyperLink = null;
            this.lblProjectTotals.Left = 4.71875F;
            this.lblProjectTotals.Name = "lblProjectTotals";
            this.lblProjectTotals.Style = "text-align: right";
            this.lblProjectTotals.Text = "Project Totals:";
            this.lblProjectTotals.Top = 0.03125F;
            this.lblProjectTotals.Width = 1.78125F;
            // 
            // fldProjectDebits
            // 
            this.fldProjectDebits.Height = 0.1875F;
            this.fldProjectDebits.Left = 6.53125F;
            this.fldProjectDebits.MultiLine = false;
            this.fldProjectDebits.Name = "fldProjectDebits";
            this.fldProjectDebits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; white-space: nowrap; d" +
    "do-char-set: 1";
            this.fldProjectDebits.Text = "Field36";
            this.fldProjectDebits.Top = 0.03125F;
            this.fldProjectDebits.Width = 1.0625F;
            // 
            // fldProjectCredits
            // 
            this.fldProjectCredits.Height = 0.1875F;
            this.fldProjectCredits.Left = 7.59375F;
            this.fldProjectCredits.MultiLine = false;
            this.fldProjectCredits.Name = "fldProjectCredits";
            this.fldProjectCredits.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; white-space: nowrap; d" +
    "do-char-set: 1";
            this.fldProjectCredits.Text = "Field36";
            this.fldProjectCredits.Top = 0.03125F;
            this.fldProjectCredits.Width = 1.0625F;
            // 
            // lblProjectSummary
            // 
            this.lblProjectSummary.Height = 0.1875F;
            this.lblProjectSummary.HyperLink = null;
            this.lblProjectSummary.Left = 4.1875F;
            this.lblProjectSummary.Name = "lblProjectSummary";
            this.lblProjectSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.lblProjectSummary.Text = "Project Summary";
            this.lblProjectSummary.Top = 0.3125F;
            this.lblProjectSummary.Width = 2.21875F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 4.03125F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.5F;
            this.Line4.Width = 2.59375F;
            this.Line4.X1 = 4.03125F;
            this.Line4.X2 = 6.625F;
            this.Line4.Y1 = 0.5F;
            this.Line4.Y2 = 0.5F;
            // 
            // lblProjectedCost
            // 
            this.lblProjectedCost.Height = 0.1875F;
            this.lblProjectedCost.HyperLink = null;
            this.lblProjectedCost.Left = 4.03125F;
            this.lblProjectedCost.Name = "lblProjectedCost";
            this.lblProjectedCost.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.lblProjectedCost.Text = "Projected Cost";
            this.lblProjectedCost.Top = 0.53125F;
            this.lblProjectedCost.Width = 1.3125F;
            // 
            // fldProjectedCost
            // 
            this.fldProjectedCost.Height = 0.1875F;
            this.fldProjectedCost.Left = 5.375F;
            this.fldProjectedCost.Name = "fldProjectedCost";
            this.fldProjectedCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.fldProjectedCost.Text = "Field1";
            this.fldProjectedCost.Top = 0.53125F;
            this.fldProjectedCost.Width = 1.21875F;
            // 
            // lblAmountUsed
            // 
            this.lblAmountUsed.Height = 0.1875F;
            this.lblAmountUsed.HyperLink = null;
            this.lblAmountUsed.Left = 4.03125F;
            this.lblAmountUsed.Name = "lblAmountUsed";
            this.lblAmountUsed.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
            this.lblAmountUsed.Text = "YTD Net";
            this.lblAmountUsed.Top = 0.71875F;
            this.lblAmountUsed.Width = 1.3125F;
            // 
            // fldAmountUsed
            // 
            this.fldAmountUsed.Height = 0.1875F;
            this.fldAmountUsed.Left = 5.375F;
            this.fldAmountUsed.Name = "fldAmountUsed";
            this.fldAmountUsed.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
            this.fldAmountUsed.Text = "Field1";
            this.fldAmountUsed.Top = 0.71875F;
            this.fldAmountUsed.Width = 1.21875F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 4.03125F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 0.90625F;
            this.Line5.Width = 2.59375F;
            this.Line5.X1 = 6.625F;
            this.Line5.X2 = 4.03125F;
            this.Line5.Y1 = 0.90625F;
            this.Line5.Y2 = 0.90625F;
            // 
            // lblAvailable
            // 
            this.lblAvailable.Height = 0.1875F;
            this.lblAvailable.HyperLink = null;
            this.lblAvailable.Left = 4.03125F;
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
            this.lblAvailable.Text = "Available Amount";
            this.lblAvailable.Top = 0.9375F;
            this.lblAvailable.Width = 1.3125F;
            // 
            // fldAvailable
            // 
            this.fldAvailable.Height = 0.1875F;
            this.fldAvailable.Left = 5.375F;
            this.fldAvailable.Name = "fldAvailable";
            this.fldAvailable.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
            this.fldAvailable.Text = "Field2";
            this.fldAvailable.Top = 0.9375F;
            this.fldAvailable.Width = 1.21875F;
            // 
            // rptProjectDetail
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.51042F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.ReportEndedAndCanceled += new System.EventHandler(this.rptProjectDetail_ReportEndedAndCanceled);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPending)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJournal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDebits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExplanation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectBinder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectDebits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectCredits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProjectedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldProjectedCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmountUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPending;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMonths;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjects;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblJournal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExplanation;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox ProjectBinder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjectTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjectSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblProjectedCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProjectedCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmountUsed;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountUsed;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAvailable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAvailable;
	}
}
