﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupAutoEntryReport.
	/// </summary>
	partial class frmSetupAutoEntryReport : BaseForm
	{
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupAutoEntryReport));
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtLowDate = new Global.T2KDateBox();
			this.txtHighDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 383);
			this.BottomPanel.Size = new System.Drawing.Size(398, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdFilePreview);
			this.ClientArea.Controls.Add(this.fraDateRange);
			this.ClientArea.Size = new System.Drawing.Size(398, 323);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(398, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(205, 30);
			this.HeaderText.Text = "Automatic Entries";
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.txtLowDate);
			this.fraDateRange.Controls.Add(this.txtHighDate);
			this.fraDateRange.Controls.Add(this.Label2);
			this.fraDateRange.Location = new System.Drawing.Point(30, 30);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(340, 90);
			this.fraDateRange.TabIndex = 6;
			this.fraDateRange.Text = "Date Range";
			// 
			// txtLowDate
			// 
			this.txtLowDate.Location = new System.Drawing.Point(20, 30);
			this.txtLowDate.Name = "txtLowDate";
			this.txtLowDate.Size = new System.Drawing.Size(120, 40);
			this.txtLowDate.TabIndex = 1;
			// 
			// txtHighDate
			// 
			this.txtHighDate.Location = new System.Drawing.Point(201, 30);
			this.txtHighDate.Name = "txtHighDate";
			this.txtHighDate.Size = new System.Drawing.Size(120, 40);
			this.txtHighDate.TabIndex = 2;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(161, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(20, 16);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(30, 140);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(145, 48);
			this.cmdFilePreview.TabIndex = 5;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmSetupAutoEntryReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(398, 279);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupAutoEntryReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Automatic Entries";
			this.Load += new System.EventHandler(this.frmSetupAutoEntryReport_Load);
			this.Activated += new System.EventHandler(this.frmSetupAutoEntryReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAutoEntryReport_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdFilePreview;
	}
}
