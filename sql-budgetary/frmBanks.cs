﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBankNames.
	/// </summary>
	public partial class frmBankNames : BaseForm
	{
		private string vs1Clip = string.Empty;

		public frmBankNames()
		{
			//
			// Required for Windows Form Designer support
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// FC:FINAL:VGE - #637 Parametered constructor used internaly
		private frmBankNames(string vs1clip)
		{
			this.vs1Clip = vs1clip;
			//
			// Required for Windows Form Designer support
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null || string.IsNullOrEmpty(InstancePtr.vs1Clip))
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBankNames InstancePtr
		{
			get
			{
				// FC:FINAL:VGE - #637 Checking if singletone has vs1clip set up. 
				var frm = (frmBankNames)Sys.GetInstance(typeof(frmBankNames));
				if (string.IsNullOrEmpty(frm.vs1Clip))
				{
					return frm;
			}
				else
				{
					return new frmBankNames(frm.vs1Clip);
		}
			}
		}

		protected frmBankNames _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int NameCol;
		int RoutingCol;
		int BankAccountCol;
		int SchoolCashCol;
		int TownCashCol;
		int AccountTypeCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool blnUnload;

		private void frmBankNames_Activated(object sender, System.EventArgs e)
		{
			int counter;
			string strComboString = "";
			// FC:FINAL:VGE - #637 We are recreating form, so it might already exist.
			/*if (modGlobal.FormExist(this))
            {
                return;
            }*/
			NameCol = 1;
			RoutingCol = 2;
			BankAccountCol = 3;
			TownCashCol = 4;
			SchoolCashCol = 5;
			AccountTypeCol = 6;
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vs1.ColAllowedKeys(TownCashCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			vs1.ColAllowedKeys(SchoolCashCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.07));
			vs1.ColWidth(NameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.35));
			vs1.ColWidth(RoutingCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(BankAccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(TownCashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(SchoolCashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(AccountTypeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.EditMaxLength = 30;
			// format flexgrid
			for (counter = 1; counter <= 99; counter++)
			{
				vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
			}
			vs1.TextMatrix(0, 0, "Bank #");
			vs1.TextMatrix(0, NameCol, "Name");
			vs1.TextMatrix(0, RoutingCol, "Routing #");
			vs1.TextMatrix(0, BankAccountCol, "Bank Account #");
			vs1.TextMatrix(0, TownCashCol, "Town Cash");
			vs1.TextMatrix(0, SchoolCashCol, "Sch Cash");
			vs1.TextMatrix(0, AccountTypeCol, "Account Type");
			vs1.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(RoutingCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(BankAccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(TownCashCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(SchoolCashCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColComboList(AccountTypeCol, "Checking|Savings");

			vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, SchoolCashCol, vs1.Rows - 1, SchoolCashCol, 0x808080);

			// FC:FINAL:VGE - #637 Sourcing grid values from saved Clip if possible
			if (!string.IsNullOrWhiteSpace(this.vs1Clip))
			{
				vs1.TextMatrixClip = this.vs1Clip;
				this.vs1Clip = null;
			}
			else
			{
				// open the database and put bank information into the grid
				rs.OpenRecordset("SELECT * FROM Banks ORDER BY BankNumber");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					for (counter = 1; counter <= 99; counter++)
					{
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rs.Get_Fields("BankNumber")) == counter)
						{
							// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
							vs1.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields("BankNumber")));
							vs1.TextMatrix(counter, NameCol, FCConvert.ToString(rs.Get_Fields_String("Name")));
							vs1.TextMatrix(counter, RoutingCol, FCConvert.ToString(rs.Get_Fields_String("RoutingNumber")));
							vs1.TextMatrix(counter, BankAccountCol, FCConvert.ToString(rs.Get_Fields_String("BankAccountNumber")));

							vs1.TextMatrix(counter, TownCashCol, FCConvert.ToString(rs.Get_Fields_String("TownCashAccount")));
							vs1.TextMatrix(counter, SchoolCashCol, "");

							if (FCConvert.ToString(rs.Get_Fields_String("AccountType")) == "S")
							{
								vs1.TextMatrix(counter, AccountTypeCol, "Savings");
							}
							else
							{
								vs1.TextMatrix(counter, AccountTypeCol, "Checking");
							}
							if (counter < 99)
							{
								rs.MoveNext();
								if (rs.EndOfFile())
								{
									break;
								}
							}
						}
						else
						{
							vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
						}
					}
					if (counter < 99)
					{
						for (counter = counter; counter <= 99; counter++)
						{
							vs1.TextMatrix(counter, 0, FCConvert.ToString(counter));
						}
					}
				}
			}
			Form_Resize();
			vs1.Select(1, NameCol);
			vs1.EditCell();
			vs1.EditSelStart = 0;
			this.Refresh();
		}

		private void frmBankNames_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmBankNames_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBankNames.FillStyle	= 0;
			//frmBankNames.ScaleWidth	= 9045;
			//frmBankNames.ScaleHeight	= 7245;
			//frmBankNames.LinkTopic	= "Form2";
			//frmBankNames.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmBankNames_Resize(object sender, System.EventArgs e)
		{
			//vs1.Height = vs1.RowHeight(0) * 10 + 75;
			//vs1.Height = 432;
			vs1.ColWidth(0, FCConvert.ToInt32(vs1.WidthOriginal * 0.07));
			vs1.ColWidth(NameCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.35));
			vs1.ColWidth(RoutingCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(BankAccountCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
			vs1.ColWidth(TownCashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(SchoolCashCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.12));
			vs1.ColWidth(AccountTypeCol, FCConvert.ToInt32(vs1.WidthOriginal * 0.15));
		}

		public void Form_Resize()
		{
			frmBankNames_Resize(this, new System.EventArgs());
		}

		private void frmBankNames_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			SaveInfo();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			SaveInfo();
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, vs1.Col)) == 0x808080)
			{
				vs1.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				if (vs1.Col == TownCashCol)
				{
					vs1.EditMaxLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))));
				}
				else if (vs1.Col == SchoolCashCol)
				{
					vs1.EditMaxLength = 4;
				}
				else
				{
					vs1.EditMaxLength = 30;
				}
				vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			if (vs1.Row > 0)
			{
				if (Strings.Trim(vs1.TextMatrix(vs1.Row, NameCol)) != "")
				{
					// FC:FINAL:VGE - #637 Saving grid's Clip for later usage.
					this.vs1Clip = vs1.TextMatrixClip;
					//FC:FINA:KV:IIT807+FC-8697
					//FC:FINAL:DDU:#2958 - fixed show correct tab
					//this.Hide();
					frmEditCashAccounts.InstancePtr.lngBankNumber = FCConvert.ToInt32(vs1.TextMatrix(vs1.Row, 0));
					frmEditCashAccounts.InstancePtr.Show(App.MainForm);
					this.Close();
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Return)
			{
				KeyCode = 0;
				Support.SendKeys("{DOWN}", false);
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (vs1.Col == TownCashCol || vs1.Col == SchoolCashCol)
			{
				if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
				{
					keyAscii = 0;
				}
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (FCConvert.ToInt32(vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vs1.Row, vs1.Col)) == 0x808080)
			{
				vs1.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else
			{
				if (vs1.Col == TownCashCol)
				{
					vs1.EditMaxLength = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))));
				}
				else if (vs1.Col == SchoolCashCol)
				{
					vs1.EditMaxLength = 4;
				}
				else
				{
					vs1.EditMaxLength = 30;
				}
				vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vs1.EditCell();
				vs1.EditSelStart = 0;
			}
		}

		private void SaveInfo()
		{
			int counter;
			int counter2;
			rs.OmitNullsOnInsert = true;
			vs1.Select(1, NameCol);
			rs.OpenRecordset("SELECT * FROM Banks ORDER BY BankNumber");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (counter = 1; counter <= 99; counter++)
				{
					rs.Edit();
					rs.Set_Fields("BankNumber", vs1.TextMatrix(counter, 0));
					rs.Set_Fields("Name", vs1.TextMatrix(counter, NameCol));
					rs.Set_Fields("RoutingNumber", vs1.TextMatrix(counter, RoutingCol));
					rs.Set_Fields("BankAccountNumber", vs1.TextMatrix(counter, BankAccountCol));
					rs.Set_Fields("TownCashAccount", vs1.TextMatrix(counter, TownCashCol));
					rs.Set_Fields("SchoolCashAccount", vs1.TextMatrix(counter, SchoolCashCol));
					if (vs1.TextMatrix(counter, AccountTypeCol) == "Savings")
					{
						rs.Set_Fields("AccountType", "S");
					}
					else
					{
						rs.Set_Fields("AccountType", "C");
					}
					rs.Update();
					rs.MoveNext();
					if (rs.EndOfFile())
					{
						counter += 1;
						break;
					}
				}
				if (counter < 99)
				{
					for (counter = counter; counter <= 99; counter++)
					{
						rs.AddNew();
						rs.Set_Fields("BankNumber", vs1.TextMatrix(counter, 0));
						rs.Set_Fields("Name", vs1.TextMatrix(counter, NameCol));
						rs.Set_Fields("RoutingNumber", vs1.TextMatrix(counter, RoutingCol));
						rs.Set_Fields("BankAccountNumber", vs1.TextMatrix(counter, BankAccountCol));
						rs.Set_Fields("TownCashAccount", vs1.TextMatrix(counter, TownCashCol));
						rs.Set_Fields("SchoolCashAccount", vs1.TextMatrix(counter, SchoolCashCol));
						if (vs1.TextMatrix(counter, AccountTypeCol) == "Savings")
						{
							rs.Set_Fields("AccountType", "S");
						}
						else
						{
							rs.Set_Fields("AccountType", "C");
						}
						rs.Update();
					}
				}
			}
			else
			{
				for (counter = 1; counter <= 99; counter++)
				{
					rs.AddNew();
					rs.Set_Fields("BankNumber", vs1.TextMatrix(counter, 0));
					rs.Set_Fields("Name", vs1.TextMatrix(counter, NameCol));
					rs.Set_Fields("RoutingNumber", vs1.TextMatrix(counter, RoutingCol));
					rs.Set_Fields("BankAccountNumber", vs1.TextMatrix(counter, BankAccountCol));
					rs.Set_Fields("TownCashAccount", vs1.TextMatrix(counter, TownCashCol));
					rs.Set_Fields("SchoolCashAccount", vs1.TextMatrix(counter, SchoolCashCol));
					if (vs1.TextMatrix(counter, AccountTypeCol) == "Savings")
					{
						rs.Set_Fields("AccountType", "S");
					}
					else
					{
						rs.Set_Fields("AccountType", "C");
					}
					rs.Update();
				}
			}
			if (blnUnload)
			{
				Close();
			}
			else
			{
				MessageBox.Show("Save successful!", "Information Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		//FC:FINAL:CHN: Allow to input numbers at Grid columns on client side (javascript).
		private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (vs1.Col == TownCashCol || vs1.Col == SchoolCashCol)
			{
                vs1.EditingControl.RemoveAlphaCharactersOnValueChange();
                vs1.EditingControl.AllowOnlyNumericInput();
			}
		}
	}
}
