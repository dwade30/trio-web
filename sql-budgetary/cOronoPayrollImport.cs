﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;

namespace TWBD0000
{
	public class cOronoPayrollImport
	{
		//=========================================================
		// Private Const CNSTDateCol = 0
		const int CNSTAccountCol = 0;
		const int CNSTDescriptionCol = 1;
		const int CNSTDebitCol = 2;
		const int CNSTCreditCol = 3;
		const int CNSTMemoCol = 1;
		private string strBadAccounts = "";
		private string strErrors = "";
		private int lngLastJournal;

		public string BadAccounts
		{
			get
			{
				string BadAccounts = "";
				BadAccounts = strBadAccounts;
				return BadAccounts;
			}
		}

		public string Errors
		{
			get
			{
				string Errors = "";
				Errors = strErrors;
				return Errors;
			}
		}

		public int LastJournal
		{
			get
			{
				int LastJournal = 0;
				LastJournal = lngLastJournal;
				return LastJournal;
			}
		}

		private void AddError(string strError)
		{
			if (Strings.Trim(strErrors) == "")
			{
				strErrors = strError;
			}
			else
			{
				strErrors += "\r\n" + strError;
			}
		}

		public bool ImportPayrollJournalEntries(string strFileName)
		{
			bool ImportPayrollJournalEntries = false;
			ImportPayrollJournalEntries = false;
			StreamReader ts = null;
			bool boolOpen = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngJournalNumber;
				clsDRWrapper Master = new clsDRWrapper();
				lngLastJournal = 0;
				boolOpen = false;
				strBadAccounts = "";
				strErrors = "";
				if (!File.Exists(strFileName))
				{
					AddError("Could not find file " + strFileName);
					return ImportPayrollJournalEntries;
				}
				lngJournalNumber = 0;
				ts = File.OpenText(strFileName);
				boolOpen = true;
				string strLine = "";
				string[] aryLine = null;
				string strAccount = "";
				double dblCredit = 0;
				double dblDebit = 0;
				int intLine;
				string strDate;
				int intPeriod = 0;
				FCCollection colEntries = new FCCollection();
				string strJournalDescription;
				intLine = 0;
				strJournalDescription = "Payroll Import";
				if (!ts.EndOfStream)
				{
					// skip header
					strLine = ts.ReadLine();
					intLine += 1;
				}
				if (!ts.EndOfStream)
				{
					strLine = ts.ReadLine();
					aryLine = Strings.Split(strLine, ",", -1, CompareConstants.vbBinaryCompare);
					if (Information.UBound(aryLine, 1) > 0)
					{
						strJournalDescription = aryLine[1];
					}
					intLine += 1;
				}
				if (!ts.EndOfStream)
				{
					// skip header
					strLine = ts.ReadLine();
					intLine += 1;
				}
				cJournalEntryInfo tEntry;
				strDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				while (!ts.EndOfStream)
				{
					strLine = ts.ReadLine();
					intLine += 1;
					if (Strings.Trim(strLine) != "")
					{
						strAccount = "";
						dblCredit = 0;
						dblDebit = 0;
						aryLine = Strings.Split(strLine, ",", -1, CompareConstants.vbBinaryCompare);
						if (Information.UBound(aryLine, 1) >= 3)
						{
							strAccount = aryLine[CNSTAccountCol];
							strAccount = strAccount.Replace(".", "-");
							if (Strings.Trim(aryLine[CNSTDebitCol]) != "")
							{
								dblDebit = FCConvert.ToDouble(aryLine[CNSTDebitCol]);
							}
							if (Strings.Trim(aryLine[CNSTCreditCol]) != "")
							{
								dblCredit = FCConvert.ToDouble(aryLine[CNSTCreditCol]);
							}
							if ((dblDebit != 0 || dblCredit != 0) && Strings.Trim(strAccount) == "")
							{
								AddError("Missing Account in line " + FCConvert.ToString(intLine));
								return ImportPayrollJournalEntries;
							}
							if (!modValidateAccount.AccountValidate(Strings.Trim(strAccount)))
							{
								strBadAccounts += Strings.Trim(strAccount) + "\r\n";
							}
							if (dblDebit != 0)
							{
								tEntry = new cJournalEntryInfo();
								tEntry.Account = strAccount;
								tEntry.Description = Strings.Trim(aryLine[CNSTDescriptionCol]);
								tEntry.EntryDate = strDate;
								tEntry.EntryType = "G";
								tEntry.Amount = dblDebit;
								colEntries.Add(tEntry);
							}
							if (dblCredit != 0)
							{
								tEntry = new cJournalEntryInfo();
								tEntry.Account = strAccount;
								tEntry.Description = Strings.Trim(aryLine[CNSTDescriptionCol]);
								tEntry.EntryDate = strDate;
								tEntry.EntryType = "G";
								tEntry.Amount = -1 * dblCredit;
								colEntries.Add(tEntry);
							}
						}
					}
				}
				ts.Close();
				boolOpen = false;
				if (colEntries.Count > 0)
				{
					intPeriod = DateTime.Today.Month;
					// get journal number
					if (modBudgetaryAccounting.LockJournal() == false)
					{
						AddError("Could not create journal");
					}
					else
					{
						Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
						if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
						{
							Master.MoveLast();
							Master.MoveFirst();
							// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							lngJournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
						}
						else
						{
							lngJournalNumber = 1;
						}
						lngLastJournal = lngJournalNumber;
						Master.AddNew();
						Master.Set_Fields("JournalNumber", lngJournalNumber);
						Master.Set_Fields("Status", "E");
						Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						Master.Set_Fields("StatusChangeDate", DateTime.Today);
						// Master.Fields["Description"] = "Payroll Import"
						Master.Set_Fields("Description", strJournalDescription);
						Master.Set_Fields("Type", "GJ");
						Master.Set_Fields("Period", intPeriod);
						// Month(CDate(strCheckInfo(4)))
						Master.Update();
						Master.Reset();
						modBudgetaryAccounting.UnlockJournal();
						clsDRWrapper rsEntries = new clsDRWrapper();
						rsEntries.OmitNullsOnInsert = true;
						rsEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
						foreach (cJournalEntryInfo jEntry in colEntries)
						{
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", jEntry.EntryType);
							rsEntries.Set_Fields("JournalEntriesDate", jEntry.EntryDate);
							rsEntries.Set_Fields("Description", "Payroll Import");
							rsEntries.Set_Fields("JournalNumber", lngJournalNumber);
							rsEntries.Set_Fields("Account", jEntry.Account);
							rsEntries.Set_Fields("Amount", jEntry.Amount);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", intPeriod);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
					ImportPayrollJournalEntries = true;
				}
				else
				{
					AddError("No entries found");
				}
				return ImportPayrollJournalEntries;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolOpen)
				{
					ts.Close();
				}
				AddError("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description);
			}
			return ImportPayrollJournalEntries;
		}
	}
}
