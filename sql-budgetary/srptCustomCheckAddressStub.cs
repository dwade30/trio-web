﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.IO;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckAddressStub.
	/// </summary>
	public partial class srptCustomCheckAddressStub : FCSectionReport
	{
		public static srptCustomCheckAddressStub InstancePtr
		{
			get
			{
				return (srptCustomCheckAddressStub)Sys.GetInstance(typeof(srptCustomCheckAddressStub));
			}
		}

		protected srptCustomCheckAddressStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsIcon.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCustomCheckAddressStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsIcon = new clsDRWrapper();

		public srptCustomCheckAddressStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//object lblReturnName, lblReturnAddress1, lblReturnAddress2, lblReturnAddress3;  // - "AutoDim"
			rsIcon.OpenRecordset("SELECT * FROM Budgetary");
			if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("ShowIcon")))
			{
				if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("UseTownSeal")))
				{
					if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
					{
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic"))
						{
							imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic");
						}
					}
					else
					{
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic"))
						{
							imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic");
						}
					}
				}
				else
				{
					if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
					{
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon")))
						{
							imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon"));
						}
					}
					else
					{
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon")))
						{
							imgIcon.Image = FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" + rsIcon.Get_Fields_String("CheckIcon"));
						}
					}
				}
			}
			lblReturnName.Text = rsIcon.Get_Fields_String("ReturnAddress1");
			lblReturnAddress1.Text = rsIcon.Get_Fields_String("ReturnAddress2");
			lblReturnAddress2.Text = rsIcon.Get_Fields_String("ReturnAddress3");
			lblReturnAddress3.Text = rsIcon.Get_Fields_String("ReturnAddress4");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldVendorName.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterVendorName);
			fldCheckNumber.Text = FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterCheck);
			fldAddress1.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress1);
			fldAddress2.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress2);
			fldAddress3.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress3);
			fldAddress4.Text = Strings.Trim(rptCustomCheck.InstancePtr.strFooterAddress4);
			if (Strings.Trim(fldAddress3.Text) == "")
			{
				fldAddress3.Text = fldAddress4.Text;
				fldAddress4.Text = "";
			}
			if (Strings.Trim(fldAddress2.Text) == "")
			{
				fldAddress2.Text = fldAddress3.Text;
				fldAddress3.Text = "";
			}
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				fldAddress1.Text = fldAddress2.Text;
				fldAddress2.Text = "";
			}
		}

		
	}
}
