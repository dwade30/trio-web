﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCheckListSelection.
	/// </summary>
	partial class frmCheckListSelection : BaseForm
	{
		public fecherFoundation.FCComboBox cmbStatusSelected;
		public fecherFoundation.FCComboBox cmbTypeAll;
		public fecherFoundation.FCCheckBox chkOrderByCheckNumber;
		public fecherFoundation.FCTextBox txtLowCheck;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraStatusOptions;
		public fecherFoundation.FCCheckBox chkDeleted;
		public fecherFoundation.FCCheckBox chkVoided;
		public fecherFoundation.FCCheckBox chkCashed;
		public fecherFoundation.FCCheckBox chkOutstanding;
		public fecherFoundation.FCCheckBox chkIssued;
		public fecherFoundation.FCFrame fraAccountType;
		public fecherFoundation.FCFrame fraTypeOptions;
		public fecherFoundation.FCCheckBox chkAP;
		public fecherFoundation.FCCheckBox chkPY;
		public fecherFoundation.FCCheckBox chkDP;
		public fecherFoundation.FCCheckBox chkRT;
		public fecherFoundation.FCCheckBox chkIN;
		public fecherFoundation.FCCheckBox chkOC;
		public fecherFoundation.FCCheckBox chkOD;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckListSelection));
			this.cmbStatusSelected = new fecherFoundation.FCComboBox();
			this.cmbTypeAll = new fecherFoundation.FCComboBox();
			this.chkOrderByCheckNumber = new fecherFoundation.FCCheckBox();
			this.txtLowCheck = new fecherFoundation.FCTextBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.fraStatusOptions = new fecherFoundation.FCFrame();
			this.chkDeleted = new fecherFoundation.FCCheckBox();
			this.chkVoided = new fecherFoundation.FCCheckBox();
			this.chkCashed = new fecherFoundation.FCCheckBox();
			this.chkOutstanding = new fecherFoundation.FCCheckBox();
			this.chkIssued = new fecherFoundation.FCCheckBox();
			this.fraAccountType = new fecherFoundation.FCFrame();
			this.fraTypeOptions = new fecherFoundation.FCFrame();
			this.chkAP = new fecherFoundation.FCCheckBox();
			this.chkPY = new fecherFoundation.FCCheckBox();
			this.chkIN = new fecherFoundation.FCCheckBox();
			this.chkOD = new fecherFoundation.FCCheckBox();
			this.chkDP = new fecherFoundation.FCCheckBox();
			this.chkOC = new fecherFoundation.FCCheckBox();
			this.chkRT = new fecherFoundation.FCCheckBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOrderByCheckNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusOptions)).BeginInit();
			this.fraStatusOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVoided)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCashed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOutstanding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).BeginInit();
			this.fraAccountType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraTypeOptions)).BeginInit();
			this.fraTypeOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(698, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkOrderByCheckNumber);
			this.ClientArea.Controls.Add(this.txtLowCheck);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.fraAccountType);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(698, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(698, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(229, 30);
			this.HeaderText.Text = "Select Transactions";
			// 
			// cmbStatusSelected
			// 
			this.cmbStatusSelected.AutoSize = false;
			this.cmbStatusSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStatusSelected.FormattingEnabled = true;
			this.cmbStatusSelected.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbStatusSelected.Location = new System.Drawing.Point(20, 30);
			this.cmbStatusSelected.Name = "cmbStatusSelected";
			this.cmbStatusSelected.Size = new System.Drawing.Size(130, 40);
			this.cmbStatusSelected.TabIndex = 15;
			this.cmbStatusSelected.SelectedIndexChanged += new System.EventHandler(this.optStatusSelected_CheckedChanged);
			// 
			// cmbTypeAll
			// 
			this.cmbTypeAll.AutoSize = false;
			this.cmbTypeAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTypeAll.FormattingEnabled = true;
			this.cmbTypeAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbTypeAll.Location = new System.Drawing.Point(20, 30);
			this.cmbTypeAll.Name = "cmbTypeAll";
			this.cmbTypeAll.Size = new System.Drawing.Size(130, 40);
			this.cmbTypeAll.TabIndex = 4;
			this.cmbTypeAll.SelectedIndexChanged += new System.EventHandler(this.optTypeSelected_CheckedChanged);
			// 
			// chkOrderByCheckNumber
			// 
			this.chkOrderByCheckNumber.Location = new System.Drawing.Point(30, 436);
			this.chkOrderByCheckNumber.Name = "chkOrderByCheckNumber";
			this.chkOrderByCheckNumber.Size = new System.Drawing.Size(207, 27);
			this.chkOrderByCheckNumber.TabIndex = 24;
			this.chkOrderByCheckNumber.Text = "Order By Check Number";
			// 
			// txtLowCheck
			// 
			this.txtLowCheck.AutoSize = false;
			this.txtLowCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtLowCheck.Location = new System.Drawing.Point(30, 66);
			this.txtLowCheck.Name = "txtLowCheck";
			this.txtLowCheck.Size = new System.Drawing.Size(74, 40);
			this.txtLowCheck.TabIndex = 22;
			this.txtLowCheck.Text = "0";
			this.txtLowCheck.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtLowCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLowCheck_KeyPress);
			this.txtLowCheck.Validating += new System.ComponentModel.CancelEventHandler(this.txtLowCheck_Validating);
			// 
			// Frame1
			// 
			this.Frame1.BackColor = System.Drawing.Color.FromName("@window");
			this.Frame1.Controls.Add(this.fraStatusOptions);
			this.Frame1.Controls.Add(this.cmbStatusSelected);
			this.Frame1.Location = new System.Drawing.Point(397, 126);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(170, 280);
			this.Frame1.TabIndex = 11;
			this.Frame1.Text = "Transaction Status";
			// 
			// fraStatusOptions
			// 
			this.fraStatusOptions.AppearanceKey = "groupBoxNoBorders";
			this.fraStatusOptions.Controls.Add(this.chkDeleted);
			this.fraStatusOptions.Controls.Add(this.chkVoided);
			this.fraStatusOptions.Controls.Add(this.chkCashed);
			this.fraStatusOptions.Controls.Add(this.chkOutstanding);
			this.fraStatusOptions.Controls.Add(this.chkIssued);
			this.fraStatusOptions.Enabled = false;
			this.fraStatusOptions.Location = new System.Drawing.Point(0, 70);
			this.fraStatusOptions.Name = "fraStatusOptions";
			this.fraStatusOptions.Size = new System.Drawing.Size(140, 210);
			this.fraStatusOptions.TabIndex = 14;
			// 
			// chkDeleted
			// 
			this.chkDeleted.Location = new System.Drawing.Point(20, 164);
			this.chkDeleted.Name = "chkDeleted";
			this.chkDeleted.Size = new System.Drawing.Size(84, 27);
			this.chkDeleted.TabIndex = 19;
			this.chkDeleted.Text = "Deleted";
			// 
			// chkVoided
			// 
			this.chkVoided.Location = new System.Drawing.Point(20, 128);
			this.chkVoided.Name = "chkVoided";
			this.chkVoided.Size = new System.Drawing.Size(78, 27);
			this.chkVoided.TabIndex = 18;
			this.chkVoided.Text = "Voided";
			// 
			// chkCashed
			// 
			this.chkCashed.Location = new System.Drawing.Point(20, 92);
			this.chkCashed.Name = "chkCashed";
			this.chkCashed.Size = new System.Drawing.Size(84, 27);
			this.chkCashed.TabIndex = 17;
			this.chkCashed.Text = "Cashed";
			// 
			// chkOutstanding
			// 
			this.chkOutstanding.Location = new System.Drawing.Point(20, 56);
			this.chkOutstanding.Name = "chkOutstanding";
			this.chkOutstanding.Size = new System.Drawing.Size(116, 27);
			this.chkOutstanding.TabIndex = 16;
			this.chkOutstanding.Text = "Outstanding";
			// 
			// chkIssued
			// 
			this.chkIssued.Location = new System.Drawing.Point(20, 20);
			this.chkIssued.Name = "chkIssued";
			this.chkIssued.Size = new System.Drawing.Size(75, 27);
			this.chkIssued.TabIndex = 15;
			this.chkIssued.Text = "Issued";
			// 
			// fraAccountType
			// 
			this.fraAccountType.BackColor = System.Drawing.Color.FromName("@window");
			this.fraAccountType.Controls.Add(this.fraTypeOptions);
			this.fraAccountType.Controls.Add(this.cmbTypeAll);
			this.fraAccountType.Location = new System.Drawing.Point(30, 126);
			this.fraAccountType.Name = "fraAccountType";
			this.fraAccountType.Size = new System.Drawing.Size(347, 280);
			this.fraAccountType.TabIndex = 0;
			this.fraAccountType.Text = "Transaction Type";
			// 
			// fraTypeOptions
			// 
			this.fraTypeOptions.AppearanceKey = "groupBoxNoBorders";
			this.fraTypeOptions.Controls.Add(this.chkAP);
			this.fraTypeOptions.Controls.Add(this.chkPY);
			this.fraTypeOptions.Controls.Add(this.chkIN);
			this.fraTypeOptions.Controls.Add(this.chkOD);
			this.fraTypeOptions.Controls.Add(this.chkDP);
			this.fraTypeOptions.Controls.Add(this.chkOC);
			this.fraTypeOptions.Controls.Add(this.chkRT);
			this.fraTypeOptions.Enabled = false;
			this.fraTypeOptions.Location = new System.Drawing.Point(0, 70);
			this.fraTypeOptions.Name = "fraTypeOptions";
			this.fraTypeOptions.Size = new System.Drawing.Size(347, 210);
			this.fraTypeOptions.TabIndex = 3;
			// 
			// chkAP
			// 
			this.chkAP.Location = new System.Drawing.Point(20, 20);
			this.chkAP.Name = "chkAP";
			this.chkAP.Size = new System.Drawing.Size(159, 27);
			this.chkAP.TabIndex = 10;
			this.chkAP.Text = "Accounts Payable";
			// 
			// chkPY
			// 
			this.chkPY.Location = new System.Drawing.Point(20, 56);
			this.chkPY.Name = "chkPY";
			this.chkPY.Size = new System.Drawing.Size(77, 27);
			this.chkPY.TabIndex = 9;
			this.chkPY.Text = "Payroll";
			// 
			// chkIN
			// 
			this.chkIN.Location = new System.Drawing.Point(20, 164);
			this.chkIN.Name = "chkIN";
			this.chkIN.Size = new System.Drawing.Size(82, 27);
			this.chkIN.TabIndex = 6;
			this.chkIN.Text = "Interest";
			// 
			// chkOD
			// 
			this.chkOD.Location = new System.Drawing.Point(200, 56);
			this.chkOD.Name = "chkOD";
			this.chkOD.Size = new System.Drawing.Size(119, 27);
			this.chkOD.TabIndex = 4;
			this.chkOD.Text = "Other Debits";
			// 
			// chkDP
			// 
			this.chkDP.Location = new System.Drawing.Point(20, 92);
			this.chkDP.Name = "chkDP";
			this.chkDP.Size = new System.Drawing.Size(91, 27);
			this.chkDP.TabIndex = 8;
			this.chkDP.Text = "Deposits";
			// 
			// chkOC
			// 
			this.chkOC.Location = new System.Drawing.Point(200, 20);
			this.chkOC.Name = "chkOC";
			this.chkOC.Size = new System.Drawing.Size(125, 27);
			this.chkOC.TabIndex = 5;
			this.chkOC.Text = "Other Credits";
			// 
			// chkRT
			// 
			this.chkRT.Location = new System.Drawing.Point(20, 128);
			this.chkRT.Name = "chkRT";
			this.chkRT.Size = new System.Drawing.Size(154, 27);
			this.chkRT.TabIndex = 7;
			this.chkRT.Text = "Returned Checks";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(520, 16);
			this.Label1.TabIndex = 21;
			this.Label1.Text = "PLEASE INPUT THE LOWEST CHECK NUMBER YOU WOULD LIKE DISPLAYED ON THIS REPORT";
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(292, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(147, 48);
			this.cmdFilePreview.TabIndex = 6;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmCheckListSelection
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(698, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCheckListSelection";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Transactions";
			this.Load += new System.EventHandler(this.frmCheckListSelection_Load);
			this.Activated += new System.EventHandler(this.frmCheckListSelection_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCheckListSelection_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOrderByCheckNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraStatusOptions)).EndInit();
			this.fraStatusOptions.ResumeLayout(false);
			this.fraStatusOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDeleted)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkVoided)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCashed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOutstanding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).EndInit();
			this.fraAccountType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraTypeOptions)).EndInit();
			this.fraTypeOptions.ResumeLayout(false);
			this.fraTypeOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdFilePreview;
	}
}
