﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomizeProjectDetail.
	/// </summary>
	public partial class frmCustomizeProjectDetail : BaseForm
	{
		public frmCustomizeProjectDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomizeProjectDetail InstancePtr
		{
			get
			{
				return (frmCustomizeProjectDetail)Sys.GetInstance(typeof(frmCustomizeProjectDetail));
			}
		}

		protected frmCustomizeProjectDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string FName = "";
		// vbPorter upgrade warning: FSize As short --> As int	OnWriteFCConvert.ToSingle(
		int FSize;
		bool UnderlineFlag;
		bool StrikethruFlag;
		bool BoldFlag;
		bool ItalicFlag;
		clsDRWrapper rs = new clsDRWrapper();
		public bool FromProj;
		bool blnSavedFormat;

		private void frmCustomizeProjectDetail_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			txtDescription.Focus();
			this.Refresh();
			if (!modBudgetaryMaster.Statics.blnProjectDetailEdit && !modBudgetaryMaster.Statics.blnProjectDetailReportEdit)
			{
				// do nothing
			}
			else
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{					
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					cmbYTDDebitsCredits.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDDebitCredit")) ? 0 : 1;
					cmbYTDDebitsCredits.SelectedIndex = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("YTDNet")) ? 1 : 0;
					bool optPendingDetail = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingDetail"));
					bool optPendingSummary = FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("PendingSummary"));
					if (!optPendingDetail && !optPendingSummary)
					{
						cmbPendingDetail.SelectedIndex = 1;
					}
					else if (optPendingDetail)
					{
						cmbPendingDetail.SelectedIndex = 0;
					}
					else 
					{
						cmbPendingDetail.SelectedIndex = 2;
					}
				}
			}
		}

		private void frmCustomizeProjectDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmCustomizeProjectDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomizeProjectDetail.FillStyle	= 0;
			//frmCustomizeProjectDetail.ScaleWidth	= 9300;
			//frmCustomizeProjectDetail.ScaleHeight	= 7380;
			//frmCustomizeProjectDetail.LinkTopic	= "Form2";
			//frmCustomizeProjectDetail.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			blnSavedFormat = false;
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (FromProj)
			{
				FromProj = false;
				frmProjectDetailSelect.InstancePtr.Show(App.MainForm);
				if (blnSavedFormat)
				{
					if (modBudgetaryMaster.Statics.blnProjectDetailReportEdit)
					{
						modBudgetaryMaster.Statics.blnProjectDetailReportEdit = false;
						frmProjectDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
					else
					{
						frmProjectDetailSelect.InstancePtr.FillFormat(true, Strings.Trim(txtDescription.Text));
					}
				}
				else
				{
					modBudgetaryMaster.Statics.blnProjectDetailReportEdit = false;
				}
			}
			else if (modBudgetaryMaster.Statics.blnProjectDetailEdit)
			{
				modBudgetaryMaster.Statics.blnProjectDetailEdit = false;
				frmGetProjectDetail.InstancePtr.Show(App.MainForm);
			}
		}

		private void frmCustomizeProjectDetail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			if (txtDescription.Text == "")
			{
				MessageBox.Show("You must enter a description for this format before you can save", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			rs.OpenRecordset("SELECT * FROM ProjectDetailFormats WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A report format with this description already exists.  Do you wish to overwrite it?", "Overwrite Format?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
					rs.Set_Fields("Description", txtDescription.Text);
					rs.Set_Fields("YTDDebitCredit", cmbYTDDebitsCredits.SelectedIndex == 0);
					rs.Set_Fields("YTDNet", cmbYTDDebitsCredits.SelectedIndex == 1);                
					rs.Set_Fields("Printer", "O");					
					rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
					rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 2);
					rs.Set_Fields("Font", "S");					
					rs.Update();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
				blnSavedFormat = true;
				Close();
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM ProjectDetailFormats WHERE ID = 0");
				rs.AddNew();
				rs.Set_Fields("Description", txtDescription.Text);
				rs.Set_Fields("YTDDebitCredit", cmbYTDDebitsCredits.SelectedIndex == 0);
				rs.Set_Fields("YTDNet", cmbYTDDebitsCredits.SelectedIndex == 1);
				rs.Set_Fields("Printer", "O");				
				rs.Set_Fields("PendingDetail", cmbPendingDetail.SelectedIndex == 0);
				rs.Set_Fields("PendingSummary", cmbPendingDetail.SelectedIndex == 2);
				rs.Set_Fields("Font", "S");
				rs.Update();
				blnSavedFormat = true;
				Close();
			}
		}
	}
}
