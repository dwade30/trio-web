﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptShowCustomReportSetup.
	/// </summary>
	partial class rptShowCustomReportSetup
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptShowCustomReportSetup));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblReportTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.chkAll = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkSingle = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkRange = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.fldMonthSelection = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.chkAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkAccountDescription = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.Shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.chkPending = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkEncumbrances = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.Shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.chkBudget = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkCurrentActivity = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkYTDActivity = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkBalance = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.chkPercent = new GrapeCity.ActiveReports.SectionReportModel.CheckBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcctType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblReportTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSingle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSelection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPending)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEncumbrances)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBudget)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCurrentActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYTDActivity)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBalance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldRow,
				this.fldType,
				this.fldAcctType,
				this.fldAccount,
				this.fldDS,
				this.fldTitle
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblReportTitle,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3
			});
			this.PageHeader.Height = 0.4166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Shape1,
				this.Label10,
				this.chkAll,
				this.chkSingle,
				this.chkRange,
				this.fldMonthSelection,
				this.Shape2,
				this.Label11,
				this.chkAccountNumber,
				this.chkAccountDescription,
				this.Shape3,
				this.Label12,
				this.chkPending,
				this.chkEncumbrances,
				this.Shape4,
				this.Label13,
				this.chkBudget,
				this.chkCurrentActivity,
				this.chkYTDActivity,
				this.chkBalance,
				this.chkPercent
			});
			this.GroupHeader1.Height = 1.739583F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line1,
				this.Label8,
				this.Label9,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17
			});
			this.GroupHeader2.Height = 0.2395833F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// lblReportTitle
			// 
			this.lblReportTitle.Height = 0.21875F;
			this.lblReportTitle.HyperLink = null;
			this.lblReportTitle.Left = 1.5F;
			this.lblReportTitle.Name = "lblReportTitle";
			this.lblReportTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.lblReportTitle.Text = "Valid Accounts List";
			this.lblReportTitle.Top = 0F;
			this.lblReportTitle.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Shape1
			// 
			this.Shape1.Height = 0.9375F;
			this.Shape1.Left = 0.5F;
			this.Shape1.Name = "Shape1";
			this.Shape1.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape1.Top = 0.21875F;
			this.Shape1.Width = 2.25F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.59375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "background-color: White";
			this.Label10.Text = "Month(s) To Report";
			this.Label10.Top = 0.125F;
			this.Label10.Width = 1.28125F;
			// 
			// chkAll
			// 
			this.chkAll.Height = 0.19F;
			this.chkAll.Left = 0.84375F;
			this.chkAll.Name = "chkAll";
			this.chkAll.Style = "";
			this.chkAll.Text = "All";
			this.chkAll.Top = 0.34375F;
			this.chkAll.Width = 1.46875F;
			// 
			// chkSingle
			// 
			this.chkSingle.Height = 0.19F;
			this.chkSingle.Left = 0.84375F;
			this.chkSingle.Name = "chkSingle";
			this.chkSingle.Style = "";
			this.chkSingle.Text = "Single Month";
			this.chkSingle.Top = 0.5F;
			this.chkSingle.Width = 1.46875F;
			// 
			// chkRange
			// 
			this.chkRange.Height = 0.19F;
			this.chkRange.Left = 0.84375F;
			this.chkRange.Name = "chkRange";
			this.chkRange.Style = "";
			this.chkRange.Text = "Range of Months";
			this.chkRange.Top = 0.65625F;
			this.chkRange.Width = 1.46875F;
			// 
			// fldMonthSelection
			// 
			this.fldMonthSelection.Height = 0.1875F;
			this.fldMonthSelection.Left = 0.5625F;
			this.fldMonthSelection.Name = "fldMonthSelection";
			this.fldMonthSelection.Style = "text-align: center";
			this.fldMonthSelection.Text = "Field1";
			this.fldMonthSelection.Top = 0.90625F;
			this.fldMonthSelection.Width = 2.125F;
			// 
			// Shape2
			// 
			this.Shape2.Height = 0.5F;
			this.Shape2.Left = 4.6875F;
			this.Shape2.Name = "Shape2";
			this.Shape2.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape2.Top = 0.21875F;
			this.Shape2.Width = 1.6875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.78125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "background-color: White";
			this.Label11.Text = "Account Information";
			this.Label11.Top = 0.125F;
			this.Label11.Width = 1.3125F;
			// 
			// chkAccountNumber
			// 
			this.chkAccountNumber.Height = 0.19F;
			this.chkAccountNumber.Left = 4.78125F;
			this.chkAccountNumber.Name = "chkAccountNumber";
			this.chkAccountNumber.Style = "";
			this.chkAccountNumber.Text = "Account Number";
			this.chkAccountNumber.Top = 0.34375F;
			this.chkAccountNumber.Width = 1.46875F;
			// 
			// chkAccountDescription
			// 
			this.chkAccountDescription.Height = 0.19F;
			this.chkAccountDescription.Left = 4.78125F;
			this.chkAccountDescription.Name = "chkAccountDescription";
			this.chkAccountDescription.Style = "";
			this.chkAccountDescription.Text = "Account Description";
			this.chkAccountDescription.Top = 0.5F;
			this.chkAccountDescription.Width = 1.53125F;
			// 
			// Shape3
			// 
			this.Shape3.Height = 0.5F;
			this.Shape3.Left = 2.90625F;
			this.Shape3.Name = "Shape3";
			this.Shape3.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape3.Top = 0.21875F;
			this.Shape3.Width = 1.6875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "background-color: White";
			this.Label12.Text = "Include in YTD";
			this.Label12.Top = 0.125F;
			this.Label12.Width = 0.96875F;
			// 
			// chkPending
			// 
			this.chkPending.Height = 0.19F;
			this.chkPending.Left = 3F;
			this.chkPending.Name = "chkPending";
			this.chkPending.Style = "";
			this.chkPending.Text = "Pending Amounts";
			this.chkPending.Top = 0.34375F;
			this.chkPending.Width = 1.46875F;
			// 
			// chkEncumbrances
			// 
			this.chkEncumbrances.Height = 0.19F;
			this.chkEncumbrances.Left = 3F;
			this.chkEncumbrances.Name = "chkEncumbrances";
			this.chkEncumbrances.Style = "";
			this.chkEncumbrances.Text = "Encumbrances";
			this.chkEncumbrances.Top = 0.5F;
			this.chkEncumbrances.Width = 1.53125F;
			// 
			// Shape4
			// 
			this.Shape4.Height = 0.71875F;
			this.Shape4.Left = 2.90625F;
			this.Shape4.Name = "Shape4";
			this.Shape4.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.Shape4.Top = 0.90625F;
			this.Shape4.Width = 2.46875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.19F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "background-color: White";
			this.Label13.Text = "Include in Report";
			this.Label13.Top = 0.8125F;
			this.Label13.Width = 1.125F;
			// 
			// chkBudget
			// 
			this.chkBudget.Height = 0.19F;
			this.chkBudget.Left = 3F;
			this.chkBudget.Name = "chkBudget";
			this.chkBudget.Style = "";
			this.chkBudget.Text = "Budget / Beg Bal";
			this.chkBudget.Top = 1.03125F;
			this.chkBudget.Width = 1.28125F;
			// 
			// chkCurrentActivity
			// 
			this.chkCurrentActivity.Height = 0.19F;
			this.chkCurrentActivity.Left = 3F;
			this.chkCurrentActivity.Name = "chkCurrentActivity";
			this.chkCurrentActivity.Style = "";
			this.chkCurrentActivity.Text = "Current Activity";
			this.chkCurrentActivity.Top = 1.1875F;
			this.chkCurrentActivity.Width = 1.28125F;
			// 
			// chkYTDActivity
			// 
			this.chkYTDActivity.Height = 0.19F;
			this.chkYTDActivity.Left = 3F;
			this.chkYTDActivity.Name = "chkYTDActivity";
			this.chkYTDActivity.Style = "";
			this.chkYTDActivity.Text = "YTD Activity";
			this.chkYTDActivity.Top = 1.34375F;
			this.chkYTDActivity.Width = 1.28125F;
			// 
			// chkBalance
			// 
			this.chkBalance.Height = 0.19F;
			this.chkBalance.Left = 4.375F;
			this.chkBalance.Name = "chkBalance";
			this.chkBalance.Style = "";
			this.chkBalance.Text = "Balance";
			this.chkBalance.Top = 1.03125F;
			this.chkBalance.Width = 0.84375F;
			// 
			// chkPercent
			// 
			this.chkPercent.Height = 0.19F;
			this.chkPercent.Left = 4.375F;
			this.chkPercent.Name = "chkPercent";
			this.chkPercent.Style = "";
			this.chkPercent.Text = "Percent";
			this.chkPercent.Top = 1.1875F;
			this.chkPercent.Width = 0.84375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.21875F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.21875F;
			this.Line1.Y2 = 0.21875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label8.Text = "Row";
			this.Label8.Top = 0.03125F;
			this.Label8.Width = 0.5F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.65625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label9.Text = "Type";
			this.Label9.Top = 0.03125F;
			this.Label9.Width = 0.46875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 1.28125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label14.Text = "Acct Type";
			this.Label14.Top = 0.03125F;
			this.Label14.Width = 0.71875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.15625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label15.Text = "Account";
			this.Label15.Top = 0.03125F;
			this.Label15.Width = 1.65625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.9375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.Label16.Text = "D/S";
			this.Label16.Top = 0.03125F;
			this.Label16.Width = 0.46875F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.59375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label17.Text = "Title";
			this.Label17.Top = 0.03125F;
			this.Label17.Width = 2.875F;
			// 
			// fldRow
			// 
			this.fldRow.Height = 0.1875F;
			this.fldRow.Left = 0F;
			this.fldRow.Name = "fldRow";
			this.fldRow.Text = "Field1";
			this.fldRow.Top = 0F;
			this.fldRow.Width = 0.5F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.65625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "text-align: center";
			this.fldType.Text = "Field1";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.46875F;
			// 
			// fldAcctType
			// 
			this.fldAcctType.Height = 0.1875F;
			this.fldAcctType.Left = 1.28125F;
			this.fldAcctType.Name = "fldAcctType";
			this.fldAcctType.Style = "text-align: center";
			this.fldAcctType.Text = "Field1";
			this.fldAcctType.Top = 0F;
			this.fldAcctType.Width = 0.71875F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 2.15625F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "text-align: left";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.65625F;
			// 
			// fldDS
			// 
			this.fldDS.Height = 0.1875F;
			this.fldDS.Left = 3.9375F;
			this.fldDS.Name = "fldDS";
			this.fldDS.Style = "text-align: center";
			this.fldDS.Text = "Field1";
			this.fldDS.Top = 0F;
			this.fldDS.Width = 0.46875F;
			// 
			// fldTitle
			// 
			this.fldTitle.Height = 0.1875F;
			this.fldTitle.Left = 4.59375F;
			this.fldTitle.Name = "fldTitle";
			this.fldTitle.Style = "text-align: left";
			this.fldTitle.Text = "Field1";
			this.fldTitle.Top = 0F;
			this.fldTitle.Width = 2.875F;
			// 
			// rptShowCustomReportSetup
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblReportTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSingle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMonthSelection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPending)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEncumbrances)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBudget)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCurrentActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkYTDActivity)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBalance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkAll;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkSingle;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkRange;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonthSelection;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkAccountDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkPending;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkEncumbrances;
		private GrapeCity.ActiveReports.SectionReportModel.Shape Shape4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkBudget;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkCurrentActivity;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkYTDActivity;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkBalance;
		private GrapeCity.ActiveReports.SectionReportModel.CheckBox chkPercent;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
	}
}
