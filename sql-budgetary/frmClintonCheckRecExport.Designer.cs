﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using System.IO;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmClintonCheckRecExport.
	/// </summary>
	partial class frmClintonCheckRecExport : BaseForm
	{
		public fecherFoundation.FCTextBox txtAcctNumber;
		public Global.T2KDateBox txtLowDate;
		public Global.T2KDateBox txtHighDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClintonCheckRecExport));
            this.txtAcctNumber = new fecherFoundation.FCTextBox();
            this.txtLowDate = new Global.T2KDateBox();
            this.txtHighDate = new Global.T2KDateBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 308);
            this.BottomPanel.Size = new System.Drawing.Size(425, 90);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.txtAcctNumber);
            this.ClientArea.Controls.Add(this.txtLowDate);
            this.ClientArea.Controls.Add(this.txtHighDate);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(445, 427);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtHighDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtLowDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAcctNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdSave, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(445, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(197, 28);
            this.HeaderText.Text = "Check Rec Export";
            // 
            // txtAcctNumber
            // 
            this.txtAcctNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtAcctNumber.Location = new System.Drawing.Point(30, 70);
            this.txtAcctNumber.MaxLength = 10;
            this.txtAcctNumber.Name = "txtAcctNumber";
            this.txtAcctNumber.Size = new System.Drawing.Size(118, 40);
            this.txtAcctNumber.TabIndex = 0;
            this.txtAcctNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAcctNumber_KeyPress);
            // 
            // txtLowDate
            // 
            this.txtLowDate.Location = new System.Drawing.Point(30, 190);
            this.txtLowDate.Mask = "##/##/####";
            this.txtLowDate.Name = "txtLowDate";
            this.txtLowDate.Size = new System.Drawing.Size(110, 22);
            this.txtLowDate.TabIndex = 1;
            // 
            // txtHighDate
            // 
            this.txtHighDate.Location = new System.Drawing.Point(213, 190);
            this.txtHighDate.Mask = "##/##/####";
            this.txtHighDate.Name = "txtHighDate";
            this.txtHighDate.Size = new System.Drawing.Size(110, 22);
            this.txtHighDate.TabIndex = 2;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 140);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(96, 17);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "DATE RANGE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 17);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "ACCT #";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(166, 204);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(23, 21);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 260);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(104, 48);
            this.cmdSave.TabIndex = 6;
            this.cmdSave.Text = "Process";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmClintonCheckRecExport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(445, 487);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmClintonCheckRecExport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Check Rec Export";
            this.Load += new System.EventHandler(this.frmClintonCheckRecExport_Load);
            this.Activated += new System.EventHandler(this.frmClintonCheckRecExport_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmClintonCheckRecExport_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLowDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHighDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdSave;
	}
}
