﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System.Linq;
using System.Runtime.InteropServices;

namespace TWBD0000
{
	public class modClintonCheckRecExport
	{
		//=========================================================
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct ClintonHeaderRecord
		{
            // vbPorter upgrade warning: BankNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] BankNumberCharArray;
            public string BankNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BankNumberCharArray);
                }
                set
                {
                    BankNumberCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
			// vbPorter upgrade warning: AccountNumber As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] AccountNumberCharArray;
            public string AccountNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountNumberCharArray);
                }
                set
                {
                    AccountNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: TranCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] TranCodeCharArray;
            public string TranCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TranCodeCharArray);
                }
                set
                {
                    TranCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: FillerNumeric As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
            public char[] FillerNumericCharArray;
            public string FillerNumeric
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FillerNumericCharArray);
                }
                set
                {
                    FillerNumericCharArray = FCUtils.FixedStringToArray(value, 7);
                }
            }
            // vbPorter upgrade warning: FillerString As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 46)]
            public char[] FillerStringCharArray;
            public string FillerString
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FillerStringCharArray);
                }
                set
                {
                    FillerStringCharArray = FCUtils.FixedStringToArray(value, 46);
                }
            }
            // vbPorter upgrade warning: ID As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] IDCharArray;
            public string ID
            {
                get
                {
                    return FCUtils.FixedStringFromArray(IDCharArray);
                }
                set
                {
                    IDCharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public ClintonHeaderRecord(int unusedParam)
			{
				this.BankNumberCharArray = new string(' ', 12).ToArray();
				this.AccountNumberCharArray = new string(' ', 10).ToArray();
				this.TranCodeCharArray = new string(' ', 2).ToArray();
				this.FillerNumericCharArray = new string(' ', 7).ToArray();
				this.FillerStringCharArray = new string(' ', 46).ToArray();
				this.IDCharArray = new string(' ', 3).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct ClintonIssueRecord
		{
			// vbPorter upgrade warning: BankNumber As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] BankNumberCharArray;
            public string BankNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BankNumberCharArray);
                }
                set
                {
                    BankNumberCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
			// vbPorter upgrade warning: AccountNumber As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] AccountNumberCharArray;
            public string AccountNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountNumberCharArray);
                }
                set
                {
                    AccountNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: TranCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] TranCodeCharArray;
            public string TranCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TranCodeCharArray);
                }
                set
                {
                    TranCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Date As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] DateCharArray;
            public string Date
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DateCharArray);
                }
                set
                {
                    DateCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Amount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] AmountCharArray;
            public string Amount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AmountCharArray);
                }
                set
                {
                    AmountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // vbPorter upgrade warning: SerialNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] SerialNumberCharArray;
            public string SerialNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SerialNumberCharArray);
                }
                set
                {
                    SerialNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: SequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] SequenceNumberCharArray;
            public string SequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SequenceNumberCharArray);
                }
                set
                {
                    SequenceNumberCharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: Name As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 13)]
            public char[] NameCharArray;
            public string Name
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NameCharArray);
                }
                set
                {
                    NameCharArray = FCUtils.FixedStringToArray(value, 13);
                }
            }
            // vbPorter upgrade warning: Filler As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] FillerCharArray;
            public string Filler
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FillerCharArray);
                }
                set
                {
                    FillerCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
            // vbPorter upgrade warning: flag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] flagCharArray;
            public string flag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(flagCharArray);
                }
                set
                {
                    flagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public ClintonIssueRecord(int unusedParam)
			{
				this.BankNumberCharArray = new string(' ', 12).ToArray();
				this.AccountNumberCharArray = new string(' ', 10).ToArray();
				this.TranCodeCharArray = new string(' ', 2).ToArray();
				this.DateCharArray = new string(' ', 6).ToArray();
				this.AmountCharArray = new string(' ', 11).ToArray();
				this.SerialNumberCharArray = new string(' ', 10).ToArray();
				this.SequenceNumberCharArray = new string(' ', 3).ToArray();
				this.NameCharArray = new string(' ', 13).ToArray();
				this.FillerCharArray = new string(' ', 12).ToArray();
				this.flagCharArray = new string(' ', 1).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct ClintonVoidRecord
		{
            // vbPorter upgrade warning: BankNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] BankNumberCharArray;
            public string BankNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BankNumberCharArray);
                }
                set
                {
                    BankNumberCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
            // vbPorter upgrade warning: AccountNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] AccountNumberCharArray;
            public string AccountNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountNumberCharArray);
                }
                set
                {
                    AccountNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: TranCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] TranCodeCharArray;
            public string TranCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TranCodeCharArray);
                }
                set
                {
                    TranCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: NewStatus As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] NewStatusCharArray;
            public string NewStatus
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NewStatusCharArray);
                }
                set
                {
                    NewStatusCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: OldStatus As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] OldStatusCharArray;
            public string OldStatus
            {
                get
                {
                    return FCUtils.FixedStringFromArray(OldStatusCharArray);
                }
                set
                {
                    OldStatusCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: SerialNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] SerialNumberCharArray;
            public string SerialNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SerialNumberCharArray);
                }
                set
                {
                    SerialNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: Filler1 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] Filler1CharArray;
            public string Filler1
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler1CharArray);
                }
                set
                {
                    Filler1CharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: Amount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] AmountCharArray;
            public string Amount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AmountCharArray);
                }
                set
                {
                    AmountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // vbPorter upgrade warning: Date As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] DateCharArray;
            public string Date
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DateCharArray);
                }
                set
                {
                    DateCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Filler2 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] Filler2CharArray;
            public string Filler2
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler2CharArray);
                }
                set
                {
                    Filler2CharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: HighSerialNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] HighSerialNumberCharArray;
            public string HighSerialNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(HighSerialNumberCharArray);
                }
                set
                {
                    HighSerialNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: Filler3 As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] Filler3CharArray;
            public string Filler3
            {
                get
                {
                    return FCUtils.FixedStringFromArray(Filler3CharArray);
                }
                set
                {
                    Filler3CharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public ClintonVoidRecord(int unusedParam)
			{
				this.BankNumberCharArray = new string(' ', 12).ToArray();
				this.AccountNumberCharArray = new string(' ', 10).ToArray();
				this.TranCodeCharArray = new string(' ', 2).ToArray();
				this.NewStatusCharArray = new string(' ', 1).ToArray();
				this.OldStatusCharArray = new string(' ', 1).ToArray();
				this.SerialNumberCharArray = new string(' ', 10).ToArray();
				this.Filler1CharArray = new string(' ', 3).ToArray();
				this.AmountCharArray = new string(' ', 11).ToArray();
				this.DateCharArray = new string(' ', 6).ToArray();
				this.Filler2CharArray = new string(' ', 3).ToArray();
				this.HighSerialNumberCharArray = new string(' ', 10).ToArray();
				this.Filler3CharArray = new string(' ', 11).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct CaribouVoidRecordWebExpress
		{
            // vbPorter upgrade warning: BankNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] BankNumberCharArray;
            public string BankNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BankNumberCharArray);
                }
                set
                {
                    BankNumberCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
			// vbPorter upgrade warning: AccountNumber As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] AccountNumberCharArray;
            public string AccountNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountNumberCharArray);
                }
                set
                {
                    AccountNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: TranCode As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] TranCodeCharArray;
            public string TranCode
            {
                get
                {
                    return FCUtils.FixedStringFromArray(TranCodeCharArray);
                }
                set
                {
                    TranCodeCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            // vbPorter upgrade warning: Date As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
            public char[] DateCharArray;
            public string Date
            {
                get
                {
                    return FCUtils.FixedStringFromArray(DateCharArray);
                }
                set
                {
                    DateCharArray = FCUtils.FixedStringToArray(value, 6);
                }
            }
            // vbPorter upgrade warning: Amount As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 11)]
            public char[] AmountCharArray;
            public string Amount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AmountCharArray);
                }
                set
                {
                    AmountCharArray = FCUtils.FixedStringToArray(value, 11);
                }
            }
            // vbPorter upgrade warning: SerialNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public char[] SerialNumberCharArray;
            public string SerialNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SerialNumberCharArray);
                }
                set
                {
                    SerialNumberCharArray = FCUtils.FixedStringToArray(value, 10);
                }
            }
            // vbPorter upgrade warning: SequenceNumber As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public char[] SequenceNumberCharArray;
            public string SequenceNumber
            {
                get
                {
                    return FCUtils.FixedStringFromArray(SequenceNumberCharArray);
                }
                set
                {
                    SequenceNumberCharArray = FCUtils.FixedStringToArray(value, 3);
                }
            }
            // vbPorter upgrade warning: Name As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 13)]
            public char[] NameCharArray;
            public string Name
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NameCharArray);
                }
                set
                {
                    NameCharArray = FCUtils.FixedStringToArray(value, 13);
                }
            }
            // vbPorter upgrade warning: Filler As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
            public char[] FillerCharArray;
            public string Filler
            {
                get
                {
                    return FCUtils.FixedStringFromArray(FillerCharArray);
                }
                set
                {
                    FillerCharArray = FCUtils.FixedStringToArray(value, 12);
                }
            }
            // vbPorter upgrade warning: flag As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public char[] flagCharArray;
            public string flag
            {
                get
                {
                    return FCUtils.FixedStringFromArray(flagCharArray);
                }
                set
                {
                    flagCharArray = FCUtils.FixedStringToArray(value, 1);
                }
            }
            // vbPorter upgrade warning: BlankOrCRLF As FixedString	OnWrite(string)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] BlankOrCRLFCharArray;
            public string BlankOrCRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(BlankOrCRLFCharArray);
                }
                set
                {
                    BlankOrCRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public CaribouVoidRecordWebExpress(int unusedParam)
			{
				this.BankNumberCharArray = new string(' ', 12).ToArray();
				this.AccountNumberCharArray = new string(' ', 10).ToArray();
				this.TranCodeCharArray = new string(' ', 2).ToArray();
				this.DateCharArray = new string(' ', 6).ToArray();
				this.AmountCharArray = new string(' ', 11).ToArray();
				this.SerialNumberCharArray = new string(' ', 10).ToArray();
				this.SequenceNumberCharArray = new string(' ', 3).ToArray();
				this.NameCharArray = new string(' ', 12).ToArray();
				this.FillerCharArray = new string(' ', 12).ToArray();
				this.flagCharArray = new string(' ', 1).ToArray();
				this.BlankOrCRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential)]


		public class StaticVariables
		{
			public ClintonHeaderRecord CCHR = new ClintonHeaderRecord(0);
			public ClintonIssueRecord CCIR = new ClintonIssueRecord(0);
			public ClintonVoidRecord CCVR = new ClintonVoidRecord(0);
			public CaribouVoidRecordWebExpress CCVRWE = new CaribouVoidRecordWebExpress(0);
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
