﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBoothBayCheckRecExport.
	/// </summary>
	public partial class frmBoothBayCheckRecExport : BaseForm
	{
        private cCheckRecOption cro = new cCheckRecOption();
        
        public cCheckRecOption Init()
        {
            this.Show(FormShowEnum.Modal);
            return cro;
        }

		public frmBoothBayCheckRecExport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBoothBayCheckRecExport InstancePtr
		{
			get
			{
				return (frmBoothBayCheckRecExport)Sys.GetInstance(typeof(frmBoothBayCheckRecExport));
			}
		}

		protected frmBoothBayCheckRecExport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();

		private void frmBoothBayCheckRecExport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBoothBayCheckRecExport_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmBoothBayCheckRecExport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (!dtpLowDate.NullableValue.HasValue || !dtpHighDate.NullableValue.HasValue)
			{
				MessageBox.Show("There is an invalid date in your date range.  Please enter a valid date range then try again.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
            
			if (dtpLowDate.NullableValue.Value.IsLaterThan(dtpHighDate.NullableValue.Value))
			{
				MessageBox.Show("Your start date must be earlier than your end date", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
            cro.StartDate = dtpLowDate.Text;
            cro.EndDate = dtpHighDate.Text;
            this.Unload();
		}
	}
}
