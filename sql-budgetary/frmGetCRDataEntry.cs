﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetCRDataEntry.
	/// </summary>
	public partial class frmGetCRDataEntry : BaseForm
	{
		public frmGetCRDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetCRDataEntry InstancePtr
		{
			get
			{
				return (frmGetCRDataEntry)Sys.GetInstance(typeof(frmGetCRDataEntry));
			}
		}

		protected frmGetCRDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		bool EmptyFlag;

		public void StartProgram()
		{
			int counter;
			clsDRWrapper rsPeriod = new clsDRWrapper();
			int lngJournal;
			//Application.DoEvents();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))));
			int intPeriod;
			intPeriod = cboEntry.ItemData(cboEntry.SelectedIndex);
			rsPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "Budgetary");
			modBudgetaryMaster.Statics.CurrentCREntry = lngJournal;
			// initialize the current account number
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND (Type = 'C' OR Type = 'W') AND Status = 'E' AND Period = " + rsPeriod.Get_Fields("Period") + " ORDER BY ID");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				modBudgetaryMaster.Statics.blnCREdit = true;
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				frmCRDataEntry.InstancePtr.ChosenJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCRDataEntry.InstancePtr.ChosenPeriod = FCConvert.ToInt32(rs.Get_Fields("Period"));
				if (rs.RecordCount() > 15)
				{
					frmCRDataEntry.InstancePtr.vs1.Rows = rs.RecordCount() + 1;
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsPeriod.Get_Fields("Type")) == "CW")
				{
					frmCRDataEntry.InstancePtr.blnWindowsCashReceipts = true;
				}
				else
				{
					frmCRDataEntry.InstancePtr.blnWindowsCashReceipts = false;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCRDataEntry.InstancePtr.txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("Period")), 2);
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					//Application.DoEvents();
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(rs.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yy"));
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, FCConvert.ToString(rs.Get_Fields("account")));
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, FCConvert.ToString(rs.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields("Amount") * -1));
					if (rs.EndOfFile() != true)
					{
						rs.MoveNext();
					}
				}
				if (rs.RecordCount() < 15)
				{
					for (counter = rs.RecordCount() + 1; counter <= frmCRDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						//Application.DoEvents();
						frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, "0");
						frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(DateTime.Today, "MM/dd/yy"));
						frmCRDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, "0");
					}
				}
				frmCRDataEntry.InstancePtr.CalculateTotals();
				frmCRDataEntry.InstancePtr.lblNetTotal.Text = Strings.FormatCurrency(frmCRDataEntry.InstancePtr.TotalAmount, 2);
				frmCRDataEntry.InstancePtr.Show(App.MainForm);
				// show the form
				modRegistry.SaveRegistryKey("CURRCRJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry));
			}
			else
			{
				// else let the user know that no account was found
				frmWait.InstancePtr.Unload();
				// get rid of the wait form
				cmbInput.SelectedIndex = 0;
				EmptyFlag = true;
				cmdGetAccountNumber_Click();
				return;
			}
			// Me.Hide
			frmWait.InstancePtr.Unload();
			Close();
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int lngJournal = 0;
			if (cboEntry.SelectedIndex != 0)
			{
				// if there is a valid account number
				if (cmbInput.SelectedIndex == 0)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnCREdit = false;
					lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))));
					frmCRDataEntry.InstancePtr.ChosenJournal = lngJournal;
					frmCRDataEntry.InstancePtr.ChosenPeriod = cboEntry.ItemData(cboEntry.SelectedIndex);
					frmCRDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
				}
				else
				{
					modBudgetaryMaster.Statics.CurrentCREntry = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
					// set the account number to the one last used
					StartProgram();
					// call the procedure to retrieve the info
				}
			}
			else
			{
				if (cmbInput.SelectedIndex == 0)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnCREdit = false;
					frmCRDataEntry.InstancePtr.ChosenJournal = 0;
					frmCRDataEntry.InstancePtr.ChosenPeriod = 0;
					frmCRDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
				}
				else
				{
					MessageBox.Show("You must select a journal to update.", "Invalid Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdQuit_Click()
		{
			Close();
			// unload this form
		}

		private void frmGetCRDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			if (!EmptyFlag)
			{
				lblLastAccount.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCREntry), 4);
				cboEntry.AddItem("0000 - New Journal Entry");
				rs.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND (Type = 'CR' OR Type = 'CW') ORDER BY JournalNumber DESC");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					while (!rs.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboEntry.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4) + " - " + "Per " + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
						// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
						cboEntry.ItemData(cboEntry.NewIndex, FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Period"))));
						rs.MoveNext();
					}
				}
				cboEntry.SelectedIndex = 0;
				cboEntry.Focus();
			}
			if (!EmptyFlag)
			{
				modBudgetaryMaster.Statics.CurrentCREntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCRJRNL"))));
				cmbInput.SelectedIndex = 0;
			}
			else
			{
				EmptyFlag = false;
			}
		}

		private void frmGetCRDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				KeyAscii = (Keys)0;
				if (frmGetCRDataEntry.InstancePtr.ActiveControl.GetName() == "cboEntry")
					cmdGetAccountNumber_Click();
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				frmGetCRDataEntry.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetCRDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetCRDataEntry.ScaleWidth	= 9045;
			//frmGetCRDataEntry.ScaleHeight	= 7245;
			//frmGetCRDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboEntry.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboEntry.Items[counter].ToString(), 4))
				{
					cboEntry.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboEntry_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEntry.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			cmdQuit_Click();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}
	}
}
