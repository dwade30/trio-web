﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetGJDataEntry.
	/// </summary>
	partial class frmGetGJDataEntry : BaseForm
	{
		public fecherFoundation.FCComboBox cmbUpdate;
		public fecherFoundation.FCComboBox cboEntry;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetGJDataEntry));
			this.cmbUpdate = new fecherFoundation.FCComboBox();
			this.cboEntry = new fecherFoundation.FCComboBox();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 389);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdGetAccountNumber);
			this.ClientArea.Controls.Add(this.cboEntry);
			this.ClientArea.Controls.Add(this.cmbUpdate);
			this.ClientArea.Controls.Add(this.lblLastAccount);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(610, 329);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(161, 30);
			this.HeaderText.Text = "Get G/J Entry";
			// 
			// cmbUpdate
			// 
			this.cmbUpdate.AutoSize = false;
			this.cmbUpdate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbUpdate.FormattingEnabled = true;
			this.cmbUpdate.Items.AddRange(new object[] {
				"Update",
				"Input"
			});
			this.cmbUpdate.Location = new System.Drawing.Point(30, 102);
			this.cmbUpdate.Name = "cmbUpdate";
			this.cmbUpdate.Size = new System.Drawing.Size(275, 40);
			this.cmbUpdate.TabIndex = 9;
			this.cmbUpdate.SelectedIndexChanged += new System.EventHandler(this.optUpdate_CheckedChanged);
			// 
			// cboEntry
			// 
			this.cboEntry.AutoSize = false;
			this.cboEntry.BackColor = System.Drawing.SystemColors.Window;
			this.cboEntry.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboEntry.FormattingEnabled = true;
			this.cboEntry.Location = new System.Drawing.Point(30, 162);
			this.cboEntry.Name = "cboEntry";
			this.cboEntry.Size = new System.Drawing.Size(275, 40);
			this.cboEntry.TabIndex = 8;
			this.cboEntry.DropDown += new System.EventHandler(this.cboEntry_DropDown);
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(30, 233);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(100, 48);
			this.cmdGetAccountNumber.TabIndex = 1;
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblLastAccount.Location = new System.Drawing.Point(196, 30);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(55, 16);
			this.lblLastAccount.TabIndex = 7;
			// 
			// Label2
			// 
			this.Label2.Font = new System.Drawing.Font("Proxima Nova Regular", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(157, 16);
			this.Label2.TabIndex = 6;
			this.Label2.Text = "LAST ENTRY ACCESSED";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 66);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(412, 16);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "SELECT A JOURNAL NUMBER OR ENTER 0 TO CREATE A NEW JOURNAL";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frmGetGJDataEntry
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 497);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGetGJDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Get G/J Entry";
			this.Load += new System.EventHandler(this.frmGetGJDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmGetGJDataEntry_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetGJDataEntry_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
