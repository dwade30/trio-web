﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Budgetary.BudgetRequests;
using Wisej.Web.Ext.CustomProperties;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmBudgetRequestEdit.
	/// </summary>
	public partial class frmBudgetRequestEdit : BaseForm
	{
		public frmBudgetRequestEdit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbRevene.SelectedIndex = 0;
			
			//FC:FINAL:DDU:##2994 - on enter click focus on process button
			txtComment.AllowKeysOnClientKeyPress("'Left','Right','Delete','Home','End','Insert','Backspace','Space','Tab',48..57,65..90,97..122", false);
            
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBudgetRequestEdit InstancePtr
		{
			get
			{
				return (frmBudgetRequestEdit)Sys.GetInstance(typeof(frmBudgetRequestEdit));
			}
		}

		protected frmBudgetRequestEdit _InstancePtr = null;

		int AccountCol;
		int CurrIndex;
		int AmountCol;
		int RecordCol;
		clsDRWrapper rs = new clsDRWrapper();
		bool blnUnload;
		string strDefaultType = "";
		bool boolSaving;
        private BudgetRequestType currentRequestType = BudgetRequestType.Initial;
		private void cmdCancelComment_Click(object sender, System.EventArgs e)
		{
			fraComments.Visible = false;
			cmdAddComment.Visible = true;
			cmdDeleteAccount.Visible = true;
			cmdAddAccount.Visible = true;
			accountGrid.Visible = true;
			lblTitle.Visible = true;
			accountGrid.Focus();
		}

		public void cmdCancelComment_Click()
		{
			cmdCancelComment_Click(cmdCancelComment, new System.EventArgs());
		}


		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			int tempWidth;
            accountGrid.Visible = false;

			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			string strLabel = "";
			//frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Loading Account Information", true, 200);
            this.ShowWait();
			this.UpdateWait("Please Wait" + "\r\n" + "Loading Account Information");
			mnuProcessAdd.Enabled = true;
			mnuProcessDelete.Enabled = true;
            cmdAddComment.Visible = true;
			cmdAddAccount.Visible = true;
			cmdDeleteAccount.Visible = true;
            if (cmbRevene.SelectedIndex == 0)
			{
				modBudgetaryMaster.Statics.strBudgetAccountType = "E";
            }
			else
			{
				modBudgetaryMaster.Statics.strBudgetAccountType = "R";
            }

			accountGrid.Rows = 1;
			frmWait.InstancePtr.IncrementProgress();
			//FC:FINAL:DDU:#2893 - set right alignments
			if (currentRequestType == BudgetRequestType.Initial)
			{
				SetGridForInitialRequest(accountGrid);
            }
			else if (currentRequestType == BudgetRequestType.Manager)
			{
				SetGridForManagerRequest(accountGrid);
            }
			else if (currentRequestType == BudgetRequestType.Committee)
			{
				SetGridForCommitteeRequest(accountGrid);
            }
			else if (currentRequestType == BudgetRequestType.Elected)
			{
				SetGridForElectedRequest(accountGrid);
            }
			else if (currentRequestType == BudgetRequestType.Approved)
			{
				SetGridForApproved(accountGrid);
            }

			SetGridAlignmentsEtc(accountGrid);
            rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(account, 1) = '" + modBudgetaryMaster.Statics.strBudgetAccountType + "' ORDER BY Account");
            if (rsBudgetInfo.EndOfFile())
            {
                modBudgetaryMaster.InitializeBudgetTable();
                rsBudgetInfo.OpenRecordset("SELECT * FROM Budget WHERE Left(account, 1) = '" + modBudgetaryMaster.Statics.strBudgetAccountType + "' ORDER BY Account");
			}

            if (!rsBudgetInfo.EndOfFile())
            {
                while (!rsBudgetInfo.EndOfFile())
                {
                    accountGrid.Rows = accountGrid.Rows + 1;
                    var currentRow = accountGrid.Rows - 1;
                    accountGrid.TextMatrix(currentRow, RecordCol, rsBudgetInfo.Get_Fields_Int32("ID"));
                    accountGrid.TextMatrix(currentRow, AccountCol, rsBudgetInfo.Get_Fields_String("Account"));
                    switch (currentRequestType)
                    {
                        case BudgetRequestType.Initial:
                            AddInitialRecordToGrid(accountGrid, rsBudgetInfo);
                            break;
                        case BudgetRequestType.Manager:
                            AddManagerRecordToGrid(accountGrid, rsBudgetInfo);
                            break;
                        case BudgetRequestType.Committee:
                            AddCommitteeRecordToGrid(accountGrid, rsBudgetInfo);
                            break;
                        case BudgetRequestType.Elected:
                            AddElectedRecordToGrid(accountGrid, rsBudgetInfo);
                            break;
                        case BudgetRequestType.Approved:
                            AddApprovedRecordToGrid(accountGrid, rsBudgetInfo);
                            break;
                    }

                    rsBudgetInfo.MoveNext();
                }
            }
			
			this.EndWait();
            fraAccountType.Visible = false;
			accountGrid.Visible = true;
			lblTitle.Visible = true;
			if (accountGrid.Rows == 1)
			{
                accountGrid.Rows = accountGrid.Rows + 1;
				accountGrid.TextMatrix(accountGrid.Rows - 1, RecordCol, "0");
				if (currentRequestType == BudgetRequestType.Initial)
				{
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol, "0");
				}
				else if (currentRequestType == BudgetRequestType.Manager)
				{
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol, "0");
				}
				else if (currentRequestType == BudgetRequestType.Committee)
				{
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol, "0");
				}
				else if (currentRequestType == BudgetRequestType.Elected)
				{
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 3, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol, "0");
				}
				else if (currentRequestType == BudgetRequestType.Approved)
				{
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 4, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 3, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Rows - 1, AmountCol, "0");
				}
				accountGrid.Select(accountGrid.Rows - 1, AccountCol);
			}
			else
			{
                accountGrid.Select(1, AmountCol);
                accountGrid.Focus();
                //vs1.EditCell();
            }

        }

        private void SetGridForInitialRequest(FCGrid grid)
        {
            grid.Cols = 3;
            RecordCol = 0;
            AccountCol = 1;
            AmountCol = 2;
            grid.TextMatrix(0, AccountCol, "Account");
            grid.TextMatrix(0, AmountCol, "Initial");
		}

        private void SetGridForManagerRequest(FCGrid grid)
        {
            grid.Cols = 4;
            RecordCol = 0;
            AccountCol = 1;
            AmountCol = 3;
            grid.TextMatrix(0, AccountCol, "Account");
            grid.TextMatrix(0, AmountCol - 1, "Initial");
            grid.ColAlignment(AmountCol - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol, "Manager");
		}

        private void SetGridForCommitteeRequest(FCGrid grid)
        {
            grid.Cols = 5;
            RecordCol = 0;
            AccountCol = 1;
            AmountCol = 4;
            grid.TextMatrix(0, AccountCol, "Account");
            grid.TextMatrix(0, AmountCol - 2, "Initial");
            grid.ColAlignment(AmountCol - 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 1, "Manager");
            grid.ColAlignment(AmountCol - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol, "Committee");
		}

        private void SetGridForElectedRequest(FCGrid grid)
        {
            grid.Cols = 6;
            RecordCol = 0;
            AccountCol = 1;
            AmountCol = 5;
            grid.TextMatrix(0, AccountCol, "Account");
            grid.TextMatrix(0, AmountCol - 3, "Initial");
            grid.ColAlignment(AmountCol - 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 2, "Manager");
            grid.ColAlignment(AmountCol - 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 1, "Committee");
            grid.ColAlignment(AmountCol - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol, "Elected");
		}

        private void SetGridForApproved(FCGrid grid)
        {
            grid.Cols = 7;
            RecordCol = 0;
            AccountCol = 1;
            AmountCol = 6;
            grid.TextMatrix(0, AccountCol, "Account");
            grid.TextMatrix(0, AmountCol - 4, "Initial");
            grid.ColAlignment(AmountCol - 4, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 3, "Manager");
            grid.ColAlignment(AmountCol - 3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 2, "Committee");
            grid.ColAlignment(AmountCol - 2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol - 1, "Elected");
            grid.ColAlignment(AmountCol - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.TextMatrix(0, AmountCol, "Approved");
		}

        private void SetGridAlignmentsEtc(FCGrid grid)
        {
            grid.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            grid.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            grid.ColWidth(RecordCol, 0);
            grid.ColWidth(AccountCol, FCConvert.ToInt32(accountGrid.WidthOriginal * 0.2815111));
            

            for (var counter = 2; counter <= grid.Cols - 1; counter++)
            {
                //Application.DoEvents();
                grid.ColWidth(counter, FCConvert.ToInt32(accountGrid.WidthOriginal * 0.1335926));
                grid.ColDataType(counter, FCGrid.DataTypeSettings.flexDTDouble);
                grid.ColFormat(counter, "#,##0");
            }
		}

        private void AddInitialRecordToGrid(FCGrid grid, clsDRWrapper recordSet)
        {
            var currentRow = grid.Rows - 1;
            grid.TextMatrix(currentRow, AmountCol, recordSet.Get_Fields_Double("InitialRequest"));
		}

        private void AddManagerRecordToGrid(FCGrid grid, clsDRWrapper recordSet)
        {
            var currentRow = grid.Rows - 1;
            grid.TextMatrix(currentRow, AmountCol - 1, recordSet.Get_Fields_Double("InitialRequest"));
            grid.TextMatrix(currentRow, AmountCol, recordSet.Get_Fields_Double("ManagerRequest"));
		}

        private void AddCommitteeRecordToGrid(FCGrid grid, clsDRWrapper recordSet)
        {
            var currentRow = grid.Rows - 1;
            grid.TextMatrix(currentRow, AmountCol - 2, recordSet.Get_Fields_Double("InitialRequest"));
            grid.TextMatrix(currentRow, AmountCol - 1, recordSet.Get_Fields_Double("ManagerRequest"));
            grid.TextMatrix(currentRow, AmountCol, recordSet.Get_Fields_Double("CommitteeRequest"));
		}

        private void AddElectedRecordToGrid(FCGrid grid, clsDRWrapper recordSet)
        {
            var currentRow = grid.Rows - 1;
            grid.TextMatrix(currentRow, AmountCol - 3, recordSet.Get_Fields_Double("InitialRequest"));
            grid.TextMatrix(currentRow, AmountCol - 2, recordSet.Get_Fields_Double("ManagerRequest"));
            grid.TextMatrix(currentRow, AmountCol - 1, recordSet.Get_Fields_Double("CommitteeRequest"));
            grid.TextMatrix(currentRow, AmountCol, Conversion.Val(recordSet.Get_Fields_Double("ElectedRequest")));
		}

        private void AddApprovedRecordToGrid(FCGrid grid, clsDRWrapper recordSet)
        {
            var currentRow = grid.Rows - 1;
            grid.TextMatrix(currentRow, AmountCol - 4, recordSet.Get_Fields_Double("InitialRequest"));
            grid.TextMatrix(currentRow, AmountCol - 3, recordSet.Get_Fields_Double("ManagerRequest"));
            grid.TextMatrix(currentRow, AmountCol - 2, recordSet.Get_Fields_Double("CommitteeRequest"));
            grid.TextMatrix(currentRow, AmountCol - 1, Conversion.Val(recordSet.Get_Fields_Double("ElectedRequest")));
            grid.TextMatrix(currentRow, AmountCol, recordSet.Get_Fields_Decimal("ApprovedAmount"));
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(btnProcess, new System.EventArgs());
		}

		private void cmdProcessComment_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OmitNullsOnInsert = true;
			rsInfo.OpenRecordset("SELECT * FROM Budget WHERE Account = '" + lblAccount.Text + "'");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				rsInfo.Edit();
			}
			else
			{
				rsInfo.AddNew();
				rsInfo.Set_Fields("Account", lblAccount);
			}
			rsInfo.Set_Fields("Comments", Strings.Trim(txtComment.Text));
			rsInfo.Update();
			accountGrid.TextMatrix(accountGrid.Row, RecordCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
			cmdCancelComment_Click();
		}

		public void cmdProcessComment_Click()
		{
			cmdProcessComment_Click(btnProcess, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			mnuProcessSave_Click();
		}

		private void frmBudgetRequestEdit_Activated(object sender, System.EventArgs e)
		{
			//if (modGlobal.FormExist(this))
			//{
			//	return;
			//}
			//if (modBudgetaryMaster.Statics.strBudgetFlag == "I")
			//{
			//	this.Text = "Update Initial Requests";
			//	this.HeaderText.Text = "Update Initial Requests";
			//}
			//else if (modBudgetaryMaster.Statics.strBudgetFlag == "M")
			//{
			//	this.Text = "Update Manager Requests";
			//	this.HeaderText.Text = "Update Manager Requests";
			//}
			//else if (modBudgetaryMaster.Statics.strBudgetFlag == "C")
			//{
			//	this.Text = "Update Committee Requests";
			//	this.HeaderText.Text = "Update Committee Requests";
			//}
			//else if (modBudgetaryMaster.Statics.strBudgetFlag == "E")
			//{
			//	this.Text = "Update Elected Requests";
			//	this.HeaderText.Text = "Update Elected Requests";
			//}
			//else if (modBudgetaryMaster.Statics.strBudgetFlag == "A")
			//{
			//	this.Text = "Update Approved Amounts";
			//	this.HeaderText.Text = "Update Approved Amounts";
			//}
			//this.Refresh();
		}

		private void frmBudgetRequestEdit_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
			if (Strings.UCase(this.ActiveControl.GetName()) == "VS1")
			{
				if (accountGrid.Col == AccountCol && accountGrid.Row > 0 && KeyCode != Keys.F9 && KeyCode != Keys.F2)
				{
					modNewAccountBox.CheckFormKeyDown(accountGrid, accountGrid.Row, accountGrid.Col, KeyCode, Shift, accountGrid.EditSelStart, accountGrid.EditText, accountGrid.EditSelLength);
				}
			}
		}


		private void frmBudgetRequestEdit_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
            currentRequestType = BudgetRequestTypeExtensions.BudgetRequestTypeFromCode(modBudgetaryMaster.Statics.strBudgetFlag);
			SetHeaderText(currentRequestType);
        }

        private void SetHeaderText(BudgetRequestType requestType)
        {
            switch (requestType)
            {
                case BudgetRequestType.Initial:
                    this.Text = "Update Initial Requests";
                    this.HeaderText.Text = "Update Initial Requests";
                    break;
                case BudgetRequestType.Manager:
                    this.Text = "Update Manager Requests";
                    this.HeaderText.Text = "Update Manager Requests";
                    break;
                case BudgetRequestType.Committee:
                    this.Text = "Update Committee Requests";
                    this.HeaderText.Text = "Update Committee Requests";
                    break;
                case BudgetRequestType.Elected:
                    this.Text = "Update Elected Requests";
                    this.HeaderText.Text = "Update Elected Requests";
                    break;
                case BudgetRequestType.Approved:
                    this.Text = "Update Approved Amounts";
                    this.HeaderText.Text = "Update Approved Amounts";
                    break;
            }
		}

		private void frmBudgetRequestEdit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				//Support.SendKeys("{TAB}", false);
				btnProcess.Focus();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void frmBudgetRequestEdit_Resize(object sender, System.EventArgs e)
		{
            int counter;
			accountGrid.ColWidth(RecordCol, 0);
			accountGrid.ColWidth(AccountCol, FCConvert.ToInt32(accountGrid.WidthOriginal * 0.2815111));
			for (counter = 2; counter <= accountGrid.Cols - 1; counter++)
			{
				accountGrid.ColWidth(counter, FCConvert.ToInt32(accountGrid.WidthOriginal * 0.1335926));
				accountGrid.ColFormat(counter, "#,##0");
			}
            accountGrid.Height = ClientArea.Height - accountGrid.Top - BottomPanel.Height;
		}

		private void mnuFileComment_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
            if (accountGrid.Row > 0 )
			{
				if (modValidateAccount.AccountValidate(accountGrid.TextMatrix(accountGrid.Row, AccountCol)))
				{
					accountGrid.Visible = false;
					lblTitle.Visible = false;
					if (FCConvert.ToDouble(accountGrid.TextMatrix(accountGrid.Row, RecordCol)) == 0)
					{
						lblAccount.Text = accountGrid.TextMatrix(accountGrid.Row, AccountCol);
						txtComment.Text = "";
						fraComments.Visible = true;
						cmdAddComment.Visible = false;
						cmdDeleteAccount.Visible = false;
						cmdAddAccount.Visible = false;
						txtComment.Focus();
					}
					else
					{
						rsInfo.OpenRecordset("SELECT * FROM Budget WHERE ID = " + accountGrid.TextMatrix(accountGrid.Row, RecordCol));
						lblAccount.Text = accountGrid.TextMatrix(accountGrid.Row, AccountCol);
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							txtComment.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comments")));
						}
						fraComments.Visible = true;
						cmdAddComment.Visible = false;
						cmdDeleteAccount.Visible = false;
						cmdAddAccount.Visible = false;
						txtComment.Focus();
					}
				}
				else
				{
					MessageBox.Show("You must enter a valid account before you may add a comment.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		public void mnuFileComment_Click()
		{
			mnuFileComment_Click(mnuFileComment, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraAccountType.Visible)
			{
				cmdProcess_Click();
			}
			else
			{
				if (fraComments.Visible)
				{
					cmdProcessComment_Click();
				}
				else
				{
					blnUnload = false;
					SaveInfo();
				}
			}
		}

		private void mnuProcessAdd_Click(object sender, System.EventArgs e)
		{
			if (accountGrid.Row < accountGrid.Rows - 1)
            {
                var lngRow = accountGrid.Row + 1;
				accountGrid.AddItem("", lngRow);
				accountGrid.TextMatrix(lngRow, RecordCol, "0");
				if (modBudgetaryMaster.Statics.strBudgetFlag == "I")
				{
					accountGrid.TextMatrix(lngRow, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "M")
				{
					accountGrid.TextMatrix(lngRow, AmountCol - 1, "0");
					accountGrid.TextMatrix(lngRow, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "C")
				{
					accountGrid.TextMatrix(lngRow, AmountCol - 2, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 1, "0");
					accountGrid.TextMatrix(lngRow, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "E")
				{
					accountGrid.TextMatrix(lngRow, AmountCol - 3, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 2, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 1, "0");
					accountGrid.TextMatrix(lngRow, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "A")
				{
					accountGrid.TextMatrix(lngRow, AmountCol - 4, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 3, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 2, "0");
					accountGrid.TextMatrix(lngRow, AmountCol - 1, "0");
					accountGrid.TextMatrix(lngRow, AmountCol, "0");
				}
				accountGrid.Select(lngRow, AccountCol);
			}
			else
			{
				accountGrid.Rows += 1;
				accountGrid.TextMatrix(accountGrid.Rows - 1, RecordCol, "0");
				if (modBudgetaryMaster.Statics.strBudgetFlag == "I")
				{
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "M")
				{
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "C")
				{
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "E")
				{
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 3, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol, "0");
				}
				else if (modBudgetaryMaster.Statics.strBudgetFlag == "A")
				{
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 4, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 3, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 2, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol - 1, "0");
					accountGrid.TextMatrix(accountGrid.Row + 1, AmountCol, "0");
				}
				accountGrid.Select(accountGrid.Rows - 1, AccountCol);
			}
		}

		public void mnuProcessAdd_Click()
		{
			mnuProcessAdd_Click(mnuProcessAdd, new System.EventArgs());
		}

		private void mnuProcessDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			if (accountGrid.Rows > 1)
			{
				if (accountGrid.TextMatrix(accountGrid.Row, AccountCol) != "")
				{
					temp = MessageBox.Show("Do you really want to delete this Account", "Delete Account?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
					if (temp == DialogResult.Yes)
					{
						accountGrid.RowHidden(accountGrid.Row, true);
					}
				}
				else
				{
					accountGrid.RowHidden(accountGrid.Row, true);
				}
				Support.SendKeys("{DOWN}", false);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (fraAccountType.Visible)
			{
				cmdProcess_Click();
			}
			else
			{
				if (fraComments.Visible)
				{
					cmdProcessComment_Click();
				}
				else
				{
					blnUnload = true;
					SaveInfo();
				}
			}
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void cmbRevene_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetDefaultType();
		}

		private void cmbTown_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetDefaultType();
		}
		/// <summary>
		/// FC:FINAL:JEI:IIT790 correct logic
		/// </summary>
		private void SetDefaultType()
		{
			//"Expense Accounts"
			if (this.cmbRevene.SelectedIndex == 0)
			{
				strDefaultType = "E";
			}
			//"Revenue Accounts"
			else if (this.cmbRevene.SelectedIndex == 1)
			{
				strDefaultType = "R";
			}
		}

		private void vs1_ChangeEdit(object sender, System.EventArgs e)
        {
            if (accountGrid.IsCurrentCellInEditMode)
			{
				if (accountGrid.Col == AccountCol)
				{
					lblTitle.Text = modAccountTitle.ReturnAccountDescription(accountGrid.EditText);
				}
			}
		}

		private void vs1_DblClick(object sender, System.EventArgs e)
		{
			mnuFileComment_Click();
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (accountGrid.Row > 0)
			{
				modAccountTitle.DetermineAccountTitle(accountGrid.TextMatrix(accountGrid.Row, AccountCol), ref lblTitle);
			}
			if (accountGrid.Col == AccountCol)
			{
				//FC:FINAL:JEI:IIT790 correct parameter list
				modNewAccountBox.SetGridFormat(accountGrid, accountGrid.Row, accountGrid.Col, false, string.Empty, strDefaultType);
			}
			mnuProcessAdd.Enabled = true;
			mnuProcessDelete.Enabled = true;
			cmdAddAccount.Visible = true;
			cmdDeleteAccount.Visible = true;
            cmdAddComment.Visible = true;
        }

		private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
            if (e.Control != null)
			{
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (AccountCol == -1 || AccountCol == col)
				{
					if (e.Control is TextBox)
					{
						(e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					}
                    e.Control.KeyDown -= vs1_KeyDownEdit;
                    e.Control.KeyUp -= vs1_KeyUpEdit;
                    e.Control.KeyDown += vs1_KeyDownEdit;
                    e.Control.KeyUp += vs1_KeyUpEdit;
					DynamicObject customClientData = new DynamicObject();
					customClientData["GRID7Light_Col"] = col;
					customClientData["intAcctCol"] = AccountCol;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			//Keys keyCode = e.KeyCode;
			//if (vs1.Col == AccountCol && e.KeyCode != Keys.F9 && e.KeyCode != Keys.F2)
			//{
			//	if (keyCode == Keys.Right && vs1.EditSelStart == vs1.EditMaxLength)
			//	{
			//		keyCode = 0;
			//		Support.SendKeys("{TAB}", false);
			//	}
			//	else
			//	{
			//		string tmp = vs1.EditText;
			//		modNewAccountBox.CheckKeyDownEditF2(vs1, vs1.Row, vs1.Col, keyCode, e.Shift ? 1 : 0, vs1.EditSelStart, ref tmp, vs1.EditSelLength);
			//		vs1.EditText = tmp;
			//	}
			//}
			//else if (keyCode == Keys.Insert)
			//{
			//	keyCode = 0;
			//	mnuProcessAdd_Click();
			//}
		}

		private void vs1_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
				if (accountGrid.Col == accountGrid.Cols - 1 && accountGrid.Row == accountGrid.Rows - 1)
				{
					mnuProcessAdd_Click();
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			short keyAscii = FCConvert.ToInt16(Strings.Asc(e.KeyChar));
			if (accountGrid.Col == AccountCol)
			{
				//modNewAccountBox.CheckAccountKeyPress(vs1, vs1.Row, vs1.Col, keyAscii, vs1.EditSelStart, vs1.EditText);
			}
			if (keyAscii == 13)
			{
				keyAscii = 0;
				if (accountGrid.Col == accountGrid.Cols - 1 && accountGrid.Row == accountGrid.Rows - 1)
				{
					mnuProcessAdd_Click();
				}
				else
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (accountGrid.Col == AmountCol)
			{
				if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 27 && keyAscii != 8 && keyAscii != 45)
				{
					keyAscii = 0;
				}
			}
		}

		private void vs1_KeyUpEdit(object sender, KeyEventArgs e)
		{
            Keys keyCode = e.KeyCode;
            if (accountGrid.Col == AccountCol && e.KeyCode != Keys.F9 && e.KeyCode != Keys.F2)
            {
                if (keyCode == Keys.Right && accountGrid.EditSelStart == accountGrid.EditMaxLength)
                {
                    keyCode = 0;
                    Support.SendKeys("{TAB}", false);
                }
                else
                {
                    string tmp = accountGrid.EditText;
                    modNewAccountBox.CheckKeyDownEditF2(accountGrid, accountGrid.Row, accountGrid.Col, keyCode, e.Shift ? 1 : 0, accountGrid.EditSelStart, ref tmp, accountGrid.EditSelLength);
                    accountGrid.EditText = tmp;
                }
            }
            else if (keyCode == Keys.Insert)
            {
                keyCode = 0;
                mnuProcessAdd_Click();
            }
   //         if (vs1.Col == AccountCol)
			//{
			//	if (e.KeyCode != Keys.Down && e.KeyCode != Keys.Up)
			//	{
			//		// up and down arrows
			//		short keyCode = FCConvert.ToInt16(e.KeyCode);
			//		modNewAccountBox.CheckAccountKeyCode(vs1, vs1.Row, vs1.Col, keyCode, 0, vs1.EditSelStart, vs1.EditText, vs1.EditSelLength);
			//	}
			//}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (accountGrid.Row < 1)
			{
				return;
			}

			modAccountTitle.DetermineAccountTitle(accountGrid.TextMatrix(accountGrid.Row, AccountCol), ref lblTitle);
			if (accountGrid.Col < AmountCol)
			{
				if (accountGrid.TextMatrix(accountGrid.Row, AccountCol) == "")
				{
					accountGrid.Col = AccountCol;
					//FC:FINAL:JEI:IIT790 correct parameter list
					modNewAccountBox.SetGridFormat(accountGrid, accountGrid.Row, accountGrid.Col, true, string.Empty, strDefaultType);
					accountGrid.EditMaxLength = modNewAccountBox.Statics.intMaxLength;
				}
                //FC:FINAL:AM:#4324 - don't change the column here; it will cause the cell to exit edit mode
                //else
                //{
                //    vs1.Col = AmountCol;
                //    vs1.EditCell();
                //    vs1.EditSelStart = 0;
                //    if (vs1.TextMatrix(vs1.Row, AmountCol) != "")
                //    {
                //        if (vs1.TextMatrix(vs1.Row, AmountCol) == "0")
                //        {
                //            vs1.EditSelLength = 1;
                //        }
                //    }
                //}
            }
			else
			{
                accountGrid.EditCell();
                accountGrid.EditSelStart = 0;
                if (accountGrid.TextMatrix(accountGrid.Row, AmountCol) != "")
                {
                    if (accountGrid.TextMatrix(accountGrid.Row, AmountCol) == "0")
                    {
                        accountGrid.EditSelLength = 1;
                    }
                }
            }
		}

		private void SetCustomFormColors()
		{
			lblTitle.ForeColor = Color.Blue;
		}

		private void SaveInfo()
		{
			clsDRWrapper rsBudgetInfo = new clsDRWrapper();
			clsDRWrapper rsAccountMaster = new clsDRWrapper();
			int counter;
			if (boolSaving)
			{
				return;
			}
			accountGrid.Row = 0;
			//Application.DoEvents();
			boolSaving = true;
			frmBusy bWait = new frmBusy();
			FCUtils.StartTask(this, () =>
			{
				bWait.Message = "Saving";
				bWait.StartBusy();
				rsAccountMaster.OmitNullsOnInsert = true;
				rsBudgetInfo.OmitNullsOnInsert = true;
				// frmWait.Init "Please Wait....." & vbNewLine & " Saving Data", False
				for (counter = 1; counter <= accountGrid.Rows - 1; counter++)
				{
					//Application.DoEvents();
					if (counter <= accountGrid.Rows - 1)
					{
						if (accountGrid.RowHidden(counter))
						{
							rsBudgetInfo.Execute("DELETE FROM Budget WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
						}
						else
						{
							bWait.Message = "Saving " + accountGrid.TextMatrix(counter, AccountCol);
							if (modValidateAccount.AccountValidate(accountGrid.TextMatrix(counter, AccountCol)))
							{
								if (accountGrid.TextMatrix(counter, RecordCol) != "0")
								{
									if (modBudgetaryMaster.Statics.strBudgetFlag == "I")
									{
										rsBudgetInfo.Execute("UPDATE Budget SET InitialRequest = " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + " WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "M")
									{
										rsBudgetInfo.Execute("UPDATE Budget SET ManagerRequest = " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + " WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "C")
									{
										rsBudgetInfo.Execute("UPDATE Budget SET CommitteeRequest = " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + " WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "E")
									{
										rsBudgetInfo.Execute("UPDATE Budget SET ElectedRequest = " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + " WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "A")
									{
										rsBudgetInfo.Execute("UPDATE Budget SET ApprovedAmount = " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + " WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
									}
								}
								else
								{
									if (modBudgetaryMaster.Statics.strBudgetFlag == "I")
									{
										rsBudgetInfo.Execute("INSERT INTO Budget (Account, InitialRequest) VALUES ('" + accountGrid.TextMatrix(counter, AccountCol) + "', " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + ")", "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "M")
									{
										rsBudgetInfo.Execute("INSERT INTO Budget (Account, ManagerRequest) VALUES ('" + accountGrid.TextMatrix(counter, AccountCol) + "', " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + ")", "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "C")
									{
										rsBudgetInfo.Execute("INSERT INTO Budget (Account, CommitteeRequest) VALUES ('" + accountGrid.TextMatrix(counter, AccountCol) + "', " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + ")", "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "E")
									{
										rsBudgetInfo.Execute("INSERT INTO Budget (Account, ElectedRequest) VALUES ('" + accountGrid.TextMatrix(counter, AccountCol) + "', " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + ")", "Budgetary");
									}
									else if (modBudgetaryMaster.Statics.strBudgetFlag == "A")
									{
										rsBudgetInfo.Execute("INSERT INTO Budget (Account, ApprovedAmount) VALUES ('" + accountGrid.TextMatrix(counter, AccountCol) + "', " + FCConvert.ToString(FCConvert.ToDecimal(accountGrid.TextMatrix(counter, AmountCol))) + ")", "Budgetary");
									}
									rsAccountMaster.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + accountGrid.TextMatrix(counter, AccountCol) + "'");
									if (rsAccountMaster.EndOfFile() != true && rsAccountMaster.BeginningOfFile() != true)
									{
										// do nothing
									}
									else
									{
										rsAccountMaster.AddNew();
										rsAccountMaster.Set_Fields("AccountType", Strings.Left(accountGrid.TextMatrix(counter, AccountCol), 1));
										rsAccountMaster.Set_Fields("Account", Strings.Trim(accountGrid.TextMatrix(counter, AccountCol)));
										if (Strings.Left(accountGrid.TextMatrix(counter, AccountCol), 1) == "E")
										{
											if (modAccountTitle.Statics.ExpDivFlag)
											{
												if (modAccountTitle.Statics.ObjFlag)
												{
													rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetExpense(accountGrid.TextMatrix(counter, AccountCol)));
												}
												else
												{
													rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetExpense(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("ThirdAccountField", modBudgetaryAccounting.GetObject(accountGrid.TextMatrix(counter, AccountCol)));
												}
											}
											else
											{
												if (modAccountTitle.Statics.ObjFlag)
												{
													rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetExpDivision(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("ThirdAccountField", modBudgetaryAccounting.GetExpense(accountGrid.TextMatrix(counter, AccountCol)));
												}
												else
												{
													rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetExpDivision(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("ThirdAccountField", modBudgetaryAccounting.GetExpense(accountGrid.TextMatrix(counter, AccountCol)));
													rsAccountMaster.Set_Fields("FourthAccountField", modBudgetaryAccounting.GetObject(accountGrid.TextMatrix(counter, AccountCol)));
												}
											}
										}
										else if (Strings.Left(accountGrid.TextMatrix(counter, AccountCol), 1) == "R")
										{
											if (modAccountTitle.Statics.RevDivFlag)
											{
												rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
												rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetRevenue(accountGrid.TextMatrix(counter, AccountCol)));
											}
											else
											{
												rsAccountMaster.Set_Fields("FirstAccountField", modBudgetaryAccounting.GetDepartment(accountGrid.TextMatrix(counter, AccountCol)));
												rsAccountMaster.Set_Fields("SecondAccountField", modBudgetaryAccounting.GetRevDivision(accountGrid.TextMatrix(counter, AccountCol)));
												rsAccountMaster.Set_Fields("ThirdAccountField", modBudgetaryAccounting.GetRevenue(accountGrid.TextMatrix(counter, AccountCol)));
											}
										}
										rsAccountMaster.Set_Fields("DateCreated", DateTime.Today);
										rsAccountMaster.Set_Fields("CurrentBudget", 0);
										rsAccountMaster.Set_Fields("Valid", true);
										rsAccountMaster.Update();
									}
								}
							}
							else
							{
								if (accountGrid.TextMatrix(counter, RecordCol) != "0")
								{
									rsBudgetInfo.Execute("DELETE FROM Budget WHERE ID = " + accountGrid.TextMatrix(counter, RecordCol), "Budgetary");
								}
							}
						}
					}
				}
				bWait.StopBusy();
				bWait.Unload();
				FCUtils.UnlockUserInterface();
			});
			bWait.Show(FCForm.FormShowEnum.Modal);
			boolSaving = false;
			/*- bWait = null; */
			if (blnUnload)
			{
				// Unload frmWait
				Close();
				//MDIParent.InstancePtr.GRID.Focus();
			}
			else
			{
				cmdProcess_Click();
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            if (accountGrid.IsCurrentCellInEditMode)
			{
				int counter;
				//FC:FINAL:MSH - save and use correct indexes of the cell
				//int row = vs1.Row, col = vs1.Col;
				int row = accountGrid.GetFlexRowIndex(e.RowIndex);
				int col = accountGrid.GetFlexColIndex(e.ColumnIndex);
				if (col == AccountCol)
				{
					if (cmbRevene.SelectedIndex == 0)
					{
						if (Strings.Left(accountGrid.EditText, 1) != "E")
						{
							MessageBox.Show("You may only enter town expense accounts at this time.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
					}
					else
					{
						if (Strings.Left(accountGrid.EditText, 1) != "R")
						{
							MessageBox.Show("You may only enter town revenue accounts at this time.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
					}
					e.Cancel = modNewAccountBox.CheckAccountValidate(accountGrid, row, col, e.Cancel);
					if (e.Cancel == false)
					{
						for (counter = 1; counter <= accountGrid.Rows - 1; counter++)
						{
							if (counter != row)
							{
								if (accountGrid.EditText == accountGrid.TextMatrix(counter, AccountCol))
								{
									MessageBox.Show("This account has already been entered.", "Account Already Entered", MessageBoxButtons.OK, MessageBoxIcon.Information);
									e.Cancel = true;
									return;
								}
							}
						}
					}
				}
				else if (col == AmountCol)
				{
					if (!Information.IsNumeric(accountGrid.EditText))
					{
						accountGrid.EditText = "0";
					}
					else if (FCConvert.ToDecimal(accountGrid.EditText) < 0)
					{
						MessageBox.Show("You may not enter a negative amount for a budget.", "Negative Budget", MessageBoxButtons.OK, MessageBoxIcon.Information);
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click();
			if (!fraAccountType.Visible && !fraComments.Visible)
			{
				btnProcess.Text = "Save";
			}
			else
			{
				btnProcess.Text = "Process";
			}
		}

		private void cmdAddComment_Click(object sender, EventArgs e)
		{
			mnuFileComment_Click(sender, e);
		}

		private void cmdAddAccount_Click(object sender, EventArgs e)
		{
			mnuProcessAdd_Click(sender, e);
		}

		private void cmdDeleteAccount_Click(object sender, EventArgs e)
		{
			mnuProcessDelete_Click(sender, e);
		}
	}
}
