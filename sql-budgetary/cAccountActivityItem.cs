﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cAccountActivityItem
	{
		//=========================================================
		private int lngJournalNumber;
		private string strDescription = string.Empty;
		private string strRCB = string.Empty;
		private double dblDebits;
		private double dblCredits;
		private string strCheckNumber = string.Empty;
		private string strActivityDate = "";
		private string strVendorName = string.Empty;
		private int intPeriod;
		private string strJournalType = string.Empty;
		private string strAccount = string.Empty;
		private int lngVendorNumber;

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public double Debits
		{
			set
			{
				dblDebits = value;
			}
			get
			{
				double Debits = 0;
				Debits = dblDebits;
				return Debits;
			}
		}

		public double Credits
		{
			set
			{
				dblCredits = value;
			}
			get
			{
				double Credits = 0;
				Credits = dblCredits;
				return Credits;
			}
		}

		public string CheckNumber
		{
			set
			{
				strCheckNumber = value;
			}
			get
			{
				string CheckNumber = "";
				CheckNumber = strCheckNumber;
				return CheckNumber;
			}
		}

		public string ActivityDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strActivityDate = value;
				}
				else
				{
					strActivityDate = "";
				}
			}
			get
			{
				string ActivityDate = "";
				ActivityDate = strActivityDate;
				return ActivityDate;
			}
		}

		public string VendorName
		{
			set
			{
				strVendorName = value;
			}
			get
			{
				string VendorName = "";
				VendorName = strVendorName;
				return VendorName;
			}
		}

		public int Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				return intPeriod;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}
	}
}
