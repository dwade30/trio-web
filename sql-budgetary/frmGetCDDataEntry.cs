﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetCDDataEntry.
	/// </summary>
	public partial class frmGetCDDataEntry : BaseForm
	{
		public frmGetCDDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetCDDataEntry InstancePtr
		{
			get
			{
				return (frmGetCDDataEntry)Sys.GetInstance(typeof(frmGetCDDataEntry));
			}
		}

		protected frmGetCDDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();

		public void StartProgram()
		{
			int counter;
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			bool alreadyClosed = false;
			clsDRWrapper rsPeriod = new clsDRWrapper();
			int lngJournal;
			int intPeriod;
			//Application.DoEvents();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
			// center it
			frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
			frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			//Application.DoEvents();
			lngJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))));
			intPeriod = cboEntry.ItemData(cboEntry.SelectedIndex);
			rsPeriod.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Period = " + FCConvert.ToString(intPeriod), "Budgetary");
			modBudgetaryMaster.Statics.CurrentCDEntry = lngJournal;
			// initialize the current account number
			// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
			rs.OpenRecordset("SELECT * FROM JournalEntries WHERE JournalNumber = " + FCConvert.ToString(lngJournal) + " AND Type = 'D' and Status = 'E' AND Period = " + rsPeriod.Get_Fields("Period") + " ORDER BY ID", "Budgetary");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is a record with that account number
				rs.MoveLast();
				rs.MoveFirst();
				modBudgetaryMaster.Statics.blnCDEdit = true;
				// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
				frmCDDataEntry.InstancePtr.ChosenJournal = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCDDataEntry.InstancePtr.ChosenPeriod = FCConvert.ToInt32(rs.Get_Fields("Period"));
				if (rs.RecordCount() > 15)
				{
					frmCDDataEntry.InstancePtr.vs1.Rows = rs.RecordCount() + 1;
				}
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				frmCDDataEntry.InstancePtr.txtPeriod.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("Period")), 2);
				for (counter = 1; counter <= rs.RecordCount(); counter++)
				{
					//Application.DoEvents();
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(rs.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yy"));
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 2, FCConvert.ToString(rs.Get_Fields_String("Description")));
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 3, modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")), 5));
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 4, modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields_Int32("WarrantNumber")), 4));
					// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 5, FCConvert.ToString(rs.Get_Fields("CheckNumber")));
					// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 6, FCConvert.ToString(rs.Get_Fields("account")));
					// TODO Get_Fields: Check the table for the column [1099] and replace with corresponding Get_Field method
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 7, FCConvert.ToString(rs.Get_Fields("1099")));
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 8, FCConvert.ToString(rs.Get_Fields_String("Project")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 9, FCConvert.ToString(rs.Get_Fields("Amount")));
					if (rs.EndOfFile() != true)
					{
						rs.MoveNext();
					}
				}
				if (rs.RecordCount() < 15)
				{
					for (counter = rs.RecordCount() + 1; counter <= frmCDDataEntry.InstancePtr.vs1.Rows - 1; counter++)
					{
						//Application.DoEvents();
						frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 0, "0");
						frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 1, Strings.Format(DateTime.Today, "MM/dd/yy"));
						frmCDDataEntry.InstancePtr.vs1.TextMatrix(counter, 9, "0");
					}
				}
				//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
				Close();
				//FC:FINAL:MSH - Issue #426: showing new form before closing current form not always set focus to the new form.
				// Hiding current form before closing gives opportunity to set focus correctly
				//this.Hide();
				frmCDDataEntry.InstancePtr.Show(App.MainForm);
				alreadyClosed = true;
				// show the form
				modRegistry.SaveRegistryKey("CURRCDJRNL", FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCDEntry));
			}
			else
			{
				// else let the user know that no account was found
				cmbUpdate.SelectedIndex = 1;
				cmdGetAccountNumber_Click();
			}
			// Me.Hide
			frmWait.InstancePtr.Unload();
			//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
			if (!alreadyClosed)
			{
				Close();
			}
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			if (cboEntry.SelectedIndex != 0)
			{
				// if there is a valid account number
				if (cmbUpdate.SelectedIndex == 1)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnCDEdit = false;
					frmCDDataEntry.InstancePtr.ChosenJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4))));
					frmCDDataEntry.InstancePtr.ChosenPeriod = cboEntry.ItemData(cboEntry.SelectedIndex);
					//FC:FINAL:MSH - Issue #426: showing new form before closing current form not always set focus to the new form.
					// Hiding current form before closing gives opportunity to set focus correctly
					//this.Hide();
					frmCDDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
				}
				else
				{
					modBudgetaryMaster.Statics.CurrentCDEntry = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Left(cboEntry.Items[cboEntry.SelectedIndex].ToString(), 4)));
					// set the account number to the one last used
					StartProgram();
					// call the procedure to retrieve the info
				}
			}
			else
			{
				if (cmbUpdate.SelectedIndex == 1)
				{
					//FC:FINAL:DDU:#2852 - fixed showing new form before closing current form
					Close();
					modBudgetaryMaster.Statics.blnCDEdit = false;
					frmCDDataEntry.InstancePtr.ChosenJournal = 0;
					frmCDDataEntry.InstancePtr.ChosenPeriod = 0;
					//FC:FINAL:MSH - Issue #426: showing new form before closing current form not always set focus to the new form.
					// Hiding current form before closing gives opportunity to set focus correctly
					//this.Hide();
					frmCDDataEntry.InstancePtr.Show(App.MainForm);
					// show the blankform
					// Me.Hide
				}
				else
				{
					MessageBox.Show("You may only update an existing journal.", "Invalid Choice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void frmGetCDDataEntry_Activated(object sender, System.EventArgs e)
		{
			int counter;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboEntry.AddItem("0000 - New Journal Entry");
			rs.OpenRecordset("SELECT * FROM JournalMaster WHERE Status = 'E' AND Type = 'CD' ORDER BY JournalNumber DESC");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				while (!rs.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboEntry.AddItem(modValidateAccount.GetFormat_6(FCConvert.ToString(rs.Get_Fields("JournalNumber")), 4) + " - " + "Per " + rs.Get_Fields("Period") + " - " + rs.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
					cboEntry.ItemData(cboEntry.NewIndex, FCConvert.ToInt32(Conversion.Val(rs.Get_Fields("Period"))));
					rs.MoveNext();
				}
			}
			cboEntry.SelectedIndex = 0;
			cboEntry.Focus();
			modBudgetaryMaster.Statics.CurrentCDEntry = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("CURRCDJRNL"))));
			cmbUpdate.SelectedIndex = 1;
			lblLastAccount.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(modBudgetaryMaster.Statics.CurrentCDEntry), 4);
		}

		private void frmGetCDDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				// handles the enter key
				KeyAscii = (Keys)0;
				if (frmGetCDDataEntry.InstancePtr.ActiveControl.GetName() == "cboEntry")
					cmdGetAccountNumber_Click();
			}
			else if (KeyAscii == Keys.Escape)
			{
				// handles the escape key
				KeyAscii = (Keys)0;
				frmGetCDDataEntry.InstancePtr.Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetCDDataEntry_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetCDDataEntry.ScaleWidth	= 9045;
			//frmGetCDDataEntry.ScaleHeight	= 7245;
			//frmGetCDDataEntry.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void SetCombo(int x)
		{
			int counter;
			for (counter = 0; counter <= cboEntry.Items.Count - 1; counter++)
			{
				if (modValidateAccount.GetFormat_6(FCConvert.ToString(x), 4) == Strings.Left(cboEntry.Items[counter].ToString(), 4))
				{
					cboEntry.SelectedIndex = counter;
					return;
				}
			}
		}

		private void cboEntry_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEntry.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 320, 0);
		}
	}
}
