//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomBudgetReport.
	/// </summary>
	partial class frmCustomBudgetReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPortrait;
		public fecherFoundation.FCLabel lblPortrait;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCTextBox Text1;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomBudgetReport));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.cmbPortrait = new fecherFoundation.FCComboBox();
			this.lblPortrait = new fecherFoundation.FCLabel();
			this.cmbReport = new fecherFoundation.FCComboBox();
			this.lblReport = new fecherFoundation.FCLabel();
			this.Text1 = new fecherFoundation.FCTextBox();
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.cboSavedReport = new fecherFoundation.FCComboBox();
			this.cmdAdd = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.fraFields = new fecherFoundation.FCFrame();
			this.lstFields = new fecherFoundation.FCDraggableListBox();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.vsLayout = new fecherFoundation.FCGrid();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.btnClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 534);
			this.BottomPanel.Size = new System.Drawing.Size(1042, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPortrait);
			this.ClientArea.Controls.Add(this.lblPortrait);
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Size = new System.Drawing.Size(1042, 474);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnClear);
			this.TopPanel.Size = new System.Drawing.Size(1042, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.btnClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(177, 30);
			this.HeaderText.Text = "Custom Budget Report";
			// 
			// cmbPortrait
			// 
			this.cmbPortrait.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
			this.cmbPortrait.Location = new System.Drawing.Point(852, 469);
			this.cmbPortrait.Name = "cmbPortrait";
			this.cmbPortrait.Size = new System.Drawing.Size(139, 40);
			this.cmbPortrait.TabIndex = 1;
			// 
			// lblPortrait
			// 
			this.lblPortrait.Location = new System.Drawing.Point(690, 483);
			this.lblPortrait.Name = "lblPortrait";
			this.lblPortrait.Size = new System.Drawing.Size(127, 16);
			this.lblPortrait.TabIndex = 2;
			this.lblPortrait.Text = "REPORT ORIENTATION";
			// 
			// cmbReport
			// 
			this.cmbReport.Items.AddRange(new object[] {
            "Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
			this.cmbReport.Location = new System.Drawing.Point(103, 30);
			this.cmbReport.Name = "cmbReport";
			this.cmbReport.Size = new System.Drawing.Size(198, 40);
			this.cmbReport.TabIndex = 12;
			this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
			// 
			// lblReport
			// 
			this.lblReport.Location = new System.Drawing.Point(20, 44);
			this.lblReport.Name = "lblReport";
			this.lblReport.Size = new System.Drawing.Size(48, 16);
			this.lblReport.TabIndex = 13;
			this.lblReport.Text = "REPORT";
			// 
			// Text1
			// 
			this.Text1.Appearance = 0;
			this.Text1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Text1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Text1.Location = new System.Drawing.Point(670, 529);
			this.Text1.Multiline = true;
			this.Text1.Name = "Text1";
			this.Text1.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.Text1.Size = new System.Drawing.Size(321, 148);
			this.Text1.TabIndex = 1;
			this.Text1.Text = "If you select more than 5 fields to \r\nprint then the report will \r\nautomatically " +
    "print in landscape \r\nformat whether you have it \r\nselected as your orientation o" +
    "r not";
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 469);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(640, 208);
			this.fraWhere.TabIndex = 5;
			this.fraWhere.Text = "Selection Criteria";
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// vsWhere
			// 
			this.vsWhere.Cols = 10;
			this.vsWhere.ColumnHeadersVisible = false;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.Location = new System.Drawing.Point(20, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.ReadOnly = false;
			this.vsWhere.Rows = 0;
			this.vsWhere.ShowFocusCell = false;
			this.vsWhere.Size = new System.Drawing.Size(600, 158);
			this.vsWhere.StandardTab = false;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 6;
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cboSavedReport);
			this.Frame2.Controls.Add(this.cmbReport);
			this.Frame2.Controls.Add(this.lblReport);
			this.Frame2.Controls.Add(this.cmdAdd);
			this.Frame2.Location = new System.Drawing.Point(670, 259);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(321, 190);
			this.Frame2.TabIndex = 8;
			this.Frame2.Text = "Report";
			// 
			// cboSavedReport
			// 
			this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboSavedReport.Location = new System.Drawing.Point(20, 80);
			this.cboSavedReport.Name = "cboSavedReport";
			this.cboSavedReport.Size = new System.Drawing.Size(281, 40);
			this.cboSavedReport.TabIndex = 11;
			this.cboSavedReport.Visible = false;
			this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
			// 
			// cmdAdd
			// 
			this.cmdAdd.AppearanceKey = "actionButton";
			this.cmdAdd.Location = new System.Drawing.Point(20, 130);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(281, 40);
			this.cmdAdd.TabIndex = 9;
			this.cmdAdd.Text = "Add Custom Report to Library";
			this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Enabled = false;
			this.fraSort.Location = new System.Drawing.Point(350, 259);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(300, 190);
			this.fraSort.TabIndex = 14;
			this.fraSort.Text = "Fields To Sort By";
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(260, 140);
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 15;
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Location = new System.Drawing.Point(30, 259);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(300, 190);
			this.fraFields.TabIndex = 17;
			this.fraFields.Text = "Fields To Display";
			this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
			// 
			// lstFields
			// 
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.CheckBoxes = true;
			this.lstFields.Location = new System.Drawing.Point(20, 30);
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(260, 140);
			this.lstFields.Style = 1;
			this.lstFields.TabIndex = 18;
			this.lstFields.ItemChecked += new Wisej.Web.ItemCheckedEventHandler(this.lstFields_SelectedIndexChanged);
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.fraMessage);
			this.Frame3.Controls.Add(this.vsLayout);
			this.Frame3.Controls.Add(this.Image1);
			this.Frame3.Controls.Add(this.Label1);
			this.Frame3.Controls.Add(this.Label2);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(1005, 239);
			this.Frame3.TabIndex = 20;
			// 
			// fraMessage
			// 
			this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraMessage.AppearanceKey = "groupBoxNoBorders";
			this.fraMessage.Controls.Add(this.Label3);
			this.fraMessage.Location = new System.Drawing.Point(78, 72);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(907, 146);
			this.fraMessage.TabIndex = 21;
			// 
			// Label3
			// 
			this.Label3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Label3.Location = new System.Drawing.Point(20, 60);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(867, 52);
			this.Label3.TabIndex = 22;
			this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// vsLayout
			// 
			this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsLayout.Enabled = false;
			this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.vsLayout.FixedCols = 0;
			this.vsLayout.Location = new System.Drawing.Point(78, 72);
			this.vsLayout.Name = "vsLayout";
			this.vsLayout.ReadOnly = false;
			this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.Rows = 1;
			this.vsLayout.ShowFocusCell = false;
			this.vsLayout.Size = new System.Drawing.Size(907, 146);
			this.vsLayout.TabIndex = 24;
			this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
			this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
			// 
			// Image1
			// 
			this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(30, 30);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(955, 22);
			this.Image1.TabIndex = 25;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 72);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(9, 60);
			this.Label1.TabIndex = 26;
			this.Label1.Text = "L E F T";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(49, 72);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(9, 86);
			this.Label2.TabIndex = 25;
			this.Label2.Text = "M A R G I N";
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuClear
			// 
			this.mnuClear.Index = -1;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Selection Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = -1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Layout";
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSeperator2,
            this.mnuFilePrint,
            this.mnuPreview,
            this.mnuSP1,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 0;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 1;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuPreview
			// 
			this.mnuPreview.Index = 2;
			this.mnuPreview.Name = "mnuPreview";
			this.mnuPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPreview.Text = "Print / Preview ";
			this.mnuPreview.Click += new System.EventHandler(this.mnuPreview_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 3;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(494, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(78, 48);
			this.cmdPrint.TabIndex = 10;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// btnClear
			// 
			this.btnClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnClear.Location = new System.Drawing.Point(856, 29);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(155, 24);
			this.btnClear.TabIndex = 11;
			this.btnClear.Text = "Clear Selection Criteria";
			this.btnClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// frmCustomBudgetReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1042, 642);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCustomBudgetReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Custom Budget Report";
			this.Load += new System.EventHandler(this.frmCustomBudgetReport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomBudgetReport_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomBudgetReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnClear)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdPrint;
		private FCButton btnClear;
	}
}