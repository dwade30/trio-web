﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptCheckRecList.
	/// </summary>
	public partial class rptCheckRecList : BaseSectionReport
	{
		public static rptCheckRecList InstancePtr
		{
			get
			{
				return (rptCheckRecList)Sys.GetInstance(typeof(rptCheckRecList));
			}
		}

		protected rptCheckRecList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCheckRecList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private cGenericCollection reportList;
		private int lngPageNumber;
		private double dblTotal;
		private int lngCount;

		public rptCheckRecList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Bank Reconciliation";
		}

		public void Init(ref cGenericCollection checkList, string strSubTitle)
		{
			reportList = checkList;
			lblSubTitle.Text = strSubTitle;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!(reportList == null))
			{
				eArgs.EOF = !reportList.IsCurrent();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			lngCount = 0;
			dblTotal = 0;
			lngPageNumber = 0;
			if (!(reportList == null))
			{
				reportList.MoveFirst();
			}
			else
			{
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!(reportList == null))
			{
				if (reportList.IsCurrent())
				{
					cCheckRecMaster crmaster;
					crmaster = (cCheckRecMaster)reportList.GetCurrentItem();
					dblTotal += crmaster.Amount;
					lngCount += 1;
					txtCheckNumber.Text = FCConvert.ToString(crmaster.CheckNumber);
					txtAmount.Text = Strings.Format(crmaster.Amount, "#,###,###,##0.00");
					if (Information.IsDate(crmaster.CheckDate))
					{
						txtCheckDate.Text = Strings.Format(crmaster.CheckDate, "MM/dd/yyyy");
					}
					else
					{
						txtCheckDate.Text = "";
					}
					if (Strings.UCase(crmaster.Status) == "1")
					{
						txtStatus.Text = "Issued";
					}
					else if (Strings.UCase(crmaster.Status) == "2")
					{
						txtStatus.Text = "Outstanding";
					}
					else if (Strings.UCase(crmaster.Status) == "3")
					{
						txtStatus.Text = "Cashed";
					}
					else if (Strings.UCase(crmaster.Status) == "V")
					{
						txtStatus.Text = "Voided";
					}
					else if (Strings.UCase(crmaster.Status) == "D")
					{
						txtStatus.Text = "Deleted";
					}
					txtPayee.Text = crmaster.Name;
					string vbPorterVar = crmaster.CheckType;
					if (vbPorterVar == "1")
					{
						txtType.Text = "AP";
					}
					else if (vbPorterVar == "2")
					{
						txtType.Text = "PY";
					}
					else if (vbPorterVar == "3")
					{
						txtType.Text = "DP";
					}
					else if (vbPorterVar == "4")
					{
						txtType.Text = "RT";
					}
					else if (vbPorterVar == "5")
					{
						txtType.Text = "IN";
					}
					else if (vbPorterVar == "6")
					{
						txtType.Text = "OC";
					}
					else if (vbPorterVar == "7")
					{
						txtType.Text = "OD";
					}
					reportList.MoveNext();
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lngPageNumber += 1;
			lblPage.Text = "Page " + FCConvert.ToString(lngPageNumber);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotal.Text = Strings.Format(dblTotal, "#,###,###,##0.00");
			txtCount.Text = Strings.Format(lngCount, "#,###,###,##0");
		}

		private void rptCheckRecList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCheckRecList.Caption	= "Check Reconciliation";
			//rptCheckRecList.Left	= 0;
			//rptCheckRecList.Top	= 0;
			//rptCheckRecList.Width	= 19650;
			//rptCheckRecList.Height	= 8490;
			//rptCheckRecList.StartUpPosition	= 3;
			//rptCheckRecList.SectionData	= "rptCheckRecList.dsx":0000;
			//End Unmaped Properties
		}
	}
}
