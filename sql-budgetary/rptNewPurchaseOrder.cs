﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptNewPurchaseOrder.
	/// </summary>
	public partial class rptNewPurchaseOrder : BaseSectionReport
	{
		public static rptNewPurchaseOrder InstancePtr
		{
			get
			{
				return (rptNewPurchaseOrder)Sys.GetInstance(typeof(rptNewPurchaseOrder));
			}
		}

		protected rptNewPurchaseOrder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
				rsDetail.Dispose();
				rsVendorInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptNewPurchaseOrder	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngLineCounter;
		// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
		Decimal curTotal;
		bool blnFirstRecord;
		public int lngID;
		public bool blnIncludeAdjustments;
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rsDetail = new clsDRWrapper();
		clsDRWrapper rsVendorInfo = new clsDRWrapper();

		public rptNewPurchaseOrder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Purchase Order";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngID == 0)
			{
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					if (frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 4) != "0")
					{
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				CheckNext:
				;
				lngLineCounter += 1;
				if (lngLineCounter <= frmEncumbranceDataEntry.InstancePtr.vs1.Rows - 1)
				{
					if (frmEncumbranceDataEntry.InstancePtr.vs1.TextMatrix(lngLineCounter, 4) != "0")
					{
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				if (blnFirstRecord)
				{
					eArgs.EOF = false;
					blnFirstRecord = false;
				}
				else
				{
					rsDetail.MoveNext();
					eArgs.EOF = rsDetail.EndOfFile();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsDept = new clsDRWrapper();
			clsDRWrapper rsIcon = new clsDRWrapper();
            try
            {
                //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
                if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
                {
                    Label2.Text = Strings.UCase(modGlobalConstants.Statics.gstrCityTown) + " OF " +
                                  Strings.UCase(modGlobalConstants.Statics.MuniName);
                }
                else
                {
                    Label2.Text = Strings.UCase(modGlobalConstants.Statics.MuniName);
                }

                rs.OpenRecordset("SELECT * FROM PurchaseOrders WHERE ID = " + FCConvert.ToString(lngID));
                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    fldVendorNumber.Text = FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber"));
                    rsVendorInfo.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                               rs.Get_Fields_Int32("VendorNumber"));
                    if (rsVendorInfo.EndOfFile() != true && rsVendorInfo.BeginningOfFile() != true)
                    {
                        fldVendorName.Text = rsVendorInfo.Get_Fields_String("CheckName");
                        fldVendorAddress1.Text = rsVendorInfo.Get_Fields_String("CheckAddress1");
                        fldVendorAddress2.Text = rsVendorInfo.Get_Fields_String("CheckAddress2");
                        fldVendorAddress3.Text = rsVendorInfo.Get_Fields_String("CheckAddress3");
                        if (Strings.Trim(FCConvert.ToString(rsVendorInfo.Get_Fields_String("CheckZip4"))) != "")
                        {
                            fldVendorAddress4.Text = rsVendorInfo.Get_Fields_String("CheckCity") + ", " +
                                                     rsVendorInfo.Get_Fields_String("CheckState") + " " +
                                                     rsVendorInfo.Get_Fields_String("CheckZip") + "-" +
                                                     rsVendorInfo.Get_Fields_String("CheckZip4");
                        }
                        else
                        {
                            fldVendorAddress4.Text = rsVendorInfo.Get_Fields_String("CheckCity") + ", " +
                                                     rsVendorInfo.Get_Fields_String("CheckState") + " " +
                                                     rsVendorInfo.Get_Fields_String("CheckZip");
                        }
                    }
                    else
                    {
                        fldVendorName.Text = "UNKNOWN";
                        fldVendorAddress1.Text = "";
                        fldVendorAddress2.Text = "";
                        fldVendorAddress3.Text = "";
                        fldVendorAddress4.Text = "";
                    }

                    if (Strings.Trim(fldVendorAddress3.Text) == "")
                    {
                        fldVendorAddress3.Text = fldVendorAddress4.Text;
                        fldVendorAddress4.Text = "";
                    }

                    if (Strings.Trim(fldVendorAddress2.Text) == "")
                    {
                        fldVendorAddress2.Text = fldVendorAddress3.Text;
                        fldVendorAddress3.Text = fldVendorAddress4.Text;
                        fldVendorAddress4.Text = "";
                    }

                    if (Strings.Trim(fldVendorAddress1.Text) == "")
                    {
                        fldVendorAddress1.Text = fldVendorAddress2.Text;
                        fldVendorAddress2.Text = fldVendorAddress3.Text;
                        fldVendorAddress3.Text = fldVendorAddress4.Text;
                        fldVendorAddress4.Text = "";
                    }

                    fldInvoiceName.Text = modRegistry.GetRegistryKey("TownName");
                    fldInvoiceAddress1.Text = modRegistry.GetRegistryKey("TownAddress1");
                    fldInvoiceAddress2.Text = modRegistry.GetRegistryKey("TownAddress2");
                    fldInvoiceAddress3.Text = modRegistry.GetRegistryKey("TownAddress3");
                    fldInvoiceAddress4.Text = modRegistry.GetRegistryKey("TownAddress4");
                    fldName.Text = modRegistry.GetRegistryKey("ShipToTownName");
                    fldAddress1.Text = modRegistry.GetRegistryKey("ShipToTownAddress1");
                    fldAddress2.Text = modRegistry.GetRegistryKey("ShipToTownAddress2");
                    fldAddress3.Text = modRegistry.GetRegistryKey("ShipToTownAddress3");
                    fldAddress4.Text = modRegistry.GetRegistryKey("ShipToTownAddress4");
                    // TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
                    fldPO.Text = FCConvert.ToString(rs.Get_Fields("PO"));
                    fldDescription.Text = rs.Get_Fields_String("Description");
                    fldDate.Text = Strings.Format(rs.Get_Fields_DateTime("PODate"), "MM/dd/yyyy");
                    rsDept.OpenRecordset(
                        "SELECT * FROM DeptDivTitles WHERE Department = '" + rs.Get_Fields_String("Department") +
                        "' AND convert(int, IsNull(Division, 0)) = 0", "TWBD0000.vb1");
                    if (rsDept.EndOfFile() != true && rsDept.BeginningOfFile() != true)
                    {
                        fldDepartment.Text = rs.Get_Fields_String("Department") + " - " +
                                             rsDept.Get_Fields_String("LongDescription");
                    }
                    else
                    {
                        fldDepartment.Text = rs.Get_Fields_String("Department") + " - UNKNOWN";
                    }

                    rsDetail.OpenRecordset("SELECT * FROM PurchaseOrderDetails WHERE PurchaseOrderID = " +
                                           FCConvert.ToString(lngID));
                }
                else
                {
                    MessageBox.Show("No information found for that PO.", "No Info", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    this.Cancel();
                    return;
                }

                rsIcon.OpenRecordset("SELECT * FROM Budgetary");
                if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("ShowIcon")))
                {
                    if (FCConvert.ToBoolean(rsIcon.Get_Fields_Boolean("UseTownSeal")))
                    {
                        if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic"))
                            {
                                imgIcon.Image =
                                    FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "\\TownSeal.pic");
                            }
                        }
                        else
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic"))
                            {
                                imgIcon.Image =
                                    FCUtils.LoadPicture(FCFileSystem.Statics.UserDataFolder + "TownSeal.pic");
                            }
                        }
                    }
                    else
                    {
                        if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) != "\\")
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" +
                                            rsIcon.Get_Fields_String("CheckIcon")))
                            {
                                imgIcon.Image = FCUtils.LoadPicture(
                                    FCFileSystem.Statics.UserDataFolder + "\\CheckIcons\\" +
                                    rsIcon.Get_Fields_String("CheckIcon"));
                            }
                        }
                        else
                        {
                            if (File.Exists(FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" +
                                            rsIcon.Get_Fields_String("CheckIcon")))
                            {
                                imgIcon.Image = FCUtils.LoadPicture(
                                    FCFileSystem.Statics.UserDataFolder + "CheckIcons\\" +
                                    rsIcon.Get_Fields_String("CheckIcon"));
                            }
                        }
                    }
                }

                blnFirstRecord = true;
                curTotal = 0;
            }
            finally
            {
                rsIcon.Dispose();
                rsDept.Dispose();
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDetailDescription.Text = rsDetail.Get_Fields_String("Description");
			// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsDetail.Get_Fields("Account"));
			// TODO Get_Fields: Check the table for the column [Quantity] and replace with corresponding Get_Field method
			fldQuantity.Text = Strings.Format(rsDetail.Get_Fields("Quantity"), "#,##0.0000");
			fldPrice.Text = Strings.Format(rsDetail.Get_Fields_Decimal("Price"), "#,##0.0000");
			fldLineTotal.Text = Strings.Format(rsDetail.Get_Fields_Decimal("Total"), "#,##0.00");
			curTotal += FCConvert.ToDecimal(fldLineTotal.Text);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotal.Text = Strings.Format(curTotal, "$#,##0.00");
			fldComments.Text = rs.Get_Fields_String("Comments");
		}

		

		private void rptNewPurchaseOrder_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
