﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptCustomCheckDoubleStub.
	/// </summary>
	public partial class srptCustomCheckDoubleStub : FCSectionReport
	{
		public static srptCustomCheckDoubleStub InstancePtr
		{
			get
			{
				return (srptCustomCheckDoubleStub)Sys.GetInstance(typeof(srptCustomCheckDoubleStub));
			}
		}

		protected srptCustomCheckDoubleStub _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCustomCheckDoubleStub	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsReportPrinterFunctions instReportFunctions = new clsReportPrinterFunctions();

		public srptCustomCheckDoubleStub()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strLargeFonttoUse = "";
			int X;
			int y;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (rptCustomCheck.InstancePtr.strCheckType == "D")
			{
				strLargeFonttoUse = instReportFunctions.GetFont(this.Document.Printer.PrinterName, 12, "Roman 12cpi");
				if (strLargeFonttoUse != string.Empty)
				{
					instReportFunctions.SetReportFontsByTag(this, "Large", strLargeFonttoUse);
				}
				for (y = 0; y <= this.Sections.Count - 1; y++)
				{
					for (X = 0; X <= this.Sections[y].Controls.Count - 1; X++)
					{
						if (Strings.UCase(FCConvert.ToString(this.Sections[y].Controls[X].Tag)) == "LARGE")
						{
							GrapeCity.ActiveReports.SectionReportModel.TextBox textbox = this.Sections[y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
							if (textbox != null)
							{
								textbox.Font = new Font(textbox.Font.Name, 10);
							}
							else
							{
								GrapeCity.ActiveReports.SectionReportModel.Label label = this.Sections[y].Controls[X] as GrapeCity.ActiveReports.SectionReportModel.Label;
								if (label != null)
								{
									label.Font = new Font(label.Font.Name, 10);
								}
							}
						}
					}
					// X
				}
				// y
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int counter;
            using (clsDRWrapper rsCheckMessage = new clsDRWrapper())
            {
                if (rptCustomCheck.InstancePtr.lngFooterVendorNumber != 0)
                {
                    rsCheckMessage.OpenRecordset("SELECT * FROM VendorMaster WHERE VendorNumber = " +
                                                 FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterVendorNumber));
                    if (rsCheckMessage.EndOfFile() != true && rsCheckMessage.BeginningOfFile() != true)
                    {
                        if (Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage"))) != "")
                        {
                            fldCheckMessage.Text =
                                Strings.Trim(FCConvert.ToString(rsCheckMessage.Get_Fields_String("CheckMessage")));
                            if (FCConvert.ToBoolean(rsCheckMessage.Get_Fields_Boolean("OneTimeCheckMessage")))
                            {
                                rsCheckMessage.Edit();
                                rsCheckMessage.Set_Fields("CheckMessage", "");
                                rsCheckMessage.Set_Fields("OneTimeCheckMessage", false);
                                rsCheckMessage.Update();
                            }
                        }
                        else
                        {
                            fldCheckMessage.Text = rptCustomCheck.InstancePtr.strCheckMessage;
                        }
                    }
                    else
                    {
                        fldCheckMessage.Text = rptCustomCheck.InstancePtr.strCheckMessage;
                    }
                }
                else
                {
                    fldCheckMessage.Text = rptCustomCheck.InstancePtr.strCheckMessage;
                }

                fldVendor.Text =
                    modValidateAccount.GetFormat_6(FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterVendorNumber),
                        5) + "  " + rptCustomCheck.InstancePtr.strFooterVendorName;
                fldDate.Text = Strings.Format(rptCustomCheck.InstancePtr.datPayDate, "MM/dd/yy");
                fldCheck.Text = FCConvert.ToString(rptCustomCheck.InstancePtr.lngFooterCheck);
                for (counter = 0; counter <= 19; counter++)
                {
                    FillRows(ref counter, ref modBudgetaryMaster.Statics.strWarr[counter],
                        ref modBudgetaryMaster.Statics.strDesc[counter], ref modBudgetaryMaster.Statics.strRef[counter],
                        ref modBudgetaryMaster.Statics.curCredit[counter],
                        ref modBudgetaryMaster.Statics.curDisc[counter],
                        ref modBudgetaryMaster.Statics.curAmt[counter]);
                }

                if (rptCustomCheck.InstancePtr.curCarryOver != 0)
                {
                    fldMuniName.Text = "";
                    fldAmount.Text = "";
                    lblAmount2.Visible = false;
                }
                else
                {
                    fldMuniName.Text = modGlobalConstants.Statics.MuniName;
                    fldAmount.Text = Strings.Format(rptCustomCheck.InstancePtr.curTotal, "#,##0.00");
                    fldAmount.Text = Strings.StrDup(13 - fldAmount.Text.Length, "*") + fldAmount.Text;
                    lblAmount2.Visible = true;
                }
            }
        }
		// vbPorter upgrade warning: intRow As short	OnWriteFCConvert.ToInt32(
		private void FillRows(ref int intRow, ref string strWarr, ref string strDesc, ref string strRef, ref string curCredit, ref string curDisc, ref string curAmt)
		{
			switch (intRow)
			{
				case 0:
					{
						FillRow1(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 1:
					{
						FillRow2(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 2:
					{
						FillRow3(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 3:
					{
						FillRow4(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 4:
					{
						FillRow5(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 5:
					{
						FillRow6(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 6:
					{
						FillRow7(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 7:
					{
						FillRow8(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 8:
					{
						FillRow9(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 9:
					{
						FillRow10(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 10:
					{
						FillRow11(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 11:
					{
						FillRow12(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 12:
					{
						FillRow13(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 13:
					{
						FillRow14(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 14:
					{
						FillRow15(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 15:
					{
						FillRow16(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 16:
					{
						FillRow17(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 17:
					{
						FillRow18(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 18:
					{
						FillRow19(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
				case 19:
					{
						FillRow20(ref strWarr, ref strDesc, ref strRef, ref curCredit, ref curDisc, ref curAmt);
						break;
					}
			}
			//end switch
		}

		private void FillRow1(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant1.Text = strWarr;
			fldDescription1.Text = strDesc;
			fldReference1.Text = strRef;
			fldDiscount1.Text = strDisc;
			fldAmt1.Text = strAmt;
			fldCredit1.Text = strCredit;
		}

		private void FillRow2(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant2.Text = strWarr;
			fldDescription2.Text = strDesc;
			fldReference2.Text = strRef;
			fldDiscount2.Text = strDisc;
			fldAmt2.Text = strAmt;
			fldCredit2.Text = strCredit;
		}

		private void FillRow3(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant3.Text = strWarr;
			fldDescription3.Text = strDesc;
			fldReference3.Text = strRef;
			fldDiscount3.Text = strDisc;
			fldAmt3.Text = strAmt;
			fldCredit3.Text = strCredit;
		}

		private void FillRow4(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant4.Text = strWarr;
			fldDescription4.Text = strDesc;
			fldReference4.Text = strRef;
			fldDiscount4.Text = strDisc;
			fldAmt4.Text = strAmt;
			fldCredit4.Text = strCredit;
		}

		private void FillRow5(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant5.Text = strWarr;
			fldDescription5.Text = strDesc;
			fldReference5.Text = strRef;
			fldDiscount5.Text = strDisc;
			fldAmt5.Text = strAmt;
			fldCredit5.Text = strCredit;
		}

		private void FillRow6(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant6.Text = strWarr;
			fldDescription6.Text = strDesc;
			fldReference6.Text = strRef;
			fldDiscount6.Text = strDisc;
			fldAmt6.Text = strAmt;
			fldCredit6.Text = strCredit;
		}

		private void FillRow7(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant7.Text = strWarr;
			fldDescription7.Text = strDesc;
			fldReference7.Text = strRef;
			fldDiscount7.Text = strDisc;
			fldAmt7.Text = strAmt;
			fldCredit7.Text = strCredit;
		}

		private void FillRow8(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant8.Text = strWarr;
			fldDescription8.Text = strDesc;
			fldReference8.Text = strRef;
			fldDiscount8.Text = strDisc;
			fldAmt8.Text = strAmt;
			fldCredit8.Text = strCredit;
		}

		private void FillRow9(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant9.Text = strWarr;
			fldDescription9.Text = strDesc;
			fldReference9.Text = strRef;
			fldDiscount9.Text = strDisc;
			fldAmt9.Text = strAmt;
			fldCredit9.Text = strCredit;
		}

		private void FillRow10(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant10.Text = strWarr;
			fldDescription10.Text = strDesc;
			fldReference10.Text = strRef;
			fldDiscount10.Text = strDisc;
			fldAmt10.Text = strAmt;
			fldCredit10.Text = strCredit;
		}

		private void FillRow11(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant11.Text = strWarr;
			fldDescription11.Text = strDesc;
			fldReference11.Text = strRef;
			fldDiscount11.Text = strDisc;
			fldAmt11.Text = strAmt;
			fldCredit11.Text = strCredit;
		}

		private void FillRow12(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant12.Text = strWarr;
			fldDescription12.Text = strDesc;
			fldReference12.Text = strRef;
			fldDiscount12.Text = strDisc;
			fldAmt12.Text = strAmt;
			fldCredit12.Text = strCredit;
		}

		private void FillRow13(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant13.Text = strWarr;
			fldDescription13.Text = strDesc;
			fldReference13.Text = strRef;
			fldDiscount13.Text = strDisc;
			fldAmt13.Text = strAmt;
			fldCredit13.Text = strCredit;
		}

		private void FillRow14(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant14.Text = strWarr;
			fldDescription14.Text = strDesc;
			fldReference14.Text = strRef;
			fldDiscount14.Text = strDisc;
			fldAmt14.Text = strAmt;
			fldCredit14.Text = strCredit;
		}

		private void FillRow15(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant15.Text = strWarr;
			fldDescription15.Text = strDesc;
			fldReference15.Text = strRef;
			fldDiscount15.Text = strDisc;
			fldAmt15.Text = strAmt;
			fldCredit15.Text = strCredit;
		}

		private void FillRow16(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant16.Text = strWarr;
			fldDescription16.Text = strDesc;
			fldReference16.Text = strRef;
			fldDiscount16.Text = strDisc;
			fldAmt16.Text = strAmt;
			fldCredit16.Text = strCredit;
		}

		private void FillRow17(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant17.Text = strWarr;
			fldDescription17.Text = strDesc;
			fldReference17.Text = strRef;
			fldDiscount17.Text = strDisc;
			fldAmt17.Text = strAmt;
			fldCredit17.Text = strCredit;
		}

		private void FillRow18(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant18.Text = strWarr;
			fldDescription18.Text = strDesc;
			fldReference18.Text = strRef;
			fldDiscount18.Text = strDisc;
			fldAmt18.Text = strAmt;
			fldCredit18.Text = strCredit;
		}

		private void FillRow19(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant19.Text = strWarr;
			fldDescription19.Text = strDesc;
			fldReference19.Text = strRef;
			fldDiscount19.Text = strDisc;
			fldAmt19.Text = strAmt;
			fldCredit19.Text = strCredit;
		}

		private void FillRow20(ref string strWarr, ref string strDesc, ref string strRef, ref string strCredit, ref string strDisc, ref string strAmt)
		{
			fldWarrant20.Text = strWarr;
			fldDescription20.Text = strDesc;
			fldReference20.Text = strRef;
			fldDiscount20.Text = strDisc;
			fldAmt20.Text = strAmt;
			fldCredit20.Text = strCredit;
		}

		
	}
}
