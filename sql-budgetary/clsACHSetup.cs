﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class clsACHSetup
	{
		//=========================================================
		private string strImmediateDestinationName = string.Empty;
		private string strImmediateDestinationRT = string.Empty;
		private string strImmediateOriginName = string.Empty;
		private string strImmediateOriginODFI = string.Empty;
		private string strImmediateOriginRT = string.Empty;
		private string strEmployerName = string.Empty;
		private string strEmployerRT = string.Empty;
		private string strEmployerID = string.Empty;
		private string strEmployerAccount = string.Empty;
		private int intEmployerAccountType;

		public string ImmediateDestinationName
		{
			set
			{
				strImmediateDestinationName = value;
			}
			get
			{
				string ImmediateDestinationName = "";
				ImmediateDestinationName = strImmediateDestinationName;
				return ImmediateDestinationName;
			}
		}

		public string ImmediateDestinationRT
		{
			set
			{
				strImmediateDestinationRT = value;
			}
			get
			{
				string ImmediateDestinationRT = "";
				ImmediateDestinationRT = strImmediateDestinationRT;
				return ImmediateDestinationRT;
			}
		}

		public string ImmediateOriginName
		{
			set
			{
				strImmediateOriginName = value;
			}
			get
			{
				string ImmediateOriginName = "";
				ImmediateOriginName = strImmediateOriginName;
				return ImmediateOriginName;
			}
		}

		public string ImmediateOriginODFI
		{
			set
			{
				strImmediateOriginODFI = value;
			}
			get
			{
				string ImmediateOriginODFI = "";
				ImmediateOriginODFI = strImmediateOriginODFI;
				return ImmediateOriginODFI;
			}
		}

		public string ImmediateOriginRT
		{
			set
			{
				strImmediateOriginRT = value;
			}
			get
			{
				string ImmediateOriginRT = "";
				ImmediateOriginRT = strImmediateOriginRT;
				return ImmediateOriginRT;
			}
		}

		public string EmployerName
		{
			set
			{
				strEmployerName = value;
			}
			get
			{
				string EmployerName = "";
				EmployerName = strEmployerName;
				return EmployerName;
			}
		}

		public string EmployerRT
		{
			set
			{
				strEmployerRT = value;
			}
			get
			{
				string EmployerRT = "";
				EmployerRT = strEmployerRT;
				return EmployerRT;
			}
		}

		public string EmployerID
		{
			set
			{
				strEmployerID = value;
			}
			get
			{
				string EmployerID = "";
				EmployerID = strEmployerID;
				return EmployerID;
			}
		}

		public string EmployerAccount
		{
			set
			{
				strEmployerAccount = value;
			}
			get
			{
				string EmployerAccount = "";
				EmployerAccount = strEmployerAccount;
				return EmployerAccount;
			}
		}

		public short EmployerAccountType
		{
			set
			{
				intEmployerAccountType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EmployerAccountType = 0;
				EmployerAccountType = FCConvert.ToInt16(intEmployerAccountType);
				return EmployerAccountType;
			}
		}
	}
}
