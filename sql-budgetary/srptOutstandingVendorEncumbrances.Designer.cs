﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptOutstandingVendorEncumbrances.
	/// </summary>
	partial class srptOutstandingVendorEncumbrances
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptOutstandingVendorEncumbrances));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRCB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCheck = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWarrant = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDebits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDebits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCredits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDebitsStar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCreditsStar = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitsStar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditsStar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPeriod,
				this.fldJournal,
				this.fldDate,
				this.fldAccount,
				this.fldDescription,
				this.fldRCB,
				this.fldType,
				this.fldCheck,
				this.fldAmount,
				this.fldWarrant
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Field13,
				this.Line1,
				this.Field26,
				this.lblAccount,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field33,
				this.lblAmount,
				this.lblPeriod,
				this.Field35
			});
			this.GroupHeader1.Height = 0.5208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.fldTotal,
				this.lblDebits,
				this.fldDebits,
				this.lblCredits,
				this.fldCredits,
				this.Line3,
				this.Field34,
				this.fldDebitsStar,
				this.fldCreditsStar
			});
			this.GroupFooter1.Height = 0.65625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label22
			// 
			this.Label22.Height = 0.21875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 11pt; font-weight: bold; text-align: center";
			this.Label22.Text = "Outstanding Encumbrances";
			this.Label22.Top = 0F;
			this.Label22.Width = 7.53125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0.8125F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field13.Tag = " ";
			this.Field13.Text = "Jrnl";
			this.Field13.Top = 0.3125F;
			this.Field13.Width = 0.34375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5F;
			this.Line1.Width = 7.5625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5625F;
			this.Line1.Y1 = 0.5F;
			this.Line1.Y2 = 0.5F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 1.1875F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field26.Tag = " ";
			this.Field26.Text = "Date";
			this.Field26.Top = 0.3125F;
			this.Field26.Width = 0.59375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.Left = 1.84375F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Tag = " ";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.3125F;
			this.lblAccount.Width = 1.84375F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 3.71875F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Description";
			this.Field29.Top = 0.3125F;
			this.Field29.Width = 1.25F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 5.34375F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field30.Tag = " ";
			this.Field30.Text = "Type";
			this.Field30.Top = 0.3125F;
			this.Field30.Width = 0.34375F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 5F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field31.Tag = " ";
			this.Field31.Text = "RCB";
			this.Field31.Top = 0.3125F;
			this.Field31.Width = 0.3125F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 5.71875F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field33.Tag = " ";
			this.Field33.Text = "Check";
			this.Field33.Top = 0.3125F;
			this.Field33.Width = 0.71875F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.Left = 6.46875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblAmount.Tag = " ";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.3125F;
			this.lblAmount.Width = 1.03125F;
			// 
			// lblPeriod
			// 
			this.lblPeriod.Height = 0.1875F;
			this.lblPeriod.Left = 0.125F;
			this.lblPeriod.Name = "lblPeriod";
			this.lblPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblPeriod.Tag = " ";
			this.lblPeriod.Text = "Per";
			this.lblPeriod.Top = 0.3125F;
			this.lblPeriod.Width = 0.25F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 0.40625F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field35.Tag = " ";
			this.Field35.Text = "Wrnt";
			this.Field35.Top = 0.3125F;
			this.Field35.Width = 0.34375F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.Height = 0.1875F;
			this.fldPeriod.Left = 0.125F;
			this.fldPeriod.MultiLine = false;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldPeriod.Text = "Field36";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldJournal
			// 
			this.fldJournal.Height = 0.1875F;
			this.fldJournal.Left = 0.8125F;
			this.fldJournal.MultiLine = false;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldJournal.Text = "Field36";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.34375F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 1.1875F;
			this.fldDate.MultiLine = false;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldDate.Text = "Field36";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.59375F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 1.84375F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldAccount.Text = "Field36";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1.84375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 3.71875F;
			this.fldDescription.MultiLine = false;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap; ddo-char-set: 1";
			this.fldDescription.Text = "Field36";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.25F;
			// 
			// fldRCB
			// 
			this.fldRCB.Height = 0.1875F;
			this.fldRCB.Left = 5F;
			this.fldRCB.MultiLine = false;
			this.fldRCB.Name = "fldRCB";
			this.fldRCB.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldRCB.Text = "Field36";
			this.fldRCB.Top = 0F;
			this.fldRCB.Width = 0.3125F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 5.34375F;
			this.fldType.MultiLine = false;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldType.Text = "Field36";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldCheck
			// 
			this.fldCheck.Height = 0.1875F;
			this.fldCheck.Left = 5.71875F;
			this.fldCheck.MultiLine = false;
			this.fldCheck.Name = "fldCheck";
			this.fldCheck.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldCheck.Text = "Field36";
			this.fldCheck.Top = 0F;
			this.fldCheck.Width = 0.71875F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 6.46875F;
			this.fldAmount.MultiLine = false;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; white-space: nowrap; dd" + "o-char-set: 1";
			this.fldAmount.Text = "Field36";
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.03125F;
			// 
			// fldWarrant
			// 
			this.fldWarrant.Height = 0.1875F;
			this.fldWarrant.Left = 0.4375F;
			this.fldWarrant.MultiLine = false;
			this.fldWarrant.Name = "fldWarrant";
			this.fldWarrant.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; white-space: nowrap; d" + "do-char-set: 1";
			this.fldWarrant.Text = "Field36";
			this.fldWarrant.Top = 0F;
			this.fldWarrant.Width = 0.34375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.78125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.Label8.Text = "Enc Total";
			this.Label8.Top = 0F;
			this.Label8.Width = 0.6875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.46875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldTotal.Text = "Field37";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.03125F;
			// 
			// lblDebits
			// 
			this.lblDebits.Height = 0.1875F;
			this.lblDebits.HyperLink = null;
			this.lblDebits.Left = 5.78125F;
			this.lblDebits.Name = "lblDebits";
			this.lblDebits.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.lblDebits.Text = "Debits";
			this.lblDebits.Top = 0.28125F;
			this.lblDebits.Width = 0.6875F;
			// 
			// fldDebits
			// 
			this.fldDebits.CanShrink = true;
			this.fldDebits.Height = 0.1875F;
			this.fldDebits.Left = 6.46875F;
			this.fldDebits.Name = "fldDebits";
			this.fldDebits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDebits.Text = "Field37";
			this.fldDebits.Top = 0.28125F;
			this.fldDebits.Width = 1.03125F;
			// 
			// lblCredits
			// 
			this.lblCredits.Height = 0.1875F;
			this.lblCredits.HyperLink = null;
			this.lblCredits.Left = 5.78125F;
			this.lblCredits.Name = "lblCredits";
			this.lblCredits.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.lblCredits.Text = "Credits";
			this.lblCredits.Top = 0.46875F;
			this.lblCredits.Width = 0.6875F;
			// 
			// fldCredits
			// 
			this.fldCredits.CanShrink = true;
			this.fldCredits.Height = 0.1875F;
			this.fldCredits.Left = 6.46875F;
			this.fldCredits.Name = "fldCredits";
			this.fldCredits.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCredits.Text = "Field37";
			this.fldCredits.Top = 0.46875F;
			this.fldCredits.Width = 1.03125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.9375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 1.5625F;
			this.Line3.X1 = 5.9375F;
			this.Line3.X2 = 7.5F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 7.5F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.Field34.Text = "*";
			this.Field34.Top = 0F;
			this.Field34.Width = 0.125F;
			// 
			// fldDebitsStar
			// 
			this.fldDebitsStar.Height = 0.1875F;
			this.fldDebitsStar.Left = 7.5F;
			this.fldDebitsStar.Name = "fldDebitsStar";
			this.fldDebitsStar.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldDebitsStar.Text = "*";
			this.fldDebitsStar.Top = 0.28125F;
			this.fldDebitsStar.Width = 0.125F;
			// 
			// fldCreditsStar
			// 
			this.fldCreditsStar.Height = 0.1875F;
			this.fldCreditsStar.Left = 7.5F;
			this.fldCreditsStar.Name = "fldCreditsStar";
			this.fldCreditsStar.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldCreditsStar.Text = "*";
			this.fldCreditsStar.Top = 0.46875F;
			this.fldCreditsStar.Width = 0.125F;
			// 
			// srptOutstandingVendorEncumbrances
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.65625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRCB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWarrant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDebitsStar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCreditsStar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRCB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCheck;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWarrant;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDebits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitsStar;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditsStar;
	}
}
