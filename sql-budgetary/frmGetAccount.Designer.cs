﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmGetAccount.
	/// </summary>
	partial class frmGetAccount : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCFrame Frame3;
		public FCGrid vs1;
		public fecherFoundation.FCButton cmdGet;
		public fecherFoundation.FCButton cmdReturn;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblLastAccount;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAccount));
			this.cmbSearchType = new fecherFoundation.FCComboBox();
			this.lblSearchType = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.vs1 = new fecherFoundation.FCGrid();
			this.cmdGet = new fecherFoundation.FCButton();
			this.cmdReturn = new fecherFoundation.FCButton();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdSearch = new fecherFoundation.FCButton();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblLastAccount = new fecherFoundation.FCLabel();
			this.cmdGetAccountNumber = new fecherFoundation.FCButton();
			this.groupBox1 = new Wisej.Web.GroupBox();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 577);
			this.BottomPanel.Size = new System.Drawing.Size(866, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.groupBox1);
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Size = new System.Drawing.Size(866, 517);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.Add(this.cmdSearch);
			this.TopPanel.Size = new System.Drawing.Size(866, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(233, 30);
			this.HeaderText.Text = "Get Vendor Account";
			// 
			// cmbSearchType
			// 
			this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Contact",
            "Tax ID #"});
			this.cmbSearchType.Location = new System.Drawing.Point(182, 30);
			this.cmbSearchType.Name = "cmbSearchType";
			this.cmbSearchType.Size = new System.Drawing.Size(245, 40);
			this.cmbSearchType.TabIndex = 1;
			this.cmbSearchType.SelectedIndexChanged += new System.EventHandler(this.optSearchType_MouseDown);
			// 
			// lblSearchType
			// 
			this.lblSearchType.Location = new System.Drawing.Point(20, 44);
			this.lblSearchType.Name = "lblSearchType";
			this.lblSearchType.Size = new System.Drawing.Size(70, 16);
			this.lblSearchType.Text = "SEARCH BY";
			// 
			// Frame3
			// 
			this.Frame3.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Frame3.AppearanceKey = "groupBoxNoBorders";
			this.Frame3.Controls.Add(this.vs1);
			this.Frame3.Controls.Add(this.cmdGet);
			this.Frame3.Controls.Add(this.cmdReturn);
			this.Frame3.Controls.Add(this.Label4);
			this.Frame3.Controls.Add(this.Label3);
			this.Frame3.Location = new System.Drawing.Point(30, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(815, 476);
			this.Frame3.TabIndex = 1;
			this.Frame3.Text = "Multiple Records";
			this.Frame3.Visible = false;
			// 
			// vs1
			// 
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vs1.Cols = 5;
			this.vs1.FixedCols = 0;
			this.vs1.Location = new System.Drawing.Point(20, 30);
			this.vs1.Name = "vs1";
			this.vs1.RowHeadersVisible = false;
			this.vs1.Rows = 50;
			this.vs1.Size = new System.Drawing.Size(786, 346);
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
			this.vs1.DoubleClick += new System.EventHandler(this.vs1_DblClick);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEvent);
			// 
			// cmdGet
			// 
			this.cmdGet.AppearanceKey = "actionButton";
			this.cmdGet.Location = new System.Drawing.Point(242, 432);
			this.cmdGet.Name = "cmdGet";
			this.cmdGet.Size = new System.Drawing.Size(155, 40);
			this.cmdGet.TabIndex = 4;
			this.cmdGet.Text = "Retrieve Record";
			this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
			// 
			// cmdReturn
			// 
			this.cmdReturn.AppearanceKey = "actionButton";
			this.cmdReturn.Location = new System.Drawing.Point(20, 432);
			this.cmdReturn.Name = "cmdReturn";
			this.cmdReturn.Size = new System.Drawing.Size(213, 40);
			this.cmdReturn.TabIndex = 3;
			this.cmdReturn.Text = "Return to Search Screen";
			this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(56, 400);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(105, 16);
			this.Label4.TabIndex = 2;
			this.Label4.Text = "DELETED VENDOR";
			// 
			// Label3
			// 
			this.Label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.Label3.BorderStyle = 1;
			this.Label3.Location = new System.Drawing.Point(20, 396);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(16, 16);
			this.Label3.TabIndex = 1;
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(402, 66);
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(82, 40);
			this.txtGetAccountNumber.TabIndex = 3;
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.cmbSearchType);
			this.Frame2.Controls.Add(this.lblSearchType);
			this.Frame2.Controls.Add(this.txtSearch);
			this.Frame2.Controls.Add(this.lblSearchInfo);
			this.Frame2.Location = new System.Drawing.Point(30, 126);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(454, 159);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "Search";
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(182, 90);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(245, 40);
			this.txtSearch.TabIndex = 3;
			this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.Location = new System.Drawing.Point(20, 104);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(143, 16);
			this.lblSearchInfo.TabIndex = 2;
			this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(650, 28);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(92, 24);
			this.cmdClear.TabIndex = 4;
			this.cmdClear.Text = "Clear Search";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// cmdSearch
			// 
			this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSearch.ImageSource = "button-search";
			this.cmdSearch.Location = new System.Drawing.Point(746, 28);
			this.cmdSearch.Name = "cmdSearch";
			this.cmdSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdSearch.TabIndex = 5;
			this.cmdSearch.Text = "Search";
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 80);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(340, 16);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "ENTER THE VENDOR NUMBER OR 0 TO ADD A NEW VENDOR";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 30);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(147, 16);
			this.Label2.Text = "LAST VENDOR ACCESSED";
			// 
			// lblLastAccount
			// 
			this.lblLastAccount.Location = new System.Drawing.Point(212, 30);
			this.lblLastAccount.Name = "lblLastAccount";
			this.lblLastAccount.Size = new System.Drawing.Size(70, 16);
			this.lblLastAccount.TabIndex = 1;
			// 
			// cmdGetAccountNumber
			// 
			this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
			this.cmdGetAccountNumber.Location = new System.Drawing.Point(30, 304);
			this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
			this.cmdGetAccountNumber.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdGetAccountNumber.Size = new System.Drawing.Size(95, 48);
			this.cmdGetAccountNumber.Text = "Process";
			this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.AppearanceKey = "groupBoxNoBorders";
			this.groupBox1.Controls.Add(this.cmdGetAccountNumber);
			this.groupBox1.Controls.Add(this.Label2);
			this.groupBox1.Controls.Add(this.lblLastAccount);
			this.groupBox1.Controls.Add(this.txtGetAccountNumber);
			this.groupBox1.Controls.Add(this.Label1);
			this.groupBox1.Controls.Add(this.Frame2);
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(503, 386);
			this.groupBox1.TabIndex = 0;
			// 
			// frmGetAccount
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(866, 685);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGetAccount";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Get Vendor Account";
			this.Load += new System.EventHandler(this.frmGetAccount_Load);
			this.Activated += new System.EventHandler(this.frmGetAccount_Activated);
			this.Resize += new System.EventHandler(this.frmGetAccount_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetAccount_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetAccount_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdGetAccountNumber;
		private GroupBox groupBox1;
	}
}
