﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for srptExpRevSummaryDivLevel.
	/// </summary>
	public partial class srptExpRevSummaryDivLevel : FCSectionReport
	{
		public static srptExpRevSummaryDivLevel InstancePtr
		{
			get
			{
				return (srptExpRevSummaryDivLevel)Sys.GetInstance(typeof(srptExpRevSummaryDivLevel));
			}
		}

		protected srptExpRevSummaryDivLevel _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsYTDActivity.Dispose();
				rsActivityDetail.Dispose();
				rsDivisionInfo.Dispose();
				rsRevYTDActivity.Dispose();
				rsRevBudgetInfo.Dispose();
				rsRevActivityDetail.Dispose();
				rsExpBudgetInfo.Dispose();
				rsCurrentActivity.Dispose();
				rsRevCurrentActivity.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptExpRevSummaryDivLevel	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strLowDetail = "";
		string strDateRange = "";
		// reporting range A - All, M - Month Range, S - Single Month
		int intLowDate;
		int intHighDate;
		bool blnIncEnc;
		// should encumbrance money be calculated into YTD amounts
		bool blnIncPending;
		// should pending activity be included in YTD amounts
		clsDRWrapper rsCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in expenses
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in expenses
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in expenses
		clsDRWrapper rsRevCurrentActivity = new clsDRWrapper();
		// recordset holds money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		// recordset holds monthly money activity within date rnage for selected departments in revenues
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		// recordset holds YTD money activity selected departments in revenues
		clsDRWrapper rsExpBudgetInfo = new clsDRWrapper();
		clsDRWrapper rsRevBudgetInfo = new clsDRWrapper();
		string strPeriodCheck = "";
		// AND or OR depending on if intLowDate > intHighDate
		string strExpOrRev = "";
		// R or E telling program whether we are reporting on Revenues or Expenses at the moment
		int intCurrentMonth;
		bool blnFirstRecord;
		// used in fetchdata to let us know not to move forward in the recordset on the first pass
		clsDRWrapper rsDivisionInfo = new clsDRWrapper();
		// holds division info for departments we are reporting on
		string CurrentDepartment = "";
		// current department we are reporting on
		string CurrentDivision = "";
		// current division we are reporting on
		public srptExpRevSummaryDivLevel()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = rsDivisionInfo.EndOfFile();
			}
			else
			{
				rsDivisionInfo.MoveNext();
				eArgs.EOF = rsDivisionInfo.EndOfFile();
			}
			// Unload srptExpRevSummaryExpLevel
			// Unload srptExpRevSummaryRevLevel
			if (!eArgs.EOF)
			{
				CurrentDivision = FCConvert.ToString(rsDivisionInfo.Get_Fields_String("Division"));
				this.Fields["Binder"].Value = rsDivisionInfo.Get_Fields_String("Division");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strLowDetail != "Di")
			{
				if (strExpOrRev == "E")
                {
                    var subReport = new srptExpRevSummaryExpLevel();
					subReport.Init(strLowDetail, strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, CurrentDivision, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
					subLowerLevelReport.Report = subReport;
				}
				else
				{
                    var subReport = new srptExpRevSummaryRevLevel();
                    subReport.Init(strDateRange, blnIncEnc, blnIncPending, CurrentDepartment, CurrentDivision, FCConvert.ToInt16(intLowDate), FCConvert.ToInt16(intHighDate));
					subLowerLevelReport.Report = subReport;
				}
			}
			else
			{
				subLowerLevelReport = null;
			}
		}

		private void RetrieveInfo()
		{
            var HighDate = intHighDate;
			var LowDate = modBudgetaryMaster.Statics.FirstMonth;
			var strPeriodCheckHolder = strPeriodCheck;
			strPeriodCheck = LowDate > HighDate ? "OR" : "AND";
			rsYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division");
			rsRevYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE (Period = 0 OR (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")) GROUP BY Department, Division");
			rsExpBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM ExpenseReportInfo GROUP BY Department, Division");
			rsRevBudgetInfo.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal FROM RevenueReportInfo GROUP BY Department, Division");
			strPeriodCheck = strPeriodCheckHolder;
			rsCurrentActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division");
			rsActivityDetail.OpenRecordset("SELECT Period, Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Period");
			rsRevCurrentActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division");
			rsRevActivityDetail.OpenRecordset("SELECT Period, Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo WHERE Period >= " + FCConvert.ToString(intLowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(intHighDate) + " GROUP BY Department, Division, Period");
		}

		private decimal GetCurrentDebits()
		{
			decimal GetCurrentDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetCurrentDebits = 0;
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
								GetCurrentDebits = rsRevCurrentActivity.Get_Fields("PostedDebitsTotal");
							}
							else
							{
								GetCurrentDebits = 0;
							}
						}
					}
					else
					{
						GetCurrentDebits = 0;
					}
				}
			}
			else
			{
				GetCurrentDebits = 0;
			}
			return GetCurrentDebits;
		}

		private decimal GetCurrentCredits()
		{
			decimal GetCurrentCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetCurrentCredits = 0;
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
								GetCurrentCredits = rsRevCurrentActivity.Get_Fields("PostedCreditsTotal");
							}
							else
							{
								GetCurrentCredits = 0;
							}
						}
					}
					else
					{
						GetCurrentCredits = 0;
					}
				}
			}
			else
			{
				GetCurrentCredits = 0;
			}
			return GetCurrentCredits;
		}

		private decimal GetCurrentNet()
		{
			decimal GetCurrentNet = 0;
			if (strExpOrRev == "E")
			{
				GetCurrentNet = GetCurrentDebits() + GetCurrentCredits();
			}
			else
			{
				GetCurrentNet = (GetCurrentDebits() + GetCurrentCredits()) * -1;
			}
			return GetCurrentNet;
		}

		private decimal GetYTDDebit()
		{
			decimal GetYTDDebit = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
						GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields("PostedDebitsTotal");
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
				}
				else
				{
					GetYTDDebit = 0;
				}
			}
			if (blnIncEnc)
			{
				GetYTDDebit += GetEncumbrance();
			}
			if (blnIncPending)
			{
				GetYTDDebit += GetPendingDebits();
			}
			return GetYTDDebit;
		}

		private decimal GetYTDCredit()
		{
			decimal GetYTDCredit = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
						GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields("PostedCreditsTotal");
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
				}
				else
				{
					GetYTDCredit = 0;
				}
			}
			if (blnIncPending)
			{
				GetYTDCredit += GetPendingCredits();
			}
			return GetYTDCredit;
		}

		private decimal GetYTDNet()
		{
			decimal GetYTDNet = 0;
			if (strExpOrRev == "E")
			{
				GetYTDNet = GetYTDDebit() + GetYTDCredit();
			}
			else
			{
				GetYTDNet = (GetYTDDebit() + GetYTDCredit()) * -1;
			}
			return GetYTDNet;
		}

		private decimal GetEncumbrance()
		{
			decimal GetEncumbrance = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
						GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
							GetEncumbrance = rsRevYTDActivity.Get_Fields("EncumbActivityTotal");
						}
						else
						{
							GetEncumbrance = 0;
						}
					}
				}
				else
				{
					GetEncumbrance = 0;
				}
			}
			return GetEncumbrance;
		}

		private decimal GetPending()
		{
			decimal GetPending = 0;
			if (strExpOrRev == "E")
			{
				GetPending = GetPendingDebits() + GetPendingCredits();
			}
			else
			{
				GetPending = (GetPendingDebits() + GetPendingCredits()) * -1;
			}
			return GetPending;
		}

		private decimal GetBalance()
		{
			decimal GetBalance = 0;
			if (strExpOrRev == "E")
			{
				GetBalance = GetNetBudget() - (GetYTDDebit() + GetYTDCredit());
			}
			else
			{
				GetBalance = GetNetBudget() - ((GetYTDDebit() + GetYTDCredit()) * -1);
			}
			return GetBalance;
		}

		private decimal GetSpent()
		{
			decimal GetSpent = 0;
			int temp;
			decimal temp2;
			decimal temp3;
			temp2 = GetBalance();
			temp3 = GetNetBudget();
			if (temp3 == 0)
			{
				GetSpent = 0;
				return GetSpent;
			}
			if (temp2 > temp3)
			{
				GetSpent = (temp3 - temp2) / temp3;
				return GetSpent;
			}
			GetSpent = (temp3 - temp2) / temp3;
			return GetSpent;
		}

		private decimal GetPendingCredits()
		{
			decimal GetPendingCredits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
						GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							GetPendingCredits = 0;
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
							GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
						}
						else
						{
							if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingCreditsTotal] not found!! (maybe it is an alias?)
								GetPendingCredits = rsRevCurrentActivity.Get_Fields("PendingCreditsTotal") * -1;
							}
							else
							{
								GetPendingCredits = 0;
							}
						}
					}
					else
					{
						GetPendingCredits = 0;
					}
				}
			}
			else
			{
				GetPendingCredits = 0;
			}
			return GetPendingCredits;
		}

		private decimal GetPendingDebits()
		{
			decimal GetPendingDebits = 0;
			if (rsCurrentActivity.EndOfFile() != true && rsCurrentActivity.BeginningOfFile() != true)
			{
				if (strExpOrRev == "E")
				{
					if (FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
						GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
					}
					else
					{
						if (rsCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							GetPendingDebits = 0;
						}
					}
				}
				else
				{
					if (rsRevCurrentActivity.EndOfFile() != true && rsRevCurrentActivity.BeginningOfFile() != true)
					{
						if (FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevCurrentActivity.Get_Fields_String("Division")) == CurrentDivision)
						{
							// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
							GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
						}
						else
						{
							if (rsRevCurrentActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
							{
								// TODO Get_Fields: Field [PendingDebitsTotal] not found!! (maybe it is an alias?)
								GetPendingDebits = rsRevCurrentActivity.Get_Fields("PendingDebitsTotal");
							}
							else
							{
								GetPendingDebits = 0;
							}
						}
					}
					else
					{
						GetPendingDebits = 0;
					}
				}
			}
			else
			{
				GetPendingDebits = 0;
			}
			return GetPendingDebits;
		}

		private decimal GetOriginalBudget()
		{
			decimal GetOriginalBudget = 0;
			if (strExpOrRev == "E")
			{
				if (rsExpBudgetInfo.EndOfFile() != true && rsExpBudgetInfo.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsExpBudgetInfo.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsExpBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsExpBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else
				{
					GetOriginalBudget = 0;
				}
			}
			else
			{
				if (rsRevBudgetInfo.EndOfFile() != true && rsRevBudgetInfo.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevBudgetInfo.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
						GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
					}
					else
					{
						if (rsRevBudgetInfo.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
							GetOriginalBudget = rsRevBudgetInfo.Get_Fields("OriginalBudgetTotal");
						}
						else
						{
							GetOriginalBudget = 0;
						}
					}
				}
				else
				{
					GetOriginalBudget = 0;
				}
			}
			return GetOriginalBudget;
		}

		private decimal GetBudgetAdjustments()
		{
			decimal GetBudgetAdjustments = 0;
			if (strExpOrRev == "E")
			{
				if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
					}
					else
					{
						if (rsYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsYTDActivity.Get_Fields("BudgetAdjustmentsTotal") * -1;
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
			}
			else
			{
				if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
				{
					if (FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Department")) == CurrentDepartment && FCConvert.ToString(rsRevYTDActivity.Get_Fields_String("Division")) == CurrentDivision)
					{
						// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
						GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
					}
					else
					{
						if (rsRevYTDActivity.FindFirstRecord2("Department, Division", CurrentDepartment + "," + CurrentDivision, ","))
						{
							// TODO Get_Fields: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
							GetBudgetAdjustments = rsRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal");
						}
						else
						{
							GetBudgetAdjustments = 0;
						}
					}
				}
				else
				{
					GetBudgetAdjustments = 0;
				}
			}
			return GetBudgetAdjustments;
		}

		private decimal GetNetBudget()
		{
			decimal GetNetBudget = 0;
			GetNetBudget = GetOriginalBudget() + GetBudgetAdjustments();
			return GetNetBudget;
		}

		private string MonthCalc(int X)
		{
			string MonthCalc = "";
			switch (X)
			{
				case 1:
					{
						MonthCalc = "January";
						break;
					}
				case 2:
					{
						MonthCalc = "February";
						break;
					}
				case 3:
					{
						MonthCalc = "March";
						break;
					}
				case 4:
					{
						MonthCalc = "April";
						break;
					}
				case 5:
					{
						MonthCalc = "May";
						break;
					}
				case 6:
					{
						MonthCalc = "June";
						break;
					}
				case 7:
					{
						MonthCalc = "July";
						break;
					}
				case 8:
					{
						MonthCalc = "August";
						break;
					}
				case 9:
					{
						MonthCalc = "September";
						break;
					}
				case 10:
					{
						MonthCalc = "October";
						break;
					}
				case 11:
					{
						MonthCalc = "November";
						break;
					}
				case 12:
					{
						MonthCalc = "December";
						break;
					}
			}
			//end switch
			return MonthCalc;
		}
		// vbPorter upgrade warning: intLowMonth As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intHighMonth As short	OnWriteFCConvert.ToInt32(
		public void Init(string strLowLevel, string strEOrR, string strMonthRange, bool blnIncludeEncumbrances, bool blnIncludePendingInformation, string strDept, short intLowMonth = 0, short intHighMonth = 0)
		{
			// initialize all variables for this report
			CurrentDepartment = strDept;
			strDateRange = strMonthRange;
			strExpOrRev = strEOrR;
			strLowDetail = strLowLevel;
			blnIncEnc = blnIncludeEncumbrances;
			blnIncPending = blnIncludePendingInformation;
			if (strDateRange == "A")
			{
				intLowDate = modBudgetaryMaster.Statics.FirstMonth;
				if (modBudgetaryMaster.Statics.FirstMonth == 1)
				{
					intHighDate = 12;
				}
				else
				{
					intHighDate = modBudgetaryMaster.Statics.FirstMonth - 1;
				}
			}
			else if (strDateRange == "M")
			{
				intLowDate = intLowMonth;
				intHighDate = intHighMonth;
			}
			else
			{
				intLowDate = intLowMonth;
				intHighDate = intLowMonth;
			}
			if (intLowDate > intHighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			// put information into recordsets
			RetrieveInfo();
			// check to see if there are any departments with activity to report on
			if (strExpOrRev == "E")
			{
				rsDivisionInfo.OpenRecordset("SELECT DISTINCT Division FROM ExpenseReportInfo WHERE Department = '" + CurrentDepartment + "' Order by division");
			}
			else
			{
				rsDivisionInfo.OpenRecordset("SELECT DISTINCT Division FROM RevenueReportInfo WHERE Department = '" + CurrentDepartment + "' order by division");
			}
			if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
				return;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            using (clsDRWrapper rsDivisionTitle = new clsDRWrapper())
            {
                // get division title
                rsDivisionTitle.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + CurrentDepartment +
                                              "' AND Division = '" + CurrentDivision + "'");
                // show information for division
                if (rsDivisionTitle.EndOfFile() != true && rsDivisionTitle.BeginningOfFile() != true)
                {
                    fldAccount.Text = CurrentDivision + "  " + rsDivisionTitle.Get_Fields_String("ShortDescription");
                }
                else
                {
                    fldAccount.Text = CurrentDivision + "  UNKNOWN";
                }

                fldBudget.Text = Strings.Format(GetNetBudget(), "#,##0.00");
                fldCurrentMonth.Text = Strings.Format(GetCurrentNet(), "#,##0.00");
                fldYTD.Text = Strings.Format(GetYTDNet(), "#,##0.00");
                // If strExpOrRev = "E" Then
                fldBalance.Text = Strings.Format(GetBalance(), "#,##0.00");
                // Else
                // fldBalance = format(GetBalance * -1, "#,##0.00")
                // End If
                fldSpent.Text = Strings.Format(GetSpent() * 100, "0.00");
                // update totals
                if (strExpOrRev == "E")
                {
                    rptExpRevSummary.InstancePtr.curExpBudget += FCConvert.ToDecimal(fldBudget.Text);
                    rptExpRevSummary.InstancePtr.curExpCurrentNet += FCConvert.ToDecimal(fldCurrentMonth.Text);
                    rptExpRevSummary.InstancePtr.curExpYTDNet += FCConvert.ToDecimal(fldYTD.Text);
                    rptExpRevSummary.InstancePtr.curExpBalance += FCConvert.ToDecimal(fldBalance.Text);
                }
                else
                {
                    rptExpRevSummary.InstancePtr.curRevBudget += FCConvert.ToDecimal(fldBudget.Text);
                    rptExpRevSummary.InstancePtr.curRevCurrentNet += FCConvert.ToDecimal(fldCurrentMonth.Text);
                    rptExpRevSummary.InstancePtr.curRevYTDNet += FCConvert.ToDecimal(fldYTD.Text);
                    // Dave 4/9/03
                    rptExpRevSummary.InstancePtr.curRevBalance += FCConvert.ToDecimal(fldBalance.Text);
                }
            }
        }

		

		private void srptExpRevSummaryDivLevel_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
