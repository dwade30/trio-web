﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cVendorOption
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private int lngVendorNumber;
		private string strVendorName = string.Empty;
		private string strDescription = string.Empty;
		private double dblAmount;

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public string VendorName
		{
			set
			{
				strVendorName = value;
			}
			get
			{
				string VendorName = "";
				VendorName = strVendorName;
				return VendorName;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
