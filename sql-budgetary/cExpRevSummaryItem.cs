﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cExpRevSummaryItem
	{
		//=========================================================
		private string strAccount = string.Empty;
		private string strAccountType = string.Empty;
		private string strFund = string.Empty;
		private string strDepartment = string.Empty;
		private string strDivision = string.Empty;
		private string strRevenue = string.Empty;
		private string strExpense = string.Empty;
		private string strExpenseObject = string.Empty;
		private double dblBudget;
		private double dblCurrentMonth;
		private double dblYTD;
		private double dblBalance;
		private double dblPercent;
		private string strDescription = string.Empty;

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string AccountType
		{
			set
			{
				strAccountType = value;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string Division
		{
			set
			{
				strDivision = value;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}

		public string Revenue
		{
			set
			{
				strRevenue = value;
			}
			get
			{
				string Revenue = "";
				Revenue = strRevenue;
				return Revenue;
			}
		}

		public string Expense
		{
			set
			{
				strExpense = value;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string ExpenseObject
		{
			set
			{
				strExpenseObject = value;
			}
			get
			{
				string ExpenseObject = "";
				ExpenseObject = strExpenseObject;
				return ExpenseObject;
			}
		}

		public double Budget
		{
			set
			{
				dblBudget = value;
			}
			get
			{
				double Budget = 0;
				Budget = dblBudget;
				return Budget;
			}
		}

		public double CurrentMonth
		{
			set
			{
				dblCurrentMonth = value;
			}
			get
			{
				double CurrentMonth = 0;
				CurrentMonth = dblCurrentMonth;
				return CurrentMonth;
			}
		}

		public double YTD
		{
			set
			{
				dblYTD = value;
			}
			get
			{
				double YTD = 0;
				YTD = dblYTD;
				return YTD;
			}
		}

		public double Balance
		{
			set
			{
				dblBalance = value;
			}
			get
			{
				double Balance = 0;
				Balance = dblBalance;
				return Balance;
			}
		}

		public double PercentUsed
		{
			set
			{
				dblPercent = value;
			}
			get
			{
				double PercentUsed = 0;
				PercentUsed = dblPercent;
				return PercentUsed;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}
	}
}
