﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCustomization.
	/// </summary>
	partial class frmCustomization : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOption;
		public fecherFoundation.FCLabel lblOption;
		public fecherFoundation.FCFrame fraAPOptions;
		public fecherFoundation.FCComboBox cmbTrackLastCheckNumber;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtReturnAddress1;
		public fecherFoundation.FCTextBox txtReturnAddress2;
		public fecherFoundation.FCTextBox txtReturnAddress3;
		public fecherFoundation.FCTextBox txtReturnAddress4;
		public fecherFoundation.FCButton cmdChooseAccounts;
		public fecherFoundation.FCCheckBox chkAutoIncrementPO;
		public fecherFoundation.FCCheckBox chkEncumbranceByDepartment;
		public fecherFoundation.FCComboBox cboPreviewSchoolAccount;
		public fecherFoundation.FCComboBox cboBalanceValidation;
		public fecherFoundation.FCCheckBox chkAPReference;
		public fecherFoundation.FCCheckBox chkWarrantLandscape;
		public fecherFoundation.FCCheckBox chkManualWarrantNumbers;
		public fecherFoundation.FCCheckBox chkRecapOrderByAccount;
		public fecherFoundation.FCCheckBox chkWarrantSignature;
		public fecherFoundation.FCCheckBox chkWarrantDeptSummary;
		public fecherFoundation.FCCheckBox chkSignature;
		public fecherFoundation.FCComboBox cboCheckFormat;
		public fecherFoundation.FCComboBox cboAPSequence;
		public fecherFoundation.FCComboBox cboPreviewAccount;
		public fecherFoundation.FCTextBox txtWarrant;
		public fecherFoundation.FCFrame fraReference;
		public fecherFoundation.FCCheckBox chkSameVendor;
		public fecherFoundation.FCFrame fraAutoIncrementPO;
		public fecherFoundation.FCTextBox txtNextPO;
		public fecherFoundation.FCLabel lblNextPO;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel lblSchoolAccountOption;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel lblTownAccountOption;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraPosting;
		public fecherFoundation.FCCheckBox chkAutoPostARBills;
		public fecherFoundation.FCCheckBox chkAutoPostAP;
		public fecherFoundation.FCCheckBox chkAutoPostCD;
		public fecherFoundation.FCCheckBox chkAutoPostCR;
		public fecherFoundation.FCCheckBox chkAutoPostEN;
		public fecherFoundation.FCCheckBox chkAutoPostAPCorr;
		public fecherFoundation.FCCheckBox chkAutoPostCM;
		public fecherFoundation.FCCheckBox chkAutoPostGJ;
		public fecherFoundation.FCCheckBox chkAutoPostCreateOpeningAdjustments;
		public fecherFoundation.FCCheckBox chkAutoPostBudgetTrnsfer;
		public fecherFoundation.FCCheckBox chkAutoPostCRDailyAudit;
		public fecherFoundation.FCCheckBox chkAutoPostMVRapidRenewal;
		public fecherFoundation.FCCheckBox chkAutoPostReversingJournals;
		public fecherFoundation.FCCheckBox chkAutoPostCLLiens;
		public fecherFoundation.FCCheckBox chkAutoPostBLCommitments;
		public fecherFoundation.FCCheckBox chkAutoPostUTLiens;
		public fecherFoundation.FCCheckBox chkAutoPostBLSupplementals;
		public fecherFoundation.FCCheckBox chkAutoPostPYProcesses;
		public fecherFoundation.FCCheckBox chkAutoPostUTBills;
		public fecherFoundation.FCCheckBox chkAutoPostCLTaxAcquired;
		public fecherFoundation.FCCheckBox chkAutoPostFADepreciation;
		public fecherFoundation.FCLabel Label27;
		public fecherFoundation.FCFrame fraACHOptions;
		public fecherFoundation.FCButton cmdPrintACHPrenoteFile;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCTextBox txtACHFederalID;
		public fecherFoundation.FCTextBox txtACHTownName;
		public fecherFoundation.FCComboBox cboFederalIDPrefix;
		public fecherFoundation.FCCheckBox chkBalanceACHFile;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtImmediateOriginName;
		public fecherFoundation.FCTextBox txtImmediateOriginRoutingNumber;
		public fecherFoundation.FCTextBox txtImmediateOriginODFINumber;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtImmediateDestinationName;
		public fecherFoundation.FCTextBox txtImmediateDestinationRoutingNumber;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCLabel Label26;
		public fecherFoundation.FCFrame fraGeneralOptions;
		public fecherFoundation.FCComboBox cboEncCarryForward;
		public fecherFoundation.FCCheckBox chkAutomaticInterestEntries;
		public fecherFoundation.FCFrame fraBankOptions;
		public fecherFoundation.FCComboBox cboCRBank;
		public fecherFoundation.FCComboBox cboAPBank;
		public fecherFoundation.FCComboBox cboPayrollBank;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCCheckBox chkProjects;
		public fecherFoundation.FCCheckBox chkDue;
		public fecherFoundation.FCCheckBox chkBudgetAccounts;
		public fecherFoundation.FCButton cmdResetSave;
		public fecherFoundation.FCComboBox cboFirstMonth;
		public fecherFoundation.FCComboBox cboValidAccount;
		public fecherFoundation.FCTextBox txtJournal;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraDueToFrom;
		public FCGrid vsFunds;
		public fecherFoundation.FCFrame fraPrintingOptions;
		public fecherFoundation.FCFrame fraLaserAdjustmentOptions;
		public fecherFoundation.FCTextBox txt1099AdjustmentHorizontal;
		public fecherFoundation.FCTextBox txt1099Adjustment;
		public fecherFoundation.FCTextBox txtCheckAdjustment;
		public fecherFoundation.FCTextBox txtReportAdjustment;
		public fecherFoundation.FCTextBox txtLabelAdjustment;
		public fecherFoundation.FCButton cmdTestCheck;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblInstruct;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCCheckBox chkShade;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomization));
			this.cmbOption = new fecherFoundation.FCComboBox();
			this.lblOption = new fecherFoundation.FCLabel();
			this.fraAPOptions = new fecherFoundation.FCFrame();
			this.btnEditWarrantMessage = new fecherFoundation.FCButton();
			this.btnSelectTownLogo = new fecherFoundation.FCButton();
			this.cmbTrackLastCheckNumber = new fecherFoundation.FCComboBox();
			this.Frame6 = new fecherFoundation.FCFrame();
			this.txtReturnAddress1 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress2 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress3 = new fecherFoundation.FCTextBox();
			this.txtReturnAddress4 = new fecherFoundation.FCTextBox();
			this.cmdChooseAccounts = new fecherFoundation.FCButton();
			this.chkAutoIncrementPO = new fecherFoundation.FCCheckBox();
			this.chkEncumbranceByDepartment = new fecherFoundation.FCCheckBox();
			this.cboPreviewSchoolAccount = new fecherFoundation.FCComboBox();
			this.cboBalanceValidation = new fecherFoundation.FCComboBox();
			this.chkAPReference = new fecherFoundation.FCCheckBox();
			this.chkWarrantLandscape = new fecherFoundation.FCCheckBox();
			this.chkManualWarrantNumbers = new fecherFoundation.FCCheckBox();
			this.chkRecapOrderByAccount = new fecherFoundation.FCCheckBox();
			this.chkWarrantSignature = new fecherFoundation.FCCheckBox();
			this.chkWarrantDeptSummary = new fecherFoundation.FCCheckBox();
			this.chkSignature = new fecherFoundation.FCCheckBox();
			this.cboCheckFormat = new fecherFoundation.FCComboBox();
			this.cboAPSequence = new fecherFoundation.FCComboBox();
			this.cboPreviewAccount = new fecherFoundation.FCComboBox();
			this.txtWarrant = new fecherFoundation.FCTextBox();
			this.fraReference = new fecherFoundation.FCFrame();
			this.chkSameVendor = new fecherFoundation.FCCheckBox();
			this.fraAutoIncrementPO = new fecherFoundation.FCFrame();
			this.txtNextPO = new fecherFoundation.FCTextBox();
			this.lblNextPO = new fecherFoundation.FCLabel();
			this.Label28 = new fecherFoundation.FCLabel();
			this.lblSchoolAccountOption = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label17 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.lblTownAccountOption = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraPosting = new fecherFoundation.FCFrame();
			this.chkAutoPostARBills = new fecherFoundation.FCCheckBox();
			this.chkAutoPostAP = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCD = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCR = new fecherFoundation.FCCheckBox();
			this.chkAutoPostEN = new fecherFoundation.FCCheckBox();
			this.chkAutoPostAPCorr = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCM = new fecherFoundation.FCCheckBox();
			this.chkAutoPostGJ = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCreateOpeningAdjustments = new fecherFoundation.FCCheckBox();
			this.chkAutoPostBudgetTrnsfer = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCRDailyAudit = new fecherFoundation.FCCheckBox();
			this.chkAutoPostMVRapidRenewal = new fecherFoundation.FCCheckBox();
			this.chkAutoPostReversingJournals = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCLLiens = new fecherFoundation.FCCheckBox();
			this.chkAutoPostBLCommitments = new fecherFoundation.FCCheckBox();
			this.chkAutoPostUTLiens = new fecherFoundation.FCCheckBox();
			this.chkAutoPostBLSupplementals = new fecherFoundation.FCCheckBox();
			this.chkAutoPostPYProcesses = new fecherFoundation.FCCheckBox();
			this.chkAutoPostUTBills = new fecherFoundation.FCCheckBox();
			this.chkAutoPostCLTaxAcquired = new fecherFoundation.FCCheckBox();
			this.chkAutoPostFADepreciation = new fecherFoundation.FCCheckBox();
			this.Label27 = new fecherFoundation.FCLabel();
			this.fraACHOptions = new fecherFoundation.FCFrame();
			this.cmdPrintACHPrenoteFile = new fecherFoundation.FCButton();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.txtImmediateDestinationName = new fecherFoundation.FCTextBox();
			this.txtImmediateDestinationRoutingNumber = new fecherFoundation.FCTextBox();
			this.Label25 = new fecherFoundation.FCLabel();
			this.Label26 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.txtACHFederalID = new fecherFoundation.FCTextBox();
			this.txtACHTownName = new fecherFoundation.FCTextBox();
			this.cboFederalIDPrefix = new fecherFoundation.FCComboBox();
			this.chkBalanceACHFile = new fecherFoundation.FCCheckBox();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtImmediateOriginName = new fecherFoundation.FCTextBox();
			this.txtImmediateOriginRoutingNumber = new fecherFoundation.FCTextBox();
			this.txtImmediateOriginODFINumber = new fecherFoundation.FCTextBox();
			this.Label22 = new fecherFoundation.FCLabel();
			this.Label23 = new fecherFoundation.FCLabel();
			this.Label24 = new fecherFoundation.FCLabel();
			this.fraGeneralOptions = new fecherFoundation.FCFrame();
			this.cboEncCarryForward = new fecherFoundation.FCComboBox();
			this.chkAutomaticInterestEntries = new fecherFoundation.FCCheckBox();
			this.fraBankOptions = new fecherFoundation.FCFrame();
			this.cboCRBank = new fecherFoundation.FCComboBox();
			this.cboAPBank = new fecherFoundation.FCComboBox();
			this.cboPayrollBank = new fecherFoundation.FCComboBox();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.chkProjects = new fecherFoundation.FCCheckBox();
			this.chkDue = new fecherFoundation.FCCheckBox();
			this.chkBudgetAccounts = new fecherFoundation.FCCheckBox();
			this.cmdResetSave = new fecherFoundation.FCButton();
			this.cboFirstMonth = new fecherFoundation.FCComboBox();
			this.cboValidAccount = new fecherFoundation.FCComboBox();
			this.txtJournal = new fecherFoundation.FCTextBox();
			this.Label21 = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraDueToFrom = new fecherFoundation.FCFrame();
			this.vsFunds = new fecherFoundation.FCGrid();
			this.fraPrintingOptions = new fecherFoundation.FCFrame();
			this.chkShade = new fecherFoundation.FCCheckBox();
			this.fraLaserAdjustmentOptions = new fecherFoundation.FCFrame();
			this.txt1099AdjustmentHorizontal = new fecherFoundation.FCTextBox();
			this.txt1099Adjustment = new fecherFoundation.FCTextBox();
			this.txtCheckAdjustment = new fecherFoundation.FCTextBox();
			this.txtReportAdjustment = new fecherFoundation.FCTextBox();
			this.txtLabelAdjustment = new fecherFoundation.FCTextBox();
			this.cmdTestCheck = new fecherFoundation.FCButton();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.lblInstruct = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.btnFileCustomCheck = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAPOptions)).BeginInit();
			this.fraAPOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnEditWarrantMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSelectTownLogo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
			this.Frame6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdChooseAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoIncrementPO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEncumbranceByDepartment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAPReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantLandscape)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManualWarrantNumbers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRecapOrderByAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantDeptSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReference)).BeginInit();
			this.fraReference.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSameVendor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAutoIncrementPO)).BeginInit();
			this.fraAutoIncrementPO.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPosting)).BeginInit();
			this.fraPosting.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostARBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCR)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostEN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostAPCorr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostGJ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCreateOpeningAdjustments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBudgetTrnsfer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCRDailyAudit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostMVRapidRenewal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostReversingJournals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCLLiens)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBLCommitments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostUTLiens)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBLSupplementals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostPYProcesses)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostUTBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCLTaxAcquired)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostFADepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraACHOptions)).BeginInit();
			this.fraACHOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintACHPrenoteFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBalanceACHFile)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGeneralOptions)).BeginInit();
			this.fraGeneralOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAutomaticInterestEntries)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBankOptions)).BeginInit();
			this.fraBankOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBudgetAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdResetSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDueToFrom)).BeginInit();
			this.fraDueToFrom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsFunds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPrintingOptions)).BeginInit();
			this.fraPrintingOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShade)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLaserAdjustmentOptions)).BeginInit();
			this.fraLaserAdjustmentOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdTestCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileCustomCheck)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 805);
			this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAPOptions);
			this.ClientArea.Controls.Add(this.cmbOption);
			this.ClientArea.Controls.Add(this.lblOption);
			this.ClientArea.Controls.Add(this.fraPrintingOptions);
			this.ClientArea.Controls.Add(this.fraACHOptions);
			this.ClientArea.Controls.Add(this.fraDueToFrom);
			this.ClientArea.Controls.Add(this.fraPosting);
			this.ClientArea.Controls.Add(this.fraGeneralOptions);
			this.ClientArea.Size = new System.Drawing.Size(1088, 633);
			this.ClientArea.Controls.SetChildIndex(this.fraGeneralOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraPosting, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraDueToFrom, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraACHOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraPrintingOptions, 0);
			this.ClientArea.Controls.SetChildIndex(this.lblOption, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmbOption, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraAPOptions, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.btnFileCustomCheck);
			this.TopPanel.Size = new System.Drawing.Size(1088, 60);
			this.TopPanel.Controls.SetChildIndex(this.btnFileCustomCheck, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(128, 30);
			this.HeaderText.Text = "Customize";
			// 
			// cmbOption
			// 
			this.cmbOption.Items.AddRange(new object[] {
            "General",
            "Accounts Payable",
            "Printing",
            "ACH",
            "Due To / From",
            "Posting"});
			this.cmbOption.Location = new System.Drawing.Point(222, 30);
			this.cmbOption.Name = "cmbOption";
			this.cmbOption.Size = new System.Drawing.Size(250, 40);
			this.cmbOption.TabIndex = 111;
			this.cmbOption.Text = "General";
			this.cmbOption.SelectedIndexChanged += new System.EventHandler(this.cmbOption_Click);
			// 
			// lblOption
			// 
			this.lblOption.Location = new System.Drawing.Point(30, 44);
			this.lblOption.Name = "lblOption";
			this.lblOption.Size = new System.Drawing.Size(126, 15);
			this.lblOption.TabIndex = 112;
			this.lblOption.Text = "BUDGETARY OPTIONS";
			// 
			// fraAPOptions
			// 
			this.fraAPOptions.AppearanceKey = "groupBoxLeftBorder ";
			this.fraAPOptions.Controls.Add(this.btnEditWarrantMessage);
			this.fraAPOptions.Controls.Add(this.btnSelectTownLogo);
			this.fraAPOptions.Controls.Add(this.cmbTrackLastCheckNumber);
			this.fraAPOptions.Controls.Add(this.Frame6);
			this.fraAPOptions.Controls.Add(this.cmdChooseAccounts);
			this.fraAPOptions.Controls.Add(this.chkAutoIncrementPO);
			this.fraAPOptions.Controls.Add(this.chkEncumbranceByDepartment);
			this.fraAPOptions.Controls.Add(this.cboPreviewSchoolAccount);
			this.fraAPOptions.Controls.Add(this.cboBalanceValidation);
			this.fraAPOptions.Controls.Add(this.chkAPReference);
			this.fraAPOptions.Controls.Add(this.chkWarrantLandscape);
			this.fraAPOptions.Controls.Add(this.chkManualWarrantNumbers);
			this.fraAPOptions.Controls.Add(this.chkRecapOrderByAccount);
			this.fraAPOptions.Controls.Add(this.chkWarrantSignature);
			this.fraAPOptions.Controls.Add(this.chkWarrantDeptSummary);
			this.fraAPOptions.Controls.Add(this.chkSignature);
			this.fraAPOptions.Controls.Add(this.cboCheckFormat);
			this.fraAPOptions.Controls.Add(this.cboAPSequence);
			this.fraAPOptions.Controls.Add(this.cboPreviewAccount);
			this.fraAPOptions.Controls.Add(this.txtWarrant);
			this.fraAPOptions.Controls.Add(this.fraReference);
			this.fraAPOptions.Controls.Add(this.fraAutoIncrementPO);
			this.fraAPOptions.Controls.Add(this.Label28);
			this.fraAPOptions.Controls.Add(this.lblSchoolAccountOption);
			this.fraAPOptions.Controls.Add(this.Label12);
			this.fraAPOptions.Controls.Add(this.Label17);
			this.fraAPOptions.Controls.Add(this.Label16);
			this.fraAPOptions.Controls.Add(this.lblTownAccountOption);
			this.fraAPOptions.Controls.Add(this.Label1);
			this.fraAPOptions.Location = new System.Drawing.Point(30, 90);
			this.fraAPOptions.Name = "fraAPOptions";
			this.fraAPOptions.Size = new System.Drawing.Size(1018, 715);
			this.fraAPOptions.TabIndex = 44;
			this.fraAPOptions.Text = "A/P Options";
			this.fraAPOptions.Visible = false;
			// 
			// btnEditWarrantMessage
			// 
			this.btnEditWarrantMessage.AppearanceKey = "actionButton";
			this.btnEditWarrantMessage.Location = new System.Drawing.Point(769, 102);
			this.btnEditWarrantMessage.Name = "btnEditWarrantMessage";
			this.btnEditWarrantMessage.Size = new System.Drawing.Size(145, 40);
			this.btnEditWarrantMessage.TabIndex = 135;
			this.btnEditWarrantMessage.Text = "Edit Warrant Message";
			this.btnEditWarrantMessage.Click += new System.EventHandler(this.btnEditWarrantMessage_Click);
			// 
			// btnSelectTownLogo
			// 
			this.btnSelectTownLogo.AppearanceKey = "actionButton";
			this.btnSelectTownLogo.Location = new System.Drawing.Point(769, 54);
			this.btnSelectTownLogo.Name = "btnSelectTownLogo";
			this.btnSelectTownLogo.Size = new System.Drawing.Size(145, 40);
			this.btnSelectTownLogo.TabIndex = 134;
			this.btnSelectTownLogo.Text = "Select Town Logo";
			this.btnSelectTownLogo.Click += new System.EventHandler(this.btnSelectTownLogo_Click);
			// 
			// cmbTrackLastCheckNumber
			// 
			this.cmbTrackLastCheckNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTrackLastCheckNumber.Location = new System.Drawing.Point(681, 240);
			this.cmbTrackLastCheckNumber.Name = "cmbTrackLastCheckNumber";
			this.cmbTrackLastCheckNumber.Size = new System.Drawing.Size(126, 40);
			this.cmbTrackLastCheckNumber.TabIndex = 31;
			this.ToolTip1.SetToolTip(this.cmbTrackLastCheckNumber, "Auto-populate next check number by bank or by check type per bank");
			// 
			// Frame6
			// 
			this.Frame6.Controls.Add(this.txtReturnAddress1);
			this.Frame6.Controls.Add(this.txtReturnAddress2);
			this.Frame6.Controls.Add(this.txtReturnAddress3);
			this.Frame6.Controls.Add(this.txtReturnAddress4);
			this.Frame6.Location = new System.Drawing.Point(20, 360);
			this.Frame6.Name = "Frame6";
			this.Frame6.Size = new System.Drawing.Size(520, 240);
			this.Frame6.TabIndex = 99;
			this.Frame6.Text = "Custom Check Return Adress";
			// 
			// txtReturnAddress1
			// 
			this.txtReturnAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtReturnAddress1.Location = new System.Drawing.Point(20, 30);
			this.txtReturnAddress1.Name = "txtReturnAddress1";
			this.txtReturnAddress1.Size = new System.Drawing.Size(480, 40);
			this.txtReturnAddress1.TabIndex = 103;
			// 
			// txtReturnAddress2
			// 
			this.txtReturnAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtReturnAddress2.Location = new System.Drawing.Point(20, 80);
			this.txtReturnAddress2.Name = "txtReturnAddress2";
			this.txtReturnAddress2.Size = new System.Drawing.Size(480, 40);
			this.txtReturnAddress2.TabIndex = 102;
			// 
			// txtReturnAddress3
			// 
			this.txtReturnAddress3.BackColor = System.Drawing.SystemColors.Window;
			this.txtReturnAddress3.Location = new System.Drawing.Point(20, 130);
			this.txtReturnAddress3.Name = "txtReturnAddress3";
			this.txtReturnAddress3.Size = new System.Drawing.Size(480, 40);
			this.txtReturnAddress3.TabIndex = 101;
			// 
			// txtReturnAddress4
			// 
			this.txtReturnAddress4.BackColor = System.Drawing.SystemColors.Window;
			this.txtReturnAddress4.Location = new System.Drawing.Point(20, 180);
			this.txtReturnAddress4.Name = "txtReturnAddress4";
			this.txtReturnAddress4.Size = new System.Drawing.Size(480, 40);
			this.txtReturnAddress4.TabIndex = 100;
			// 
			// cmdChooseAccounts
			// 
			this.cmdChooseAccounts.AppearanceKey = "actionButton";
			this.cmdChooseAccounts.Location = new System.Drawing.Point(809, 190);
			this.cmdChooseAccounts.Name = "cmdChooseAccounts";
			this.cmdChooseAccounts.Size = new System.Drawing.Size(145, 40);
			this.cmdChooseAccounts.TabIndex = 73;
			this.cmdChooseAccounts.Text = "Select Accounts";
			this.cmdChooseAccounts.Visible = false;
			this.cmdChooseAccounts.Click += new System.EventHandler(this.cmdChooseAccounts_Click);
			// 
			// chkAutoIncrementPO
			// 
			this.chkAutoIncrementPO.Location = new System.Drawing.Point(580, 593);
			this.chkAutoIncrementPO.Name = "chkAutoIncrementPO";
			this.chkAutoIncrementPO.Size = new System.Drawing.Size(221, 23);
			this.chkAutoIncrementPO.TabIndex = 66;
			this.chkAutoIncrementPO.Text = "Auto Number Encumbrance POs";
			this.chkAutoIncrementPO.CheckedChanged += new System.EventHandler(this.chkAutoIncrementPO_CheckedChanged);
			// 
			// chkEncumbranceByDepartment
			// 
			this.chkEncumbranceByDepartment.Location = new System.Drawing.Point(580, 557);
			this.chkEncumbranceByDepartment.Name = "chkEncumbranceByDepartment";
			this.chkEncumbranceByDepartment.Size = new System.Drawing.Size(207, 23);
			this.chkEncumbranceByDepartment.TabIndex = 62;
			this.chkEncumbranceByDepartment.Text = "Encumbrances by Department";
			// 
			// cboPreviewSchoolAccount
			// 
			this.cboPreviewSchoolAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboPreviewSchoolAccount.Items.AddRange(new object[] {
            "1 - Do Not Print Account Titles",
            "2 - Print Complete Titles",
            "3 - No Default"});
			this.cboPreviewSchoolAccount.Location = new System.Drawing.Point(20, 136);
			this.cboPreviewSchoolAccount.Name = "cboPreviewSchoolAccount";
			this.cboPreviewSchoolAccount.Size = new System.Drawing.Size(518, 40);
			this.cboPreviewSchoolAccount.TabIndex = 27;
			// 
			// cboBalanceValidation
			// 
			this.cboBalanceValidation.BackColor = System.Drawing.SystemColors.Window;
			this.cboBalanceValidation.Items.AddRange(new object[] {
            "1 - Only allow AP and EN entries which won\'t put accounts over budget",
            "2 - Give warning if AP or EN entries will put accounts over budget",
            "3 - Allow any AP or EN entries against accounts"});
			this.cboBalanceValidation.Location = new System.Drawing.Point(179, 190);
			this.cboBalanceValidation.Name = "cboBalanceValidation";
			this.cboBalanceValidation.Size = new System.Drawing.Size(600, 40);
			this.cboBalanceValidation.TabIndex = 28;
			this.cboBalanceValidation.SelectedIndexChanged += new System.EventHandler(this.cboBalanceValidation_SelectedIndexChanged);
			// 
			// chkAPReference
			// 
			this.chkAPReference.Location = new System.Drawing.Point(20, 628);
			this.chkAPReference.Name = "chkAPReference";
			this.chkAPReference.Size = new System.Drawing.Size(197, 23);
			this.chkAPReference.TabIndex = 39;
			this.chkAPReference.Text = "Check A/P Reference Codes";
			this.chkAPReference.CheckedChanged += new System.EventHandler(this.chkAPReference_CheckedChanged);
			// 
			// chkWarrantLandscape
			// 
			this.chkWarrantLandscape.Location = new System.Drawing.Point(580, 340);
			this.chkWarrantLandscape.Name = "chkWarrantLandscape";
			this.chkWarrantLandscape.Size = new System.Drawing.Size(253, 23);
			this.chkWarrantLandscape.TabIndex = 35;
			this.chkWarrantLandscape.Text = "Landscape Warrant / Warrant Preview";
			// 
			// chkManualWarrantNumbers
			// 
			this.chkManualWarrantNumbers.Location = new System.Drawing.Point(580, 376);
			this.chkManualWarrantNumbers.Name = "chkManualWarrantNumbers";
			this.chkManualWarrantNumbers.Size = new System.Drawing.Size(263, 23);
			this.chkManualWarrantNumbers.TabIndex = 36;
			this.chkManualWarrantNumbers.Text = "Allow manual entry of Warrant Numbers";
			// 
			// chkRecapOrderByAccount
			// 
			this.chkRecapOrderByAccount.Location = new System.Drawing.Point(580, 413);
			this.chkRecapOrderByAccount.Name = "chkRecapOrderByAccount";
			this.chkRecapOrderByAccount.Size = new System.Drawing.Size(230, 23);
			this.chkRecapOrderByAccount.TabIndex = 38;
			this.chkRecapOrderByAccount.Text = "Warrant Recap Order by Accounts";
			// 
			// chkWarrantSignature
			// 
			this.chkWarrantSignature.Location = new System.Drawing.Point(580, 485);
			this.chkWarrantSignature.Name = "chkWarrantSignature";
			this.chkWarrantSignature.Size = new System.Drawing.Size(215, 23);
			this.chkWarrantSignature.TabIndex = 33;
			this.chkWarrantSignature.Text = "Warrant Preview Signature Line";
			// 
			// chkWarrantDeptSummary
			// 
			this.chkWarrantDeptSummary.Location = new System.Drawing.Point(580, 521);
			this.chkWarrantDeptSummary.Name = "chkWarrantDeptSummary";
			this.chkWarrantDeptSummary.Size = new System.Drawing.Size(259, 23);
			this.chkWarrantDeptSummary.TabIndex = 34;
			this.chkWarrantDeptSummary.Text = "Warrant Preview Department Summary";
			// 
			// chkSignature
			// 
			this.chkSignature.Location = new System.Drawing.Point(580, 449);
			this.chkSignature.Name = "chkSignature";
			this.chkSignature.Size = new System.Drawing.Size(173, 23);
			this.chkSignature.TabIndex = 32;
			this.chkSignature.Text = "Use Electronic Signature";
			// 
			// cboCheckFormat
			// 
			this.cboCheckFormat.BackColor = System.Drawing.SystemColors.Window;
			this.cboCheckFormat.Location = new System.Drawing.Point(179, 240);
			this.cboCheckFormat.Name = "cboCheckFormat";
			this.cboCheckFormat.Size = new System.Drawing.Size(250, 40);
			this.cboCheckFormat.TabIndex = 29;
			this.cboCheckFormat.DropDown += new System.EventHandler(this.cboCheckFormat_DropDown);
			// 
			// cboAPSequence
			// 
			this.cboAPSequence.BackColor = System.Drawing.SystemColors.Window;
			this.cboAPSequence.Items.AddRange(new object[] {
            "1 - Vendor Number",
            "2 - Vendor Name"});
			this.cboAPSequence.Location = new System.Drawing.Point(179, 290);
			this.cboAPSequence.Name = "cboAPSequence";
			this.cboAPSequence.Size = new System.Drawing.Size(250, 40);
			this.cboAPSequence.TabIndex = 30;
			// 
			// cboPreviewAccount
			// 
			this.cboPreviewAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboPreviewAccount.Items.AddRange(new object[] {
            "1 - Do Not Print Account Titles",
            "2 - Print Dept / Div - Exp / Obj // Dept / Div - Rev Titles",
            "3 - Print Dept / Div - Dept / Div Titles",
            "4 - Print Exp / Obj - Rev Titles",
            "5 - No Default"});
			this.cboPreviewAccount.Location = new System.Drawing.Point(20, 56);
			this.cboPreviewAccount.Name = "cboPreviewAccount";
			this.cboPreviewAccount.Size = new System.Drawing.Size(518, 40);
			this.cboPreviewAccount.TabIndex = 26;
			// 
			// txtWarrant
			// 
			this.txtWarrant.BackColor = System.Drawing.SystemColors.Window;
			this.txtWarrant.Enabled = false;
			this.txtWarrant.Location = new System.Drawing.Point(570, 56);
			this.txtWarrant.Name = "txtWarrant";
			this.txtWarrant.Size = new System.Drawing.Size(85, 40);
			this.txtWarrant.TabIndex = 45;
			this.txtWarrant.Validating += new System.ComponentModel.CancelEventHandler(this.txtWarrant_Validating);
			// 
			// fraReference
			// 
			this.fraReference.AppearanceKey = "groupBoxNoBorders";
			this.fraReference.Controls.Add(this.chkSameVendor);
			this.fraReference.Location = new System.Drawing.Point(20, 628);
			this.fraReference.Name = "fraReference";
			this.fraReference.Size = new System.Drawing.Size(219, 67);
			this.fraReference.TabIndex = 55;
			// 
			// chkSameVendor
			// 
			this.chkSameVendor.Enabled = false;
			this.chkSameVendor.Location = new System.Drawing.Point(20, 30);
			this.chkSameVendor.Name = "chkSameVendor";
			this.chkSameVendor.Size = new System.Drawing.Size(139, 23);
			this.chkSameVendor.TabIndex = 40;
			this.chkSameVendor.Text = "Same Vendor Only";
			// 
			// fraAutoIncrementPO
			// 
			this.fraAutoIncrementPO.AppearanceKey = "groupBoxNoBorders";
			this.fraAutoIncrementPO.Controls.Add(this.txtNextPO);
			this.fraAutoIncrementPO.Controls.Add(this.lblNextPO);
			this.fraAutoIncrementPO.Enabled = false;
			this.fraAutoIncrementPO.Location = new System.Drawing.Point(580, 628);
			this.fraAutoIncrementPO.Name = "fraAutoIncrementPO";
			this.fraAutoIncrementPO.Size = new System.Drawing.Size(230, 90);
			this.fraAutoIncrementPO.TabIndex = 67;
			// 
			// txtNextPO
			// 
			this.txtNextPO.BackColor = System.Drawing.SystemColors.Window;
			this.txtNextPO.Enabled = false;
			this.txtNextPO.Location = new System.Drawing.Point(105, 1);
			this.txtNextPO.Name = "txtNextPO";
			this.txtNextPO.Size = new System.Drawing.Size(68, 40);
			this.txtNextPO.TabIndex = 69;
			// 
			// lblNextPO
			// 
			this.lblNextPO.Enabled = false;
			this.lblNextPO.Location = new System.Drawing.Point(20, 15);
			this.lblNextPO.Name = "lblNextPO";
			this.lblNextPO.Size = new System.Drawing.Size(52, 16);
			this.lblNextPO.TabIndex = 68;
			this.lblNextPO.Text = "NEXT PO";
			// 
			// Label28
			// 
			this.Label28.Location = new System.Drawing.Point(458, 254);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(206, 17);
			this.Label28.TabIndex = 133;
			this.Label28.Text = "REMEMBER LAST CHECK NUMBER";
			this.ToolTip1.SetToolTip(this.Label28, "Auto-populate next check number by bank or by check type per bank");
			// 
			// lblSchoolAccountOption
			// 
			this.lblSchoolAccountOption.Location = new System.Drawing.Point(20, 110);
			this.lblSchoolAccountOption.Name = "lblSchoolAccountOption";
			this.lblSchoolAccountOption.Size = new System.Drawing.Size(300, 16);
			this.lblSchoolAccountOption.TabIndex = 61;
			this.lblSchoolAccountOption.Text = "WARRANT & PREVIEW SCHOOL ACCOUNT OPTIONS";
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(20, 204);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(137, 16);
			this.Label12.TabIndex = 60;
			this.Label12.Text = "BALANCE VALIDATION";
			// 
			// Label17
			// 
			this.Label17.Location = new System.Drawing.Point(20, 254);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(97, 16);
			this.Label17.TabIndex = 53;
			this.Label17.Text = "CHECK FORMAT";
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(20, 304);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(97, 16);
			this.Label16.TabIndex = 51;
			this.Label16.Text = "A/P SEQUENCE";
			// 
			// lblTownAccountOption
			// 
			this.lblTownAccountOption.Location = new System.Drawing.Point(20, 30);
			this.lblTownAccountOption.Name = "lblTownAccountOption";
			this.lblTownAccountOption.Size = new System.Drawing.Size(288, 16);
			this.lblTownAccountOption.TabIndex = 49;
			this.lblTownAccountOption.Text = "WARRANT & PREVIEW TOWN ACCOUNT OPTIONS";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(570, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(153, 16);
			this.Label1.TabIndex = 47;
			this.Label1.Text = "NEXT WARRANT NUMBER";
			// 
			// fraPosting
			// 
			this.fraPosting.Controls.Add(this.chkAutoPostARBills);
			this.fraPosting.Controls.Add(this.chkAutoPostAP);
			this.fraPosting.Controls.Add(this.chkAutoPostCD);
			this.fraPosting.Controls.Add(this.chkAutoPostCR);
			this.fraPosting.Controls.Add(this.chkAutoPostEN);
			this.fraPosting.Controls.Add(this.chkAutoPostAPCorr);
			this.fraPosting.Controls.Add(this.chkAutoPostCM);
			this.fraPosting.Controls.Add(this.chkAutoPostGJ);
			this.fraPosting.Controls.Add(this.chkAutoPostCreateOpeningAdjustments);
			this.fraPosting.Controls.Add(this.chkAutoPostBudgetTrnsfer);
			this.fraPosting.Controls.Add(this.chkAutoPostCRDailyAudit);
			this.fraPosting.Controls.Add(this.chkAutoPostMVRapidRenewal);
			this.fraPosting.Controls.Add(this.chkAutoPostReversingJournals);
			this.fraPosting.Controls.Add(this.chkAutoPostCLLiens);
			this.fraPosting.Controls.Add(this.chkAutoPostBLCommitments);
			this.fraPosting.Controls.Add(this.chkAutoPostUTLiens);
			this.fraPosting.Controls.Add(this.chkAutoPostBLSupplementals);
			this.fraPosting.Controls.Add(this.chkAutoPostPYProcesses);
			this.fraPosting.Controls.Add(this.chkAutoPostUTBills);
			this.fraPosting.Controls.Add(this.chkAutoPostCLTaxAcquired);
			this.fraPosting.Controls.Add(this.chkAutoPostFADepreciation);
			this.fraPosting.Controls.Add(this.Label27);
			this.fraPosting.Location = new System.Drawing.Point(30, 90);
			this.fraPosting.Name = "fraPosting";
			this.fraPosting.Size = new System.Drawing.Size(1018, 476);
			this.fraPosting.TabIndex = 110;
			this.fraPosting.Text = "Posting";
			this.fraPosting.Visible = false;
			// 
			// chkAutoPostARBills
			// 
			this.chkAutoPostARBills.Location = new System.Drawing.Point(560, 70);
			this.chkAutoPostARBills.Name = "chkAutoPostARBills";
			this.chkAutoPostARBills.Size = new System.Drawing.Size(179, 23);
			this.chkAutoPostARBills.TabIndex = 131;
			this.chkAutoPostARBills.Text = "Accounts Receivable Bills";
			// 
			// chkAutoPostAP
			// 
			this.chkAutoPostAP.Location = new System.Drawing.Point(20, 70);
			this.chkAutoPostAP.Name = "chkAutoPostAP";
			this.chkAutoPostAP.Size = new System.Drawing.Size(133, 23);
			this.chkAutoPostAP.TabIndex = 130;
			this.chkAutoPostAP.Text = "Accounts Payable";
			// 
			// chkAutoPostCD
			// 
			this.chkAutoPostCD.Location = new System.Drawing.Point(20, 178);
			this.chkAutoPostCD.Name = "chkAutoPostCD";
			this.chkAutoPostCD.Size = new System.Drawing.Size(150, 23);
			this.chkAutoPostCD.TabIndex = 129;
			this.chkAutoPostCD.Text = "Cash Disbursements";
			// 
			// chkAutoPostCR
			// 
			this.chkAutoPostCR.Location = new System.Drawing.Point(20, 358);
			this.chkAutoPostCR.Name = "chkAutoPostCR";
			this.chkAutoPostCR.Size = new System.Drawing.Size(161, 23);
			this.chkAutoPostCR.TabIndex = 128;
			this.chkAutoPostCR.Text = "Manual Cash Receipts";
			// 
			// chkAutoPostEN
			// 
			this.chkAutoPostEN.Location = new System.Drawing.Point(20, 286);
			this.chkAutoPostEN.Name = "chkAutoPostEN";
			this.chkAutoPostEN.Size = new System.Drawing.Size(115, 23);
			this.chkAutoPostEN.TabIndex = 127;
			this.chkAutoPostEN.Text = "Encumbrances";
			// 
			// chkAutoPostAPCorr
			// 
			this.chkAutoPostAPCorr.Location = new System.Drawing.Point(20, 106);
			this.chkAutoPostAPCorr.Name = "chkAutoPostAPCorr";
			this.chkAutoPostAPCorr.Size = new System.Drawing.Size(205, 23);
			this.chkAutoPostAPCorr.TabIndex = 126;
			this.chkAutoPostAPCorr.Text = "Accounts Payable Corrections";
			// 
			// chkAutoPostCM
			// 
			this.chkAutoPostCM.Location = new System.Drawing.Point(20, 250);
			this.chkAutoPostCM.Name = "chkAutoPostCM";
			this.chkAutoPostCM.Size = new System.Drawing.Size(109, 23);
			this.chkAutoPostCM.TabIndex = 125;
			this.chkAutoPostCM.Text = "Credit Memos";
			// 
			// chkAutoPostGJ
			// 
			this.chkAutoPostGJ.Location = new System.Drawing.Point(20, 322);
			this.chkAutoPostGJ.Name = "chkAutoPostGJ";
			this.chkAutoPostGJ.Size = new System.Drawing.Size(128, 23);
			this.chkAutoPostGJ.TabIndex = 124;
			this.chkAutoPostGJ.Text = "General Journals";
			// 
			// chkAutoPostCreateOpeningAdjustments
			// 
			this.chkAutoPostCreateOpeningAdjustments.Location = new System.Drawing.Point(20, 214);
			this.chkAutoPostCreateOpeningAdjustments.Name = "chkAutoPostCreateOpeningAdjustments";
			this.chkAutoPostCreateOpeningAdjustments.Size = new System.Drawing.Size(198, 23);
			this.chkAutoPostCreateOpeningAdjustments.TabIndex = 123;
			this.chkAutoPostCreateOpeningAdjustments.Text = "Create Opening Adjustments";
			// 
			// chkAutoPostBudgetTrnsfer
			// 
			this.chkAutoPostBudgetTrnsfer.Location = new System.Drawing.Point(20, 142);
			this.chkAutoPostBudgetTrnsfer.Name = "chkAutoPostBudgetTrnsfer";
			this.chkAutoPostBudgetTrnsfer.Size = new System.Drawing.Size(122, 23);
			this.chkAutoPostBudgetTrnsfer.TabIndex = 122;
			this.chkAutoPostBudgetTrnsfer.Text = "Budget Transfer";
			// 
			// chkAutoPostCRDailyAudit
			// 
			this.chkAutoPostCRDailyAudit.Location = new System.Drawing.Point(560, 106);
			this.chkAutoPostCRDailyAudit.Name = "chkAutoPostCRDailyAudit";
			this.chkAutoPostCRDailyAudit.Size = new System.Drawing.Size(181, 23);
			this.chkAutoPostCRDailyAudit.TabIndex = 121;
			this.chkAutoPostCRDailyAudit.Text = "Cash Receipts Daily Audit";
			// 
			// chkAutoPostMVRapidRenewal
			// 
			this.chkAutoPostMVRapidRenewal.Location = new System.Drawing.Point(560, 178);
			this.chkAutoPostMVRapidRenewal.Name = "chkAutoPostMVRapidRenewal";
			this.chkAutoPostMVRapidRenewal.Size = new System.Drawing.Size(202, 23);
			this.chkAutoPostMVRapidRenewal.TabIndex = 120;
			this.chkAutoPostMVRapidRenewal.Text = "Motor Vehicle Rapid Renewal";
			// 
			// chkAutoPostReversingJournals
			// 
			this.chkAutoPostReversingJournals.Location = new System.Drawing.Point(20, 394);
			this.chkAutoPostReversingJournals.Name = "chkAutoPostReversingJournals";
			this.chkAutoPostReversingJournals.Size = new System.Drawing.Size(140, 23);
			this.chkAutoPostReversingJournals.TabIndex = 119;
			this.chkAutoPostReversingJournals.Text = "Reversing Journals";
			// 
			// chkAutoPostCLLiens
			// 
			this.chkAutoPostCLLiens.Location = new System.Drawing.Point(560, 250);
			this.chkAutoPostCLLiens.Name = "chkAutoPostCLLiens";
			this.chkAutoPostCLLiens.Size = new System.Drawing.Size(200, 23);
			this.chkAutoPostCLLiens.TabIndex = 118;
			this.chkAutoPostCLLiens.Text = "Real Estate Collections Liens";
			// 
			// chkAutoPostBLCommitments
			// 
			this.chkAutoPostBLCommitments.Location = new System.Drawing.Point(560, 322);
			this.chkAutoPostBLCommitments.Name = "chkAutoPostBLCommitments";
			this.chkAutoPostBLCommitments.Size = new System.Drawing.Size(174, 23);
			this.chkAutoPostBLCommitments.TabIndex = 117;
			this.chkAutoPostBLCommitments.Text = "Tax Billing Commitments";
			// 
			// chkAutoPostUTLiens
			// 
			this.chkAutoPostUTLiens.Location = new System.Drawing.Point(560, 430);
			this.chkAutoPostUTLiens.Name = "chkAutoPostUTLiens";
			this.chkAutoPostUTLiens.Size = new System.Drawing.Size(134, 23);
			this.chkAutoPostUTLiens.TabIndex = 116;
			this.chkAutoPostUTLiens.Text = "Utility Billing Liens";
			// 
			// chkAutoPostBLSupplementals
			// 
			this.chkAutoPostBLSupplementals.Location = new System.Drawing.Point(560, 358);
			this.chkAutoPostBLSupplementals.Name = "chkAutoPostBLSupplementals";
			this.chkAutoPostBLSupplementals.Size = new System.Drawing.Size(180, 23);
			this.chkAutoPostBLSupplementals.TabIndex = 115;
			this.chkAutoPostBLSupplementals.Text = "Tax Billing Supplementals";
			// 
			// chkAutoPostPYProcesses
			// 
			this.chkAutoPostPYProcesses.Location = new System.Drawing.Point(560, 214);
			this.chkAutoPostPYProcesses.Name = "chkAutoPostPYProcesses";
			this.chkAutoPostPYProcesses.Size = new System.Drawing.Size(133, 23);
			this.chkAutoPostPYProcesses.TabIndex = 114;
			this.chkAutoPostPYProcesses.Text = "Payroll Processes";
			// 
			// chkAutoPostUTBills
			// 
			this.chkAutoPostUTBills.Location = new System.Drawing.Point(560, 394);
			this.chkAutoPostUTBills.Name = "chkAutoPostUTBills";
			this.chkAutoPostUTBills.Size = new System.Drawing.Size(126, 23);
			this.chkAutoPostUTBills.TabIndex = 113;
			this.chkAutoPostUTBills.Text = "Utility Billing Bills";
			// 
			// chkAutoPostCLTaxAcquired
			// 
			this.chkAutoPostCLTaxAcquired.Location = new System.Drawing.Point(560, 286);
			this.chkAutoPostCLTaxAcquired.Name = "chkAutoPostCLTaxAcquired";
			this.chkAutoPostCLTaxAcquired.Size = new System.Drawing.Size(247, 23);
			this.chkAutoPostCLTaxAcquired.TabIndex = 112;
			this.chkAutoPostCLTaxAcquired.Text = "Real Estate Collections Tax Acquired";
			// 
			// chkAutoPostFADepreciation
			// 
			this.chkAutoPostFADepreciation.Location = new System.Drawing.Point(560, 142);
			this.chkAutoPostFADepreciation.Name = "chkAutoPostFADepreciation";
			this.chkAutoPostFADepreciation.Size = new System.Drawing.Size(181, 23);
			this.chkAutoPostFADepreciation.TabIndex = 111;
			this.chkAutoPostFADepreciation.Text = "Fixed Assets Depreciation";
			// 
			// Label27
			// 
			this.Label27.Location = new System.Drawing.Point(20, 44);
			this.Label27.Name = "Label27";
			this.Label27.Size = new System.Drawing.Size(424, 16);
			this.Label27.TabIndex = 132;
			this.Label27.Text = "SELECT THE JOURNAL TYPE(S) THAT WILL BE ALLOWED TO AUTO POST";
			// 
			// fraACHOptions
			// 
			this.fraACHOptions.AppearanceKey = "groupBoxLeftBorder ";
			this.fraACHOptions.Controls.Add(this.cmdPrintACHPrenoteFile);
			this.fraACHOptions.Controls.Add(this.Frame5);
			this.fraACHOptions.Controls.Add(this.Frame3);
			this.fraACHOptions.Controls.Add(this.Frame4);
			this.fraACHOptions.FormatCaption = false;
			this.fraACHOptions.Location = new System.Drawing.Point(30, 90);
			this.fraACHOptions.Name = "fraACHOptions";
			this.fraACHOptions.Size = new System.Drawing.Size(600, 650);
			this.fraACHOptions.TabIndex = 77;
			this.fraACHOptions.Text = "ACH Options";
			this.fraACHOptions.Visible = false;
			// 
			// cmdPrintACHPrenoteFile
			// 
			this.cmdPrintACHPrenoteFile.AppearanceKey = "actionButton";
			this.cmdPrintACHPrenoteFile.Location = new System.Drawing.Point(20, 590);
			this.cmdPrintACHPrenoteFile.Name = "cmdPrintACHPrenoteFile";
			this.cmdPrintACHPrenoteFile.Size = new System.Drawing.Size(210, 40);
			this.cmdPrintACHPrenoteFile.TabIndex = 89;
			this.cmdPrintACHPrenoteFile.Text = "Print ACH Prenote File";
			this.cmdPrintACHPrenoteFile.Click += new System.EventHandler(this.cmdPrintACHPrenoteFile_Click);
			// 
			// Frame5
			// 
			this.Frame5.Controls.Add(this.txtImmediateDestinationName);
			this.Frame5.Controls.Add(this.txtImmediateDestinationRoutingNumber);
			this.Frame5.Controls.Add(this.Label25);
			this.Frame5.Controls.Add(this.Label26);
			this.Frame5.Location = new System.Drawing.Point(20, 30);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(560, 140);
			this.Frame5.TabIndex = 78;
			this.Frame5.Text = "Bank / Immediate Destination";
			// 
			// txtImmediateDestinationName
			// 
			this.txtImmediateDestinationName.BackColor = System.Drawing.SystemColors.Window;
			this.txtImmediateDestinationName.Location = new System.Drawing.Point(158, 30);
			this.txtImmediateDestinationName.Name = "txtImmediateDestinationName";
			this.txtImmediateDestinationName.Size = new System.Drawing.Size(381, 40);
			this.txtImmediateDestinationName.TabIndex = 79;
			// 
			// txtImmediateDestinationRoutingNumber
			// 
			this.txtImmediateDestinationRoutingNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtImmediateDestinationRoutingNumber.Location = new System.Drawing.Point(158, 80);
			this.txtImmediateDestinationRoutingNumber.Name = "txtImmediateDestinationRoutingNumber";
			this.txtImmediateDestinationRoutingNumber.Size = new System.Drawing.Size(196, 40);
			this.txtImmediateDestinationRoutingNumber.TabIndex = 81;
			// 
			// Label25
			// 
			this.Label25.Location = new System.Drawing.Point(20, 44);
			this.Label25.Name = "Label25";
			this.Label25.Size = new System.Drawing.Size(82, 20);
			this.Label25.TabIndex = 90;
			this.Label25.Text = "BANK NAME";
			// 
			// Label26
			// 
			this.Label26.Location = new System.Drawing.Point(20, 94);
			this.Label26.Name = "Label26";
			this.Label26.Size = new System.Drawing.Size(77, 20);
			this.Label26.TabIndex = 80;
			this.Label26.Text = "ROUTING #";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.txtACHFederalID);
			this.Frame3.Controls.Add(this.txtACHTownName);
			this.Frame3.Controls.Add(this.cboFederalIDPrefix);
			this.Frame3.Controls.Add(this.chkBalanceACHFile);
			this.Frame3.Controls.Add(this.Label19);
			this.Frame3.Controls.Add(this.Label15);
			this.Frame3.Controls.Add(this.Label14);
			this.Frame3.Location = new System.Drawing.Point(20, 390);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(560, 190);
			this.Frame3.TabIndex = 95;
			this.Frame3.Text = "Town Information";
			// 
			// txtACHFederalID
			// 
			this.txtACHFederalID.BackColor = System.Drawing.SystemColors.Window;
			this.txtACHFederalID.Location = new System.Drawing.Point(207, 78);
			this.txtACHFederalID.Name = "txtACHFederalID";
			this.txtACHFederalID.Size = new System.Drawing.Size(160, 40);
			this.txtACHFederalID.TabIndex = 86;
			// 
			// txtACHTownName
			// 
			this.txtACHTownName.BackColor = System.Drawing.SystemColors.Window;
			this.txtACHTownName.Location = new System.Drawing.Point(207, 30);
			this.txtACHTownName.Name = "txtACHTownName";
			this.txtACHTownName.Size = new System.Drawing.Size(333, 40);
			this.txtACHTownName.TabIndex = 85;
			// 
			// cboFederalIDPrefix
			// 
			this.cboFederalIDPrefix.BackColor = System.Drawing.SystemColors.Window;
			this.cboFederalIDPrefix.Location = new System.Drawing.Point(207, 130);
			this.cboFederalIDPrefix.Name = "cboFederalIDPrefix";
			this.cboFederalIDPrefix.Size = new System.Drawing.Size(90, 40);
			this.cboFederalIDPrefix.TabIndex = 87;
			// 
			// chkBalanceACHFile
			// 
			this.chkBalanceACHFile.Location = new System.Drawing.Point(367, 130);
			this.chkBalanceACHFile.Name = "chkBalanceACHFile";
			this.chkBalanceACHFile.Size = new System.Drawing.Size(139, 23);
			this.chkBalanceACHFile.TabIndex = 88;
			this.chkBalanceACHFile.Text = "Balanced ACH File";
			// 
			// Label19
			// 
			this.Label19.Location = new System.Drawing.Point(20, 94);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(82, 20);
			this.Label19.TabIndex = 98;
			this.Label19.Text = "EMPLOYER ID";
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(20, 44);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(82, 20);
			this.Label15.TabIndex = 97;
			this.Label15.Text = "TOWN NAME";
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(20, 144);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(129, 16);
			this.Label14.TabIndex = 96;
			this.Label14.Text = "EMPLOYER ID PREFIX";
			// 
			// Frame4
			// 
			this.Frame4.Controls.Add(this.txtImmediateOriginName);
			this.Frame4.Controls.Add(this.txtImmediateOriginRoutingNumber);
			this.Frame4.Controls.Add(this.txtImmediateOriginODFINumber);
			this.Frame4.Controls.Add(this.Label22);
			this.Frame4.Controls.Add(this.Label23);
			this.Frame4.Controls.Add(this.Label24);
			this.Frame4.Location = new System.Drawing.Point(20, 190);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(560, 190);
			this.Frame4.TabIndex = 91;
			this.Frame4.Text = "Immediate Origin";
			// 
			// txtImmediateOriginName
			// 
			this.txtImmediateOriginName.BackColor = System.Drawing.SystemColors.Window;
			this.txtImmediateOriginName.Location = new System.Drawing.Point(158, 30);
			this.txtImmediateOriginName.Name = "txtImmediateOriginName";
			this.txtImmediateOriginName.Size = new System.Drawing.Size(386, 40);
			this.txtImmediateOriginName.TabIndex = 82;
			// 
			// txtImmediateOriginRoutingNumber
			// 
			this.txtImmediateOriginRoutingNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtImmediateOriginRoutingNumber.Location = new System.Drawing.Point(158, 80);
			this.txtImmediateOriginRoutingNumber.Name = "txtImmediateOriginRoutingNumber";
			this.txtImmediateOriginRoutingNumber.Size = new System.Drawing.Size(196, 40);
			this.txtImmediateOriginRoutingNumber.TabIndex = 83;
			// 
			// txtImmediateOriginODFINumber
			// 
			this.txtImmediateOriginODFINumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtImmediateOriginODFINumber.Location = new System.Drawing.Point(158, 130);
			this.txtImmediateOriginODFINumber.Name = "txtImmediateOriginODFINumber";
			this.txtImmediateOriginODFINumber.Size = new System.Drawing.Size(196, 40);
			this.txtImmediateOriginODFINumber.TabIndex = 84;
			// 
			// Label22
			// 
			this.Label22.Location = new System.Drawing.Point(20, 44);
			this.Label22.Name = "Label22";
			this.Label22.Size = new System.Drawing.Size(79, 20);
			this.Label22.TabIndex = 94;
			this.Label22.Text = "BANK NAME";
			// 
			// Label23
			// 
			this.Label23.Location = new System.Drawing.Point(20, 94);
			this.Label23.Name = "Label23";
			this.Label23.Size = new System.Drawing.Size(72, 20);
			this.Label23.TabIndex = 93;
			this.Label23.Text = "ROUTING #";
			// 
			// Label24
			// 
			this.Label24.Location = new System.Drawing.Point(20, 144);
			this.Label24.Name = "Label24";
			this.Label24.Size = new System.Drawing.Size(45, 20);
			this.Label24.TabIndex = 92;
			this.Label24.Text = "ODFI #";
			// 
			// fraGeneralOptions
			// 
			this.fraGeneralOptions.AppearanceKey = "groupBoxLeftBorder ";
			this.fraGeneralOptions.Controls.Add(this.cboEncCarryForward);
			this.fraGeneralOptions.Controls.Add(this.chkAutomaticInterestEntries);
			this.fraGeneralOptions.Controls.Add(this.fraBankOptions);
			this.fraGeneralOptions.Controls.Add(this.chkProjects);
			this.fraGeneralOptions.Controls.Add(this.chkDue);
			this.fraGeneralOptions.Controls.Add(this.chkBudgetAccounts);
			this.fraGeneralOptions.Controls.Add(this.cmdResetSave);
			this.fraGeneralOptions.Controls.Add(this.cboFirstMonth);
			this.fraGeneralOptions.Controls.Add(this.cboValidAccount);
			this.fraGeneralOptions.Controls.Add(this.txtJournal);
			this.fraGeneralOptions.Controls.Add(this.Label21);
			this.fraGeneralOptions.Controls.Add(this.Label20);
			this.fraGeneralOptions.Controls.Add(this.Label18);
			this.fraGeneralOptions.Controls.Add(this.Label2);
			this.fraGeneralOptions.Location = new System.Drawing.Point(30, 90);
			this.fraGeneralOptions.Name = "fraGeneralOptions";
			this.fraGeneralOptions.Size = new System.Drawing.Size(1018, 490);
			this.fraGeneralOptions.TabIndex = 11;
			this.fraGeneralOptions.Text = "General Options";
			// 
			// cboEncCarryForward
			// 
			this.cboEncCarryForward.BackColor = System.Drawing.SystemColors.Window;
			this.cboEncCarryForward.Items.AddRange(new object[] {
            "Individual Accounts",
            "Previous Year Encumbrance Control"});
			this.cboEncCarryForward.Location = new System.Drawing.Point(194, 180);
			this.cboEncCarryForward.Name = "cboEncCarryForward";
			this.cboEncCarryForward.Size = new System.Drawing.Size(314, 40);
			this.cboEncCarryForward.TabIndex = 75;
			// 
			// chkAutomaticInterestEntries
			// 
			this.chkAutomaticInterestEntries.Location = new System.Drawing.Point(622, 338);
			this.chkAutomaticInterestEntries.Name = "chkAutomaticInterestEntries";
			this.chkAutomaticInterestEntries.Size = new System.Drawing.Size(292, 23);
			this.chkAutomaticInterestEntries.TabIndex = 74;
			this.chkAutomaticInterestEntries.Text = "Create Automatic Interest Check Rec Entries";
			this.ToolTip1.SetToolTip(this.chkAutomaticInterestEntries, "Selecting this option will cause all check rec entries in a single journal to be " +
        "combined.");
			// 
			// fraBankOptions
			// 
			this.fraBankOptions.Controls.Add(this.cboCRBank);
			this.fraBankOptions.Controls.Add(this.cboAPBank);
			this.fraBankOptions.Controls.Add(this.cboPayrollBank);
			this.fraBankOptions.Controls.Add(this.Label11);
			this.fraBankOptions.Controls.Add(this.Label7);
			this.fraBankOptions.Controls.Add(this.Label8);
			this.fraBankOptions.Location = new System.Drawing.Point(20, 230);
			this.fraBankOptions.Name = "fraBankOptions";
			this.fraBankOptions.Size = new System.Drawing.Size(575, 190);
			this.fraBankOptions.TabIndex = 25;
			this.fraBankOptions.Text = "Bank Numbers";
			// 
			// cboCRBank
			// 
			this.cboCRBank.BackColor = System.Drawing.SystemColors.Window;
			this.cboCRBank.Items.AddRange(new object[] {
            "None"});
			this.cboCRBank.Location = new System.Drawing.Point(172, 130);
			this.cboCRBank.Name = "cboCRBank";
			this.cboCRBank.Size = new System.Drawing.Size(383, 40);
			this.cboCRBank.TabIndex = 54;
			this.cboCRBank.DropDown += new System.EventHandler(this.cboCRBank_DropDown);
			// 
			// cboAPBank
			// 
			this.cboAPBank.BackColor = System.Drawing.SystemColors.Window;
			this.cboAPBank.Items.AddRange(new object[] {
            "None"});
			this.cboAPBank.Location = new System.Drawing.Point(172, 30);
			this.cboAPBank.Name = "cboAPBank";
			this.cboAPBank.Size = new System.Drawing.Size(383, 40);
			this.cboAPBank.TabIndex = 50;
			this.cboAPBank.DropDown += new System.EventHandler(this.cboAPBank_DropDown);
			// 
			// cboPayrollBank
			// 
			this.cboPayrollBank.BackColor = System.Drawing.SystemColors.Window;
			this.cboPayrollBank.Items.AddRange(new object[] {
            "None"});
			this.cboPayrollBank.Location = new System.Drawing.Point(172, 82);
			this.cboPayrollBank.Name = "cboPayrollBank";
			this.cboPayrollBank.Size = new System.Drawing.Size(383, 40);
			this.cboPayrollBank.TabIndex = 52;
			this.cboPayrollBank.DropDown += new System.EventHandler(this.cboPayrollBank_DropDown);
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 144);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(87, 16);
			this.Label11.TabIndex = 43;
			this.Label11.Text = "CR DEPOSITS";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(20, 44);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(117, 16);
			this.Label7.TabIndex = 42;
			this.Label7.Text = "A/P CHECK REC";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(20, 94);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(129, 16);
			this.Label8.TabIndex = 41;
			this.Label8.Text = "PAYROLL CHECK REC";
			// 
			// chkProjects
			// 
			this.chkProjects.Location = new System.Drawing.Point(622, 266);
			this.chkProjects.Name = "chkProjects";
			this.chkProjects.Size = new System.Drawing.Size(153, 23);
			this.chkProjects.TabIndex = 56;
			this.chkProjects.Text = "Use Project Numbers";
			// 
			// chkDue
			// 
			this.chkDue.Checked = true;
			this.chkDue.CheckState = Wisej.Web.CheckState.Checked;
			this.chkDue.Location = new System.Drawing.Point(622, 302);
			this.chkDue.Name = "chkDue";
			this.chkDue.Size = new System.Drawing.Size(168, 23);
			this.chkDue.TabIndex = 57;
			this.chkDue.Text = "Use Due To / Due From";
			this.chkDue.CheckedChanged += new System.EventHandler(this.chkDue_CheckedChanged);
			// 
			// chkBudgetAccounts
			// 
			this.chkBudgetAccounts.Location = new System.Drawing.Point(622, 230);
			this.chkBudgetAccounts.Name = "chkBudgetAccounts";
			this.chkBudgetAccounts.Size = new System.Drawing.Size(309, 23);
			this.chkBudgetAccounts.TabIndex = 58;
			this.chkBudgetAccounts.Text = "Load only valid accounts for the budget process";
			this.ToolTip1.SetToolTip(this.chkBudgetAccounts, "This option will load only accounts in the valid accounts file instead of all acc" +
        "ounts available");
			// 
			// cmdResetSave
			// 
			this.cmdResetSave.AppearanceKey = "actionButton";
			this.cmdResetSave.Location = new System.Drawing.Point(20, 430);
			this.cmdResetSave.Name = "cmdResetSave";
			this.cmdResetSave.Size = new System.Drawing.Size(295, 40);
			this.cmdResetSave.TabIndex = 59;
			this.cmdResetSave.Text = "Reset Journal Warrant & PO Locks";
			this.cmdResetSave.Click += new System.EventHandler(this.cmdResetSave_Click);
			// 
			// cboFirstMonth
			// 
			this.cboFirstMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboFirstMonth.Items.AddRange(new object[] {
            "1 - January",
            "2 - February",
            "3 - March",
            "4 - April",
            "5 - May",
            "6 - June",
            "7 - July",
            "8 - August",
            "9 - September",
            "10 - October",
            "11 - November",
            "12 - December"});
			this.cboFirstMonth.Location = new System.Drawing.Point(194, 130);
			this.cboFirstMonth.Name = "cboFirstMonth";
			this.cboFirstMonth.Size = new System.Drawing.Size(180, 40);
			this.cboFirstMonth.TabIndex = 48;
			// 
			// cboValidAccount
			// 
			this.cboValidAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboValidAccount.Items.AddRange(new object[] {
            "1 - Only Allow Accounts in the Valid Accounts File",
            "2 - Give Warning if Accounts not in Valid Accounts File",
            "3 - Allow any Account that is set up"});
			this.cboValidAccount.Location = new System.Drawing.Point(194, 80);
			this.cboValidAccount.Name = "cboValidAccount";
			this.cboValidAccount.Size = new System.Drawing.Size(450, 40);
			this.cboValidAccount.TabIndex = 46;
			// 
			// txtJournal
			// 
			this.txtJournal.BackColor = System.Drawing.SystemColors.Window;
			this.txtJournal.Enabled = false;
			this.txtJournal.Location = new System.Drawing.Point(194, 30);
			this.txtJournal.Name = "txtJournal";
			this.txtJournal.Size = new System.Drawing.Size(90, 40);
			this.txtJournal.TabIndex = 6;
			this.txtJournal.Validating += new System.ComponentModel.CancelEventHandler(this.txtJournal_Validating);
			// 
			// Label21
			// 
			this.Label21.Location = new System.Drawing.Point(20, 194);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(126, 16);
			this.Label21.TabIndex = 76;
			this.Label21.Text = "EOY ENCUMBRANCE";
			// 
			// Label20
			// 
			this.Label20.Location = new System.Drawing.Point(20, 144);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(126, 16);
			this.Label20.TabIndex = 9;
			this.Label20.Text = "FIRST FISCAL MONTH";
			// 
			// Label18
			// 
			this.Label18.Location = new System.Drawing.Point(20, 94);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(129, 16);
			this.Label18.TabIndex = 8;
			this.Label18.Text = "VALID ACCOUNT USE";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(145, 16);
			this.Label2.TabIndex = 7;
			this.Label2.Text = "NEXT JOURNAL NUMBER";
			// 
			// fraDueToFrom
			// 
			this.fraDueToFrom.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraDueToFrom.AppearanceKey = "groupBoxNoBorders";
			this.fraDueToFrom.Controls.Add(this.vsFunds);
			this.fraDueToFrom.Location = new System.Drawing.Point(30, 90);
			this.fraDueToFrom.Name = "fraDueToFrom";
			this.fraDueToFrom.Size = new System.Drawing.Size(892, 523);
			this.fraDueToFrom.TabIndex = 106;
			this.fraDueToFrom.Text = "Due To / From";
			this.fraDueToFrom.Visible = false;
			// 
			// vsFunds
			// 
			this.vsFunds.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsFunds.Cols = 7;
			this.vsFunds.Location = new System.Drawing.Point(0, 30);
			this.vsFunds.Name = "vsFunds";
			this.vsFunds.Rows = 1;
			this.vsFunds.Size = new System.Drawing.Size(858, 503);
			this.vsFunds.StandardTab = false;
			this.vsFunds.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsFunds.TabIndex = 107;
			this.vsFunds.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsFunds_ValidateEdit);
			this.vsFunds.CurrentCellChanged += new System.EventHandler(this.vsFunds_RowColChange);
			this.vsFunds.CellClick += new Wisej.Web.DataGridViewCellEventHandler(this.vsFunds_ClickEvent);
			this.vsFunds.KeyUp += new Wisej.Web.KeyEventHandler(this.vsFunds_KeyUpEvent);
			// 
			// fraPrintingOptions
			// 
			this.fraPrintingOptions.AppearanceKey = "groupBoxLeftBorder ";
			this.fraPrintingOptions.Controls.Add(this.chkShade);
			this.fraPrintingOptions.Controls.Add(this.fraLaserAdjustmentOptions);
			this.fraPrintingOptions.Location = new System.Drawing.Point(30, 90);
			this.fraPrintingOptions.Name = "fraPrintingOptions";
			this.fraPrintingOptions.Size = new System.Drawing.Size(616, 472);
			this.fraPrintingOptions.TabIndex = 10;
			this.fraPrintingOptions.Text = "Printing Options";
			this.fraPrintingOptions.Visible = false;
			// 
			// chkShade
			// 
			this.chkShade.Location = new System.Drawing.Point(20, 390);
			this.chkShade.Name = "chkShade";
			this.chkShade.Size = new System.Drawing.Size(340, 23);
			this.chkShade.TabIndex = 24;
			this.chkShade.Text = "Shade Every Other Line in Summary / Detail Reports";
			// 
			// fraLaserAdjustmentOptions
			// 
			this.fraLaserAdjustmentOptions.Controls.Add(this.txt1099AdjustmentHorizontal);
			this.fraLaserAdjustmentOptions.Controls.Add(this.txt1099Adjustment);
			this.fraLaserAdjustmentOptions.Controls.Add(this.txtCheckAdjustment);
			this.fraLaserAdjustmentOptions.Controls.Add(this.txtReportAdjustment);
			this.fraLaserAdjustmentOptions.Controls.Add(this.txtLabelAdjustment);
			this.fraLaserAdjustmentOptions.Controls.Add(this.cmdTestCheck);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label13);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label6);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label4);
			this.fraLaserAdjustmentOptions.Controls.Add(this.lblInstruct);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label5);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label9);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label10);
			this.fraLaserAdjustmentOptions.Controls.Add(this.Label3);
			this.fraLaserAdjustmentOptions.Location = new System.Drawing.Point(20, 30);
			this.fraLaserAdjustmentOptions.Name = "fraLaserAdjustmentOptions";
			this.fraLaserAdjustmentOptions.Size = new System.Drawing.Size(580, 350);
			this.fraLaserAdjustmentOptions.TabIndex = 11;
			this.fraLaserAdjustmentOptions.Text = "Laser Printer Adjustment";
			// 
			// txt1099AdjustmentHorizontal
			// 
			this.txt1099AdjustmentHorizontal.BackColor = System.Drawing.SystemColors.Window;
			this.txt1099AdjustmentHorizontal.Location = new System.Drawing.Point(351, 56);
			this.txt1099AdjustmentHorizontal.Name = "txt1099AdjustmentHorizontal";
			this.txt1099AdjustmentHorizontal.Size = new System.Drawing.Size(80, 40);
			this.txt1099AdjustmentHorizontal.TabIndex = 70;
			// 
			// txt1099Adjustment
			// 
			this.txt1099Adjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txt1099Adjustment.Location = new System.Drawing.Point(251, 56);
			this.txt1099Adjustment.Name = "txt1099Adjustment";
			this.txt1099Adjustment.Size = new System.Drawing.Size(80, 40);
			this.txt1099Adjustment.TabIndex = 14;
			// 
			// txtCheckAdjustment
			// 
			this.txtCheckAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheckAdjustment.Location = new System.Drawing.Point(251, 106);
			this.txtCheckAdjustment.Name = "txtCheckAdjustment";
			this.txtCheckAdjustment.Size = new System.Drawing.Size(80, 40);
			this.txtCheckAdjustment.TabIndex = 16;
			// 
			// txtReportAdjustment
			// 
			this.txtReportAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportAdjustment.Location = new System.Drawing.Point(251, 156);
			this.txtReportAdjustment.Name = "txtReportAdjustment";
			this.txtReportAdjustment.Size = new System.Drawing.Size(80, 40);
			this.txtReportAdjustment.TabIndex = 20;
			// 
			// txtLabelAdjustment
			// 
			this.txtLabelAdjustment.BackColor = System.Drawing.SystemColors.Window;
			this.txtLabelAdjustment.Location = new System.Drawing.Point(251, 206);
			this.txtLabelAdjustment.Name = "txtLabelAdjustment";
			this.txtLabelAdjustment.Size = new System.Drawing.Size(80, 40);
			this.txtLabelAdjustment.TabIndex = 22;
			// 
			// cmdTestCheck
			// 
			this.cmdTestCheck.AppearanceKey = "actionButton";
			this.cmdTestCheck.Location = new System.Drawing.Point(450, 106);
			this.cmdTestCheck.Name = "cmdTestCheck";
			this.cmdTestCheck.Size = new System.Drawing.Size(110, 40);
			this.cmdTestCheck.TabIndex = 18;
			this.cmdTestCheck.Text = "Test Check";
			this.cmdTestCheck.Click += new System.EventHandler(this.cmdTestCheck_Click);
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(351, 30);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(80, 16);
			this.Label13.TabIndex = 72;
			this.Label13.Text = "HOR";
			this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(251, 30);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(80, 16);
			this.Label6.TabIndex = 71;
			this.Label6.Text = "VERT";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 70);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(147, 20);
			this.Label4.TabIndex = 21;
			this.Label4.Text = "1099 / 1096 FORMS";
			// 
			// lblInstruct
			// 
			this.lblInstruct.Location = new System.Drawing.Point(20, 256);
			this.lblInstruct.Name = "lblInstruct";
			this.lblInstruct.Size = new System.Drawing.Size(498, 45);
			this.lblInstruct.TabIndex = 19;
			this.lblInstruct.Text = "ENTER THE NUMBER OF VERTICAL LINES OR HORIZONTAL  THAT THE DATA ON YOUR REPORT NE" +
    "EDS TO BE MOVED.  FRACTIONAL NUMBERS CAN BE ENTERED";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 310);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(119, 20);
			this.Label5.TabIndex = 17;
			this.Label5.Text = "EXAMPLES (1, 1.5, 2)";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 120);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(94, 20);
			this.Label9.TabIndex = 15;
			this.Label9.Text = "LASER CHECKS";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 170);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(172, 20);
			this.Label10.TabIndex = 13;
			this.Label10.Text = "SUMMARY / DETAIL REPORTS";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 220);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(94, 20);
			this.Label3.TabIndex = 12;
			this.Label3.Text = "LASER LABELS";
			// 
			// btnFileCustomCheck
			// 
			this.btnFileCustomCheck.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.btnFileCustomCheck.Location = new System.Drawing.Point(868, 29);
			this.btnFileCustomCheck.Name = "btnFileCustomCheck";
			this.btnFileCustomCheck.Size = new System.Drawing.Size(180, 24);
			this.btnFileCustomCheck.TabIndex = 1;
			this.btnFileCustomCheck.Text = "Edit Custom Check Format";
			this.btnFileCustomCheck.Click += new System.EventHandler(this.btnFileCustomCheck_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(489, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmCustomization
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1088, 693);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomization";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Customize";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmCustomization_Load);
			this.Activated += new System.EventHandler(this.frmCustomization_Activated);
			this.Resize += new System.EventHandler(this.frmCustomization_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomization_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomization_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAPOptions)).EndInit();
			this.fraAPOptions.ResumeLayout(false);
			this.fraAPOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnEditWarrantMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSelectTownLogo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
			this.Frame6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdChooseAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoIncrementPO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkEncumbranceByDepartment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAPReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantLandscape)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkManualWarrantNumbers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRecapOrderByAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkWarrantDeptSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraReference)).EndInit();
			this.fraReference.ResumeLayout(false);
			this.fraReference.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSameVendor)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAutoIncrementPO)).EndInit();
			this.fraAutoIncrementPO.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraPosting)).EndInit();
			this.fraPosting.ResumeLayout(false);
			this.fraPosting.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostARBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCR)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostEN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostAPCorr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostGJ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCreateOpeningAdjustments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBudgetTrnsfer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCRDailyAudit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostMVRapidRenewal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostReversingJournals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCLLiens)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBLCommitments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostUTLiens)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostBLSupplementals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostPYProcesses)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostUTBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostCLTaxAcquired)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAutoPostFADepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraACHOptions)).EndInit();
			this.fraACHOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintACHPrenoteFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkBalanceACHFile)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraGeneralOptions)).EndInit();
			this.fraGeneralOptions.ResumeLayout(false);
			this.fraGeneralOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAutomaticInterestEntries)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBankOptions)).EndInit();
			this.fraBankOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.chkProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBudgetAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdResetSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDueToFrom)).EndInit();
			this.fraDueToFrom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsFunds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPrintingOptions)).EndInit();
			this.fraPrintingOptions.ResumeLayout(false);
			this.fraPrintingOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShade)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraLaserAdjustmentOptions)).EndInit();
			this.fraLaserAdjustmentOptions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdTestCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnFileCustomCheck)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton btnFileCustomCheck;
		public FCButton btnEditWarrantMessage;
		public FCButton btnSelectTownLogo;
	}
}
