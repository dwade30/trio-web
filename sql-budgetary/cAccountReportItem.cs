﻿//Fecher vbPorter - Version 1.0.0.27
namespace TWBD0000
{
	public class cAccountReportItem
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strAccount = string.Empty;
		private double dblDebits;
		private double dblCredits;
		private double dblBeginningBalance;
		private double dblEndBalance;
		private double dblNet;
		private double dblBalanceAdjustments;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Debits
		{
			set
			{
				dblDebits = value;
			}
			get
			{
				double Debits = 0;
				Debits = dblDebits;
				return Debits;
			}
		}

		public double Credits
		{
			set
			{
				dblCredits = value;
			}
			get
			{
				double Credits = 0;
				Credits = dblCredits;
				return Credits;
			}
		}

		public double BeginningBalance
		{
			set
			{
				dblBeginningBalance = value;
			}
			get
			{
				double BeginningBalance = 0;
				BeginningBalance = dblBeginningBalance;
				return BeginningBalance;
			}
		}

		public double EndBalance
		{
			set
			{
				dblEndBalance = value;
			}
			get
			{
				double EndBalance = 0;
				EndBalance = dblEndBalance;
				return EndBalance;
			}
		}

		public double Net
		{
			set
			{
				dblNet = value;
			}
			get
			{
				double Net = 0;
				Net = dblNet;
				return Net;
			}
		}
	}
}
