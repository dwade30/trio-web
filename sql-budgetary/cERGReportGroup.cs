﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWBD0000
{
	public class cERGReportGroup
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strGroupValue = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collExpenses = new cGenericCollection();
		//private cGenericCollection collRevenues = new cGenericCollection();
		//private cGenericCollection collGLs = new cGenericCollection();
		private cGenericCollection collExpenses_AutoInitialized;
		private cGenericCollection collRevenues_AutoInitialized;
		private cGenericCollection collGLs_AutoInitialized;

		private cGenericCollection collExpenses
		{
			get
			{
				if (collExpenses_AutoInitialized == null)
				{
					collExpenses_AutoInitialized = new cGenericCollection();
				}
				return collExpenses_AutoInitialized;
			}
			set
			{
				collExpenses_AutoInitialized = value;
			}
		}

		private cGenericCollection collRevenues
		{
			get
			{
				if (collRevenues_AutoInitialized == null)
				{
					collRevenues_AutoInitialized = new cGenericCollection();
				}
				return collRevenues_AutoInitialized;
			}
			set
			{
				collRevenues_AutoInitialized = value;
			}
		}

		private cGenericCollection collGLs
		{
			get
			{
				if (collGLs_AutoInitialized == null)
				{
					collGLs_AutoInitialized = new cGenericCollection();
				}
				return collGLs_AutoInitialized;
			}
			set
			{
				collGLs_AutoInitialized = value;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string GroupValue
		{
			set
			{
				strGroupValue = value;
			}
			get
			{
				string GroupValue = "";
				GroupValue = strGroupValue;
				return GroupValue;
			}
		}

		public cGenericCollection Expenses
		{
			get
			{
				cGenericCollection Expenses = null;
				Expenses = collExpenses;
				return Expenses;
			}
		}

		public cGenericCollection Revenues
		{
			get
			{
				cGenericCollection Revenues = null;
				Revenues = collRevenues;
				return Revenues;
			}
		}

		public cGenericCollection GeneralLedgers
		{
			get
			{
				cGenericCollection GeneralLedgers = null;
				GeneralLedgers = collGLs;
				return GeneralLedgers;
			}
		}
	}
}
