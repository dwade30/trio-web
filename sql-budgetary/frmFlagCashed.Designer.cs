//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmFlagCashed.
	/// </summary>
	partial class frmFlagCashed : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSelected;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cndCancelRange;
		public fecherFoundation.FCTextBox txtHigh;
		public fecherFoundation.FCTextBox txtLow;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCButton cmdSelect;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCFrame fraAccountType;
		public fecherFoundation.FCFrame fraOptions;
		public fecherFoundation.FCCheckBox chkOD;
		public fecherFoundation.FCCheckBox chkOC;
		public fecherFoundation.FCCheckBox chkIN;
		public fecherFoundation.FCCheckBox chkRT;
		public fecherFoundation.FCCheckBox chkDP;
		public fecherFoundation.FCCheckBox chkPY;
		public fecherFoundation.FCCheckBox chkAP;
		public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCLabel lblTotalAmount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblTotalItemsFlagged;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblBank;
		public fecherFoundation.FCLabel lblStatementDate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFlagCashed));
			this.cmbSelected = new fecherFoundation.FCComboBox();
			this.fraRange = new fecherFoundation.FCFrame();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cndCancelRange = new fecherFoundation.FCButton();
			this.txtHigh = new fecherFoundation.FCTextBox();
			this.txtLow = new fecherFoundation.FCTextBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.cmdSelect = new fecherFoundation.FCButton();
			this.cmdClearAll = new fecherFoundation.FCButton();
			this.fraAccountType = new fecherFoundation.FCFrame();
			this.fraOptions = new fecherFoundation.FCFrame();
			this.chkOD = new fecherFoundation.FCCheckBox();
			this.chkOC = new fecherFoundation.FCCheckBox();
			this.chkIN = new fecherFoundation.FCCheckBox();
			this.chkRT = new fecherFoundation.FCCheckBox();
			this.chkDP = new fecherFoundation.FCCheckBox();
			this.chkPY = new fecherFoundation.FCCheckBox();
			this.chkAP = new fecherFoundation.FCCheckBox();
			this.vs1 = new fecherFoundation.FCGrid();
			this.lblTotalAmount = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblTotalItemsFlagged = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblBank = new fecherFoundation.FCLabel();
			this.lblStatementDate = new fecherFoundation.FCLabel();
			this.cmdFileRange = new fecherFoundation.FCButton();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
			this.fraRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cndCancelRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).BeginInit();
			this.fraAccountType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).BeginInit();
			this.fraOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraRange);
			this.ClientArea.Controls.Add(this.cmdSelect);
			this.ClientArea.Controls.Add(this.cmdClearAll);
			this.ClientArea.Controls.Add(this.fraAccountType);
			this.ClientArea.Controls.Add(this.vs1);
			this.ClientArea.Controls.Add(this.lblTotalAmount);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.lblTotalItemsFlagged);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblBank);
			this.ClientArea.Controls.Add(this.lblStatementDate);
			this.ClientArea.Size = new System.Drawing.Size(1078, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileRange);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileRange, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(151, 30);
			this.HeaderText.Text = "Flag Cashed";
			// 
			// cmbSelected
			// 
			this.cmbSelected.AutoSize = false;
			this.cmbSelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSelected.FormattingEnabled = true;
			this.cmbSelected.Items.AddRange(new object[] {
				"Selected",
				"All"
			});
			this.cmbSelected.Location = new System.Drawing.Point(20, 30);
			this.cmbSelected.Name = "cmbSelected";
			this.cmbSelected.Size = new System.Drawing.Size(121, 40);
			this.cmbSelected.TabIndex = 12;
			this.cmbSelected.SelectedIndexChanged += new System.EventHandler(this.optSelected_CheckedChanged);
			// 
			// fraRange
			// 
			this.fraRange.Anchor = Wisej.Web.AnchorStyles.None;
			this.fraRange.BackColor = System.Drawing.Color.FromName("@window");
			this.fraRange.Controls.Add(this.cmdOK);
			this.fraRange.Controls.Add(this.cndCancelRange);
			this.fraRange.Controls.Add(this.txtHigh);
			this.fraRange.Controls.Add(this.txtLow);
			this.fraRange.Controls.Add(this.Label3);
			this.fraRange.Location = new System.Drawing.Point(387, 181);
			this.fraRange.Name = "fraRange";
			this.fraRange.Size = new System.Drawing.Size(300, 150);
			this.fraRange.TabIndex = 23;
			this.fraRange.Text = "Check Range";
			this.fraRange.Visible = false;
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.Location = new System.Drawing.Point(20, 90);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(90, 40);
			this.cmdOK.TabIndex = 28;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cndCancelRange
			// 
			this.cndCancelRange.AppearanceKey = "actionButton";
			this.cndCancelRange.Location = new System.Drawing.Point(130, 90);
			this.cndCancelRange.Name = "cndCancelRange";
			this.cndCancelRange.Size = new System.Drawing.Size(90, 40);
			this.cndCancelRange.TabIndex = 27;
			this.cndCancelRange.Text = "Cancel";
			this.cndCancelRange.Click += new System.EventHandler(this.cndCancelRange_Click);
			// 
			// txtHigh
			// 
			this.txtHigh.AutoSize = false;
			this.txtHigh.BackColor = System.Drawing.SystemColors.Window;
			this.txtHigh.LinkItem = null;
			this.txtHigh.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtHigh.LinkTopic = null;
			this.txtHigh.Location = new System.Drawing.Point(180, 30);
			this.txtHigh.Name = "txtHigh";
			this.txtHigh.Size = new System.Drawing.Size(100, 40);
			this.txtHigh.TabIndex = 25;
			this.txtHigh.Text = "Text1";
			this.txtHigh.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtHigh_KeyPress);
			// 
			// txtLow
			// 
			this.txtLow.AutoSize = false;
			this.txtLow.BackColor = System.Drawing.SystemColors.Window;
			this.txtLow.LinkItem = null;
			this.txtLow.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLow.LinkTopic = null;
			this.txtLow.Location = new System.Drawing.Point(20, 30);
			this.txtLow.Name = "txtLow";
			this.txtLow.Size = new System.Drawing.Size(100, 40);
			this.txtLow.TabIndex = 24;
			this.txtLow.Text = "Text1";
			this.txtLow.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLow_KeyPress);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(141, 44);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(20, 16);
			this.Label3.TabIndex = 26;
			this.Label3.Text = "TO";
			// 
			// cmdSelect
			// 
			this.cmdSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdSelect.AppearanceKey = "actionButton";
			this.cmdSelect.Location = new System.Drawing.Point(30, 466);
			this.cmdSelect.Name = "cmdSelect";
			this.cmdSelect.Size = new System.Drawing.Size(90, 40);
			this.cmdSelect.TabIndex = 20;
			this.cmdSelect.Text = "Select All";
			this.cmdSelect.Visible = false;
			this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
			// 
			// cmdClearAll
			// 
			this.cmdClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.cmdClearAll.AppearanceKey = "actionButton";
			this.cmdClearAll.Location = new System.Drawing.Point(140, 466);
			this.cmdClearAll.Name = "cmdClearAll";
			this.cmdClearAll.Size = new System.Drawing.Size(90, 40);
			this.cmdClearAll.TabIndex = 19;
			this.cmdClearAll.Text = "Clear All";
			this.cmdClearAll.Visible = false;
			this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
			// 
			// fraAccountType
			// 
			this.fraAccountType.Anchor = AnchorStyles.None;
			this.fraAccountType.BackColor = System.Drawing.Color.FromName("@window");
			this.fraAccountType.Controls.Add(this.cmbSelected);
			this.fraAccountType.Controls.Add(this.fraOptions);
			this.fraAccountType.Location = new System.Drawing.Point(400, 40);
			this.fraAccountType.Name = "fraAccountType";
			this.fraAccountType.Size = new System.Drawing.Size(239, 414);
			this.fraAccountType.TabIndex = 0;
			this.fraAccountType.Text = "Transaction Type";
			// 
			// fraOptions
			// 
			this.fraOptions.AppearanceKey = "groupBoxNoBorders";
			this.fraOptions.BackColor = System.Drawing.Color.FromName("@window");
			this.fraOptions.Controls.Add(this.chkOD);
			this.fraOptions.Controls.Add(this.chkOC);
			this.fraOptions.Controls.Add(this.chkIN);
			this.fraOptions.Controls.Add(this.chkRT);
			this.fraOptions.Controls.Add(this.chkDP);
			this.fraOptions.Controls.Add(this.chkPY);
			this.fraOptions.Controls.Add(this.chkAP);
			this.fraOptions.Enabled = false;
			this.fraOptions.Location = new System.Drawing.Point(1, 72);
			this.fraOptions.Name = "fraOptions";
			this.fraOptions.Size = new System.Drawing.Size(199, 341);
			this.fraOptions.TabIndex = 3;
			// 
			// chkOD
			// 
			this.chkOD.Location = new System.Drawing.Point(19, 296);
			this.chkOD.Name = "chkOD";
			this.chkOD.Size = new System.Drawing.Size(119, 27);
			this.chkOD.TabIndex = 10;
			this.chkOD.Text = "Other Debits";
			// 
			// chkOC
			// 
			this.chkOC.Location = new System.Drawing.Point(19, 250);
			this.chkOC.Name = "chkOC";
			this.chkOC.Size = new System.Drawing.Size(125, 27);
			this.chkOC.TabIndex = 9;
			this.chkOC.Text = "Other Credits";
			// 
			// chkIN
			// 
			this.chkIN.Location = new System.Drawing.Point(19, 204);
			this.chkIN.Name = "chkIN";
			this.chkIN.Size = new System.Drawing.Size(82, 27);
			this.chkIN.TabIndex = 8;
			this.chkIN.Text = "Interest";
			// 
			// chkRT
			// 
			this.chkRT.Location = new System.Drawing.Point(19, 158);
			this.chkRT.Name = "chkRT";
			this.chkRT.Size = new System.Drawing.Size(154, 27);
			this.chkRT.TabIndex = 7;
			this.chkRT.Text = "Returned Checks";
			// 
			// chkDP
			// 
			this.chkDP.Location = new System.Drawing.Point(19, 112);
			this.chkDP.Name = "chkDP";
			this.chkDP.Size = new System.Drawing.Size(91, 27);
			this.chkDP.TabIndex = 6;
			this.chkDP.Text = "Deposits";
			// 
			// chkPY
			// 
			this.chkPY.Location = new System.Drawing.Point(19, 66);
			this.chkPY.Name = "chkPY";
			this.chkPY.Size = new System.Drawing.Size(77, 27);
			this.chkPY.TabIndex = 5;
			this.chkPY.Text = "Payroll";
			// 
			// chkAP
			// 
			this.chkAP.Location = new System.Drawing.Point(19, 20);
			this.chkAP.Name = "chkAP";
			this.chkAP.Size = new System.Drawing.Size(159, 27);
			this.chkAP.TabIndex = 4;
			this.chkAP.Text = "Accounts Payable";
			// 
			// vs1
			// 
			this.vs1.AllowSelection = false;
			this.vs1.AllowUserToResizeColumns = false;
			this.vs1.AllowUserToResizeRows = false;
			this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vs1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vs1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vs1.BackColorBkg = System.Drawing.Color.Empty;
			this.vs1.BackColorFixed = System.Drawing.Color.Empty;
			this.vs1.BackColorSel = System.Drawing.Color.Empty;
			this.vs1.Cols = 7;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vs1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vs1.ColumnHeadersHeight = 30;
			this.vs1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vs1.DefaultCellStyle = dataGridViewCellStyle2;
			this.vs1.DragIcon = null;
			this.vs1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vs1.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vs1.FixedCols = 0;
			this.vs1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vs1.FrozenCols = 0;
			this.vs1.GridColor = System.Drawing.Color.Empty;
			this.vs1.GridColorFixed = System.Drawing.Color.Empty;
			this.vs1.Location = new System.Drawing.Point(30, 66);
			this.vs1.Name = "vs1";
			this.vs1.ReadOnly = true;
			this.vs1.ExtendLastCol = true;
			this.vs1.RowHeadersVisible = false;
			this.vs1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vs1.RowHeightMin = 0;
			this.vs1.Rows = 50;
			this.vs1.ScrollTipText = null;
			this.vs1.ShowColumnVisibilityMenu = false;
			this.vs1.Size = new System.Drawing.Size(1028, 344);
			this.vs1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vs1.TabIndex = 12;
			this.vs1.Visible = false;
			this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
			this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
			this.vs1.CellClick += new DataGridViewCellEventHandler(this.vs1_ClickEvent);
			// 
			// lblTotalAmount
			// 
			this.lblTotalAmount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblTotalAmount.Location = new System.Drawing.Point(384, 430);
			this.lblTotalAmount.Name = "lblTotalAmount";
			this.lblTotalAmount.Size = new System.Drawing.Size(88, 16);
			this.lblTotalAmount.TabIndex = 18;
			this.lblTotalAmount.Text = "0.00";
			this.lblTotalAmount.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label2.Location = new System.Drawing.Point(250, 430);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(95, 16);
			this.Label2.TabIndex = 17;
			this.Label2.Text = "TOTAL AMOUNT";
			this.Label2.Visible = false;
			// 
			// lblTotalItemsFlagged
			// 
			this.lblTotalItemsFlagged.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.lblTotalItemsFlagged.Location = new System.Drawing.Point(162, 430);
			this.lblTotalItemsFlagged.Name = "lblTotalItemsFlagged";
			this.lblTotalItemsFlagged.Size = new System.Drawing.Size(50, 16);
			this.lblTotalItemsFlagged.TabIndex = 16;
			this.lblTotalItemsFlagged.Text = "0";
			this.lblTotalItemsFlagged.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
			this.Label1.Location = new System.Drawing.Point(30, 430);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(95, 16);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "ITEMS FLAGGED";
			this.Label1.Visible = false;
			// 
			// lblBank
			// 
			this.lblBank.Location = new System.Drawing.Point(30, 30);
			this.lblBank.Name = "lblBank";
			this.lblBank.Size = new System.Drawing.Size(282, 16);
			this.lblBank.TabIndex = 14;
			this.lblBank.Text = "LABEL1";
			this.lblBank.Visible = false;
			// 
			// lblStatementDate
			// 
			this.lblStatementDate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblStatementDate.Location = new System.Drawing.Point(827, 30);
			this.lblStatementDate.Name = "lblStatementDate";
			this.lblStatementDate.Size = new System.Drawing.Size(221, 16);
			this.lblStatementDate.TabIndex = 13;
			this.lblStatementDate.Text = "LABEL1";
			this.lblStatementDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.lblStatementDate.Visible = false;
			// 
			// cmdFileRange
			// 
			this.cmdFileRange.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileRange.AppearanceKey = "toolbarButton";
			this.cmdFileRange.Location = new System.Drawing.Point(946, 29);
			this.cmdFileRange.Name = "cmdFileRange";
			this.cmdFileRange.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdFileRange.Size = new System.Drawing.Size(97, 24);
			this.cmdFileRange.TabIndex = 10;
			this.cmdFileRange.Text = "Select Range";
			this.cmdFileRange.Click += new System.EventHandler(this.mnuFileRange_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(488, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(103, 48);
			this.cmdProcessSave.TabIndex = 3;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmFlagCashed
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmFlagCashed";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Flag Cashed";
			this.Load += new System.EventHandler(this.frmFlagCashed_Load);
			this.Activated += new System.EventHandler(this.frmFlagCashed_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFlagCashed_KeyPress);
			this.Resize += new System.EventHandler(this.frmFlagCashed_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
			this.fraRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cndCancelRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountType)).EndInit();
			this.fraAccountType.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraOptions)).EndInit();
			this.fraOptions.ResumeLayout(false);
			this.fraOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkOC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAP)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		public FCButton cmdFileRange;
		private FCButton cmdProcessSave;
	}
}