﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmAddCheckDep.
	/// </summary>
	public partial class frmAddCheckDep : BaseForm
	{
		public frmAddCheckDep()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.vs1.FixedRows = 1;
			//FC:FINAL:ASZ: add code from Load
			// define column variables
			BankCol = 0;
			TypeCol = 1;
			CheckCol = 2;
			AmountCol = 3;
			CodeCol = 4;
			StatusDateCol = 5;
			PayeeCol = 6;
			CheckDateCol = 7;
			// set up column sizes
			vs1.ColWidth(BankCol, 500);
			vs1.ColWidth(TypeCol, 500);
			vs1.ColWidth(CheckCol, 800);
			vs1.ColWidth(AmountCol, 1200);
			vs1.ColWidth(CodeCol, 600);
			vs1.ColWidth(StatusDateCol, 800);
			vs1.ColWidth(PayeeCol, 3400);
			vs1.ColWidth(CheckDateCol, 800);
			dblColPercents[0] = 0.0556179;
			dblColPercents[1] = 0.0556179;
			dblColPercents[2] = 0.0893258;
			dblColPercents[3] = 0.1348314;
			dblColPercents[4] = 0.0674157;
			dblColPercents[5] = 0.0893258;
			dblColPercents[6] = 0.3825842;
			dblColPercents[7] = 0.0893258;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAddCheckDep InstancePtr
		{
			get
			{
				return (frmAddCheckDep)Sys.GetInstance(typeof(frmAddCheckDep));
			}
		}

		protected frmAddCheckDep _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/1/02
		// This form will be used by towns to add new checks and
		// deposits that have been processed to the check rec file
		// so it can be calculated into the check rec process
		// ********************************************************
		int BankCol;
		int TypeCol;
		int CheckCol;
		int AmountCol;
		int CodeCol;
		int StatusDateCol;
		int PayeeCol;
		int CheckDateCol;
		double[] dblColPercents = new double[8 + 1];
		bool blnMenuFlag;
		bool blnUnload;
		// Dave 12/14/2006---------------------------------------------------
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		// -------------------------------------------------------------------
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			int counter2;
			clsDRWrapper rsBankInfo = new clsDRWrapper();
			rsInfo.OmitNullsOnInsert = true;
			rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster");
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, CheckCol) != "")
				{
					for (counter2 = AmountCol; counter2 <= vs1.Cols - 1; counter2++)
					{
						if (vs1.TextMatrix(counter, counter2) == "")
						{
							MessageBox.Show("You are missing information in your record.  Please enter the information or delete the record then try again.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vs1.Select(counter, counter2);
							return;
						}
					}
				}
				else
				{
					if (FCConvert.ToDecimal(vs1.TextMatrix(counter, AmountCol)) != 0)
					{
						MessageBox.Show("You are missing information in your record.  Please enter the information or delete the record then try again.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vs1.Select(counter, CheckCol);
						return;
					}
				}
			}
			//FC:FINAL:MSH - Values in table start from Row = 1, not from Row = 2 (same with issue #740)
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.TextMatrix(counter, CheckCol) != "")
				{
					rsInfo.AddNew();
					rsInfo.Set_Fields("CheckNumber", Strings.Trim(vs1.TextMatrix(counter, CheckCol)));
					rsInfo.Set_Fields("Type", Strings.Trim(vs1.TextMatrix(counter, TypeCol)));
					rsInfo.Set_Fields("CheckDate", Strings.Trim(vs1.TextMatrix(counter, CheckDateCol)));
					rsInfo.Set_Fields("Name", Strings.Trim(vs1.TextMatrix(counter, PayeeCol)));
					rsInfo.Set_Fields("Amount", Strings.Trim(vs1.TextMatrix(counter, AmountCol)));
					//FC:FINAL:MSH - The 'Status' can be equal { "1", "2", "3", "V", "D" }, so we can simplify this part of code (same with issue #740)
					/*
                    if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) < 4)
                    {
                        rsInfo.Set_Fields("Status", Strings.Trim(vs1.TextMatrix(counter, CodeCol)));
                    }
                    else if (FCConvert.ToDouble(Strings.Trim(vs1.TextMatrix(counter, CodeCol))) == 4)
                    {
                        rsInfo.Set_Fields("Status", "V");
                    }
                    else
                    {
                        rsInfo.Set_Fields("Status", "D");
                    }
                    */
					rsInfo.Set_Fields("Status", Strings.Trim(vs1.TextMatrix(counter, CodeCol)));
					rsInfo.Set_Fields("EnteredDate", DateTime.Today);
					rsInfo.Set_Fields("EnteredBy", modGlobalConstants.Statics.clsSecurityClass.Get_UserName());
					rsInfo.Set_Fields("StatusDate", Strings.Trim(vs1.TextMatrix(counter, StatusDateCol)));
					if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "1" || FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "2")
					{
						rsBankInfo.OpenRecordset("SELECT * FROM Banks WHERE ID = " + FCConvert.ToString(Conversion.Val(Strings.Trim(vs1.TextMatrix(counter, BankCol)))));
						if (rsBankInfo.EndOfFile() != true && rsBankInfo.BeginningOfFile() != true)
						{
							if (Information.IsDate(rsBankInfo.Get_Fields("StatementDate")))
							{
								if (FCConvert.ToString(rsInfo.Get_Fields_String("Status")) == "1")
								{
									if (rsInfo.Get_Fields_DateTime("CheckDate") <= rsBankInfo.Get_Fields_DateTime("StatementDate"))
									{
										rsInfo.Set_Fields("Status", "2");
										rsInfo.Set_Fields("StatusDate", DateTime.Today);
									}
								}
								else
								{
									if (rsInfo.Get_Fields_DateTime("CheckDate") > rsBankInfo.Get_Fields_DateTime("StatementDate"))
									{
										rsInfo.Set_Fields("Status", "1");
										rsInfo.Set_Fields("StatusDate", DateTime.Today);
									}
								}
							}
						}
					}
					rsInfo.Set_Fields("BankNumber", Strings.Trim(vs1.TextMatrix(counter, BankCol)));
					clsReportChanges.AddChange("Check: " + vs1.TextMatrix(counter, CheckCol) + "  Date: " + vs1.TextMatrix(counter, CheckDateCol) + "  Payee: " + vs1.TextMatrix(counter, PayeeCol) + "  Amount: " + vs1.TextMatrix(counter, AmountCol) + "  - added");
					rsInfo.Update(true);
				}
			}
			// This function takes all the change records and writes them into the AuditChanges table in the database
			clsReportChanges.SaveToAuditChangesTable("Check Rec Add Check / Deposit", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
			clsReportChanges.Reset();
			if (blnUnload)
			{
				Close();
			}
			else
			{
				vs1.Rows = 1;
				for (counter = 1; counter <= 49; counter++)
				{
					vs1.AddItem("", counter);
					vs1.TextMatrix(counter, BankCol, FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank));
					vs1.TextMatrix(counter, StatusDateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
					vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
					vs1.TextMatrix(counter, TypeCol, "1");
					vs1.TextMatrix(counter, CodeCol, "1");
				}
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
				vs1.Select(1, TypeCol);
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void frmAddCheckDep_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			vs1.Select(1, TypeCol);
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			blnMenuFlag = false;
			this.Refresh();
		}

		private void frmAddCheckDep_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmAddCheckDep_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAddCheckDep.FillStyle	= 0;
			//frmAddCheckDep.ScaleWidth	= 9480;
			//frmAddCheckDep.ScaleHeight	= 7215;
			//frmAddCheckDep.LinkTopic	= "Form2";
			//frmAddCheckDep.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter;
			//FC:FINAL:DDU:#2993 - set alignment of grid
			//vs1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vs1.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vs1.ColAlignment(BankCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(TypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(AmountCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vs1.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(StatusDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(PayeeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vs1.ColAlignment(CheckDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set mergerows for title
			//vs1.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			//vs1.MergeRow(0, true);
			// show titles for columns
			//FC:FINAL:SBE - use label for first fixed row
			this.vs1HeaderText.Text = "--Status--";
			//vs1.TextMatrix(0, BankCol, "--Status--");
			//vs1.MergeRow(0, true, 0, vs1.Cols - 1);
			//vs1.TextMatrix(0, StatusDateCol, "--Status--");
			vs1.TextMatrix(0, BankCol, "Bank");
			vs1.TextMatrix(0, TypeCol, "Type");
			vs1.TextMatrix(0, CheckCol, "Check");
			vs1.TextMatrix(0, AmountCol, "Amount");
			vs1.TextMatrix(0, CodeCol, "Code");
			vs1.TextMatrix(0, StatusDateCol, "Date");
			vs1.TextMatrix(0, PayeeCol, "Payee");
			vs1.TextMatrix(0, CheckDateCol, "Date");
			// set datatype for date columns
			vs1.ColDataType(AmountCol, FCGrid.DataTypeSettings.flexDTCurrency);
			// vs1.ColDataType(CheckDateCol) = flexDTDate
			// set format type for columns
			vs1.ColFormat(AmountCol, "#,###.00");
			// set up editmask for date columns
			vs1.ColEditMask(StatusDateCol, "00/00/00");
			// set up combo lists for type and code columns
			vs1.ColComboList(TypeCol, "#1;1" + "\t" + "Accounts Payable|#2;2" + "\t" + "Payroll|#3;3" + "\t" + "Deposit|#4;4" + "\t" + "Returned Check|#5;5" + "\t" + "Interest|#6;6" + "\t" + "Other Credits|#7;7" + "\t" + "Other Debits");
			vs1.ColComboList(CodeCol, "#1;1" + "\t" + "Issued|#2;2" + "\t" + "Outstanding|#3;3" + "\t" + "Cashed / Cleared|#4;V" + "\t" + "Voided|#5;D" + "\t" + "Deleted");
			// show a border between the titles and the data input section of the grid
			vs1.Select(1, 0, 1, vs1.Cols - 1);
			vs1.CellBorder(Color.FromArgb(1, 1, 1), -1, -1, -1, 1, -1, -1);
			lblBank.Text = App.MainForm.StatusBarText3;
			for (counter = 1; counter <= vs1.Rows - 1; counter++)
			{
				vs1.TextMatrix(counter, BankCol, FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank));
				vs1.TextMatrix(counter, StatusDateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
				vs1.TextMatrix(counter, AmountCol, FCConvert.ToString(0));
				vs1.TextMatrix(counter, TypeCol, FCConvert.ToString(1));
				vs1.TextMatrix(counter, CodeCol, FCConvert.ToString(1));
				// vs1.TextMatrix(counter, CheckDateCol) = format(Date, "MM/dd/yy")
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
			//FC:FINAL:SBE - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vs1.ColAllowedKeys(CheckCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
			vs1.ColAllowedKeys(AmountCol, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
		}

		private void frmAddCheckDep_Resize(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 0; counter <= 7; counter++)
			{
				vs1.ColWidth(counter, FCConvert.ToInt32(vs1.WidthOriginal * dblColPercents[counter]));
			}
			//FC:FINAL:ASZ - use anchors: vs1.Height = vs1.RowHeight(0) * 17 + 75;
		}

		private void frmAddCheckDep_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
				// ElseIf KeyAscii = 13 Then
				// KeyAscii = 0
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vs1.AddItem("", vs1.Row);
			vs1.TextMatrix(vs1.Row, BankCol, FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank));
			vs1.TextMatrix(vs1.Row, StatusDateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
			vs1.TextMatrix(vs1.Row, AmountCol, FCConvert.ToString(0));
			vs1.TextMatrix(vs1.Row, TypeCol, "");
			vs1.TextMatrix(vs1.Row, CodeCol, "");
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			answer = MessageBox.Show("Are you sure you wish to delete this record?", "Delete Record?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (answer == DialogResult.Yes)
			{
				vs1.RemoveItem(vs1.Row);
			}
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = false;
			cmdProcess.Focus();
			cmdProcess_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			blnUnload = true;
			cmdProcess.Focus();
			cmdProcess_Click();
		}

		private void vs1_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vs1.Col == CheckDateCol)
			{
				vs1.EditMask = "0#/0#/0#";
			}
			else
			{
				vs1.EditMask = "";
			}
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void vs1_Enter(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
		}

		private void vs1_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys keyCode = e.KeyCode;
			if (keyCode == Keys.Left)
			{
				if (vs1.Col == PayeeCol)
				{
					if (vs1.EditSelStart == 0)
					{
						keyCode = 0;
						vs1.Col = CodeCol;
					}
				}
			}
			else if (e.KeyCode == Keys.Right)
			{
				if (vs1.Col == CodeCol)
				{
					keyCode = 0;
					vs1.Col = PayeeCol;
				}
			}
			else if (e.KeyCode == Keys.Return)
			{
				if (vs1.Col == CheckCol)
				{
					if (Strings.Trim(vs1.EditText) == "")
					{
						if (vs1.Row > vs1.FixedRows)
						{
							if (Information.IsNumeric(vs1.TextMatrix(vs1.Row - 1, vs1.Col)))
							{
								vs1.EditText = FCConvert.ToString(Conversion.Val(vs1.TextMatrix(vs1.Row - 1, vs1.Col)) + 1);
							}
						}
					}
				}
				else if (vs1.Col == PayeeCol)
				{
					if (Strings.Trim(vs1.EditText) == "")
					{
						if (vs1.Row > vs1.FixedRows)
						{
							vs1.EditText = vs1.TextMatrix(vs1.Row - 1, vs1.Col);
                        }
					}
				}
			}
		}

		private void vs1_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				keyAscii = 0;
                //FC:FINAL:AM:#4327 - select next cell
                //Support.SendKeys("{TAB}", false);
                if (vs1.Col < vs1.Cols)
                {
                    vs1.Col++;
                }
                else if (vs1.Row < vs1.Rows)
                {
                    vs1.Row++;
                    vs1.Col = 0;
                }
				return;
			}
			if (vs1.Col == CheckCol || vs1.Col == AmountCol)
			{
				if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 46 && keyAscii != 8)
				{
					keyAscii = 0;
				}
			}
		}

		private void vs1_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				blnMenuFlag = true;
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionByRow;
				vs1.Row = vs1.MouseRow;
				//this.PopupMenu(mnuRows, this, e.Location);
				mnuRows.ShowDropDown(this, e.X, e.Y);
				//Application.DoEvents();
				blnMenuFlag = false;
			}
		}

		private void vs1_RowColChange(object sender, System.EventArgs e)
		{
			if (vs1.HighLight == FCGrid.HighLightSettings.flexHighlightAlways && blnMenuFlag == false)
			{
				vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vs1.SelectionMode = FCGrid.SelectionModeSettings.flexSelectionFree;
			}
			if (vs1.TextMatrix(vs1.Row, TypeCol) == "" || vs1.Col < TypeCol)
			{
				vs1.Col = TypeCol;
			}
			if (vs1.Col == StatusDateCol)
			{
				vs1.Col += 1;
			}
			if (vs1.Col == CheckCol)
			{
				if (vs1.TextMatrix(vs1.Row, TypeCol) == "3" || vs1.TextMatrix(vs1.Row, TypeCol) == "5" || vs1.TextMatrix(vs1.Row, TypeCol) == "6" || vs1.TextMatrix(vs1.Row, TypeCol) == "7")
				{
					vs1.Col += 1;
				}
				else if (vs1.TextMatrix(vs1.Row, TypeCol) == "4")
				{
					if (Strings.Trim(vs1.TextMatrix(vs1.Row, CheckCol)) == "")
					{
						vs1.TextMatrix(vs1.Row, CheckCol, "0");
					}
				}
			}
			if (vs1.Col == PayeeCol)
			{
				vs1.EditMaxLength = 35;
			}
			else if (vs1.Col == CheckCol)
			{
				vs1.EditMaxLength = 8;
			}
			else
			{
				vs1.EditMaxLength = 0;
			}
			vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.EditCell();
			vs1.EditSelStart = 0;
			if (vs1.Col == AmountCol)
			{
				if (vs1.TextMatrix(vs1.Row, vs1.Col) == "0")
				{
					vs1.EditSelLength = 1;
				}
				else
				{
					vs1.EditSelLength = 0;
				}
			}
			else
			{
				vs1.EditSelLength = 0;
			}
		}

		private void vs1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vs1.GetFlexRowIndex(e.RowIndex);
			int col = vs1.GetFlexColIndex(e.ColumnIndex);
			if (col == TypeCol)
			{
				if (vs1.EditText == "")
				{
					return;
				}
				else
				{
					if (FCConvert.ToDouble(vs1.EditText.Split('\t')[0]) > 2)
					{
						vs1.TextMatrix(row, CheckDateCol, Strings.Format(DateTime.Today, "MM/dd/yy"));
						string vbPorterVar = vs1.EditText.Split('\t')[0];
						if (vbPorterVar == "3")
						{
							vs1.TextMatrix(row, PayeeCol, "Deposit");
							vs1.TextMatrix(row, CheckCol, "0");
						}
						else if (vbPorterVar == "4")
						{
							vs1.TextMatrix(row, PayeeCol, "Returned Check");
							vs1.TextMatrix(row, CheckCol, "0");
						}
						else if (vbPorterVar == "5")
						{
							vs1.TextMatrix(row, PayeeCol, "Interest");
							vs1.TextMatrix(row, CheckCol, "0");
						}
						else if (vbPorterVar == "6")
						{
							vs1.TextMatrix(row, PayeeCol, "Other Credits");
							vs1.TextMatrix(row, CheckCol, "0");
							// vs1.TextMatrix(Row, CodeCol) = "3"
						}
						else if (vbPorterVar == "7")
						{
							vs1.TextMatrix(row, PayeeCol, "Other Debits");
							vs1.TextMatrix(row, CheckCol, "0");
							// vs1.TextMatrix(Row, CodeCol) = "3"
						}
					}
				}
			}
			else if (col == CheckDateCol)
			{
				if (!Information.IsDate(vs1.EditText))
				{
					vs1.EditText = "";
					vs1.EditMask = "";
					vs1.TextMatrix(row, CheckDateCol, "");
					if (row > 2)
					{
						vs1.EditText = vs1.TextMatrix(row - 1, col);
					}
					e.Cancel = false;
				}
				// ElseIf Col = CheckCol Then
				// If Trim(vs1.EditText) = "" Then
				// If Row > 2 Then
				// If IsNumeric(vs1.TextMatrix(Row - 1, Col)) Then
				// vs1.EditText = Val(vs1.TextMatrix(Row - 1, Col)) + 1
				// End If
				// End If
				// End If
			}
			else if (col == CodeCol)
			{
				if (vs1.EditText == "")
				{
					return;
				}
			}
			else if (col == AmountCol)
			{
				if (!Information.IsNumeric(vs1.EditText))
				{
					e.Cancel = true;
					return;
				}
				// ElseIf Col = PayeeCol Then
				// If Trim(vs1.EditText) = "" Then
				// If Row > 2 Then
				// vs1.EditText = vs1.TextMatrix(Row - 1, Col)
				// End If
				// End If
			}
		}


        private void Vs1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null)
            {
                e.Control.KeyDown -= vs1_KeyDownEdit;
                e.Control.KeyDown += vs1_KeyDownEdit;
                e.Control.KeyPress -= vs1_KeyPressEdit;
                e.Control.KeyPress += vs1_KeyPressEdit;
            }
        }

        private void SetCustomFormColors()
		{
			lblBank.ForeColor = Color.Blue;
		}
	}
}
