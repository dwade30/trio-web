﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmCheckListSelection.
	/// </summary>
	public partial class frmCheckListSelection : BaseForm
	{
		public frmCheckListSelection()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:ASZ: add code from Load
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:ASZ: default selection - All
			cmbTypeAll.SelectedIndex = 0;
			cmbStatusSelected.SelectedIndex = 0;
			//FC:FINAL:DDU:#3009 - allow only numeric input
			txtLowCheck.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCheckListSelection InstancePtr
		{
			get
			{
				return (frmCheckListSelection)Sys.GetInstance(typeof(frmCheckListSelection));
			}
		}

		protected frmCheckListSelection _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         5/2/02
		// This form will be used to select which transaction will be
		// shown on the check file list report
		// ********************************************************
		public string strTemp = string.Empty;
		public string strTitle = "";
		public string strCheckSQL = "";
		public string strOtherSQL = "";
		public bool blnNoChecks;
		public bool blnNoOther;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusSelected.SelectedIndex == 1)
			{
				if (chkIssued.CheckState == CheckState.Unchecked && chkOutstanding.CheckState == CheckState.Unchecked && chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			CreateSQL();
			CreateTitle();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			if (chkOrderByCheckNumber.CheckState == CheckState.Checked)
			{
				rptCheckFileListByNumber.InstancePtr.strTransactions = "M";
				frmReportViewer.InstancePtr.Init(rptCheckFileListByNumber.InstancePtr);
			}
			else
			{
				rptCheckFileList.InstancePtr.strTransactions = "M";
				frmReportViewer.InstancePtr.Init(rptCheckFileList.InstancePtr);
			}
		}

		private void frmCheckListSelection_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCheckListSelection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCheckListSelection.FillStyle	= 0;
			//frmCheckListSelection.ScaleWidth	= 9045;
			//frmCheckListSelection.ScaleHeight	= 6930;
			//frmCheckListSelection.LinkTopic	= "Form2";
			//frmCheckListSelection.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			//FC:FINAL:ASZ: moved code to Constructor
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCheckListSelection_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusSelected.SelectedIndex == 1)
			{
				if (chkIssued.CheckState == CheckState.Unchecked && chkOutstanding.CheckState == CheckState.Unchecked && chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			CreateSQL();
			CreateTitle();
			//FC:FINA:KV:IIT807+FC-8697
			this.Hide();
			if (chkOrderByCheckNumber.CheckState == CheckState.Checked)
			{
				rptCheckFileListByNumber.InstancePtr.strTransactions = "M";
				frmReportViewer.InstancePtr.Init(rptCheckFileListByNumber.InstancePtr);
			}
			else
			{
				rptCheckFileList.InstancePtr.strTransactions = "M";
				frmReportViewer.InstancePtr.Init(rptCheckFileList.InstancePtr);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Unchecked && chkPY.CheckState == CheckState.Unchecked && chkDP.CheckState == CheckState.Unchecked && chkRT.CheckState == CheckState.Unchecked && chkIN.CheckState == CheckState.Unchecked && chkOC.CheckState == CheckState.Unchecked && chkOD.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction type before you may continue", "No Type Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbStatusSelected.SelectedIndex == 1)
			{
				if (chkIssued.CheckState == CheckState.Unchecked && chkOutstanding.CheckState == CheckState.Unchecked && chkCashed.CheckState == CheckState.Unchecked && chkVoided.CheckState == CheckState.Unchecked && chkDeleted.CheckState == CheckState.Unchecked)
				{
					MessageBox.Show("You must select at least 1 transaction status before you may continue", "No Status Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			CreateSQL();
			CreateTitle();
			if (chkOrderByCheckNumber.CheckState == CheckState.Checked)
			{
				rptCheckFileListByNumber.InstancePtr.strTransactions = "M";
				modDuplexPrinting.DuplexPrintReport(rptCheckFileListByNumber.InstancePtr);
			}
			else
			{
				rptCheckFileList.InstancePtr.strTransactions = "M";
				modDuplexPrinting.DuplexPrintReport(rptCheckFileList.InstancePtr);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click()
		{
			Support.SendKeys("{F10}", false);
		}

		private void optStatusAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkIssued.CheckState = CheckState.Unchecked;
			chkOutstanding.CheckState = CheckState.Unchecked;
			chkCashed.CheckState = CheckState.Unchecked;
			chkVoided.CheckState = CheckState.Unchecked;
			chkDeleted.CheckState = CheckState.Unchecked;
			fraStatusOptions.Enabled = false;
		}

		private void optStatusSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//fraStatusOptions.Enabled = true;
			if (sender == cmbStatusSelected)
			{
				switch (cmbStatusSelected.SelectedIndex)
				{
					case 0:
						{
							optStatusAll_CheckedChanged(sender, e);
							break;
						}
					case 1:
						{
							fraStatusOptions.Enabled = true;
							break;
						}
				}
			}
		}

		private void optTypeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			chkAP.CheckState = CheckState.Unchecked;
			chkPY.CheckState = CheckState.Unchecked;
			chkDP.CheckState = CheckState.Unchecked;
			chkIN.CheckState = CheckState.Unchecked;
			chkRT.CheckState = CheckState.Unchecked;
			chkOC.CheckState = CheckState.Unchecked;
			chkOD.CheckState = CheckState.Unchecked;
			fraTypeOptions.Enabled = false;
		}

		private void optTypeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:ASZ: radiobuttons to combobox events
			//fraTypeOptions.Enabled = true;
			if (sender == cmbTypeAll)
			{
				switch (cmbTypeAll.SelectedIndex)
				{
					case 1:
						{
							fraTypeOptions.Enabled = true;
							break;
						}
					case 0:
						{
							optTypeAll_CheckedChanged(sender, e);
							break;
						}
				}
			}
		}

		private void txtLowCheck_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLowCheck_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Strings.Trim(txtLowCheck.Text) == "")
			{
				txtLowCheck.Text = "0";
			}
		}

		private void CreateSQL()
		{
			blnNoChecks = false;
			blnNoOther = false;
			strTemp = "SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (";
			strCheckSQL = "SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (";
			strOtherSQL = "SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND CheckNumber >= " + txtLowCheck.Text + " AND (";
			if (cmbTypeAll.SelectedIndex == 1)
			{
				if (chkAP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '1' OR ";
					strCheckSQL += "Type = '1' OR ";
				}
				if (chkPY.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '2' OR ";
					strCheckSQL += "Type = '2' OR ";
				}
				if (chkDP.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '3' OR ";
					strOtherSQL += "Type = '3' OR ";
				}
				if (chkRT.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '4' OR ";
					strOtherSQL += "Type = '4' OR ";
				}
				if (chkIN.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '5' OR ";
					strOtherSQL += "Type = '5' OR ";
				}
				if (chkOC.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '6' OR ";
					strOtherSQL += "Type = '6' OR ";
				}
				if (chkOD.CheckState == CheckState.Checked)
				{
					strTemp += "Type = '7' OR ";
					strOtherSQL += "Type = '7' OR ";
				}
				strTemp = Strings.Left(strTemp, strTemp.Length - 4) + ") AND (";
				if (Strings.Right(strCheckSQL, 1) == "(")
				{
					if (cmbTypeAll.SelectedIndex == 1)
					{
						blnNoChecks = true;
					}
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 6) + " AND (";
				}
				else
				{
					strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 4) + ") AND (";
				}
				if (Strings.Right(strOtherSQL, 1) == "(")
				{
					if (cmbTypeAll.SelectedIndex == 1)
					{
						blnNoOther = true;
					}
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 6) + " AND (";
				}
				else
				{
					strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 4) + ") AND (";
				}
			}
			else
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 6) + " AND (";
				strCheckSQL += "Type = '1' OR Type = '2') AND (";
				strOtherSQL += "Type = '3' OR Type = '4' OR Type = '5' OR Type = '6' OR Type = '7') AND (";
			}
			if (cmbStatusSelected.SelectedIndex == 1)
			{
				if (chkIssued.CheckState == CheckState.Checked)
				{
					strTemp += "Status = '1' OR ";
					strCheckSQL += "Status = '1' OR ";
					strOtherSQL += "Status = '1' OR ";
				}
				if (chkOutstanding.CheckState == CheckState.Checked)
				{
					strTemp += "Status = '2' OR ";
					strCheckSQL += "Status = '2' OR ";
					strOtherSQL += "Status = '2' OR ";
				}
				if (chkCashed.CheckState == CheckState.Checked)
				{
					strTemp += "Status = '3' OR ";
					strCheckSQL += "Status = '3' OR ";
					strOtherSQL += "Status = '3' OR ";
				}
				if (chkVoided.CheckState == CheckState.Checked)
				{
					strTemp += "Status = 'V' OR ";
					strCheckSQL += "Status = 'V' OR ";
					strOtherSQL += "Status = 'V' OR ";
				}
				if (chkDeleted.CheckState == CheckState.Checked)
				{
					strTemp += "Status = 'D' OR ";
					strCheckSQL += "Status = 'D' OR ";
					strOtherSQL += "Status = 'D' OR ";
				}
				strTemp = Strings.Left(strTemp, strTemp.Length - 4) + ")";
				strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 4) + ")";
				strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 4) + ")";
			}
			else
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 6);
				strCheckSQL = Strings.Left(strCheckSQL, strCheckSQL.Length - 6);
				strOtherSQL = Strings.Left(strOtherSQL, strOtherSQL.Length - 6);
			}
		}

		private void CreateTitle()
		{
			strTitle = "";
			if (cmbStatusSelected.SelectedIndex == 0)
			{
				strTitle = "Issued, Outstanding, Cashed, Voided, Deleted, ";
			}
			else
			{
				if (chkIssued.CheckState == CheckState.Checked)
				{
					strTitle += "Issued, ";
				}
				if (chkOutstanding.CheckState == CheckState.Checked)
				{
					strTitle += "Outstanding, ";
				}
				if (chkCashed.CheckState == CheckState.Checked)
				{
					strTitle += "Cashed, ";
				}
				if (chkVoided.CheckState == CheckState.Checked)
				{
					strTitle += "Voided, ";
				}
				if (chkDeleted.CheckState == CheckState.Checked)
				{
					strTitle += "Deleted, ";
				}
			}
			if (cmbTypeAll.SelectedIndex == 0)
			{
				strTitle += "AP Checks, PY Checks, Deposits, Returned Checks, Interest, Other Credits, Other Debits";
			}
			else
			{
				if (chkAP.CheckState == CheckState.Checked)
				{
					strTitle += "AP Checks, ";
				}
				if (chkPY.CheckState == CheckState.Checked)
				{
					strTitle += "PY Checks, ";
				}
				if (chkDP.CheckState == CheckState.Checked)
				{
					strTitle += "Deposits, ";
				}
				if (chkRT.CheckState == CheckState.Checked)
				{
					strTitle += "Returned Checks, ";
				}
				if (chkIN.CheckState == CheckState.Checked)
				{
					strTitle += "Interest, ";
				}
				if (chkOC.CheckState == CheckState.Checked)
				{
					strTitle += "Other Credits, ";
				}
				if (chkOD.CheckState == CheckState.Checked)
				{
					strTitle += "Other Debits, ";
				}
				strTitle = Strings.Left(strTitle, strTitle.Length - 2);
			}
		}
	}
}
