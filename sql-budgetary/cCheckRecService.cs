﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	public class cCheckRecService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public void PurgeChecks()
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
				clsDRWrapper rsInfo = new clsDRWrapper();
				decimal curTotal;
				rsInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
				// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
				curTotal = rsInfo.Get_Fields("Balance");
				rsInfo.OpenRecordset("SELECT Status, Type, SUM(Amount) as TotalAmount, COUNT(Amount) as TotalCount FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " GROUP BY Status, Type ORDER BY Status, Type");
				// Cashed Deposits
				if (rsInfo.FindFirstRecord2("Status, Type", "3,3", ","))
				{
					curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				// Returned Checks
				if (rsInfo.FindFirstRecord2("Status, Type", "3,4", ","))
				{
					curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				// Interest
				if (rsInfo.FindFirstRecord2("Status, Type", "3,5", ","))
				{
					curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				// Other Credits
				if (rsInfo.FindFirstRecord2("Status, Type", "3,6", ","))
				{
					curTotal += rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				// Cashed Checks
				if (rsInfo.FindFirstRecord2("Status, Type", "3,1", ","))
				{
					curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				if (rsInfo.FindFirstRecord2("Status, Type", "3,2", ","))
				{
					curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				// Other Debits
				if (rsInfo.FindFirstRecord2("Status, Type", "3,7", ","))
				{
					curTotal -= rsInfo.Get_Fields_Decimal("TotalAmount");
				}
				rsInfo.OpenRecordset("SELECT * FROM Banks WHERE CurrentBank = 1");
				rsInfo.Edit();
				// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
				if (rsInfo.Get_Fields("Balance") != curTotal)
				{
					// TODO Get_Fields: Check the table for the column [Balance] and replace with corresponding Get_Field method
					clsReportChanges.AddChange("Beginning Balance changed.  Old Value: " + Strings.Format(rsInfo.Get_Fields("Balance"), "#,##0.00") + "  New Value: " + Strings.Format(curTotal, "#,##0.00"));
				}
				rsInfo.Set_Fields("Balance", curTotal);
				rsInfo.Update(true);
				clsDRWrapper rsArchiveInfo = new clsDRWrapper();
				int lngTotal = 0;
				int lngPurged = 0;
				rsArchiveInfo.OpenRecordset("SELECT * FROM CheckRecArchive WHERE ID = 0");
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					lngTotal = rsInfo.RecordCount();
				}
				else
				{
					MessageBox.Show("No Records Found To Purge", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				rsArchiveInfo.OmitNullsOnInsert = true;
				rsInfo.OpenRecordset("SELECT * FROM CheckRecMaster WHERE BankNumber = " + FCConvert.ToString(modBudgetaryMaster.Statics.intCurrentBank) + " AND Status <> '1' AND Status <> '2' ORDER BY Status, Type");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					lngPurged = rsInfo.RecordCount();
					rsInfo.MoveFirst();
					do
					{
						//Application.DoEvents();
						rsArchiveInfo.AddNew();
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("CheckNumber", rsInfo.Get_Fields("CheckNumber"));
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("Type", rsInfo.Get_Fields("Type"));
						if (Information.IsDate(rsInfo.Get_Fields("CheckDate")))
						{
							rsArchiveInfo.Set_Fields("CheckDate", rsInfo.Get_Fields_DateTime("CheckDate"));
						}
						rsArchiveInfo.Set_Fields("Name", rsInfo.Get_Fields_String("Name"));
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("Amount", rsInfo.Get_Fields("Amount"));
						rsArchiveInfo.Set_Fields("Status", rsInfo.Get_Fields_String("Status"));
						rsArchiveInfo.Set_Fields("StatusDate", rsInfo.Get_Fields_DateTime("StatusDate"));
						// TODO Get_Fields: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
						rsArchiveInfo.Set_Fields("BankNumber", rsInfo.Get_Fields("BankNumber"));
						rsArchiveInfo.Set_Fields("ArchiveDate", DateTime.Today);
						rsArchiveInfo.Set_Fields("DirectDeposit", rsInfo.Get_Fields_Boolean("DirectDeposit"));
						rsArchiveInfo.Update(true);
						// TODO Get_Fields: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						clsReportChanges.AddChange("Check: " + rsInfo.Get_Fields("CheckNumber") + "  Date: " + rsInfo.Get_Fields_DateTime("CheckDate") + "  Payee: " + rsInfo.Get_Fields_String("Name") + "  Amount: " + rsInfo.Get_Fields("Amount") + "  - purged");
						rsInfo.Delete();
						rsInfo.Update();
					}
					while (rsInfo.EndOfFile() != true);
				}
				clsReportChanges.SaveToAuditChangesTable("Check Rec Purge Check / Reset Balance", Strings.Trim(modBudgetaryMaster.Statics.intCurrentBank.ToString()), "", "");
				MessageBox.Show(Strings.Format(lngTotal, "00") + " Records Read" + "\r\n" + "\r\n" + Strings.Format(lngTotal - lngPurged, "00") + " Records Written" + "\r\n" + "\r\n" + Strings.Format(lngPurged, "00") + " Records Purged", "Purge Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
