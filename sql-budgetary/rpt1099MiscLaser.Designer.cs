﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rpt1099Laser.
	/// </summary>
	partial class rpt1099MiscLaser
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt1099MiscLaser));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldPayer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPayerFederalID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorName1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCityStateZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAttorneyFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStateID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldVendorName2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRoyalties = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOtherIncome = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFishingBoatProceeds = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldFederalIncomeTaxWithheld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMedical = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldPayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPayerFederalID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAttorneyFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRoyalties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOtherIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFishingBoatProceeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalIncomeTaxWithheld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMedical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPayer,
            this.fldAddress1,
            this.fldAddress2,
            this.fldAddress3,
            this.fldRent,
            this.fldPayerFederalID,
            this.fldTaxID,
            this.fldVendorName1,
            this.fldVendorAddress1,
            this.fldVendorAddress2,
            this.fldCityStateZip,
            this.fldVendorNumber,
            this.fldAttorneyFee,
            this.fldStateID,
            this.fldVendorName2,
            this.fldRoyalties,
            this.fldOtherIncome,
            this.fldFishingBoatProceeds,
            this.fldFederalIncomeTaxWithheld,
            this.fldMedical,
            this.fldPhone});
            this.Detail.Height = 5.5F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldPayer
            // 
            this.fldPayer.CanGrow = false;
            this.fldPayer.Height = 0.19F;
            this.fldPayer.Left = 0.375F;
            this.fldPayer.MultiLine = false;
            this.fldPayer.Name = "fldPayer";
            this.fldPayer.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldPayer.Text = "Field1";
            this.fldPayer.Top = 0.8125F;
            this.fldPayer.Width = 2.75F;
            // 
            // fldAddress1
            // 
            this.fldAddress1.CanGrow = false;
            this.fldAddress1.Height = 0.19F;
            this.fldAddress1.Left = 0.375F;
            this.fldAddress1.MultiLine = false;
            this.fldAddress1.Name = "fldAddress1";
            this.fldAddress1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldAddress1.Text = "Field1";
            this.fldAddress1.Top = 0.96875F;
            this.fldAddress1.Width = 2.75F;
            // 
            // fldAddress2
            // 
            this.fldAddress2.CanGrow = false;
            this.fldAddress2.Height = 0.19F;
            this.fldAddress2.Left = 0.375F;
            this.fldAddress2.MultiLine = false;
            this.fldAddress2.Name = "fldAddress2";
            this.fldAddress2.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldAddress2.Text = "Field1";
            this.fldAddress2.Top = 1.125F;
            this.fldAddress2.Width = 2.75F;
            // 
            // fldAddress3
            // 
            this.fldAddress3.CanGrow = false;
            this.fldAddress3.Height = 0.19F;
            this.fldAddress3.Left = 0.375F;
            this.fldAddress3.MultiLine = false;
            this.fldAddress3.Name = "fldAddress3";
            this.fldAddress3.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldAddress3.Text = "Field1";
            this.fldAddress3.Top = 1.28125F;
            this.fldAddress3.Width = 2.75F;
            // 
            // fldRent
            // 
            this.fldRent.CanGrow = false;
            this.fldRent.Height = 0.19F;
            this.fldRent.Left = 3.75F;
            this.fldRent.MultiLine = false;
            this.fldRent.Name = "fldRent";
            this.fldRent.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldRent.Text = "Field1";
            this.fldRent.Top = 0.75F;
            this.fldRent.Width = 1.09375F;
            // 
            // fldPayerFederalID
            // 
            this.fldPayerFederalID.CanGrow = false;
            this.fldPayerFederalID.Height = 0.19F;
            this.fldPayerFederalID.Left = 0.4375F;
            this.fldPayerFederalID.MultiLine = false;
            this.fldPayerFederalID.Name = "fldPayerFederalID";
            this.fldPayerFederalID.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldPayerFederalID.Text = "Field1";
            this.fldPayerFederalID.Top = 2.28125F;
            this.fldPayerFederalID.Width = 1.125F;
            // 
            // fldTaxID
            // 
            this.fldTaxID.CanGrow = false;
            this.fldTaxID.Height = 0.19F;
            this.fldTaxID.Left = 2.1875F;
            this.fldTaxID.MultiLine = false;
            this.fldTaxID.Name = "fldTaxID";
            this.fldTaxID.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldTaxID.Text = "Field1";
            this.fldTaxID.Top = 2.28125F;
            this.fldTaxID.Width = 1.125F;
            // 
            // fldVendorName1
            // 
            this.fldVendorName1.CanGrow = false;
            this.fldVendorName1.Height = 0.19F;
            this.fldVendorName1.Left = 0.437F;
            this.fldVendorName1.MultiLine = false;
            this.fldVendorName1.Name = "fldVendorName1";
            this.fldVendorName1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldVendorName1.Text = "Field1";
            this.fldVendorName1.Top = 2.681F;
            this.fldVendorName1.Width = 2.65625F;
            // 
            // fldVendorAddress1
            // 
            this.fldVendorAddress1.CanGrow = false;
            this.fldVendorAddress1.Height = 0.19F;
            this.fldVendorAddress1.Left = 0.4375F;
            this.fldVendorAddress1.MultiLine = false;
            this.fldVendorAddress1.Name = "fldVendorAddress1";
            this.fldVendorAddress1.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldVendorAddress1.Text = "Field1";
            this.fldVendorAddress1.Top = 3.09375F;
            this.fldVendorAddress1.Width = 2.53125F;
            // 
            // fldVendorAddress2
            // 
            this.fldVendorAddress2.CanGrow = false;
            this.fldVendorAddress2.Height = 0.19F;
            this.fldVendorAddress2.Left = 0.4375F;
            this.fldVendorAddress2.MultiLine = false;
            this.fldVendorAddress2.Name = "fldVendorAddress2";
            this.fldVendorAddress2.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldVendorAddress2.Text = "Field1";
            this.fldVendorAddress2.Top = 3.25F;
            this.fldVendorAddress2.Width = 2.53125F;
            // 
            // fldCityStateZip
            // 
            this.fldCityStateZip.CanGrow = false;
            this.fldCityStateZip.Height = 0.19F;
            this.fldCityStateZip.Left = 0.4375F;
            this.fldCityStateZip.MultiLine = false;
            this.fldCityStateZip.Name = "fldCityStateZip";
            this.fldCityStateZip.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldCityStateZip.Text = "Field1";
            this.fldCityStateZip.Top = 3.71875F;
            this.fldCityStateZip.Width = 2.5625F;
            // 
            // fldVendorNumber
            // 
            this.fldVendorNumber.CanGrow = false;
            this.fldVendorNumber.Height = 0.19F;
            this.fldVendorNumber.Left = 0.78125F;
            this.fldVendorNumber.MultiLine = false;
            this.fldVendorNumber.Name = "fldVendorNumber";
            this.fldVendorNumber.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldVendorNumber.Text = "Field1";
            this.fldVendorNumber.Top = 4.21875F;
            this.fldVendorNumber.Width = 1.09375F;
            // 
            // fldAttorneyFee
            // 
            this.fldAttorneyFee.CanGrow = false;
            this.fldAttorneyFee.Height = 0.1875F;
            this.fldAttorneyFee.Left = 5.25F;
            this.fldAttorneyFee.MultiLine = false;
            this.fldAttorneyFee.Name = "fldAttorneyFee";
            this.fldAttorneyFee.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldAttorneyFee.Text = "0.00";
            this.fldAttorneyFee.Top = 3.252F;
            this.fldAttorneyFee.Width = 1F;
            // 
            // fldStateID
            // 
            this.fldStateID.CanGrow = false;
            this.fldStateID.Height = 0.19F;
            this.fldStateID.Left = 5.25F;
            this.fldStateID.MultiLine = false;
            this.fldStateID.Name = "fldStateID";
            this.fldStateID.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldStateID.Text = "Field1";
            this.fldStateID.Top = 4.625F;
            this.fldStateID.Width = 0.9375F;
            // 
            // fldVendorName2
            // 
            this.fldVendorName2.CanGrow = false;
            this.fldVendorName2.Height = 0.19F;
            this.fldVendorName2.Left = 0.4375F;
            this.fldVendorName2.MultiLine = false;
            this.fldVendorName2.Name = "fldVendorName2";
            this.fldVendorName2.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldVendorName2.Text = "Field1";
            this.fldVendorName2.Top = 2.837F;
            this.fldVendorName2.Width = 2.65625F;
            // 
            // fldRoyalties
            // 
            this.fldRoyalties.CanGrow = false;
            this.fldRoyalties.Height = 0.19F;
            this.fldRoyalties.Left = 3.75F;
            this.fldRoyalties.MultiLine = false;
            this.fldRoyalties.Name = "fldRoyalties";
            this.fldRoyalties.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldRoyalties.Text = "Field1";
            this.fldRoyalties.Top = 1.21875F;
            this.fldRoyalties.Width = 1.09375F;
            // 
            // fldOtherIncome
            // 
            this.fldOtherIncome.CanGrow = false;
            this.fldOtherIncome.Height = 0.19F;
            this.fldOtherIncome.Left = 3.75F;
            this.fldOtherIncome.MultiLine = false;
            this.fldOtherIncome.Name = "fldOtherIncome";
            this.fldOtherIncome.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldOtherIncome.Text = "Field1";
            this.fldOtherIncome.Top = 1.657F;
            this.fldOtherIncome.Width = 1.09375F;
            // 
            // fldFishingBoatProceeds
            // 
            this.fldFishingBoatProceeds.CanGrow = false;
            this.fldFishingBoatProceeds.Height = 0.19F;
            this.fldFishingBoatProceeds.Left = 3.75F;
            this.fldFishingBoatProceeds.MultiLine = false;
            this.fldFishingBoatProceeds.Name = "fldFishingBoatProceeds";
            this.fldFishingBoatProceeds.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldFishingBoatProceeds.Text = "Field1";
            this.fldFishingBoatProceeds.Top = 2.28125F;
            this.fldFishingBoatProceeds.Width = 1.09375F;
            // 
            // fldFederalIncomeTaxWithheld
            // 
            this.fldFederalIncomeTaxWithheld.CanGrow = false;
            this.fldFederalIncomeTaxWithheld.Height = 0.19F;
            this.fldFederalIncomeTaxWithheld.Left = 5.250001F;
            this.fldFederalIncomeTaxWithheld.MultiLine = false;
            this.fldFederalIncomeTaxWithheld.Name = "fldFederalIncomeTaxWithheld";
            this.fldFederalIncomeTaxWithheld.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldFederalIncomeTaxWithheld.Text = "0.00";
            this.fldFederalIncomeTaxWithheld.Top = 1.6565F;
            this.fldFederalIncomeTaxWithheld.Width = 1F;
            // 
            // fldMedical
            // 
            this.fldMedical.CanGrow = false;
            this.fldMedical.Height = 0.19F;
            this.fldMedical.Left = 5.25F;
            this.fldMedical.MultiLine = false;
            this.fldMedical.Name = "fldMedical";
            this.fldMedical.Style = "font-family: \'Roman 10cpi\'; text-align: right; white-space: nowrap; ddo-char-set:" +
    " 1";
            this.fldMedical.Text = "0.00";
            this.fldMedical.Top = 2.28125F;
            this.fldMedical.Width = 1F;
            // 
            // fldPhone
            // 
            this.fldPhone.CanGrow = false;
            this.fldPhone.Height = 0.19F;
            this.fldPhone.Left = 0.375F;
            this.fldPhone.MultiLine = false;
            this.fldPhone.Name = "fldPhone";
            this.fldPhone.Style = "font-family: \'Roman 10cpi\'; white-space: nowrap; ddo-char-set: 1";
            this.fldPhone.Text = "Field1";
            this.fldPhone.Top = 1.4375F;
            this.fldPhone.Width = 2.75F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rpt1099MiscLaser
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldPayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPayerFederalID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCityStateZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAttorneyFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldVendorName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRoyalties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOtherIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFishingBoatProceeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldFederalIncomeTaxWithheld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMedical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayerFederalID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorName1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCityStateZip;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAttorneyFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStateID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldVendorName2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRoyalties;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOtherIncome;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFishingBoatProceeds;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFederalIncomeTaxWithheld;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMedical;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
