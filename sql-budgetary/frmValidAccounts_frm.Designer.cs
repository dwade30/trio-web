﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Core;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmValidAccounts.
	/// </summary>
	partial class frmValidAccounts : BaseForm
	{
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuNew;
        public fecherFoundation.FCToolStripMenuItem mnuDelete;
        public fecherFoundation.FCGrid vs1;
		public fecherFoundation.FCButton btnFileNew;
		public fecherFoundation.FCButton btnFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperatorNew;
		public fecherFoundation.FCButton btnProcessAddRevenue;
		public fecherFoundation.FCButton btnProcessAddLedger;
		public fecherFoundation.FCButton btnProcessAddUsed;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmValidAccounts));
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.vs1 = new fecherFoundation.FCGrid();
            this.btnFileNew = new fecherFoundation.FCButton();
            this.btnFileDelete = new fecherFoundation.FCButton();
            this.mnuFileSeperatorNew = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcessAddRevenue = new fecherFoundation.FCButton();
            this.btnProcessAddLedger = new fecherFoundation.FCButton();
            this.btnProcessAddUsed = new fecherFoundation.FCButton();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddRevenue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddLedger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 576);
            this.BottomPanel.Size = new System.Drawing.Size(1167, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Size = new System.Drawing.Size(1187, 606);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnProcessAddRevenue);
            this.TopPanel.Controls.Add(this.btnProcessAddLedger);
            this.TopPanel.Controls.Add(this.btnProcessAddUsed);
            this.TopPanel.Size = new System.Drawing.Size(1187, 60);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessAddUsed, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessAddLedger, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessAddRevenue, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(163, 28);
            this.HeaderText.Text = "Valid Accounts";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuDelete});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New";
            this.mnuNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.btnFileDelete_Click);
            // 
            // vs1
            // 
            this.vs1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs1.Cols = 7;
            this.vs1.ExtendLastCol = true;
            this.vs1.Location = new System.Drawing.Point(30, 30);
            this.vs1.Name = "vs1";
            this.vs1.Rows = 50;
            this.vs1.SelectionMode = fecherFoundation.FCGrid.SelectionModeSettings.flexSelectionByRow;
            this.vs1.Size = new System.Drawing.Size(1127, 546);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 0;
            this.vs1.Visible = false;
            this.vs1.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEdit);
            this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
            this.vs1.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.vs1_KeyUpEdit);
            this.vs1.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vs1_BeforeEdit);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            // 
            // btnFileNew
            // 
            this.btnFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileNew.Location = new System.Drawing.Point(361, 27);
            this.btnFileNew.Name = "btnFileNew";
            this.btnFileNew.Size = new System.Drawing.Size(42, 24);
            this.btnFileNew.TabIndex = 1;
            this.btnFileNew.Text = "New";
            this.btnFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // btnFileDelete
            // 
            this.btnFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileDelete.Location = new System.Drawing.Point(406, 27);
            this.btnFileDelete.Name = "btnFileDelete";
            this.btnFileDelete.Shortcut = Wisej.Web.Shortcut.F4;
            this.btnFileDelete.Size = new System.Drawing.Size(53, 24);
            this.btnFileDelete.TabIndex = 0;
            this.btnFileDelete.Text = "Delete ";
            this.btnFileDelete.Click += new System.EventHandler(this.btnFileDelete_Click);
            // 
            // mnuFileSeperatorNew
            // 
            this.mnuFileSeperatorNew.Index = -1;
            this.mnuFileSeperatorNew.Name = "mnuFileSeperatorNew";
            this.mnuFileSeperatorNew.Text = "-";
            // 
            // btnProcessAddRevenue
            // 
            this.btnProcessAddRevenue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessAddRevenue.Location = new System.Drawing.Point(463, 27);
            this.btnProcessAddRevenue.Name = "btnProcessAddRevenue";
            this.btnProcessAddRevenue.Size = new System.Drawing.Size(238, 24);
            this.btnProcessAddRevenue.TabIndex = 2;
            this.btnProcessAddRevenue.Text = "Add All Available Revenue Accounts";
            this.btnProcessAddRevenue.Click += new System.EventHandler(this.btnProcessAddRevenue_Click);
            // 
            // btnProcessAddLedger
            // 
            this.btnProcessAddLedger.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessAddLedger.Location = new System.Drawing.Point(705, 27);
            this.btnProcessAddLedger.Name = "btnProcessAddLedger";
            this.btnProcessAddLedger.Size = new System.Drawing.Size(280, 24);
            this.btnProcessAddLedger.TabIndex = 3;
            this.btnProcessAddLedger.Text = "Add All Available General Ledger Accounts";
            this.btnProcessAddLedger.Click += new System.EventHandler(this.btnProcessAddLedger_Click);
            // 
            // btnProcessAddUsed
            // 
            this.btnProcessAddUsed.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessAddUsed.Location = new System.Drawing.Point(989, 27);
            this.btnProcessAddUsed.Name = "btnProcessAddUsed";
            this.btnProcessAddUsed.Size = new System.Drawing.Size(158, 24);
            this.btnProcessAddUsed.TabIndex = 4;
            this.btnProcessAddUsed.Text = "Add All Used Accounts";
            this.btnProcessAddUsed.Click += new System.EventHandler(this.btnProcessAddUsed_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = -1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = -1;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = -1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = -1;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 3;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmValidAccounts
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1187, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmValidAccounts";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Valid Accounts";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmValidAccounts_Load);
            this.Activated += new System.EventHandler(this.frmValidAccounts_Activated);
            this.Resize += new System.EventHandler(this.frmValidAccounts_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmValidAccounts_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmValidAccounts_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddRevenue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddLedger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessAddUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
