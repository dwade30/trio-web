﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmEditReportSetup.
	/// </summary>
	partial class frmEditReportSetup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCButton cmdPrint;
		public Global.T2KBackFillDecimal txtAmount;
		public Global.T2KOverTypeBox txtYear;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditReportSetup));
            this.cmbName = new fecherFoundation.FCComboBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.txtAmount = new Global.T2KBackFillDecimal();
            this.txtYear = new Global.T2KOverTypeBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.cboTaxFormType = new Wisej.Web.ComboBox();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 326);
            this.BottomPanel.Size = new System.Drawing.Size(647, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.ClientArea.Controls.Add(this.cboTaxFormType);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cmdPrint);
            this.ClientArea.Controls.Add(this.cmbName);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(667, 336);
            this.ClientArea.TabIndex = 0;
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblName, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbName, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrint, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboTaxFormType, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(667, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 28);
            this.HeaderText.Text = "Print Edit Report";
            // 
            // cmbName
            // 
            this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Number"});
            this.cmbName.Location = new System.Drawing.Point(211, 150);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(130, 40);
            this.cmbName.TabIndex = 4;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 167);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(65, 16);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "ORDER BY";
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.DialogResult = Wisej.Web.DialogResult.OK;
            this.cmdPrint.Location = new System.Drawing.Point(30, 278);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(100, 48);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(211, 90);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(130, 22);
            this.txtAmount.TabIndex = 2;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(211, 30);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(70, 40);
            this.txtYear.TabIndex = 5;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(65, 16);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "TAX YEAR";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(110, 16);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "MINIMUM AMOUNT";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 0;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print / Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(30, 228);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(110, 16);
            this.fcLabel1.TabIndex = 1001;
            this.fcLabel1.Text = "FORM TYPE(S)";
            // 
            // cboTaxFormType
            // 
            this.cboTaxFormType.AutoSize = false;
            this.cboTaxFormType.DisplayMember = "Description";
            this.cboTaxFormType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboTaxFormType.LabelText = null;
            this.cboTaxFormType.Location = new System.Drawing.Point(211, 211);
            this.cboTaxFormType.Name = "cboTaxFormType";
            this.cboTaxFormType.Size = new System.Drawing.Size(130, 40);
            this.cboTaxFormType.TabIndex = 1003;
            this.cboTaxFormType.ValueMember = "ID";
            // 
            // frmEditReportSetup
            // 
            this.AcceptButton = this.cmdPrint;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(667, 396);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditReportSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Print Edit Report";
            this.Load += new System.EventHandler(this.frmEditReportSetup_Load);
            this.Activated += new System.EventHandler(this.frmEditReportSetup_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditReportSetup_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion
        public FCLabel fcLabel1;
        private ComboBox cboTaxFormType;
    }
}
