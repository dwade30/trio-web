﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmRevView.
	/// </summary>
	public partial class frmRevView : BaseForm
	{
		private bool expandedCollapsedProgramatically = false;
		private bool disableExpandCollapseOnClick = false;

		public frmRevView()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRevView InstancePtr
		{
			get
			{
				return (frmRevView)Sys.GetInstance(typeof(frmRevView));
			}
		}

		protected frmRevView _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// vbPorter upgrade warning: DivLength As short --> As int	OnWrite(string)
		int DivLength;
		// holds division length
		int DeptLength;
		// holds department length
		int RevLength;
		int RevNode;
		int DeptNode;
		int DivNode;
		// vbPorter upgrade warning: ExpDivLength As short --> As int	OnWrite(string)
		int ExpDivLength;
		string ExpDivision;
		string tempDivision = "";
		clsDRWrapper rs = new clsDRWrapper();
		clsDRWrapper rs2 = new clsDRWrapper();
		clsDRWrapper rs3 = new clsDRWrapper();

		private void frmRevView_Activated(object sender, System.EventArgs e)
		{
			int temp;
			int temp2;
			int temp3;
			int temp4;
			string tempDivision;
			if (modGlobal.FormExist(this))
			{
				return;
			}
			// set global settings
			vs1.GridLines = FCGrid.GridStyleSettings.flexGridNone;
			vs1.ScrollTrack = true;
			vs1.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
			//vs1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vs1.BackColorBkg = Color.White;
			vs1.SheetBorder = Color.White;
			// make it look good
			//vs1.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			//vs1.AutoSize(0);
			vs1.ColWidth(0, FCConvert.ToInt32((vs1.WidthOriginal * 0.95)));
			vs1.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			DivLength = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2));
			ExpDivLength = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2));
			if (DivLength == 0)
			{
				DeptNode = 1;
				RevNode = 2;
			}
			else
			{
				RevNode = 3;
				DivNode = 2;
				DeptNode = 1;
			}
			tempDivision = modValidateAccount.GetFormat("0", ref DivLength);
			ExpDivision = modValidateAccount.GetFormat("0", ref ExpDivLength);
			if (modAccountTitle.Statics.RevDivFlag)
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + ExpDivision + "'");
			}
			else
			{
				rs.OpenRecordset("SELECT * FROM DeptDivTitles");
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				vs1.Rows = rs.RecordCount();
				rs.OpenRecordset("SELECT * FROM RevTitles");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rs.MoveLast();
					rs.MoveFirst();
					vs1.Rows += rs.RecordCount();
				}
			}
			else
			{
				vs1.Rows = 1;
			}
			vs1.Visible = true;
			temp2 = 0;
			rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Division = '" + ExpDivision + "' ORDER BY Department");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				for (temp = 1; temp <= rs.RecordCount(); temp++)
				{
					//vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, temp2, 0, 0x80000017);
					vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, temp2, 0, 0xC0FFFF);
					vs1.TextMatrix(temp2, 0, rs.Get_Fields_String("Department") + " - " + rs.Get_Fields_String("ShortDescription"));
					vs1.RowOutlineLevel(temp2, FCConvert.ToInt16(DeptNode));
					vs1.IsSubtotal(temp2, true);
					temp2 += 1;
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						rs2.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rs.Get_Fields_String("Department") + "' AND NOT Division = '" + ExpDivision + "' ORDER BY Division");
						if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
						{
							rs2.MoveLast();
							rs2.MoveFirst();
							for (temp3 = 1; temp3 <= rs2.RecordCount(); temp3++)
							{
								vs1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, temp2, 0, 0xFF0000);
								vs1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, temp2, 0, 0xC0C0C0);
								vs1.IsSubtotal(temp2, true);
								vs1.TextMatrix(temp2, 0, rs2.Get_Fields_String("Division") + " - " + rs2.Get_Fields_String("ShortDescription"));
								vs1.RowOutlineLevel(temp2, FCConvert.ToInt16(DivNode));
								temp2 += 1;
								rs3.OpenRecordset("SELECT * FROM RevTitles WHERE Division = '" + rs2.Get_Fields_String("Division") + "' AND Department = '" + rs.Get_Fields_String("Department") + "' ORDER BY Revenue");
								if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
								{
									rs3.MoveLast();
									rs3.MoveFirst();
									for (temp4 = 1; temp4 <= rs3.RecordCount(); temp4++)
									{
										vs1.TextMatrix(temp2, 0, rs3.Get_Fields_String("Revenue") + " - " + rs3.Get_Fields_String("ShortDescription"));
										vs1.RowOutlineLevel(temp2, FCConvert.ToInt16(RevNode));
										temp2 += 1;
										rs3.MoveNext();
									}
								}
								rs2.MoveNext();
							}
						}
					}
					else
					{
						rs3.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rs.Get_Fields_String("Department") + "' ORDER BY Revenue");
						if (rs3.EndOfFile() != true && rs3.BeginningOfFile() != true)
						{
							rs3.MoveLast();
							rs3.MoveFirst();
							for (temp4 = 1; temp4 <= rs3.RecordCount(); temp4++)
							{
								vs1.TextMatrix(temp2, 0, rs3.Get_Fields_String("Revenue") + " - " + rs3.Get_Fields_String("ShortDescription"));
								vs1.RowOutlineLevel(temp2, FCConvert.ToInt16(RevNode));
								temp2 += 1;
								rs3.MoveNext();
							}
						}
					}
					rs.MoveNext();
				}
			}
			for (temp = 0; temp <= temp2 - 1; temp++)
			{
				if (vs1.RowOutlineLevel(temp) == DivNode)
				{
					expandedCollapsedProgramatically = true;
					vs1.IsCollapsed(temp, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					expandedCollapsedProgramatically = false;
				}
			}
			for (temp = 0; temp <= temp2 - 1; temp++)
			{
				if (vs1.RowOutlineLevel(temp) == DeptNode)
				{
					expandedCollapsedProgramatically = true;
					vs1.IsCollapsed(temp, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					expandedCollapsedProgramatically = false;
				}
			}
			vs1_Collapsed();
            modColorScheme.ColorGrid(vs1);
			this.Refresh();
		}

		private void frmRevView_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRevView.FillStyle	= 0;
			//frmRevView.ScaleWidth	= 5880;
			//frmRevView.ScaleHeight	= 4725;
			//frmRevView.LinkTopic	= "Form2";
			//frmRevView.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetCustomFormColors();
		}

		private void frmRevView_Resize(object sender, System.EventArgs e)
		{
			vs1_Collapsed();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//FC:FINAL:BBE:#649 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
			//frmRevenueRanges.InstancePtr.Show(App.MainForm);
		}

		//FC:FINAL:BBE:#649 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			base.OnFormClosed(e);
			frmRevenueRanges.InstancePtr.Show(App.MainForm);
		}

		private void frmRevView_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vs1_ClickEvent(object sender, System.EventArgs e)
		{
			if (disableExpandCollapseOnClick)
			{
				disableExpandCollapseOnClick = false;
				return;
			}
			if (vs1.IsCollapsed(vs1.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
			{
				expandedCollapsedProgramatically = true;
				vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
				expandedCollapsedProgramatically = false;
			}
			else
			{
				expandedCollapsedProgramatically = true;
				vs1.IsCollapsed(vs1.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				expandedCollapsedProgramatically = false;
			}
		}

		private void vs1_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			if (!expandedCollapsedProgramatically)
			{
				disableExpandCollapseOnClick = true;
			}
			vs1_Collapsed();
		}

		private void vs1_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			if (!expandedCollapsedProgramatically)
			{
				disableExpandCollapseOnClick = true;
			}
			vs1_Collapsed();
		}

		private void vs1_Collapsed()
		{
			int temp;
			int counter;
			int rows = 0;
			int height;
			bool DeptFlag = false;
			bool DivisionFlag = false;
			for (counter = 0; counter <= vs1.Rows - 1; counter++)
			{
				if (vs1.RowOutlineLevel(counter) == DeptNode)
				{
					if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						DeptFlag = true;
					}
					else
					{
						rows += 1;
						DeptFlag = false;
					}
				}
				else if (vs1.RowOutlineLevel(counter) == DivNode)
				{
					if (DeptFlag == true)
					{
						// do nothing
					}
					else if (vs1.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						rows += 1;
						DivisionFlag = true;
					}
					else
					{
						rows += 1;
						DivisionFlag = false;
					}
				}
				else
				{
					if (DeptFlag == true || DivisionFlag == true)
					{
						// do nothing
					}
					else
					{
						rows += 1;
					}
				}
			}
			//if (rows <= 10)
			//{
			//    vs1.Height = (rows * vs1.RowHeight(0)) + 75;
			//}
			//else
			//{
			//    vs1.Height = (10 * vs1.RowHeight(0)) + 75;
			//}
		}

		private void SetCustomFormColors()
		{
			lblDept.BackColor = ColorTranslator.FromOle(0xC0FFFF);
			lblDiv.BackColor = ColorTranslator.FromOle(0xC0C0C0);
			lblRev.BackColor = Color.White;
			Label2.ForeColor = Color.Blue;
		}
	}
}
