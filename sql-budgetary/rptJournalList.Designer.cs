﻿namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptJournalList.
	/// </summary>
	partial class rptJournalList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptJournalList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDeptDiv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDeptRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldJournal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpense = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRevenue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLedger = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncumbrance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpControl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRevControl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncControl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncWrong = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRevWrong = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpWrong = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOB = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRevTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLedgerTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCashTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldJournalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOOBCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldExpErrorCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRevErrorCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEncErrorCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.subOFDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.subSchoolOFDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpense)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevenue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLedger)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncWrong)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevWrong)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpWrong)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLedgerTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOOBCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpErrorCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevErrorCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncErrorCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldJournal,
				this.fldDate,
				this.fldType,
				this.fldPeriod,
				this.fldExpense,
				this.fldRevenue,
				this.fldLedger,
				this.fldCash,
				this.fldEncumbrance,
				this.fldDescription,
				this.fldExpControl,
				this.fldRevControl,
				this.fldEncControl,
				this.fldEncWrong,
				this.fldRevWrong,
				this.fldExpWrong,
				this.fldOB,
				this.fldOF
			});
			this.Detail.Height = 0.34375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblDateRange,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.Field13,
				this.Line1,
				this.Field26,
				this.lblDeptDiv,
				this.Field28,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field33,
				this.Field34,
				this.Field35,
				this.lblDeptRange
			});
			this.PageHeader.Height = 0.9791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.Field36,
				this.fldExpTotal,
				this.fldRevTotal,
				this.fldLedgerTotal,
				this.fldCashTotal,
				this.fldEncTotal,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.fldJournalCount,
				this.fldOOBCount,
				this.fldExpErrorCount,
				this.fldRevErrorCount,
				this.fldEncErrorCount,
				this.Label13,
				this.subOFDetail,
				this.subSchoolOFDetail
			});
			this.GroupFooter1.Height = 1.5F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Journal Summary List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.1875F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.375F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label5";
			this.lblDateRange.Top = 0.40625F;
			this.lblDateRange.Width = 5.4375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 0.59375F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field13.Tag = " ";
			this.Field13.Text = "Date";
			this.Field13.Top = 0.78125F;
			this.Field13.Width = 0.53125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.96875F;
			this.Line1.Width = 7.96875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.96875F;
			this.Line1.Y1 = 0.96875F;
			this.Line1.Y2 = 0.96875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 1.15625F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field26.Tag = " ";
			this.Field26.Text = "Type";
			this.Field26.Top = 0.78125F;
			this.Field26.Width = 0.34375F;
			// 
			// lblDeptDiv
			// 
			this.lblDeptDiv.Height = 0.1875F;
			this.lblDeptDiv.Left = 0.25F;
			this.lblDeptDiv.Name = "lblDeptDiv";
			this.lblDeptDiv.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDeptDiv.Tag = " ";
			this.lblDeptDiv.Text = "Jrnl";
			this.lblDeptDiv.Top = 0.78125F;
			this.lblDeptDiv.Width = 0.34375F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 1.53125F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Field28.Tag = " ";
			this.Field28.Text = "Per";
			this.Field28.Top = 0.78125F;
			this.Field28.Width = 0.25F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 1.8125F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field29.Tag = " ";
			this.Field29.Text = "Expense";
			this.Field29.Top = 0.78125F;
			this.Field29.Width = 0.84375F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 2.78125F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field30.Tag = " ";
			this.Field30.Text = "Revenue";
			this.Field30.Top = 0.78125F;
			this.Field30.Width = 0.84375F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 3.78125F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field31.Tag = " ";
			this.Field31.Text = "G / L";
			this.Field31.Top = 0.78125F;
			this.Field31.Width = 0.84375F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 4.65625F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field33.Tag = " ";
			this.Field33.Text = "Cash";
			this.Field33.Top = 0.78125F;
			this.Field33.Width = 0.84375F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 5.53125F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Field34.Tag = " ";
			this.Field34.Text = "Enc";
			this.Field34.Top = 0.78125F;
			this.Field34.Width = 0.75F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 6.53125F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Field35.Tag = " ";
			this.Field35.Text = "Description";
			this.Field35.Top = 0.78125F;
			this.Field35.Width = 1.46875F;
			// 
			// lblDeptRange
			// 
			this.lblDeptRange.Height = 0.1875F;
			this.lblDeptRange.Left = 1.5F;
			this.lblDeptRange.Name = "lblDeptRange";
			this.lblDeptRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDeptRange.Text = "Field37";
			this.lblDeptRange.Top = 0.21875F;
			this.lblDeptRange.Width = 5.1875F;
			// 
			// fldJournal
			// 
			this.fldJournal.CanGrow = false;
			this.fldJournal.CanShrink = true;
			this.fldJournal.Height = 0.19F;
			this.fldJournal.Left = 0.25F;
			this.fldJournal.Name = "fldJournal";
			this.fldJournal.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldJournal.Text = "Field36";
			this.fldJournal.Top = 0F;
			this.fldJournal.Width = 0.34375F;
			// 
			// fldDate
			// 
			this.fldDate.CanGrow = false;
			this.fldDate.CanShrink = true;
			this.fldDate.Height = 0.19F;
			this.fldDate.Left = 0.59375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = "Field36";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.53125F;
			// 
			// fldType
			// 
			this.fldType.CanGrow = false;
			this.fldType.CanShrink = true;
			this.fldType.Height = 0.19F;
			this.fldType.Left = 1.15625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "Field36";
			this.fldType.Top = 0F;
			this.fldType.Width = 0.34375F;
			// 
			// fldPeriod
			// 
			this.fldPeriod.CanGrow = false;
			this.fldPeriod.CanShrink = true;
			this.fldPeriod.Height = 0.19F;
			this.fldPeriod.Left = 1.53125F;
			this.fldPeriod.Name = "fldPeriod";
			this.fldPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPeriod.Text = "Field36";
			this.fldPeriod.Top = 0F;
			this.fldPeriod.Width = 0.25F;
			// 
			// fldExpense
			// 
			this.fldExpense.CanGrow = false;
			this.fldExpense.CanShrink = true;
			this.fldExpense.Height = 0.19F;
			this.fldExpense.Left = 1.8125F;
			this.fldExpense.Name = "fldExpense";
			this.fldExpense.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpense.Text = "Field36";
			this.fldExpense.Top = 0F;
			this.fldExpense.Width = 0.84375F;
			// 
			// fldRevenue
			// 
			this.fldRevenue.CanGrow = false;
			this.fldRevenue.CanShrink = true;
			this.fldRevenue.Height = 0.19F;
			this.fldRevenue.Left = 2.8125F;
			this.fldRevenue.Name = "fldRevenue";
			this.fldRevenue.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldRevenue.Text = "Field36";
			this.fldRevenue.Top = 0F;
			this.fldRevenue.Width = 0.84375F;
			// 
			// fldLedger
			// 
			this.fldLedger.CanGrow = false;
			this.fldLedger.CanShrink = true;
			this.fldLedger.Height = 0.19F;
			this.fldLedger.Left = 3.78125F;
			this.fldLedger.Name = "fldLedger";
			this.fldLedger.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldLedger.Text = "Field36";
			this.fldLedger.Top = 0F;
			this.fldLedger.Width = 0.84375F;
			// 
			// fldCash
			// 
			this.fldCash.CanGrow = false;
			this.fldCash.CanShrink = true;
			this.fldCash.Height = 0.19F;
			this.fldCash.Left = 4.65625F;
			this.fldCash.Name = "fldCash";
			this.fldCash.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCash.Text = "Field36";
			this.fldCash.Top = 0F;
			this.fldCash.Width = 0.84375F;
			// 
			// fldEncumbrance
			// 
			this.fldEncumbrance.CanGrow = false;
			this.fldEncumbrance.CanShrink = true;
			this.fldEncumbrance.Height = 0.19F;
			this.fldEncumbrance.Left = 5.53125F;
			this.fldEncumbrance.Name = "fldEncumbrance";
			this.fldEncumbrance.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldEncumbrance.Text = "Field36";
			this.fldEncumbrance.Top = 0F;
			this.fldEncumbrance.Width = 0.84375F;
			// 
			// fldDescription
			// 
			this.fldDescription.CanGrow = false;
			this.fldDescription.CanShrink = true;
			this.fldDescription.Height = 0.19F;
			this.fldDescription.Left = 6.53125F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldDescription.Text = "Field36";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 1.46875F;
			// 
			// fldExpControl
			// 
			this.fldExpControl.CanGrow = false;
			this.fldExpControl.CanShrink = true;
			this.fldExpControl.Height = 0.19F;
			this.fldExpControl.Left = 1.8125F;
			this.fldExpControl.Name = "fldExpControl";
			this.fldExpControl.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpControl.Text = "Field36";
			this.fldExpControl.Top = 0.15625F;
			this.fldExpControl.Width = 0.84375F;
			// 
			// fldRevControl
			// 
			this.fldRevControl.CanGrow = false;
			this.fldRevControl.CanShrink = true;
			this.fldRevControl.Height = 0.19F;
			this.fldRevControl.Left = 2.8125F;
			this.fldRevControl.Name = "fldRevControl";
			this.fldRevControl.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldRevControl.Text = "Field36";
			this.fldRevControl.Top = 0.15625F;
			this.fldRevControl.Width = 0.84375F;
			// 
			// fldEncControl
			// 
			this.fldEncControl.CanGrow = false;
			this.fldEncControl.CanShrink = true;
			this.fldEncControl.Height = 0.19F;
			this.fldEncControl.Left = 5.53125F;
			this.fldEncControl.Name = "fldEncControl";
			this.fldEncControl.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldEncControl.Text = "Field36";
			this.fldEncControl.Top = 0.15625F;
			this.fldEncControl.Width = 0.84375F;
			// 
			// fldEncWrong
			// 
			this.fldEncWrong.CanGrow = false;
			this.fldEncWrong.Height = 0.19F;
			this.fldEncWrong.Left = 6.375F;
			this.fldEncWrong.Name = "fldEncWrong";
			this.fldEncWrong.Text = "*";
			this.fldEncWrong.Top = 0F;
			this.fldEncWrong.Width = 0.09375F;
			// 
			// fldRevWrong
			// 
			this.fldRevWrong.CanGrow = false;
			this.fldRevWrong.Height = 0.19F;
			this.fldRevWrong.Left = 3.65625F;
			this.fldRevWrong.Name = "fldRevWrong";
			this.fldRevWrong.Text = "*";
			this.fldRevWrong.Top = 0F;
			this.fldRevWrong.Width = 0.09375F;
			// 
			// fldExpWrong
			// 
			this.fldExpWrong.CanGrow = false;
			this.fldExpWrong.Height = 0.19F;
			this.fldExpWrong.Left = 2.65625F;
			this.fldExpWrong.Name = "fldExpWrong";
			this.fldExpWrong.Text = "*";
			this.fldExpWrong.Top = 0F;
			this.fldExpWrong.Width = 0.09375F;
			// 
			// fldOB
			// 
			this.fldOB.CanGrow = false;
			this.fldOB.Height = 0.19F;
			this.fldOB.Left = 0F;
			this.fldOB.Name = "fldOB";
			this.fldOB.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldOB.Text = "OB";
			this.fldOB.Top = 0F;
			this.fldOB.Width = 0.21875F;
			// 
			// fldOF
			// 
			this.fldOF.CanGrow = false;
			this.fldOF.Height = 0.19F;
			this.fldOF.Left = 0F;
			this.fldOF.Name = "fldOF";
			this.fldOF.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.fldOF.Text = "OF";
			this.fldOF.Top = 0F;
			this.fldOF.Width = 0.21875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.8125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.03125F;
			this.Line2.Width = 4.59375F;
			this.Line2.X1 = 1.8125F;
			this.Line2.X2 = 6.40625F;
			this.Line2.Y1 = 0.03125F;
			this.Line2.Y2 = 0.03125F;
			// 
			// Field36
			// 
			this.Field36.Height = 0.1875F;
			this.Field36.Left = 1.09375F;
			this.Field36.Name = "Field36";
			this.Field36.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: center; ddo" + "-char-set: 1";
			this.Field36.Text = "Totals";
			this.Field36.Top = 0.0625F;
			this.Field36.Width = 0.5625F;
			// 
			// fldExpTotal
			// 
			this.fldExpTotal.Height = 0.1875F;
			this.fldExpTotal.Left = 1.71875F;
			this.fldExpTotal.Name = "fldExpTotal";
			this.fldExpTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldExpTotal.Text = "Field36";
			this.fldExpTotal.Top = 0.0625F;
			this.fldExpTotal.Width = 0.9375F;
			// 
			// fldRevTotal
			// 
			this.fldRevTotal.Height = 0.1875F;
			this.fldRevTotal.Left = 2.71875F;
			this.fldRevTotal.Name = "fldRevTotal";
			this.fldRevTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldRevTotal.Text = "Field36";
			this.fldRevTotal.Top = 0.0625F;
			this.fldRevTotal.Width = 0.9375F;
			// 
			// fldLedgerTotal
			// 
			this.fldLedgerTotal.Height = 0.1875F;
			this.fldLedgerTotal.Left = 3.6875F;
			this.fldLedgerTotal.Name = "fldLedgerTotal";
			this.fldLedgerTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldLedgerTotal.Text = "Field36";
			this.fldLedgerTotal.Top = 0.0625F;
			this.fldLedgerTotal.Width = 0.9375F;
			// 
			// fldCashTotal
			// 
			this.fldCashTotal.Height = 0.1875F;
			this.fldCashTotal.Left = 4.65625F;
			this.fldCashTotal.Name = "fldCashTotal";
			this.fldCashTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCashTotal.Text = "Field36";
			this.fldCashTotal.Top = 0.0625F;
			this.fldCashTotal.Width = 0.84375F;
			// 
			// fldEncTotal
			// 
			this.fldEncTotal.Height = 0.1875F;
			this.fldEncTotal.Left = 5.53125F;
			this.fldEncTotal.Name = "fldEncTotal";
			this.fldEncTotal.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldEncTotal.Text = "Field36";
			this.fldEncTotal.Top = 0.0625F;
			this.fldEncTotal.Width = 0.84375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.19F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 3.34375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label8.Text = "Journals Listed";
			this.Label8.Top = 0.34375F;
			this.Label8.Width = 0.90625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.19F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.34375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label9.Text = "Out of Balance";
			this.Label9.Top = 0.5F;
			this.Label9.Width = 0.90625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.19F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.34375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label10.Text = "Expense Control Errors";
			this.Label10.Top = 0.65625F;
			this.Label10.Width = 1.46875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.19F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.34375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label11.Text = "Revenue Control Errors";
			this.Label11.Top = 0.8125F;
			this.Label11.Width = 1.46875F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.19F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.34375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.Label12.Text = "Encumbrance Control Errors";
			this.Label12.Top = 0.96875F;
			this.Label12.Width = 1.6875F;
			// 
			// fldJournalCount
			// 
			this.fldJournalCount.Height = 0.19F;
			this.fldJournalCount.Left = 2.53125F;
			this.fldJournalCount.Name = "fldJournalCount";
			this.fldJournalCount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldJournalCount.Text = "Field36";
			this.fldJournalCount.Top = 0.34375F;
			this.fldJournalCount.Width = 0.75F;
			// 
			// fldOOBCount
			// 
			this.fldOOBCount.Height = 0.19F;
			this.fldOOBCount.Left = 2.53125F;
			this.fldOOBCount.Name = "fldOOBCount";
			this.fldOOBCount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldOOBCount.Text = "Field36";
			this.fldOOBCount.Top = 0.5F;
			this.fldOOBCount.Width = 0.75F;
			// 
			// fldExpErrorCount
			// 
			this.fldExpErrorCount.Height = 0.19F;
			this.fldExpErrorCount.Left = 2.53125F;
			this.fldExpErrorCount.Name = "fldExpErrorCount";
			this.fldExpErrorCount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldExpErrorCount.Text = "Field36";
			this.fldExpErrorCount.Top = 0.65625F;
			this.fldExpErrorCount.Width = 0.75F;
			// 
			// fldRevErrorCount
			// 
			this.fldRevErrorCount.Height = 0.19F;
			this.fldRevErrorCount.Left = 2.53125F;
			this.fldRevErrorCount.Name = "fldRevErrorCount";
			this.fldRevErrorCount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldRevErrorCount.Text = "Field36";
			this.fldRevErrorCount.Top = 0.8125F;
			this.fldRevErrorCount.Width = 0.75F;
			// 
			// fldEncErrorCount
			// 
			this.fldEncErrorCount.Height = 0.19F;
			this.fldEncErrorCount.Left = 2.53125F;
			this.fldEncErrorCount.Name = "fldEncErrorCount";
			this.fldEncErrorCount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldEncErrorCount.Text = "Field36";
			this.fldEncErrorCount.Top = 0.96875F;
			this.fldEncErrorCount.Width = 0.75F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.46875F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "";
			this.Label13.Text = "* - Incorrect control entry";
			this.Label13.Top = 0.375F;
			this.Label13.Width = 1.75F;
			// 
			// subOFDetail
			// 
			this.subOFDetail.CloseBorder = false;
			this.subOFDetail.Height = 0.125F;
			this.subOFDetail.Left = 0F;
			this.subOFDetail.Name = "subOFDetail";
			this.subOFDetail.Report = null;
			this.subOFDetail.Top = 1.21875F;
			this.subOFDetail.Width = 8F;
			// 
			// subSchoolOFDetail
			// 
			this.subSchoolOFDetail.CloseBorder = false;
			this.subSchoolOFDetail.Height = 0.125F;
			this.subSchoolOFDetail.Left = 0F;
			this.subSchoolOFDetail.Name = "subSchoolOFDetail";
			this.subSchoolOFDetail.Report = null;
			this.subSchoolOFDetail.Top = 1.375F;
			this.subSchoolOFDetail.Width = 8F;
			// 
			// rptJournalList
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			//this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptDiv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDeptRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpense)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevenue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLedger)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncumbrance)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncWrong)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevWrong)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpWrong)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLedgerTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCashTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldJournalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOOBCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldExpErrorCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRevErrorCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEncErrorCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpense;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRevenue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLedger;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncumbrance;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpControl;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRevControl;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncControl;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncWrong;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRevWrong;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpWrong;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOF;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDeptDiv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblDeptRange;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field36;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRevTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLedgerTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCashTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldJournalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOOBCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExpErrorCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRevErrorCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncErrorCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subOFDetail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subSchoolOFDetail;
	}
}
