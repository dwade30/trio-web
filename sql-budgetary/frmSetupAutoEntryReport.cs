﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;
using GrapeCity.ActiveReports;
using System;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmSetupAutoEntryReport.
	/// </summary>
	public partial class frmSetupAutoEntryReport : BaseForm
	{
		public frmSetupAutoEntryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupAutoEntryReport InstancePtr
		{
			get
			{
				return (frmSetupAutoEntryReport)Sys.GetInstance(typeof(frmSetupAutoEntryReport));
			}
		}

		protected frmSetupAutoEntryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmSetupAutoEntryReport_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAutoEntryReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAutoEntryReport.FillStyle	= 0;
			//frmSetupAutoEntryReport.ScaleWidth	= 3885;
			//frmSetupAutoEntryReport.ScaleHeight	= 1995;
			//frmSetupAutoEntryReport.LinkTopic	= "Form2";
			//frmSetupAutoEntryReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupAutoEntryReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (!Information.IsDate(txtLowDate.Text))
			{
				MessageBox.Show("You must enter a valid start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLowDate.Focus();
				return;
			}
			if (!Information.IsDate(txtHighDate.Text))
			{
				MessageBox.Show("You must enter a valid end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtHighDate.Focus();
				return;
			}
			if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must be befroe your ending date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLowDate.Focus();
				return;
			}
			rptCheckFileList.InstancePtr.strTransactions = "A";
			//FC:FINAL:AM:FC-8697
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptCheckFileList.InstancePtr);
			//this.Hide();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (!Information.IsDate(txtLowDate.Text))
			{
				MessageBox.Show("You must enter a valid start date before you may continue.", "Invalid Start Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLowDate.Focus();
				return;
			}
			if (!Information.IsDate(txtHighDate.Text))
			{
				MessageBox.Show("You must enter a valid end date before you may continue.", "Invalid End Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtHighDate.Focus();
				return;
			}
			if (DateAndTime.DateValue(txtLowDate.Text).ToOADate() > DateAndTime.DateValue(txtHighDate.Text).ToOADate())
			{
				MessageBox.Show("Your start date must be befroe your ending date.", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtLowDate.Focus();
				return;
			}
			rptCheckFileList.InstancePtr.strTransactions = "A";
			rptCheckFileList.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
