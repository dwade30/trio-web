﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for rptMultipleJournals.
	/// </summary>
	public partial class rptMultipleJournals : BaseSectionReport
	{
		public static rptMultipleJournals InstancePtr
		{
			get
			{
				return (rptMultipleJournals)Sys.GetInstance(typeof(rptMultipleJournals));
			}
		}

		protected rptMultipleJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMultipleJournals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		public string strReportType = string.Empty;
		clsDRWrapper rsInfo = new clsDRWrapper();
		public string strSQL = string.Empty;
		public bool blnShowLiquidatedEncActivity;

		public rptMultipleJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public void Init(string strTypeOfReport, bool boolShowLiquidatedEncumbranceActivity, string strSQLToUse)
		{
			strSQL = strSQLToUse;
			strReportType = strTypeOfReport;
			blnShowLiquidatedEncActivity = boolShowLiquidatedEncumbranceActivity;
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// Unload frmMultipleJournalSelection
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			if (strReportType == "P")
			{
				lblTitle.Text = "Posted Journal Report";
				frmReportViewer.InstancePtr.Text = "Posted Journal Report";
				this.Document.Name = "Posted Journal Report";
			}
			else
			{
				lblTitle.Text = "Unposted Journal Report";
				frmReportViewer.InstancePtr.Text = "Unposted Journal Report";
				this.Document.Name = "Unposted Journal Report";
			}
			rsInfo.OpenRecordset(strSQL);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsInfo.MoveNext();
			if (rsInfo.EndOfFile() != true)
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			}
			else
			{
				Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			}
			rsInfo.MovePrevious();
			// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			this.UserData = rsInfo.Get_Fields("JournalNumber");
			if (strReportType == "P")
			{
				subJournalInfo.Report = new rptPostedJournal();
			}
			else
			{
				subJournalInfo.Report = new rptUnpostedJournal();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		

		private void rptMultipleJournals_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
