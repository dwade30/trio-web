﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using SharedApplication.Extensions;
namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmUpdatePreviousSelectYear.
	/// </summary>
	public partial class frmUpdatePreviousSelectYear : BaseForm
	{
		public frmUpdatePreviousSelectYear()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdatePreviousSelectYear InstancePtr
		{
			get
			{
				return (frmUpdatePreviousSelectYear)Sys.GetInstance(typeof(frmUpdatePreviousSelectYear));
			}
		}

		protected frmUpdatePreviousSelectYear _InstancePtr = null;
		int intYear;
		string strReturn;
		clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
		clsDRWrapper rsYTDActivity = new clsDRWrapper();
		clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
		clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
		clsDRWrapper rsActivityDetail = new clsDRWrapper();
		clsDRWrapper rsLedgerActivityDetail = new clsDRWrapper();
		clsDRWrapper rsInfo = new clsDRWrapper();

		private void frmUpdatePreviousSelectYear_Activated(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboYear.Clear();
			rsTemp.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				do
				{
					cboYear.AddItem(FCConvert.ToString(rsTemp.Get_Fields_Int32("ArchiveID")));
					rsTemp.MoveNext();
				}
				while (rsTemp.EndOfFile() != true);
			}
			if (cboYear.Items.Count > 0)
			{
				cboYear.SelectedIndex = 0;
			}
			cboYear.Focus();
			this.Refresh();
		}

		private void frmUpdatePreviousSelectYear_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmUpdatePreviousSelectYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			string strCurDir = "";
			int ans;
			string strArchivePath;
			clsDRWrapper rsPastInfo = new clsDRWrapper();
			string strAccount = "";
			Decimal curYTDAmount;
			Decimal curRevCtrlYTD;
			Decimal curExpCtrlYTD;
			Decimal curFundBalYTD;
			int counter;
			strReturn = cboYear.Text;
			if (Strings.Trim(strReturn) == "")
			{
				intYear = 0;
			}
			else
			{
				intYear = FCConvert.ToInt16(FCConvert.ToDouble(strReturn));
			}
			if (intYear == 0)
			{
				return;
			}
			else if (Strings.Trim(Conversion.Str(intYear)).Length != 4)
			{
				MessageBox.Show("You must input a four digit year before you may continue.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strArchivePath = "Archive_" + FCConvert.ToString(intYear);
			rsInfo.GroupName = strArchivePath;
			// troges126
			modBudgetaryAccounting.CalculateAccountInfo(strArchivePath);

			rsYTDActivity.GroupName = strArchivePath;
			rsLedgerYTDActivity.GroupName = strArchivePath;
			rsRevYTDActivity.GroupName = strArchivePath;
			rsActivityDetail.GroupName = strArchivePath;
			rsLedgerActivityDetail.GroupName = strArchivePath;
			rsRevActivityDetail.GroupName = strArchivePath;
			rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account");
			rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
			rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account");
			rsActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account, Period");
			rsLedgerActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account, Period");
			rsRevActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account, Period");
			rsPastInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Year = " + cboYear.Text + " ORDER BY Account");
			rsInfo.OpenRecordset("SELECT * FROM AccountMaster");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					try
					{
				
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strAccount = rsInfo.Get_Fields_String("Account");
						
						bool executeUpdateRecord = false;
						if (Strings.Left(strAccount, 1) == "G")
						{
							if (modBudgetaryAccounting.GetAccount(strAccount) == modBudgetaryAccounting.Statics.strExpCtrAccount)
							{
								// curTownExpCtrlYTD = GetYTDNet(strAccount)
							}
							else if (modBudgetaryAccounting.GetAccount(strAccount) == modBudgetaryAccounting.Statics.strRevCtrAccount)
							{
								// curTownRevCtrlYTD = GetYTDNet(strAccount)
							}
							else if (modBudgetaryAccounting.GetAccount(strAccount) == modBudgetaryAccounting.Statics.strFundBalCtrAccount)
							{
								// curTownFundBalYTD = GetYTDNet(strAccount)
							}
							else
							{
								executeUpdateRecord = true;
								goto UpdateRecord;
							}
						}
						else
						{
							executeUpdateRecord = true;
							goto UpdateRecord;
						}
					UpdateRecord:
						;
						if (executeUpdateRecord)
						{
							if (rsPastInfo.FindFirstRecord("Account", strAccount))
							{
								rsPastInfo.Edit();
								curYTDAmount = GetYTDNet(strAccount).ToDecimal();// FCConvert.ToDecimal(GetYTDNet(ref strAccount));
								if (curYTDAmount != (rsPastInfo.Get_Fields_Decimal("ActualSpent") + rsPastInfo.Get_Fields_Decimal("YTDAdjustments")))
								{
									rsPastInfo.Set_Fields("YTDAdjustments", curYTDAmount - rsPastInfo.Get_Fields_Decimal("ActualSpent"));
									if (modBudgetaryMaster.Statics.FirstMonth == 7)
									{
										rsPastInfo.Set_Fields("JuneActual", (rsPastInfo.Get_Fields_Decimal("ActualSpent") - rsPastInfo.Get_Fields_Decimal("JanActual") - rsPastInfo.Get_Fields_Decimal("FebActual") - rsPastInfo.Get_Fields_Decimal("MarActual") - rsPastInfo.Get_Fields_Decimal("AprActual") - rsPastInfo.Get_Fields_Decimal("MayActual") - rsPastInfo.Get_Fields_Decimal("JulyActual") - rsPastInfo.Get_Fields_Decimal("AugActual") - rsPastInfo.Get_Fields_Decimal("SeptActual") - rsPastInfo.Get_Fields_Decimal("OctActual") - rsPastInfo.Get_Fields_Decimal("NovActual") - rsPastInfo.Get_Fields_Decimal("DecActual")) + rsPastInfo.Get_Fields_Decimal("YTDAdjustments"));
									}
									else
									{
										rsPastInfo.Set_Fields("DecActual", (rsPastInfo.Get_Fields_Decimal("ActualSpent") - rsPastInfo.Get_Fields_Decimal("JanActual") - rsPastInfo.Get_Fields_Decimal("FebActual") - rsPastInfo.Get_Fields_Decimal("MarActual") - rsPastInfo.Get_Fields_Decimal("AprActual") - rsPastInfo.Get_Fields_Decimal("MayActual") - rsPastInfo.Get_Fields_Decimal("JulyActual") - rsPastInfo.Get_Fields_Decimal("AugActual") - rsPastInfo.Get_Fields_Decimal("SeptActual") - rsPastInfo.Get_Fields_Decimal("OctActual") - rsPastInfo.Get_Fields_Decimal("NovActual") - rsPastInfo.Get_Fields_Decimal("JuneActual")) + rsPastInfo.Get_Fields_Decimal("YTDAdjustments"));
									}
								}
								curYTDAmount = GetBudgetAdjustments(ref strAccount, strArchivePath).ToDecimal();
								// TODO Get_Fields: Check the table for the column [account] and replace with corresponding Get_Field method
								if (Strings.Left(FCConvert.ToString(rsInfo.Get_Fields_String("account")), 1) == "E")
								{
									// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
									rsPastInfo.Set_Fields("EndBudget", rsInfo.Get_Fields_Decimal("CurrentBudget"));
									// rsPastInfo.Fields["startbudget"] = rsInfo.Fields["CurrentBudget"] + curYTDAmount
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
									rsPastInfo.Set_Fields("EndBudget", rsInfo.Get_Fields_Decimal("CurrentBudget"));
									// rsPastInfo.Fields["startbudget"] = rsInfo.Fields["CurrentBudget"] - curYTDAmount
								}
								if (curYTDAmount != rsPastInfo.Get_Fields_Decimal("BudgetAdjustments"))
								{
									rsPastInfo.Set_Fields("YTDBudgetAdjustments", curYTDAmount - rsPastInfo.Get_Fields_Decimal("BudgetAdjustments"));
								}
								else
								{
									rsPastInfo.Set_Fields("YTDBudgetAdjustments", 0);
								}
								curYTDAmount = FCConvert.ToDecimal(GetCarryOver(ref strAccount, strArchivePath));
								// TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
								if (curYTDAmount != rsPastInfo.Get_Fields_Decimal("CarryForward"))
								{
									// TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
									rsPastInfo.Set_Fields("YTDCarryForward", curYTDAmount - rsPastInfo.Get_Fields_Decimal("CarryForward"));
								}
								else
								{
									rsPastInfo.Set_Fields("YTDCarryForward", 0);
								}
								rsPastInfo.Update(true);
							}
							else
							{
								rsPastInfo.OmitNullsOnInsert = true;
								rsPastInfo.AddNew();
								rsPastInfo.Set_Fields("Year", cboYear.Text);
								rsPastInfo.Set_Fields("Account", strAccount);
								// TODO Get_Fields: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
								rsPastInfo.Set_Fields("StartBudget", rsInfo.Get_Fields_Decimal("CurrentBudget"));
								rsPastInfo.Set_Fields("JanBudget", rsInfo.Get_Fields_Decimal("JanBudget"));
								rsPastInfo.Set_Fields("FebBudget", rsInfo.Get_Fields_Decimal("FebBudget"));
								rsPastInfo.Set_Fields("MarBudget", rsInfo.Get_Fields_Decimal("MarBudget"));
								rsPastInfo.Set_Fields("AprBudget", rsInfo.Get_Fields_Decimal("AprBudget"));
								rsPastInfo.Set_Fields("MayBudget", rsInfo.Get_Fields_Decimal("MayBudget"));
								rsPastInfo.Set_Fields("JuneBudget", rsInfo.Get_Fields_Decimal("JuneBudget"));
								rsPastInfo.Set_Fields("JulyBudget", rsInfo.Get_Fields_Decimal("JulyBudget"));
								rsPastInfo.Set_Fields("AugBudget", rsInfo.Get_Fields_Decimal("AugBudget"));
								rsPastInfo.Set_Fields("SeptBudget", rsInfo.Get_Fields_Decimal("SeptBudget"));
								rsPastInfo.Set_Fields("OctBudget", rsInfo.Get_Fields_Decimal("OctBudget"));
								rsPastInfo.Set_Fields("NovBudget", rsInfo.Get_Fields_Decimal("NovBudget"));
								rsPastInfo.Set_Fields("DecBudget", rsInfo.Get_Fields_Decimal("DecBudget"));
								rsPastInfo.Set_Fields("JanActual", GetYTDNet_6(strAccount, 1));
								rsPastInfo.Set_Fields("FebActual", GetYTDNet_6(strAccount, 2));
								rsPastInfo.Set_Fields("MarActual", GetYTDNet_6(strAccount, 3));
								rsPastInfo.Set_Fields("AprActual", GetYTDNet_6(strAccount, 4));
								rsPastInfo.Set_Fields("MayActual", GetYTDNet_6(strAccount, 5));
								rsPastInfo.Set_Fields("JuneActual", GetYTDNet_6(strAccount, 6));
								rsPastInfo.Set_Fields("JulyActual", GetYTDNet_6(strAccount, 7));
								rsPastInfo.Set_Fields("AugActual", GetYTDNet_6(strAccount, 8));
								rsPastInfo.Set_Fields("SeptActual", GetYTDNet_6(strAccount, 9));
								rsPastInfo.Set_Fields("OctActual", GetYTDNet_6(strAccount, 10));
								rsPastInfo.Set_Fields("NovActual", GetYTDNet_6(strAccount, 11));
								rsPastInfo.Set_Fields("DecActual", GetYTDNet_6(strAccount, 12));
								rsPastInfo.Set_Fields("EndBudget", rsInfo.Get_Fields("CurrentBudget"));
								rsPastInfo.Set_Fields("YTDBudgetAdjustments", GetBudgetAdjustments(ref strAccount, strArchivePath));
								rsPastInfo.Set_Fields("YTDCarryForward", GetCarryOver(ref strAccount, strArchivePath));
								rsPastInfo.Set_Fields("YTDAdjustments", GetYTDNet( strAccount));
								rsPastInfo.Update(true);
							}
						}
					}
					catch (Exception ex)
                    {
						throw ex;
                    }
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				for (counter = 1; counter <= 99; counter++)
				{
					if (!modAccountTitle.Statics.YearFlag)
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00";
					}
					else
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount;
					}
					if (rsPastInfo.FindFirstRecord("Account", strAccount))
					{
						curExpCtrlYTD = FCConvert.ToDecimal(GetYTDNet(strAccount));
					}
					else
					{
						curExpCtrlYTD = 0;
					}
					if (!modAccountTitle.Statics.YearFlag)
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00";
					}
					else
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount;
					}
					if (rsPastInfo.FindFirstRecord("Account", strAccount))
					{
						curRevCtrlYTD = FCConvert.ToDecimal(GetYTDNet(strAccount));
					}
					else
					{
						curRevCtrlYTD = 0;
					}
					if (!modAccountTitle.Statics.YearFlag)
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00";
					}
					else
					{
						strAccount = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount;
					}
					if (rsPastInfo.FindFirstRecord("Account", strAccount))
					{
						curFundBalYTD = FCConvert.ToDecimal(GetYTDNet(strAccount));
						rsPastInfo.Edit();
						if (rsPastInfo.Get_Fields_Decimal("ActualSpent") != (curFundBalYTD + (curExpCtrlYTD + curRevCtrlYTD) + rsPastInfo.Get_Fields_Decimal("YTDAdjustments")))
						{
							rsPastInfo.Set_Fields("YTDAdjustments", (curFundBalYTD + (curExpCtrlYTD + curRevCtrlYTD)) - rsPastInfo.Get_Fields_Decimal("ActualSpent"));
							if (modBudgetaryMaster.Statics.FirstMonth == 7)
							{
								rsPastInfo.Set_Fields("JuneActual", (rsPastInfo.Get_Fields_Decimal("ActualSpent") - rsPastInfo.Get_Fields_Decimal("JanActual") - rsPastInfo.Get_Fields_Decimal("FebActual") - rsPastInfo.Get_Fields_Decimal("MarActual") - rsPastInfo.Get_Fields_Decimal("AprActual") - rsPastInfo.Get_Fields_Decimal("MayActual") - rsPastInfo.Get_Fields_Decimal("JulyActual") - rsPastInfo.Get_Fields_Decimal("AugActual") - rsPastInfo.Get_Fields_Decimal("SeptActual") - rsPastInfo.Get_Fields_Decimal("OctActual") - rsPastInfo.Get_Fields_Decimal("NovActual") - rsPastInfo.Get_Fields_Decimal("DecActual")) + rsPastInfo.Get_Fields_Decimal("YTDAdjustments"));
							}
							else
							{
								rsPastInfo.Set_Fields("DecActual", (rsPastInfo.Get_Fields_Decimal("ActualSpent") - rsPastInfo.Get_Fields_Decimal("JanActual") - rsPastInfo.Get_Fields_Decimal("FebActual") - rsPastInfo.Get_Fields_Decimal("MarActual") - rsPastInfo.Get_Fields_Decimal("AprActual") - rsPastInfo.Get_Fields_Decimal("MayActual") - rsPastInfo.Get_Fields_Decimal("JulyActual") - rsPastInfo.Get_Fields_Decimal("AugActual") - rsPastInfo.Get_Fields_Decimal("SeptActual") - rsPastInfo.Get_Fields_Decimal("OctActual") - rsPastInfo.Get_Fields_Decimal("NovActual") - rsPastInfo.Get_Fields_Decimal("JuneActual")) + rsPastInfo.Get_Fields_Decimal("YTDAdjustments"));
							}
						}
						// rsPastInfo.Fields["EndBudget"] = rsPastInfo.Fields["StartBudget"] + GetBudgetAdjustments(strAccount, strArchivePath)
						// rsPastInfo.Fields["BudgetAdjustments"] = GetBudgetAdjustments(strAccount, strArchivePath)
						// rsPastInfo.Fields["CarryForward"] = GetCarryOver(strAccount, strArchivePath)
						curYTDAmount = FCConvert.ToDecimal(GetBudgetAdjustments(ref strAccount, strArchivePath));
						if (curYTDAmount != rsPastInfo.Get_Fields_Decimal("BudgetAdjustments"))
						{
							rsPastInfo.Set_Fields("YTDBudgetAdjustments", curYTDAmount - rsPastInfo.Get_Fields_Decimal("BudgetAdjustments"));
						}
						curYTDAmount = FCConvert.ToDecimal(GetCarryOver(ref strAccount, strArchivePath));
						// TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
						if (curYTDAmount != rsPastInfo.Get_Fields_Decimal("CarryForward"))
						{
							// TODO Get_Fields: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
							rsPastInfo.Set_Fields("YTDCarryForward", curYTDAmount - rsPastInfo.Get_Fields_Decimal("CarryForward"));
						}
						rsPastInfo.Update(true);
					}
				}
			}
			frmWait.InstancePtr.Unload();
			//Application.DoEvents();
			MessageBox.Show("Update Successful", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Close();
		}

		private double GetYTDDebit( string strAcct, short intMonth = -1)
		{
			double GetYTDDebit = 0;
			if (intMonth == -1)
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
					{
						if (rsYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsYTDActivity.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
					{
						if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevYTDActivity.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
					{
						if (rsLedgerYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsLedgerYTDActivity.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			else
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
					{
						if (rsActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsActivityDetail.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevActivityDetail.EndOfFile() != true && rsRevActivityDetail.BeginningOfFile() != true)
					{
						if (rsRevActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsRevActivityDetail.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerActivityDetail.EndOfFile() != true && rsLedgerActivityDetail.BeginningOfFile() != true)
					{
						if (rsLedgerActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
							GetYTDDebit = rsLedgerActivityDetail.Get_Fields_Decimal("PostedDebitsTotal").ToDouble();
						}
						else
						{
							GetYTDDebit = 0;
						}
					}
					else
					{
						GetYTDDebit = 0;
					}
				}
			}
			return GetYTDDebit;
		}

		private double GetYTDCredit( string strAcct, short intMonth = -1)
		{
			double GetYTDCredit = 0;
			if (intMonth == -1)
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsYTDActivity.EndOfFile() != true && rsYTDActivity.BeginningOfFile() != true)
					{
						if (rsYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsYTDActivity.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevYTDActivity.EndOfFile() != true && rsRevYTDActivity.BeginningOfFile() != true)
					{
						if (rsRevYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevYTDActivity.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
					{
						if (rsLedgerYTDActivity.FindFirstRecord("Account", strAcct))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsLedgerYTDActivity.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			else
			{
				if (Strings.Left(strAcct, 1) == "E")
				{
					if (rsActivityDetail.EndOfFile() != true && rsActivityDetail.BeginningOfFile() != true)
					{
						if (rsActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsActivityDetail.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "R")
				{
					if (rsRevActivityDetail.EndOfFile() != true && rsRevActivityDetail.BeginningOfFile() != true)
					{
						if (rsRevActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsRevActivityDetail.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
				else if (Strings.Left(strAcct, 1) == "G")
				{
					if (rsLedgerActivityDetail.EndOfFile() != true && rsLedgerActivityDetail.BeginningOfFile() != true)
					{
						if (rsLedgerActivityDetail.FindFirstRecord2("Period, Account", FCConvert.ToString(intMonth) + "," + strAcct, ","))
						{
							// TODO Get_Fields: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
							GetYTDCredit = rsLedgerActivityDetail.Get_Fields_Decimal("PostedCreditsTotal").ToDouble() * -1;
						}
						else
						{
							GetYTDCredit = 0;
						}
					}
					else
					{
						GetYTDCredit = 0;
					}
				}
			}
			return GetYTDCredit;
		}

		private double GetYTDNet_6(string strAcct, short intMonth = -1)
		{
			return GetYTDNet(strAcct, intMonth);
		}

		private double GetYTDNet( string strAcct, short intMonth = -1)
		{
			double GetYTDNet = 0;
			int temp;
			if (intMonth == -1)
			{
				GetYTDNet = GetYTDDebit( strAcct) - GetYTDCredit( strAcct);
			}
			else
			{
				GetYTDNet = GetYTDDebit( strAcct, intMonth) - GetYTDCredit( strAcct, intMonth);
			}
			return GetYTDNet;
		}

		private double GetBudgetAdjustments(ref string strAcct, string strPath = "")
		{
			//double GetBudgetAdjustments = 0;
			int temp;
			int HighDate;
			int LowDate;
			int intExpStart;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			HighDate = modBudgetaryAccounting.GetBDVariable("FiscalStart").ToString().ToIntegerValue(); 
			if (HighDate == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate -= 1;
			}
			LowDate = modBudgetaryAccounting.GetBDVariable("FiscalStart").ToString().ToIntegerValue();
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rs2.GroupName = strPath;
			rs2.OpenRecordset("SELECT SUM(Amount) AS BudAdj FROM JournalEntries WHERE Account = '" + strAcct + "' AND RCB = 'B' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
			// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
			//if (!(FCConvert.ToString(rs2.Get_Fields_Decimal("BudAdj")) == ""))
			//{
				// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
				return rs2.Get_Fields_Decimal("BudAdj").ToDouble();
			//}
			//else
			//{
			//	GetBudgetAdjustments = 0;
			//}
			//return GetBudgetAdjustments;
		}

		private double GetCarryOver(ref string strAcct, string strPath = "")
		{
			double GetCarryOver = 0;
			int temp;
			int HighDate;
			int LowDate;
			int intExpStart;
			string strPeriodCheck = "";
			clsDRWrapper rs2 = new clsDRWrapper();
			HighDate = modBudgetaryAccounting.GetBDVariable("FiscalStart").ToString().ToIntegerValue();
			if (HighDate == 1)
			{
				HighDate = 12;
			}
			else
			{
				HighDate -= 1;
			}
			LowDate = modBudgetaryAccounting.GetBDVariable("FiscalStart").ToString().ToIntegerValue();
			if (LowDate > HighDate)
			{
				strPeriodCheck = "OR";
			}
			else
			{
				strPeriodCheck = "AND";
			}
			rs2.GroupName = strPath;
			rs2.OpenRecordset("SELECT SUM(Amount) AS BudAdj FROM JournalEntries WHERE Account = '" + strAcct + "' AND RCB = 'B' and CarryForward = 1 AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ")");
			// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
			//if (!(FCConvert.ToString(rs2.Get_Fields("BudAdj")) == ""))
			//{
				// TODO Get_Fields: Field [BudAdj] not found!! (maybe it is an alias?)
				return rs2.Get_Fields_Decimal("BudAdj").ToDouble();
			//}
			//else
			//{
			//	GetCarryOver = 0;
			//}
			//return GetCarryOver;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(sender, e);
		}
	}
}
