﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using GrapeCity.ActiveReports;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmPrintPurchaseOrder.
	/// </summary>
	public partial class frmPrintPurchaseOrder : BaseForm
	{
		public frmPrintPurchaseOrder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtTownAddress = new System.Collections.Generic.List<Global.T2KOverTypeBox>();
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_0, 0);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_1, 1);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_2, 2);
			this.txtTownAddress.AddControlArrayElement(txtTownAddress_3, 3);
			this.Label1.AddControlArrayElement(Label1_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrintPurchaseOrder InstancePtr
		{
			get
			{
				return (frmPrintPurchaseOrder)Sys.GetInstance(typeof(frmPrintPurchaseOrder));
			}
		}

		protected frmPrintPurchaseOrder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int KeyCol;
		int JournalCol;
		int VendorNumberCol;
		int VendorNameCol;
		int DescriptionCol;
		int POCol;

		private void frmPrintPurchaseOrder_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPrintPurchaseOrder_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintPurchaseOrder.FillStyle	= 0;
			//frmPrintPurchaseOrder.ScaleWidth	= 9045;
			//frmPrintPurchaseOrder.ScaleHeight	= 7410;
			//frmPrintPurchaseOrder.LinkTopic	= "Form2";
			//frmPrintPurchaseOrder.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtTownName.Text = modRegistry.GetRegistryKey("TownName");
			txtTownAddress[0].Text = modRegistry.GetRegistryKey("TownAddress1");
			txtTownAddress[1].Text = modRegistry.GetRegistryKey("TownAddress2");
			txtTownAddress[2].Text = modRegistry.GetRegistryKey("TownAddress3");
			txtTownAddress[3].Text = modRegistry.GetRegistryKey("TownAddress4");
			KeyCol = 0;
			JournalCol = 1;
			VendorNumberCol = 2;
			VendorNameCol = 3;
			DescriptionCol = 4;
			POCol = 5;
			vsEncumbrances.ColWidth(KeyCol, 0);
			vsEncumbrances.ColWidth(JournalCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.085));
			vsEncumbrances.ColWidth(VendorNumberCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.085));
			vsEncumbrances.ColWidth(VendorNameCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.3388466));
			// set encumbrance flexgrid column widths
			vsEncumbrances.ColWidth(DescriptionCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.2754072));
			vsEncumbrances.ColWidth(POCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.1));
			// 442099
			vsEncumbrances.TextMatrix(0, JournalCol, "Jrnl#");
			vsEncumbrances.TextMatrix(0, VendorNumberCol, "Vndr#");
			vsEncumbrances.TextMatrix(0, VendorNameCol, "Vendor Name");
			// set encumbrance flexgrid column headings
			vsEncumbrances.TextMatrix(0, DescriptionCol, "Description");
			vsEncumbrances.TextMatrix(0, POCol, "PO#");
			vsEncumbrances.ColAlignment(JournalCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEncumbrances.ColAlignment(VendorNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsEncumbrances.ColAlignment(POCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //vsEncumbrances.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsEncumbrances.Cols - 1, 4);
            FillGrid();
		}

		private void FillGrid()
		{
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsVendor = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM Encumbrances WHERE VendorNumber = 0 and PO <> '' ORDER BY TempVendorName");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsEncumbrances.Rows += 1;
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, KeyCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, JournalCol, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNameCol, FCConvert.ToString(rs.Get_Fields_String("TempVendorName")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, POCol, FCConvert.ToString(rs.Get_Fields("PO")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.OpenRecordset("SELECT * FROM Encumbrances WHERE VendorNumber <> 0 ORDER BY VendorNumber");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rsVendor.OpenRecordset("SELECT * FROM VendorMaster ORDER BY VendorNumber");
				do
				{
					vsEncumbrances.Rows += 1;
					if (rsVendor.FindFirstRecord("VendorNumber", rs.Get_Fields_Int32("VendorNumber")))
					{
						vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNameCol, FCConvert.ToString(rsVendor.Get_Fields_String("CheckName")));
					}
					else
					{
						vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNameCol, "UNKNOWN");
					}
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, KeyCol, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
					// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, JournalCol, FCConvert.ToString(rs.Get_Fields("JournalNumber")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, VendorNumberCol, FCConvert.ToString(rs.Get_Fields_Int32("VendorNumber")));
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [PO] and replace with corresponding Get_Field method
					vsEncumbrances.TextMatrix(vsEncumbrances.Rows - 1, POCol, FCConvert.ToString(rs.Get_Fields("PO")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void frmPrintPurchaseOrder_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmPrintPurchaseOrder_Resize(object sender, System.EventArgs e)
		{
			vsEncumbrances.ColWidth(KeyCol, 0);
			vsEncumbrances.ColWidth(JournalCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.085));
			vsEncumbrances.ColWidth(VendorNumberCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.085));
			vsEncumbrances.ColWidth(VendorNameCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.4));
			// set encumbrance flexgrid column widths
			vsEncumbrances.ColWidth(DescriptionCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.3));
			vsEncumbrances.ColWidth(POCol, FCConvert.ToInt32(vsEncumbrances.WidthOriginal * 0.1));
			// 442099
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (vsEncumbrances.Row > 0)
			{
				modRegistry.SaveRegistryKey("TownName", txtTownName.Text);
				modRegistry.SaveRegistryKey("TownAddress1", txtTownAddress[0].Text);
				modRegistry.SaveRegistryKey("TownAddress2", txtTownAddress[1].Text);
				modRegistry.SaveRegistryKey("TownAddress3", txtTownAddress[2].Text);
				modRegistry.SaveRegistryKey("TownAddress4", txtTownAddress[3].Text);
				rptPurchaseOrder.InstancePtr.lngID = FCConvert.ToInt32(vsEncumbrances.TextMatrix(vsEncumbrances.Row, KeyCol));
				rptPurchaseOrder.InstancePtr.blnIncludeAdjustments = chkAdjustments.CheckState == CheckState.Checked;
				//rptPurchaseOrder.InstancePtr.Show(App.MainForm);
				// FC: FINAL: KV: IIT807 + FC - 8697
				this.Hide();
				frmReportViewer.InstancePtr.Init(rptPurchaseOrder.InstancePtr);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (vsEncumbrances.Row > 0)
			{
				modRegistry.SaveRegistryKey("TownName", txtTownName.Text);
				modRegistry.SaveRegistryKey("TownAddress1", txtTownAddress[0].Text);
				modRegistry.SaveRegistryKey("TownAddress2", txtTownAddress[1].Text);
				modRegistry.SaveRegistryKey("TownAddress3", txtTownAddress[2].Text);
				modRegistry.SaveRegistryKey("TownAddress4", txtTownAddress[3].Text);
				rptPurchaseOrder.InstancePtr.lngID = FCConvert.ToInt32(vsEncumbrances.TextMatrix(vsEncumbrances.Row, KeyCol));
				rptPurchaseOrder.InstancePtr.blnIncludeAdjustments = chkAdjustments.CheckState == CheckState.Checked;
				rptPurchaseOrder.InstancePtr.PrintReport();
				this.Hide();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void vsEncumbrances_ClickEvent(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex > -1)
			{
				// FC: FINAL: KV: IIT807 + FC - 8697
				this.Hide();
				modRegistry.SaveRegistryKey("TownName", txtTownName.Text);
				modRegistry.SaveRegistryKey("TownAddress1", txtTownAddress[0].Text);
				modRegistry.SaveRegistryKey("TownAddress2", txtTownAddress[1].Text);
				modRegistry.SaveRegistryKey("TownAddress3", txtTownAddress[2].Text);
				modRegistry.SaveRegistryKey("TownAddress4", txtTownAddress[3].Text);
				rptPurchaseOrder.InstancePtr.lngID = FCConvert.ToInt32(vsEncumbrances.TextMatrix(vsEncumbrances.Row, KeyCol));
				rptPurchaseOrder.InstancePtr.blnIncludeAdjustments = chkAdjustments.CheckState == CheckState.Checked;
				//rptPurchaseOrder.InstancePtr.Show(App.MainForm);
				frmReportViewer.InstancePtr.Init(rptPurchaseOrder.InstancePtr);
			}
		}
	}
}
