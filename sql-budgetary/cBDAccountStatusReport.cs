﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Global;

namespace TWBD0000
{
	public class cBDAccountStatusReport
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collAccounts = new cGenericCollection();
		private cGenericCollection collAccounts_AutoInitialized;

		private cGenericCollection collAccounts
		{
			get
			{
				if (collAccounts_AutoInitialized == null)
				{
					collAccounts_AutoInitialized = new cGenericCollection();
				}
				return collAccounts_AutoInitialized;
			}
			set
			{
				collAccounts_AutoInitialized = new cGenericCollection();
			}
		}

		private string strAccount = string.Empty;
		private string strEndAccount = string.Empty;
		private int intStartPeriod;
		private int intEndPeriod;
		private int intActivityRangeType;
		// 0 all
		// 1 date range
		// 2 period range
		private int intAccountRangeType;
		// 0 all
		// 1 single
		// 2 account range
		// 3 Department
		// 4 Range of Department
		private bool boolShowLiquidatedEncumbranceActivity;
		private int intDateType;
		// 0 posted date
		// 1 activity date
		private string strStartDate = "";
		private string strEndDate = "";

		public string StartDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strStartDate = value;
				}
				else
				{
					strStartDate = "";
				}
			}
			get
			{
				string StartDate = "";
				StartDate = strStartDate;
				return StartDate;
			}
		}

		public string EndDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strEndDate = value;
				}
				else
				{
					strEndDate = "";
				}
			}
			get
			{
				string EndDate = "";
				EndDate = strEndDate;
				return EndDate;
			}
		}

		public short DateType
		{
			set
			{
				intDateType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short DateType = 0;
				DateType = FCConvert.ToInt16(intDateType);
				return DateType;
			}
		}

		public bool ShowLiquidateEncumbranceActivity
		{
			set
			{
				boolShowLiquidatedEncumbranceActivity = value;
			}
			get
			{
				bool ShowLiquidateEncumbranceActivity = false;
				ShowLiquidateEncumbranceActivity = boolShowLiquidatedEncumbranceActivity;
				return ShowLiquidateEncumbranceActivity;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string EndAccount
		{
			set
			{
				strEndAccount = value;
			}
			get
			{
				string EndAccount = "";
				EndAccount = strEndAccount;
				return EndAccount;
			}
		}

		public short StartPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					intStartPeriod = value;
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short StartPeriod = 0;
				StartPeriod = FCConvert.ToInt16(intStartPeriod);
				return StartPeriod;
			}
		}

		public short EndPeriod
		{
			set
			{
				if (value > 0 && value < 13)
				{
					intEndPeriod = value;
				}
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short EndPeriod = 0;
				EndPeriod = FCConvert.ToInt16(intEndPeriod);
				return EndPeriod;
			}
		}

		public cGenericCollection Accounts
		{
			set
			{
				collAccounts = value;
			}
			get
			{
				cGenericCollection Accounts = null;
				Accounts = collAccounts;
				return Accounts;
			}
		}

		public short ActivityRangeType
		{
			set
			{
				intActivityRangeType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short ActivityRangeType = 0;
				ActivityRangeType = FCConvert.ToInt16(intActivityRangeType);
				return ActivityRangeType;
			}
		}

		public short AccountRangeType
		{
			set
			{
				intAccountRangeType = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short AccountRangeType = 0;
				AccountRangeType = FCConvert.ToInt16(intAccountRangeType);
				return AccountRangeType;
			}
		}
	}
}
