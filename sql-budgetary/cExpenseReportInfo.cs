﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace TWBD0000
{
	public class cExpenseReportInfo
	{
		//=========================================================
		private int lngRecordID;
		private string strAccount = string.Empty;
		private int intPeriod;
		private string strDepartment = string.Empty;
		private string strDivision = string.Empty;
		private string strExpense = string.Empty;
		private string strObject = string.Empty;
		private double dblBudgetAdjustments;
		private double dblOriginalBudget;
		private double dblPostedDebits;
		private double dblPostedCredits;
		private double dblEncumbActivity;
		private double dblPendingDebits;
		private double dblPendingCredits;
		private double dblEncumbranceDebits;
		private double dblEncumbranceCredits;
		private double dblMonthlyBudget;
		private double dblMonthlyBudgetAdjustments;
		private string strProject = string.Empty;
		private double dblAutoBudgetAdjustments;
		private double dblPendingEncumbranceActivity;
		private double dblPendingEncumbranceDebits;
		private double dblCarryForward;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string Division
		{
			set
			{
				strDivision = value;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}

		public string Expense
		{
			set
			{
				strExpense = value;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string AccountObject
		{
			set
			{
				strObject = value;
			}
			get
			{
				string AccountObject = "";
				AccountObject = strObject;
				return AccountObject;
			}
		}

		public double BudgetAdjustments
		{
			set
			{
				dblBudgetAdjustments = value;
			}
			get
			{
				double BudgetAdjustments = 0;
				BudgetAdjustments = dblBudgetAdjustments;
				return BudgetAdjustments;
			}
		}

		public double OriginalBudget
		{
			set
			{
				dblOriginalBudget = value;
			}
			get
			{
				double OriginalBudget = 0;
				OriginalBudget = dblOriginalBudget;
				return OriginalBudget;
			}
		}

		public double PostedDebits
		{
			set
			{
				dblPostedDebits = value;
			}
			get
			{
				double PostedDebits = 0;
				PostedDebits = dblPostedDebits;
				return PostedDebits;
			}
		}

		public double PostedCredits
		{
			set
			{
				dblPostedCredits = value;
			}
			get
			{
				double PostedCredits = 0;
				PostedCredits = dblPostedCredits;
				return PostedCredits;
			}
		}

		public double EncumbranceActivity
		{
			set
			{
				dblEncumbActivity = value;
			}
			get
			{
				double EncumbranceActivity = 0;
				EncumbranceActivity = dblEncumbActivity;
				return EncumbranceActivity;
			}
		}

		public double PendingDebits
		{
			set
			{
				dblPendingDebits = value;
			}
			get
			{
				double PendingDebits = 0;
				PendingDebits = dblPendingDebits;
				return PendingDebits;
			}
		}

		public double PendingCredits
		{
			set
			{
				dblPendingCredits = value;
			}
			get
			{
				double PendingCredits = 0;
				PendingCredits = dblPendingCredits;
				return PendingCredits;
			}
		}

		public double EncumbranceDebits
		{
			set
			{
				dblEncumbranceDebits = value;
			}
			get
			{
				double EncumbranceDebits = 0;
				EncumbranceDebits = dblEncumbranceDebits;
				return EncumbranceDebits;
			}
		}

		public double EncumbranceCredits
		{
			set
			{
				dblEncumbranceCredits = value;
			}
			get
			{
				double EncumbranceCredits = 0;
				EncumbranceCredits = dblEncumbranceCredits;
				return EncumbranceCredits;
			}
		}

		public double MonthlyBudget
		{
			set
			{
				dblMonthlyBudget = value;
			}
			get
			{
				double MonthlyBudget = 0;
				MonthlyBudget = dblMonthlyBudget;
				return MonthlyBudget;
			}
		}

		public double MonthlyBudgetAdjustments
		{
			set
			{
				dblMonthlyBudgetAdjustments = value;
			}
			get
			{
				double MonthlyBudgetAdjustments = 0;
				MonthlyBudgetAdjustments = dblMonthlyBudgetAdjustments;
				return MonthlyBudgetAdjustments;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public double AutoBudgetAdjustments
		{
			set
			{
				dblAutoBudgetAdjustments = value;
			}
			get
			{
				double AutoBudgetAdjustments = 0;
				AutoBudgetAdjustments = dblAutoBudgetAdjustments;
				return AutoBudgetAdjustments;
			}
		}

		public double PendingEncumbranceActivity
		{
			set
			{
				dblPendingEncumbranceActivity = value;
			}
			get
			{
				double PendingEncumbranceActivity = 0;
				PendingEncumbranceActivity = dblPendingEncumbranceActivity;
				return PendingEncumbranceActivity;
			}
		}

		public double PendingEncumbranceDebits
		{
			set
			{
				dblPendingEncumbranceDebits = value;
			}
			get
			{
				double PendingEncumbranceDebits = 0;
				PendingEncumbranceDebits = dblPendingEncumbranceDebits;
				return PendingEncumbranceDebits;
			}
		}

		public double CarryForward
		{
			set
			{
				dblCarryForward = value;
			}
			get
			{
				double CarryForward = 0;
				CarryForward = dblCarryForward;
				return CarryForward;
			}
		}
	}
}
