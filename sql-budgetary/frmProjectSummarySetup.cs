﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWBD0000
{
	/// <summary>
	/// Summary description for frmProjectSummarySetup.
	/// </summary>
	public partial class frmProjectSummarySetup : BaseForm
	{
		public frmProjectSummarySetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblTo = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTo.AddControlArrayElement(lblTo_0, 0);
			this.lblTo.AddControlArrayElement(lblTo_2, 2);
			this.lblTo.AddControlArrayElement(lblTo_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbRange.SelectedIndex = 2;
			cmbAllAccounts.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmProjectSummarySetup InstancePtr
		{
			get
			{
				return (frmProjectSummarySetup)Sys.GetInstance(typeof(frmProjectSummarySetup));
			}
		}

		protected frmProjectSummarySetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         2/21/2002
		// This form will be used by people to select the search criteria
		// they wish to use to get information for the Expense Summary
		// Report
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();
		int FirstMonth;

		private void chkCheckAccountRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckAccountRange.CheckState == CheckState.Checked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					cboSingleProject.Visible = false;
					cboSingleProject.SelectedIndex = -1;
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					cboSingleFund.Visible = false;
					cboSingleFund.SelectedIndex = -1;
				}
				else
				{
					cboBeginningProject.Visible = false;
					cboEndingProject.Visible = false;
					cboBeginningProject.SelectedIndex = -1;
					cboEndingProject.SelectedIndex = -1;
					lblTo[1].Visible = false;
					lblTo[2].Visible = false;
				}
			}
			else
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					cboSingleProject.Visible = true;
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					cboSingleFund.Visible = true;
				}
				else
				{
					cboBeginningProject.Visible = true;
					cboEndingProject.Visible = true;
					lblTo[2].Visible = true;
				}
			}
		}

		private void chkCheckDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCheckDateRange.CheckState == CheckState.Checked)
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = false;
					cboSingleMonth.SelectedIndex = -1;
				}
				else
				{
					cboBeginningMonth.Visible = false;
					cboEndingMonth.Visible = false;
					lblTo[0].Visible = false;
					cboBeginningMonth.SelectedIndex = -1;
					cboEndingMonth.SelectedIndex = -1;
				}
			}
			else
			{
				if (cmbRange.SelectedIndex == 1)
				{
					cboSingleMonth.Visible = true;
					cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
				else
				{
					cboBeginningMonth.Visible = true;
					cboEndingMonth.Visible = true;
					lblTo[0].Visible = true;
					cboBeginningMonth.SelectedIndex = FirstMonth - 1;
					cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
				}
			}
		}

		private void cmdCancelPrint_Click(object sender, System.EventArgs e)
		{
			if (!modBudgetaryMaster.Statics.blnProjectSummaryEdit)
			{
				frmProjectSummarySelect.InstancePtr.Show(App.MainForm);
			}
			Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: answer As short, int --> As DialogResult
			DialogResult answer;
			clsDRWrapper rs2 = new clsDRWrapper();
			if (Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a Description for this Selection Criteria before you may proceed.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (Strings.UCase(txtDescription.Text) == "DEFAULT")
			{
				MessageBox.Show("You may not save over the default.  You must change the description before you may save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDescription.Focus();
				return;
			}
			if (cmbRange.SelectedIndex == 0 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboBeginningMonth.SelectedIndex == -1 || cboEndingMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter values for the range of months to report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (!modBudgetaryMaster.CheckReportDateRange_8(FCConvert.ToInt16(cboBeginningMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndingMonth.SelectedIndex + 1)))
				{
					MessageBox.Show("You have entered an invalid date range for this report", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbRange.SelectedIndex == 1 && chkCheckDateRange.CheckState != CheckState.Checked)
			{
				if (cboSingleMonth.SelectedIndex == -1)
				{
					MessageBox.Show("You must specify the month you wish to have reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (chkCheckAccountRange.CheckState == CheckState.Unchecked)
			{
				if (cmbAllAccounts.SelectedIndex == 1)
				{
					if (cboSingleProject.SelectedIndex == -1 && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify which Project you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 3)
				{
					if (cboSingleFund.SelectedIndex == -1 && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify which Fund you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbAllAccounts.SelectedIndex == 2)
				{
					if ((cboBeginningProject.SelectedIndex == -1 || cboEndingProject.SelectedIndex == -1) && chkCheckAccountRange.CheckState == CheckState.Unchecked)
					{
						MessageBox.Show("You must specify the range of Projects you want reported", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cboBeginningProject.SelectedIndex > cboEndingProject.SelectedIndex)
					{
						MessageBox.Show("Your beginning Department must be lower then your ending Department", "Unable to Create Report", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
			}
			rs.OpenRecordset("SELECT * FROM ReportTitles WHERE Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "' AND Type = 'PS'");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				answer = MessageBox.Show("A selection criteria already exists with this description.  Do you wish to overwrite this?", "Overwrite Selection Criteria?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (answer == DialogResult.Yes)
				{
					rs.Edit();
				}
				else
				{
					txtDescription.Focus();
					return;
				}
			}
			else
			{
				rs.AddNew();
			}
			rs.Set_Fields("Description", txtDescription.Text);
			rs.Set_Fields("Type", "PS");
			if (cmbRange.SelectedIndex == 1)
			{
				rs.Set_Fields("SelectedMonths", "S");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboSingleMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			else if (cmbRange.SelectedIndex == 0)
			{
				rs.Set_Fields("SelectedMonths", "R");
				if (chkCheckDateRange.CheckState == CheckState.Unchecked)
				{
					rs.Set_Fields("BegMonth", cboBeginningMonth.SelectedIndex + 1);
					rs.Set_Fields("EndMonth", cboEndingMonth.SelectedIndex + 1);
					rs.Set_Fields("CheckMonthRange", false);
				}
				else
				{
					rs.Set_Fields("CheckMonthRange", true);
				}
			}
			else
			{
				rs.Set_Fields("SelectedMonths", "A");
				rs.Set_Fields("CheckMonthRange", false);
			}
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				rs.Set_Fields("SelectedAccounts", "A");
				rs.Set_Fields("CheckAccountRange", false);
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				rs.Set_Fields("SelectedAccounts", "S");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboSingleProject.Text, 4));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				rs.Set_Fields("SelectedAccounts", "F");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboSingleFund.Text, FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))))));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			else
			{
				rs.Set_Fields("SelectedAccounts", "D");
				if (chkCheckAccountRange.CheckState == CheckState.Checked)
				{
					rs.Set_Fields("CheckAccountRange", true);
				}
				else
				{
					rs.Set_Fields("BegDeptExp", Strings.Left(cboBeginningProject.Text, 4));
					rs.Set_Fields("EndDeptExp", Strings.Left(cboEndingProject.Text, 4));
					rs.Set_Fields("CheckAccountRange", false);
				}
			}
			rs.Update();
			if (!modBudgetaryMaster.Statics.blnProjectSummaryEdit)
			{
				//! Load frmWait; // show the wait form
				frmWait.InstancePtr.Left = (FCGlobal.Screen.Width - frmWait.InstancePtr.Width) / 2;
				// center it
				frmWait.InstancePtr.Top = (FCGlobal.Screen.Height - frmWait.InstancePtr.Height) / 2;
				frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Loading Data";
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				modBudgetaryAccounting.Statics.SearchResults.OpenRecordset("SELECT * FROM ReportTitles WHERE Type = 'PS' AND Description = '" + modCustomReport.FixQuotes(txtDescription.Text) + "'");
				frmProjectSummarySelect.InstancePtr.Show(App.MainForm);
				frmProjectSummarySelect.InstancePtr.FillCriteria(true, Strings.Trim(txtDescription.Text));
				frmWait.InstancePtr.Refresh();
				//Application.DoEvents();
				frmWait.InstancePtr.Unload();
				ResetForm();
				Close();
			}
			else
			{
				ResetForm();
				Close();
			}
		}

		public void cmdSave_Click()
		{
			cmdSave_Click(cmdSave, new System.EventArgs());
		}

		private void frmProjectSummarySetup_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
		}

		private void frmProjectSummarySetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
			}
		}

		private void frmProjectSummarySetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmProjectSummarySetup.FillStyle	= 0;
			//frmProjectSummarySetup.ScaleWidth	= 9045;
			//frmProjectSummarySetup.ScaleHeight	= 6930;
			//frmProjectSummarySetup.LinkTopic	= "Form2";
			//frmProjectSummarySetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int counter = 0;
			clsDRWrapper rs2 = new clsDRWrapper();
			//FC:FINAL:SBE - redesigned
			//cboBeginningProject.Left = cboBeginningProject.Left + cboBeginningProject.Width - (4 * 115 + 350);
			//cboSingleProject.Left = FCConvert.ToInt32(cboSingleProject.Left + (cboSingleProject.Width - (4 * 115 + 350)) - (0.5 * (cboSingleProject.Width - (4 * 115 + 350))));
			//cboBeginningProject.Width = 4 * 115 + 350;
			//cboEndingProject.Width = 4 * 115 + 350;
			//cboSingleProject.Width = 4 * 115 + 350;
			//cboSingleFund.Left = FCConvert.ToInt32(cboSingleFund.Left + (cboSingleFund.Width - (4 * 115 + 350)) - (0.5 * (cboSingleFund.Width - (4 * 115 + 350))));
			//cboSingleFund.Width = 4 * 115 + 350;
			rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Account = '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + "' ORDER BY Fund");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Fund] and replace with corresponding Get_Field method
					cboSingleFund.AddItem(rs.Get_Fields("Fund") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			rs.OpenRecordset("SELECT * FROM ProjectMaster ORDER BY ProjectCode");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				rs.MoveLast();
				rs.MoveFirst();
				counter = 0;
				while (rs.EndOfFile() != true)
				{
					cboBeginningProject.AddItem(rs.Get_Fields_String("ProjectCode") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboEndingProject.AddItem(rs.Get_Fields_String("ProjectCode") + " - " + rs.Get_Fields_String("ShortDescription"));
					cboSingleProject.AddItem(rs.Get_Fields_String("ProjectCode") + " - " + rs.Get_Fields_String("ShortDescription"));
					counter += 1;
					rs.MoveNext();
				}
			}
			cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
			FirstMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(modBudgetaryAccounting.GetBDVariable("FiscalStart"))));
			cboBeginningMonth.SelectedIndex = FirstMonth - 1;
			cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
			if (modBudgetaryMaster.Statics.blnProjectSummaryEdit || modBudgetaryMaster.Statics.blnProjectSummaryReportEdit)
			{
				if (modBudgetaryAccounting.Statics.SearchResults.IsntAnything())
				{
					// do nothing
				}
				else
				{
					txtDescription.Text = FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("Description"));
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths")) == "S")
					{
						cmbRange.SelectedIndex = 1;
						cboSingleMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedMonths") == "R")
					{
						cmbRange.SelectedIndex = 0;
						cboBeginningMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("BegMonth")) - 1;
						cboEndingMonth.SelectedIndex = FCConvert.ToInt32(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Int32("EndMonth")) - 1;
						if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckMonthRange") == true)
						{
							chkCheckDateRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbRange.SelectedIndex = 2;
					}
					if (FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts")) == "A")
					{
						cmbAllAccounts.SelectedIndex = 0;
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "S")
					{
						cmbAllAccounts.SelectedIndex = 1;
						for (counter = 0; counter <= cboSingleProject.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboSingleProject.Items[counter].ToString(), 4) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboSingleProject.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else if (modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("SelectedAccounts") == "F")
					{
						cmbAllAccounts.SelectedIndex = 3;
						for (counter = 0; counter <= cboSingleFund.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboSingleFund.Items[counter].ToString(), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboSingleFund.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
					else
					{
						cmbAllAccounts.SelectedIndex = 2;
						for (counter = 0; counter <= cboBeginningProject.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboBeginningProject.Items[counter].ToString(), 4) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("BegDeptExp")))
							{
								cboBeginningProject.SelectedIndex = counter;
								break;
							}
						}
						for (counter = 0; counter <= cboEndingProject.Items.Count - 1; counter++)
						{
							if (Strings.Left(cboEndingProject.Items[counter].ToString(), 4) == FCConvert.ToString(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_String("EndDeptExp")))
							{
								cboEndingProject.SelectedIndex = counter;
								break;
							}
						}
						if (FCConvert.ToBoolean(modBudgetaryAccounting.Statics.SearchResults.Get_Fields_Boolean("CheckAccountRange")))
						{
							chkCheckAccountRange.CheckState = CheckState.Checked;
						}
					}
				}
			}
			this.Refresh();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			modBudgetaryMaster.Statics.blnProjectSummaryReportEdit = false;
			if (modBudgetaryMaster.Statics.blnProjectSummaryEdit)
			{
				frmGetProjectSummary.InstancePtr.Show(App.MainForm);
            }
            else
            {
                //FC:FINAL:BSE:#4063 form should open previous form instead of closing itself
                frmProjectSummarySelect.InstancePtr.Show(App.MainForm);
            }
		}

		private void frmProjectSummarySetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (!modBudgetaryMaster.Statics.blnProjectSummaryEdit)
				{
					frmProjectSummarySelect.InstancePtr.Show(App.MainForm);
				}
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdSave_Click();
		}

		private void cmbAllAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAllAccounts.SelectedIndex == 0)
			{
				optAllAccounts_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 1)
			{
				optSingleProject_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 2)
			{
				optProjectRange_CheckedChanged(sender, e);
			}
			else if (cmbAllAccounts.SelectedIndex == 3)
			{
				optSingleFund_CheckedChanged(sender, e);
			}
		}

		private void optAllAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningProject.Visible = false;
			cboEndingProject.Visible = false;
			cboBeginningProject.SelectedIndex = -1;
			cboEndingProject.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleProject.Visible = false;
			cboSingleProject.SelectedIndex = -1;
			cboSingleFund.Visible = false;
			cboSingleFund.SelectedIndex = -1;
			lblTo[1].Visible = false;
			chkCheckAccountRange.CheckState = CheckState.Unchecked;
			chkCheckAccountRange.Visible = false;
		}

		private void optAllMonths_CheckedChanged(object sender, System.EventArgs e)
		{
			chkCheckDateRange.CheckState = CheckState.Unchecked;
			chkCheckDateRange.Visible = false;
			cboBeginningMonth.Visible = false;
			cboBeginningMonth.SelectedIndex = -1;
			cboEndingMonth.Visible = false;
			cboEndingMonth.SelectedIndex = -1;
			lblTo[0].Visible = false;
			cboSingleMonth.SelectedIndex = -1;
			cboSingleMonth.Visible = false;
		}

		private void optProjectRange_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningProject.Visible = true;
			cboEndingProject.Visible = true;
			lblTo[2].Visible = true;
			cboSingleProject.Visible = false;
			cboSingleFund.Visible = false;
			cboSingleFund.SelectedIndex = -1;
			lblTo[1].Visible = false;
			chkCheckAccountRange.CheckState = CheckState.Unchecked;
			chkCheckAccountRange.Visible = true;
		}

		private void cmbRange_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbRange.SelectedIndex == 0)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 1)
			{
				optSingle_CheckedChanged(sender, e);
			}
			else if (cmbRange.SelectedIndex == 2)
			{
				optAllMonths_CheckedChanged(sender, e);
			}
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.Visible = true;
			cboEndingMonth.Visible = true;
			lblTo[0].Visible = true;
			cboSingleMonth.Visible = false;
			chkCheckDateRange.CheckState = CheckState.Unchecked;
			chkCheckDateRange.Visible = true;
			cboBeginningMonth.SelectedIndex = FirstMonth - 1;
			cboEndingMonth.SelectedIndex = DateTime.Today.Month - 1;
		}

		private void optSingle_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningMonth.Visible = false;
			cboBeginningMonth.SelectedIndex = -1;
			cboEndingMonth.Visible = false;
			cboEndingMonth.SelectedIndex = -1;
			lblTo[0].Visible = false;
			cboSingleMonth.Visible = true;
			chkCheckDateRange.CheckState = CheckState.Unchecked;
			chkCheckDateRange.Visible = true;
			cboSingleMonth.SelectedIndex = DateTime.Today.Month - 1;
		}

		private void optSingleProject_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningProject.Visible = false;
			cboEndingProject.Visible = false;
			cboBeginningProject.SelectedIndex = -1;
			cboEndingProject.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleProject.Visible = true;
			cboSingleFund.Visible = false;
			cboSingleFund.SelectedIndex = -1;
			lblTo[1].Visible = false;
			chkCheckAccountRange.CheckState = CheckState.Unchecked;
			chkCheckAccountRange.Visible = true;
		}

		private void ResetForm()
		{
			txtDescription.Text = "";
			cboSingleProject.SelectedIndex = -1;
			cboSingleProject.Visible = false;
			cboSingleFund.SelectedIndex = -1;
			cboSingleFund.Visible = false;
			cboBeginningProject.SelectedIndex = -1;
			cboBeginningProject.Visible = false;
			cboEndingProject.SelectedIndex = -1;
			cboEndingProject.Visible = false;
			lblTo[2].Visible = false;
			cmbRange.SelectedIndex = 2;
			cboBeginningMonth.Visible = false;
			cboEndingMonth.Visible = false;
			lblTo[1].Visible = false;
			cmbAllAccounts.SelectedIndex = 0;
			fraMonths.Visible = true;
			cmdSave.Enabled = true;
			cmdCancelPrint.Enabled = true;
			fraDeptRange.Visible = true;
		}

		private void cboBeginningProject_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboBeginningProject.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboEndingProject_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboEndingProject.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboSingleProject_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleProject.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cboSingleFund_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboSingleFund.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void optSingleFund_CheckedChanged(object sender, System.EventArgs e)
		{
			cboBeginningProject.Visible = false;
			cboEndingProject.Visible = false;
			cboBeginningProject.SelectedIndex = -1;
			cboEndingProject.SelectedIndex = -1;
			lblTo[2].Visible = false;
			cboSingleProject.Visible = false;
			cboSingleProject.SelectedIndex = -1;
			cboSingleFund.Visible = true;
			lblTo[1].Visible = false;
			chkCheckAccountRange.CheckState = CheckState.Unchecked;
			chkCheckAccountRange.Visible = true;
		}
	}
}
