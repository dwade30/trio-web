//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWBD0000
{
    /// <summary>
    /// Summary description for frmAPDataEntry.
    /// </summary>
    partial class frmAPDataEntry : BaseForm
    {
        public System.Collections.Generic.List<Global.T2KOverTypeBox> txtAddress;
        public fecherFoundation.FCFrame fraPurchaseOrders;
        public fecherFoundation.FCComboBox cboPurchaseOrderDepartment;
        public fecherFoundation.FCGrid vsPurchaseOrders;
        public fecherFoundation.FCLabel lblPurchaseOrderDepartment;
        public fecherFoundation.FCFrame fraAccountBalance;
        public fecherFoundation.FCButton cmdBalanceOK;
        public fecherFoundation.FCLabel lblBalPendingYTDNet;
        public fecherFoundation.FCLabel lblBalBalance;
        public fecherFoundation.FCLabel lblBalPostedYTDNet;
        public fecherFoundation.FCLabel lblBalNetBudget;
        public fecherFoundation.FCLabel Label6;
        public fecherFoundation.FCLabel Label5;
        public fecherFoundation.FCLabel Label4;
        public fecherFoundation.FCLabel Label3;
        public fecherFoundation.FCLabel lblBalAccount;
        public fecherFoundation.FCFrame fraJournalSave;
        public fecherFoundation.FCTextBox txtJournalDescription;
        public fecherFoundation.FCButton cmdCancelSave;
        public fecherFoundation.FCButton cmdOKSave;
        public fecherFoundation.FCComboBox cboSaveJournal;
        public fecherFoundation.FCLabel lblJournalDescription;
        public fecherFoundation.FCLabel lblJournalSave;
        public fecherFoundation.FCLabel lblSaveInstructions;
        public fecherFoundation.FCFrame Frame2;
        public fecherFoundation.FCComboBox cboEncDept;
        public fecherFoundation.FCGrid vs3;
        public fecherFoundation.FCLabel lblDepartment;
        public fecherFoundation.FCFrame fraEncDetail;
        public fecherFoundation.FCButton cmdSelect;
        public fecherFoundation.FCButton cmdNo;
        public fecherFoundation.FCButton cmdOk;
        public fecherFoundation.FCGrid vs4;
        public fecherFoundation.FCLabel lblInstructions;
        public fecherFoundation.FCFrame frmSearch;
        public fecherFoundation.FCButton cmdCancel;
        public fecherFoundation.FCButton cmdSearch;
        public fecherFoundation.FCTextBox txtSearch;
        public fecherFoundation.FCFrame frmInfo;
        public fecherFoundation.FCListBox lstRecords;
        public fecherFoundation.FCButton cmdReturn;
        public fecherFoundation.FCButton cmdRetrieve;
        public fecherFoundation.FCLabel lblVendorName;
        public fecherFoundation.FCLabel lblRecordNumber;
        public Global.T2KOverTypeBox txtAddress_0;
        public Global.T2KOverTypeBox txtVendor;
        public Global.T2KOverTypeBox txtCheck;
        public Global.T2KOverTypeBox txtDescription;
        public Global.T2KOverTypeBox txtReference;
        public fecherFoundation.FCFrame fraBorder;
        public fecherFoundation.FCComboBox cboJournal;
        public fecherFoundation.FCFrame Frame1;
        public fecherFoundation.FCComboBox cboFrequency;
        public Global.T2KDateBox txtUntil;
        public fecherFoundation.FCLabel lblFrequency;
        public fecherFoundation.FCLabel lblDays;
        public fecherFoundation.FCLabel lblUntil;
        public fecherFoundation.FCCheckBox chkSeperate;
        public Global.T2KDateBox txtPayable;
        public Global.T2KOverTypeBox txtPeriod;
        public fecherFoundation.FCLabel lblPayable;
        public fecherFoundation.FCLabel lblJournal;
        public fecherFoundation.FCLabel lblPeriod;
        public Global.T2KOverTypeBox txtAddress_1;
        public Global.T2KOverTypeBox txtAddress_2;
        public Global.T2KOverTypeBox txtAddress_3;
        public Global.T2KBackFillDecimal txtAmount2;
        public fecherFoundation.FCTextBox txtAmount;
        public fecherFoundation.FCCheckBox chkUseAltCashAccount;
        public FCGrid vsAltCashAccount;
        public fecherFoundation.FCTextBox txtZip4;
        public fecherFoundation.FCTextBox txtZip;
        public fecherFoundation.FCTextBox txtState;
        public fecherFoundation.FCTextBox txtCity;
        public FCGrid vs1;
        public fecherFoundation.FCLabel lblReadOnly;
        public fecherFoundation.FCLabel lblExpense;
        public fecherFoundation.FCLabel lblActualPayment;
        public fecherFoundation.FCLabel lblRemAmount;
        public fecherFoundation.FCLabel lblVendor;
        public fecherFoundation.FCLabel lblDescription;
        public fecherFoundation.FCLabel lblReference;
        public fecherFoundation.FCLabel lblAmount;
        public fecherFoundation.FCLabel lblCheck;
        public fecherFoundation.FCLabel lblRemaining;
        public fecherFoundation.FCLabel lblActual;
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuProcessAccounts;
        public fecherFoundation.FCToolStripMenuItem mnuFileChangePayableDate;
        public fecherFoundation.FCToolStripMenuItem mnuProcessDelete;
        public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteEntry;
        public fecherFoundation.FCToolStripMenuItem mnuProcessEncumbrance;
        public fecherFoundation.FCToolStripMenuItem mnuFileShowPurchaseOrders;
        public fecherFoundation.FCToolStripMenuItem mnuViewAttachedDocuments;
        public fecherFoundation.FCToolStripMenuItem mnuFileSep2;
        public fecherFoundation.FCToolStripMenuItem mnuProcessSearch;
        public fecherFoundation.FCToolStripMenuItem mnuFileAddVendor;
        public fecherFoundation.FCToolStripMenuItem mnuFileSep;
        public fecherFoundation.FCToolStripMenuItem mnuProcessPreviousEntry;
        public fecherFoundation.FCToolStripMenuItem mnuProcessNextEntry;
        public fecherFoundation.FCToolStripMenuItem Seperator1;
        public fecherFoundation.FCToolStripMenuItem mnuFileDisplayAmount;
        public fecherFoundation.FCToolStripMenuItem mnuFileVendorDetail;
        public fecherFoundation.FCToolStripMenuItem mnuFileSep3;
        public fecherFoundation.FCToolStripMenuItem mnuFileSave;
        public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
        public fecherFoundation.FCToolStripMenuItem Seperator;
        public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
        private Wisej.Web.ToolTip ToolTip1;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAPDataEntry));
            this.fraPurchaseOrders = new fecherFoundation.FCFrame();
            this.cboPurchaseOrderDepartment = new fecherFoundation.FCComboBox();
            this.vsPurchaseOrders = new fecherFoundation.FCGrid();
            this.lblPurchaseOrderDepartment = new fecherFoundation.FCLabel();
            this.fraAccountBalance = new fecherFoundation.FCFrame();
            this.cmdBalanceOK = new fecherFoundation.FCButton();
            this.lblBalPendingYTDNet = new fecherFoundation.FCLabel();
            this.lblBalBalance = new fecherFoundation.FCLabel();
            this.lblBalPostedYTDNet = new fecherFoundation.FCLabel();
            this.lblBalNetBudget = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblBalAccount = new fecherFoundation.FCLabel();
            this.fraJournalSave = new fecherFoundation.FCFrame();
            this.txtJournalDescription = new fecherFoundation.FCTextBox();
            this.cmdCancelSave = new fecherFoundation.FCButton();
            this.cmdOKSave = new fecherFoundation.FCButton();
            this.cboSaveJournal = new fecherFoundation.FCComboBox();
            this.lblJournalDescription = new fecherFoundation.FCLabel();
            this.lblJournalSave = new fecherFoundation.FCLabel();
            this.lblSaveInstructions = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.btnCancelEncumbrance = new fecherFoundation.FCButton();
            this.cboEncDept = new fecherFoundation.FCComboBox();
            this.vs3 = new fecherFoundation.FCGrid();
            this.lblDepartment = new fecherFoundation.FCLabel();
            this.fraEncDetail = new fecherFoundation.FCFrame();
            this.cmdSelect = new fecherFoundation.FCButton();
            this.cmdNo = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.vs4 = new fecherFoundation.FCGrid();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.frmSearch = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.frmInfo = new fecherFoundation.FCFrame();
            this.lstRecords = new fecherFoundation.FCListBox();
            this.cmdReturn = new fecherFoundation.FCButton();
            this.cmdRetrieve = new fecherFoundation.FCButton();
            this.lblVendorName = new fecherFoundation.FCLabel();
            this.lblRecordNumber = new fecherFoundation.FCLabel();
            this.txtAddress_0 = new Global.T2KOverTypeBox();
            this.txtVendor = new Global.T2KOverTypeBox();
            this.txtCheck = new Global.T2KOverTypeBox();
            this.txtDescription = new Global.T2KOverTypeBox();
            this.txtReference = new Global.T2KOverTypeBox();
            this.fraBorder = new fecherFoundation.FCFrame();
            this.cboJournal = new fecherFoundation.FCComboBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cboFrequency = new fecherFoundation.FCComboBox();
            this.txtUntil = new Global.T2KDateBox();
            this.lblFrequency = new fecherFoundation.FCLabel();
            this.lblDays = new fecherFoundation.FCLabel();
            this.lblUntil = new fecherFoundation.FCLabel();
            this.chkSeperate = new fecherFoundation.FCCheckBox();
            this.txtPayable = new Global.T2KDateBox();
            this.txtPeriod = new Global.T2KOverTypeBox();
            this.lblPayable = new fecherFoundation.FCLabel();
            this.lblJournal = new fecherFoundation.FCLabel();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.txtAddress_1 = new Global.T2KOverTypeBox();
            this.txtAddress_2 = new Global.T2KOverTypeBox();
            this.txtAddress_3 = new Global.T2KOverTypeBox();
            this.txtAmount2 = new Global.T2KBackFillDecimal();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.chkUseAltCashAccount = new fecherFoundation.FCCheckBox();
            this.vsAltCashAccount = new fecherFoundation.FCGrid();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.vs1 = new fecherFoundation.FCGrid();
            this.lblReadOnly = new fecherFoundation.FCLabel();
            this.lblExpense = new fecherFoundation.FCLabel();
            this.lblActualPayment = new fecherFoundation.FCLabel();
            this.lblRemAmount = new fecherFoundation.FCLabel();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.lblReference = new fecherFoundation.FCLabel();
            this.lblAmount = new fecherFoundation.FCLabel();
            this.lblCheck = new fecherFoundation.FCLabel();
            this.lblRemaining = new fecherFoundation.FCLabel();
            this.lblActual = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcessAccounts = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileChangePayableDate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDeleteEntry = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessEncumbrance = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileShowPurchaseOrders = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewAttachedDocuments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSep2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAddVendor = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSep = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDisplayAmount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessPreviousEntry = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessNextEntry = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileVendorDetail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSep3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcessSave = new fecherFoundation.FCButton();
            this.btnFileSave = new fecherFoundation.FCButton();
            this.btnFileVendorDetail = new fecherFoundation.FCButton();
            this.btnProcessNextEntry = new fecherFoundation.FCButton();
            this.btnProcessPreviousEntry = new fecherFoundation.FCButton();
            this.btnProcessSearch = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPurchaseOrders)).BeginInit();
            this.fraPurchaseOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPurchaseOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).BeginInit();
            this.fraAccountBalance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).BeginInit();
            this.fraJournalSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelEncumbrance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEncDetail)).BeginInit();
            this.fraEncDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSearch)).BeginInit();
            this.frmSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmInfo)).BeginInit();
            this.frmInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).BeginInit();
            this.fraBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUntil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseAltCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAltCashAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileVendorDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 636);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            this.BottomPanel.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.vsAltCashAccount);
            this.ClientArea.Controls.Add(this.fraEncDetail);
            this.ClientArea.Controls.Add(this.fraPurchaseOrders);
            this.ClientArea.Controls.Add(this.frmInfo);
            this.ClientArea.Controls.Add(this.chkUseAltCashAccount);
            this.ClientArea.Controls.Add(this.fraAccountBalance);
            this.ClientArea.Controls.Add(this.fraJournalSave);
            this.ClientArea.Controls.Add(this.frmSearch);
            this.ClientArea.Controls.Add(this.txtAddress_0);
            this.ClientArea.Controls.Add(this.txtVendor);
            this.ClientArea.Controls.Add(this.txtCheck);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtReference);
            this.ClientArea.Controls.Add(this.fraBorder);
            this.ClientArea.Controls.Add(this.txtAddress_1);
            this.ClientArea.Controls.Add(this.txtAddress_2);
            this.ClientArea.Controls.Add(this.txtAddress_3);
            this.ClientArea.Controls.Add(this.txtAmount2);
            this.ClientArea.Controls.Add(this.txtAmount);
            this.ClientArea.Controls.Add(this.txtZip4);
            this.ClientArea.Controls.Add(this.txtZip);
            this.ClientArea.Controls.Add(this.txtState);
            this.ClientArea.Controls.Add(this.txtCity);
            this.ClientArea.Controls.Add(this.vs1);
            this.ClientArea.Controls.Add(this.lblReadOnly);
            this.ClientArea.Controls.Add(this.lblExpense);
            this.ClientArea.Controls.Add(this.lblActualPayment);
            this.ClientArea.Controls.Add(this.lblRemAmount);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Controls.Add(this.lblReference);
            this.ClientArea.Controls.Add(this.lblAmount);
            this.ClientArea.Controls.Add(this.lblCheck);
            this.ClientArea.Controls.Add(this.lblRemaining);
            this.ClientArea.Controls.Add(this.lblActual);
            this.ClientArea.Size = new System.Drawing.Size(1078, 538);
            this.ClientArea.Controls.SetChildIndex(this.lblActual, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRemaining, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCheck, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReference, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVendor, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblRemAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblActualPayment, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblExpense, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblReadOnly, 0);
            this.ClientArea.Controls.SetChildIndex(this.vs1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCity, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtState, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtZip, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtZip4, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAmount, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAmount2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress_3, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress_2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress_1, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraBorder, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReference, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtCheck, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtVendor, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAddress_0, 0);
            this.ClientArea.Controls.SetChildIndex(this.frmSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraJournalSave, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAccountBalance, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkUseAltCashAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.frmInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPurchaseOrders, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraEncDetail, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsAltCashAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnProcessSearch);
            this.TopPanel.Controls.Add(this.btnProcessPreviousEntry);
            this.TopPanel.Controls.Add(this.btnProcessNextEntry);
            this.TopPanel.Controls.Add(this.btnFileVendorDetail);
            this.TopPanel.Controls.Add(this.btnFileSave);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileSave, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnFileVendorDetail, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessNextEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessPreviousEntry, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnProcessSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(143, 28);
            this.HeaderText.Text = "Invoice Entry";
            // 
            // fraPurchaseOrders
            // 
            this.fraPurchaseOrders.BackColor = System.Drawing.Color.White;
            this.fraPurchaseOrders.Controls.Add(this.cboPurchaseOrderDepartment);
            this.fraPurchaseOrders.Controls.Add(this.vsPurchaseOrders);
            this.fraPurchaseOrders.Controls.Add(this.lblPurchaseOrderDepartment);
            this.fraPurchaseOrders.Location = new System.Drawing.Point(1142, 180);
            this.fraPurchaseOrders.Name = "fraPurchaseOrders";
            this.fraPurchaseOrders.Size = new System.Drawing.Size(636, 456);
            this.fraPurchaseOrders.TabIndex = 82;
            this.fraPurchaseOrders.Text = "Purchase Order List";
            this.fraPurchaseOrders.Visible = false;
            // 
            // cboPurchaseOrderDepartment
            // 
            this.cboPurchaseOrderDepartment.BackColor = System.Drawing.SystemColors.Window;
            this.cboPurchaseOrderDepartment.Location = new System.Drawing.Point(180, 30);
            this.cboPurchaseOrderDepartment.Name = "cboPurchaseOrderDepartment";
            this.cboPurchaseOrderDepartment.Size = new System.Drawing.Size(169, 40);
            this.cboPurchaseOrderDepartment.TabIndex = 83;
            this.cboPurchaseOrderDepartment.SelectedIndexChanged += new System.EventHandler(this.cboPurchaseOrderDepartment_SelectedIndexChanged);
            // 
            // vsPurchaseOrders
            // 
            this.vsPurchaseOrders.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPurchaseOrders.Cols = 7;
            this.vsPurchaseOrders.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsPurchaseOrders.Location = new System.Drawing.Point(20, 90);
            this.vsPurchaseOrders.Name = "vsPurchaseOrders";
            this.vsPurchaseOrders.Rows = 50;
            this.vsPurchaseOrders.Size = new System.Drawing.Size(596, 346);
            this.vsPurchaseOrders.TabIndex = 84;
            this.vsPurchaseOrders.CurrentCellChanged += new System.EventHandler(this.vsPurchaseOrders_RowColChange);
            this.vsPurchaseOrders.Click += new System.EventHandler(this.vsPurchaseOrders_ClickEvent);
            this.vsPurchaseOrders.DoubleClick += new System.EventHandler(this.vsPurchaseOrders_DblClick);
            this.vsPurchaseOrders.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPurchaseOrders_KeyDownEvent);
            this.vsPurchaseOrders.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsPurchaseOrders_KeyPressEvent);
            // 
            // lblPurchaseOrderDepartment
            // 
            this.lblPurchaseOrderDepartment.Location = new System.Drawing.Point(20, 44);
            this.lblPurchaseOrderDepartment.Name = "lblPurchaseOrderDepartment";
            this.lblPurchaseOrderDepartment.Size = new System.Drawing.Size(90, 15);
            this.lblPurchaseOrderDepartment.TabIndex = 85;
            this.lblPurchaseOrderDepartment.Text = "DEPARTMENT";
            this.lblPurchaseOrderDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraAccountBalance
            // 
            this.fraAccountBalance.BackColor = System.Drawing.Color.White;
            this.fraAccountBalance.Controls.Add(this.cmdBalanceOK);
            this.fraAccountBalance.Controls.Add(this.lblBalPendingYTDNet);
            this.fraAccountBalance.Controls.Add(this.lblBalBalance);
            this.fraAccountBalance.Controls.Add(this.lblBalPostedYTDNet);
            this.fraAccountBalance.Controls.Add(this.lblBalNetBudget);
            this.fraAccountBalance.Controls.Add(this.Label6);
            this.fraAccountBalance.Controls.Add(this.Label5);
            this.fraAccountBalance.Controls.Add(this.Label4);
            this.fraAccountBalance.Controls.Add(this.Label3);
            this.fraAccountBalance.Controls.Add(this.lblBalAccount);
            this.fraAccountBalance.Location = new System.Drawing.Point(1235, 156);
            this.fraAccountBalance.Name = "fraAccountBalance";
            this.fraAccountBalance.Size = new System.Drawing.Size(312, 265);
            this.fraAccountBalance.TabIndex = 71;
            this.fraAccountBalance.Text = "Account Balance";
            this.fraAccountBalance.Visible = false;
            // 
            // cmdBalanceOK
            // 
            this.cmdBalanceOK.AppearanceKey = "actionButton";
            this.cmdBalanceOK.Location = new System.Drawing.Point(66, 205);
            this.cmdBalanceOK.Name = "cmdBalanceOK";
            this.cmdBalanceOK.Size = new System.Drawing.Size(74, 40);
            this.cmdBalanceOK.TabIndex = 77;
            this.cmdBalanceOK.Text = "OK";
            this.cmdBalanceOK.Click += new System.EventHandler(this.cmdBalanceOK_Click);
            // 
            // lblBalPendingYTDNet
            // 
            this.lblBalPendingYTDNet.AutoSize = true;
            this.lblBalPendingYTDNet.Location = new System.Drawing.Point(177, 135);
            this.lblBalPendingYTDNet.Name = "lblBalPendingYTDNet";
            this.lblBalPendingYTDNet.Size = new System.Drawing.Size(122, 15);
            this.lblBalPendingYTDNet.TabIndex = 81;
            this.lblBalPendingYTDNet.Text = "PENDING YTD NET";
            this.lblBalPendingYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBalBalance
            // 
            this.lblBalBalance.AutoSize = true;
            this.lblBalBalance.Location = new System.Drawing.Point(227, 170);
            this.lblBalBalance.Name = "lblBalBalance";
            this.lblBalBalance.Size = new System.Drawing.Size(65, 15);
            this.lblBalBalance.TabIndex = 80;
            this.lblBalBalance.Text = "BALANCE";
            this.lblBalBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBalPostedYTDNet
            // 
            this.lblBalPostedYTDNet.AutoSize = true;
            this.lblBalPostedYTDNet.Location = new System.Drawing.Point(178, 100);
            this.lblBalPostedYTDNet.Name = "lblBalPostedYTDNet";
            this.lblBalPostedYTDNet.Size = new System.Drawing.Size(117, 15);
            this.lblBalPostedYTDNet.TabIndex = 79;
            this.lblBalPostedYTDNet.Text = "POSTED YTD NET";
            this.lblBalPostedYTDNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBalNetBudget
            // 
            this.lblBalNetBudget.AutoSize = true;
            this.lblBalNetBudget.Location = new System.Drawing.Point(205, 65);
            this.lblBalNetBudget.Name = "lblBalNetBudget";
            this.lblBalNetBudget.Size = new System.Drawing.Size(87, 15);
            this.lblBalNetBudget.TabIndex = 78;
            this.lblBalNetBudget.Text = "99,999,999.00";
            this.lblBalNetBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(20, 135);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(122, 15);
            this.Label6.TabIndex = 76;
            this.Label6.Text = "PENDING YTD NET";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(75, 170);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(65, 15);
            this.Label5.TabIndex = 75;
            this.Label5.Text = "BALANCE";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(26, 100);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(117, 15);
            this.Label4.TabIndex = 74;
            this.Label4.Text = "POSTED YTD NET";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(53, 65);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 15);
            this.Label3.TabIndex = 73;
            this.Label3.Text = "NET BUDGET";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBalAccount
            // 
            this.lblBalAccount.AutoSize = true;
            this.lblBalAccount.Location = new System.Drawing.Point(71, 30);
            this.lblBalAccount.Name = "lblBalAccount";
            this.lblBalAccount.Size = new System.Drawing.Size(68, 15);
            this.lblBalAccount.TabIndex = 72;
            this.lblBalAccount.Text = "ACCOUNT";
            this.lblBalAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraJournalSave
            // 
            this.fraJournalSave.BackColor = System.Drawing.Color.White;
            this.fraJournalSave.Controls.Add(this.txtJournalDescription);
            this.fraJournalSave.Controls.Add(this.cmdCancelSave);
            this.fraJournalSave.Controls.Add(this.cmdOKSave);
            this.fraJournalSave.Controls.Add(this.cboSaveJournal);
            this.fraJournalSave.Controls.Add(this.lblJournalDescription);
            this.fraJournalSave.Controls.Add(this.lblJournalSave);
            this.fraJournalSave.Controls.Add(this.lblSaveInstructions);
            this.fraJournalSave.Location = new System.Drawing.Point(1172, 112);
            this.fraJournalSave.Name = "fraJournalSave";
            this.fraJournalSave.Size = new System.Drawing.Size(818, 270);
            this.fraJournalSave.TabIndex = 58;
            this.fraJournalSave.Text = "Save Journal";
            this.fraJournalSave.Visible = false;
            // 
            // txtJournalDescription
            // 
            this.txtJournalDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtJournalDescription.Location = new System.Drawing.Point(167, 150);
            this.txtJournalDescription.MaxLength = 100;
            this.txtJournalDescription.Name = "txtJournalDescription";
            this.txtJournalDescription.Size = new System.Drawing.Size(430, 40);
            this.txtJournalDescription.TabIndex = 61;
            // 
            // cmdCancelSave
            // 
            this.cmdCancelSave.AppearanceKey = "actionButton";
            this.cmdCancelSave.Location = new System.Drawing.Point(244, 210);
            this.cmdCancelSave.Name = "cmdCancelSave";
            this.cmdCancelSave.Size = new System.Drawing.Size(71, 40);
            this.cmdCancelSave.TabIndex = 63;
            this.cmdCancelSave.Text = "Cancel";
            this.cmdCancelSave.Click += new System.EventHandler(this.cmdCancelSave_Click);
            // 
            // cmdOKSave
            // 
            this.cmdOKSave.AppearanceKey = "actionButton";
            this.cmdOKSave.Location = new System.Drawing.Point(167, 210);
            this.cmdOKSave.Name = "cmdOKSave";
            this.cmdOKSave.Size = new System.Drawing.Size(57, 40);
            this.cmdOKSave.TabIndex = 62;
            this.cmdOKSave.Text = "OK";
            this.cmdOKSave.Click += new System.EventHandler(this.cmdOKSave_Click);
            // 
            // cboSaveJournal
            // 
            this.cboSaveJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboSaveJournal.Location = new System.Drawing.Point(167, 90);
            this.cboSaveJournal.Name = "cboSaveJournal";
            this.cboSaveJournal.Size = new System.Drawing.Size(430, 40);
            this.cboSaveJournal.TabIndex = 60;
            this.cboSaveJournal.SelectedIndexChanged += new System.EventHandler(this.cboSaveJournal_SelectedIndexChanged);
            this.cboSaveJournal.DropDown += new System.EventHandler(this.cboSaveJournal_DropDown);
            // 
            // lblJournalDescription
            // 
            this.lblJournalDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalDescription.Location = new System.Drawing.Point(20, 164);
            this.lblJournalDescription.Name = "lblJournalDescription";
            this.lblJournalDescription.Size = new System.Drawing.Size(88, 15);
            this.lblJournalDescription.TabIndex = 65;
            this.lblJournalDescription.Text = "DESCRIPTION";
            this.lblJournalDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJournalSave
            // 
            this.lblJournalSave.BackColor = System.Drawing.Color.Transparent;
            this.lblJournalSave.Location = new System.Drawing.Point(20, 104);
            this.lblJournalSave.Name = "lblJournalSave";
            this.lblJournalSave.Size = new System.Drawing.Size(63, 15);
            this.lblJournalSave.TabIndex = 64;
            this.lblJournalSave.Text = "JOURNAL";
            this.lblJournalSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaveInstructions
            // 
            this.lblSaveInstructions.BackColor = System.Drawing.Color.Transparent;
            this.lblSaveInstructions.Location = new System.Drawing.Point(20, 44);
            this.lblSaveInstructions.Name = "lblSaveInstructions";
            this.lblSaveInstructions.Size = new System.Drawing.Size(784, 15);
            this.lblSaveInstructions.TabIndex = 59;
            this.lblSaveInstructions.Text = "PLEASE SELECT THE JOUNAL YOU WISH TO SAVE THIS ENTRY IN, TYPE IN A DESCRIPTION FO" +
    "R THE JOURNAL, AND CLICK THE OK BUTTON";
            this.lblSaveInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.btnCancelEncumbrance);
            this.Frame2.Controls.Add(this.cboEncDept);
            this.Frame2.Controls.Add(this.vs3);
            this.Frame2.Controls.Add(this.lblDepartment);
            this.Frame2.Location = new System.Drawing.Point(1200, 10);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(840, 492);
            this.Frame2.TabIndex = 51;
            this.Frame2.Text = "Encumbrance List";
            this.Frame2.Visible = false;
            // 
            // btnCancelEncumbrance
            // 
            this.btnCancelEncumbrance.Anchor = Wisej.Web.AnchorStyles.Top;
            this.btnCancelEncumbrance.AppearanceKey = "actionButton";
            this.btnCancelEncumbrance.Location = new System.Drawing.Point(379, 438);
            this.btnCancelEncumbrance.Name = "btnCancelEncumbrance";
            this.btnCancelEncumbrance.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnCancelEncumbrance.Size = new System.Drawing.Size(126, 48);
            this.btnCancelEncumbrance.TabIndex = 71;
            this.btnCancelEncumbrance.Text = "Cancel";
            // 
            // cboEncDept
            // 
            this.cboEncDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboEncDept.Location = new System.Drawing.Point(180, 30);
            this.cboEncDept.Name = "cboEncDept";
            this.cboEncDept.Size = new System.Drawing.Size(169, 40);
            this.cboEncDept.TabIndex = 69;
            this.cboEncDept.SelectedIndexChanged += new System.EventHandler(this.cboEncDept_SelectedIndexChanged);
            // 
            // vs3
            // 
            this.vs3.Cols = 7;
            this.vs3.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vs3.FixedCols = 0;
            this.vs3.Location = new System.Drawing.Point(20, 90);
            this.vs3.Name = "vs3";
            this.vs3.RowHeadersVisible = false;
            this.vs3.Rows = 1;
            this.vs3.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vs3.Size = new System.Drawing.Size(800, 330);
            this.vs3.TabIndex = 52;
            this.vs3.CurrentCellChanged += new System.EventHandler(this.vs3_RowColChange);
            this.vs3.Click += new System.EventHandler(this.vs3_ClickEvent);
            this.vs3.DoubleClick += new System.EventHandler(this.vs3_DblClick);
            this.vs3.KeyDown += new Wisej.Web.KeyEventHandler(this.vs3_KeyDownEvent);
            this.vs3.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vs3_KeyPressEvent);
            // 
            // lblDepartment
            // 
            this.lblDepartment.Location = new System.Drawing.Point(20, 44);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(106, 15);
            this.lblDepartment.TabIndex = 70;
            this.lblDepartment.Text = "DEPARTMENT";
            this.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraEncDetail
            // 
            this.fraEncDetail.BackColor = System.Drawing.Color.White;
            this.fraEncDetail.Controls.Add(this.cmdSelect);
            this.fraEncDetail.Controls.Add(this.cmdNo);
            this.fraEncDetail.Controls.Add(this.cmdOk);
            this.fraEncDetail.Controls.Add(this.vs4);
            this.fraEncDetail.Controls.Add(this.lblInstructions);
            this.fraEncDetail.Location = new System.Drawing.Point(1142, 116);
            this.fraEncDetail.Name = "fraEncDetail";
            this.fraEncDetail.Size = new System.Drawing.Size(720, 406);
            this.fraEncDetail.TabIndex = 53;
            this.fraEncDetail.Text = "Encumbrance Detail Items";
            this.fraEncDetail.Visible = false;
            // 
            // cmdSelect
            // 
            this.cmdSelect.AppearanceKey = "actionButton";
            this.cmdSelect.Location = new System.Drawing.Point(112, 346);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(88, 40);
            this.cmdSelect.TabIndex = 66;
            this.cmdSelect.Text = "Select All";
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // cmdNo
            // 
            this.cmdNo.AppearanceKey = "actionButton";
            this.cmdNo.Location = new System.Drawing.Point(220, 346);
            this.cmdNo.Name = "cmdNo";
            this.cmdNo.Size = new System.Drawing.Size(72, 40);
            this.cmdNo.TabIndex = 57;
            this.cmdNo.Text = "Cancel";
            this.cmdNo.Click += new System.EventHandler(this.cmdNo_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.Location = new System.Drawing.Point(20, 346);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(72, 40);
            this.cmdOk.TabIndex = 56;
            this.cmdOk.Text = "Ok";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // vs4
            // 
            this.vs4.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vs4.Cols = 6;
            this.vs4.FixedCols = 0;
            this.vs4.Location = new System.Drawing.Point(20, 65);
            this.vs4.Name = "vs4";
            this.vs4.RowHeadersVisible = false;
            this.vs4.Rows = 1;
            this.vs4.Size = new System.Drawing.Size(680, 261);
            this.vs4.TabIndex = 54;
            this.vs4.Click += new System.EventHandler(this.vs4_ClickEvent);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(677, 15);
            this.lblInstructions.TabIndex = 55;
            this.lblInstructions.Text = "PLEASE CHECK ANY OF THE ITEMS YOU WOULD LIKE BROUGHT OVER TO THE A / P DATA ENTRY" +
    " SCREEN AND CLICK OK";
            this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmSearch
            // 
            this.frmSearch.BackColor = System.Drawing.Color.White;
            this.frmSearch.Controls.Add(this.cmdCancel);
            this.frmSearch.Controls.Add(this.cmdSearch);
            this.frmSearch.Controls.Add(this.txtSearch);
            this.frmSearch.Location = new System.Drawing.Point(1172, 22);
            this.frmSearch.Name = "frmSearch";
            this.frmSearch.Size = new System.Drawing.Size(300, 150);
            this.frmSearch.TabIndex = 33;
            this.frmSearch.Text = "Vendor Search";
            this.frmSearch.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(126, 90);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(96, 40);
            this.cmdCancel.TabIndex = 36;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.Location = new System.Drawing.Point(20, 90);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(96, 40);
            this.cmdSearch.TabIndex = 35;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(20, 30);
            this.txtSearch.MaxLength = 35;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(260, 40);
            this.txtSearch.TabIndex = 34;
            this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // frmInfo
            // 
            this.frmInfo.BackColor = System.Drawing.Color.White;
            this.frmInfo.Controls.Add(this.lstRecords);
            this.frmInfo.Controls.Add(this.cmdReturn);
            this.frmInfo.Controls.Add(this.cmdRetrieve);
            this.frmInfo.Controls.Add(this.lblVendorName);
            this.frmInfo.Controls.Add(this.lblRecordNumber);
            this.frmInfo.Location = new System.Drawing.Point(1172, 30);
            this.frmInfo.Name = "frmInfo";
            this.frmInfo.Size = new System.Drawing.Size(847, 347);
            this.frmInfo.TabIndex = 37;
            this.frmInfo.Text = "Multiple Records";
            this.frmInfo.Visible = false;
            // 
            // lstRecords
            // 
            this.lstRecords.BackColor = System.Drawing.SystemColors.Window;
            this.lstRecords.Location = new System.Drawing.Point(20, 65);
            this.lstRecords.Name = "lstRecords";
            this.lstRecords.Size = new System.Drawing.Size(807, 202);
            this.lstRecords.TabIndex = 38;
            this.lstRecords.DoubleClick += new System.EventHandler(this.lstRecords_DoubleClick);
            // 
            // cmdReturn
            // 
            this.cmdReturn.AppearanceKey = "actionButton";
            this.cmdReturn.Location = new System.Drawing.Point(439, 287);
            this.cmdReturn.Name = "cmdReturn";
            this.cmdReturn.Size = new System.Drawing.Size(113, 40);
            this.cmdReturn.TabIndex = 42;
            this.cmdReturn.Text = "Cancel ";
            this.cmdReturn.Click += new System.EventHandler(this.cmdReturn_Click);
            // 
            // cmdRetrieve
            // 
            this.cmdRetrieve.AppearanceKey = "actionButton";
            this.cmdRetrieve.Location = new System.Drawing.Point(271, 287);
            this.cmdRetrieve.Name = "cmdRetrieve";
            this.cmdRetrieve.Size = new System.Drawing.Size(148, 40);
            this.cmdRetrieve.TabIndex = 41;
            this.cmdRetrieve.Text = "Retrieve Record";
            this.cmdRetrieve.Click += new System.EventHandler(this.cmdRetrieve_Click);
            // 
            // lblVendorName
            // 
            this.lblVendorName.Location = new System.Drawing.Point(158, 30);
            this.lblVendorName.Name = "lblVendorName";
            this.lblVendorName.Size = new System.Drawing.Size(42, 15);
            this.lblVendorName.TabIndex = 40;
            this.lblVendorName.Text = "NAME";
            this.lblVendorName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRecordNumber
            // 
            this.lblRecordNumber.Location = new System.Drawing.Point(20, 30);
            this.lblRecordNumber.Name = "lblRecordNumber";
            this.lblRecordNumber.Size = new System.Drawing.Size(68, 15);
            this.lblRecordNumber.TabIndex = 39;
            this.lblRecordNumber.Text = "VENDOR #";
            this.lblRecordNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAddress_0
            // 
            this.txtAddress_0.Location = new System.Drawing.Point(715, 54);
            this.txtAddress_0.MaxLength = 50;
            this.txtAddress_0.Name = "txtAddress_0";
            this.txtAddress_0.Size = new System.Drawing.Size(343, 40);
            this.txtAddress_0.TabIndex = 7;
            this.txtAddress_0.Enter += new System.EventHandler(this.txtAddress_Enter);
            // 
            // txtVendor
            // 
            this.txtVendor.Location = new System.Drawing.Point(606, 54);
            this.txtVendor.MaxLength = 5;
            this.txtVendor.Name = "txtVendor";
            this.txtVendor.Size = new System.Drawing.Size(99, 40);
            this.txtVendor.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtVendor, "Enter E for Encumbrance List, S for Vendor Search, or A for Add Vendor");
            this.txtVendor.Enter += new System.EventHandler(this.txtVendor_Enter);
            this.txtVendor.TextChanged += new System.EventHandler(this.txtVendor_TextChanged);
            this.txtVendor.Validating += new System.ComponentModel.CancelEventHandler(this.txtVendor_Validate);
            this.txtVendor.KeyDown += new Wisej.Web.KeyEventHandler(this.txtVendor_KeyDownEvent);
            // 
            // txtCheck
            // 
            clientEvent1.Event = "keypress";
            this.javaScript1.GetJavaScriptEvents(this.txtCheck).Add(clientEvent1);
            this.txtCheck.Location = new System.Drawing.Point(400, 305);
            this.txtCheck.MaxLength = 6;
            this.txtCheck.Name = "txtCheck";
            this.txtCheck.Size = new System.Drawing.Size(153, 40);
            this.txtCheck.TabIndex = 18;
            this.txtCheck.Enter += new System.EventHandler(this.txtCheck_Enter);
            this.txtCheck.Validating += new System.ComponentModel.CancelEventHandler(this.txtCheck_Validate);
            this.txtCheck.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCheck_KeyPressEvent);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(142, 255);
            this.txtDescription.MaxLength = 25;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(435, 40);
            this.txtDescription.TabIndex = 15;
            this.txtDescription.Enter += new System.EventHandler(this.txtDescription_Enter);
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(142, 305);
            this.txtReference.MaxLength = 15;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(156, 40);
            this.txtReference.TabIndex = 16;
            this.txtReference.Enter += new System.EventHandler(this.txtReference_Enter);
            this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validate);
            // 
            // fraBorder
            // 
            this.fraBorder.AppearanceKey = "groupBoxNoBorders";
            this.fraBorder.Controls.Add(this.cboJournal);
            this.fraBorder.Controls.Add(this.Frame1);
            this.fraBorder.Controls.Add(this.chkSeperate);
            this.fraBorder.Controls.Add(this.txtPayable);
            this.fraBorder.Controls.Add(this.txtPeriod);
            this.fraBorder.Controls.Add(this.lblPayable);
            this.fraBorder.Controls.Add(this.lblJournal);
            this.fraBorder.Controls.Add(this.lblPeriod);
            this.fraBorder.Location = new System.Drawing.Point(30, 55);
            this.fraBorder.Name = "fraBorder";
            this.fraBorder.Size = new System.Drawing.Size(571, 190);
            this.fraBorder.TabIndex = 24;
            // 
            // cboJournal
            // 
            this.cboJournal.BackColor = System.Drawing.SystemColors.Window;
            this.cboJournal.Enabled = false;
            this.cboJournal.Location = new System.Drawing.Point(90, 0);
            this.cboJournal.Name = "cboJournal";
            this.cboJournal.Size = new System.Drawing.Size(278, 40);
            this.cboJournal.TabIndex = 0;
            this.cboJournal.SelectedIndexChanged += new System.EventHandler(this.cboJournal_SelectedIndexChanged);
            this.cboJournal.DropDown += new System.EventHandler(this.cboJournal_DropDown);
            this.cboJournal.Enter += new System.EventHandler(this.cboJournal_Enter);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cboFrequency);
            this.Frame1.Controls.Add(this.txtUntil);
            this.Frame1.Controls.Add(this.lblFrequency);
            this.Frame1.Controls.Add(this.lblDays);
            this.Frame1.Controls.Add(this.lblUntil);
            this.Frame1.Location = new System.Drawing.Point(243, 50);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(319, 130);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "Auto Payment";
            // 
            // cboFrequency
            // 
            this.cboFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.cboFrequency.Items.AddRange(new object[] {
            " ",
            "7",
            "14",
            "21",
            "30",
            "60",
            "90",
            "120",
            "150",
            "180",
            "365"});
            this.cboFrequency.Location = new System.Drawing.Point(127, 30);
            this.cboFrequency.Name = "cboFrequency";
            this.cboFrequency.Size = new System.Drawing.Size(132, 40);
            this.cboFrequency.TabIndex = 4;
            this.cboFrequency.Enter += new System.EventHandler(this.cboFrequency_Enter);
            // 
            // txtUntil
            // 
            this.txtUntil.Location = new System.Drawing.Point(127, 80);
            this.txtUntil.MaxLength = 10;
            this.txtUntil.Name = "txtUntil";
            this.txtUntil.Size = new System.Drawing.Size(133, 22);
            this.txtUntil.TabIndex = 5;
            this.txtUntil.Enter += new System.EventHandler(this.txtUntil_Enter);
            // 
            // lblFrequency
            // 
            this.lblFrequency.Location = new System.Drawing.Point(20, 44);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(101, 16);
            this.lblFrequency.TabIndex = 50;
            this.lblFrequency.Text = "FREQUENCY";
            this.lblFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDays
            // 
            this.lblDays.Location = new System.Drawing.Point(275, 44);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(38, 16);
            this.lblDays.TabIndex = 49;
            this.lblDays.Text = "DAYS";
            this.lblDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUntil
            // 
            this.lblUntil.Location = new System.Drawing.Point(20, 94);
            this.lblUntil.Name = "lblUntil";
            this.lblUntil.Size = new System.Drawing.Size(88, 16);
            this.lblUntil.TabIndex = 48;
            this.lblUntil.Text = "UNTIL";
            this.lblUntil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkSeperate
            // 
            this.chkSeperate.Location = new System.Drawing.Point(380, 4);
            this.chkSeperate.Name = "chkSeperate";
            this.chkSeperate.Size = new System.Drawing.Size(125, 22);
            this.chkSeperate.TabIndex = 1;
            this.chkSeperate.Text = "Separate Check";
            this.chkSeperate.CheckedChanged += new System.EventHandler(this.chkSeperate_CheckedChanged);
            // 
            // txtPayable
            // 
            this.txtPayable.Enabled = false;
            this.txtPayable.Location = new System.Drawing.Point(90, 100);
            this.txtPayable.MaxLength = 10;
            this.txtPayable.Name = "txtPayable";
            this.txtPayable.Size = new System.Drawing.Size(133, 22);
            this.txtPayable.TabIndex = 3;
            this.txtPayable.Enter += new System.EventHandler(this.txtPayable_Enter);
            this.txtPayable.Validating += new System.ComponentModel.CancelEventHandler(this.txtPayable_Validate);
            // 
            // txtPeriod
            // 
            this.txtPeriod.Enabled = false;
            this.txtPeriod.Location = new System.Drawing.Point(90, 50);
            this.txtPeriod.MaxLength = 2;
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.Size = new System.Drawing.Size(133, 40);
            this.txtPeriod.TabIndex = 2;
            this.txtPeriod.Enter += new System.EventHandler(this.txtPeriod_Enter);
            this.txtPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.txtPeriod_Validate);
            this.txtPeriod.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPeriod_KeyPressEvent);
            // 
            // lblPayable
            // 
            this.lblPayable.Location = new System.Drawing.Point(6, 114);
            this.lblPayable.Name = "lblPayable";
            this.lblPayable.Size = new System.Drawing.Size(59, 15);
            this.lblPayable.TabIndex = 46;
            this.lblPayable.Text = "PAYABLE";
            this.lblPayable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJournal
            // 
            this.lblJournal.Location = new System.Drawing.Point(0, 14);
            this.lblJournal.Name = "lblJournal";
            this.lblJournal.Size = new System.Drawing.Size(63, 15);
            this.lblJournal.TabIndex = 45;
            this.lblJournal.Text = "JOURNAL";
            this.lblJournal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPeriod
            // 
            this.lblPeriod.Location = new System.Drawing.Point(0, 64);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(53, 15);
            this.lblPeriod.TabIndex = 44;
            this.lblPeriod.Text = "PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAddress_1
            // 
            this.txtAddress_1.Location = new System.Drawing.Point(715, 104);
            this.txtAddress_1.MaxLength = 35;
            this.txtAddress_1.Name = "txtAddress_1";
            this.txtAddress_1.Size = new System.Drawing.Size(343, 40);
            this.txtAddress_1.TabIndex = 8;
            this.txtAddress_1.Enter += new System.EventHandler(this.txtAddress_Enter);
            // 
            // txtAddress_2
            // 
            this.txtAddress_2.Location = new System.Drawing.Point(715, 154);
            this.txtAddress_2.MaxLength = 35;
            this.txtAddress_2.Name = "txtAddress_2";
            this.txtAddress_2.Size = new System.Drawing.Size(343, 40);
            this.txtAddress_2.TabIndex = 9;
            this.txtAddress_2.Enter += new System.EventHandler(this.txtAddress_Enter);
            // 
            // txtAddress_3
            // 
            this.txtAddress_3.Location = new System.Drawing.Point(715, 204);
            this.txtAddress_3.MaxLength = 35;
            this.txtAddress_3.Name = "txtAddress_3";
            this.txtAddress_3.Size = new System.Drawing.Size(343, 40);
            this.txtAddress_3.TabIndex = 10;
            this.txtAddress_3.Enter += new System.EventHandler(this.txtAddress_Enter);
            // 
            // txtAmount2
            // 
            this.txtAmount2.Location = new System.Drawing.Point(142, 355);
            this.txtAmount2.MaxLength = 14;
            this.txtAmount2.Name = "txtAmount2";
            this.txtAmount2.Size = new System.Drawing.Size(156, 22);
            this.txtAmount2.TabIndex = 17;
            this.txtAmount2.Visible = false;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.Location = new System.Drawing.Point(142, 355);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(156, 40);
            this.txtAmount.TabIndex = 17;
            this.txtAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
            this.txtAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAmount_Validating);
            // 
            // chkUseAltCashAccount
            // 
            this.chkUseAltCashAccount.Location = new System.Drawing.Point(328, 366);
            this.chkUseAltCashAccount.Name = "chkUseAltCashAccount";
            this.chkUseAltCashAccount.Size = new System.Drawing.Size(81, 22);
            this.chkUseAltCashAccount.TabIndex = 20;
            this.chkUseAltCashAccount.Text = "Alt Cash";
            this.chkUseAltCashAccount.CheckedChanged += new System.EventHandler(this.chkUseAltCashAccount_CheckedChanged);
            // 
            // vsAltCashAccount
            // 
            this.vsAltCashAccount.Cols = 1;
            this.vsAltCashAccount.ColumnHeadersVisible = false;
            this.vsAltCashAccount.Enabled = false;
            this.vsAltCashAccount.FixedCols = 0;
            this.vsAltCashAccount.FixedRows = 0;
            this.vsAltCashAccount.Location = new System.Drawing.Point(448, 355);
            this.vsAltCashAccount.Name = "vsAltCashAccount";
            this.vsAltCashAccount.RowHeadersVisible = false;
            this.vsAltCashAccount.Rows = 1;
            this.vsAltCashAccount.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsAltCashAccount.Size = new System.Drawing.Size(210, 42);
            this.vsAltCashAccount.TabIndex = 21;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(994, 254);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(64, 40);
            this.txtZip4.TabIndex = 14;
            this.txtZip4.Enter += new System.EventHandler(this.txtZip4_Enter);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(914, 254);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(74, 40);
            this.txtZip.TabIndex = 13;
            this.txtZip.Enter += new System.EventHandler(this.txtZip_Enter);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Location = new System.Drawing.Point(851, 254);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(57, 40);
            this.txtState.TabIndex = 12;
            this.txtState.Enter += new System.EventHandler(this.txtState_Enter);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(715, 254);
            this.txtCity.MaxLength = 35;
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(130, 40);
            this.txtCity.TabIndex = 11;
            this.txtCity.Enter += new System.EventHandler(this.txtCity_Enter);
            // 
            // vs1
            // 
            this.vs1.Cols = 8;
            this.vs1.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vs1.Location = new System.Drawing.Point(30, 472);
            this.vs1.Name = "vs1";
            this.vs1.ReadOnly = false;
            this.vs1.Rows = 16;
            this.vs1.Size = new System.Drawing.Size(1011, 162);
            this.vs1.StandardTab = false;
            this.vs1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vs1.TabIndex = 22;
            this.vs1.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vs1_KeyPressEdit);
            this.vs1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vs1_ChangeEdit);
            this.vs1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vs1_ValidateEdit);
            this.vs1.CurrentCellChanged += new System.EventHandler(this.vs1_RowColChange);
            this.vs1.Enter += new System.EventHandler(this.vs1_Enter);
            this.vs1.Click += new System.EventHandler(this.vs1_ClickEvent);
            this.vs1.KeyDown += new Wisej.Web.KeyEventHandler(this.vs1_KeyDownEvent);
            // 
            // lblReadOnly
            // 
            this.lblReadOnly.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblReadOnly.Location = new System.Drawing.Point(30, 30);
            this.lblReadOnly.Name = "lblReadOnly";
            this.lblReadOnly.Size = new System.Drawing.Size(119, 15);
            this.lblReadOnly.TabIndex = 67;
            this.lblReadOnly.Text = "VIEW ONLY";
            this.lblReadOnly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblReadOnly.Visible = false;
            // 
            // lblExpense
            // 
            this.lblExpense.Location = new System.Drawing.Point(30, 448);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(500, 16);
            this.lblExpense.TabIndex = 32;
            this.lblExpense.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblExpense, "Right click to see account balance");
            this.lblExpense.MouseUp += new Wisej.Web.MouseEventHandler(this.lblExpense_MouseUp);
            // 
            // lblActualPayment
            // 
            this.lblActualPayment.Location = new System.Drawing.Point(398, 415);
            this.lblActualPayment.Name = "lblActualPayment";
            this.lblActualPayment.Size = new System.Drawing.Size(77, 15);
            this.lblActualPayment.TabIndex = 31;
            this.lblActualPayment.Text = "0.00";
            this.lblActualPayment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRemAmount
            // 
            this.lblRemAmount.Location = new System.Drawing.Point(157, 415);
            this.lblRemAmount.Name = "lblRemAmount";
            this.lblRemAmount.Size = new System.Drawing.Size(76, 15);
            this.lblRemAmount.TabIndex = 30;
            this.lblRemAmount.Text = "0.00";
            this.lblRemAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblVendor
            // 
            this.lblVendor.Location = new System.Drawing.Point(606, 33);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(73, 15);
            this.lblVendor.TabIndex = 29;
            this.lblVendor.Text = "VENDOR";
            this.lblVendor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 260);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(90, 16);
            this.lblDescription.TabIndex = 28;
            this.lblDescription.Text = "DESCRIPTION";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReference
            // 
            this.lblReference.Location = new System.Drawing.Point(30, 319);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(98, 16);
            this.lblReference.TabIndex = 27;
            this.lblReference.Text = "REFERENCE";
            this.lblReference.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(30, 369);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(62, 16);
            this.lblAmount.TabIndex = 26;
            this.lblAmount.Text = "AMOUNT";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCheck
            // 
            this.lblCheck.Location = new System.Drawing.Point(328, 319);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(53, 16);
            this.lblCheck.TabIndex = 25;
            this.lblCheck.Text = "CHECK#";
            this.lblCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRemaining
            // 
            this.lblRemaining.Location = new System.Drawing.Point(30, 415);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(136, 15);
            this.lblRemaining.TabIndex = 24;
            this.lblRemaining.Text = "REMAINING AMOUNT";
            this.lblRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblActual
            // 
            this.lblActual.Location = new System.Drawing.Point(281, 415);
            this.lblActual.Name = "lblActual";
            this.lblActual.Size = new System.Drawing.Size(136, 15);
            this.lblActual.TabIndex = 22;
            this.lblActual.Text = "ACTUAL PAYMENT";
            this.lblActual.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessAccounts,
            this.mnuFileChangePayableDate,
            this.mnuProcessDelete,
            this.mnuProcessDeleteEntry,
            this.mnuProcessEncumbrance,
            this.mnuFileShowPurchaseOrders,
            this.mnuViewAttachedDocuments,
            this.mnuFileSep2,
            this.mnuFileAddVendor,
            this.mnuFileSep,
            this.mnuFileDisplayAmount});
            this.MainMenu1.Name = null;
            // 
            // mnuProcessAccounts
            // 
            this.mnuProcessAccounts.Enabled = false;
            this.mnuProcessAccounts.Index = 0;
            this.mnuProcessAccounts.Name = "mnuProcessAccounts";
            this.mnuProcessAccounts.Text = "Show Valid Accounts";
            this.mnuProcessAccounts.Visible = false;
            // 
            // mnuFileChangePayableDate
            // 
            this.mnuFileChangePayableDate.Enabled = false;
            this.mnuFileChangePayableDate.Index = 1;
            this.mnuFileChangePayableDate.Name = "mnuFileChangePayableDate";
            this.mnuFileChangePayableDate.Text = "Change Payable Date";
            this.mnuFileChangePayableDate.Visible = false;
            this.mnuFileChangePayableDate.Click += new System.EventHandler(this.mnuFileChangePayableDate_Click);
            // 
            // mnuProcessDelete
            // 
            this.mnuProcessDelete.Enabled = false;
            this.mnuProcessDelete.Index = 2;
            this.mnuProcessDelete.Name = "mnuProcessDelete";
            this.mnuProcessDelete.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuProcessDelete.Text = "Delete Detail Item";
            this.mnuProcessDelete.Click += new System.EventHandler(this.mnuProcessDelete_Click);
            // 
            // mnuProcessDeleteEntry
            // 
            this.mnuProcessDeleteEntry.Enabled = false;
            this.mnuProcessDeleteEntry.Index = 3;
            this.mnuProcessDeleteEntry.Name = "mnuProcessDeleteEntry";
            this.mnuProcessDeleteEntry.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuProcessDeleteEntry.Text = "Delete Journal Entry";
            this.mnuProcessDeleteEntry.Click += new System.EventHandler(this.mnuProcessDeleteEntry_Click);
            // 
            // mnuProcessEncumbrance
            // 
            this.mnuProcessEncumbrance.Index = 4;
            this.mnuProcessEncumbrance.Name = "mnuProcessEncumbrance";
            this.mnuProcessEncumbrance.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuProcessEncumbrance.Text = "Show Encumbrance List";
            this.mnuProcessEncumbrance.Click += new System.EventHandler(this.mnuProcessEncumbrance_Click);
            // 
            // mnuFileShowPurchaseOrders
            // 
            this.mnuFileShowPurchaseOrders.Index = 5;
            this.mnuFileShowPurchaseOrders.Name = "mnuFileShowPurchaseOrders";
            this.mnuFileShowPurchaseOrders.Shortcut = Wisej.Web.Shortcut.CtrlP;
            this.mnuFileShowPurchaseOrders.Text = "Show Purchase Order List";
            this.mnuFileShowPurchaseOrders.Visible = false;
            this.mnuFileShowPurchaseOrders.Click += new System.EventHandler(this.mnuFileShowPurchaseOrders_Click);
            // 
            // mnuViewAttachedDocuments
            // 
            this.mnuViewAttachedDocuments.Index = 6;
            this.mnuViewAttachedDocuments.Name = "mnuViewAttachedDocuments";
            this.mnuViewAttachedDocuments.Text = "View Attached Documents";
            this.mnuViewAttachedDocuments.Click += new System.EventHandler(this.mnuViewAttachedDocuments_Click);
            // 
            // mnuFileSep2
            // 
            this.mnuFileSep2.Index = 7;
            this.mnuFileSep2.Name = "mnuFileSep2";
            this.mnuFileSep2.Text = "-";
            // 
            // mnuFileAddVendor
            // 
            this.mnuFileAddVendor.Index = 8;
            this.mnuFileAddVendor.Name = "mnuFileAddVendor";
            this.mnuFileAddVendor.Text = "Add Vendor";
            this.mnuFileAddVendor.Click += new System.EventHandler(this.mnuFileAddVendor_Click);
            // 
            // mnuFileSep
            // 
            this.mnuFileSep.Index = 9;
            this.mnuFileSep.Name = "mnuFileSep";
            this.mnuFileSep.Text = "-";
            // 
            // mnuFileDisplayAmount
            // 
            this.mnuFileDisplayAmount.Index = 10;
            this.mnuFileDisplayAmount.Name = "mnuFileDisplayAmount";
            this.mnuFileDisplayAmount.Text = "Display Journal Amount";
            this.mnuFileDisplayAmount.Click += new System.EventHandler(this.mnuFileDisplayAmount_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSearch
            // 
            this.mnuProcessSearch.Index = -1;
            this.mnuProcessSearch.Name = "mnuProcessSearch";
            this.mnuProcessSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuProcessSearch.Text = "Vendor Search";
            this.mnuProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // mnuProcessPreviousEntry
            // 
            this.mnuProcessPreviousEntry.Index = -1;
            this.mnuProcessPreviousEntry.Name = "mnuProcessPreviousEntry";
            this.mnuProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuProcessPreviousEntry.Text = "Previous Journal Entry";
            this.mnuProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
            // 
            // Seperator1
            // 
            this.Seperator1.Index = -1;
            this.Seperator1.Name = "Seperator1";
            this.Seperator1.Text = "-";
            // 
            // mnuProcessNextEntry
            // 
            this.mnuProcessNextEntry.Index = -1;
            this.mnuProcessNextEntry.Name = "mnuProcessNextEntry";
            this.mnuProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuProcessNextEntry.Text = "Next Journal Entry";
            this.mnuProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
            // 
            // mnuFileVendorDetail
            // 
            this.mnuFileVendorDetail.Index = -1;
            this.mnuFileVendorDetail.Name = "mnuFileVendorDetail";
            this.mnuFileVendorDetail.Shortcut = Wisej.Web.Shortcut.F9;
            this.mnuFileVendorDetail.Text = "Vendor Detail ";
            this.mnuFileVendorDetail.Click += new System.EventHandler(this.mnuFileVendorDetail_Click);
            // 
            // mnuFileSep3
            // 
            this.mnuFileSep3.Index = -1;
            this.mnuFileSep3.Name = "mnuFileSep3";
            this.mnuFileSep3.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = -1;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = -1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Continue";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = -1;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // btnProcessSave
            // 
            this.btnProcessSave.AppearanceKey = "acceptButton";
            this.btnProcessSave.Location = new System.Drawing.Point(480, 30);
            this.btnProcessSave.Name = "btnProcessSave";
            this.btnProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcessSave.Size = new System.Drawing.Size(174, 48);
            this.btnProcessSave.TabIndex = 23;
            this.btnProcessSave.Text = "Save & Continue";
            this.btnProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // btnFileSave
            // 
            this.btnFileSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileSave.Location = new System.Drawing.Point(1001, 29);
            this.btnFileSave.Name = "btnFileSave";
            this.btnFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnFileSave.Size = new System.Drawing.Size(47, 24);
            this.btnFileSave.TabIndex = 1;
            this.btnFileSave.Text = "Save";
            this.btnFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // btnFileVendorDetail
            // 
            this.btnFileVendorDetail.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnFileVendorDetail.Location = new System.Drawing.Point(896, 29);
            this.btnFileVendorDetail.Name = "btnFileVendorDetail";
            this.btnFileVendorDetail.Shortcut = Wisej.Web.Shortcut.F9;
            this.btnFileVendorDetail.Size = new System.Drawing.Size(102, 24);
            this.btnFileVendorDetail.TabIndex = 2;
            this.btnFileVendorDetail.Text = "Vendor Detail";
            this.btnFileVendorDetail.Click += new System.EventHandler(this.mnuFileVendorDetail_Click);
            // 
            // btnProcessNextEntry
            // 
            this.btnProcessNextEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessNextEntry.Location = new System.Drawing.Point(763, 29);
            this.btnProcessNextEntry.Name = "btnProcessNextEntry";
            this.btnProcessNextEntry.Shortcut = Wisej.Web.Shortcut.F8;
            this.btnProcessNextEntry.Size = new System.Drawing.Size(130, 24);
            this.btnProcessNextEntry.TabIndex = 3;
            this.btnProcessNextEntry.Text = "Next Journal Entry";
            this.btnProcessNextEntry.Click += new System.EventHandler(this.mnuProcessNextEntry_Click);
            // 
            // btnProcessPreviousEntry
            // 
            this.btnProcessPreviousEntry.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessPreviousEntry.Location = new System.Drawing.Point(605, 29);
            this.btnProcessPreviousEntry.Name = "btnProcessPreviousEntry";
            this.btnProcessPreviousEntry.Shortcut = Wisej.Web.Shortcut.F7;
            this.btnProcessPreviousEntry.Size = new System.Drawing.Size(154, 24);
            this.btnProcessPreviousEntry.TabIndex = 4;
            this.btnProcessPreviousEntry.Text = "Previous Journal Entry";
            this.btnProcessPreviousEntry.Click += new System.EventHandler(this.mnuProcessPreviousEntry_Click);
            // 
            // btnProcessSearch
            // 
            this.btnProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnProcessSearch.Location = new System.Drawing.Point(492, 29);
            this.btnProcessSearch.Name = "btnProcessSearch";
            this.btnProcessSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.btnProcessSearch.Size = new System.Drawing.Size(109, 24);
            this.btnProcessSearch.TabIndex = 5;
            this.btnProcessSearch.Text = "Vendor Search";
            this.btnProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // frmAPDataEntry
            // 
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 598);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmAPDataEntry";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Invoice Entry";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmAPDataEntry_Load);
            this.Activated += new System.EventHandler(this.frmAPDataEntry_Activated);
            this.Resize += new System.EventHandler(this.frmAPDataEntry_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAPDataEntry_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAPDataEntry_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPurchaseOrders)).EndInit();
            this.fraPurchaseOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsPurchaseOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountBalance)).EndInit();
            this.fraAccountBalance.ResumeLayout(false);
            this.fraAccountBalance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalanceOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraJournalSave)).EndInit();
            this.fraJournalSave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelEncumbrance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEncDetail)).EndInit();
            this.fraEncDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmSearch)).EndInit();
            this.frmSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frmInfo)).EndInit();
            this.frmInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRetrieve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBorder)).EndInit();
            this.fraBorder.ResumeLayout(false);
            this.fraBorder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUntil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseAltCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAltCashAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vs1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFileVendorDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessNextEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessPreviousEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcessSearch)).EndInit();
            this.ResumeLayout(false);

        }
		#endregion

		private System.ComponentModel.IContainer components;
        private FCButton btnProcessSave;
        private FCButton btnFileSave;
        private FCButton btnFileVendorDetail;
        public FCButton btnProcessNextEntry;
        public FCButton btnProcessPreviousEntry;
        public FCButton btnProcessSearch;
        private JavaScript javaScript1;
        private FCButton btnCancelEncumbrance;
    }
}